﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Lookup; 

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api
{
    public class KbbCalculator : IKbbCalculator
    {
        private static IResolver _resolver;
        private const int BookOutTypeId = 1;

        static KbbCalculator()
        {
            _resolver = RegistryFactory.GetResolver();
        }

        public Matrix Calculate(IVehicleConfiguration configuration)
        {
            IKbbService service = Resolve<IKbbService>();

            Vehicle vehicle = service.Vehicle(configuration.VehicleId);

            int mileage = configuration.Mileage.HasValue ? configuration.Mileage.Value : vehicle.Year.MileageZeroPoint;

            return Calculate(vehicle, configuration, configuration.Region, mileage);
        }

        public Matrix Calculate(IVehicleConfiguration configuration,IList<string> equipmentSelectedList )
        {
            IKbbService service = Resolve<IKbbService>();

            Vehicle vehicle = service.Vehicle(configuration.VehicleId);

            int mileage = configuration.Mileage.HasValue ? configuration.Mileage.Value : vehicle.Year.MileageZeroPoint;

            return Calculate(vehicle, equipmentSelectedList, configuration, configuration.Region, mileage);
        }
        
        static Matrix Calculate(Vehicle vehicle, IVehicleConfiguration configuration, Region region, int mileage)
        {
            Matrix matrix = new Matrix();

            CalculateBase(configuration.BookDate.Id, vehicle, region, matrix);

            CalculateOptions(vehicle, configuration, region, matrix);

            CalculateMileage(configuration.BookDate.Id, vehicle, mileage, matrix);

            CalculateFinal(vehicle, matrix);

            CalculateVisibility(matrix);

            return matrix;
        }

        static Matrix Calculate(Vehicle vehicle,IList<string> equipmentSelectedList, IVehicleConfiguration configuration, Region region, int mileage)
        {
            Matrix matrix = new Matrix();

            CalculateBase(configuration.BookDate.Id, vehicle, region, matrix);

            CalculateOptions(vehicle, equipmentSelectedList, configuration, region, matrix);

            CalculateMileage(configuration.BookDate.Id, vehicle, mileage, matrix);

            CalculateFinal(vehicle, matrix);

            CalculateVisibility(matrix);

            return matrix;
        }

        // This method is created to avoid text files.(Text file converted to stored procedure)
        // It is replacement of method Calculate.
        public Matrix CalculateData(IVehicleConfiguration configuration, int dealerId)
        {
            IKbbService service = Resolve<IKbbService>();

            Vehicle vehicle = service.Vehicle(configuration.VehicleId);

            return CalculateData(vehicle, configuration, configuration.Region, dealerId, BookOutTypeId);
        }

        // This method is created to avoid text files.(Text file converted to stored procedure)
        // It is replacement of method Calculate.
        public Matrix CalculateData(IVehicleConfiguration configuration,IList<string> equipmentSelectedList , int dealerId)
        {
            IKbbService service = Resolve<IKbbService>();

            Vehicle vehicle = service.Vehicle(configuration.VehicleId);

            return CalculateData(vehicle,equipmentSelectedList, configuration, configuration.Region, dealerId, BookOutTypeId);
        }

        /// <summary>
        /// Calculate KBB price
        /// </summary>
        /// <param name="vehicle"></param>
        /// <param name="equipmentSelectedList"></param>
        /// <param name="configuration"></param>
        /// <param name="region"></param>
        /// <param name="dealerId"></param>
        /// <param name="BookOutTypeId"></param>
        /// <returns></returns>
        static Matrix CalculateData(Vehicle vehicle, IList<string> equipmentSelectedList, IVehicleConfiguration configuration, Region region, int dealerId, int BookOutTypeId)
        {
            Matrix matrix = new Matrix();

            IList<FetchValuationResultsDto> fetchValuationResultDtos = CalculateNewBase(configuration.BookDate.Id, vehicle, matrix, configuration.Mileage, dealerId, BookOutTypeId);
            CalculateOptions(vehicle,equipmentSelectedList,configuration, region, matrix);
            SetMileage(fetchValuationResultDtos.Where(vr => vr.CategoryId == PriceType.Mileage).FirstOrDefault().Adjustment, matrix);
            CalculateFinal(vehicle, matrix);
            CalculateVisibility(matrix);

            return matrix;
        }

        static Matrix CalculateData(Vehicle vehicle, IVehicleConfiguration configuration, Region region, int dealerId, int BookOutTypeId)
        {
            Matrix matrix = new Matrix();

            IList<FetchValuationResultsDto> fetchValuationResultDtos = CalculateNewBase(configuration.BookDate.Id, vehicle, matrix, configuration.Mileage, dealerId, BookOutTypeId);
            CalculateOptions(vehicle, configuration, region, matrix);
            SetMileage(fetchValuationResultDtos.Where(vr => vr.CategoryId == PriceType.Mileage).FirstOrDefault().Adjustment, matrix);
            CalculateFinal(vehicle, matrix);
            CalculateVisibility(matrix);

            return matrix;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookDate"></param>
        /// <param name="vehicle"></param>
        /// <param name="matrix"></param>
        /// <param name="mileage"></param>
        /// <param name="dealerId"></param>
        /// <param name="bookOutTypeId"></param>
        /// <returns>Returns Adjustments for Mileage,Adjustment,ModelYear in List from Stored Proc</returns>
        static IList<FetchValuationResultsDto> CalculateNewBase(int bookDate, Vehicle vehicle, Matrix matrix, int? mileage, int dealerId, int bookOutTypeId)
        {
            ILookupBasePrice priceLookup = Resolve<ILookupBasePrice>();
            int year = Convert.ToInt32(vehicle.Year.Name);
            IList<FetchValuationResultsDto> fetchValuationResultDtos = priceLookup.Lookup(bookDate, vehicle.Id, new Region { Id = Region.NationalBaseRegionId }, mileage, dealerId, year, bookOutTypeId);

            foreach (FetchValuationResultsDto baseValue in fetchValuationResultDtos)
            {
                PriceType type = baseValue.CategoryId;
                decimal value = baseValue.Adjustment;

                // RegionalBaseValue is truncated, not rounded
                matrix[type, Valuation.Base].Value = Math.Truncate(value);
            }

            return fetchValuationResultDtos.Where(vr => vr.CategoryId == PriceType.Mileage || vr.CategoryId == PriceType.Adjustment || vr.CategoryId == PriceType.ModelYear).ToList();

        }

        // set mileage for each priceType(mileage parameter got from stored procedure)
        static void SetMileage(decimal mileage, Matrix matrix)
        {
            foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
            {
                matrix[priceType, Valuation.Mileage].Value = matrix[priceType, Valuation.Base].Value > 0 ? mileage : 0;
            }

        }
        
        static void CalculateBase(int bookDate, IKeyedValue vehicle, Region region, Matrix matrix)
        {
            ILookupBasePrice priceLookup = Resolve<ILookupBasePrice>();
            IList<Price> nationalBaseValues = priceLookup.Lookup(bookDate, vehicle.Id, new Region { Id = Region.NationalBaseRegionId });

            Price valuationBase = new Price {PriceType = PriceType.Undefined, RegionAdjustmentTypeId = 0, Value = 0};
            foreach (Price p in nationalBaseValues.Where(p => p.PriceType == PriceType.ValuationBase))
            {
                valuationBase = p;
            }

            ILookupRegionAdjustment adjustmentLookup = Resolve<ILookupRegionAdjustment>();
            IList<RegionAdjustment> adjustments = adjustmentLookup.Lookup(_resolver, bookDate, vehicle.Id);

            IList<RegionAdjustment> regAdjustments = adjustments.Where(ra => ra.RegionId == region.Id).ToList();

            foreach (Price baseValue in nationalBaseValues)
            {
                PriceType type = baseValue.PriceType;
                decimal value = baseValue.Value;

                RegionAdjustment adjustment = regAdjustments.FirstOrDefault(regionAdjustment => regionAdjustment.RegionAdjustmentTypeId == baseValue.RegionAdjustmentTypeId);

                // If the base value is 0, or there is no region adjustment, leave the value unadjusted.
                if ((adjustment == null) || (value <= 0))
                {
                    // add valuationBase unchanged (to be used in mileage adjustment calcs)
                    matrix[type, Valuation.Base].Value = value;
                    continue;
                }

                if (type == PriceType.ValuationBase)
                {
                    // add valuationBase unchanged (to be used in mileage adjustment calcs)
                    matrix[type, Valuation.Base].Value = value;
                    continue;
                }
                
                switch(adjustment.AdjustmentType)
                {
                    case AdjustmentType.Percent:
                        value += valuationBase.Value * adjustment.Factor;
                        break;
                    case AdjustmentType.Value:
                        value += adjustment.Factor;
                        break;
                }

                // RegionalBaseValue is truncated, not rounded
                matrix[type, Valuation.Base].Value = Math.Truncate(value);
            }
        }
        /// <summary>
        /// Caculate options based on Selected Equipments passed.
        /// </summary>
        /// <param name="vehicle"></param>
        /// <param name="equipmentSelectedList"></param>
        /// <param name="configuration"></param>
        /// <param name="region"></param>
        /// <param name="matrix"></param>
        static void CalculateOptions(Vehicle vehicle, IList<string> equipmentSelectedList,IVehicleConfiguration configuration, Region region, Matrix matrix)
        {
            ILookupOptionAdjustment lookup = Resolve<ILookupOptionAdjustment>();

            foreach (VehicleOptionAdjustment adjustment in lookup.Lookup(configuration.BookDate.Id, vehicle.Id))
            {
                VehicleOptionAdjustment adj = adjustment; // no warnings plz

                //IOptionState state = equipmentSelectedList.OptionStates.FirstOrDefault(
                //    x => x.OptionId == adj.OptionId);
                string selectedEquipment=equipmentSelectedList.FirstOrDefault(x => x == adj.OptionId.ToString());

                VehicleOption option = vehicle.Options.FirstOrDefault(
                    x => x.Id == adj.OptionId);

                if (option == null)
                {
                    continue;
                }

                bool adjust = false;

                // Selected Options:
                // Ignore selected "Standard" options. The value associated with those options is negative, meaning 
                // it's what you need to deduct from the vehicle value if they are not selected. When they're selected,
                // though, ignore them.
                // "Available" options have positive values, and do need to be added in if selected.                
                if (selectedEquipment!=null)
                {
                    if (option.Availability == VehicleOptionAvailability.Available)
                    {
                        adjust = true;
                    }
                }

                // Unselected Options:
                // If a "Standard" option is not selected, its value needs to be taken off the vehicle UNLESS the 
                // reason it's not selected is because a conflicting option has been turned on. For example: No 
                // standard "Moon Roof"? Deduct $400 unless it's off because "Panorama Roof" is on.
                // If an "Available" option is not selected, just ignore it.
                else
                {
                    if (option.Availability == VehicleOptionAvailability.Standard)
                    {
                        // "Conflicts with" rule update - add all standard options that are not selected
                        adjust = true; //!HasSelectedRelatedOption(configuration.OptionStates, option, VehicleOptionRelationshipType.ConflictsWith);
                    }
                }

                // Update the option adjustment total.
                if (adjust)
                {
                    matrix[adj.Adjustment.PriceType, Valuation.Option].Value =
                        Sum(matrix[adj.Adjustment.PriceType, Valuation.Option].Value, adj.Adjustment.Value);


                    // Calculating adjustment values for high and low range.
                    switch (adj.Adjustment.PriceType)
                    {
                        case PriceType.TradeInGood:
                            matrix[PriceType.TradeInRangeHighGood, Valuation.Option].Value =
                       Sum(matrix[PriceType.TradeInRangeHighGood, Valuation.Option].Value, adj.Adjustment.Value);
                            matrix[PriceType.TradeInRangeLowGood, Valuation.Option].Value =
                       Sum(matrix[PriceType.TradeInRangeLowGood, Valuation.Option].Value, adj.Adjustment.Value);
                            break;

                        case PriceType.TradeInFair:
                            matrix[PriceType.TradeInRangeHighFair, Valuation.Option].Value =
                      Sum(matrix[PriceType.TradeInRangeHighFair, Valuation.Option].Value, adj.Adjustment.Value);
                            matrix[PriceType.TradeInRangeLowFair, Valuation.Option].Value =
                       Sum(matrix[PriceType.TradeInRangeLowFair, Valuation.Option].Value, adj.Adjustment.Value);
                            break;


                        case PriceType.TradeInExcellent:
                            matrix[PriceType.TradeInRangeHighExcellent, Valuation.Option].Value =
                      Sum(matrix[PriceType.TradeInRangeHighExcellent, Valuation.Option].Value, adj.Adjustment.Value);
                            matrix[PriceType.TradeInRangeLowExcellent, Valuation.Option].Value =
                       Sum(matrix[PriceType.TradeInRangeLowExcellent, Valuation.Option].Value, adj.Adjustment.Value);
                            break;


                        case PriceType.TradeInVeryGood:
                            matrix[PriceType.TradeInVeryGoodRangeHigh, Valuation.Option].Value =
                      Sum(matrix[PriceType.TradeInVeryGoodRangeHigh, Valuation.Option].Value, adj.Adjustment.Value);
                            matrix[PriceType.TradeInVeryGoodRangeLow, Valuation.Option].Value =
                       Sum(matrix[PriceType.TradeInVeryGoodRangeLow, Valuation.Option].Value, adj.Adjustment.Value);
                            break;
                    }

                }
            }
        }

        static void CalculateOptions(Vehicle vehicle, IVehicleConfiguration configuration, Region region, Matrix matrix)
        {
            ILookupOptionAdjustment lookup = Resolve<ILookupOptionAdjustment>();

            foreach (VehicleOptionAdjustment adjustment in lookup.Lookup(configuration.BookDate.Id, vehicle.Id))
            {
                VehicleOptionAdjustment adj = adjustment; // no warnings plz

                IOptionState state = configuration.OptionStates.FirstOrDefault(
                    x => x.OptionId == adj.OptionId);

                VehicleOption option = vehicle.Options.FirstOrDefault(
                    x => x.Id == adj.OptionId);

                if (state == null || option == null)
                {
                    continue;
                }

                bool adjust = false;

                // Selected Options:
                // Ignore selected "Standard" options. The value associated with those options is negative, meaning 
                // it's what you need to deduct from the vehicle value if they are not selected. When they're selected,
                // though, ignore them.
                // "Available" options have positive values, and do need to be added in if selected.                
                if (state.Selected)
                {
                    if (option.Availability == VehicleOptionAvailability.Available)
                    {                                                
                        adjust = true;
                    }                    
                }

                // Unselected Options:
                // If a "Standard" option is not selected, its value needs to be taken off the vehicle UNLESS the 
                // reason it's not selected is because a conflicting option has been turned on. For example: No 
                // standard "Moon Roof"? Deduct $400 unless it's off because "Panorama Roof" is on.
                // If an "Available" option is not selected, just ignore it.
                else
                {
                    if (option.Availability == VehicleOptionAvailability.Standard)
                    {
                        // "Conflicts with" rule update - add all standard options that are not selected
                        adjust = true; //!HasSelectedRelatedOption(configuration.OptionStates, option, VehicleOptionRelationshipType.ConflictsWith);
                    }
                }

                // Update the option adjustment total.
                if (adjust)
                {
                    matrix[adj.Adjustment.PriceType, Valuation.Option].Value = 
                        Sum(matrix[adj.Adjustment.PriceType, Valuation.Option].Value, adj.Adjustment.Value);


                    // Calculating adjustment values for high and low range.
                    switch (adj.Adjustment.PriceType)
                    {
                        case PriceType.TradeInGood:
                            matrix[PriceType.TradeInRangeHighGood, Valuation.Option].Value =
                       Sum(matrix[PriceType.TradeInRangeHighGood, Valuation.Option].Value, adj.Adjustment.Value);
                            matrix[PriceType.TradeInRangeLowGood, Valuation.Option].Value =
                       Sum(matrix[PriceType.TradeInRangeLowGood, Valuation.Option].Value, adj.Adjustment.Value);
                            break;

                        case PriceType.TradeInFair:
                            matrix[PriceType.TradeInRangeHighFair, Valuation.Option].Value =
                      Sum(matrix[PriceType.TradeInRangeHighFair, Valuation.Option].Value, adj.Adjustment.Value);
                            matrix[PriceType.TradeInRangeLowFair, Valuation.Option].Value =
                       Sum(matrix[PriceType.TradeInRangeLowFair, Valuation.Option].Value, adj.Adjustment.Value);
                            break;


                        case PriceType.TradeInExcellent:
                            matrix[PriceType.TradeInRangeHighExcellent, Valuation.Option].Value =
                      Sum(matrix[PriceType.TradeInRangeHighExcellent, Valuation.Option].Value, adj.Adjustment.Value);
                            matrix[PriceType.TradeInRangeLowExcellent, Valuation.Option].Value =
                       Sum(matrix[PriceType.TradeInRangeLowExcellent, Valuation.Option].Value, adj.Adjustment.Value);
                            break;


                        case PriceType.TradeInVeryGood:
                            matrix[PriceType.TradeInVeryGoodRangeHigh, Valuation.Option].Value =
                      Sum(matrix[PriceType.TradeInVeryGoodRangeHigh, Valuation.Option].Value, adj.Adjustment.Value);
                            matrix[PriceType.TradeInVeryGoodRangeLow, Valuation.Option].Value =
                       Sum(matrix[PriceType.TradeInVeryGoodRangeLow, Valuation.Option].Value, adj.Adjustment.Value);
                            break;
                    }

                }
            }
        }

        private static bool HasSelectedRelatedOption(IEnumerable<IOptionState> states, VehicleOption option, VehicleOptionRelationshipType relationshipType)
        {
            VehicleOptionRelationship relation = option.Relationships.FirstOrDefault(
                x => x.RelationshipType == relationshipType);

            bool selected = false;

            if (relation != null)
            {
                foreach (VehicleOption relatedOption in relation.RelatedOptions)
                {
                    int id = relatedOption.Id; // no warning plz

                    IOptionState relatedState = states.FirstOrDefault(
                        x => x.OptionId == id);

                    if (relatedState != null)
                    {
                        selected |= relatedState.Selected;

                        break;
                    }
                }
            }

            return selected;
        }

        static void CalculateMileage(int bookDate, Vehicle vehicle, int mileage, Matrix matrix)
        {
            ILookupMileageAdjustment lookup = Resolve<ILookupMileageAdjustment>();

            IList<MileageAdjustment> adjustments = lookup.Lookup(
                bookDate, 
                vehicle.VehicleType,
                vehicle.Year.Id,
                vehicle.MileageGroupId);

            foreach (MileageAdjustment adjustment in adjustments)
            {
                if (!adjustment.Covers(mileage))
                {
                    continue;
                }

                decimal value = 0m;
                decimal basePrice = matrix[PriceType.ValuationBase, Valuation.Base].Value.GetValueOrDefault(0);

                // NOTE: see KBB.Kbb.GetValues sproc to match calculations between legacy and VVG
                switch (adjustment.AdjustmentType)
                {
                    case AdjustmentType.Percent:
                        value = Math.Truncate(basePrice*adjustment.Adjustment);
                        break;
                    case AdjustmentType.Value:
                        value = Math.Truncate(basePrice + adjustment.Adjustment);
                        break;
                }

                foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
                {
                    matrix[priceType, Valuation.Mileage].Value = matrix[priceType, Valuation.Base].Value > 0 ? value : 0;
                }
            }
        }

        static void CalculateFinal(Vehicle vehicle, Matrix matrix)
        {
            foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
            {
                if (priceType == PriceType.Undefined)
                {
                    continue;
                }

                decimal? basePrice = matrix[priceType, Valuation.Base].Value;

                if (!basePrice.HasValue)
                {
                    continue;
                }

                decimal? finalPrice = basePrice;

                if (basePrice > 0)
                {
                    decimal? optionAdjustment = matrix[priceType, Valuation.Option].Value;

                    decimal? mileageAdjustment = matrix[priceType, Valuation.Mileage].Value;

                    decimal? totalAdjustment = Sum(optionAdjustment, mileageAdjustment);

                    if (totalAdjustment.HasValue)
                    {
                        if (Math.Sign(totalAdjustment.Value) == -1)
                        {
                            decimal percentage = vehicle.Year.MaximumDeductionPercentage;

                            decimal maximumAdjustment = basePrice.Value*percentage;

                            if (Math.Abs(totalAdjustment.Value) > maximumAdjustment)
                            {
                                totalAdjustment = -1*maximumAdjustment;
                            }
                        }
                    }

                    finalPrice = Sum(basePrice, totalAdjustment);
                }

                matrix[priceType, Valuation.Final].Value = finalPrice;
            }
        }

        static void CalculateVisibility(Matrix matrix)
        {
            foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
            {
                // no base price ... not visible

                if (matrix[priceType, Valuation.Base].Value.HasValue == false)
                {
                    foreach (Valuation valuation in Enum.GetValues(typeof(Valuation)))
                    {
                        matrix[priceType, valuation].Visibility = Matrix.CellVisibility.NotDisplayed;
                    }
                }
            }
        }

        static decimal? Sum(decimal? a, decimal? b)
        {
            if (a.HasValue)
            {
                if (b.HasValue)
                {
                    return a.Value + b.Value;
                }

                return a.Value;
            }

            if (b.HasValue)
            {
                return b.Value;
            }

            return null;
        }

        static T Resolve<T>()
        {
            return _resolver.Resolve<T>();
        }
    }
}
