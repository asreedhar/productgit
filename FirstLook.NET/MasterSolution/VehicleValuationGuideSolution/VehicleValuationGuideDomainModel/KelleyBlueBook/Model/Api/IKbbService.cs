using System.Collections.Generic;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api
{
    public interface IKbbService
    {
        BookDate BookDate();

        Region ToRegion(string zipCode);

        IList<VehicleOption> DefaultOptions(int vehicleId, string vin);

        Vehicle Vehicle(int vehicleId);
        
        IList<VehicleOptionAdjustment> OptionAdjustments(int vehicle);

        IList<Region> Regions();

        IList<PriceTypeData> PriceTypeData(int dataLoadId);

        IList<VehicleOptions> GetOptions(int vehicleId, string vin, int businessUnitId);
    }
}