namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api
{
    /// <summary>
    /// Action performed on a configuration for an option.
    /// </summary>
    public interface IOptionAction
    {
        /// <summary>
        /// Option identifier.
        /// </summary>
        int OptionId { get; }

        /// <summary>
        /// Type of action -- e.g. add, remove.
        /// </summary>
        OptionActionType ActionType { get; }
    }
}