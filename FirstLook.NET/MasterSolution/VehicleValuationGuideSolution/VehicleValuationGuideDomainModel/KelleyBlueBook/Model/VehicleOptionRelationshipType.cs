﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model
{
    [Serializable]
    public enum VehicleOptionRelationshipType
    {
        Undefined,
        ConflictsWith = 9,
        RequiresAll = 11,
        RadioButtonWith = 15
    }
}
