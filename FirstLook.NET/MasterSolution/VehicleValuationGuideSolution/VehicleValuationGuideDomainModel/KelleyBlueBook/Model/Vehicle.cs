﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model
{
    [Serializable]
    public class Vehicle : KeyedValue
    {
        private Year _year;
        private Make _make;
        private Model _model;
        private Trim _trim;

        public Year Year
        {
            get { return _year; }
            internal set { _year = value; }
        }

        public Make Make
        {
            get { return _make; }
            internal set { _make = value; }
        }

        public Model Model
        {
            get { return _model; }
            internal set { _model = value; }
        }

        public Trim Trim
        {
            get { return _trim; }
            internal set { _trim = value; }
        }

        private VehicleType _vehicleType;
        private int _mileageGroupId;

        public VehicleType VehicleType
        {
            get { return _vehicleType; }
            internal set { _vehicleType = value; }
        }

        public int MileageGroupId
        {
            get { return _mileageGroupId; }
            internal set { _mileageGroupId = value; }
        }

        private string _shortName;
        private string _blueBookName;
        private string _subTrim;

        public string ShortName
        {
            get { return _shortName; }
            internal set { _shortName = value; }
        }

        public string BlueBookName
        {
            get { return _blueBookName; }
            internal set { _blueBookName = value; }
        }

        public string SubTrim
        {
            get { return _subTrim; }
            internal set { _subTrim = value; }
        }

        IList<VehicleOption> _options = new List<VehicleOption>();

        public IList<VehicleOption> Options
        {
            get { return _options; }
            internal set { _options = value; }
        }

        IList<Specification> _specifications = new List<Specification>();

        public IList<Specification> Specifications
        {
            get { return _specifications; }
            internal set { _specifications = value; }
        }
    }
}
