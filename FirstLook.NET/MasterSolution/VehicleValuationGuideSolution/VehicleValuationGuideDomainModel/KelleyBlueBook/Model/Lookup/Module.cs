﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Lookup
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ILookupBookDate, CachedLookupBookDate>(ImplementationScope.Shared);

            registry.Register<ILookupMake, CachedLookupMake>(ImplementationScope.Shared);

            registry.Register<ILookupModel, CachedLookupModel>(ImplementationScope.Shared);

            registry.Register<ILookupRegion, CachedLookupRegion>(ImplementationScope.Shared);

            registry.Register<ILookupTrim, CachedLookupTrim>(ImplementationScope.Shared);

            registry.Register<ILookupVehicle, CachedLookupVehicle>(ImplementationScope.Shared);

            registry.Register<ILookupVin, CachedLookupVin>(ImplementationScope.Shared);

            registry.Register<ILookupVinOptions, CachedLookupVinOptions>(ImplementationScope.Shared);

            registry.Register<ILookupYear, CachedLookupYear>(ImplementationScope.Shared);

            registry.Register<ILookupBasePrice, CachedLookupBasePrice>(ImplementationScope.Shared);

            registry.Register<ILookupMileageAdjustment, CachedLookupMileageAdjustment>(ImplementationScope.Shared);

            registry.Register<ILookupOptionAdjustment, CachedLookupOptionAdjustment>(ImplementationScope.Shared);

            registry.Register<ILookupRegionAdjustment, CachedLookupRegionAdjustment>(ImplementationScope.Shared);

            registry.Register<ILookupPriceType, CachedLookupPriceType>(ImplementationScope.Shared);
        }
    }
}
