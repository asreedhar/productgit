﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Lookup
{
    public interface ILookupBasePrice
    {
        IList<Price> Lookup(int bookDate, int vehicleId, Region region);

        IList<FetchValuationResultsDto> Lookup(int bookDate, int vehicleId, Region region, int? mileage, int dealerId, int year, int bookOutTypeId);
    }

    public class LookupBasePrice : ILookupBasePrice
    {
        public IList<Price> Lookup(int bookDate, int vehicleId, Region region)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Price> serializer = registry.Resolve<ISqlSerializer<Price>>();

            using (IDataReader reader = service.BasePrices(bookDate, region, vehicleId))
            {
                return serializer.Deserialize(reader);
            }
        }


        public IList<FetchValuationResultsDto> Lookup(int bookDate, int vehicleId, Region region, int? mileage, int dealerId, int year, int bookOutTypeId)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<FetchValuationResultsDto> serializer = registry.Resolve<ISqlSerializer<FetchValuationResultsDto>>();

            using (IDataReader reader = service.BasePrices(bookDate, region, vehicleId, mileage, dealerId, year, bookOutTypeId))
            {
                return serializer.Deserialize(reader);
            }


        }
    }

    public class CachedLookupBasePrice : CachedLookup, ILookupBasePrice
    {
        private readonly ILookupBasePrice _source = new LookupBasePrice();

        public IList<Price> Lookup(int bookDate, int vehicleId, Region region)
        {
            string key = CreateCacheKey(region, vehicleId);

            IList<Price> values = Cache.Get(key) as IList<Price>;

            if (values == null)
            {
                values = _source.Lookup(bookDate, vehicleId, region);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }

        public IList<FetchValuationResultsDto> Lookup(int bookDate, int vehicleId, Region region, int? mileage, int dealerId, int year, int bookOutTypeId)
        {
            string key = CreateCacheKey(region, vehicleId);

            IList<FetchValuationResultsDto> values = Cache.Get(key) as IList<FetchValuationResultsDto>;

            if (values == null)
            {
                values = _source.Lookup(bookDate, vehicleId, region, mileage, dealerId, year, bookOutTypeId);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }


    }
}
