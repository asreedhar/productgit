﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Lookup
{
    public interface ILookupRegion {

        IList<Region> Lookup(IResolver resolver, int bookDate);

        Region Lookup(IResolver resolver, int bookDate, string zipCode);
    }

    public class LookupRegion : ILookupRegion
    {
        public IList<Region> Lookup(IResolver resolver, int bookDate)
        {
            ISqlService service = resolver.Resolve<ISqlService>();

            ISqlSerializer<Region> serializer = resolver.Resolve<ISqlSerializer<Region>>();

            using (IDataReader reader = service.Regions(bookDate))
            {
                return serializer.Deserialize(reader);
            }
        }

        public Region Lookup(IResolver resolver, int bookDate, string zipCode)
        {
            ISqlService service = resolver.Resolve<ISqlService>();

            ISqlSerializer<Region> serializer = resolver.Resolve<ISqlSerializer<Region>>();

            using (IDataReader reader = service.Region(bookDate, zipCode))
            {
                if (reader.Read())
                {
                    return serializer.Deserialize((IDataRecord)reader);
                }

                return null;
            }
        }
    }

    public class CachedLookupRegion : CachedLookup, ILookupRegion
    {
        private readonly ILookupRegion _source = new LookupRegion();

        public IList<Region> Lookup(IResolver resolver, int bookDate)
        {
            string key = CreateCacheKey();

            IList<Region> values = Cache.Get(key) as IList<Region>;

            if (values == null) {
                values = _source.Lookup(resolver, bookDate);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }

        public Region Lookup(IResolver resolver, int bookDate, string zipCode)
        {
            string key = CreateCacheKey(bookDate, zipCode);

            Region value = Cache.Get(key) as Region;

            if (value == null)
            {
                value = _source.Lookup(resolver, bookDate, zipCode);

                Cache.Add(key, value, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return value;
        }
    }
}
