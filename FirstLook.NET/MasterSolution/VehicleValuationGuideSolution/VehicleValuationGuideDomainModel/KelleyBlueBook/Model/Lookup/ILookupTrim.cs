﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Lookup
{
    public interface ILookupTrim
    {
        IList<Trim> Lookup(int bookDate, int yearId, int makeId, int modelId);
    }

    public class LookupTrim : ILookupTrim
    {
        public IList<Trim> Lookup(int bookDate, int yearId, int makeId, int modelId)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Trim> serializer = registry.Resolve<ISqlSerializer<Trim>>();

            using (IDataReader reader = service.Query(bookDate, yearId, makeId, modelId))
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupTrim : CachedLookup, ILookupTrim
    {
        private readonly ILookupTrim _source = new LookupTrim();

        public IList<Trim> Lookup(int bookDate, int yearId, int makeId, int modelId)
        {
            string key = CreateCacheKey(bookDate, yearId, makeId, modelId);

            IList<Trim> values = Cache.Get(key) as IList<Trim>;

            if (values == null)
            {
                values = _source.Lookup(bookDate, yearId, makeId, modelId);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
