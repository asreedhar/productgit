﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Lookup
{
    public interface ILookupRegionAdjustment
    {
        IList<RegionAdjustment> Lookup(IResolver resolver, int bookDate, int vehicle);
    }

    public class LookupRegionAdjustment : ILookupRegionAdjustment
    {
        public IList<RegionAdjustment> Lookup(IResolver resolver, int bookDate, int vehicle)
        {
            ISqlService service = resolver.Resolve<ISqlService>();

            ISqlSerializer<RegionAdjustment> serializer = resolver.Resolve<ISqlSerializer<RegionAdjustment>>();

            using (IDataReader reader = service.RegionAdjustments(bookDate, vehicle))
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupRegionAdjustment : CachedLookup, ILookupRegionAdjustment
    {
        private readonly ILookupRegionAdjustment _source = new LookupRegionAdjustment();

        public IList<RegionAdjustment> Lookup(IResolver resolver, int bookDate, int vehicle)
        {
            string key = CreateCacheKey(bookDate, vehicle);

            IList<RegionAdjustment> values = Cache.Get(key) as IList<RegionAdjustment>;

            if (values == null)
            {
                values = _source.Lookup(resolver, bookDate, vehicle);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
