﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Lookup
{
    public interface ILookupOptionAdjustment
    {
        IList<VehicleOptionAdjustment> Lookup(int bookDate, int vehicle);
    }

    public class LookupOptionAdjustment : ILookupOptionAdjustment
    {
        public IList<VehicleOptionAdjustment> Lookup(int bookDate, int vehicle)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<VehicleOptionAdjustment> serializer = registry.Resolve<ISqlSerializer<VehicleOptionAdjustment>>();

            using (IDataReader reader = service.OptionAdjustments(bookDate, vehicle))
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupOptionAdjustment : CachedLookup, ILookupOptionAdjustment
    {
        private readonly ILookupOptionAdjustment _source = new LookupOptionAdjustment();

        public IList<VehicleOptionAdjustment> Lookup(int bookDate, int vehicle)
        {
            string key = CreateCacheKey(bookDate, vehicle);

            IList<VehicleOptionAdjustment> values = Cache.Get(key) as IList<VehicleOptionAdjustment>;

            if (values == null)
            {
                values = _source.Lookup(bookDate, vehicle);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
