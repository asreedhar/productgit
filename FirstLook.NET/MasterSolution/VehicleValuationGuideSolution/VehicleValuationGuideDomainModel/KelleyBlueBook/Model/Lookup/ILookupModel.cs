﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Lookup
{
    public interface ILookupModel
    {
        IList<Model> Lookup(int bookDate, int yearId, int makeId);
    }

    public class LookupModel : ILookupModel
    {
        public IList<Model> Lookup(int bookDate, int yearId, int makeId)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Model> serializer = registry.Resolve<ISqlSerializer<Model>>();

            using (IDataReader reader = service.Query(bookDate, yearId, makeId))
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupModel : CachedLookup, ILookupModel
    {
        private readonly ILookupModel _source = new LookupModel();

        public IList<Model> Lookup(int bookDate, int yearId, int makeId)
        {
            string key = CreateCacheKey(bookDate, yearId, makeId);

            IList<Model> values = Cache.Get(key) as IList<Model>;

            if (values == null)
            {
                values = _source.Lookup(bookDate, yearId, makeId);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
