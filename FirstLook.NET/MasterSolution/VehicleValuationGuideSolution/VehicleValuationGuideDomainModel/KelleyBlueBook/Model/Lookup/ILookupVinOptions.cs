﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Lookup
{
    public interface ILookupVinOptions
    {
        IList<int> Lookup(int bookDate, int vehicleId, string vin);

        IList<VehicleOptions> Lookup(int vehicleId, string vin, int businessUnitId);
    }

    public class LookupVinOptions : ILookupVinOptions
    {
        public IList<int> Lookup(int bookDate, int vehicleId, string vin)
        {
            ISqlService service = RegistryFactory.GetResolver().Resolve<ISqlService>();

            using (IDataReader reader = service.DefaultOptions(bookDate, vehicleId, vin))
            {
                List<int> values = new List<int>();

                while (reader.Read())
                {
                    int id = reader.GetInt32(reader.GetOrdinal("OptionId"));

                    values.Add(id);
                }

                return values;
            }
        }
        
        public IList<VehicleOptions> Lookup(int vehicleId, string vin, int businessUnitId)
        {
            ISqlService service = RegistryFactory.GetResolver().Resolve<ISqlService>();

            using (IDataReader reader = service.GetOptions(vehicleId, vin, businessUnitId))
            {
                List<VehicleOptions> values = new List<VehicleOptions>();

                while (reader.Read())
                {
                    int id = reader.GetInt32(reader.GetOrdinal("VehicleOptionId"));
                    int cateoryId = reader.GetInt32(reader.GetOrdinal("CategoryId"));
                    string description = reader.GetString(reader.GetOrdinal("Description"));
                    int displayOrder = reader.GetInt32(reader.GetOrdinal("DisplayOrder"));
                    bool isDefault = Convert.ToBoolean(reader.GetValue(reader.GetOrdinal("IsStandard")));
                    bool status = Convert.ToBoolean(reader.GetValue(reader.GetOrdinal("Status")));

                    values.Add(new VehicleOptions
                    {
                        VehicleOpionId = id,
                        CategoryId = cateoryId,
                        IsDefault = isDefault,
                        Description = description,
                        SortOrder = displayOrder,
                        Status = status
                    });

                }

                return values;
            }
        }
        
    }

    public class CachedLookupVinOptions : CachedLookup, ILookupVinOptions
    {
        private readonly ILookupVinOptions _source = new LookupVinOptions();

        public IList<int> Lookup(int bookDate, int vehicleId, string vin)
        {
            string key = CreateCacheKey(bookDate, vehicleId, vin);

            IList<int> values = Cache.Get(key) as IList<int>;

            if (values == null)
            {
                values = _source.Lookup(bookDate, vehicleId, vin);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }

        public IList<VehicleOptions> Lookup(int vehicleId, string vin, int businessUnitId)
        {
            string key = CreateCacheKey(vehicleId, vin, businessUnitId);

            IList<VehicleOptions> values = Cache.Get(key) as IList<VehicleOptions>;

            if (values == null)
            {
                values = _source.Lookup(vehicleId, vin, businessUnitId);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
