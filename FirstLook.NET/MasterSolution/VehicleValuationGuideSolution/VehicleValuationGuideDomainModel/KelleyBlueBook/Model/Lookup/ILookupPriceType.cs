﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Lookup
{
    public interface ILookupPriceType
    {
        IList<PriceTypeData> Lookup(int dataloadID);
    }
    public class LookupPriceType : ILookupPriceType
    {
        public IList<PriceTypeData> Lookup(int dataloadID)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<PriceTypeData> serializer = registry.Resolve<ISqlSerializer<PriceTypeData>>();

            using (IDataReader reader = service.PriceType(dataloadID))
            {
                return serializer.Deserialize(reader);
            }
        }
    }
    public class CachedLookupPriceType : CachedLookup, ILookupPriceType
    {
        private readonly ILookupPriceType _source = new LookupPriceType();

        public IList<PriceTypeData> Lookup(int dataloadID)
        {
            string key = CreateCacheKey();

            IList<PriceTypeData> values = Cache.Get(key) as IList<PriceTypeData>;

            if (values == null)
            {
                values = _source.Lookup(dataloadID);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
