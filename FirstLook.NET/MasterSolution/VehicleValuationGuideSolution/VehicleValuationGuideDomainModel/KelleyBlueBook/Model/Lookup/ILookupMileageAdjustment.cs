﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Lookup
{
    public interface ILookupMileageAdjustment
    {
        IList<MileageAdjustment> Lookup(int bookDate, VehicleType vehicleType, int yearId, int mileageGroupId);
    }

    public class LookupMileageAdjustment : ILookupMileageAdjustment
    {
        public IList<MileageAdjustment> Lookup(int bookDate, VehicleType vehicleType, int yearId, int mileageGroupId)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<MileageAdjustment> serializer = registry.Resolve<ISqlSerializer<MileageAdjustment>>();

            using (IDataReader reader = service.MileageAdjustment(bookDate, vehicleType, yearId, mileageGroupId))
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupMileageAdjustment : CachedLookup, ILookupMileageAdjustment
    {
        private readonly ILookupMileageAdjustment _source = new LookupMileageAdjustment();

        public IList<MileageAdjustment> Lookup(int bookDate, VehicleType vehicleType, int yearId, int mileageGroupId)
        {
            string key = CreateCacheKey(bookDate, vehicleType, yearId, mileageGroupId);

            IList<MileageAdjustment> values = Cache.Get(key) as IList<MileageAdjustment>;

            if (values == null)
            {
                values = _source.Lookup(bookDate, vehicleType, yearId, mileageGroupId);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
