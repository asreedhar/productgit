﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model
{
    [Serializable]
    public class VehicleOption : KeyedValue
    {
        private VehicleOptionAvailability _availability;
        private VehicleOptionType _optionType;
        private VehicleOptionCategory _category;
        private bool _isDefaultConfiguration;

        public VehicleOptionAvailability Availability
        {
            get { return _availability; }
            internal set { _availability = value; }
        }

        public VehicleOptionCategory Category
        {
            get { return _category; }
            internal set { _category = value; }
        }

        public VehicleOptionType OptionType
        {
            get { return _optionType; }
            internal set { _optionType = value; }
        }

        public bool IsDefaultConfiguration
        {
            get { return _isDefaultConfiguration; }
            internal set { _isDefaultConfiguration = value; }
        }

        private IList<VehicleOptionRelationship> _relationships = new List<VehicleOptionRelationship>();

        public IList<VehicleOptionRelationship> Relationships
        {
            get { return _relationships; }
            internal set { _relationships = value; }
        }
    }
}
