﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model
{
    [Serializable]
    public enum VehicleType
    {
        Undefined   = 0,
        UsedCar     = 1,
        OlderCar    = 2
    }
}
