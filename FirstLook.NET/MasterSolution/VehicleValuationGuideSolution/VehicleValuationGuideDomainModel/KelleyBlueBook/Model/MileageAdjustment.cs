﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model
{
    [Serializable]
    public class MileageAdjustment
    {
        private AdjustmentType _adjustmentType;
        private decimal _adjustment;
        private int _mileageMin;
        private int _mileageMax;

        public AdjustmentType AdjustmentType
        {
            get { return _adjustmentType; }
            internal set { _adjustmentType = value; }
        }

        public decimal Adjustment
        {
            get { return _adjustment; }
            internal set { _adjustment = value; }
        }

        public int MileageMin
        {
            get { return _mileageMin; }
            internal set { _mileageMin = value; }
        }

        public int MileageMax
        {
            get { return _mileageMax; }
            internal set { _mileageMax = value; }
        }

        public bool Covers(int mileage)
        {
            return mileage >= MileageMin && mileage <= MileageMax;
        }
    }
}
