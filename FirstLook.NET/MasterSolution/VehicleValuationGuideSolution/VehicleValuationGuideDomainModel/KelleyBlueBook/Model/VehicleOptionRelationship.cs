﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model
{
    [Serializable]
    public class VehicleOptionRelationship
    {
        private VehicleOptionRelationshipType _relationshipType;
        private IList<VehicleOption> _relatedOptions;

        public VehicleOptionRelationshipType RelationshipType
        {
            get { return _relationshipType; }
            internal set { _relationshipType = value; }
        }

        public IList<VehicleOption> RelatedOptions
        {
            get { return _relatedOptions; }
            internal set { _relatedOptions = value; }
        }
    }
}
