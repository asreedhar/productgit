﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model
{
    [Serializable]
    public enum SpecificationType
    {
        Undefined,
        Specifications,
        Warranty,
        Dimensions,
        General,
        Safety,
        Features,
        MpgRatings,
        Engine,
        CrashTest,
        Performance,
    }
}
