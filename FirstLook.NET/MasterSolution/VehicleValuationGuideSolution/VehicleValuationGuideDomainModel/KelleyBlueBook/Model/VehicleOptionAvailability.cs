﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model
{
    [Serializable]
    public enum VehicleOptionAvailability
    {
        Undefined,
        Available,
        Standard
    }
}
