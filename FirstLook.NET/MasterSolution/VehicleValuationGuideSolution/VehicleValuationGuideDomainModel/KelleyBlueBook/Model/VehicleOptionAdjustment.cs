﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model
{
    [Serializable]
    public class VehicleOptionAdjustment
    {
        private int _optionId;
        private Price _adjustment;

        public int OptionId
        {
            get { return _optionId; }
            internal set { _optionId = value; }
        }

        public Price Adjustment
        {
            get { return _adjustment; }
            internal set { _adjustment = value; }
        }
    }
}
