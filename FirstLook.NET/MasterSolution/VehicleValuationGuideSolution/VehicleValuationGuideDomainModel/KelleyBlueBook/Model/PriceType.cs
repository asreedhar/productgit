﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model 
{
    [Serializable]
    public enum PriceType
    {
        Undefined               = 0,
        /// <summary>
        /// This is the expected finance value of a fully reconditioned vehicle.
        /// A trusted benchmark value for wholesale and retail lenders. Based on
        /// Kelley Blue Book’s Auction Value, the Wholesale Lending Value assumes
        /// that the vehicle is in good or excellent condition, fully reconditioned,
        /// inspected and prepared for retail sale. Mileage adjustment is not taken
        /// into account. See “Blue Book Value” field for the Wholesale Lending price
        /// with adjustments based on the vehicle’s actual mileage.
        /// </summary>
        Wholesale               = 1,
        /// <summary>
        /// The Kelley Blue Book Suggested Retail Value is representative of dealers’
        /// asking prices and is the starting point for negotiation between a consumer
        /// and a dealer. The Suggested Retail Value assumes that the vehicle has been
        /// fully reconditioned and has a clean title history. This value also takes
        /// into account the dealers’ profit, costs for advertising, sales commissions
        /// and other costs of doing business. The final sale price will likely be
        /// less depending on the vehicle’s actual condition, popularity, type of
        /// warranty offered, and local market conditions. Mileage adjustment is not
        /// taken into account. See “Blue Book Value” field for the Retail price with
        /// adjustments based on the vehicle’s actual mileage.
        /// </summary>
        Retail                  = 2,
        TradeInFair             = 3,
        TradeInGood             = 4,
        TradeInExcellent        = 5,
        PrivatePartyFair        = 6,
        PrivatePartyGood        = 7,
        PrivatePartyExcellent   = 8,
        Msrp                    = 10,
        AuctionFair             = 75,
        AuctionGood             = 76,
        AuctionExcellent        = 77,
        ValuationBase           = 82,
        TradeInVeryGood         = 109,
	    PrivatePartyVeryGood    = 110,
        AuctionVeryGood         = 111,

        TradeInRangeLowFair = 1003,
        TradeInRangeLowGood = 1004,
        TradeInRangeLowExcellent = 1005,
        TradeInVeryGoodRangeLow = 1109,

        TradeInRangeHighFair = 2003,
        TradeInRangeHighGood = 2004,
        TradeInRangeHighExcellent = 2005,
        TradeInVeryGoodRangeHigh = 2109,
        
        Mileage=3000,
        Adjustment=4000,
        ModelYear=5000
    }
}
