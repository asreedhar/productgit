﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model
{
    [Serializable]
    public class Region : KeyedValue
    {
        // static id for NationalBase
        public static int NationalBaseRegionId = 9;

        public RegionType Type { get; internal set; }

        public enum RegionType
        {
            Undefined = 0,
            NationalBase = 5,
            VimsRegionality = 7
        }

    }
}
