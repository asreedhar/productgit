﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model
{
    public interface IKeyedValue
    {
        int Id { get; }

        string Name { get; }

        int SortOrder { get; }
    }
     [Serializable]
    public abstract class KeyedValue : IKeyedValue
    {
        private int _id;
        private string _name;
        private int _sortOrder;

        public int Id
        {
            get { return _id; }
            internal set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            internal set { _name = value; }
        }

        public int SortOrder
        {
            get { return _sortOrder; }
            internal set { _sortOrder = value; }
        }
    }

    [Serializable]
    public class Year : KeyedValue
    {
        private int _mileageZeroPoint;
        private decimal _maximumDeductionPercentage;

        public int MileageZeroPoint
        {
            get { return _mileageZeroPoint; }
            internal set { _mileageZeroPoint = value; }
        }

        public decimal MaximumDeductionPercentage
        {
            get { return _maximumDeductionPercentage; }
            internal set { _maximumDeductionPercentage = value; }
        }
    }

    [Serializable]
    public class Make : KeyedValue
    {
    }

    [Serializable]
    public class Model : KeyedValue
    {
    }

    [Serializable]
    public class Trim : KeyedValue
    {
        private int _vehicleId;

        public int VehicleId
        {
            get { return _vehicleId; }
            internal set { _vehicleId = value; }
        }
    }

    [Serializable]
    public class BookDate
    {
        public int Id { get; internal set; }
    }

    [Serializable]
    public class PriceTypeData
    {
        public int DataLoadId { get; set; }
        public int PriceTypeId { get; set; }
        public string DisplayName { get; set; }
    }
}
