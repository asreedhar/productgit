﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model
{
    [Serializable]
    public class Price
    {
        public PriceType PriceType { get; set; }

        public decimal Value { get; set; }

        public int? RegionAdjustmentTypeId { get; internal set; }
    }
}
