﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model
{
    [Serializable]
    public enum VehicleOptionCategory
    {
        Undefined,
        DriveTrain,
        Engine,
        Transmission
    }
}
