﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model
{
    [Serializable]
    public class Specification : KeyedValue
    {
        private SpecificationType _specificationType;
        private string _unit;
        private string _value;

        public SpecificationType SpecificationType
        {
            get { return _specificationType; }
            internal set { _specificationType = value; }
        }

        public string Unit
        {
            get { return _unit; }
            internal set { _unit = value; }
        }

        public string Value
        {
            get { return _value; }
            internal set { _value = value; }
        }
    }
}
