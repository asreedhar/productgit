﻿using System;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Naaa.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model.Api;
using FirstLook.VehicleValuationGuide.DomainModel.Utility.Model;

namespace FirstLook.VehicleValuationGuide.DomainModel.Naaa.Commands.Impl
{
    [Serializable]
    public class TransactionsCommand : ICommand<TransactionsResultsDto, TransactionsArgumentsDto>
    {
        public TransactionsResultsDto Execute(TransactionsArgumentsDto parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            INaaaService service = resolver.Resolve<INaaaService>();

            Vic vic = service.Vic(parameters.Uid);

            Page<Transaction> page = service.Transactions(
                vic.Value,
                parameters.Area,
                parameters.Period,
                new PageArguments
                    {
                        MaximumRows = parameters.MaximumRows,
                        SortExpression = parameters.SortColumns,
                        StartRowIndex = parameters.StartRowIndex
                    });

            return new TransactionsResultsDto
            {
                Arguments = parameters,
                TotalRowCount = page.TotalRowCount,
                Transactions = Mapper.Map(page.Items)
            };
        }
    }
}
