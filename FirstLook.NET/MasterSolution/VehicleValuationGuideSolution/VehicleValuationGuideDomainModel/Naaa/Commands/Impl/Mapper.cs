﻿using System;
using System.Collections.Generic;
using FirstLook.VehicleValuationGuide.DomainModel.Naaa.Commands.TransferObjects;
using FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Utility.Commands.TransferObjects;
using FirstLook.VehicleValuationGuide.DomainModel.Utility.Model;

namespace FirstLook.VehicleValuationGuide.DomainModel.Naaa.Commands.Impl
{
    public static class Mapper
    {
        public static List<AreaDto> Map(IList<Area> areas)
        {
            List<AreaDto> values = new List<AreaDto>();

            foreach (Area area in areas)
            {
                values.Add(Map(area));
            }

            return values;
        }

        private static AreaDto Map(Area area)
        {
            return new AreaDto
            {
                Id = area.Id,
                Name = area.Name
            };
        }

        public static List<PeriodDto> Map(IList<Period> periods)
        {
            List<PeriodDto> values = new List<PeriodDto>();

            foreach (Period period in periods)
            {
                values.Add(Map(period));
            }

            return values;
        }

        private static PeriodDto Map(Period period)
        {
            return new PeriodDto
            {
                Id = period.Id,
                Name = period.Name
            };
        }

        public static AggregateIntegerDto Map(Aggregate<int> aggregate)
        {
            return new AggregateIntegerDto
            {
                HasAverage = aggregate.Average.HasValue,
                HasMaximum = aggregate.Maximum.HasValue,
                HasMinimum = aggregate.Minimum.HasValue,
                Average = aggregate.Average.GetValueOrDefault(),
                Maximum = aggregate.Maximum.GetValueOrDefault(),
                Minimum = aggregate.Minimum.GetValueOrDefault(),
                SampleSize = aggregate.SampleSize
            };
        }

        public static AggregateDecimalDto Map(Aggregate<decimal> aggregate)
        {
            return new AggregateDecimalDto
            {
                HasAverage = aggregate.Average.HasValue,
                HasMaximum = aggregate.Maximum.HasValue,
                HasMinimum = aggregate.Minimum.HasValue,
                Average = aggregate.Average.GetValueOrDefault(),
                Maximum = aggregate.Maximum.GetValueOrDefault(),
                Minimum = aggregate.Minimum.GetValueOrDefault(),
                SampleSize = aggregate.SampleSize
            };
        }

        public static List<TransactionDto> Map(IList<Transaction> transactions)
        {
            List<TransactionDto> values = new List<TransactionDto>();

            foreach (Transaction transaction in transactions)
            {
                values.Add(
                    new TransactionDto
                    {
                        Engine = transaction.Engine,
                        Mileage = transaction.Mileage,
                        RegionName = transaction.RegionName,
                        RowNumber = transaction.RowNumber,
                        SaleDate = transaction.SaleDate,
                        SalePrice = transaction.SalePrice,
                        SaleTypeName = transaction.SaleTypeName,
                        Series = transaction.Series,
                        Transmission = transaction.Transmission
                    });
            }

            return values;
        }
    }
}
