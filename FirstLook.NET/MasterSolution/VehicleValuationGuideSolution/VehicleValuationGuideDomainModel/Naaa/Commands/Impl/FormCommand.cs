using System;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Naaa.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Naaa.Commands.Impl
{
    [Serializable]
    public class FormCommand : ICommand<FormResultsDto, FormArgumentsDto>
    {
        public FormResultsDto Execute(FormArgumentsDto parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();
            
            INaaaService service = resolver.Resolve<INaaaService>();

            return new FormResultsDto
                       {
                           Arguments = parameters,
                           Areas = Mapper.Map(service.Areas()),
                           Periods = Mapper.Map(service.Periods()),
                       };
        }
    }
}
