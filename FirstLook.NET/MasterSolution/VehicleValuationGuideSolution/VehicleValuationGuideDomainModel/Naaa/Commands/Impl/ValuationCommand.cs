﻿using System;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Naaa.Commands.TransferObjects;
using FirstLook.VehicleValuationGuide.DomainModel.Naaa.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Naaa.Commands.Impl
{
    [Serializable]
    public class ValuationCommand : ICommand<ValuationResultsDto, ValuationArgumentsDto>
    {
        public ValuationResultsDto Execute(ValuationArgumentsDto parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            INaaaService service = resolver.Resolve<INaaaService>();

            Vic vic = service.Vic(parameters.Uid);

            Report report = service.Report(
                vic.Value,
                parameters.Area,
                parameters.Period,
                parameters.Mileage);

            return new ValuationResultsDto
            {
                Arguments = parameters,
                Report = new ReportDto
                {
                    MileageReport = Mapper.Map(report.MileageReport),
                    SalePriceReport = Mapper.Map(report.SalePriceReport),
                    SalePriceSimilarMileageReport = Mapper.Map(report.SalePriceSimilarMileageReport),
                    LowMileage = report.LowMileage,
                    HighMileage = report.HighMileage
                }
            };
        }
    }
}
