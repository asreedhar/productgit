using FirstLook.Common.Core.Command;
using FirstLook.VehicleValuationGuide.DomainModel.Naaa.Commands.TransferObjects.Envelopes;

namespace FirstLook.VehicleValuationGuide.DomainModel.Naaa.Commands.Impl
{
    public class CommandFactory : ICommandFactory
    {
        public ICommand<FormResultsDto, FormArgumentsDto> CreateFormCommand()
        {
            return new FormCommand();
        }

        public ICommand<ValuationResultsDto, ValuationArgumentsDto> CreateValuationCommand()
        {
            return new ValuationCommand();
        }

        public ICommand<TransactionsResultsDto, TransactionsArgumentsDto> CreateTransactionsCommand()
        {
            return new TransactionsCommand();
        }
    }
}
