﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.Utility.Commands.TransferObjects;

namespace FirstLook.VehicleValuationGuide.DomainModel.Naaa.Commands.TransferObjects
{
    [Serializable]
    public class ReportDto
    {
        private AggregateDecimalDto _salePriceReport;
        private AggregateDecimalDto _salePriceSimilarMileageReport;
        private AggregateIntegerDto _mileageReport;
        
        public AggregateDecimalDto SalePriceReport
        {
            get { return _salePriceReport; }
            set { _salePriceReport = value; }
        }

        public AggregateDecimalDto SalePriceSimilarMileageReport
        {
            get { return _salePriceSimilarMileageReport; }
            set { _salePriceSimilarMileageReport = value; }
        }

        public AggregateIntegerDto MileageReport
        {
            get { return _mileageReport; }
            set { _mileageReport = value; }
        }

        private int _lowMileage;
        private int _highMileage;

        public int LowMileage
        {
            get { return _lowMileage; }
            set { _lowMileage = value; }
        }

        public int HighMileage
        {
            get { return _highMileage; }
            set { _highMileage = value; }
        }
    }
}
