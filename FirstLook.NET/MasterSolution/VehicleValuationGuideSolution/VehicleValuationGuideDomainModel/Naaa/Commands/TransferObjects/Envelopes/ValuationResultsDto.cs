﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Naaa.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ValuationResultsDto
    {
        private ValuationArgumentsDto _arguments;
        
        public ValuationArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        private ReportDto _report;

        public ReportDto Report
        {
            get { return _report; }
            set { _report = value; }
        }
    }
}
