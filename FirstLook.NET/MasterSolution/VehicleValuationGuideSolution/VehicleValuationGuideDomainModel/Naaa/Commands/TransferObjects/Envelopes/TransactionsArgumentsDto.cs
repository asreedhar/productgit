﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.Utility.Commands.TransferObjects;

namespace FirstLook.VehicleValuationGuide.DomainModel.Naaa.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class TransactionsArgumentsDto : PaginatedArgumentsDto
    {
        private int _area;
        private int _period;
        private int _uid;

        public int Area
        {
            get { return _area; }
            set { _area = value; }
        }

        public int Period
        {
            get { return _period; }
            set { _period = value; }
        }

        public int Uid
        {
            get { return _uid; }
            set { _uid = value; }
        }
    }
}
