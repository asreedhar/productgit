﻿using System;
using System.Collections.Generic;
using FirstLook.VehicleValuationGuide.DomainModel.Utility.Commands.TransferObjects;

namespace FirstLook.VehicleValuationGuide.DomainModel.Naaa.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class TransactionsResultsDto : PaginatedResultsDto
    {
        private TransactionsArgumentsDto _arguments;
        
        public TransactionsArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        private List<TransactionDto> _transactions;

        public List<TransactionDto> Transactions
        {
            get { return _transactions; }
            set { _transactions = value; }
        }
    }
}
