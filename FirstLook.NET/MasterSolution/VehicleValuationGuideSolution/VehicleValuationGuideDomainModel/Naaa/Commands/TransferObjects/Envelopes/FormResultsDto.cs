﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Naaa.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class FormResultsDto
    {
        private FormArgumentsDto _arguments;
        
        public FormArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        private List<AreaDto> _areas;
        private List<PeriodDto> _periods;

        public List<AreaDto> Areas
        {
            get { return _areas; }
            set { _areas = value; }
        }

        public List<PeriodDto> Periods
        {
            get { return _periods; }
            set { _periods = value; }
        }
    }
}
