using FirstLook.Common.Core.Command;
using FirstLook.VehicleValuationGuide.DomainModel.Naaa.Commands.TransferObjects.Envelopes;

namespace FirstLook.VehicleValuationGuide.DomainModel.Naaa.Commands
{
    public interface ICommandFactory
    {
        ICommand<FormResultsDto, FormArgumentsDto>
            CreateFormCommand();

        ICommand<ValuationResultsDto, ValuationArgumentsDto>
            CreateValuationCommand();

        ICommand<TransactionsResultsDto, TransactionsArgumentsDto>
            CreateTransactionsCommand();
    }
}
