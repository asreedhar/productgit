﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model
{
    [Serializable]
    public class Transaction
    {
        private int _rowNumber;
        private DateTime _saleDate;
        private string _regionName;
        private string _saleTypeName;
        private string _series;
        private decimal _salePrice;
        private int _mileage;
        private string _engine;
        private string _transmission;

        public int RowNumber
        {
            get { return _rowNumber; }
            internal set { _rowNumber = value; }
        }

        public DateTime SaleDate
        {
            get { return _saleDate; }
            internal set { _saleDate = value; }
        }

        public string RegionName
        {
            get { return _regionName; }
            internal set { _regionName = value; }
        }

        public string SaleTypeName
        {
            get { return _saleTypeName; }
            internal set { _saleTypeName = value; }
        }

        public string Series
        {
            get { return _series; }
            internal set { _series = value; }
        }

        public decimal SalePrice
        {
            get { return _salePrice; }
            internal set { _salePrice = value; }
        }

        public int Mileage
        {
            get { return _mileage; }
            internal set { _mileage = value; }
        }

        public string Engine
        {
            get { return _engine; }
            internal set { _engine = value; }
        }

        public string Transmission
        {
            get { return _transmission; }
            internal set { _transmission = value; }
        }
    }
}
