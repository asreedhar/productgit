﻿using System.Collections.Generic;
using System.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model.Sql
{
    public interface ISqlSerializer<T>
    {
        T DeserializeGraph(IDataReader reader);

        IList<T> Deserialize(IDataReader reader);

        T Deserialize(IDataRecord record);
    }
}
