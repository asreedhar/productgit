﻿using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Utility.Model;

namespace FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model.Sql
{
    public class Module : IModule
    {
        public class SerializerModule : IModule
        {
            public void Configure(IRegistry registry)
            {
                registry.Register<ISqlSerializer<Area>, AreaSerializer>(ImplementationScope.Shared);

                registry.Register<ISqlSerializer<Period>, PeriodSerializer>(ImplementationScope.Shared);

                registry.Register<ISqlSerializer<Vic>, VicSerializer>(ImplementationScope.Shared);

                registry.Register<ISqlSerializer<Report>, ReportSerializer>(ImplementationScope.Shared);

                registry.Register<ISqlSerializer<Page<Transaction>>, TransactionSerializer>(ImplementationScope.Shared);
            }
        }

        public void Configure(IRegistry registry)
        {
            registry.Register<ISqlService, SqlService>(ImplementationScope.Shared);

            registry.Register<SerializerModule>();
        }
    }
}
