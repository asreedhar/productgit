﻿using System.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model.Sql
{
    public interface ISqlService
    {
        /// <summary>
        /// Retrieve a reference list of (time) periods.
        /// </summary>
        IDataReader Periods();

        /// <summary>
        /// Retrieve a reference list of geographic areas.
        /// </summary>
        IDataReader Areas();

        /// <summary>
        /// Retrieve the VIC for the given UID.
        /// </summary>
        IDataReader Vic(int uid);

        /// <summary>
        /// Report of auction transactions for the given criteria.
        /// </summary>
        IDataReader Report(string vic, int area, int period);

        /// <summary>
        /// Report of auction transactions for the given criteria.
        /// </summary>
        IDataReader Report(string vic, int area, int period, int lowMileage, int highMileage);

        /// <summary>
        /// A page of auction transactions for the given criteria.
        /// </summary>
        IDataReader Transactions(string vic, int area, int period, string sortColumns, int maximumRows, int startRowIndex);
    }
}
