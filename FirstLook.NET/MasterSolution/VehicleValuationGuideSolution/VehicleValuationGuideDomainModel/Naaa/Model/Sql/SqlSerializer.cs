﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Utility.Model;

namespace FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model.Sql
{
    public abstract class Serializer<T> : ISqlSerializer<T>
    {
        public virtual T DeserializeGraph(IDataReader reader)
        {
            throw new NotSupportedException();
        }

        public virtual IList<T> Deserialize(IDataReader reader)
        {
            List<T> values = new List<T>();

            while (reader.Read())
            {
                values.Add(Deserialize((IDataRecord)reader));
            }

            return values;
        }

        public abstract T Deserialize(IDataRecord record);

        protected static TContract Resolve<TContract>()
        {
            return RegistryFactory.GetResolver().Resolve<TContract>();
        }
    }

    public class AreaSerializer : Serializer<Area>
    {
        public override Area Deserialize(IDataRecord record)
        {
            string name = DataRecord.GetString(record, "Name");

            int id = record.GetInt32(record.GetOrdinal("Id"));

            return new Area {Name = name, Id = id};
        }
    }

    public class PeriodSerializer : Serializer<Period>
    {
        public override Period Deserialize(IDataRecord record)
        {
            string name = DataRecord.GetString(record, "Name");

            int id = record.GetInt32(record.GetOrdinal("Id"));

            return new Period { Name = name, Id = id };
        }
    }

    public class VicSerializer : Serializer<Vic>
    {
        public override Vic Deserialize(IDataRecord record)
        {
            string vic = DataRecord.GetString(record, "Vic");

            int uid = record.GetInt32(record.GetOrdinal("Uid"));

            return new Vic { Uid = uid, Value = vic };
        }
    }

    public class ReportSerializer : Serializer<Report>
    {
        public override Report DeserializeGraph(IDataReader reader)
        {
            Aggregate<decimal> salePrice;

            Aggregate<int> mileage;

            if (reader.Read())
            {
                salePrice = new Aggregate<decimal>
                {
                    Minimum = reader.GetDecimal(reader.GetOrdinal("MinSalePrice")),
                    Average = reader.GetDecimal(reader.GetOrdinal("AvgSalePrice")),
                    Maximum = reader.GetDecimal(reader.GetOrdinal("MaxSalePrice")),
                    SampleSize = reader.GetInt32(reader.GetOrdinal("VehicleCount"))
                };

                mileage = new Aggregate<int>
                {
                    Minimum = reader.GetInt32(reader.GetOrdinal("MinMileage")),
                    Average = reader.GetInt32(reader.GetOrdinal("AvgMileage")),
                    Maximum = reader.GetInt32(reader.GetOrdinal("MaxMileage")),
                    SampleSize = reader.GetInt32(reader.GetOrdinal("VehicleCount"))
                };
            }
            else
            {
                salePrice = new Aggregate<decimal>();

                mileage = new Aggregate<int>();
            }

            Aggregate<decimal> salePriceSimilarMileage;

            if (reader.NextResult() && reader.Read()){
                salePriceSimilarMileage = new Aggregate<decimal>
                {
                    Average =
                        reader.GetDecimal(
                        reader.GetOrdinal("AvgSalePrice")),
                    SampleSize =
                        reader.GetInt32(
                        reader.GetOrdinal("VehicleCount"))
                };
            }
            else
            {
                salePriceSimilarMileage = new Aggregate<decimal>();
            }

            return new Report
            {
                MileageReport = mileage,
                SalePriceReport = salePrice,
                SalePriceSimilarMileageReport = salePriceSimilarMileage
            };
        }

        public override Report Deserialize(IDataRecord record)
        {
            throw new NotSupportedException();
        }
    }

    public class TransactionSerializer : Serializer<Page<Transaction>>
    {
        public override Page<Transaction> DeserializeGraph(IDataReader reader)
        {
            IList<Transaction> items = new List<Transaction>();

            while (reader.Read())
            {
                int rowNumber = reader.GetInt32(reader.GetOrdinal("RowNumber"));
                DateTime saleDate = reader.GetDateTime(reader.GetOrdinal("SaleDate"));
                string regionName = reader.GetString(reader.GetOrdinal("RegionName"));
                string saleTypeName = reader.GetString(reader.GetOrdinal("SaleTypeName"));
                string series = reader.GetString(reader.GetOrdinal("Series"));
                decimal salePrice = reader.GetDecimal(reader.GetOrdinal("SalePrice"));
                int mileage = reader.GetInt32(reader.GetOrdinal("Mileage"));
                string engine = reader.GetString(reader.GetOrdinal("Engine"));
                string transmission = reader.GetString(reader.GetOrdinal("Transmission"));

                items.Add(
                    new Transaction
                    {
                        Engine = engine,
                        Mileage = mileage,
                        RegionName = regionName,
                        RowNumber = rowNumber,
                        SaleDate = saleDate,
                        SalePrice = salePrice,
                        SaleTypeName = saleTypeName,
                        Series = series,
                        Transmission = transmission
                    });
            }

            int totalRowCount = 0;

            if (reader.NextResult())
            {
                if (reader.Read())
                {
                    totalRowCount = reader.GetInt32(reader.GetOrdinal("TotalRowCount"));
                }
            }

            return new Page<Transaction>
            {
                TotalRowCount = totalRowCount,
                Items = items
            };
        }

        public override Page<Transaction> Deserialize(IDataRecord record)
        {
            throw new NotSupportedException();
        }
    }
}
