﻿using System.Data;
using FirstLook.Common.Core.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model.Sql
{
    public class SqlService : ISqlService
    {
        const string DatabaseName = "Reports";

        private readonly ICloneReader _clone;

        public SqlService() : this (new CloneReader())
        {
        }

        public SqlService(ICloneReader clone)
        {
            _clone = clone;
        }

        public IDataReader Periods()
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "NationalAuction.ReportingPeriodList#Fetch";

                    return _clone.Snapshot(command, "Period");
                }
            }
        }

        public IDataReader Areas()
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "NationalAuction.ReportingAreaList#Fetch";

                    return _clone.Snapshot(command, "Area");
                }
            }
        }

        public IDataReader Vic(int uid)
        {
            using (IDbConnection connection = Database.Connection("NADA"))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = "SELECT Uid, Vic FROM dbo.Vehicles WHERE Uid = @Uid";

                    Database.AddWithValue(command, "Uid", uid, DbType.Int32);

                    return _clone.Snapshot(command, "Vic");
                }
            }
        }

        public IDataReader Report(string vic, int area, int period)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    // command.CommandType = CommandType.StoredProcedure;
                    // command.CommandText = "NationalAuction.OverallReport#Fetch";

                    command.CommandType = CommandType.Text;
                    command.CommandText = @"
                        SELECT	MaxSalePrice    = HighSalePrice,
		                        AvgSalePrice    = AveSalePrice,
		                        MinSalePrice    = LowSalePrice,
		                        MaxMileage      = CONVERT(INT,HighMileage),
		                        AvgMileage      = CONVERT(INT,AveMileage),
		                        MinMileage      = CONVERT(INT,LowMileage),
		                        VehicleCount    = TransactionCount
                        FROM	[AuctionNet].dbo.AuctionTransaction_A1
                        WHERE	VIC = @VIC
                        AND     AreaID = @AreaId
                        AND     PeriodID = @PeriodId
                    ";

                    Database.AddWithValue(command, "VIC", vic, DbType.String);

                    Database.AddWithValue(command, "AreaID", area, DbType.Int32);

                    Database.AddWithValue(command, "PeriodID", period, DbType.Int32);

                    return _clone.Snapshot(command, "Report");
                }
            }
        }

        public IDataReader Report(string vic, int area, int period, int lowMileage, int highMileage)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    // command.CommandType = CommandType.StoredProcedure;
                    // command.CommandText = "NationalAuction.MileageReport#Fetch";

                    command.CommandType = CommandType.Text;
                    command.CommandText = @"
                        SELECT	AvgSalePrice    = AveSalePrice,
		                        VehicleCount    = TransactionCount
                        FROM	[AuctionNet].dbo.AuctionTransaction_A2
                        WHERE	VIC = @VIC
                        AND     AreaID = @AreaId
                        AND     PeriodID = @PeriodId
                        AND     LowMileageRange = @LowMileage
                        AND     HighMileageRange = @HighMileage
                    ";

                    Database.AddWithValue(command, "VIC", vic, DbType.String);

                    Database.AddWithValue(command, "AreaID", area, DbType.Int32);

                    Database.AddWithValue(command, "PeriodID", period, DbType.Int32);

                    Database.AddWithValue(command, "LowMileage", lowMileage, DbType.Int32);

                    Database.AddWithValue(command, "HighMileage", highMileage, DbType.Int32);

                    return _clone.Snapshot(command, "Report");
                }
            }
        }

        public IDataReader Transactions(string vic, int area, int period, string sortColumns, int maximumRows, int startRowIndex)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "NationalAuction.VehicleList#Fetch";

                    Database.AddWithValue(command, "VIC", vic, DbType.String);

                    Database.AddWithValue(command, "VIN", "1FTEX1EV5AFA23249", DbType.String);

                    Database.AddWithValue(command, "AreaID", area, DbType.Int32);

                    Database.AddWithValue(command, "PeriodID", period, DbType.Int32);

                    Database.AddWithValue(command, "SortColumns", sortColumns, DbType.String);

                    Database.AddWithValue(command, "MaximumRows", maximumRows, DbType.Int32);

                    Database.AddWithValue(command, "StartRowIndex", startRowIndex, DbType.Int32);

                    return _clone.Snapshot(command, "Report", "TotalRowCount");
                }
            }
        }
    }

    public interface ICloneReader
    {
        IDataReader Snapshot(IDbCommand command, params string[] names);
    }

    internal class CloneReader : ICloneReader
    {
        public IDataReader Snapshot(IDbCommand command, params string[] names)
        {
            DataSet set = new DataSet();

            using (IDataReader reader = command.ExecuteReader())
            {
                foreach (string name in names)
                {
                    DataTable table = new DataTable(name);

                    table.Load(reader);

                    set.Tables.Add(table);
                }
            }

            return set.CreateDataReader();
        }
    }
}
