﻿using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model.Lookup
{
    public interface ILookupVic
    {
        Vic Lookup(int uid);
    }

    public class LookupVic : ILookupVic
    {
        public Vic Lookup(int uid)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Vic> serializer = registry.Resolve<ISqlSerializer<Vic>>();

            using (IDataReader reader = service.Vic(uid))
            {
                if (reader.Read())
                {
                    return serializer.Deserialize((IDataRecord)reader);
                }

                return null;
            }
        }
    }

    public class CachedLookupVic : CachedLookup, ILookupVic
    {
        private readonly ILookupVic _source = new LookupVic();

        public Vic Lookup(int uid)
        {
            string key = CreateCacheKey(uid);

            Vic values = Cache.Get(key) as Vic;

            if (values == null)
            {
                values = _source.Lookup(uid);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
