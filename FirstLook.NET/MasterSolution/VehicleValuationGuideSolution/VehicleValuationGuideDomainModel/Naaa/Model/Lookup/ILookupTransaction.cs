﻿using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model.Sql;
using FirstLook.VehicleValuationGuide.DomainModel.Utility.Model;

namespace FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model.Lookup
{
    public interface ILookupTransaction
    {
        Page<Transaction> Lookup(string vic, int area, int period, string sortColumns, int maximumRows, int startRowIndex);
    }

    public class LookupTransaction : ILookupTransaction
    {
        public Page<Transaction> Lookup(string vic, int area, int period, string sortColumns, int maximumRows, int startRowIndex)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Page<Transaction>> serializer = registry.Resolve<ISqlSerializer<Page<Transaction>>>();

            using (IDataReader reader = service.Transactions(vic, area, period, sortColumns, maximumRows, startRowIndex))
            {
                return serializer.DeserializeGraph(reader);
            }
        }
    }

    public class CachedLookupTransaction : CachedLookup, ILookupTransaction
    {
        private readonly ILookupTransaction _source = new LookupTransaction();

        public Page<Transaction> Lookup(string vic, int area, int period, string sortColumns, int maximumRows, int startRowIndex)
        {
            string key = CreateCacheKey(vic, area, period, sortColumns, maximumRows, startRowIndex);

            Page<Transaction> values = Cache.Get(key) as Page<Transaction>;

            if (values == null)
            {
                values = _source.Lookup(vic, area, period, sortColumns, maximumRows, startRowIndex);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
