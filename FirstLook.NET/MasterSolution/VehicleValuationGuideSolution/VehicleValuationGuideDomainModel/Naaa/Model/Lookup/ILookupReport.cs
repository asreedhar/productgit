﻿using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model.Lookup
{
    public interface ILookupReport
    {
        Report Lookup(string vic, int area, int period, int? mileage);
    }

    public class LookupReport : ILookupReport
    {
        public Report Lookup(string vic, int area, int period, int? mileage)
        {
            int lowMileage = 0, highMileage = 9999;
            
            if (mileage.HasValue)
            {
                lowMileage = ((mileage.Value/10000)*10000);

                highMileage += lowMileage;
            }

            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Report> serializer = registry.Resolve<ISqlSerializer<Report>>();

            DataSet report = new DataSet("Report");

            DataTable everyMileage = new DataTable("Overall");

            everyMileage.Load(service.Report(vic, area, period));

            report.Tables.Add(everyMileage);

            DataTable similarMileage = new DataTable("Mileage");

            similarMileage.Load(service.Report(vic, area, period, lowMileage, highMileage));

            report.Tables.Add(similarMileage);

            Report value = serializer.DeserializeGraph(report.CreateDataReader());

            value.LowMileage = lowMileage;

            value.HighMileage = highMileage;

            return value;
        }
    }

    public class CachedLookupReport : CachedLookup, ILookupReport
    {
        private readonly ILookupReport _source = new LookupReport();

        public Report Lookup(string vic, int area, int period, int? mileage)
        {
            string key = CreateCacheKey(vic, area, period, mileage.HasValue ? mileage.Value : 0);

            Report values = Cache.Get(key) as Report;

            if (values == null)
            {
                values = _source.Lookup(vic, area, period, mileage);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
