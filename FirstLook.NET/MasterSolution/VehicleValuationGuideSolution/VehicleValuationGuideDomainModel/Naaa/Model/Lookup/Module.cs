﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model.Lookup
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ILookupArea, CachedLookupArea>(ImplementationScope.Shared);

            registry.Register<ILookupPeriod, CachedLookupPeriod>(ImplementationScope.Shared);

            registry.Register<ILookupVic, CachedLookupVic>(ImplementationScope.Shared);

            registry.Register<ILookupReport, CachedLookupReport>(ImplementationScope.Shared);

            registry.Register<ILookupTransaction, CachedLookupTransaction>(ImplementationScope.Shared);
        }
    }
}
