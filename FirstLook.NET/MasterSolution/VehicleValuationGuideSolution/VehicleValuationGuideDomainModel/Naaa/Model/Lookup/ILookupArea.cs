﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model.Lookup
{
    public interface ILookupArea
    {
        IList<Area> Lookup();
    }

    public class LookupArea : ILookupArea
    {
        public IList<Area> Lookup()
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Area> serializer = registry.Resolve<ISqlSerializer<Area>>();

            using (IDataReader reader = service.Areas())
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupArea : CachedLookup, ILookupArea
    {
        private readonly ILookupArea _source = new LookupArea();

        public IList<Area> Lookup()
        {
            string key = CreateCacheKey();

            IList<Area> values = Cache.Get(key) as IList<Area>;

            if (values == null)
            {
                values = _source.Lookup();

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
