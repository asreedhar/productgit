﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.Utility.Model;

namespace FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model
{
    [Serializable]
    public class Report
    {
        private Aggregate<decimal> _salePriceReport;
        private Aggregate<decimal> _salePriceSimilarMileageReport;
        private Aggregate<int> _mileageReport;

        public Aggregate<decimal> SalePriceReport
        {
            get { return _salePriceReport; }
            internal set { _salePriceReport = value; }
        }

        public Aggregate<decimal> SalePriceSimilarMileageReport
        {
            get { return _salePriceSimilarMileageReport; }
            internal set { _salePriceSimilarMileageReport = value; }
        }

        public Aggregate<int> MileageReport
        {
            get { return _mileageReport; }
            internal set { _mileageReport = value; }
        }

        private int _lowMileage, _highMileage;

        public int LowMileage
        {
            get { return _lowMileage; }
            internal set { _lowMileage = value; }
        }

        public int HighMileage
        {
            get { return _highMileage; }
            internal set { _highMileage = value; }
        }
    }
}
