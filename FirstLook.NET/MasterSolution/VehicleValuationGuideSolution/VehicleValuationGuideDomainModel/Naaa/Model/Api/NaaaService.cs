﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model.Lookup;
using FirstLook.VehicleValuationGuide.DomainModel.Utility.Model;

namespace FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model.Api
{
    public class NaaaService : INaaaService
    {
        public IList<Period> Periods()
        {
            ILookupPeriod handle = Resolve<ILookupPeriod>();

            return handle.Lookup();
        }

        public IList<Area> Areas()
        {
            ILookupArea handle = Resolve<ILookupArea>();

            return handle.Lookup();
        }

        public Vic Vic(int uid)
        {
            ILookupVic handle = Resolve<ILookupVic>();

            return handle.Lookup(uid);
        }

        public Report Report(string vic, int area, int period, int? mileage)
        {
            ILookupReport handle = Resolve<ILookupReport>();

            return handle.Lookup(vic, area, period, mileage);
        }

        public Page<Transaction> Transactions(string vic, int area, int period, PageArguments arguments)
        {
            ILookupTransaction handle = Resolve<ILookupTransaction>();

            return handle.Lookup(vic, area, period, arguments.SortExpression, arguments.MaximumRows, arguments.StartRowIndex);
        }

        static T Resolve<T>()
        {
            return RegistryFactory.GetResolver().Resolve<T>();
        }
    }
}
