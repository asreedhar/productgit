﻿using System.Collections.Generic;
using FirstLook.VehicleValuationGuide.DomainModel.Utility.Model;

namespace FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model.Api
{
    public interface INaaaService
    {
        IList<Period> Periods();

        IList<Area> Areas();

        Vic Vic(int uid);

        Report Report(string vic, int area, int period, int? mileage);

        Page<Transaction> Transactions(string vic, int area, int period, PageArguments arguments);
    }
}
