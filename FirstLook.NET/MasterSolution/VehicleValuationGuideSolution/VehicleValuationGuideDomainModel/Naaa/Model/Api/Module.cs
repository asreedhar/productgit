﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model.Api
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<INaaaService, NaaaService>(ImplementationScope.Shared);
        }
    }
}
