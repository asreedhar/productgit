﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<Api.Module>();

            registry.Register<Lookup.Module>();

            registry.Register<Sql.Module>();
        }
    }
}
