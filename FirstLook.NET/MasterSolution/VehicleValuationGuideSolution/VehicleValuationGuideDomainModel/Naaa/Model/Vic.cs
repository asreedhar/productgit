﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model
{
    [Serializable]
    public class Vic
    {
        private int _uid;
        private string _value;

        public int Uid
        {
            get { return _uid; }
            internal set { _uid = value; }
        }

        public string Value
        {
            get { return _value; }
            internal set { _value = value; }
        }
    }
}
