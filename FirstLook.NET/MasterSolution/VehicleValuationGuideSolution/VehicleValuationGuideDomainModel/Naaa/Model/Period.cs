﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model
{
    [Serializable]
    public class Period
    {
        private int _id;
        private string _name;

        public int Id
        {
            get { return _id; }
            internal set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            internal set { _name = value; }
        }
    }
}
