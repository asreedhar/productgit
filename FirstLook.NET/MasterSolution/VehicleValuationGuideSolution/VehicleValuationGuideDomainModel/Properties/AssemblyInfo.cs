﻿using System.Reflection;
using System.Runtime.CompilerServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("FirstLook.VehicleValuationGuide.DomainModel")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyProduct("FirstLook.VehicleValuationGuide.DomainModel")]

[assembly: InternalsVisibleTo("FirstLook.VehicleValuationGuide.DomainModel.Test")]

[assembly: InternalsVisibleTo("FirstLook.VehicleValuationGuide.DomainModel.Test.Integration")]
