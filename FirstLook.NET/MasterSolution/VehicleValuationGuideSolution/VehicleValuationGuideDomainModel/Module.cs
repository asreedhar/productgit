﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<BlackBook.Module>();

            registry.Register<KelleyBlueBook.Module>();

            registry.Register<Nada.Module>();

            registry.Register<Galves.Module>();

            registry.Register<Naaa.Module>();

            registry.Register<Manheim.Module>();

            registry.Register<Edmunds.Module>();
        }
    }
}
