﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Repositories.Datastores;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Repositories.Gateways
{
    public class OptionActionGateway : GatewayBase
    {
        [Serializable]
        public class Row
        {
            public int VehicleConfigurationHistoryId { get; set; }
            
            public int DataLoadId { get; set; }

            public byte OptionActionTypeId { get; set; }

            public byte OptionActionIndex { get; set; }

            public long OptionId { get; set; }
        }

        protected class RowSerializer : Serializer<Row>
        {
            public override Row Deserialize(IDataRecord record)
            {
                int vehicleConfigurationHistoryId = record.GetInt32(record.GetOrdinal("VehicleConfigurationHistoryId"));

                byte optionActionTypeId = record.GetByte(record.GetOrdinal("OptionActionTypeId"));

                byte optionActionIndex = record.GetByte(record.GetOrdinal("OptionActionIndex"));

                long optionId = record.GetInt64(record.GetOrdinal("OptionID"));

                int dataLoadId = record.GetInt32(record.GetOrdinal("DataLoadID"));

                return new Row
                {
                    VehicleConfigurationHistoryId = vehicleConfigurationHistoryId,
                    DataLoadId = dataLoadId,
                    OptionId = optionId,
                    OptionActionIndex = optionActionIndex,
                    OptionActionTypeId = optionActionTypeId
                };
            }
        }

        public IList<Row> Fetch(int dataLoadId, int vehicleConfigurationHistoryId)
        {
            string key = CreateCacheKey(dataLoadId, vehicleConfigurationHistoryId);

            IList<Row> rows = Cache.Get(key) as IList<Row>;

            if (rows == null)
            {
                IEdmundsDatastore datastore = Resolve<IEdmundsDatastore>();

                using (IDataReader reader = datastore.OptionAction_Fetch(dataLoadId, vehicleConfigurationHistoryId))
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    rows = serializer.Deserialize(reader);

                    Remember(key, rows);
                }
            }

            return rows;
        }

        public void Insert(int dataLoadId, int vehicleConfigurationHistoryId, byte accessoryActionTypeId, byte accessoryActionIndex, long optionId)
        {
            IEdmundsDatastore datastore = Resolve<IEdmundsDatastore>();

            datastore.OptionAction_Insert(dataLoadId, vehicleConfigurationHistoryId, accessoryActionTypeId, accessoryActionIndex, optionId);
        }
    }
}
