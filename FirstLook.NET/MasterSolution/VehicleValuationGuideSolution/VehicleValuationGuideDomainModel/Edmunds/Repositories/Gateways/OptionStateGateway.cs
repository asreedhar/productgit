﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Repositories.Datastores;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Repositories.Gateways
{
    public class OptionStateGateway : GatewayBase
    {
        [Serializable]
        public class Row
        {
            public int VehicleConfigurationHistoryId { get; set; }

            public int DataLoadId { get; set; }

            public long OptionId { get; set; }

            public bool Selected { get; set; }
        }

        protected class RowSerializer : Serializer<Row>
        {
            public override Row Deserialize(IDataRecord record)
            {
                int vehicleConfigurationId = record.GetInt32(record.GetOrdinal("VehicleConfigurationHistoryId"));

                bool selected = record.GetBoolean(record.GetOrdinal("Selected"));

                long optionId = record.GetInt64(record.GetOrdinal("OptionId"));

                int dataLoadId = record.GetInt32(record.GetOrdinal("DataLoadId"));

                return new Row
                {
                    VehicleConfigurationHistoryId = vehicleConfigurationId,
                    OptionId = optionId,
                    DataLoadId = dataLoadId,
                    Selected = selected
                };
            }
        }

        public IList<Row> Fetch(int dataLoadId, int vehicleConfigurationHistoryId)
        {
            string key = CreateCacheKey(dataLoadId, vehicleConfigurationHistoryId);

            IList<Row> rows = Cache.Get(key) as IList<Row>;

            if (rows == null)
            {
                IEdmundsDatastore datastore = Resolve<IEdmundsDatastore>();

                using (IDataReader reader = datastore.OptionState_Fetch(dataLoadId, vehicleConfigurationHistoryId))
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    rows = serializer.Deserialize(reader);

                    Remember(key, rows);
                }
            }

            return rows;
        }

        public void Insert(int dataLoadId, int vehicleConfigurationHistoryId, long optionId, bool selected)
        {
            IEdmundsDatastore datastore = Resolve<IEdmundsDatastore>();

            datastore.OptionState_Insert(dataLoadId, vehicleConfigurationHistoryId, optionId, selected);
        }
    }
}
