﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Repositories.Datastores;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Repositories.Gateways
{
    public class MatrixCellGateway : GatewayBase
    {
        [Serializable]
        public class Row
        {
            public int MatrixId { get; set; }

            public int DataLoadId { get; set; }

            public byte PriceTypeId { get; set; }

            public byte ValuationId { get; set; }

            public int ConditionId { get; set; }

            public bool Visible { get; set; }

            public decimal? Value { get; set; }
        }

        protected class RowSerializer : Serializer<Row>
        {
            public override Row Deserialize(IDataRecord record)
            {
                int matrixId = record.GetInt32(record.GetOrdinal("MatrixId"));

                int dataLoadId = record.GetInt32(record.GetOrdinal("DataLoadId"));

                byte priceTypeId = record.GetByte(record.GetOrdinal("PriceTypeId"));

                byte valuationId = record.GetByte(record.GetOrdinal("ValuationId"));

                int conditionId = record.GetInt32(record.GetOrdinal("ConditionId"));

                bool visible = record.GetBoolean(record.GetOrdinal("Visible"));

                decimal? value = null;

                int ordinal = record.GetOrdinal("Value");

                if (!record.IsDBNull(ordinal))
                {
                    value = record.GetDecimal(ordinal);
                }
                
                return new Row
                {
                    MatrixId = matrixId,
                    DataLoadId = dataLoadId,
                    PriceTypeId = priceTypeId,
                    ValuationId = valuationId,
                    ConditionId = conditionId,
                    Visible = visible,
                    Value = value
                };
            }
        }

        public IList<Row> Fetch(int dataLoadId, int matrixId)
        {
            string key = CreateCacheKey(dataLoadId, matrixId);

            IList<Row> rows = Cache.Get(key) as IList<Row>;

            if (rows == null)
            {
                IEdmundsDatastore datastore = Resolve<IEdmundsDatastore>();

                using (IDataReader reader = datastore.MatrixCell_Fetch(dataLoadId, matrixId))
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    rows = serializer.Deserialize(reader);

                    Remember(key, rows);
                }
            }

            return rows;
        }

        public void Insert(int dataLoadId, int matrixId, byte priceTypeId, byte conditionId, byte valuationId, bool visible, decimal? value)
        {
            IEdmundsDatastore datastore = Resolve<IEdmundsDatastore>();

            datastore.MatrixCell_Insert(dataLoadId, matrixId, priceTypeId, conditionId, valuationId, visible, value);
        }
    }
}
