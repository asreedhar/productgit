﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Repositories.Datastores;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Repositories.Gateways
{
    public class VehicleConfigurationGateway : GatewayBase
    {
        [Serializable]
        public class Row
        {
            public int Id { get; set; }

            public string Vin { get; set; }

            public int VehicleId { get; set; }
        }

        protected class RowSerializer : Serializer<Row>
        {
            public override Row Deserialize(IDataRecord record)
            {
                int id = record.GetInt32(record.GetOrdinal("VehicleConfigurationId"));

                int vehicleId = record.GetInt32(record.GetOrdinal("VehicleId"));

                string vin = record.GetString(record.GetOrdinal("VIN"));

                return new Row {Id = id, VehicleId = vehicleId, Vin = vin};
            }
        }

        public Row Fetch(int id)
        {
            string key = CreateCacheKey(id);

            Row row = Cache.Get(key) as Row;

            if (row == null)
            {
                IEdmundsDatastore datastore = Resolve<IEdmundsDatastore>();

                using (IDataReader reader = datastore.VehicleConfiguration_Fetch(id))
                {
                    if (reader.Read())
                    {
                        ISerializer<Row> serializer = new RowSerializer();

                        row = serializer.Deserialize((IDataRecord)reader);

                        Remember(key, row);
                    }
                }
            }

            return row;
        }

        public Row Insert(int vehicleId)
        {
            IEdmundsDatastore datastore = Resolve<IEdmundsDatastore>();

            using (IDataReader reader = datastore.VehicleConfiguration_Insert(vehicleId))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    return serializer.Deserialize((IDataRecord)reader);
                }
            }

            return null;
        }
    }
}
