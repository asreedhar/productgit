﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Repositories.Datastores;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Repositories.Gateways
{
    public class MatrixGateway : GatewayBase
    {
        [Serializable]
        public class Row
        {
            public int Id { get; set; }

            public int DataLoadId { get; set; }
        }

        protected class RowSerializer : Serializer<Row>
        {
            public override Row Deserialize(IDataRecord record)
            {
                int id = record.GetInt32(record.GetOrdinal("MatrixId"));

                int dataLoadId = record.GetInt32(record.GetOrdinal("DataLoadId"));

                return new Row {Id = id, DataLoadId = dataLoadId};
            }
        }

        public Row Insert(int dataLoadId)
        {
            IEdmundsDatastore datastore = Resolve<IEdmundsDatastore>();

            using (IDataReader reader = datastore.Matrix_Insert(dataLoadId))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    return serializer.Deserialize((IDataRecord)reader);
                }
            }

            return null;
        }
    }
}
