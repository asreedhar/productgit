﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Repositories.Datastores;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Repositories.Gateways
{
    public class VehicleConfigurationHistoryMatrixGateway : GatewayBase
    {
        [Serializable]
        public class Row
        {
            public int VehicleConfigurationHistoryId { get; set; }

            public int DataLoadId { get; set; }

            public int MatrixId { get; set; }
        }

        protected class RowSerializer : Serializer<Row>
        {
            public override Row Deserialize(IDataRecord record)
            {
                int vehicleConfigurationHistoryId = record.GetInt32(record.GetOrdinal("VehicleConfigurationHistoryId"));

                int dataLoadId = record.GetInt32(record.GetOrdinal("DataLoadId"));

                int matrixId = record.GetInt32(record.GetOrdinal("MatrixId"));

                return new Row
                {
                    VehicleConfigurationHistoryId = vehicleConfigurationHistoryId,
                    DataLoadId = dataLoadId,
                    MatrixId = matrixId
                };
            }
        }

        public Row Fetch(int dataLoadId, int vehicleConfigurationHistoryId)
        {
            string key = CreateCacheKey(dataLoadId, vehicleConfigurationHistoryId);

            Row row = Cache.Get(key) as Row;

            if (row == null)
            {
                IEdmundsDatastore datastore = Resolve<IEdmundsDatastore>();

                using (IDataReader reader = datastore.VehicleConfiguration_History_Matrix_Fetch(dataLoadId, vehicleConfigurationHistoryId))
                {
                    if (reader.Read())
                    {
                        ISerializer<Row> serializer = new RowSerializer();

                        row = serializer.Deserialize((IDataRecord)reader);

                        Remember(key, row);
                    }
                }
            }

            return row;
        }

        public void Insert(int dataLoadId, int vehicleConfigurationHistoryId, int matrixId)
        {
            IEdmundsDatastore datastore = Resolve<IEdmundsDatastore>();

            datastore.VehicleConfiguration_History_Matrix_Insert(dataLoadId, vehicleConfigurationHistoryId, matrixId);
        }
    }
}
