﻿using System.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Repositories.Datastores
{
    public interface IClientDatastore
    {
        IDataReader Vehicle_Edmunds_Fetch(int vehicleId);

        void Vehicle_Edmunds_Insert(int vehicleId, int vehicleConfigurationId);
    }
}
