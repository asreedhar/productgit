﻿using System;
using System.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Repositories.Datastores
{
    public interface IEdmundsDatastore
    {
        void CloneData(int styleId);

        IDataReader VehicleConfiguration_History_Fetch(int vehicleConfigurationId);

        IDataReader VehicleConfiguration_Fetch(int vehicleConfigurationId);

        IDataReader VehicleConfiguration_History_Fetch(int vehicleConfigurationId, DateTime on);

        IDataReader VehicleConfiguration_History_Fetch(int vehicleConfigurationId, int vehicleConfigurationHistoryId);

        IDataReader OptionAction_Fetch(int dataLoadId, int vehicleConfigurationHistoryId);

        IDataReader OptionState_Fetch(int dataLoadId, int vehicleConfigurationHistoryId);

        IDataReader VehicleConfiguration_Insert(int vehicleId);

        IDataReader VehicleConfiguration_History_Insert(int vehicleConfigurationId, int dataLoadId, int styleId, string stateCode, int? mileage, int changeTypeBitFlags, int auditRowId);

        void OptionAction_Insert(int dataLoadId, int vehicleConfigurationHistoryId, byte accessoryActionTypeId, byte accessoryActionIndex, long optionId);

        void OptionState_Insert(int dataLoadId, int vehicleConfigurationHistoryId, long optionId, bool selected);

        IDataReader VehicleConfiguration_History_Matrix_Fetch(int dataLoadId, int vehicleConfigurationHistoryId);

        IDataReader MatrixCell_Fetch(int dataLoadId, int matrixId);

        IDataReader Matrix_Insert(int dataLoadId);

        void MatrixCell_Insert(int dataLoadId, int matrixId, byte priceTypeId, int conditionId, byte valuationId, bool visible, decimal? value);

        void VehicleConfiguration_History_Matrix_Insert(int dataLoadId, int vehicleConfigurationHistoryId, int matrixId);
    }
}
