﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Repositories.Datastores
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IClientDatastore, ClientDatastore>(ImplementationScope.Shared);

            registry.Register<IEdmundsDatastore, EdmundsDatastore>(ImplementationScope.Shared);
        }
    }
}
