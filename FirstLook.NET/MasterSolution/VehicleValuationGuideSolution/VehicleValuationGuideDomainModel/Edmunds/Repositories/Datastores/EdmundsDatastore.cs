﻿using System;
using System.Data;
using System.Reflection;
using FirstLook.Common.Core.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Repositories.Datastores
{
    public class EdmundsDatastore : SessionDataStore, IEdmundsDatastore
    {
        internal const string Prefix = "FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Repositories.Resources";

        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        public void CloneData(int styleId)
        {
            using (IDbCommand command = CreateCommand())
            {
                command.CommandText = "Edmunds.CloneData";

                command.CommandType = CommandType.StoredProcedure;

                Database.AddWithValue(command, "ED_STYLE_ID", styleId, DbType.Int32);
                
                command.ExecuteNonQuery();
            }
        }

        public IDataReader VehicleConfiguration_History_Fetch(int vehicleConfigurationId)
        {
            const string queryName = Prefix + ".EdmundsDatastore_VehicleConfiguration_History_Fetch_All.txt";

            return Query(
                new[] { "VehicleConfiguration_History" },
                queryName,
                new Parameter("VehicleConfigurationID", vehicleConfigurationId, DbType.Int32));
        }

        public IDataReader VehicleConfiguration_Fetch(int vehicleConfigurationId)
        {
            const string queryName = Prefix + ".EdmundsDatastore_VehicleConfiguration_Fetch.txt";

            return Query(
                new[] { "VehicleConfiguration" },
                queryName,
                new Parameter("VehicleConfigurationID", vehicleConfigurationId, DbType.Int32));
        }

        public IDataReader VehicleConfiguration_History_Fetch(int vehicleConfigurationId, DateTime on)
        {
            const string queryName = Prefix + ".EdmundsDatastore_VehicleConfiguration_History_Fetch_DateTime.txt";

            return Query(
                new[] { "VehicleConfiguration_History" },
                queryName,
                new Parameter("VehicleConfigurationID", vehicleConfigurationId, DbType.Int32),
                new Parameter("Date", on, DbType.DateTime));
        }

        public IDataReader VehicleConfiguration_History_Fetch(int vehicleConfigurationId, int vehicleConfigurationHistoryId)
        {
            const string queryName = Prefix + ".EdmundsDatastore_VehicleConfiguration_History_Fetch_Id.txt";

            return Query(
                new[] { "VehicleConfiguration_History" },
                queryName,
                new Parameter("VehicleConfigurationID", vehicleConfigurationId, DbType.Int32),
                new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32));
        }

        public IDataReader OptionAction_Fetch(int dataLoadId, int vehicleConfigurationHistoryId)
        {
            const string queryName = Prefix + ".EdmundsDatastore_OptionAction_Fetch.txt";

            return Query(
                new[] { "OptionAction" },
                queryName,
                new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32),
                new Parameter("DataLoadID", dataLoadId, DbType.Int32));
        }

        public IDataReader OptionState_Fetch(int dataLoadId, int vehicleConfigurationHistoryId)
        {
            const string queryName = Prefix + ".EdmundsDatastore_OptionState_Fetch.txt";

            return Query(
                new[] { "OptionState" },
                queryName,
                new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32),
                new Parameter("DataLoadID", dataLoadId, DbType.Int32));
        }

        public IDataReader VehicleConfiguration_Insert(int vehicleId)
        {
            const string queryNameI = Prefix + ".EdmundsDatastore_VehicleConfiguration_Insert.txt";

            const string queryNameF = Prefix + ".EdmundsDatastore_VehicleConfiguration_Fetch_Identity.txt";

            NonQuery(
                queryNameI,
                new Parameter("VehicleID", vehicleId, DbType.Int32));

            return Query(
                new[] { "VehicleConfiguration" },
                queryNameF);
        }

        public IDataReader VehicleConfiguration_History_Insert(int vehicleConfigurationId, int dataLoadId, int styleId, string stateCode, int? mileage, int changeTypeBitFlags, int auditRowId)
        {
            const string queryNameI = Prefix + ".EdmundsDatastore_VehicleConfiguration_History_Insert.txt";

            const string queryNameF = Prefix + ".EdmundsDatastore_VehicleConfiguration_History_Fetch_Identity.txt";

            Parameter m = mileage.HasValue
                              ? new Parameter("Mileage", mileage.Value, DbType.Int32)
                              : new Parameter("Mileage", DBNull.Value, DbType.Int32);

            NonQuery(
                queryNameI,
                new Parameter("VehicleConfigurationID", vehicleConfigurationId, DbType.Int32),
                new Parameter("DataLoadID", dataLoadId, DbType.Int32),
                new Parameter("StyleID", styleId, DbType.Int32),
                new Parameter("StateCode", stateCode, DbType.String),
                m,
                new Parameter("ChangeTypeBitFlags", changeTypeBitFlags, DbType.Int32),
                new Parameter("AuditRowID", auditRowId, DbType.Int32));

            return Query(
                new[] { "VehicleConfiguration" },
                queryNameF);
        }

        public void OptionAction_Insert(int dataLoadId, int vehicleConfigurationHistoryId, byte accessoryActionTypeId, byte accessoryActionIndex, long optionId)
        {
            const string queryName = Prefix + ".EdmundsDatastore_OptionAction_Insert.txt";

            NonQuery(
                queryName,
                new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32),
                new Parameter("OptionActionTypeID", accessoryActionTypeId, DbType.Byte),
                new Parameter("OptionActionIndex", accessoryActionIndex, DbType.Byte),
                new Parameter("OptionID", optionId, DbType.Int64),
                new Parameter("DataLoadID", dataLoadId, DbType.Int32));
        }

        public void OptionState_Insert(int dataLoadId, int vehicleConfigurationHistoryId, long optionId, bool selected)
        {
            const string queryName = Prefix + ".EdmundsDatastore_OptionState_Insert.txt";

            NonQuery(
                queryName,
                new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32),
                new Parameter("DataLoadID", dataLoadId, DbType.Int32),
                new Parameter("OptionID", optionId, DbType.Int64),
                new Parameter("Selected", selected, DbType.Boolean));
        }

        public IDataReader VehicleConfiguration_History_Matrix_Fetch(int dataLoadId, int vehicleConfigurationHistoryId)
        {
            const string queryName = Prefix + ".EdmundsDatastore_VehicleConfiguration_History_Matrix_Fetch.txt";

            return Query(
                new[] { "VehicleConfiguration_History_Matrix" },
                queryName,
                new Parameter("DataLoadID", dataLoadId, DbType.Int32),
                new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32));
        }

        public IDataReader MatrixCell_Fetch(int dataLoadId, int matrixId)
        {
            const string queryName = Prefix + ".EdmundsDatastore_MatrixCell_Fetch.txt";

            return Query(
                new[] { "MatrixCell" },
                queryName,
                new Parameter("DataLoadID", dataLoadId, DbType.Int32),
                new Parameter("MatrixID", matrixId, DbType.Int32));
        }

        public IDataReader Matrix_Insert(int dataLoadId)
        {
            const string queryNameI = Prefix + ".EdmundsDatastore_Matrix_Insert.txt";

            const string queryNameF = Prefix + ".EdmundsDatastore_Matrix_Fetch_Identity.txt";

            NonQuery(queryNameI, new Parameter("DataLoadID", dataLoadId, DbType.Int32));

            return Query(new[] {"Matrix"}, queryNameF);
        }

        public void MatrixCell_Insert(int dataLoadId, int matrixId, byte priceTypeId, int conditionId, byte valuationId, bool visible, decimal? value)
        {
            const string queryName = Prefix + ".EdmundsDatastore_MatrixCell_Insert.txt";

            Parameter parameter = value.HasValue
                              ? new Parameter("Value", value, DbType.Decimal)
                              : new Parameter("Value", DBNull.Value, DbType.Decimal);

            NonQuery(
                queryName,
                new Parameter("MatrixID", matrixId, DbType.Int32),
                new Parameter("DataLoadID", dataLoadId, DbType.Int32),
                new Parameter("PriceTypeID", priceTypeId, DbType.Byte),
                new Parameter("ConditionID", conditionId, DbType.Int32),
                new Parameter("ValuationID", valuationId, DbType.Byte),
                new Parameter("Visible", visible, DbType.Boolean),
                parameter);
        }

        public void VehicleConfiguration_History_Matrix_Insert(int dataLoadId, int vehicleConfigurationHistoryId, int matrixId)
        {
            const string queryName = Prefix + ".EdmundsDatastore_VehicleConfiguration_History_Matrix_Insert.txt";

            NonQuery(
                queryName,
                new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32),
                new Parameter("MatrixID", matrixId, DbType.Int32),
                new Parameter("DataLoadID", dataLoadId, DbType.Int32));
        }
    }
}
