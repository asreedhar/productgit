﻿using System;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Repositories.Entities
{
    [Serializable]
    public class Edition<T> : IEdition<T>
    {
        private IDateRange _dateRange;
        private IUser _user;
        private T _data;

        public IDateRange DateRange
        {
            get { return _dateRange; }
            internal set { _dateRange = value; }
        }

        public IUser User
        {
            get { return _user; }
            internal set { _user = value; }
        }

        public T Data
        {
            get { return _data; }
            internal set { _data = value; }
        }
    }
}
