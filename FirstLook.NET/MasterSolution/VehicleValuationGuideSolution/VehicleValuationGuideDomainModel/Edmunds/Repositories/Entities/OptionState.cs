﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Repositories.Entities
{
    [Serializable]
    public class OptionState : IOptionState
    {
        private long _optionId;
        private bool _selected;

        public long OptionId
        {
            get { return _optionId; }
            internal set { _optionId = value; }
        }

        public bool Selected
        {
            get { return _selected; }
            internal set { _selected = value; }
        }
    }
}
