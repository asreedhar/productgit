using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Repositories.Entities
{
    [Serializable]
    public class VehicleConfiguration : IVehicleConfiguration
    {
        private DataLoad _dataLoad;

        public DataLoad DataLoad
        {
            get { return _dataLoad; }
            internal set { _dataLoad = value; }
        }

        private State _state;
        private int _styleId;
        private string _vin;
        private int? _mileage;
        private int? _genericColorId;
        private int? _manufacturerColorId;
        
        public State State
        {
            get { return _state; }
            internal set { _state = value; }
        }

        public int StyleId
        {
            get { return _styleId; }
            internal set { _styleId = value; }
        }

        public string Vin
        {
            get { return _vin; }
            internal set { _vin = value; }
        }

        public int? Mileage
        {
            get { return _mileage; }
            internal set { _mileage = value; }
        }

        public int? GenericColorId
        {
            get { return _genericColorId; }
            set { _genericColorId = value; }
        }

        public int? ManufacturerColorId
        {
            get { return _manufacturerColorId; }
            set { _manufacturerColorId = value; }
        }
        
        private readonly IList<IOptionAction> _optionActions = new List<IOptionAction>();
        private readonly IList<IOptionState> _optionStates = new List<IOptionState>();

        public IList<IOptionAction> OptionActions
        {
            get { return _optionActions; }
        }

        public IList<IOptionState> OptionStates
        {
            get { return _optionStates; }
        }

        public ChangeType Compare(IVehicleConfiguration configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            ChangeType flags = ChangeType.None;

            if (configuration.DataLoad.Id != DataLoad.Id) flags |= ChangeType.DataLoad;

            if (!Equals(configuration.State.Name, State.Name)) flags |= ChangeType.State;

            if (configuration.StyleId != StyleId) flags |= ChangeType.StyleId;

            if (Nullable.Compare(configuration.Mileage, Mileage) != 0) flags |= ChangeType.Mileage;

            if (configuration.OptionActions.Count != OptionActions.Count)
            {
                flags |= ChangeType.OptionActions;
            }
            else
            {
                bool identical = configuration.OptionActions.All(
                    a => OptionActions.Any(
                             b => a.OptionId == b.OptionId &&
                                  a.ActionType == b.ActionType));

                if (!identical)
                {
                    flags |= ChangeType.OptionActions;
                }
            }

            if (configuration.OptionStates.Count != OptionStates.Count)
            {
                flags |= ChangeType.OptionStates;
            }
            else
            {
                bool identical = configuration.OptionStates.All(
                    a => OptionStates.Any(
                             b => a.OptionId == b.OptionId &&
                                  a.Selected == b.Selected));

                if (!identical)
                {
                    flags |= ChangeType.OptionStates;
                }
            }

            return flags;
        }
    }
}
