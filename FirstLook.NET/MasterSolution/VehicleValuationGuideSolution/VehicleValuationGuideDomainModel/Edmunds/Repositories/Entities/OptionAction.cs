﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Repositories.Entities
{
    [Serializable]
    public class OptionAction : IOptionAction
    {
        private long _optionId;
        private OptionActionType _actionType;

        public long OptionId
        {
            get { return _optionId; }
            internal set { _optionId = value; }
        }

        public OptionActionType ActionType
        {
            get { return _actionType; }
            internal set { _actionType = value; }
        }
    }
}
