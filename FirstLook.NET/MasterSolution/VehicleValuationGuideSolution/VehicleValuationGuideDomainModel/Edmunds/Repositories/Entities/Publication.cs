﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Repositories.Entities
{
    [Serializable]
    public class Publication : PublicationInfo, IPublication
    {
        public IVehicleConfiguration VehicleConfiguration { get; internal set; }

        public Matrix Matrix { get; internal set; }

        public IEdmundsService Service { get; internal set; }
    }
}
