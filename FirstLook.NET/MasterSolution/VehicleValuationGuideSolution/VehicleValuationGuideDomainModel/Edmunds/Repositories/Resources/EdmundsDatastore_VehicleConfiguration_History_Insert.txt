﻿
INSERT INTO Edmunds.VehicleConfiguration_History
        ( VehicleConfigurationID ,
          DataLoadID ,
          StyleID ,
          StateCode ,
          Mileage ,
          ChangeTypeBitFlags ,
          AuditRowID
        )
VALUES  ( @VehicleConfigurationID,
          @DataLoadID,
          @StyleID,
          @StateCode,
          @Mileage,
          @ChangeTypeBitFlags,
          @AuditRowID
        )
