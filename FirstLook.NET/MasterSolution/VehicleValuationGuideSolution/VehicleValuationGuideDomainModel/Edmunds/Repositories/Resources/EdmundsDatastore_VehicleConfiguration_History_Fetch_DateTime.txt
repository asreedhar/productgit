﻿
SELECT  H.VehicleConfigurationHistoryID ,
        H.VehicleConfigurationID ,
        H.DataLoadID ,
        H.StyleID ,
        H.StateCode ,
        H.Mileage ,
        H.ChangeTypeBitFlags ,
        -- audit row
        R.AuditRowID ,
        R.AuditID ,
        R.ValidFrom ,
        R.ValidUpTo ,
        R.WasInsert ,
        R.WasUpdate ,
        R.WasDelete ,
        -- user
        U.UserID ,
        U.UserTypeID ,
        U.Handle
FROM    Edmunds.VehicleConfiguration_History H
JOIN    Audit.AuditRow R ON R.AuditRowID = H.AuditRowID
JOIN	Audit.Audit A ON A.AuditID = R.AuditID
JOIN	Client.[User] U ON U.UserID = A.UserID
WHERE   H.VehicleConfigurationID = @VehicleConfigurationID
AND     @Date BETWEEN R.ValidFrom AND R.ValidUpTo
