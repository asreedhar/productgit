﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Repositories
{
    public class VehicleConfigurationBuilderFactory : IVehicleConfigurationBuilderFactory
    {
        public IVehicleConfigurationBuilder NewBuilder()
        {
            return new VehicleConfigurationBuilder();
        }

        public IVehicleConfigurationBuilder NewBuilder(IVehicleConfiguration configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            return new VehicleConfigurationBuilder(configuration);
        }
    }
}
