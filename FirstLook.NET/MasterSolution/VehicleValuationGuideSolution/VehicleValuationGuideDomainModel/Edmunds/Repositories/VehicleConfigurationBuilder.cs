﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Api;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Repositories.Entities;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Repositories
{
    public class VehicleConfigurationBuilder : IVehicleConfigurationBuilder
    {
        public VehicleConfigurationBuilder()
        {
            
        }

        public VehicleConfigurationBuilder(IVehicleConfiguration configuration)
        {
            DataLoad = configuration.DataLoad;
            StyleId = configuration.StyleId;
            Vin = configuration.Vin;
            State = configuration.State;
            Mileage = configuration.Mileage;

            foreach (IOptionAction action in configuration.OptionActions)
            {
                _actions.Add(Clone(action));
            }

            foreach (IOptionState state in configuration.OptionStates)
            {
                _states.Add(Clone(state));
            }
        }

        private DataLoad _dataLoad;

        public DataLoad DataLoad
        {
            get { return _dataLoad; }
            set { _dataLoad = value; }
        }

        private State _state;
        private int _styleId;
        private string _vin;
        private int? _mileage;
        private int? _genericColorId;
        private int? _manufacturerColorId;

        public int StyleId
        {
            get { return _styleId; }
            set { _styleId = value; }
        }

        public string Vin
        {
            get { return _vin; }
            set { _vin = value; }
        }

        public State State
        {
            get { return _state; }
            set { _state = value; }
        }

        public int? Mileage
        {
            get { return _mileage; }
            set { _mileage = value; }
        }

        public int? GenericColorId
        {
            get { return _genericColorId; }
            set { _genericColorId = value; }
        }

        public int? ManufacturerColorId
        {
            get { return _manufacturerColorId; }
            set { _manufacturerColorId = value; }
        }

        private readonly List<OptionAction> _actions = new List<OptionAction>();
        private readonly List<OptionState> _states = new List<OptionState>();

        public IOptionState Add(Option option)
        {
            if (option == null)
            {
                throw new ArgumentNullException("option");
            }

            OptionState state = _states.FirstOrDefault(x => x.OptionId == option.Id);

            if (state == null)
            {
                state = new OptionState
                {
                    OptionId = option.Id,
                    Selected = false
                };

                _states.Add(state);
            }

            return state;
        }

        public IOptionState Add(long optionId, bool selected)
        {
            VerifyOptionName(optionId);

            OptionState state = _states.FirstOrDefault(x => x.OptionId == optionId);

            if (state == null)
            {
                state = new OptionState
                {
                    OptionId = optionId,
                    Selected = selected
                };

                _states.Add(state);
            }

            return state;
        }
        
        public void Select(long optionId)
        {
            VerifyOptionName(optionId);

            OptionState state = _states.FirstOrDefault(x => x.OptionId == optionId);

            if (state != null)
            {
                state.Selected = true;
            }
        }

        public void Deselect(long optionId)
        {
            VerifyOptionName(optionId);

            OptionState state = _states.FirstOrDefault(x => x.OptionId == optionId);

            if (state != null)
            {
                state.Selected = false;
            }
        }

        public IEnumerable<long> Selected()
        {
            return _states.Where(x => x.Selected).Select(y => y.OptionId);
        }

        public bool IsSelected(long optionId, bool returnValue)
        {
            VerifyOptionName(optionId);

            OptionState state = _states.FirstOrDefault(x => x.OptionId == optionId);

            if (state != null)
            {
                return state.Selected;
            }

            return returnValue;
        }

        public IOptionAction Do(long optionId, OptionActionType actionType)
        {
            VerifyOptionName(optionId);

            OptionAction action = new OptionAction
            {
                OptionId = optionId,
                ActionType = actionType
            };

            _actions.Add(action);

            return action;
        }

        public IOptionAction Undo(long optionId)
        {
            VerifyOptionName(optionId);

            OptionAction action = _actions.FirstOrDefault(x => x.OptionId == optionId);

            if (action != null)
            {
                _actions.Remove(action);
            }

            return action;
        }

        public IVehicleConfiguration ToVehicleConfiguration()
        {
            VehicleConfiguration configuration = new VehicleConfiguration
            {
                DataLoad = DataLoad,
                StyleId = StyleId,
                Vin = Vin,
                State = State,
                Mileage = Mileage,
                GenericColorId = GenericColorId,
                ManufacturerColorId = ManufacturerColorId
            };

            foreach (OptionAction action in _actions)
            {
                configuration.OptionActions.Add(Clone(action));
            }

            foreach (OptionState state in _states)
            {
                configuration.OptionStates.Add(Clone(state));
            }

            return configuration;
        }

        private static void VerifyOptionName(long optionId)
        {
            if (optionId == 0)
            {
                throw new ArgumentOutOfRangeException("optionId", optionId, "non-zero please");
            }
        }

        private static OptionAction Clone(IOptionAction action)
        {
            return new OptionAction {ActionType = action.ActionType, OptionId = action.OptionId};
        }

        private static OptionState Clone(IOptionState state)
        {
            return new OptionState {OptionId = state.OptionId, Selected = state.Selected};
        }
    }
}
