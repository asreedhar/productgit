﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Envelopes.Publications;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands
{
    public interface ICommandFactory
    {
        ICommand<StatesResultsDto, StatesArgumentsDto>
            CreateStatesCommand();

        ICommand<TraversalResultsDto, TraversalArgumentsDto>
            CreateTraversalCommand();

        ICommand<SuccessorResultsDto, SuccessorArgumentsDto>
            CreateSuccessorCommand();

        ICommand<InitialValuationResultsDto, InitialValuationArgumentsDto>
            CreateInitialValuationCommand();

        ICommand<AdjustedValuationResultsDto, AdjustedValuationArgumentsDto>
            CreateAdjustedValuationCommand();

        #region Publication

        ICommand<PublicationListResultsDto, IdentityContextDto<PublicationListArgumentsDto>>
            CreatePublicationListCommand();

        ICommand<PublicationOnDateResultsDto, IdentityContextDto<PublicationOnDateArgumentsDto>>
            CreatePublicationOnDateCommand();

        ICommand<PublicationLoadResultsDto, IdentityContextDto<PublicationLoadArgumentsDto>>
            CreatePublicationLoadCommand();

        ICommand<PublicationSaveResultsDto, IdentityContextDto<PublicationSaveArgumentsDto>>
            CreatePublicationSaveCommand();

        #endregion
    }
}
