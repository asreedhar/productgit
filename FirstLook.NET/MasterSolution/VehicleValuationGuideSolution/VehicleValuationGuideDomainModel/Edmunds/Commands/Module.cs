﻿using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.Impl;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ICommandFactory, CommandFactory>(ImplementationScope.Shared);
        }
    }
}
