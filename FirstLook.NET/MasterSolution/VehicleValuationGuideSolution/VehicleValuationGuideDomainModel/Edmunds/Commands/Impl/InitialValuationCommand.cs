using System;
using System.Linq;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.Impl
{
    [Serializable]
    public class InitialValuationCommand : ICommand<InitialValuationResultsDto, InitialValuationArgumentsDto>
    {
        public InitialValuationResultsDto Execute(InitialValuationArgumentsDto parameters)
        {
            if (parameters.StyleId == 0)
            {
                throw new ArgumentNullException("parameters", "StyleID is 0");
            }

            IResolver resolver = RegistryFactory.GetResolver();

            IEdmundsService service = resolver.Resolve<IEdmundsService>();

            State state = State.Values.First(x => Equals(x.Code, parameters.State));

            IVehicleConfiguration configuration = resolver.Resolve<IEdmundsConfiguration>().InitialConfiguration(
                parameters.StyleId,
                parameters.Vin,
                state,
                parameters.HasMileage ? parameters.Mileage : default(int?),
                parameters.HasGenericColorId ? parameters.GenericColorId: default(int?),
                parameters.HasManufacturerColorId ? parameters.ManufacturerColorId : default(int?));

            IEdmundsCalculator calculator = resolver.Resolve<IEdmundsCalculator>();

            Matrix matrix = calculator.Calculate(service, configuration);

            PriceTablesDto tables = Mapper.Map(matrix);

            InitialValuationResultsDto results = new InitialValuationResultsDto
            {
                Arguments = parameters,
                Colors = Mapper.Map(service.Colors(parameters.StyleId)),
                Options = Mapper.Map(service.Options(parameters.StyleId)),
                Tables = tables,
                VehicleConfiguration = Mapper.Map(configuration)
            };

            return results;
        }
    }
}
