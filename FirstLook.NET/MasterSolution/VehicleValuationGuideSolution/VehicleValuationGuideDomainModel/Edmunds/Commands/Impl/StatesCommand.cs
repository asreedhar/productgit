﻿using System;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.Impl
{
    [Serializable]
    public class StatesCommand : ICommand<StatesResultsDto,StatesArgumentsDto>
    {
        public StatesResultsDto Execute(StatesArgumentsDto parameters)
        {
            return new StatesResultsDto
            {
                States = Mapper.Map(State.Values)
            };
        }
    }
}
