﻿using System.Collections.Generic;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Publications;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.Impl.Publications
{
    public static class PublicationMapper
    {
        public static List<PublicationInfoDto> Map(IList<IEdition<IPublicationInfo>> publications)
        {
            List<PublicationInfoDto> values = new List<PublicationInfoDto>();

            foreach (IEdition<IPublicationInfo> edition in publications)
            {
                IPublicationInfo publication = edition.Data;

                values.Add(
                    new PublicationInfoDto
                    {
                        ChangeAgent = (ChangeAgentDto)publication.ChangeAgent,
                        ChangeType = (ChangeTypeDto)publication.ChangeType,
                        Id = publication.Id,
                        Edition = new EditionDto
                        {
                            BeginDate = edition.DateRange.BeginDate,
                            EndDate = edition.DateRange.EndDate,
                            User = new UserDto
                            {
                                FirstName = edition.User.FirstName,
                                LastName = edition.User.LastName,
                                UserName = edition.User.UserName
                            }
                        }
                    });
            }

            return values;
        }

        public static PublicationDto Map(IEdition<IPublication> edition)
        {
            if (edition == null) return null;

            IPublication publication = edition.Data;

            IEdmundsService service = publication.Service;

            IVehicleConfiguration configuration = publication.VehicleConfiguration;

            IList<Option> options = service.Options(configuration.StyleId);

            IList<ManufacturerColor> colors = service.Colors(configuration.StyleId);

            return new PublicationDto
            {
                ChangeAgent = (ChangeAgentDto)publication.ChangeAgent,
                ChangeType = (ChangeTypeDto)publication.ChangeType,
                Id = publication.Id,
                Edition = new EditionDto
                {
                    BeginDate = edition.DateRange.BeginDate,
                    EndDate = edition.DateRange.EndDate,
                    User = new UserDto
                    {
                        FirstName = edition.User.FirstName,
                        LastName = edition.User.LastName,
                        UserName = edition.User.UserName
                    }
                },
                Colors = Mapper.Map(colors),
                Tables = Mapper.Map(publication.Matrix),
                Options = Mapper.Map(options),
                VehicleConfiguration = Mapper.Map(configuration)
            };
        }
    }
}
