﻿using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Envelopes.Publications;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.Impl.Publications
{
    public class PublicationSaveCommand : ICommand<PublicationSaveResultsDto, IdentityContextDto<PublicationSaveArgumentsDto>>
    {
        public PublicationSaveResultsDto Execute(IdentityContextDto<PublicationSaveArgumentsDto> parameters)
        {
            // resolve dependencies

            IResolver resolver = RegistryFactory.GetResolver();

            IVehicleRepository vehicleRepository = resolver.Resolve<IVehicleRepository>();

            IEdmundsRepository nadaRepository = resolver.Resolve<IEdmundsRepository>();
            
            IEdmundsCalculator calculator = resolver.Resolve<IEdmundsCalculator>();

            IEdmundsService service = resolver.Resolve<IEdmundsService>();

            // local variables

            PublicationSaveArgumentsDto arguments = parameters.Arguments;

            IdentityDto identity = parameters.Identity;

            // build configuration and its matrix

            IVehicleConfiguration configuration = Mapper.Map(arguments.VehicleConfiguration, null, null, null, null);

            Matrix matrix = calculator.Calculate(service, configuration);

            // save configuration

            IBroker broker = Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);

            IPrincipal principal = Principal.Get(identity.Name, identity.AuthorityName);

            ClientVehicleIdentification vehicle = vehicleRepository.Identification(broker, arguments.Vehicle);

            IEdition<IPublication> publication = nadaRepository.Save(
                broker,
                principal,
                vehicle,
                configuration,
                matrix);

            return new PublicationSaveResultsDto
            {
                Arguments = arguments,
                Publication = PublicationMapper.Map(publication)
            };
        }
    }
}
