﻿using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Envelopes.Publications;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.Impl.Publications
{
    public class PublicationListCommand : ICommand<PublicationListResultsDto, IdentityContextDto<PublicationListArgumentsDto>>
    {
        public PublicationListResultsDto Execute(IdentityContextDto<PublicationListArgumentsDto> parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            IVehicleRepository vehicleRepository = resolver.Resolve<IVehicleRepository>();

            IEdmundsRepository nadaRepository = resolver.Resolve<IEdmundsRepository>();

            PublicationListArgumentsDto arguments = parameters.Arguments;

            IdentityDto identity = parameters.Identity;

            IBroker broker = Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);

            ClientVehicleIdentification vehicle = vehicleRepository.Identification(broker, arguments.Vehicle);

            IList<IEdition<IPublicationInfo>> publications = nadaRepository.Load(broker, vehicle);

            return new PublicationListResultsDto
            {
                Arguments = arguments,
                Publications = PublicationMapper.Map(publications)
            };
        }
    }
}
