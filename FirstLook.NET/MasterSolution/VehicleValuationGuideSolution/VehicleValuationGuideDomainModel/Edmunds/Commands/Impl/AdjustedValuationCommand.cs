using System;
using System.Linq;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.Impl
{
    [Serializable]
    public class AdjustedValuationCommand : ICommand<AdjustedValuationResultsDto, AdjustedValuationArgumentsDto>
    {
        public AdjustedValuationResultsDto Execute(AdjustedValuationArgumentsDto parameters)
        {
            if (parameters.VehicleConfiguration == null)
            {
                throw new ArgumentNullException("parameters", "VehicleConfiguration is null");
            }

            IResolver resolver = RegistryFactory.GetResolver();

            State state = State.Values.FirstOrDefault(x => Equals(x.Code, parameters.State));

            IVehicleConfiguration configuration = Mapper.Map(
                parameters.VehicleConfiguration,
                state,
                parameters.HasMileage ? parameters.Mileage : default(int?),
                parameters.HasGenericColorId ? parameters.GenericColorId : default(int?),
                parameters.HasManufacturerColorId ? parameters.ManufacturerColorId : default(int?));

            OptionActionDto action = parameters.OptionAction;

            if (action != null)
            {
                configuration = resolver.Resolve<IEdmundsConfiguration>().UpdateConfiguration(
                    resolver.Resolve<IEdmundsService>(),
                    configuration,
                    new Action { OptionId = action.OptionId, ActionType = (OptionActionType) action.ActionType });
            }

            Matrix matrix = resolver.Resolve<IEdmundsCalculator>().Calculate(
                resolver.Resolve<IEdmundsService>(),
                configuration);

            PriceTablesDto tables = Mapper.Map(matrix);

            AdjustedValuationResultsDto results = new AdjustedValuationResultsDto
            {
                Arguments = parameters,
                Tables = tables,
                VehicleConfiguration = Mapper.Map(configuration)
            };

            return results;
        }

        class Action : IOptionAction
        {
            public long OptionId { get; set; }

            public OptionActionType ActionType { get; set; }
        }
    }
}
