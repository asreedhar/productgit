using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Traversal;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.Impl
{
    public static class Mapper
    {
        public static List<StateDto> Map(IEnumerable<State> states)
        {
            List<StateDto> values = new List<StateDto>();

            foreach (State state in states)
            {
                values.Add(Map(state));
            }

            return values;
        }

        private static StateDto Map(State state)
        {
            return new StateDto {Name = state.Name, Code = state.Code};
        }

        public static List<NodeDto> Map(IEnumerable<ITreeNode> nodes)
        {
            return Map(nodes, Operation.No, Operation.No);
        }

        public static List<NodeDto> Map(IEnumerable<ITreeNode> nodes, Operation parents, Operation children)
        {
            List<NodeDto> list = new List<NodeDto>();

            foreach (ITreeNode node in nodes)
            {
                list.Add(Map(node, parents, children));
            }

            return list;
        }

        public class Operation
        {
            private readonly Operation _next;
            private readonly bool _perform;

            protected Operation(bool perform, Operation next)
            {
                _next = next ?? this;
                _perform = perform;
            }

            public Operation Next
            {
                get { return _next; }
            }

            public bool Perform
            {
                get { return _perform; }
            }

            public static readonly Operation Yes;
            public static readonly Operation No;
            public static readonly Operation NoThenYes;
            public static readonly Operation YesThenNo;
            public static readonly Operation YesThenYesThenNo;
            public static readonly Operation YesThenYesThenYesThenNo;

            static Operation()
            {
                Yes = new Operation(true, null);
                No = new Operation(false, null);
                NoThenYes = new Operation(false, Yes);
                YesThenNo = new Operation(true, No);
                YesThenYesThenNo = new Operation(true, YesThenNo);
                YesThenYesThenYesThenNo = new Operation(true, YesThenYesThenNo);
            }
        }

        public static NodeDto Map(ITreeNode node, Operation parents, Operation children)
        {
            ITreePath treePath = new TreePath();

            node.Save(treePath);

            NodeDto dto = new NodeDto
            {
                Value = node.Value,
                State = treePath.State,
                StyleId = node.StyleId.GetValueOrDefault(),
                HasStyleId = node.StyleId.HasValue,
                Label = node.Label
            };

            if (children.Perform)
            {
                dto.Children = Map(node.Children);
            }

            if (parents.Perform && node.Parent != null)
            {
                dto.Parent = Map(node.Parent, parents.Next, children.Next);
            }

            return dto;
        }

        public static IVehicleConfiguration Map(VehicleConfigurationDto configuration, State state, int? mileage, int? genericColorId, int? manufacturerColorId)
        {
            if (state == null)
            {
                state = State.Values.First(x => Equals(x.Code, configuration.State.Code));
            }

            if (!mileage.HasValue)
            {
                mileage = configuration.HasMileage ? configuration.Mileage : default(int?);
            }

            if (!genericColorId.HasValue)
            {
                genericColorId = configuration.HasGenericColorId ? configuration.GenericColorId : default(int?);
            }

            if (!manufacturerColorId.HasValue)
            {
                manufacturerColorId = configuration.HasManufacturerColorId ? configuration.ManufacturerColorId : default(int?);
            }

            IVehicleConfigurationBuilderFactory factory = RegistryFactory.GetResolver().Resolve<IVehicleConfigurationBuilderFactory>();

            IVehicleConfigurationBuilder builder = factory.NewBuilder();

            builder.DataLoad = new DataLoad {Id = configuration.DataLoad.Id, Value = configuration.DataLoad.Value};
            builder.StyleId = configuration.StyleId;
            builder.Vin = configuration.Vin;
            builder.State = state;
            builder.Mileage = mileage;
            builder.GenericColorId = genericColorId;
            builder.ManufacturerColorId = manufacturerColorId;

            foreach (OptionActionDto value in configuration.OptionActions)
            {
                builder.Do(value.OptionId, (OptionActionType)value.ActionType);
            }

            foreach (OptionStateDto value in configuration.OptionStates)
            {
                builder.Add(value.OptionId, value.Selected);
            }

            return builder.ToVehicleConfiguration();
        }

        public static PriceTablesDto Map(Matrix matrix)
        {
            PriceTablesDto tables = new PriceTablesDto();

            foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
            {
                if (priceType == PriceType.Undefined)
                {
                    continue;
                }

                PriceTableDto table = new PriceTableDto();

                switch (priceType)
                {
                    case PriceType.Retail:
                        tables.Retail = table;
                        break;
                    case PriceType.TradeIn:
                        tables.TradeIn = table;
                        break;
                    case PriceType.PrivateParty:
                        tables.PrivateParty = table;
                        break;
                }

                foreach (Valuation valuation in Enum.GetValues(typeof(Valuation)))
                {
                    if (valuation == Valuation.Undefined)
                    {
                        continue;
                    }

                    PriceTableRowDto row = new PriceTableRowDto();

                    switch (valuation)
                    {
                        case Valuation.Base:
                            row.RowType = PriceTableRowTypeDto.Base;
                            break;
                        case Valuation.Final:
                            row.RowType = PriceTableRowTypeDto.Final;
                            break;
                        case Valuation.Mileage:
                            row.RowType = PriceTableRowTypeDto.Mileage;
                            break;
                        case Valuation.Option:
                            row.RowType = PriceTableRowTypeDto.Option;
                            break;
                        case Valuation.Color:
                            row.RowType = PriceTableRowTypeDto.Color;
                            break;
                        case Valuation.Region:
                            row.RowType = PriceTableRowTypeDto.Region;
                            break;
                        case Valuation.Condition:
                            row.RowType = PriceTableRowTypeDto.Condition;
                            break;
                    }

                    int? outstanding = GetPriceValue(matrix, priceType, Condition.Outstanding, valuation);
                    int? clean = GetPriceValue(matrix, priceType, Condition.Clean, valuation);
                    int? average = GetPriceValue(matrix, priceType, Condition.Average, valuation);
                    int? rough = GetPriceValue(matrix, priceType, Condition.Rough, valuation);

                    PricesDto prices = new PricesDto
                    {
                        Outstanding = outstanding.GetValueOrDefault(),
                        Clean = clean.GetValueOrDefault(),
                        Average = average.GetValueOrDefault(),
                        Rough = rough.GetValueOrDefault()
                    };

                    row.Prices = prices;

                    table.Rows.Add(row);
                }
            }

            return tables;
        }

        internal static int? GetPriceValue(Matrix matrix, PriceType market, Condition condition, Valuation valuation)
        {
            decimal? value = null;

            bool visible = matrix[market, condition, valuation].Visible;

            if (visible)
            {
                value = matrix[market, condition, valuation].Value;
            }

            return value.HasValue ? Convert.ToInt32(value.Value) : default(int?);
        }

        public static VehicleConfigurationDto Map(IVehicleConfiguration configuration)
        {
            return new VehicleConfigurationDto
            {
                DataLoad = new DataLoadDto { Id = configuration.DataLoad.Id, Value = configuration.DataLoad.Value },
                StyleId = configuration.StyleId,
                Vin = configuration.Vin,
                State = Map(configuration.State),
                Mileage = configuration.Mileage.GetValueOrDefault(),
                HasMileage = configuration.Mileage.HasValue,
                GenericColorId = configuration.GenericColorId.GetValueOrDefault(),
                HasGenericColorId = configuration.GenericColorId.HasValue,
                ManufacturerColorId = configuration.ManufacturerColorId.GetValueOrDefault(),
                HasManufacturerColorId = configuration.ManufacturerColorId.HasValue,
                OptionActions = Map(configuration.OptionActions),
                OptionStates = Map(configuration.OptionStates)
            };
        }

        private static List<OptionActionDto> Map(IEnumerable<IOptionAction> actions)
        {
            List<OptionActionDto> values = new List<OptionActionDto>();

            foreach (IOptionAction action in actions)
            {
                values.Add(
                    new OptionActionDto
                    {
                        ActionType = (OptionActionTypeDto)action.ActionType,
                        OptionId = action.OptionId
                    });
            }

            return values;
        }

        private static List<OptionStateDto> Map(IEnumerable<IOptionState> states)
        {
            List<OptionStateDto> values = new List<OptionStateDto>();

            foreach (IOptionState state in states)
            {
                values.Add(
                    new OptionStateDto
                    {
                        Selected = state.Selected,
                        OptionId = state.OptionId
                    });
            }

            return values;
        }

        public static List<OptionDto> Map(IList<Option> adjustments)
        {
            List<OptionDto> values = new List<OptionDto>();

            foreach (Option option in adjustments)
            {
                values.Add(
                    new OptionDto
                    {
                        Id = option.Id,
                        Name = option.Name,
                        PrivateParty = option.Prices.First(x => x.PriceType == PriceType.PrivateParty).Amount,
                        Retail = option.Prices.First(x => x.PriceType == PriceType.Retail).Amount,
                        TradeIn = option.Prices.First(x => x.PriceType == PriceType.TradeIn).Amount
                    });
            }

            return values;
        }

        public static List<ManufacturerColorDto> Map(IList<ManufacturerColor> colors)
        {
            List<ManufacturerColorDto> values = new List<ManufacturerColorDto>();

            foreach (ManufacturerColor color in colors)
            {
                values.Add(
                    new ManufacturerColorDto
                    {
                        Id = color.Id,
                        Name = color.Name,
                        Code = color.Code,
                        Color = ColorTranslator.ToHtml(color.Color),
                        ColorType = (ColorTypeDto)color.ColorType,
                        GenericColor = new GenericColorDto
                        {
                            Id = color.GenericColor.Id,
                            Name = color.GenericColor.Name
                        }
                    });
            }

            return values;
        }
    }
}
