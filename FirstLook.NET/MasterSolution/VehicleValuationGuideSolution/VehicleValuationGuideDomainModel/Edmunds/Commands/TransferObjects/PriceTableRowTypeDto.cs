﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects
{
    [Serializable]
    public enum PriceTableRowTypeDto
    {
        Undefined,
        Base,
        Option,
        Mileage,
        Color,
        Region,
        Condition,
        Final
    }
}
