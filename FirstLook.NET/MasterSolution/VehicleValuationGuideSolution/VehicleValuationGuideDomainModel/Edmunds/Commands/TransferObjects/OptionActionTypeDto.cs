using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects
{
    [Serializable]
    public enum OptionActionTypeDto
    {
        Undefined,
        Add,
        Remove
    }
}
