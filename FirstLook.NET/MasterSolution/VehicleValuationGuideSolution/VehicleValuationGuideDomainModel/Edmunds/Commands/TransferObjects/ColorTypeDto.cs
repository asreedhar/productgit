﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects
{
    [Serializable]
    public enum ColorTypeDto
    {
        Undefined,
        Exterior,
        Interior
    }
}
