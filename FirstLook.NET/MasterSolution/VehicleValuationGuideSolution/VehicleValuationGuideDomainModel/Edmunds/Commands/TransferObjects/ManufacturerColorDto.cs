﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects
{
    [Serializable]
    public class ManufacturerColorDto
    {
        private GenericColorDto _genericColor;
        private int _id;
        private string _name;
        private string _code;
        private ColorTypeDto _colorType;
        private string _color;

        public GenericColorDto GenericColor
        {
            get { return _genericColor; }
            set { _genericColor = value; }
        }

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        public ColorTypeDto ColorType
        {
            get { return _colorType; }
            set { _colorType = value; }
        }

        public string Color
        {
            get { return _color; }
            set { _color = value; }
        }
    }
}
