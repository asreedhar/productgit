﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Traversal
{
    [Serializable]
    public class NodeDto
    {
        private NodeDto _parent;

        public NodeDto Parent
        {
            get { return _parent; }
            set { _parent = value; }
        }

        private List<NodeDto> _children = new List<NodeDto>();

        public List<NodeDto> Children
        {
            get { return _children; }
            set { _children = value; }
        }

        private string _value;

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }

        private string _state;

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }

        private int _styleId;
        private bool _hasStyleId;

        public int StyleId
        {
            get { return _styleId; }
            set { _styleId = value; }
        }

        public bool HasStyleId
        {
            get { return _hasStyleId; }
            set { _hasStyleId = value; }
        }

        private string _label;

        public string Label
        {
            get { return _label; }
            set { _label = value; }
        }
    }
}
