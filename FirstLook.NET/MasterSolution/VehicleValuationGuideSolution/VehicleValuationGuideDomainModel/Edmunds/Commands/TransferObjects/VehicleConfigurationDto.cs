﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects
{
    [Serializable]
    public class VehicleConfigurationDto
    {
        private DataLoadDto _dataLoad;

        public DataLoadDto DataLoad
        {
            get { return _dataLoad; }
            set { _dataLoad = value; }
        }

        private int _styleId;
        private string _vin;
        
        public int StyleId
        {
            get { return _styleId; }
            set { _styleId = value; }
        }

        public string Vin
        {
            get { return _vin; }
            set { _vin = value; }
        }

        private int _mileage;
        private bool _hasMileage;

        public int Mileage
        {
            get { return _mileage; }
            set { _mileage = value; }
        }

        public bool HasMileage
        {
            get { return _hasMileage; }
            set { _hasMileage = value; }
        }

        private int _genericColorId;
        private bool _hasGenericColorId;

        public int GenericColorId
        {
            get { return _genericColorId; }
            set { _genericColorId = value; }
        }

        public bool HasGenericColorId
        {
            get { return _hasGenericColorId; }
            set { _hasGenericColorId = value; }
        }

        private int _manufacturerColorId;
        private bool _hasManufacturerColorId;

        public int ManufacturerColorId
        {
            get { return _manufacturerColorId; }
            set { _manufacturerColorId = value; }
        }

        public bool HasManufacturerColorId
        {
            get { return _hasManufacturerColorId; }
            set { _hasManufacturerColorId = value; }
        }

        private StateDto _state;

        public StateDto State
        {
            get { return _state; }
            set { _state = value; }
        }

        private List<OptionStateDto> _optionStates;

        public List<OptionStateDto> OptionStates
        {
            get { return _optionStates; }
            set { _optionStates = value; }
        }

        private List<OptionActionDto> _optionActions;

        public List<OptionActionDto> OptionActions
        {
            get { return _optionActions; }
            set { _optionActions = value; }
        }
    }
}
