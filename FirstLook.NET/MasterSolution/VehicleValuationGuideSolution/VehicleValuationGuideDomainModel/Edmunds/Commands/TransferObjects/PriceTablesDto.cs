﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects
{
    [Serializable]
    public class PriceTablesDto
    {
        private PriceTableDto _retail;
        private PriceTableDto _privateParty;
        private PriceTableDto _tradeIn;

        public PriceTableDto Retail
        {
            get { return _retail; }
            set { _retail = value; }
        }

        public PriceTableDto PrivateParty
        {
            get { return _privateParty; }
            set { _privateParty = value; }
        }

        public PriceTableDto TradeIn
        {
            get { return _tradeIn; }
            set { _tradeIn = value; }
        }
    }
}
