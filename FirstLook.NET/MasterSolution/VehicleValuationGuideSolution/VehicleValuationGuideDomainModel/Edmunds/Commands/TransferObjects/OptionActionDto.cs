﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects
{
    [Serializable]
    public class OptionActionDto
    {
        private OptionActionTypeDto _actionType;
        private long _optionId;

        public OptionActionTypeDto ActionType
        {
            get { return _actionType; }
            set { _actionType = value; }
        }

        public long OptionId
        {
            get { return _optionId; }
            set { _optionId = value; }
        }
    }
}
