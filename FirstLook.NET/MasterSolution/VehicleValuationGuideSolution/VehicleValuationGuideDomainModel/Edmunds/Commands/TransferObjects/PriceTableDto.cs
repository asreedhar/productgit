﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects
{
    [Serializable]
    public class PriceTableDto
    {
        private List<PriceTableRowDto> _rows = new List<PriceTableRowDto>();

        public List<PriceTableRowDto> Rows
        {
            get { return _rows; }
            set { _rows = value; }
        }
    }
}
