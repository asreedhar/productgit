﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects
{
    [Serializable]
    public class OptionStateDto
    {
        private long _optionId;
        private bool _selected;

        public long OptionId
        {
            get { return _optionId; }
            set { _optionId = value; }
        }

        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; }
        }
    }
}
