﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Publications
{
    [Serializable]
    public enum ChangeAgentDto
    {
        Undefined,
        User,
        System
    }
}
