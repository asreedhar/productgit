﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Publications
{
    [Serializable]
    public class EditionDto
    {
        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public UserDto User { get; set; }
    }
}
