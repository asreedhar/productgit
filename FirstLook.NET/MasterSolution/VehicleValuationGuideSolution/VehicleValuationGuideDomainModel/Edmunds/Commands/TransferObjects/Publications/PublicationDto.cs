﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Publications
{
    [Serializable]
    public class PublicationDto : PublicationInfoDto
    {
        public VehicleConfigurationDto VehicleConfiguration { get; set; }

        public List<ManufacturerColorDto> Colors { get; set; }

        public List<OptionDto> Options { get; set; }

        public PriceTablesDto Tables { get; set; }
    }
}
