﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Publications
{
    [Serializable]
    [Flags]
    public enum ChangeTypeDto
    {
        None = 0,
        DataLoad = 1 << 0,
        State = 1 << 1,
        StyleId = 1 << 2,
        Mileage = 1 << 3,
        OptionActions = 1 << 4,
        OptionStates = 1 << 5
    }
}
