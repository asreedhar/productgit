﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects
{
    [Serializable]
    public class PricesDto
    {
        private int _outstanding;
        private int _clean;
        private int _average;
        private int _rough;

        public int Outstanding
        {
            get { return _outstanding; }
            set { _outstanding = value; }
        }

        public int Clean
        {
            get { return _clean; }
            set { _clean = value; }
        }

        public int Average
        {
            get { return _average; }
            set { _average = value; }
        }

        public int Rough
        {
            get { return _rough; }
            set { _rough = value; }
        }
    }
}
