﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects
{
    [Serializable]
    public class OptionDto
    {
        private long _id;
        private string _name;

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private int _retail;
        private int _privateParty;
        private int _tradeIn;

        public int Retail
        {
            get { return _retail; }
            set { _retail = value; }
        }

        public int PrivateParty
        {
            get { return _privateParty; }
            set { _privateParty = value; }
        }

        public int TradeIn
        {
            get { return _tradeIn; }
            set { _tradeIn = value; }
        }
    }
}
