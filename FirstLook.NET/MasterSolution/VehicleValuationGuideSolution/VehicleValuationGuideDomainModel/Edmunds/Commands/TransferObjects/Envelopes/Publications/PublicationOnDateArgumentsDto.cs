using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Envelopes.Publications
{
    [Serializable]
    public class PublicationOnDateArgumentsDto : PublicationArgumentsDto
    {
        public DateTime On { get; set; }
    }
}
