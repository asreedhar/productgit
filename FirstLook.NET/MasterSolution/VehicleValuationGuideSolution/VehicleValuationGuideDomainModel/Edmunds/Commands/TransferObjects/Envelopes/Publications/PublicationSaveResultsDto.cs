using System;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Publications;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Envelopes.Publications
{
    [Serializable]
    public class PublicationSaveResultsDto
    {
        public PublicationSaveArgumentsDto Arguments { get; set; }

        public PublicationDto Publication { get; set; }
    }
}
