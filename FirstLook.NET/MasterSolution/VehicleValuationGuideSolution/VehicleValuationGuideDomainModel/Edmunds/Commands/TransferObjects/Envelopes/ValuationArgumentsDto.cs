using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ValuationArgumentsDto : ArgumentsDto
    {
        private int _mileage;
        private bool _hasMileage;
        
        public bool HasMileage
        {
            get { return _hasMileage; }
            set { _hasMileage = value; }
        }

        public int Mileage
        {
            get { return _mileage; }
            set { _mileage = value; }
        }

        private int _genericColorId;
        private bool _hasGenericColorId;

        public int GenericColorId
        {
            get { return _genericColorId; }
            set { _genericColorId = value; }
        }

        public bool HasGenericColorId
        {
            get { return _hasGenericColorId; }
            set { _hasGenericColorId = value; }
        }

        private int _manufacturerColorId;
        private bool _hasManufacturerColorId;

        public int ManufacturerColorId
        {
            get { return _manufacturerColorId; }
            set { _manufacturerColorId = value; }
        }

        public bool HasManufacturerColorId
        {
            get { return _hasManufacturerColorId; }
            set { _hasManufacturerColorId = value; }
        }
    }
}