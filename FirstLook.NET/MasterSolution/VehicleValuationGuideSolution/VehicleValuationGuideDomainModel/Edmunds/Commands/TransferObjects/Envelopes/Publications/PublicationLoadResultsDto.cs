using System;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Publications;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Envelopes.Publications
{
    [Serializable]
    public class PublicationLoadResultsDto
    {
        public PublicationLoadArgumentsDto Arguments { get; set; }

        public PublicationDto Publication { get; set;}
    }
}
