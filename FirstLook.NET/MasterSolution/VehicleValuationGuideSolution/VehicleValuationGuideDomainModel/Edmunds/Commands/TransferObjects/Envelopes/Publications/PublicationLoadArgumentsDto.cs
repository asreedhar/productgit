using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Envelopes.Publications
{
    [Serializable]
    public class PublicationLoadArgumentsDto : PublicationArgumentsDto
    {
        public int Id { get; set; }
    }
}
