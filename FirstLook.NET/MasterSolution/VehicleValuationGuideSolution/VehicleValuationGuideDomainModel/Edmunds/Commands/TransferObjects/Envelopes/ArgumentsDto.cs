﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ArgumentsDto
    {
        private string _state;

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }
    }
}
