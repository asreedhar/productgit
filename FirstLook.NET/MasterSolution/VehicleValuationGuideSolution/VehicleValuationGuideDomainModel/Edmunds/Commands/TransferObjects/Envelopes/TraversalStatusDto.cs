﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public enum TraversalStatusDto
    {
        Success,
        InvalidVinLength,
        InvalidVinCharacters,
        InvalidVinChecksum,
        NoDataForVin
    }
}
