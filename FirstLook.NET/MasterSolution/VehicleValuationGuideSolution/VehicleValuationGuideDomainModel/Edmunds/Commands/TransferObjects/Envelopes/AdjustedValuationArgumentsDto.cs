﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class AdjustedValuationArgumentsDto : ValuationArgumentsDto
    {
        private VehicleConfigurationDto _configuration;
        private OptionActionDto _optionAction;

        public VehicleConfigurationDto VehicleConfiguration
        {
            get { return _configuration; }
            set { _configuration = value; }
        }

        public OptionActionDto OptionAction
        {
            get { return _optionAction; }
            set { _optionAction = value; }
        }
    }
}
