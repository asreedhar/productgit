﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class InitialValuationResultsDto : ValuationResultsDto
    {
        private InitialValuationArgumentsDto _arguments;
        private VehicleConfigurationDto _vehicleConfiguration;
        private List<OptionDto> _options;
        private List<ManufacturerColorDto> _colors;

        public InitialValuationArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public VehicleConfigurationDto VehicleConfiguration
        {
            get { return _vehicleConfiguration; }
            set { _vehicleConfiguration = value; }
        }

        public List<OptionDto> Options
        {
            get { return _options; }
            set { _options = value; }
        }

        public List<ManufacturerColorDto> Colors
        {
            get { return _colors; }
            set { _colors = value; }
        }
    }
}
