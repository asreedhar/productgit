﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class InitialValuationArgumentsDto : ValuationArgumentsDto
    {
        private string _vin;
        private int _styleId;

        public string Vin
        {
            get { return _vin; }
            set { _vin = value; }
        }

        public int StyleId
        {
            get { return _styleId; }
            set { _styleId = value; }
        }
    }
}
