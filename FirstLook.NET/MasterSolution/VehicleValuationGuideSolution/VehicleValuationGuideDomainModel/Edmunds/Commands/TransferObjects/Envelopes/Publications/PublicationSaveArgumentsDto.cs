using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Commands.TransferObjects.Envelopes.Publications
{
    [Serializable]
    public class PublicationSaveArgumentsDto : PublicationArgumentsDto
    {
        public VehicleConfigurationDto VehicleConfiguration { get; set; }
    }
}
