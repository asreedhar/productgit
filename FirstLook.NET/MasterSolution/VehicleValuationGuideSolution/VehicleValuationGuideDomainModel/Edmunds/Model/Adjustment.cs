﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model
{
    [Serializable]
    public class Adjustment
    {
        private AdjustmentType _adjustmentType;
        private Condition _condition;
        private PriceType _priceType;
        private float _percent;

        public AdjustmentType AdjustmentType
        {
            get { return _adjustmentType; }
            internal set { _adjustmentType = value; }
        }

        public Condition Condition
        {
            get { return _condition; }
            internal set { _condition = value; }
        }

        public float Percent
        {
            get { return _percent; }
            internal set { _percent = value; }
        }

        public PriceType PriceType
        {
            get { return _priceType; }
            internal set { _priceType = value; }
        }
    }
}
