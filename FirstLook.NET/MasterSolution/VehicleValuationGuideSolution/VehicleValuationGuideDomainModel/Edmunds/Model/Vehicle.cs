﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model
{
    [Serializable]
    public class Vehicle
    {
        private Year _year;
        private Make _make;
        private Model _model;
        private Style _style;

        public Year Year
        {
            get { return _year; }
            internal set { _year = value; }
        }

        public Make Make
        {
            get { return _make; }
            internal set { _make = value; }
        }

        public Model Model
        {
            get { return _model; }
            internal set { _model = value; }
        }

        public Style Style
        {
            get { return _style; }
            internal set { _style = value; }
        }

        private IList<Price> _basePrices;

        public IList<Price> BasePrices
        {
            get { return _basePrices; }
            internal set { _basePrices = value; }
        }
    }
}
