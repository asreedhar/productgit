﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model
{
    [Serializable]
    public class Option
    {
        private long _id;
        private string _name;

        public long Id
        {
            get { return _id; }
            internal set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            internal set { _name = value; }
        }

        private IList<Price> _prices;

        public IList<Price> Prices
        {
            get { return _prices; }
            set { _prices = value; }
        }
    }
}
