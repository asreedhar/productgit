﻿using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Lookup
{
    public interface ILookupRegion
    {
        Region Lookup(int dataLoad, string stateCode);
    }

    public class LookupRegion : ILookupRegion
    {
        public Region Lookup(int dataLoad, string stateCode)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Region> serializer = registry.Resolve<ISqlSerializer<Region>>();

            using (IDataReader reader = service.Region(dataLoad, stateCode))
            {
                if (reader.Read())
                {
                    return serializer.Deserialize((IDataRecord)reader);
                }

                return null;
            }
        }
    }

    public class CachedLookupRegion : CachedLookup, ILookupRegion
    {
        private readonly ILookupRegion _source = new LookupRegion();

        public Region Lookup(int dataLoad, string stateCode)
        {
            string key = CreateCacheKey(dataLoad, stateCode);

            Region values = Cache.Get(key) as Region;

            if (values == null)
            {
                values = _source.Lookup(dataLoad, stateCode);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
