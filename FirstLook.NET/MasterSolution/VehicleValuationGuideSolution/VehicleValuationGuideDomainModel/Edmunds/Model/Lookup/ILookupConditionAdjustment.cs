﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Lookup
{
    public interface ILookupConditionAdjustment
    {
        IList<Adjustment> Lookup(int dataLoad, int styleId);
    }

    public class LookupConditionAdjustment : ILookupConditionAdjustment
    {
        public IList<Adjustment> Lookup(int dataLoad, int styleId)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Adjustment> serializer = registry.Resolve<ISqlSerializer<Adjustment>>();

            using (IDataReader reader = service.ConditionAdjustments(dataLoad, styleId))
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupConditionAdjustment : CachedLookup, ILookupConditionAdjustment
    {
        private readonly ILookupConditionAdjustment _source = new LookupConditionAdjustment();

        public IList<Adjustment> Lookup(int dataLoad, int styleId)
        {
            string key = CreateCacheKey(dataLoad, styleId);

            IList<Adjustment> values = Cache.Get(key) as IList<Adjustment>;

            if (values == null)
            {
                values = _source.Lookup(dataLoad, styleId);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
