﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Lookup
{
    public interface ILookupOption
    {
        IList<Option> Lookup(int dataLoad, int styleId);
    }

    public class LookupOption : ILookupOption
    {
        public IList<Option> Lookup(int dataLoad, int styleId)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Option> serializer = registry.Resolve<ISqlSerializer<Option>>();

            using (IDataReader reader = service.Options(dataLoad, styleId))
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupOption : CachedLookup, ILookupOption
    {
        private readonly ILookupOption _source = new LookupOption();

        public IList<Option> Lookup(int dataLoad, int styleId)
        {
            string key = CreateCacheKey(dataLoad, styleId);

            IList<Option> values = Cache.Get(key) as IList<Option>;

            if (values == null)
            {
                values = _source.Lookup(dataLoad, styleId);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
