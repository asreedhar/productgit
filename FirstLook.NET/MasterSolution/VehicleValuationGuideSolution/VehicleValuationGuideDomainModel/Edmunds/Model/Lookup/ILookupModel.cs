﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Lookup
{
    public interface ILookupModel
    {
        IList<Model> Lookup(int dataLoad, int year, string make);
    }

    public class LookupModel : ILookupModel
    {
        public IList<Model> Lookup(int dataLoad, int year, string make)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Model> serializer = registry.Resolve<ISqlSerializer<Model>>();

            using (IDataReader reader = service.Query(dataLoad, year, make))
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupModel : CachedLookup, ILookupModel
    {
        private readonly ILookupModel _source = new LookupModel();

        public IList<Model> Lookup(int dataLoad, int year, string make)
        {
            string key = CreateCacheKey(dataLoad, year, make);

            IList<Model> values = Cache.Get(key) as IList<Model>;

            if (values == null)
            {
                values = _source.Lookup(dataLoad, year, make);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
