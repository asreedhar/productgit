﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Lookup
{
    public interface ILookupDataLoad
    {
        IList<DataLoad> Lookup();
    }

    public class LookupDataLoad : ILookupDataLoad
    {
        public IList<DataLoad> Lookup()
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<DataLoad> serializer = registry.Resolve<ISqlSerializer<DataLoad>>();

            using (IDataReader reader = service.DataLoad())
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupDataLoad : CachedLookup, ILookupDataLoad
    {
        private readonly ILookupDataLoad _source = new LookupDataLoad();

        public IList<DataLoad> Lookup()
        {
            string key = CreateCacheKey();

            IList<DataLoad> values = Cache.Get(key) as IList<DataLoad>;

            if (values == null)
            {
                values = _source.Lookup();

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
