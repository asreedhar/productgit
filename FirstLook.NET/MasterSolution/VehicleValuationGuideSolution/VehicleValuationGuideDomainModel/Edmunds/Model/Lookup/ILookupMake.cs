﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Lookup
{
    public interface ILookupMake
    {
        IList<Make> Lookup(int dataLoad, int yearId);
    }

    public class LookupMake : ILookupMake
    {
        public IList<Make> Lookup(int dataLoad, int yearId)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Make> serializer = registry.Resolve<ISqlSerializer<Make>>();

            using (IDataReader reader = service.Query(dataLoad, yearId))
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupMake : CachedLookup, ILookupMake
    {
        private readonly ILookupMake _source = new LookupMake();

        public IList<Make> Lookup(int dataLoad, int yearId)
        {
            string key = CreateCacheKey(dataLoad, yearId);

            IList<Make> values = Cache.Get(key) as IList<Make>;

            if (values == null)
            {
                values = _source.Lookup(dataLoad, yearId);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
