﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Lookup
{
    public interface ILookupRegionAdjustment
    {
        IList<Adjustment> Lookup(int dataLoad, int styleId, int regionId);
    }

    public class LookupRegionAdjustment : ILookupRegionAdjustment
    {
        public IList<Adjustment> Lookup(int dataLoad, int styleId, int regionId)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Adjustment> serializer = registry.Resolve<ISqlSerializer<Adjustment>>();

            using (IDataReader reader = service.RegionAdjustments(dataLoad, styleId, regionId))
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupRegionAdjustment : CachedLookup, ILookupRegionAdjustment
    {
        private readonly ILookupRegionAdjustment _source = new LookupRegionAdjustment();

        public IList<Adjustment> Lookup(int dataLoad, int styleId, int regionId)
        {
            string key = CreateCacheKey(dataLoad, styleId, regionId);

            IList<Adjustment> values = Cache.Get(key) as IList<Adjustment>;

            if (values == null)
            {
                values = _source.Lookup(dataLoad, styleId, regionId);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
