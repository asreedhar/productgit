﻿using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Lookup
{
    public interface ILookupCertifiedCost
    {
        CertifiedCost Lookup(int dataLoad, int styleId);
    }

    public class LookupCertifiedCost : ILookupCertifiedCost
    {
        public CertifiedCost Lookup(int dataLoad, int styleId)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<CertifiedCost> serializer = registry.Resolve<ISqlSerializer<CertifiedCost>>();

            using (IDataReader reader = service.CertifiedCost(dataLoad, styleId))
            {
                if (reader.Read())
                {
                    return serializer.Deserialize((IDataRecord)reader);
                }

                return null;
            }
        }
    }

    public class CachedLookupCertifiedCost : CachedLookup, ILookupCertifiedCost
    {
        private readonly ILookupCertifiedCost _source = new LookupCertifiedCost();

        public CertifiedCost Lookup(int dataLoad, int styleId)
        {
            string key = CreateCacheKey(dataLoad, styleId);

            CertifiedCost values = Cache.Get(key) as CertifiedCost;

            if (values == null)
            {
                values = _source.Lookup(dataLoad, styleId);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
