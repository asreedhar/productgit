﻿using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Lookup
{
    public interface ILookupMileageAdjustment
    {
        MileageAdjustment Lookup(int dataLoad, int styleId);
    }

    public class LookupMileageAdjustment : ILookupMileageAdjustment
    {
        public MileageAdjustment Lookup(int dataLoad, int styleId)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<MileageAdjustment> serializer = registry.Resolve<ISqlSerializer<MileageAdjustment>>();

            using (IDataReader reader = service.MileageAdjustments(dataLoad, styleId))
            {
                if (reader.Read())
                {
                    return serializer.Deserialize((IDataRecord)reader);
                }

                return null;
            }
        }
    }

    public class CachedLookupMileageAdjustment : CachedLookup, ILookupMileageAdjustment
    {
        private readonly ILookupMileageAdjustment _source = new LookupMileageAdjustment();

        public MileageAdjustment Lookup(int dataLoad, int styleId)
        {
            string key = CreateCacheKey(dataLoad, styleId);

            MileageAdjustment values = Cache.Get(key) as MileageAdjustment;

            if (values == null)
            {
                values = _source.Lookup(dataLoad, styleId);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
