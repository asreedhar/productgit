﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Lookup
{
    public interface ILookupColorAdjustment
    {
        IList<Adjustment> Lookup(int dataLoad, int styleId, int regionId, int colorId);
    }

    public class LookupColorAdjustment : ILookupColorAdjustment
    {
        public IList<Adjustment> Lookup(int dataLoad, int styleId, int regionId, int colorId)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Adjustment> serializer = registry.Resolve<ISqlSerializer<Adjustment>>();

            using (IDataReader reader = service.ColorAdjustments(dataLoad, styleId, regionId, colorId))
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupColorAdjustment : CachedLookup, ILookupColorAdjustment
    {
        private readonly ILookupColorAdjustment _source = new LookupColorAdjustment();

        public IList<Adjustment> Lookup(int dataLoad, int styleId, int regionId, int colorId)
        {
            string key = CreateCacheKey(dataLoad, styleId, regionId, colorId);

            IList<Adjustment> values = Cache.Get(key) as IList<Adjustment>;

            if (values == null)
            {
                values = _source.Lookup(dataLoad, styleId, regionId, colorId);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
