﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Lookup
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ILookupDataLoad, CachedLookupDataLoad>(ImplementationScope.Shared);

            registry.Register<ILookupYear, CachedLookupYear>(ImplementationScope.Shared);

            registry.Register<ILookupMake, CachedLookupMake>(ImplementationScope.Shared);

            registry.Register<ILookupModel, CachedLookupModel>(ImplementationScope.Shared);

            registry.Register<ILookupStyle, CachedLookupStyle>(ImplementationScope.Shared);

            registry.Register<ILookupVin, CachedLookupVin>(ImplementationScope.Shared);

            registry.Register<ILookupVehicle, CachedLookupVehicle>(ImplementationScope.Shared);

            // new lookups

            registry.Register<ILookupCertifiedCost, CachedLookupCertifiedCost>(ImplementationScope.Shared);

            registry.Register<ILookupCertifiedCriteria, CachedLookupCertifiedCriteria>(ImplementationScope.Shared);

            registry.Register<ILookupCertifiedMileageAdjustment, CachedLookupCertifiedMileageAdjustment>(ImplementationScope.Shared);

            registry.Register<ILookupColor, CachedLookupColor>(ImplementationScope.Shared);

            registry.Register<ILookupColorAdjustment, CachedLookupColorAdjustment>(ImplementationScope.Shared);

            registry.Register<ILookupConditionAdjustment, CachedLookupConditionAdjustment>(ImplementationScope.Shared);

            registry.Register<ILookupMileageAdjustment, CachedLookupMileageAdjustment>(ImplementationScope.Shared);

            registry.Register<ILookupOption, CachedLookupOption>(ImplementationScope.Shared);

            registry.Register<ILookupRegion, CachedLookupRegion>(ImplementationScope.Shared);

            registry.Register<ILookupRegionAdjustment, CachedLookupRegionAdjustment>(ImplementationScope.Shared);
        }
    }
}
