﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Lookup
{
    public interface ILookupCertifiedMileageAdjustment
    {
        IList<CertifiedMileageAdjustment> Lookup(int dataLoad, int styleId);
    }

    public class LookupCertifiedMileageAdjustment : ILookupCertifiedMileageAdjustment
    {
        public IList<CertifiedMileageAdjustment> Lookup(int dataLoad, int styleId)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<CertifiedMileageAdjustment> serializer = registry.Resolve<ISqlSerializer<CertifiedMileageAdjustment>>();

            using (IDataReader reader = service.CertifiedMileageAdjustments(dataLoad, styleId))
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupCertifiedMileageAdjustment : CachedLookup, ILookupCertifiedMileageAdjustment
    {
        private readonly ILookupCertifiedMileageAdjustment _source = new LookupCertifiedMileageAdjustment();

        public IList<CertifiedMileageAdjustment> Lookup(int dataLoad, int styleId)
        {
            string key = CreateCacheKey(dataLoad, styleId);

            IList<CertifiedMileageAdjustment> values = Cache.Get(key) as IList<CertifiedMileageAdjustment>;

            if (values == null)
            {
                values = _source.Lookup(dataLoad, styleId);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
