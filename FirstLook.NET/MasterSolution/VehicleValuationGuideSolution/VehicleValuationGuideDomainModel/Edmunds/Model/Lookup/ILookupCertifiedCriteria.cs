﻿using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Lookup
{
    public interface ILookupCertifiedCriteria
    {
        CertifiedCriteria Lookup(int dataLoad, int styleId);
    }

    public class LookupCertifiedCriteria : ILookupCertifiedCriteria
    {
        public CertifiedCriteria Lookup(int dataLoad, int styleId)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<CertifiedCriteria> serializer = registry.Resolve<ISqlSerializer<CertifiedCriteria>>();

            using (IDataReader reader = service.CertifiedCriteria(dataLoad, styleId))
            {
                if (reader.Read())
                {
                    return serializer.Deserialize((IDataRecord)reader);
                }

                return null;
            }
        }
    }

    public class CachedLookupCertifiedCriteria : CachedLookup, ILookupCertifiedCriteria
    {
        private readonly ILookupCertifiedCriteria _source = new LookupCertifiedCriteria();

        public CertifiedCriteria Lookup(int dataLoad, int styleId)
        {
            string key = CreateCacheKey(dataLoad, styleId);

            CertifiedCriteria values = Cache.Get(key) as CertifiedCriteria;

            if (values == null)
            {
                values = _source.Lookup(dataLoad, styleId);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
