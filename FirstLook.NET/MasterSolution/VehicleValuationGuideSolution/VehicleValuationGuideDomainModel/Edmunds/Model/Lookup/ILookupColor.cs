﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Lookup
{
    public interface ILookupColor
    {
        IList<ManufacturerColor> Lookup(int dataLoad, int styleId);
    }

    public class LookupColor : ILookupColor
    {
        public IList<ManufacturerColor> Lookup(int dataLoad, int styleId)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<ManufacturerColor> serializer = registry.Resolve<ISqlSerializer<ManufacturerColor>>();

            using (IDataReader reader = service.Colors(dataLoad, styleId))
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupColor : CachedLookup, ILookupColor
    {
        private readonly ILookupColor _source = new LookupColor();

        public IList<ManufacturerColor> Lookup(int dataLoad, int styleId)
        {
            string key = CreateCacheKey(dataLoad, styleId);

            IList<ManufacturerColor> values = Cache.Get(key) as IList<ManufacturerColor>;

            if (values == null)
            {
                values = _source.Lookup(dataLoad, styleId);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
