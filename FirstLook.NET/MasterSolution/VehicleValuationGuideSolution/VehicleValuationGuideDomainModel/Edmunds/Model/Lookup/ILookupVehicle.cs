﻿using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Lookup
{
    public interface ILookupVehicle
    {
        Vehicle Lookup(int dataLoad, int styleId);
    }

    public class LookupVehicle : ILookupVehicle
    {
        public Vehicle Lookup(int dataLoad, int styleId)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Vehicle> serializer = registry.Resolve<ISqlSerializer<Vehicle>>();

            using (IDataReader reader = service.Vehicle(dataLoad, styleId))
            {
                if (reader.Read())
                {
                    return serializer.Deserialize((IDataRecord)reader);
                }

                return null;
            }
        }
    }

    public class CachedLookupVehicle : CachedLookup, ILookupVehicle
    {
        private readonly ILookupVehicle _source = new LookupVehicle();

        public Vehicle Lookup(int dataLoad, int styleId)
        {
            string key = CreateCacheKey(dataLoad, styleId);

            Vehicle values = Cache.Get(key) as Vehicle;

            if (values == null)
            {
                values = _source.Lookup(dataLoad, styleId);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
