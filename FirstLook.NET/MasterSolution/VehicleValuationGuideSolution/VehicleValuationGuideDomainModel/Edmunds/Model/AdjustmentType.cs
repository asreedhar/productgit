﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model
{
    [Serializable]
    public enum AdjustmentType
    {
        Undefined,
        Color,
        Condition,
        Region
    }
}
