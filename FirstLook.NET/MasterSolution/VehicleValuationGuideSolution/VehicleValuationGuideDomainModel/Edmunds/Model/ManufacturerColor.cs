﻿using System;
using System.Drawing;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model
{
    [Serializable]
    public class ManufacturerColor
    {
        private GenericColor _genericColor;
        private int _id;
        private string _name;
        private string _code;
        private ColorType _colorType;
        private Color _color;

        public GenericColor GenericColor
        {
            get { return _genericColor; }
            internal set { _genericColor = value; }
        }

        public int Id
        {
            get { return _id; }
            internal set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            internal set { _name = value; }
        }

        public string Code
        {
            get { return _code; }
            internal set { _code = value; }
        }

        public ColorType ColorType
        {
            get { return _colorType; }
            internal set { _colorType = value; }
        }

        public Color Color
        {
            get { return _color; }
            internal set { _color = value; }
        }
    }
}
