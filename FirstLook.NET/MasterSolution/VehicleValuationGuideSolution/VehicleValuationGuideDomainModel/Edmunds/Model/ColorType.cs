﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model
{
    [Serializable]
    public enum ColorType
    {
        Undefined,
        Exterior,
        Interior
    }
}
