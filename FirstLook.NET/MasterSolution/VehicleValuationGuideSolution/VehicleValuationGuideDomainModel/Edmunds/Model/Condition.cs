﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model
{
    [Serializable]
    public enum Condition
    {
        Undefined,
        Average,
        Clean,
        Outstanding,
        Rough
    }
}
