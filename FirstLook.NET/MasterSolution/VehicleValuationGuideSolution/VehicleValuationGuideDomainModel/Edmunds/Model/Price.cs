﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model
{
    [Serializable]
    public class Price
    {
        private PriceType _priceType;
        private int _amount;

        public PriceType PriceType
        {
            get { return _priceType; }
            internal set { _priceType = value; }
        }

        public int Amount
        {
            get { return _amount; }
            internal set { _amount = value; }
        }
    }
}
