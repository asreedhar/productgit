﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model
{
    [Serializable]
    public class MileageAdjustment
    {
        private int _low0, _low1, _low2, _low3, _low4;

        public int Low0
        {
            get { return _low0; }
            internal set { _low0 = value; }
        }

        public int Low1
        {
            get { return _low1; }
            internal set { _low1 = value; }
        }

        public int Low2
        {
            get { return _low2; }
            internal set { _low2 = value; }
        }

        public int Low3
        {
            get { return _low3; }
            internal set { _low3 = value; }
        }

        public int Low4
        {
            get { return _low4; }
            internal set { _low4 = value; }
        }

        private int _high0, _high1, _high2, _high3, _high4;

        public int High0
        {
            get { return _high0; }
            internal set { _high0 = value; }
        }

        public int High1
        {
            get { return _high1; }
            internal set { _high1 = value; }
        }

        public int High2
        {
            get { return _high2; }
            internal set { _high2 = value; }
        }

        public int High3
        {
            get { return _high3; }
            internal set { _high3 = value; }
        }

        public int High4
        {
            get { return _high4; }
            internal set { _high4 = value; }
        }

        private float _add1, _add2, _add3, _add4;

        public float Add1
        {
            get { return _add1; }
            internal set { _add1 = value; }
        }

        public float Add2
        {
            get { return _add2; }
            internal set { _add2 = value; }
        }

        public float Add3
        {
            get { return _add3; }
            internal set { _add3 = value; }
        }

        public float Add4
        {
            get { return _add4; }
            internal set { _add4 = value; }
        }

        private float _deduct1, _deduct2, _deduct3, _deduct4;

        public float Deduct1
        {
            get { return _deduct1; }
            internal set { _deduct1 = value; }
        }

        public float Deduct2
        {
            get { return _deduct2; }
            internal set { _deduct2 = value; }
        }

        public float Deduct3
        {
            get { return _deduct3; }
            internal set { _deduct3 = value; }
        }

        public float Deduct4
        {
            get { return _deduct4; }
            internal set { _deduct4 = value; }
        }
    }
}
