﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model
{
    [Serializable]
    public class CertifiedCriteria
    {
        private int _maxYears;
        private int _maxMileage;

        public int MaxYears
        {
            get { return _maxYears; }
            internal set { _maxYears = value; }
        }

        public int MaxMileage
        {
            get { return _maxMileage; }
            internal set { _maxMileage = value; }
        }
    }
}
