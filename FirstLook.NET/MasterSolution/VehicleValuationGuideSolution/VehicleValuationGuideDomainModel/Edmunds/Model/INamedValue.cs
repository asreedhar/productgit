﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model
{
    public interface INamedValue
    {
        string Value { get; }
    }

    [Serializable]
    public abstract class NamedValue : INamedValue
    {
        private string _value;

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }

    [Serializable]
    public class Year
    {
        private int _value;

        public int Value
        {
            get { return _value; }
            internal set { _value = value; }
        }
    }

    [Serializable]
    public class Make : NamedValue
    {
    }

    [Serializable]
    public class Model : NamedValue
    {
    }

    [Serializable]
    public class Style : NamedValue
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            internal set { _id = value; }
        }
    }

    [Serializable]
    public class DataLoad : NamedValue
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            internal set { _id = value; }
        }
    }
}
