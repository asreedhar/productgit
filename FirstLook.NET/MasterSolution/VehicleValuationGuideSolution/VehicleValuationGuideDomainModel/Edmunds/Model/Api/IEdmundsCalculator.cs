﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Api
{
    public interface IEdmundsCalculator
    {
        Matrix Calculate(IEdmundsService service, IVehicleConfiguration configuration);
    }

    [Serializable]
    public enum Valuation
    {
        Undefined,
        Base,
        Option,
        Mileage,
        Color,
        Region,
        Condition,
        Final
    }

    public class Matrix
    {
        public class Cell
        {
            private bool _visible = true;

            private decimal? _value;

            internal Cell()
            {
            }

            public bool Visible
            {
                get { return _visible; }
                internal set { _visible = value; }
            }

            public decimal? Value
            {
                get { return _value; }
                internal set { _value = value; }
            }
        }

        internal static readonly int ConditionCount = Enum.GetNames(typeof(Condition)).Length;

        private static readonly int PriceTypeCount = Enum.GetNames(typeof(PriceType)).Length;

        private static readonly int ValuationCount = Enum.GetNames(typeof(Valuation)).Length;

        private readonly Cell[] _values;

        public Matrix()
        {
            int arrayLength = PriceTypeCount * ConditionCount * ValuationCount;

            _values = new Cell[arrayLength];

            foreach (Condition condition in Enum.GetValues(typeof(Condition)))
            {
                foreach (PriceType market in Enum.GetValues(typeof(PriceType)))
                {
                    foreach (Valuation valuation in Enum.GetValues(typeof(Valuation)))
                    {
                        Cell cell = new Cell();

                        if (market == PriceType.Undefined || condition == Condition.Undefined || valuation == Valuation.Undefined)
                        {
                            cell.Visible = false;
                        }

                        this[market, condition, valuation] = cell;
                    }
                }
            }
        }

        public Cell this[PriceType priceType, Condition condition, Valuation valuation]
        {
            get
            {
                return _values[Index(priceType, condition, valuation)];
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentException("A Matrix Cell should never be set to null!");
                }

                _values[Index(priceType, condition, valuation)] = value;
            }
        }

        private Cell _certified = new Cell { Visible = false };

        public Cell Certified
        {
            get { return _certified; }
            internal set { _certified = value; }
        }

        private static int Index(PriceType priceType, Condition condition, Valuation valuation)
        {
            int rowOffset = ConditionCount * ValuationCount, colOffset = ValuationCount;

            int row = ((int)priceType);

            int col = ((int)condition);

            int dep = ((int)valuation);

            return row * rowOffset + col * colOffset + dep;
        }
    }
}
