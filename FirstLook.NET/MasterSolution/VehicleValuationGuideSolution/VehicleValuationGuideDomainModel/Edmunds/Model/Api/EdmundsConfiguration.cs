﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Api
{
    public class EdmundsConfiguration : IEdmundsConfiguration
    {
        public IVehicleConfiguration InitialConfiguration(int styleId, string vin, State state, int? mileage, int? genericColorId, int? manufacturerColorId)
        {
            IEdmundsService service = Resolve<IEdmundsService>();

            IVehicleConfigurationBuilderFactory factory = Resolve<IVehicleConfigurationBuilderFactory>();

            IVehicleConfigurationBuilder builder = factory.NewBuilder();

            builder.DataLoad = service.DataLoad();
            builder.StyleId = styleId;
            builder.Vin = vin;
            builder.State = state;
            builder.Mileage = mileage;
            builder.GenericColorId = genericColorId;
            builder.ManufacturerColorId = manufacturerColorId;

            InitializeConfiguration(service, builder);

            return builder.ToVehicleConfiguration();
        }

        private static void InitializeConfiguration(IEdmundsService service, IVehicleConfigurationBuilder builder)
        {
            IList<Option> options = service.Options(builder.StyleId);

            foreach (Option option in options)
            {
                builder.Add(option);
            }
        }

        public IVehicleConfiguration UpdateConfiguration(IEdmundsService service, IVehicleConfiguration configuration, IOptionAction action)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            if (action.ActionType == OptionActionType.Undefined)
            {
                throw new ArgumentOutOfRangeException("action", OptionActionType.Undefined, "Action type cannot be undefined");
            }

            Vehicle vehicle = service.Vehicle(configuration.StyleId);

            bool repeat = configuration.OptionActions.Count(
                              x => x.ActionType == action.ActionType &&
                                   x.OptionId == action.OptionId) > 0;

            if (repeat)
            {
                return configuration; // been there, done that ...
            }

            IVehicleConfigurationBuilderFactory factory = Resolve<IVehicleConfigurationBuilderFactory>();

            IVehicleConfigurationBuilder builder;

            bool undo = configuration.OptionActions.Count(
                              x => x.ActionType != action.ActionType &&
                                   x.OptionId == action.OptionId) > 0;

            if (undo)
            {
                builder = factory.NewBuilder();

                builder.DataLoad = service.DataLoad();
                builder.StyleId = configuration.StyleId;
                builder.Vin = configuration.Vin;
                builder.State = configuration.State;
                builder.Mileage = configuration.Mileage;
                builder.GenericColorId = configuration.GenericColorId;
                builder.ManufacturerColorId = configuration.ManufacturerColorId;

                InitializeConfiguration(service, builder);

                foreach (IOptionAction replay in configuration.OptionActions)
                {
                    if (replay.OptionId != action.OptionId)
                    {
                        PerformAction(service, replay, vehicle, builder);
                    }
                }
            }
            else
            {
                builder = factory.NewBuilder(configuration);

                PerformAction(service, action, vehicle, builder);
            }

            return builder.ToVehicleConfiguration();
        }

        static void PerformAction(IEdmundsService service, IOptionAction action, Vehicle vehicle, IVehicleConfigurationBuilder builder)
        {
            IList<Option> options = service.Options(vehicle.Style.Id);

            Option option = options.FirstOrDefault(x => Equals(x.Id, action.OptionId));

            if (option == null)
            {
                throw new ArgumentException(string.Format("No such Option {0}", action.OptionId), "action");
            }

            builder.Do(action.OptionId, action.ActionType);

            switch (action.ActionType)
            {
                case OptionActionType.Undefined:
                    throw new ArgumentException("Undefined OptionActionType", "action");
                case OptionActionType.Add:
                    AddOption(builder, option);
                    break;
                case OptionActionType.Remove:
                    RemoveOption(builder, option);
                    break;
            }
        }

        static void AddOption(IVehicleConfigurationBuilder builder, Option option)
        {
            if (builder.IsSelected(option.Id, true))
            {
                return;
            }

            builder.Select(option.Id);
        }

        static void RemoveOption(IVehicleConfigurationBuilder builder, Option adjustment)
        {
            if (!builder.IsSelected(adjustment.Id, false))
            {
                return;
            }

            builder.Deselect(adjustment.Id);
        }

        protected static T Resolve<T>()
        {
            return RegistryFactory.GetResolver().Resolve<T>();
        }
    }
}
