﻿using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Api
{
    public interface IEdmundsService
    {
        DataLoad DataLoad();

        Region Region(string stateCode);

        Vehicle Vehicle(int styleId);

        IList<Option> Options(int styleId);

        IList<ManufacturerColor> Colors(int styleId);

        IList<Adjustment> ColorAdjustments(int styleId, int regionId, int colorId);

        IList<Adjustment> ConditionAdjustments(int styleId);

        MileageAdjustment MileageAdjustment(int styleId);

        IList<Adjustment> RegionAdjustments(int styleId, int regionId);
    }
}
