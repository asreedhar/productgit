﻿using System;
using System.Linq;
using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Api
{
    public class EdmundsCalculator : IEdmundsCalculator
    {
        public Matrix Calculate(IEdmundsService service, IVehicleConfiguration configuration)
        {
            Vehicle vehicle = service.Vehicle(configuration.StyleId);

            return Calculate(service, vehicle, configuration);
        }

        static Matrix Calculate(IEdmundsService service, Vehicle vehicle, IVehicleConfiguration configuration)
        {
            Region region = service.Region(configuration.State.Code);

            Matrix matrix = new Matrix();

            CalculateBase(vehicle, matrix);

            CalculateOptions(service, configuration, matrix);

            CalculateMileage(service, configuration, matrix);

            CalculateColor(service, configuration, region, matrix);

            CalculateRegion(service, configuration, region, matrix);

            CalculateCondition(service, configuration, matrix);

            CalculateFinal(matrix);

            return matrix;
        }

        private static void CalculateBase(Vehicle vehicle, Matrix matrix)
        {
            foreach (Price value in vehicle.BasePrices)
            {
                foreach (Condition condition in Enum.GetValues(typeof(Condition)))
                {
                    matrix[value.PriceType, condition, Valuation.Base].Value = value.Amount;
                }
            }
        }

        private static void CalculateOptions(IEdmundsService service, IVehicleConfiguration configuration, Matrix matrix)
        {
            foreach (Option option in service.Options(configuration.StyleId))
            {
                Option opt = option; // no warnings plz

                IOptionState state = configuration.OptionStates.FirstOrDefault(
                    x => Equals(x.OptionId, opt.Id));

                if (state.Selected)
                {
                    foreach (Condition condition in Enum.GetValues(typeof(Condition)))
                    {
                        foreach (Price price in opt.Prices)
                        {
                            decimal? value = matrix[price.PriceType, condition, Valuation.Option].Value;

                            decimal? sum = Sum(value, price.Amount);

                            matrix[price.PriceType, condition, Valuation.Option].Value = sum;
                        }
                    }
                }
            }
        }

        private static void CalculateMileage(IEdmundsService service, IVehicleConfiguration configuration, Matrix matrix)
        {
            if (configuration.Mileage.HasValue)
            {
                int mileage = configuration.Mileage.Value;

                decimal? mileageAdjustment = null;

                MileageAdjustment adjustment = service.MileageAdjustment(configuration.StyleId);

                // 1. Low_Mileage <= Odometer <= High_Mileage
                // 2. Low_Bound_1 <= Odometer < Low_Mileage
                // 3. Low_Bound_2 <= Odometer < Low_Bound_1
                // 4. Low_Bound_3 <= Odometer < Low_Bound_2
                // 5. Low_Bound_4 <= Odometer < Low_Bound_3
                // 6. Odometer < Low_Bound_4

                if (adjustment.Low0 <= mileage && mileage <= adjustment.High0)
                {
                    mileageAdjustment = 0;
                }
                else if (adjustment.Low1 <= mileage && mileage < adjustment.Low0)
                {
                    // (Odometer – Low_Mileage) * Add_Factor_1

                    mileageAdjustment = Convert.ToDecimal(
                        (mileage - adjustment.Low0) * adjustment.Add1);
                }
                else if (adjustment.Low2 <= mileage && mileage < adjustment.Low1)
                {
                    // [(Low_Bound_1 - Odometer) * Add_Factor_2] + [(Low_Mileage – Low_Bound_1) * Add_Factor_1]

                    mileageAdjustment = Convert.ToDecimal(
                        (adjustment.Low1 - mileage) * adjustment.Add2 +
                        (adjustment.Low0 - adjustment.Low1) * adjustment.Add1);
                }
                else if (adjustment.Low3 <= mileage && mileage < adjustment.Low2)
                {
                    // [(Low_Bound_2 – Odometer) * Add_Factor_3] + [(Low_Bound_1 – Low_Bound_2) * Add_Factor_2] + [(Low_Mileage – Low_Bound_1) * Add_Factor_1]

                    mileageAdjustment = Convert.ToDecimal(
                        (adjustment.Low2 - mileage) * adjustment.Add3 +
                        (adjustment.Low1 - adjustment.Low2) * adjustment.Add2 +
                        (adjustment.Low0 - adjustment.Low1) * adjustment.Add1);
                }
                else if (adjustment.Low4 <= mileage && mileage < adjustment.Low3)
                {
                    // [(Low_Bound_3 – Odometer) * Add_Factor_4] + [(Low_Bound_2 – Low_Bound_3) * Add_Factor_3] + [(Low_Bound_1 – Low_Bound_2) * Add_Factor_2] + [(Low_Mileage – Low_Bound_1) * Add_Factor_1]

                    mileageAdjustment = Convert.ToDecimal(
                        (adjustment.Low3 - mileage) * adjustment.Add4 +
                        (adjustment.Low2 - adjustment.Low3) * adjustment.Add3 +
                        (adjustment.Low1 - adjustment.Low2) * adjustment.Add2 +
                        (adjustment.Low0 - adjustment.Low1) * adjustment.Add1);
                }
                else if (mileage < adjustment.Low4)
                {
                    mileageAdjustment = Convert.ToDecimal(
                        (adjustment.Low3 - adjustment.Low4) * adjustment.Add4 +
                        (adjustment.Low2 - adjustment.Low3) * adjustment.Add3 +
                        (adjustment.Low1 - adjustment.Low2) * adjustment.Add2 +
                        (adjustment.Low0 - adjustment.Low1) * adjustment.Add1);
                }

                // 7. High_Mileage < Odometer < High_Bound_1
                // 8. High_Bound_1 < Odometer <= High_Bound_2
                // 9. High_Bound_2 < Odometer <= High_Bound_3
                // 10. High_Bound_3 < Odometer <= High_Bound_4
                // 11. Odometer > High_Bound_4

                else if (adjustment.High0 < mileage && mileage < adjustment.High1)
                {
                    // (High_Mileage – Odometer) * Deduct_Factor_1

                    mileageAdjustment = Convert.ToDecimal(
                        (adjustment.High0 - mileage) * adjustment.Deduct1);
                }
                else if (adjustment.High1 < mileage && mileage <= adjustment.High2)
                {
                    // [(High_Mileage – High_Bound_1) * Deduct_Factor_1] + [(High_Bound_1 - Odometer) * Deduct_Factor_2]

                    mileageAdjustment = Convert.ToDecimal(
                        (adjustment.High0 - adjustment.High1) * adjustment.Deduct1 +
                        (adjustment.High1 - mileage) * adjustment.Deduct2);
                }
                else if (adjustment.High2 < mileage && mileage <= adjustment.High3)
                {
                    // [(High_Mileage – High_Bound_1) * Deduct_Factor_1] + [(High_Bound_1 – High_Bound_2) * Deduct_Factor_2] + [(High_Bound_2 – Odometer) * Deduct_Factor_3]

                    mileageAdjustment = Convert.ToDecimal(
                        (adjustment.High0 - adjustment.High1) * adjustment.Deduct1 +
                        (adjustment.High1 - adjustment.High2) * adjustment.Deduct2 +
                        (adjustment.High2 - mileage) * adjustment.Deduct3);
                }
                else if (adjustment.High3 < mileage && mileage <= adjustment.High4)
                {
                    // [(High_Mileage – High_Bound_1) * Deduct_Factor_1] + [(High_Bound_1 – High_Bound_2) * Deduct_Factor_2] + [(High_Bound_2 – High_Bound_3) * Deduct_Factor_3] + [(High_Bound_3 – Odometer) * Deduct_Factor_4]

                    mileageAdjustment = Convert.ToDecimal(
                        (adjustment.High0 - adjustment.High1) * adjustment.Deduct1 +
                        (adjustment.High1 - adjustment.High2) * adjustment.Deduct2 +
                        (adjustment.High2 - adjustment.High3) * adjustment.Deduct3 +
                        (adjustment.High3 - mileage) * adjustment.Deduct4);
                }
                else if (mileage > adjustment.High4)
                {
                    // [(High_Mileage – High_Bound_1) * Deduct_Factor_1] + [(High_Bound_1 – High_Bound_2) * Deduct_Factor_2] + [(High_Bound_2 – High_Bound_3) * Deduct_Factor_3] + [(High_Bound_3 – High_Bound_4) * Deduct_Factor_4]

                    mileageAdjustment = Convert.ToDecimal(
                        (adjustment.High0 - adjustment.High1) * adjustment.Deduct1 +
                        (adjustment.High1 - adjustment.High2) * adjustment.Deduct2 +
                        (adjustment.High2 - adjustment.High3) * adjustment.Deduct3 +
                        (adjustment.High3 - adjustment.High4) * adjustment.Deduct4);
                }

                if (mileageAdjustment.HasValue)
                {
                    decimal value = mileageAdjustment.Value;

                    if (Math.Sign(value) == 1)
                    {
                        // min(Mileage adjustment, .50 * TMVU_VEHICLE.Retail_Value)

                        decimal? limit = matrix[PriceType.Retail, Condition.Undefined, Valuation.Base].Value;

                        mileageAdjustment = Math.Min(value, limit.Value/2);
                    }
                    else
                    {
                        // max(Mileage adjustment, -.50 * TMVU_VEHICLE.Tradein_Value)

                        decimal? limit = matrix[PriceType.Retail, Condition.Undefined, Valuation.Base].Value;

                        mileageAdjustment = Math.Max(value, limit.Value/-2);
                    }

                    mileageAdjustment = Math.Round(mileageAdjustment.Value, MidpointRounding.ToEven);
                }

                // save value

                foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
                {
                    foreach (Condition condition in Enum.GetValues(typeof(Condition)))
                    {
                        matrix[priceType, condition, Valuation.Mileage].Value = mileageAdjustment;
                    }
                }
            }
        }

        private static void CalculateColor(IEdmundsService service, IVehicleConfiguration configuration, Region region, Matrix matrix)
        {
            if (configuration.GenericColorId.HasValue)
            {
                int colorId = configuration.GenericColorId.Value;

                foreach (Adjustment adjustment in service.ColorAdjustments(configuration.StyleId, region.Id, colorId))
                {
                    Condition condition = adjustment.Condition;

                    foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
                    {
                        if (priceType == PriceType.Undefined)
                        {
                            continue;
                        }

                        decimal? price = matrix[priceType, condition, Valuation.Base].Value;

                        decimal? value = null;

                        if (price.HasValue)
                        {
                            value = Convert.ToDecimal(adjustment.Percent)*price.Value;

                            value = Math.Round(value.Value, MidpointRounding.ToEven);
                        }

                        matrix[priceType, condition, Valuation.Color].Value = value;
                    }
                }
            }
        }

        private static void CalculateRegion(IEdmundsService service, IVehicleConfiguration configuration, Region region, Matrix matrix)
        {
            foreach (Adjustment adjustment in service.RegionAdjustments(configuration.StyleId, region.Id))
            {
                foreach (PriceType priceType in Enum.GetValues(typeof (PriceType)))
                {
                    foreach (Condition condition in Enum.GetValues(typeof (Condition)))
                    {
                        decimal? price = matrix[priceType, condition, Valuation.Base].Value;

                        decimal? value = null;

                        if (price.HasValue)
                        {
                            value = Convert.ToDecimal(adjustment.Percent)*price.Value;

                            value = Math.Round(value.Value, MidpointRounding.ToEven);
                        }

                        matrix[priceType, condition, Valuation.Region].Value = value;
                    }
                }
            }
        }

        private static void CalculateCondition(IEdmundsService service, IVehicleConfiguration configuration, Matrix matrix)
        {
            foreach (Adjustment adjustment in service.ConditionAdjustments(configuration.StyleId))
            {
                PriceType priceType = adjustment.PriceType;

                Condition condition = adjustment.Condition;

                decimal? price = matrix[priceType, condition, Valuation.Base].Value;

                decimal? value = null;

                if (price.HasValue)
                {
                    value = Convert.ToDecimal(adjustment.Percent) * price.Value;

                    value = Math.Round(value.Value, MidpointRounding.ToEven);
                }

                matrix[priceType, condition, Valuation.Condition].Value = value;
            }
        }

        private static void CalculateFinal(Matrix matrix)
        {
            foreach (PriceType priceType in Enum.GetValues(typeof(PriceType)))
            {
                if (priceType == PriceType.Undefined)
                {
                    continue;
                }

                foreach (Condition condition in Enum.GetValues(typeof(Condition)))
                {
                    if (condition == Condition.Undefined)
                    {
                        continue;
                    }

                    decimal? basePrice = matrix[priceType, condition, Valuation.Base].Value;

                    if (!basePrice.HasValue)
                    {
                        continue;
                    }

                    decimal? optionAdjustment = matrix[priceType, condition, Valuation.Option].Value;

                    decimal? mileageAdjustment = matrix[priceType, condition, Valuation.Mileage].Value;

                    decimal? colorAdjustment = matrix[priceType, condition, Valuation.Color].Value;

                    decimal? conditionAdjustment = matrix[priceType, condition, Valuation.Condition].Value;

                    decimal? regionAdjustment = matrix[priceType, condition, Valuation.Region].Value;

                    decimal? finalPrice = Sum(
                        Sum(
                            Sum(
                                Sum(
                                    Sum(basePrice, optionAdjustment),
                                    mileageAdjustment),
                                colorAdjustment),
                            regionAdjustment),
                        conditionAdjustment);

                    if (finalPrice < 0)
                    {
                        finalPrice = null;
                    }

                    matrix[priceType, condition, Valuation.Final].Value = finalPrice;
                }
            }
        }

        static decimal? Sum(decimal? a, decimal? b)
        {
            if (a.HasValue)
            {
                if (b.HasValue)
                {
                    return a.Value + b.Value;
                }

                return a.Value;
            }

            if (b.HasValue)
            {
                return b.Value;
            }

            return null;
        }

        static T Resolve<T>()
        {
            return RegistryFactory.GetResolver().Resolve<T>();
        }
    }
}
