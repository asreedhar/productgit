﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Lookup;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Api
{
    public class EdmundsTraversal : IEdmundsTraversal
    {
        protected const string PairSeparator = "||", ValueSeparator = "=";

        public ITree CreateTree()
        {
            return new Tree
            {
                Root = new InitialNode()
            };
        }

        public ITree CreateTree(string vin)
        {
            return new Tree
            {
                Root = new VinNode(vin)
            };
        }

        protected static T Resolve<T>()
        {
            return RegistryFactory.GetResolver().Resolve<T>();
        }

        sealed class Tree : ITree
        {
            private ITreeNode _root;

            public ITreeNode Root
            {
                get { return _root; }
                set { _root = value; }
            }

            public ITreeNode GetNodeByPath(ITreePath path)
            {
                if (path != null)
                {
                    if (!string.IsNullOrEmpty(path.State))
                    {
                        int yearValue = 0;

                        string makeName = null,
                               modelName = null,
                               styleName = null;

                        int styleId = 0;

                        string vin = null;

                        string[] values = path.State.Split(new[] { PairSeparator }, StringSplitOptions.None);

                        foreach (var value in values)
                        {
                            string[] pair = value.Split(new[] { ValueSeparator }, StringSplitOptions.None);

                            string k = pair[0], v = pair[1];

                            switch (k)
                            {
                                case "year":
                                    yearValue = int.Parse(v);
                                    break;
                                case "make":
                                    makeName = v;
                                    break;
                                case "model":
                                    modelName = v;
                                    break;
                                case "style":
                                    styleName = v;
                                    break;
                                case "vin":
                                    vin = v;
                                    break;
                                case "id":
                                    styleId = int.Parse(v);
                                    break;
                            }
                        }

                        if (yearValue == 0)
                        {
                            if (string.IsNullOrEmpty(vin))
                            {
                                return new InitialNode();
                            }

                            return new VinNode(vin);
                        }

                        YearNode yearNode = new YearNode(yearValue);

                        if (string.IsNullOrEmpty(makeName))
                        {
                            return yearNode;
                        }

                        MakeNode makeNode = new MakeNode(yearNode, makeName);

                        if (string.IsNullOrEmpty(modelName))
                        {
                            return makeNode;
                        }

                        ModelNode modelNode = new ModelNode(makeNode, modelName);

                        if (string.IsNullOrEmpty(styleName))
                        {
                            return modelNode;
                        }

                        return new StyleNode(modelNode, styleName, styleId);
                    }
                }

                return Root;
            }
        }

        sealed class InitialNode : TreeNode
        {
            private IList<ITreeNode> _children;

            public override string Label
            {
                get { return ""; }
            }

            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        _children = new List<ITreeNode>();

                        foreach (Year year in Resolve<ILookupYear>().Lookup(CurrentDataLoad()))
                        {
                            _children.Add(new YearNode(year.Value));
                        }
                    }

                    return _children;
                }
            }

            public override void Save(ITreePath path)
            {
                path.State = string.Empty;
            }
        }

        static string Serialize(ITreeNode node)
        {
            return node.Label.ToLower() + ValueSeparator + node.Value;
        }

        sealed class YearNode : TreeNode
        {
            private readonly int _value;

            public YearNode(int value)
                : base(value.ToString())
            {
                _value = value;
            }

            public YearNode(ITreeNode parent, int value)
                : base(parent, value.ToString())
            {
                _value = value;
            }

            public override string Label
            {
                get { return "Year"; }
            }

            private IList<ITreeNode> _children;

            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        _children = new List<ITreeNode>();

                        foreach (Make make in Resolve<ILookupMake>().Lookup(CurrentDataLoad(), _value))
                        {
                            _children.Add(new MakeNode(this, make.Value));
                        }
                    }

                    return _children;
                }
            }

            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = Serialize(this);
                }
            }
        }

        sealed class MakeNode : TreeNode
        {
            public MakeNode(ITreeNode year, string name)
                : base(year, name)
            {
            }

            public override string Label
            {
                get { return "Make"; }
            }

            private IList<ITreeNode> _children;

            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        int year = int.Parse(Parent.Value);

                        _children = new List<ITreeNode>();

                        foreach (Model model in Resolve<ILookupModel>().Lookup(CurrentDataLoad(), year, Value))
                        {
                            _children.Add(new ModelNode(this, model.Value));
                        }
                    }

                    return _children;
                }
            }

            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = Serialize(Parent) + PairSeparator + Serialize(this);
                }
            }
        }

        sealed class ModelNode : TreeNode
        {
            public ModelNode(ITreeNode manufacturer, string name)
                : base(manufacturer, name)
            {
            }

            public override string Label
            {
                get { return "Model"; }
            }

            private IList<ITreeNode> _children;

            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        _children = new List<ITreeNode>();

                        int year = int.Parse(Parent.Parent.Value);

                        foreach (Style style in Resolve<ILookupStyle>().Lookup(CurrentDataLoad(), year, Parent.Value, Value))
                        {
                            _children.Add(new StyleNode(this, style.Value, style.Id));
                        }
                    }

                    return _children;
                }
            }

            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = Serialize(Parent.Parent)
                        + PairSeparator + Serialize(Parent)
                        + PairSeparator + Serialize(this);
                }
            }
        }

        sealed class StyleNode : TreeNode
        {
            public StyleNode(ITreeNode parent, string value, int? vehicleId)
                : base(parent, value, vehicleId)
            {
            }

            public override string Label
            {
                get { return "Style"; }
            }

            private readonly IList<ITreeNode> _children = new List<ITreeNode>(0);

            public override IList<ITreeNode> Children
            {
                get
                {
                    return _children;
                }
            }

            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = Serialize(Parent.Parent.Parent)    // year
                                 + PairSeparator + Serialize(Parent.Parent)   // make
                                 + PairSeparator + Serialize(Parent)          // model
                                 + PairSeparator + Serialize(this);           // style
                }
            }
        }

        sealed class VinNode : TreeNode
        {
            private readonly string _vin;

            public VinNode(string vin)
            {
                _vin = vin;
            }

            public override string Label
            {
                get { return "Vin"; }
            }

            private IList<ITreeNode> _children;

            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        _children = new List<ITreeNode>();

                        IList<Vehicle> vehicles = Resolve<ILookupVin>().Lookup(CurrentDataLoad(), _vin);

                        switch (Vary.By(vehicles))
                        {
                            case VaryBy.Make:
                                Make(vehicles);
                                break;
                            case VaryBy.Model:
                                Model(vehicles);
                                break;
                            case VaryBy.Style:
                                Style(vehicles);
                                break;
                            default:
                                Style(vehicles);
                                break;
                        }
                    }

                    return _children;
                }
            }

            private void Make(IList<Vehicle> infos)
            {
                for (int i = 0, l = infos.Count; i < l; i++)
                {
                    Vehicle info = infos[i];

                    string make = info.Make.Value,
                           model = info.Model.Value,
                           style = info.Style.Value;

                    int year = info.Year.Value, id = info.Style.Id;

                    if (i == 0)
                    {
                        ITreeNode initialNode = new InitialNode(),
                                  yearNode = new YearNode(initialNode, year);

                        _parent = yearNode;
                    }

                    ITreeNode manufacturerNode = new MakeNode(_parent, make),
                              modelNode = new ModelNode(manufacturerNode, model),
                              styleNode = new StyleNode(modelNode, style, id);

                    _children.Add(styleNode);
                }
            }

            private void Model(IList<Vehicle> infos)
            {
                for (int i = 0, l = infos.Count; i < l; i++)
                {
                    Vehicle info = infos[i];

                    string make = info.Make.Value,
                           model = info.Model.Value,
                           style = info.Style.Value;

                    int year = info.Year.Value, id = info.Style.Id;

                    if (i == 0)
                    {
                        ITreeNode initialNode = new InitialNode(),
                                  yearNode = new YearNode(initialNode, year),
                                  manufacturerNode = new MakeNode(yearNode, make);

                        _parent = manufacturerNode;
                    }

                    ITreeNode modelNode = new ModelNode(_parent, model),
                              styleNode = new StyleNode(modelNode, style, id);

                    _children.Add(styleNode);
                }
            }

            private void Style(IList<Vehicle> infos)
            {
                for (int i = 0, l = infos.Count; i < l; i++)
                {
                    Vehicle info = infos[i];

                    string make = info.Make.Value,
                           model = info.Model.Value,
                           style = info.Style.Value;

                    int year = info.Year.Value, id = info.Style.Id;

                    if (i == 0)
                    {
                        ITreeNode initialNode = new InitialNode(),
                                  yearNode = new YearNode(initialNode, year),
                                  makeNode = new MakeNode(yearNode, make),
                                  modelNode = new ModelNode(makeNode, model);

                        _parent = modelNode;
                    }

                    ITreeNode styleNode = new StyleNode(_parent, style, id);

                    _children.Add(styleNode);
                }
            }

            public override int? StyleId
            {
                get
                {
                    if (Children.Count == 1)
                    {
                        return Children[0].StyleId;
                    }

                    return base.StyleId;
                }
            }

            private ITreeNode _parent;

            public override ITreeNode Parent
            {
                get
                {
                    IList<ITreeNode> eek = Children; // why oh why

                    return _parent;
                }
            }

            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = Serialize(this);
                }
            }
        }
    }

    public abstract class TreeNode : ITreeNode
    {
        private readonly ITreeNode _parent;
        private string _value;
        private int? _styleId;

        protected TreeNode()
        {
        }

        protected TreeNode(string value)
        {
            _value = value;
        }

        protected TreeNode(ITreeNode parent, string value)
        {
            _parent = parent;
            _value = value;
        }

        protected TreeNode(ITreeNode parent, string value, int? vehicleId)
        {
            _parent = parent;
            _value = value;
            _styleId = vehicleId;
        }

        public abstract string Label { get; }

        public virtual string Value
        {
            get { return _value; }
            internal set { _value = value; }
        }

        public virtual int? StyleId
        {
            get { return _styleId; }
            internal set { _styleId = value; }
        }

        public virtual ITreeNode Parent
        {
            get { return _parent; }
        }

        public abstract IList<ITreeNode> Children { get; }

        public virtual bool HasChildren
        {
            get { return Children.Count == 0; }
        }

        public abstract void Save(ITreePath path);

        protected int CurrentDataLoad()
        {
            IResolver resolver = RegistryFactory.GetResolver();

            ILookupDataLoad lookup = resolver.Resolve<ILookupDataLoad>();

            return lookup.Lookup().Max(x => x.Id);
        }
    }

    public class TreePath : ITreePath
    {
        private string _state;

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }
    }
}
