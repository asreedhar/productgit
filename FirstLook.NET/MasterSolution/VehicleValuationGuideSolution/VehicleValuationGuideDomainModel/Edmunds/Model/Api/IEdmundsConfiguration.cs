﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Api
{
    public interface IEdmundsConfiguration
    {
        IVehicleConfiguration InitialConfiguration(
            int styleId,
            string vin,
            State state,
            int? mileage,
            int? genericColorId,
            int? manufacturerColorId);

        IVehicleConfiguration UpdateConfiguration(
            IEdmundsService service,
            IVehicleConfiguration configuration,
            IOptionAction action);
    }

    public interface IVehicleConfigurationBuilderFactory
    {
        IVehicleConfigurationBuilder NewBuilder();

        IVehicleConfigurationBuilder NewBuilder(IVehicleConfiguration configuration);
    }

    public interface IVehicleConfigurationBuilder
    {
        DataLoad DataLoad { get; set; }

        State State { get; set; }

        int StyleId { get; set; }

        string Vin { get; set; }

        int? Mileage { get; set; }

        int? GenericColorId { get; set;}

        int? ManufacturerColorId { get; set; }

        IOptionState Add(Option option);

        IOptionState Add(long optionId, bool selected);

        void Select(long optionId);

        void Deselect(long optionId);

        IEnumerable<long> Selected();

        bool IsSelected(long optionId, bool returnValue);

        IOptionAction Do(long optionId, OptionActionType actionType);

        IOptionAction Undo(long optionId);

        IVehicleConfiguration ToVehicleConfiguration();
    }

    public interface IVehicleConfiguration
    {
        DataLoad DataLoad { get; }

        State State { get; }

        int StyleId { get; }

        string Vin { get; }

        int? Mileage { get; }

        int? GenericColorId { get; }

        int? ManufacturerColorId { get; }

        IList<IOptionAction> OptionActions { get; }

        IList<IOptionState> OptionStates { get; }

        ChangeType Compare(IVehicleConfiguration configuration);
    }

    public interface IOptionState
    {
        long OptionId { get; }

        bool Selected { get; }
    }

    [Serializable]
    public enum OptionActionType
    {
        Undefined,
        Add,
        Remove
    }

    public interface IOptionAction
    {
        long OptionId { get; }

        OptionActionType ActionType { get; }
    }
}
