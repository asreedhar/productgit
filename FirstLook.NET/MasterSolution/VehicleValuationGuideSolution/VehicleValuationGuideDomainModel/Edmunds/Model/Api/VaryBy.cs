﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Api
{
    [Serializable]
    internal enum VaryBy
    {
        None,
        Make,
        Model,
        Style
    }
}