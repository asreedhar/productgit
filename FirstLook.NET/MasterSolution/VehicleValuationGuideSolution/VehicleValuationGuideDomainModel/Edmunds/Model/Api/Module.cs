﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Api
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IEdmundsCalculator, EdmundsCalculator>(ImplementationScope.Shared);

            registry.Register<IEdmundsConfiguration, EdmundsConfiguration>(ImplementationScope.Shared);

            registry.Register<IEdmundsService, EdmundsService>(ImplementationScope.Shared);

            registry.Register<IEdmundsTraversal, EdmundsTraversal>(ImplementationScope.Shared);
        }
    }
}
