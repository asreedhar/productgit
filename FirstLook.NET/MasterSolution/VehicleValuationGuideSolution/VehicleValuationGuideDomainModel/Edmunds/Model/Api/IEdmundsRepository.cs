﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Vehicles.Model;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Api
{
    public interface IEdmundsRepository
    {
        IList<IEdition<IPublicationInfo>> Load(
            IBroker broker,
            ClientVehicleIdentification vehicle);

        IEdition<IPublication> Load(
            IBroker broker,
            ClientVehicleIdentification vehicle,
            DateTime on);

        IEdition<IPublication> Load(
            IBroker broker,
            ClientVehicleIdentification vehicle,
            int id);

        IEdition<IPublication> Save(
            IBroker broker,
            IPrincipal principal,
            ClientVehicleIdentification vehicle,
            IVehicleConfiguration configuration,
            Matrix matrix);
    }

    public interface IDateRange
    {
        bool Covers(DateTime date);

        DateTime BeginDate { get; }

        DateTime EndDate { get; }
    }

    public interface IEdition<T>
    {
        IDateRange DateRange { get; }

        IUser User { get; }

        T Data { get; }
    }

    [Serializable]
    public enum ChangeAgent
    {
        Undefined,
        User,
        System
    }

    [Serializable]
    [Flags]
    public enum ChangeType
    {
        None = 0,
        DataLoad = 1 << 0,
        State = 1 << 1,
        StyleId = 1 << 2,
        Mileage = 1 << 3,
        OptionActions = 1 << 4,
        OptionStates = 1 << 5
    }

    public interface IPublicationInfo
    {
        ChangeAgent ChangeAgent { get; }

        ChangeType ChangeType { get; }

        int Id { get; }
    }

    public interface IPublication : IPublicationInfo
    {
        IVehicleConfiguration VehicleConfiguration { get; }

        Matrix Matrix { get; }

        IEdmundsService Service { get; }
    }
}
