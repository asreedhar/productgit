﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Lookup;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Api
{
    public delegate int EdmundsServiceDataLoad();

    public class EdmundsService : IEdmundsService
    {
        private readonly IResolver _resolver;

        private readonly EdmundsServiceDataLoad _dataLoad;

        public EdmundsService(IResolver resolver, EdmundsServiceDataLoad dataLoad)
        {
            _resolver = resolver;

            _dataLoad = dataLoad;
        }

        public EdmundsService()
        {
            _resolver = RegistryFactory.GetResolver();

            _dataLoad = () => _resolver.Resolve<ILookupDataLoad>().Lookup().Max(x => x.Id);
        }

        public DataLoad DataLoad()
        {
            ILookupDataLoad lookup = Resolve<ILookupDataLoad>();

            IList<DataLoad> dates = lookup.Lookup();

            return dates.First(x => x.Id == dates.Max(y => y.Id));
        }

        public Region Region(string stateCode)
        {
            ILookupRegion lookup = Resolve<ILookupRegion>();

            return lookup.Lookup(_dataLoad(), stateCode);
        }

        public Vehicle Vehicle(int styleId)
        {
            ILookupVehicle lookup = Resolve<ILookupVehicle>();

            return lookup.Lookup(_dataLoad(), styleId);
        }

        public IList<Option> Options(int styleId)
        {
            ILookupOption lookup = Resolve<ILookupOption>();

            return lookup.Lookup(_dataLoad(), styleId);
        }

        public IList<ManufacturerColor> Colors(int styleId)
        {
            ILookupColor lookup = Resolve<ILookupColor>();

            return lookup.Lookup(_dataLoad(), styleId);
        }

        public IList<Adjustment> ColorAdjustments(int styleId, int regionId, int colorId)
        {
            ILookupColorAdjustment lookup = Resolve<ILookupColorAdjustment>();

            return lookup.Lookup(_dataLoad(), styleId, regionId, colorId);
        }

        public IList<Adjustment> ConditionAdjustments(int styleId)
        {
            ILookupConditionAdjustment lookup = Resolve<ILookupConditionAdjustment>();

            return lookup.Lookup(_dataLoad(), styleId);
        }

        public MileageAdjustment MileageAdjustment(int styleId)
        {
            ILookupMileageAdjustment lookup = Resolve<ILookupMileageAdjustment>();

            return lookup.Lookup(_dataLoad(), styleId);
        }

        public IList<Adjustment> RegionAdjustments(int styleId, int regionId)
        {
            ILookupRegionAdjustment lookup = Resolve<ILookupRegionAdjustment>();

            return lookup.Lookup(_dataLoad(), styleId, regionId);
        }
        
        static T Resolve<T>()
        {
            return RegistryFactory.GetResolver().Resolve<T>();
        }
    }
}
