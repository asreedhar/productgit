﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model
{
    [Serializable]
    public enum PriceType
    {
        Undefined,
        Retail,
        PrivateParty,
        TradeIn
    }
}
