﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model
{
    [Serializable]
    public class CertifiedMileageAdjustment
    {
        private int _low;
        private int _high;
        private double _percent;

        public int Low
        {
            get { return _low; }
            internal set { _low = value; }
        }

        public int High
        {
            get { return _high; }
            internal set { _high = value; }
        }

        public double Percent
        {
            get { return _percent; }
            internal set { _percent = value; }
        }

        public bool Covers(int mileage)
        {
            return (Low <= mileage && mileage <= High);
        }
    }
}
