using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Sql
{
    public abstract class Serializer<T> : ISqlSerializer<T>
    {
        public IList<T> Deserialize(IDataReader reader)
        {
            List<T> values = new List<T>();

            while (reader.Read())
            {
                values.Add(Deserialize((IDataRecord)reader));
            }

            return values;
        }

        public abstract T Deserialize(IDataRecord record);

        protected static TContract Resolve<TContract>()
        {
            return RegistryFactory.GetResolver().Resolve<TContract>();
        }
    }

    public class DataLoadSerializer : Serializer<DataLoad>
    {
        public override DataLoad Deserialize(IDataRecord record)
        {
            DateTime time = record.GetDateTime(record.GetOrdinal("DataLoadTime"));

            int id = record.GetInt32(record.GetOrdinal("DataLoadID"));

            return new DataLoad { Id = id, Value = string.Format("{0:dddd, MMMM d, yyyy}", time) };
        }
    }

    public class StateSerializer : Serializer<State>
    {
        public override State Deserialize(IDataRecord record)
        {
            string state = DataRecord.GetString(record, "State");

            string code = DataRecord.GetString(record, "Code");

            return new State {Name = state, Code = code};
        }
    }

    public class YearSerializer : Serializer<Year>
    {
        public override Year Deserialize(IDataRecord record)
        {
            int id = record.GetInt32((record.GetOrdinal("Year")));

            return new Year{ Value = id };
        }
    }
    
    public class MakeSerializer : Serializer<Make>
    {
        public override Make Deserialize(IDataRecord record)
        {
            string name = record.GetString(record.GetOrdinal("Make"));

            return new Make { Value = name };
        }
    }

    public class ModelSerializer : Serializer<Model>
    {
        public override Model Deserialize(IDataRecord record)
        {
            string name = record.GetString(record.GetOrdinal("Model"));

            return new Model { Value = name };
        }
    }

    public class StyleSerializer : Serializer<Style>
    {
        public override Style Deserialize(IDataRecord record)
        {
            string name = record.GetString(record.GetOrdinal("STYLE_USED"));

            int id = record.GetInt32(record.GetOrdinal("ED_STYLE_ID"));

            return new Style {Value = name, Id = id};
        }
    }

    public class VehicleSerializer : Serializer<Vehicle>
    {
        public override Vehicle Deserialize(IDataRecord record)
        {
            Year year = Resolve<ISqlSerializer<Year>>().Deserialize(record);

            Make manufacturer = Resolve<ISqlSerializer<Make>>().Deserialize(record);

            Model model = Resolve<ISqlSerializer<Model>>().Deserialize(record);

            Style style = Resolve<ISqlSerializer<Style>>().Deserialize(record);

            int retail = record.GetInt32(record.GetOrdinal("RETAIL_VALUE"));

            int privateParty = record.GetInt32(record.GetOrdinal("PPARTY_VALUE"));

            int tradeIn = record.GetInt32(record.GetOrdinal("TRADEIN_VALUE"));

            return new Vehicle
            {
                Year = year,
                Make = manufacturer,
                Model = model,
                Style = style,
                BasePrices = new List<Price>
                {
                    new Price{Amount = retail, PriceType = PriceType.Retail},
                    new Price{Amount = privateParty, PriceType = PriceType.PrivateParty},
                    new Price{Amount = tradeIn, PriceType = PriceType.TradeIn}
                }
            };
        }
    }

    public class AdjustmentSerializer : Serializer<Adjustment>
    {
        public override Adjustment Deserialize(IDataRecord record)
        {
            AdjustmentType adjustmentType = (AdjustmentType) record.GetInt32(record.GetOrdinal("AdjustmentType"));

            Condition condition = (Condition)record.GetInt32(record.GetOrdinal("TMV_CONDITION_ID"));

            PriceType priceType = (PriceType) record.GetInt32(record.GetOrdinal("PriceType"));

            float percent = record.GetFloat(record.GetOrdinal("Percent"));

            return new Adjustment
            {
                AdjustmentType = adjustmentType,
                Condition = condition,
                PriceType = priceType,
                Percent = percent
            };
        }
    }

    public class MileageAdjustmentSerializer : Serializer<MileageAdjustment>
    {
        public override MileageAdjustment Deserialize(IDataRecord record)
        {
            int low0, low1, low2, low3, low4;

            low0 = record.GetInt32(record.GetOrdinal("LOW_MILEAGE"));
            low1 = record.GetInt32(record.GetOrdinal("LOW_BOUND_1"));
            low2 = record.GetInt32(record.GetOrdinal("LOW_BOUND_2"));
            low3 = record.GetInt32(record.GetOrdinal("LOW_BOUND_3"));
            low4 = record.GetInt32(record.GetOrdinal("LOW_BOUND_4"));

            int high0, high1, high2, high3, high4;

            high0 = record.GetInt32(record.GetOrdinal("HIGH_MILEAGE"));
            high1 = record.GetInt32(record.GetOrdinal("HIGH_BOUND_1"));
            high2 = record.GetInt32(record.GetOrdinal("HIGH_BOUND_2"));
            high3 = record.GetInt32(record.GetOrdinal("HIGH_BOUND_3"));
            high4 = record.GetInt32(record.GetOrdinal("HIGH_BOUND_4"));

            float add1, add2, add3, add4;

            add1 = record.GetFloat(record.GetOrdinal("ADD_FACTOR_1"));
            add2 = record.GetFloat(record.GetOrdinal("ADD_FACTOR_2"));
            add3 = record.GetFloat(record.GetOrdinal("ADD_FACTOR_3"));
            add4 = record.GetFloat(record.GetOrdinal("ADD_FACTOR_4"));

            float ded1, ded2, ded3, ded4;

            ded1 = record.GetFloat(record.GetOrdinal("DEDUCT_FACTOR_1"));
            ded2 = record.GetFloat(record.GetOrdinal("DEDUCT_FACTOR_2"));
            ded3 = record.GetFloat(record.GetOrdinal("DEDUCT_FACTOR_3"));
            ded4 = record.GetFloat(record.GetOrdinal("DEDUCT_FACTOR_4"));

            return new MileageAdjustment
            {
                Low0 = low0,
                Low1 = low1,
                Low2 = low2,
                Low3 = low3,
                Low4 = low4,

                High0 = high0,
                High1 = high1,
                High2 = high2,
                High3 = high3,
                High4 = high4,

                Add1 = add1,
                Add2 = add2,
                Add3 = add3,
                Add4 = add4,

                Deduct1 = ded1,
                Deduct2 = ded2,
                Deduct3 = ded3,
                Deduct4 = ded4,
            };
        }
    }

    public class OptionSerializer : Serializer<Option>
    {
        public override Option Deserialize(IDataRecord record)
        {
            long id = record.GetInt64((record.GetOrdinal("FEATURE_ID")));

            string name = record.GetString(record.GetOrdinal("FEATURE_DESCRIPTION"));

            int retail = record.GetInt32((record.GetOrdinal("RETAIL_VALUE_FEATURES")));

            int pparty = record.GetInt32((record.GetOrdinal("PPARTY_VALUE_FEATURES")));

            int tradein = record.GetInt32((record.GetOrdinal("TRADEIN_VALUE_FEATURES")));

            return new Option
            {
                Id = id,
                Name = name,
                Prices = new List<Price>
                               {
                                   new Price{Amount = retail, PriceType = PriceType.Retail},
                                   new Price{Amount = pparty, PriceType = PriceType.PrivateParty},
                                   new Price{Amount = tradein, PriceType = PriceType.TradeIn}
                               }
            };
        }
    }

    public class ManufacturerColorSerializer : Serializer<ManufacturerColor>
    {
        public override ManufacturerColor Deserialize(IDataRecord record)
        {
            int genericColorId = record.GetInt32((record.GetOrdinal("TMV_GENERIC_COLOR_ID")));

            string genericColorName = record.GetString(record.GetOrdinal("TMV_GENERIC_COLOR"));

            int colorId = record.GetInt32((record.GetOrdinal("color_id")));

            string colorName = record.GetString(record.GetOrdinal("mfgr_color_name"));

            string colorCode = record.GetString(record.GetOrdinal("mfgr_color_code"));

            string colorType = record.GetString(record.GetOrdinal("interior_exterior_flag"));

            Color color = Color.Empty;

            if (!record.IsDBNull(record.GetOrdinal("r_code")))
            {
                int r = record.GetInt32(record.GetOrdinal("r_code"));

                int g = record.GetInt32(record.GetOrdinal("g_code"));

                int b = record.GetInt32(record.GetOrdinal("b_code"));

                color = Color.FromArgb(r, g, b);
            }
            
            return new ManufacturerColor
            {
                GenericColor = new GenericColor { Id = genericColorId, Name = genericColorName },
                Id = colorId,
                Name = colorName,
                Code = colorCode,
                Color = color,
                ColorType = "I".Equals(colorType) ? ColorType.Interior : ("E".Equals(colorType) ? ColorType.Exterior : ColorType.Undefined)
            };
        }
    }

    public class RegionSerializer : Serializer<Region>
    {
        public override Region Deserialize(IDataRecord record)
        {
            int id = record.GetInt32((record.GetOrdinal("TMV_REGION_ID")));

            string name = record.GetString(record.GetOrdinal("TMV_REGION"));

            return new Region
            {
                Id = id,
                Name = name
            };
        }
    }

    public class CertifiedCostSerializer : Serializer<CertifiedCost>
    {
        public override CertifiedCost Deserialize(IDataRecord record)
        {
            int cost = record.GetInt32(record.GetOrdinal("CPO_COST"));

            double premiumPercent = record.GetDouble(record.GetOrdinal("CPO_PREMIUM_PERCENT"));

            return new CertifiedCost{Cost = cost, PremiumPercent = premiumPercent};
        }
    }

    public class CertifiedCriteriaSerializer : Serializer<CertifiedCriteria>
    {
        public override CertifiedCriteria Deserialize(IDataRecord record)
        {
            int miles = record.GetInt32(record.GetOrdinal("MAX_YEARS"));

            int years = record.GetInt32(record.GetOrdinal("MAX_MILEAGE"));

            return new CertifiedCriteria{MaxMileage = miles, MaxYears = years};
        }
    }

    public class CertifiedMileageAdjustmentSerializer : Serializer<CertifiedMileageAdjustment>
    {
        public override CertifiedMileageAdjustment Deserialize(IDataRecord record)
        {
            int high = record.GetInt32(record.GetOrdinal("INTERVAL_MAX"));

            int low = record.GetInt32(record.GetOrdinal("INTERVAL_MIN"));

            double percent = record.GetDouble(record.GetOrdinal("CPO_MILEAGE_PERCENT"));

            return new CertifiedMileageAdjustment{Low = low, High = high, Percent = percent};
        }
    }
}
