﻿using System.Data;
using FirstLook.Common.Core.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Sql
{
    public class CurrentSqlService : SqlServiceBase
    {
        public CurrentSqlService()
        {
        }

        public CurrentSqlService(ICloneReader clone)
            : base(clone)
        {
        }

        protected override string DatabaseName
        {
            get { return "Edmunds"; }
        }

        protected override string SchemaName
        {
            get { return "Edmunds"; }
        }

        protected override string And
        {
            get { return "--"; }
        }
    }

    public class SnapshotSqlService : SqlServiceBase
    {
        public SnapshotSqlService()
        {
        }

        public SnapshotSqlService(ICloneReader clone)
            : base(clone)
        {
        }

        protected override string DatabaseName
        {
            get { return "Vehicle"; }
        }

        protected override string SchemaName
        {
            get { return "Edmunds"; }
        }

        protected override string And
        {
            get { return "AND"; }
        }
    }

    public abstract class SqlServiceBase : ISqlService
    {
        protected const string Prefix = "FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Sql.Resources.";

        protected abstract string DatabaseName { get; }

        protected abstract string SchemaName { get; }

        protected abstract string And { get; }

        private readonly ICloneReader _clone;

        protected SqlServiceBase()
        {
            _clone = new CloneReader();
        }

        protected SqlServiceBase(ICloneReader clone)
        {
            _clone = clone;
        }

        protected string CommandText(string resourceName)
        {
            return string.Format(
                Database.GetCommandText(
                    GetType().Assembly,
                    Prefix + resourceName),
                SchemaName,
                And);
        }

        public IDataReader DataLoad()
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = CommandText("SqlService_DataLoad.txt");

                    return _clone.Snapshot(command, "DataLoad");
                }
            }
        }

        public IDataReader Region(int dataLoad, string stateCode)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = CommandText("SqlService_Region.txt");

                    Database.AddWithValue(command, "State", stateCode, DbType.String);

                    return _clone.Snapshot(command, "Region");
                }
            }
        }

        public IDataReader Query(int dataLoad)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = CommandText("SqlService_Years.txt");

                    return _clone.Snapshot(command, "Year");
                }
            }
        }

        public IDataReader Query(int dataLoad, int year)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = CommandText("SqlService_Makes.txt");

                    Database.AddWithValue(command, "Year", year, DbType.Int32);

                    return _clone.Snapshot(command, "Make");
                }
            }
        }

        public IDataReader Query(int dataLoad, int year, string make)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = CommandText("SqlService_Models.txt");

                    Database.AddWithValue(command, "Year", year, DbType.Int32);

                    Database.AddWithValue(command, "Make", make, DbType.String);

                    return _clone.Snapshot(command, "Model");
                }
            }
        }

        public IDataReader Query(int dataLoad, int year, string make, string model)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = CommandText("SqlService_Styles.txt");

                    Database.AddWithValue(command, "Year", year, DbType.Int32);

                    Database.AddWithValue(command, "Make", make, DbType.String);

                    Database.AddWithValue(command, "Model", model, DbType.String);

                    return _clone.Snapshot(command, "Style");
                }
            }
        }

        public IDataReader Query(int dataLoad, string vin)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = CommandText("SqlService_Style__Vin.txt");

                    Database.AddWithValue(command, "VIN", vin, DbType.String);

                    return _clone.Snapshot(command, "VIN");
                }
            }
        }

        public IDataReader Vehicle(int dataLoad, int styleId)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = CommandText("SqlService_Style__Id.txt");

                    Database.AddWithValue(command, "StyleId", styleId, DbType.Int32);

                    return _clone.Snapshot(command, "Vehicle");
                }
            }
        }

        public IDataReader Options(int dataLoad, int styleId)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = CommandText("SqlService_Options.txt");

                    Database.AddWithValue(command, "StyleId", styleId, DbType.Int32);

                    return _clone.Snapshot(command, "Options");
                }
            }
        }

        public IDataReader Colors(int dataLoad, int styleId)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = CommandText("SqlService_Colors.txt");

                    Database.AddWithValue(command, "StyleId", styleId, DbType.Int32);

                    return _clone.Snapshot(command, "Colors");
                }
            }
        }

        public IDataReader ColorAdjustments(int dataLoad, int styleId, int regionId, int colorId)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = CommandText("SqlService_ColorAdjustments.txt");

                    Database.AddWithValue(command, "StyleId", styleId, DbType.Int32);

                    Database.AddWithValue(command, "RegionId", regionId, DbType.Int32);

                    Database.AddWithValue(command, "ColorId", colorId, DbType.Int32);

                    return _clone.Snapshot(command, "ColorAdjustments");
                }
            }
        }

        public IDataReader RegionAdjustments(int dataLoad, int styleId, int regionId)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = CommandText("SqlService_RegionAdjustments.txt");

                    Database.AddWithValue(command, "StyleId", styleId, DbType.Int32);

                    Database.AddWithValue(command, "RegionId", regionId, DbType.Int32);

                    return _clone.Snapshot(command, "RegionAdjustments");
                }
            }
        }

        public IDataReader ConditionAdjustments(int dataLoad, int styleId)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = CommandText("SqlService_ConditionAdjustments.txt");

                    Database.AddWithValue(command, "StyleId", styleId, DbType.Int32);

                    return _clone.Snapshot(command, "ConditionAdjustments");
                }
            }
        }

        public IDataReader MileageAdjustments(int dataLoad, int styleId)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = CommandText("SqlService_MileageAdjustments.txt");

                    Database.AddWithValue(command, "StyleId", styleId, DbType.Int32);

                    return _clone.Snapshot(command, "MileageAdjustments");
                }
            }
        }

        public IDataReader CertifiedCost(int dataLoad, int styleId)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = CommandText("SqlService_CertifiedCost.txt");

                    Database.AddWithValue(command, "StyleId", styleId, DbType.Int32);

                    return _clone.Snapshot(command, "CertifiedCost");
                }
            }
        }

        public IDataReader CertifiedCriteria(int dataLoad, int styleId)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = CommandText("SqlService_CertifiedCriteria.txt");

                    Database.AddWithValue(command, "StyleId", styleId, DbType.Int32);

                    return _clone.Snapshot(command, "CertifiedCriteria");
                }
            }
        }

        public IDataReader CertifiedMileageAdjustments(int dataLoad, int styleId)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;

                    command.CommandText = CommandText("SqlService_CertifiedMileageAdjustments.txt");

                    Database.AddWithValue(command, "StyleId", styleId, DbType.Int32);

                    return _clone.Snapshot(command, "CertifiedMileageAdjustments");
                }
            }
        }
    }

    public interface ICloneReader
    {
        IDataReader Snapshot(IDbCommand command, params string[] names);
    }

    internal class CloneReader : ICloneReader
    {
        public IDataReader Snapshot(IDbCommand command, params string[] names)
        {
            DataSet set = new DataSet();

            using (IDataReader reader = command.ExecuteReader())
            {
                foreach (string name in names)
                {
                    DataTable table = new DataTable(name);

                    table.Load(reader);

                    set.Tables.Add(table);
                }
            }

            return set.CreateDataReader();
        }
    }
}
