﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Sql
{
    public class Module : IModule
    {
        internal class SerializerModule : IModule
        {
            public void Configure(IRegistry registry)
            {
                registry.Register<ISqlSerializer<State>, StateSerializer>(ImplementationScope.Shared);
                registry.Register<ISqlSerializer<DataLoad>, DataLoadSerializer>(ImplementationScope.Shared);
                registry.Register<ISqlSerializer<Year>, YearSerializer>(ImplementationScope.Shared);
                registry.Register<ISqlSerializer<Make>, MakeSerializer>(ImplementationScope.Shared);
                registry.Register<ISqlSerializer<Model>, ModelSerializer>(ImplementationScope.Shared);
                registry.Register<ISqlSerializer<Style>, StyleSerializer>(ImplementationScope.Shared);
                registry.Register<ISqlSerializer<Vehicle>, VehicleSerializer>(ImplementationScope.Shared);
                registry.Register<ISqlSerializer<Adjustment>, AdjustmentSerializer>(ImplementationScope.Shared);
                registry.Register<ISqlSerializer<MileageAdjustment>, MileageAdjustmentSerializer>(ImplementationScope.Shared);
                registry.Register<ISqlSerializer<Option>, OptionSerializer>(ImplementationScope.Shared);
                registry.Register<ISqlSerializer<ManufacturerColor>, ManufacturerColorSerializer>(ImplementationScope.Shared);
                registry.Register<ISqlSerializer<Region>, RegionSerializer>(ImplementationScope.Shared);
                registry.Register<ISqlSerializer<CertifiedCost>, CertifiedCostSerializer>(ImplementationScope.Shared);
                registry.Register<ISqlSerializer<CertifiedCriteria>, CertifiedCriteriaSerializer>(ImplementationScope.Shared);
                registry.Register<ISqlSerializer<CertifiedMileageAdjustment>, CertifiedMileageAdjustmentSerializer>(ImplementationScope.Shared);
            }
        }

        public void Configure(IRegistry registry)
        {
            registry.Register<ISqlService, CurrentSqlService>(ImplementationScope.Shared);

            registry.Register<SerializerModule>();
        }
    }
}
