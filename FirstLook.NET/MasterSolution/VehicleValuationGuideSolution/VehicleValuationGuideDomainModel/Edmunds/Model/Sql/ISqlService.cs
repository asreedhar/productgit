﻿using System.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model.Sql
{
    public interface ISqlService
    {
        IDataReader DataLoad();

        IDataReader Region(int dataLoad, string stateCode);

        IDataReader Query(int dataLoad);

        IDataReader Query(int dataLoad, int year);

        IDataReader Query(int dataLoad, int year, string make);

        IDataReader Query(int dataLoad, int year, string make, string model);

        IDataReader Query(int dataLoad, string vin);

        IDataReader Vehicle(int dataLoad, int styleId);

        IDataReader Options(int dataLoad, int styleId);

        IDataReader Colors(int dataLoad, int styleId);

        IDataReader ColorAdjustments(int dataLoad, int styleId, int regionId, int colorId);

        IDataReader RegionAdjustments(int dataLoad, int styleId, int regionId);

        IDataReader ConditionAdjustments(int dataLoad, int styleId);

        IDataReader MileageAdjustments(int dataLoad, int styleId);

        IDataReader CertifiedCost(int dataLoad, int styleId);

        IDataReader CertifiedCriteria(int dataLoad, int styleId);

        IDataReader CertifiedMileageAdjustments(int dataLoad, int styleId);
    }
}
