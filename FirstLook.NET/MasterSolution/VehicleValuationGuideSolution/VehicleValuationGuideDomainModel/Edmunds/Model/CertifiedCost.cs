﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Edmunds.Model
{
    [Serializable]
    public class CertifiedCost
    {
        private int _cost;
        private double _premiumPercent;

        public int Cost
        {
            get { return _cost; }
            internal set { _cost = value; }
        }

        public double PremiumPercent
        {
            get { return _premiumPercent; }
            internal set { _premiumPercent = value; }
        }
    }
}
