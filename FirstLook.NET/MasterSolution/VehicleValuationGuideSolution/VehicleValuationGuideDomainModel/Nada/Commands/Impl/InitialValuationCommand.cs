using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.Impl
{
    [Serializable]
    public class InitialValuationCommand : ICommand<InitialValuationResultsDto, InitialValuationArgumentsDto>
    {
        public InitialValuationResultsDto Execute(InitialValuationArgumentsDto parameters)
        {
            IResolver registry = RegistryFactory.GetResolver();

            INadaService service = registry.Resolve<INadaService>();

            Vehicle vehicle = service.Information(parameters.Uid);
            
            State state = service.States().First(x => Equals(x.Code, parameters.State));

            Region region = service.ToRegion(state);

            IVehicleConfiguration configuration = registry.Resolve<INadaConfiguration>().InitialConfiguration(
                parameters.Uid,
                parameters.Vin,
                state,
                parameters.HasMileage ? parameters.Mileage : default(int?));

            IList<Accessory> accessories;

            if (string.IsNullOrEmpty(parameters.Vin))
            {
                accessories = service.Accessories(vehicle.Body.Uid, region);
            }
            else
            {
                accessories = service.Accessories(vehicle.Body.Uid, region, parameters.Vin);
            }

            Matrix matrix = registry.Resolve<INadaCalculator>().Calculate(service, configuration);

            PriceTableDto table = Mapper.Map(matrix);

            InitialValuationResultsDto results = new InitialValuationResultsDto
            {
                Arguments = parameters,
                Accessories = Mapper.Map(accessories, configuration.AccessoryStates),
                Table = table,
                VehicleConfiguration = Mapper.Map(configuration)
            };

            return results;
        }
    }  
}
