﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Traversal;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api;
using FirstLook.VehicleValuationGuide.DomainModel.Utility.Model;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.Impl
{
    [Serializable]
    public class TraversalCommand : ICommand<TraversalResultsDto, TraversalArgumentsDto>
    {
        public TraversalResultsDto Execute(TraversalArgumentsDto parameters)
        {
            INadaTraversal traversal = RegistryFactory.GetResolver().Resolve<INadaTraversal>();

            string vin = parameters.Vin;

            bool vinIsNullOrEmpty = string.IsNullOrEmpty(vin);

            ITree tree;

            if (vinIsNullOrEmpty)
            {
                tree = traversal.CreateTree();
            }
            else
            {
                tree = traversal.CreateTree(vin);
            }

            Mapper.Operation thisParents, thisChildren, nextParents, nextChildren;

            nextParents = nextChildren = Mapper.Operation.No; // sensible defaults

            TraversalStatusDto status = TraversalStatusDto.Success;

            ITreeNode root = tree.Root;

            if (vinIsNullOrEmpty)
            {
                thisParents = Mapper.Operation.No;

                thisChildren = Mapper.Operation.No;
            }
            else
            {
                if (root.Parent == null)
                {
                    thisParents = Mapper.Operation.No;

                    thisChildren = Mapper.Operation.No;

                    switch (Vin.Inspect(vin))
                    {
                        case Vin.Status.InvalidLength:
                            status = TraversalStatusDto.InvalidVinLength;
                            break;
                        case Vin.Status.InvalidCharacters:
                            status = TraversalStatusDto.InvalidVinCharacters;
                            break;
                        case Vin.Status.InvalidChecksum:
                            status = TraversalStatusDto.InvalidVinChecksum;
                            break;
                        case Vin.Status.Ok:
                            status = TraversalStatusDto.NoDataForVin;
                            break;
                    }
                }
                else
                {
                    thisParents = Mapper.Operation.Yes;

                    thisChildren = Mapper.Operation.NoThenYes;

                    string label = root.Parent.Label;

                    if (Equals("Trim", label) || Equals("Model", label))
                    {
                        nextParents = Mapper.Operation.No;
                    }
                    else if (Equals("Make", label))
                    {
                        nextParents = Mapper.Operation.YesThenNo;
                    }
                }
            }
            
            List<NodeDto> emptyList = new List<NodeDto>();

            TraversalResultsDto results = new TraversalResultsDto
            {
                Arguments = parameters,
                Path = new PathDto
                {
                    CurrentNode = root != null ? Mapper.Map(tree.Root, thisParents, thisChildren) : null,
                    Successors = root != null ? Mapper.Map(tree.Root.Children, nextParents, nextChildren) : emptyList
                },
                Status = status
            };

            return results;
        }
    }
}
