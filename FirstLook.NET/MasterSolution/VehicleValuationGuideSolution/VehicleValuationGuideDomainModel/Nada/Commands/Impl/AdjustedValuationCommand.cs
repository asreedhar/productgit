﻿using System;
using System.Linq;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.Impl
{
    [Serializable]
    public class AdjustedValuationCommand : ICommand<AdjustedValuationResultsDto, AdjustedValuationArgumentsDto>
    {
        public AdjustedValuationResultsDto Execute(AdjustedValuationArgumentsDto parameters)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException("parameters");
            }

            if (parameters.VehicleConfiguration == null)
            {
                throw new ArgumentNullException("parameters", "VehicleConfiguration");
            }

            // allow empty option action for changes in mileage/state

            IResolver resolver = RegistryFactory.GetResolver();

            INadaService service = resolver.Resolve<INadaService>();

            State state = service.States().First(x => Equals(x.Code, parameters.State));

            IVehicleConfiguration configuration = Mapper.Map(
                parameters.VehicleConfiguration,
                state,
                parameters.HasMileage ? parameters.Mileage : default(int?));

            AccessoryActionDto action = parameters.AccessoryAction;

            if (action != null)
            {
                configuration = resolver.Resolve<INadaConfiguration>().UpdateConfiguration(
                    configuration,
                    new Action
                    {
                        AccessoryCode = action.AccessoryCode,
                        ActionType = (AccessoryActionType)action.ActionType
                    });
            }

            Matrix matrix = resolver.Resolve<INadaCalculator>().Calculate(service, configuration);

            AdjustedValuationResultsDto results = new AdjustedValuationResultsDto
            {
                Arguments = parameters,
                Table = Mapper.Map(matrix),
                VehicleConfiguration = Mapper.Map(configuration)
            };

            return results;
        }

        class Action : IAccessoryAction
        {
            public string AccessoryCode { get; set; }

            public AccessoryActionType ActionType { get; set; }
        }
    } 
}
