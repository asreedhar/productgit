﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes.Publications;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.Impl.Publications
{
    public class PublicationOnDateCommand : ICommand<PublicationOnDateResultsDto, IdentityContextDto<PublicationOnDateArgumentsDto>>
    {
        public PublicationOnDateResultsDto Execute(IdentityContextDto<PublicationOnDateArgumentsDto> parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            IVehicleRepository vehicleRepository = resolver.Resolve<IVehicleRepository>();

            INadaRepository nadaRepository = resolver.Resolve<INadaRepository>();

            PublicationOnDateArgumentsDto arguments = parameters.Arguments;

            IdentityDto identity = parameters.Identity;

            IBroker broker = Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);

            ClientVehicleIdentification vehicle = vehicleRepository.Identification(broker, arguments.Vehicle);

            IEdition<IPublication> publication = nadaRepository.Load(broker, vehicle, arguments.On);

            return new PublicationOnDateResultsDto
            {
                Arguments = arguments,
                Publication = PublicationMapper.Map(publication)
            };
        }
    }
}
