﻿using System.Collections.Generic;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Publications;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.Impl.Publications
{
    public static class PublicationMapper
    {
        public static List<PublicationInfoDto> Map(IList<IEdition<IPublicationInfo>> publications)
        {
            List<PublicationInfoDto> values = new List<PublicationInfoDto>();

            foreach (IEdition<IPublicationInfo> edition in publications)
            {
                IPublicationInfo publication = edition.Data;

                values.Add(
                    new PublicationInfoDto
                    {
                        ChangeAgent = (ChangeAgentDto)publication.ChangeAgent,
                        ChangeType = (ChangeTypeDto)publication.ChangeType,
                        Id = publication.Id,
                        Edition = new EditionDto
                        {
                            BeginDate = edition.DateRange.BeginDate,
                            EndDate = edition.DateRange.EndDate,
                            User = new UserDto
                            {
                                FirstName = edition.User.FirstName,
                                LastName = edition.User.LastName,
                                UserName = edition.User.UserName
                            }
                        }
                    });
            }

            return values;
        }

        public static PublicationDto Map(IEdition<IPublication> edition)
        {
            if (edition == null) return null;

            IPublication publication = edition.Data;

            INadaService service = publication.Service;

            IVehicleConfiguration configuration = publication.VehicleConfiguration;

            int uid = configuration.Uid;

            string vin = configuration.Vin;

            Region region = service.ToRegion(configuration.State);

            IList<Accessory> accessories;

            if (string.IsNullOrEmpty(vin))
            {
                accessories = service.Accessories(uid, region);
            }
            else
            {
                accessories = service.Accessories(uid, region, vin);
            }

            return new PublicationDto
            {
                ChangeAgent = (ChangeAgentDto)publication.ChangeAgent,
                ChangeType = (ChangeTypeDto)publication.ChangeType,
                //Id = publication.Id,
                //Edition = new EditionDto
                //{
                //    BeginDate = edition.DateRange.BeginDate,
                //    EndDate = edition.DateRange.EndDate,
                //    User = new UserDto
                //    {
                //        FirstName = edition.User.FirstName,
                //        LastName = edition.User.LastName,
                //        UserName = edition.User.UserName
                //    }
                //},
                Table = Mapper.Map(publication.Matrix),
                Accessories = Mapper.Map(accessories, configuration.AccessoryStates),
                VehicleConfiguration = Mapper.Map(configuration)
            };
        }
    }
}
