﻿using System;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Traversal;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.Impl
{
    [Serializable]
    public class SuccessorCommand : ICommand<SuccessorResultsDto, SuccessorArgumentsDto>
    {
        public SuccessorResultsDto Execute(SuccessorArgumentsDto parameters)
        {
            INadaTraversal traversal = RegistryFactory.GetResolver().Resolve<INadaTraversal>();

            ITree tree = traversal.CreateTree();
            
            ITreePath path = new TreePath
            {
                State = parameters.Successor
            };

            ITreeNode node = tree.GetNodeByPath(path);

            SuccessorResultsDto results = new SuccessorResultsDto
            {
                Arguments = parameters,
                Path = new PathDto
                {
                    CurrentNode = Mapper.Map(node, Mapper.Operation.Yes, Mapper.Operation.No),
                    Successors = Mapper.Map(node.Children)
                }
            };

            return results;
        }
    }
}
