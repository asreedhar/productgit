﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Traversal;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api;
using ValueType=FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.ValueType;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.Impl
{
    public static class Mapper
    {
        public static List<NodeDto> Map(IEnumerable<ITreeNode> nodes)
        {
            return Map(nodes, Operation.No, Operation.No);
        }

        public static List<NodeDto> Map(IEnumerable<ITreeNode> nodes, Operation parents, Operation children)
        {
            List<NodeDto> list = new List<NodeDto>();

            foreach (ITreeNode node in nodes)
            {
                list.Add(Map(node, parents, children));
            }

            return list;
        }

        public class Operation
        {
            private readonly Operation _next;
            private readonly bool _perform;

            protected Operation(bool perform, Operation next)
            {
                _next = next ?? this;
                _perform = perform;
            }

            public Operation Next
            {
                get { return _next; }
            }

            public bool Perform
            {
                get { return _perform; }
            }

            public static readonly Operation Yes;
            public static readonly Operation No;
            public static readonly Operation NoThenYes;
            public static readonly Operation YesThenNo;
            public static readonly Operation YesThenYesThenNo;

            static Operation()
            {
                Yes = new Operation(true, null);
                No = new Operation(false, null);
                NoThenYes = new Operation(false, Yes);
                YesThenNo = new Operation(true, No);
                YesThenYesThenNo = new Operation(true, YesThenNo);
            }
        }

        public static NodeDto Map(ITreeNode node, Operation parents, Operation children)
        {
            ITreePath treePath = new TreePath();

            node.Save(treePath);

            NodeDto dto = new NodeDto
            {
                Id = node.Id,
                Name = node.Name,
                State = treePath.State,
                Uid = node.Uid.GetValueOrDefault(),
                HasUid = node.Uid.HasValue,
                Label = node.Label
            };

            if (children.Perform)
            {
                dto.Children = Map(node.Children);
            }

            if (parents.Perform && node.Parent != null)
            {
                dto.Parent = Map(node.Parent, parents.Next, children.Next);
            }

            return dto;
        }

        public static PriceTableDto Map(Matrix matrix)
        {
            PriceTableDto table = new PriceTableDto();

            foreach (ValueType valueType in Enum.GetValues(typeof(ValueType)))
            {
                if (valueType == ValueType.Undefined)
                {
                    continue;
                }

                foreach (Valuation valuation in Enum.GetValues(typeof(Valuation)))
                {
                    if (valuation == Valuation.Undefined)
                    {
                        continue;
                    }

                    decimal? value = matrix[valueType, valuation].Value;

                    SetPrice(table, valueType, valuation, value);
                }
            }

            return table;
        }

        private static void SetPrice(PriceTableDto table, ValueType priceType, Valuation valuation, decimal? value)
        {
            PriceTableRowTypeDto rowType;

            switch (valuation)
            {
                case Valuation.Base:
                    rowType = PriceTableRowTypeDto.Base;
                    break;
                case Valuation.Final:
                    rowType = PriceTableRowTypeDto.Final;
                    break;
                case Valuation.Mileage:
                    rowType = PriceTableRowTypeDto.Mileage;
                    break;
                case Valuation.Option:
                    rowType = PriceTableRowTypeDto.Option;
                    break;
                default:
                    return;
            }

            PriceTableRowDto row = table.Rows.FirstOrDefault(
                x => x.RowType == rowType);

            if (row == null)
            {
                row = new PriceTableRowDto
                {
                    Prices = new PricesDto(),
                    RowType = rowType
                };

                table.Rows.Add(row);
            }

            switch (priceType)
            {
                case ValueType.Loan:
                    row.Prices.Loan = value.GetValueOrDefault();
                    row.Prices.HasLoan = value.HasValue;
                    break;
                case ValueType.Retail:
                    row.Prices.Retail = value.GetValueOrDefault();
                    row.Prices.HasRetail = value.HasValue;
                    break;
                case ValueType.Trade:
                    row.Prices.TradeInClean = value.GetValueOrDefault();
                    row.Prices.HasTradeInClean = value.HasValue;
                    break;
                case ValueType.TradeAverage:
                    row.Prices.TradeInAverage = value.GetValueOrDefault();
                    row.Prices.HasTradeInAverage = value.HasValue;
                    break;
                case ValueType.TradeRough:
                    row.Prices.TradeInRough = value.GetValueOrDefault();
                    row.Prices.HasTradeInRough = value.HasValue;
                    break;
            }
        }

        public static List<AccessoryDto> Map(IList<Accessory> accessories, IList<IAccessoryState> states)
        {
            List<AccessoryDto> values = new List<AccessoryDto>();

            foreach (Accessory accessory in accessories)
            {
                string code = accessory.Code;

                values.Add(
                    new AccessoryDto
                    {
                        Code = code,
                        Description = accessory.Description,
                        Prices = Map(accessory.Values)
                    });
            }

            values.Sort(new AccessoryComparer());

            return values;
        }

        private static PricesDto Map(IEnumerable<Value> values)
        {
            PricesDto dto = new PricesDto
                       {
                           HasLoan = values.Any(x => x.ValueType == ValueType.Loan),
                           HasRetail = values.Any(x => x.ValueType == ValueType.Retail),
                           HasTradeInAverage = values.Any(x => x.ValueType == ValueType.TradeAverage),
                           HasTradeInClean = values.Any(x => x.ValueType == ValueType.Trade),
                           HasTradeInRough = values.Any(x => x.ValueType == ValueType.TradeRough)
                       };

            if (dto.HasLoan)
            {
                dto.Loan = values.First(x => x.ValueType == ValueType.Loan).ValueAmount;
            }

            if (dto.HasRetail)
            {
                dto.Retail = values.First(x => x.ValueType == ValueType.Retail).ValueAmount;
            }

            if (dto.HasTradeInAverage)
            {
                dto.TradeInAverage = values.First(x => x.ValueType == ValueType.TradeAverage).ValueAmount;
            }

            if (dto.HasTradeInClean)
            {
                dto.TradeInClean = values.First(x => x.ValueType == ValueType.Trade).ValueAmount;
            }

            if (dto.HasTradeInRough)
            {
                dto.TradeInRough = values.First(x => x.ValueType == ValueType.TradeRough).ValueAmount;
            }

            return dto;
        }

        public static List<StateDto> Map(IEnumerable<State> states)
        {
            List<StateDto> values = new List<StateDto>();

            foreach (State state in states)
            {
                values.Add(Map(state));
            }

            return values;
        }

        private static StateDto Map(State state)
        {
            return new StateDto { Code = state.Code, Name = state.Name };
        }

        public static VehicleConfigurationDto Map(IVehicleConfiguration configuration)  
        {
            return new VehicleConfigurationDto
            {
                Period = new PeriodDto{Id = configuration.Period.Id},
                Uid = configuration.Uid,
                Vin = configuration.Vin,
                Mileage = configuration.Mileage.GetValueOrDefault(),
                HasMileage = configuration.Mileage.HasValue,
                State = Map(configuration.State),
                AccessoryActions = Map(configuration.AccessoryActions),
                AccessoryStates = Map(configuration.AccessoryStates)
            };
        }

        private static List<AccessoryStateDto> Map(IEnumerable<IAccessoryState> states)
        {
            List<AccessoryStateDto> values = new List<AccessoryStateDto>();

            foreach (IAccessoryState state in states)
            {
                values.Add(
                    new AccessoryStateDto
                    {
                        Selected = state.Selected,
                        Enabled = state.Enabled,
                        AccessoryCode = state.AccessoryCode
                    });
            }

            return values;
        }

        private static List<AccessoryActionDto> Map(IEnumerable<IAccessoryAction> actions)
        {
            List<AccessoryActionDto> values = new List<AccessoryActionDto>();

            foreach (IAccessoryAction action in actions)
            {
                values.Add(
                    new AccessoryActionDto
                    {
                        ActionType = (AccessoryActionTypeDto)action.ActionType,
                        AccessoryCode = action.AccessoryCode
                    });
            }

            return values;
        }

        public static IVehicleConfiguration Map(VehicleConfigurationDto configuration, State state, int? mileage)
        {
            if (state == null)
            {
                INadaService service = RegistryFactory.GetResolver().Resolve<INadaService>();

                state = service.States().First(x => Equals(x.Code, configuration.State.Code));
            }

            if (!mileage.HasValue)
            {
                mileage = configuration.HasMileage ? configuration.Mileage : default(int?);
            }

            IVehicleConfigurationBuilderFactory factory = RegistryFactory.GetResolver().Resolve<IVehicleConfigurationBuilderFactory>();

            IVehicleConfigurationBuilder builder = factory.NewBuilder();

            builder.Period = new Period {Id = configuration.Period.Id};
            builder.Uid = configuration.Uid;
            builder.Vin = configuration.Vin;
            builder.State = state;
            builder.Mileage = mileage;

            foreach (AccessoryActionDto value in configuration.AccessoryActions)
            {
                builder.Do(value.AccessoryCode, (AccessoryActionType) value.ActionType);
            }

            foreach (AccessoryStateDto value in configuration.AccessoryStates)
            {
                builder.Add(value.AccessoryCode, value.Enabled, value.Selected);
            }

            return builder.ToVehicleConfiguration();
        }
    }
}
