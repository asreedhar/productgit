﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.Impl
{
    [Serializable]
    public class StatesCommand : ICommand<StatesResultsDto,StatesArgumentsDto>
    {
        public StatesResultsDto Execute(StatesArgumentsDto parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            IList<State> states = resolver.Resolve<INadaService>().States();

            return new StatesResultsDto
            {
                States = Mapper.Map(states)
            };
        }
    }
}
