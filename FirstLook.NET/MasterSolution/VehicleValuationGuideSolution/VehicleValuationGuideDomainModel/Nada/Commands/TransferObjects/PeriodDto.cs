﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects
{
    [Serializable]
    public class PeriodDto
    {
        private int _id;
        private int _year;
        private int _month;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int Year
        {
            get { return _year; }
            set { _year = value; }
        }

        public int Month
        {
            get { return _month; }
            set { _month = value; }
        }
    }
}
