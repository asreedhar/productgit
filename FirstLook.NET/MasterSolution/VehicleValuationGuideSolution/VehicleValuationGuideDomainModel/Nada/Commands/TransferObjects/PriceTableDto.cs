﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects
{
    [Serializable]
    public class PriceTableDto
    {
        private bool _hasTradeInClean;
        private bool _hasTradeInAverage;
        private bool _hasTradeInRough;
        private bool _hasLoan;
        private bool _hasRetail;

        public bool HasTradeInClean
        {
            get { return _hasTradeInClean; }
            set { _hasTradeInClean = value; }
        }

        public bool HasTradeInAverage
        {
            get { return _hasTradeInAverage; }
            set { _hasTradeInAverage = value; }
        }

        public bool HasTradeInRough
        {
            get { return _hasTradeInRough; }
            set { _hasTradeInRough = value; }
        }

        public bool HasLoan
        {
            get { return _hasLoan; }
            set { _hasLoan = value; }
        }

        public bool HasRetail
        {
            get { return _hasRetail; }
            set { _hasRetail = value; }
        }

        private List<PriceTableRowDto> _rows = new List<PriceTableRowDto>();

        public List<PriceTableRowDto> Rows
        {
            get { return _rows; }
            set { _rows = value; }
        }
    }
}
