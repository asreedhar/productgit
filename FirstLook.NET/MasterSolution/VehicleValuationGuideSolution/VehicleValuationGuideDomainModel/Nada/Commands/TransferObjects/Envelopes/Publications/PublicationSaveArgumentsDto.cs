using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes.Publications
{
    [Serializable]
    public class PublicationSaveArgumentsDto : PublicationArgumentsDto
    {
        public VehicleConfigurationDto VehicleConfiguration { get; set; }
    }
}
