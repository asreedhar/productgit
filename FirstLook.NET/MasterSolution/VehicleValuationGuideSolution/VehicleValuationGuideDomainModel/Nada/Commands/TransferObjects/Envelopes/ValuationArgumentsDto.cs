namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes
{
    public class ValuationArgumentsDto
    {
        private string _state;
        private int _mileage;
        private bool _hasMileage;

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }

        public int Mileage
        {
            get { return _mileage; }
            set { _mileage = value; }
        }

        public bool HasMileage
        {
            get { return _hasMileage; }
            set { _hasMileage = value; }
        }
    } 
}