﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public enum TraversalStatusDto
    {
        Success,
        InvalidVinLength,
        InvalidVinCharacters,
        InvalidVinChecksum,
        NoDataForVin
    }
}
