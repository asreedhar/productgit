﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class AdjustedValuationArgumentsDto : ValuationArgumentsDto
    {
        private VehicleConfigurationDto _vehicleConfiguration;

        public VehicleConfigurationDto VehicleConfiguration
        {
            get { return _vehicleConfiguration; }
            set { _vehicleConfiguration = value; }
        }

        private AccessoryActionDto _accessoryAction;

        public AccessoryActionDto AccessoryAction
        {
            get { return _accessoryAction; }
            set { _accessoryAction = value; }
        }
    } 
}
