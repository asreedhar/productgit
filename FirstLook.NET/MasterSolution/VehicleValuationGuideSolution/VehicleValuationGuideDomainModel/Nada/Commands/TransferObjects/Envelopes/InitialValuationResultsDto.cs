﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class InitialValuationResultsDto : ValuationResultsDto
    {
        public InitialValuationArgumentsDto Arguments { get; set; }

        public List<AccessoryDto> Accessories { get; set; }

        public VehicleConfigurationDto VehicleConfiguration { get; set; }
    } 
}
