using System;
using System.Collections.Generic;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Publications;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes.Publications
{
    [Serializable]
    public class PublicationListResultsDto
    {
        public PublicationListArgumentsDto Arguments { get; set; }

        public List<PublicationInfoDto> Publications { get; set; }
    }
}
