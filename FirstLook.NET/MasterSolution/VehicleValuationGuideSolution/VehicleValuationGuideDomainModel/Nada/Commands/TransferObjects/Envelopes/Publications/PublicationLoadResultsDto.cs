using System;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Publications;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes.Publications
{
    [Serializable]
    public class PublicationLoadResultsDto
    {
        public PublicationLoadArgumentsDto Arguments { get; set; }

        public PublicationDto Publication { get; set;}
    }
}
