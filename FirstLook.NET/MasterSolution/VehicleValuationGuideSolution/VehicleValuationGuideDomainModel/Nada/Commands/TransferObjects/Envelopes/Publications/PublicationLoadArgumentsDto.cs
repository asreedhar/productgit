using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes.Publications
{
    [Serializable]
    public class PublicationLoadArgumentsDto : PublicationArgumentsDto
    {
        public int Id { get; set; }
    }
}
