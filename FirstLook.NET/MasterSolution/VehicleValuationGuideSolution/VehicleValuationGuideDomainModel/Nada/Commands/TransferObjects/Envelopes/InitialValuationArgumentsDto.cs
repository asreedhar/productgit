﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class InitialValuationArgumentsDto : ValuationArgumentsDto
    {
        private string _vin;
        private int _uid;

        public string Vin
        {
            get { return _vin; }
            set { _vin = value; }
        }

        public int Uid
        {
            get { return _uid; }
            set { _uid = value; }
        }
    } 
}
