﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects
{
    [Serializable]
    public class VehicleConfigurationDto
    {
        private PeriodDto _period;

        public PeriodDto Period
        {
            get { return _period; }
            set { _period = value; }
        }

        private int _uid;
        private string _vin;

        public int Uid
        {
            get { return _uid; }
            set { _uid = value; }
        }

        public string Vin
        {
            get { return _vin; }
            set { _vin = value; }
        }

        private int _mileage;
        private bool _hasMileage;

        public int Mileage
        {
            get { return _mileage; }
            set { _mileage = value; }
        }

        public bool HasMileage
        {
            get { return _hasMileage; }
            set { _hasMileage = value; }
        }

        private StateDto _state;

        public StateDto State
        {
            get { return _state; }
            set { _state = value; }
        }

        private List<AccessoryStateDto> _accessoryStates;

        public List<AccessoryStateDto> AccessoryStates
        {
            get { return _accessoryStates; }
            set { _accessoryStates = value; }
        }

        private List<AccessoryActionDto> _accessoryActions;

        public List<AccessoryActionDto> AccessoryActions
        {
            get { return _accessoryActions; }
            set { _accessoryActions = value; }
        }
    }
}
