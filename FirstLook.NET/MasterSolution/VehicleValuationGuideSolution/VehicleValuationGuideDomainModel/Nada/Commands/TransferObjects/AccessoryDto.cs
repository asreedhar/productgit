﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects
{
    [Serializable]
    public class AccessoryDto
    {
        private string _code;
        private string _description;

        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private PricesDto _prices;

        public PricesDto Prices
        {
            get { return _prices; }
            set { _prices = value; }
        }
    }

    internal class AccessoryComparer : IComparer<AccessoryDto>
    {
        public int Compare(AccessoryDto x, AccessoryDto y)
        {
            if (x == null)
            {
                if (y == null)
                {
                    return 0;
                }

                return -1;
            }

            if (y == null)
            {
                return 1;
            }

            return string.Compare(x.Description, y.Description);
        }
    }
}
