using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects
{
    [Serializable]
    public enum AccessoryActionTypeDto
    {
        Undefined,
        Add,
        Remove
    }
}
