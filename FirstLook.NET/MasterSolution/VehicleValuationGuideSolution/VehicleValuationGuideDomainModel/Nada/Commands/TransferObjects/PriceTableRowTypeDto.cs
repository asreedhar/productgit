﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects
{
    [Serializable]
    public enum PriceTableRowTypeDto
    {
        Undefined,
        Base,
        Option,
        Mileage,
        Final
    }
}
