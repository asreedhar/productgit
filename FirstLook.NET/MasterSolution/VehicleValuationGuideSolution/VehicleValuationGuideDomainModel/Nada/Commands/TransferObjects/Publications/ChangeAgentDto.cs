﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Publications
{
    [Serializable]
    public enum ChangeAgentDto
    {
        Undefined,
        User,
        System
    }
}
