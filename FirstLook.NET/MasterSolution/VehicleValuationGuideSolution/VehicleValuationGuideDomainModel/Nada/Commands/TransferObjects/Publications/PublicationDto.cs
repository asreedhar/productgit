﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Publications
{
    [Serializable]
    public class PublicationDto : PublicationInfoDto
    {
        public VehicleConfigurationDto VehicleConfiguration { get; set; }

        public List<AccessoryDto> Accessories { get; set; }

        public PriceTableDto Table { get; set; }
    }
}
