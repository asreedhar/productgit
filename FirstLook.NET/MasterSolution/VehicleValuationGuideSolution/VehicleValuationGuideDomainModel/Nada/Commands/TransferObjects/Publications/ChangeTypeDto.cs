﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Publications
{
    [Serializable]
    [Flags]
    public enum ChangeTypeDto
    {
        None = 0,
        Period = 1 << 0,
        State = 1 << 1,
        Uid = 1 << 2,
        Mileage = 1 << 3,
        AccessoryActions = 1 << 4,
        AccessoryStates = 1 << 5
    }
}
