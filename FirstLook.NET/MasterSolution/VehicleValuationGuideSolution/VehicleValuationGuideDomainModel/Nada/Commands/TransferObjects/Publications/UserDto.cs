﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Publications
{
    [Serializable]
    public class UserDto
    {
        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
