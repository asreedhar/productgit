﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Publications
{
    [Serializable]
    public class PublicationInfoDto
    {
        public ChangeAgentDto ChangeAgent { get; set; }

        public ChangeTypeDto ChangeType { get; set; }

        public int Id { get; set; }

        public EditionDto Edition { get; set; }
    }
}
