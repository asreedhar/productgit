﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects
{
    [Serializable]
    public class AccessoryActionDto
    {
        private AccessoryActionTypeDto _actionType;
        private string _accessoryCode;

        public AccessoryActionTypeDto ActionType
        {
            get { return _actionType; }
            set { _actionType = value; }
        }

        public string AccessoryCode
        {
            get { return _accessoryCode; }
            set { _accessoryCode = value; }
        }
    }
}
