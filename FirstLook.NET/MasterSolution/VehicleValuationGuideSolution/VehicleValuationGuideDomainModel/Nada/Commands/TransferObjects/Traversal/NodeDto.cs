﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Traversal
{
    [Serializable]
    public class NodeDto
    {
        private NodeDto _parent;

        public NodeDto Parent
        {
            get { return _parent; }
            set { _parent = value; }
        }

        private List<NodeDto> _children = new List<NodeDto>();

        public List<NodeDto> Children
        {
            get { return _children; }
            set { _children = value; }
        }

        private int _id;
        private string _name;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _state;

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }

        private int _uid;
        private bool _hasUid;

        public int Uid
        {
            get { return _uid; }
            set { _uid = value; }
        }

        public bool HasUid
        {
            get { return _hasUid; }
            set { _hasUid = value; }
        }

        private string _label;

        public string Label
        {
            get { return _label; }
            set { _label = value; }
        }
    }
}
