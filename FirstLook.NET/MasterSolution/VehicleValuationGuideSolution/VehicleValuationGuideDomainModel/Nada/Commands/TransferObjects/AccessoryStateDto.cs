﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects
{
    [Serializable]
    public class AccessoryStateDto
    {
        private string _accessoryCode;
        private bool _selected;
        private bool _enabled;

        public string AccessoryCode
        {
            get { return _accessoryCode; }
            set { _accessoryCode = value; }
        }

        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; }
        }
    }
}
