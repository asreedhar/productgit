﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects
{
    [Serializable]
    public class PricesDto
    {
        private decimal _tradeInClean;
        private decimal _tradeInAverage;
        private decimal _tradeInRough;
        private decimal _loan;
        private decimal _retail;

        public decimal TradeInClean
        {
            get { return _tradeInClean; }
            set { _tradeInClean = value; }
        }

        public decimal TradeInAverage
        {
            get { return _tradeInAverage; }
            set { _tradeInAverage = value; }
        }

        public decimal TradeInRough
        {
            get { return _tradeInRough; }
            set { _tradeInRough = value; }
        }

        public decimal Loan
        {
            get { return _loan; }
            set { _loan = value; }
        }

        public decimal Retail
        {
            get { return _retail; }
            set { _retail = value; }
        }

        private bool _hasTradeInClean;
        private bool _hasTradeInAverage;
        private bool _hasTradeInRough;
        private bool _hasLoan;
        private bool _hasRetail;

        public bool HasTradeInClean
        {
            get { return _hasTradeInClean; }
            set { _hasTradeInClean = value; }
        }

        public bool HasTradeInAverage
        {
            get { return _hasTradeInAverage; }
            set { _hasTradeInAverage = value; }
        }

        public bool HasTradeInRough
        {
            get { return _hasTradeInRough; }
            set { _hasTradeInRough = value; }
        }

        public bool HasLoan
        {
            get { return _hasLoan; }
            set { _hasLoan = value; }
        }

        public bool HasRetail
        {
            get { return _hasRetail; }
            set { _hasRetail = value; }
        }
    }
}
