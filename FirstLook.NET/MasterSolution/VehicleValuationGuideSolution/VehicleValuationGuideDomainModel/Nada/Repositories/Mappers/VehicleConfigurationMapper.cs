using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories;
using FirstLook.Client.DomainModel.Clients.Repositories.Entities;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Sql;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Datastores;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Entities;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Gateways;
using ValueType=FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.ValueType;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Mappers
{
    public class VehicleConfigurationMapper
    {
        private readonly VehicleGateway _vehicleGateway = new VehicleGateway();

        private readonly VehicleConfigurationHistoryGateway _vehicleConfigurationHistoryGateway = new VehicleConfigurationHistoryGateway();

        private readonly VehicleConfigurationGateway _vehicleConfigurationGateway = new VehicleConfigurationGateway();

        private readonly AccessoryActionGateway _actionGateway = new AccessoryActionGateway();

        private readonly AccessoryStateGateway _stateGateway = new AccessoryStateGateway();

        private readonly MatrixGateway _matrixGateway = new MatrixGateway();

        private readonly MatrixCellGateway _matrixCellGateway = new MatrixCellGateway();

        private readonly VehicleConfigurationHistoryMatrixGateway _vehicleConfigurationHistoryMatrixGateway = new VehicleConfigurationHistoryMatrixGateway();

        public IList<IEdition<IPublicationInfo>> Load(IBroker broker, ClientVehicleIdentification vehicle)
        {
            VehicleGateway.Row vehicleRow = _vehicleGateway.Fetch(vehicle.Id);

            IList<IEdition<IPublicationInfo>> values = new List<IEdition<IPublicationInfo>>();

            if (vehicleRow != null)
            {
                IList<VehicleConfigurationHistoryGateway.Row> vehicleConfigurationHistoryRows = _vehicleConfigurationHistoryGateway.Fetch(vehicleRow.VehicleConfigurationId);

                foreach (var vehicleConfigurationHistoryRow in vehicleConfigurationHistoryRows)
                {
                    ChangeType changeType = (ChangeType) vehicleConfigurationHistoryRow.ChangeTypeBitFlags;

                    AuditRow auditRow = vehicleConfigurationHistoryRow.AuditRow;

                    IUser user = GetUser(vehicleConfigurationHistoryRow);

                    values.Add(
                        new Edition<IPublicationInfo>
                        {
                            Data = new PublicationInfo
                            {
                                ChangeAgent = ChangeAgent.User,
                                ChangeType = changeType,
                                Id = vehicleConfigurationHistoryRow.Id
                            },
                            DateRange = GetDateRange(auditRow),
                            User = user
                        });
                }
            }

            return values;
        }

        public IEdition<IPublication> Load(IBroker broker, ClientVehicleIdentification vehicle, DateTime on)
        {
            VehicleGateway.Row vehicleRow = _vehicleGateway.Fetch(vehicle.Id);

            if (vehicleRow == null)
            {
                return null;
            }

            VehicleConfigurationHistoryGateway.Row vehicleConfigurationHistoryRow =
                _vehicleConfigurationHistoryGateway.Fetch(vehicleRow.VehicleConfigurationId, on);

            if (vehicleConfigurationHistoryRow == null)
            {
                return null;
            }

            return GetPublication(vehicleConfigurationHistoryRow);
        }

        public IEdition<IPublication> Load(IBroker broker, ClientVehicleIdentification vehicle, int id)
        {
            VehicleGateway.Row vehicleRow = _vehicleGateway.Fetch(vehicle.Id);

            if (vehicleRow == null)
            {
                return null;
            }

            VehicleConfigurationHistoryGateway.Row vehicleConfigurationHistoryRow =
                _vehicleConfigurationHistoryGateway.Fetch(vehicleRow.VehicleConfigurationId, id);

            if (vehicleConfigurationHistoryRow == null)
            {
                return null;
            }

            return GetPublication(vehicleConfigurationHistoryRow);
        }

        public IEdition<IPublication> Save(IBroker broker, ClientVehicleIdentification vehicle, IVehicleConfiguration configuration, Matrix matrix)
        {
            VehicleGateway.Row vehicleRow = _vehicleGateway.Fetch(vehicle.Id);

            VehicleConfigurationHistoryGateway.Row vehicleConfigurationHistoryRow;

            ChangeType changeType;

            if (vehicleRow == null)
            {
                CloneData(configuration);

                VehicleConfigurationGateway.Row vehicleConfigurationRow = _vehicleConfigurationGateway.Insert(
                    vehicle.Vehicle.Id);

                _vehicleGateway.Insert(
                    vehicle.Id,
                    vehicleConfigurationRow.Id);

                vehicleConfigurationHistoryRow = Insert(
                    vehicleConfigurationRow,
                    configuration,
                    matrix,
                    ChangeType.None,
                    null);

                changeType = ChangeType.None;
            }
            else
            {
                vehicleConfigurationHistoryRow = _vehicleConfigurationHistoryGateway.Fetch(
                        vehicleRow.VehicleConfigurationId,
                        DateTime.Now);

                VehicleConfigurationGateway.Row vehicleConfigurationRow = _vehicleConfigurationGateway.Fetch(
                    vehicleConfigurationHistoryRow.VehicleConfigurationId);

                IEdition<IPublication> currentPublication = GetPublication(vehicleConfigurationHistoryRow);

                changeType = currentPublication.Data.VehicleConfiguration.Compare(configuration);

                if (changeType != ChangeType.None)
                {
                    CloneData(configuration);

                    vehicleConfigurationHistoryRow = Insert(
                        vehicleConfigurationRow,
                        configuration,
                        matrix,
                        changeType,
                        vehicleConfigurationHistoryRow.AuditRow);
                }
            }

            return new Edition<IPublication>
            {
                Data = new Publication
                {
                    ChangeAgent = ChangeAgent.User,
                    ChangeType = changeType,
                    Id = vehicleConfigurationHistoryRow.Id,
                    Matrix = matrix,
                    Service = GetService(configuration.Period.Id),
                    VehicleConfiguration = configuration
                },
                DateRange = GetDateRange(vehicleConfigurationHistoryRow.AuditRow),
                User = GetUser(vehicleConfigurationHistoryRow)
            };
        }

        private static void CloneData(IVehicleConfiguration configuration)
        {
            RegistryFactory.GetResolver().Resolve<INadaDatastore>().CloneData(
                configuration.Period.Id,
                configuration.Uid,
                configuration.Vin);
        }

        private VehicleConfigurationHistoryGateway.Row Insert(VehicleConfigurationGateway.Row vehicleConfigurationRow, IVehicleConfiguration configuration, Matrix matrix, ChangeType changeType, AuditRow auditRow)
        {
            VehicleConfigurationHistoryGateway.Row vehicleConfigurationHistoryRow;

            int period = configuration.Period.Id;

            if (auditRow == null)
            {
                vehicleConfigurationHistoryRow = _vehicleConfigurationHistoryGateway.Insert(
                    vehicleConfigurationRow.Id,
                    period,
                    configuration.Uid,
                    configuration.State.Code,
                    configuration.Mileage,
                    (int) changeType);
            }
            else
            {
                vehicleConfigurationHistoryRow = _vehicleConfigurationHistoryGateway.Update(
                    vehicleConfigurationRow.Id,
                    period,
                    configuration.Uid,
                    configuration.State.Code,
                    configuration.Mileage,
                    (int)changeType,
                    auditRow);
            }

            for (int i = 0, l = configuration.AccessoryActions.Count; i < l; i++)
            {
                IAccessoryAction action = configuration.AccessoryActions[i];

                _actionGateway.Insert(
                    period,
                    vehicleConfigurationHistoryRow.Id,
                    (byte) action.ActionType,
                    (byte)i,
                    action.AccessoryCode);
            }

            for (int i = 0, l = configuration.AccessoryStates.Count; i < l; i++)
            {
                IAccessoryState state = configuration.AccessoryStates[i];

                _stateGateway.Insert(
                    period,
                    vehicleConfigurationHistoryRow.Id,
                    state.AccessoryCode,
                    state.Enabled,
                    state.Selected);
            }

            MatrixGateway.Row matrixRow = _matrixGateway.Insert(period);

            new VehicleConfigurationHistoryMatrixGateway().Insert(
                period,
                vehicleConfigurationHistoryRow.Id,
                matrixRow.Id);

            foreach (ValueType valueType in Enum.GetValues(typeof(ValueType)))
            {
                if (valueType == ValueType.Undefined) continue;

                foreach (Valuation valuation in Enum.GetValues(typeof (Valuation)))
                {
                    if (valuation == Valuation.Undefined) continue;

                    Matrix.Cell cell = matrix[valueType, valuation];

                    _matrixCellGateway.Insert(
                        period,
                        matrixRow.Id,
                        (int) valueType,
                        (byte) valuation,
                        cell.Visible,
                        cell.Value);
                }
            }
            return vehicleConfigurationHistoryRow;
        }

        #region Helper Methods

        private static IUser GetUser(VehicleConfigurationHistoryGateway.Row vehicleConfigurationHistoryRow)
        {
            User u = vehicleConfigurationHistoryRow.User;

            IPrincipal principal;

            switch (u.UserType)
            {
                case UserType.Member:
                    principal = new DealerRepository().Principal(u.Handle);
                    break;
                case UserType.System:
                    principal = new AgentRepository().Principal(u.Id);
                    break;
                default:
                    throw new ArgumentException("unknown user type");
            }

            return principal.User;
        }

        private Matrix GetMatrix(VehicleConfigurationHistoryGateway.Row vehicleConfigurationHistoryRow)
        {
            VehicleConfigurationHistoryMatrixGateway.Row vehicleConfigurationHistoryMatrixRow =
                _vehicleConfigurationHistoryMatrixGateway.Fetch(
                    vehicleConfigurationHistoryRow.Period,
                    vehicleConfigurationHistoryRow.Id);

            IList<MatrixCellGateway.Row> matrixRows =
                _matrixCellGateway.Fetch(
                vehicleConfigurationHistoryMatrixRow.Period,
                vehicleConfigurationHistoryMatrixRow.MatrixId);

            Matrix matrix = new Matrix();

            foreach (MatrixCellGateway.Row matrixRow in matrixRows)
            {
                Matrix.Cell cell = matrix[(ValueType)matrixRow.ValueTypeId, (Valuation)matrixRow.ValuationId];

                cell.Value = matrixRow.Value;

                cell.Visible = matrixRow.Visible;
            }

            return matrix;
        }

        private static DateRange GetDateRange(AuditRow auditRow)
        {
            return new DateRange
            {
                BeginDate = auditRow.ValidFrom,
                EndDate = auditRow.ValidUpTo
            };
        }

        private static INadaService GetService(int periodId)
        {
            IResolver resolver =
                RegistryFactory.GetRegistry().Clone().Register<ISqlService, SnapshotSqlService>(
                    ImplementationScope.Isolated).CreateScope();

            return new NadaService(resolver, () => periodId);
        }

        private IEdition<IPublication> GetPublication(VehicleConfigurationHistoryGateway.Row vehicleConfigurationHistoryRow)
        {
            ChangeType changeType = (ChangeType)vehicleConfigurationHistoryRow.ChangeTypeBitFlags;

            AuditRow auditRow = vehicleConfigurationHistoryRow.AuditRow;

            IUser user = GetUser(vehicleConfigurationHistoryRow);

            Matrix matrix = GetMatrix(vehicleConfigurationHistoryRow);

            INadaService service = GetService(vehicleConfigurationHistoryRow.Period);

            IEdition<IPublication> value = new Edition<IPublication>
            {
                Data = new Publication
                {
                    ChangeAgent = ChangeAgent.User,
                    ChangeType = changeType,
                    Id = vehicleConfigurationHistoryRow.Id,
                    Matrix = matrix,
                    Service = service,
                    VehicleConfiguration = GetVehicleConfiguration(vehicleConfigurationHistoryRow)
                },
                DateRange = GetDateRange(auditRow),
                User = user
            };

            return value;
        }

        private IVehicleConfiguration GetVehicleConfiguration(VehicleConfigurationHistoryGateway.Row vehicleConfigurationHistoryRow)
        {
            VehicleConfigurationGateway.Row vehicleConfigurationRow = _vehicleConfigurationGateway.Fetch(
                vehicleConfigurationHistoryRow.VehicleConfigurationId);

            IList<AccessoryActionGateway.Row> actionRows = _actionGateway.Fetch(
                vehicleConfigurationHistoryRow.Period,
                vehicleConfigurationHistoryRow.Id);

            IList<AccessoryStateGateway.Row> stateRows = _stateGateway.Fetch(
                vehicleConfigurationHistoryRow.Period,
                vehicleConfigurationHistoryRow.Id);

            IResolver resolver = RegistryFactory.GetResolver();

            IVehicleConfigurationBuilderFactory factory = resolver.Resolve<IVehicleConfigurationBuilderFactory>();

            IVehicleConfigurationBuilder builder = factory.NewBuilder();

            INadaService service = resolver.Resolve<INadaService>();

            builder.Period = new Period {Id = vehicleConfigurationHistoryRow.Period};
            builder.Uid = vehicleConfigurationHistoryRow.Uid;
            builder.Vin = vehicleConfigurationRow.Vin;
            builder.State = service.States().FirstOrDefault(x => x.Code == vehicleConfigurationHistoryRow.StateCode);
            builder.Mileage = vehicleConfigurationHistoryRow.Mileage;

            foreach (AccessoryActionGateway.Row actionRow in actionRows)
            {
                builder.Do(actionRow.AccCode, (AccessoryActionType) actionRow.AccessoryActionTypeId);
            }

            foreach (AccessoryStateGateway.Row stateRow in stateRows)
            {
                builder.Add(stateRow.AccCode, stateRow.Enabled, stateRow.Selected);
            }

            return builder.ToVehicleConfiguration();
        }

        #endregion
    }
}
