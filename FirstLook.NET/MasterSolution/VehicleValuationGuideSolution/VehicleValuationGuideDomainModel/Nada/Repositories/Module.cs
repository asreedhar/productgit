﻿using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<Datastores.Module>();

            registry.Register<INadaRepository, NadaRepository>(ImplementationScope.Shared);

            registry.Register<IVehicleConfigurationBuilderFactory, VehicleConfigurationBuilderFactory>(ImplementationScope.Shared);
        }

    }
}
