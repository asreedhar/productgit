﻿using System;
using System.Data;
using System.Reflection;
using FirstLook.Common.Core.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Datastores
{
    public class NadaDatastore : SessionDataStore, INadaDatastore
    {
        internal const string Prefix = "FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Resources";

        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        public void CloneData(int period, int uid, string vin)
        {
            using (IDbCommand command = CreateCommand())
            {
                command.CommandText = "Nada.CloneData";

                command.CommandType = CommandType.StoredProcedure;

                Database.AddWithValue(command, "Period", period, DbType.Int32);

                Database.AddWithValue(command, "Uid", uid, DbType.Int32);

                Database.AddWithValue(command, "Vin", vin, DbType.String);
                
                command.ExecuteNonQuery();
            }
        }

        public IDataReader VehicleConfiguration_History_Fetch(int vehicleConfigurationId)
        {
            const string queryName = Prefix + ".NadaDatastore_VehicleConfiguration_History_Fetch_All.txt";

            return Query(
                new[] { "VehicleConfiguration_History" },
                queryName,
                new Parameter("VehicleConfigurationID", vehicleConfigurationId, DbType.Int32));
        }

        public IDataReader VehicleConfiguration_Fetch(int vehicleConfigurationId)
        {
            const string queryName = Prefix + ".NadaDatastore_VehicleConfiguration_Fetch.txt";

            return Query(
                new[] { "VehicleConfiguration" },
                queryName,
                new Parameter("VehicleConfigurationID", vehicleConfigurationId, DbType.Int32));
        }

        public IDataReader VehicleConfiguration_History_Fetch(int vehicleConfigurationId, DateTime on)
        {
            const string queryName = Prefix + ".NadaDatastore_VehicleConfiguration_History_Fetch_DateTime.txt";

            return Query(
                new[] { "VehicleConfiguration_History" },
                queryName,
                new Parameter("VehicleConfigurationID", vehicleConfigurationId, DbType.Int32),
                new Parameter("Date", on, DbType.DateTime));
        }

        public IDataReader VehicleConfiguration_History_Fetch(int vehicleConfigurationId, int vehicleConfigurationHistoryId)
        {
            const string queryName = Prefix + ".NadaDatastore_VehicleConfiguration_History_Fetch_Id.txt";

            return Query(
                new[] { "VehicleConfiguration_History" },
                queryName,
                new Parameter("VehicleConfigurationID", vehicleConfigurationId, DbType.Int32),
                new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32));
        }

        public IDataReader AccessoryAction_Fetch(int period, int vehicleConfigurationHistoryId)
        {
            const string queryName = Prefix + ".NadaDatastore_AccessoryAction_Fetch.txt";

            return Query(
                new[] { "AccessoryAction" },
                queryName,
                new Parameter("Period", period, DbType.Int32),
                new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32));
        }

        public IDataReader AccessoryState_Fetch(int period, int vehicleConfigurationHistoryId)
        {
            const string queryName = Prefix + ".NadaDatastore_AccessoryState_Fetch.txt";

            return Query(
                new[] { "AccessoryState" },
                queryName,
                new Parameter("Period", period, DbType.Int32),
                new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32));
        }

        public IDataReader VehicleConfiguration_Insert(int vehicleId)
        {
            const string queryNameI = Prefix + ".NadaDatastore_VehicleConfiguration_Insert.txt";

            const string queryNameF = Prefix + ".NadaDatastore_VehicleConfiguration_Fetch_Identity.txt";

            NonQuery(
                queryNameI,
                new Parameter("VehicleID", vehicleId, DbType.Int32));

            return Query(
                new[] { "VehicleConfiguration" },
                queryNameF);
        }

        public IDataReader VehicleConfiguration_History_Insert(int vehicleConfigurationId, int period, int uid, string state, int? mileage, int changeTypeBitFlags, int auditRowId)
        {
            const string queryNameI = Prefix + ".NadaDatastore_VehicleConfiguration_History_Insert.txt";

            const string queryNameF = Prefix + ".NadaDatastore_VehicleConfiguration_History_Fetch_Identity.txt";

            Parameter m = mileage.HasValue
                              ? new Parameter("Mileage", mileage.Value, DbType.Int32)
                              : new Parameter("Mileage", DBNull.Value, DbType.Int32);

            NonQuery(
                queryNameI,
                new Parameter("VehicleConfigurationID", vehicleConfigurationId, DbType.Int32),
                new Parameter("Period", period, DbType.Int32),
                new Parameter("Uid", uid, DbType.Int32),
                m,
                new Parameter("ChangeTypeBitFlags", changeTypeBitFlags, DbType.Int32),
                new Parameter("AuditRowID", auditRowId, DbType.Int32),
                new Parameter("StateCode", state, DbType.String));

            return Query(
                new[] { "VehicleConfiguration" },
                queryNameF);
        }

        public void AccessoryAction_Insert(int period, int vehicleConfigurationHistoryId, byte accessoryActionTypeId, byte accessoryActionIndex, string accessoryCode)
        {
            const string queryName = Prefix + ".NadaDatastore_AccessoryAction_Insert.txt";

            NonQuery(
                queryName,
                new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32),
                new Parameter("AccessoryActionTypeID", accessoryActionTypeId, DbType.Byte),
                new Parameter("AccessoryActionIndex", accessoryActionIndex, DbType.Byte),
                new Parameter("AccCode", accessoryCode, DbType.String),
                new Parameter("Period", period, DbType.Int32));
        }

        public void AccessoryState_Insert(int period, int vehicleConfigurationHistoryId, string accessoryCode, bool enabled, bool selected)
        {
            const string queryName = Prefix + ".NadaDatastore_AccessoryState_Insert.txt";

            NonQuery(
                queryName,
                new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32),
                new Parameter("Period", period, DbType.Int32),
                new Parameter("AccCode", accessoryCode, DbType.String),
                new Parameter("Enabled", enabled, DbType.Boolean),
                new Parameter("Selected", selected, DbType.Boolean));
        }

        public IDataReader VehicleConfiguration_History_Matrix_Fetch(int period, int vehicleConfigurationHistoryId)
        {
            const string queryName = Prefix + ".NadaDatastore_VehicleConfiguration_History_Matrix_Fetch.txt";

            return Query(
                new[] { "VehicleConfiguration_History_Matrix" },
                queryName,
                new Parameter("Period", period, DbType.Int32),
                new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32));
        }

        public IDataReader MatrixCell_Fetch(int period, int matrixId)
        {
            const string queryName = Prefix + ".NadaDatastore_MatrixCell_Fetch.txt";

            return Query(
                new[] { "MatrixCell" },
                queryName,
                new Parameter("Period", period, DbType.Int32),
                new Parameter("MatrixID", matrixId, DbType.Int32));
        }

        public IDataReader Matrix_Insert(int period)
        {
            const string queryNameI = Prefix + ".NadaDatastore_Matrix_Insert.txt";

            const string queryNameF = Prefix + ".NadaDatastore_Matrix_Fetch_Identity.txt";

            NonQuery(
                queryNameI,
                new Parameter("Period", period, DbType.Int32));

            return Query(new[] {"Matrix"}, queryNameF);
        }

        public void MatrixCell_Insert(int period, int matrixId, int valueTypeId, byte valuationId, bool visible, decimal? value)
        {
            const string queryName = Prefix + ".NadaDatastore_MatrixCell_Insert.txt";

            Parameter parameter = value.HasValue
                              ? new Parameter("Value", value, DbType.Decimal)
                              : new Parameter("Value", DBNull.Value, DbType.Decimal);

            NonQuery(
                queryName,
                new Parameter("MatrixID", matrixId, DbType.Int32),
                new Parameter("Period", period, DbType.Int32),
                new Parameter("ValueTypeID", valueTypeId, DbType.Int32),
                new Parameter("ValuationID", valuationId, DbType.Byte),
                new Parameter("Visible", visible, DbType.Boolean),
                parameter);
        }

        public void VehicleConfiguration_History_Matrix_Insert(int period, int vehicleConfigurationHistoryId, int matrixId)
        {
            const string queryName = Prefix + ".NadaDatastore_VehicleConfiguration_History_Matrix_Insert.txt";

            NonQuery(
                queryName,
                new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32),
                new Parameter("MatrixID", matrixId, DbType.Int32),
                new Parameter("Period", period, DbType.Int32));
        }
    }
}
