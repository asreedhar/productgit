﻿using System;
using System.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Datastores
{
    public interface INadaDatastore
    {
        void CloneData(int period, int uid, string vin);

        IDataReader VehicleConfiguration_History_Fetch(int vehicleConfigurationId);

        IDataReader VehicleConfiguration_Fetch(int vehicleConfigurationId);

        IDataReader VehicleConfiguration_History_Fetch(int vehicleConfigurationId, DateTime on);

        IDataReader VehicleConfiguration_History_Fetch(int vehicleConfigurationId, int vehicleConfigurationHistoryId);

        IDataReader AccessoryAction_Fetch(int period, int vehicleConfigurationHistoryId);

        IDataReader AccessoryState_Fetch(int period, int vehicleConfigurationHistoryId);

        IDataReader VehicleConfiguration_Insert(int vehicleId);

        IDataReader VehicleConfiguration_History_Insert(int vehicleConfigurationId, int period, int uid, string state, int? mileage, int changeTypeBitFlags, int auditRowId);

        void AccessoryAction_Insert(int period, int vehicleConfigurationHistoryId, byte accessoryActionTypeId, byte accessoryActionIndex, string accessoryCode);

        void AccessoryState_Insert(int period, int vehicleConfigurationHistoryId, string accessoryCode, bool enabled, bool selected);

        IDataReader VehicleConfiguration_History_Matrix_Fetch(int period, int vehicleConfigurationHistoryId);

        IDataReader MatrixCell_Fetch(int period, int matrixId);

        IDataReader Matrix_Insert(int period);

        void MatrixCell_Insert(int period, int matrixId, int valueTypeId, byte valuationId, bool visible, decimal? value);

        void VehicleConfiguration_History_Matrix_Insert(int period, int vehicleConfigurationHistoryId, int matrixId);
    }
}
