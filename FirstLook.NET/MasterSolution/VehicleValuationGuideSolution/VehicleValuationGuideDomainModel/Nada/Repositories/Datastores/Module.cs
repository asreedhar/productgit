﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Datastores
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IClientDatastore, ClientDatastore>(ImplementationScope.Shared);

            registry.Register<INadaDatastore, NadaDatastore>(ImplementationScope.Shared);
        }
    }
}
