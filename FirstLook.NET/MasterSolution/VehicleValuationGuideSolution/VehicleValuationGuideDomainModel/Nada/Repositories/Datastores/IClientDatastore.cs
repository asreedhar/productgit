﻿using System.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Datastores
{
    public interface IClientDatastore
    {
        IDataReader Vehicle_Nada_Fetch(int vehicleId);

        void Vehicle_Nada_Insert(int vehicleId, int vehicleConfigurationId);
    }
}
