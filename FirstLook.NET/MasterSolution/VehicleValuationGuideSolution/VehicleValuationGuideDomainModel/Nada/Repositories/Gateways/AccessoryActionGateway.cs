﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Datastores;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Gateways
{
    public class AccessoryActionGateway : GatewayBase
    {
        [Serializable]
        public class Row
        {
            public int VehicleConfigurationHistoryId { get; set; }

            public byte AccessoryActionTypeId { get; set; }

            public byte AccessoryActionIndex { get; set; }

            public int Period { get; set; }

            public string AccCode { get; set; }
        }

        protected class RowSerializer : Serializer<Row>
        {
            public override Row Deserialize(IDataRecord record)
            {
                int vehicleConfigurationHistoryId = record.GetInt32(record.GetOrdinal("VehicleConfigurationHistoryId"));

                byte accessoryActionTypeId = record.GetByte(record.GetOrdinal("AccessoryActionTypeId"));

                byte accessoryActionIndex = record.GetByte(record.GetOrdinal("AccessoryActionIndex"));

                int period = record.GetInt32(record.GetOrdinal("Period"));

                string accCode = record.GetString(record.GetOrdinal("AccCode"));

                return new Row
                {
                    VehicleConfigurationHistoryId = vehicleConfigurationHistoryId,
                    Period = period,
                    AccCode = accCode,
                    AccessoryActionIndex = accessoryActionIndex,
                    AccessoryActionTypeId = accessoryActionTypeId
                };
            }
        }

        public IList<Row> Fetch(int period, int vehicleConfigurationHistoryId)
        {
            string key = CreateCacheKey(period, vehicleConfigurationHistoryId);

            IList<Row> rows = Cache.Get(key) as IList<Row>;

            if (rows == null)
            {
                INadaDatastore datastore = Resolve<INadaDatastore>();

                using (IDataReader reader = datastore.AccessoryAction_Fetch(period, vehicleConfigurationHistoryId))
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    rows = serializer.Deserialize(reader);

                    Remember(key, rows);
                }
            }

            return rows;
        }

        public void Insert(int period, int vehicleConfigurationHistoryId, byte accessoryActionTypeId, byte accessoryActionIndex, string accessoryCode)
        {
            INadaDatastore datastore = Resolve<INadaDatastore>();

            datastore.AccessoryAction_Insert(period, vehicleConfigurationHistoryId, accessoryActionTypeId, accessoryActionIndex, accessoryCode);
        }
    }
}
