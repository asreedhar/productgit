﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Datastores;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Gateways
{
    public class AccessoryStateGateway : GatewayBase
    {
        [Serializable]
        public class Row
        {
            public int VehicleConfigurationHistoryId { get; set; }

            public int Period { get; set; }

            public string AccCode { get; set; }

            public bool Enabled { get; set; }

            public bool Selected { get; set; }
        }

        protected class RowSerializer : Serializer<Row>
        {
            public override Row Deserialize(IDataRecord record)
            {
                int vehicleConfigurationId = record.GetInt32(record.GetOrdinal("VehicleConfigurationHistoryId"));

                int period = record.GetInt32(record.GetOrdinal("Period"));

                bool selected = record.GetBoolean(record.GetOrdinal("Selected"));

                bool enabled = record.GetBoolean(record.GetOrdinal("Enabled"));

                string accCode = record.GetString(record.GetOrdinal("AccCode"));

                return new Row
                {
                    VehicleConfigurationHistoryId = vehicleConfigurationId,
                    Period = period,
                    AccCode = accCode,
                    Enabled = enabled,
                    Selected = selected
                };
            }
        }

        public IList<Row> Fetch(int period, int vehicleConfigurationHistoryId)
        {
            string key = CreateCacheKey(period, vehicleConfigurationHistoryId);

            IList<Row> rows = Cache.Get(key) as IList<Row>;

            if (rows == null)
            {
                INadaDatastore datastore = Resolve<INadaDatastore>();

                using (IDataReader reader = datastore.AccessoryState_Fetch(period, vehicleConfigurationHistoryId))
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    rows = serializer.Deserialize(reader);

                    Remember(key, rows);
                }
            }

            return rows;
        }

        public void Insert(int period, int vehicleConfigurationHistoryId, string accessoryCode, bool enabled, bool selected)
        {
            INadaDatastore datastore = Resolve<INadaDatastore>();

            datastore.AccessoryState_Insert(period, vehicleConfigurationHistoryId, accessoryCode, enabled, selected);
        }
    }
}
