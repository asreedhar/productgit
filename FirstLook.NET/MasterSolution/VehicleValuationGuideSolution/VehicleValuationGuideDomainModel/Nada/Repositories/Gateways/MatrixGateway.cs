﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Datastores;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Gateways
{
    public class MatrixGateway : GatewayBase
    {
        [Serializable]
        public class Row
        {
            public int Period { get; set; }

            public int Id { get; set; }
        }

        protected class RowSerializer : Serializer<Row>
        {
            public override Row Deserialize(IDataRecord record)
            {
                int period = record.GetInt32(record.GetOrdinal("Period"));

                int id = record.GetInt32(record.GetOrdinal("MatrixId"));

                return new Row {Period = period, Id = id};
            }
        }

        public Row Insert(int period)
        {
            INadaDatastore datastore = Resolve<INadaDatastore>();

            using (IDataReader reader = datastore.Matrix_Insert(period))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    return serializer.Deserialize((IDataRecord)reader);
                }
            }

            return null;
        }
    }
}
