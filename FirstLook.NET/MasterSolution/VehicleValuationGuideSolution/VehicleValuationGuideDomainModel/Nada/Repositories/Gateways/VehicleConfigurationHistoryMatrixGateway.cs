﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Datastores;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Gateways
{
    public class VehicleConfigurationHistoryMatrixGateway : GatewayBase
    {
        [Serializable]
        public class Row
        {
            public int VehicleConfigurationHistoryId { get; set; }

            public int MatrixId { get; set; }

            public int Period { get; set; }
        }

        protected class RowSerializer : Serializer<Row>
        {
            public override Row Deserialize(IDataRecord record)
            {
                int vehicleConfigurationHistoryId = record.GetInt32(record.GetOrdinal("VehicleConfigurationHistoryId"));

                int matrixId = record.GetInt32(record.GetOrdinal("MatrixId"));

                int period = record.GetInt32(record.GetOrdinal("Period"));

                return new Row
                {
                    VehicleConfigurationHistoryId = vehicleConfigurationHistoryId,
                    MatrixId = matrixId,
                    Period = period
                };
            }
        }

        public Row Fetch(int period, int vehicleConfigurationHistoryId)
        {
            string key = CreateCacheKey(vehicleConfigurationHistoryId);

            Row row = Cache.Get(key) as Row;

            if (row == null)
            {
                INadaDatastore datastore = Resolve<INadaDatastore>();

                using (IDataReader reader = datastore.VehicleConfiguration_History_Matrix_Fetch(period, vehicleConfigurationHistoryId))
                {
                    if (reader.Read())
                    {
                        ISerializer<Row> serializer = new RowSerializer();

                        row = serializer.Deserialize((IDataRecord)reader);

                        Remember(key, row);
                    }
                }
            }

            return row;
        }

        public void Insert(int period, int vehicleConfigurationHistoryId, int matrixId)
        {
            INadaDatastore datastore = Resolve<INadaDatastore>();

            datastore.VehicleConfiguration_History_Matrix_Insert(period, vehicleConfigurationHistoryId, matrixId);
        }
    }
}
