﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Entities;
using FirstLook.Client.DomainModel.Clients.Repositories.Serializers;
using FirstLook.Client.DomainModel.Clients.Repositories.Utility;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Datastores;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Gateways
{
    public class VehicleConfigurationHistoryGateway : AuditingGateway
    {
        [Serializable]
        public class Row
        {
            public int Id { get; set; }

            public int VehicleConfigurationId { get; set; }

            public int Period { get; set; }

            public int Uid { get; set; }

            public int? Mileage { get; set; }

            public int ChangeTypeBitFlags { get; set; }

            public AuditRow AuditRow { get; set; }

            public User User { get; set; }

            public string StateCode { get; set; }
        }

        protected class RowSerializer : Serializer<Row>
        {
            public override Row Deserialize(IDataRecord record)
            {
                int vehicleConfigurationHistoryId = record.GetInt32(record.GetOrdinal("VehicleConfigurationHistoryId"));

                int vehicleConfigurationId = record.GetInt32(record.GetOrdinal("VehicleConfigurationId"));

                int period = record.GetInt32(record.GetOrdinal("Period"));

                int uid = record.GetInt32(record.GetOrdinal("Uid"));

                int? mileage = null;

                int ordinal = record.GetOrdinal("Mileage");

                if (!record.IsDBNull(ordinal))
                {
                    mileage = record.GetInt32(ordinal);
                }
                
                int changeTypeBitFlags = record.GetInt32(record.GetOrdinal("ChangeTypeBitFlags"));

                AuditRow auditRow = new AuditRowSerializer().Deserialize(record);

                string stateCode = record.GetString(record.GetOrdinal("StateCode")).Trim();

                User user = new UserSerializer().Deserialize(record);

                return new Row
                {
                    Id = vehicleConfigurationHistoryId,
                    VehicleConfigurationId = vehicleConfigurationId,
                    ChangeTypeBitFlags = changeTypeBitFlags,
                    Mileage = mileage,
                    Period = period,
                    StateCode = stateCode,
                    Uid = uid,
                    AuditRow = auditRow,
                    User = user
                };
            }
        }

        public IList<Row> Fetch(int vehicleConfigurationId)
        {
            INadaDatastore datastore = Resolve<INadaDatastore>();

            using (IDataReader reader = datastore.VehicleConfiguration_History_Fetch(vehicleConfigurationId))
            {
                ISerializer<Row> serializer = new RowSerializer();

                return serializer.Deserialize(reader);
            }
        }

        public Row Fetch(int vehicleConfigurationId, DateTime on)
        {
            string key = CreateCacheKey(vehicleConfigurationId, on);

            Row row = Cache.Get(key) as Row;

            if (row == null)
            {
                INadaDatastore datastore = Resolve<INadaDatastore>();

                using (IDataReader reader = datastore.VehicleConfiguration_History_Fetch(vehicleConfigurationId, on))
                {
                    if (reader.Read())
                    {
                        ISerializer<Row> serializer = new RowSerializer();

                        row = serializer.Deserialize((IDataRecord) reader);

                        Remember(key, row);
                    }
                }
            }

            return row;
        }

        public Row Fetch(int vehicleConfigurationId, int vehicleConfigurationHistoryId)
        {
            string key = CreateCacheKey(vehicleConfigurationId, vehicleConfigurationHistoryId);

            Row row = Cache.Get(key) as Row;

            if (row == null)
            {
                INadaDatastore datastore = Resolve<INadaDatastore>();

                using (IDataReader reader = datastore.VehicleConfiguration_History_Fetch(vehicleConfigurationId, vehicleConfigurationHistoryId))
                {
                    if (reader.Read())
                    {
                        ISerializer<Row> serializer = new RowSerializer();

                        row = serializer.Deserialize((IDataRecord)reader);

                        Remember(key, row);
                    }
                }
            }

            return row;
        }

        public Row Insert(int vehicleConfigurationId, int period, int uid, string state, int? mileage, int changeTypeBitFlags)
        {
            INadaDatastore datastore = Resolve<INadaDatastore>();

            using (IDataReader reader = datastore.VehicleConfiguration_History_Insert(
                vehicleConfigurationId,
                period, uid, state, mileage, changeTypeBitFlags,
                Insert().Id))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    return serializer.Deserialize((IDataRecord)reader);
                }
            }

            return null;
        }

        public Row Update(int vehicleConfigurationId, int period, int uid, string state, int? mileage, int changeTypeBitFlags, AuditRow auditRow)
        {
            INadaDatastore datastore = Resolve<INadaDatastore>();

            using (IDataReader reader = datastore.VehicleConfiguration_History_Insert(
                vehicleConfigurationId,
                period, uid, state, mileage, changeTypeBitFlags,
                Update(auditRow).Id))
            {
                if (reader.Read())
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    return serializer.Deserialize((IDataRecord)reader);
                }
            }

            return null;
        }
    }
}
