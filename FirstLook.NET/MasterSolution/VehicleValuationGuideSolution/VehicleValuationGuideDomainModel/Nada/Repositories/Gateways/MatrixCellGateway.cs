﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Datastores;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Gateways
{
    public class MatrixCellGateway : GatewayBase
    {
        [Serializable]
        public class Row
        {
            public int MatrixId { get; set; }

            public int Period { get; set; }

            public int ValueTypeId { get; set; }

            public byte ValuationId { get; set; }

            public bool Visible { get; set; }

            public decimal? Value { get; set; }
        }

        protected class RowSerializer : Serializer<Row>
        {
            public override Row Deserialize(IDataRecord record)
            {
                int matrixId = record.GetInt32(record.GetOrdinal("MatrixId"));

                int period = record.GetInt32(record.GetOrdinal("Period"));

                int valueTypeId = record.GetInt32(record.GetOrdinal("ValueTypeId"));

                byte valuationId = record.GetByte(record.GetOrdinal("ValuationId"));

                bool visible = record.GetBoolean(record.GetOrdinal("Visible"));

                decimal? value = null;

                int ordinal = record.GetOrdinal("Value");

                if (!record.IsDBNull(ordinal))
                {
                    value = record.GetDecimal(ordinal);
                }
                
                return new Row
                {
                    MatrixId = matrixId,
                    Period = period,
                    ValueTypeId = valueTypeId,
                    ValuationId = valuationId,
                    Visible = visible,
                    Value = value
                };
            }
        }

        public IList<Row> Fetch(int period, int matrixId)
        {
            string key = CreateCacheKey(period, matrixId);

            IList<Row> rows = Cache.Get(key) as IList<Row>;

            if (rows == null)
            {
                INadaDatastore datastore = Resolve<INadaDatastore>();

                using (IDataReader reader = datastore.MatrixCell_Fetch(period, matrixId))
                {
                    ISerializer<Row> serializer = new RowSerializer();

                    rows = serializer.Deserialize(reader);

                    Remember(key, rows);
                }
            }

            return rows;
        }

        public void Insert(int period, int matrixId, int valueTypeId, byte valuationId, bool visible, decimal? value)
        {
            INadaDatastore datastore = Resolve<INadaDatastore>();

            datastore.MatrixCell_Insert(period, matrixId, valueTypeId, valuationId, visible, value);
        }
    }
}
