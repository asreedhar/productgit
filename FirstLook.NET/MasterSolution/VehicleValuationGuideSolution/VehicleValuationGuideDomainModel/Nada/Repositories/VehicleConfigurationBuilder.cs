﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Entities;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories
{
    public class VehicleConfigurationBuilder : IVehicleConfigurationBuilder
    {
        public VehicleConfigurationBuilder()
        {
            
        }

        public VehicleConfigurationBuilder(IVehicleConfiguration configuration)
        {
            Period = configuration.Period;
            Uid = configuration.Uid;
            Vin = configuration.Vin;
            State = configuration.State;
            Mileage = configuration.Mileage;

            foreach (IAccessoryAction action in configuration.AccessoryActions)
            {
                _actions.Add(Clone(action));
            }

            foreach (IAccessoryState state in configuration.AccessoryStates)
            {
                _states.Add(Clone(state));
            }
        }

        private Period _period;
        private State _state;
        private int _uid;
        private string _vin;
        private int? _mileage;
        
        public Period Period
        {
            get { return _period; }
            set { _period = value; }
        }

        public int Uid
        {
            get { return _uid; }
            set { _uid = value; }
        }

        public string Vin
        {
            get { return _vin; }
            set { _vin = value; }
        }

        public State State
        {
            get { return _state; }
            set { _state = value; }
        }

        public int? Mileage
        {
            get { return _mileage; }
            set { _mileage = value; }
        }
        
        private readonly List<AccessoryAction> _actions = new List<AccessoryAction>();
        private readonly List<AccessoryState> _states = new List<AccessoryState>();

        public IAccessoryState Add(Accessory accessory)
        {
            if (accessory == null)
            {
                throw new ArgumentNullException("accessory");
            }

            AccessoryState state = _states.FirstOrDefault(x => Equals(x.AccessoryCode, accessory.Code));

            if (state == null)
            {
                state = new AccessoryState
                {
                    AccessoryCode = accessory.Code,
                    Enabled = !(accessory.IsAdded || accessory.IsIncluded),
                    Selected = accessory.IsAdded || accessory.IsIncluded
                };

                _states.Add(state);
            }

            return state;
        }

        public IAccessoryState Add(string accessoryCode, bool enabled, bool selected)
        {
            VerifyAccessoryCode(accessoryCode);

            AccessoryState state = _states.FirstOrDefault(x => Equals(x.AccessoryCode, accessoryCode));

            if (state == null)
            {
                state = new AccessoryState
                {
                    AccessoryCode = accessoryCode,
                    Enabled = enabled,
                    Selected = selected
                };

                _states.Add(state);
            }

            return state;
        }
        
        public void Disable(string accessoryCode)
        {
            VerifyAccessoryCode(accessoryCode);

            AccessoryState state = _states.FirstOrDefault(x => Equals(x.AccessoryCode, accessoryCode));

            if (state != null)
            {
                state.Enabled = false;
            }
        }

        public void Enable(string accessoryCode)
        {
            VerifyAccessoryCode(accessoryCode);

            AccessoryState state = _states.FirstOrDefault(x => Equals(x.AccessoryCode, accessoryCode));

            if (state != null)
            {
                state.Enabled = true;
            }
        }

        public bool Enabled(string accessoryCode)
        {
            VerifyAccessoryCode(accessoryCode);

            AccessoryState state = _states.FirstOrDefault(x => Equals(x.AccessoryCode, accessoryCode));

            if (state != null)
            {
                return state.Enabled;
            }

            return false;
        }

        public void Select(string accessoryCode)
        {
            VerifyAccessoryCode(accessoryCode);

            AccessoryState state = _states.FirstOrDefault(x => Equals(x.AccessoryCode, accessoryCode));

            if (state != null)
            {
                state.Selected = true;
            }
        }

        public void Deselect(string accessoryCode)
        {
            VerifyAccessoryCode(accessoryCode);

            AccessoryState state = _states.FirstOrDefault(x => Equals(x.AccessoryCode, accessoryCode));

            if (state != null)
            {
                state.Selected = false;
            }
        }

        public IEnumerable<string> Selected()
        {
            return _states.Where(x => x.Selected).Select(y => y.AccessoryCode);
        }

        public bool IsSelected(string accessoryCode, bool returnValue)
        {
            VerifyAccessoryCode(accessoryCode);

            AccessoryState state = _states.FirstOrDefault(x => Equals(x.AccessoryCode, accessoryCode));

            if (state != null)
            {
                return state.Selected;
            }

            return returnValue;
        }

        public IAccessoryAction Do(string accessoryCode, AccessoryActionType actionType)
        {
            VerifyAccessoryCode(accessoryCode);

            AccessoryAction action = new AccessoryAction
            {
                AccessoryCode = accessoryCode,
                ActionType = actionType
            };

            _actions.Add(action);

            return action;
        }

        public IAccessoryAction Undo(string accessoryCode)
        {
            VerifyAccessoryCode(accessoryCode);

            AccessoryAction action = _actions.FirstOrDefault(x => Equals(x.AccessoryCode, accessoryCode));

            if (action != null)
            {
                _actions.Remove(action);
            }

            return action;
        }

        public IVehicleConfiguration ToVehicleConfiguration()
        {
            VehicleConfiguration configuration = new VehicleConfiguration
            {
                Period = Period,
                Uid = Uid,
                Vin = Vin,
                State = State,
                Mileage = Mileage
            };

            foreach (AccessoryAction action in _actions)
            {
                configuration.AccessoryActions.Add(Clone(action));
            }

            foreach (AccessoryState state in _states)
            {
                configuration.AccessoryStates.Add(Clone(state));
            }

            return configuration;
        }

        private static void VerifyAccessoryCode(string accessoryCode)
        {
            if (string.IsNullOrEmpty(accessoryCode))
            {
                throw new ArgumentNullException("accessoryCode");
            }
        }

        private static AccessoryAction Clone(IAccessoryAction action)
        {
            return new AccessoryAction { ActionType = action.ActionType, AccessoryCode = action.AccessoryCode };
        }

        private static AccessoryState Clone(IAccessoryState state)
        {
            return new AccessoryState { AccessoryCode = state.AccessoryCode, Enabled = state.Enabled, Selected = state.Selected };
        }
    }
}
