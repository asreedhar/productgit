﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Entities
{
    [Serializable]
    public class AccessoryAction : IAccessoryAction
    {
        private string _accessoryCode;
        private AccessoryActionType _actionType;

        public string AccessoryCode
        {
            get { return _accessoryCode; }
            internal set { _accessoryCode = value; }
        }

        public AccessoryActionType ActionType
        {
            get { return _actionType; }
            internal set { _actionType = value; }
        }
    }
}
