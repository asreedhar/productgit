﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Entities
{
    [Serializable]
    public class Publication : PublicationInfo, IPublication
    {
        public IVehicleConfiguration VehicleConfiguration { get; internal set; }

        public Matrix Matrix { get; internal set; }

        public INadaService Service { get; internal set; }
    }
}
