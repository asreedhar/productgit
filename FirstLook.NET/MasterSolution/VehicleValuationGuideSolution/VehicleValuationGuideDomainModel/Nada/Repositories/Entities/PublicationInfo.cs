﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Entities
{
    [Serializable]
    public class PublicationInfo : IPublicationInfo
    {
        private ChangeAgent _changeAgent;
        private ChangeType _changeType;
        private int _id;

        public ChangeAgent ChangeAgent
        {
            get { return _changeAgent; }
            internal set { _changeAgent = value; }
        }

        public ChangeType ChangeType
        {
            get { return _changeType; }
            internal set { _changeType = value; }
        }

        public int Id
        {
            get { return _id; }
            internal set { _id = value; }
        }
    }
}
