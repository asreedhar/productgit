﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Entities
{
    [Serializable]
    public class VehicleConfiguration : IVehicleConfiguration
    {
        private Period _period;
        private State _state;
        private string _vin;
        private int _uid;
        private int? _mileage;
        
        public Period Period
        {
            get { return _period; }
            internal set { _period = value; }
        }
        
        public string Vin
        {
            get { return _vin; }
            internal set { _vin = value; }
        }

        public int Uid
        {
            get { return _uid; }
            internal set { _uid = value; }
        }

        public State State
        {
            get { return _state; }
            internal set { _state = value; }
        }

        public int? Mileage
        {
            get { return _mileage; }
            internal set { _mileage = value; }
        }

        private readonly IList<IAccessoryAction> _accessoryActions = new List<IAccessoryAction>();
        private readonly IList<IAccessoryState> _accessoryStates = new List<IAccessoryState>();

        public IList<IAccessoryAction> AccessoryActions
        {
            get { return _accessoryActions; }
        }

        public IList<IAccessoryState> AccessoryStates
        {
            get { return _accessoryStates; }
        }

        #region IVehicleConfiguration Members

        public ChangeType Compare(IVehicleConfiguration configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            ChangeType flags = ChangeType.None;

            if (configuration.Period.Id != Period.Id) flags |= ChangeType.Period;

            if (configuration.State.Code != State.Code) flags |= ChangeType.State;

            if (configuration.Uid != Uid) flags |= ChangeType.Uid;

            if (Nullable.Compare(configuration.Mileage, Mileage) != 0) flags |= ChangeType.Mileage;
            
            if (configuration.AccessoryActions.Count != AccessoryActions.Count)
            {
                flags |= ChangeType.AccessoryActions;
            }
            else
            {
                bool identical = configuration.AccessoryActions.All(
                    a => AccessoryActions.Any(
                             b => Equals(a.AccessoryCode, b.AccessoryCode) &&
                                  a.ActionType == b.ActionType));
                
                if (!identical)
                {
                    flags |= ChangeType.AccessoryActions;
                }
            }

            if (configuration.AccessoryStates.Count != AccessoryStates.Count)
            {
                flags |= ChangeType.AccessoryStates;
            }
            else
            {
                bool identical = configuration.AccessoryStates.All(
                    a => AccessoryStates.Any(
                             b => Equals(a.AccessoryCode, b.AccessoryCode) &&
                                  a.Enabled == b.Enabled &&
                                  a.Selected == b.Selected));

                if (!identical)
                {
                    flags |= ChangeType.AccessoryStates;
                }
            }

            return flags;
        }

        #endregion
    }
}
