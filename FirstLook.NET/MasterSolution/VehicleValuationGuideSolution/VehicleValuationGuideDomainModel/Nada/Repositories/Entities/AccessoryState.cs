﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Entities
{
    [Serializable]
    public class AccessoryState : IAccessoryState
    {
        private string _accessoryCode;
        private bool _selected;
        private bool _enabled;

        public string AccessoryCode
        {
            get { return _accessoryCode; }
            internal set { _accessoryCode = value; }
        }

        public bool Enabled
        {
            get { return _enabled; }
            internal set { _enabled = value; }
        }

        public bool Selected
        {
            get { return _selected; }
            internal set { _selected = value; }
        }
    }
}
