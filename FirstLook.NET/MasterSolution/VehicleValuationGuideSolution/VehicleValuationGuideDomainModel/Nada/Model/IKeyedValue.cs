﻿using System;
using System.Globalization;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model
{
    public interface IKeyedValue
    {
        int Id { get; }

        string Name { get; }
    }

    [Serializable]
    public abstract class KeyedValue : IKeyedValue
    {
        private int _id;
        private string _name;

        public virtual int Id
        {
            get { return _id; }
            internal set { _id = value; }
        }

        public virtual string Name
        {
            get { return _name; }
            internal set { _name = value; }
        }
    }

    [Serializable]
    public class Period : KeyedValue
    {
        private int _year;
        private int _month;

        public override int Id
        {
            internal set
            {
                base.Id = value;

                _year = base.Id / 100;

                _month = base.Id - (_year * 100);
            }
        }

        public override string Name
        {
            get
            {
                DateTime monthYear = new DateTime(_year, _month, 1);

                return string.Format(
                    CultureInfo.InvariantCulture,
                    "{0} {1}",
                    monthYear.ToString("MMMM"), monthYear.Year);
            }
        }
    }

    [Serializable]
    public class Year : KeyedValue
    {
        
    }

    [Serializable]
    public class Make : KeyedValue
    {

    }

    [Serializable]
    public class Series : KeyedValue
    {

    }

    [Serializable]
    public class Body : KeyedValue
    {
        private int _uid;

        public int Uid
        {
            get { return _uid; }
            internal set { _uid = value; }
        }
    }
}
