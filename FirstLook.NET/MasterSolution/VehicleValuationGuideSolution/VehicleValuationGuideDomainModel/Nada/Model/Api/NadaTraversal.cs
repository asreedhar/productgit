﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Lookup;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api
{
    public class NadaTraversal : INadaTraversal
    {
        protected const string PairSeparator = "||", ValueSeparator = "=";

        public ITree CreateTree()
        {
            return new Tree
            {
                Root = new InitialNode()
            };
        }

        public ITree CreateTree(string vin)
        {
            return new Tree
            {
                Root = new VinNode(vin)
            };
        }

        protected static T Resolve<T>()
        {
            return RegistryFactory.GetResolver().Resolve<T>();
        }

        sealed class Tree : ITree
        {
            private ITreeNode _root;

            public ITreeNode Root
            {
                get { return _root; }
                set { _root = value; }
            }

            public ITreeNode GetNodeByPath(ITreePath path)
            {
                if (path != null)
                {
                    if (!string.IsNullOrEmpty(path.State))
                    {
                        int yearId = 0, makeId = 0, seriesId = 0, bodyId = 0;

                        string yearName = null, makeName = null, seriesName = null, bodyName = null;

                        string vin = null;

                        int uid = 0;

                        string[] values = path.State.Split(new[] { PairSeparator }, StringSplitOptions.None);

                        foreach (var value in values)
                        {
                            string[] pair = value.Split(new[] { ValueSeparator }, StringSplitOptions.None);

                            string k = pair[0], v = pair[1];

                            string[] fields = v.Split(new[] { ',' });

                            string id = fields[0], name = fields.Length > 1 ? fields[1] : string.Empty;

                            switch (k)
                            {
                                case "year":
                                    yearId = int.Parse(id);
                                    yearName = name;
                                    break;
                                case "make":
                                    makeId = int.Parse(id);
                                    makeName = name;
                                    break;
                                case "series":
                                    seriesId = int.Parse(id);
                                    seriesName = name;
                                    break;
                                case "body":
                                    bodyId = int.Parse(id);
                                    bodyName = name;
                                    break;
                                case "vin":
                                    vin = id;
                                    break;
                                case "uid":
                                    uid = int.Parse(id);
                                    break;
                            }
                        }

                        if (yearId == 0)
                        {
                            if (string.IsNullOrEmpty(vin))
                            {
                                return new InitialNode();
                            }

                            return new VinNode(vin);
                        }

                        YearNode yearNode = new YearNode(yearId, yearName);

                        if (makeId == 0)
                        {
                            return yearNode;
                        }

                        MakeNode makeNode = new MakeNode(yearNode, makeId, makeName);

                        if (seriesId == 0)
                        {
                            return makeNode;
                        }

                        SeriesNode seriesNode = new SeriesNode(makeNode, seriesId, seriesName);

                        if (bodyId == 0)
                        {
                            return seriesNode;
                        }

                        return new BodyNode(seriesNode, bodyId, bodyName, uid);
                    }
                }

                return Root;
            }
        }

        sealed class InitialNode : TreeNode
        {
            private IList<ITreeNode> _children;

            public override string Label
            {
                get { return ""; }
            }

            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        _children = new List<ITreeNode>();

                        foreach (Year year in Resolve<ILookupYear>().Lookup(CurrentPeriod()))
                        {
                            _children.Add(new YearNode(year.Id, year.Name));
                        }
                    }

                    return _children;
                }
            }

            public override void Save(ITreePath path)
            {
                path.State = string.Empty;
            }
        }

        static string Serialize(string key, ITreeNode node)
        {
            return key + ValueSeparator + node.Id + "," + node.Name;
        }

        sealed class YearNode : TreeNode
        {
            public YearNode(ITreeNode initial, int id, string name)
                : base(initial, id, name)
            {
            }

            public YearNode(int id, string name)
                : base(id, name)
            {
            }

            public override string Label
            {
                get { return "Year"; }
            }

            private IList<ITreeNode> _children;

            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        _children = new List<ITreeNode>();

                        foreach (Make make in Resolve<ILookupMake>().Lookup(CurrentPeriod(), Id))
                        {
                            _children.Add(new MakeNode(this, make.Id, make.Name));
                        }
                    }

                    return _children;
                }
            }

            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = Serialize("year", this);
                }
            }
        }

        sealed class MakeNode : TreeNode
        {
            public MakeNode(ITreeNode year, int id, string name)
                : base(year, id, name)
            {
            }

            public override string Label
            {
                get { return "Make"; }
            }

            private IList<ITreeNode> _children;

            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        _children = new List<ITreeNode>();

                        foreach (Series series in Resolve<ILookupSeries>().Lookup(CurrentPeriod(), Parent.Id, Id))
                        {
                            _children.Add(new SeriesNode(this, series.Id, series.Name));
                        }
                    }

                    return _children;
                }
            }

            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = Serialize("year", Parent) + PairSeparator + Serialize("make", this);
                }
            }
        }

        sealed class SeriesNode : TreeNode
        {
            public SeriesNode(ITreeNode make, int id, string name)
                : base(make, id, name)
            {
            }

            public override string Label
            {
                get { return "Series"; }
            }

            private IList<ITreeNode> _children;

            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        _children = new List<ITreeNode>();

                        foreach (Body body in Resolve<ILookupBody>().Lookup(CurrentPeriod(), Parent.Parent.Id, Parent.Id, Id))
                        {
                            _children.Add(new BodyNode(this, body.Id, body.Name, body.Uid));
                        }
                    }

                    return _children;
                }
            }

            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = Serialize("year", Parent.Parent)
                        + PairSeparator + Serialize("make", Parent)
                        + PairSeparator + Serialize("series", this);
                }
            }
        }

        sealed class BodyNode : TreeNode
        {
            public BodyNode(ITreeNode series, int id, string name, int uid)
                : base(series, id, name, uid)
            {
            }

            public override string Label
            {
                get { return "Body"; }
            }

            private readonly IList<ITreeNode> _children = new List<ITreeNode>(0);

            public override IList<ITreeNode> Children
            {
                get
                {
                    return _children;
                }
            }

            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = Serialize("year", Parent.Parent.Parent)
                        + PairSeparator + Serialize("make", Parent.Parent)
                        + PairSeparator + Serialize("series", Parent)
                        + PairSeparator + Serialize("body", this)
                        + PairSeparator + "uid" + ValueSeparator + Uid + ",,";
                }
            }
        }

        sealed class VinNode : TreeNode
        {
            private readonly string _vin;

            public VinNode(string vin)
            {
                _vin = vin;
            }

            public override string Label
            {
                get { return "Vin"; }
            }

            private IList<ITreeNode> _children;

            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        _children = new List<ITreeNode>();

                        IList<Vehicle> vehicles = Resolve<ILookupVin>().Lookup(CurrentPeriod(), _vin);

                        switch (Vary.By(vehicles))
                        {
                            case VaryBy.Make:
                                Make(vehicles);
                                break;
                            case VaryBy.Series:
                                Model(vehicles);
                                break;
                            case VaryBy.Body:
                                Body(vehicles);
                                break;
                            default:
                                Body(vehicles);
                                break;
                        }
                    }

                    return _children;
                }
            }

            private void Make(IList<Vehicle> infos)
            {
                for (int i = 0, l = infos.Count; i < l; i++)
                {
                    Vehicle info = infos[i];

                    IKeyedValue year = info.Year,
                                make = info.Make,
                                series = info.Series,
                                body = info.Body;

                    if (i == 0)
                    {
                        ITreeNode initialNode = new InitialNode(),
                                  yearNode = new YearNode(initialNode, year.Id, year.Name);

                        _parent = yearNode;
                    }

                    ITreeNode makeNode = new MakeNode(_parent, make.Id, make.Name),
                              seriesNode = new SeriesNode(makeNode, series.Id, series.Name),
                              bodyNode = new BodyNode(seriesNode, body.Id, body.Name, info.Body.Uid);

                    _children.Add(bodyNode);
                }
            }

            private void Model(IList<Vehicle> infos)
            {
                for (int i = 0, l = infos.Count; i < l; i++)
                {
                    Vehicle info = infos[i];

                    IKeyedValue year = info.Year,
                                make = info.Make,
                                series = info.Series,
                                body = info.Body;

                    if (i == 0)
                    {
                        ITreeNode initialNode = new InitialNode(),
                                  yearNode = new YearNode(initialNode, year.Id, year.Name),
                                  makeNode = new MakeNode(yearNode, make.Id, make.Name);

                        _parent = makeNode;
                    }

                    ITreeNode seriesNode = new SeriesNode(_parent, series.Id, series.Name),
                              bodyNode = new BodyNode(seriesNode, body.Id, body.Name, info.Body.Uid);

                    _children.Add(bodyNode);
                }
            }

            private void Body(IList<Vehicle> infos)
            {
                for (int i = 0, l = infos.Count; i < l; i++)
                {
                    Vehicle info = infos[i];

                    IKeyedValue year = info.Year,
                                make = info.Make,
                                series = info.Series,
                                body = info.Body;

                    if (i == 0)
                    {
                        ITreeNode initialNode = new InitialNode(),
                                  yearNode = new YearNode(initialNode, year.Id, year.Name),
                                  makeNode = new MakeNode(yearNode, make.Id, make.Name),
                                  seriesNode = new SeriesNode(makeNode, series.Id, series.Name);

                        _parent = seriesNode;
                    }

                    ITreeNode bodyNode = new BodyNode(_parent, body.Id, body.Name, info.Body.Uid);

                    _children.Add(bodyNode);
                }
            }

            public override int? Uid
            {
                get
                {
                    if (Children.Count == 1)
                    {
                        return Children[0].Uid;
                    }

                    return base.Uid;
                }
            }

            private ITreeNode _parent;

            public override ITreeNode Parent
            {
                get
                {
                    IList<ITreeNode> eek = Children; // make sure we have parent set

                    return _parent;
                }
            }

            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = "vin" + ValueSeparator + _vin + ",,";
                }
            }
        }
    }

    public abstract class TreeNode : ITreeNode
    {
        private readonly ITreeNode _parent;
        private int _id;
        private string _name;
        private int? _uid;

        protected TreeNode()
        {
        }

        protected TreeNode(int id, string name)
        {
            _id = id;
            _name = name;
        }

        protected TreeNode(ITreeNode parent, int id, string name)
        {
            _parent = parent;
            _id = id;
            _name = name;
        }

        protected TreeNode(ITreeNode parent, int id, string name, int? uid)
        {
            _parent = parent;
            _id = id;
            _name = name;
            _uid = uid;
        }

        public abstract string Label { get; }

        public virtual int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public virtual string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public virtual int? Uid
        {
            get { return _uid; }
            set { _uid = value; }
        }

        public virtual ITreeNode Parent
        {
            get
            {
                return _parent;
            }
        }

        public abstract IList<ITreeNode> Children { get; }

        public virtual bool HasChildren
        {
            get
            {
                return Children.Count == 0;
            }
        }

        public abstract void Save(ITreePath path);

        protected int CurrentPeriod()
        {
            IResolver resolver = RegistryFactory.GetResolver();

            ILookupPeriod lookup = resolver.Resolve<ILookupPeriod>();

            return lookup.Lookup().Max(x => x.Id);
        }
    }

    public class TreePath : ITreePath
    {
        private string _state;

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }
    }
}
