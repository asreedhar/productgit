﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<INadaCalculator, NadaCalculator>(ImplementationScope.Shared);

            registry.Register<INadaConfiguration, NadaConfiguration>(ImplementationScope.Shared);

            registry.Register<INadaService, NadaService>(ImplementationScope.Shared);

            registry.Register<INadaTraversal, NadaTraversal>(ImplementationScope.Shared);
        }
    }
}
