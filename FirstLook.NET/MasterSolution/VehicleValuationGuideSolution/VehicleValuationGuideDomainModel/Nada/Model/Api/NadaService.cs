﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Lookup;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api
{
    public delegate int NadaServicePeriod();

    [Serializable]
    public class NadaService : INadaService
    {
        private readonly IResolver _resolver;

        private readonly NadaServicePeriod _period;

        public NadaService(IResolver resolver, NadaServicePeriod period)
        {
            _resolver = resolver;

            _period = period;
        }

        public NadaService()
        {
            _resolver = RegistryFactory.GetResolver();

            _period = () => _resolver.Resolve<ILookupPeriod>().Lookup().Max(x => x.Id);
        }

        public IList<State> States()
        {
            ILookupState region = Resolve<ILookupState>();

            return region.Lookup(Period().Id);
        }

        public Period Period()
        {
            return new Period {Id = _period()};
        }

        public Region ToRegion(State state)
        {
            ILookupRegion region = Resolve<ILookupRegion>();

            return region.Lookup(GetPeriodId(), state.Name);
        }

        public Vehicle Information(int uid)
        {
            ILookupVehicle vehicle = Resolve<ILookupVehicle>();

            return vehicle.Lookup(GetPeriodId(), uid);
        }

        public IList<Accessory> Accessories(int uid, Region region)
        {
            ILookupAccessory accessoryLookup = Resolve<ILookupAccessory>();

            return accessoryLookup.Lookup(GetPeriodId(), uid, region);
        }

        public IList<Accessory> Accessories(int uid, Region region, string vin)
        {
            int periodId = GetPeriodId();

            ILookupVinAccessory vinAccessoryLookup = Resolve<ILookupVinAccessory>();

            return vinAccessoryLookup.Lookup(periodId, uid, region, vin);
        }

        public IList<AccessoryRelationship> AccessoryRelationships(int uid)
        {
            ILookupAccessoryRelationship lookup = Resolve<ILookupAccessoryRelationship>();

            return lookup.Lookup(GetPeriodId(), uid);
        }

        public MileageAdjustment MileageAdjustment(int uid, Region region, int mileage)
        {
            ILookupMileageAdjustment lookup = Resolve<ILookupMileageAdjustment>();

            return lookup.Lookup(GetPeriodId(), uid, region, mileage);
        }

        public IList<Value> BaseValues(int uid, Region region)
        {
            ILookupVehicleValue lookup = Resolve<ILookupVehicleValue>();

            return lookup.Lookup(GetPeriodId(), uid, region);
        }

        private int GetPeriodId()
        {
            return _period();
        }

        private T Resolve<T>()
        {
            return _resolver.Resolve<T>();
        }
    }
}
