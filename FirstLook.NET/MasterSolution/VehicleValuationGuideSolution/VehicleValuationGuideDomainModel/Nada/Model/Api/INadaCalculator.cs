﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api
{
    public interface INadaCalculator
    {
        Matrix Calculate(INadaService service, IVehicleConfiguration configuration);
    }

    [Serializable]
    public enum Valuation
    {
        Undefined,
        Base,
        Option,
        Mileage,
        Final
    }

    public class Matrix
    {
        public class Cell
        {
            private bool _visible = true;

            private decimal? _value;

            internal Cell()
            {
            }

            public bool Visible
            {
                get { return _visible; }
                internal set { _visible = value; }
            }

            public decimal? Value
            {
                get { return _value; }
                internal set { _value = value; }
            }
        }

        private static readonly int ValueTypeCount = Enum.GetNames(typeof(ValueType)).Length;

        private static readonly int ValuationCount = Enum.GetNames(typeof(Valuation)).Length;

        private readonly Cell[] _values;

        public Matrix()
        {
            int arrayLength = ValueTypeCount * ValuationCount;

            _values = new Cell[arrayLength];

            foreach (ValueType priceType in Enum.GetValues(typeof(ValueType)))
            {
                foreach (Valuation valuation in Enum.GetValues(typeof(Valuation)))
                {
                    Cell cell = new Cell();

                    if (priceType == ValueType.Undefined ||
                        valuation == Valuation.Undefined)
                    {
                        cell.Visible = false;
                    }

                    this[priceType, valuation] = cell;
                }
            }
        }


        public Cell this[ValueType priceType, Valuation valuation]
        {
            get
            {
                return _values[Index(priceType, valuation)];
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentException("A Matrix Cell should never be set to null!");
                }

                _values[Index(priceType, valuation)] = value;
            }
        }

        private static int Index(ValueType valueType, Valuation valuation)
        {
            int rowLength = ValuationCount; // = number of columns

            int row = (int) valueType;

            int col = ((int)valuation);

            return row * rowLength + col; // = (row * number of columns) + col
        }
    }
}
