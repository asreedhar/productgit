﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api
{
    public interface INadaConfiguration
    {
        IVehicleConfiguration InitialConfiguration(int uid, string vin, State state, int? mileage);

        IVehicleConfiguration UpdateConfiguration(IVehicleConfiguration configuration, IAccessoryAction action);
    }

    public interface IVehicleConfigurationBuilderFactory
    {
        IVehicleConfigurationBuilder NewBuilder();

        IVehicleConfigurationBuilder NewBuilder(IVehicleConfiguration configuration);
    }

    public interface IVehicleConfigurationBuilder
    {
        Period Period { get; set; }

        State State { get; set; }

        int Uid { get; set; }

        string Vin { get; set; }

        int? Mileage { get; set; }

        IAccessoryState Add(Accessory accessory);

        IAccessoryState Add(string accessoryCode, bool enabled, bool selected);

        void Disable(string accessoryCode);

        void Enable(string accessoryCode);

        bool Enabled(string accessoryCode);

        void Select(string accessoryCode);

        void Deselect(string accessoryCode);

        IEnumerable<string> Selected();

        bool IsSelected(string accessoryCode, bool returnValue);

        IAccessoryAction Do(string accessoryCode, AccessoryActionType actionType);

        IAccessoryAction Undo(string accessoryCode);

        IVehicleConfiguration ToVehicleConfiguration();
    }

    public interface IVehicleConfiguration
    {
        Period Period { get; }

        State State { get; }

        int Uid { get; }

        string Vin { get; }

        int? Mileage { get; }

        IList<IAccessoryAction> AccessoryActions { get; }

        IList<IAccessoryState> AccessoryStates { get; }

        ChangeType Compare(IVehicleConfiguration configuration);
    }

    public interface IAccessoryState
    {
        string AccessoryCode { get; }

        bool Selected { get; }

        bool Enabled { get; }
    }

    [Serializable]
    public enum AccessoryActionType
    {
        Undefined,
        Add,
        Remove
    }

    public interface IAccessoryAction
    {
        string AccessoryCode { get; }

        AccessoryActionType ActionType { get; }
    }
}
