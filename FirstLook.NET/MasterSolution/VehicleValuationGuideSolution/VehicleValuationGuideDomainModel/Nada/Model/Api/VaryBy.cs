﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api
{
    [Serializable]
    internal enum VaryBy
    {
        None,
        Make,
        Series,
        Body
    }
}