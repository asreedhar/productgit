﻿using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api
{
    public interface INadaService
    {
        IList<State> States();

        Period Period();

        Region ToRegion(State state);

        Vehicle Information(int uid);

        IList<Accessory> Accessories(int uid, Region region);

        IList<Accessory> Accessories(int uid, Region region, string vin);

        IList<AccessoryRelationship> AccessoryRelationships(int uid);

        MileageAdjustment MileageAdjustment(int uid, Region region, int mileage);

        IList<Value> BaseValues(int uid, Region region);
    }
}
