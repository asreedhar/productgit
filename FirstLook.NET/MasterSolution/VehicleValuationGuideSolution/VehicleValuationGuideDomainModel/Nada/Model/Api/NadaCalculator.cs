﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api
{
    public class NadaCalculator : INadaCalculator
    {
        public Matrix Calculate(INadaService service, IVehicleConfiguration configuration)
        {
            Vehicle vehicle = service.Information(configuration.Uid);

            Region region = service.ToRegion(configuration.State);

            return Calculate(service, vehicle, configuration, region);
        }

        static Matrix Calculate(INadaService service, Vehicle vehicle, IVehicleConfiguration configuration, Region region)
        {
            Matrix matrix = new Matrix();

            CalculateBase(service, vehicle, region, matrix);

            CalculateOptions(service, vehicle, configuration, region, matrix);

            CalculateMileage(service, vehicle, configuration, region, matrix);

            CalculateFinal(matrix);

            return matrix;
        }

        private static void CalculateBase(INadaService service, Vehicle vehicle, Region region, Matrix matrix)
        {
            foreach (Value value in service.BaseValues(vehicle.Body.Uid, region))
            {
                matrix[value.ValueType, Valuation.Base].Value = value.ValueAmount;
            }
        }

        private static void CalculateOptions(INadaService service, Vehicle vehicle, IVehicleConfiguration configuration, Region region, Matrix matrix)
        {
            IList<Accessory> accessories = service.Accessories(
                vehicle.Body.Uid,
                region);

            IList<AccessoryRelationship> relationships = service.AccessoryRelationships(
                vehicle.Body.Uid);

            // verify excludes are respected

            IEnumerable<AccessoryRelationship> excludes = relationships.Where(
                x => x.RelationshipType == AccessoryRelationshipType.Excluded &&
                     configuration.AccessoryStates.First(
                         y => Equals(y.AccessoryCode, x.AccessoryCode)).Selected);

            bool excludeViolations = excludes.Any(
                x => x.RelatedAccessories.Any(
                         y => configuration.AccessoryStates.Any(
                                  z => z.Selected && Equals(y.Code, z.AccessoryCode))));

            if (excludeViolations)
            {
                bool ignoreViolation = excludes.All(
                    x => x.RelatedAccessories.All(
                             y => configuration.AccessoryStates.Any(
                                      z => Equals(y.Code, z.AccessoryCode) &&
                                           z.Selected &&
                                           !z.Enabled
                                      )));

                if (!ignoreViolation)
                {
                    throw new ArgumentException("Invalid Configuration (Excludes)", "configuration");
                }
            }

            // verify includes are respected

            IEnumerable<AccessoryRelationship> includes = relationships.Where(
                x => x.RelationshipType == AccessoryRelationshipType.Included &&
                     configuration.AccessoryStates.First(
                         y => Equals(y.AccessoryCode, x.AccessoryCode)).Selected);

            bool includeViolations = includes.Any(
                x => x.RelatedAccessories.Any(
                         y => configuration.AccessoryStates.Any(
                                  z => !z.Selected && Equals(y.Code, z.AccessoryCode))));

            if (includeViolations)
            {
                throw new ArgumentException("Invalid Configuration (Include)", "configuration");
            }

            // get the billable accessories (selected but not part of an include)

            IEnumerable<IAccessoryState> states = configuration.AccessoryStates.Where(
                x => x.Selected &&
                     !includes.Any(
                          y => y.RelatedAccessories.Any(
                                   z => Equals(x.AccessoryCode, z.Code))));

            IEnumerable<Accessory> billable = accessories.Where(
                x => states.Any(
                         y => Equals(x.Code, y.AccessoryCode)));

            foreach (Accessory accessory in billable)
            {
                foreach (Value value in accessory.Values)
                {
                    decimal? oldTotal = matrix[value.ValueType, Valuation.Option].Value;

                    decimal? newTotal = Sum(oldTotal, value.ValueAmount);

                    matrix[value.ValueType, Valuation.Option].Value = newTotal;

                    if (value.ValueType == ValueType.Trade)
                    {
                        matrix[ValueType.TradeAverage, Valuation.Option].Value = newTotal;

                        matrix[ValueType.TradeRough, Valuation.Option].Value = newTotal;
                    }
                }
            }
        }

        private static void CalculateMileage(INadaService service, Vehicle vehicle, IVehicleConfiguration configuration, Region region, Matrix matrix)
        {
            if (configuration.Mileage.HasValue)
            {
                int mileage = configuration.Mileage.Value;

                MileageAdjustment adjustment = service.MileageAdjustment(
                    vehicle.Body.Uid,
                    region,
                    mileage);

                foreach (ValueType valueType in Enum.GetValues(typeof(ValueType)))
                {
                    matrix[valueType, Valuation.Mileage].Value = adjustment.Value;
                }
            }
        }

        private static void CalculateFinal(Matrix matrix)
        {
            foreach (ValueType valueType in Enum.GetValues(typeof(ValueType)))
            {
                if (valueType == ValueType.Undefined)
                {
                    continue;
                }

                decimal? basePrice = matrix[valueType, Valuation.Base].Value;

                if (!basePrice.HasValue)
                {
                    continue;
                }

                decimal? finalPrice = 0;

                if (basePrice > 0)
                {
                    decimal? optionAdjustment = matrix[valueType, Valuation.Option].Value;

                    decimal? mileageAdjustment = matrix[valueType, Valuation.Mileage].Value;

                    finalPrice = Sum(Sum(basePrice, optionAdjustment), mileageAdjustment);
                }

                matrix[valueType, Valuation.Final].Value = finalPrice;
            }
        }

        static decimal? Sum(decimal? a, decimal? b)
        {
            if (a.HasValue)
            {
                if (b.HasValue)
                {
                    return a.Value + b.Value;
                }

                return a.Value;
            }

            if (b.HasValue)
            {
                return b.Value;
            }

            return null;
        }
    }
}
