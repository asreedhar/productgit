﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api
{
    public class NadaConfiguration : INadaConfiguration
    {
        public IVehicleConfiguration InitialConfiguration(int uid, string vin, State state, int? mileage)
        {
            INadaService service = Resolve<INadaService>();

            IVehicleConfigurationBuilderFactory factory = Resolve<IVehicleConfigurationBuilderFactory>();

            IVehicleConfigurationBuilder builder = factory.NewBuilder();

            builder.Period = service.Period();
            builder.Uid = uid;
            builder.Vin = vin;
            builder.State = state;
            builder.Mileage = mileage;

            Vehicle vehicle = service.Information(uid);

            Region region = service.ToRegion(state);

            InitializeConfiguration(service, vehicle, builder, region);

            return builder.ToVehicleConfiguration();
        }

        private static void InitializeConfiguration(INadaService service, Vehicle vehicle, IVehicleConfigurationBuilder builder, Region region)
        {
            IList<Accessory> accessories;

            if (string.IsNullOrEmpty(builder.Vin))
            {
                accessories = service.Accessories(
                    vehicle.Body.Uid,
                    region);
            }
            else
            {
                accessories = service.Accessories(
                    builder.Uid,
                    region,
                    builder.Vin);
            }

            IList<AccessoryRelationship> relationships = service.AccessoryRelationships(vehicle.Body.Uid);

            foreach (Accessory accessory in accessories)
            {
                builder.Add(accessory);
            }

            foreach (Accessory accessory in accessories.Where(x => x.IsAdded))
            {
                string accessoryCode = accessory.Code;

                foreach (AccessoryRelationship relationship in relationships.Where(x => Equals(x.AccessoryCode, accessoryCode)))
                {
                    switch (relationship.RelationshipType)
                    {
                        case AccessoryRelationshipType.Included:
                            break;
                        case AccessoryRelationshipType.Excluded:
                            foreach (Accessory related in relationship.RelatedAccessories)
                            {
                                builder.Disable(related.Code);
                            }
                            break;
                    }
                }
            }
        }

        public IVehicleConfiguration UpdateConfiguration(IVehicleConfiguration configuration, IAccessoryAction action)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            if (action.ActionType == AccessoryActionType.Undefined)
            {
                throw new ArgumentOutOfRangeException("action", AccessoryActionType.Undefined, "Action type cannot be undefined");
            }

            INadaService service = Resolve<INadaService>();

            Vehicle vehicle = service.Information(configuration.Uid);

            bool repeat = configuration.AccessoryActions.Count(
                              x => x.ActionType == action.ActionType &&
                                   x.AccessoryCode == action.AccessoryCode) > 0;

            if (repeat)
            {
                return configuration; // been there, done that ...
            }

            Region region = service.ToRegion(configuration.State);

            IVehicleConfigurationBuilderFactory factory = Resolve<IVehicleConfigurationBuilderFactory>();

            IVehicleConfigurationBuilder builder;

            bool undo = configuration.AccessoryActions.Count(
                              x => x.ActionType != action.ActionType &&
                                   x.AccessoryCode == action.AccessoryCode) > 0;

            if (undo)
            {
                builder = factory.NewBuilder();

                builder.Period = service.Period();
                builder.Uid = configuration.Uid;
                builder.Vin = configuration.Vin;
                builder.State = configuration.State;
                builder.Mileage = configuration.Mileage;

                InitializeConfiguration(service, vehicle, builder, region);

                foreach (IAccessoryAction replay in configuration.AccessoryActions)
                {
                    if (replay.AccessoryCode != action.AccessoryCode)
                    {
                        PerformAction(service, replay, vehicle, builder, region);
                    }
                }
            }
            else
            {
                builder = factory.NewBuilder(configuration);

                PerformAction(service, action, vehicle, builder, region);
            }

            return builder.ToVehicleConfiguration();
        }

        static void PerformAction(INadaService service, IAccessoryAction action, Vehicle vehicle, IVehicleConfigurationBuilder builder, Region region)
        {
            IList<Accessory> accessories = service.Accessories(
                vehicle.Body.Uid,
                region);

            Accessory accessory = accessories.FirstOrDefault(x => Equals(x.Code, action.AccessoryCode));

            if (accessory == null)
            {
                throw new ArgumentException(string.Format("No such accessory {0}", action.AccessoryCode), "action");
            }

            builder.Do(action.AccessoryCode, action.ActionType);

            switch (action.ActionType)
            {
                case AccessoryActionType.Undefined:
                    throw new ArgumentException("Undefined OptionActionType", "action");
                case AccessoryActionType.Add:
                    AddAccessory(service, vehicle, builder, accessories, accessory);
                    break;
                case AccessoryActionType.Remove:
                    RemoveAccessory(service, vehicle, builder, accessories, accessory, false);
                    break;
            }
        }

        static void AddAccessory(INadaService service, Vehicle vehicle, IVehicleConfigurationBuilder builder, IList<Accessory> accessories, Accessory accessory)
        {
            if (builder.IsSelected(accessory.Code, true))
            {
                return;
            }
            
            builder.Select(accessory.Code);

            IList<AccessoryRelationship> relationships = service.AccessoryRelationships(vehicle.Body.Uid);

            foreach (AccessoryRelationship relationship in relationships.Where(x => Equals(x.AccessoryCode, accessory.Code)))
            {
                switch (relationship.RelationshipType)
                {
                    case AccessoryRelationshipType.Undefined:
                        throw new NotSupportedException("AccessoryRelationshipType.Undefined");
                    case AccessoryRelationshipType.Included:
                        Included(service, vehicle, builder, accessories, relationship);
                        break;
                    case AccessoryRelationshipType.Excluded:
                        Excluded(service, vehicle, builder, accessories, relationship);
                        break;
                }
            }
        }

        static void RemoveAccessory(INadaService service, Vehicle vehicle, IVehicleConfigurationBuilder builder, IList<Accessory> accessories, Accessory accessory, bool updateActions)
        {
            if (updateActions)
            {
                builder.Undo(accessory.Code);
            }

            if (!builder.IsSelected(accessory.Code, false))
            {
                return;
            }

            builder.Deselect(accessory.Code);

            IList<AccessoryRelationship> relationships = service.AccessoryRelationships(vehicle.Body.Uid);

            foreach (AccessoryRelationship relationship in relationships.Where(x => Equals(x.AccessoryCode, accessory.Code)))
            {
                switch (relationship.RelationshipType)
                {
                    case AccessoryRelationshipType.Undefined:
                        throw new NotSupportedException("AccessoryRelationshipType.Undefined");
                    case AccessoryRelationshipType.Excluded:
                        // no-op: no effect
                        break;
                    case AccessoryRelationshipType.Included:
                        foreach (Accessory relatedAccessory in relationship.RelatedAccessories)
                        {
                            RemoveAccessory(service, vehicle, builder, accessories, relatedAccessory, true);
                        }
                        break;
                }
            }

            RemoveDependentRequiresAll(service, vehicle, builder, relationships, accessories, accessory);
        }

        static void Excluded(INadaService service, Vehicle vehicle, IVehicleConfigurationBuilder builder, IList<Accessory> accessories, AccessoryRelationship relationship)
        {
            foreach (Accessory accessory in relationship.RelatedAccessories)
            {
                RemoveAccessory(service, vehicle, builder, accessories, accessory, true);
            }
        }

        static void Included(INadaService service, Vehicle vehicle, IVehicleConfigurationBuilder builder, IList<Accessory> accessories, AccessoryRelationship relationship)
        {
            foreach (Accessory accessory in relationship.RelatedAccessories)
            {
                AddAccessory(service, vehicle, builder, accessories, accessory);
            }
        }

        /// <summary>
        /// When you remove an option, there may be another option that caused its selection through a
        /// requires all relationship.  When this happens, that option (the one with the requires all)
        /// must be de-selected.  That is what this method does.
        /// </summary>
        static void RemoveDependentRequiresAll(INadaService service, Vehicle vehicle, IVehicleConfigurationBuilder builder, IEnumerable<AccessoryRelationship> relationships, IList<Accessory> accessories, Accessory accessory)
        {
            // IEnumerable<IAccessoryState> selectedStates = configuration.AccessoryStates.Where(x => x.Selected);

            IEnumerable<string> selectedAccessoryCodes = builder.Selected();

            foreach (string selectedAccessoryCode in selectedAccessoryCodes)
            {
                string code = selectedAccessoryCode;

                Accessory selectedAccessory = accessories.FirstOrDefault(x => Equals(x.Code, code));

                if (selectedAccessory == null) continue;

                IEnumerable<AccessoryRelationship> inclusions = relationships.Where(
                    x => x.RelationshipType == AccessoryRelationshipType.Included &&
                         Equals(x.AccessoryCode, code) &&
                         x.RelatedAccessories.Any(y => Equals(y.Code, accessory.Code)));

                foreach (AccessoryRelationship inclusion in inclusions)
                {
                    RemoveAccessory(service, vehicle, builder, accessories, selectedAccessory, true);

                    foreach (Accessory relatedOption in inclusion.RelatedAccessories)
                    {
                        RemoveAccessory(service, vehicle, builder, accessories, relatedOption, true);
                    }
                }
            }
        }

        protected static T Resolve<T>()
        {
            return RegistryFactory.GetResolver().Resolve<T>();
        }
    }
}
