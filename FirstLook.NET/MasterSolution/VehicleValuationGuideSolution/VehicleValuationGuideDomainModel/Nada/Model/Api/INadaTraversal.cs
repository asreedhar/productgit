﻿using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api
{
    public interface INadaTraversal
    {
        ITree CreateTree();

        ITree CreateTree(string vin);
    }

    public interface ITree
    {
        ITreeNode Root { get; }

        ITreeNode GetNodeByPath(ITreePath path);
    }

    public interface ITreeNode
    {
        ITreeNode Parent { get; }

        string Label { get; }

        int Id { get; }

        string Name { get; }

        int? Uid { get; }

        IList<ITreeNode> Children { get; }

        bool HasChildren { get; }

        void Save(ITreePath path);
    }

    public interface ITreePath
    {
        string State { get; set; }
    }
}
