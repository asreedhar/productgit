﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model
{
    [Serializable]
    public class Vehicle
    {
        private Period _period;
        private Year _year;
        private Make _make;
        private Series _series;
        private Body _body;

        public Period Period
        {
            get { return _period; }
            internal set { _period = value; }
        }

        public Year Year
        {
            get { return _year; }
            internal set { _year = value; }
        }

        public Make Make
        {
            get { return _make; }
            internal set { _make = value; }
        }

        public Series Series
        {
            get { return _series; }
            internal set { _series = value; }
        }

        public Body Body
        {
            get { return _body; }
            internal set { _body = value; }
        }

        private string _mileageCode;
        private string _vic;
        private int _curbWeight;
        private int _gvw;
        private int _gcw;
        private int _msrp;

        public string MileageCode
        {
            get { return _mileageCode; }
            internal set { _mileageCode = value; }
        }

        public string Vic
        {
            get { return _vic; }
            internal set { _vic = value; }
        }

        public int CurbWeight
        {
            get { return _curbWeight; }
            internal set { _curbWeight = value; }
        }

        public int Gvw
        {
            get { return _gvw; }
            internal set { _gvw = value; }
        }

        public int Gcw
        {
            get { return _gcw; }
            internal set { _gcw = value; }
        }

        public int Msrp
        {
            get { return _msrp; }
            internal set { _msrp = value; }
        }
    }
}