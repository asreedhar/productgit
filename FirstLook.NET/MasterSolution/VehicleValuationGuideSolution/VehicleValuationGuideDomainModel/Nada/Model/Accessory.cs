﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model
{
    [Serializable]
    public class Accessory
    {
        private string _code;
        private string _description;
        private bool _isAdded;
        private bool _isIncluded;

        public string Code
        {
            get { return _code; }
            internal set { _code = value; }
        }

        public string Description
        {
            get { return _description; }
            internal set { _description = value; }
        }

        public bool IsAdded
        {
            get { return _isAdded; }
            internal set { _isAdded = value; }
        }

        public bool IsIncluded
        {
            get { return _isIncluded; }
            internal set { _isIncluded = value; }
        }

        private IList<Value> _values = new List<Value>();

        public IList<Value> Values
        {
            get { return _values; }
            internal set { _values = value; }
        }
    }
}
