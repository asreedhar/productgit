﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model
{
    [Serializable]
    public class State
    {
        private string _name;
        private string _code;

        public string Name
        {
            get { return _name; }
            internal set { _name = value; }
        }

        public string Code
        {
            get { return _code; }
            internal set { _code = value; }
        }
    }
}
