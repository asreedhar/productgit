﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model
{
    [Serializable]
    public class Value
    {
        private ValueType _valueType;
        private int _valueAmount;

        public ValueType ValueType
        {
            get { return _valueType; }
            internal set { _valueType = value; }
        }

        public int ValueAmount
        {
            get { return _valueAmount; }
            internal set { _valueAmount = value; }
        }
    }
}
