﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model
{
    [Serializable]
    public enum ValueType
    {
        Undefined       = 0,
        Retail          = 1,
        Trade           = 2,
        Loan            = 3,
        TradeAverage    = 4,
        TradeRough      = 5
    }
}
