﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model
{
    [Serializable]
    public enum AccessoryRelationshipType
    {
        Undefined,
        Included,
        Excluded
    }
}
