﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model
{
    [Serializable]
    public class MileageAdjustment
    {
        private decimal _value;

        public decimal Value
        {
            get { return _value; }
            internal set { _value = value; }
        }
    }
}
