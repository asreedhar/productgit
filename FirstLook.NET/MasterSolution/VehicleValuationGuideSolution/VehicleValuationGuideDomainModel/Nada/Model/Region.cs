﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model
{
    [Serializable]
    public enum Region
    {
        Undefined           = 0, 
        Eastern             = 1,
        PacificNorthwest    = 2,
        Southwestern        = 3,
        Midwest             = 4,
        Central             = 5,
        Southeastern        = 6,
        NewEngland          = 7,
        DesertSouthwest     = 8,
        MountainStates      = 9,
        CaliforniaRegion    = 10
    }
}
