﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Lookup
{
    public interface ILookupBody
    {
        IList<Body> Lookup(int periodId, int yearId, int makeId, int seriesId);
    }

    public class LookupBody : ILookupBody
    {
        public IList<Body> Lookup(int periodId, int yearId, int makeId, int seriesId)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Body> serializer = registry.Resolve<ISqlSerializer<Body>>();

            using (IDataReader reader = service.Query(periodId, yearId, makeId, seriesId))
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupBody : CachedLookup, ILookupBody
    {
        private readonly ILookupBody _source = new LookupBody();

        public IList<Body> Lookup(int periodId, int yearId, int makeId, int seriesId)
        {
            string key = CreateCacheKey(periodId, yearId, makeId, seriesId);

            IList<Body> values = Cache.Get(key) as IList<Body>;

            if (values == null)
            {
                values = _source.Lookup(periodId, yearId, makeId, seriesId);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
