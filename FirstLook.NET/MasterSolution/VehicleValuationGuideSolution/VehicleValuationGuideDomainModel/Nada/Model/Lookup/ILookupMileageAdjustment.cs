﻿using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Lookup
{
    public interface ILookupMileageAdjustment
    {
        MileageAdjustment Lookup(int period, int uid, Region region, int mileage);
    }

    public class LookupMileageAdjustment : ILookupMileageAdjustment
    {
        public MileageAdjustment Lookup(int period, int uid, Region region, int mileage)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<MileageAdjustment> serializer = registry.Resolve<ISqlSerializer<MileageAdjustment>>();

            using (IDataReader reader = service.MileageAdjustment(period, uid, region, mileage))
            {
                if (reader.Read())
                {
                    return serializer.Deserialize((IDataRecord)reader);
                }

                return null;
            }
        }
    }

    public class CachedLookupMileageAdjustment : CachedLookup, ILookupMileageAdjustment
    {
        private readonly ILookupMileageAdjustment _source = new LookupMileageAdjustment();

        public MileageAdjustment Lookup(int period, int uid, Region region, int mileage)
        {
            string key = CreateCacheKey(period, uid, region, mileage);

            MileageAdjustment values = Cache.Get(key) as MileageAdjustment;

            if (values == null)
            {
                values = _source.Lookup(period, uid, region, mileage);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
