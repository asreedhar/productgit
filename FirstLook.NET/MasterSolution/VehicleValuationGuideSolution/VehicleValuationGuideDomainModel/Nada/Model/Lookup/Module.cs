﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Lookup
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ILookupState, CachedLookupState>(ImplementationScope.Shared);

            registry.Register<ILookupPeriod, CachedLookupPeriod>(ImplementationScope.Shared);

            registry.Register<ILookupYear, CachedLookupYear>(ImplementationScope.Shared);

            registry.Register<ILookupMake, CachedLookupMake>(ImplementationScope.Shared);

            registry.Register<ILookupSeries, CachedLookupSeries>(ImplementationScope.Shared);

            registry.Register<ILookupBody, CachedLookupBody>(ImplementationScope.Shared);

            registry.Register<ILookupVehicle, CachedLookupVehicle>(ImplementationScope.Shared);

            registry.Register<ILookupVehicleValue, CachedLookupVehicleValue>(ImplementationScope.Shared);

            registry.Register<ILookupVin, CachedLookupVin>(ImplementationScope.Shared);

            registry.Register<ILookupAccessory, CachedLookupAccessory>(ImplementationScope.Shared);

            registry.Register<ILookupAccessoryRelationship, CachedLookupAccessoryRelationship>(ImplementationScope.Shared);

            registry.Register<ILookupVinAccessory, CachedLookupVinAccessory>(ImplementationScope.Shared);

            registry.Register<ILookupRegion, CachedLookupRegion>(ImplementationScope.Shared);

            registry.Register<ILookupMileageAdjustment, CachedLookupMileageAdjustment>(ImplementationScope.Shared);
        }
    }
}
