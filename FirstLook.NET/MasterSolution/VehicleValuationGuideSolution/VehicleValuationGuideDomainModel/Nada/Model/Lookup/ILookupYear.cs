﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Lookup
{
    public interface ILookupYear
    {
        IList<Year> Lookup(int periodId);
    }

    public class LookupYear : ILookupYear
    {
        public IList<Year> Lookup(int periodId)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Year> serializer = registry.Resolve<ISqlSerializer<Year>>();

            using (IDataReader reader = service.Query(periodId))
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupYear : CachedLookup, ILookupYear
    {
        private readonly ILookupYear _source = new LookupYear();

        public IList<Year> Lookup(int periodId)
        {
            string key = CreateCacheKey(periodId);

            IList<Year> values = Cache.Get(key) as IList<Year>;

            if (values == null)
            {
                values = _source.Lookup(periodId);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
