﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Lookup
{
    public interface ILookupVin
    {
        IList<Vehicle> Lookup(int periodId, string vin);
    }

    public class LookupVin : ILookupVin
    {
        public IList<Vehicle> Lookup(int periodId, string vin)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Vehicle> serializer = registry.Resolve<ISqlSerializer<Vehicle>>();

            using (IDataReader reader = service.Query(periodId, vin))
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupVin : CachedLookup, ILookupVin
    {
        private readonly ILookupVin _source = new LookupVin();

        public IList<Vehicle> Lookup(int periodId, string vin)
        {
            string key = CreateCacheKey(periodId, vin);

            IList<Vehicle> values = Cache.Get(key) as IList<Vehicle>;

            if (values == null)
            {
                values = _source.Lookup(periodId, vin);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
