﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Lookup
{
    public interface ILookupState
    {
        IList<State> Lookup(int period);
    }

    public class LookupState : ILookupState
    {
        public IList<State> Lookup(int period)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<State> serializer = registry.Resolve<ISqlSerializer<State>>();

            using (IDataReader reader = service.States(period))
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupState : CachedLookup, ILookupState
    {
        private readonly ILookupState _source = new LookupState();

        public IList<State> Lookup(int period)
        {
            string key = CreateCacheKey();

            IList<State> values = Cache.Get(key) as IList<State>;

            if (values == null)
            {
                values = _source.Lookup(period);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
