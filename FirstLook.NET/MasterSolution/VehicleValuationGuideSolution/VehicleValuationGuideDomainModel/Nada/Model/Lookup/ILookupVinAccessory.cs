﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Lookup
{
    public interface ILookupVinAccessory
    {
        IList<Accessory> Lookup(int periodId, int uid, Region region, string vin);
    }

    public class LookupVinAccessory : ILookupVinAccessory
    {
        public IList<Accessory> Lookup(int periodId, int uid, Region region, string vin)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Accessory> serializer = registry.Resolve<ISqlSerializer<Accessory>>();

            using (IDataReader reader = service.Accessories(periodId, uid, region, vin))
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupVinAccessory : CachedLookup, ILookupVinAccessory
    {
        private readonly ILookupVinAccessory _source = new LookupVinAccessory();

        public IList<Accessory> Lookup(int periodId, int uid, Region region, string vin)
        {
            string key = CreateCacheKey(periodId, uid, region, vin);

            IList<Accessory> values = Cache.Get(key) as IList<Accessory>;

            if (values == null)
            {
                values = _source.Lookup(periodId, uid, region, vin);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
