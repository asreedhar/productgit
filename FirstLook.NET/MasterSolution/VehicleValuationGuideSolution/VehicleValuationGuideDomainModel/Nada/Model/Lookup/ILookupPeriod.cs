﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Lookup
{
    public interface ILookupPeriod
    {
        IList<Period> Lookup();
    }

    public class LookupPeriod : ILookupPeriod
    {
        public IList<Period> Lookup()
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Period> serializer = registry.Resolve<ISqlSerializer<Period>>();

            using (IDataReader reader = service.Query())
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupPeriod : CachedLookup, ILookupPeriod
    {
        private readonly ILookupPeriod _source = new LookupPeriod();

        public IList<Period> Lookup()
        {
            string key = CreateCacheKey();

            IList<Period> values = Cache.Get(key) as IList<Period>;

            if (values == null)
            {
                values = _source.Lookup();

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
