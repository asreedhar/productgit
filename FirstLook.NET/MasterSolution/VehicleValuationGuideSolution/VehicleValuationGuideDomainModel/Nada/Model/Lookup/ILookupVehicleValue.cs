﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Lookup
{
    public interface ILookupVehicleValue
    {
        IList<Value> Lookup(int periodId, int uid, Region region);
    }

    public class LookupVehicleValue : ILookupVehicleValue
    {
        public IList<Value> Lookup(int periodId, int uid, Region region)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Value> serializer = registry.Resolve<ISqlSerializer<Value>>();

            using (IDataReader reader = service.VehicleValues(periodId, uid, region))
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupVehicleValue : CachedLookup, ILookupVehicleValue
    {
        private readonly ILookupVehicleValue _source = new LookupVehicleValue();

        public IList<Value> Lookup(int periodId, int uid, Region region)
        {
            string key = CreateCacheKey(periodId, uid, region);

            IList<Value> values = Cache.Get(key) as IList<Value>;

            if (values == null)
            {
                values = _source.Lookup(periodId, uid, region);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
