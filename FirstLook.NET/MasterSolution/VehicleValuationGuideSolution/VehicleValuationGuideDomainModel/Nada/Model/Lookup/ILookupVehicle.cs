﻿using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Lookup
{
    public interface ILookupVehicle
    {
        Vehicle Lookup(int periodId, int uid);
    }

    public class LookupVehicle : ILookupVehicle
    {
        public Vehicle Lookup(int periodId, int uid)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Vehicle> serializer = registry.Resolve<ISqlSerializer<Vehicle>>();

            using (IDataReader reader = service.Vehicle(periodId, uid))
            {
                if (reader.Read())
                {
                    return serializer.Deserialize((IDataRecord)reader);
                }

                return null;
            }
        }
    }

    public class CachedLookupVehicle : CachedLookup, ILookupVehicle
    {
        private readonly ILookupVehicle _source = new LookupVehicle();

        public Vehicle Lookup(int periodId, int uid)
        {
            string key = CreateCacheKey(periodId, uid);

            Vehicle values = Cache.Get(key) as Vehicle;

            if (values == null)
            {
                values = _source.Lookup(periodId, uid);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
