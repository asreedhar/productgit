﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Lookup
{
    public interface ILookupAccessory
    {
        IList<Accessory> Lookup(int periodId, int uid, Region region);
    }

    public class LookupAccessory : ILookupAccessory
    {
        public IList<Accessory> Lookup(int periodId, int uid, Region region)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Accessory> serializer = registry.Resolve<ISqlSerializer<Accessory>>();

            using (IDataReader reader = service.Accessories(periodId, uid, region))
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupAccessory : CachedLookup, ILookupAccessory
    {
        private readonly ILookupAccessory _source = new LookupAccessory();

        public IList<Accessory> Lookup(int periodId, int uid, Region region)
        {
            string key = CreateCacheKey(periodId, uid, region);

            IList<Accessory> values = Cache.Get(key) as IList<Accessory>;

            if (values == null)
            {
                values = _source.Lookup(periodId, uid, region);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
