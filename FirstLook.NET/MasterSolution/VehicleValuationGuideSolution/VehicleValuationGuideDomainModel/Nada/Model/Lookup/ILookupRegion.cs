﻿using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Lookup
{
    public interface ILookupRegion
    {
        Region Lookup(int periodId, string stateName);
    }

    public class LookupRegion : ILookupRegion
    {
        public Region Lookup(int periodId, string stateName)
        {
            ISqlService service = RegistryFactory.GetResolver().Resolve<ISqlService>();

            ISqlSerializer<Region> ser = RegistryFactory.GetResolver().Resolve<ISqlSerializer<Region>>();

            using (IDataReader reader = service.Region(periodId, stateName))
            {
                if (reader.Read())
                {
                    return ser.Deserialize((IDataRecord)reader);
                }

                return Region.Undefined;
            }
        }
    }

    public class CachedLookupRegion : CachedLookup, ILookupRegion
    {
        private readonly ILookupRegion _source = new LookupRegion();

        public Region Lookup(int periodId, string stateName)
        {
            string key = CreateCacheKey(periodId, stateName);

            Region? values = Cache.Get(key) as Region?;

            if (values.GetValueOrDefault() == Region.Undefined)
            {
                values = _source.Lookup(periodId, stateName);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values.GetValueOrDefault();
        }
    }
}
