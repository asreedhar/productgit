﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Lookup
{
    public interface ILookupSeries
    {
        IList<Series> Lookup(int periodId, int yearId, int makeId);
    }

    public class LookupSeries : ILookupSeries
    {
        public IList<Series> Lookup(int periodId, int yearId, int makeId)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Series> serializer = registry.Resolve<ISqlSerializer<Series>>();

            using (IDataReader reader = service.Query(periodId, yearId, makeId))
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class CachedLookupSeries : CachedLookup, ILookupSeries
    {
        private readonly ILookupSeries _source = new LookupSeries();

        public IList<Series> Lookup(int periodId, int yearId, int makeId)
        {
            string key = CreateCacheKey(periodId, yearId, makeId);

            IList<Series> values = Cache.Get(key) as IList<Series>;

            if (values == null)
            {
                values = _source.Lookup(periodId, yearId, makeId);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
