using System.Globalization;
using System.Text;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Lookup
{
    public class CachedLookup
    {
        protected string CreateCacheKey(params object[] parameters)
        {
            StringBuilder sb = new StringBuilder("u", 1024);

            sb.Append(GetType().GetHashCode().ToString(CultureInfo.InvariantCulture));

            if (parameters.Length > 0)
            {
                sb.Append("?");

                foreach (object parameter in parameters)
                {
                    sb.Append(parameter);
                    sb.Append("&");
                }

                sb.Length = sb.Length - 1;
            }

            return sb.ToString();
        }

        protected ICache Cache
        {
            get
            {
                return RegistryFactory.GetResolver().Resolve<ICache>();
            }
        }
    }
}