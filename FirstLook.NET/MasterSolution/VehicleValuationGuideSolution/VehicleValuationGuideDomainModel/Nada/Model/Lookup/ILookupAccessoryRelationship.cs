﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Lookup
{
    public interface ILookupAccessoryRelationship
    {
        IList<AccessoryRelationship> Lookup(int periodId, int uid);
    }

    public class LookupAccessoryRelationship : ILookupAccessoryRelationship
    {
        public IList<AccessoryRelationship> Lookup(int periodId, int uid)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<AccessoryRelationship> serializer = registry.Resolve<ISqlSerializer<AccessoryRelationship>>();

            List<AccessoryRelationship> relationships = new List<AccessoryRelationship>();

            using (IDataReader reader = service.AccessoryIncludes(periodId, uid))
            {
                relationships.AddRange(serializer.Deserialize(reader));
            }

            using (IDataReader reader = service.AccessoryExcludes(periodId, uid))
            {
                relationships.AddRange(serializer.Deserialize(reader));
            }

            return relationships;
        }
    }

    public class CachedLookupAccessoryRelationship : CachedLookup, ILookupAccessoryRelationship
    {
        private readonly ILookupAccessoryRelationship _source = new LookupAccessoryRelationship();

        public IList<AccessoryRelationship> Lookup(int periodId, int uid)
        {
            string key = CreateCacheKey(periodId, uid);

            IList<AccessoryRelationship> values = Cache.Get(key) as IList<AccessoryRelationship>;

            if (values == null)
            {
                values = _source.Lookup(periodId, uid);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
