﻿using System.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Sql
{
    public interface ISqlService
    {
        IDataReader Query();

        IDataReader Query(int period);

        IDataReader Query(int period, int year);

        IDataReader Query(int period, int year, int make);

        IDataReader Query(int period, int year, int make, int series);

        IDataReader Query(int period, string vin);

        IDataReader States(int period);

        IDataReader Region(int period, string stateName);

        IDataReader Vehicle(int period, int uid);

        IDataReader VehicleValues(int period, int uid, Region region);

        IDataReader Accessories(int period, int uid, Region region, string vin);

        IDataReader Accessories(int period, int uid, Region region);

        IDataReader AccessoryExcludes(int period, int uid);

        IDataReader AccessoryIncludes(int period, int uid);

        IDataReader MileageAdjustment(int period, int uid, Region region, int mileage);
    }
}
