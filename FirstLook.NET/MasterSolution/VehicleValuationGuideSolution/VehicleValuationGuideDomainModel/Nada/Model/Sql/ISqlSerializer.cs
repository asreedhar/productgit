﻿using System.Collections.Generic;
using System.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Sql
{
    public interface ISqlSerializer<T>
    {
        IList<T> Deserialize(IDataReader reader);

        T Deserialize(IDataRecord record);
    }
}
