﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Sql
{
    public class Module : IModule
    {
        public class SerializerModule : IModule
        {
            public void Configure(IRegistry registry)
            {
                registry.Register<ISqlSerializer<Period>, PeriodSerializer>(ImplementationScope.Shared);

                registry.Register<ISqlSerializer<State>, StateSerializer>(ImplementationScope.Shared);

                registry.Register<ISqlSerializer<Year>, YearSerializer>(ImplementationScope.Shared);

                registry.Register<ISqlSerializer<Make>, MakeSerializer>(ImplementationScope.Shared);

                registry.Register<ISqlSerializer<Series>, SeriesSerializer>(ImplementationScope.Shared);

                registry.Register<ISqlSerializer<Body>, BodySerializer>(ImplementationScope.Shared);

                registry.Register<ISqlSerializer<Vehicle>, VehicleSerializer>(ImplementationScope.Shared);

                registry.Register<ISqlSerializer<Accessory>, AccessorySerializer>(ImplementationScope.Shared);

                registry.Register<ISqlSerializer<AccessoryRelationship>, AccessoryRelationshipSerializer>(ImplementationScope.Shared);

                registry.Register<ISqlSerializer<Value>, ValueSerializer>(ImplementationScope.Shared);

                registry.Register<ISqlSerializer<MileageAdjustment>, MileageAdjustmentSerializer>(ImplementationScope.Shared);

                registry.Register<ISqlSerializer<Region>, RegionSerializer>(ImplementationScope.Shared);
            }
        }

        public void Configure(IRegistry registry)
        {
            registry.Register<ISqlService, CurrentSqlService>(ImplementationScope.Isolated);

            registry.Register<SerializerModule>();
        }
    }
}
