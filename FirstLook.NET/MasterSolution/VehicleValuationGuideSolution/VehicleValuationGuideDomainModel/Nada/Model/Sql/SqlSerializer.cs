﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Data;
using NADAVehicle_Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Sql
{
    static class Extensions
    {
        public static bool HasColumn(this IDataRecord dr, string columnName)
        {
            for (int i = 0; i < dr.FieldCount; i++)
            {
                if (dr.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                {
                    return true;
                }
            }

            return false;
        }
    }

    public class StateSerializer : ISqlSerializer<State>
    {
        public IList<State> Deserialize(IDataReader reader)
        {
            List<State> values = new List<State>();

            while (reader.Read())
            {
                values.Add(Deserialize((IDataRecord)reader));
            }

            return values;
        }

        public State Deserialize(IDataRecord record)
        {
            string code = record.GetString(record.GetOrdinal("StateCode"));

            string name = record.GetString(record.GetOrdinal("StateDescr"));

            return new State { Code = code.Trim(), Name = name.Trim()};
        }
    }

    public class PeriodSerializer : ISqlSerializer<Period>
    {
        public IList<Period> Deserialize(IDataReader reader)
        {
            List<Period> values = new List<Period>();

            while (reader.Read())
            {
                values.Add(Deserialize((IDataRecord)reader));
            }

            return values;
        }

        public Period Deserialize(IDataRecord record)
        {
            int id = record.GetInt32(record.GetOrdinal("Id"));

            return new Period { Id = id };
        }
    }

    public class YearSerializer : ISqlSerializer<Year>
    {
        public IList<Year> Deserialize(IDataReader reader)
        {
            List<Year> values = new List<Year>();

            while (reader.Read())
            {
                values.Add(Deserialize((IDataRecord)reader));
            }

            return values;
        }

        public Year Deserialize(IDataRecord record)
        {
            int id = record.GetInt32(record.GetOrdinal(VehicleUI_Data.VEHICLE_YEAR_FIELD));

            string name;

            if (record.HasColumn(VehicleUI_Data.VEHICLE_YEAR_DESCR_FIELD))
            {
                name = DataRecord.GetString(record, VehicleUI_Data.VEHICLE_YEAR_DESCR_FIELD);
            }
            else
            {
                name = id.ToString();
            }

            return new Year {Id = id, Name = name};
        }
    }

    public class MakeSerializer : ISqlSerializer<Make>
    {
        public IList<Make> Deserialize(IDataReader reader)
        {
            List<Make> values = new List<Make>();

            while (reader.Read())
            {
                values.Add(Deserialize((IDataRecord)reader));
            }

            return values;
        }

        public Make Deserialize(IDataRecord record)
        {
            int id = record.GetInt32(record.GetOrdinal(VehicleUI_Data.MAKE_CODE_FIELD));

            string name = DataRecord.GetString(record, VehicleUI_Data.MAKE_DESCR_FIELD);

            return new Make{Id = id, Name = name};
        }
    }

    public class SeriesSerializer : ISqlSerializer<Series>
    {
        public IList<Series> Deserialize(IDataReader reader)
        {
            List<Series> values = new List<Series>();

            while (reader.Read())
            {
                values.Add(Deserialize((IDataRecord)reader));
            }

            return values;
        }

        public Series Deserialize(IDataRecord record)
        {
            int id = record.GetInt32(record.GetOrdinal(VehicleUI_Data.SERIES_CODE_FIELD));

            string name = DataRecord.GetString(record, VehicleUI_Data.SERIES_DESCR_FIELD);

            return new Series {Id = id, Name = name};
        }
    }

    public class BodySerializer : ISqlSerializer<Body>
    {
        public IList<Body> Deserialize(IDataReader reader)
        {
            List<Body> values = new List<Body>();

            while (reader.Read())
            {
                values.Add(Deserialize((IDataRecord)reader));
            }

            return values;
        }

        public Body Deserialize(IDataRecord record)
        {
            int id = record.GetInt32(record.GetOrdinal(VehicleUI_Data.BODY_CODE_FIELD));

            string name = DataRecord.GetString(record, VehicleUI_Data.BODY_DESCR_FIELD);

            Body body;

            if (record.HasColumn(VehicleUI_Data.UID_FIELD))
            {
                int uid = record.GetInt32(record.GetOrdinal(VehicleUI_Data.UID_FIELD));

                body = new Body { Id = id, Name = name, Uid = uid };
            }
            else
            {
                body = new Body { Name = name, Uid = id };
            }
            
            return body;
        }
    }

    public class VehicleSerializer : ISqlSerializer<Vehicle>
    {
        public IList<Vehicle> Deserialize(IDataReader reader)
        {
            List<Vehicle> values = new List<Vehicle>();

            while (reader.Read())
            {
                values.Add(Deserialize((IDataRecord)reader));
            }

            return values;
        }

        public Vehicle Deserialize(IDataRecord record)
        {
            ISqlSerializer<Year> serYear = new YearSerializer();
            ISqlSerializer<Make> serMake = new MakeSerializer();
            ISqlSerializer<Series> serSeries = new SeriesSerializer();
            ISqlSerializer<Body> serBody = new BodySerializer();

            Year year = serYear.Deserialize(record);
            Make make = serMake.Deserialize(record);
            Series series = serSeries.Deserialize(record);
            Body body = serBody.Deserialize(record);

            string mileageCode = DataRecord.GetString(record, VehicleUI_Data.MILEAGE_CODE_FIELD);

            string vic = DataRecord.GetString(record, VehicleUI_Data.MILEAGE_CODE_FIELD);

            int curbWeight = record.GetInt32(record.GetOrdinal(VehicleUI_Data.CURB_WEIGHT_FIELD));

            int gvw = record.GetInt32(record.GetOrdinal(VehicleUI_Data.GVW_FIELD));

            int gcw = record.GetInt32(record.GetOrdinal(VehicleUI_Data.GCW_FIELD));

            int msrp = record.GetInt32(record.GetOrdinal(VehicleUI_Data.MSRP_FIELD));

            return new Vehicle
            {
                Year = year,
                Make = make,
                Series = series,
                Body = body,
                MileageCode = mileageCode,
                Vic = vic,
                CurbWeight = curbWeight,
                Gvw = gvw,
                Gcw = gcw,
                Msrp = msrp
            };
        }
    }

    public class AccessorySerializer : ISqlSerializer<Accessory>
    {
        public IList<Accessory> Deserialize(IDataReader reader)
        {
            List<Accessory> values = new List<Accessory>();

            while (reader.Read())
            {
                values.Add(Deserialize((IDataRecord)reader));
            }

            return values;
        }

        public Accessory Deserialize(IDataRecord record)
        {
            string code = DataRecord.GetString(record, VehicleUI_Data.ACC_CODE_FIELD);

            bool included = record.GetBoolean(record.GetOrdinal(VehicleUI_Data.ACC_ISINCLUDED_FIELD));

            bool added = record.GetBoolean(record.GetOrdinal(VehicleUI_Data.ACC_ISADDED_FIELD));

            string description = DataRecord.GetString(record, VehicleUI_Data.ACC_DESCR_FIELD);

            int retail = record.GetInt32(record.GetOrdinal(VehicleUI_Data.ACC_RETAIL_FIELD));

            int trade = record.GetInt32(record.GetOrdinal(VehicleUI_Data.ACC_TRADEIN_FIELD));

            int loan = record.GetInt32(record.GetOrdinal(VehicleUI_Data.ACC_LOAN_FIELD));

            Accessory accessory = new Accessory
            {
                Code = code,
                Description = description,
                IsIncluded = included,
                IsAdded = added
            };

            accessory.Values.Add(new Value {ValueType = ValueType.Retail, ValueAmount = retail});

            accessory.Values.Add(new Value {ValueType = ValueType.Trade, ValueAmount = trade});

            accessory.Values.Add(new Value {ValueType = ValueType.Loan, ValueAmount = loan});

            return accessory;
        }
    }

    public class AccessoryRelationshipSerializer : ISqlSerializer<AccessoryRelationship>
    {
        public IList<AccessoryRelationship> Deserialize(IDataReader reader)
        {
            List<AccessoryRelationship> values = new List<AccessoryRelationship>();

            AccessoryRelationship currentRelationship = null;

            while (reader.Read())
            {
                AccessoryRelationshipType type = AccessoryRelationshipType.Undefined;

                string code = DataRecord.GetString(reader, VehicleUI_Data.ACC_CODE_FIELD);

                string relatedCode = string.Empty;

                if (reader.HasColumn(VehicleUI_Data.ACC_INCLUDE_CODE_FIELD))
                {
                    type = AccessoryRelationshipType.Included;

                    relatedCode = DataRecord.GetString(reader, VehicleUI_Data.ACC_INCLUDE_CODE_FIELD);
                }

                if (reader.HasColumn(VehicleUI_Data.ACC_EXCLUDE_CODE_FIELD))
                {
                    type = AccessoryRelationshipType.Excluded;

                    relatedCode = DataRecord.GetString(reader, VehicleUI_Data.ACC_EXCLUDE_CODE_FIELD);
                }

                if (currentRelationship == null || !Equals(currentRelationship.AccessoryCode, code))
                {
                    currentRelationship = new AccessoryRelationship
                    {
                        AccessoryCode = code,
                        RelationshipType = type
                    };

                    values.Add(currentRelationship);
                }
                
                // eek? need full accessory? or just code?

                currentRelationship.RelatedAccessories.Add(
                    new Accessory
                        {
                            Code = relatedCode
                        });
            }

            return values;
        }

        public AccessoryRelationship Deserialize(IDataRecord record)
        {
            throw new NotSupportedException();
        }
    }

    public class ValueSerializer : ISqlSerializer<Value>
    {
        public IList<Value> Deserialize(IDataReader reader)
        {
            List<Value> values = new List<Value>();

            while (reader.Read())
            {
                int retail = reader.GetInt32(reader.GetOrdinal(VehicleUI_Data.BASE_RETAIL_FIELD));

                values.Add(Build(ValueType.Retail, retail));

                int tradeIn = reader.GetInt32(reader.GetOrdinal(VehicleUI_Data.BASE_TRADEIN_FIELD));

                values.Add(Build(ValueType.Trade, tradeIn));

                int loan = reader.GetInt32(reader.GetOrdinal(VehicleUI_Data.BASE_LOAN_FIELD));

                values.Add(Build(ValueType.Loan, loan));

                int tradeInAvg = reader.GetInt32(reader.GetOrdinal(VehicleUI_Data.BASE_AVG_TRADEIN_FIELD));

                if (tradeInAvg > 0)
                {
                    values.Add(Build(ValueType.TradeAverage, tradeInAvg));
                }

                int tradeInRgh = reader.GetInt32(reader.GetOrdinal(VehicleUI_Data.BASE_ROUGH_TRADEIN_FIELD));

                if (tradeInRgh > 0)
                {
                    values.Add(Build(ValueType.TradeRough, tradeInRgh));
                }
            }

            return values;
        }

        public Value Deserialize(IDataRecord record)
        {
            throw new NotSupportedException();
        }

        private static Value Build(ValueType valueType, int valueAmount)
        {
            return new Value
            {
                ValueAmount = valueAmount,
                ValueType = valueType
            };
        }
    }

    public class MileageAdjustmentSerializer : ISqlSerializer<MileageAdjustment>
    {
        public IList<MileageAdjustment> Deserialize(IDataReader reader)
        {
            List<MileageAdjustment> values = new List<MileageAdjustment>();

            while (reader.Read())
            {
                values.Add(Deserialize((IDataRecord)reader));
            }

            return values;
        }

        public MileageAdjustment Deserialize(IDataRecord record)
        {
            decimal value = record.GetDecimal(record.GetOrdinal(VehicleUI_Data.ADJUSTMENT_FIELD));

            return new MileageAdjustment
            {
                Value = value
            };
        }
    }

    public class RegionSerializer : ISqlSerializer<Region>
    {
        public IList<Region> Deserialize(IDataReader reader)
        {
            List<Region> values = new List<Region>();

            while (reader.Read())
            {
                values.Add(Deserialize((IDataRecord)reader));
            }

            return values;
        }

        public Region Deserialize(IDataRecord record)
        {
            int value = record.GetInt32(record.GetOrdinal(VehicleUI_Data.REGIONSTATE_ID_FIELD));

            return (Region) value;
        }
    }
}
