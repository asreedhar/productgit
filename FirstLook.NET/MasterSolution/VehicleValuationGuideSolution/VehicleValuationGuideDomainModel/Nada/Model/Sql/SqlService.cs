﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using System.Reflection.Emit;
using System.Xml;
using FirstLook.Common.Core.Data;
using NADAVehicle_Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Sql
{
    [CLSCompliant(false)]
    public abstract class SqlServiceBase : ISqlService
    {
        protected abstract string DatabaseName { get; }

        protected abstract string SchemaName { get; }

        protected abstract NADAVehicle_System.Vehicle VehicleSystem { get; }

        public IDataReader Query()
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = String.Format(
                        "SELECT DISTINCT Period AS Id FROM {0}.Vehicles ORDER BY Period DESC",
                        SchemaName);

                    command.CommandType = CommandType.Text;

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        DataTable table = new DataTable("Period");

                        table.Load(reader);

                        return table.CreateDataReader();
                    }
                }
            }
        }

        public IDataReader Query(int period)
        {
            VehicleUI_Data data = VehicleSystem.getYears(period);

            return new DataTableReader(data.Tables[VehicleUI_Data.YEARS_TABLE]);
        }

        public IDataReader Query(int period, int year)
        {
            VehicleUI_Data data = VehicleSystem.getMakes(period, year);

            return new DataTableReader(data.Tables[VehicleUI_Data.MAKES_TABLE]);
        }

        public IDataReader Query(int period, int year, int make)
        {
            VehicleUI_Data data = VehicleSystem.getSeries(period, year, make);

            return new DataTableReader(data.Tables[VehicleUI_Data.SERIES_TABLE]);
        }

        public IDataReader Query(int period, int year, int make, int series)
        {
            VehicleUI_Data data = VehicleSystem.getBodies(period, year, make, series);

            return new DataTableReader(data.Tables[VehicleUI_Data.BODIES_TABLE]);
        }

        public IDataReader Query(int period, string vin)
        {
            VehicleUI_Data data = VehicleSystem.getVehiclesFromVin(period, vin);

            return new DataTableReader(data.Tables[VehicleUI_Data.VEHICLES_TABLE]);
        }

        public IDataReader States(int period)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = string.Format("{0}.stp_States_get", SchemaName);

                    command.CommandType = CommandType.StoredProcedure;

                    Database.AddWithValue(command, "Period", period, DbType.Int32);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        DataTable table = new DataTable("States");

                        table.Load(reader);

                        return table.CreateDataReader();
                    }
                }
            }
        }

        public IDataReader Region(int period, string stateName)
        {
            string filter = string.Format("{0} = '{1}'", VehicleUI_Data.REGIONSTATE_DESCR_FIELD, stateName);

            VehicleUI_Data data = VehicleSystem.getRegionStates(period);

            DataTable table = data.Tables[VehicleUI_Data.REGIONSTATES_TABLE];

            DataView view = new DataView(table) {RowFilter = filter};

            return view.ToTable(true).CreateDataReader();
        }

        public IDataReader Vehicle(int period, int uid)
        {
            VehicleUI_Data data = VehicleSystem.getVehicles(period, uid);

            return new DataTableReader(data.Tables[VehicleUI_Data.VEHICLES_TABLE]);
        }

        public IDataReader VehicleValues(int period, int uid, Region region)
        {
            VehicleUI_Data data = VehicleSystem.getVehicleValues(period, uid, (int) region);

            return new DataTableReader(data.Tables[VehicleUI_Data.VEHICLE_VALUES_TABLE]);
        }

        public IDataReader Accessories(int period, int uid, Region region)
        {
            VehicleUI_Data data = VehicleSystem.getVehicleAccessory(period, uid, (int) region);

            return new DataTableReader(data.Tables[VehicleUI_Data.ACCESSORIES_TABLE]);
        }

        public IDataReader Accessories(int period, int uid, Region region, string vin)
        {
            VehicleUI_Data data = VehicleSystem.getVehicleAccessory(period, uid, (int) region);

            VehicleSystem.getVinVehicleAccessory(data, period, vin, uid);

            return new DataTableReader(data.Tables[VehicleUI_Data.ACCESSORIES_TABLE]);
        }

        public IDataReader AccessoryExcludes(int period, int uid)
        {
            VehicleUI_Data data = VehicleSystem.getVehicleAccessoryMultExcludes(period, uid);

            return new DataTableReader(data.Tables[VehicleUI_Data.EXCLUSIVE_ACCESSORIES_TABLE]);
        }

        public IDataReader AccessoryIncludes(int period, int uid)
        {
            VehicleUI_Data data = VehicleSystem.getVehicleAccessoryPackageIncludes(period, uid);

            return new DataTableReader(data.Tables[VehicleUI_Data.INCLUSIVE_ACCESSORIES_TABLE]);
        }

        public IDataReader MileageAdjustment(int period, int uid, Region region, int mileage)
        {
            // get vehicle

            VehicleUI_Data data = VehicleSystem.getVehicles(period, uid);

            // get base vehicle values

            VehicleSystem.getVehicleValues(data, period, uid, (int) region);

            // get mileage values

            DataTable vehicleTable = data.Tables[VehicleUI_Data.VEHICLES_TABLE];

            DataRow vehicleRow = vehicleTable.Rows[0];

            int year = (int) vehicleRow[VehicleUI_Data.VEHICLE_YEAR_FIELD];

            string mileageClass = (string) vehicleRow[VehicleUI_Data.MILEAGE_CODE_FIELD];

            VehicleSystem.getVehicleMileage(data, period, year, mileageClass);

            // get the adjustment ...

            decimal mileageAdj = NADAVehicle_System.Vehicle.getMileageAdj(data, mileage);

            mileageAdj = Math.Floor(mileageAdj);

            // build the result
            
            DataTable table = new DataTable("MileageAdjustment");

            DataColumnCollection columns = table.Columns;

            columns.Add(VehicleUI_Data.ADJUSTMENT_FIELD, typeof(decimal));

            DataRow row = table.NewRow();

            row[VehicleUI_Data.ADJUSTMENT_FIELD] = mileageAdj;

            table.Rows.Add(row);

            return new DataTableReader(table);
        }

        public DataSet Snapshot(int uid, Region region, string vin)
        {
            DataTable periodTable = new DataTable("Period");

            periodTable.Load(Query());

            int period = (int) periodTable.Rows[0]["Id"];

            DataTable statesTable = new DataTable("States");

            statesTable.Load(States(period));

            VehicleUI_Data data = VehicleSystem.getRegionStates(period);

            VehicleSystem.getVehicles(data, period, uid);

            VehicleSystem.getVehicleValues(data, period, uid, (int) region);

            VehicleSystem.getVehicleAccessory(data, period, uid, (int) region);

            if (!string.IsNullOrEmpty(vin))
            {
                VehicleSystem.getVinVehicleAccessory(data, period, vin, uid);
            }

            VehicleSystem.getVehicleAccessoryMultExcludes(data, period, uid);

            VehicleSystem.getVehicleAccessoryPackageIncludes(data, period, uid);

            VehicleSystem.getVehicleAccessoryPackageIncludes(data, period, uid);

            DataTable vehicleTable = data.Tables[VehicleUI_Data.VEHICLES_TABLE];

            DataRow vehicleRow = vehicleTable.Rows[0];

            int year = (int) vehicleRow[VehicleUI_Data.VEHICLE_YEAR_FIELD];

            string mileageClass = (string) vehicleRow[VehicleUI_Data.MILEAGE_CODE_FIELD];

            VehicleSystem.getVehicleMileage(data, period, year, mileageClass);

            string ns = data.Tables[0].Namespace;

            periodTable.Namespace = ns;

            statesTable.Namespace = ns;

            foreach (DataColumn column in periodTable.Columns)
            {
                column.Namespace = ns;
            }

            foreach (DataColumn column in statesTable.Columns)
            {
                column.Namespace = ns;
            }

            data.Tables.Add(periodTable);

            data.Tables.Add(statesTable);

            return data;
        }
    }

    [CLSCompliant(false)]
    [System.ComponentModel.DesignerCategory("Code")]
    public class EmbeddedVehicle : NADAVehicle_System.Vehicle
    {
        public EmbeddedVehicle(string connectionString, string xmlConfig)
            : base(connectionString, string.Empty)
        {
            XmlDocument document = new XmlDocument();

            using (Stream stream = GetType().Assembly.GetManifestResourceStream(xmlConfig))
            {
                if (stream != null)
                {
                    document.Load(stream);
                }
                else
                {
                    throw new ArgumentOutOfRangeException(
                        "xmlConfig",
                        xmlConfig,
                        "Not a valid resource name");
                }
            }

            _database = new DataAccess.SqlServer
            {
                ConnectionString = connectionString,
                SpConfigXmlDoc = document
            };
        }
    }

    [CLSCompliant(false)]
    public class SnapshotEmbeddedVehicle : EmbeddedVehicle
    {
        private readonly string _schemaName;

        private readonly ChangeNotificationHashtable _hashtable;

        public SnapshotEmbeddedVehicle(string connectionString, string xmlConfig, string schemaName)
            : base(connectionString, xmlConfig)
        {
            _schemaName = schemaName;

            _hashtable = new ChangeNotificationHashtable();

            _hashtable.Added += Hashtable_Added;

            Setter().Invoke(_database, _hashtable);
        }
        
        private void Hashtable_Added(object sender, AddedEventArgs e)
        {
            SqlCommand command = e.Command;

            if (!command.CommandText.StartsWith(_schemaName))
            {
                command.CommandText = _schemaName + "." + command.CommandText;
            }
        }

        private delegate void DynamicMethodSetHandler(object source, object value);

        private static DynamicMethodSetHandler Setter()
        {
            Type type = typeof(DataAccess.SqlServer);

            FieldInfo field = type.GetField(
                "_commandParametersHashTable",
                BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);

            DynamicMethod setter = new DynamicMethod(
                "DynamicSet",
                typeof(void),
                new[] { typeof(object), typeof(object) }, type, true);

            ILGenerator generator = setter.GetILGenerator();

            generator.Emit(OpCodes.Ldarg_0);

            generator.Emit(OpCodes.Ldarg_1);

            if (field.FieldType.IsValueType)
            {
                generator.Emit(OpCodes.Unbox_Any, type);
            }

            generator.Emit(OpCodes.Stfld, field);

            generator.Emit(OpCodes.Ret);

            return (DynamicMethodSetHandler)setter.CreateDelegate(typeof(DynamicMethodSetHandler));
        }

        class AddedEventArgs : EventArgs
        {
            private readonly SqlCommand _command;

            public AddedEventArgs(SqlCommand command)
            {
                _command = command;
            }

            public SqlCommand Command
            {
                get { return _command; }
            }
        }

        class ChangeNotificationHashtable : Hashtable
        {
            public event EventHandler<AddedEventArgs> Added;

            public override object this[object key]
            {
                set
                {
                    SqlCommand command = value as SqlCommand;

                    if (command != null)
                    {
                        EventHandler<AddedEventArgs> handler = Added;

                        if (handler != null)
                        {
                            handler(this, new AddedEventArgs(command));
                        }
                    }

                    base[key] = value;
                }
            }
        }
    }

    [CLSCompliant(false)]
    public class CurrentSqlService : SqlServiceBase
    {
        private readonly NADAVehicle_System.Vehicle _vehicleSystem;

        protected override string DatabaseName
        {
            get { return "Nada"; }
        }

        protected override string SchemaName
        {
            get { return "dbo"; }
        }

        protected override NADAVehicle_System.Vehicle VehicleSystem
        {
            get { return _vehicleSystem; }
        }

        public CurrentSqlService()
        {
            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[DatabaseName];

            _vehicleSystem = new EmbeddedVehicle(
                settings.ConnectionString,
                "FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Sql.Resources.VehicleDBSTP.xml");
        }
    }

    [CLSCompliant(false)]
    public class SnapshotSqlService : SqlServiceBase
    {
        private readonly NADAVehicle_System.Vehicle _vehicleSystem;

        protected override string DatabaseName
        {
            get { return "Vehicle"; }
        }

        protected override string SchemaName
        {
            get { return "Nada"; }
        }

        protected override NADAVehicle_System.Vehicle VehicleSystem
        {
            get { return _vehicleSystem; }
        }

        public SnapshotSqlService()
        {
            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[DatabaseName];

            _vehicleSystem = new SnapshotEmbeddedVehicle(
                settings.ConnectionString,
                "FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Sql.Resources.VehicleDBSTP.xml",
                SchemaName);
        }
    }
}
