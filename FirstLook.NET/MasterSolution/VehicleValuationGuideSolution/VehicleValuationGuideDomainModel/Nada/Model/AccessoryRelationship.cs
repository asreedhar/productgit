﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Nada.Model
{
    [Serializable]
    public class AccessoryRelationship
    {
        private string _accessoryCode;
        private IList<Accessory> _relatedAccessories = new List<Accessory>();
        private AccessoryRelationshipType _relationshipType;

        public string AccessoryCode
        {
            get { return _accessoryCode; }
            internal set { _accessoryCode = value; }
        }

        public IList<Accessory> RelatedAccessories
        {
            get { return _relatedAccessories; }
            internal set { _relatedAccessories = value; }
        }

        public AccessoryRelationshipType RelationshipType
        {
            get { return _relationshipType; }
            internal set { _relationshipType = value; }
        }
    }
}
