﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects
{
    /// <summary>
    /// An action to add or remove an option from a vehicle configuration.
    /// </summary>
    [Serializable]
    public class OptionActionDto
    {
        /// <summary>
        /// Option code.
        /// </summary>
        public string Uoc { get; set; }

        /// <summary>
        /// Type of action to perform.
        /// </summary>
        public OptionActionTypeDto ActionType { get; set; }
    }
}
