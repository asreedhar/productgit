﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects
{
    /// <summary>
    /// Types of vehicle colors.
    /// </summary>
    [Serializable]
    public enum ColorTypeDto
    {
        /// <summary>
        /// Undefined.
        /// </summary>
        Undefined,

        /// <summary>
        /// Exterior color.
        /// </summary>
        Exterior,

        /// <summary>
        /// Interior color.
        /// </summary>
        Interior,

        /// <summary>
        /// Leather color.
        /// </summary>
        Leather
    }
}
