﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Traversal
{
    /// <summary>
    /// A node and its children.
    /// </summary>
    [Serializable]
    public class PathDto
    {
        /// <summary>
        /// Node.
        /// </summary>
        public NodeDto CurrentNode { get; set; }

        /// <summary>
        /// Children to the current node. Seemingly used instead of CurrentNode->Children.
        /// </summary>
        public List<NodeDto> Successors { get; set; }
    }
}
