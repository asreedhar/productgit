﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Traversal
{
    /// <summary>
    /// A node in a tree.
    /// </summary>
    [Serializable]
    public class NodeDto
    {
        /// <summary>
        /// Parent node.
        /// </summary>
        public NodeDto Parent { get; set; }

        /// <summary>
        /// Children nodes. Not used.
        /// </summary>
        public List<NodeDto> Children { get; set; }

        /// <summary>
        /// BlackBook vehicle identifier. Only present if this node is specific enough to identify a particular vehicle.
        /// </summary>
        public string Uvc { get; set; }

        /// <summary>
        /// Display label of the node, e.g. Year.
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// Value of the node. E.g. 2008.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Combination of label and value. For example, "year=2008".
        /// </summary>
        public string State { get; set; }
    }
}
