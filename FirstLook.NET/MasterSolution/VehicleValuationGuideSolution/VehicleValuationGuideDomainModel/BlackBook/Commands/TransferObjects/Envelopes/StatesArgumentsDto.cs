﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments for getting US state codes.
    /// </summary>
    [Serializable]
    public class StatesArgumentsDto
    {
    }
}