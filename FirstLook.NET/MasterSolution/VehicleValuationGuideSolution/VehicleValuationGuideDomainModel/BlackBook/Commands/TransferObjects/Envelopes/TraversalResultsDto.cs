﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Traversal;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Results of creating and traversing a tree.
    /// </summary>
    [Serializable]
    public class TraversalResultsDto
    {
        /// <summary>
        /// Vin, if one was provided.
        /// </summary>
        public TraversalArgumentsDto Arguments { get; set; }

        /// <summary>
        /// A tree, and the node in the tree that we were able to traverse to given the arguments.
        /// </summary>
        public PathDto Path { get; set; }

        /// <summary>
        /// Result of traversing the tree.
        /// </summary>
        public TraversalStatusDto Status { get; set; }
    }
}