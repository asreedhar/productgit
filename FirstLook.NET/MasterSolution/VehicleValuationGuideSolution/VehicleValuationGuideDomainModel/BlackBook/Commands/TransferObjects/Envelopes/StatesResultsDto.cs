﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Results of getting all US states.
    /// </summary>
    [Serializable]
    public class StatesResultsDto
    {
        /// <summary>
        /// Empty.
        /// </summary>
        public StatesArgumentsDto Arguments { get; set; }

        /// <summary>
        /// List of US state names and codes.
        /// </summary>
        public List<StateDto> States { get; set; }
    }
}