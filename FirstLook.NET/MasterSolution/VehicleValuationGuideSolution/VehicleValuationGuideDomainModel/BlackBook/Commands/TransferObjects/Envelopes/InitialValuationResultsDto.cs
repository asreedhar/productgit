﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Results of getting an initial configuration and valuation for a vehicle.
    /// </summary>
    [Serializable]
    public class InitialValuationResultsDto : ValuationResultsDto
    {
        /// <summary>
        /// The vin and uvc used to get the initial valuation.
        /// </summary>
        public InitialValuationArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Initial vehicle configuration.
        /// </summary>
        public VehicleConfigurationDto VehicleConfiguration { get; set; }

        /// <summary>
        /// Vehicle details.
        /// </summary>
        public VehicleInformationDto VehicleInformation { get; set; }

        /// <summary>
        /// List of options.
        /// </summary>
        public List<OptionDto> Options { get; set; }
    }
}
