﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments for a command that might optionally have a US state code.
    /// </summary>
    [Serializable]
    public class ArgumentsDto
    {
        /// <summary>
        /// US state code.
        /// </summary>
        public string State { get; set; }
    }
}