﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments for getting an initial configuration and valuation for a vehicle.
    /// </summary>
    [Serializable]
    public class InitialValuationArgumentsDto : ValuationArgumentsDto
    {
        /// <summary>
        /// Vin.
        /// </summary>
        public string Vin { get; set; }

        /// <summary>
        /// Uvc.
        /// </summary>
        public string Uvc { get; set; }
    }
}
