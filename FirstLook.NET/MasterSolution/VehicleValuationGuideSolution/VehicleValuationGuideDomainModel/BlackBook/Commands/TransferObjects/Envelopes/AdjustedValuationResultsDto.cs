﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Results of an adjusted valuation.
    /// </summary>
    [Serializable]
    public class AdjustedValuationResultsDto : ValuationResultsDto
    {
        /// <summary>
        /// The original vehicle configuration and the option action that was applied to it.
        /// </summary>
        public AdjustedValuationArgumentsDto Arguments { get; set; }

        /// <summary>
        /// The updated vehicle configuration.
        /// </summary>
        public VehicleConfigurationDto VehicleConfiguration { get; set; }
    }
}
