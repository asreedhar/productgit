﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Publications;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes.Publications
{
    /// <summary>
    /// Results of loading a specific publication.
    /// </summary>
    [Serializable]
    public class PublicationLoadResultsDto
    {
        /// <summary>
        /// Publication identifiers.
        /// </summary>
        public PublicationLoadArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Publication.
        /// </summary>
        public PublicationDto Publication { get; set; }
    }
}