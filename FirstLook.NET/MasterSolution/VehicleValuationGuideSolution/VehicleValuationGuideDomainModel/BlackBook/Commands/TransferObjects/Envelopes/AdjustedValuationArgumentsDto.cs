﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments for an adjusted valuation command.
    /// </summary>
    [Serializable]
    public class AdjustedValuationArgumentsDto : ValuationArgumentsDto
    {
        /// <summary>
        /// Vehicle configuration to update with an option action.
        /// </summary>
        public VehicleConfigurationDto VehicleConfiguration { get; set; }
        
        /// <summary>
        /// Option action to perform on the configuration.
        /// </summary>
        public OptionActionDto OptionAction { get; set; }
    }
}
