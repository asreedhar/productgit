using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments used in valuation commands.
    /// </summary>
    [Serializable]
    public class ValuationArgumentsDto : ArgumentsDto
    {
        /// <summary>
        /// Has a mileage been provided?
        /// </summary>
        public bool HasMileage { get; set; }

        /// <summary>
        /// Mileage, if provided.
        /// </summary>
        public int Mileage { get; set; }
    }
}