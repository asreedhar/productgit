﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Traversal;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Results of getting the successor node to a node.
    /// </summary>
    [Serializable]
    public class SuccessorResultsDto
    {
        /// <summary>
        /// Node and the details of the successor.
        /// </summary>
        public SuccessorArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Successor node and its children.
        /// </summary>
        public PathDto Path { get; set; }
    }
}