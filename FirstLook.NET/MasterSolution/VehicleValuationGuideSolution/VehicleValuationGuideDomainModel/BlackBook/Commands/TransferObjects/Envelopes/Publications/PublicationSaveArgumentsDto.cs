﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes.Publications
{
    /// <summary>
    /// Arguments to save a vehicle configuration for a broker and vehicle.
    /// </summary>
    [Serializable]
    public class PublicationSaveArgumentsDto : PublicationArgumentsDto
    {
        /// <summary>
        /// Vehicle configuration to save.
        /// </summary>
        public VehicleConfigurationDto VehicleConfiguration { get; set; }    
    }
}
