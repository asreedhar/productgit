﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments for retrieving the successor node to a given node.
    /// </summary>
    [Serializable]
    public class SuccessorArgumentsDto : ArgumentsDto
    {
        /// <summary>
        /// Current node.
        /// </summary>
        public string Node { get; set; }

        /// <summary>
        /// Successor node.
        /// </summary>
        public string Successor { get; set; }
    }
}
