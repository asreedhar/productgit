﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Publications;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes.Publications
{
    /// <summary>
    /// Results of saving a vehicle configuration.
    /// </summary>
    [Serializable]
    public class PublicationSaveResultsDto
    {
        /// <summary>
        /// Broker, vehicle and configuration details.
        /// </summary>
        public PublicationSaveArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Publication that has been saved.
        /// </summary>
        public PublicationDto Publication { get; set; }
    }
}