using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Results of a valuation command.
    /// </summary>
    [Serializable]
    public class ValuationResultsDto
    {
        /// <summary>
        /// Valuation prices.
        /// </summary>
        public PriceTablesDto Tables { get; set; }
    }
}