﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes.Publications
{
    /// <summary>
    /// Arguments used to retrieve publications for a dealer and vehicle.
    /// </summary>
    [Serializable]
    public class PublicationArgumentsDto
    {
        /// <summary>
        /// Broker whose vehicle we want to get BlackBook publications for.
        /// </summary>
        public Guid Broker { get; set; }

        /// <summary>
        /// Vehicle to get BlackBook publications for.
        /// </summary>
        public Guid Vehicle { get; set; }
    }
}