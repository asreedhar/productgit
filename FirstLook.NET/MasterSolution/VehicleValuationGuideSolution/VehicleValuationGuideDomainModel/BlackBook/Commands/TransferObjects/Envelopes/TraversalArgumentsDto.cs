﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments for traversing a tree by vin, if provided.
    /// </summary>
    [Serializable]
    public class TraversalArgumentsDto : ArgumentsDto
    {
        /// <summary>
        /// Vin. Optional.
        /// </summary>
        public string Vin { get; set; }
    }
}
