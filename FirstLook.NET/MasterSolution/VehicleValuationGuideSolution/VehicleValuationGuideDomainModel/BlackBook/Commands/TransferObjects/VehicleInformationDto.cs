﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects
{
    /// <summary>
    /// Details of a vehicle.
    /// </summary>
    [Serializable]
    public class VehicleInformationDto
    {
        /// <summary>
        /// Uvc. BlackBook vehicle identifier.
        /// </summary>
        public string Uvc { get; set; }

        /// <summary>
        /// Msrp.
        /// </summary>
        public int Msrp { get; set; }

        /// <summary>
        /// Finance advance.
        /// </summary>
        public int FinanceAdvance { get; set; }

        /// <summary>
        /// Wheel base.
        /// </summary>
        public string WheelBase { get; set; }

        /// <summary>
        /// Taxable horsepower.
        /// </summary>
        public string TaxableHorsepower { get; set; }

        /// <summary>
        /// Weight.
        /// </summary>
        public string Weight { get; set; }

        /// <summary>
        /// Tire size.
        /// </summary>
        public string TireSize { get; set; }

        /// <summary>
        /// Base horsepower.
        /// </summary>
        public string BaseHorsepower { get; set; }

        /// <summary>
        /// Fuel type.
        /// </summary>
        public string FuelType { get; set; }

        /// <summary>
        /// Number of cylinders.
        /// </summary>
        public string Cylinders { get; set; }

        /// <summary>
        /// Drive train.
        /// </summary>
        public string DriveTrain { get; set; }

        /// <summary>
        /// Transmission.
        /// </summary>
        public string Transmission { get; set; }

        /// <summary>
        /// Engine type.
        /// </summary>
        public string Engine { get; set; }

        /// <summary>
        /// Class of vehicle.
        /// </summary>
        public string VehicleClass { get; set; }
    }
}
