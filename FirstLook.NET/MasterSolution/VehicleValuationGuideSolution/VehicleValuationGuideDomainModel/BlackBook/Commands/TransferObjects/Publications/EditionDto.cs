﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Publications
{
    /// <summary>
    /// Details on an edition of a publication.
    /// </summary>
    [Serializable]
    public class EditionDto
    {
        /// <summary>
        /// Date from which the publication is valid.
        /// </summary>
        public DateTime BeginDate { get; set; }

        /// <summary>
        /// Date to which the publication is valid.
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// User responsible for the edition.
        /// </summary>
        public UserDto User { get; set; }
    }
}
