﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Publications
{
    /// <summary>
    /// Publication information.
    /// </summary>
    [Serializable]
    public class PublicationInfoDto
    {
        /// <summary>
        /// Who is responsible for the new publication.
        /// </summary>
        public ChangeAgentDto ChangeAgent { get; set; }

        /// <summary>
        /// What changed between publications?
        /// </summary>
        public ChangeTypeDto ChangeType { get; set; }

        /// <summary>
        /// Identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Edition details.
        /// </summary>
        public EditionDto Edition { get; set; }
    }
}
