﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Publications
{
    /// <summary>
    /// What changed between vehicle configurations?
    /// </summary>
    [Serializable]
    [Flags]
    public enum ChangeTypeDto
    {
        /// <summary>
        /// No change.
        /// </summary>
        None = 0,               

        /// <summary>
        /// US State changed.
        /// </summary>
        State = 1 << 0,

        /// <summary>
        /// Uvc.
        /// </summary>
        Uvc = 1 << 1,

        /// <summary>
        /// Mileage changed.
        /// </summary>
        Mileage = 1 << 2,

        /// <summary>
        /// Option actions changed.
        /// </summary>
        OptionActions = 1 << 3,

        /// <summary>
        /// Option states changed.
        /// </summary>
        OptionStates = 1 << 4,

        /// <summary>
        /// Time period changed.
        /// </summary>
        BookDate = 1 << 5
    }
}
