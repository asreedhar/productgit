﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Publications
{
    /// <summary>
    /// Who is responsible for a new vehicle configuration?
    /// </summary>
    [Serializable]
    public enum ChangeAgentDto
    {
        /// <summary>
        /// Undefined.
        /// </summary>
        Undefined,

        /// <summary>
        /// User-driven change.
        /// </summary>
        User,
        
        /// <summary>
        /// System-driven change.
        /// </summary>
        System
    }
}
