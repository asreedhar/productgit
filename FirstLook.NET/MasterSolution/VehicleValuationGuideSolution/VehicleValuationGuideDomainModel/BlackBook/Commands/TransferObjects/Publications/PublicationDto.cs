﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Publications
{
    /// <summary>
    /// Publication of vehicle reference data, a configuration on it, and the valuation of the configuration.
    /// </summary>
    [Serializable]
    public class PublicationDto : PublicationInfoDto
    {
        /// <summary>
        /// Vehicle information.
        /// </summary>
        public VehicleInformationDto VehicleInformation { get; set; }

        /// <summary>
        /// Vehicle configuration.
        /// </summary>
        public VehicleConfigurationDto VehicleConfiguration { get; set; }                       

        /// <summary>
        /// Price tables.
        /// </summary>
        public PriceTablesDto Tables { get; set; }

        /// <summary>
        /// List of options.
        /// </summary>
        public List<OptionDto> Options { get; set; }

        /// <summary>
        /// List of colors.
        /// </summary>
        public List<ColorDto> Colors { get; set; }   
    }
}
