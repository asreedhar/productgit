﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects
{
    /// <summary>
    /// Type of a price row.
    /// </summary>
    [Serializable]
    public enum PriceTableRowTypeDto
    {
        /// <summary>
        /// Undefined.
        /// </summary>
        Undefined,

        /// <summary>
        /// Base prices; i.e. without option or mile adjustments.
        /// </summary>
        Base,

        /// <summary>
        /// Option adjustment prices.
        /// </summary>
        Option,

        /// <summary>
        /// Mileage adjustment prices.
        /// </summary>
        Mileage,

        /// <summary>
        /// Final prices; i.e. base + option + mileage.
        /// </summary>
        Final
    }
}
