﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects
{
    /// <summary>
    /// Vehicle color.
    /// </summary>
    [Serializable]
    public class ColorDto
    {
        /// <summary>
        /// Type of color -- e.g. interior, exterior, etc.
        /// </summary>
        public ColorTypeDto ColorType { get; set; }

        /// <summary>
        /// Text description of the color.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Associated color codes.
        /// </summary>
        public List<string> Swatches { get; set; }
    }
}
