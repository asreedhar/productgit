﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects
{
    /// <summary>
    /// Reference data version information.
    /// </summary>
    [Serializable]
    public class BookDateDto
    {
        /// <summary>
        /// Data load identifier.
        /// </summary>
        public int Id { get; set; }
    }
}
