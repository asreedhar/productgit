﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects
{
    /// <summary>
    /// The state of an option -- i.e. is it on by default and if not has it been selected?
    /// </summary>
    [Serializable]
    public class OptionStateDto
    {
        /// <summary>
        /// Option code.
        /// </summary>
        public string Uoc { get; set; }

        /// <summary>
        /// Is the option selected?
        /// </summary>
        public bool Selected { get; set; }

        /// <summary>
        /// Is the option on by default?
        /// </summary>
        public bool Enabled { get; set; }
    }
}
