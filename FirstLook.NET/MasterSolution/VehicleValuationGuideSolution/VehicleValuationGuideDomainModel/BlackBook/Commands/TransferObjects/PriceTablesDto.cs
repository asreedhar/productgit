﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects
{
    /// <summary>
    /// Price tables for different transaction types.
    /// </summary>
    [Serializable]
    public class PriceTablesDto
    {
        /// <summary>
        /// Prices for a car to be sold at retail.
        /// </summary>
        public PriceTableDto Retail { get; set; }

        /// <summary>
        /// Prices for a car sold at wholesale.
        /// </summary>
        public PriceTableDto Wholesale { get; set; }

        /// <summary>
        /// Prices for a trade-in.
        /// </summary>
        public PriceTableDto TradeIn { get; set; }

        /// <summary>
        /// Prices for a finance advance.
        /// </summary>
        public PriceTableDto FinanceAdvance { get; set; }
    }
}
