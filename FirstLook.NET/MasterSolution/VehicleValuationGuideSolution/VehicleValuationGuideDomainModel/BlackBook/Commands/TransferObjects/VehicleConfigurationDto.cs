﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects
{
    /// <summary>
    /// Vehicle configuration.
    /// </summary>
    [Serializable]
    public class VehicleConfigurationDto
    {
        /// <summary>
        /// Version of the reference data.
        /// </summary>
        public BookDateDto BookDate { get; set; }
    
        /// <summary>
        /// Two letter US state identifier.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Uvc. BlackBook vehicle identifier.
        /// </summary>
        public string Uvc { get; set; }

        /// <summary>
        /// Vin.
        /// </summary>
        public string Vin { get; set; }

        /// <summary>
        /// Mileage.
        /// </summary>
        public int Mileage { get; set; }

        /// <summary>
        /// Does the car have a mileage value?
        /// </summary>
        public bool HasMileage { get; set; }

        /// <summary>
        /// List of option actions to perform.
        /// </summary>
        public List<OptionActionDto> OptionActions { get; set; }

        /// <summary>
        /// List of options and whether or not they are selected.
        /// </summary>
        public List<OptionStateDto> OptionStates { get; set; }

        /// <summary>
        /// Initialize the option lists.
        /// </summary>
        public VehicleConfigurationDto()
        {
            OptionStates = new List<OptionStateDto>();
            OptionActions = new List<OptionActionDto>();
        }
    }
}
