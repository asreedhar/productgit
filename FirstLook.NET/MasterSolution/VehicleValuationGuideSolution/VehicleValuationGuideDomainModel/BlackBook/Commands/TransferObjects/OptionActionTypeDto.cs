﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects
{
    /// <summary>
    /// Types of actions that may be performed on a vehicle configuration with an option.
    /// </summary>
    [Serializable]
    public enum OptionActionTypeDto
    {
        /// <summary>
        /// Undefined.
        /// </summary>
        Undefined,

        /// <summary>
        /// Add the option to the configuration.
        /// </summary>
        Add,

        /// <summary>
        /// Remove the option from the configuration.
        /// </summary>
        Remove
    }
}
