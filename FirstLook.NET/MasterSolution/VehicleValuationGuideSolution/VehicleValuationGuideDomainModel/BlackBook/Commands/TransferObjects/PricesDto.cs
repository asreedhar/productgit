﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects
{
    /// <summary>
    /// Prices for the various conditions of an object.
    /// </summary>
    [Serializable]
    public class PricesDto
    {
        /// <summary>
        /// Price for an extra clean condition.
        /// </summary>
        public int ExtraClean { get; set; }

        /// <summary>
        /// Price for a clean condition.
        /// </summary>
        public int Clean { get; set; }

        /// <summary>
        /// Price for an average condition.
        /// </summary>
        public int Average { get; set; }

        /// <summary>
        /// Price for a rough condition.
        /// </summary>
        public int Rough { get; set; }

        /// <summary>
        /// Is there a price for an extra clean condition?
        /// </summary>
        public bool HasExtraClean { get; set; }

        /// <summary>
        /// Is there a price for a clean condition?
        /// </summary>
        public bool HasClean { get; set; }

        /// <summary>
        /// Is there a price for an average condition?
        /// </summary>
        public bool HasAverage { get; set; }

        /// <summary>
        /// Is there a price for a rough condition?
        /// </summary>
        public bool HasRough { get; set; }
    }
}
