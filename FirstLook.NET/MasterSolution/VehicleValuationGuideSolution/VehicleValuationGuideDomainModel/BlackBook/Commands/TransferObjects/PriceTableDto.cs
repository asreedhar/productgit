﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects
{
    /// <summary>
    /// Table of price values.
    /// </summary>
    [Serializable]
    public class PriceTableDto
    {
        /// <summary>
        /// Are there prices for an extra clean condition?
        /// </summary>
        public bool HasExtraClean { get; set; }

        /// <summary>
        /// Are there prices for a clean condition?
        /// </summary>
        public bool HasClean { get; set; }

        /// <summary>
        /// Are there prices for an average condition?
        /// </summary>
        public bool HasAverage { get; set; }

        /// <summary>
        /// Are there prices for a rough condition?
        /// </summary>
        public bool HasRough { get; set; }        

        /// <summary>
        /// Price rows.
        /// </summary>
        public List<PriceTableRowDto> Rows { get; set; }

        /// <summary>
        /// Initialize the price rows.
        /// </summary>
        public PriceTableDto()
        {
            Rows = new List<PriceTableRowDto>();
        }
    }
}
