﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects
{
    /// <summary>
    /// US state.
    /// </summary>
    [Serializable]
    public class StateDto
    {
        /// <summary>
        /// Name of the state.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Two letter state code.
        /// </summary>
        public string Code { get; set; }
    }
}
