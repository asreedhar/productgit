﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects
{
    /// <summary>
    /// A vehicle configuration option.
    /// </summary>
    [Serializable]
    public class OptionDto
    {
        /// <summary>
        /// Option code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Description of the option.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Prices associated with the option.
        /// </summary>
        public PricesDto Prices { get; set; }
    }
}
