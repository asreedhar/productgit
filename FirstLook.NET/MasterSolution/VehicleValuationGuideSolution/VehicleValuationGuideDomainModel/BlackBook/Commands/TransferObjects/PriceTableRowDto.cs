﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects
{
    /// <summary>
    /// Row in a price table.
    /// </summary>
    [Serializable]
    public class PriceTableRowDto
    {
        /// <summary>
        /// Type of price row.
        /// </summary>
        public PriceTableRowTypeDto RowType { get; set; }

        /// <summary>
        /// Prices.
        /// </summary>
        public PricesDto Prices { get; set; }
    }
}
