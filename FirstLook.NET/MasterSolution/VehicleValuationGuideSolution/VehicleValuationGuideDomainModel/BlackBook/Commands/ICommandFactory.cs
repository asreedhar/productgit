﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes.Publications;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands
{
    /// <summary>
    /// Interface for a BlackBook command factory.
    /// </summary>
    public interface ICommandFactory
    {
        /// <summary>
        /// Create a command to get the US states.
        /// </summary>
        /// <returns>Command to get US states.</returns>
        ICommand<StatesResultsDto, StatesArgumentsDto>
            CreateStatesCommand();

        /// <summary>
        /// Create a command to create a tree from a vin, if provided, and traverse as far down it as possible.
        /// </summary>
        /// <returns>Command to create and traverse a tree.</returns>
        ICommand<TraversalResultsDto, TraversalArgumentsDto>
            CreateTraversalCommand();

        /// <summary>
        /// Create a command to get a node that is a successor to another.
        /// </summary>
        /// <returns>Command to get successors to a node.</returns>
        ICommand<SuccessorResultsDto, SuccessorArgumentsDto>
            CreateSuccessorCommand();

        /// <summary>
        /// Create a command to get an initial configuration and valuation for a vehicle.
        /// </summary>
        /// <returns>Command to get an initial valuation.</returns>
        ICommand<InitialValuationResultsDto, InitialValuationArgumentsDto>
            CreateInitialValuationCommand();

        /// <summary>
        /// Create a command to get a new valuation for an updated configuration.
        /// </summary>
        /// <returns>Command to get an adusted valuation.</returns>
        ICommand<AdjustedValuationResultsDto, AdjustedValuationArgumentsDto>
            CreateAdjustedValuationCommand();

        #region Publications

        /// <summary>
        /// Create a command to get a list of publications for a dealer and vehicle.
        /// </summary>
        /// <returns>Command to get list of available publications.</returns>
        ICommand<PublicationListResultsDto, IdentityContextDto<PublicationListArgumentsDto>>
            CreatePublicationListCommand();

        /// <summary>
        /// Create a command to get the publication that is valid on a given date.
        /// </summary>
        /// <returns>Command to get a publication for a date.</returns>
        ICommand<PublicationOnDateResultsDto, IdentityContextDto<PublicationOnDateArgumentsDto>>
            CreatePublicationOnDateCommand();

        /// <summary>
        /// Create a command to get the publication with the given identifiers.
        /// </summary>
        /// <returns>Command to get a specific publication.</returns>
        ICommand<PublicationLoadResultsDto, IdentityContextDto<PublicationLoadArgumentsDto>>
            CreatePublicationLoadCommand();

        /// <summary>
        /// Create a command to save a publication.
        /// </summary>
        /// <returns>Command to save a publication.</returns>
        ICommand<PublicationSaveResultsDto, IdentityContextDto<PublicationSaveArgumentsDto>>
            CreatePublicationSaveCommand();

        #endregion
    }
}