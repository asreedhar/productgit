﻿using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.Impl;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands
{
    /// <summary>
    /// Module for registering BlackBook command components.
    /// </summary>
    public class Module : IModule
    {
        /// <summary>
        /// Register the command factory.
        /// </summary>
        /// <param name="registry">Registry.</param>
        public void Configure(IRegistry registry)
        {
            registry.Register<ICommandFactory, CommandFactory>(ImplementationScope.Shared);
        }
    }
}
