﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.Impl.Publications;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes.Publications;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.Impl
{
    /// <summary>
    /// Factory for BlackBook commands.
    /// </summary>
    public class CommandFactory : ICommandFactory
    {
        /// <summary>
        /// Create a command to get the US states.
        /// </summary>
        /// <returns>Command to get US states.</returns>
        public ICommand<StatesResultsDto, StatesArgumentsDto> CreateStatesCommand()
        {
            return new StatesCommand();
        }

        /// <summary>
        /// Create a command to create a tree from a vin, if provided, and traverse as far down it as possible.
        /// </summary>
        /// <returns>Command to create and traverse a tree.</returns>
        public ICommand<TraversalResultsDto, TraversalArgumentsDto> CreateTraversalCommand()
        {
            return new TraversalCommand();
        }

        /// <summary>
        /// Create a command to get a node that is a successor to another.
        /// </summary>
        /// <returns>Command to get successors to a node.</returns>
        public ICommand<SuccessorResultsDto, SuccessorArgumentsDto> CreateSuccessorCommand()
        {
            return new SuccessorCommand();
        }

        /// <summary>
        /// Create a command to get an initial configuration and valuation for a vehicle.
        /// </summary>
        /// <returns>Command to get an initial valuation.</returns>
        public ICommand<InitialValuationResultsDto, InitialValuationArgumentsDto> CreateInitialValuationCommand()
        {
            return new InitialValuationCommand();
        }

        /// <summary>
        /// Create a command to get a new valuation for an updated configuration.
        /// </summary>
        /// <returns>Command to get an adusted valuation.</returns>
        public ICommand<AdjustedValuationResultsDto, AdjustedValuationArgumentsDto> CreateAdjustedValuationCommand()
        {
            return new AdjustedValuationCommand();
        }

        #region Publications

        /// <summary>
        /// Create a command to get a list of publications for a dealer and vehicle.
        /// </summary>
        /// <returns>Command to get list of available publications.</returns>
        public ICommand<PublicationListResultsDto, IdentityContextDto<PublicationListArgumentsDto>> CreatePublicationListCommand()
        {
            return new PublicationListCommand();
        }

        /// <summary>
        /// Create a command to get the publication that is valid on a given date.
        /// </summary>
        /// <returns>Command to get a publication for a date.</returns>
        public ICommand<PublicationOnDateResultsDto, IdentityContextDto<PublicationOnDateArgumentsDto>> CreatePublicationOnDateCommand()
        {
            return new PublicationOnDateCommand();
        }

        /// <summary>
        /// Create a command to get the publication with the given identifiers.
        /// </summary>
        /// <returns>Command to get a specific publication.</returns>
        public ICommand<PublicationLoadResultsDto, IdentityContextDto<PublicationLoadArgumentsDto>> CreatePublicationLoadCommand()
        {
            return new PublicationLoadCommand();
        }

        /// <summary>
        /// Create a command to save a publication.
        /// </summary>
        /// <returns>Command to save a publication.</returns>
        public ICommand<PublicationSaveResultsDto, IdentityContextDto<PublicationSaveArgumentsDto>> CreatePublicationSaveCommand()
        {
            return new PublicationSaveCommand();
        }

        #endregion
    }
}