using System;
using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Traversal;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api;
using FirstLook.VehicleValuationGuide.DomainModel.Utility.Model;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.Impl
{
    /// <summary>
    /// Command to create a tree and traverse as far down it as possible given the arguments.
    /// </summary>
    [Serializable]
    public class TraversalCommand : ICommand<TraversalResultsDto, TraversalArgumentsDto>
    {
        /// <summary>
        /// Create a tree. If no vin is provided, the tree will just consist of years and makes. If a vin is provided,
        /// its decoded values will be used to provide a fuller tree. The current node will be how far down we are
        /// able to traverse this tree given the arguments.
        /// </summary>
        /// <param name="parameters">Vin. Optional.</param>
        /// <returns>A node that is as far down a tree as possible given the arguments.</returns>
        public TraversalResultsDto Execute(TraversalArgumentsDto parameters)
        {
            IBlackBookTraversal traversal = RegistryFactory.GetResolver().Resolve<IBlackBookTraversal>();

            ITree tree;

            string vin = parameters.Vin;

            bool vinIsNullOrEmpty = string.IsNullOrEmpty(vin);

            if (!string.IsNullOrEmpty(parameters.State))
            {
                tree = vinIsNullOrEmpty ? traversal.CreateTree(new Adjustment { State = parameters.State })
                                        : traversal.CreateTree(vin, new Adjustment { State = parameters.State });
            }
            else
            {
                tree = vinIsNullOrEmpty ? traversal.CreateTree() : traversal.CreateTree(vin);
            }

            TraversalStatusDto status = TraversalStatusDto.Success;

            ITreeNode root = tree.Root;

            Mapper.Operation thisParents, thisChildren, nextChildren;
            Mapper.Operation nextParents = nextChildren = Mapper.Operation.No;

            if (vinIsNullOrEmpty)
            {
                thisParents = Mapper.Operation.No;
                thisChildren = Mapper.Operation.No;
            }
            else
            {
                if (root.Parent == null)
                {
                    thisParents = Mapper.Operation.No;

                    thisChildren = Mapper.Operation.No;

                    switch (Vin.Inspect(vin))
                    {
                        case Vin.Status.InvalidLength:
                            status = TraversalStatusDto.InvalidVinLength;
                            break;
                        case Vin.Status.InvalidCharacters:
                            status = TraversalStatusDto.InvalidVinCharacters;
                            break;
                        case Vin.Status.InvalidChecksum:
                            status = TraversalStatusDto.InvalidVinChecksum;
                            break;
                        case Vin.Status.Ok:
                            status = TraversalStatusDto.NoDataForVin;
                            break;
                    }
                }
                else
                {
                    thisParents = Mapper.Operation.Yes;
                    thisChildren = Mapper.Operation.NoThenYes;

                    switch (root.Parent.Label)
                    {
                        case "Year":
                            nextParents = new Mapper.Operation(true, true, true, false);
                            break;
                        case "Make":
                            nextParents = new Mapper.Operation(true, true, false);
                            break;
                        case "Model":
                            nextParents = Mapper.Operation.YesThenNo;
                            break;
                        case "Series":
                        case "Style":
                            nextParents = Mapper.Operation.No;
                            break;
                    }
                }
            }

            TraversalResultsDto results = new TraversalResultsDto
            {
                Arguments = parameters,
                Path = new PathDto
                {
                    CurrentNode = root != null ? Mapper.Map(root, thisParents, thisChildren) : null,
                    Successors = root != null ? Mapper.Map(root.Children, nextParents, nextChildren) : new List<NodeDto>()
                },
                Status = status
            };

            return results;
        }
    }
}