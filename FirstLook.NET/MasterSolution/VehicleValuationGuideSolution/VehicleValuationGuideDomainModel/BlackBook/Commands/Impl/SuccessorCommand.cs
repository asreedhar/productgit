using System;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Traversal;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.Impl
{
    /// <summary>
    /// Command to get the successor node to a given node in a tree.
    /// </summary>
    [Serializable]
    public class SuccessorCommand : ICommand<SuccessorResultsDto, SuccessorArgumentsDto>
    {
        /// <summary>
        /// Get the successor node to a given node, and that successor's children.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public SuccessorResultsDto Execute(SuccessorArgumentsDto parameters)
        {
            IBlackBookTraversal traversal = RegistryFactory.GetResolver().Resolve<IBlackBookTraversal>();

            ITree tree = !string.IsNullOrEmpty(parameters.State)
                             ? traversal.CreateTree(new Adjustment { State = parameters.State })
                             : traversal.CreateTree();

            ITreeNode node = tree.GetNodeByPath(new TreePath { State = parameters.Successor });

            SuccessorResultsDto results = new SuccessorResultsDto
            {
                Arguments = parameters,
                Path = new PathDto
                {
                    CurrentNode = Mapper.Map(node, Mapper.Operation.Yes, Mapper.Operation.No),
                    Successors = Mapper.Map(node.Children)
                }
            };

            return results;
        }
    }
}