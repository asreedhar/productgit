using System;
using System.Linq;
using System.Collections.Generic;
using System.Drawing;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Traversal;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api;
using Color=FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Color;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.Impl
{
    /// <summary>
    /// Helper class for mapping entities into transfer objects.
    /// </summary>
    internal static class Mapper
    {
        /// <summary>
        /// Map a price matrix into a transfer object.
        /// </summary>
        /// <param name="matrix">Price matrix.</param>
        /// <returns>Transfer object.</returns>
        public static PriceTablesDto Map(Matrix matrix)
        {
            PriceTablesDto tables = new PriceTablesDto();

            foreach (Market market in Enum.GetValues(typeof(Market)))
            {
                if (market == Market.Undefined)
                {
                    continue;
                }

                PriceTableDto table = new PriceTableDto();

                switch (market)
                {
                    case Market.Retail:
                        tables.Retail = table;
                        break;
                    case Market.TradeIn:
                        tables.TradeIn = table;
                        break;
                    case Market.Wholesale:
                        tables.Wholesale = table;
                        break;
                    case Market.FinanceAdvance:
                        tables.FinanceAdvance = table;
                        break;
                }

                foreach (Valuation valuation in Enum.GetValues(typeof (Valuation)))
                {
                    if (valuation == Valuation.Undefined)
                    {
                        continue;
                    }

                    PriceTableRowDto row = new PriceTableRowDto();

                    switch(valuation)
                    {
                        case Valuation.Base:
                            row.RowType = PriceTableRowTypeDto.Base;
                            break;
                        case Valuation.Final:
                            row.RowType = PriceTableRowTypeDto.Final;
                            break;
                        case Valuation.Mileage:
                            row.RowType = PriceTableRowTypeDto.Mileage;
                            break;
                        case Valuation.Option:
                            row.RowType = PriceTableRowTypeDto.Option;
                            break;
                    }

                    int? extraClean = GetPriceValue(matrix, market, Condition.ExtraClean, valuation);
                    int? clean = GetPriceValue(matrix, market, Condition.Clean, valuation);
                    int? average = GetPriceValue(matrix, market, Condition.Average, valuation);
                    int? rough = GetPriceValue(matrix, market, Condition.Rough, valuation);

                    PricesDto prices = new PricesDto
                                           {
                                               ExtraClean  = extraClean.GetValueOrDefault(),
                                               Clean       = clean.GetValueOrDefault(),
                                               Average     = average.GetValueOrDefault(),
                                               Rough       = rough.GetValueOrDefault(),
                                               HasExtraClean = extraClean.HasValue,
                                               HasClean = clean.HasValue,
                                               HasAverage = average.HasValue,
                                               HasRough = rough.HasValue
                                           };
                    
                    row.Prices = prices;

                    table.Rows.Add(row);
                }
            }
            
            return tables;
        }

        /// <summary>
        /// Get the value for the given market, condition and valuation from the price matrix, if one exists.
        /// </summary>
        /// <param name="matrix">Price matrix.</param>
        /// <param name="market">Market.</param>
        /// <param name="condition">Condition.</param>
        /// <param name="valuation">Valuation.</param>
        /// <returns>Price value, if on exists.</returns>
        internal static int? GetPriceValue(Matrix matrix, Market market, Condition condition, Valuation valuation)
        {
            int? value = null;
            
            Matrix.CellVisibility visibility = matrix[market, condition, valuation].Visibility;

            if (visibility == Matrix.CellVisibility.Value)
            {
                value = matrix[market, condition, valuation].Value;
            }

            return value;
        }

        /// <summary>
        /// Map a car's options into a transfer object collection.
        /// </summary>
        /// <param name="car">Car.</param>
        /// <param name="options">Car's options.</param>
        /// <returns>Transfer object collection.</returns>
        public static List<OptionDto> Map(CarInfo car, IEnumerable<Option> options)
        {
            IBlackBookCalculator calculator = RegistryFactory.GetResolver().Resolve<IBlackBookCalculator>();

            List<OptionDto> values = new List<OptionDto>();

            foreach (Option option in options)
            {
                Value value = calculator.Calculate(car, option);

                PricesDto prices = new PricesDto
                {
                    ExtraClean = value.ExtraClean.GetValueOrDefault(),
                    Clean = value.Clean.GetValueOrDefault(),
                    Average = value.Average.GetValueOrDefault(),
                    Rough = value.Rough.GetValueOrDefault(),
                    HasExtraClean = value.ExtraClean.HasValue,
                    HasClean = value.Clean.HasValue,
                    HasAverage = value.Average.HasValue,
                    HasRough = value.Rough.HasValue
                };

                values.Add(
                    new OptionDto
                        {
                            Code = option.UniversalOptionCode,
                            Description = option.Description,
                            Prices = prices
                        });
            }

            return values;
        }

        /// <summary>
        /// Map a collection of states into a transfer object collection.
        /// </summary>
        /// <param name="states">Collection of states.</param>
        /// <returns>Transfer object collection.</returns>
        public static List<StateDto> Map(IEnumerable<State> states)
        {
            List<StateDto> values = new List<StateDto>();

            foreach (State state in states)
            {
                values.Add(new StateDto { Code = state.Code, Name = state.Name });
            }

            return values;
        }

        /// <summary>
        /// Map a collection of tree nodes into a transfer object collection.
        /// </summary>
        /// <param name="nodes">Collection of nodes.</param>
        /// <returns>Transfer object collection</returns>
        public static List<NodeDto> Map(IEnumerable<ITreeNode> nodes)
        {
            return Map(nodes, Operation.No, Operation.No);
        }

        /// <summary>
        /// Map a collection of treen nodes into a collection of transfer objects.
        /// </summary>
        /// <param name="nodes">Tree nodes.</param>
        /// <param name="parents">Should parent nodes be mapped?</param>
        /// <param name="children">Should child nodes be mapped?</param>
        /// <returns>Collection of transfer objects.</returns>
        public static List<NodeDto> Map(IEnumerable<ITreeNode> nodes, Operation parents, Operation children)
        {
            List<NodeDto> list = new List<NodeDto>();

            foreach (ITreeNode node in nodes)
            {
                list.Add(Map(node, parents, children));
            }

            return list;
        }

        /// <summary>
        /// A mapping operation.
        /// </summary>
        public class Operation
        {
            private readonly Operation _next;
            private readonly bool _perform;

            /// <summary>
            /// Initialize with an array of actions.
            /// </summary>
            /// <param name="perform">Collection of actions to perform.</param>
            public Operation(params bool[] perform)
            {
                _perform = perform[0];

                if (perform.Length == 1)
                {
                    _next = this;
                }
                else
                {
                    bool[] next = new bool[perform.Length-1];

                    Array.Copy(perform, 1, next, 0, next.Length);

                    _next = new Operation(next);
                }
            }

            /// <summary>
            /// Create this operation with whether it should be performed, and what the next operation is.
            /// </summary>
            /// <param name="perform">Should this operation be performed?</param>
            /// <param name="next">Next operation.</param>
            protected Operation(bool perform, Operation next)
            {
                _next = next ?? this;
                _perform = perform;
            }

            /// <summary>
            /// Next operation.
            /// </summary>
            public Operation Next
            {
                get { return _next; }
            }

            /// <summary>
            /// Should an action be performed?
            /// </summary>
            public bool Perform
            {
                get { return _perform; }
            }

            public static readonly Operation Yes;
            public static readonly Operation No;
            public static readonly Operation NoThenYes;
            public static readonly Operation YesThenNo;
            public static readonly Operation YesThenNoThenYes;

            static Operation()
            {
                Yes = new Operation(true, null);
                No = new Operation(false, null);
                NoThenYes = new Operation(false, Yes);
                YesThenNo = new Operation(true, No);
                YesThenNoThenYes = new Operation(true, NoThenYes);
            }
        }

        /// <summary>
        /// Map a tree node into a transfer object.
        /// </summary>
        /// <param name="node">Tree node.</param>
        /// <param name="parents">Should parent nodes be mapped?</param>
        /// <param name="children">Should child nodes be mapped?</param>
        /// <returns>Transfer object.</returns>
        public static NodeDto Map(ITreeNode node, Operation parents, Operation children)
        {
            ITreePath treePath = new TreePath();

            node.Save(treePath);

            NodeDto dto = new NodeDto
                              {
                                  Label = node.Label,
                                  Value = node.Value,
                                  Uvc = node.UniversalVehicleCode,
                                  State = treePath.State
                              };
            
            if (children.Perform)
            {
                dto.Children = Map(node.Children);
            }

            if (parents.Perform && node.Parent != null)
            {
                dto.Parent = Map(node.Parent, parents.Next, children.Next);
            }

            return dto;
        }

        /// <summary>
        /// Map a vehicle configuration into a transfer object.
        /// </summary>
        /// <param name="configuration">Vehicle configuration.</param>
        /// <returns>Transfer object.</returns>
        public static VehicleConfigurationDto Map(IVehicleConfiguration configuration)
        {
            return new VehicleConfigurationDto
            {
                Uvc           = configuration.Uvc,
                Vin           = configuration.Vin,
                State         = configuration.State == null ? null : configuration.State.Code,
                Mileage       = configuration.Mileage.GetValueOrDefault(),
                HasMileage    = configuration.Mileage.HasValue,
                OptionActions = Map(configuration.OptionActions),
                OptionStates  = Map(configuration.OptionStates),
                BookDate      = new BookDateDto { Id = configuration.BookDate.Id }
            };
        }       

        /// <summary>
        /// Map a car into a transfer object.
        /// </summary>
        /// <param name="car">Car.</param>
        /// <returns>Transfer object.</returns>
        public static VehicleInformationDto Map(Car car)
        {
            return new VehicleInformationDto
            {
                BaseHorsepower    = car.BaseHorsepower,
                Cylinders         = car.Cylinders,
                DriveTrain        = car.DriveTrain,
                Engine            = car.Engine,
                FinanceAdvance    = car.FinanceAdvance.GetValueOrDefault(),
                FuelType          = car.FuelType,
                Msrp              = car.Msrp.GetValueOrDefault(),
                TaxableHorsepower = car.TaxableHorsepower,
                TireSize          = car.TireSize,
                Transmission      = car.Transmission,
                Uvc               = car.Uvc,
                VehicleClass      = car.VehicleClass,
                Weight            = car.Weight,
                WheelBase         = car.WheelBase
            };
        }

        /// <summary>
        /// Map a collection of option actions into a collection of transfer objects.
        /// </summary>
        /// <param name="actions">Collection of option actions.</param>
        /// <returns>Collection of transfer objects.</returns>
        private static List<OptionActionDto> Map(IEnumerable<IOptionAction> actions)
        {
            List<OptionActionDto> values = new List<OptionActionDto>();

            foreach (IOptionAction action in actions)
            {
                values.Add(
                    new OptionActionDto
                    {
                        ActionType = (OptionActionTypeDto)action.ActionType,
                        Uoc = action.Uoc
                    });
            }

            return values;
        }

        /// <summary>
        /// Map a collection of option states into a collection of transfer objects.
        /// </summary>
        /// <param name="states">Collection of option states.</param>
        /// <returns>Collection of transfer objects.</returns>
        private static List<OptionStateDto> Map(IEnumerable<IOptionState> states)
        {
            List<OptionStateDto> values = new List<OptionStateDto>();

            foreach (IOptionState state in states)
            {
                values.Add(
                    new OptionStateDto
                    {
                        Selected = state.Selected,
                        Enabled = state.Enabled,
                        Uoc = state.Uoc
                    });
            }

            return values;
        }

        /// <summary>
        /// Map a transfer object into a vehicle configuration.
        /// </summary>
        /// <param name="configuration">Vehicle configuration.</param>
        /// <param name="state">US state.</param>
        /// <param name="mileage">Mileage adjustment.</param>
        /// <returns>Vehicle configuration.</returns>
        public static IVehicleConfiguration Map(VehicleConfigurationDto configuration, State state, int? mileage)
        {
            if (state == null)
            {
                state = State.Values.FirstOrDefault(x => Equals(configuration.State, x.Code));
            }
            
            return new VehicleConfiguration(
                configuration.Uvc,
                state,
                configuration.Vin,
                mileage.HasValue ? mileage : configuration.Mileage,
                new BookDate { Id = configuration.BookDate.Id })
            {
                OptionActions = Map(configuration.OptionActions),
                OptionStates  = Map(configuration.OptionStates)
            };
        }        

        /// <summary>
        /// Map a collection of transfer objects into a collection of option actions.
        /// </summary>
        /// <param name="values">Collection of transfer objects.</param>
        /// <returns>List of option actions.</returns>
        private static IList<IOptionAction> Map(IEnumerable<OptionActionDto> values)
        {
            List<IOptionAction> actions = new List<IOptionAction>();

            foreach (OptionActionDto value in values)
            {
                actions.Add(Map(value));
            }

            return actions;
        }

        /// <summary>
        /// Map a collection of transfer objects into a collection of option states.
        /// </summary>
        /// <param name="values">Collection of transfer objects.</param>
        /// <returns>List of option states.</returns>
        private static IList<IOptionState> Map(IEnumerable<OptionStateDto> values)
        {
            List<IOptionState> states = new List<IOptionState>();

            foreach (OptionStateDto value in values)
            {
                states.Add(Map(value));
            }

            return states;
        }

        /// <summary>
        /// Map a transfer object into an option state.
        /// </summary>
        /// <param name="value">Transfer object.</param>
        /// <returns>Optino state.</returns>
        private static OptionState Map(OptionStateDto value)
        {
            return new OptionState
            {
                Uoc = value.Uoc,
                Enabled = value.Enabled,
                Selected = value.Selected
            };
        }

        /// <summary>
        /// Map a transfer object into an option actino.
        /// </summary>
        /// <param name="action">Transfer object.</param>
        /// <returns>Option action.</returns>
        public static IOptionAction Map(OptionActionDto action)
        {
            return new OptionAction
            {
                ActionType = (OptionActionType)action.ActionType,
                Uoc = action.Uoc
            };
        }
    }
}
