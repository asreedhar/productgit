﻿using System.Collections.Generic;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Publications;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.Impl.Publications
{
    /// <summary>
    /// Helper class to map publication entities into transfer objects.
    /// </summary>
    public static class PublicationMapper
    {        
        /// <summary>
        /// Map an edition of a publication into a transfer object.
        /// </summary>
        /// <param name="edition">Edition of a publication.</param>
        /// <returns>Publication transfer object.</returns>
        public static PublicationDto Map(IEdition<IPublication> edition)
        {
            if (edition == null)
            {
                return null;
            }

            IPublication publication = edition.Data;
            Car car = publication.Car;            

            return new PublicationDto
            {
                // Publication.
                VehicleInformation   = Mapper.Map(car),
                Options              = Mapper.Map(car, car.Options),
                VehicleConfiguration = Mapper.Map(publication.VehicleConfiguration),
                Tables               = Mapper.Map(publication.Matrix),
                
                // Publication Info.
                ChangeAgent = (ChangeAgentDto)publication.ChangeAgent,
                ChangeType  = (ChangeTypeDto)publication.ChangeType,
                //Id          = publication.VehicleConfigurationHistoryId,
                //Edition = new EditionDto
                //{
                //    BeginDate = edition.DateRange.BeginDate,
                //    EndDate   = edition.DateRange.EndDate,
                //    User      = new UserDto
                //    {
                //        FirstName = edition.User.FirstName,
                //        LastName  = edition.User.LastName,
                //        UserName  = edition.User.UserName
                //    }
                //}
                
            };
        }

        /// <summary>
        /// Map a list of the identifying information of publication editions into a list of transfer objects.
        /// </summary>
        /// <param name="publications">List of the identifying information of publication editions.</param>
        /// <returns>List of transfer objects.</returns>
        public static List<PublicationInfoDto> Map(IList<IEdition<IPublicationInfo>> publications)
        {
            List<PublicationInfoDto> values = new List<PublicationInfoDto>();

            foreach (IEdition<IPublicationInfo> edition in publications)
            {
                IPublicationInfo publication = edition.Data;

                values.Add(
                    new PublicationInfoDto
                    {
                        ChangeAgent = (ChangeAgentDto)publication.ChangeAgent,
                        ChangeType  = (ChangeTypeDto)publication.ChangeType,
                        Id          = publication.VehicleConfigurationHistoryId,
                        Edition     = new EditionDto
                        {
                            BeginDate = edition.DateRange.BeginDate,
                            EndDate   = edition.DateRange.EndDate,
                            User      = new UserDto
                            {
                                FirstName = edition.User.FirstName,
                                LastName  = edition.User.LastName,
                                UserName  = edition.User.UserName
                            }
                        }
                    });
            }

            return values;
        }
    }
}
