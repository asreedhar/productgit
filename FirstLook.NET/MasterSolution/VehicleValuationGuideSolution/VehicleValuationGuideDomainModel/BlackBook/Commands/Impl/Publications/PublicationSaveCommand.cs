﻿using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes.Publications;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.Impl.Publications
{
    /// <summary>
    /// Command to save a publication.
    /// </summary>
    public class PublicationSaveCommand : ICommand<PublicationSaveResultsDto, IdentityContextDto<PublicationSaveArgumentsDto>>
    {
        /// <summary>
        /// Save a publication.
        /// </summary>
        /// <param name="parameters">Publication details to save.</param>
        /// <returns>Publication that was saved.</returns>
        public PublicationSaveResultsDto Execute(IdentityContextDto<PublicationSaveArgumentsDto> parameters)
        {
            // Resolve dependencies.
            IResolver            resolver            = RegistryFactory.GetResolver();
            IVehicleRepository   vehicleRepository   = resolver.Resolve<IVehicleRepository>();
            IBlackBookRepository blackBookRepository = resolver.Resolve<IBlackBookRepository>();
            IBlackBookCalculator calculator          = resolver.Resolve<IBlackBookCalculator>();
            IBlackBookService    service             = resolver.Resolve<IBlackBookService>();

            // Local variables.
            PublicationSaveArgumentsDto arguments = parameters.Arguments;
            IdentityDto identity = parameters.Identity;
            State state = State.Identify(arguments.VehicleConfiguration.State ?? string.Empty);

            // Build configuration and its matrix.            
            IVehicleConfiguration configuration = Mapper.Map(arguments.VehicleConfiguration, state, arguments.VehicleConfiguration.Mileage);

            Car car = string.IsNullOrEmpty(configuration.Vin)
                          ? service.Vehicle(configuration.Uvc, new Adjustment { State = state.Code })
                          : service.Vehicle(configuration.Uvc, configuration.Vin, new Adjustment { State = state.Code });
            
            Matrix matrix = calculator.Calculate(car, configuration);

            // Get client, vehicle and user details.
            IBroker broker = Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);
            IPrincipal principal = Principal.Get(identity.Name, identity.AuthorityName);
            ClientVehicleIdentification vehicle = vehicleRepository.Identification(broker, arguments.Vehicle);            

            // Save the configuration.
            IEdition<IPublication> publication = 
                blackBookRepository.Save(broker, principal, vehicle, configuration, matrix, configuration.BookDate.Id);

            return new PublicationSaveResultsDto
            {
                Arguments = arguments,
                Publication = PublicationMapper.Map(publication)
            };
        }
    }
}