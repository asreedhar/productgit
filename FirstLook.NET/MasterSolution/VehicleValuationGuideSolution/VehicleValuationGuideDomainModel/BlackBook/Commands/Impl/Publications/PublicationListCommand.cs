﻿using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes.Publications;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.Impl.Publications
{    
    /// <summary>
    /// Command to get the list of publications for a dealer and vehicle.
    /// </summary>
    public class PublicationListCommand : ICommand<PublicationListResultsDto, IdentityContextDto<PublicationListArgumentsDto>>
    {        
        /// <summary>
        /// Get the list of publications for a dealer and vehicle.
        /// </summary>
        /// <param name="parameters">Dealer and vehicle identifiers.</param>
        /// <returns>List of publications.</returns>
        public PublicationListResultsDto Execute(IdentityContextDto<PublicationListArgumentsDto> parameters)
        {
            IResolver            resolver            = RegistryFactory.GetResolver();
            IVehicleRepository   vehicleRepository   = resolver.Resolve<IVehicleRepository>();
            IBlackBookRepository blackBookRepository = resolver.Resolve<IBlackBookRepository>();

            // Get the details of the client and vehicle.
            IBroker broker = Broker.Get(parameters.Identity.Name, parameters.Identity.AuthorityName, parameters.Arguments.Broker);
            ClientVehicleIdentification vehicle = vehicleRepository.Identification(broker, parameters.Arguments.Vehicle);

            // Get the list of publications.
            IList<IEdition<IPublicationInfo>> publications = blackBookRepository.Load(broker, vehicle);

            return new PublicationListResultsDto
            {
                Arguments = parameters.Arguments,
                Publications = PublicationMapper.Map(publications)
            };
        }        
    }
}