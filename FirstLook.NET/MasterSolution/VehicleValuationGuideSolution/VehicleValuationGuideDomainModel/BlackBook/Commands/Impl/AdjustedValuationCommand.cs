using System;
using System.Linq;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.Impl
{
    /// <summary>
    /// Command to get a new valuation for an updated configuration.
    /// </summary>
    [Serializable]
    public class AdjustedValuationCommand : ICommand<AdjustedValuationResultsDto, AdjustedValuationArgumentsDto>
    {
        /// <summary>
        /// Get a new valuation for the updated configuration.
        /// </summary>
        /// <param name="parameters">Configuration and the new option adjustments.</param>
        /// <returns>Updated configuration and adjustment.</returns>
        public AdjustedValuationResultsDto Execute(AdjustedValuationArgumentsDto parameters)
        {
            // A configuration is not optional.
            if (parameters.VehicleConfiguration == null)
            {
                throw new ArgumentNullException("parameters", "VehicleConfiguration is null");
            }

            IResolver resolver = RegistryFactory.GetResolver();
            IBlackBookService service = resolver.Resolve<IBlackBookService>();

            // Parse the configuration from the parameters.
            IVehicleConfiguration configuration = Mapper.Map(
                parameters.VehicleConfiguration,
                State.Values.FirstOrDefault(x => Equals(x.Code, parameters.State)),
                parameters.HasMileage ? parameters.Mileage : default(int?));            

            Car car;

            // Get the car without a state adjustment, and with a vin if present.
            if (string.IsNullOrEmpty(parameters.State))
            {
                car = string.IsNullOrEmpty(configuration.Vin)
                          ? service.Vehicle(configuration.Uvc)
                          : service.Vehicle(configuration.Uvc, configuration.Vin);
            }
            // Get the car with a state adjustment, and also by vin if present.
            else
            {
                car = string.IsNullOrEmpty(configuration.Vin)
                          ? service.Vehicle(configuration.Uvc, new Adjustment { State = parameters.State })
                          : service.Vehicle(configuration.Uvc, configuration.Vin, new Adjustment { State = parameters.State });
            }

            // Update the configuration if there's a reason to.            
            if (parameters.OptionAction != null)
            {
                configuration = resolver.Resolve<IBlackBookConfiguration>().UpdateConfiguration(
                    configuration,
                    Mapper.Map(parameters.OptionAction));                               
            }

            // Get the new valuation.
            Matrix matrix = resolver.Resolve<IBlackBookCalculator>().Calculate(car, configuration);

            // Return the updated configuration and valuation.
            return new AdjustedValuationResultsDto
            {
                Arguments            = parameters,
                Tables               = Mapper.Map(matrix),
                VehicleConfiguration = Mapper.Map(configuration)
            };            
        }
    }
}