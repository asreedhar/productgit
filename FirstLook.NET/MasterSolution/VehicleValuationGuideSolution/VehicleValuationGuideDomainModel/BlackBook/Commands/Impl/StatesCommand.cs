﻿using System;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.Impl
{
    /// <summary>
    /// Command to get the list of US states.
    /// </summary>
    [Serializable]
    public class StatesCommand : ICommand<StatesResultsDto, StatesArgumentsDto>
    {
        /// <summary>
        /// Get all US states.
        /// </summary>
        /// <param name="parameters">Empty.</param>
        /// <returns>List of US state names and codes.</returns>
        public StatesResultsDto Execute(StatesArgumentsDto parameters)
        {
            return new StatesResultsDto
            {
                States = Mapper.Map(State.Values)
            };
        }
    }
}