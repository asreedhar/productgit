using System;
using System.Linq;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.Impl
{
    /// <summary>
    /// Command to get an initial configuration and valuation for a vehicle.
    /// </summary>
    [Serializable]
    public class InitialValuationCommand : ICommand<InitialValuationResultsDto, InitialValuationArgumentsDto>
    {
        /// <summary>
        /// Get the initial valuation of the car defined by the given uvc and possibly vin.
        /// </summary>
        /// <param name="parameters">Uvc and possibly vin.</param>
        /// <returns>Initial configuration.</returns>
        public InitialValuationResultsDto Execute(InitialValuationArgumentsDto parameters)
        {
            // Uvc is not optional.
            if (string.IsNullOrEmpty(parameters.Uvc))
            {
                throw new ArgumentNullException("parameters", "UVC is null");
            }

            IResolver resolver = RegistryFactory.GetResolver();
            IBlackBookService service = resolver.Resolve<IBlackBookService>();                        

            Car car;            
            State state = null;

            // Get the car without a state adjustment, and with a vin if present.
            if (string.IsNullOrEmpty(parameters.State))
            {                                
                car = string.IsNullOrEmpty(parameters.Vin)
                          ? service.Vehicle(parameters.Uvc)
                          : service.Vehicle(parameters.Uvc, parameters.Vin);
            }
            // Get the car with a state adjustment, and also by vin if present.
            else
            {                
                state = State.Values.First(x => Equals(x.Code, parameters.State));                

                car = string.IsNullOrEmpty(parameters.Vin)
                          ? service.Vehicle(parameters.Uvc, new Adjustment { State = parameters.State })
                          : service.Vehicle(parameters.Uvc, parameters.Vin, new Adjustment { State = parameters.State });
            }

            // Get the initial configuration.
            IVehicleConfiguration configuration = resolver.Resolve<IBlackBookConfiguration>().InitialConfiguration(
                parameters.Uvc,
                parameters.Vin,
                state,
                parameters.HasMileage ? parameters.Mileage : default(int?));

            // Get the valuation of the configuration.            
            Matrix matrix = resolver.Resolve<IBlackBookCalculator>().Calculate(car, configuration);
            
            // Return the car and configuration and valuation.
            return new InitialValuationResultsDto
            {
                Arguments            = parameters,
                Options              = Mapper.Map(car, car.Options),
                Tables               = Mapper.Map(matrix),
                VehicleConfiguration = Mapper.Map(configuration),
                VehicleInformation   = Mapper.Map(car)
            };            
        }
    }
}