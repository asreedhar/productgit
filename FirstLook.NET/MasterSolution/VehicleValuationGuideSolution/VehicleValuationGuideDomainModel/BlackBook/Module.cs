﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook
{
    /// <summary>
    /// Module for registering BlackBook domain model components.
    /// </summary>
    public class Module : IModule
    {
        /// <summary>
        /// Register BlackBook domain model components.
        /// </summary>
        /// <param name="registry"></param>
        public void Configure(IRegistry registry)
        {
            registry.Register<Commands.Module>();
            registry.Register<Model.Module>();
            registry.Register<Repositories.Module>();
        }
    }
}
