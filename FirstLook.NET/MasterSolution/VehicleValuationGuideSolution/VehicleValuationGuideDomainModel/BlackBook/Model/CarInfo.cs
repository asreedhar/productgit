﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model
{
    /// <summary>
    /// Car information.
    /// </summary>
    [Serializable]
    public class CarInfo
    {
        /// <summary>
        /// Year.
        /// </summary>
        public int Year { get; internal set; }

        /// <summary>
        /// Make.
        /// </summary>
        public string Make { get; internal set; }

        /// <summary>
        /// Model.
        /// </summary>
        public string Model { get; internal set; }

        /// <summary>
        /// Series.
        /// </summary>
        public string Series { get; internal set; }

        /// <summary>
        /// Body style.
        /// </summary>
        public string BodyStyle { get; internal set; }

        /// <summary>
        /// Vin.
        /// </summary>
        public string Vin { get; internal set; }

        /// <summary>
        /// Uvc.
        /// </summary>
        public string Uvc { get; internal set; }
    }
}
