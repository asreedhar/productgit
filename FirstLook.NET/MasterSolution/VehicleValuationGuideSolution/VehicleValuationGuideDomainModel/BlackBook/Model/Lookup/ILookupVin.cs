﻿using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Lookup
{
    /// <summary>
    /// Interface for BlackBook lookups for cars with the given vin.
    /// </summary>
    public interface ILookupVin
    {
        /// <summary>
        /// Lookup the cars with the given vin.
        /// </summary>
        /// <param name="vin">Vin.</param>
        /// <returns>Cars.</returns>
        IList<CarInfo> Lookup(string vin);
    }
}
