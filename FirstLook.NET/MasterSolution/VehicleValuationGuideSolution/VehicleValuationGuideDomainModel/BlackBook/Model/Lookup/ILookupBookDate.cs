﻿namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Lookup
{
    /// <summary>
    /// Interface for lookups for book dates.
    /// </summary>
    public interface ILookupBookDate
    {
        /// <summary>
        /// Lookup the current book date.
        /// </summary>
        /// <remarks>
        /// Book date lookups do not hit the external BlackBook service.
        /// </remarks>
        /// <returns>Book date, with data load identifier.</returns>
        BookDate Lookup();
    }
}
