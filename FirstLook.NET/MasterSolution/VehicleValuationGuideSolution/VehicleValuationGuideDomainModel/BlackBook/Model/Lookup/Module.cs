﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Lookup
{
    /// <summary>
    /// Module for registering lookup components.
    /// </summary>
    public class Module : IModule
    {
        /// <summary>
        /// Register lookup components.
        /// </summary>
        /// <param name="registry">Registry.</param>
        public void Configure(IRegistry registry)
        {
            registry.Register<ILookupBodyStyle,         LookupBodyStyle>(ImplementationScope.Shared);
            registry.Register<ILookupBookDate,          LookupBookDate>(ImplementationScope.Shared);
            registry.Register<ILookupMake,              LookupMake>(ImplementationScope.Shared);
            registry.Register<ILookupModel,             LookupModel>(ImplementationScope.Shared);
            registry.Register<ILookupSeries,            LookupSeries>(ImplementationScope.Shared);
            registry.Register<ILookupStandardEquipment, CachedLookupStandardEquipment>(ImplementationScope.Shared);
            registry.Register<ILookupUvc,               CachedLookupUvc>(ImplementationScope.Shared);
            registry.Register<ILookupVin,               LookupVin>(ImplementationScope.Shared);
            registry.Register<ILookupYear,              LookupYear>(ImplementationScope.Shared);
        }
    }
}
