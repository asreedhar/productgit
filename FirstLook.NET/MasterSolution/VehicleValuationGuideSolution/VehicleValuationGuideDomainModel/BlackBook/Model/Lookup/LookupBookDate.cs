﻿using System.Data;
using System.Linq;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Lookup
{
    public class LookupBookDate : ILookupBookDate
    {
        public BookDate Lookup()
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<BookDate> serializer = registry.Resolve<ISqlSerializer<BookDate>>();

            using (IDataReader reader = service.BookDate())
            {
                return serializer.Deserialize(reader).FirstOrDefault();
            }
        }
    }

    public class CachedLookupBookDate : CachedLookup, ILookupBookDate
    {
        private readonly ILookupBookDate _source = new LookupBookDate();

        public BookDate Lookup()
        {
            string key = CreateCacheKey();

            BookDate value = Cache.Get(key) as BookDate;

            if (value == null)
            {
                value = _source.Lookup();

                Cache.Add(key, value, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return value;
        }
    }
}
