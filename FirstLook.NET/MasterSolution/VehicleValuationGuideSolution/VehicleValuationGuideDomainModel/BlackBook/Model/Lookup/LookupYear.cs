using System.Collections.Generic;
using System.Data;
using System.Linq;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Lookup
{
    public class LookupYear : ILookupYear
    {
        public IList<string> Lookup()
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Year> serializer = registry.Resolve<ISqlSerializer<Year>>();

            using (IDataReader reader = service.Query())
            {
                return serializer.Deserialize(reader).Select(x => x.Value).ToList();
            }
        }
    }

    public class CachedLookupYear : CachedLookup, ILookupYear
    {
        private readonly ILookupYear _source = new LookupYear();

        public IList<string> Lookup()
        {
            string key = CreateCacheKey();

            IList<string> values = Cache.Get(key) as IList<string>;

            if (values == null)
            {
                values = _source.Lookup();

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}