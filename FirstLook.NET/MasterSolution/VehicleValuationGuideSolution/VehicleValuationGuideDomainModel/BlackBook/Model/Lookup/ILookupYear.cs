﻿using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Lookup
{
    /// <summary>
    /// Interface for BlackBook lookups for years.
    /// </summary>
    public interface ILookupYear
    {
        /// <summary>
        /// Lookup the BlackBook years.
        /// </summary>
        /// <remarks>
        /// BlackBook years and makes are housed internally, and so the external service will not be hit.
        /// </remarks>
        /// <returns>Years.</returns>
        IList<string> Lookup();
    }
}