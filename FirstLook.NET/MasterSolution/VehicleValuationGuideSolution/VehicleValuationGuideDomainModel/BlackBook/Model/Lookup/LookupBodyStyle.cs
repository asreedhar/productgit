using System.Collections.Generic;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Xml;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Lookup
{
    /// <summary>
    /// BlackBook lookups for body styles.
    /// </summary>
    public class LookupBodyStyle : ILookupBodyStyle
    {
        /// <summary>
        /// Lookup the body styles for the car with the given year, make, model and series.
        /// </summary>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        /// <param name="model">Model.</param>
        /// <param name="series">Series.</param>
        /// <returns>List of body styles.</returns>
        public IList<CarInfo> Lookup(string year, string make, string model, string series)
        {
            IResolver registry = RegistryFactory.GetResolver();

            IXmlService service = registry.Resolve<IXmlService>();

            IXmlSerializer<Cars> serializer = registry.Resolve<IXmlSerializer<Cars>>();

            Cars cars = serializer.Deserialize(service.Query(int.Parse(year), make, model, series));

            return cars.Values;
        }
    }
}