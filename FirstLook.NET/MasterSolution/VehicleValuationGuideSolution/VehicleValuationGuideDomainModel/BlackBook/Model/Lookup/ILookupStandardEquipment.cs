﻿namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Lookup
{
    /// <summary>
    /// Interface for BlackBook lookups for standard equipment.
    /// </summary>
    public interface ILookupStandardEquipment
    {
        /// <summary>
        /// Lookup the standard equipment for the car with the given uvc.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <returns>Standard equipment.</returns>
        StandardEquipment Lookup(string uvc);
    }
}
