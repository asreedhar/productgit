using System.Collections.Generic;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Xml;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Lookup
{
    /// <summary>
    /// BlackBook lookups for cars with the given vin.
    /// </summary>
    public class LookupVin : ILookupVin
    {
        /// <summary>
        /// Lookup the cars with the given vin.
        /// </summary>
        /// <param name="vin">Vin.</param>
        /// <returns>Cars.</returns>
        public IList<CarInfo> Lookup(string vin)
        {
            IResolver registry = RegistryFactory.GetResolver();

            IXmlService service = registry.Resolve<IXmlService>();

            IXmlSerializer<Cars> serializer = registry.Resolve<IXmlSerializer<Cars>>();

            Cars cars = serializer.Deserialize(service.Vin(vin));

            return cars.Values;
        }
    }
}