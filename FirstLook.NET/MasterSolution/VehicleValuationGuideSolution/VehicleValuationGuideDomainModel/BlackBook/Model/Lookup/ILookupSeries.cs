﻿using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Lookup
{
    /// <summary>
    /// Interface for BlackBook lookups for series.
    /// </summary>
    public interface ILookupSeries
    {
        /// <summary>
        /// Lookup the series for cars with the give year, make and model.
        /// </summary>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        /// <param name="model">Model.</param>
        /// <returns>List of series.</returns>
        IList<CarInfo> Lookup(string year, string make, string model);
    }
}
