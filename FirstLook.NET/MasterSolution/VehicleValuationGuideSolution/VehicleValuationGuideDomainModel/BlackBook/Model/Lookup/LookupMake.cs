using System.Collections.Generic;
using System.Data;
using System.Linq;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Sql;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Lookup
{
    public class LookupMake : ILookupMake
    {
        public IList<string> Lookup(string year)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISqlService service = registry.Resolve<ISqlService>();

            ISqlSerializer<Make> serializer = registry.Resolve<ISqlSerializer<Make>>();

            using (IDataReader reader = service.Query(int.Parse(year)))
            {
                return serializer.Deserialize(reader).Select(x => x.Value).ToList();
            }
        }
    }

    public class CachedLookupMake : CachedLookup, ILookupMake
    {
        private readonly ILookupMake _source = new LookupMake();

        public IList<string> Lookup(string year)
        {
            string key = CreateCacheKey(year);

            IList<string> values = Cache.Get(key) as IList<string>;

            if (values == null)
            {
                values = _source.Lookup(year);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}