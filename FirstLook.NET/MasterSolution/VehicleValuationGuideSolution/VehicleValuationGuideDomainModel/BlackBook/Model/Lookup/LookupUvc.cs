using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Xml;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities;
using FirstLook.VehicleValuationGuide.DomainModel.Utility.Model;


namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Lookup
{
    public abstract class BaseLookupUvc : ILookupUvc
    {
        protected void Verify(string uvc)
        {
            if (string.IsNullOrEmpty(uvc))
            {
                throw new ArgumentNullException("uvc", "Uvc is not optional.");
            }
        }

        protected void Verify(string uvc, string vin)
        {
            Verify(uvc);

            if (string.IsNullOrEmpty(vin))
            {
                throw new ArgumentNullException("vin", "Vin is not optional.");
            }
        }

        protected Car Pick(Cars cars)
        {
            return cars.Values.FirstOrDefault() as Car;
        }

        protected Car Pick(Cars cars, string uvc, string vin)
        {
            return cars.Values.FirstOrDefault(car => (Equals(car.Uvc, uvc) && Equals(car.Vin, Vin.Squish(vin)))) as Car;
        }

        protected T Resolve<T>()
        {
            return RegistryFactory.GetResolver().Resolve<T>();
        }

        public abstract Car Lookup(string uvc, string state);

        public abstract Car Lookup(string uvc, string vin, string state);
    }

    public class LookupUvc : BaseLookupUvc
    {
        public Cars All(string uvc, string state)
        {
            Verify(uvc);

            IXmlService service = Resolve<IXmlService>();

            IXmlSerializer<Cars> serializer = Resolve<IXmlSerializer<Cars>>();

            Cars cars;

            if (!string.IsNullOrEmpty(state))
            {
                cars = serializer.Deserialize(service.Uvc(uvc, new XmlAdjustment { State = state }));
            }
            else
            {
                cars = serializer.Deserialize(service.Uvc(uvc));
            }

            return cars;
        }

        public override Car Lookup(string uvc, string state)
        {
            return Pick(All(uvc, state));
        }

        public Cars All(string uvc, string vin, string state)
        {
            Verify(uvc, vin);

            IXmlService service = Resolve<IXmlService>();

            IXmlSerializer<Cars> serializer = Resolve<IXmlSerializer<Cars>>();

            Cars cars;

            if (!string.IsNullOrEmpty(state))
            {
                cars = serializer.Deserialize(service.Vin(vin, new XmlAdjustment { State = state }));
            }
            else
            {
                cars = serializer.Deserialize(service.Vin(vin));
            }

            return cars;
        }

        public override Car Lookup(string uvc, string vin, string state)
        {
            return Pick(All(uvc, state), uvc, vin);
        }
    }

    public class CachedLookupUvc : BaseLookupUvc
    {
        private readonly LookupUvc _source = new LookupUvc();

        /// <summary>
        /// Lookup a car by uvc. A state adjustment may optionally be used.
        /// </summary>
        /// <remarks>
        /// Due to BlackBook peculiarity, when querying by uvc alone you may end up getting a vin that is different
        /// than the one you queried BlackBook for to get the uvc in the first place. Highly recommended that you use
        /// the Lookup(uvc, vin, state) variant instead if you have vin handy.
        /// </remarks>
        /// <param name="uvc">Uvc.</param>
        /// <param name="state">Two letter US state identifier. Optional.</param>
        /// <returns>Car.</returns>
        public override Car Lookup(string uvc, string state)
        {
            Verify(uvc);

            //Below code is commented for fogbugz case:31680
            //if (Resolve<IBlackBookRepository>().CarCache_IsValid(uvc, state))
            //{
            //    try
            //    {
            //        return Resolve<IBlackBookRepository>().Load_Car(uvc, state).Car;
            //    }
            //    catch (Exception e)
            //    {

            //        Console.WriteLine(e.Message);
            //        // Load_car exceptioned or returned null.
            //        return null;
            //    }
            //}

           // Cars cars = _source.All(uvc, state);

           //Remember(cars, state);

           // return Pick(cars);

            string uvState = StringAppend(uvc, state);
            Car car = GenericCache.Instance.Get(uvState) as Car;
            
            if (car == null)
            {
                Cars values = null;
                values = _source.All(uvc,state);
                car = Pick(values);
                GenericCache.Instance.Add(uvState, car, CacheConstants.NoAbsoluteExpiration, CacheConstants.SlidingExpiration);
                Remember(values, state);
            }

            return car;
        }        

        /// <summary>
        /// Lookup a car by uvc and vin. A state adjustment may optionally be used.
        /// </summary>
        /// <remarks>
        /// If the car with the given uvc and vin has not been cached, we will query the BlackBook service for it.
        /// The query will look for cars matching the vin, **NOT** the uvc (since the uvc does not necessarily
        /// map correctly back to the vin if queried alone). All retrieved vehicles will be put in the database, with
        /// the one matching the uvc returned.
        /// </remarks>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="state">Two letter US state identifier. Optional.</param>
        /// <returns>Car.</returns>
        public override Car Lookup(string uvc, string vin, string state)
        {
            Verify(uvc, vin);

            
            if (Resolve<IBlackBookRepository>().CarCache_IsValid(uvc, vin, state))
            {
                try
                {
                    return Resolve<IBlackBookRepository>().Load_Car(uvc, vin, state).Car;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    // Load_car exceptioned or returned null.
                    return null;
                }
            }
            

            //string key = lookup.CreateCacheKey(year);

            //IList<string> values = Cache.Get(key) as IList<string>;

            Cars cars = _source.All(uvc, vin, state);

            Remember(cars, state);
            
            return Pick(cars, uvc, vin);
        }

        protected void Remember(Cars cars, string state)
        {
            IBlackBookRepository repository = Resolve<IBlackBookRepository>();

            BookDate bookDate = Resolve<ILookupBookDate>().Lookup();

            foreach (CarInfo carInfo in cars.Values)
            {
                Car car = carInfo as Car;

                if (car == null)
                {
                    continue;
                }

                // Save this car if it's not in the database, or if it is but has outdated values.

                CarEntity entity = repository.Load_Car(car.Uvc, car.Vin, state);

                if (entity == null || entity.HashCode != car.Hash())
                {
                    entity = repository.Save_Car(car, state, bookDate.Id);
                }

                // Extend the cache window.

                repository.CarCache_Extend(entity.CarId);
            }
        }
        protected string StringAppend(string uvc,string state)
        {
            return string.Concat(uvc, state);
        }
    }
}