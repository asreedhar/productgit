using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Lookup
{
    /// <summary>
    /// Interface for BlackBook lookups for makes.
    /// </summary>
    public interface ILookupMake
    {
        /// <summary>
        /// Lookup the makes for the cars with the given year.
        /// </summary>
        /// <remarks>
        /// BlackBook years and makes are housed internally, and so the external service will not be hit.
        /// </remarks>
        /// <param name="year">Year.</param>
        /// <returns>List of makes.</returns>
        IList<string> Lookup(string year);
    }
}