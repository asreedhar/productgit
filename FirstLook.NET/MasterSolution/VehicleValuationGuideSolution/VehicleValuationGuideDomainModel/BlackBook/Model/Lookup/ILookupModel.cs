﻿using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Lookup
{
    /// <summary>
    /// Interface for BlackBook lookups for models.
    /// </summary>
    public interface ILookupModel
    {
        /// <summary>
        /// Lookup the models for the cars with the given year and make.
        /// </summary>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        /// <returns>List of models.</returns>
        IList<CarInfo> Lookup(string year, string make);
    }
}