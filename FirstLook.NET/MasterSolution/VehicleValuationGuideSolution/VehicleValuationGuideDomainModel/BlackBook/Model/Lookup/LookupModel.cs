using System.Collections.Generic;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Xml;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Lookup
{
    /// <summary>
    /// BlackBook lookups for models.
    /// </summary>
    public class LookupModel : ILookupModel
    {
        /// <summary>
        /// Lookup the models for the cars with the given year and make.
        /// </summary>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        /// <returns>List of models.</returns>
        public IList<CarInfo> Lookup(string year, string make)
        {
            IResolver registry = RegistryFactory.GetResolver();

            IXmlService service = registry.Resolve<IXmlService>();

            IXmlSerializer<Cars> serializer = registry.Resolve<IXmlSerializer<Cars>>();

            Cars cars = serializer.Deserialize(service.Query(int.Parse(year), make));

            return cars.Values;
        }
    }
}