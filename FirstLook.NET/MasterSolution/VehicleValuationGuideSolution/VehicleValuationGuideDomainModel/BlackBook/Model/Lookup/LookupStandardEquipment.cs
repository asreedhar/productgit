using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Xml;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Lookup
{
    public class LookupStandardEquipment : ILookupStandardEquipment
    {
        public StandardEquipment Lookup(string uvc)
        {
            IResolver registry = RegistryFactory.GetResolver();

            IXmlService service = registry.Resolve<IXmlService>();

            IXmlSerializer<StandardEquipment> serializer = registry.Resolve<IXmlSerializer<StandardEquipment>>();

            StandardEquipment equipment = serializer.Deserialize(service.Uvc(uvc));
            
            return equipment;
        }
    }

    /// <summary>
    /// BlackBook lookups for standard equipment.
    /// </summary>
    public class CachedLookupStandardEquipment : ILookupStandardEquipment
    {
        private readonly LookupStandardEquipment _source = new LookupStandardEquipment();

        /// <summary>
        /// Lookup the standard equipment for the car with the given uvc.
        /// </summary>
        /// <remarks>
        /// <para>If there is a cached record of standard equipment for the car with the given uvc, and if that cache 
        /// is still valid, then that record will be returned without hitting the BlackBook service.</para>
        /// <para>Otherwise, the BlackBook service will be hit. If the retrieved record is different than the cached
        /// copy, it will be saved. Finally, the cache window will be extended.</para>
        /// </remarks>
        /// <param name="uvc">Uvc.</param>
        /// <returns>Standard equipment.</returns>
        public StandardEquipment Lookup(string uvc)
        {
            IResolver registry = RegistryFactory.GetResolver();

            IBlackBookRepository repository = registry.Resolve<IBlackBookRepository>();

            // If the cached copy is current, return that.
            if (repository.EquipmentCache_IsValid(uvc))
            {
                return repository.Load_Equipment(uvc).StandardEquipment;
            }

            StandardEquipment equipment = _source.Lookup(uvc);

            // Save to database if retrieved values are different.
            StandardEquipmentEntity entity = repository.Load_Equipment(uvc);
            if (entity == null || entity.HashCode != equipment.Hash())
            {
                BookDate bookDate = registry.Resolve<ILookupBookDate>().Lookup();
                entity = repository.Save_Equipment(uvc, equipment, bookDate.Id);
            }

            // Extend the cache window.
            repository.EquipmentCache_Extend(entity.Id);
           
            return equipment;
        }        
    }
}