﻿namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Lookup
{
    /// <summary>
    /// Interface for BlackBook lookups for cars by uvc.
    /// </summary>
    public interface ILookupUvc
    {
        /// <summary>
        /// Lookup a car by uvc. A state adjustment may optionally be used.
        /// </summary>
        /// <remarks>
        /// Due to BlackBook peculiarity, when querying by uvc alone you may end up getting a vin that is different
        /// than the one you queried BlackBook for to get the uvc in the first place. Highly recommended that you use
        /// the Lookup(uvc, vin, state) variant instead if you have vin handy.
        /// </remarks>
        /// <param name="uvc">Uvc.</param>
        /// <param name="state">Two letter US state identifier. Optional.</param>
        /// <returns>Car.</returns>
        Car Lookup(string uvc, string state);

        /// <summary>
        /// Lookup a car by uvc and vin. A state adjustment may optionally be used.
        /// </summary>
        /// <remarks>
        /// If the car with the given uvc and vin has not been cached, we will query the BlackBook service for it.
        /// The query will look for cars matching the vin, **NOT** the uvc (since the uvc does not necessarily
        /// map correctly back to the vin if queried alone). All retrieved vehicles will be put in the database, with
        /// the one matching the uvc returned.
        /// </remarks>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="state">Two letter US state identifier. Optional.</param>
        /// <returns>Car.</returns>
        Car Lookup(string uvc, string vin, string state);
    }
}
