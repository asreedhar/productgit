using System.Collections.Generic;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Xml;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Lookup
{
    /// <summary>
    /// BlackBook lookups for series.
    /// </summary>
    public class LookupSeries : ILookupSeries
    {
        /// <summary>
        /// Lookup the series for cars with the give year, make and model.
        /// </summary>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        /// <param name="model">Model.</param>
        /// <returns>List of series.</returns>
        public IList<CarInfo> Lookup(string year, string make, string model)
        {
            IResolver registry = RegistryFactory.GetResolver();

            IXmlService service = registry.Resolve<IXmlService>();

            IXmlSerializer<Cars> serializer = registry.Resolve<IXmlSerializer<Cars>>();

            Cars cars = serializer.Deserialize(service.Query(int.Parse(year), make, model));

            return cars.Values;
        }
    }
}