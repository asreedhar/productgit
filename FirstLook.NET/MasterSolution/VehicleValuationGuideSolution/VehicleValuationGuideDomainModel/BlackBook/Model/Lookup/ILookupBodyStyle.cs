﻿using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Lookup
{
    /// <summary>
    /// Interface for BlackBook lookups for body styles.
    /// </summary>
    public interface ILookupBodyStyle
    {
        /// <summary>
        /// Lookup the body styles for the car with the given year, make, model and series.
        /// </summary>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        /// <param name="model">Model.</param>
        /// <param name="series">Series.</param>
        /// <returns>List of body styles.</returns>
        IList<CarInfo> Lookup(string year, string make, string model, string series);
    }
}
