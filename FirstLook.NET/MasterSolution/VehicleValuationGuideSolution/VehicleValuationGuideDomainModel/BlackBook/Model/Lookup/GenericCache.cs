﻿using System;
using System.Web;
using System.Web.Caching;
using ICache = FirstLook.Common.Core.Cache.ICache;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Lookup
{
    /// <summary>
    /// Sigleton class to cache object
    /// </summary>
   public sealed class GenericCache: ICache
   {
           private static readonly GenericCache Single = new GenericCache();

           private readonly Cache _cache;

            private GenericCache()
            {
                _cache = HttpContext.Current.Cache;
            }

            /// <summary>
            /// Caching the object
            /// </summary>
            /// <param name="vin">Vin.</param>
            /// <returns>Cars.</returns>
            public object Add(string key, object value, DateTime absoluteExpiration, TimeSpan slidingExpiration)
            {
                return _cache.Add(
                    key,
                    value,
                    null,
                    absoluteExpiration,
                    slidingExpiration,
                    CacheItemPriority.Normal,
                    null);
            }
            public object Get(string key)
            {
                return _cache.Get(key);
            }

            public object Remove(string key)
            {
                return _cache.Remove(key);
            }
            public static GenericCache Instance
            {
                get { return Single; }
            }
   }
}
