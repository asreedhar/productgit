using System;
using FirstLook.Common.Core.Hashing;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model
{
    /// <summary>
    /// A piece of standard equipment.
    /// </summary>
    [Serializable]
    public class StandardEquipmentRecord : IHashable
    {
        /// <summary>
        /// Category the equipment belongs to.
        /// </summary>
        public string Category { get; internal set; }

        /// <summary>
        /// Subcategory the equipment belongs to.
        /// </summary>
        public string Subcategory { get; internal set; }

        /// <summary>
        /// Value of the equipment.
        /// </summary>
        public string Value { get; internal set; }

        /// <summary>
        /// Create a hash value.
        /// </summary>
        /// <returns>Hash value.</returns>
        public long Hash()
        {
            HashAlgorithm algo = new HashAlgorithm();

            algo.AddString(Category);
            algo.AddString(Subcategory);
            algo.AddString(Value);

            return algo.Value;
        }
    }
}
