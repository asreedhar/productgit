using System;
using FirstLook.Common.Core.Hashing;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model
{
    /// <summary>
    /// A mileage adjustment.
    /// </summary>
    [Serializable]
    public class MileageAdjustment : IValue, IHashable, IComparable
    {
        /// <summary>
        /// Beginning mile range.
        /// </summary>
        public int BeginRange { get; internal set; }

        /// <summary>
        /// Ending mile range.
        /// </summary>
        public int EndRange { get; internal set; }

        /// <summary>
        /// Finance advance.
        /// </summary>
        public int? FinanceAdvance { get; internal set; }

        /// <summary>
        /// Value for a car in extra clean condition.
        /// </summary>
        public int? ExtraClean { get; internal set; }

        /// <summary>
        /// Value for a car in clean condition.
        /// </summary>
        public int? Clean { get; internal set; }

        /// <summary>
        /// Value for a car in average condition.
        /// </summary>
        public int? Average { get; internal set; }

        /// <summary>
        /// Value for a car in rough condition.
        /// </summary>
        public int? Rough { get; internal set; }

        /// <summary>
        /// Does this mileage adjustment cover the given mileage?
        /// </summary>
        public bool Covers(int mileage)
        {
            return mileage >= BeginRange && mileage <= EndRange;
        }

        /// <summary>
        /// Create a hash value from this mileage adjustment.
        /// </summary>
        /// <returns></returns>
        public long Hash()
        {
            HashAlgorithm algorithm = new HashAlgorithm();

            algorithm.AddInt(BeginRange);
            algorithm.AddInt(EndRange);
            algorithm.AddInt(FinanceAdvance);
            algorithm.AddInt(ExtraClean);
            algorithm.AddInt(Clean);
            algorithm.AddInt(Average);
            algorithm.AddInt(Rough);

            return algorithm.Value;
        }

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer that indicates the relative order of the objects being compared. The return value has these meanings: Value Meaning Less than zero This instance is less than <paramref name="obj"/>. Zero This instance is equal to <paramref name="obj"/>. Greater than zero This instance is greater than <paramref name="obj"/>. 
        /// </returns>
        /// <param name="obj">An object to compare with this instance. </param><exception cref="T:System.ArgumentException"><paramref name="obj"/> is not the same type as this instance. </exception><filterpriority>2</filterpriority>
        public int CompareTo(object obj)
        {
            MileageAdjustment adj = obj as MileageAdjustment;
            if (adj == null)
            {
                throw new ArgumentException("Cannot compare to anything other than a MileageAdjustment.");
            }

            // Begin range.
            if (BeginRange < adj.BeginRange)
            {
                return -1;
            }
            if (BeginRange > adj.BeginRange)
            {
                return 1;
            }

            // End range.
            if (EndRange < adj.EndRange)
            {
                return -1;
            }
            if (EndRange > adj.EndRange)
            {
                return 1;
            }

            // Finance advance.
            if (FinanceAdvance < adj.FinanceAdvance)
            {
                return -1;
            }
            if (FinanceAdvance > adj.FinanceAdvance)
            {
                return 1;
            }

            // Extra clean.
            if (ExtraClean < adj.ExtraClean)
            {
                return -1;
            }
            if (ExtraClean > adj.ExtraClean)
            {
                return 1;
            }

            // Clean.
            if (Clean < adj.Clean)
            {
                return -1;
            }
            if (Clean > adj.Clean)
            {
                return 1;
            }

            // Average.
            if (Average < adj.Average)
            {
                return -1;
            }
            if (Average > adj.Average)
            {
                return 1;
            }

            // Rough.
            if (Rough < adj.Rough)
            {
                return -1;
            }
            if (Rough > adj.Rough)
            {
                return 1;
            }

            return 0;
        }
    }
}
