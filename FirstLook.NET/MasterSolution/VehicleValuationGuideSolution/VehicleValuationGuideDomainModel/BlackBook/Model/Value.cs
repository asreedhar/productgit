using System;
using FirstLook.Common.Core.Hashing;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model
{
    /// <summary>
    /// A vehicle valuation.
    /// </summary>
    [Serializable]
    public class Value : IValue, IHashable
    {
        /// <summary>
        /// Value for a car in extra clean condition.
        /// </summary>
        public int? ExtraClean { get; internal set; }

        /// <summary>
        /// Value for a car in clean condition.
        /// </summary>
        public int? Clean { get; internal set; }

        /// <summary>
        /// Value for a car in average condition.
        /// </summary>
        public int? Average { get; internal set; }

        /// <summary>
        /// Value for a car in rough condition.
        /// </summary>
        public int? Rough { get; internal set; }

        /// <summary>
        /// Minimum value from this valuation.
        /// </summary>
        public int? Minimum
        {
            get
            {
                int?[] values = new[] {ExtraClean, Clean, Average, Rough};

                int? min = null;

                foreach (int? value in values)
                {
                    min = Min(min, value);
                }

                return min;
            }
        }

        /// <summary>
        /// Compare two nullable ints for the lower value.
        /// </summary>
        /// <param name="a">Value to compare.</param>
        /// <param name="b">Value to compare.</param>
        /// <returns>Lowest value.</returns>
        internal static int? Min(int? a, int? b)
        {
            if (a.HasValue)
            {
                if (b.HasValue)
                {
                    if (a > b)
                    {
                        a = b;
                    }
                }
            }
            else
            {
                if (b.HasValue)
                {
                    a = b;
                }
            }
            return a;
        }

        /// <summary>
        /// Create a hash value from this valuation.
        /// </summary>
        /// <returns>Hash value.</returns>
        public long Hash()
        {
            HashAlgorithm algorithm = new HashAlgorithm();
            
            algorithm.AddInt(Average);
            algorithm.AddInt(Clean);
            algorithm.AddInt(ExtraClean);
            algorithm.AddInt(Rough);
            
            return algorithm.Value;
        }
    }
}
