﻿using System.Configuration;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Configuration
{
    /// <summary>
    /// BlackBook service details.
    /// </summary>
    public class BlackBookConfigurationSection : ConfigurationSection
    {
        /// <summary>
        /// Default host name of the BlackBook service.
        /// </summary>
        public const string DefaultHostName = "www.blackbookportals.com";

        /// <summary>
        /// Default port of the BlackBook service.
        /// </summary>
        public const int DefaultPort = 80;

        /// <summary>
        /// Default url path of the BlackBook service.
        /// </summary>
        public const string DefaultUrlPath = "/bb/products/usedcarxmlportal.asp";

        /// <summary>
        /// Default timeout for BlackBook requests.
        /// </summary>
        public const int DefaultRequestTimeout = 5;

        /// <summary>
        /// Host name of the BlackBook service.
        /// </summary>
        [ConfigurationProperty("hostName", IsRequired = false, DefaultValue = DefaultHostName)]
        public string HostName
        {
            get { return (string)this["hostName"]; }
            set { this["hostName"] = value; }
        }

        /// <summary>
        /// Port of the BlackBook service.
        /// </summary>
        [ConfigurationProperty("port", IsRequired = false, DefaultValue = DefaultPort)]
        public int Port
        {
            get { return (int)this["port"]; }
            set { this["port"] = value; }
        }

        /// <summary>
        /// Url path of the BlackBook service.
        /// </summary>
        [ConfigurationProperty("urlPath", IsRequired = true, DefaultValue = DefaultUrlPath)]
        public string UrlPath
        {
            get { return (string)this["urlPath"]; }
            set { this["urlPath"] = value; }
        }

        /// <summary>
        /// Timeout for BlackBook requests.
        /// </summary>
        [ConfigurationProperty("requestTimeout", IsRequired = false, DefaultValue = DefaultRequestTimeout)]
        public int RequestTimeout
        {
            get { return (int)this["requestTimeout"]; }
            set { this["requestTimeout"] = value; }
        }
    }
}
