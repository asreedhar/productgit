﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using System.Linq;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Configuration;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Xml
{
    /// <summary>
    /// Xml service.
    /// </summary>
    public class XmlService : IXmlService
    {
        const string HTTP_PROXY = "ord4-ops-01";
        const int HTTP_PROXY_PORT = 3128;


        /// <summary>
        /// Query by year and make.
        /// </summary>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        /// <returns>Xml reader with results.</returns>
        public XmlReader Query(int year, string make)
        {
            ValidateYear(year);

            ValidateMake(make);

            XmlQuery query = new XmlQuery();

            query.Query(year, make);

            return DoRequest(query.ToString());
        }

        /// <summary>
        /// Query by year, make and model.
        /// </summary>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        /// <param name="model">Model.</param>
        /// <returns>Xml reader with results.</returns>
        public XmlReader Query(int year, string make, string model)
        {
            ValidateYear(year);

            ValidateMake(make);

            ValidateModel(model);

            XmlQuery query = new XmlQuery();

            query.Query(year, make, model);

            return DoRequest(query.ToString());
        }

        /// <summary>
        /// Query by year, make and model with a state and / or mileage adjustment.
        /// </summary>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        /// <param name="model">Model.</param>
        /// <param name="adjustment">State and / or mileage adjustment.</param>
        /// <returns>Xml reader with results.</returns>
        public XmlReader Query(int year, string make, string model, XmlAdjustment adjustment)
        {
            ValidateYear(year);

            ValidateMake(make);

            ValidateModel(model);

            ValidateAdjustment(adjustment);

            XmlQuery query = new XmlQuery();

            query.Query(year, make, model, adjustment);

            return DoRequest(query.ToString());
        }

        /// <summary>
        /// Query by year, make, model and series.
        /// </summary>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        /// <param name="model">Model.</param>
        /// <param name="series">Series.</param>
        /// <returns>Xml reader with results.</returns>
        public XmlReader Query(int year, string make, string model, string series)
        {
            ValidateYear(year);

            ValidateMake(make);

            ValidateModel(model);

            ValidateSeries(series);

            XmlQuery query = new XmlQuery();

            query.Query(year, make, model, series);

            return DoRequest(query.ToString());
        }

        /// <summary>
        /// Query by year, make, model and series with a state and / or mileage adjustment.
        /// </summary>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        /// <param name="model">Model.</param>
        /// <param name="series">Series.</param>
        /// <param name="adjustment">Adjustment.</param>
        /// <returns>Xml reader with results.</returns>
        public XmlReader Query(int year, string make, string model, string series, XmlAdjustment adjustment)
        {
            ValidateYear(year);

            ValidateMake(make);

            ValidateModel(model);

            ValidateSeries(series);

            ValidateAdjustment(adjustment);

            XmlQuery query = new XmlQuery();

            query.Query(year, make, model, series, adjustment);

            return DoRequest(query.ToString());
        }

        /// <summary>
        /// Query by vin.
        /// </summary>
        /// <param name="vin">Vin.</param>
        /// <returns>Xml reader with results.</returns>
        public XmlReader Vin(string vin)
        {
            ValidateVin(vin);

            XmlQuery query = new XmlQuery();

            query.Vin(vin);

            return DoRequest(query.ToString());
        }

        /// <summary>
        /// Query by vin with state and / or mileage adjustment.
        /// </summary>
        /// <param name="vin">Vin.</param>
        /// <param name="adjustment">State and / or mileage adjustment.</param>
        /// <returns>Xml reader.</returns>
        public XmlReader Vin(string vin, XmlAdjustment adjustment)
        {
            ValidateVin(vin);

            ValidateAdjustment(adjustment);

            XmlQuery query = new XmlQuery();

            query.Vin(vin, adjustment);

            return DoRequest(query.ToString());
        }

        /// <summary>
        /// Query by uvc.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <returns>Xml reader with results.</returns>
        public XmlReader Uvc(string uvc)
        {
            ValidateUvc(uvc);

            XmlQuery query = new XmlQuery();

            query.Uvc(uvc);

            return DoRequest(query.ToString());
        }

        /// <summary>
        /// Query by uvc and state and / or mileage adjustment.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="adjustment">Adjustment.</param>
        /// <returns>Xml reader with results.</returns>
        public XmlReader Uvc(string uvc, XmlAdjustment adjustment)
        {
            ValidateUvc(uvc);

            ValidateAdjustment(adjustment);

            XmlQuery query = new XmlQuery();

            query.Uvc(uvc, adjustment);

            return DoRequest(query.ToString());
        }

        /// <summary>
        /// Query for standard equipment by uvc.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <returns>Xml reader with results.</returns>
        public XmlReader StandardEquipment(string uvc)
        {
            ValidateUvc(uvc);

            XmlQuery query = new XmlQuery();

            query.Equipment(uvc);

            return DoRequest(query.ToString());
        }

        /// <summary>
        /// Perform a request.
        /// </summary>
        /// <param name="uriText">Uri text.</param>
        /// <returns>Xml reader.</returns>
        private static XmlReader DoRequest(string uriText)
        {
            HttpWebRequest request = ConstructWebRequest(uriText);

            WebClient client = new WebClient();
            #if DEBUG
                client.Proxy = request.Proxy;
/*
                var r = client.DownloadString(request.RequestUri);
                Console.WriteLine(r);
*/
            #endif

            using (WebResponse response = request.GetResponse())
            {
                using (XmlTextReader reader = new XmlTextReader(response.GetResponseStream()))
                {
                    return XmlReader.Create(ToMemoryStream(reader));
                }
            }

        }

        /// <summary>
        /// Convert the given xml reader to a memory stream.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <returns>Memory stream.</returns>
        internal static MemoryStream ToMemoryStream(XmlReader reader)
        {
            return ToMemoryStream(reader, new UTF8Encoding(false));
        }

        /// <summary>
        /// Convert the given xml reader with the given encoding to a memory stream.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <param name="encoding">Encoding.</param>
        /// <returns>Memory stream.</returns>
        internal static MemoryStream ToMemoryStream(XmlReader reader, Encoding encoding)
        {
            MemoryStream stream = new MemoryStream();

            XmlTextWriter wr = new XmlTextWriter(stream, encoding);

            wr.WriteNode(reader, true);

            wr.Flush();

            stream.Seek(0, SeekOrigin.Begin);

            return stream;
        }

        /// <summary>
        /// Convert the xml text reader to a memory stream.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <returns>Memory stream.</returns>
        internal static MemoryStream ToMemoryStream(XmlTextReader reader)
        {
            return ToMemoryStream(reader, reader.Encoding);
        }

        /// <summary>
        /// Construct a web request.
        /// </summary>
        /// <param name="uriText">Uri text.</param>
        /// <returns>Http web request.</returns>
        private static HttpWebRequest ConstructWebRequest(string uriText)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uriText);
            request.Method = "GET";
            request.ContentType = "text/xml";
            request.AllowAutoRedirect = true;

            // use an http proxy in debug mode
            #if DEBUG
                request.Proxy = new WebProxy(HTTP_PROXY,HTTP_PROXY_PORT);
            #endif
            
            // request.Timeout = Timeout;
            return request;
        }

        /// <summary>
        /// Validate a year.
        /// </summary>
        /// <param name="year">Year.</param>
        private static void ValidateYear(int year)
        {
            if (year < 1980 || year > 2020)
            {
                throw new ArgumentOutOfRangeException("year", year, "1980 <= Year <= 2020");
            }
        }

        /// <summary>
        /// Validate a make.
        /// </summary>
        /// <param name="make">Make.</param>
        private static void ValidateMake(string make)
        {
            if (string.IsNullOrEmpty(make))
            {
                throw new ArgumentNullException("make", "Make cannot be neither null nor empty");
            }
        }

        /// <summary>
        /// Validate a model.
        /// </summary>
        /// <param name="model">Model.</param>
        private static void ValidateModel(string model)
        {
            if (string.IsNullOrEmpty(model))
            {
                throw new ArgumentNullException("model", "Model cannot be neither null nor empty");
            }
        }

        /// <summary>
        /// Validate a series.
        /// </summary>
        /// <param name="series">Series. Okay if empty.</param>
        private static void ValidateSeries(string series)
        {
            // Empty string is ok -- see "2009 - Lexus - ES 350 -  - 4D Luxury Sedan"           
            if (series == null)
            {
                throw new ArgumentNullException("series", "Series cannot be null");
            }
        }

        /// <summary>
        /// Validate a vin.
        /// </summary>
        /// <param name="vin">Vin.</param>
        private static void ValidateVin(string vin)
        {
            if (string.IsNullOrEmpty(vin))
            {
                throw new ArgumentNullException("vin", "Vin cannot be neither null nor empty");
            }

            if (vin.Length != 17)
            {
                throw new ArgumentOutOfRangeException("vin", "Length(Vin) != 17");
            }
        }

        /// <summary>
        /// Validate a uvc.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        private static void ValidateUvc(string uvc)
        {
            if (string.IsNullOrEmpty(uvc))
            {
                throw new ArgumentNullException("uvc", "UVC cannot be neither null nor empty");
            }
        }

        /// <summary>
        /// Validate a state and / or mileage adjustment.
        /// </summary>
        /// <param name="adjustment">State and / or mileage adjustment.</param>
        private static void ValidateAdjustment(XmlAdjustment adjustment)
        {
            if (adjustment != null)
            {
                if (!string.IsNullOrEmpty(adjustment.State))
                {
                    if (null == State.Values.First(value => Equals(adjustment.State, value.Code)))
                    {
                        throw new ArgumentException("Unrecognized state", "adjustment");
                    }
                }

                if (adjustment.Mileage.HasValue && adjustment.Mileage < 0)
                {
                    throw new ArgumentOutOfRangeException("adjustment", adjustment.Mileage, "Mileage < 0");
                }
            }
        }

        private static int _timeout;

        /// <summary>
        /// Service timeout.
        /// </summary>
        public static int Timeout
        {
            get
            {
                if (_timeout == 0)
                {
                    BlackBookConfigurationSection configuration = (BlackBookConfigurationSection)ConfigurationManager.GetSection("blackBook") ??
                                                                  new BlackBookConfigurationSection();
                    _timeout = configuration.RequestTimeout;
                }

                return _timeout;
            }
        }
    }
}
