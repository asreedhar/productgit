﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Xml
{
    /// <summary>
    /// Standard equipment serializer.
    /// </summary>
    public class StandardEquipmentSerializer : IXmlSerializer<StandardEquipment>
    {
        /// <summary>
        /// Deserialize standard equipment from the given xml reader.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <returns>Standard equipment.</returns>
        public StandardEquipment Deserialize(XmlReader reader)
        {
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        if ("stdequipment".Equals(reader.Name))
                        {
                            return StdEquipment(reader);
                        }
                        break;
                }
            }

            throw new NotSupportedException("Bad Parser/XML");
        }

        /// <summary>
        /// Deserialize standard equipment from the given xml reader.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <returns>Standard equipment.</returns>
        private static StandardEquipment StdEquipment(XmlReader reader)
        {
            StandardEquipment equipment = new StandardEquipment();

            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        if ("categories".Equals(reader.Name))
                        {
                            equipment.Categories = Categories(reader);
                        }
                        else if ("stdequiprec".Equals(reader.Name))
                        {
                            equipment.StandardEquipmentRecords.Add(StdEquipmentRec(reader));
                        }
                        break;
                    case XmlNodeType.EndElement:
                        if ("stdequipment".Equals(reader.Name))
                        {
                            return equipment;
                        }
                        break;
                }
            }

            throw new NotSupportedException("Bad Parser");
        }

        /// <summary>
        /// Deserialize a list of equipment categories from the given xml reader.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <returns>List of equipment categories.</returns>
        private static IList<string> Categories(XmlReader reader)
        {
            List<string> categories = new List<string>();

            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        if ("category".Equals(reader.Name))
                        {
                            categories.Add(ElementText(reader, "category"));
                        }
                        break;
                    case XmlNodeType.EndElement:
                        if ("categories".Equals(reader.Name))
                        {
                            return categories;
                        }
                        break;
                }
            }

            throw new NotSupportedException("Bad Parser");
        }

        /// <summary>
        /// Deserialize a standard equipment record from the given xml reader.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <returns>Standard equipment record.</returns>
        private static StandardEquipmentRecord StdEquipmentRec(XmlReader reader)
        {
            StandardEquipmentRecord record = new StandardEquipmentRecord();

            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        if ("category".Equals(reader.Name))
                        {
                            record.Category = ElementText(reader, "category");
                        }
                        if ("subcat".Equals(reader.Name))
                        {
                            record.Subcategory = ElementText(reader, "subcat");
                        }
                        if ("value".Equals(reader.Name))
                        {
                            record.Value = ElementText(reader, "value");
                        }
                        break;
                    case XmlNodeType.EndElement:
                        if ("stdequiprec".Equals(reader.Name))
                        {
                            return record;
                        }
                        break;
                }
            }

            throw new NotSupportedException("Bad Parser");
        }

        /// <summary>
        /// Deserialize text from the given xml reader.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <param name="elementName">Element name.</param>
        /// <returns>String.</returns>
        private static string ElementText(XmlReader reader, IEquatable<string> elementName)
        {
            if (reader.IsEmptyElement)
            {
                return string.Empty;
            }

            StringBuilder sb = new StringBuilder();

            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Whitespace:
                    case XmlNodeType.Text:
                        sb.Append(reader.Value);
                        break;
                    case XmlNodeType.EndElement:
                        if (elementName.Equals(reader.Name))
                        {
                            return sb.ToString();
                        }
                        break;
                }
            }

            throw new NotSupportedException("Bad Parser");
        }
    }
}