using System.Xml;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Xml
{
    /// <summary>
    /// Interface for an xml serializer.
    /// </summary>
    /// <typeparam name="T">Type for serialization.</typeparam>
    public interface IXmlSerializer<T>
    {
        /// <summary>
        /// Deserialize an object of type T from the given xml reader.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <returns>Object of type T.</returns>
        T Deserialize(XmlReader reader);
    }
}