﻿using System;
using System.Configuration;
using System.Text;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Configuration;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Xml
{
    /// <summary>
    /// Methods for constructing queries for BlackBook's xml service.
    /// </summary>
    public class XmlQuery
    {
        /// <summary>
        /// Query string.
        /// </summary>
        private readonly StringBuilder _sb;

        /// <summary>
        /// Query parameter separator.
        /// </summary>
        private char _qs;

        /// <summary>
        /// Is the query string frozen, i.e. has it already been constructed?
        /// </summary>
        private bool _frozen;

        /// <summary>
        /// Configure the query.
        /// </summary>
        public XmlQuery()
        {
            BlackBookConfigurationSection configuration = (BlackBookConfigurationSection)ConfigurationManager.GetSection("blackBook") ??
                                                          new BlackBookConfigurationSection();

            _sb = new StringBuilder();

            _sb.Append("http://").Append(configuration.HostName);

            if (configuration.Port != 80)
            {
                _sb.Append(":").Append(configuration.Port);
            }

            _sb.Append("/").Append(configuration.UrlPath);

            _qs = '?';
        }

        /// <summary>
        /// Convert this query to a string.
        /// </summary>
        /// <returns>String query.</returns>
        public override string ToString()
        {
            return _sb.ToString();
        }

        /// <summary>
        /// Construct a query for vehicles by year and make.
        /// </summary>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        public void Query(int year, string make)
        {
            EnsureNotFrozen();

            AppendYear(year);

            AppendMake(make);

            Freeze();
        }

        /// <summary>
        /// Construct a query for vehicles by year, make and model.
        /// </summary>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        /// <param name="model">Model.</param>
        public void Query(int year, string make, string model)
        {
            EnsureNotFrozen();

            AppendYear(year);

            AppendMake(make);

            AppendModel(model);

            Freeze();
        }

        /// <summary>
        /// Construct a query for vehicles by year, make and model, with optional state and / or mileage adjustments.        
        /// </summary>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        /// <param name="model">Model.</param>
        /// <param name="adjustment">State and / or mileage adjustment.</param>
        public void Query(int year, string make, string model, XmlAdjustment adjustment)
        {
            EnsureNotFrozen();

            AppendYear(year);

            AppendMake(make);

            AppendModel(model);

            AppendAdjustment(adjustment);

            Freeze();
        }

        /// <summary>
        /// Construct a query for vehicles by year, make, model and series.
        /// </summary>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        /// <param name="model">Model.</param>
        /// <param name="series">Series.</param>
        public void Query(int year, string make, string model, string series)
        {
            EnsureNotFrozen();

            AppendYear(year);

            AppendMake(make);

            AppendModel(model);

            AppendSeries(series);

            AppendAllVins();

            Freeze();
        }

        /// <summary>
        /// Construct a query for vehicles by year, make, model and series with an optional state and / or mileage
        /// adjustment.        
        /// </summary>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        /// <param name="model">Model.</param>
        /// <param name="series">Series.</param>
        /// <param name="adjustment">State and / or mileage adjustment.</param>
        public void Query(int year, string make, string model, string series, XmlAdjustment adjustment)
        {
            EnsureNotFrozen();

            AppendYear(year);

            AppendMake(make);

            AppendModel(model);

            AppendSeries(series);

            AppendAdjustment(adjustment);

            AppendAllVins();

            Freeze();
        }

        /// <summary>
        /// Construct a query for vehicles by vin.
        /// </summary>
        /// <param name="vin">Vin.</param>
        public void Vin(string vin)
        {
            EnsureNotFrozen();

            AppendVin(vin);

            AppendCheckDigit();

            Freeze();
        }

        /// <summary>
        /// Construct a query for vehicles by vin with an optional state and / or mileage adjustment.
        /// </summary>
        /// <param name="vin">Vin.</param>
        /// <param name="adjustment">State and / or mileage adjustment.</param>
        public void Vin(string vin, XmlAdjustment adjustment)
        {
            EnsureNotFrozen();

            AppendVin(vin);

            AppendAdjustment(adjustment);

            AppendCheckDigit();

            Freeze();
        }

        /// <summary>
        /// Construct a query for vehicles by uvc.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        public void Uvc(string uvc)
        {
            EnsureNotFrozen();

            AppendUvc(uvc);

            AppendAllVins();

            Freeze();
        }

        /// <summary>
        /// Construct a query for vehicles by uvc with an optional state and / or mileage adjustment.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="adjustment">State and / or mileage adjustment.</param>
        public void Uvc(string uvc, XmlAdjustment adjustment)
        {
            EnsureNotFrozen();

            AppendUvc(uvc);

            AppendAdjustment(adjustment);

            AppendAllVins();

            Freeze();
        }

        /// <summary>
        /// Construct a query for a vehicle's equipment by uvc.        
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        public void Equipment(string uvc)
        {
            EnsureNotFrozen();

            AppendUvc(uvc);

            AppendCategories();

            Freeze();
        }

        /// <summary>
        /// Add a year to the query string.
        /// </summary>
        /// <param name="year">Year.</param>
        private void AppendYear(int year)
        {
            _sb.Append(QuerySeperator()).Append("year=").Append(year);
        }

        /// <summary>
        /// Add a make to the query string.
        /// </summary>
        /// <param name="make">Make.</param>
        private void AppendMake(string make)
        {
            _sb.Append(QuerySeperator()).Append("make=").Append(Escape(make));
        }

        /// <summary>
        /// Add a model to the query string.
        /// </summary>
        /// <param name="model">Model.</param>
        private void AppendModel(string model)
        {
            _sb.Append(QuerySeperator()).Append("model=").Append(Escape(model));
        }

        /// <summary>
        /// Add a series to the query string.
        /// </summary>
        /// <param name="series">Series.</param>
        private void AppendSeries(string series)
        {
            _sb.Append(QuerySeperator()).Append("series=").Append(Escape(series));
        }

        /// <summary>
        /// Add a vin to the query string.
        /// </summary>
        /// <param name="vin">Vin.</param>
        private void AppendVin(string vin)
        {
            _sb.Append(QuerySeperator()).Append("vin=").Append(vin);
        }

        /// <summary>
        /// Add a uvc to the query string.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        private void AppendUvc(string uvc)
        {
            _sb.Append(QuerySeperator()).Append("uvc=").Append(uvc);
        }

        /// <summary>
        /// Add a state and / or mileage adjustment to the query string.
        /// </summary>
        /// <param name="adjustment">State and / or mileage adjustment.</param>
        private void AppendAdjustment(XmlAdjustment adjustment)
        {
            if (adjustment != null)
            {
                if (adjustment.Mileage.HasValue && adjustment.Mileage.Value >= 0)
                {
                    _sb.Append(QuerySeperator()).Append("mileage=").Append(adjustment.Mileage.Value);
                }

                if (!string.IsNullOrEmpty(adjustment.State))
                {
                    _sb.Append(QuerySeperator()).Append("state=").Append(adjustment.State);
                }
            }
        }

        /// <summary>
        /// Add the option to ask for all matching vins to the query string.
        /// </summary>
        private void AppendAllVins()
        {
            _sb.Append(QuerySeperator()).Append("allvins=Y");
        }

        /// <summary>
        /// Add the option to include all categories to the query string.
        /// </summary>
        private void AppendCategories()
        {
            _sb.Append(QuerySeperator()).Append("categories=ALL");
        }

        /// <summary>
        /// Add the option to instruct BlackBook to validate the vin to the query string.
        /// </summary>
        private void AppendCheckDigit()
        {
            _sb.Append(QuerySeperator()).Append("chkdigit=Y");
        }

        /// <summary>
        /// Return the separator between parts of the query string. First separator is '?', all subsequent are '&'.
        /// </summary>
        /// <returns>Query string separator.</returns>
        private char QuerySeperator()
        {
            char t = _qs;

            if (_qs == '?')
            {
                _qs = '&';
            }

            return t;
        }

        /// <summary>
        /// Make sure the query string hasn't been marked as being frozen.
        /// </summary>
        private void EnsureNotFrozen()
        {
            if (_frozen)
            {
                throw new NotSupportedException("Query Frozen");
            }
        }

        /// <summary>
        /// Freeze the query string - prevents additional parameters from being added once the query string has been
        /// constructed.
        /// </summary>
        private void Freeze()
        {
            _frozen = true;
        }

        /// <summary>
        /// Ensure the given value is properly escaped.
        /// </summary>
        /// <param name="value">String to escape.</param>
        /// <returns>Escaped string.</returns>
        private static string Escape(string value)
        {
            return Uri.EscapeDataString(value);
        }
    }
}
