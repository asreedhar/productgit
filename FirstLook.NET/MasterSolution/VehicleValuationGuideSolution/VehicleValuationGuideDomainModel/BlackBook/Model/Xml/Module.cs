﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Xml
{
    /// <summary>
    /// Module for registering BlackBook xml service components.
    /// </summary>
    public class Module : IModule
    {
        /// <summary>
        /// Module for registering xml serializers.
        /// </summary>
        public class Serialization : IModule
        {
            /// <summary>
            /// Register xml serializers.
            /// </summary>
            /// <param name="registry">Registry.</param>
            public void Configure(IRegistry registry)
            {
                registry.Register<IXmlSerializer<Cars>, CarsSerializer>(ImplementationScope.Shared);
                registry.Register<IXmlSerializer<StandardEquipment>, StandardEquipmentSerializer>(ImplementationScope.Shared);
            }
        }

        /// <summary>
        /// Register xml components.
        /// </summary>
        /// <param name="registry"></param>
        public void Configure(IRegistry registry)
        {
            registry.Register<Serialization>();
            registry.Register<IXmlService, CachingXmlService>(ImplementationScope.Shared);
        }
    }
}
