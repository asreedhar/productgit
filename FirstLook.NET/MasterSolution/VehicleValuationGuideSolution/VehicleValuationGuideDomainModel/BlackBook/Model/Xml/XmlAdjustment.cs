﻿namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Xml
{
    /// <summary>
    /// State or mileage adjustment.
    /// </summary>
    public class XmlAdjustment
    {
        private string _state;
        private int? _mileage;

        /// <summary>
        /// State adjustment.
        /// </summary>
        public string State
        {
            get { return _state; }
            set { _state = value; }
        }

        /// <summary>
        /// Mileage adjustment.
        /// </summary>
        public int? Mileage
        {
            get { return _mileage; }
            set { _mileage = value; }
        }

        /// <summary>
        /// Stringify this adjustment.
        /// </summary>
        /// <returns>String representation of this adjustment.</returns>
        public override string ToString()
        {
            return string.Format("State: {0}, Mileage: {1}", _state, _mileage.HasValue ? _mileage.Value : -1);
        }
    }
}
