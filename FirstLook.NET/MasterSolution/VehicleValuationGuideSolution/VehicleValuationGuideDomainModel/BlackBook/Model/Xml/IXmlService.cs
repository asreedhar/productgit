﻿using System.Xml;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Xml
{
    /// <summary>
    /// Xml BlackBook service.
    /// </summary>
    public interface IXmlService
    {        
        /// <summary>
        /// Query by year and make.
        /// </summary>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        /// <returns>Xml reader with results.</returns>
        XmlReader Query(int year, string make);

        /// <summary>
        /// Query by year, make and model.
        /// </summary>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        /// <param name="model">Model.</param>
        /// <returns>Xml reader with results.</returns>
        XmlReader Query(int year, string make, string model);

        /// <summary>
        /// Query by year, make, model and state and / or mileage adjustment.
        /// </summary>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        /// <param name="model">Model.</param>
        /// <param name="adjustment">State and / or mileage adjustment.</param>
        /// <returns>Xml reader with results.</returns>
        XmlReader Query(int year, string make, string model, XmlAdjustment adjustment);

        /// <summary>
        /// Query by year, make, model and series.
        /// </summary>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        /// <param name="model">Model.</param>
        /// <param name="series">Series.</param>
        /// <returns>Xml reader with results.</returns>
        XmlReader Query(int year, string make, string model, string series);

        /// <summary>
        /// Query by year, make, model, series and state and / or mileage adjustment.
        /// </summary>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        /// <param name="model">Model.</param>
        /// <param name="series">Series.</param>
        /// <param name="adjustment">State and / or mileage adjustment.</param>
        /// <returns>Xml reader with results.</returns>
        XmlReader Query(int year, string make, string model, string series, XmlAdjustment adjustment);

        /// <summary>
        /// Query by vin.
        /// </summary>
        /// <param name="vin">Vin.</param>
        /// <returns>Xml reader with results.</returns>
        XmlReader Vin(string vin);

        /// <summary>
        /// Query by vin with state and / or mileage adjustment.
        /// </summary>
        /// <param name="vin">Vin.</param>
        /// <param name="adjustment">State and / or mileage adjustment.</param>
        /// <returns>Xml reader.</returns>
        XmlReader Vin(string vin, XmlAdjustment adjustment);

        /// <summary>
        /// Query by uvc.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <returns>Xml reader with results.</returns>
        XmlReader Uvc(string uvc);

        /// <summary>
        /// Query by uvc and state and / or mileage adjustment.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="adjustment">Adjustment.</param>
        /// <returns>Xml reader with results.</returns>
        XmlReader Uvc(string uvc, XmlAdjustment adjustment);

        /// <summary>
        /// Query for standard equipment by uvc.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <returns>Xml reader with results.</returns>
        XmlReader StandardEquipment(string uvc);
    }
}
