﻿using System.Globalization;
using System.IO;
using System.Text;
using System.Xml;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Xml
{
    /// <summary>
    /// Caching xml service.
    /// </summary>
    public class CachingXmlService : IXmlService
    {
        /// <summary>
        /// Xml service.
        /// </summary>
        private readonly IXmlService _service = new XmlService();

        /// <summary>
        /// Fetch delegate.
        /// </summary>
        /// <returns></returns>
        private delegate XmlReader Fetch();

        /// <summary>
        /// Query by year and make.
        /// </summary>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        /// <returns>Xml reader with results.</returns>
        public XmlReader Query(int year, string make)
        {
            string key = CreateCacheKey(year, make);

            byte[] bytes = GetBytes(() => _service.Query(year, make), key);

            return XmlReader.Create(new MemoryStream(bytes));
        }

        /// <summary>
        /// Query by year, make and model.
        /// </summary>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        /// <param name="model">Model.</param>
        /// <returns>Xml reader with results.</returns>
        public XmlReader Query(int year, string make, string model)
        {
            string key = CreateCacheKey(year, make, model);

            byte[] bytes = GetBytes(() => _service.Query(year, make, model), key);

            return XmlReader.Create(new MemoryStream(bytes));
        }

        /// <summary>
        /// Query by year, make and model with a state and / or mileage adjustment.
        /// </summary>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        /// <param name="model">Model.</param>
        /// <param name="adjustment">State and / or mileage adjustment.</param>
        /// <returns>Xml reader with results.</returns>
        public XmlReader Query(int year, string make, string model, XmlAdjustment adjustment)
        {
            string key = CreateCacheKey(year, make, model, adjustment);

            byte[] bytes = GetBytes(() => _service.Query(year, make, model, adjustment), key);

            return XmlReader.Create(new MemoryStream(bytes));
        }

        /// <summary>
        /// Query by year, make, model and series.
        /// </summary>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        /// <param name="model">Model.</param>
        /// <param name="series">Series.</param>
        /// <returns>Xml reader with results.</returns>
        public XmlReader Query(int year, string make, string model, string series)
        {
            string key = CreateCacheKey(year, make, model, series);

            byte[] bytes = GetBytes(() => _service.Query(year, make, model, series), key);

            return XmlReader.Create(new MemoryStream(bytes));
        }

        /// <summary>
        /// Query by year, make, model and series with a state and / or mileage adjustment.
        /// </summary>
        /// <param name="year">Year.</param>
        /// <param name="make">Make.</param>
        /// <param name="model">Model.</param>
        /// <param name="series">Series.</param>
        /// <param name="adjustment">Adjustment.</param>
        /// <returns>Xml reader with results.</returns>
        public XmlReader Query(int year, string make, string model, string series, XmlAdjustment adjustment)
        {
            string key = CreateCacheKey(year, make, model, series, adjustment);

            byte[] bytes = GetBytes(() => _service.Query(year, make, model, series, adjustment), key);

            return XmlReader.Create(new MemoryStream(bytes));
        }

        /// <summary>
        /// Query by vin.
        /// </summary>
        /// <param name="vin">Vin.</param>
        /// <returns>Xml reader with results.</returns>
        public XmlReader Vin(string vin)
        {
            string key = CreateCacheKey("vin", vin);

            byte[] bytes = GetBytes(() => _service.Vin(vin), key);

            return XmlReader.Create(new MemoryStream(bytes));
        }

        /// <summary>
        /// Query by vin with state and / or mileage adjustment.
        /// </summary>
        /// <param name="vin">Vin.</param>
        /// <param name="adjustment">State and / or mileage adjustment.</param>
        /// <returns>Xml reader.</returns>
        public XmlReader Vin(string vin, XmlAdjustment adjustment)
        {
            string key = CreateCacheKey("vin", vin, adjustment);

            byte[] bytes = GetBytes(() => _service.Vin(vin, adjustment), key);

            return XmlReader.Create(new MemoryStream(bytes));
        }

        /// <summary>
        /// Query by uvc.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <returns>Xml reader with results.</returns>
        public XmlReader Uvc(string uvc)
        {
            string key = CreateCacheKey("uvc", uvc);

            byte[] bytes = GetBytes(() => _service.Uvc(uvc), key);

            return XmlReader.Create(new MemoryStream(bytes));
        }

        /// <summary>
        /// Query by uvc and state and / or mileage adjustment.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="adjustment">Adjustment.</param>
        /// <returns>Xml reader with results.</returns>
        public XmlReader Uvc(string uvc, XmlAdjustment adjustment)
        {
            string key = CreateCacheKey("uvc", uvc, adjustment);

            byte[] bytes = GetBytes(() => _service.Uvc(uvc, adjustment), key);

            return XmlReader.Create(new MemoryStream(bytes));
        }

        /// <summary>
        /// Query for standard equipment by uvc.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <returns>Xml reader with results.</returns>
        public XmlReader StandardEquipment(string uvc)
        {
            string key = CreateCacheKey("std", uvc);

            byte[] bytes = GetBytes(() => _service.StandardEquipment(uvc), key);

            return XmlReader.Create(new MemoryStream(bytes));
        }

        /// <summary>
        /// Get bytes from the given delegate with the given key.
        /// </summary>
        /// <param name="fetch">Fetch delegate.</param>
        /// <param name="key">Key.</param>
        /// <returns>Byte array of results.</returns>
        private byte[] GetBytes(Fetch fetch, string key)
        {
            byte[] bytes = Cache.Get(key) as byte[];

            if (bytes == null)
            {
                XmlReader reader = fetch();

                MemoryStream ms = XmlService.ToMemoryStream(reader);

                bytes = ms.ToArray();

                Cache.Add(key, bytes, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return bytes;
        }

        /// <summary>
        /// Create a cache key from the given parameters.
        /// </summary>
        /// <param name="parameters">Array of parameters.</param>
        /// <returns>String cache key.</returns>
        protected string CreateCacheKey(params object[] parameters)
        {
            StringBuilder sb = new StringBuilder("u", 1024);

            sb.Append(GetType().GetHashCode().ToString(CultureInfo.InvariantCulture));

            if (parameters.Length > 0)
            {
                sb.Append("?");

                foreach (object parameter in parameters)
                {
                    sb.Append(parameter);
                    sb.Append("&");
                }

                sb.Length = sb.Length - 1;
            }

            return sb.ToString();
        }

        /// <summary>
        /// Helper for resolving the cache.
        /// </summary>
        protected ICache Cache
        {
            get
            {
                return RegistryFactory.GetResolver().Resolve<ICache>();
            }
        }
    }
}