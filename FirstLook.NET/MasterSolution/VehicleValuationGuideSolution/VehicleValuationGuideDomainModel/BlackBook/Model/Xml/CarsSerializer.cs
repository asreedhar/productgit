﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Xml;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Xml
{
    /// <summary>
    /// Car serializer.
    /// </summary>
    public class CarsSerializer : IXmlSerializer<Cars>
    {
        /// <summary>
        /// Deserialize a collection of cars from the given xml reader.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <returns>Collection of cars.</returns>
        public Cars Deserialize(XmlReader reader)
        {
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        if ("cars".Equals(reader.Name))
                        {
                            return Cars(reader);
                        }
                        break;
                }
            }

            throw new NotSupportedException("Bad Parser/XML");
        }

        /// <summary>
        /// Deserialize a collection of cars from the given xml reader.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <returns>Collection of cars.</returns>
        private static Cars Cars(XmlReader reader)
        {
            Cars cars = new Cars();

            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        if ("car".Equals(reader.Name))
                        {
                            cars.Values.Add(Car(reader));
                        }
                        break;
                    case XmlNodeType.EndElement:
                        if ("cars".Equals(reader.Name))
                        {
                            return cars;
                        }
                        break;
                }
            }

            throw new NotSupportedException("Bad Parser/XML");
        }

        /// <summary>
        /// Deserialize a car from the given xml reader.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <returns>Car info.</returns>
        private static CarInfo Car(XmlReader reader)
        {
            CarInfo info = new CarInfo();

            Car car = new Car();

            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        if ("year".Equals(reader.Name))
                        {
                            info.Year = int.Parse(ElementText(reader, "year"));
                        }
                        else if ("make".Equals(reader.Name))
                        {
                            info.Make = ElementText(reader, "make");
                        }
                        else if ("model".Equals(reader.Name))
                        {
                            info.Model = ElementText(reader, "model");
                        }
                        else if ("series".Equals(reader.Name))
                        {
                            info.Series = ElementText(reader, "series");
                        }
                        else if ("body_style".Equals(reader.Name))
                        {
                            info.BodyStyle = ElementText(reader, "body_style");
                        }
                        else if ("vin".Equals(reader.Name))
                        {
                            info.Vin = ElementText(reader, "vin");
                        }
                        else if ("uvc".Equals(reader.Name))
                        {
                            info.Uvc = ElementText(reader, "uvc");

                            car = new Car
                                      {
                                          Year = info.Year,
                                          Make = info.Make,
                                          Model = info.Model,
                                          Series = info.Series,
                                          BodyStyle = info.BodyStyle,
                                          Vin = info.Vin,
                                          Uvc = info.Uvc
                                      };
                        }

                        else if ("msrp".Equals(reader.Name))
                        {
                            car.Msrp = int.Parse(ElementText(reader, "msrp"));
                        }
                        else if ("finance_advance".Equals(reader.Name))
                        {
                            car.FinanceAdvance = int.Parse(ElementText(reader, "finance_advance"));
                        }

                        else if ("residual_value".Equals(reader.Name))
                        {
                            car.ResidualValue = ResidualValue(reader);
                        }
                        else if ("tradein_value".Equals(reader.Name))
                        {
                            car.TradeInValue = Value(reader, "tradein_value");
                        }
                        else if ("wholesale_value".Equals(reader.Name))
                        {
                            car.WholesaleValue = Value(reader, "wholesale_value");
                        }
                        else if ("retail_value".Equals(reader.Name))
                        {
                            car.RetailValue = Value(reader, "retail_value");
                        }

                        else if ("mileage_adjustments".Equals(reader.Name))
                        {
                            car.MileageAdjustments = MileageAdjustments(reader);
                        }

                        else if ("price_includes".Equals(reader.Name))
                        {
                            car.PriceIncludes = ElementText(reader, "price_includes");
                        }

                        else if ("options".Equals(reader.Name))
                        {
                            car.Options = Options(reader);
                        }

                        else if ("wheel_base".Equals(reader.Name))
                        {
                            car.WheelBase = ElementText(reader, "wheel_base");
                        }
                        else if ("taxable_hp".Equals(reader.Name))
                        {
                            car.TaxableHorsepower = ElementText(reader, "taxable_hp");
                        }
                        else if ("weight".Equals(reader.Name))
                        {
                            car.Weight = ElementText(reader, "weight");
                        }
                        else if ("tire_size".Equals(reader.Name))
                        {
                            car.TireSize = ElementText(reader, "tire_size");
                        }
                        else if ("base_hp".Equals(reader.Name))
                        {
                            car.BaseHorsepower = ElementText(reader, "base_hp");
                        }
                        else if ("fuel_type".Equals(reader.Name))
                        {
                            car.FuelType = ElementText(reader, "fuel_type");
                        }
                        else if ("cylinders".Equals(reader.Name))
                        {
                            car.Cylinders = ElementText(reader, "cylinders");
                        }
                        else if ("drive_train".Equals(reader.Name))
                        {
                            car.DriveTrain = ElementText(reader, "drive_train");
                        }
                        else if ("transmission".Equals(reader.Name))
                        {
                            car.Transmission = ElementText(reader, "transmission");
                        }
                        else if ("engine".Equals(reader.Name))
                        {
                            car.Engine = ElementText(reader, "engine");
                        }
                        else if ("veh_class".Equals(reader.Name))
                        {
                            car.VehicleClass = ElementText(reader, "veh_class");
                        }

                        else if ("allvins".Equals(reader.Name))
                        {
                            car.AllVins = AllVins(reader);
                        }

                        else if ("check_digit".Equals(reader.Name))
                        {
                            switch (ElementText(reader, "check_digit"))
                            {
                                case "Y":
                                    car.CheckDigit = true;
                                    break;
                                case "N":
                                    car.CheckDigit = false;
                                    break;
                            }
                        }
                        break;
                    case XmlNodeType.EndElement:
                        if ("car".Equals(reader.Name))
                        {
                            return string.IsNullOrEmpty(car.Uvc) ? info : car;
                        }
                        break;
                }
            }

            throw new NotSupportedException("Bad Parser/XML");
        }

        /// <summary>
        /// Deserialize a residual value from the given xml reader.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <returns>Residual value.</returns>
        private static ResidualValue ResidualValue(XmlReader reader)
        {
            ResidualValue value = new ResidualValue();

            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        if ("resid12".Equals(reader.Name))
                        {
                            value.Resid12 = ElementInt32Q(reader, "resid12");
                        }
                        if ("resid24".Equals(reader.Name))
                        {
                            value.Resid24 = ElementInt32Q(reader, "resid24");
                        }
                        if ("resid30".Equals(reader.Name))
                        {
                            value.Resid30 = ElementInt32Q(reader, "resid30");
                        }
                        if ("resid36".Equals(reader.Name))
                        {
                            value.Resid36 = ElementInt32Q(reader, "resid36");
                        }
                        if ("resid42".Equals(reader.Name))
                        {
                            value.Resid42 = ElementInt32Q(reader, "resid42");
                        }
                        if ("resid48".Equals(reader.Name))
                        {
                            value.Resid48 = ElementInt32Q(reader, "resid48");
                        }
                        if ("resid60".Equals(reader.Name))
                        {
                            value.Resid60 = ElementInt32Q(reader, "resid60");
                        }
                        if ("resid72".Equals(reader.Name))
                        {
                            value.Resid72 = ElementInt32Q(reader, "resid72");
                        }
                        break;
                    case XmlNodeType.EndElement:
                        if ("residual_value".Equals(reader.Name))
                        {
                            return value;
                        }
                        break;
                }
            }

            throw new NotSupportedException("Bad Parser/XML");
        }

        /// <summary>
        /// Deserialize a value from the given xml reader.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <param name="elementName">Element name.</param>
        /// <returns>Value.</returns>
        private static Value Value(XmlReader reader, IEquatable<string> elementName)
        {
            Value value = new Value();

            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        if ("extra_clean".Equals(reader.Name))
                        {
                            value.ExtraClean = ElementInt32Q(reader, "extra_clean");
                        }
                        if ("clean".Equals(reader.Name))
                        {
                            value.Clean = ElementInt32Q(reader, "clean");
                        }
                        if ("average".Equals(reader.Name))
                        {
                            value.Average = ElementInt32Q(reader, "average");
                        }
                        if ("rough".Equals(reader.Name))
                        {
                            value.Rough = ElementInt32Q(reader, "rough");
                        }
                        break;
                    case XmlNodeType.EndElement:
                        if (elementName.Equals(reader.Name))
                        {
                            return value;
                        }
                        break;
                }
            }

            throw new NotSupportedException("Bad Parser/XML");
        }

        /// <summary>
        /// Deserialize a list of mileage adjustments from the given xml reader.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <returns>List of mileage adjustments.</returns>
        private static IList<MileageAdjustment> MileageAdjustments(XmlReader reader)
        {
            IList<MileageAdjustment> values = new List<MileageAdjustment>();

            if (reader.IsEmptyElement)
            {
                return values;
            }

            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        if ("mileage_range".Equals(reader.Name))
                        {
                            values.Add(MileageRange(reader));
                        }
                        break;
                    case XmlNodeType.EndElement:
                        if ("mileage_adjustments".Equals(reader.Name))
                        {
                            return values;
                        }
                        break;
                }
            }

            throw new NotSupportedException("Bad Parser/XML");
        }

        /// <summary>
        /// Deserialize a mileage adjustment from the given xml reader.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <returns>Mileage adjustment.</returns>
        private static MileageAdjustment MileageRange(XmlReader reader)
        {
            MileageAdjustment value = new MileageAdjustment();

            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        if ("begin_range".Equals(reader.Name))
                        {
                            value.BeginRange = ElementInt32(reader, "begin_range");
                        }
                        if ("end_range".Equals(reader.Name))
                        {
                            value.EndRange = ElementInt32(reader, "end_range");
                        }
                        if ("finance_advance".Equals(reader.Name))
                        {
                            value.FinanceAdvance = ElementInt32Q(reader, "finance_advance");
                        }
                        if ("extra_clean".Equals(reader.Name))
                        {
                            value.ExtraClean = ElementInt32Q(reader, "extra_clean");
                        }
                        if ("clean".Equals(reader.Name))
                        {
                            value.Clean = ElementInt32Q(reader, "clean");
                        }
                        if ("average".Equals(reader.Name))
                        {
                            value.Average = ElementInt32Q(reader, "average");
                        }
                        if ("rough".Equals(reader.Name))
                        {
                            value.Rough = ElementInt32Q(reader, "rough");
                        }
                        break;
                    case XmlNodeType.EndElement:
                        if ("mileage_range".Equals(reader.Name))
                        {
                            return value;
                        }
                        break;
                }
            }

            throw new NotSupportedException("Bad Parser/XML");
        }

        /// <summary>
        /// Deserialize a list of option adjustments from the given xml reader.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <returns>List of option adjustments.</returns>
        private static IList<Option> Options(XmlReader reader)
        {
            IList<Option> values = new List<Option>();

            if (reader.IsEmptyElement)
            {
                return values;
            }

            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        if ("option".Equals(reader.Name))
                        {
                            values.Add(Option(reader));
                        }
                        break;
                    case XmlNodeType.EndElement:
                        if ("options".Equals(reader.Name))
                        {
                            return values;
                        }
                        break;
                }
            }

            throw new NotSupportedException("Bad Parser/XML");
        }

        /// <summary>
        /// Deserialize an option adjustment from the given xml reader.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <returns>Option adjustment.</returns>
        private static Option Option(XmlReader reader)
        {
            Option value = new Option();

            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        if ("ad_code".Equals(reader.Name))
                        {
                            value.AdCode = ElementText(reader, "ad_code");
                        }
                        if ("description".Equals(reader.Name))
                        {
                            value.Description = ElementText(reader, "description");
                        }
                        if ("add_deduct".Equals(reader.Name))
                        {
                            switch (ElementText(reader, "add_deduct"))
                            {
                                case "Deduct":
                                    value.OptionAdjustmentSign = OptionAdjustmentSign.Deduct;
                                    break;
                                case "Add":
                                    value.OptionAdjustmentSign = OptionAdjustmentSign.Add;
                                    break;
                                default:
                                    value.OptionAdjustmentSign = OptionAdjustmentSign.Unknown;
                                    break;
                            }
                        }
                        if ("amount".Equals(reader.Name))
                        {
                            value.Amount = ElementInt32Q(reader, "amount");
                        }
                        if ("resid12".Equals(reader.Name))
                        {
                            value.Residual.Resid12 = int.Parse(ElementText(reader, "resid12"));
                        }
                        if ("resid24".Equals(reader.Name))
                        {
                            value.Residual.Resid24 = int.Parse(ElementText(reader, "resid24"));
                        }
                        if ("resid30".Equals(reader.Name))
                        {
                            value.Residual.Resid30 = int.Parse(ElementText(reader, "resid30"));
                        }
                        if ("resid36".Equals(reader.Name))
                        {
                            value.Residual.Resid36 = int.Parse(ElementText(reader, "resid36"));
                        }
                        if ("resid42".Equals(reader.Name))
                        {
                            value.Residual.Resid42 = int.Parse(ElementText(reader, "resid42"));
                        }
                        if ("resid48".Equals(reader.Name))
                        {
                            value.Residual.Resid48 = int.Parse(ElementText(reader, "resid48"));
                        }
                        if ("resid60".Equals(reader.Name))
                        {
                            value.Residual.Resid60 = int.Parse(ElementText(reader, "resid60"));
                        }
                        if ("resid72".Equals(reader.Name))
                        {
                            value.Residual.Resid72 = int.Parse(ElementText(reader, "resid72"));
                        }
                        if ("flag".Equals(reader.Name))
                        {
                            switch(ElementText(reader, "flag"))
                            {
                                case "Y":
                                    value.Flag = true;
                                    break;
                                case "N":
                                    value.Flag = false;
                                    break;
                            }
                        }
                        break;
                    case XmlNodeType.EndElement:
                        if ("option".Equals(reader.Name))
                        {
                            return value;
                        }
                        break;
                }
            }

            throw new NotSupportedException("Bad Parser/XML");
        }

        /// <summary>
        /// Deserialize a list of colors from the given xml reaer.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <returns>List of colors.</returns>
        private static IList<Color> Colors(XmlReader reader)
        {
            IList<Color> values = new List<Color>();

            if (reader.IsEmptyElement)
            {
                return values;
            }

            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        if ("color".Equals(reader.Name))
                        {
                            values.Add(Color(reader));
                        }
                        break;
                    case XmlNodeType.EndElement:
                        if ("colors".Equals(reader.Name))
                        {
                            return values;
                        }
                        break;
                }
            }

            throw new NotSupportedException("Bad Parser/XML");
        }

        /// <summary>
        /// Deserialize a color from the given xml reader.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <returns>Color.</returns>
        private static Color Color(XmlReader reader)
        {
            Color value = new Color();

            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        if ("category".Equals(reader.Name))
                        {
                            value.Category = ElementText(reader, "category");
                        }
                        if ("description".Equals(reader.Name))
                        {
                            value.Description = ElementText(reader, "description");
                        }
                        if ("swatches".Equals(reader.Name))
                        {
                            value.Swatches = Swatches(reader);
                        }
                        break;
                    case XmlNodeType.EndElement:
                        if ("color".Equals(reader.Name))
                        {
                            return value;
                        }
                        break;
                }
            }

            throw new NotSupportedException("Bad Parser/XML");
        }

        /// <summary>
        /// Deserialize a list of color swatches from the given xml reader.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <returns>List of color swatches.</returns>
        private static IList<System.Drawing.Color> Swatches(XmlReader reader)
        {
            IList<System.Drawing.Color> values = new List<System.Drawing.Color>();

            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        if ("swatch".Equals(reader.Name))
                        {
                            //BUGZID: 25899 
                            try
                            {
                                values.Add(ColorTranslator.FromHtml(ElementText(reader, "swatch")));
                            }
                            catch (Exception)
                            {
                                //I'm not exceptionally thrilled about this, but when BB sends us bad data, ie ##B09E92 instead of #B09E92
                                //... we really don't want to fail the entire request.  So swallow the exception, and continue.
                            }
                        }
                        break;
                    case XmlNodeType.EndElement:
                        if ("swatches".Equals(reader.Name))
                        {
                            return values;
                        }
                        break;
                }
            }

            throw new NotSupportedException("Bad Parser/XML");
        }

        /// <summary>
        /// Deserialize a list of vins from the given xml reader.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <returns>List of vins.</returns>
        private static IList<string> AllVins(XmlReader reader)
        {
            IList<string> values = new List<string>();

            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        if ("vin".Equals(reader.Name))
                        {
                            values.Add(ElementText(reader, "vin"));
                        }
                        break;
                    case XmlNodeType.EndElement:
                        if ("allvins".Equals(reader.Name))
                        {
                            return values;
                        }
                        break;
                }
            }

            throw new NotSupportedException("Bad Parser/XML");
        }

        /// <summary>
        /// Deserialize an int from the given xml reader.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <param name="elementName">Element name.</param>
        /// <returns>Int.</returns>
        private static int ElementInt32(XmlReader reader, IEquatable<string> elementName)
        {
            return ElementInt32Q(reader, elementName).GetValueOrDefault();
        }

        /// <summary>
        /// Deserialize a nullable int from the given xml reader.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <param name="elementName">Element name.</param>
        /// <returns>Nullable int.</returns>
        private static int? ElementInt32Q(XmlReader reader, IEquatable<string> elementName)
        {
            string text = ElementText(reader, elementName);

            if (!string.IsNullOrEmpty(text))
            {
                int val;

                if (int.TryParse(text, out val))
                {
                    return val;
                }
            }

            return null;
        }

        /// <summary>
        /// Deserialize text from the given xml reader.
        /// </summary>
        /// <param name="reader">Xml reader.</param>
        /// <param name="elementName">Element name.</param>
        /// <returns>String.</returns>
        private static string ElementText(XmlReader reader, IEquatable<string> elementName)
        {
            if (reader.IsEmptyElement)
            {
                return string.Empty;
            }

            StringBuilder sb = new StringBuilder();

            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Whitespace:
                    case XmlNodeType.Text:
                        sb.Append(reader.Value);
                        break;
                    case XmlNodeType.EndElement:
                        if (elementName.Equals(reader.Name))
                        {
                            return sb.ToString();
                        }
                        break;
                }
            }

            throw new NotSupportedException("Bad Parser");
        }
    }
}