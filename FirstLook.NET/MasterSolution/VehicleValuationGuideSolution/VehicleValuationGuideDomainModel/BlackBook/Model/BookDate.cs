﻿namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model
{
    /// <summary>
    /// Reference data version information.
    /// </summary>
    public class BookDate
    {
        /// <summary>
        /// Data load identifier.
        /// </summary>
        public int Id { get; internal set; }
    }
}
