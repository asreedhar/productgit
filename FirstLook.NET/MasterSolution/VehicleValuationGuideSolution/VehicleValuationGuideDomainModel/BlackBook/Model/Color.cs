using System;
using System.Collections.Generic;
using FirstLook.Common.Core.Hashing;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model
{
    /// <summary>
    /// Color.
    /// </summary>
    [Serializable]
    public class Color : IHashable, IComparable
    {       
        /// <summary>
        /// Color category.
        /// </summary>
        public string Category { get; internal set; }

        /// <summary>
        /// Description.
        /// </summary>
        public string Description { get; internal set; }

        /// <summary>
        /// Color swatches.
        /// </summary>
        public IList<System.Drawing.Color> Swatches { get; internal set; }

        /// <summary>
        /// Initialize the swatches to be an empty list.
        /// </summary>
        public Color()
        {
            Swatches = new List<System.Drawing.Color>();
        }

        /// <summary>
        /// Create a hash value from this color.
        /// </summary>
        /// <returns>Hash value.</returns>
        public long Hash()
        {
            HashAlgorithm algorithm = new HashAlgorithm();
            
            algorithm.AddString(Category);
            algorithm.AddString(Description);
                        
            foreach (System.Drawing.Color color in Swatches)
            {
                algorithm.AddString(color.ToString());
            }

            return algorithm.Value;
        }

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer that indicates the relative order of the objects being compared. The return value has these meanings: Value Meaning Less than zero This instance is less than <paramref name="obj"/>. Zero This instance is equal to <paramref name="obj"/>. Greater than zero This instance is greater than <paramref name="obj"/>. 
        /// </returns>
        /// <param name="obj">An object to compare with this instance. </param><exception cref="T:System.ArgumentException"><paramref name="obj"/> is not the same type as this instance. </exception><filterpriority>2</filterpriority>
        public int CompareTo(object obj)
        {
            Color color = obj as Color;
            if (color == null)
            {
                throw new ArgumentException("Cannot compare to anything other than a Color.");
            }

            int comp = Category.CompareTo(color.Category);
            if (comp != 0)
            {
                return comp;
            }

            comp = Description.CompareTo(color.Description);
            if (comp != 0)
            {
                return comp;
            }

            List<string> swatches1 = new List<string>();
            foreach (System.Drawing.Color swatch in Swatches)
            {
               swatches1.Add(swatch.ToString());
            }
            swatches1.Sort();

            List<string> swatches2 = new List<string>();
            foreach (System.Drawing.Color swatch in color.Swatches)
            {
                swatches2.Add(swatch.ToString());
            }
            swatches2.Sort();

            int lengths = swatches2.Count - swatches1.Count;
            if (lengths != 0)
            {
                return lengths;
            }

            for (int i = 0; i < swatches1.Count; i++)
            {
                comp = swatches1[i].CompareTo(swatches2[i]);
                if (comp != 0)
                {
                    return comp;
                }
            }

            return 0;
        }
    }
}
