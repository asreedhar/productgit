﻿using System.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Sql
{
    public interface ISqlService
    {
        /// <summary>
        /// Get the current book date.
        /// </summary>
        /// <returns></returns>
        IDataReader BookDate();

        /// <summary>
        /// Get the list of years.
        /// </summary>
        /// <returns>Data reader with years.</returns>
        IDataReader Query();

        /// <summary>
        /// Get the list of makes.
        /// </summary>
        /// <param name="year">Year.</param>
        /// <returns>Data reader with makes.</returns>
        IDataReader Query(int year);
    }
}
