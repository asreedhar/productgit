﻿SELECT DISTINCT Make
FROM BlackBook.UsedCarYearMake
WHERE Year = @Year
ORDER BY Make ASC