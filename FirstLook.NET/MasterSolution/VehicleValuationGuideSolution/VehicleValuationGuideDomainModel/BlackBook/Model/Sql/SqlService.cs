﻿using System.Data;
using FirstLook.Common.Core.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Sql
{
    public class SqlService : ISqlService
    {
        #region Properties

        /// <summary>
        /// Path to KBB sql file resources.
        /// </summary>
        protected const string Prefix = "FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Sql.Resources.";

        /// <summary>
        /// Clone reader, for optionally persisting result sets to xml files.
        /// </summary>
        private readonly ICloneReader _clone;

        /// <summary>
        /// Name of the database to access.
        /// </summary>
        protected string DatabaseName { get { return "BlackBook"; } }

        #endregion

        #region Construction

        /// <summary>
        /// Initialize this sql service with the default clone reader.
        /// </summary>
        public SqlService()
        {
            _clone = new CloneReader();
        }

        /// <summary>
        /// Initialize this sql service with the given clone reader.
        /// </summary>
        /// <param name="clone">Clone reader.</param>
        public SqlService(ICloneReader clone)
        {
            _clone = clone;
        }

        #endregion

        #region Utility

        /// <summary>
        /// Create a command from the given resource given the current database, schema and 'and' qualifier.
        /// </summary>
        /// <param name="resourceName">Resource name.</param>
        /// <returns>Command text.</returns>
        protected string CommandText(string resourceName)
        {
            return Database.GetCommandText(GetType().Assembly, Prefix + resourceName);
        }

        #endregion

        /// <summary>
        /// Get the current book date.
        /// </summary>
        /// <returns>Data reader with years.</returns>
        public IDataReader BookDate()
        {
            using (IDbConnection connection = Database.Connection("Vehicle"))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = CommandText("SqlService_BookDate.txt");

                    return _clone.Snapshot(command, "BookDate");
                }
            }
        }


        /// <summary>
        /// Get the list of years.
        /// </summary>
        /// <returns>Data reader with years.</returns>
        public IDataReader Query()
        {
            using (IDbConnection connection = Database.Connection("VehicleCatalog"))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = CommandText("SqlService_Years.txt");
                    
                    return _clone.Snapshot(command, "Year");
                }
            }
        }

        /// <summary>
        /// Get the list of makes.
        /// </summary>
        /// <param name="year">Year.</param>
        /// <returns>Data reader with makes.</returns>
        public IDataReader Query(int year)
        {
            using (IDbConnection connection = Database.Connection("VehicleCatalog"))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = CommandText("SqlService_Makes.txt");

                    Database.AddWithValue(command, "Year",       year,       DbType.Int32);

                    return _clone.Snapshot(command, "Make");
                }
            }
        }
    }
}
