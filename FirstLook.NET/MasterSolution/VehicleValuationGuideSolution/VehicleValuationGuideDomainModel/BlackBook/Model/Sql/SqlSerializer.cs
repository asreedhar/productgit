﻿using System.Collections.Generic;
using System.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Sql
{
    public abstract class SqlSerializer<T> : ISqlSerializer<T>
    {
        public virtual IList<T> Deserialize(IDataReader reader)
        {
            List<T> list = new List<T>();

            while (reader.Read())
            {
                list.Add(Deserialize((IDataRecord)reader));
            }

            return list;
        }

        public abstract T Deserialize(IDataRecord record);
    }

    public class BookDateSerializer : SqlSerializer<BookDate>
    {
        public override BookDate Deserialize(IDataRecord record)
        {
            return new BookDate
            {
                Id = record.GetInt32(record.GetOrdinal("DataLoadID"))
            };
        }
    }

    public class YearSerializer : SqlSerializer<Year>
    {
        public override Year Deserialize(IDataRecord record)
        {
            short value = record.GetInt16(record.GetOrdinal("Year"));

            return new Year {Value = value.ToString()};
        }
    }

    public class MakeSerializer : SqlSerializer<Make>
    {
        public override Make Deserialize(IDataRecord record)
        {
            string value = record.GetString(record.GetOrdinal("Make"));

            return new Make {Value = value};
        }
    }
}
