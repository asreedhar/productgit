﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Sql
{
    public class Module : IModule
    {
        internal class Serialization : IModule
        {
            public void Configure(IRegistry registry)
            {
                registry.Register<ISqlSerializer<BookDate>, BookDateSerializer>(ImplementationScope.Shared);
                registry.Register<ISqlSerializer<Year>, YearSerializer>(ImplementationScope.Shared);
                registry.Register<ISqlSerializer<Make>, MakeSerializer>(ImplementationScope.Shared);
            }
        }

        public void Configure(IRegistry registry)
        {
            registry.Register<ISqlService, SqlService>(ImplementationScope.Shared);

            registry.Register<Serialization>();
        }
    }
}
