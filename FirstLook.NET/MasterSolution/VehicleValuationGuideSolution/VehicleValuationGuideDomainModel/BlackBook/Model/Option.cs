﻿using System;
using FirstLook.Common.Core.Hashing;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model
{
    /// <summary>
    /// Option.
    /// </summary>
    [Serializable]
    public class Option : IHashable, IComparable
    {        
        /// <summary>
        /// Ad code.
        /// </summary>
        public string AdCode { get; internal set; }

        /// <summary>
        /// Description of the option.
        /// </summary>
        public string Description { get; internal set; }

        /// <summary>
        /// Whether the option is an addition or removal.
        /// </summary>
        public OptionAdjustmentSign OptionAdjustmentSign { get; internal set; }

        /// <summary>
        /// Amount.
        /// </summary>
        public int? Amount { get; internal set; }

        /// <summary>
        /// Residual values.
        /// </summary>
        public ResidualValue Residual { get; internal set; }

        /// <summary>
        /// Flag.
        /// </summary>
        public bool Flag { get; internal set; }

        /// <summary>
        /// Get the universal option code.
        /// </summary>
        public string UniversalOptionCode
        {
            get
            {
                string prefix = OptionAdjustmentSign == OptionAdjustmentSign.Add ? "A" : "D";                
                return prefix + prefix + AdCode;
            }
        }

        /// <summary>
        /// Initialize the residual value to be empty.
        /// </summary>
        public Option()
        {
            Residual = new ResidualValue();
        }

        /// <summary>
        /// Create a hash value from this option.
        /// </summary>
        /// <returns>Hash value.</returns>
        public long Hash()
        {
            HashAlgorithm algorithm = new HashAlgorithm();

            algorithm.AddString(UniversalOptionCode);
            algorithm.AddString(Description);            
            algorithm.AddInt(Amount);
            algorithm.AddLong(Residual.Hash());
            algorithm.AddBool(Flag);

            return algorithm.Value;
        }

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer that indicates the relative order of the objects being compared. The return value 
        /// has these meanings: Value Meaning Less than zero This instance is less than <paramref name="obj"/>. Zero 
        /// This instance is equal to <paramref name="obj"/>. Greater than zero This instance is greater than 
        /// <paramref name="obj"/>. 
        /// </returns>
        /// <param name="obj">
        /// An object to compare with this instance.
        /// </param>
        /// <exception cref="T:System.ArgumentException">
        /// <paramref name="obj"/> is not the same type as this instance. 
        /// </exception>
        /// <filterpriority>2</filterpriority>
        public int CompareTo(object obj)
        {
            Option option = obj as Option;
            if (option == null)
            {
                throw new ArgumentException("Cannot compare to anything other than a Option.");
            }

            int comp = UniversalOptionCode.CompareTo(option.UniversalOptionCode);
            if (comp != 0)
            {
                return comp;
            }

            comp = Description.CompareTo(option.Description);
            if (comp != 0)
            {
                return comp;
            }

            if (Amount < option.Amount)
            {
                return -1;
            }
            if (Amount > option.Amount)
            {
                return 1;
            }

            comp = Residual.CompareTo(option.Residual);
            if (comp != 0)
            {
                return comp;
            }

            if (!Flag && option.Flag)
            {
                return -1;
            }
            if (Flag && !option.Flag)
            {
                return 1;
            }

            return 0;
        }
    }
}
