using System;
using FirstLook.Common.Core.Hashing;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model
{
    /// <summary>
    /// Residual value.
    /// </summary>
    [Serializable]
    public class ResidualValue : IHashable, IComparable
    {
        /// <summary>
        /// 12 month residual value.
        /// </summary>
        public int? Resid12 { get; internal set; }

        /// <summary>
        /// 24 month residual value.
        /// </summary>
        public int? Resid24 { get; internal set; }

        /// <summary>
        /// 30 month residual value.
        /// </summary>
        public int? Resid30 { get; internal set; }

        /// <summary>
        /// 36 month residual value.
        /// </summary>
        public int? Resid36 { get; internal set; }

        /// <summary>
        /// 42 month residual value.
        /// </summary>
        public int? Resid42 { get; internal set; }

        /// <summary>
        /// 48 month residual value.
        /// </summary>
        public int? Resid48 { get; internal set; }

        /// <summary>
        /// 60 month residual value.
        /// </summary>
        public int? Resid60 { get; internal set; }

        /// <summary>
        /// 72 month residual value.
        /// </summary>
        public int? Resid72 { get; internal set; }
        
        /// <summary>
        /// Create a hash value from this residual value.
        /// </summary>
        /// <returns>Hash value.</returns>
        public long Hash()
        {
            HashAlgorithm algorithm = new HashAlgorithm();
            
            algorithm.AddInt(Resid12);
            algorithm.AddInt(Resid24);
            algorithm.AddInt(Resid30);
            algorithm.AddInt(Resid36);
            algorithm.AddInt(Resid42);
            algorithm.AddInt(Resid48);
            algorithm.AddInt(Resid60);
            algorithm.AddInt(Resid72);

            return algorithm.Value;
        }

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer that indicates the relative order of the objects being compared. The return value has these meanings: Value Meaning Less than zero This instance is less than <paramref name="obj"/>. Zero This instance is equal to <paramref name="obj"/>. Greater than zero This instance is greater than <paramref name="obj"/>. 
        /// </returns>
        /// <param name="obj">An object to compare with this instance. </param><exception cref="T:System.ArgumentException"><paramref name="obj"/> is not the same type as this instance. </exception><filterpriority>2</filterpriority>
        public int CompareTo(object obj)
        {
            ResidualValue value = obj as ResidualValue;
            if (value == null)
            {
                throw new ArgumentException("Cannot compare to anything other than a Color.");
            }

            // Resid 12.
            if (Resid12 < value.Resid12)
            {
                return -1;
            }
            if (Resid12 > value.Resid12)
            {
                return 1;
            }

            // Resid 24.
            if (Resid24 < value.Resid24)
            {
                return -1;
            }
            if (Resid24 > value.Resid24)
            {
                return 1;
            }

            // Resid 30.
            if (Resid30 < value.Resid30)
            {
                return -1;
            }
            if (Resid30 > value.Resid30)
            {
                return 1;
            }

            // Resid 36.
            if (Resid36 < value.Resid36)
            {
                return -1;
            }
            if (Resid36 > value.Resid36)
            {
                return 1;
            }

            // Resid 42.
            if (Resid42 < value.Resid42)
            {
                return -1;
            }
            if (Resid42 > value.Resid42)
            {
                return 1;
            }

            // Resid 48.
            if (Resid48 < value.Resid48)
            {
                return -1;
            }
            if (Resid48 > value.Resid48)
            {
                return 1;
            }

            // Resid 60.
            if (Resid60 < value.Resid60)
            {
                return -1;
            }
            if (Resid60 > value.Resid60)
            {
                return 1;
            }

            // Resid 72.
            if (Resid72 < value.Resid72)
            {
                return -1;
            }
            if (Resid72 > value.Resid72)
            {
                return 1;
            }

            return 0;
        }
    }
}
