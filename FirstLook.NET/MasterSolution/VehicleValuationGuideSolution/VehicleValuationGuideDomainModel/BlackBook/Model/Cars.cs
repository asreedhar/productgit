﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model
{
    /// <summary>
    /// A collection of cars.
    /// </summary>
    [Serializable]
    public class Cars
    {        
        /// <summary>
        /// List of cars.
        /// </summary>
        public IList<CarInfo> Values { get; internal set; }

        /// <summary>
        /// Initialize the car list to be empty.
        /// </summary>
        public Cars()
        {
            Values = new List<CarInfo>();
        }
    }
}
