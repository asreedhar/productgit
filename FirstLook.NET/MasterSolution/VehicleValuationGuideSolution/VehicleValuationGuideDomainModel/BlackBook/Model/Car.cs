﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core.Hashing;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model
{
    /// <summary>
    /// Details of a specific car.
    /// </summary>
    [Serializable]
    public class Car : CarInfo, IHashable
    {
        #region Base Values

        /// <summary>
        /// Mileage.
        /// </summary>
        public int? Mileage { get; internal set; }

        /// <summary>
        /// MSRP.
        /// </summary>
        public int? Msrp { get; internal set; }

        /// <summary>
        /// Finance advance.
        /// </summary>
        public int? FinanceAdvance { get; internal set; }

        /// <summary>
        /// Residual value.
        /// </summary>
        public ResidualValue ResidualValue { get; internal set; }

        /// <summary>
        /// Trade-in value.
        /// </summary>
        public Value TradeInValue { get; internal set; }

        /// <summary>
        /// Wholesale value.
        /// </summary>
        public Value WholesaleValue { get; internal set; }

        /// <summary>
        /// Retail value.
        /// </summary>
        public Value RetailValue { get; internal set; }

        /// <summary>
        /// Price includes.
        /// </summary>
        public string PriceIncludes { get; internal set; }

        #endregion
        
        #region Adjustments

        /// <summary>
        /// Mileage adjustments.
        /// </summary>
        public IList<MileageAdjustment> MileageAdjustments { get; internal set; }        

        /// <summary>
        /// Options.
        /// </summary>
        public IList<Option> Options { get; internal set; }

        #endregion

        #region Vehicle Information

        /// <summary>
        /// Wheelbase.
        /// </summary>
        public string WheelBase { get; internal set; }

        /// <summary>
        /// Taxable horsepower.
        /// </summary>
        public string TaxableHorsepower { get; internal set; }

        /// <summary>
        /// Weight.
        /// </summary>
        public string Weight { get; internal set; }

        /// <summary>
        /// Tire size.
        /// </summary>
        public string TireSize { get; internal set; }

        /// <summary>
        /// Base horsepower.
        /// </summary>
        public string BaseHorsepower { get; internal set; }

        /// <summary>
        /// Fuel type.
        /// </summary>
        public string FuelType { get; internal set; }

        /// <summary>
        /// Cylinders.
        /// </summary>
        public string Cylinders { get; internal set; }

        /// <summary>
        /// Drive train.
        /// </summary>
        public string DriveTrain { get; internal set; }

        /// <summary>
        /// Transmission.
        /// </summary>
        public string Transmission { get; internal set; }

        /// <summary>
        /// Engine.
        /// </summary>
        public string Engine { get; internal set; }

        /// <summary>
        /// Vehicle class.
        /// </summary>
        public string VehicleClass { get; internal set; }

        #endregion

        #region Additional Information

        /// <summary>
        /// All vins tied to this car.
        /// </summary>
        public IList<string> AllVins { get; internal set; }

        /// <summary>
        /// Check digit.
        /// </summary>
        public bool? CheckDigit { get; internal set; }

        #endregion

        #region Hashing

        /// <summary>
        /// Create a hash value from this car.
        /// </summary>
        /// <returns>Unsigned long hash value.</returns>
        public long Hash()
        {
            HashAlgorithm algo = new HashAlgorithm();

            // Car info.

            algo.AddInt(Year);
            algo.AddString(Make);
            algo.AddString(Model);
            algo.AddString(Series);
            algo.AddString(BodyStyle);
            algo.AddString(Utility.Model.Vin.Squish(Vin)); // Ensures comparisons are same between full and squish vins.
            algo.AddString(Uvc);

            // Base values.

            algo.AddInt(Mileage);
            algo.AddInt(Msrp);
            algo.AddInt(FinanceAdvance);
            algo.AddLong(ResidualValue.Hash());
            algo.AddLong(TradeInValue.Hash());
            algo.AddLong(WholesaleValue.Hash());
            algo.AddLong(RetailValue.Hash());
            algo.AddString(PriceIncludes);

            // Mileage adjustments - sorted to prevent order from affecting hash.

            List<MileageAdjustment> adjustments = MileageAdjustments as List<MileageAdjustment>;
            if (adjustments == null)
            {
                throw new ApplicationException();
            }

            adjustments.Sort();

            foreach (MileageAdjustment adjustment in adjustments)
            {
                algo.AddLong(adjustment.Hash());
            }

            // Options - sorted to prevent order from affecting hash.

            List<Option> options = Options as List<Option>;
            if (options == null)
            {
                throw new ApplicationException();
            }

            options.Sort();

            foreach (Option option in options)
            {
                algo.AddLong(option.Hash());
            }

            // Vehicle information.

            algo.AddString(WheelBase);
            algo.AddString(TaxableHorsepower);
            algo.AddString(Weight);
            algo.AddString(TireSize);
            algo.AddString(BaseHorsepower);
            algo.AddString(FuelType);
            algo.AddString(Cylinders);
            algo.AddString(DriveTrain);
            algo.AddString(Transmission);
            algo.AddString(Engine);
            algo.AddString(VehicleClass);

            // Additional information - sorted to prevent order from affecting hash.

            return algo.Value;
        }

        #endregion

        /// <summary>
        /// Initialize all of the car's lists.
        /// </summary>
        public Car()
        {
            AllVins = new List<string>();
            MileageAdjustments = new List<MileageAdjustment>();
            Options = new List<Option>();
            ResidualValue = new ResidualValue();            
            RetailValue = new Value();
            TradeInValue = new Value();
            WholesaleValue = new Value();
        }        
    }
}
