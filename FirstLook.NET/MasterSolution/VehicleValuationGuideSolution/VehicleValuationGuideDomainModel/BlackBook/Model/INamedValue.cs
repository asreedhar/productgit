﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model
{
    public interface INamedValue
    {
        string Value { get; }
    }

    [Serializable]
    public abstract class NamedValue : INamedValue
    {
        private string _value;

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }

    [Serializable]
    public class Year : NamedValue
    {
    }

    [Serializable]
    public class Make : NamedValue
    {
    }
}
