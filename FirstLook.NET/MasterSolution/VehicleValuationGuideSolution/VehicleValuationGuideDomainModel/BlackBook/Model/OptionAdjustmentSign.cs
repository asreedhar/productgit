using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model
{
    /// <summary>
    /// Possibilities for an option adjustment.
    /// </summary>
    [Serializable]
    public enum OptionAdjustmentSign
    {
        /// <summary>
        /// Unknown.
        /// </summary>
        Unknown,

        /// <summary>
        /// Add an option.
        /// </summary>
        Add,

        /// <summary>
        /// Deduct an option.
        /// </summary>
        Deduct
    }
}
