using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model
{
    /// <summary>
    /// Module for registering model components.
    /// </summary>
    public class Module : IModule
    {
        /// <summary>
        /// Register model components.
        /// </summary>
        /// <param name="registry">Registry.</param>
        public void Configure(IRegistry registry)
        {
            registry.Register<Lookup.Module>();

            registry.Register<Api.Module>();

            registry.Register<Sql.Module>();

            registry.Register<Xml.Module>();
        }
    }
}
