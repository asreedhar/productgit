﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core.Hashing;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model
{
    /// <summary>
    /// A vehicle's standard equipment.
    /// </summary>
    [Serializable]
    public class StandardEquipment : IHashable
    {        
        /// <summary>
        /// Categories of equipment.
        /// </summary>
        public IList<string> Categories { get; internal set; }

        /// <summary>
        /// All the equipment.
        /// </summary>
        public IList<StandardEquipmentRecord> StandardEquipmentRecords { get; internal set; }

        /// <summary>
        /// Initialize the lists to empty values.
        /// </summary>
        public StandardEquipment()
        {
            StandardEquipmentRecords = new List<StandardEquipmentRecord>();
            Categories = new List<string>();
        }

        /// <summary>
        /// Create a hash value.
        /// </summary>
        /// <returns>Hash value.</returns>
        public long Hash()
        {
            HashAlgorithm algo = new HashAlgorithm();

            foreach (string category in Categories)
            {
                algo.AddString(category);
            }

            foreach (StandardEquipmentRecord record in StandardEquipmentRecords)
            {
                algo.AddLong(record.Hash());
            }

            return algo.Value;
        }
    }
}
