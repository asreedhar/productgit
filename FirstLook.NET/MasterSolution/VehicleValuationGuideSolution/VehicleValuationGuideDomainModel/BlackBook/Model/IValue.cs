namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model
{
    /// <summary>
    /// A vehicle valuation.
    /// </summary>
    public interface IValue
    {
        /// <summary>
        /// Value for a car in extra clean condition.
        /// </summary>
        int? ExtraClean { get; }

        /// <summary>
        /// Value for a car in clean condition.
        /// </summary>
        int? Clean { get; }

        /// <summary>
        /// Value for a car in average condition.
        /// </summary>
        int? Average { get; }

        /// <summary>
        /// Value for a car in rough condition.
        /// </summary>
        int? Rough { get; }
    }
}