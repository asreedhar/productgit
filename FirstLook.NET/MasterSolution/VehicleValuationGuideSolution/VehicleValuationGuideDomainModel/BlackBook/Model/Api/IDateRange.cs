﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// Interface for a date range.
    /// </summary>
    public interface IDateRange
    {        
        /// <summary>
        /// Beginning of the date range.
        /// </summary>
        DateTime BeginDate { get; }

        /// <summary>
        /// End of the date range.
        /// </summary>
        DateTime EndDate { get; }

        /// <summary>
        /// Is the given date covered by this range?
        /// </summary>
        /// <param name="date">Date to check if covered.</param>
        /// <returns>True if the date is covered, false otherwise.</returns>
        bool Covers(DateTime date);
    }
}
