using System;
using System.Collections.Generic;
using System.Linq;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// Vehicle configuration.
    /// </summary>
    [Serializable]
    public class VehicleConfiguration : IVehicleConfiguration
    {
        /// <summary>
        /// Initialize a vehicle configuration for the given values.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="state">US state identifier.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="mileage">Mileage.</param>
        /// <param name="bookDate">Reference data version information.</param>
        public VehicleConfiguration(string uvc, State state, string vin, int? mileage, BookDate bookDate)
        {
            OptionActions = new List<IOptionAction>();
            OptionStates  = new List<IOptionState>();
            State         = state;
            Uvc           = uvc;
            Vin           = vin;
            Mileage       = mileage;
            BookDate      = bookDate;
        }

        /// <summary>
        /// Copy the given configuration.
        /// </summary>
        /// <param name="configuration">Vehicle configuration with which to intialize this one.</param>
        public VehicleConfiguration(IVehicleConfiguration configuration)
        {
            OptionActions = DeepCopy(configuration.OptionActions);
            OptionStates  = DeepCopy(configuration.OptionStates);
            State         = configuration.State;
            Uvc           = configuration.Uvc;
            Vin           = configuration.Vin;
            Mileage       = configuration.Mileage;
            BookDate      = new BookDate { Id = configuration.BookDate.Id };
        }

        /// <summary>
        /// Reference data version information.
        /// </summary>
        public BookDate BookDate { get; internal set; }

        /// <summary>
        /// US state identifier.
        /// </summary>
        public State State { get; internal set; }

        /// <summary>
        /// Uvc.
        /// </summary>
        public string Uvc { get; internal set; }

        /// <summary>
        /// Vin.
        /// </summary>
        public string Vin { get; internal set; }

        /// <summary>
        /// Mileage.
        /// </summary>
        public int? Mileage { get; internal set; }

        /// <summary>
        /// Option actions.
        /// </summary>
        public IList<IOptionAction> OptionActions { get; internal set; }

        /// <summary>
        /// Option states.
        /// </summary>
        public IList<IOptionState> OptionStates { get; internal set; }

        /// <summary>
        /// Compare the given vehicle configuration to this one to determine what is different.
        /// </summary>        
        /// <param name="configuration">Configuration to compare.</param>
        /// <returns>A bit field of what is different.</returns>
        public ChangeType Compare(IVehicleConfiguration configuration)
        {
            ChangeType changes = ChangeType.None;   
        
            // Book date.
            if (BookDate.Id != configuration.BookDate.Id)
            {
                changes |= ChangeType.BookDate;
            }

            // Uvc.
            if (Uvc != configuration.Uvc)
            {
                changes |= ChangeType.Uvc;
            }

            // US state.
            if (State != configuration.State)
            {
                changes |= ChangeType.State;
            }

            // Mileage.
            if (Mileage != configuration.Mileage)
            {
                changes |= ChangeType.Mileage;
            }

            // Option actions.
            if (OptionActions.Count != configuration.OptionActions.Count)
            {
                changes |= ChangeType.OptionActions;
            }
            else
            {
                foreach (IOptionAction lhsAction in OptionActions)
                {
                    bool found = false;
                    foreach (IOptionAction rhsAction in configuration.OptionActions)
                    {
                        if (lhsAction.ActionType == rhsAction.ActionType && lhsAction.Uoc == rhsAction.Uoc)
                        {
                            found = true;
                        }
                    }
                    if (!found)
                    {
                        changes |= ChangeType.OptionActions;
                        break;
                    }
                }
            }

            // Option states.
            if (OptionStates.Count != configuration.OptionStates.Count)
            {
                changes |= ChangeType.OptionStates;
            }
            else
            {
                foreach (IOptionState lhsState in OptionStates)
                {
                    bool found = false;
                    foreach (IOptionState rhsState in configuration.OptionStates)
                    {
                        if (lhsState.Enabled == rhsState.Enabled && 
                            lhsState.Selected == rhsState.Selected &&
                            lhsState.Uoc == rhsState.Uoc)
                        {
                            found = true;
                        }
                    }
                    if (!found)
                    {
                        changes |= ChangeType.OptionStates;
                        break;
                    }
                }
            }

            return changes;
        }

        /// <summary>
        /// Get the option state that matches the given adjustment.
        /// </summary>
        /// <param name="adjustment">Option adjustment.</param>
        /// <returns>Option state that matches, if any do.</returns>
        public OptionState this[Option adjustment]
        {
            get { return (OptionState) OptionStates.FirstOrDefault(x => Equals(x.Uoc, adjustment.UniversalOptionCode)); }
        }

        /// <summary>
        /// Create a deep copy of the given option actions.
        /// </summary>
        /// <param name="actions">Option actions to copy.</param>
        /// <returns>New list of the same option actions.</returns>
        protected static IList<IOptionAction> DeepCopy(IEnumerable<IOptionAction> actions)
        {            
            List<IOptionAction> copy = new List<IOptionAction>();

            foreach (IOptionAction action in actions)
            {
                copy.Add(new OptionAction { ActionType = action.ActionType, Uoc = action.Uoc});
            }

            return copy;
        }

        /// <summary>
        /// Create a deep copy of the given option states.
        /// </summary>
        /// <param name="states">Option states to copy.</param>
        /// <returns>New list of the same option states.</returns>
        protected static IList<IOptionState> DeepCopy(IEnumerable<IOptionState> states)
        {
            List<IOptionState> copy = new List<IOptionState>();

            foreach (IOptionState state in states)
            {
                copy.Add(new OptionState {Uoc = state.Uoc, Selected = state.Selected, Enabled = state.Enabled});
            }

            return copy;
        }
    }
}