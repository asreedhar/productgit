﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// Interface for a BlackBook repository.
    /// </summary>
    public interface IBlackBookRepository
    {        
        #region Car Methods

        /// <summary>
        /// Load the car that has the given uvc and vin and is currently valid.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="state">Two letter US state identifier. Optional.</param>
        /// <returns>Car.</returns>
        CarEntity Load_Car(string uvc, string vin, string state);

        /// <summary>
        /// Load the car that has the given uvc and vin that was valid at the given date.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="state">Two letter US state identifier. Optional.</param>
        /// <param name="date">Time period to get a car record for.</param>
        /// <returns>Car.</returns>
        CarEntity Load_Car(string uvc, string vin, string state, DateTime date);

        /// <summary>
        /// Load the car with the given uvc that is currently valid. If there are multiple vins in the database for 
        /// this uvc, it will just return the first.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="state">Two letter US state identifier. Optional.</param>
        /// <returns>Car.</returns>
        CarEntity Load_Car(string uvc, string state);

        /// <summary>
        /// Load the car with the given uvc that was valid at the given date. If there are multiple vins in the 
        /// database for this uvc, it will just return the first.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="state">Two letter US state identifier. Optional.</param>
        /// <param name="date">Time period to get a car record for.</param>
        /// <returns>Car.</returns>
        CarEntity Load_Car(string uvc, string state, DateTime date);

        /// <summary>
        /// Load the uvcs tied to the given vin.
        /// </summary>
        /// <param name="vin">Vin.</param>
        /// <returns>List of uvcs.</returns>
        IList<string> Load_Uvc(string vin);

        /// <summary>
        /// Save a car.
        /// </summary>        
        /// <param name="car">Car to save.</param>
        /// <param name="state">Two letter US state identifier.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>Car entity that was either newly saved or already existed.</returns>
        CarEntity Save_Car(Car car, string state, int dataLoadId);

        /// <summary>
        /// Is the cache for the car with the given values valid?
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="state">Two letter US state identifier.</param>
        /// <returns>True if we're in the cache window, false otherwise.</returns>
        bool CarCache_IsValid(string uvc, string vin, string state);

        /// <summary>
        /// Is the cache valid for any car with the given uvc?
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="state">Two letter US state identifier. Optional.</param>
        /// <returns>True if we're in the cache window for any car with that uvc, false otherwise.</returns>
        bool CarCache_IsValid(string uvc, string state);

        /// <summary>
        /// Extend the cache window for the car with the given values.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="state">Two letter US state identifier.</param>
        /// <returns>Whether the operation succeeded or not.</returns>
        bool CarCache_Extend(string uvc, string vin, string state);

        /// <summary>
        /// Extend the cache window for the car with the given identifier.
        /// </summary>
        /// <param name="carId">Car identifier.</param>
        /// <returns>Whether the operation succeeded or not.</returns>
        bool CarCache_Extend(int carId);

        #endregion        

        #region Standard Equipment Methods

        /// <summary>
        /// Get the standard equipment for the vehicle with the given uvc.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <returns>Standard equipment entity.</returns>
        StandardEquipmentEntity Load_Equipment(string uvc);

        /// <summary>
        /// Save standard equipment for the vehicle with the given uvc.
        /// </summary>        
        /// <param name="uvc">Uvc.</param>
        /// <param name="equipment">Standard equipment.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>Standard equipment entity.</returns>
        StandardEquipmentEntity Save_Equipment(string uvc, StandardEquipment equipment, int dataLoadId);

        /// <summary>
        /// Is the cache for the standard equipment for the car with the given uvc valid?
        /// </summary>
        /// <param name="uvc">Uvc.</param>        
        /// <returns>True if we're in the cache window, false otherwise.</returns>
        bool EquipmentCache_IsValid(string uvc);

        /// <summary>
        /// Extend the cache window for the standard equipment for the car with the given uvc.
        /// </summary>
        /// <param name="uvc">Uvc.</param>      
        /// <returns>Whether the operation succeeded or not.</returns>
        bool EquipmentCache_Extend(string uvc);

        /// <summary>
        /// Extend the cache window for the standard equipment with the given identifier.
        /// </summary>
        /// <param name="standardEquipmentId">Standard equipment identifier.</param>
        /// <returns>Whether the operation succeeded or not.</returns>
        bool EquipmentCache_Extend(int standardEquipmentId);

        #endregion

        #region Vehicle Configuration Methods

        /// <summary>
        /// Fetch a vehicle configuration.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="state">Two letter US state identifier.</param>
        /// <param name="mileage">Mileage.</param>
        /// <returns>Vehicle configuration entity.</returns>
        VehicleConfigurationEntity Load_Configuration(string uvc, string vin, string state, int? mileage);

        /// <summary>        
        /// Save a vehicle configuration.
        /// </summary>                
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Client-vehicle association.</param>        
        /// <param name="configuration">Vehicle configuration to save.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>Vehicle configuration entity.</returns>
        VehicleConfigurationEntity Save_Configuration(IBroker broker, ClientVehicleIdentification vehicle, 
                                                      IVehicleConfiguration configuration, int dataLoadId);

        #endregion

        #region Matrix Methods

        /// <summary>
        /// Load the price matrix for the car and configuration with the given identifiers.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <returns>Price matrix.</returns>
        Matrix Load_Matrix(int carHistoryId, int vehicleConfigurationHistoryId);

        /// <summary>
        /// Save a price matrix for the car and configuration with the given identifiers.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <param name="matrix">Price matrix to save.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>The price matrix that was saved.</returns>
        Matrix Save_Matrix(int carHistoryId, int vehicleConfigurationHistoryId, Matrix matrix, int dataLoadId);

        #endregion

        #region Publication Methods

        /// <summary>
        /// Load info about the publications tied to the given broker and vehicle.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Vehicle.</param>
        /// <returns>List of publication details.</returns>
        IList<IEdition<IPublicationInfo>> Load(IBroker broker, ClientVehicleIdentification vehicle);

        /// <summary>
        /// Load the publication for the given broker and vehicle that is valid at the given time.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Vehicle.</param>
        /// <param name="on">Date on which the publication should be valid.</param>
        /// <returns>Publication.</returns>
        IEdition<IPublication> Load(IBroker broker, ClientVehicleIdentification vehicle, DateTime on);

        /// <summary>
        /// Load the publication for the given broker and vehicle that has the given identifier.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Vehicle.</param>
        /// <param name="id">Publication identifier (a.k.a. vehicle configuration history identifier).</param>
        /// <returns>Publication.</returns>
        IEdition<IPublication> Load(IBroker broker, ClientVehicleIdentification vehicle, int id);

        /// <summary>
        /// Save the configuration and price details for the given client and vehicle.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="principal">Principal.</param>
        /// <param name="vehicle">Vehicle.</param>
        /// <param name="configuration">Vehicle configuration.</param>
        /// <param name="matrix">Price details.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>Publication that was saved.</returns>
        IEdition<IPublication> Save(IBroker broker, IPrincipal principal, ClientVehicleIdentification vehicle, 
                                    IVehicleConfiguration configuration, Matrix matrix, int dataLoadId);

        #endregion
    }
}
