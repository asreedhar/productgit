﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// How cars vary from each other.
    /// </summary>
    [Serializable]
    internal enum VaryBy
    {
        /// <summary>
        /// Cars do not vary.
        /// </summary>
        None,
        
        /// <summary>
        /// By make.
        /// </summary>
        Make,

        /// <summary>
        /// By model.
        /// </summary>
        Model,

        /// <summary>
        /// By series.
        /// </summary>
        Series,

        /// <summary>
        /// By body style.
        /// </summary>
        BodyStyle
    }
}