﻿namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// Path in a traversal tree.
    /// </summary>
    public class TreePath : ITreePath
    {
        /// <summary>
        /// The state of the path. If the user has selected a year, make and model, this might look something like 
        /// "year=2008||make=Ford||model=Focus".
        /// </summary>
        public string State { get; set; }
    }
}