﻿namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// Interface for a BlackBook car, configuration and valuation.
    /// </summary>
    public interface IPublication : IPublicationInfo
    {
        /// <summary>
        /// Car for which the configuration and valuation apply.
        /// </summary>
        Car Car { get; }

        /// <summary>
        /// Configuration of the car.
        /// </summary>
        IVehicleConfiguration VehicleConfiguration { get; }

        /// <summary>
        /// Valuation of the configuration.
        /// </summary>
        Matrix Matrix { get; }
    }
}
