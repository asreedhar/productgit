﻿using FirstLook.Client.DomainModel.Clients.Model;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// An edition of a publication of type T that was released at a certain time by a particular user.
    /// </summary>
    /// <typeparam name="T">Publication data.</typeparam>
    public interface IEdition<T>
    {
        /// <summary>
        /// Date range for which the data is valid.
        /// </summary>
        IDateRange DateRange { get; }

        /// <summary>
        /// The user responsible for the data.
        /// </summary>
        IUser User { get; }

        /// <summary>
        /// The data.
        /// </summary>
        T Data { get; }
    }
}
