﻿namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// A BlackBook car, configuration and valuation.
    /// </summary>
    public class Publication : PublicationInfo, IPublication
    {
        /// <summary>
        /// Car for which the configuration and valuation apply.
        /// </summary>
        public Car Car { get; internal set; }

        /// <summary>
        /// Configuration of the car.
        /// </summary>
        public IVehicleConfiguration VehicleConfiguration { get; internal set; }

        /// <summary>
        /// Valuation of the configuration.
        /// </summary>
        public Matrix Matrix { get; internal set; }        
    }
}
