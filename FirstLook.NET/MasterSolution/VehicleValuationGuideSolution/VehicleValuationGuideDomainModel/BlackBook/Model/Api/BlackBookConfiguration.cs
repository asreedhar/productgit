﻿using System;
using System.Linq;
using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// BlackBook configuration methods.
    /// </summary>
    public class BlackBookConfiguration : IBlackBookConfiguration
    {
        /// <summary>
        /// Get the default configuration of the vehicle defined by the given values.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="state">State.</param>
        /// <param name="mileage">Mileage.</param>
        /// <returns>Default vehicle configuration.</returns>
        public IVehicleConfiguration InitialConfiguration(string uvc, string vin, State state, int? mileage)
        {            
            Adjustment adjustment = new Adjustment { State = state == null ? string.Empty : state.Code };

            Car vehicle = string.IsNullOrEmpty(vin) 
                            ? BlackBookService.Vehicle(uvc, adjustment)
                            : BlackBookService.Vehicle(uvc, vin, adjustment);

            VehicleConfiguration configuration = new VehicleConfiguration(uvc, state, vin, mileage, BlackBookService.BookDate());

            if (vehicle != null)
            {
                InitializeConfiguration(vehicle, configuration);
            }

            return configuration;
        }

        /// <summary>
        /// Set the default configuration options for the given vehicle.
        /// </summary>
        /// <param name="vehicle">Vehicle.</param>
        /// <param name="configuration">Vehicle configuration to populate with options.</param>
        private static void InitializeConfiguration(Car vehicle, IVehicleConfiguration configuration)
        {            
            foreach (Option option in vehicle.Options)
            {
                configuration.OptionStates.Add(new OptionState
                {
                    Uoc      = option.UniversalOptionCode,
                    Selected = option.Flag ? true : false, // If default, the option is selected.
                    Enabled  = option.Flag ? false : true  // If default, the option is not clickable.
                });
            }            
        }

        /// <summary>
        /// Update the given configuration with the given user action.
        /// </summary>
        /// <param name="configuration">Vehicle configuration.</param>
        /// <param name="action">Option action.</param>
        /// <returns>Updated vehicle configuration.</returns>
        public IVehicleConfiguration UpdateConfiguration(IVehicleConfiguration configuration, IOptionAction action)
        {
            // Validate the input.
            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }
            if (action.ActionType == OptionActionType.Undefined)
            {
                throw new ArgumentOutOfRangeException("action", OptionActionType.Undefined, "Action type cannot be undefined");
            }
            
            Adjustment adjustment = new Adjustment { State = configuration.State == null ? string.Empty : configuration.State.Code };

            // Get the vehicle (by vin, if one was given).
            Car vehicle = string.IsNullOrEmpty(configuration.Vin)
                              ? BlackBookService.Vehicle(configuration.Uvc, adjustment)
                              : BlackBookService.Vehicle(configuration.Uvc, configuration.Vin, adjustment);
            
            bool repeat = configuration.OptionActions.Any(
                x => x.ActionType == action.ActionType &&
                     Equals(x.Uoc, action.Uoc));

            if (repeat)
            {
                return configuration; // been there, done that ...
            }

            VehicleConfiguration newConfiguration;

            bool undo = configuration.OptionActions.Any(
                x => x.ActionType != action.ActionType &&
                     Equals(x.Uoc, action.Uoc));

            if (undo)
            {
                newConfiguration = new VehicleConfiguration(
                    configuration.Uvc,
                    configuration.State,
                    configuration.Vin,
                    configuration.Mileage,
                    BlackBookService.BookDate());                

                InitializeConfiguration(vehicle, newConfiguration);

                foreach (IOptionAction replay in configuration.OptionActions)
                {
                    if (!Equals(replay.Uoc, action.Uoc))
                    {
                        PerformAction(replay, vehicle, newConfiguration);
                    }
                }
            }
            else
            {
                newConfiguration = new VehicleConfiguration(configuration);

                PerformAction(action, vehicle, newConfiguration);
            }

            return newConfiguration;
        }

        /// <summary>
        /// Perform an option action on the given configuration of the given vehicle.
        /// </summary>
        /// <param name="action">Option action.</param>
        /// <param name="vehicle">Vehicle.</param>
        /// <param name="configuration">Vehicle configuration to update.</param>
        private static void PerformAction(IOptionAction action, Car vehicle, VehicleConfiguration configuration)
        {
            Option adjustment = vehicle.Options.FirstOrDefault(
                x => Equals(x.UniversalOptionCode, action.Uoc));

            if (adjustment == null)
            {
                throw new ArgumentException(string.Format("No such Option {0}", action.Uoc), "action");
            }

            configuration.OptionActions.Add(action);

            switch (action.ActionType)
            {
                case OptionActionType.Undefined:
                    throw new ArgumentException("Undefined OptionActionType", "action");
                case OptionActionType.Add:
                    AddOption(configuration, adjustment);
                    break;
                case OptionActionType.Remove:
                    RemoveOption(configuration, adjustment);
                    break;
            }
        }

        /// <summary>
        /// Add an option to a vehicle configuration.
        /// </summary>
        /// <param name="configuration">Vehicle configuration to update.</param>
        /// <param name="adjustment">Option adjustment to add.</param>
        private static void AddOption(VehicleConfiguration configuration, Option adjustment)
        {
            OptionState adjustmentState = configuration[adjustment];

            if (adjustmentState == null || adjustmentState.Selected)
            {
                return;
            }

            adjustmentState.Selected = true;
        }

        /// <summary>
        /// Remove an option from a vehicle configuration.
        /// </summary>
        /// <param name="configuration">Vehicle configuration to update.</param>
        /// <param name="adjustment">Option adjustment to remove.</param>
        private static void RemoveOption(VehicleConfiguration configuration, Option adjustment)
        {
            OptionState adjustmentState = configuration[adjustment];

            if (adjustmentState == null || !adjustmentState.Selected)
            {
                return;
            }

            adjustmentState.Selected = false;
        }

        /// <summary>
        /// Helper for resolving the BlackBook service.
        /// </summary>
        protected static IBlackBookService BlackBookService
        {
            get { return RegistryFactory.GetResolver().Resolve<IBlackBookService>(); }
        }
    }
}
