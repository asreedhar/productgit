﻿namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// Interface for a path in a traversal tree.
    /// </summary>
    public interface ITreePath
    {
        /// <summary>
        /// The state of the path. If the user has selected a year, make and model, this might look something like 
        /// "year=2008||make=Ford||model=Focus".
        /// </summary>
        string State { get; set; }
    }
}