﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// Calculator for determining prices for a BlackBook vehicle configuration.
    /// </summary>
    public class BlackBookCalculator : IBlackBookCalculator
    {
        /// <summary>
        /// Calculate the value of the given option as applied to the given car.
        /// </summary>
        /// <param name="car">Car.</param>
        /// <param name="option">Option to apply to the car.</param>
        /// <returns>Price values.</returns>
        public Value Calculate(CarInfo car, Option option)
        {
            return SkipOption(option) ? new Value() : OptionValue(car, option);
        }

        /// <summary>
        /// Calculate the valuation of the car with the given configuration.
        /// </summary>
        /// <param name="car">Car.</param>
        /// <param name="configuration">Configuration of the car.</param>
        /// <returns>Matrix of price values.</returns>
        public Matrix Calculate(Car car, IVehicleConfiguration configuration)
        {
            Matrix matrix = new Matrix();

            CalculateBase(car, matrix);
            CalculateOptions(car, configuration, matrix);
            CalculateMileage(car, configuration, matrix);
            CalculateFinal(matrix);
            CalculateVisibility(car, configuration, matrix);

            return matrix;
        }

        /// <summary>
        /// Sets the base valuation of a car. <code>internal</code> for testing.
        /// </summary>
        /// <param name="car">Car.</param>
        /// <param name="matrix">Price matrix to set with base values.</param>
        internal void CalculateBase(Car car, Matrix matrix)
        {
            Set(Market.Retail,    matrix, car.RetailValue,    Valuation.Base);
            Set(Market.Wholesale, matrix, car.WholesaleValue, Valuation.Base);
            Set(Market.TradeIn,   matrix, car.TradeInValue,   Valuation.Base);

            // BUGZID: 15234 - Finance advance changes by option and mileage adjustments. See FB case for details.
            matrix[Market.FinanceAdvance, Condition.Average, Valuation.Base].Value = 
                car.FinanceAdvance != null ? car.FinanceAdvance.Value : 0;
        }

        /// <summary>
        /// Calculate the price changes for a vehicle configuration due to option adjustments.
        /// </summary>
        /// <param name="car">Car.</param>
        /// <param name="configuration">Configuration of the car.</param>
        /// <param name="matrix">Price matrix to set with option adjustment values.</param>
        private static void CalculateOptions(Car car, IVehicleConfiguration configuration, Matrix matrix)
        {
            Value total = new Value();

            int yearDiff = YearDiff(car);

            // Get the selected option adjustments.
            IEnumerable<Option> options = car.Options.Where(
                x => configuration.OptionStates.Any(
                         y => y.Selected && Equals(y.Uoc, x.UniversalOptionCode)));

            // Two special kinds of option adjustments are handled differently.
            bool exempt = options.Any(x => Equals(x.AdCode, "RR") || Equals(x.AdCode, "RG"));

            // Sum up all option adjustments per condition.
            foreach (Option option in options)
            {
                if (SkipOption(option))
                {
                    continue;
                }

                Value value = OptionValue(car, option, yearDiff);

                total.ExtraClean = Sum(total.ExtraClean, value.ExtraClean);
                total.Clean      = Sum(total.Clean,      value.Clean);
                total.Average    = Sum(total.Average,    value.Average);
                total.Rough      = Sum(total.Rough,      value.Rough);                
            }

            // Apply option adjustments, while also factorining in 50% rule.
            ValueConditionAdapter optionValuation = new ValueConditionAdapter(total);
            foreach (Condition condition in Enum.GetValues(typeof(Condition)))
            {
                if (condition == Condition.Undefined)
                {
                    continue;
                }

                int? value = optionValuation[condition];

                if (!value.HasValue)
                {
                    continue;
                }

                foreach (Market market in Enum.GetValues(typeof (Market)))
                {
                    if(market == Market.Undefined)
                    {
                        continue;
                    }

                    int amount = value.Value;

                    int? full = matrix[market, condition, Valuation.Base].Value;

                    if (full.HasValue)
                    {
                        if (full.Value == 0)
                        {
                            matrix[market, condition, Valuation.Option].Value = 0;
                        }
                        else
                        {
                            if (yearDiff > 6 && !exempt)
                            {
                                double half = Math.Round(Convert.ToDouble(full) / 2.0d);

                                if (half < Math.Abs(amount))
                                {
                                    amount = Math.Sign(amount)*Convert.ToInt32(half);
                                }
                            }

                            matrix[market, condition, Valuation.Option].Value = amount;
                        }
                    }
                    else
                    {
                        matrix[market, condition, Valuation.Option].Value = null;
                    }
                }
            }
        }

        /// <summary>
        /// Get the price values of an option adjustment on a car.
        /// </summary>
        /// <param name="car">Car.</param>
        /// <param name="option">Option adjustment on the car.</param>
        /// <returns>Price values.</returns>
        private static Value OptionValue(CarInfo car, Option option)
        {
            return OptionValue(car, option, YearDiff(car));
        }

        /// <summary>
        /// Get the price values of an option adjustment on a car that is the given number of years old.
        /// </summary>
        /// <param name="car">Car.</param>
        /// <param name="option">Option adjustment on the car.</param>
        /// <param name="yearDiff">Number of years between this year and the car model year.</param>
        /// <returns>Price values.</returns>
        private static Value OptionValue(CarInfo car, Option option, int yearDiff)
        {
            Value value = new Value();

            string code = option.AdCode;

            int? amount = option.Amount;

            if (option.OptionAdjustmentSign == OptionAdjustmentSign.Deduct)
            {
                amount = -amount;
            }

            bool isRr = "RR".Equals(code, StringComparison.OrdinalIgnoreCase);

            bool isRg = "RG".Equals(code, StringComparison.OrdinalIgnoreCase);

            if (isRr || isRg)
            {
                if (yearDiff > 6)
                {
                    value.ExtraClean = null;
                    value.Clean = amount;
                    value.Average = amount - 500;

                    if (isRr)
                    {
                        value.Rough = amount - 1200;
                    }
                    else //if (isRg), implied by isRr || is Rg
                    {
                        value.Rough = amount - 1050;
                    }
                }
                else
                {
                    value.ExtraClean = amount + 400;
                    value.Clean = amount;

                    if (car.Model.Contains('1') ||
                        car.Model.Contains('2') ||
                        car.Model.Contains('3'))
                    {
                        value.Average = amount - 600;
                        value.Rough = amount - 1300;
                    }
                    else
                    {
                        value.Average = amount - 500;
                        value.Rough = amount - 1100;
                    }
                }
            }
            else
            {
                value.ExtraClean = amount; //this might need to be null.
                value.Clean      = amount;
                value.Average    = amount;
                value.Rough      = amount;
            }

            return value;
        }

        /// <summary>
        /// Should an option be skipped during calculation?
        /// </summary>
        /// <param name="option">Option adjustment.</param>
        /// <returns>True if the option is null or has no value.</returns>
        private static bool SkipOption(Option option)
        {
            if (option == null)
            {
                return true;
            }

            if (string.IsNullOrEmpty(option.AdCode))
            {
                return true;
            }

            if (!option.Amount.HasValue)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Calculate the price changes for a vehicle configuration due to option adjustments.
        /// </summary>
        /// <param name="car">Car.</param>
        /// <param name="configuration">Configuration of the car.</param>
        /// <param name="matrix">Price matrix to set with mileage adjustment values.</param>
        private static void CalculateMileage(Car car, IVehicleConfiguration configuration, Matrix matrix)
        {
            if (!configuration.Mileage.HasValue)
            {
                return;
            }

            int mileage = configuration.Mileage.Value;            

            foreach (MileageAdjustment adjustment in car.MileageAdjustments)
            {
                if (adjustment.Covers(mileage))
                {                    
                    ValueConditionAdapter mileageAdjustmentValuation = new ValueConditionAdapter(adjustment);                    

                    foreach (Condition condition in Enum.GetValues(typeof(Condition)))
                    {
                        if(condition == Condition.Undefined)
                        {
                            continue;
                        }

                        int? amount = mileageAdjustmentValuation[condition];

                        if (!amount.HasValue)
                        {
                            continue;
                        }

                        // 50% rule
                        foreach (Market market in Enum.GetValues(typeof (Market)))
                        {
                            if (market == Market.Undefined)
                            {
                                continue;
                            }

                            // If we're dealing with Finance Advance, pull the amount out of the special FinanceAdvance 
                            // portion of the adjustment.
                            if (market == Market.FinanceAdvance)
                            {
                                if (adjustment.FinanceAdvance.HasValue)
                                {
                                    amount = adjustment.FinanceAdvance;
                                }
                                else
                                {
                                    continue;
                                }
                            }

                            int? baseValue = matrix[market, condition, Valuation.Base].Value;

                            if (!baseValue.HasValue || baseValue.Value == 0)
                            {
                                continue;
                            }

                            int? optionValue = matrix[market, condition, Valuation.Option].Value;

                            int baseAndOption = baseValue.Value;

                            if (optionValue.HasValue)
                            {
                                baseAndOption += optionValue.Value;
                            }

                            //Yes, this logic is weird but correct.  
                            //take 1/2 the FINAL value (base + opt + mileage), then compare against the mileage adjustment again!
                            int theAmount = amount.Value;
                            double half = Math.Round(Convert.ToDouble(baseAndOption) / 2.0d);

                            if (half < Math.Abs(amount.Value))
                            {
                                amount = Convert.ToInt32(Math.Round(Math.Sign(theAmount)*(Convert.ToDouble(baseAndOption)/2.0d)));
                            }

                            matrix[market, condition, Valuation.Mileage].Value = amount;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Calculate the final price changes - adds option and mileage adjustments to the base value for each kind of
        /// market and condition.
        /// </summary>        
        /// <param name="matrix">Price matrix to set with final values.</param>
        private static void CalculateFinal(Matrix matrix)
        {
            foreach (Condition condition in Enum.GetValues(typeof(Condition)))
            {
                if(condition == Condition.Undefined)
                {
                    continue;
                }

                foreach (Market market in Enum.GetValues(typeof (Market)))
                {
                    if (market == Market.Undefined)
                    {
                        continue;
                    }

                    int? baseValue = matrix[market, condition, Valuation.Base].Value;

                    if (!baseValue.HasValue || baseValue.Value == 0)
                    {
                        continue;
                    }

                    int value = baseValue.Value;

                    int? option = matrix[market, condition, Valuation.Option].Value;

                    if (option.HasValue)
                    {
                        value += option.Value;
                    }

                    int? mileage = matrix[market, condition, Valuation.Mileage].Value;

                    if (mileage.HasValue)
                    {
                        value += mileage.Value;
                    }

                    matrix[market, condition, Valuation.Final].Value = value;
                }
            }
        }

        /// <summary>
        /// Set a row in the price matrix with the given values.
        /// </summary>
        /// <param name="market">Market.</param>
        /// <param name="matrix">Price matrix to set with values.</param>
        /// <param name="value">Values to set.</param>
        /// <param name="valuation">Kind of valuation.</param>
        private static void Set(Market market, Matrix matrix, IValue value, Valuation valuation)
        {
            matrix[market, Condition.ExtraClean, valuation].Value = value.ExtraClean;
            matrix[market, Condition.Clean,      valuation].Value = value.Clean;
            matrix[market, Condition.Average,    valuation].Value = value.Average;
            matrix[market, Condition.Rough,      valuation].Value = value.Rough;
        }

        /// <summary>
        /// Calculate the visibility of each cell in the price matrix.
        /// </summary>
        /// <param name="car">Car.</param>
        /// <param name="configuration">Configuration of the car.</param>
        /// <param name="matrix">Price matrix.</param>
        internal static void CalculateVisibility(Car car, IVehicleConfiguration configuration, Matrix matrix)
        {
            // Base.
            SetVisibility(matrix, Market.Retail,    Valuation.Base, car.RetailValue);
            SetVisibility(matrix, Market.Wholesale, Valuation.Base, car.WholesaleValue);
            SetVisibility(matrix, Market.TradeIn,   Valuation.Base, car.TradeInValue);

            // BUGZID: 15234 - Finance advance changes by option and mileage adjustments. See FB case for details.
            Value financeAdvanceValue = new Value { Average = matrix[Market.FinanceAdvance, Condition.Average, Valuation.Base].Value };            
            SetVisibility(matrix, Market.FinanceAdvance, Valuation.Base, financeAdvanceValue);

            foreach (Market market in Enum.GetValues(typeof (Market)))
            {
                if (market == Market.Undefined)
                {
                    continue;
                }

                foreach (Condition condition in Enum.GetValues(typeof (Condition)))
                {
                    if (condition == Condition.Undefined)
                    {
                        continue;
                    }

                    matrix[market, condition, Valuation.Mileage].Visibility =
                        matrix[market, condition, Valuation.Base].Visibility;

                    // Mileage.
                    // Excessive Mileage may render an entire Condition NotApplicable.

                    if (configuration.Mileage.HasValue)                    
                    {
                        int mileage = configuration.Mileage.Value;                        

                        foreach (MileageAdjustment adjustment in car.MileageAdjustments)
                        {
                            if (!adjustment.Covers(mileage))
                            {
                                continue;
                            }

                            ValueConditionAdapter mileageAdjustmentValuation = new ValueConditionAdapter(adjustment);

                            int? mileageAmount = mileageAdjustmentValuation[condition];

                            if (!mileageAmount.HasValue)
                            {
                                matrix[market, condition, Valuation.Base].Visibility =
                                    Matrix.CellVisibility.NotApplicable;

                                matrix[market, condition, Valuation.Mileage].Visibility =
                                    Matrix.CellVisibility.NotApplicable;
                            }
                        }
                    }

                    // Options, depends on Base (after adjusted by mileage as well).
                    matrix[market, condition, Valuation.Option].Visibility =
                        matrix[market, condition, Valuation.Base].Visibility;

                    // Final values also are bound by Base (after adjusted by mileage).                                        
                    matrix[market, condition, Valuation.Final].Visibility =
                        matrix[market, condition, Valuation.Base].Visibility;
                }
            }
        }

        /// <summary>
        /// Calculate the visibility of each cell in a price matrix.
        /// </summary>
        /// <param name="matrix">Price matrix.</param>
        /// <param name="market">Market.</param>        
        /// <param name="valuation">Valuation.</param>
        /// <param name="value">Condition values for the market + valuation rows in the configuration.</param>        
        private static void SetVisibility(Matrix matrix, Market market, Valuation valuation, IValue value)
        {
            ValueConditionAdapter theVal = new ValueConditionAdapter(value);

            foreach(Condition condition in Enum.GetValues(typeof(Condition)))
            {
                int? val = theVal[condition];

                Matrix.CellVisibility cellVisibility = Matrix.CellVisibility.NotDisplayed;
                if(val.HasValue)
                {
                    cellVisibility = val.Value == 0 ? Matrix.CellVisibility.NotApplicable : Matrix.CellVisibility.Value;
                }

                matrix[market, condition, valuation].Visibility = cellVisibility;
            }
        }

        /// <summary>
        /// Helper function to get the difference in years between today and the BlackBook year cutoff.
        /// </summary>
        /// <param name="car">Car information.</param>
        /// <returns>Difference in years.</returns>
        private static int YearDiff(CarInfo car)
        {
            DateTime today = DateTime.Now.Date;

            int year;

            if (today > new DateTime(today.Year, 4, 24))
            {
                year = today.Year;
            }
            else
            {
                year = today.Year - 1;
            }

            return year - car.Year;
        }

        /// <summary>
        /// Helper function for adding two nullable ints.
        /// </summary>
        /// <param name="a">Nullable integer.</param>
        /// <param name="b">Nullable integer.</param>
        /// <returns>Nullable sum.</returns>
        private static int? Sum(int? a, int? b)
        {            
            if (a.HasValue)
            {
                if (b.HasValue)
                {
                    return a.Value + b.Value;
                }
                return a.Value;
            }

            if (b.HasValue)
            {
                return b.Value;
            }
            return null;
        }
    }
}
