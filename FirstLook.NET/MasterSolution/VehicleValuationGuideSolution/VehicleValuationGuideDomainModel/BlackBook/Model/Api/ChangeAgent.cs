﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// The agent that caused a vehicle configuration to change.
    /// </summary>
    [Serializable]
    public enum ChangeAgent
    {
        /// <summary>
        /// Not defined.
        /// </summary>
        Undefined,

        /// <summary>
        /// User.
        /// </summary>
        User,

        /// <summary>
        /// System.
        /// </summary>
        System
    }
}
