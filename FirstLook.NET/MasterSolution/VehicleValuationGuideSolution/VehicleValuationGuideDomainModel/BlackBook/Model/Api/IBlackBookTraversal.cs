namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// Inteface for traversal methods for defining a vehicle via drilldown options.
    /// </summary>
    public interface IBlackBookTraversal
    {
        /// <summary>
        /// Create a new tree.
        /// </summary>
        /// <returns>Tree defined by a root node.</returns>
        ITree CreateTree();

        /// <summary>
        /// Create a new tree with a state and / or mileage adjustment.
        /// </summary>
        /// <param name="adjustment">State and / or mileage adjustment.</param>
        /// <returns>Tree defined by a root node and adjustment.</returns>
        ITree CreateTree(Adjustment adjustment);

        /// <summary>
        /// Create a tree off the provided vin.
        /// </summary>
        /// <param name="vin">Vin.</param>
        /// <returns>Tree initialized with a vin.</returns>
        ITree CreateTree(string vin);

        /// <summary>
        /// Create a tree off the provided vin with a state and / or mileage adjustment.
        /// </summary>
        /// <param name="vin">Vin.</param>
        /// <param name="adjustment">State and / or mileage adjustment.</param>
        /// <returns>Tree initialized with a vin and adjustment.</returns>
        ITree CreateTree(string vin, Adjustment adjustment);
    }
}