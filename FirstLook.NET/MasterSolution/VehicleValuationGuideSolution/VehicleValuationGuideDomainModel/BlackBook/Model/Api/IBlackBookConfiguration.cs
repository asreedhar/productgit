﻿namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// Interface for BlackBook configuration methods.
    /// </summary>
    public interface IBlackBookConfiguration
    {
        /// <summary>
        /// Get the default configuration of the vehicle defined by the given values.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="state">State.</param>
        /// <param name="mileage">Mileage.</param>
        /// <returns>Default vehicle configuration.</returns>
        IVehicleConfiguration InitialConfiguration(string uvc, string vin, State state, int? mileage);

        /// <summary>
        /// Update the given configuration with the given user action.
        /// </summary>
        /// <param name="configuration">Vehicle configuration.</param>
        /// <param name="action">Option action.</param>
        /// <returns>Updated vehicle configuration.</returns>
        IVehicleConfiguration UpdateConfiguration(IVehicleConfiguration configuration, IOptionAction action);
    }
}
