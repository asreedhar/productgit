using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// State of a vehicle configuration option.
    /// </summary>
    [Serializable]
    public class OptionState : IOptionState
    {
        /// <summary>
        /// Universal option code (option identifier).
        /// </summary>
        public string Uoc { get; internal set; }

        /// <summary>
        /// Is this option selected in a vehicle configuration?
        /// </summary>
        public bool Selected { get; internal set; }

        /// <summary>
        /// Is this option able to be selected in a vehicle configuration?
        /// </summary>
        public bool Enabled { get; internal set; }
    }
}