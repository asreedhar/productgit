﻿using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// Interface for a node in a traversal tree.
    /// </summary>
    public interface ITreeNode
    {
        /// <summary>
        /// Parent node, e.g. a Year node.
        /// </summary>
        ITreeNode Parent { get; }

        /// <summary>
        /// Label of the node, e.g. "Make".
        /// </summary>
        string Label { get; }

        /// <summary>
        /// Value of the node, e.g. "Ford".
        /// </summary>
        string Value { get; }

        /// <summary>
        /// UVC. BlackBook vehicle identifier. Used if this node is specific enough to identify a vehicle.
        /// </summary>
        string UniversalVehicleCode { get; }

        /// <summary>
        /// Children below this node.
        /// </summary>
        IList<ITreeNode> Children { get; }

        /// <summary>
        /// Does this node have children?
        /// </summary>
        bool HasChildren { get; }

        /// <summary>
        /// Set the given path to reflect this node.
        /// </summary>
        /// <param name="path">Tree path.</param>
        void Save(ITreePath path);
    }
}