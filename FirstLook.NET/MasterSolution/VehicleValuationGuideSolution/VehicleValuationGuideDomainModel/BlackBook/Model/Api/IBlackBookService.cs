namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// Interface for a BlackBook service.
    /// </summary>
    public interface IBlackBookService
    {
        /// <summary>
        /// Get the current book date.
        /// </summary>
        /// <returns>Current data load identifier.</returns>
        BookDate BookDate();

        /// <summary>
        /// Get the car with the given uvc.
        /// </summary>
        /// <remarks>        
        /// Uvc-only lookups are not saved to the database, since we don't trust the vin that BlackBook returns. This 
        /// ensures values in the database are uvc + vin only.
        /// </remarks>
        /// <param name="uvc">Uvc.</param>
        /// <returns>Car.</returns>
        Car Vehicle(string uvc);        

        /// <summary>
        /// Get the car with the given uvc and state and mileage.
        /// </summary>
        /// <remarks>        
        /// Uvc-only lookups are not saved to the database, since we don't trust the vin that BlackBook returns. This 
        /// ensures values in the database are uvc + vin only.
        /// </remarks>
        /// <param name="uvc">Uvc.</param>
        /// <param name="adjustment">State and mileage.</param>
        /// <returns>Car.</returns>
        Car Vehicle(string uvc, Adjustment adjustment);

        /// <summary>
        /// Get the car with the given uvc and vin.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <returns>Car.</returns>
        Car Vehicle(string uvc, string vin);

        /// <summary>
        /// Get the car with the given uvc, vin, state and mileage.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="adjustment">State and mileage.</param>
        /// <returns>Car.</returns>
        Car Vehicle(string uvc, string vin, Adjustment adjustment);

        /// <summary>
        /// Get the standard equipment of the car with the given uvc.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <returns>Standard equipment.</returns>
        StandardEquipment Equipment(string uvc);
    }
}