using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// Vehicle configuration option action.
    /// </summary>
    [Serializable]
    public class OptionAction : IOptionAction
    {
        /// <summary>
        /// Universal option code (option identifier).
        /// </summary>
        public string Uoc { get; internal set; }

        /// <summary>
        /// Action type.
        /// </summary>
        public OptionActionType ActionType { get; internal set; }
    }
}