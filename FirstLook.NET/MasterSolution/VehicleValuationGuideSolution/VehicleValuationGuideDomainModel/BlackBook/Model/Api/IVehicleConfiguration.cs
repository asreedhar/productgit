using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// Vehicle configuration interface.
    /// </summary>
    public interface IVehicleConfiguration
    {
        /// <summary>
        /// Reference data version information.
        /// </summary>
        BookDate BookDate { get; }

        /// <summary>
        /// US state identifier. Optional.
        /// </summary>
        State State { get; }

        /// <summary>
        /// Vehicle identifier.
        /// </summary>
        string Uvc { get; }

        /// <summary>
        /// Vin.
        /// </summary>
        string Vin { get; }

        /// <summary>
        /// Mileage. Optional.
        /// </summary>
        int? Mileage { get; }

        /// <summary>
        /// Option actions.
        /// </summary>
        IList<IOptionAction> OptionActions { get; }

        /// <summary>
        /// Option states.
        /// </summary>
        IList<IOptionState> OptionStates { get; }

        /// <summary>
        /// Compare the given vehicle configuration to this one to determine what is different.
        /// </summary>        
        /// <param name="configuration">Configuration to compare.</param>
        /// <returns>A bit field of what is different.</returns>
        ChangeType Compare(IVehicleConfiguration configuration);
    }
}