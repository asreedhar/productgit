using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Lookup;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// Traversal methods for defining a vehicle via drilldown options.
    /// </summary>
    public class BlackBookTraversal : IBlackBookTraversal
    {
        /// <summary>
        /// Separator to be used in a path between nodes. For example, "year=2008||make=Ford".
        /// </summary>
        protected const string PairSeparator = "||";

        /// <summary>
        /// Separator to be used between a node label and value. For example "year=2008".
        /// </summary>
        protected const string ValueSeparator = "=";

        #region Tree Creation

        /// <summary>
        /// Create a new tree.
        /// </summary>
        /// <returns>Tree defined by a root node.</returns>
        public ITree CreateTree()
        {
            return new Tree
            {
                Root = new InitialNode()
            };
        }

        /// <summary>
        /// Create a new tree with a state and / or mileage adjustment.
        /// </summary>
        /// <param name="adjustment">State and / or mileage adjustment.</param>
        /// <returns>Tree defined by a root node and adjustment.</returns>
        public ITree CreateTree(Adjustment adjustment)
        {
            return new Tree
            {
                Adjustment = adjustment,
                Root = new InitialNode()
            };
        }

        /// <summary>
        /// Create a tree off the provided vin.
        /// </summary>
        /// <param name="vin">Vin.</param>
        /// <returns>Tree initialized with a vin.</returns>
        public ITree CreateTree(string vin)
        {
            return new Tree
            {
                Root = new VinNode(vin)
            };
        }

        /// <summary>
        /// Create a tree off the provided vin with a state and / or mileage adjustment.
        /// </summary>
        /// <param name="vin">Vin.</param>
        /// <param name="adjustment">State and / or mileage adjustment.</param>
        /// <returns>Tree initialized with a vin and adjustment.</returns>
        public ITree CreateTree(string vin, Adjustment adjustment)
        {
            return new Tree
            {
                Adjustment = adjustment,
                Root = new VinNode(vin)
            };
        }

        #endregion        

        #region Tree / Node Implementations

        /// <summary>
        /// Tree.
        /// </summary>
        sealed class Tree : ITree
        {
            /// <summary>
            /// State and / or mileage adjustments.
            /// </summary>
            public Adjustment Adjustment { get; set; }

            /// <summary>
            /// Root node.
            /// </summary>
            public ITreeNode Root { get; set; }

            /// <summary>
            /// Get the node in the tree that has the given path.
            /// </summary>
            /// <param name="path">Node path.</param>
            /// <returns>Node.</returns>
            public ITreeNode GetNodeByPath(ITreePath path)
            {
                if (path != null)
                {
                    if (!string.IsNullOrEmpty(path.State))
                    {
                        string year = null, make = null, model = null, series = null, style = null;

                        string vin = null, uvc = null;

                        bool hasSeries = false;

                        string[] values = path.State.Split(new[] { PairSeparator }, StringSplitOptions.None);

                        foreach (var value in values)
                        {
                            string[] pair = value.Split(new[] { ValueSeparator }, StringSplitOptions.None);

                            string k = pair[0], v = pair[1];

                            switch (k)
                            {
                                case "year":
                                    year = v;
                                    break;
                                case "make":
                                    make = v;
                                    break;
                                case "model":
                                    model = v;
                                    break;
                                case "series":
                                    hasSeries = true;
                                    series = v;
                                    break;
                                case "style":
                                    style = v;
                                    break;
                                case "vin":
                                    vin = v;
                                    break;
                                case "uvc":
                                    uvc = v;
                                    break;
                            }
                        }

                        if (string.IsNullOrEmpty(year))
                        {
                            if (string.IsNullOrEmpty(vin))
                            {
                                return new InitialNode();
                            }

                            return new VinNode(vin);
                        }

                        YearNode yearNode = new YearNode(year);

                        if (string.IsNullOrEmpty(make))
                        {
                            return yearNode;
                        }

                        MakeNode makeNode = new MakeNode(yearNode, make);

                        if (string.IsNullOrEmpty(model))
                        {
                            return makeNode;
                        }

                        ModelNode modelNode = new ModelNode(makeNode, model);

                        if (!hasSeries)
                        {
                            return modelNode;
                        }

                        SeriesNode seriesNode = new SeriesNode(modelNode, series);

                        if (string.IsNullOrEmpty(style))
                        {
                            return seriesNode;
                        }

                        return new BodyStyleNode(seriesNode, style, uvc);
                    }
                }

                return Root;
            }
        }        

        /// <summary>
        /// Root node of a tree. Children are year nodes.
        /// </summary>
        sealed class InitialNode : TreeNode
        {
            private IList<ITreeNode> _children;

            /// <summary>
            /// Children - list of year nodes.
            /// </summary>
            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        _children = new List<ITreeNode>();

                        IList<string> values = Resolve<ILookupYear>().Lookup();

                        foreach (string value in values)
                        {
                            _children.Add(new YearNode(value));
                        }
                    }

                    return _children;
                }
            }

            /// <summary>
            /// Set the given path to represent this node.
            /// </summary>
            /// <param name="path">Tree path.</param>
            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = string.Empty;
                }
            }
        }

        /// <summary>
        /// Year node.
        /// </summary>
        sealed class YearNode : TreeNode
        {
            /// <summary>
            /// Create this year node to be a child of the given parent.
            /// </summary>
            /// <param name="parent">Parent node.</param>
            /// <param name="year">Year value.</param>
            public YearNode(ITreeNode parent, string year) : base(parent, "Year", year)
            {
            }

            /// <summary>
            /// Create a year node.
            /// </summary>
            /// <param name="year">Year value.</param>
            public YearNode(string year) : base("Year", year)
            {
            }

            private IList<ITreeNode> _children;

            /// <summary>
            /// Children - list of makes.
            /// </summary>
            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        _children = new List<ITreeNode>();

                        IList<string> infos = Resolve<ILookupMake>().Lookup(Value);

                        foreach (string value in infos)
                        {
                            _children.Add(new MakeNode(this, value));
                        }
                    }

                    return _children;
                }
            }

            /// <summary>
            /// Set the given path to represent this node.
            /// </summary>
            /// <param name="path">Tree path.</param>
            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = Serialize(this);
                }
            }
        }

        /// <summary>
        /// Make node.
        /// </summary>
        sealed class MakeNode : TreeNode
        {
            /// <summary>
            /// Create this make node as a child of the given parent.
            /// </summary>
            /// <param name="parent">Parent node.</param>
            /// <param name="value">Make value.</param>
            public MakeNode(ITreeNode parent, string value) : base(parent, "Make", value)
            {
            }            

            private IList<ITreeNode> _children;

            /// <summary>
            /// Children - list of models.
            /// </summary>
            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        _children = new List<ITreeNode>();

                        IList<CarInfo> infos = Resolve<ILookupModel>().Lookup(Parent.Value, Value);

                        foreach (CarInfo info in infos)
                        {
                            _children.Add(new ModelNode(this, info.Model));
                        }
                    }

                    return _children;
                }
            }

            /// <summary>
            /// Set the given path to represent this node.
            /// </summary>
            /// <param name="path">Tree path.</param>
            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = Serialize(Parent) + PairSeparator + Serialize(this);
                }
            }
        }

        /// <summary>
        /// Model node.
        /// </summary>
        sealed class ModelNode : TreeNode
        {
            /// <summary>
            /// Create this model node as a child of the given parent.
            /// </summary>
            /// <param name="parent">Parent node.</param>
            /// <param name="value">Model value.</param>
            public ModelNode(ITreeNode parent, string value) : base(parent, "Model", value)
            {
            }
           
            private IList<ITreeNode> _children;

            /// <summary>
            /// Children - list of series.
            /// </summary>
            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        _children = new List<ITreeNode>();

                        IList<CarInfo> infos = Resolve<ILookupSeries>().Lookup(Parent.Parent.Value, Parent.Value, Value);

                        foreach (CarInfo info in infos)
                        {
                            _children.Add(new SeriesNode(this, info.Series));
                        }
                    }

                    return _children;
                }
            }

            /// <summary>
            /// Set the given path to represent this node.
            /// </summary>
            /// <param name="path">Tree path.</param>
            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = Serialize(Parent.Parent)
                        + PairSeparator + Serialize(Parent)
                        + PairSeparator + Serialize(this);
                }
            }
        }

        /// <summary>
        /// Series node.
        /// </summary>
        sealed class SeriesNode : TreeNode
        {
            /// <summary>
            /// Create this series node as a child of the given node.
            /// </summary>
            /// <param name="parent">Parent node.</param>
            /// <param name="value">Series value.</param>
            public SeriesNode(ITreeNode parent, string value) : base(parent, "Series", value)
            {
            }            

            private IList<ITreeNode> _children;

            /// <summary>
            /// Children - list of body styles.
            /// </summary>
            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        _children = new List<ITreeNode>();

                        IList<CarInfo> infos = Resolve<ILookupBodyStyle>().Lookup(
                            Parent.Parent.Parent.Value,
                            Parent.Parent.Value,
                            Parent.Value,
                            Value);

                        foreach (CarInfo info in infos)
                        {
                            _children.Add(new BodyStyleNode(this, info.BodyStyle, info.Uvc));
                        }
                    }

                    return _children;
                }
            }

            /// <summary>
            /// Set the given path to represent this node.
            /// </summary>
            /// <param name="path">Tree path.</param>
            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = Serialize(Parent.Parent.Parent)
                        + PairSeparator + Serialize(Parent.Parent)
                        + PairSeparator + Serialize(Parent)
                        + PairSeparator + Serialize(this);
                }
            }
        }

        /// <summary>
        /// Body style node.
        /// </summary>
        sealed class BodyStyleNode : TreeNode
        {
            /// <summary>
            /// Create this series node as a child of the given node.
            /// </summary>
            /// <param name="parent">Parent node.</param>
            /// <param name="value">Body style value.</param>
            /// <param name="universalVehicleCode">Uvc.</param>
            public BodyStyleNode(ITreeNode parent, string value, string universalVehicleCode) : base(parent, "Style", value, universalVehicleCode)
            {
            }

            /// <summary>
            /// Children - empty.
            /// </summary>
            public override IList<ITreeNode> Children
            {
                get { return new ITreeNode[0]; }
            }

            /// <summary>
            /// Set the given path to represent this node.
            /// </summary>
            /// <param name="path">Tree path.</param>
            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = Serialize(Parent.Parent.Parent.Parent)
                                 + PairSeparator + Serialize(Parent.Parent.Parent)
                                 + PairSeparator + Serialize(Parent.Parent)
                                 + PairSeparator + Serialize(Parent)
                                 + PairSeparator + Serialize(this)
                                 + PairSeparator + "uvc" + ValueSeparator + UniversalVehicleCode;
                }
            }
        }

        /// <summary>
        /// Vin node.
        /// </summary>
        sealed class VinNode : TreeNode
        {
            /// <summary>
            /// Create a vin node with the given vin.
            /// </summary>
            /// <param name="value">Vin.</param>
            public VinNode(string value) : base("Vin", value)
            {
            }

            private IList<CarInfo> _info;

            private IList<ITreeNode> _children;

            /// <summary>
            /// Children of this vin node.
            /// </summary>
            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        _children = new List<ITreeNode>();

                        _info = Resolve<ILookupVin>().Lookup(Value);

                        switch (Vary.By(_info))
                        {
                            case VaryBy.Make:
                                Make(_info);
                                break;
                            case VaryBy.Model:
                                Model(_info);
                                break;
                            case VaryBy.Series:
                                Series(_info);
                                break;
                            case VaryBy.BodyStyle:
                                BodyStyle(_info);
                                break;
                            default:
                                BodyStyle(_info);
                                break;
                        }
                    }

                    return _children;
                }
            }

            /// <summary>
            /// Set the make node from the given list of car info.
            /// </summary>
            /// <param name="infos">List of car info.</param>
            private void Make(IList<CarInfo> infos)
            {
                for (int i = 0; i < infos.Count; i++)
                {
                    CarInfo info = infos[i];

                    string year = info.Year.ToString(),
                           make = info.Make,
                           model = info.Model,
                           series = info.Series,
                           bodyStyle = info.BodyStyle,
                           uvc = info.Uvc;

                    if (i == 0)
                    {
                        ITreeNode initialNode = new InitialNode(),
                                  yearNode = new YearNode(initialNode, year);

                        _parent = yearNode;
                    }

                    ITreeNode makeNode = new MakeNode(_parent, make),
                              modelNode = new ModelNode(makeNode, model),
                              seriesNode = new SeriesNode(modelNode, series),
                              bodyStyleNode = new BodyStyleNode(seriesNode, bodyStyle, uvc);

                    _children.Add(bodyStyleNode);
                }
            }

            /// <summary>
            /// Set the model node from the given list of car info.
            /// </summary>
            /// <param name="infos">List of car info.</param>
            private void Model(IList<CarInfo> infos)
            {
                for (int i = 0; i < infos.Count; i++)
                {
                    CarInfo info = infos[i];

                    string year = info.Year.ToString(),
                           make = info.Make,
                           model = info.Model,
                           series = info.Series,
                           bodyStyle = info.BodyStyle,
                           uvc = info.Uvc;

                    if (i == 0)
                    {
                        ITreeNode initialNode = new InitialNode(),
                                  yearNode = new YearNode(initialNode, year),
                                  makeNode = new MakeNode(yearNode, make);

                        _parent = makeNode;
                    }

                    ITreeNode modelNode = new ModelNode(_parent, model),
                              seriesNode = new SeriesNode(modelNode, series),
                              bodyStyleNode = new BodyStyleNode(seriesNode, bodyStyle, uvc);

                    _children.Add(bodyStyleNode);
                }
            }

            /// <summary>
            /// Set the series node from the given list of car info.
            /// </summary>
            /// <param name="infos">List of car info.</param>
            private void Series(IList<CarInfo> infos)
            {
                for (int i = 0; i < infos.Count; i++)
                {
                    CarInfo info = infos[i];

                    string year = info.Year.ToString(),
                           make = info.Make,
                           model = info.Model,
                           series = info.Series,
                           bodyStyle = info.BodyStyle,
                           uvc = info.Uvc;

                    if (i == 0)
                    {
                        ITreeNode initialNode = new InitialNode(),
                                  yearNode = new YearNode(initialNode, year),
                                  makeNode = new MakeNode(yearNode, make),
                                  modelNode = new ModelNode(makeNode, model);

                        _parent = modelNode;
                    }

                    ITreeNode seriesNode = new SeriesNode(_parent, series),
                              bodyStyleNode = new BodyStyleNode(seriesNode, bodyStyle, uvc);

                    _children.Add(bodyStyleNode);
                }
            }

            /// <summary>
            /// Set the body style node from the given list of car info.
            /// </summary>
            /// <param name="infos">List of car info.</param>
            private void BodyStyle(IList<CarInfo> infos)
            {
                for (int i = 0; i < infos.Count; i++)
                {
                    CarInfo info = infos[i];

                    if (i == 0)
                    {
                        string year = info.Year.ToString(),
                               make = info.Make,
                               model = info.Model,
                               series = info.Series;

                        ITreeNode initialNode = new InitialNode(),
                                  yearNode = new YearNode(initialNode, year),
                                  makeNode = new MakeNode(yearNode, make),
                                  modelNode = new ModelNode(makeNode, model),
                                  seriesNode = new SeriesNode(modelNode, series);

                        _parent = seriesNode;
                    }

                    _children.Add(new BodyStyleNode(_parent, info.BodyStyle, info.Uvc));
                }
            }

            private ITreeNode _parent;

            /// <summary>
            /// Parent node.
            /// </summary>
            public override ITreeNode Parent
            {
                get
                {
                    IList<ITreeNode> eek = Children; // why oh why

                    return _parent;
                }
            }

            /// <summary>
            /// Uvc.
            /// </summary>
            public override string UniversalVehicleCode
            {
                get
                {
                    return Children.Count == 1 ? _info.First().Uvc : base.UniversalVehicleCode;
                }
            }

            /// <summary>
            /// Set the given path to represent this node.
            /// </summary>
            /// <param name="path">Tree path.</param>
            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = Serialize(this);
                }
            }
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Helper for resolving a type from the registry.
        /// </summary>
        /// <typeparam name="T">Type to get an implementation for.</typeparam>
        /// <returns>Implementatino of T.</returns>
        protected static T Resolve<T>()
        {
            return RegistryFactory.GetResolver().Resolve<T>();
        }

        /// <summary>
        /// Produce a string representation of the given node.
        /// </summary>
        /// <param name="node">Node.</param>
        /// <returns>String in the form of "year=2008".</returns>
        static string Serialize(ITreeNode node)
        {
            return node.Label.ToLower() + ValueSeparator + node.Value;
        }

        #endregion
    }
}
