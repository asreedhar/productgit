﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// How a vehicle configuration changed from the previous version.
    /// </summary>
    [Serializable]
    [Flags]
    public enum ChangeType
    {
        /// <summary>
        /// No change.
        /// </summary>
        None = 0,

        /// <summary>
        /// US state changed.
        /// </summary>
        State = 1 << 0,

        /// <summary>
        /// Uvc changed.
        /// </summary>
        Uvc = 1 << 1,

        /// <summary>
        /// Mileage changed.
        /// </summary>
        Mileage = 1 << 2,

        /// <summary>
        /// Option actions changed.
        /// </summary>
        OptionActions = 1 << 3,

        /// <summary>
        /// Option states changed.
        /// </summary>
        OptionStates = 1 << 4,

        /// <summary>
        /// Reference data changed.
        /// </summary>
        BookDate = 1 << 5
    }
}
