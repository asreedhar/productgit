﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// Represents the prices BlackBook shows on their website.
    /// </summary>
    [Serializable]
    public class Matrix
    {
        /// <summary>
        /// Visibility of a cell in a price matrix.
        /// </summary>
        public enum CellVisibility
        {
            /// <summary>
            /// Cell is visible and has a value.
            /// </summary>
            Value,

            /// <summary>
            /// Cell is not applicable.
            /// </summary>
            NotApplicable,

            /// <summary>
            /// Cell is hidden.
            /// </summary>
            NotDisplayed
        }

        /// <summary>
        /// Cell in a price matrix.
        /// </summary>
        public class Cell
        {
            /// <summary>
            /// Default a cell to having a value.
            /// </summary>
            internal Cell()
            {
                Visibility = CellVisibility.Value;
            }

            /// <summary>
            /// Visibility of this cell.
            /// </summary>
            public CellVisibility Visibility { get; set; }

            /// <summary>
            /// Value of this cell, if it has one.
            /// </summary>
            public int? Value { get; set; }
        }

        /// <summary>
        /// Get the number of markets.
        /// </summary>
        internal static readonly int MarketCount = Enum.GetNames(typeof (Market)).Length;

        /// <summary>
        /// Get the number of conditions.
        /// </summary>
        internal static readonly int ConditionCount = Enum.GetNames(typeof(Condition)).Length;

        /// <summary>
        /// Get the number of valuations.
        /// </summary>
        internal static readonly int ValuationCount = Enum.GetNames(typeof(Valuation)).Length;

        /// <summary>
        /// The price matrix.
        /// </summary>
        private readonly Cell[] _values;

        /// <summary>
        /// Initialize a new price matrix.
        /// </summary>
        public Matrix()
        {
            int arrayLength = MarketCount * ConditionCount * ValuationCount;

            _values = new Cell[arrayLength];

            foreach (Condition condition in Enum.GetValues(typeof(Condition)))
            {
                foreach (Market market in Enum.GetValues(typeof (Market)))
                {
                    foreach (Valuation valuation in Enum.GetValues(typeof (Valuation)))
                    {
                        Cell cell = new Cell();

                        if(market == Market.Undefined || condition == Condition.Undefined || valuation == Valuation.Undefined)
                        {
                            cell.Visibility = CellVisibility.NotDisplayed;
                        }

                        this[market, condition, valuation] = cell;
                    }
                }
            }
        }             

        /// <summary>
        /// Cell in the matrix that is for the given market, condition and valuation.
        /// </summary>
        /// <param name="market">Market.</param>
        /// <param name="condition">Condition.</param>
        /// <param name="valuation">Valuation.</param>
        /// <returns>Cell in the matrix.</returns>
        public Cell this[Market market, Condition condition, Valuation valuation]
        {
            get
            {
                return _values[Index(market, condition, valuation)];
            }
            set
            {
                if(value == null)
                {
                    throw new ArgumentException("A Matrix Cell should never be set to null!");
                }

                _values[Index(market, condition, valuation)] = value;
            }
        }

        /// <summary>
        /// Get the index of the cell in the matrix that is for the given market, condition and valuation.
        /// </summary>
        /// <param name="market">Market.</param>
        /// <param name="condition">Condition.</param>
        /// <param name="valuation">Valuation.</param>
        /// <returns>Cell index.</returns>
        private static int Index(Market market, Condition condition, Valuation valuation)
        {
            int rowOffset = ConditionCount * ValuationCount;
            int colOffset = ValuationCount;

            int row = ((int) market);

            int col = ((int) condition);

            int dep = ((int) valuation);

            return row * rowOffset + col * colOffset + dep;
        }
    }
}