﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// A date range.
    /// </summary>
    public class DateRange : IDateRange
    {
        /// <summary>
        /// Beginning of the date range.
        /// </summary>
        public DateTime BeginDate { get; internal set; }

        /// <summary>
        /// End of the date range.
        /// </summary>
        public DateTime EndDate { get; internal set; }

        /// <summary>
        /// Is the given date covered by this range?
        /// </summary>
        /// <param name="date">Date to check if covered.</param>
        /// <returns>True if the date is covered, false otherwise.</returns>
        public bool Covers(DateTime date)
        {
            return date >= BeginDate && date <= EndDate;
        }
    }
}
