﻿namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// Interface for a calculator for determining values of BlackBook configurations.
    /// </summary>
    public interface IBlackBookCalculator
    {
        /// <summary>
        /// Calculate the value of the option for the given car.
        /// </summary>
        /// <param name="car">Car details.</param>
        /// <param name="option">Option details.</param>
        /// <returns>Price.</returns>
        Value Calculate(CarInfo car, Option option);

        /// <summary>
        /// Calculate a price matrix for the given vehicle configuration.
        /// </summary>
        /// <param name="car">Car.</param>
        /// <param name="configuration">Vehicle configuration.</param>
        /// <returns>Price matrix.</returns>
        Matrix Calculate(Car car, IVehicleConfiguration configuration);
    }
}
