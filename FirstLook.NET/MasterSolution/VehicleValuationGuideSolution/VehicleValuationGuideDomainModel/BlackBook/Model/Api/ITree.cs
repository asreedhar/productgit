﻿namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// Interface for a traversal tree.
    /// </summary>
    public interface ITree
    {
        /// <summary>
        /// State and / or mileage adjustments.
        /// </summary>
        Adjustment Adjustment { get; }

        /// <summary>
        /// Root node of the tree.
        /// </summary>
        ITreeNode Root { get; }

        /// <summary>
        /// Get the node in this tree that matches the given path.
        /// </summary>
        /// <param name="path">Tree path.</param>
        /// <returns>Node that matches the path in this tree.</returns>
        ITreeNode GetNodeByPath(ITreePath path);
    }
}