using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Lookup;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// BlackBook service.
    /// </summary>
    public class BlackBookService : IBlackBookService
    {
        /// <summary>
        /// Get the current book date.
        /// </summary>
        /// <returns>Current data load identifier.</returns>
        public BookDate BookDate()
        {
            return Resolve<ILookupBookDate>().Lookup();
        }

        /// <summary>
        /// Get the car with the given uvc.
        /// </summary>
        /// <remarks>        
        /// Uvc-only lookups are not saved to the database, since we don't trust the vin that BlackBook returns. This 
        /// ensures values in the database are uvc + vin only.
        /// </remarks>
        /// <param name="uvc">Uvc.</param>
        /// <returns>Car.</returns>
        public Car Vehicle(string uvc)
        {                        
            return Resolve<ILookupUvc>().Lookup(uvc, null);
        }        

        /// <summary>
        /// Get the car with the given uvc and state and mileage.
        /// </summary>
        /// <remarks>        
        /// Uvc-only lookups are not saved to the database, since we don't trust the vin that BlackBook returns. This 
        /// ensures values in the database are uvc + vin only.
        /// </remarks>
        /// <param name="uvc">Uvc.</param>
        /// <param name="adjustment">State and mileage.</param>
        /// <returns>Car.</returns>
        public Car Vehicle(string uvc, Adjustment adjustment)
        {
            return Resolve<ILookupUvc>().Lookup(uvc, adjustment.State);
        }

        /// <summary>
        /// Get the car with the given uvc and vin.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <returns>Car.</returns>
        public Car Vehicle(string uvc, string vin)
        {
            return Resolve<ILookupUvc>().Lookup(uvc, vin, null);
        }

        /// <summary>
        /// Get the car with the given uvc, vin, state and mileage.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="adjustment">State and mileage.</param>
        /// <returns>Car.</returns>
        public Car Vehicle(string uvc, string vin, Adjustment adjustment)
        {
            return Resolve<ILookupUvc>().Lookup(uvc, vin, adjustment.State);            
        }

        /// <summary>
        /// Get the standard equipment of the car with the given uvc.
        /// </summary>              
        /// <param name="uvc">Uvc.</param>
        /// <returns>Standard equipment.</returns>
        public StandardEquipment Equipment(string uvc)
        {
            return Resolve<ILookupStandardEquipment>().Lookup(uvc);
        }

        /// <summary>
        /// Helper function for resolving an implementation of an interface.
        /// </summary>
        /// <typeparam name="T">Interface to get an implementation for.</typeparam>
        /// <returns>Implementation of T.</returns>
        protected static T Resolve<T>()
        {
            return RegistryFactory.GetResolver().Resolve<T>();
        }
    }
}
