﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// Adapts an IValue with a convenience method to access it by the Condition enum.  Made to reduce namespace coupling.
    /// </summary>
    [Serializable]
    internal class ValueConditionAdapter : IValue
    {
        private readonly IValue _value;

        /// <summary>
        /// Populate from the given value.
        /// </summary>
        /// <param name="value">Value.</param>
        public ValueConditionAdapter(IValue value)
        {
            if(value == null)
            {
                throw new ArgumentNullException("value", "IValue to adapt should not be null.");
            }

            _value = value;
        }

        /// <summary>
        /// Get the value for the given condition, if it exists.
        /// </summary>
        /// <param name="condition">Condition to retrieve value for.</param>
        /// <returns>Value for the condition, if it exists.</returns>
        public int? this[Condition condition]
        {
            get
            {
                int? val = null;

                switch (condition)
                {
                    case Condition.ExtraClean:
                        val = _value.ExtraClean;
                        break;
                    case Condition.Clean:
                        val = _value.Clean;
                        break;
                    case Condition.Average:
                        val = _value.Average;
                        break;
                    case Condition.Rough:
                        val = _value.Rough;
                        break;
                    default:
                        break;
                }

                return val;
            }
        }

        #region Implementation of IValue

        /// <summary>
        /// Extra clean value.
        /// </summary>
        public int? ExtraClean
        {
            get { return _value.ExtraClean; }
        }

        /// <summary>
        /// Clean value.
        /// </summary>
        public int? Clean
        {
            get { return _value.Clean; }
        }

        /// <summary>
        /// Average value.
        /// </summary>
        public int? Average
        {
            get { return _value.Average; }
        }

        /// <summary>
        /// Rough value.
        /// </summary>
        public int? Rough
        {
            get { return _value.Rough; }
        }

        #endregion
    }
}
