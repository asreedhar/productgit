namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// Interface for a vehicle configuration option action.
    /// </summary>
    public interface IOptionAction
    {
        /// <summary>
        /// Universal option code (option identifier).
        /// </summary>
        string Uoc { get; }

        /// <summary>
        /// Action type.
        /// </summary>
        OptionActionType ActionType { get; }
    }
}