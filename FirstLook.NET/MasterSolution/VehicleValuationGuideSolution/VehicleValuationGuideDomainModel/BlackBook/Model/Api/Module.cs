﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// Module for registering BlackBook model components.
    /// </summary>
    public class Module : IModule
    {
        /// <summary>
        /// Register the key BlackBook components.
        /// </summary>
        /// <param name="registry">Registry.</param>
        public void Configure(IRegistry registry)
        {
            registry.Register<IBlackBookCalculator,    BlackBookCalculator>(ImplementationScope.Shared);
            registry.Register<IBlackBookConfiguration, BlackBookConfiguration>(ImplementationScope.Shared);
            registry.Register<IBlackBookService,       BlackBookService>(ImplementationScope.Shared);
            registry.Register<IBlackBookTraversal,     BlackBookTraversal>(ImplementationScope.Shared);
        }
    }
}
