﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// Helper for determining difference in cars.
    /// </summary>
    internal static class Vary
    {
        /// <summary>
        /// Determine how a list of cars varies from each other.
        /// </summary>
        /// <param name="values">List of cars.</param>
        /// <returns>How the cars vary.</returns>
        public static VaryBy By(IList<CarInfo> values)
        {
            if (values == null || values.Count < 2)
            {
                return VaryBy.None;
            }

            int ctr = 0;

            while (ctr < 4)
            {
                string reference = null, item = null;

                int lop = 0;

                for (int i = 0, l = values.Count; i < l; i++)
                {
                    CarInfo value = values[i];
                
                    if (value == null)
                    {
                        continue;
                    }

                    switch (ctr)
                    {
                        case 0:
                            item = value.Make;
                            break;
                        case 1:
                            item = value.Model;
                            break;
                        case 2:
                            item = value.Series;
                            break;
                        case 3:
                            item = value.BodyStyle;
                            break;
                    }

                    if (string.IsNullOrEmpty(item) && l == 1)
                    {
                        return (VaryBy) Enum.ToObject(typeof (VaryBy), ctr);
                    }

                    if (lop++ == 0)
                    {
                        reference = item;
                    }
                    else
                    {
                        if (!Equals(reference, item))
                        {
                            return (VaryBy) Enum.ToObject(typeof (VaryBy), ctr+1);
                        }
                    }
                }

                ctr++;
            }

            return VaryBy.None;
        }

        /// <summary>
        /// Get a list of different values according to how the given cars vary.
        /// </summary>
        /// <param name="values">List of cars.</param>
        /// <param name="vary">What value should be pulled from each car.</param>
        /// <returns>List of values.</returns>
        public static IList<string> To(IList<CarInfo> values, VaryBy vary)
        {
            IList<string> items = new List<string>();

            foreach (CarInfo value in values)
            {
                switch (vary)
                {
                    case VaryBy.Make:
                        items.Add(value.Make);
                        break;
                    case VaryBy.Model:
                        items.Add(value.Model);
                        break;
                    case VaryBy.Series:
                        items.Add(value.Series);
                        break;
                    case VaryBy.BodyStyle:
                        items.Add(value.BodyStyle);
                        break;
                }
            }

            return items;
        }
    }
}