﻿namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// Identifying details of a publication.
    /// </summary>
    public class PublicationInfo : IPublicationInfo
    {
        /// <summary>
        /// Who is responsible for this publication.
        /// </summary>
        public ChangeAgent ChangeAgent { get; internal set; }

        /// <summary>
        /// How this publication differs from a previous publication.
        /// </summary>
        public ChangeType ChangeType { get; internal set; }        

        /// <summary>
        /// Identifier of the configuration historical record.
        /// </summary>
        public int VehicleConfigurationHistoryId { get; internal set; }
    }
}
