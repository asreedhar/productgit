﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// Market for determining BlackBook prices.
    /// </summary>
    [Serializable]
    public enum Market
    {
        /// <summary>
        /// Undefined.
        /// </summary>
        Undefined,

        /// <summary>
        /// Retail.
        /// </summary>
        Retail,

        /// <summary>
        /// Wholesale.
        /// </summary>
        Wholesale,

        /// <summary>
        /// Trade-in.
        /// </summary>
        TradeIn,

        /// <summary>
        /// Finance advance.
        /// </summary>
        FinanceAdvance
    }
}
