﻿namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// Interface for the identifying details of a publication.
    /// </summary>
    public interface IPublicationInfo
    {
        /// <summary>
        /// Who is responsible for this publication.
        /// </summary>
        ChangeAgent ChangeAgent { get; }

        /// <summary>
        /// How this publication differs from a previous publication.
        /// </summary>
        ChangeType ChangeType { get; }

        /// <summary>
        /// Identifier of the configuration historical record.
        /// </summary>
        int VehicleConfigurationHistoryId { get; }
    }
}
