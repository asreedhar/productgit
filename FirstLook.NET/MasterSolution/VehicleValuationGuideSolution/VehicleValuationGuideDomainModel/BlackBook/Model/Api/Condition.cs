﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// Condition of a vehicle.
    /// </summary>
    [Serializable]
    public enum Condition
    {
        /// <summary>
        /// Undefined.
        /// </summary>
        Undefined,

        /// <summary>
        /// Extra clean.
        /// </summary>
        ExtraClean,

        /// <summary>
        /// Clean.
        /// </summary>
        Clean,

        /// <summary>
        /// Average.
        /// </summary>
        Average,

        /// <summary>
        /// Rough.
        /// </summary>
        Rough
    }
}
