namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// State and mileage adjustments to a car lookup.
    /// </summary>
    public class Adjustment
    {
        /// <summary>
        /// Two letter US state identifier.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Mileage.
        /// </summary>
        public int Mileage { get; set; }
    }
}