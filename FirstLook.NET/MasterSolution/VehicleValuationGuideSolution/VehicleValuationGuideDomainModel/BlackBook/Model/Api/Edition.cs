﻿using FirstLook.Client.DomainModel.Clients.Model;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// An edition of a publication of type T that was released at a certain time by a particular user.
    /// </summary>
    /// <typeparam name="T">Publication data.</typeparam>
    public class Edition<T> : IEdition<T>
    {
        /// <summary>
        /// Date range for which the data is valid.
        /// </summary>
        public IDateRange DateRange { get; internal set; }

        /// <summary>
        /// The user responsible for the data.
        /// </summary>
        public IUser User { get; internal set; }

        /// <summary>
        /// The data.
        /// </summary>
        public T Data { get; internal set; }
    }
}
