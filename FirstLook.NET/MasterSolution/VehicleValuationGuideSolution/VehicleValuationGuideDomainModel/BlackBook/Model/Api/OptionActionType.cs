using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// Type of an option action.
    /// </summary>
    [Serializable]
    public enum OptionActionType
    {
        /// <summary>
        /// No action defined.
        /// </summary>
        Undefined,

        /// <summary>
        /// Add an option.
        /// </summary>
        Add,

        /// <summary>
        /// Remove an option.
        /// </summary>
        Remove
    }
}