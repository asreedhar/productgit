namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{    
    /// <summary>
    /// Interface for the state of a vehicle configuration option.
    /// </summary>
    public interface IOptionState
    {
        /// <summary>
        /// Universal option code (option identifier).
        /// </summary>
        string Uoc { get; }

        /// <summary>
        /// Is this option selected in a vehicle configuration?
        /// </summary>
        bool Selected { get; }

        /// <summary>
        /// Is this option able to be selected in a vehicle configuration?
        /// </summary>
        bool Enabled { get; }
    }
}