﻿using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api
{
    /// <summary>
    /// Abstract base class of all tree nodes.
    /// </summary>
    internal abstract class TreeNode : ITreeNode
    {       
        /// <summary>
        /// Create an empty tree node.
        /// </summary>
        protected TreeNode()
            : this(string.Empty, string.Empty)
        {
        }

        /// <summary>
        /// Create a node with a label and value.
        /// </summary>
        /// <param name="label">Label of the node, e.g. "Year".</param>
        /// <param name="value">Value of the node, e.g. "2008".</param>
        protected TreeNode(string label, string value)
            : this(null, label, value)
        {
        }

        /// <summary>
        /// Create a tree node that is a child of another node, and has the given label and value.
        /// </summary>
        /// <param name="parent">Parent node, e.g. a Year node.</param>
        /// <param name="label">Label of the node, e.g. "Make".</param>
        /// <param name="value">Value of the node, e.g. "Ford".</param>
        protected TreeNode(ITreeNode parent, string label, string value)
            : this(parent, label, value, string.Empty)
        {            
        }

        /// <summary>
        /// Create a tree node that is a child of another node, has the given label and value, and has a UVC.
        /// </summary>
        /// <param name="parent">Parent node, e.g. a Year node.</param>
        /// <param name="label">Label of the node, e.g. "Make".</param>
        /// <param name="value">Value of the node, e.g. "Ford".</param>
        /// <param name="universalVehicleCode">UVC. BlackBook vehicle identifier.</param>
        protected TreeNode(ITreeNode parent, string label, string value, string universalVehicleCode)
        {
            Parent = parent;
            Label = label;
            Value = value;
            UniversalVehicleCode = universalVehicleCode;
        }

        /// <summary>
        /// Label of the node, e.g. "Make".
        /// </summary>
        public virtual string Label { get; private set; }

        /// <summary>
        /// Value of the node, e.g. "Ford".
        /// </summary>
        public virtual string Value { get; private set; }

        /// <summary>
        /// UVC. BlackBook vehicle identifier that is only used if this node is specific enough to identify a vehicle.
        /// </summary>
        public virtual string UniversalVehicleCode { get; private set; }

        /// <summary>
        /// Parent node, e.g. a Year node.
        /// </summary>
        public virtual ITreeNode Parent { get; private set; }

        /// <summary>
        /// Children to this node.
        /// </summary>
        public abstract IList<ITreeNode> Children { get; }

        /// <summary>
        /// Does this node have children?
        /// </summary>
        public virtual bool HasChildren
        {
            get
            {
                return Children.Count == 0;
            }
        }

        /// <summary>
        /// Set the given path to reflect this node.
        /// </summary>
        /// <param name="path">Tree path.</param>
        public abstract void Save(ITreePath path);
    }
}