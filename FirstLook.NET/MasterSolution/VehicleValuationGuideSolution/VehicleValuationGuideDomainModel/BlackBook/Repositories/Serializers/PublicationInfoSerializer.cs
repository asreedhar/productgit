﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Repositories;
using FirstLook.Client.DomainModel.Clients.Repositories.Entities;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Serializers
{
    /// <summary>
    /// Serializer for publication information.
    /// </summary>
    public class PublicationInfoSerializer : Serializer<IEdition<IPublicationInfo>>
    {
        /// <summary>
        /// Deserialize publication information from the data record.
        /// </summary>
        /// <param name="record">Data record.</param>
        /// <returns>Edition of publication information.</returns>
        public override IEdition<IPublicationInfo> Deserialize(IDataRecord record)
        {
            // Identifying details of the publication.
            IPublicationInfo publication = new PublicationInfo
            {                
                VehicleConfigurationHistoryId = record.GetInt32(record.GetOrdinal("VehicleConfigurationHistoryID")),
                ChangeAgent = (ChangeAgent)record.GetByte(record.GetOrdinal("ChangeAgentID")),
                ChangeType = (ChangeType)record.GetInt32(record.GetOrdinal("ChangeTypeBitFlags"))
            };

            // When the publication is valid.
            IDateRange range = new DateRange
            {
                BeginDate = record.GetDateTime(record.GetOrdinal("ValidFrom")),
                EndDate = record.GetDateTime(record.GetOrdinal("ValidUpTo"))
            };

            // User responsible for the publication.
            UserType userType = (UserType)record.GetByte(record.GetOrdinal("UserTypeID"));
            Guid userHandle = record.GetGuid(record.GetOrdinal("Handle"));
            int userId = record.GetInt32(record.GetOrdinal("UserID"));

            IUser user;
            switch (userType)
            {
                case UserType.Member:
                    user = new DealerRepository().Principal(userHandle).User;
                    break;
                default:
                    user = new AgentRepository().Principal(userId).User;
                    break;
            }

            // Publication details.
            return new Edition<IPublicationInfo>
            {
                Data = publication,
                DateRange = range,
                User = user
            };
        }
    }
}
