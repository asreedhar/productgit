﻿using System.Data;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Entities;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Serializers
{
    /// <summary>
    /// Serializer for vehicle configuration entities.
    /// </summary>
    public class VehicleConfigurationSerializer : Serializer<VehicleConfigurationEntity>
    {
        /// <summary>
        /// Deserialize vehicle configuration values from the data record.
        /// </summary>
        /// <param name="record">Data record.</param>
        /// <returns>Vehicle configuration entity.</returns>
        public override VehicleConfigurationEntity Deserialize(IDataRecord record)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            string stateCode = record.GetString(record.GetOrdinal("StateCode"));            

            string uvc = record.GetString(record.GetOrdinal("Uvc"));

            string vin = record.IsDBNull(record.GetOrdinal("Vin"))
                             ? string.Empty
                             : record.GetString(record.GetOrdinal("Vin"));

            int? mileage = record.IsDBNull(record.GetOrdinal("Mileage"))
                             ? default(int?)
                             : record.GetInt32(record.GetOrdinal("Mileage"));

            Model.State state = Model.State.Identify(stateCode);

            Model.BookDate bookDate = new Model.BookDate { Id = record.GetInt32(record.GetOrdinal("DataLoadID")) };

            return new VehicleConfigurationEntity
            {
                VehicleConfigurationHistoryId = record.GetInt32(record.GetOrdinal("VehicleConfigurationHistoryID")),
                VehicleConfigurationId        = record.GetInt32(record.GetOrdinal("VehicleConfigurationID")),
                CarHistoryId                  = record.GetInt32(record.GetOrdinal("CarHistoryID")),
                ChangeAgent                   = (ChangeAgent)record.GetByte(record.GetOrdinal("ChangeAgentID")),
                ChangeType                    = (ChangeType)record.GetInt32(record.GetOrdinal("ChangeTypeBitFlags")),
                AuditRow                      = resolver.Resolve<ISerializer<AuditRow>>().Deserialize(record),
                User                          = resolver.Resolve<ISerializer<User>>().Deserialize(record),
                VehicleConfiguration          = new VehicleConfiguration(uvc, state, vin, mileage, bookDate)
            };
        }
    }
}
