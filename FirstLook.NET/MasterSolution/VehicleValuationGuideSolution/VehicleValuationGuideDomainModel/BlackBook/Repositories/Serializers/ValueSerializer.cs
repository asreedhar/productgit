﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Serializers
{
    /// <summary>
    /// Serializer for value repository entities.
    /// </summary>
    public class ValueSerializer : Serializer<Value>
    {       
        /// <summary>
        /// Deserialize value values from the data record.
        /// </summary>
        /// <param name="record">Data record.</param>
        /// <returns>Value entity.</returns>
        public override Value Deserialize(IDataRecord record)
        {
            return new Value
            {
                Average      = DataRecord.GetNullableInt32(record, "Average"),
                CarHistoryId = record.GetInt32(record.GetOrdinal("CarHistoryID")),
                Clean        = DataRecord.GetNullableInt32(record, "Clean"),
                ExtraClean   = DataRecord.GetNullableInt32(record, "ExtraClean"),
                Rough        = DataRecord.GetNullableInt32(record, "Rough"),
                ValueId      = record.GetInt32(record.GetOrdinal("ValueID"))
            };
        }        
    }
}
