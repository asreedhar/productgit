﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Serializers
{
    /// <summary>
    /// Serializer for vehicle-to-configuration association data.
    /// </summary>
    public class ClientVehicleSerializer : Serializer<ClientVehicle>
    {
        /// <summary>
        /// Deserialize the vehicle-to-configuration association data.
        /// </summary>
        /// <param name="record">Data record.</param>
        /// <returns>Vehicle-to-configuration association.</returns>
        public override ClientVehicle Deserialize(IDataRecord record)
        {
            return new ClientVehicle
            {
                VehicleId = record.GetInt32(record.GetOrdinal("VehicleID")),
                VehicleConfigurationId = record.GetInt32(record.GetOrdinal("VehicleConfigurationID"))
            };
        }
    }
}
