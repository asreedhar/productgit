﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Serializers
{
    /// <summary>
    /// Serializer for swatch repository entities.
    /// </summary>
    public class SwatchSerializer : Serializer<Swatch>
    {
        /// <summary>
        /// Deserialize color swatch values from the data record.
        /// </summary>
        /// <param name="record">Data record.</param>
        /// <returns>Swatch entity.</returns>
        public override Swatch Deserialize(IDataRecord record)
        {
            return new Swatch
            {
                CarHistoryId = record.GetInt32 (record.GetOrdinal("CarHistoryID")),
                ColorId      = record.GetInt32 (record.GetOrdinal("ColorID")),
                HtmlColor    = record.GetString(record.GetOrdinal("HtmlColor")),
                HtmlIndex    = record.GetByte  (record.GetOrdinal("HtmlIndex"))
            };
        }
    }
}
