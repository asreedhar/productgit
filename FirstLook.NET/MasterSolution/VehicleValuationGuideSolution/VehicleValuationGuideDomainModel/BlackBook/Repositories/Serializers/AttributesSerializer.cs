﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Serializers
{
    /// <summary>
    /// Serializer for car attribute repository entities.
    /// </summary>
    public class AttributesSerializer : Serializer<Attributes>
    {
        /// <summary>
        /// Deserialize attribute values from the data record.
        /// </summary>
        /// <param name="record">Data record.</param>
        /// <returns>Attribute entity.</returns>
        public override Attributes Deserialize(IDataRecord record)
        {
            return new Attributes
            {
                BaseHorsepower    = record.GetString(record.GetOrdinal("BaseHorsepower")),
                CarHistoryId      = record.GetInt32 (record.GetOrdinal("CarHistoryID")),
                Cylinders         = record.GetString(record.GetOrdinal("Cylinders")),
                DriveTrain        = record.GetString(record.GetOrdinal("DriveTrain")),
                Engine            = record.GetString(record.GetOrdinal("Engine")),
                FuelType          = record.GetString(record.GetOrdinal("FuelType")),
                TaxableHorsepower = record.GetString(record.GetOrdinal("TaxableHorsepower")),
                TireSize          = record.GetString(record.GetOrdinal("TireSize")),
                Transmission      = record.GetString(record.GetOrdinal("Transmission")),
                VehicleClass      = record.GetString(record.GetOrdinal("VehicleClass")),
                Weight            = record.GetString(record.GetOrdinal("Weight")),
                WheelBase         = record.GetString(record.GetOrdinal("WheelBase"))
            };
        }
    }
}
