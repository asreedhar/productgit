﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Serializers
{
    /// <summary>
    /// Serializer for car price repository entities.
    /// </summary>
    public class PricesSerializer : Serializer<Prices>
    {
        /// <summary>
        /// Deserialize price values from the data record.
        /// </summary>
        /// <param name="record">Data record.</param>
        /// <returns>Prices entity.</returns>
        public override Prices Deserialize(IDataRecord record)
        {            
            return new Prices
            {
                CarHistoryId     = record.GetInt32(record.GetOrdinal("CarHistoryID")),
                FinanceAdvance   = DataRecord.GetNullableInt32(record, "FinanceAdvance"),
                Mileage          = DataRecord.GetNullableInt32(record, "Mileage"),
                Msrp             = DataRecord.GetNullableInt32(record, "Msrp"),
                PriceIncludes    = DataRecord.GetString(record, "PriceIncludes"),
                ResidualValueId  = record.GetInt32(record.GetOrdinal("ResidualValueID")),
                RetailValueId    = record.GetInt32(record.GetOrdinal("RetailValueID")),
                TradeInValueId   = record.GetInt32(record.GetOrdinal("TradeInValueID")),
                WholesaleValueId = record.GetInt32(record.GetOrdinal("WholesaleValueID"))

            };
        }
    }
}
