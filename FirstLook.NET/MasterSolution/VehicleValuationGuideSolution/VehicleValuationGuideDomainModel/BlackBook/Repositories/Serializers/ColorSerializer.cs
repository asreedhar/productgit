﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Serializers
{
    /// <summary>
    /// Serializer for color repository entities.
    /// </summary>
    public class ColorSerializer : Serializer<Color>
    {
        /// <summary>
        /// Deserialize color values from the data record.
        /// </summary>
        /// <param name="record">Data record.</param>
        /// <returns>Color entity.</returns>
        public override Color Deserialize(IDataRecord record)
        {
            return new Color
            {
                CarHistoryId = record.GetInt32 (record.GetOrdinal("CarHistoryID")),
                Category     = record.GetString(record.GetOrdinal("Category")),
                ColorId      = record.GetInt32 (record.GetOrdinal("ColorID")),
                Description  = record.GetString(record.GetOrdinal("Description"))
            };
        }
    }
}
