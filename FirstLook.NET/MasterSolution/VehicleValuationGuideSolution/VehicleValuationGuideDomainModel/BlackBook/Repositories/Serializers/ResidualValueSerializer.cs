﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Serializers
{
    /// <summary>
    /// Serializer for residual value repository entities.
    /// </summary>
    public class ResidualValueSerializer : Serializer<ResidualValue>
    {
        /// <summary>
        /// Deserialize residual values from the data record.
        /// </summary>
        /// <param name="record">Data record.</param>
        /// <returns>Residual value entity.</returns>
        public override ResidualValue Deserialize(IDataRecord record)
        {
            return new ResidualValue
            {
                CarHistoryId    = record.GetInt32(record.GetOrdinal("CarHistoryID")),
                ResidualValueId = record.GetInt32(record.GetOrdinal("ResidualValueID")),
                Resid12         = DataRecord.GetNullableInt32(record, "Resid12"),
                Resid24         = DataRecord.GetNullableInt32(record, "Resid24"),
                Resid30         = DataRecord.GetNullableInt32(record, "Resid30"),
                Resid36         = DataRecord.GetNullableInt32(record, "Resid36"),
                Resid42         = DataRecord.GetNullableInt32(record, "Resid42"),
                Resid48         = DataRecord.GetNullableInt32(record, "Resid48"),
                Resid60         = DataRecord.GetNullableInt32(record, "Resid60"),
                Resid72         = DataRecord.GetNullableInt32(record, "Resid72"),
            };
        }
    }
}
