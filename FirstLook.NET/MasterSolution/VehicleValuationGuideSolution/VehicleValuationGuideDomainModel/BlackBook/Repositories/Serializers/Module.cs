﻿using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Serializers
{
    /// <summary>
    /// Module for registering repository serializers.
    /// </summary>
    public class Module : IModule
    {
        /// <summary>
        /// Register repository serializers.
        /// </summary>
        /// <param name="registry"></param>
        public void Configure(IRegistry registry)
        {            
            registry.Register<ISerializer<Attributes>,                 AttributesSerializer>          (ImplementationScope.Shared);
            registry.Register<ISerializer<ClientVehicle>,              ClientVehicleSerializer>       (ImplementationScope.Shared);
            registry.Register<ISerializer<Color>,                      ColorSerializer>               (ImplementationScope.Shared);
            registry.Register<ISerializer<Description>,                DescriptionSerializer>         (ImplementationScope.Shared);
            registry.Register<ISerializer<MileageAdjustment>,          MileageAdjustmentSerializer>   (ImplementationScope.Shared);
            registry.Register<ISerializer<Option>,                     OptionSerializer>              (ImplementationScope.Shared);            
            registry.Register<ISerializer<Prices>,                     PricesSerializer>              (ImplementationScope.Shared);
            registry.Register<ISerializer<ResidualValue>,              ResidualValueSerializer>       (ImplementationScope.Shared);
            registry.Register<ISerializer<Swatch>,                     SwatchSerializer>              (ImplementationScope.Shared);
            registry.Register<ISerializer<Value>,                      ValueSerializer>               (ImplementationScope.Shared);
            registry.Register<ISerializer<VehicleConfigurationEntity>, VehicleConfigurationSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<IEdition<IPublicationInfo>>, PublicationInfoSerializer>     (ImplementationScope.Shared);            
        }
    }
}
