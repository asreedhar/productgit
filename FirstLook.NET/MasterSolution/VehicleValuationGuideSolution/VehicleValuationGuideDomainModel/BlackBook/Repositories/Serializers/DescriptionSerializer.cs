﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Serializers
{
    /// <summary>
    /// Serializer for car description repository entities.
    /// </summary>
    public class DescriptionSerializer :  Serializer<Description>
    {
        /// <summary>
        /// Deserialize decsription values from the data record.
        /// </summary>
        /// <param name="record">Data record.</param>
        /// <returns>Description entity.</returns>
        public override Description Deserialize(IDataRecord record)
        {
            return new Description
            {
                BodyStyle    = record.GetString(record.GetOrdinal("BodyStyle")),
                CarHistoryId = record.GetInt32 (record.GetOrdinal("CarHistoryID")),
                Make         = record.GetString(record.GetOrdinal("Make")),
                Model        = record.GetString(record.GetOrdinal("Model")),
                Series       = record.GetString(record.GetOrdinal("Series")),
                Year         = record.GetInt32 (record.GetOrdinal("Year"))
            };
        }
    }
}
