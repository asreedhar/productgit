﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Serializers
{
    /// <summary>
    /// Serializer for option repository entities.
    /// </summary>
    public class OptionSerializer : Serializer<Option>
    {
        /// <summary>
        /// Deserialize option values from the data record.
        /// </summary>
        /// <param name="record">Data record.</param>
        /// <returns>Option entity.</returns>
        public override Option Deserialize(IDataRecord record)
        {
            return new Option
            {
                AdCode          = record.GetString(record.GetOrdinal("AdCode")),
                AdjustmentSign  = (Model.OptionAdjustmentSign)record.GetByte(record.GetOrdinal("OptionAdjustmentSignID")),
                Amount          = DataRecord.GetNullableInt32(record, "Amount"),
                CarHistoryId    = record.GetInt32(record.GetOrdinal("CarHistoryID")),
                Description     = record.GetString(record.GetOrdinal("Description")),
                Flag            = record.GetBoolean(record.GetOrdinal("Flag")),
                ResidualValueId = record.GetInt32(record.GetOrdinal("ResidualValueID"))
            };
        }
    }
}
