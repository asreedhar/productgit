﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Serializers
{
    /// <summary>
    /// Serializer for mileage adjustment repository entities.
    /// </summary>
    public class MileageAdjustmentSerializer : Serializer<MileageAdjustment>
    {
        /// <summary>
        /// Deserialize mileage adjusement values from the data record.
        /// </summary>
        /// <param name="record">Data record.</param>
        /// <returns>Mileage adjustment entity.</returns>
        public override MileageAdjustment Deserialize(IDataRecord record)
        {            
            return new MileageAdjustment
            {
                BeginRange          = record.GetInt32(record.GetOrdinal("BeginRange")),
                CarHistoryId        = record.GetInt32(record.GetOrdinal("CarHistoryID")),
                EndRange            = record.GetInt32(record.GetOrdinal("EndRange")),                
                FinanceAdvance      = DataRecord.GetNullableInt32(record, "FinanceAdvance"),
                MileageAdjustmentId = record.GetInt32(record.GetOrdinal("MileageAdjustmentID")),
                ValueId             = record.GetInt32(record.GetOrdinal("ValueID"))
            };
        }
    }
}
