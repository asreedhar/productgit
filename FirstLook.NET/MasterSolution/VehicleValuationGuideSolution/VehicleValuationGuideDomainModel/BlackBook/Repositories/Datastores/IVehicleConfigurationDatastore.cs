﻿using System;
using System.Data;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Datastores
{
    /// <summary>
    /// Interface for vehicle configuration datastores.
    /// </summary>
    public interface IVehicleConfigurationDatastore
    {
        /// <summary>
        /// Fetch the identifying details of the publications of vehicle configurations for the client and vehicle
        /// with the given identifiers.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="clientVehicleId">Client-vehicle association identifier.</param>
        /// <returns>Data reader with a list of publications.</returns>
        IDataReader VehicleConfigurations_Fetch(int clientId, int clientVehicleId);
        
        /// <summary>
        /// Fetch the identifier of the vehicle configuration for a client and vehicle that is valid now if one exists.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="clientVehicleId">Client-vehicle association identifier.</param>
        /// <returns>Data reader with vehicle configuration identifier.</returns>
        IDataReader VehicleConfiguration_Fetch(int clientId, int clientVehicleId);

        /// <summary>
        /// Insert a vehicle configuration.
        /// </summary>
        /// <param name="vehicleId">Vehicle identifier. Optional.</param>        
        /// <returns>Data reader with new identifier.</returns>
        IDataReader VehicleConfiguration_Insert(int? vehicleId);
        
        /// <summary>
        /// Fetch the vehicle configuration history record for the car defined by the given values.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="stateCode">Two letter US state identifier.</param>
        /// <param name="mileage">Mileage.</param>        
        /// <returns>Data reader with vehicle configuration history values.</returns>
        IDataReader VehicleConfiguration_History_Fetch(string uvc, string vin, string stateCode, int? mileage);
        
        /// <summary>
        /// Fetch the vehicle configuration history record for the given identifiers..
        /// </summary>        
        /// <param name="clientId">Client identifier.</param>
        /// <param name="clientVehicleId">Client-vehicle association identifier.</param>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>        
        /// <returns>Data read with vehicle configuration history values.</returns>
        IDataReader VehicleConfiguration_History_Fetch(int clientId, int clientVehicleId, int vehicleConfigurationHistoryId);       

        /// <summary>
        /// Fetch the vehicle configuration history record for the given client and vehicle that was valid on the given
        /// date.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="clientVehicleId">Client-vehicle association identifier.</param>
        /// <param name="on">Date on which the configuration should be valid.</param>
        /// <returns>Date reader with vehicle configuration history values.</returns>
        IDataReader VehicleConfiguration_History_Fetch(int clientId, int clientVehicleId, DateTime on);
        
        /// <summary>
        /// Insert a row into the vehicle configuration history table.
        /// </summary>
        /// <param name="entity">Vehicle configuration entity with values to save.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>Data reader with history values.</returns>
        IDataReader VehicleConfiguration_History_Insert(VehicleConfigurationEntity entity, int dataLoadId);
        
        /// <summary>
        /// Fetch the option actions tied to an historical record of a vehicle configuration.
        /// </summary>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <returns>Data reader with option actions.</returns>
        IDataReader OptionActions_Fetch(int vehicleConfigurationHistoryId);        

        /// <summary>
        /// Insert an option action.
        /// </summary>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>        
        /// <param name="action">Option action to save.</param>
        /// <param name="index">Index of the action.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        void OptionAction_Insert(int vehicleConfigurationHistoryId, IOptionAction action, int index, int dataLoadId);
        
        /// <summary>
        /// Fetch the option states tied to an historical record of a vehicle configuration.
        /// </summary>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <returns>Data reader with option states.</returns>
        IDataReader OptionStates_Fetch(int vehicleConfigurationHistoryId);

        /// <summary>
        /// Insert an option state.
        /// </summary>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <param name="state">Option state to save.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        void OptionState_Insert(int vehicleConfigurationHistoryId, IOptionState state, int dataLoadId);
    }
}
