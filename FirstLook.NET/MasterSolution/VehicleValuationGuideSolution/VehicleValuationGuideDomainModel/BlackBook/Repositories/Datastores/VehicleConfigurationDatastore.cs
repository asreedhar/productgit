﻿using System;
using System.Data;
using System.Reflection;
using FirstLook.Common.Core.Data;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities;
using FirstLook.VehicleValuationGuide.DomainModel.Utility.Model;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Datastores
{
    /// <summary>
    /// Vehicle configuration datastore.
    /// </summary>
    public class VehicleConfigurationDatastore : SessionDataStore, IVehicleConfigurationDatastore
    {
        #region Properties

        /// <summary>
        /// Get the current assembly.
        /// </summary>
        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        /// <summary>
        /// Prefix for resources.
        /// </summary>
        const string Prefix = "FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Resources";

        #endregion        

        #region Vehicle Configuration

        /// <summary>
        /// Fetch the identifying details of the publications of vehicle configurations for the client and vehicle
        /// with the given identifiers.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="clientVehicleId">Client-vehicle association identifier.</param>
        /// <returns>Data reader with a list of publications.</returns>
        public IDataReader VehicleConfigurations_Fetch(int clientId, int clientVehicleId)
        {
            const string queryName = Prefix + ".VehicleConfigurationDatastore_Configurations_Fetch.txt";

            return Query(new[] { "Publications" },
                         queryName,
                         new Parameter("ClientID", clientId, DbType.Int32),
                         new Parameter("VehicleID", clientVehicleId, DbType.Int32));
        }        

        /// <summary>
        /// Fetch the identifier of the vehicle configuration for a client and vehicle that is valid now if one exists.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="clientVehicleId">Client-vehicle association identifier.</param>
        /// <returns>Data reader with vehicle configuration identifier.</returns>
        public IDataReader VehicleConfiguration_Fetch(int clientId, int clientVehicleId)
        {
            const string queryName = Prefix + ".VehicleConfigurationDatastore_Configuration_Fetch.txt";

            return Query(new[] { "VehicleConfiguration" },
                         queryName,
                         new Parameter("ClientID",  clientId,        DbType.Int32),
                         new Parameter("VehicleID", clientVehicleId, DbType.Int32));
        }

        /// <summary>
        /// Insert a vehicle configuration.
        /// </summary>
        /// <param name="vehicleId">Vehicle identifier. Optional.</param>        
        /// <returns>Data reader with new identifier.</returns>
        public IDataReader VehicleConfiguration_Insert(int? vehicleId)
        {
            const string queryNameI = Prefix + ".VehicleConfigurationDatastore_Configuration_Insert.txt";

            const string queryNameF = Prefix + ".VehicleConfigurationDatastore_Configuration_Fetch_Identity.txt";

            object vehicleIdObj = vehicleId.HasValue ? (object)vehicleId.Value : DBNull.Value;

            NonQuery(queryNameI,
                     new Parameter("VehicleID",  vehicleIdObj, DbType.Int32));

            return Query(new[] { "VehicleConfiguration" },
                         queryNameF);
        }

        #endregion

        #region Vehicle Configuration History

        /// <summary>
        /// Fetch the vehicle configuration history record for the car defined by the given values.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="stateCode">Two letter US state identifier.</param>
        /// <param name="mileage">Mileage.</param>
        /// <returns>Data reader with vehicle configuration history values.</returns>
        public IDataReader VehicleConfiguration_History_Fetch(string uvc, string vin, string stateCode, int? mileage)
        {
            const string queryName = Prefix + ".VehicleConfigurationDatastore_Configuration_History_Fetch_Values.txt";

            object mileageObj = mileage.HasValue ? (object)mileage.Value : DBNull.Value;
            object vinObj = !string.IsNullOrEmpty(vin) ? (object)Vin.Squish(vin) : DBNull.Value;

            return Query(new[] { "VehicleConfiguration" },
                         queryName,
                         new Parameter("Uvc",     uvc,        DbType.String),
                         new Parameter("Vin",     vinObj,     DbType.String),
                         new Parameter("Code",    stateCode,  DbType.String),
                         new Parameter("Mileage", mileageObj, DbType.Int32));
        }

        /// <summary>
        /// Fetch the vehicle configuration history record for the given identifiers..
        /// </summary>        
        /// <param name="clientId">Client identifier.</param>
        /// <param name="clientVehicleId">Client-vehicle association identifier.</param>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>        
        /// <returns>Data read with vehicle configuration history values.</returns>
        public IDataReader VehicleConfiguration_History_Fetch(int clientId, int clientVehicleId, int vehicleConfigurationHistoryId)
        {
            const string queryName = Prefix + ".VehicleConfigurationDatastore_Configuration_History_Fetch_Id.txt";

            return Query(new[] { "VehicleConfiguration" },
                         queryName,
                         new Parameter("ClientID",                      clientId,                      DbType.Int32),
                         new Parameter("VehicleID",                     clientVehicleId,               DbType.Int32),
                         new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32));
        }

        /// <summary>
        /// Fetch the vehicle configuration history record for the given client and vehicle that was valid on the given
        /// date.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="clientVehicleId">Client-vehicle association identifier.</param>
        /// <param name="on">Date on which the configuration should be valid.</param>
        /// <returns>Date reader with vehicle configuration history values.</returns>
        public IDataReader VehicleConfiguration_History_Fetch(int clientId, int clientVehicleId, DateTime on)
        {
            const string queryName = Prefix + ".VehicleConfigurationDatastore_Configuration_History_Fetch_On.txt";

            return Query(new[] { "VehicleConfiguration" },
                         queryName,
                         new Parameter("ClientID",  clientId,        DbType.Int32),
                         new Parameter("VehicleID", clientVehicleId, DbType.Int32),
                         new Parameter("On",        on,              DbType.DateTime));
        }        

        /// <summary>
        /// Insert a row into the vehicle configuration history table.
        /// </summary>
        /// <param name="entity">Vehicle configuration entity with values to save.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>Data reader with history values.</returns>
        public IDataReader VehicleConfiguration_History_Insert(VehicleConfigurationEntity entity, int dataLoadId)
        {
            const string queryNameI = Prefix + ".VehicleConfigurationDatastore_Configuration_History_Insert.txt";

            const string queryNameF = Prefix + ".VehicleConfigurationDatastore_Configuration_History_Fetch_Identity.txt";

            NonQuery(queryNameI,
                     new Parameter("VehicleConfigurationID", entity.VehicleConfigurationId,       DbType.Int32),
                     new Parameter("CarHistoryID",           entity.CarHistoryId,                 DbType.Int32),
                     new Parameter("DataLoadID",             dataLoadId,                          DbType.Int32),
                     new Parameter("Mileage",                entity.VehicleConfiguration.Mileage, DbType.Int32),
                     new Parameter("AuditRowID",             entity.AuditRow.Id,                  DbType.Int32),
                     new Parameter("ChangeAgentID",          (int)entity.ChangeAgent,             DbType.Byte),
                     new Parameter("ChangeTypeBitFlags",     (int)entity.ChangeType,              DbType.Int32));

            return Query(new[] { "VehicleConfiguration" },
                         queryNameF);
        }

        #endregion

        #region Options

        /// <summary>
        /// Fetch the option actions tied to an historical record of a vehicle configuration.
        /// </summary>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <returns>Data reader with option actions.</returns>
        public IDataReader OptionActions_Fetch(int vehicleConfigurationHistoryId)
        {
            const string queryName = Prefix + ".VehicleConfigurationDatastore_OptionAction_Fetch.txt";

            return Query(new[] { "Count", "VehicleConfiguration" },
                         queryName,
                         new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32));
        }

        /// <summary>
        /// Insert an option action.
        /// </summary>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>        
        /// <param name="action">Option action to save.</param>
        /// <param name="index">Index of the action.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        public void OptionAction_Insert(int vehicleConfigurationHistoryId, IOptionAction action, int index, int dataLoadId)
        {
            const string queryName = Prefix + ".VehicleConfigurationDatastore_OptionAction_Insert.txt";

            NonQuery(queryName,
                     new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32),
                     new Parameter("DataLoadID",                    dataLoadId,                    DbType.Int32),
                     new Parameter("OptionActionTypeID",            (int)action.ActionType,        DbType.Int32),
                     new Parameter("OptionActionIndex",             index,                         DbType.Int32),
                     new Parameter("Uoc",                           action.Uoc,                    DbType.String));
        }

        /// <summary>
        /// Fetch the option states tied to an historical record of a vehicle configuration.
        /// </summary>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <returns>Data reader with option states.</returns>
        public IDataReader OptionStates_Fetch(int vehicleConfigurationHistoryId)
        {
            const string queryName = Prefix + ".VehicleConfigurationDatastore_OptionState_Fetch.txt";

            return Query(new[] { "VehicleConfiguration" },
                         queryName,
                         new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32));
        }

        /// <summary>
        /// Insert an option state.
        /// </summary>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <param name="state">Option state to save.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        public void OptionState_Insert(int vehicleConfigurationHistoryId, IOptionState state, int dataLoadId)
        {
            const string queryName = Prefix + ".VehicleConfigurationDatastore_OptionState_Insert.txt";

            NonQuery(queryName,
                     new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32),
                     new Parameter("DataLoadID",                    dataLoadId,                    DbType.Int32),
                     new Parameter("Uoc",                           state.Uoc,                     DbType.String),
                     new Parameter("Enabled",                       state.Enabled,                 DbType.Boolean),
                     new Parameter("Selected",                      state.Selected,                DbType.Boolean));
        }

        #endregion
    }
}
