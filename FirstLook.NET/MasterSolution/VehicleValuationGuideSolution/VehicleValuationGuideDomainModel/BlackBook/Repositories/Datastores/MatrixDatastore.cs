﻿using System;
using System.Data;
using System.Reflection;
using FirstLook.Common.Core.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Datastores
{
    /// <summary>
    /// Price matrix datastore.
    /// </summary>
    public class MatrixDatastore : SessionDataStore, IMatrixDatastore
    {
        #region Properties

        /// <summary>
        /// Get the current assembly.
        /// </summary>
        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        /// <summary>
        /// Prefix for resources.
        /// </summary>
        const string Prefix = "FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Resources";

        #endregion

        /// <summary>
        /// Fetch the price matrix for the car and configuration with the given identifiers.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <returns>Data reader with matrix values.</returns>
        public IDataReader Matrix_Fetch(int carHistoryId, int vehicleConfigurationHistoryId)
        {
            const string queryName = Prefix + ".MatrixDatastore_Matrix_Fetch_Id.txt";

            return Query(new[] { "Matrix" },
                         queryName,
                         new Parameter("CarHistoryID",                  carHistoryId,                  DbType.Int32),
                         new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32));   
        }

        /// <summary>
        /// Insert into the price matrix table.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>Data reader with the matrix identifier.</returns>
        public IDataReader Matrix_Insert(int carHistoryId, int vehicleConfigurationHistoryId, int dataLoadId)
        {
            const string queryNameI = Prefix + ".MatrixDatastore_Matrix_Insert.txt";

            const string queryNameF = Prefix + ".MatrixDatastore_Matrix_Fetch_Identity.txt";

            NonQuery(queryNameI,
                     new Parameter("CarHistoryID",                  carHistoryId,                  DbType.Int32),
                     new Parameter("VehicleConfigurationHistoryID", vehicleConfigurationHistoryId, DbType.Int32),
                     new Parameter("DataLoadID",                    dataLoadId,                    DbType.Int32));

            return Query(new[] { "Matrix" },
                         queryNameF);
        }

        /// <summary>
        /// Insert a price matrix cell.
        /// </summary>
        /// <param name="matrixId">Matrix identifier.</param>
        /// <param name="marketId">Market identifier.</param>
        /// <param name="conditionId">Condition identifier.</param>
        /// <param name="valuationId">Valuation identifier.</param>
        /// <param name="visible">Visible?</param>
        /// <param name="value">Value.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        public void MatrixCell_Insert(int matrixId, int marketId, int conditionId, int valuationId, bool visible, int? value, int dataLoadId)
        {
            const string queryName = Prefix + ".MatrixDatastore_MatrixCell_Insert.txt";

            object valueObj = value.HasValue ? (object)value.Value : DBNull.Value;

            NonQuery(queryName,
                     new Parameter("MatrixID",    matrixId,    DbType.Int32),
                     new Parameter("DataLoadID",  dataLoadId,  DbType.Int32),
                     new Parameter("MarketID",    marketId,    DbType.Int16),
                     new Parameter("ConditionID", conditionId, DbType.Int16),
                     new Parameter("ValuationID", valuationId, DbType.Int16),
                     new Parameter("Visible",     visible,     DbType.Boolean),
                     new Parameter("Value",       valueObj,    DbType.Int32));
        }
    }
}
