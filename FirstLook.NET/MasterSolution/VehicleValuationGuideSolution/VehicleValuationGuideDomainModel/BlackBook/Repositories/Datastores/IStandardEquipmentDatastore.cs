﻿using System.Data;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Datastores
{
    /// <summary>
    /// Interface for a standard equipment datastore.
    /// </summary>
    public interface IStandardEquipmentDatastore
    {        
        /// <summary>
        /// Check if the cache for the standard equipment for the car with the given uvc has expired.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <returns>Data reader with the boolean result of if the cache has expired.</returns>
        IDataReader CachePolicy_IsValid(string uvc);

        /// <summary>
        /// Push out the window of the cache policy for the standard equipment with the given identifier. The length 
        /// of this window is defined by the values in Vehicle.BlackBook.CachePolicy.
        /// </summary>
        /// <param name="standardEquipmentId">Standard equipment identifier.</param>
        void CachePolicy_Upsert(int standardEquipmentId);

        /// <summary>
        /// Fetch the standard equipment for the vehicle with the given uvc.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <returns>Data reader with the standard equipment identifier.</returns>
        IDataReader StandardEquipment_Fetch(string uvc);

        /// <summary>
        /// Insert standard equipment.
        /// </summary>
        /// <param name="uvc">Uvc.</param>        
        /// <returns>Data reader with standard equipment identifier.</returns>
        IDataReader StandardEquipment_Insert(string uvc);

        /// <summary>
        /// Fetch the most recent valid history entry for the standard equipment with the given identifier.
        /// </summary>
        /// <param name="standardEquipmentId">Standard equipment identifier.</param>        
        /// <returns>Data reader with the historical entry.</returns>
        IDataReader StandardEquipment_History_Fetch(int standardEquipmentId);

        /// <summary>
        /// Fetch the most recent history entry for the standard equipment for the vehicle with the given uvc.
        /// </summary>        
        /// <param name="uvc">Uvc.</param>        
        /// <returns>Data reader with the historical entry.</returns>
        IDataReader StandardEquipment_History_Fetch(string uvc);

        /// <summary>
        /// Insert an historical record of standard equipment.
        /// </summary>
        /// <param name="entity">Standard equipment values to insert.</param>       
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>Data reader with the historical entry identifier.</returns>
        IDataReader StandardEquipment_History_Insert(StandardEquipmentEntity entity, int dataLoadId);

        /// <summary>
        /// Get all standard equipment tied to the given history identifier.
        /// </summary>
        /// <param name="standardEquipmentHistoryId">Standard equipment history identifier.</param>
        /// <returns>Data reader with all categories, subcategories and values tied to the history entry.</returns>
        IDataReader StandardEquipmentRecords_Fetch(int standardEquipmentHistoryId);

        /// <summary>
        /// Insert a standard equipment record.
        /// </summary>
        /// <param name="historyId">Standard equipment history identifier.</param>
        /// <param name="categoryId">Category identifier.</param>
        /// <param name="subCategoryId">Subcategory identifier.</param>
        /// <param name="value">Value.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        void StandardEquipmentRecord_Insert(int historyId, int categoryId, int subCategoryId, string value, int dataLoadId);

        /// <summary>
        /// Fetch the standard equipment category with the given name.
        /// </summary>
        /// <param name="name">Category name.</param>
        /// <returns>Data reader with the category identifier.</returns>
        IDataReader Category_Fetch(string name);

        /// <summary>
        /// Insert an equipment category.
        /// </summary>
        /// <param name="name">Category name.</param>
        /// <returns>Data reader.</returns>
        IDataReader Category_Insert(string name);

        /// <summary>
        /// Fetch the standard equipment subcategory with the given name that is under the given category.
        /// </summary>
        /// <param name="categoryId">Category identifier.</param>
        /// <param name="name">Subcategory name.</param>
        /// <returns>Data reader with the subcategory identifier.</returns>
        IDataReader SubCategory_Fetch(int categoryId, string name);

        /// <summary>
        /// Insert an equipment subcategory under the given category.
        /// </summary>
        /// <param name="categoryId">Category identifier.</param>
        /// <param name="name">Subcategory name.</param>
        /// <returns>Data reader.</returns>
        IDataReader SubCategory_Insert(int categoryId, string name);
    }
}
