﻿using System.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Datastores
{
    /// <summary>
    /// Interface for a client datastore within the context of BlackBook operations.
    /// </summary>
    public interface IClientDatastore
    {
        /// <summary>
        /// Get the vehicle with BlackBook configurations that has the given identifier.
        /// </summary>
        /// <param name="vehicleId">Vehicle identifier.</param>
        /// <returns>Data reader with details on the vehicle.</returns>
        IDataReader Vehicle_BlackBook_Fetch(int vehicleId);

        /// <summary>
        /// Insert an association between the vehicle and configuration with the given identifiers.
        /// </summary>
        /// <param name="vehicleId">Vehicle identifier.</param>
        /// <param name="vehicleConfigurationId">Vehicle configuration identifier.</param>        
        void Vehicle_BlackBook_Insert(int vehicleId, int vehicleConfigurationId);
    }
}
