﻿using System.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Datastores
{
    /// <summary>
    /// Interface for a price matrix datastore.
    /// </summary>
    public interface IMatrixDatastore
    {
        /// <summary>
        /// Fetch the price matrix for the car and configuration with the given identifiers.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <returns>Data reader with matrix values.</returns>
        IDataReader Matrix_Fetch(int carHistoryId, int vehicleConfigurationHistoryId);

        /// <summary>
        /// Insert into the price matrix table.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>Data reader with the matrix identifier.</returns>
        IDataReader Matrix_Insert(int carHistoryId, int vehicleConfigurationHistoryId, int dataLoadId);

        /// <summary>
        /// Insert a price matrix cell.
        /// </summary>
        /// <param name="matrixId">Matrix identifier.</param>
        /// <param name="marketId">Market identifier.</param>
        /// <param name="conditionId">Condition identifier.</param>
        /// <param name="valuationId">Valuation identifier.</param>
        /// <param name="visible">Visible?</param>
        /// <param name="value">Value.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        void MatrixCell_Insert(int matrixId, int marketId, int conditionId, int valuationId, bool visible, int? value, int dataLoadId);
    }
}
