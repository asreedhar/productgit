﻿using System;
using System.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Datastores
{
    /// <summary>
    /// Interface for a car datastore.
    /// </summary>
    public interface ICarDatastore
    {
        /// <summary>
        /// Fetch car attributes.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <returns>Data reader with attribute values.</returns>
        IDataReader Car_Attributes_Fetch(int carHistoryId);

        /// <summary>
        /// Insert car attributes into the database.
        /// </summary>        
        /// <param name="attributes">Car attributes.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        void Car_Attributes_Insert(Entities.Attributes attributes, int dataLoadId);

        /// <summary>
        /// Check if the cache for the car with the given identifiers has expired.        
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="stateCode">Two letter US state identifier.</param>
        /// <returns>Data reader with the boolean result of if the cache has expired.</returns>
        IDataReader Car_CachePolicy_IsValid(string uvc, string vin, string stateCode);     
        
        /// <summary>
        /// Check if the cache for any car with the given uvc has expired.        
        /// </summary>
        /// <param name="uvc">Uvc.</param>        
        /// <param name="stateCode">Two letter US state identifier.</param>
        /// <returns>Data reader with the boolean result of if the cache has expired.</returns>
        IDataReader Car_CachePolicy_IsValid(string uvc, string stateCode);

        /// <summary>
        /// Push out the window of the cache policy for the car with the given identifier. The length of this window
        /// is defined by the values in Vehicle.BlackBook.CachePolicy.
        /// </summary>
        /// <param name="carId">Car identifier.</param>
        void Car_CachePolicy_Upsert(int carId);

        /// <summary>
        /// Fetch a car description.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <returns>Data reader with description values.</returns>
        IDataReader Car_Description_Fetch(int carHistoryId);

        /// <summary>
        /// Insert car description values into the database.
        /// </summary>        
        /// <param name="description">Car description.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        void Car_Description_Insert(Entities.Description description, int dataLoadId);

        /// <summary>
        /// Fetch an historical record of a car, if one exists. Does not fetch all of the car's 
        /// values, just the ones pertaining to the Car_History table.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="stateCode">Two letter identifier of the US state.</param>
        /// <param name="date">Date.</param>
        /// <returns>Data reader with car history details.</returns>
        IDataReader Car_History_Fetch(string uvc, string vin, string stateCode, DateTime date);

        /// <summary>
        /// Fetch the historical record of a car with the given identifier. Does not fetch all of the car's values, 
        /// just the ones pertaining to the Car_History table.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>        
        /// <returns>Data reader with car history details.</returns>
        IDataReader Car_History_Fetch(int carHistoryId);

        /// <summary>
        /// Insert a row into the car history table.
        /// </summary>
        /// <param name="entity">Car entity.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>Data reader.</returns>
        IDataReader Car_History_Insert(Entities.CarEntity entity, int dataLoadId);
       
        /// <summary>
        /// Fetch the details of the car with the given values.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="stateCode">Two letter US state identifier.</param>
        /// <returns>Data reader with the car identifier.</returns>
        IDataReader Car_Fetch(string uvc, string vin, string stateCode);

        /// <summary>
        /// Insert a car with the given uvc and vin into the database.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="stateCode">Two letter identifier of the US state.</param>
        /// <returns>Data reader.</returns>
        IDataReader Car_Insert(string uvc, string vin, string stateCode);

        /// <summary>
        /// Fetch a car's prices.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <returns>Data reader with price data.</returns>
        IDataReader Car_Prices_Fetch(int carHistoryId);

        /// <summary>
        /// Insert car prices.
        /// </summary>        
        /// <param name="prices">Prices.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        void Car_Prices_Insert(Entities.Prices prices, int dataLoadId);

        /// <summary>
        /// Fetch a car's colors.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <returns>Data reader with colors.</returns>
        IDataReader Color_Fetch(int carHistoryId);

        /// <summary>
        /// Insert a color.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>        
        /// <param name="category">Color category.</param>
        /// <param name="description">Color description.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>Data reader.</returns>
        IDataReader Color_Insert(int carHistoryId, string category, string description, int dataLoadId);

        /// <summary>
        /// Fetch a car's mileage adjustments.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <returns>Data reader with mileage adjustments.</returns>
        IDataReader MileageAdjustment_Fetch(int carHistoryId);

        /// <summary>
        /// Insert a mileage adjustment.
        /// </summary>        
        /// <param name="adjustment">Mileage adjustment values.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        void MileageAdjustment_Insert(Entities.MileageAdjustment adjustment, int dataLoadId);

        /// <summary>
        /// Fetch a car's options.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <returns>Data reader with options.</returns>
        IDataReader Options_Fetch(int carHistoryId);

        /// <summary>
        /// Insert an option adjustment.
        /// </summary>        
        /// <param name="option">Option adjustment.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        void Options_Insert(Entities.Option option, int dataLoadId);

        /// <summary>
        /// Fetch a car's residual value.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <param name="residualValueId">Residual value identifier.</param>
        /// <returns>Data reader with residual value values.</returns>
        IDataReader ResidualValue_Fetch(int carHistoryId, int residualValueId);

        /// <summary>
        /// Insert a car's residual values.
        /// </summary>        
        /// <param name="value">Residual values.</param>
        /// <returns>Data reader.</returns>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        IDataReader ResidualValue_Insert(Entities.ResidualValue value, int dataLoadId);

        /// <summary>
        /// Fetch color swatches tied to a color.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <param name="colorId">Color identifier.</param>
        IDataReader Swatch_Fetch(int carHistoryId, int colorId);

        /// <summary>
        /// Insert a color swatch.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <param name="colorId">Color identifier.</param>
        /// <param name="htmlIndex">Html index.</param>
        /// <param name="htmlColor">Html color.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        void Swatch_Insert(int carHistoryId, int colorId, int htmlIndex, string htmlColor, int dataLoadId);

        /// <summary>
        /// Fetch uvcs tied to the given vin.
        /// </summary>
        /// <param name="vin">Vin.</param>
        /// <returns>Data reader with uvcs.</returns>
        IDataReader Uvc_Fetch(string vin);

        /// <summary>
        /// Fetch a car's value.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <param name="valueId">Value identifier.</param>
        /// <returns>Data reader with value values.</returns>
        IDataReader Value_Fetch(int carHistoryId, int valueId);

        /// <summary>
        /// Insert values.
        /// </summary>        
        /// <param name="values">Values to insert.</param>
        /// <returns>Data reader.</returns>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        IDataReader Value_Insert(Entities.Value values, int dataLoadId);

        /// <summary>
        /// Fetch the vins tied to the given uvc.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <returns>Data reader with vins.</returns>
        IDataReader Vin_Fetch(string uvc);        
    }
}
