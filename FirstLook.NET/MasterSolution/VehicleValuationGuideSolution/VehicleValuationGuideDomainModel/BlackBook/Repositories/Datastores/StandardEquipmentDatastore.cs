﻿using System.Data;
using System.Reflection;
using FirstLook.Common.Core.Data;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Datastores
{
    /// <summary>
    /// Standard equipment datastore.
    /// </summary>
    public class StandardEquipmentDatastore : SessionDataStore, IStandardEquipmentDatastore
    {
        #region Properties

        /// <summary>
        /// Get the current assembly.
        /// </summary>
        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        /// <summary>
        /// Prefix for resources.
        /// </summary>
        const string Prefix = "FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Resources";

        #endregion       

        /// <summary>
        /// Check if the cache for the standard equipment for the car with the given uvc has expired.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <returns>Data reader with the boolean result of if the cache has expired.</returns>
        public IDataReader CachePolicy_IsValid(string uvc)
        {
            const string queryName = Prefix + ".StandardEquipmentDatastore_CachePolicy_IsValid.txt";

            return Query(new[] { "CachePolicy" },
                         queryName,
                         new Parameter("Uvc", uvc, DbType.String));            
        }        

        /// <summary>
        /// Push out the window of the cache policy for the standard equipment with the given identifier. The length 
        /// of this window is defined by the values in Vehicle.BlackBook.CachePolicy.
        /// </summary>
        /// <param name="standardEquipmentId">Standard equipment identifier.</param>
        public void CachePolicy_Upsert(int standardEquipmentId)
        {
            const string queryName = Prefix + ".StandardEquipmentDatastore_CachePolicy_Upsert.txt";

            NonQuery(queryName,
                     new Parameter("StandardEquipmentID", standardEquipmentId, DbType.Int32));
        }

        /// <summary>
        /// Fetch the standard equipment for the vehicle with the given uvc.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <returns>Data reader with the standard equipment identifier.</returns>
        public IDataReader StandardEquipment_Fetch(string uvc)
        {
            const string queryName = Prefix + ".StandardEquipmentDatastore_Equipment_Fetch_Uvc.txt";

            return Query(new[] { "StandardEquipment" },
                         queryName,
                         new Parameter("Uvc", uvc, DbType.String));
        }

        /// <summary>
        /// Insert standard equipment.
        /// </summary>
        /// <param name="uvc">Uvc.</param>        
        /// <returns>Data reader with the standard equipment identifier.</returns>
        public IDataReader StandardEquipment_Insert(string uvc)
        {
            const string queryNameI = Prefix + ".StandardEquipmentDatastore_Equipment_Insert.txt";

            const string queryNameF = Prefix + ".StandardEquipmentDatastore_Equipment_Fetch_Identity.txt";

            NonQuery(queryNameI,
                     new Parameter("Uvc", uvc, DbType.String));

            return Query(new[] { "StandardEquipment" },
                         queryNameF);
        }

        /// <summary>
        /// Fetch the most recent valid history entry for the standard equipment with the given identifier.
        /// </summary>
        /// <param name="standardEquipmentId">Standard equipment identifier.</param>        
        /// <returns>Data reader with the historical entry.</returns>
        public IDataReader StandardEquipment_History_Fetch(int standardEquipmentId)
        {
            const string queryName = Prefix + ".StandardEquipmentDatastore_History_Fetch_Id.txt";

            return Query(new[] { "StandardEquipment" },
                         queryName,
                         new Parameter("StandardEquipmentID", standardEquipmentId, DbType.Int32));
        }

        /// <summary>
        /// Fetch the most recent history entry for the standard equipment for the vehicle with the given uvc.
        /// </summary>        
        /// <param name="uvc">Uvc.</param>
        /// <returns>Data reader with the historical entry.</returns>
        public IDataReader StandardEquipment_History_Fetch(string uvc)
        {
            const string queryName = Prefix + ".StandardEquipmentDatastore_History_Fetch_Uvc.txt";

            return Query(new[] { "StandardEquipment" },
                         queryName,
                         new Parameter("Uvc", uvc, DbType.String));
        }

        /// <summary>
        /// Insert an historical record of standard equipment.
        /// </summary>
        /// <param name="entity">Standard equipment values to insert.</param>       
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>Data reader with the historical entry identifier.</returns>
        public IDataReader StandardEquipment_History_Insert(StandardEquipmentEntity entity, int dataLoadId)
        {
            const string queryNameI = Prefix + ".StandardEquipmentDatastore_History_Insert.txt";

            const string queryNameF = Prefix + ".StandardEquipmentDatastore_History_Fetch_Identity.txt";

            NonQuery(queryNameI,
                     new Parameter("StandardEquipmentID",       entity.Id,          DbType.Int32),
                     new Parameter("DataLoadID",                dataLoadId,         DbType.Int32),
                     new Parameter("StandardEquipmentHashCode", entity.HashCode,    DbType.Int64),
                     new Parameter("AuditRowID",                entity.AuditRow.Id, DbType.Int32));

            return Query(new[] { "StandardEquipment" },
                         queryNameF);
        }

        /// <summary>
        /// Get all standard equipment records tied to the given history identifier.
        /// </summary>
        /// <param name="standardEquipmentHistoryId">Standard equipment history identifier.</param>
        /// <returns>Data reader with all categories, subcategories and values tied to the history entry.</returns>
        public IDataReader StandardEquipmentRecords_Fetch(int standardEquipmentHistoryId)
        {
            const string queryName = Prefix + ".StandardEquipmentDatastore_Record_Fetch_Id.txt";

            return Query(new[] { "StandardEquipment" },
                         queryName,
                         new Parameter("StandardEquipmentHistoryID", standardEquipmentHistoryId, DbType.Int32));            
        }

        /// <summary>
        /// Insert a standard equipment record.
        /// </summary>
        /// <param name="historyId">Standard equipment history identifier.</param>
        /// <param name="categoryId">Category identifier.</param>
        /// <param name="subCategoryId">Subcategory identifier.</param>
        /// <param name="value">Value.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        public void StandardEquipmentRecord_Insert(int historyId, int categoryId, int subCategoryId, string value, int dataLoadId)
        {
            const string queryName = Prefix + ".StandardEquipmentDatastore_Record_Insert.txt";

            NonQuery(queryName,
                     new Parameter("StandardEquipmentHistoryID", historyId,     DbType.Int32),
                     new Parameter("DataLoadID",                 dataLoadId,    DbType.Int32),
                     new Parameter("CategoryID",                 categoryId,    DbType.Int32),
                     new Parameter("SubCategoryID",              subCategoryId, DbType.Int32),
                     new Parameter("Value",                      value,         DbType.String));
        }

        /// <summary>
        /// Fetch the standard equipment category with the given name.
        /// </summary>
        /// <param name="name">Category name.</param>
        /// <returns>Data reader with the category identifier.</returns>
        public IDataReader Category_Fetch(string name)
        {
            const string queryName = Prefix + ".StandardEquipmentDatastore_Category_Fetch_Name.txt";

            return Query(new[] { "StandardEquipment" },
                         queryName,
                         new Parameter("Name", name, DbType.String));    
        }

        /// <summary>
        /// Insert an equipment category.
        /// </summary>
        /// <param name="name">Category name.</param>
        /// <returns>Data reader.</returns>
        public IDataReader Category_Insert(string name)
        {
            const string queryNameI = Prefix + ".StandardEquipmentDatastore_Category_Insert.txt";

            const string queryNameF = Prefix + ".StandardEquipmentDatastore_Category_Fetch_Identity.txt";

            NonQuery(queryNameI,
                     new Parameter("Name", name, DbType.String));

            return Query(new[] { "Category" },
                         queryNameF);
        }

        /// <summary>
        /// Fetch the standard equipment subcategory with the given name that is under the given category.
        /// </summary>
        /// <param name="categoryId">Category identifier.</param>
        /// <param name="name">Subcategory name.</param>
        /// <returns>Data reader with the subcategory identifier.</returns>
        public IDataReader SubCategory_Fetch(int categoryId, string name)
        {
            const string queryName = Prefix + ".StandardEquipmentDatastore_SubCategory_Fetch_Name.txt";

            return Query(new[] { "StandardEquipment" },
                         queryName,
                         new Parameter("CategoryID", categoryId, DbType.Int32),
                         new Parameter("Name",       name,       DbType.String));    
        }

        /// <summary>
        /// Insert an equipment subcategory under the given category.
        /// </summary>
        /// <param name="categoryId">Category identifier.</param>
        /// <param name="name">Subcategory name.</param>
        /// <returns>Data reader.</returns>
        public IDataReader SubCategory_Insert(int categoryId, string name)
        {
            const string queryNameI = Prefix + ".StandardEquipmentDatastore_SubCategory_Insert.txt";

            const string queryNameF = Prefix + ".StandardEquipmentDatastore_SubCategory_Fetch_Identity.txt";

            NonQuery(queryNameI,
                     new Parameter("CategoryID", categoryId, DbType.Int32),
                     new Parameter("Name",       name,       DbType.String));

            return Query(new[] { "Category" },
                         queryNameF);
        }
    }
}