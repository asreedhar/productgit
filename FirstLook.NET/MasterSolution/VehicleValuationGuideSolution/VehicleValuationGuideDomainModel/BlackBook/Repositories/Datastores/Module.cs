﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Datastores
{
    /// <summary>
    /// Module for registering repository datastores.
    /// </summary>
    public class Module : IModule
    {
        /// <summary>
        /// Register repository datastores.
        /// </summary>
        public void Configure(IRegistry registry)
        {
            registry.Register<ICarDatastore,                  CarDatastore>(ImplementationScope.Shared);
            registry.Register<IClientDatastore,               ClientDatastore>(ImplementationScope.Shared);
            registry.Register<IMatrixDatastore,               MatrixDatastore>(ImplementationScope.Shared);            
            registry.Register<IStandardEquipmentDatastore,    StandardEquipmentDatastore>(ImplementationScope.Shared);
            registry.Register<IVehicleConfigurationDatastore, VehicleConfigurationDatastore>(ImplementationScope.Shared);
        }
    }
}
