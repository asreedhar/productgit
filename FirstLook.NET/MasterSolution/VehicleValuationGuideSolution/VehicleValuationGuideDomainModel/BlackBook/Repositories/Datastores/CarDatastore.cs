﻿using System;
using System.Data;
using System.Reflection;
using FirstLook.Common.Core.Data;
using FirstLook.VehicleValuationGuide.DomainModel.Utility.Model;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Datastores
{
    /// <summary>
    /// Car datastore.
    /// </summary>
    public class CarDatastore : SessionDataStore, ICarDatastore
    {
        #region Properties

        /// <summary>
        /// Get the current assembly.
        /// </summary>
        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        /// <summary>
        /// Prefix for resources.
        /// </summary>
        const string Prefix = "FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Resources";

        #endregion
       
        /// <summary>
        /// Fetch car attributes.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <returns>Data reader with attribute values.</returns>
        public IDataReader Car_Attributes_Fetch(int carHistoryId)
        {
            const string queryName = Prefix + ".CarDatastore_Car_Attributes_Fetch_Id.txt";

            return Query(new[] { "Attribute" },
                         queryName,
                         new Parameter("CarHistoryID", carHistoryId, DbType.Int32));
        }

        /// <summary>
        /// Insert car attributes into the database.
        /// </summary>        
        /// <param name="attributes">Car attributes.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        public void Car_Attributes_Insert(Entities.Attributes attributes, int dataLoadId)
        {
            const string queryName = Prefix + ".CarDatastore_Car_Attributes_Insert.txt";

            NonQuery(queryName,
                     new Parameter("CarHistoryID",      attributes.CarHistoryId,      DbType.Int32),
                     new Parameter("DataLoadID",        dataLoadId,                   DbType.Int32),
                     new Parameter("WheelBase",         attributes.WheelBase,         DbType.String),
                     new Parameter("TaxableHorsepower", attributes.TaxableHorsepower, DbType.String),
                     new Parameter("Weight",            attributes.Weight,            DbType.String),
                     new Parameter("TireSize",          attributes.TireSize,          DbType.String),
                     new Parameter("BaseHorsepower",    attributes.BaseHorsepower,    DbType.String),
                     new Parameter("FuelType",          attributes.FuelType,          DbType.String),
                     new Parameter("Cylinders",         attributes.Cylinders,         DbType.String),
                     new Parameter("DriveTrain",        attributes.DriveTrain,        DbType.String),                     
                     new Parameter("Transmission",      attributes.Transmission,      DbType.String),
                     new Parameter("Engine",            attributes.Engine,            DbType.String),
                     new Parameter("VehicleClass",      attributes.VehicleClass,      DbType.String));
        }

        /// <summary>
        /// Check if the cache for the car with the given uvc and vin has expired.        
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="stateCode">Two letter US state identifier.</param>
        /// <returns>Data reader with the boolean result of if the cache has expired.</returns>
        public IDataReader Car_CachePolicy_IsValid(string uvc, string vin, string stateCode)
        {
            const string queryName = Prefix + ".CarDatastore_Car_CachePolicy_IsValid.txt";

            return Query(new [] { "CachePolicy" },
                         queryName,
                         new Parameter("Uvc",  uvc,             DbType.String),
                         new Parameter("Vin",  Vin.Squish(vin), DbType.String),
                         new Parameter("Code", stateCode,       DbType.String));
        }

        /// <summary>
        /// Check if the cache for any car with the given uvc has expired.        
        /// </summary>
        /// <param name="uvc">Uvc.</param>        
        /// <param name="stateCode">Two letter US state identifier.</param>
        /// <returns>Data reader with the boolean result of if the cache has expired.</returns>
        public IDataReader Car_CachePolicy_IsValid(string uvc, string stateCode)
        {
            const string queryName = Prefix + ".CarDatastore_Car_CachePolicy_IsValid.txt";

            return Query(new[] { "CachePolicy" },
                         queryName,
                         new Parameter("Uvc",  uvc,          DbType.String),
                         new Parameter("Vin",  DBNull.Value, DbType.String),
                         new Parameter("Code", stateCode,    DbType.String));
        }

        /// <summary>
        /// Push out the window of the cache policy for the car with the given identifier. The length of this window
        /// is defined by the values in Vehicle.BlackBook.CachePolicy.
        /// </summary>
        /// <param name="carId">Car identifier.</param>
        public void Car_CachePolicy_Upsert(int carId)
        {
            const string queryName = Prefix + ".CarDatastore_Car_CachePolicy_Upsert.txt";

            NonQuery(queryName,
                     new Parameter("CarID", carId, DbType.Int32));
        }

        /// <summary>
        /// Fetch a car description.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <returns>Data reader with description values.</returns>
        public IDataReader Car_Description_Fetch(int carHistoryId)
        {
            const string queryName = Prefix + ".CarDatastore_Car_Description_Fetch_Id.txt";

            return Query(new[] { "Description" },
                         queryName,
                         new Parameter("CarHistoryID", carHistoryId, DbType.Int32));
        }

        /// <summary>
        /// Insert car description values into the database.
        /// </summary>        
        /// <param name="description">Car description.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        public void Car_Description_Insert(Entities.Description description, int dataLoadId)
        {
            const string queryName = Prefix + ".CarDatastore_Car_Description_Insert.txt";

            NonQuery(queryName,
                     new Parameter("CarHistoryID", description.CarHistoryId, DbType.Int32),
                     new Parameter("DataLoadID",   dataLoadId,               DbType.Int32),
                     new Parameter("Year",         description.Year,         DbType.Int32),
                     new Parameter("Make",         description.Make,         DbType.String),
                     new Parameter("Model",        description.Model,        DbType.String),
                     new Parameter("Series",       description.Series,       DbType.String),
                     new Parameter("BodyStyle",    description.BodyStyle,    DbType.String));
        }

        /// <summary>
        /// Fetch the most recent valid historical record of a car, if one exists. Does not fetch all of the car's 
        /// values, just the ones pertaining to the Car_History table.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin. Optional.</param>
        /// <param name="stateCode">Two letter identifier of the US state.</param>
        /// <param name="date">Date.</param>
        /// <returns>Data reader with car history details.</returns>
        public IDataReader Car_History_Fetch(string uvc, string vin, string stateCode, DateTime date)
        {
            const string queryName = Prefix + ".CarDatastore_Car_History_Fetch.txt";

            object squishVin = string.IsNullOrEmpty(vin) ? (object)DBNull.Value : Vin.Squish(vin);

            return Query(new[] { "Car" },
                         queryName,
                         new Parameter("Uvc",  uvc,       DbType.String),
                         new Parameter("Vin",  squishVin, DbType.String),
                         new Parameter("Code", stateCode, DbType.String),
                         new Parameter("Date", date,      DbType.DateTime));                        
        }

        /// <summary>
        /// Fetch the historical record of a car with the given identifier. Does not fetch all of the car's values, 
        /// just the ones pertaining to the Car_History table.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>        
        /// <returns>Data reader with car history details.</returns>
        public IDataReader Car_History_Fetch(int carHistoryId)
        {
            const string queryName = Prefix + ".CarDatastore_Car_History_Fetch_Id.txt";

            return Query(new[] { "Car" },
                         queryName,
                         new Parameter("CarHistoryID", carHistoryId, DbType.Int32));    
        }

        /// <summary>
        /// Insert a row into the car history table.
        /// </summary>
        /// <param name="entity">Car entity.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>Data reader.</returns>
        public IDataReader Car_History_Insert(Entities.CarEntity entity, int dataLoadId)
        {
            const string queryNameI = Prefix + ".CarDatastore_Car_History_Insert.txt";

            const string queryNameF = Prefix + ".CarDatastore_Car_History_Fetch_Identity.txt";

            NonQuery(queryNameI,
                     new Parameter("CarID",       entity.CarId,       DbType.Int32),
                     new Parameter("DataLoadID",  dataLoadId,         DbType.Int32),
                     new Parameter("CarHashCode", entity.HashCode,    DbType.Int64),
                     new Parameter("AuditRowID",  entity.AuditRow.Id, DbType.Int32));

            return Query(new[] { "Car" },
                         queryNameF);
        }        
       
        /// <summary>
        /// Fetch the details of the car with the given values.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="stateCode">Two letter US state identifier.</param>
        /// <returns>Data reader with the car identifier.</returns>
        public IDataReader Car_Fetch(string uvc, string vin, string stateCode)
        {
            const string queryName = Prefix + ".CarDatastore_Car_Fetch.txt";

            return Query(new[] { "Car" },
                         queryName,
                         new Parameter("Uvc",  uvc,             DbType.String),
                         new Parameter("Vin",  Vin.Squish(vin), DbType.String),
                         new Parameter("Code", stateCode,       DbType.String));
        }

        /// <summary>
        /// Insert a car with the given uvc and vin into the database.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="stateCode">Two letter identifier of the US state.</param>
        /// <returns>Data reader.</returns>
        public IDataReader Car_Insert(string uvc, string vin, string stateCode)
        {
            const string queryNameI = Prefix + ".CarDatastore_Car_Insert.txt";

            const string queryNameF = Prefix + ".CarDatastore_Car_Fetch_Identity.txt";

            NonQuery(queryNameI,
                     new Parameter("Uvc",  uvc,             DbType.String),
                     new Parameter("Vin",  Vin.Squish(vin), DbType.String),
                     new Parameter("Code", stateCode,       DbType.String));

            return Query(new[] { "Car" },
                         queryNameF);
        }

        /// <summary>
        /// Fetch a car's prices.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <returns>Data reader with price data.</returns>
        public IDataReader Car_Prices_Fetch(int carHistoryId)
        {
            const string queryName = Prefix + ".CarDatastore_Car_Prices_Fetch_Id.txt";

            return Query(new[] { "Price" },
                         queryName,
                         new Parameter("CarHistoryID", carHistoryId, DbType.Int32));
        }

        /// <summary>
        /// Insert car prices.
        /// </summary>        
        /// <param name="prices">Prices.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        public void Car_Prices_Insert(Entities.Prices prices, int dataLoadId)
        {
            const string queryName = Prefix + ".CarDatastore_Car_Prices_Insert.txt";

            object advance = prices.FinanceAdvance.HasValue ? (object)prices.FinanceAdvance.Value : DBNull.Value;
            object mileage = prices.Mileage.HasValue        ? (object)prices.Mileage.Value        : DBNull.Value;
            object msrp    = prices.Msrp.HasValue           ? (object)prices.Msrp.Value           : DBNull.Value;            

            NonQuery(queryName,
                     new Parameter("CarHistoryID",     prices.CarHistoryId,     DbType.Int32),
                     new Parameter("DataLoadID",       dataLoadId,              DbType.Int32),
                     new Parameter("Mileage",          mileage,                 DbType.Int32),
                     new Parameter("Msrp",             msrp,                    DbType.Int32),
                     new Parameter("FinanceAdvance",   advance,                 DbType.Int32),
                     new Parameter("ResidualValueID",  prices.ResidualValueId,  DbType.Int32),
                     new Parameter("TradeInValueID",   prices.TradeInValueId,   DbType.Int32),
                     new Parameter("WholesaleValueID", prices.WholesaleValueId, DbType.Int32),
                     new Parameter("RetailValueID",    prices.RetailValueId,    DbType.Int32),
                     new Parameter("PriceIncludes",    prices.PriceIncludes,    DbType.String));
        }        

        /// <summary>
        /// Fetch a car's colors.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <returns>Data reader with colors.</returns>
        public IDataReader Color_Fetch(int carHistoryId)
        {
            const string queryName = Prefix + ".CarDatastore_Color_Fetch_Id.txt";

            return Query(new[] { "Color" },
                         queryName,
                         new Parameter("CarHistoryID", carHistoryId, DbType.Int32));   
        }

        /// <summary>
        /// Insert a color.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>        
        /// <param name="category">Color category.</param>
        /// <param name="description">Color description.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>Data reader.</returns>        
        public IDataReader Color_Insert(int carHistoryId, string category, string description, int dataLoadId)
        {
            const string queryNameI = Prefix + ".CarDatastore_Color_Insert.txt";

            const string queryNameF = Prefix + ".CarDatastore_Color_Fetch_Identity.txt";

            NonQuery(queryNameI,
                     new Parameter("CarHistoryID", carHistoryId, DbType.Int32),
                     new Parameter("DataLoadID",   dataLoadId,   DbType.Int32),
                     new Parameter("Category",     category,     DbType.String),
                     new Parameter("Description",  description,  DbType.String));

            return Query(new[] { "MileageAdjustment" },
                         queryNameF);
        }

        /// <summary>
        /// Fetch a car's mileage adjustments.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <returns>Data reader with mileage adjustments.</returns>
        public IDataReader MileageAdjustment_Fetch(int carHistoryId)
        {
            const string queryName = Prefix + ".CarDatastore_MileageAdjustment_Fetch_Id.txt";

            return Query(new[] { "MileageAdjustment" },
                         queryName,
                         new Parameter("CarHistoryID", carHistoryId, DbType.Int32));  
        }

        /// <summary>
        /// Insert a mileage adjustment.
        /// </summary>        
        /// <param name="adjustment">Mileage adjustment values.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        public void MileageAdjustment_Insert(Entities.MileageAdjustment adjustment, int dataLoadId)
        {
            const string queryName = Prefix + ".CarDatastore_MileageAdjustment_Insert.txt";

            object financeAdvance = adjustment.FinanceAdvance.HasValue ? (object)adjustment.FinanceAdvance.Value : DBNull.Value;

            NonQuery(queryName,
                     new Parameter("CarHistoryID",   adjustment.CarHistoryId, DbType.Int32),
                     new Parameter("DataLoadID",     dataLoadId,              DbType.Int32),
                     new Parameter("BeginRange",     adjustment.BeginRange,   DbType.Int32),
                     new Parameter("EndRange",       adjustment.EndRange,     DbType.Int32),
                     new Parameter("FinanceAdvance", financeAdvance,          DbType.Int32),
                     new Parameter("ValueID",        adjustment.ValueId,      DbType.Int32));
        }

        /// <summary>
        /// Fetch a car's options.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <returns>Data reader with options.</returns>
        public IDataReader Options_Fetch(int carHistoryId)
        {
            const string queryName = Prefix + ".CarDatastore_OptionAdjustment_Fetch_Id.txt";

            return Query(new[] { "Option" },
                         queryName,
                         new Parameter("CarHistoryID", carHistoryId, DbType.Int32));
        }

        /// <summary>
        /// Insert an option adjustment.
        /// </summary>        
        /// <param name="option">Option adjustment.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        public void Options_Insert(Entities.Option option, int dataLoadId)
        {
            const string queryName = Prefix + ".CarDatastore_OptionAdjustment_Insert.txt";

            object amount = option.Amount.HasValue ? (object)option.Amount.Value : DBNull.Value;

            NonQuery(queryName,
                     new Parameter("CarHistoryID",           option.CarHistoryId,        DbType.Int32),
                     new Parameter("DataLoadID",             dataLoadId,                 DbType.Int32),
                     new Parameter("AdCode",                 option.AdCode,              DbType.String),
                     new Parameter("Description",            option.Description,         DbType.String),
                     new Parameter("OptionAdjustmentSignID", (int)option.AdjustmentSign, DbType.Int16),
                     new Parameter("Amount",                 amount,                     DbType.Int32),
                     new Parameter("ResidualValueID",        option.ResidualValueId,     DbType.Int32),
                     new Parameter("Flag",                   option.Flag,                DbType.Boolean));
        }

        /// <summary>
        /// Fetch a car's residual value.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <param name="residualValueId">Residual value identifier.</param>
        /// <returns>Data reader with residual value values.</returns>
        public IDataReader ResidualValue_Fetch(int carHistoryId, int residualValueId)
        {
            const string queryName = Prefix + ".CarDatastore_ResidualValue_Fetch_Id.txt";

            return Query(new[] { "ResidualValue" },
                         queryName,
                         new Parameter("CarHistoryID",    carHistoryId,    DbType.Int32),
                         new Parameter("ResidualValueID", residualValueId, DbType.Int32));
        }

        /// <summary>
        /// Insert a car's residual values.
        /// </summary>        
        /// <param name="value">Residual values.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>Data reader.</returns>        
        public IDataReader ResidualValue_Insert(Entities.ResidualValue value, int dataLoadId)
        {
            const string queryNameI = Prefix + ".CarDatastore_ResidualValue_Insert.txt";

            const string queryNameF = Prefix + ".CarDatastore_ResidualValue_Fetch_Identity.txt";

            object resid12 = value.Resid12.HasValue ? (object)value.Resid12.Value : DBNull.Value;
            object resid24 = value.Resid24.HasValue ? (object)value.Resid24.Value : DBNull.Value;
            object resid30 = value.Resid30.HasValue ? (object)value.Resid30.Value : DBNull.Value;
            object resid36 = value.Resid36.HasValue ? (object)value.Resid36.Value : DBNull.Value;
            object resid42 = value.Resid42.HasValue ? (object)value.Resid42.Value : DBNull.Value;
            object resid48 = value.Resid48.HasValue ? (object)value.Resid48.Value : DBNull.Value;
            object resid60 = value.Resid60.HasValue ? (object)value.Resid60.Value : DBNull.Value;
            object resid72 = value.Resid72.HasValue ? (object)value.Resid72.Value : DBNull.Value;

            NonQuery(queryNameI,
                     new Parameter("CarHistoryID", value.CarHistoryId, DbType.Int32),
                     new Parameter("DataLoadID",   dataLoadId,         DbType.Int32),
                     new Parameter("Resid12",      resid12,            DbType.Int32),
                     new Parameter("Resid24",      resid24,            DbType.Int32),
                     new Parameter("Resid30",      resid30,            DbType.Int32),
                     new Parameter("Resid36",      resid36,            DbType.Int32),
                     new Parameter("Resid42",      resid42,            DbType.Int32),
                     new Parameter("Resid48",      resid48,            DbType.Int32),
                     new Parameter("Resid60",      resid60,            DbType.Int32),
                     new Parameter("Resid72",      resid72,            DbType.Int32));

            return Query(new[] { "ResidualValue" },
                         queryNameF);
        }

        /// <summary>
        /// Fetch color swatches tied to a color.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <param name="colorId">Color identifier.</param>
        public IDataReader Swatch_Fetch(int carHistoryId, int colorId)
        {
            const string queryName = Prefix + ".CarDatastore_Swatch_Fetch_Id.txt";

            return Query(new[] { "Count", "Swatch" },
                         queryName,
                         new Parameter("CarHistoryID", carHistoryId, DbType.Int32),
                         new Parameter("ColorID",      colorId, DbType.Int32));
        }

        /// <summary>
        /// Insert a color swatch.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <param name="colorId">Color identifier.</param>
        /// <param name="htmlIndex">Html index.</param>
        /// <param name="htmlColor">Html color.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        public void Swatch_Insert(int carHistoryId, int colorId, int htmlIndex, string htmlColor, int dataLoadId)
        {
            const string queryName = Prefix + ".CarDatastore_Swatch_Insert.txt";

            NonQuery(queryName,
                     new Parameter("CarHistoryID", carHistoryId, DbType.Int32),
                     new Parameter("DataLoadID",   dataLoadId,   DbType.Int32),
                     new Parameter("ColorID",      colorId,      DbType.Int32),
                     new Parameter("HtmlIndex",    htmlIndex,    DbType.Int16),
                     new Parameter("HtmlColor",    htmlColor,    DbType.String));
        }

        /// <summary>
        /// Fetch uvcs tied to the given vin.
        /// </summary>
        /// <param name="vin">Vin.</param>
        /// <returns>Data reader with uvcs.</returns>
        public IDataReader Uvc_Fetch(string vin)
        {
            const string queryName = Prefix + ".CarDatastore_Uvc_Fetch_Vin.txt";

            return Query(new[] { "Uvc" },
                         queryName,
                         new Parameter("Vin", Vin.Squish(vin), DbType.String));
        }

        /// <summary>
        /// Fetch a car's value.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <param name="valueId">Value identifier.</param>
        /// <returns>Data reader with value values.</returns>
        public IDataReader Value_Fetch(int carHistoryId, int valueId)
        {
            const string queryName = Prefix + ".CarDatastore_Value_Fetch_Id.txt";

            return Query(new[] { "Value" },
                         queryName,
                         new Parameter("CarHistoryID", carHistoryId, DbType.Int32),
                         new Parameter("ValueID",      valueId,      DbType.Int32));
        }

        /// <summary>
        /// Insert values.
        /// </summary>        
        /// <param name="values">Values to insert.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>Data reader.</returns>        
        public IDataReader Value_Insert(Entities.Value values, int dataLoadId)
        {
            const string queryNameI = Prefix + ".CarDatastore_Value_Insert.txt";

            const string queryNameF = Prefix + ".CarDatastore_Value_Fetch_Identity.txt";

            object average    = values.Average.HasValue    ? (object)values.Average.Value : DBNull.Value;
            object clean      = values.Clean.HasValue      ? (object)values.Clean.Value   : DBNull.Value;
            object extraClean = values.ExtraClean.HasValue ? (object)values.ExtraClean    : DBNull.Value;
            object rough      = values.Rough.HasValue      ? (object)values.Rough         : DBNull.Value;

            NonQuery(queryNameI,
                     new Parameter("CarHistoryID", values.CarHistoryId, DbType.Int32),
                     new Parameter("DataLoadID",   dataLoadId,          DbType.Int32),
                     new Parameter("Average",      average,             DbType.Int32),
                     new Parameter("Clean",        clean,               DbType.Int32),
                     new Parameter("ExtraClean",   extraClean,          DbType.Int32),
                     new Parameter("Rough",        rough,               DbType.Int32));

            return Query(new[] { "Value" },
                         queryNameF);
        }

        /// <summary>
        /// Fetch the vins tied to the given uvc.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <returns>Data reader with vins.</returns>
        public IDataReader Vin_Fetch(string uvc)
        {
            const string queryName = Prefix + ".CarDatastore_Vin_Fetch_Uvc.txt";

            return Query(new[] { "Vin" },
                         queryName,
                         new Parameter("Uvc", uvc, DbType.String));
        }        
    }
}
