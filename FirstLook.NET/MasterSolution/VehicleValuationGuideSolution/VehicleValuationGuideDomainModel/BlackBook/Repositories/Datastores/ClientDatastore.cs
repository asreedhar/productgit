﻿using System.Data;
using System.Reflection;
using FirstLook.Common.Core.Data;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Datastores
{
    /// <summary>
    /// Client datastore for BlackBook operations.
    /// </summary>
    public class ClientDatastore : SessionDataStore, IClientDatastore
    {
        #region Properties

        /// <summary>
        /// Get the current assembly.
        /// </summary>
        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        /// <summary>
        /// Prefix for resources.
        /// </summary>
        const string Prefix = "FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Resources";

        #endregion

        /// <summary>
        /// Get the vehicle with BlackBook configurations that has the given identifier.
        /// </summary>
        /// <param name="vehicleId">Vehicle identifier.</param>
        /// <returns>Data reader with details on the vehicle.</returns>
        public IDataReader Vehicle_BlackBook_Fetch(int vehicleId)
        {
            const string queryName = Prefix + ".ClientDatastore_Vehicle_BlackBook_Fetch.txt";

            return Query(new[] { "Vehicle_BlackBook" },
                         queryName,
                         new Parameter("VehicleID", vehicleId, DbType.Int32));
        }

        /// <summary>
        /// Insert an association between the vehicle and configuration with the given identifiers.
        /// </summary>
        /// <param name="vehicleId">Vehicle identifier.</param>
        /// <param name="vehicleConfigurationId">Vehicle configuration identifier.</param>        
        public void Vehicle_BlackBook_Insert(int vehicleId, int vehicleConfigurationId)
        {
            const string queryName = Prefix + ".ClientDatastore_Vehicle_BlackBook_Insert.txt";

            NonQuery(queryName,
                     new Parameter("VehicleID",              vehicleId,              DbType.Int32),
                     new Parameter("VehicleConfigurationID", vehicleConfigurationId, DbType.Int32));
        }
    }
}
