﻿using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories
{
    /// <summary>
    /// Module for registering repository components.
    /// </summary>
    public class Module : IModule
    {
        /// <summary>
        /// Register BlackBook repository components.
        /// </summary>        
        public void Configure(IRegistry registry)
        {            
            registry.Register<Datastores.Module>();

            registry.Register<Serializers.Module>();

            registry.Register<IBlackBookRepository, BlackBookRepository>(ImplementationScope.Shared);
        }
    }
}
