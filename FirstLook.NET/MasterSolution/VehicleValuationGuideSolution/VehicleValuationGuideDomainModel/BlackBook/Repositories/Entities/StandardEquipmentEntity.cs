﻿using FirstLook.Client.DomainModel.Clients.Model.Auditing;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities
{
    /// <summary>
    /// Standard equipment entity.
    /// </summary>
    public class StandardEquipmentEntity
    {
        /// <summary>
        /// Standard equipment identifier.
        /// </summary>
        public int Id { get; internal set; }

        /// <summary>
        /// Standard equipment history identifier.
        /// </summary>
        public int HistoryId { get; internal set; }

        /// <summary>
        /// Audit row.
        /// </summary>
        public AuditRow AuditRow { get; internal set; }

        /// <summary>
        /// Uvc of the car the equipment belongs to.
        /// </summary>
        public string Uvc { get; internal set; }

        /// <summary>
        /// Hash of the equipment values.
        /// </summary>
        public long HashCode { get; internal set; }

        /// <summary>
        /// Standard equipment values.
        /// </summary>
        public Model.StandardEquipment StandardEquipment { get; internal set; }

        /// <summary>
        /// Initialize the standard equipment.
        /// </summary>
        public StandardEquipmentEntity()
        {
            StandardEquipment = new Model.StandardEquipment(); 
        }
    }
}
