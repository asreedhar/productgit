﻿using System.Drawing;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Repositories;
using FirstLook.Client.DomainModel.Clients.Repositories.Entities;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities
{
    /// <summary>
    /// Mapper between model objects and repository entities.
    /// </summary>
    public static class Mapper
    {
        #region Attributes

        /// <summary>
        /// Map attributes from a model object into a repository entity.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <param name="car">Model object.</param>                
        /// <returns>Repository entity.</returns>
        public static Attributes Map(int carHistoryId, Model.Car car)
        {
            return new Attributes
            {
                BaseHorsepower    = car.BaseHorsepower,
                CarHistoryId      = carHistoryId,
                Cylinders         = car.Cylinders,
                DriveTrain        = car.DriveTrain,
                Engine            = car.Engine,
                FuelType          = car.FuelType,
                TaxableHorsepower = car.TaxableHorsepower,
                TireSize          = car.TireSize,
                Transmission      = car.Transmission,
                VehicleClass      = car.VehicleClass,
                Weight            = car.Weight,
                WheelBase         = car.WheelBase                
            };
        }

        /// <summary>
        /// Map attribute entity values into a car model object.
        /// </summary>
        /// <param name="car">Car model object to populate.</param>
        /// <param name="attributes">Attribute entity values.</param>        
        public static void Map(Model.Car car, Attributes attributes)
        {
            car.BaseHorsepower    = attributes.BaseHorsepower;
            car.Cylinders         = attributes.Cylinders;
            car.DriveTrain        = attributes.DriveTrain;
            car.Engine            = attributes.Engine;
            car.FuelType          = attributes.FuelType;
            car.TaxableHorsepower = attributes.TaxableHorsepower;
            car.TireSize          = attributes.TireSize;
            car.Transmission      = attributes.Transmission;
            car.VehicleClass      = attributes.VehicleClass;
            car.Weight            = attributes.Weight;
            car.WheelBase         = attributes.WheelBase;            
        }

        #endregion

        #region Color

        /// <summary>
        /// Map a color from a repository entity into a model object.
        /// </summary>
        /// <param name="color">Repository entity.</param>
        /// <returns>Model object.</returns>
        public static Model.Color Map(Color color)
        {
            Model.Color model = new Model.Color
            {
                Category = color.Category,
                Description = color.Description
            };

            foreach (Swatch swatch in color.Swatches)
            {
                model.Swatches.Add(ColorTranslator.FromHtml(swatch.HtmlColor));
            }

            return model;
        }

        #endregion

        #region Description

        /// <summary>
        /// Map a description from a model object into a repository entity.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <param name="description">Model object.</param>                
        /// <returns>Repository entity.</returns>
        public static Description Map(int carHistoryId, Model.CarInfo description)
        {
            return new Description
            {                
                BodyStyle    = description.BodyStyle,
                CarHistoryId = carHistoryId,
                Make         = description.Make,
                Model        = description.Model,
                Series       = description.Series,
                Year         = description.Year
            };
        }

        /// <summary>
        /// Map description entity values into a car model object.
        /// </summary>
        /// <param name="car">Car model object to populate.</param>
        /// <param name="description">Description entity values.</param>        
        public static void Map(Model.Car car, Description description)
        {
            car.BodyStyle = description.BodyStyle;
            car.Make      = description.Make;
            car.Model     = description.Model;
            car.Series    = description.Series;
            car.Year      = description.Year;
        }

        #endregion

        #region Mileage Adjustment

        /// <summary>
        /// Map a value from a model object into a repository entity.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <param name="valueId">Value identifier.</param>        
        /// <param name="adjustment">Model object.</param>
        /// <returns>Repository entity.</returns>
        public static MileageAdjustment Map(int carHistoryId, int valueId, Model.MileageAdjustment adjustment)
        {
            return new MileageAdjustment
            {
                BeginRange     = adjustment.BeginRange,
                CarHistoryId   = carHistoryId,
                EndRange       = adjustment.EndRange,
                FinanceAdvance = adjustment.FinanceAdvance,                
                ValueId        = valueId
            };
        }

        /// <summary>
        /// Map a value from a repository entity into a model object.
        /// </summary>
        /// <param name="adjustment">Mileage adjustment repository entity.</param>
        /// <param name="value">Value repository entity.</param>
        /// <returns>Model object.</returns>
        public static Model.MileageAdjustment Map(MileageAdjustment adjustment, Value value)
        {
            return new Model.MileageAdjustment
            {
                Average        = value.Average,
                BeginRange     = adjustment.BeginRange,
                Clean          = value.Clean,
                EndRange       = adjustment.EndRange,
                ExtraClean     = value.ExtraClean,
                FinanceAdvance = adjustment.FinanceAdvance,
                Rough          = value.Rough
            };
        }

        #endregion

        #region Prices

        /// <summary>
        /// Map price entity values into a car model object.
        /// </summary>
        /// <param name="car">Car model object to populate.</param>
        /// <param name="prices">Price entity values.</param>        
        public static void Map(Model.Car car, Prices prices)
        {
            car.FinanceAdvance = prices.FinanceAdvance;
            car.Mileage        = prices.Mileage;
            car.Msrp           = prices.Msrp;
            car.PriceIncludes  = prices.PriceIncludes;            
        }

        #endregion

        #region Option

        /// <summary>
        /// Map an option from a model object into a repository entity.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <param name="residualValueId">Residual value identifier.</param>
        /// <param name="option">Model object.</param>
        /// <returns>Repository entity.</returns>
        public static Option Map(int carHistoryId, int residualValueId, Model.Option option)
        {
            return new Option
            {
                AdCode          = option.AdCode,
                AdjustmentSign  = option.OptionAdjustmentSign,
                Amount          = option.Amount,              
                CarHistoryId    = carHistoryId,
                Description     = option.Description,
                Flag            = option.Flag,
                ResidualValueId = residualValueId
            };
        }

        /// <summary>
        /// Map an option from a repository entity into a model object.
        /// </summary>
        /// <param name="option">Option repository entity.</param>
        /// <param name="residualValue">Residual value repository entity.</param>
        /// <returns>Model object.</returns>
        public static Model.Option Map(Option option, ResidualValue residualValue)
        {
            return new Model.Option
            { 
                AdCode               = option.AdCode,
                Amount               = option.Amount,
                Description          = option.Description,
                Flag                 = option.Flag, 
                OptionAdjustmentSign = option.AdjustmentSign,
                Residual             = Map(residualValue)                
            };
        }

        #endregion        

        #region Residual Value

        /// <summary>
        /// Map a residual value from a model object into a repository entity.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <param name="residualValue">Model object.</param>
        /// <returns>Repository entity.</returns>
        public static ResidualValue Map(int carHistoryId, Model.ResidualValue residualValue)
        {
            return new ResidualValue
            {
                CarHistoryId = carHistoryId,
                Resid12      = residualValue.Resid12,
                Resid24      = residualValue.Resid24,
                Resid30      = residualValue.Resid30,
                Resid36      = residualValue.Resid36,
                Resid42      = residualValue.Resid42,
                Resid48      = residualValue.Resid48,
                Resid60      = residualValue.Resid60,
                Resid72      = residualValue.Resid72
            };
        }

        /// <summary>
        /// Map a residual value from a repository entity into a model object.
        /// </summary>
        /// <param name="entity">Repository entity.</param>
        /// <returns>Model object.</returns>
        public static Model.ResidualValue Map(ResidualValue entity)
        {
            return new Model.ResidualValue
            {
                Resid12 = entity.Resid12,
                Resid24 = entity.Resid24,
                Resid30 = entity.Resid30,
                Resid36 = entity.Resid36,
                Resid42 = entity.Resid42,
                Resid48 = entity.Resid48,
                Resid60 = entity.Resid60,
                Resid72 = entity.Resid72
            };
        }

        #endregion

        #region Value

        /// <summary>
        /// Map a value from a model object into a repository entity.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <param name="value">Model object.</param>        
        /// <returns>Repository entity.</returns>
        public static Value Map(int carHistoryId, Model.Value value)
        {
            return new Value
            {
                Average      = value.Average,
                CarHistoryId = carHistoryId,
                Clean        = value.Clean,
                ExtraClean   = value.ExtraClean,
                Rough        = value.Rough,
            };
        }

        /// <summary>
        /// Map a value from a repository entity into a model object.
        /// </summary>
        /// <param name="value">Repository entity.</param>
        /// <returns>Model object.</returns>
        public static Model.Value Map(Value value)
        {
            return new Model.Value
            {
                Average    = value.Average,
                Clean      = value.Clean,
                ExtraClean = value.ExtraClean,
                Rough      = value.Rough
            };
        }

        #endregion

        #region Publication

        /// <summary>
        /// Create an edition of a publication from a car, configuration, matrix and user.
        /// </summary>
        /// <param name="car">Car entity.</param>
        /// <param name="configuration">Configuration entity.</param>
        /// <param name="matrix">Price matrix.</param>
        /// <returns>Edition of a publication.</returns>
        public static Model.Api.IEdition<Model.Api.IPublication> Map(CarEntity car, VehicleConfigurationEntity configuration, Model.Api.Matrix matrix)
        {
            Model.Api.IPublication publication = new Model.Api.Publication
            {
                Car                           = car.Car,
                ChangeAgent                   = configuration.ChangeAgent,
                ChangeType                    = configuration.ChangeType,
                Matrix                        = matrix,
                VehicleConfiguration          = configuration.VehicleConfiguration,
                VehicleConfigurationHistoryId = configuration.VehicleConfigurationHistoryId
            };

            IUser user;
            switch (configuration.User.UserType)
            {
                case UserType.Member:
                    user = new DealerRepository().Principal(configuration.User.Handle).User;
                    break;
                default:
                    user = new AgentRepository().Principal(configuration.User.Id).User;
                    break;
            }

            return new Model.Api.Edition<Model.Api.IPublication>
            {
                DateRange = new Model.Api.DateRange
                {
                    BeginDate = configuration.AuditRow.ValidFrom,
                    EndDate   = configuration.AuditRow.ValidUpTo
                },
                User = user,
                Data = publication
            };
        }

        #endregion
    }
}
