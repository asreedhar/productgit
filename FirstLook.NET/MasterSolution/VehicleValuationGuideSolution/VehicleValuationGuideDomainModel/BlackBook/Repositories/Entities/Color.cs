﻿using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities
{
    /// <summary>
    /// Car color.
    /// </summary>    
    public class Color
    {
        /// <summary>
        /// Car history identifier.
        /// </summary>
        public int CarHistoryId { get; internal set; }

        /// <summary>
        /// Color identifier.
        /// </summary>
        public int ColorId { get; internal set; }

        /// <summary>
        /// Category.
        /// </summary>
        public string Category { get; internal set; }

        /// <summary>
        /// Description.
        /// </summary>
        public string Description { get; internal set; }

        /// <summary>
        /// Color swatches belonging to this color.
        /// </summary>
        public IList<Swatch> Swatches { get; internal set; }

        /// <summary>
        /// Initialize the swatch list.
        /// </summary>
        public Color()
        {
            Swatches = new List<Swatch>();
        }

    }
}
