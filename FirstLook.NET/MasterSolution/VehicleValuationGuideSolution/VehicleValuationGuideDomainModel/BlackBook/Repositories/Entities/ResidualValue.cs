﻿namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities
{
    /// <summary>
    /// Residual values.
    /// </summary>    
    public class ResidualValue
    {
        /// <summary>
        /// Car history identifier.
        /// </summary>
        public int CarHistoryId { get; internal set; }

        /// <summary>
        /// Residual value identifier.
        /// </summary>
        public int ResidualValueId { get; internal set; }

        /// <summary>
        /// 12 month residual value.
        /// </summary>
        public int? Resid12 { get; internal set; }

        /// <summary>
        /// 24 month residual value.
        /// </summary>
        public int? Resid24 { get; internal set; }

        /// <summary>
        /// 30 month residual value.
        /// </summary>
        public int? Resid30 { get; internal set; }

        /// <summary>
        /// 36 month residual value.
        /// </summary>
        public int? Resid36 { get; internal set; }

        /// <summary>
        /// 42 month residual value.
        /// </summary>
        public int? Resid42 { get; internal set; }

        /// <summary>
        /// 48 month residual value.
        /// </summary>
        public int? Resid48 { get; internal set; }
        
        /// <summary>
        /// 60 month residual value.
        /// </summary>
        public int? Resid60 { get; internal set; }

        /// <summary>
        /// 72 month residual value.
        /// </summary>
        public int? Resid72 { get; internal set; }
    }
}
