﻿namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities
{
    /// <summary>
    /// Car prices.
    /// </summary>
    public class Prices
    {
        /// <summary>
        /// Car history identifier.
        /// </summary>
        public int CarHistoryId { get; internal set; }

        /// <summary>
        /// Mileage.
        /// </summary>
        public int? Mileage { get; internal set; }

        /// <summary>
        /// Msrp.
        /// </summary>
        public int? Msrp { get; internal set; }

        /// <summary>
        /// Finance advance.
        /// </summary>
        public int? FinanceAdvance { get; internal set; }

        /// <summary>
        /// Residual value identifier.
        /// </summary>
        public int ResidualValueId { get; internal set; }

        /// <summary>
        /// Trade-in value identifier.
        /// </summary>
        public int TradeInValueId { get; internal set; }

        /// <summary>
        /// Wholesale value identifier.
        /// </summary>
        public int WholesaleValueId { get; internal set; }

        /// <summary>
        /// Retail value identifier..
        /// </summary>
        public int RetailValueId { get; internal set; }

        /// <summary>
        /// Price includes.
        /// </summary>
        public string PriceIncludes { get; internal set; }
    }
}
