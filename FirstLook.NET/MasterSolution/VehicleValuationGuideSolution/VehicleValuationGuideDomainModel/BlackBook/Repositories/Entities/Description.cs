﻿namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities
{
    /// <summary>
    /// Description of a car.
    /// </summary>    
    public class Description
    {
        /// <summary>
        /// Car history identifier.
        /// </summary>
        public int CarHistoryId { get; internal set; }

        /// <summary>
        /// Year.
        /// </summary>
        public int Year { get; internal set; }

        /// <summary>
        /// Make.
        /// </summary>
        public string Make { get; internal set; }

        /// <summary>
        /// Model.
        /// </summary>
        public string Model { get; internal set; }

        /// <summary>
        /// Series.
        /// </summary>
        public string Series { get; internal set; }

        /// <summary>
        /// Body style.
        /// </summary>
        public string BodyStyle { get; internal set; }        
    }
}
