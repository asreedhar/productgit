﻿namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities
{
    /// <summary>
    /// A client's vehicle that has had a configuration made.
    /// </summary>
    public class ClientVehicle
    {
        /// <summary>
        /// Vehicle identifier.
        /// </summary>
        public int VehicleId { get; internal set; }

        /// <summary>
        /// Vehicle configuration identifier.
        /// </summary>
        public int VehicleConfigurationId { get; internal set; }
    }
}
