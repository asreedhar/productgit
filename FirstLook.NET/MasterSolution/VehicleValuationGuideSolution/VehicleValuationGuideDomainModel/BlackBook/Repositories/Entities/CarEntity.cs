﻿using FirstLook.Client.DomainModel.Clients.Model.Auditing;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities
{
    /// <summary>
    /// Car entity.
    /// </summary>
    public class CarEntity
    {                
        /// <summary>
        /// Car identifier.
        /// </summary>
        public int CarId { get; internal set; }

        /// <summary>
        /// Identifier of a car's values at one point in time.
        /// </summary>
        public int CarHistoryId { get; internal set; }

        /// <summary>
        /// Car values.
        /// </summary>
        public Model.Car Car { get; internal set; }

        /// <summary>
        /// Hash of the car's values.
        /// </summary>
        public long HashCode { get; internal set; }        

        /// <summary>
        /// Audit row.
        /// </summary>
        public AuditRow AuditRow { get; internal set; }       
 
        /// <summary>
        /// Initialize the car on construction.
        /// </summary>
        public CarEntity()
        {
            Car = new Model.Car();
        }
    }
}
