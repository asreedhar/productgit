﻿namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities
{
    /// <summary>
    /// Mileage adjustment.
    /// </summary>
    public class MileageAdjustment
    {
        /// <summary>
        /// Car history identifier.
        /// </summary>
        public int CarHistoryId { get; internal set;}

        /// <summary>
        /// Adjustment identifier.
        /// </summary>
        public int MileageAdjustmentId { get; internal set; }

        /// <summary>
        /// Begin range.
        /// </summary>
        public int BeginRange { get; internal set; }

        /// <summary>
        /// End range.
        /// </summary>
        public int EndRange { get; internal set; }

        /// <summary>
        /// Finance advance.
        /// </summary>
        public int? FinanceAdvance{ get; internal set; }

        /// <summary>
        /// Value identifier.
        /// </summary>
        public int ValueId { get; internal set; }
    }
}
