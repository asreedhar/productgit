﻿using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Entities;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities
{
    /// <summary>
    /// Vehicle configuration entity.
    /// </summary>
    public class VehicleConfigurationEntity
    {
        /// <summary>
        /// Vehicle configuration identifier.
        /// </summary>
        public int VehicleConfigurationId { get; internal set; }

        /// <summary>
        /// Identifier of a configuration's values at one point in time.
        /// </summary>
        public int VehicleConfigurationHistoryId { get; internal set; }
        
        /// <summary>
        /// Identifier of an historical car record.
        /// </summary>
        public int CarHistoryId { get; internal set; }

        /// <summary>
        /// Who was responsible for changing this vehicle configuration.
        /// </summary>
        public Model.Api.ChangeAgent ChangeAgent { get; internal set; }

        /// <summary>
        /// How this vehicle configuration changed (if at all) from the last one (if one exists).
        /// </summary>
        public Model.Api.ChangeType ChangeType { get; internal set; }

        /// <summary>
        /// Audit row for the historical record.
        /// </summary>
        public AuditRow AuditRow { get; internal set; }

        /// <summary>
        /// User responsible for the configuration.
        /// </summary>
        public User User { get; internal set; }

        /// <summary>
        /// Vehicle configuration values.
        /// </summary>
        public Model.Api.IVehicleConfiguration VehicleConfiguration { get; internal set; }
    }
}
