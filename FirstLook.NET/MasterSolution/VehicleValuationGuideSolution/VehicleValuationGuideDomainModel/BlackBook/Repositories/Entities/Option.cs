﻿using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities
{
    /// <summary>
    /// Option adjustment.
    /// </summary>
    public class Option
    {
        /// <summary>
        /// Car history identifier.
        /// </summary>
        public int CarHistoryId { get; internal set; }

        /// <summary>
        /// Adjustment code.
        /// </summary>
        public string AdCode { get; internal set; }

        /// <summary>
        /// Description.
        /// </summary>
        public string Description { get; internal set; }

        /// <summary>
        /// Add or deduct.
        /// </summary>
        public OptionAdjustmentSign AdjustmentSign { get; internal set; }

        /// <summary>
        /// Amount.
        /// </summary>
        public int? Amount { get; internal set; }

        /// <summary>
        /// Residual value identifier.
        /// </summary>
        public int ResidualValueId { get; internal set; }

        /// <summary>
        /// Flag.
        /// </summary>
        public bool Flag { get; internal set; }
    }
}
