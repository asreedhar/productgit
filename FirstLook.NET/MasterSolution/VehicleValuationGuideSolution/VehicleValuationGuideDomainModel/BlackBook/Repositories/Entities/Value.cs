﻿namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities
{
    /// <summary>
    /// A vehicle value.
    /// </summary>
    public class Value
    {
        /// <summary>
        /// Car history identifier.
        /// </summary>
        public int CarHistoryId { get; internal set; }

        /// <summary>
        /// Value identifier.
        /// </summary>
        public int ValueId { get; internal set; }        

        /// <summary>
        /// Average.
        /// </summary>
        public int? Average { get; internal set; }

        /// <summary>
        /// Clean.
        /// </summary>
        public int? Clean { get; internal set; }

        /// <summary>
        /// Extra clean.
        /// </summary>
        public int? ExtraClean { get; internal set; }        

        /// <summary>
        /// Rough.
        /// </summary>
        public int? Rough { get; internal set; }
    }
}
