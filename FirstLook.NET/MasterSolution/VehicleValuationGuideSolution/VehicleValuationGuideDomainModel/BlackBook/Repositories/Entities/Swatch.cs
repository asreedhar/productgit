﻿namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities
{
    /// <summary>
    /// Color swatch.
    /// </summary>    
    public class Swatch
    {
        /// <summary>
        /// Car history identifier.
        /// </summary>
        public int CarHistoryId { get; internal set; }

        /// <summary>
        /// Color this swatch belongs to.
        /// </summary>
        public int ColorId { get; internal set; }

        /// <summary>
        /// Index of the html color.
        /// </summary>
        public int HtmlIndex { get; internal set; }

        /// <summary>
        /// Name of the html color.
        /// </summary>
        public string HtmlColor { get; internal set; }
    }
}
