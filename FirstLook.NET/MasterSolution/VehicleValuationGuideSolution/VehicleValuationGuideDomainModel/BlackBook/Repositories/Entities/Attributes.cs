﻿namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities
{
    /// <summary>
    /// Attributes of a car.
    /// </summary>    
    public class Attributes
    {
        /// <summary>
        /// Car history identifier.
        /// </summary>
        public int CarHistoryId { get; internal set; }

        /// <summary>
        /// Wheel base.
        /// </summary>       
        public string WheelBase { get; internal set; }

        /// <summary>
        /// Taxable horsepower.
        /// </summary>
        public string TaxableHorsepower { get; internal set; }

        /// <summary>
        /// Weight.
        /// </summary>
        public string Weight { get; internal set; }

        /// <summary>
        /// Size of the tires.
        /// </summary>
        public string TireSize { get; internal set; }

        /// <summary>
        /// Base horsepower.
        /// </summary>
        public string BaseHorsepower { get; internal set; }

        /// <summary>
        /// Type of fuel.
        /// </summary>
        public string FuelType { get; internal set; }

        /// <summary>
        /// Number of cylinders.
        /// </summary>
        public string Cylinders { get; internal set; }

        /// <summary>
        /// Drive train.
        /// </summary>
        public string DriveTrain { get; internal set; }

        /// <summary>
        /// Transmission.
        /// </summary>
        public string Transmission { get; internal set; }

        /// <summary>
        /// Engine.
        /// </summary>
        public string Engine { get; internal set; }

        /// <summary>
        /// Vehicle class.
        /// </summary>
        public string VehicleClass { get; internal set; }
    }
}
