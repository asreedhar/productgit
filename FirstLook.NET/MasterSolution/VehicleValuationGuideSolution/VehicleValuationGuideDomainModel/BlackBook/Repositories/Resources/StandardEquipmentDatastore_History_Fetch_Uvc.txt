﻿
SELECT
	SEH.StandardEquipmentHistoryID,
	SEH.StandardEquipmentID,
	SEH.StandardEquipmentHashCode,
	SEH.AuditRowID,
	SE.Uvc,
	AR.ValidFrom,
	AR.ValidUpTo,
	AR.WasInsert,
	AR.WasUpdate,
	AR.WasDelete
FROM
	BlackBook.StandardEquipment_History SEH
JOIN
	BlackBook.StandardEquipment SE ON SE.StandardEquipmentID = SEH.StandardEquipmentID	
JOIN
	Audit.AuditRow AR ON AR.AuditRowID = SEH.AuditRowID
WHERE
	SE.Uvc = @Uvc
AND
	GETDATE() BETWEEN AR.ValidFrom AND AR.ValidUpTo
AND
	AR.WasDelete = 0