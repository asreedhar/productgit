﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Datastores;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities;
using IRepository=FirstLook.Client.DomainModel.Clients.Model.Auditing.IRepository;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Gateways
{
    /// <summary>
    /// Car data gateway.
    /// </summary>
    public class CarGateway : GatewayBase
    {
        #region Properties

        /// <summary>
        /// Initialize the car datastore.
        /// </summary>
        public CarGateway()
        {
            _datastore = Resolve<ICarDatastore>();            
        }

        /// <summary>
        /// Car datastore.
        /// </summary>
        private readonly ICarDatastore _datastore;

        #endregion                

        #region Cache Policy

        /// <summary>
        /// Is the cache for the car with the given uvc and vin valid?
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="state">Two letter US state identifier. Optional.</param>
        /// <returns>True if the cache is current and valid, false otherwise.</returns>
        public bool Cache_IsValid(string uvc, string vin, string state)
        {
            using (IDataReader reader = _datastore.Car_CachePolicy_IsValid(uvc, vin, state))
            {
                if (!reader.Read())
                {
                    throw new DataException("Expected validity check on car cache.");
                }

                return reader.GetInt32(reader.GetOrdinal("IsValid")) == 1;
            }               
        }

        /// <summary>
        /// Is the cache for any car with the given uvc valid?
        /// </summary>
        /// <param name="uvc">Uvc.</param>        
        /// <param name="state">Two letter US state identifier.</param>
        /// <returns>True if the cache is current and valid, false otherwise.</returns>
        public bool Cache_IsValid(string uvc, string state)
        {
            using (IDataReader reader = _datastore.Car_CachePolicy_IsValid(uvc, state))
            {
                if (!reader.Read())
                {
                    throw new DataException("Expected validity check on car cache.");
                }

                return reader.GetInt32(reader.GetOrdinal("IsValid")) == 1;
            }
        }

        /// <summary>
        /// Extend the validitiy of the cache for the car with the given identifier. Length that the cache window is
        /// extended is determined by the database table Vehicle.BlackBook.CachePolicy.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="state">Two letter US state identifier.</param>
        /// <returns>True, just to appease function signatures of callers.</returns>
        public bool Cache_Extend(string uvc, string vin, string state)
        {
            CarEntity entity = Fetch_History(uvc, vin, state, DateTime.Now);
            if (entity == null)
            {
                throw new DataException("No vehicle exists for the given values.");
            }

            _datastore.Car_CachePolicy_Upsert(entity.CarId);
            return true;
        }

        /// <summary>
        /// Extend the validitiy of the cache for the car with the given identifier. Length that the cache window is
        /// extended is determined by the database table Vehicle.BlackBook.CachePolicy.
        /// </summary>
        /// <param name="carId">Car identifier.</param>
        /// <returns>True, just to appease function signatures of callers.</returns>
        public bool Cache_Extend(int carId)
        {
            _datastore.Car_CachePolicy_Upsert(carId);
            return true;
        }

        #endregion

        #region Fetch Methods

        /// <summary>
        /// Fetch a car that is valid at the given date.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="state">Two letter US state identifier.</param>
        /// <param name="date">Date.</param>
        /// <returns>Car entity, or null if one didn't exist.</returns>
        public CarEntity Fetch(string uvc, string vin, string state, DateTime date)
        {
            CarEntity entity = Fetch_History(uvc, vin, state, date);
            if (entity == null)
            {
                return null;
            }

            Fetch_Description(entity);
            Fetch_Attributes(entity);
            Fetch_Prices(entity);
            Fetch_MileageAdjustments(entity);
            Fetch_Options(entity);

            return entity;
        }

        /// <summary>
        /// Fetch the car with the historical record for the given identifier.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <returns>Car entity.</returns>
        public CarEntity Fetch(int carHistoryId)
        {
            CarEntity entity = Fetch_History(carHistoryId);
            if (entity == null)
            {
                return null;
            }

            Fetch_Description(entity);
            Fetch_Attributes(entity);
            Fetch_Prices(entity);
            Fetch_MileageAdjustments(entity);
            Fetch_Options(entity);

            return entity;
        }

        /// <summary>
        /// Fetch a car history record.
        /// </summary>        
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="state">Two letter US state identifier.</param>
        /// <param name="date">Date.</param>
        /// <returns>Entity if one was found. Null otherwise.</returns>
        protected CarEntity Fetch_History(string uvc, string vin, string state, DateTime date)
        {            
            using (IDataReader reader = _datastore.Car_History_Fetch(uvc, vin, state, date))
            {
                if (reader.Read())
                {
                    CarEntity entity = new CarEntity
                    {
                        AuditRow     = Resolve<ISerializer<AuditRow>>().Deserialize((IDataRecord)reader),
                        CarId        = reader.GetInt32(reader.GetOrdinal("CarID")),
                        CarHistoryId = reader.GetInt32(reader.GetOrdinal("CarHistoryID")),
                        HashCode     = reader.GetInt64(reader.GetOrdinal("CarHashCode"))                        
                    };

                    entity.Car.Uvc = reader.GetString(reader.GetOrdinal("Uvc"));
                    entity.Car.Vin = reader.GetString(reader.GetOrdinal("Vin"));

                    return entity;
                }
            }
            return null;
        }

        /// <summary>
        /// Fetch a car history record by its identifier.
        /// </summary>
        /// <param name="carHistoryId"></param>
        /// <returns></returns>
        protected CarEntity Fetch_History(int carHistoryId)
        {
            using (IDataReader reader = _datastore.Car_History_Fetch(carHistoryId))
            {
                if (reader.Read())
                {
                    CarEntity entity = new CarEntity
                    {
                        AuditRow     = Resolve<ISerializer<AuditRow>>().Deserialize((IDataRecord)reader),
                        CarId        = reader.GetInt32(reader.GetOrdinal("CarID")),
                        CarHistoryId = reader.GetInt32(reader.GetOrdinal("CarHistoryID")),
                        HashCode     = reader.GetInt64(reader.GetOrdinal("CarHashCode"))                        
                    };

                    entity.Car.Uvc = reader.GetString(reader.GetOrdinal("Uvc"));
                    entity.Car.Vin = reader.GetString(reader.GetOrdinal("Vin"));

                    return entity;
                }
            }
            return null;   
        }

        /// <summary>
        /// Fetch a car's description values.
        /// </summary>
        /// <param name="entity">Entity that will be populated with description values, and that has the car history
        /// identifier to search against.</param>
        protected void Fetch_Description(CarEntity entity)
        {            
            using (IDataReader reader = _datastore.Car_Description_Fetch(entity.CarHistoryId))
            {
                if (!reader.Read())
                {
                    throw new DataException("Expected car description.");
                }

                Description description = Resolve<ISerializer<Description>>().Deserialize((IDataRecord)reader);

                Mapper.Map(entity.Car, description);
            }   
        }

        /// <summary>
        /// Fetch car attributes.
        /// </summary>
        /// <param name="entity">Entity that will be populated with attribute values, and that has the car history 
        /// identifier to search against.</param>
        protected void Fetch_Attributes(CarEntity entity)
        {            
            using (IDataReader reader = _datastore.Car_Attributes_Fetch(entity.CarHistoryId))
            {
                if (!reader.Read())
                {
                    throw new DataException("Expected car attributes.");
                }                

                Attributes attributes = Resolve<ISerializer<Attributes>>().Deserialize((IDataRecord)reader);

                Mapper.Map(entity.Car, attributes);
            }                      
        }

        /// <summary>
        /// Fetch car prices - including residual, retail, trade-in and wholesale values.
        /// </summary>
        /// <param name="entity">Entity that will be populated with prices, and that has the car history identifier
        /// to search against.</param>
        protected void Fetch_Prices(CarEntity entity)
        {
            Prices prices;
    
            using (IDataReader reader = _datastore.Car_Prices_Fetch(entity.CarHistoryId))
            {
                if (!reader.Read())
                {
                    throw new DataException("Expected car prices.");
                }
                                
                prices = Resolve<ISerializer<Prices>>().Deserialize((IDataRecord)reader);

                Mapper.Map(entity.Car, prices);
            }           

            entity.Car.ResidualValue  = Mapper.Map(Fetch_ResidualValue(entity.CarHistoryId, prices.ResidualValueId));
            entity.Car.RetailValue    = Mapper.Map(Fetch_Value(entity.CarHistoryId, prices.RetailValueId));
            entity.Car.TradeInValue   = Mapper.Map(Fetch_Value(entity.CarHistoryId, prices.TradeInValueId));
            entity.Car.WholesaleValue = Mapper.Map(Fetch_Value(entity.CarHistoryId, prices.WholesaleValueId));
        }

        /// <summary>
        /// Fetch a residual value.
        /// </summary>        
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <param name="residualValueId">Residual value identifier.</param>
        /// <returns>Residual value.</returns>
        protected ResidualValue Fetch_ResidualValue(int carHistoryId, int residualValueId)
        {
            using (IDataReader reader = _datastore.ResidualValue_Fetch(carHistoryId, residualValueId))
            {
                if (!reader.Read())
                {
                    throw new DataException("Expected residual values.");
                }
                return Resolve<ISerializer<ResidualValue>>().Deserialize((IDataRecord)reader);               
            }   
        }

        /// <summary>
        /// Fetch a value.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <param name="valueId">Value identifier.</param>
        /// <returns>Value.</returns>
        protected Value Fetch_Value(int carHistoryId, int valueId)
        {
            using (IDataReader reader = _datastore.Value_Fetch(carHistoryId, valueId))
            {
                if (!reader.Read())
                {
                    throw new DataException("Expected trade-in value.");
                }
                return Resolve<ISerializer<Value>>().Deserialize((IDataRecord)reader);                
            }
        }

        /// <summary>
        /// Fetch mileage adjustments.
        /// </summary>
        /// <param name="entity">Entity to be populated with mileage adjustments, and also that has the car history
        /// identifier to search against.</param>
        protected void Fetch_MileageAdjustments(CarEntity entity)
        {            
            using (IDataReader reader = _datastore.MileageAdjustment_Fetch(entity.CarHistoryId))
            {
                ISerializer<MileageAdjustment> serializer = Resolve<ISerializer<MileageAdjustment>>();

                while (reader.Read())
                {
                    MileageAdjustment adjustment = serializer.Deserialize((IDataRecord)reader);
                    Value value = Fetch_Value(entity.CarHistoryId, adjustment.ValueId);

                    entity.Car.MileageAdjustments.Add(Mapper.Map(adjustment, value));
                }
            }            
        }

        /// <summary>
        /// Fetch option adjustments.
        /// </summary>
        /// <param name="entity">Entity to be populated with option adjustments, and also that has the car history
        /// identifier to search against.</param>
        protected void Fetch_Options(CarEntity entity)
        {            
            using (IDataReader reader = _datastore.Options_Fetch(entity.CarHistoryId))
            {
                ISerializer<Option> serializer = Resolve<ISerializer<Option>>();

                while (reader.Read())
                {
                    Option option = serializer.Deserialize((IDataRecord)reader);
                    ResidualValue value = Fetch_ResidualValue(entity.CarHistoryId, option.ResidualValueId);
             
                    entity.Car.Options.Add(Mapper.Map(option, value));
                }
            }            
        }

        /// <summary>
        /// Fetch the uvcs tied to the given vin.
        /// </summary>
        /// <param name="vin">Vin.</param>
        /// <returns>List of uvcs.</returns>
        public IList<string> Fetch_Uvc(string vin)
        {
            IList<string> uvcs = new List<string>();

            using (IDataReader reader = _datastore.Uvc_Fetch(vin))
            {
                while (reader.Read())
                {
                    uvcs.Add(reader.GetString(reader.GetOrdinal("Uvc")));
                }
            }

            return uvcs;
        }

        #endregion

        #region Save Methods

        /// <summary>
        /// Save a car.
        /// </summary>        
        /// <param name="car">Car to save.</param>
        /// <param name="state">Two letter US state identifier.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>Car entity that either was newly saved or already existed.</returns>        
        public CarEntity Save(Model.Car car, string state, int dataLoadId)
        {            
            CarEntity entity = Fetch_History(car.Uvc, car.Vin, state, DateTime.Now);            
            if (entity == null)
            {
                entity = new CarEntity
                {
                    AuditRow = null,
                    Car      = car,                    
                    CarId    = -1,
                    HashCode = car.Hash()
                };
            }            
            // If the car already exists and hasn't changed, just kick it straight back.
            else if (entity.HashCode == car.Hash())
            {
                entity.Car = car;
                return entity;
            }
            // Otherwise, we've got a new version of an existing car.
            else
            {
                entity.Car = car;
                entity.HashCode = car.Hash();
            }
                       
            Save_Audit(entity);           
            Save_Car(entity, state);            
            Save_History(entity, dataLoadId);
            Save_Description(entity, dataLoadId);
            Save_Attributes(entity, dataLoadId);
            Save_Prices(entity, dataLoadId);
            Save_MileageAdjustments(entity, dataLoadId);
            Save_Options(entity, dataLoadId);

            return entity;
        }

        /// <summary>
        /// Save audit information for the given car. If the entity had no previous audit row, then just create a new
        /// audit row. If the entity did have one, then update that previous audit row as being no longer valid, and
        /// create a new audit row.
        /// </summary>        
        /// <param name="entity">Entity information about the car being saved.</param>
        protected void Save_Audit(CarEntity entity)
        {
            try
            {
                entity.AuditRow = entity.AuditRow == null
                                      ? Resolve<IRepository>().Insert(true, false, false)
                                      : Resolve<IRepository>().Update(entity.AuditRow, false, true, false);
            }
            catch (Exception)
            {
                // The row has already been written.  Ignore the exception.
            }
        }

        /// <summary>
        /// Save a root car record, if one doesn't already exist. Will set the car identifier of the given entity 
        /// (again, if it doesn't already exist).
        /// </summary>
        /// <param name="entity">Car entity to save.</param>
        /// <param name="state">Two letter US state identifier.</param>
        protected void Save_Car(CarEntity entity, string state)
        {
            try
            {
                if (entity.CarId == -1)
                {
                    using (IDataReader reader = _datastore.Car_Insert(entity.Car.Uvc, entity.Car.Vin, state))
                    {
                        if (!reader.Read())
                        {
                            throw new DataException("Expected car results.");
                        }
                        entity.CarId = reader.GetInt32(reader.GetOrdinal("CarID"));
                    }
                }
            }
            catch (Exception)
            {
                // The row has already been written.  Ignore the exception.
            }
        }

        /// <summary>
        /// Save a historical record of a car. Will set the car history identifier in the given entity.
        /// </summary>
        /// <param name="entity">Car values to save, and that has the car identifier to save an historical record 
        /// against.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>        
        protected void Save_History(CarEntity entity, int dataLoadId)
        {
            try
            {
                using (IDataReader reader = _datastore.Car_History_Insert(entity, dataLoadId))
                {
                    if (!reader.Read())
                    {
                        throw new DataException("Expected car history results.");
                    }
                    entity.CarHistoryId = reader.GetInt32(reader.GetOrdinal("CarHistoryID"));
                }
            }
            catch (Exception)
            {
                // The row has already been written.  Ignore the exception.
            }
        }

        /// <summary>
        /// Save car description values.
        /// </summary>
        /// <param name="entity">Car entity with the values to save, as well as the car history identifier to save
        /// against.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        protected void Save_Description(CarEntity entity, int dataLoadId)
        {
            try
            {
                _datastore.Car_Description_Insert(Mapper.Map(entity.CarHistoryId, entity.Car as Model.CarInfo), dataLoadId);
            }
            catch (Exception)
            {
                // The row has already been written.  Ignore the exception.
            }
        }

        /// <summary>
        /// Save car attribute values.
        /// </summary>
        /// <param name="entity">Car entity with the values to save, as well as the car history identifier to save
        /// against.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        protected void Save_Attributes(CarEntity entity, int dataLoadId)
        {
            try
            {
                _datastore.Car_Attributes_Insert(Mapper.Map(entity.CarHistoryId, entity.Car), dataLoadId);
            }
            catch (Exception)
            {
                // The row has already been written.  Ignore the exception.
            }
        }

        /// <summary>
        /// Save car prices.
        /// </summary>
        /// <param name="entity">Car entity with the values to save, as well as the car history identifier to save
        /// against.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        protected void Save_Prices(CarEntity entity, int dataLoadId)
        {
            try
            {
                Prices prices = new Prices();

                prices.ResidualValueId  = Save_ResidualValue(entity.CarHistoryId, entity.Car.ResidualValue, dataLoadId);
                prices.RetailValueId    = Save_Value(entity.CarHistoryId, entity.Car.RetailValue, dataLoadId);
                prices.TradeInValueId   = Save_Value(entity.CarHistoryId, entity.Car.TradeInValue, dataLoadId);
                prices.WholesaleValueId = Save_Value(entity.CarHistoryId, entity.Car.WholesaleValue, dataLoadId);
                     
                prices.FinanceAdvance   = entity.Car.FinanceAdvance;
                prices.Msrp             = entity.Car.Msrp;
                prices.Mileage          = entity.Car.Mileage;
                prices.CarHistoryId     = entity.CarHistoryId;
                prices.PriceIncludes    = entity.Car.PriceIncludes;

                _datastore.Car_Prices_Insert(prices, dataLoadId);
            }
            catch (Exception)
            {
                // The row has already been written.  Ignore the exception.
            }
        }

        /// <summary>
        /// Save a price value for the historical car record with the given identifier.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <param name="value">Value to save.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>Value identifier.</returns>        
        protected int Save_Value(int carHistoryId, Model.Value value, int dataLoadId)
        {
            using (IDataReader reader = _datastore.Value_Insert(Mapper.Map(carHistoryId, value), dataLoadId))
            {
                if (!reader.Read())
                {
                    throw new DataException("Expected retail value results.");
                }
                return reader.GetInt32(reader.GetOrdinal("ValueID"));
            }
        }

        /// <summary>
        /// Save a residual value for the historical car record with the given identifier.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <param name="residualValue">Residual value to save.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>Residual value identifier.</returns>
        protected int Save_ResidualValue(int carHistoryId, Model.ResidualValue residualValue, int dataLoadId)
        {
            using (IDataReader reader = _datastore.ResidualValue_Insert(Mapper.Map(carHistoryId, residualValue), dataLoadId))
            {
                if (!reader.Read())
                {
                    throw new DataException("Expected residual value results.");
                }
                return reader.GetInt32(reader.GetOrdinal("ResidualValueID"));
            }
        }        

        /// <summary>
        /// Save a car's mileage adjustments.
        /// </summary>
        /// <param name="entity">Car entity with the values to save, as well as the car history identifier to save
        /// against.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        protected void Save_MileageAdjustments(CarEntity entity, int dataLoadId)
        {
            Model.Value value = new Model.Value();
            
            foreach (Model.MileageAdjustment mileageAdjustment in entity.Car.MileageAdjustments)
            {
                value.Average    = mileageAdjustment.Average;
                value.Clean      = mileageAdjustment.Clean;
                value.ExtraClean = mileageAdjustment.ExtraClean;
                value.Rough      = mileageAdjustment.Rough;

                int valueId = Save_Value(entity.CarHistoryId, value, dataLoadId);
               
                _datastore.MileageAdjustment_Insert(Mapper.Map(entity.CarHistoryId, valueId, mileageAdjustment), dataLoadId);
            }
        }

        /// <summary>
        /// Save a car's options.
        /// </summary>
        /// <param name="entity">Car entity with the values to save, as well as the car history identifier to save
        /// against.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        protected void Save_Options(CarEntity entity, int dataLoadId)
        {
            foreach (Model.Option option in entity.Car.Options)
            {
                int residualValueId = Save_ResidualValue(entity.CarHistoryId, option.Residual, dataLoadId);                
                _datastore.Options_Insert(Mapper.Map(entity.CarHistoryId, residualValueId, option), dataLoadId);
            }
        }

        #endregion
    }
}
