﻿using System;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Datastores;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Gateways
{
    /// <summary>
    /// Price matrix data gateway.
    /// </summary>
    public class MatrixGateway : GatewayBase
    {
        #region Properties

        /// <summary>
        /// Initialize the price matrix datastore.
        /// </summary>
        public MatrixGateway()
        {
            _datastore = Resolve<IMatrixDatastore>();
        }

        /// <summary>
        /// Price matrix datastore.
        /// </summary>
        private readonly IMatrixDatastore _datastore;

        #endregion

        /// <summary>
        /// Fetch a price matrix.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <returns>Price matrix.</returns>
        public Matrix Fetch(int carHistoryId, int vehicleConfigurationHistoryId)
        {
            bool found = false;
            Matrix matrix = new Matrix();

            using (IDataReader reader = _datastore.Matrix_Fetch(carHistoryId, vehicleConfigurationHistoryId))
            {
                while (reader.Read())
                {
                    found = true;

                    Market    market    = (Market)reader.GetByte(reader.GetOrdinal("MarketID"));
                    Condition condition = (Condition)reader.GetByte(reader.GetOrdinal("ConditionID"));
                    Valuation valuation = (Valuation)reader.GetByte(reader.GetOrdinal("ValuationID"));

                    Matrix.Cell cell = matrix[market, condition, valuation];
                    cell.Value       = DataRecord.GetNullableInt32(reader, "Value");
                    bool visibility  = reader.GetBoolean(reader.GetOrdinal("Visible"));                                        
                    cell.Visibility  = visibility ? Matrix.CellVisibility.Value : Matrix.CellVisibility.NotDisplayed;
                }
            }

            return found ? matrix : null;
        }

        /// <summary>
        /// Save a price matrix for a vehicle and configuration.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <param name="matrix">Price matrix to save.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>The price matrix that was saved.</returns>
        public Matrix Save(int carHistoryId, int vehicleConfigurationHistoryId, Matrix matrix, int dataLoadId)
        {
            // If there already is a matrix for this configuration, do nothing.
            if (Fetch(carHistoryId, vehicleConfigurationHistoryId) != null)
            {
                return matrix;
            }

            int matrixId;

            using (IDataReader reader = _datastore.Matrix_Insert(carHistoryId, vehicleConfigurationHistoryId, dataLoadId))
            {
                if (!reader.Read())
                {
                    throw new DataException("Expected price matrix identifier.");
                }
                matrixId = reader.GetInt32(reader.GetOrdinal("MatrixID"));
            }

            Matrix.Cell cell;

            foreach (Market market in Enum.GetValues(typeof(Market)))
            {
                if (market == Market.Undefined)
                {
                    continue;
                }

                foreach (Condition condition in Enum.GetValues(typeof(Condition)))
                {            
                    if (condition == Condition.Undefined)
                    {
                        continue;
                    }

                    foreach (Valuation valuation in Enum.GetValues(typeof(Valuation)))
                    {
                        if (valuation == Valuation.Undefined)
                        {
                            continue;
                        }

                        cell = matrix[market, condition, valuation];
                        
                        bool visible = cell.Visibility == Matrix.CellVisibility.Value;

                        _datastore.MatrixCell_Insert(matrixId, (int)market, (int)condition, (int)valuation, visible, cell.Value, dataLoadId);
                    }
                }
            }

            return matrix;
        }
    }
}
