﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Datastores;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Gateways
{
    /// <summary>
    /// Gateway for vehicle-to-configuration association operations.
    /// </summary>
    public class ClientVehicleGateway : GatewayBase
    {
        /// <summary>
        /// Fetch the BlackBook configuration for the vehicle with the given identifier.
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <returns></returns>
        public ClientVehicle Fetch(int vehicleId)
        {
            string key = CreateCacheKey(vehicleId);

            ClientVehicle vehicle = Cache.Get(key) as ClientVehicle;

            if (vehicle == null)
            {
                using (IDataReader reader = Resolve<IClientDatastore>().Vehicle_BlackBook_Fetch(vehicleId))
                {
                    if (reader.Read())
                    {
                        vehicle = Resolve<ISerializer<ClientVehicle>>().Deserialize((IDataRecord)reader);
                        Remember(key, vehicle);
                    }
                }
            }
            return vehicle;
        }       

        /// <summary>
        /// Insert an association between the vehicle and BlackBook configuration with the given identifiers.
        /// </summary>
        /// <param name="vehicleId">Vehicle identifier.</param>
        /// <param name="vehicleConfigurationId">Vehicle configuration identifier.</param>
        public void Insert(int vehicleId, int vehicleConfigurationId)
        {            
            Resolve<IClientDatastore>().Vehicle_BlackBook_Insert(vehicleId, vehicleConfigurationId);
        }
    }
}
