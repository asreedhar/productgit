﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Entities;
using FirstLook.Client.DomainModel.Clients.Repositories.Gateways;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Datastores;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities;
using IRepository=FirstLook.Client.DomainModel.Clients.Model.Auditing.IRepository;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Gateways
{
    /// <summary>
    /// Vehicle configuration data gateway.
    /// </summary>
    public class VehicleConfigurationGateway : GatewayBase
    {
        #region Properties

        /// <summary>
        /// Initialize the vehicle configuration datastore.
        /// </summary>
        public VehicleConfigurationGateway()
        {
            _datastore = Resolve<IVehicleConfigurationDatastore>();
        }

        /// <summary>
        /// Vehicle configuration datastore.
        /// </summary>
        private readonly IVehicleConfigurationDatastore _datastore;

        #endregion

        #region Fetch Methods

        /// <summary>
        /// Fetch the list of identifying details of publication editions for the given client and vehicle.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Client-vehicle association.</param>
        /// <returns>List of identifying details of the configurations.</returns>
        public IList<IEdition<IPublicationInfo>> Fetch_List(IBroker broker, ClientVehicleIdentification vehicle)
        {
            using (IDataReader reader = _datastore.VehicleConfigurations_Fetch(broker.Id, vehicle.Id))
            {
                IList<IEdition<IPublicationInfo>> publications = new List<IEdition<IPublicationInfo>>();

                while (reader.Read())
                {
                    publications.Add(Resolve<ISerializer<IEdition<IPublicationInfo>>>().Deserialize((IDataRecord)reader));
                }

                return publications;
            }
        }        

        /// <summary>
        /// Fetch the vehicle configuration with the given identifiers.
        /// </summary>        
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Client-vehicle association.</param>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>        
        /// <returns>Vehicle configuration entity, or null if one doesn't exist.</returns>
        public VehicleConfigurationEntity Fetch(IBroker broker, ClientVehicleIdentification vehicle, int vehicleConfigurationHistoryId)
        {
            VehicleConfigurationEntity entity = Fetch_History(broker.Id, vehicle.Id, vehicleConfigurationHistoryId);
            if (entity == null)
            {
                return null;
            }

            Fetch_OptionActions(entity);
            Fetch_OptionStates(entity);

            return entity;
        }

        /// <summary>
        /// Fetch the vehicle configuration for the client and vehicle that was valid on the given date.
        /// </summary>        
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Client-vehicle association.</param>
        /// <param name="on">Date on which the configuration was valid.</param>        
        /// <returns>Vehicle configuration entity, or null if one doesn't exist.</returns>
        public VehicleConfigurationEntity Fetch(IBroker broker, ClientVehicleIdentification vehicle, DateTime on)
        {
            VehicleConfigurationEntity entity = Fetch_History(broker.Id, vehicle.Id, on);
            if (entity == null)
            {
                return null;
            }

            Fetch_OptionActions(entity);
            Fetch_OptionStates(entity);

            return entity;
        }

        /// <summary>
        /// Fetch the vehicle configuration for the vehicle defined by the given values.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin. Optional.</param>
        /// <param name="state">Two letter US state identifier.</param>
        /// <param name="mileage">Mileage.</param>
        /// <returns>Vehicle configuration entity, or null if one doesn't exist.</returns>
        public VehicleConfigurationEntity Fetch(string uvc, string vin, string state, int? mileage)
        {
            VehicleConfigurationEntity entity = Fetch_History(uvc, vin, state, mileage);
            if (entity == null)
            {
                return null;
            }

            Fetch_OptionActions(entity);
            Fetch_OptionStates(entity);

            return entity;
        }        

        /// <summary>
        /// Fetch the identifier of the vehicle configuration that has been tied to a client and vehicle with the given
        /// identifier. -1 will be returned if no configuration exists.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="clientVehicleId">Client-vehicle association identifier.</param>        
        /// <returns>Vehicle configuration identifier, or -1 if one does not exist or if no vin is provided.</returns>
        protected int Fetch_Configuration(int clientId, int clientVehicleId)
        {
            using (IDataReader reader = _datastore.VehicleConfiguration_Fetch(clientId, clientVehicleId))
            {
                if (reader.Read())
                {
                    return reader.GetInt32(reader.GetOrdinal("VehicleConfigurationID"));
                }
                return -1;
            }
        }

        /// <summary>
        /// Fetch an historical record of a vehicle configuration.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin. Optional.</param>
        /// <param name="stateCode">Two letter US state identifier.</param>
        /// <param name="mileage">Mileage. Optional.</param>
        /// <returns>Vehicle configuration entity if one exists for the given values. Null otherwise.</returns>
        protected VehicleConfigurationEntity Fetch_History(string uvc, string vin, string stateCode, int? mileage)
        {
            using (IDataReader reader = _datastore.VehicleConfiguration_History_Fetch(uvc, vin, stateCode, mileage))
            {
                if (reader.Read())
                {
                    return Resolve<ISerializer<VehicleConfigurationEntity>>().Deserialize((IDataRecord) reader);
                }

                return null;
            }
        }

        /// <summary>
        /// Fetch a vehicle configuration historical record for the given identifiers.
        /// </summary>        
        /// <param name="clientId">Client identifier.</param>
        /// <param name="clientVehicleId">Client-vehicle association identifiers.</param>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>        
        /// <returns>Vehicle configuration entity.</returns>
        protected VehicleConfigurationEntity Fetch_History(int clientId, int clientVehicleId, int vehicleConfigurationHistoryId)
        {
            using (IDataReader reader = _datastore.VehicleConfiguration_History_Fetch(clientId, clientVehicleId, vehicleConfigurationHistoryId))
            {
                if (reader.Read())
                {
                    return Resolve<ISerializer<VehicleConfigurationEntity>>().Deserialize((IDataRecord)reader);
                }

                return null;
            }
        }        

        /// <summary>
        /// Fetch a vehicle configuration historical record for the given client and vehicle that was valid on the
        /// given date.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="clientVehicleId">Client-vehicle association identifier.</param>
        /// <param name="on">Date on which the configuration was valid.</param>
        /// <returns>Vehicle configuration entity.</returns>
        protected VehicleConfigurationEntity Fetch_History(int clientId, int clientVehicleId, DateTime on)
        {
            using (IDataReader reader = _datastore.VehicleConfiguration_History_Fetch(clientId, clientVehicleId, on))
            {
                if (reader.Read())
                {
                    return Resolve<ISerializer<VehicleConfigurationEntity>>().Deserialize((IDataRecord)reader);
                }

                return null;
            }
        }

        /// <summary>
        /// Fetch a vehicle configuration's option adjustments.
        /// </summary>
        /// <param name="entity">Configuration entity to populate with option actions, and that also has the 
        /// identifiers to search against.</param>
        protected void Fetch_OptionActions(VehicleConfigurationEntity entity)
        {
            using (IDataReader reader = _datastore.OptionActions_Fetch(entity.VehicleConfigurationHistoryId))
            {                
                if (!reader.Read())
                {
                    throw new DataException("Expected result for vehicle configuration option action count.");
                }

                // Read the result count to help with preserving indices.
                int actionCount = reader.GetInt32(reader.GetOrdinal("Count"));
                IOptionAction[] actions = new IOptionAction[actionCount];
                
                if (!reader.NextResult())
                {
                    throw new DataException("Expected result for vehicle configuration option actions.");
                }

                // Read the option actions.
                while (reader.Read())
                {
                    int index      = reader.GetByte(reader.GetOrdinal("OptionActionIndex"));
                    actions[index] = new OptionAction
                                     {
                                        ActionType = (OptionActionType)reader.GetByte(reader.GetOrdinal("OptionActionTypeID")),
                                        Uoc        = reader.GetString(reader.GetOrdinal("Uoc"))
                                     };
                }

                entity.VehicleConfiguration.OptionActions.Clear();
                foreach (IOptionAction action in actions)
                {
                    entity.VehicleConfiguration.OptionActions.Add(action);
                }                
            }
        }

        /// <summary>
        /// Fetch a vehicle configuration's option states.
        /// </summary>
        /// <param name="entity">Configuration entity to populate with option states, and that also has the 
        /// identifiers to search against.</param>
        protected void Fetch_OptionStates(VehicleConfigurationEntity entity)
        {            
            using (IDataReader reader = _datastore.OptionStates_Fetch(entity.VehicleConfigurationHistoryId))
            {
                entity.VehicleConfiguration.OptionStates.Clear();

                while (reader.Read())
                {
                    entity.VehicleConfiguration.OptionStates.Add(
                        new OptionState
                        {
                            Enabled  = reader.GetBoolean(reader.GetOrdinal("Enabled")),
                            Selected = reader.GetBoolean(reader.GetOrdinal("Selected")),
                            Uoc      = reader.GetString(reader.GetOrdinal("Uoc"))
                        });
                }
            }            
        }

        #endregion

        #region Save Methods

        /// <summary>
        /// Save a vehicle configuration.
        /// </summary>                
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Details on the association between the client and the vehicle.</param>
        /// <param name="configuration">Vehicle configuration to save.</param>
        /// <param name="agent">Who is responsible for this configuration?</param>        
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>Vehicle configuration entity.</returns>
        public VehicleConfigurationEntity Save(IBroker broker, ClientVehicleIdentification vehicle, IVehicleConfiguration configuration, 
                                               ChangeAgent agent, int dataLoadId)
        {
            string state = configuration.State != null ? configuration.State.Code : string.Empty;

            VehicleConfigurationEntity entity = Fetch(broker, vehicle, DateTime.Now);

            // If no historical configuration record exists, create a new one.
            if (entity == null)
            {
                // Find the car.
                int carHistoryId;
                using (IDataReader reader = Resolve<ICarDatastore>().Car_History_Fetch(configuration.Uvc, configuration.Vin, state, DateTime.Now))
                {
                    if (!reader.Read())
                    {
                        throw new DataException("Expected car history identifier.");
                    }
                    carHistoryId = reader.GetInt32(reader.GetOrdinal("CarHistoryID"));
                }

                // Find if there's a configuration for the given client and vehicle. Will get -1 if one doesn't.
                int vehicleConfigurationId = Fetch_Configuration(broker.Id, vehicle.Id);

                // Create the new entity.
                entity = new VehicleConfigurationEntity
                {
                    AuditRow                      = null,
                    CarHistoryId                  = carHistoryId,
                    VehicleConfiguration          = configuration,
                    VehicleConfigurationHistoryId = -1,
                    VehicleConfigurationId        = vehicleConfigurationId,
                    ChangeAgent                   = agent,
                    ChangeType                    = ChangeType.None
                };
            }
            // An historical record does exist.
            else
            {
                // If nothing has changed between the existing configuration and the new one, just return the new one.
                ChangeType changeType = configuration.Compare(entity.VehicleConfiguration);
                if (changeType == ChangeType.None)
                {
                    return entity;
                }

                // Otherwise, set the new configuration.
                entity.VehicleConfiguration = configuration;
                entity.ChangeAgent = agent;
                entity.ChangeType = changeType;
            }

            Save_Audit(entity);
            Save_Configuration(entity, vehicle);
            Save_History(entity, dataLoadId);
            Save_OptionActions(entity.VehicleConfigurationHistoryId, configuration.OptionActions, dataLoadId);
            Save_OptionStates(entity.VehicleConfigurationHistoryId, configuration.OptionStates, dataLoadId);

            // Get the user. The user isn't necessarily of type 'System', but the way the code is that's the quickest
            // way to just get the UserID without going through associated 'Member' tables.
            Audit audit = new AuditGateway().Fetch(entity.AuditRow.Id);
            entity.User = new UserGateway().Fetch(UserType.System, audit.User); 

            return entity;
        }

        /// <summary>
        /// Save audit information for the given vehicle configuration. If the entity had no previous audit row, then 
        /// just create a new audit row. If the entity did have one, then update that previous audit row as being no 
        /// longer valid, and create a new audit row.
        /// </summary>        
        /// <param name="entity">Entity information about the vehicle configuration being saved.</param>
        protected void Save_Audit(VehicleConfigurationEntity entity)
        {            
            entity.AuditRow = entity.AuditRow == null
                                  ? Resolve<IRepository>().Insert(true, false, false)
                                  : Resolve<IRepository>().Update(entity.AuditRow, false, true, false);            
        }

        /// <summary>
        /// If a vehicle configuration does not exist for a given vehicle, then create it. Only saves a vehicle 
        /// identifier if a vin is provided.
        /// </summary>        
        /// <param name="entity">Vehicle configuration entity to save. This entity will also be populated with the 
        /// vehicle configuration identifier if a vin is provided.</param>
        /// <param name="vehicle">Client-vehicle association identifier.</param>
        protected void Save_Configuration(VehicleConfigurationEntity entity, ClientVehicleIdentification vehicle)
        {            
            if (entity.VehicleConfigurationId == -1)
            {                                                    
                using (IDataReader reader = _datastore.VehicleConfiguration_Insert(vehicle.Vehicle.Id))
                {
                    if (!reader.Read())
                    {
                        throw new DataException("Expected vehicle configuration identifier.");
                    }
                    entity.VehicleConfigurationId = reader.GetInt32(reader.GetOrdinal("VehicleConfigurationID"));
                }

                // Save the configuration - vehicle association.
                new ClientVehicleGateway().Insert(vehicle.Id, entity.VehicleConfigurationId);
            }            
        }

        /// <summary>
        /// Save an historical record of a vehicle configuration.
        /// </summary>                
        /// <param name="entity">vehicle configuration entity to save a historical record for. This entity will also be 
        /// populated with the identifier of the historical record.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        protected void Save_History(VehicleConfigurationEntity entity, int dataLoadId)
        {
            using (IDataReader reader = _datastore.VehicleConfiguration_History_Insert(entity, dataLoadId))
            {
                if (!reader.Read())
                {
                    throw new DataException("Expected vehicle configuration history results.");
                }

                entity.VehicleConfigurationHistoryId = reader.GetInt32(reader.GetOrdinal("VehicleConfigurationHistoryID"));
            }
        }

        /// <summary>
        /// Save a vehicle configuration's option actions.
        /// </summary>        
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>        
        /// <param name="actions">Option actions to save.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        protected void Save_OptionActions(int vehicleConfigurationHistoryId, IList<IOptionAction> actions, int dataLoadId)
        {
            for (int i = 0; i < actions.Count; i++)
            {                
                _datastore.OptionAction_Insert(vehicleConfigurationHistoryId, actions[i], i, dataLoadId);
            }
        }

        /// <summary>
        /// Save a vehicle configuration's option states.
        /// </summary>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <param name="optionStates">Option states to save.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        protected void Save_OptionStates(int vehicleConfigurationHistoryId, IList<IOptionState> optionStates, int dataLoadId)
        {
            foreach (IOptionState optionState in optionStates)
            {
                _datastore.OptionState_Insert(vehicleConfigurationHistoryId, optionState, dataLoadId);
            }
        }

        #endregion
    }
}
