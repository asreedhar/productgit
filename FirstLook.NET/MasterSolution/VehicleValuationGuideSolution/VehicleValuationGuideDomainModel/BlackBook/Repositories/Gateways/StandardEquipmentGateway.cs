﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Utility;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Datastores;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities;
using IRepository=FirstLook.Client.DomainModel.Clients.Model.Auditing.IRepository;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Gateways
{
    /// <summary>
    /// Standard equipment data gateway.
    /// </summary>
    public class StandardEquipmentGateway : AuditingGateway
    {
        #region Properties

        /// <summary>
        /// Initialize the datastore.
        /// </summary>
        public StandardEquipmentGateway()
        {
            _datastore = Resolve<IStandardEquipmentDatastore>();
        }

        /// <summary>
        /// Standard equipment datastore.
        /// </summary>
        private readonly IStandardEquipmentDatastore _datastore;

        #endregion        

        #region Cache Policy

        /// <summary>
        /// Is the cache for the standard equipment for the car with the given uvc current and valid?
        /// </summary>
        /// <param name="uvc">Uvc.</param>        
        /// <returns>True if the cache is current and valid, false otherwise.</returns>
        public bool Cache_IsValid(string uvc)
        {
            using (IDataReader reader = _datastore.CachePolicy_IsValid(uvc))
            {
                if (!reader.Read())
                {
                    throw new DataException("Expected validity check on standard equipment cache.");
                }

                return reader.GetInt32(reader.GetOrdinal("IsValid")) == 1;
            }
        }

        /// <summary>
        /// Extend the validitiy of the cache for the standard equipment for the car with the given uvc. Length that 
        /// the cache window is extended is determined by the database table Vehicle.BlackBook.CachePolicy.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <returns>True, just to appease function signatures of callers.</returns>
        public bool Cache_Extend(string uvc)
        {
            StandardEquipmentEntity entity = Fetch_History(uvc);
            if (entity == null)
            {
                throw new DataException("No standard equipment exists for the given uvc.");
            }

            _datastore.CachePolicy_Upsert(entity.Id);
            return true;
        }

        /// <summary>
        /// Extend the validitiy of the cache for the standard equipment with the given identifier. Length that the 
        /// cache window is extended is determined by the database table Vehicle.BlackBook.CachePolicy.
        /// </summary>
        /// <param name="standardEquipmentId">Standard equipment identifier.</param>
        /// <returns>True, just to appease function signatures of callers.</returns>
        public bool Cache_Extend(int standardEquipmentId)
        {
            _datastore.CachePolicy_Upsert(standardEquipmentId);
            return true;
        }

        #endregion

        #region Fetch Methods

        /// <summary>
        /// Fetch the standard equipment for the vehicle with the given uvc.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <returns>Standard equipment.</returns>
        public StandardEquipmentEntity Fetch(string uvc)
        {
            StandardEquipmentEntity entity = Fetch_History(uvc);
            if (entity == null)
            {
                return null;
            }

            Dictionary<string, bool> categories = new Dictionary<string,bool>();

            using (IDataReader reader = _datastore.StandardEquipmentRecords_Fetch(entity.HistoryId))
            {
                while (reader.Read())
                {
                    string category    = reader.GetString(reader.GetOrdinal("Category"));
                    string subCategory = reader.GetString(reader.GetOrdinal("SubCategory"));
                    string value       = reader.GetString(reader.GetOrdinal("Value"));
                    
                    categories[category] = true;
                    entity.StandardEquipment.StandardEquipmentRecords.Add(new StandardEquipmentRecord
                                                                              {
                                                                                  Category    = category,
                                                                                  Subcategory = subCategory,
                                                                                  Value       = value                                                                                  
                                                                              });
                }

                foreach (string key in categories.Keys)
                {
                    entity.StandardEquipment.Categories.Add(key);
                }                
            }

            return entity;
        }

        /// <summary>
        /// Fetch the most recent historical record for the car with the given uvc's standard equipment. Does not 
        /// fetch all the standard equipment - just its identifiers and such.
        /// </summary>
        /// <param name="uvc">Uvc</param>
        /// <returns>Standard equipment entity, or null if one doesn't exist.</returns>
        protected StandardEquipmentEntity Fetch_History(string uvc)
        {
            using (IDataReader reader = _datastore.StandardEquipment_History_Fetch(uvc))
            {
                if (reader.Read())
                {
                    return new StandardEquipmentEntity
                    {
                        Id        = reader.GetInt32(reader.GetOrdinal("StandardEquipmentID")),
                        HistoryId = reader.GetInt32(reader.GetOrdinal("StandardEquipmentHistoryID")),
                        Uvc       = reader.GetString(reader.GetOrdinal("Uvc")),
                        AuditRow  = Resolve<ISerializer<AuditRow>>().Deserialize((IDataRecord)reader),
                        HashCode  = reader.GetInt64(reader.GetOrdinal("StandardEquipmentHashCode"))
                    };                    
                }
            }
            return null;
        }

        /// <summary>
        /// Fetch the identifier of the category with the given name, if it exists.
        /// </summary>
        /// <param name="category">Category name.</param>
        /// <returns>Category identifier, or -1 if it doesn't yet exist.</returns>
        protected int Fetch_Category(string category)
        {
            using (IDataReader reader = _datastore.Category_Fetch(category))
            {
                if (!reader.Read())
                {
                    return -1;
                }
                return reader.GetInt32(reader.GetOrdinal("CategoryID"));
            }
        }

        /// <summary>
        /// Fetch the identifier of the subcategory with the given name in the category with the given identifier, if
        /// it exists.
        /// </summary>
        /// <param name="categoryId">Category identifier.</param>
        /// <param name="subCategory">Subcategory name.</param>
        /// <returns>Subcategory identifier, or -1 if it doesn't yet exist.</returns>
        protected int Fetch_SubCategory(int categoryId, string subCategory)
        {
            using (IDataReader reader = _datastore.SubCategory_Fetch(categoryId, subCategory))
            {
                if (!reader.Read())
                {
                    return -1;
                }
                return reader.GetInt32(reader.GetOrdinal("SubCategoryID"));
            }
        }

        #endregion

        #region Save Methods

        /// <summary>
        /// Save standard equipment for the vehicle with the given uvc.
        /// </summary>        
        /// <param name="uvc">Uvc.</param>
        /// <param name="equipment">Standard equipment.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>Standard equipment entity.</returns>
        public StandardEquipmentEntity Save(string uvc, StandardEquipment equipment, int dataLoadId)
        {            
            StandardEquipmentEntity entity = Fetch_History(uvc);            

            if (entity == null)
            {
                entity = new StandardEquipmentEntity
                {
                    AuditRow          = null,
                    HistoryId         = -1,
                    Id                = -1,
                    Uvc               = uvc,
                    HashCode          = equipment.Hash(),
                    StandardEquipment = equipment
                };
            }
            // If the equipment already exists with the same values, then return that.
            else if (entity.HashCode == equipment.Hash())
            {
                entity.StandardEquipment = equipment;   
            }            

            Save_Audit(entity);
            Save_Equipment(entity);
            Save_History(entity, dataLoadId);
            Save_Records(entity, dataLoadId);

            return entity;
        }  

        /// <summary>
        /// Save audit information for the given standard equipment. If the entity had no previous audit row, then just 
        /// create a new audit row. If the entity did have one, then update that previous audit row as being no longer 
        /// valid, and create a new audit row.
        /// </summary>        
        /// <param name="entity">Entity information about the standard equipment being saved.</param>
        protected void Save_Audit(StandardEquipmentEntity entity)
        {
            entity.AuditRow = entity.AuditRow == null
                                  ? Resolve<IRepository>().Insert(true, false, false)
                                  : Resolve<IRepository>().Update(entity.AuditRow, false, true, false);
        }

        /// <summary>
        /// Save a standard equipment, if it doesn't yet exist.
        /// </summary>
        /// <param name="entity">Standard equipment to save.</param>
        protected void Save_Equipment(StandardEquipmentEntity entity)
        {            
            if (entity.Id == -1)
            {
                using (IDataReader reader = _datastore.StandardEquipment_Insert(entity.Uvc))
                {
                    if (!reader.Read())
                    {
                        throw new DataException("Expected standard equipment identifier.");
                    }
                    entity.Id = reader.GetInt32(reader.GetOrdinal("StandardEquipmentID"));
                }
            }
        }

        /// <summary>
        /// Save a historical record of standard equipment.
        /// </summary>
        /// <param name="entity">Standard equipment values to save, and that has the identifier to save an historical 
        /// record against.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        protected void Save_History(StandardEquipmentEntity entity, int dataLoadId)
        {
            using (IDataReader reader = _datastore.StandardEquipment_History_Insert(entity, dataLoadId))
            {
                if (!reader.Read())
                {
                    throw new DataException("Expected standard equipment history identifier.");
                }
                entity.HistoryId = reader.GetInt32(reader.GetOrdinal("StandardEquipmentHistoryID"));
            }
        }

        /// <summary>
        /// Save standard equipment records.
        /// </summary>
        /// <param name="entity">Standard equipment entity with records to save, and that has the historical identifier
        /// to save values against.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        protected void Save_Records(StandardEquipmentEntity entity, int dataLoadId)
        {            
            foreach (StandardEquipmentRecord record in entity.StandardEquipment.StandardEquipmentRecords)
            {                
                int categoryId    = Save_Category(record.Category);
                int subCategoryId = Save_SubCategory(categoryId, record.Subcategory);
                                
                _datastore.StandardEquipmentRecord_Insert(entity.HistoryId, categoryId, subCategoryId, record.Value, dataLoadId);
            }   
        }

        /// <summary>
        /// Save a standard equipment category, if it doesn't already exist.
        /// </summary>
        /// <param name="category">Category to save.</param>
        /// <returns>Category identifier.</returns>
        protected int Save_Category(string category)
        {
            int categoryId = Fetch_Category(category);            

            if (categoryId == -1)
            {
                using (IDataReader reader = _datastore.Category_Insert(category))
                {
                    if (!reader.Read())
                    {
                        throw new DataException("Expected category identifier.");
                    }
                    categoryId = reader.GetInt32(reader.GetOrdinal("CategoryID"));
                }
            }

            return categoryId;
        }

        /// <summary>
        /// Save a standard equipment subcategory, if it doesn't already exist.
        /// </summary>
        /// <param name="categoryId">Identifier of the category the subcategory belongs to.</param>
        /// <param name="subCategory">Subcategory to save.</param>
        /// <returns>Subcategory identifier.</returns>
        protected int Save_SubCategory(int categoryId, string subCategory)
        {
            int subCategoryId = Fetch_SubCategory(categoryId, subCategory);
            
            if (subCategoryId == -1)
            {
                using (IDataReader reader = _datastore.SubCategory_Insert(categoryId, subCategory))
                {
                    if (!reader.Read())
                    {
                        throw new DataException("Expected subcategory identifier.");
                    }
                    subCategoryId = reader.GetInt32(reader.GetOrdinal("SubCategoryID"));
                }
            }

            return subCategoryId;
        }              

        #endregion
    }
}
