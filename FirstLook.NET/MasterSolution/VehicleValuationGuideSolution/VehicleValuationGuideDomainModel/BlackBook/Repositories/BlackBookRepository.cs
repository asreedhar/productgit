using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Api;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Entities;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories.Gateways;
using IRepository=FirstLook.Client.DomainModel.Clients.Model.Auditing.IRepository;

namespace FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Repositories
{
    /// <summary>
    /// BlackBook repository. For data operations on car and standard equipment reference data, as well as 
    /// configurations and valuations.
    /// </summary>
    public class BlackBookRepository : RepositoryBase, IBlackBookRepository
    {
        #region Car Methods

        /// <summary>
        /// Load the car that has the given uvc and vin and is currently valid.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="state">Two letter US state identifier. Optional.</param>
        /// <returns>Car.</returns>
        public CarEntity Load_Car(string uvc, string vin, string state)
        {
            return DoInSession(() => new CarGateway().Fetch(uvc, vin, state ?? string.Empty, DateTime.Now));
        }

        /// <summary>
        /// Load the car that has the given uvc and vin that was valid at the given date.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="state">Two letter US state identifier. Optional.</param>
        /// <param name="date">Time period to get a car record for.</param>
        /// <returns>Car.</returns>
        public CarEntity Load_Car(string uvc, string vin, string state, DateTime date)
        {            
            return DoInSession(() => new CarGateway().Fetch(uvc, vin, state ?? string.Empty, date));
        }

        /// <summary>
        /// Load the car with the given uvc that is currently valid. If there are multiple vins in the database for 
        /// this uvc, it will just return the first.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="state">Two letter US state identifier. Optional.</param>
        /// <returns>Car.</returns>
        public CarEntity Load_Car(string uvc, string state)
        {            
            return DoInSession(() => new CarGateway().Fetch(uvc, null, state ?? string.Empty, DateTime.Now));
        }

        /// <summary>
        /// Load the car with the given uvc that was valid at the given date. If there are multiple vins in the 
        /// database for this uvc, it will just return the first.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="state">Two letter US state identifier. Optional.</param>
        /// <param name="date">Time period to get a car record for.</param>
        /// <returns>Car.</returns>
        public CarEntity Load_Car(string uvc, string state, DateTime date)
        {
            return DoInSession(() => new CarGateway().Fetch(uvc, null, state ?? string.Empty, date));
        }        

        /// <summary>
        /// Load the uvcs tied to the given vin.
        /// </summary>
        /// <param name="vin">Vin.</param>
        /// <returns>List of uvcs.</returns>
        public IList<string> Load_Uvc(string vin)
        {
            return DoInSession(() => new CarGateway().Fetch_Uvc(vin));
        }        

        /// <summary>
        /// Save a car.
        /// </summary>        
        /// <param name="car">Car to save.</param>
        /// <param name="state">Two letter US state identifier.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>Car entity that was either newly saved or already existed.</returns>
        public CarEntity Save_Car(Car car, string state, int dataLoadId)
        {
            return DoInTransaction
            (
                delegate(IDataSession session)
                {
                    if (session.Items["Audit"] as Audit == null)
                    {
                        IAuthority authority = AuthorityFactory.GetAuthority("SYSTEM");

                        IAccountManager manager = authority.AccountManager;

                        IUser user = manager.User("blackbook");

                        IPrincipal principal = manager.Principal(user);

                        session.Items["Audit"] = Resolve<IRepository>().Create(principal);
                    }
                },
                () => new CarGateway().Save(car, state ?? string.Empty, dataLoadId)
            );
        }

        /// <summary>
        /// Is the cache for the car with the given values valid/
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="state">Two letter US state identifier.</param>
        /// <returns>True if we're in the cache window, false otherwise.</returns>
        public bool CarCache_IsValid(string uvc, string vin, string state)
        {            
            return DoInSession(() => new CarGateway().Cache_IsValid(uvc, vin, state ?? string.Empty));
        }

        /// <summary>
        /// Is the cache valid for any car with the given uvc?
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="state">Two letter US state identifier. Optional.</param>
        /// <returns>True if we're in the cache window for any car with that uvc, false otherwise.</returns>
        public bool CarCache_IsValid(string uvc, string state)
        {
            return DoInSession(() => new CarGateway().Cache_IsValid(uvc, state ?? string.Empty));
        }

        /// <summary>
        /// Extend the cache window for the car with the given values.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="state">Two letter US state identifier.</param>
        /// <returns>Whether the operation succeeded or not.</returns>
        public bool CarCache_Extend(string uvc, string vin, string state)
        {            
            return DoInSession(() => new CarGateway().Cache_Extend(uvc, vin, state ?? string.Empty));
        }

        /// <summary>
        /// Extend the cache window for the car with the given identifier.
        /// </summary>
        /// <param name="carId">Car identifier.</param>
        /// <returns>Whether the operation succeeded or not.</returns>
        public bool CarCache_Extend(int carId)
        {            
            return DoInSession(() => new CarGateway().Cache_Extend(carId));
        }

        #endregion        

        #region Standard Equipment Methods

        /// <summary>
        /// Get the standard equipment for the vehicle with the given uvc.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <returns>Standard equipment entity.</returns>
        public StandardEquipmentEntity Load_Equipment(string uvc)
        {
            return DoInSession(() => new StandardEquipmentGateway().Fetch(uvc));
        }

        /// <summary>
        /// Save standard equipment for the vehicle with the given uvc.
        /// </summary>        
        /// <param name="uvc">Uvc.</param>
        /// <param name="equipment">Standard equipment.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>Standard equipment entity.</returns>        
        public StandardEquipmentEntity Save_Equipment(string uvc, StandardEquipment equipment, int dataLoadId)
        {
            return DoInTransaction
            (
                delegate(IDataSession session)
                {
                    if (session.Items["Audit"] as Audit == null)
                    {
                        IAuthority authority = AuthorityFactory.GetAuthority("SYSTEM");

                        IAccountManager manager = authority.AccountManager;

                        IUser user = manager.User("blackbook");

                        IPrincipal principal = manager.Principal(user);

                        session.Items["Audit"] = Resolve<IRepository>().Create(principal);
                    }
                }, 
                () => new StandardEquipmentGateway().Save(uvc, equipment, dataLoadId)
            );
        }

        /// <summary>
        /// Is the cache for the standard equipment for the car with the given uvc valid?
        /// </summary>
        /// <param name="uvc">Uvc.</param>        
        /// <returns>True if we're in the cache window, false otherwise.</returns>
        public bool EquipmentCache_IsValid(string uvc)
        {
            return DoInSession(() => new StandardEquipmentGateway().Cache_IsValid(uvc));
        }

        /// <summary>
        /// Extend the cache window for the standard equipment for the car with the given uvc.
        /// </summary>
        /// <param name="uvc">Uvc.</param>        
        public bool EquipmentCache_Extend(string uvc)
        {
            return DoInSession(() => new StandardEquipmentGateway().Cache_Extend(uvc));
        }

        /// <summary>
        /// Extend the cache window for the standard equipment with the given identifier.
        /// </summary>
        /// <param name="standardEquipmentId">Standard equipment identifier.</param>
        public bool EquipmentCache_Extend(int standardEquipmentId)
        {
            return DoInSession(() => new StandardEquipmentGateway().Cache_Extend(standardEquipmentId));
        }

        #endregion

        #region Vehicle Configuration Methods

        /// <summary>
        /// Fetch a vehicle configuration.
        /// </summary>
        /// <param name="uvc">Uvc.</param>
        /// <param name="vin">Vin.</param>
        /// <param name="state">Two letter US state identifier.</param>
        /// <param name="mileage">Mileage.</param>
        /// <returns>Vehicle configuration entity.</returns>
        public VehicleConfigurationEntity Load_Configuration(string uvc, string vin, string state, int? mileage)
        {            
            return DoInSession(() => new VehicleConfigurationGateway().Fetch(uvc, vin, state ?? string.Empty, mileage));
        }

        /// <summary>        
        /// Save a system-initiated vehicle configuration.
        /// </summary>                
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Client-vehicle association.</param>        
        /// <param name="configuration">Vehicle configuration to save.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>Vehicle configuration entity.</returns>
        public VehicleConfigurationEntity Save_Configuration(IBroker broker, ClientVehicleIdentification vehicle, 
                                                             IVehicleConfiguration configuration, int dataLoadId)
        {
            return DoInTransaction
            (
                delegate(IDataSession session)
                {
                    if (session.Items["Audit"] as Audit == null)
                    {
                        IAuthority authority = AuthorityFactory.GetAuthority("SYSTEM");

                        IAccountManager manager = authority.AccountManager;

                        IUser user = manager.User("blackbook");

                        IPrincipal principal = manager.Principal(user);

                        session.Items["Audit"] = Resolve<IRepository>().Create(principal);
                    }
                },
                () => new VehicleConfigurationGateway().Save(broker, vehicle, configuration, ChangeAgent.System, dataLoadId)
            );
        }

        #endregion

        #region Matrix Methods

        /// <summary>
        /// Load the price matrix for the car and configuration with the given identifiers.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <returns>Price matrix.</returns>
        public Matrix Load_Matrix(int carHistoryId, int vehicleConfigurationHistoryId)
        {
            return DoInSession(() => new MatrixGateway().Fetch(carHistoryId, vehicleConfigurationHistoryId));
        }

        /// <summary>
        /// Save a price matrix for the car and configuration with the given identifiers.
        /// </summary>
        /// <param name="carHistoryId">Car history identifier.</param>
        /// <param name="vehicleConfigurationHistoryId">Vehicle configuration history identifier.</param>
        /// <param name="matrix">Price matrix to save.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>The price matrix that was saved.</returns>
        public Matrix Save_Matrix(int carHistoryId, int vehicleConfigurationHistoryId, Matrix matrix, int dataLoadId)
        {
            return DoInTransaction(() => new MatrixGateway().Save(carHistoryId, vehicleConfigurationHistoryId, matrix, dataLoadId));
        }

        #endregion

        #region Publication Methods

        /// <summary>
        /// Load info about the publications tied to the given broker and vehicle.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Vehicle.</param>
        /// <returns>List of publication details.</returns>
        public IList<IEdition<IPublicationInfo>> Load(IBroker broker, ClientVehicleIdentification vehicle)
        {
            return DoInSession(() => new VehicleConfigurationGateway().Fetch_List(broker, vehicle));
        }

        /// <summary>
        /// Load the publication for the given broker and vehicle that is valid at the given time.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Vehicle.</param>
        /// <param name="on">Date on which the publication should be valid.</param>
        /// <returns>Publication.</returns>
        public IEdition<IPublication> Load(IBroker broker, ClientVehicleIdentification vehicle, DateTime on)
        {
            return DoInSession(
                delegate
                {
                    VehicleConfigurationEntity configuration = new VehicleConfigurationGateway().Fetch(broker, vehicle, on);

                    if (configuration == null)
                    {
                        return null;
                    }

                    CarEntity car = new CarGateway().Fetch(configuration.CarHistoryId);

                    Matrix matrix = new MatrixGateway().Fetch(configuration.CarHistoryId, configuration.VehicleConfigurationHistoryId);

                    return Mapper.Map(car, configuration, matrix);
                }
            );
        }

        /// <summary>
        /// Load the publication for the given broker and vehicle that has the given identifier.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Vehicle.</param>
        /// <param name="id">Publication identifier (a.k.a. vehicle configuration history identifier).</param>
        /// <returns>Publication.</returns>
        public IEdition<IPublication> Load(IBroker broker, ClientVehicleIdentification vehicle, int id)
        {            
            return DoInSession(
                delegate
                {
                    VehicleConfigurationEntity configuration = new VehicleConfigurationGateway().Fetch(broker, vehicle, id);
                    
                    if (configuration == null)
                    {
                        return null;
                    }

                    CarEntity car = new CarGateway().Fetch(configuration.CarHistoryId);

                    Matrix matrix = new MatrixGateway().Fetch(configuration.CarHistoryId, id);

                    return Mapper.Map(car, configuration, matrix);
                }
            );            
        }

        /// <summary>
        /// Save the configuration and price details for the given client and vehicle.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="principal">Principal.</param>
        /// <param name="vehicle">Vehicle.</param>
        /// <param name="configuration">Vehicle configuration.</param>
        /// <param name="matrix">Price details.</param>
        /// <param name="dataLoadId">Reference data version identifier.</param>
        /// <returns>Publication that was saved.</returns>
        public IEdition<IPublication> Save(IBroker broker, IPrincipal principal, ClientVehicleIdentification vehicle, 
                                           IVehicleConfiguration configuration, Matrix matrix, int dataLoadId)
        {   
            return DoInTransaction
            (
                delegate(IDataSession session)
                {
                    if (session.Items["Audit"] as Audit == null)
                    {
                        session.Items["Audit"] = Resolve<IRepository>().Create(principal);
                    }
                },
                delegate
                {
                    VehicleConfigurationEntity configEntity = 
                        new VehicleConfigurationGateway().Save(broker, vehicle, configuration, ChangeAgent.User, dataLoadId);

                    new MatrixGateway().Save(configEntity.CarHistoryId, configEntity.VehicleConfigurationHistoryId, matrix, dataLoadId);

                    CarEntity carEntity = new CarGateway().Fetch(configEntity.CarHistoryId);

                    return Mapper.Map(carEntity, configEntity, matrix);                        
                }                
            );            
        }

        #endregion

        #region Miscellaneous

        /// <summary>
        /// The BlackBook database is Vehicle.
        /// </summary>
        protected override string DatabaseName
        {
            get { return "Vehicle"; }
        }

        /// <summary>
        /// Do nothing on the start of a transaction.
        /// </summary>                
        protected override void OnBeginTransaction(IDataSession session)
        {
            // Do nothing on the start of a transaction.
        }

        #endregion
    }
}
