namespace FirstLook.VehicleValuationGuide.DomainModel.Utility.Commands.TransferObjects
{
    public class PaginatedResultsDto
    {
        private int _totalRowCount;

        public int TotalRowCount
        {
            get { return _totalRowCount; }
            set { _totalRowCount = value; }
        }
    }
}