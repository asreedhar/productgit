﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Utility.Commands.TransferObjects
{
    [Serializable]
    public class AggregateIntegerDto
    {
        private int _minimum;
        private int _average;
        private int _maximum;
        private int _sampleSize;

        public int Minimum
        {
            get { return _minimum; }
            set { _minimum = value; }
        }

        public int Average
        {
            get { return _average; }
            set { _average = value; }
        }

        public int Maximum
        {
            get { return _maximum; }
            set { _maximum = value; }
        }

        public int SampleSize
        {
            get { return _sampleSize; }
            set { _sampleSize = value; }
        }

        private bool _hasMinimum;
        private bool _hasAverage;
        private bool _hasMaximum;

        public bool HasMinimum
        {
            get { return _hasMinimum; }
            set { _hasMinimum = value; }
        }

        public bool HasAverage
        {
            get { return _hasAverage; }
            set { _hasAverage = value; }
        }

        public bool HasMaximum
        {
            get { return _hasMaximum; }
            set { _hasMaximum = value; }
        }
    }
}
