﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Utility.Commands.TransferObjects
{
    [Serializable]
    public class LabelValueDto : IEquatable<LabelValueDto>
    {
        private string _label;
        private string _value;

        public string Label
        {
            get { return _label; }
            set { _label = value; }
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public bool Equals(LabelValueDto other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other._label, _label) && Equals(other._value, _value);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (LabelValueDto)) return false;
            return Equals((LabelValueDto) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((_label != null ? _label.GetHashCode() : 0)*397) ^ (_value != null ? _value.GetHashCode() : 0);
            }
        }

        public static bool operator ==(LabelValueDto left, LabelValueDto right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(LabelValueDto left, LabelValueDto right)
        {
            return !Equals(left, right);
        }

    }
}