﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Utility.Model
{
    [Serializable]
    public class PageArguments
    {
        private string _sortExpression;
        private int _startRowIndex;
        private int _maximumRows;

        public string SortExpression
        {
            get { return _sortExpression; }
            internal set { _sortExpression = value; }
        }

        public int StartRowIndex
        {
            get { return _startRowIndex; }
            internal set { _startRowIndex = value; }
        }

        public int MaximumRows
        {
            get { return _maximumRows; }
            internal set { _maximumRows = value; }
        }
    }
}
