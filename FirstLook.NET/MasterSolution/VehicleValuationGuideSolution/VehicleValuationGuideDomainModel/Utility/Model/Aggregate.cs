﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Utility.Model
{
    [Serializable]
    public class Aggregate<T> where T : struct
    {
        private T? _minimum;
        private T? _average;
        private T? _maximum;
        private int _sampleSize;

        public T? Minimum
        {
            get { return _minimum; }
            internal set { _minimum = value; }
        }

        public T? Average
        {
            get { return _average; }
            internal set { _average = value; }
        }

        public T? Maximum
        {
            get { return _maximum; }
            internal set { _maximum = value; }
        }

        public int SampleSize
        {
            get { return _sampleSize; }
            internal set { _sampleSize = value; }
        }
    }
}
