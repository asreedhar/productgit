﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Utility.Model
{
    [Serializable]
    public class Page<T>
    {
        private PageArguments _arguments;

        public PageArguments Arguments
        {
            get { return _arguments; }
            internal set { _arguments = value; }
        }

        private int _totalRowCount;

        public int TotalRowCount
        {
            get { return _totalRowCount; }
            internal set { _totalRowCount = value; }
        }

        private IList<T> _items;

        public IList<T> Items
        {
            get { return _items; }
            internal set { _items = value; }
        }
    }
}
