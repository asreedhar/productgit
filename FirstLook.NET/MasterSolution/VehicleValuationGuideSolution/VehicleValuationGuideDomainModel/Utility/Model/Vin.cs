﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace FirstLook.VehicleValuationGuide.DomainModel.Utility.Model
{
    public static class Vin
    {
        public enum Status
        {
            Ok,
            Null,
            InvalidLength,
            InvalidCharacters,
            InvalidChecksum
        }

        private static readonly Regex Pattern = new Regex("^[ABCDEFGHJKLMNPRSTUVWXYZ0123456789]{17}$", RegexOptions.IgnoreCase);

        private static readonly Dictionary<char, int> Values = new Dictionary<char, int>
            {
                {'A', 1}, {'B', 2}, {'C', 3}, {'D', 4}, {'E', 5}, {'F', 6}, {'G', 7}, {'H', 8},
                {'J', 1}, {'K', 2}, {'L', 3}, {'M', 4}, {'N', 5}, {'P', 7}, {'R', 9},
                {'S', 2}, {'T', 3}, {'U', 4}, {'V', 5}, {'W', 6}, {'X', 7}, {'Y', 8}, {'Z', 9},
                {'0', 0}, {'1', 1}, {'2', 2}, {'3', 3}, {'4', 4}, {'5', 5}, {'6', 6}, {'7', 7}, {'8', 8}, {'9', 9}
            };

        private static readonly int[] Factors = new[] { 8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2 };

        public static Status Inspect(string vin)
        {
            if (vin == null)
            {
                return Status.Null;
            }

            if (vin.Length != 17)
            {
                return Status.InvalidLength;
            }

            if (!Pattern.IsMatch(vin))
            {
                return Status.InvalidCharacters;
            }

            string text = vin.ToUpperInvariant();

            int total = 0;

            for (int i = 0; i < 17; i++)
            {
                total += (Values[text[i]] * Factors[i]);
            }

            int remainder = total % 11;

            char check = (remainder == 10) ? 'X' : remainder.ToString()[0];

            if (vin[8] != check)
            {
                return Status.InvalidChecksum;
            }

            return Status.Ok;
        }

        /// <summary>
        /// Create a squish vin from a 17 character vin. If the vin is already squished (i.e. is 9 characters), it will
        /// be returned as is. Does not inspect the 17 character VIN as to its validity.
        /// </summary>
        /// <param name="vin">Vin.</param>
        /// <returns>Squish vin.</returns>
        public static string Squish(string vin)
        {
            if (string.IsNullOrEmpty(vin))
            {
                throw new ArgumentException("Empty VIN cannot be squished.");
            }
            if (vin.Length == 9)
            {
                return vin;
            }
            if (vin.Length == 17)
            {                
                return vin.Substring(0, 8) + vin[9];
            }
            throw new ArgumentException("Can only squish a 17 character standard VIN.");
        }
    }
}
