﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<Commands.Module>();

            registry.Register<Model.Module>();
        }
    }
}
