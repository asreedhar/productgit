﻿using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.Impl;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands
{
    public class Module :IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ICommandFactory, CommandFactory>(ImplementationScope.Shared);
        }
    }
}
