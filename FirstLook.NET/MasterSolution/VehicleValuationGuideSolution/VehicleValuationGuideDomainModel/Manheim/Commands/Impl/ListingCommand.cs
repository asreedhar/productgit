﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Lookup;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.Impl
{
    public class ListingCommand : ICommand<ListingResultsDto, ListingArgumentsDto>
    {
        public ListingResultsDto Execute(ListingArgumentsDto parameters)
        {
            NetworkCredential credentials = new NetworkCredential("hndrkwebservice", "hndrkwebservice");

            ILookupListing lookup = RegistryFactory.GetResolver().Resolve<ILookupListing>();

            IList<PreSaleListing> list = lookup.Lookup(
                credentials,
                Mapper.Map(parameters.Criteria));

            List<ListingDto> listings = Mapper.Map(list.Skip(parameters.StartRowIndex).Take(parameters.MaximumRows));

            ListingResultsDto results = new ListingResultsDto
            {
                Arguments = parameters,
                Results = listings,
                TotalRowCount = list.Count
            };

            return results;
        }
    }
}
