﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Lookup;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Soap;
using FirstLook.VehicleValuationGuide.DomainModel.Utility.Commands.TransferObjects;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.Impl
{
    public class DomainCommand : ICommand<DomainResultsDto, DomainArgumentsDto>
    {
        public DomainResultsDto Execute(DomainArgumentsDto parameters)
        {
            NetworkCredential credentials = new NetworkCredential("hndrkwebservice", "hndrkwebservice");

            ILookupSaleSummary service = RegistryFactory.GetResolver().Resolve<ILookupSaleSummary>();

            IList<SaleSummary> list = service.Lookup(credentials, new Criteria());

            DomainResultsDto results = new DomainResultsDto
            {
                Arguments = parameters,
                Auctions = new List<LabelValueDto>(list.Select(x => new LabelValueDto { Label = x.AuctionName, Value = x.AuctionId }).Distinct()),
                ChannelCodes = new List<LabelValueDto>(list.Select(x => new LabelValueDto { Label = x.ChannelCode }).Distinct()),
                Consignors = new List<LabelValueDto>(list.Select(x => new LabelValueDto { Label = x.Description, Value = x.Consignor }).Distinct()),
                SaleDates = new List<LabelValueDto>(list.Select(x => new LabelValueDto { Label = x.SaleDate.ToShortDateString(), Value = x.SaleDate.ToString("yyyy-MM-dd") }).Distinct())
            };

            return results;
        }
    }
}
