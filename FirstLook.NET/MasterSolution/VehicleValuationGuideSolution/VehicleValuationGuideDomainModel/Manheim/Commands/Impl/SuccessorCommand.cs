﻿using System;
using System.Net;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects.Traversal;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Api;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.Impl
{
    [Serializable]
    public class SuccessorCommand : ICommand<SuccessorResultsDto, SuccessorArgumentsDto>
    {
        public SuccessorResultsDto Execute(SuccessorArgumentsDto parameters)
        {
            IManheimTraversal traversal = RegistryFactory.GetResolver().Resolve<IManheimTraversal>();

            ITree tree = traversal.CreateTree(new NetworkCredential("hndrkwebservice", "hndrkwebservice"));
            
            ITreePath path = new TreePath
            {
                State = parameters.Successor
            };

            ITreeNode node = tree.GetNodeByPath(path);

            SuccessorResultsDto results = new SuccessorResultsDto
            {
                Arguments = parameters,
                Path = new PathDto
                {
                    CurrentNode = Mapper.Map(node, Mapper.Operation.No, Mapper.Operation.No),
                    Successors = Mapper.Map(node.Children)
                }
            };

            return results;
        }
    }
}
