﻿using System;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects.Envelopes;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.Impl
{
    public class CommandFactory : ICommandFactory
    {
        public ICommand<TraversalResultsDto, TraversalArgumentsDto> CreateTraversalCommand()
        {
            return new TraversalCommand();
        }

        public ICommand<SuccessorResultsDto, SuccessorArgumentsDto> CreateSuccessorCommand()
        {
            return new SuccessorCommand();
        }

        public ICommand<DomainResultsDto, DomainArgumentsDto> CreateDomainCommand()
        {
            return new DomainCommand();
        }

        public ICommand<SaleSummaryResultsDto, SaleSummaryArgumentsDto> CreateSaleSummaryCommand()
        {
            return new SaleSummaryCommand();
        }

        public ICommand<ListingResultsDto, ListingArgumentsDto> CreateListingCommand()
        {
            return new ListingCommand();
        }
    }
}
