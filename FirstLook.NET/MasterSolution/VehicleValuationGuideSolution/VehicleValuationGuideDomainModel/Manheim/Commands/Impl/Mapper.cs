﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects.Traversal;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Api;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Soap;
using FirstLook.VehicleValuationGuide.DomainModel.Utility.Commands.TransferObjects;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.Impl
{
    public static class Mapper
    {
        public static List<NodeDto> Map(IEnumerable<ITreeNode> nodes)
        {
            return Map(nodes, Operation.No, Operation.No);
        }

        public static List<NodeDto> Map(IEnumerable<ITreeNode> nodes, Operation parents, Operation children)
        {
            List<NodeDto> list = new List<NodeDto>();

            foreach (ITreeNode node in nodes)
            {
                list.Add(Map(node, parents, children));
            }

            return list;
        }

        public class Operation
        {
            private readonly Operation _next;
            private readonly bool _perform;

            protected Operation(bool perform, Operation next)
            {
                _next = next ?? this;
                _perform = perform;
            }

            public Operation Next
            {
                get { return _next; }
            }

            public bool Perform
            {
                get { return _perform; }
            }

            public static readonly Operation Yes;
            public static readonly Operation No;
            public static readonly Operation NoThenYes;
            public static readonly Operation YesThenNo;
            public static readonly Operation YesThenYesThenNo;

            static Operation()
            {
                Yes = new Operation(true, null);
                No = new Operation(false, null);
                NoThenYes = new Operation(false, Yes);
                YesThenNo = new Operation(true, No);
                YesThenYesThenNo = new Operation(true, YesThenNo);
            }
        }

        public static NodeDto Map(ITreeNode node, Operation parents, Operation children)
        {
            ITreePath treePath = new TreePath();

            node.Save(treePath);

            NodeDto dto = new NodeDto
            {
                Id = node.Id,
                Name = node.Name,
                State = treePath.State,
                Mid = node.Mid,
                HasMid = !String.IsNullOrEmpty(node.Mid),
                Label = node.Label
            };

            if (children.Perform)
            {
                dto.Children = Map(node.Children);
            }

            if (parents.Perform && node.Parent != null)
            {
                dto.Parent = Map(node.Parent, parents.Next, children.Next);
            }

            return dto;
        }

        public static List<SaleSummaryDto> Map(IEnumerable<SaleSummary> list)
        {
            List<SaleSummaryDto> values = new List<SaleSummaryDto>();

            foreach (SaleSummary item in list)
            {
                values.Add(
                    new SaleSummaryDto
                    {
                        AuctionId = item.AuctionId,
                        AuctionName = item.AuctionName,
                        ChannelCode = item.ChannelCode,
                        Consignor = item.Consignor,
                        Description = item.Description,
                        Lane = item.Lane,
                        SaleDate = item.SaleDate,
                        SaleNumber = item.SaleNumber,
                        Vehicles = item.Vehicles
                    });
            }

            return values;
        }

        public static List<ListingDto> Map(IEnumerable<PreSaleListing> list)
        {
            List<ListingDto> values = new List<ListingDto>();

            foreach (PreSaleListing item in list)
            {
                values.Add(
                    new ListingDto
                    {
                        Channel = item.Channel,
                        Comments = item.Comments,
                        Consignor = item.Consignor,
                        DateModified = item.DateModified,
                        PhysicalLocation = item.PhysicalLocation,
                        SaleDate = item.SaleDate,
                        SaleInfo = new SaleInfoDto
                        {
                            LaneNumber = item.SaleInfo.LaneNumber,
                            RunNumber = item.SaleInfo.RunNumber,
                            SaleNumber = item.SaleInfo.SaleNumber,
                            SaleYear = item.SaleInfo.SaleYear
                        },
                        SellerAddress = new AddressDto
                        {
                            City = item.SellerAddress.City,
                            Country = item.SellerAddress.Country,
                            PostalCode = item.SellerAddress.PostalCode,
                            StateProvinceRegion = item.SellerAddress.StateProvinceRegion
                        },
                        Vehicle = Map(item.Vehicle)
                    });
            }

            return values;
        }

        private static VehicleDto Map(Vehicle vehicle)
        {
            return new VehicleDto
            {
                Type = Map(vehicle.Type),
                Detail = Map(vehicle.Detail)
            };
        }

        private static VehicleTypeDto Map(VehicleType type)
        {
            return new VehicleTypeDto
            {
                Body = Map(type.Body),
                Make = Map(type.Make),
                Mid = type.Mid,
                Model = Map(type.Model),
                Year = Map(type.Year),
                VinInfo = Map(type.VinInfo)
            };
        }

        private static VinInfoDto Map(VinInfo info)
        {
            return new VinInfoDto
            {
                Vin = info.Vin,
                VinPrefix8 = info.VinPrefix8,
                YearCharacter = info.YearCharacter
            };
        }

        private static LabelValueDto Map(IKeyedValue value)
        {
            if (value == null) return null;

            return new LabelValueDto { Label = value.Name, Value = value.Id };
        }

        private static VehicleDetailDto Map(VehicleDetail detail)
        {
            List<VehicleOptionDto> options = new List<VehicleOptionDto>();

            Append(options, "Airbag", detail.Airbag);
            Append(options, "AirConditioning", detail.AirConditioning);
            Append(options, "AlloyWheels", detail.AlloyWheels);
            Append(options, "AntiLockBrakes", detail.AntiLockBrakes);
            Append(options, "ChildSeat", detail.ChildSeat);
            Append(options, "ClimateControl", detail.ClimateControl);
            Append(options, "CruiseControl", detail.CruiseControl);
            Append(options, "Door", detail.Door);
            Append(options, "Drivetrain", detail.Drivetrain);
            Append(options, "Engine", detail.Engine);
            Append(options, "ExteriorColor", detail.ExteriorColor);
            Append(options, "FrameDamage", detail.FrameDamage);
            Append(options, "Fuel", detail.Fuel);
            Append(options, "Interior", detail.Interior);
            Append(options, "InteriorColor", detail.InteriorColor);
            Append(options, "PowerLocks", detail.PowerLocks);
            Append(options, "PowerMirrors", detail.PowerMirrors);
            Append(options, "PowerSeats", detail.PowerSeats);
            Append(options, "PowerSteering", detail.PowerSteering);
            Append(options, "PowerWindows", detail.PowerWindows);
            Append(options, "RearAirConditioning", detail.RearAirConditioning);
            Append(options, "RearDefroster", detail.RearDefroster);
            Append(options, "Roof", detail.Roof);
            Append(options, "Stereo", detail.Stereo);
            Append(options, "TiltWheel", detail.TiltWheel);
            Append(options, "TintedGlass", detail.TintedGlass);
            Append(options, "Transmission", detail.Transmission);

            options.AddRange(
                detail.AdditionalOptions.Select(
                x => new VehicleOptionDto
                {
                    Display = x.Display,
                    Value = x.Value
                }));

            return new VehicleDetailDto
            {
                Description = detail.Description,
                InvoicePrice = detail.InvoicePrice,
                Mileage = detail.Mileage,
                MsrPrice = detail.MsrPrice,
                RetailPrice = detail.RetailPrice,
                StockNumber = detail.StockNumber,
                Options = options
            };
        }

        private static void Append(ICollection<VehicleOptionDto> options, string type, VehicleOption option)
        {
            if (option != null)
            {
                options.Add(
                    new VehicleOptionDto
                    {
                        Display = option.Display,
                        Type = type,
                        Value = option.Value
                    });
            }
        }

        public static Criteria Map(CriteriaDto dto)
        {
            return dto == null ? new Criteria() : new Criteria
            {
                AuctionId = dto.AuctionId,
                BodyDesc = dto.BodyDesc,
                ChannelCodes = String.IsNullOrEmpty(dto.ChannelCode) ? new string[0] : new[] { dto.ChannelCode },
                Consignor = dto.Consignor,
                MakeDesc = dto.MakeDesc,
                MaxMileage = dto.MaxMileage,
                MaxPrice = dto.MaxPrice,
                MaxVehicleYear = dto.MaxVehicleYear,
                MinMileage = dto.MinMileage,
                MinPrice = dto.MinPrice,
                MinVehicleYear = dto.MinVehicleYear,
                ModelDesc = dto.ModelDesc,
                SaleDate = dto.SaleDate
            };
        }
    }
}
