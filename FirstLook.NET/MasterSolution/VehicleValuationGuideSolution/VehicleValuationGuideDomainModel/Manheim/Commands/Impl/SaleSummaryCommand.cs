﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Lookup;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Soap;
using FirstLook.VehicleValuationGuide.DomainModel.Utility.Commands.TransferObjects;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.Impl
{
    public class SaleSummaryCommand : ICommand<SaleSummaryResultsDto, SaleSummaryArgumentsDto>
    {
        public SaleSummaryResultsDto Execute(SaleSummaryArgumentsDto parameters)
        {
            NetworkCredential credentials = new NetworkCredential("hndrkwebservice", "hndrkwebservice");

            ILookupSaleSummary service = RegistryFactory.GetResolver().Resolve<ILookupSaleSummary>();

            CriteriaDto dto = parameters.Criteria;

            Criteria val = dto == null ? new Criteria() : new Criteria
            {
                AuctionId = dto.AuctionId,
                BodyDesc = dto.BodyDesc,
                ChannelCodes = string.IsNullOrEmpty(dto.ChannelCode) ? new string[0] : new[] { dto.ChannelCode },
                Consignor = dto.Consignor,
                MakeDesc = dto.MakeDesc,
                MaxMileage = dto.MaxMileage,
                MaxPrice = dto.MaxPrice,
                MaxVehicleYear = dto.MaxVehicleYear,
                MinMileage = dto.MinMileage,
                MinPrice = dto.MinPrice,
                MinVehicleYear = dto.MinVehicleYear,
                ModelDesc = dto.ModelDesc,
                SaleDate = dto.SaleDate
            };

            IList<SaleSummary> list = service.Lookup(credentials, val);

            List<SaleSummaryDto> sales = Mapper.Map(list.Skip(parameters.StartRowIndex).Take(parameters.MaximumRows));

            SaleSummaryResultsDto results = new SaleSummaryResultsDto
            {
                Arguments = parameters,
                Results = sales
            };

            return results;
        }
    }
}
