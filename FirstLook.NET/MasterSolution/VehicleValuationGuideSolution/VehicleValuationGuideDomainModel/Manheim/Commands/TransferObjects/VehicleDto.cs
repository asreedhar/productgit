﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects
{
    [Serializable]
    public class VehicleDto
    {
        private VehicleTypeDto _type;
        private VehicleDetailDto _detail;
        
        public VehicleTypeDto Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public VehicleDetailDto Detail
        {
            get { return _detail; }
            set { _detail = value; }
        }
    }
}
