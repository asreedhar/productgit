﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects
{
    [Serializable]
    public class SaleSummaryDto
    {
        private string _auctionId;
        private string _auctionName;
        private string _channelCode;
        private string _consignor;
        private string _description;
        private string _lane;
        private DateTime _saleDate;
        private string _saleNumber;
        private int _vehicles;

        public string AuctionId
        {
            get { return _auctionId; }
            set { _auctionId = value; }
        }

        public string AuctionName
        {
            get { return _auctionName; }
            set { _auctionName = value; }
        }

        public string ChannelCode
        {
            get { return _channelCode; }
            set { _channelCode = value; }
        }

        public string Consignor
        {
            get { return _consignor; }
            set { _consignor = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public string Lane
        {
            get { return _lane; }
            set { _lane = value; }
        }

        public DateTime SaleDate
        {
            get { return _saleDate; }
            set { _saleDate = value; }
        }

        public string SaleNumber
        {
            get { return _saleNumber; }
            set { _saleNumber = value; }
        }

        public int Vehicles
        {
            get { return _vehicles; }
            set { _vehicles = value; }
        }
    }
}
