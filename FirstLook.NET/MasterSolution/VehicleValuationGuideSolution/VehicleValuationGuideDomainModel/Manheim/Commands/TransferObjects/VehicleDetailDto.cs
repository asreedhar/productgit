﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects
{
    [Serializable]
    public class VehicleDetailDto
    {
        private List<VehicleOptionDto> _options;

        public List<VehicleOptionDto> Options
        {
            get { return _options; }
            set { _options = value; }
        }

        private string _description;
        private double _invoicePrice;
        private string _mileage;
        private double _msrPrice;
        private double _retailPrice;
        private string _stockNumber;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public double InvoicePrice
        {
            get { return _invoicePrice; }
            set { _invoicePrice = value; }
        }

        public string Mileage
        {
            get { return _mileage; }
            set { _mileage = value; }
        }

        public double MsrPrice
        {
            get { return _msrPrice; }
            set { _msrPrice = value; }
        }

        public double RetailPrice
        {
            get { return _retailPrice; }
            set { _retailPrice = value; }
        }

        public string StockNumber
        {
            get { return _stockNumber; }
            set { _stockNumber = value; }
        }
    }
}
