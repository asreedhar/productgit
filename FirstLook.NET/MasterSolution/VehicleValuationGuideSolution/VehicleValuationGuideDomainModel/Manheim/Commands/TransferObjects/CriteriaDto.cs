﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects
{
    [Serializable]
    public class CriteriaDto
    {
        private string _auctionId;
        private string _bodyDesc;
        private string _channelCode;
        private string _consignor;
        private string _makeDesc;
        private string _maxMileage;
        private string _maxPrice;
        private string _maxVehicleYear;
        private string _minMileage;
        private string _minPrice;
        private string _minVehicleYear;
        private string _modelDesc;
        private string _saleDate;

        public string AuctionId
        {
            get { return _auctionId; }
            set { _auctionId = value; }
        }

        public string BodyDesc
        {
            get { return _bodyDesc; }
            set { _bodyDesc = value; }
        }

        public string ChannelCode
        {
            get { return _channelCode; }
            set { _channelCode = value; }
        }

        public string Consignor
        {
            get { return _consignor; }
            set { _consignor = value; }
        }

        public string MakeDesc
        {
            get { return _makeDesc; }
            set { _makeDesc = value; }
        }

        public string MaxMileage
        {
            get { return _maxMileage; }
            set { _maxMileage = value; }
        }

        public string MaxPrice
        {
            get { return _maxPrice; }
            set { _maxPrice = value; }
        }

        public string MaxVehicleYear
        {
            get { return _maxVehicleYear; }
            set { _maxVehicleYear = value; }
        }

        public string MinMileage
        {
            get { return _minMileage; }
            set { _minMileage = value; }
        }

        public string MinPrice
        {
            get { return _minPrice; }
            set { _minPrice = value; }
        }

        public string MinVehicleYear
        {
            get { return _minVehicleYear; }
            set { _minVehicleYear = value; }
        }

        public string ModelDesc
        {
            get { return _modelDesc; }
            set { _modelDesc = value; }
        }

        public string SaleDate
        {
            get { return _saleDate; }
            set { _saleDate = value; }
        }
    }
}
