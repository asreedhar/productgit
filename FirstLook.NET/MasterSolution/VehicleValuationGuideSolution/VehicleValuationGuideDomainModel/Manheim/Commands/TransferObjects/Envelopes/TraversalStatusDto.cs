﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public enum TraversalStatusDto
    {
        Success,
        InvalidVinLength,
        InvalidVinCharacters,
        InvalidVinChecksum,
        NoDataForVin
    }
}
