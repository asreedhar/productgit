﻿using System;
using System.Collections.Generic;
using FirstLook.VehicleValuationGuide.DomainModel.Utility.Commands.TransferObjects;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class DomainResultsDto
    {
        private DomainArgumentsDto _arguments;

        public DomainArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        private List<LabelValueDto> _auctions;
        private List<LabelValueDto> _channelCodes;
        private List<LabelValueDto> _consignors;
        private List<LabelValueDto> _saleDates;

        public List<LabelValueDto> Auctions
        {
            get { return _auctions; }
            set { _auctions = value; }
        }

        public List<LabelValueDto> ChannelCodes
        {
            get { return _channelCodes; }
            set { _channelCodes = value; }
        }

        public List<LabelValueDto> Consignors
        {
            get { return _consignors; }
            set { _consignors = value; }
        }

        public List<LabelValueDto> SaleDates
        {
            get { return _saleDates; }
            set { _saleDates = value; }
        }
    }
}
