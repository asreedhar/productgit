﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects.Traversal;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class TraversalResultsDto
    {
        private TraversalArgumentsDto _arguments;
        private PathDto _path;
        private TraversalStatusDto _status;

        public TraversalArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public PathDto Path
        {
            get { return _path; }
            set { _path = value; }
        }

        public TraversalStatusDto Status
        {
            get { return _status; }
            set { _status = value; }
        }
    }
}
