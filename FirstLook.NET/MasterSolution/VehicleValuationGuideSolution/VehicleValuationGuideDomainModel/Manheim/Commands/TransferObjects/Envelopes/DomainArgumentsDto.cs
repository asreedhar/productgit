﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class DomainArgumentsDto
    {
        private Guid _broker;

        public Guid Broker
        {
            get { return _broker; }
            set { _broker = value; }
        }
    }
}
