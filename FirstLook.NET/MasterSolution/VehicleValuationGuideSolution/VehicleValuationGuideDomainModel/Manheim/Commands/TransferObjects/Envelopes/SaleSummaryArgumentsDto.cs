﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.Utility.Commands.TransferObjects;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class SaleSummaryArgumentsDto : PaginatedArgumentsDto
    {
        private Guid _broker;

        public Guid Broker
        {
            get { return _broker; }
            set { _broker = value; }
        }

        private CriteriaDto _criteria;

        public CriteriaDto Criteria
        {
            get { return _criteria; }
            set { _criteria = value; }
        }
    }
}
