﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects.Traversal;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class SuccessorResultsDto
    {
        private SuccessorArgumentsDto _arguments;
        private PathDto _path;

        public SuccessorArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public PathDto Path
        {
            get { return _path; }
            set { _path = value; }
        }
    }
}
