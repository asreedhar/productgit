﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class TraversalArgumentsDto
    {
        private string _vin;

        public string Vin
        {
            get { return _vin; }
            set { _vin = value; }
        }
    }
}
