﻿using System;
using System.Collections.Generic;
using FirstLook.VehicleValuationGuide.DomainModel.Utility.Commands.TransferObjects;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ListingResultsDto : PaginatedResultsDto
    {
        private ListingArgumentsDto _arguments;

        public ListingArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        private List<ListingDto> _results;

        public List<ListingDto> Results
        {
            get { return _results; }
            set { _results = value; }
        }
    }
}
