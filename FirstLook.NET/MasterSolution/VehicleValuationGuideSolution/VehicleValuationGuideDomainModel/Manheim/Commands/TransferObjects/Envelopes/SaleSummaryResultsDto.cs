﻿using System;
using System.Collections.Generic;
using FirstLook.VehicleValuationGuide.DomainModel.Utility.Commands.TransferObjects;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class SaleSummaryResultsDto : PaginatedResultsDto
    {
        private SaleSummaryArgumentsDto _arguments;
        private List<SaleSummaryDto> _results;

        public SaleSummaryArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        public List<SaleSummaryDto> Results
        {
            get { return _results; }
            set { _results = value; }
        }
    }
}
