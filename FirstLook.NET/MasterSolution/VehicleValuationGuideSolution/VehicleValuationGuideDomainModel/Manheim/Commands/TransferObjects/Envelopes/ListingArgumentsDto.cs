﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.Utility.Commands.TransferObjects;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ListingArgumentsDto : PaginatedArgumentsDto
    {
        private Guid _broker;
        private CriteriaDto _criteria;

        public Guid Broker
        {
            get { return _broker; }
            set { _broker = value; }
        }

        public CriteriaDto Criteria
        {
            get { return _criteria; }
            set { _criteria = value; }
        }
    }
}
