﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects
{
    [Serializable]
    public class VinInfoDto
    {
        private string _vin;
        private string _vinPrefix8;
        private string _yearCharacter;

        public string Vin
        {
            get { return _vin; }
            set { _vin = value; }
        }

        public string VinPrefix8
        {
            get { return _vinPrefix8; }
            set { _vinPrefix8 = value; }
        }

        public string YearCharacter
        {
            get { return _yearCharacter; }
            set { _yearCharacter = value; }
        }
    }
}
