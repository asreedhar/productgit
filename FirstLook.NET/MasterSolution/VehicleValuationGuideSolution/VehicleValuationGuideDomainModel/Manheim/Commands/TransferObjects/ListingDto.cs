﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects
{
    [Serializable]
    public class ListingDto
    {
        private string _consignor;
        private DateTime _saleDate;

        public string Consignor
        {
            get { return _consignor; }
            set { _consignor = value; }
        }

        public DateTime SaleDate
        {
            get { return _saleDate; }
            set { _saleDate = value; }
        }

        private string _channel;
        private string _comments;
        private DateTime? _dateModified;
        private string _physicalLocation;

        public string Channel
        {
            get { return _channel; }
            set { _channel = value; }
        }

        public string Comments
        {
            get { return _comments; }
            set { _comments = value; }
        }

        public DateTime? DateModified
        {
            get { return _dateModified; }
            set { _dateModified = value; }
        }

        public string PhysicalLocation
        {
            get { return _physicalLocation; }
            set { _physicalLocation = value; }
        }

        private SaleInfoDto _saleInfo;

        public SaleInfoDto SaleInfo
        {
            get { return _saleInfo; }
            set { _saleInfo = value; }
        }

        private AddressDto _sellerAddress;

        public AddressDto SellerAddress
        {
            get { return _sellerAddress; }
            set { _sellerAddress = value; }
        }

        private VehicleDto _vehicle;

        public VehicleDto Vehicle
        {
            get { return _vehicle; }
            set { _vehicle = value; }
        }
    }
}
