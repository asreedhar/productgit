﻿using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects.Traversal
{
    [Serializable]
    public class NodeDto
    {
        private NodeDto _parent;

        public NodeDto Parent
        {
            get { return _parent; }
            set { _parent = value; }
        }

        private List<NodeDto> _children;

        public List<NodeDto> Children
        {
            get { return _children; }
            set { _children = value; }
        }

        private string _id;
        private string _name;

        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _state;

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }

        private string _mid;
        private bool _hasMid;

        public string Mid
        {
            get { return _mid; }
            set { _mid = value; }
        }

        public bool HasMid
        {
            get { return _hasMid; }
            set { _hasMid = value; }
        }

        private string _label;

        public string Label
        {
            get { return _label; }
            set { _label = value; }
        }
    }
}
