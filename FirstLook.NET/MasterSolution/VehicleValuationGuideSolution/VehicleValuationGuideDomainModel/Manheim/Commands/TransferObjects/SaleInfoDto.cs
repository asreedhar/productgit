﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects
{
    [Serializable]
    public class SaleInfoDto
    {
        private int _laneNumber;
        private int _runNumber;
        private int _saleNumber;
        private int _saleYear;

        public int LaneNumber
        {
            get { return _laneNumber; }
            set { _laneNumber = value; }
        }

        public int RunNumber
        {
            get { return _runNumber; }
            set { _runNumber = value; }
        }

        public int SaleNumber
        {
            get { return _saleNumber; }
            set { _saleNumber = value; }
        }

        public int SaleYear
        {
            get { return _saleYear; }
            set { _saleYear = value; }
        }
    }
}
