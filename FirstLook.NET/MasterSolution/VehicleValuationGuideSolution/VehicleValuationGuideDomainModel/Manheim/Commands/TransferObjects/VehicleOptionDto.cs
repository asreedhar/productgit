﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects
{
    [Serializable]
    public class VehicleOptionDto
    {
        private string _display;
        private string _value;
        private string _type;

        public string Display
        {
            get { return _display; }
            internal set { _display = value; }
        }

        public string Value
        {
            get { return _value; }
            internal set { _value = value; }
        }

        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }
    }
}
