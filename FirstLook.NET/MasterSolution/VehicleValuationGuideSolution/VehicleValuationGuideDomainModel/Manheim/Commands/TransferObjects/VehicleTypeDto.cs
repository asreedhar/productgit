﻿using System;
using FirstLook.VehicleValuationGuide.DomainModel.Utility.Commands.TransferObjects;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects
{
    [Serializable]
    public class VehicleTypeDto
    {
        private LabelValueDto _year;
        private LabelValueDto _make;
        private LabelValueDto _model;
        private LabelValueDto _body;

        public LabelValueDto Year
        {
            get { return _year; }
            set { _year = value; }
        }

        public LabelValueDto Make
        {
            get { return _make; }
            set { _make = value; }
        }

        public LabelValueDto Model
        {
            get { return _model; }
            set { _model = value; }
        }

        public LabelValueDto Body
        {
            get { return _body; }
            set { _body = value; }
        }

        private string _mid;

        public string Mid
        {
            get { return _mid; }
            set { _mid = value; }
        }

        private VinInfoDto _vinInfo;

        public VinInfoDto VinInfo
        {
            get { return _vinInfo; }
            set { _vinInfo = value; }
        }
    }
}
