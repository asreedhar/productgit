﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects
{
    [Serializable]
    public class AddressDto
    {
        private string _city;
        private string _country;
        private string _postalCode;
        private string _stateProvinceRegion;

        public string City
        {
            get { return _city; }
            set { _city = value; }
        }

        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }

        public string PostalCode
        {
            get { return _postalCode; }
            set { _postalCode = value; }
        }

        public string StateProvinceRegion
        {
            get { return _stateProvinceRegion; }
            set { _stateProvinceRegion = value; }
        }
    }
}
