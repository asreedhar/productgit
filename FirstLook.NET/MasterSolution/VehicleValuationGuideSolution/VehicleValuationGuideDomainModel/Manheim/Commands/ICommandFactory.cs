﻿using FirstLook.Common.Core.Command;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands.TransferObjects.Envelopes;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Commands
{
    public interface ICommandFactory
    {
        ICommand<TraversalResultsDto, TraversalArgumentsDto>
            CreateTraversalCommand();

        ICommand<SuccessorResultsDto, SuccessorArgumentsDto>
            CreateSuccessorCommand();

        ICommand<DomainResultsDto, DomainArgumentsDto>
            CreateDomainCommand();

        ICommand<SaleSummaryResultsDto, SaleSummaryArgumentsDto>
            CreateSaleSummaryCommand();

        ICommand<ListingResultsDto, ListingArgumentsDto>
            CreateListingCommand();
    }
}
