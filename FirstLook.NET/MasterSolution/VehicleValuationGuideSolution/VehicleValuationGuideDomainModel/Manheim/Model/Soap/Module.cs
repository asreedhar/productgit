﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Soap
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ISoapService, SoapService>(ImplementationScope.Shared);
        }
    }
}
