using System;
using FirstLook.VehicleValuationGuide.DomainModel.Utility.Model;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Soap
{
    [Serializable]
    public class Criteria : IEquatable<Criteria>
    {
        private string _auctionId;
        private string _bodyDesc;
        private string[] _channelCodes;
        private string _consignor;
        private string _makeDesc;
        private string _maxMileage;
        private string _maxPrice;
        private string _maxVehicleYear;
        private string _minMileage;
        private string _minPrice;
        private string _minVehicleYear;
        private string _modelDesc;
        private string _saleDate;

        public string AuctionId
        {
            get { return _auctionId; }
            internal set { _auctionId = value; }
        }

        public string BodyDesc
        {
            get { return _bodyDesc; }
            internal set { _bodyDesc = value; }
        }

        public string[] ChannelCodes
        {
            get { return _channelCodes; }
            internal set { _channelCodes = value; }
        }

        public string Consignor
        {
            get { return _consignor; }
            internal set { _consignor = value; }
        }

        public string MakeDesc
        {
            get { return _makeDesc; }
            internal set { _makeDesc = value; }
        }

        public string MaxMileage
        {
            get { return _maxMileage; }
            internal set { _maxMileage = value; }
        }

        public string MaxPrice
        {
            get { return _maxPrice; }
            internal set { _maxPrice = value; }
        }

        public string MaxVehicleYear
        {
            get { return _maxVehicleYear; }
            internal set { _maxVehicleYear = value; }
        }

        public string MinMileage
        {
            get { return _minMileage; }
            internal set { _minMileage = value; }
        }

        public string MinPrice
        {
            get { return _minPrice; }
            internal set { _minPrice = value; }
        }

        public string MinVehicleYear
        {
            get { return _minVehicleYear; }
            internal set { _minVehicleYear = value; }
        }

        public string ModelDesc
        {
            get { return _modelDesc; }
            internal set { _modelDesc = value; }
        }

        public string SaleDate
        {
            get { return _saleDate; }
            internal set { _saleDate = value; }
        }

        #region Overrides of Object

        public bool Equals(Criteria other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other._auctionId, _auctionId) &&
                   Equals(other._bodyDesc, _bodyDesc) &&
                   ArrayUtils.AreEqual(other._channelCodes, _channelCodes) &&
                   Equals(other._consignor, _consignor) &&
                   Equals(other._makeDesc, _makeDesc) &&
                   Equals(other._maxMileage, _maxMileage) &&
                   Equals(other._maxPrice, _maxPrice) &&
                   Equals(other._maxVehicleYear, _maxVehicleYear) &&
                   Equals(other._minMileage, _minMileage) &&
                   Equals(other._minPrice, _minPrice) &&
                   Equals(other._minVehicleYear, _minVehicleYear) &&
                   Equals(other._modelDesc, _modelDesc) &&
                   Equals(other._saleDate, _saleDate);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (Criteria)) return false;
            return Equals((Criteria) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int result = (_auctionId != null ? _auctionId.GetHashCode() : 0);
                result = (result*397) ^ (_bodyDesc != null ? _bodyDesc.GetHashCode() : 0);
                result = (result*397) ^ (_channelCodes != null ? ArrayUtils.GetHashCode(_channelCodes) : 0);
                result = (result*397) ^ (_consignor != null ? _consignor.GetHashCode() : 0);
                result = (result*397) ^ (_makeDesc != null ? _makeDesc.GetHashCode() : 0);
                result = (result*397) ^ (_maxMileage != null ? _maxMileage.GetHashCode() : 0);
                result = (result*397) ^ (_maxPrice != null ? _maxPrice.GetHashCode() : 0);
                result = (result*397) ^ (_maxVehicleYear != null ? _maxVehicleYear.GetHashCode() : 0);
                result = (result*397) ^ (_minMileage != null ? _minMileage.GetHashCode() : 0);
                result = (result*397) ^ (_minPrice != null ? _minPrice.GetHashCode() : 0);
                result = (result*397) ^ (_minVehicleYear != null ? _minVehicleYear.GetHashCode() : 0);
                result = (result*397) ^ (_modelDesc != null ? _modelDesc.GetHashCode() : 0);
                result = (result*397) ^ (_saleDate != null ? _saleDate.GetHashCode() : 0);
                return result;
            }
        }

        #endregion
    }
}