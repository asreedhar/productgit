using System;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using FirstLook.VehicleValuationGuide.DomainModel.InventorySearch;
using FirstLook.VehicleValuationGuide.DomainModel.VehicleDecoder;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Soap
{
    public class SoapService : ISoapService
    {
        protected const string DecodeType = "MMR";

        protected const string MinYear = "1980";

        public IList<Year> Query(ICredentials credentials)
        {
            VehicleDecoderServiceImplService service = new VehicleDecoderServiceImplService
            {
                Credentials = credentials
            };
            
            IdNamePair[] years = service.getPartialYearList(MinYear, DateTime.Now.Year.ToString());

            return new List<Year>(years.Select(x => new Year {Id = x.id, Name = x.name}));
        }

        public IList<Make> Query(ICredentials credentials, Year year)
        {
            VehicleDecoderServiceImplService service = new VehicleDecoderServiceImplService
            {
                Credentials = credentials
            };

            IdNamePair[] makes = service.getMMRMakeList(year.Id);

            return new List<Make>(makes.Select(x => new Make { Id = x.id, Name = x.name }));
        }

        public IList<Model> Query(ICredentials credentials, Year year, Make make)
        {
            VehicleDecoderServiceImplService service = new VehicleDecoderServiceImplService
            {
                Credentials = credentials
            };

           
            IdNamePair[] models = service.getMMRModelList(year.Id, make.Id);

            return new List<Model>(models.Select(x => new Model { Id = x.id, Name = x.name }));
        }

        public IList<Body> Query(ICredentials credentials, Year year, Make make, Model model)
        {
            VehicleDecoderServiceImplService service = new VehicleDecoderServiceImplService
            {
                Credentials = credentials
            };

            IdNamePair[] bodies = service.getMMRBodyList(year.Id, make.Id, model.Id);

            return new List<Body>(bodies.Select(x => new Body { Id = x.id, Name = x.name }));
        }

        public VehicleType Vehicle(ICredentials credentials, Year year, Make make, Model model, Body body)
        {
            VehicleDecoderServiceImplService service = new VehicleDecoderServiceImplService
            {
                Credentials = credentials
            };

          
            VehicleDecoder.Vehicle vehicle = service.getByYearMakeModelBody(year.Id, make.Id, model.Id, body.Id, DecodeType);

            return new VehicleType{Year = year, Make = make, Model = model, Body = body, Mid = vehicle.type.mid};
        }

        public IList<VehicleType> Vin(ICredentials credentials, string vin)
        {
            VehicleDecoderServiceImplService service = new VehicleDecoderServiceImplService
            {
                Credentials = credentials
            };

           
            VehicleDecoder.Vehicle[] vehicles = service.decodeVin(vin, string.Empty, string.Empty, DecodeType);

            return new List<VehicleType>(vehicles.Select(x => Map(x)));
        }

        private static VehicleType Map(VehicleDecoder.Vehicle x)
        {
            VehicleDecoder.VehicleType type = x.type;

            return new VehicleType
            {
                Year = new Year { Id = type.year.ToString(), Name = type.year.ToString() },
                Make = new Make { Id = type.make.id, Name = type.make.name },
                Model = new Model { Id = type.model.id, Name = type.model.name },
                Body = new Body { Id = type.body.id, Name = type.body.name },
                Mid = type.mid,
                VinInfo = new VinInfo{Vin = type.vinInfo.vin, VinPrefix8 = type.vinInfo.vinPrefix8, YearCharacter = type.vinInfo.yearCharacter}
            };
        }

        public IList<SaleSummary> Sales(ICredentials credentials, Criteria criteria)
        {
            InventorySearchServiceImplService service = new InventorySearchServiceImplService
            {
                Credentials = credentials
            };

           
            InventorySearch.SaleSummary[] list = service.getSaleSummaries(
                new GetSaleSummariesRequest
                    {
                        auctionId = criteria.AuctionId,
                        bodyDesc = criteria.BodyDesc,
                        channelCodes = criteria.ChannelCodes,
                        consignor = criteria.Consignor,
                        makeDesc = criteria.MakeDesc,
                        maxMileage = criteria.MaxMileage,
                        maxPrice = criteria.MaxPrice,
                        maxVehicleYear = criteria.MaxVehicleYear,
                        minMileage = criteria.MinMileage,
                        minPrice = criteria.MinPrice,
                        minVehicleYear = criteria.MinVehicleYear,
                        modelDesc = criteria.ModelDesc,
                        saleDate = criteria.SaleDate
                    });

            return new List<SaleSummary>(
                list.Select(
                x => new SaleSummary
                {
                    AuctionId = x.auctionId,
                    AuctionName = x.auctionName,
                    ChannelCode = x.channelcode,
                    Consignor = x.consignor,
                    Description = x.description,
                    Lane = x.lane,
                    SaleDate = DateTime.Parse(x.saleDate),
                    SaleNumber = x.saleNumber,
                    Vehicles = int.Parse(x.vehicles)
                }));
        }

        public IList<PreSaleListing> Listings(ICredentials credentials, Criteria criteria)
        {
            InventorySearchServiceImplService service = new InventorySearchServiceImplService
            {
                Credentials = credentials
            };

            
            GetListingsResponse response = service.getListings(
                new GetListingsRequest
                {
                    auctionId = criteria.AuctionId,
                    bodyDesc = criteria.BodyDesc,
                    channelCodes = criteria.ChannelCodes,
                    consignor = criteria.Consignor,
                    makeDesc = criteria.MakeDesc,
                    maxMileage = criteria.MaxMileage,
                    maxPrice = criteria.MaxPrice,
                    maxVehicleYear = criteria.MaxVehicleYear,
                    minMileage = criteria.MinMileage,
                    minPrice = criteria.MinPrice,
                    minVehicleYear = criteria.MinVehicleYear,
                    modelDesc = criteria.ModelDesc,
                    saleDate = criteria.SaleDate
                });

            return new List<PreSaleListing>(
                response.listings.Select(
                    x => new PreSaleListing
                    {
                        Channel = x.channel.id,
                        Comments = x.comments,
                        Consignor = x.consignor,
                        DateModified = x.dateModified,
                        Options = x.options == null ? new List<Option>() : new List<Option>(
                            x.options.Select(
                            y => new Option
                            {
                                Code = y.code,
                                Display = y.display,
                                Value = y.value
                            })),
                        PhysicalLocation = x.physicalLocation,
                        SaleDate = DateTime.Parse(x.saleDate),
                        SaleInfo = new SaleInfo
                        {
                            LaneNumber = x.saleInfo.laneNumber,
                            RunNumber = x.saleInfo.runNumber,
                            SaleNumber = x.saleInfo.saleNumber,
                            SaleYear = x.saleInfo.saleYear
                        },
                        SellerAddress = new Address
                        {
                            City = x.seller.dealership.address.city,
                            Country = x.seller.dealership.address.country,
                            PostalCode = x.seller.dealership.address.postalCode,
                            StateProvinceRegion = x.seller.dealership.address.stateProvinceRegion
                        },
                        Vehicle = new Vehicle
                        {
                            Type = Map(x),
                            Detail = new VehicleDetail
                            {
                                AdditionalOptions = x.vehicle.detail.additionalOptions == null ? new List<VehicleOption>() : new List<VehicleOption>(x.vehicle.detail.additionalOptions.Select(y => Map(y))),
                                Airbag = Map(x.vehicle.detail.airbag),
                                AirConditioning = Map(x.vehicle.detail.airConditioning),
                                AlloyWheels = Map(x.vehicle.detail.alloyWheels),
                                AntiLockBrakes = Map(x.vehicle.detail.antiLockBrakes),
                                ChildSeat = Map(x.vehicle.detail.childSeat),
                                ClimateControl = Map(x.vehicle.detail.climateControl),
                                CruiseControl = Map(x.vehicle.detail.cruiseControl),
                                Description = x.vehicle.detail.description,
                                Door = Map(x.vehicle.detail.door),
                                Drivetrain = Map(x.vehicle.detail.drivetrain),
                                Engine = Map(x.vehicle.detail.engine),
                                ExteriorColor = Map(x.vehicle.detail.exteriorColor),
                                FrameDamage = Map(x.vehicle.detail.frameDamage),
                                Fuel = Map(x.vehicle.detail.fuel),
                                Interior = Map(x.vehicle.detail.interior),
                                InteriorColor = Map(x.vehicle.detail.interiorColor),
                                InvoicePrice = x.vehicle.detail.invoicePrice,
                                Mileage = x.vehicle.detail.mileage,
                                MsrPrice = x.vehicle.detail.msrPrice,
                                PowerLocks = Map(x.vehicle.detail.powerLocks),
                                PowerMirrors = Map(x.vehicle.detail.powerMirrors),
                                PowerSeats = Map(x.vehicle.detail.powerSeats),
                                PowerSteering = Map(x.vehicle.detail.powerSteering),
                                PowerWindows = Map(x.vehicle.detail.powerWindows),
                                PriorPaint = Map(x.vehicle.detail.priorPaint),
                                RearAirConditioning = Map(x.vehicle.detail.rearAirConditioning),
                                RearDefroster = Map(x.vehicle.detail.rearDefroster),
                                RetailPrice = x.vehicle.detail.retailPrice,
                                Roof = Map(x.vehicle.detail.roof),
                                Stereo = Map(x.vehicle.detail.stereo),
                                StockNumber = x.vehicle.detail.stockNumber,
                                TiltWheel = Map(x.vehicle.detail.tiltWheel),
                                TintedGlass = Map(x.vehicle.detail.tintedGlass),
                                Transmission = Map(x.vehicle.detail.transmission)
                            }
                        }
                    }));
        }

        private static VehicleOption Map(InventorySearch.VehicleOption y)
        {
            if (y == null) return null;

            return new VehicleOption
            {
                Display = y.display,
                Value = y.value
            };
        }

        private static VehicleType Map(InventorySearch.Listing x)
        {
            InventorySearch.VehicleType type = x.vehicle.type;

            return new VehicleType
            {
                Year = new Year { Id = type.year.ToString(), Name = type.year.ToString() },
                Make = new Make { Id = type.make.id, Name = type.make.name },
                Model = new Model { Id = type.model.id, Name = type.model.name },
                Body = new Body { Id = type.body.id, Name = type.body.name },
                Mid = type.mid,
                VinInfo = new VinInfo { Vin = type.vinInfo.vin, VinPrefix8 = type.vinInfo.vinPrefix8, YearCharacter = type.vinInfo.yearCharacter }
            };
        }
    }
}
