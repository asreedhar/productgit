using System.Collections.Generic;
using System.Net;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Soap
{
    public interface ISoapService
    {
        IList<Year> Query(ICredentials credentials);

        IList<Make> Query(ICredentials credentials, Year year);

        IList<Model> Query(ICredentials credentials, Year year, Make make);

        IList<Body> Query(ICredentials credentials, Year year, Make make, Model model);

        IList<VehicleType> Vin(ICredentials credentials, string vin);

        VehicleType Vehicle(ICredentials credentials, Year year, Make make, Model model, Body body);

        IList<SaleSummary> Sales(ICredentials credentials, Criteria criteria);

        IList<PreSaleListing> Listings(ICredentials credentials, Criteria criteria);
    }
}
