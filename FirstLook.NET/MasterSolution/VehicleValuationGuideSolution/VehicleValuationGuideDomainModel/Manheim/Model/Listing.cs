using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model
{
    [Serializable]
    public class Listing
    {
        private string _channel;
        private string _comments;
        private DateTime? _dateModified;
        private List<Option> _options;
        private string _physicalLocation;
        private SaleInfo _saleInfo;
        private Address _sellerAddress;
        private Vehicle _vehicle;

        public string Channel
        {
            get { return _channel; }
            set { _channel = value; }
        }

        public string Comments
        {
            get { return _comments; }
            set { _comments = value; }
        }

        public DateTime? DateModified
        {
            get { return _dateModified; }
            set { _dateModified = value; }
        }

        public List<Option> Options
        {
            get { return _options; }
            set { _options = value; }
        }

        public string PhysicalLocation
        {
            get { return _physicalLocation; }
            set { _physicalLocation = value; }
        }

        public SaleInfo SaleInfo
        {
            get { return _saleInfo; }
            set { _saleInfo = value; }
        }

        public Address SellerAddress
        {
            get { return _sellerAddress; }
            set { _sellerAddress = value; }
        }

        public Vehicle Vehicle
        {
            get { return _vehicle; }
            internal set { _vehicle = value; }
        }
    }
}