using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model
{
    [Serializable]
    public class Address
    {
        private string _city;
        private string _country;
        private string _postalCode;
        private string _stateProvinceRegion;

        public string City
        {
            get { return _city; }
            internal set { _city = value; }
        }

        public string Country
        {
            get { return _country; }
            internal set { _country = value; }
        }

        public string PostalCode
        {
            get { return _postalCode; }
            internal set { _postalCode = value; }
        }

        public string StateProvinceRegion
        {
            get { return _stateProvinceRegion; }
            internal set { _stateProvinceRegion = value; }
        }
    }
}