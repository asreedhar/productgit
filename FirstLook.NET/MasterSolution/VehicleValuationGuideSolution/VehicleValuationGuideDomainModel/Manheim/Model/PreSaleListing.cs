﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model
{
    [Serializable]
    public class PreSaleListing : Listing
    {
        private string _consignor;
        private DateTime _saleDate;

        public string Consignor
        {
            get { return _consignor; }
            internal set { _consignor = value; }
        }

        public DateTime SaleDate
        {
            get { return _saleDate; }
            internal set { _saleDate = value; }
        }
    }
}
