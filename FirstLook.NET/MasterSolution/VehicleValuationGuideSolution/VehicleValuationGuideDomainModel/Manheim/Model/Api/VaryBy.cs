﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Api
{
    [Serializable]
    internal enum VaryBy
    {
        None,
        Make,
        Model,
        Body
    }
}