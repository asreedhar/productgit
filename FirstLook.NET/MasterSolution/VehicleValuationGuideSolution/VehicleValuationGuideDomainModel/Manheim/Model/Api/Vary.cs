using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Api
{
    internal static class Vary
    {
        public static VaryBy By(IList<VehicleType> values)
        {
            if (values == null || values.Count == 0)
            {
                return VaryBy.None;
            }

            int ctr = 0;

            while (ctr < 3)
            {
                IKeyedValue reference = null, item = null;

                int lop = 0;

                for (int i = 0, l = values.Count; i < l; i++)
                {
                    VehicleType value = values[i];
                
                    if (value == null)
                    {
                        continue;
                    }

                    switch (ctr)
                    {
                        case 0:
                            item = value.Make;
                            break;
                        case 1:
                            item = value.Model;
                            break;
                        case 2:
                            item = value.Body;
                            break;
                    }

                    if (item == null && l == 1)
                    {
                        return (VaryBy) Enum.ToObject(typeof (VaryBy), ctr);
                    }

                    if (lop++ == 0)
                    {
                        reference = item;
                    }
                    else
                    {
                        string a = reference != null ? reference.Id : null,
                               b = item != null ? item.Id : null;

                        if (a != b)
                        {
                            return (VaryBy) Enum.ToObject(typeof (VaryBy), ctr+1);
                        }
                    }
                }

                ctr++;
            }

            return VaryBy.None;
        }

        public static IList<IKeyedValue> To(IList<VehicleType> values, VaryBy vary)
        {
            IList<IKeyedValue> items = new List<IKeyedValue>();

            foreach (VehicleType value in values)
            {
                switch (vary)
                {
                    case VaryBy.Make:
                        items.Add(value.Make);
                        break;
                    case VaryBy.Model:
                        items.Add(value.Model);
                        break;
                    case VaryBy.Body:
                        items.Add(value.Body);
                        break;
                }
            }

            return items;
        }
    }
}