﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Api
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IManheimTraversal, ManheimTraversal>(ImplementationScope.Shared);
        }
    }
}
