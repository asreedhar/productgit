﻿using System.Collections.Generic;
using System.Net;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Api
{
    public interface IManheimTraversal
    {
        ITree CreateTree(ICredentials credentials);

        ITree CreateTree(ICredentials credentials, string vin);
    }

    public interface ITree
    {
        ITreeNode Root { get; }

        ITreeNode GetNodeByPath(ITreePath path);
    }

    public interface ITreeNode
    {
        ITreeNode Parent { get; }

        string Label { get; }

        string Id { get; }

        string Name { get; }

        string Mid { get; }

        IList<ITreeNode> Children { get; }

        bool HasChildren { get; }

        void Save(ITreePath path);
    }

    public interface ITreePath
    {
        string State { get; set; }
    }
}
