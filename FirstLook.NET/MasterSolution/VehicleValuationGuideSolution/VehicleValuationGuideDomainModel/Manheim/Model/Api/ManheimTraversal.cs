﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Lookup;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Api
{
    public class ManheimTraversal : IManheimTraversal
    {
        protected const string PairSeparator = "||", ValueSeparator = "=";

        public ITree CreateTree(ICredentials credentials)
        {
            return new Tree
            {
                Credentials = credentials,
                Root = new InitialNode(credentials)
            };
        }

        public ITree CreateTree(ICredentials credentials, string vin)
        {
            return new Tree
            {
                Credentials = credentials,
                Root = new VinNode(credentials, vin)
            };
        }

        protected static T Resolve<T>()
        {
            return RegistryFactory.GetResolver().Resolve<T>();
        }

        sealed class Tree : ITree
        {
            private ICredentials _credentials;
            private ITreeNode _root;

            internal ICredentials Credentials
            {
                get { return _credentials; }
                set { _credentials = value; }
            }

            public ITreeNode Root
            {
                get { return _root; }
                set { _root = value; }
            }

            public ITreeNode GetNodeByPath(ITreePath path)
            {
                if (path != null)
                {
                    if (!string.IsNullOrEmpty(path.State))
                    {
                        string yearId = null, makeId = null, modelId = null, trimId = null;

                        string yearName = null, makeName = null, modelName = null, trimName = null;

                        string vin = null;

                        string[] values = path.State.Split(new[] { PairSeparator }, StringSplitOptions.None);

                        foreach (var value in values)
                        {
                            string[] pair = value.Split(new[] { ValueSeparator }, StringSplitOptions.None);

                            string k = pair[0], v = pair[1];

                            string[] fields = v.Split(new[] { ',' });

                            int len = fields.Length;

                            string id = fields[0],
                                   name = len > 1 ? fields[1] : string.Empty;

                            switch (k)
                            {
                                case "year":
                                    yearId = id;
                                    yearName = name;
                                    break;
                                case "make":
                                    makeId = id;
                                    makeName = name;
                                    break;
                                case "model":
                                    modelId = id;
                                    modelName = name;
                                    break;
                                case "body":
                                    trimId = id;
                                    trimName = name;
                                    break;
                                case "vin":
                                    vin = id;
                                    break;
                            }
                        }

                        if (string.IsNullOrEmpty(yearId))
                        {
                            if (string.IsNullOrEmpty(vin))
                            {
                                return new InitialNode(Credentials);
                            }

                            return new VinNode(_credentials, vin);
                        }

                        YearNode yearNode = new YearNode(Credentials, yearId, yearName);

                        if (string.IsNullOrEmpty(makeId))
                        {
                            return yearNode;
                        }

                        MakeNode makeNode = new MakeNode(Credentials, yearNode, makeId, makeName);

                        if (string.IsNullOrEmpty(modelId))
                        {
                            return makeNode;
                        }

                        ModelNode modelNode = new ModelNode(Credentials, makeNode, modelId, modelName);

                        if (string.IsNullOrEmpty(trimId))
                        {
                            return modelNode;
                        }

                        return new BodyNode(Credentials, modelNode, trimId, trimName);
                    }
                }

                return Root;
            }
        }

        sealed class InitialNode : TreeNode
        {
            private IList<ITreeNode> _children;

            public InitialNode(ICredentials credentials) : base(credentials)
            {
            }

            public override string Label
            {
                get { return ""; }
            }

            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        _children = new List<ITreeNode>();

                        foreach (Year year in Resolve<ILookupYear>().Lookup(Credentials))
                        {
                            _children.Add(new YearNode(Credentials, year.Id, year.Name));
                        }
                    }

                    return _children;
                }
            }

            public override void Save(ITreePath path)
            {
                path.State = string.Empty;
            }
        }

        static string Serialize(string key, ITreeNode node)
        {
            return key + ValueSeparator + node.Id + "," + node.Name;
        }

        sealed class YearNode : TreeNode
        {
            public YearNode(ICredentials credentials, ITreeNode parent, string id, string name)
                : base(credentials, parent, id, name)
            {
            }

            public YearNode(ICredentials credentials, string id, string name)
                : base(credentials, id, name)
            {
            }

            public override string Label
            {
                get { return "Year"; }
            }

            private IList<ITreeNode> _children;

            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        _children = new List<ITreeNode>();

                        foreach (Make make in Resolve<ILookupMake>().Lookup(Credentials, new Year{Id = Id, Name = Name}))
                        {
                            _children.Add(new MakeNode(Credentials, this, make.Id, make.Name));
                        }
                    }

                    return _children;
                }
            }

            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = Serialize("year", this);
                }
            }
        }

        sealed class MakeNode : TreeNode
        {
            public MakeNode(ICredentials credentials, ITreeNode year, string id, string name)
                : base(credentials, year, id, name)
            {
            }

            public override string Label
            {
                get { return "Make"; }
            }

            private IList<ITreeNode> _children;

            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        _children = new List<ITreeNode>();

                        Year year = new Year { Id = Parent.Id, Name = Parent.Name };

                        Make make = new Make { Id = Id, Name = Name };

                        foreach (Model model in Resolve<ILookupModel>().Lookup(Credentials, year, make))
                        {
                            _children.Add(new ModelNode(Credentials, this, model.Id, model.Name));
                        }
                    }

                    return _children;
                }
            }

            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = Serialize("year", Parent) + PairSeparator + Serialize("make", this);
                }
            }
        }

        sealed class ModelNode : TreeNode
        {
            public ModelNode(ICredentials credentials, ITreeNode make, string id, string name)
                : base(credentials, make, id, name)
            {
            }

            public override string Label
            {
                get { return "Model"; }
            }

            private IList<ITreeNode> _children;

            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        _children = new List<ITreeNode>();

                        Year year = new Year { Id = Parent.Parent.Id, Name = Parent.Parent.Name };

                        Make make = new Make { Id = Parent.Id, Name = Parent.Name };

                        Model model = new Model { Id = Id, Name = Name };

                        foreach (Body trim in Resolve<ILookupBody>().Lookup(Credentials, year, make, model))
                        {
                            _children.Add(new BodyNode(Credentials, this, trim.Id, trim.Name));
                        }
                    }

                    return _children;
                }
            }

            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = Serialize("year", Parent.Parent)
                        + PairSeparator + Serialize("make", Parent)
                        + PairSeparator + Serialize("model", this);
                }
            }
        }

        sealed class BodyNode : TreeNode
        {
            public BodyNode(ICredentials credentials, ITreeNode model, string id, string name)
                : base(credentials, model, id, name)
            {
            }

            public override string Label
            {
                get { return "Body"; }
            }

            private readonly IList<ITreeNode> _children = new List<ITreeNode>(0);

            public override IList<ITreeNode> Children
            {
                get
                {
                    return _children;
                }
            }

            public override string Mid
            {
                get
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(Parent.Parent.Parent.Id);
                    sb.Append(Parent.Parent.Id);
                    sb.Append(Parent.Id);
                    sb.Append(Id);
                    return sb.ToString();
                }
            }

            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = Serialize("year", Parent.Parent.Parent)
                        + PairSeparator + Serialize("make", Parent.Parent)
                        + PairSeparator + Serialize("model", Parent)
                        + PairSeparator + Serialize("body", this);
                }
            }
        }

        sealed class VinNode : TreeNode
        {
            private readonly string _vin;

            public VinNode(ICredentials credentials, string vin) : base (credentials)
            {
                _vin = vin;
            }

            public override string Label
            {
                get { return "Vin"; }
            }

            private IList<ITreeNode> _children;

            public override IList<ITreeNode> Children
            {
                get
                {
                    if (_children == null)
                    {
                        _children = new List<ITreeNode>();

                        IList<VehicleType> vehicles = Resolve<ILookupVin>().Lookup(Credentials, _vin);

                        switch (Vary.By(vehicles))
                        {
                            case VaryBy.Make:
                                Make(vehicles);
                                break;
                            case VaryBy.Model:
                                Model(vehicles);
                                break;
                            case VaryBy.Body:
                                Trim(vehicles);
                                break;
                            default:
                                Trim(vehicles);
                                break;
                        }
                    }

                    return _children;
                }
            }

            private void Make(IList<VehicleType> infos)
            {
                for (int i = 0, l = infos.Count; i < l; i++)
                {
                    VehicleType info = infos[i];

                    IKeyedValue year = info.Year,
                                make = info.Make,
                                model = info.Model,
                                trim = info.Body;

                    if (i == 0)
                    {
                        ITreeNode initialNode = new InitialNode(Credentials),
                                  yearNode = new YearNode(Credentials, initialNode, year.Id, year.Name);

                        _parent = yearNode;
                    }

                    ITreeNode makeNode = new MakeNode(Credentials, _parent, make.Id, make.Name),
                              modelNode = new ModelNode(Credentials, makeNode, model.Id, model.Name),
                              trimNode = new BodyNode(Credentials, modelNode, trim.Id, trim.Name);

                    _children.Add(trimNode);
                }
            }

            private void Model(IList<VehicleType> infos)
            {
                for (int i = 0, l = infos.Count; i < l; i++)
                {
                    VehicleType info = infos[i];

                    IKeyedValue year = info.Year,
                                make = info.Make,
                                model = info.Model,
                                trim = info.Body;

                    if (i == 0)
                    {
                        ITreeNode initialNode = new InitialNode(Credentials),
                                  yearNode = new YearNode(Credentials, initialNode, year.Id, year.Name),
                                  makeNode = new MakeNode(Credentials, yearNode, make.Id, make.Name);

                        _parent = makeNode;
                    }

                    ITreeNode modelNode = new ModelNode(Credentials, _parent, model.Id, model.Name),
                              trimNode = new BodyNode(Credentials, modelNode, trim.Id, trim.Name);

                    _children.Add(trimNode);
                }
            }

            private void Trim(IList<VehicleType> infos)
            {
                for (int i = 0, l = infos.Count; i < l; i++)
                {
                    VehicleType info = infos[i];

                    IKeyedValue year = info.Year,
                                make = info.Make,
                                model = info.Model,
                                trim = info.Body;

                    if (i == 0)
                    {
                        ITreeNode initialNode = new InitialNode(Credentials),
                                  yearNode = new YearNode(Credentials, initialNode, year.Id, year.Name),
                                  makeNode = new MakeNode(Credentials, yearNode, make.Id, make.Name),
                                  modelNode = new ModelNode(Credentials, makeNode, model.Id, model.Name);

                        _parent = modelNode;
                    }

                    ITreeNode trimNode = new BodyNode(Credentials, _parent, trim.Id, trim.Name);

                    _children.Add(trimNode);
                }
            }

            private ITreeNode _parent;

            public override ITreeNode Parent
            {
                get
                {
                    IList<ITreeNode> eek = Children; // still?

                    return _parent;
                }
            }

            public override string Mid
            {
                get
                {
                    if (Children.Count == 1)
                    {
                        return _children.First().Mid;
                    }

                    return base.Mid;
                }
            }

            public override void Save(ITreePath path)
            {
                if (path != null)
                {
                    path.State = "vin" + ValueSeparator + _vin + ",,";
                }
            }
        }
    }

    public abstract class TreeNode : ITreeNode
    {
        private readonly ICredentials _credentials;
        private readonly ITreeNode _parent;
        private readonly string _id;
        private readonly string _name;
        private readonly string _mid;

        protected TreeNode(ICredentials credentials)
        {
            _credentials = credentials;
        }

        protected TreeNode(ICredentials credentials, string id, string name)
        {
            _credentials = credentials;
            _id = id;
            _name = name;
        }

        protected TreeNode(ICredentials credentials, ITreeNode parent, string id, string name)
        {
            _credentials = credentials;
            _parent = parent;
            _id = id;
            _name = name;
        }

        protected TreeNode(ICredentials credentials, ITreeNode parent, string id, string name, string mid)
        {
            _credentials = credentials;
            _parent = parent;
            _id = id;
            _name = name;
            _mid = mid;
        }

        public ICredentials Credentials
        {
            get { return _credentials; }
        }

        public abstract string Label { get; }

        public virtual string Id
        {
            get { return _id; }
        }

        public virtual string Name
        {
            get { return _name; }
        }

        public virtual string Mid
        {
            get { return _mid; }
        }

        public virtual ITreeNode Parent
        {
            get
            {
                return _parent;
            }
        }

        public abstract IList<ITreeNode> Children { get; }

        public virtual bool HasChildren
        {
            get
            {
                return Children.Count == 0;
            }
        }

        public abstract void Save(ITreePath path);
    }

    public class TreePath : ITreePath
    {
        private string _state;

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }
    }
}
