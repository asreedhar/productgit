using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model
{
    [Serializable]
    public class Vehicle
    {
        private VehicleType _type;
        private VehicleDetail _detail;

        public VehicleType Type
        {
            get { return _type; }
            internal set { _type = value; }
        }

        public VehicleDetail Detail
        {
            get { return _detail; }
            internal set { _detail = value; }
        }
    }
}