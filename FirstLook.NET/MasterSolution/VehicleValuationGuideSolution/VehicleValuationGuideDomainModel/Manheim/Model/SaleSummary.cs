﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model
{
    [Serializable]
    public class SaleSummary
    {
        private string _auctionId;
        private string _auctionName;
        private string _channelCode;
        private string _consignor;
        private string _description;
        private string _lane;
        private DateTime _saleDate;
        private string _saleNumber;
        private int _vehicles;

        public string AuctionId
        {
            get { return _auctionId; }
            internal set { _auctionId = value; }
        }

        public string AuctionName
        {
            get { return _auctionName; }
            internal set { _auctionName = value; }
        }

        public string ChannelCode
        {
            get { return _channelCode; }
            internal set { _channelCode = value; }
        }

        public string Consignor
        {
            get { return _consignor; }
            internal set { _consignor = value; }
        }

        public string Description
        {
            get { return _description; }
            internal set { _description = value; }
        }

        public string Lane
        {
            get { return _lane; }
            internal set { _lane = value; }
        }

        public DateTime SaleDate
        {
            get { return _saleDate; }
            internal set { _saleDate = value; }
        }

        public string SaleNumber
        {
            get { return _saleNumber; }
            internal set { _saleNumber = value; }
        }

        public int Vehicles
        {
            get { return _vehicles; }
            internal set { _vehicles = value; }
        }
    }
}
