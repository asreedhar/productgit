﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model
{
    [Serializable]
    public class VehicleType
    {
        private Year _year;
        private Make _make;
        private Model _model;
        private Body _body;

        public Year Year
        {
            get { return _year; }
            internal set { _year = value; }
        }

        public Make Make
        {
            get { return _make; }
            internal set { _make = value; }
        }

        public Model Model
        {
            get { return _model; }
            internal set { _model = value; }
        }

        public Body Body
        {
            get { return _body; }
            internal set { _body = value; }
        }

        private string _mid;

        public string Mid
        {
            get { return _mid; }
            internal set { _mid = value; }
        }

        private VinInfo _vinInfo;

        public VinInfo VinInfo
        {
            get { return _vinInfo; }
            internal set { _vinInfo = value; }
        }
    }
}
