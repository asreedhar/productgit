﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model
{
    public interface IKeyedValue
    {
        string Id { get; }

        string Name { get; }
    }

    [Serializable]
    public abstract class KeyedValue : IKeyedValue
    {
        private string _id;
        private string _name;

        public string Id
        {
            get { return _id; }
            internal set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            internal set { _name = value; }
        }
    }

    [Serializable]
    public class Year : KeyedValue
    {
        
    }

    [Serializable]
    public class Make : KeyedValue
    {

    }

    [Serializable]
    public class Model : KeyedValue
    {

    }

    [Serializable]
    public class Body : KeyedValue
    {

    }
}
