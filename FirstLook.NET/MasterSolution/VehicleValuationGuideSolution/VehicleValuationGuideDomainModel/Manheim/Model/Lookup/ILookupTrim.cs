﻿using System.Collections.Generic;
using System.Net;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Soap;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Lookup
{
    public interface ILookupBody
    {
        IList<Body> Lookup(ICredentials credentials, Year year, Make make, Model model);
    }

    public class LookupBody : ILookupBody
    {
        public IList<Body> Lookup(ICredentials credentials, Year year, Make make, Model model)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISoapService service = registry.Resolve<ISoapService>();

            return service.Query(credentials, year, make, model);
        }
    }

    public class CachedLookupBody : CachedLookup, ILookupBody
    {
        private readonly ILookupBody _source = new LookupBody();

        public IList<Body> Lookup(ICredentials credentials, Year year, Make make, Model model)
        {
            string key = CreateCacheKey(year.Id, make.Id, model.Id);

            IList<Body> values = Cache.Get(key) as IList<Body>;

            if (values == null)
            {
                values = _source.Lookup(credentials, year, make, model);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
