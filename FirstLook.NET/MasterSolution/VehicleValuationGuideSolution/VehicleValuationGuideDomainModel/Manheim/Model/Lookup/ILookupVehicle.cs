using System.Net;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Soap;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Lookup
{
    public interface ILookupVehicle
    {
        VehicleType Lookup(ICredentials credentials, Year year, Make make, Model model, Body body);
    }

    public class LookupVehicle : ILookupVehicle
    {
        public VehicleType Lookup(ICredentials credentials, Year year, Make make, Model model, Body body)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISoapService service = registry.Resolve<ISoapService>();

            return service.Vehicle(credentials, year, make, model, body);
        }
    }

    public class CachedLookupVehicle : CachedLookup, ILookupVehicle
    {
        private readonly ILookupVehicle _source = new LookupVehicle();

        public VehicleType Lookup(ICredentials credentials, Year year, Make make, Model model, Body body)
        {
            string key = CreateCacheKey(year.Id, make.Id, model.Id, body.Id);

            VehicleType values = Cache.Get(key) as VehicleType;

            if (values == null)
            {
                values = _source.Lookup(credentials, year, make, model, body);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
