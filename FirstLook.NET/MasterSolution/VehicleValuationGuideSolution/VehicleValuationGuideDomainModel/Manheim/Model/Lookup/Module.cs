﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Lookup
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ILookupMake, CachedLookupMake>(ImplementationScope.Shared);

            registry.Register<ILookupModel, CachedLookupModel>(ImplementationScope.Shared);

            registry.Register<ILookupBody, CachedLookupBody>(ImplementationScope.Shared);

            registry.Register<ILookupVehicle, CachedLookupVehicle>(ImplementationScope.Shared);

            registry.Register<ILookupVin, CachedLookupVin>(ImplementationScope.Shared);

            registry.Register<ILookupYear, CachedLookupYear>(ImplementationScope.Shared);

            registry.Register<ILookupListing, CachedLookupListing>(ImplementationScope.Shared);

            registry.Register<ILookupSaleSummary, CachedLookupSaleSummary>(ImplementationScope.Shared);
        }
    }
}
