﻿using System.Collections.Generic;
using System.Net;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Soap;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Lookup
{
    public interface ILookupYear
    {
        IList<Year> Lookup(ICredentials credentials);
    }

    public class LookupYear : ILookupYear
    {
        public IList<Year> Lookup(ICredentials credentials)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISoapService service = registry.Resolve<ISoapService>();

            return service.Query(credentials);
        }
    }

    public class CachedLookupYear : CachedLookup, ILookupYear
    {
        private readonly ILookupYear _source = new LookupYear();

        public IList<Year> Lookup(ICredentials credentials)
        {
            string key = CreateCacheKey();

            IList<Year> values = Cache.Get(key) as IList<Year>;

            if (values == null)
            {
                values = _source.Lookup(credentials);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
