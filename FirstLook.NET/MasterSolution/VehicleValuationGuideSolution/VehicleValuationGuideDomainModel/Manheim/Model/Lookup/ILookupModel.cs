﻿using System.Collections.Generic;
using System.Net;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Soap;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Lookup
{
    public interface ILookupModel
    {
        IList<Model> Lookup(ICredentials credentials, Year year, Make make);
    }

    public class LookupModel : ILookupModel
    {
        public IList<Model> Lookup(ICredentials credentials, Year year, Make make)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISoapService service = registry.Resolve<ISoapService>();

            return service.Query(credentials, year, make);
        }
    }

    public class CachedLookupModel : CachedLookup, ILookupModel
    {
        private readonly ILookupModel _source = new LookupModel();

        public IList<Model> Lookup(ICredentials credentials, Year year, Make make)
        {
            string key = CreateCacheKey(year.Id, make.Id);

            IList<Model> values = Cache.Get(key) as IList<Model>;

            if (values == null)
            {
                values = _source.Lookup(credentials, year, make);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
