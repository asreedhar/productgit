using System.Collections.Generic;
using System.Net;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Soap;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Lookup
{
    public interface ILookupVin
    {
        IList<VehicleType> Lookup(ICredentials credentials, string vin);
    }

    public class LookupVin : ILookupVin
    {
        public IList<VehicleType> Lookup(ICredentials credentials, string vin)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISoapService service = registry.Resolve<ISoapService>();

            return service.Vin(credentials, vin);
        }
    }

    public class CachedLookupVin : CachedLookup, ILookupVin
    {
        private readonly ILookupVin _source = new LookupVin();

        public IList<VehicleType> Lookup(ICredentials credentials, string vin)
        {
            string key = CreateCacheKey(vin);

            IList<VehicleType> values = Cache.Get(key) as IList<VehicleType>;

            if (values == null)
            {
                values = _source.Lookup(credentials, vin);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
