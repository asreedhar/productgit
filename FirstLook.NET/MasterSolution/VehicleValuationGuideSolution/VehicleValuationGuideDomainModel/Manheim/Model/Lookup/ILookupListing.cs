﻿using System.Collections.Generic;
using System.Net;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Soap;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Lookup
{
    public interface ILookupListing
    {
        IList<PreSaleListing> Lookup(ICredentials credentials, Criteria criteria);
    }

    public class LookupListing : ILookupListing
    {
        public IList<PreSaleListing> Lookup(ICredentials credentials, Criteria criteria)
        {
            ISoapService service = RegistryFactory.GetResolver().Resolve<ISoapService>();

            return service.Listings(credentials, criteria);
        }
    }

    public class CachedLookupListing : CachedLookup, ILookupListing
    {
        private readonly ILookupListing _source = new LookupListing();

        public IList<PreSaleListing> Lookup(ICredentials credentials, Criteria criteria)
        {
            string key = CreateCacheKey(criteria.GetHashCode());

            IList<PreSaleListing> values = Cache.Get(key) as IList<PreSaleListing>;

            if (values == null)
            {
                values = _source.Lookup(credentials, criteria);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
