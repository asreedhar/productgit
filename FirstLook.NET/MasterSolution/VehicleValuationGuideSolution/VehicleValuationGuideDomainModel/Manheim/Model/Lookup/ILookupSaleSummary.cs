﻿using System.Collections.Generic;
using System.Net;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Soap;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Lookup
{
    public interface ILookupSaleSummary
    {
        IList<SaleSummary> Lookup(ICredentials credentials, Criteria criteria);
    }

    public class LookupSaleSummary : ILookupSaleSummary
    {
        public IList<SaleSummary> Lookup(ICredentials credentials, Criteria criteria)
        {
            ISoapService service = RegistryFactory.GetResolver().Resolve<ISoapService>();

            return service.Sales(credentials, criteria);
        }
    }

    public class CachedLookupSaleSummary : CachedLookup, ILookupSaleSummary
    {
        private readonly ILookupSaleSummary _source = new LookupSaleSummary();

        public IList<SaleSummary> Lookup(ICredentials credentials, Criteria criteria)
        {
            string key = CreateCacheKey(criteria.GetHashCode());

            IList<SaleSummary> values = Cache.Get(key) as IList<SaleSummary>;

            if (values == null)
            {
                values = _source.Lookup(credentials, criteria);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
