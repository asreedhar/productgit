﻿using System.Collections.Generic;
using System.Net;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Soap;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model.Lookup
{
    public interface ILookupMake
    {
        IList<Make> Lookup(ICredentials credentials, Year year);
    }

    public class LookupMake : ILookupMake
    {
        public IList<Make> Lookup(ICredentials credentials, Year year)
        {
            IResolver registry = RegistryFactory.GetResolver();

            ISoapService service = registry.Resolve<ISoapService>();

            return service.Query(credentials, year);
        }
    }

    public class CachedLookupMake : CachedLookup, ILookupMake
    {
        private readonly ILookupMake _source = new LookupMake();

        public IList<Make> Lookup(ICredentials credentials, Year year)
        {
            string key = CreateCacheKey(year.Id);

            IList<Make> values = Cache.Get(key) as IList<Make>;

            if (values == null)
            {
                values = _source.Lookup(credentials, year);

                Cache.Add(key, values, CacheConstants.NoAbsoluteExpiration, CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
