using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model
{
    [Serializable]
    public class Option
    {
        private string _code;
        private string _value;
        private string _display;

        public string Code
        {
            get { return _code; }
            internal set { _code = value; }
        }

        public string Value
        {
            get { return _value; }
            internal set { _value = value; }
        }

        public string Display
        {
            get { return _display; }
            internal set { _display = value; }
        }
    }
}