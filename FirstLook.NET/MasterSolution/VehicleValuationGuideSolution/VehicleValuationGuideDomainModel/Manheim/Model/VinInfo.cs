﻿using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model
{
    [Serializable]
    public class VinInfo
    {
        private string _vin;
        private string _vinPrefix8;
        private string _yearCharacter;

        public string Vin
        {
            get { return _vin; }
            internal set { _vin = value; }
        }

        public string VinPrefix8
        {
            get { return _vinPrefix8; }
            internal set { _vinPrefix8 = value; }
        }

        public string YearCharacter
        {
            get { return _yearCharacter; }
            internal set { _yearCharacter = value; }
        }
    }
}
