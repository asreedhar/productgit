using System;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model
{
    [Serializable]
    public class VehicleOption
    {
        private string _display;
        private string _value;

        public string Display
        {
            get { return _display; }
            internal set { _display = value; }
        }

        public string Value
        {
            get { return _value; }
            internal set { _value = value; }
        }
    }
}