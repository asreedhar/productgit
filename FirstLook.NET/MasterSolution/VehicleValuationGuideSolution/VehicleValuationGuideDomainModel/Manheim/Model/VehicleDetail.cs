using System;
using System.Collections.Generic;

namespace FirstLook.VehicleValuationGuide.DomainModel.Manheim.Model
{
    [Serializable]
    public class VehicleDetail
    {
        private List<VehicleOption> _additionalOptions;
        private VehicleOption _airConditioning;
        private VehicleOption _airbag;
        private VehicleOption _alloyWheels;
        private VehicleOption _antiLockBrakes;
        private VehicleOption _childSeat;
        private VehicleOption _climateControl;
        private VehicleOption _cruiseControl;
        private string _description;
        private VehicleOption _door;
        private VehicleOption _drivetrain;
        private VehicleOption _engine;
        private VehicleOption _exteriorColor;
        private VehicleOption _frameDamage;
        private VehicleOption _fuel;
        private VehicleOption _interior;
        private VehicleOption _interiorColor;
        private double _invoicePrice;
        private string _mileage;
        private double _msrPrice;
        private VehicleOption _powerLocks;
        private VehicleOption _powerMirrors;
        private VehicleOption _powerSeats;
        private VehicleOption _powerSteering;
        private VehicleOption _powerWindows;
        private VehicleOption _priorPaint;
        private VehicleOption _rearAirConditioning;
        private VehicleOption _rearDefroster;
        private double _retailPrice;
        private VehicleOption _roof;
        private VehicleOption _stereo;
        private string _stockNumber;
        private VehicleOption _tiltWheel;
        private VehicleOption _tintedGlass;
        private VehicleOption _transmission;

        public List<VehicleOption> AdditionalOptions
        {
            get { return _additionalOptions; }
            internal set { _additionalOptions = value; }
        }

        public VehicleOption AirConditioning
        {
            get { return _airConditioning; }
            internal set { _airConditioning = value; }
        }

        public VehicleOption Airbag
        {
            get { return _airbag; }
            internal set { _airbag = value; }
        }

        public VehicleOption AlloyWheels
        {
            get { return _alloyWheels; }
            internal set { _alloyWheels = value; }
        }

        public VehicleOption AntiLockBrakes
        {
            get { return _antiLockBrakes; }
            internal set { _antiLockBrakes = value; }
        }

        public VehicleOption ChildSeat
        {
            get { return _childSeat; }
            internal set { _childSeat = value; }
        }

        public VehicleOption ClimateControl
        {
            get { return _climateControl; }
            internal set { _climateControl = value; }
        }

        public VehicleOption CruiseControl
        {
            get { return _cruiseControl; }
            internal set { _cruiseControl = value; }
        }

        public string Description
        {
            get { return _description; }
            internal set { _description = value; }
        }

        public VehicleOption Door
        {
            get { return _door; }
            internal set { _door = value; }
        }

        public VehicleOption Drivetrain
        {
            get { return _drivetrain; }
            internal set { _drivetrain = value; }
        }

        public VehicleOption Engine
        {
            get { return _engine; }
            internal set { _engine = value; }
        }

        public VehicleOption ExteriorColor
        {
            get { return _exteriorColor; }
            internal set { _exteriorColor = value; }
        }

        public VehicleOption FrameDamage
        {
            get { return _frameDamage; }
            internal set { _frameDamage = value; }
        }

        public VehicleOption Fuel
        {
            get { return _fuel; }
            internal set { _fuel = value; }
        }

        public VehicleOption Interior
        {
            get { return _interior; }
            internal set { _interior = value; }
        }

        public VehicleOption InteriorColor
        {
            get { return _interiorColor; }
            internal set { _interiorColor = value; }
        }

        public double InvoicePrice
        {
            get { return _invoicePrice; }
            internal set { _invoicePrice = value; }
        }

        public string Mileage
        {
            get { return _mileage; }
            internal set { _mileage = value; }
        }

        public double MsrPrice
        {
            get { return _msrPrice; }
            internal set { _msrPrice = value; }
        }

        public VehicleOption PowerLocks
        {
            get { return _powerLocks; }
            internal set { _powerLocks = value; }
        }

        public VehicleOption PowerMirrors
        {
            get { return _powerMirrors; }
            internal set { _powerMirrors = value; }
        }

        public VehicleOption PowerSeats
        {
            get { return _powerSeats; }
            internal set { _powerSeats = value; }
        }

        public VehicleOption PowerSteering
        {
            get { return _powerSteering; }
            internal set { _powerSteering = value; }
        }

        public VehicleOption PowerWindows
        {
            get { return _powerWindows; }
            internal set { _powerWindows = value; }
        }

        public VehicleOption PriorPaint
        {
            get { return _priorPaint; }
            internal set { _priorPaint = value; }
        }

        public VehicleOption RearAirConditioning
        {
            get { return _rearAirConditioning; }
            internal set { _rearAirConditioning = value; }
        }

        public VehicleOption RearDefroster
        {
            get { return _rearDefroster; }
            internal set { _rearDefroster = value; }
        }

        public double RetailPrice
        {
            get { return _retailPrice; }
            internal set { _retailPrice = value; }
        }

        public VehicleOption Roof
        {
            get { return _roof; }
            internal set { _roof = value; }
        }

        public VehicleOption Stereo
        {
            get { return _stereo; }
            internal set { _stereo = value; }
        }

        public string StockNumber
        {
            get { return _stockNumber; }
            internal set { _stockNumber = value; }
        }

        public VehicleOption TiltWheel
        {
            get { return _tiltWheel; }
            internal set { _tiltWheel = value; }
        }

        public VehicleOption TintedGlass
        {
            get { return _tintedGlass; }
            internal set { _tintedGlass = value; }
        }

        public VehicleOption Transmission
        {
            get { return _transmission; }
            internal set { _transmission = value; }
        }
    }
}