using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;
using FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities;
using NUnit.Framework;
using Style=FirstLook.VehicleInformation.DomainModel.Customization.Model.Style;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Mappers
{
    [TestFixture]
    public class ChangeDescriptionOptionTest
    {
        private ChangeDescriptionOption _mapper;

        private IRegistry _registry;

        private IDataSession _session;

        [SetUp]
        public void SetUp()
        {
            _registry = RegistryFactory.GetRegistry();

            _registry.Register<IDataSessionManager, DataSessionManager>(
                ImplementationScope.Shared);

            _registry.Register<ICache, MemoryCache>(
                ImplementationScope.Shared);

            _registry.Register<Client.DomainModel.Clients.Module>();

            _registry.Register<Module>();

            _session = _registry.Resolve<IDataSessionManager>().CreateSession("Vehicle");

            _session.OpenConnection();

            _mapper = new ChangeDescriptionOption();

            _session.Items["Audit"] = _registry.Resolve<IRepository>().Create(Principal.Get("admin", "CAS"));
        }

        [TearDown]
        public void TearDown()
        {
            _session.Close();

            _session = null;

            _mapper = null;

            _registry.Dispose();
        }

        [Test]
        public void SystemOptionChangeDescription_Fetch()
        {
            _mapper.Fetch(new Style(1));
        }

        [Test]
        public void SystemOptionChangeDescription_Save()
        {
            IList<ChangeDescription<Option>> list = _mapper.Fetch(new Style(1));

            ChangeDescription<Option> description = new ChangeDescription<Option>();

            description.Entity = new OptionMemento(1, 1, "ABC");

            description.Description = "World";

            list.Add(description);

            _mapper.Save(list);
        }

        [Test]
        public void SystemOptionChangeDescription_Edit()
        {
            IList<ChangeDescription<Option>> list = _mapper.Fetch(new Style(1));

            if (list.Count > 0)
            {
                list[0].Description = "Hello";
            }

            _mapper.Save(list);
        }

        [Test]
        public void SystemOptionChangeDescription_Delete()
        {
            IList<ChangeDescription<Option>> list = _mapper.Fetch(new Style(1));

            if (list.Count > 0)
            {
                list.Clear();
            }

            _mapper.Save(list);
        }
    }
}
