using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;
using FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities;
using NUnit.Framework;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Mappers
{
    [TestFixture]
    public class ChangePositionCategoryHeaderTest
    {
        private ChangePositionCategoryHeader _mapper;

        private IRegistry _registry;

        private IDataSession _session;

        [SetUp]
        public void SetUp()
        {
            _registry = RegistryFactory.GetRegistry();

            _registry.Register<IDataSessionManager, DataSessionManager>(
                ImplementationScope.Shared);

            _registry.Register<ICache, MemoryCache>(
                ImplementationScope.Shared);

            _registry.Register<Client.DomainModel.Clients.Module>();

            _registry.Register<Module>();

            _session = _registry.Resolve<IDataSessionManager>().CreateSession("Vehicle");

            _session.OpenConnection();

            _session.Items["Audit"] = _registry.Resolve<IRepository>().Create(Principal.Get("admin", "CAS"));

            _mapper = new ChangePositionCategoryHeader();
        }

        [TearDown]
        public void TearDown()
        {
            _session.Close();

            _session = null;

            _mapper = null;

            _registry.Dispose();
        }

        [Test]
        public void SystemCategoryHeaderChangePosition_Fetch()
        {
            _mapper.Fetch();
        }

        [Test]
        public void SystemCategoryHeaderChangePosition_Save()
        {
            IList<ChangePosition<CategoryHeader>> list = _mapper.Fetch();

            ChangePosition<CategoryHeader> location = new ChangePosition<CategoryHeader>();

            location.Entity = new CategoryHeaderMemento(1);

            location.Position = 0;

            list.Add(location);

            _mapper.Save(list);
        }

        [Test]
        public void SystemCategoryHeaderChangePosition_Edit()
        {
            IList<ChangePosition<CategoryHeader>> list = _mapper.Fetch();

            if (list.Count > 0)
            {
                list[0].Position = 1;
            }

            _mapper.Save(list);
        }

        [Test]
        public void SystemCategoryHeaderChangePosition_Delete()
        {
            IList<ChangePosition<CategoryHeader>> list = _mapper.Fetch();

            if (list.Count > 0)
            {
                list.Clear();
            }

            _mapper.Save(list);
        }
    }
}
