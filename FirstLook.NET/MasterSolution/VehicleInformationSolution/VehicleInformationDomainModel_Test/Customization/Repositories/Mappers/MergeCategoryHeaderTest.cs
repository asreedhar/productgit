using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;
using FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities;
using NUnit.Framework;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Mappers
{
    [TestFixture]
    public class MergeCategoryHeaderTest
    {
        private MergeCategoryHeader _mapper;

        private IRegistry _registry;

        private IDataSession _session;

        [SetUp]
        public void SetUp()
        {
            _registry = RegistryFactory.GetRegistry();

            _registry.Register<IDataSessionManager, DataSessionManager>(
                ImplementationScope.Shared);

            _registry.Register<ICache, MemoryCache>(
                ImplementationScope.Shared);

            _registry.Register<Client.DomainModel.Clients.Module>();

            _registry.Register<Module>();

            _session = _registry.Resolve<IDataSessionManager>().CreateSession("Vehicle");

            _session.OpenConnection();

            _session.Items["Audit"] = _registry.Resolve<IRepository>().Create(Principal.Get("admin", "CAS"));

            _mapper = new MergeCategoryHeader();
        }

        [TearDown]
        public void TearDown()
        {
            _session.Close();

            _session = null;

            _mapper = null;

            _registry.Dispose();
        }

        [Test]
        public void SystemCategoryHeaderMerge_Fetch()
        {
            _mapper.Fetch();
        }

        [Test]
        public void SystemCategoryHeaderMerge_Save()
        {
            IList<Merge<CategoryHeader>> list = _mapper.Fetch();

            Merge<CategoryHeader> merge = new Merge<CategoryHeader>();

            merge.Entity = new CategoryHeaderMemento(1);

            merge.Parent = new CategoryHeaderMemento(2);

            list.Add(merge);

            _mapper.Save(list);
        }

        [Test]
        public void SystemCategoryHeaderMerge_Edit()
        {
            IList<Merge<CategoryHeader>> list = _mapper.Fetch();

            if (list.Count > 0)
            {
                list[0].Parent = new CategoryHeaderMemento(3);
            }

            _mapper.Save(list);
        }

        [Test]
        public void SystemCategoryHeaderMerge_Delete()
        {
            IList<Merge<CategoryHeader>> list = _mapper.Fetch();

            if (list.Count > 0)
            {
                list.Clear();
            }

            _mapper.Save(list);
        }
    }
}
