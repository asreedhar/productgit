using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using NUnit.Framework;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Gateways
{
    [TestFixture]
    public class CustomizationStoreTest
    {
        private const int TestUser = 666;

        private ICustomizationStore _store;

        private IRegistry _registry;

        private IDataSession _session;

        [SetUp]
        public void SetUp()
        {
            _registry = RegistryFactory.GetRegistry();

            _registry.Register<IDataSessionManager, DataSessionManager>(
                ImplementationScope.Shared);

            _registry.Register<ICache, MemoryCache>(
                ImplementationScope.Shared);

            _store = new DbCustomizationStore();

            _session = _registry.Resolve<IDataSessionManager>().CreateSession("Chrome");

            _session.OpenConnection();
        }

        [TearDown]
        public void TearDown()
        {
            _session.Close();

            _session = null;

            _store = null;

            RegistryFactory.GetRegistry().Dispose();
        }

        //[Test]
        //public void Customizations_Insert()
        //{
        //    Audit.Repositories.Entities.Audit audit = new Audit.Repositories.Entities.Audit(TestUser);

        //    AuditEntity auditEntity = CustomizationStoreHelper.InsertAudit(_store, _transaction, audit);

        //    AuditRow auditRow = new AuditRow(true, false, false);

        //    AuditRowEntity auditRowEntity = CustomizationStoreHelper.InsertAuditRow(_store, _transaction, auditEntity, auditRow);

        //    Customizations<Category> customizations = new Customizations<Category>(Owner.System);

        //    CustomizationsEntity<Category> customizationsEntity = CustomizationStoreHelper.InsertCustomizations<Category>(_store, _transaction, auditRowEntity, customizations.Owner);

        //    Assert.IsNotNull(customizationsEntity);

        //    Assert.Greater(customizationsEntity.Id, 0, "Non-Zero ID");

        //    Assert.AreEqual(customizations.Owner, customizationsEntity.Owner);
        //}

        //[Test]
        //public void Customization_Insert()
        //{
        //    Audit.Repositories.Entities.Audit audit = new Audit.Repositories.Entities.Audit(TestUser);

        //    AuditEntity auditEntity = CustomizationStoreHelper.InsertAudit(_store, _transaction, audit);

        //    AuditRow ar = new AuditRow(true, false, false);

        //    AuditRowEntity are1 = CustomizationStoreHelper.InsertAuditRow(_store, _transaction, auditEntity, ar);

        //    Customizations<Category> customizations = new Customizations<Category>(Owner.System);

        //    CustomizationsEntity<Category> customizationsEntity = CustomizationStoreHelper.InsertCustomizations<Category>(_store, _transaction, are1, customizations.Owner);

        //    AuditRowEntity are2 = CustomizationStoreHelper.InsertAuditRow(_store, _transaction, auditEntity, ar);

        //    CustomizationEntity entity = CustomizationStoreHelper.InsertCustomization(_store, _transaction, are2, customizationsEntity.Id, CustomizationType.ChangeName);

        //    Assert.IsNotNull(entity);

        //    Assert.AreEqual(CustomizationType.ChangeName, entity.Type);

        //    Assert.Greater(entity.Id, 0);
        //}

        //[Test]
        //public void Memento_Insert()
        //{
        //    Audit.Repositories.Entities.Audit audit = new Audit.Repositories.Entities.Audit(TestUser);

        //    AuditEntity auditEntity = CustomizationStoreHelper.InsertAudit(_store, _transaction, audit);

        //    AuditRow ar = new AuditRow(true, false, false);

        //    AuditRowEntity are1 = CustomizationStoreHelper.InsertAuditRow(_store, _transaction, auditEntity, ar);

        //    MementoEntity entity = CustomizationStoreHelper.InsertMemento(_store, _transaction, are1, MementoType.Category);

        //    Assert.IsNotNull(entity);

        //    Assert.AreEqual(MementoType.Category, entity.Type);

        //    Assert.Greater(entity.Id, 0);
        //}

        //[Test]
        //public void Memento_Category_Insert()
        //{
        //    Audit.Repositories.Entities.Audit audit = new Audit.Repositories.Entities.Audit(TestUser);

        //    AuditEntity auditEntity = CustomizationStoreHelper.InsertAudit(_store, _transaction, audit);

        //    AuditRow ar = new AuditRow(true, false, false);

        //    AuditRowEntity are1 = CustomizationStoreHelper.InsertAuditRow(_store, _transaction, auditEntity, ar);

        //    MementoEntity memento = CustomizationStoreHelper.InsertMemento(_store, _transaction, are1, MementoType.Category);

        //    CategoryMemento category = new CategoryMemento(123);

        //    CategoryMementoEntity categoryEntity = CustomizationStoreHelper.InsertMemento_Category(_store, _transaction, memento, category);

        //    Assert.IsNotNull(categoryEntity);

        //    Assert.AreEqual(category.Id, categoryEntity.Id);
        //}
    }
}