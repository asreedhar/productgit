using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using NUnit.Framework;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.VinMatch
{
    [TestFixture]
    public class VinEquipmentGatewayTest
    {
        private IRegistry _registry;

        private VinEquipmentGateway _gateway;

        private IDataSession _session;

        [SetUp]
        public void SetUp()
        {
            _registry = RegistryFactory.GetRegistry();

            _registry.Register<IDataSessionManager, DataSessionManager>(
                ImplementationScope.Shared);

            _registry.Register<ICache, MemoryCache>(
                ImplementationScope.Shared);

            _registry.Register<IVinMatchStore, DbVinMatchStore>(
                ImplementationScope.Shared);

            _gateway = new VinEquipmentGateway();

            _session = _registry.Resolve<IDataSessionManager>().CreateSession("Chrome");

            _session.OpenConnection();
        }

        [TearDown]
        public void TearDown()
        {
            _gateway = null;

            _registry.Dispose();
        }

        [Test]
        public void TestFetch()
        {
            _gateway.Fetch(47477, "US");
        }
    }
}
