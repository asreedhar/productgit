using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using NUnit.Framework;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.VinMatch
{
    [TestFixture]
    public class VinPatternGatewayTest
    {
        private IRegistry _registry;

        private VinPatternGateway _gateway;

        private IDataSession _session;

        [SetUp]
        public void SetUp()
        {
            _registry = RegistryFactory.GetRegistry();

            _registry.Register<IDataSessionManager, DataSessionManager>(
                ImplementationScope.Shared);

            _registry.Register<ICache, MemoryCache>(
                ImplementationScope.Shared);

            _registry.Register<IVinMatchStore, DbVinMatchStore>(
                ImplementationScope.Shared);

            _gateway = new VinPatternGateway();

            _session = _registry.Resolve<IDataSessionManager>().CreateSession("Chrome");

            _session.OpenConnection();
        }

        [TearDown]
        public void TearDown()
        {
            _gateway = null;

            _registry.Dispose();
        }

        [Test]
        public void TestFetch()
        {
            _gateway.Fetch("1GYEK63N36R123736", "US");
        }
    }
}
