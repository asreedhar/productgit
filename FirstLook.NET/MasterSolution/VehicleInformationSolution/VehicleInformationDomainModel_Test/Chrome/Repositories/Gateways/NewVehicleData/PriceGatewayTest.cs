using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using NUnit.Framework;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.NewVehicleData
{
    [TestFixture]
    public class PriceGatewayTest
    {
        private PriceGateway _gateway;

        private IRegistry _registry;

        [SetUp]
        public void SetUp()
        {
            _registry = RegistryFactory.GetRegistry();

            _registry.Register<IDataSessionManager, DataSessionManager>(
                ImplementationScope.Shared);

            _registry.Register<ICache, MemoryCache>(
                ImplementationScope.Shared);

            _registry.Register<INewVehicleDataStore, DbNewVehicleDataStore>(
                ImplementationScope.Shared);

            _gateway = new PriceGateway();
        }

        [TearDown]
        public void TearDown()
        {
            _gateway = null;

            _registry.Dispose();
        }

        [Test]
        public void TestFetch()
        {
            _gateway.Fetch(277128);
        }
    }
}
