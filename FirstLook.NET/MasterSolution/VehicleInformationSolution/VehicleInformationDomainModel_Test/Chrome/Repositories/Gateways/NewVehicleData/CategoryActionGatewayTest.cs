using System.Collections.Generic;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using NUnit.Framework;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.NewVehicleData
{
    [TestFixture]
    public class CategoryActionGatewayTest
    {
        private OptionGateway _options;

        private StandardGateway _standards;

        private CategoryActionGateway _gateway;

        private IRegistry _registry;

        [SetUp]
        public void SetUp()
        {
            _registry = RegistryFactory.GetRegistry();

            _registry.Register<IDataSessionManager, DataSessionManager>(
                ImplementationScope.Shared);

            _registry.Register<ICache, MemoryCache>(
                ImplementationScope.Shared);

            _registry.Register<INewVehicleDataStore, DbNewVehicleDataStore>(
                ImplementationScope.Shared);

            _gateway = new CategoryActionGateway();

            _options = new OptionGateway();

            _standards = new StandardGateway();
        }

        [TearDown]
        public void TearDown()
        {
            _gateway = null;

            _options = null;

            _standards = null;

            _registry.Dispose();
        }

        [Test]
        public void TestLoad()
        {
            _gateway.Load(277128);
        }

        [Test]
        public void TestFetchStandards()
        {
            const int styleId = 277128;

            IList<Standard> standardList = _standards.Fetch(styleId);

            foreach (Standard standard in standardList)
            {
                _gateway.Fetch(
                    styleId,
                    standard.Sequence,
                    CategoryActionGateway.FeatureType.Standard);
            }
        }

        [Test]
        public void TestFetchOptions()
        {
            const int styleId = 277128;

            IList<Option> optionList = _options.Fetch(styleId);

            foreach (Option option in optionList)
            {
                _gateway.Fetch(
                    styleId,
                    option.Sequence,
                    CategoryActionGateway.FeatureType.Option);
            }
        }
    }
}
