using System.Xml;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects.Instruction;
using FirstLook.VehicleInformation.DomainModel.Specification.Commands;
using FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects;
using NUnit.Framework;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Commands
{
    [TestFixture]
    public class InstructionSetSaveCommandTest
    {
        private const string Vin = "1G1ZE5EB4AF110623";

        private IRegistry _registry;

        [SetUp]
        public void SetUp()
        {
            _registry = RegistryFactory.GetRegistry();

            _registry.Register<IDataSessionManager, DataSessionManager>(
                ImplementationScope.Shared);

            _registry.Register<ICache, MemoryCache>(
                ImplementationScope.Shared);

            _registry.Register<Client.DomainModel.Clients.Module>();

            _registry.Register<Notification.Module>();
        }

        [TearDown]
        public void TearDown()
        {
            _registry.Dispose();
        }

        [Test]
        public void Test()
        {
            // (A) save style

            ISpecificationCommandFactory specificationFactory = _registry.Resolve<ISpecificationCommandFactory>();

            ICommand<StyleDto, IdentityContextDto<StyleDto>> saveStyle = specificationFactory.CreateStyleSaveCommand();

            StyleDto style = new StyleDto();

            style.Document = new StyleProviderDocumentDto();
            style.Document.Document = GetTestStyleDocument();
            style.Document.Version = 1;

            style.Reference = new StyleProviderDocumentDto();
            style.Reference.Document = GetTestReferenceDocument();
            style.Reference.Version = 2;

            style.Summary = new StyleSummaryDto();
            style.Summary.BodyStyleName = "B";
            style.Summary.DivisionName = "D";
            style.Summary.ManufacturerName = "M";
            style.Summary.ModelName = "O";
            style.Summary.ModelYear = 2010;
            style.Summary.StyleName = "S";
            style.Summary.SubdivisionName = "U";
            style.Summary.TrimName = "T";

            StyleIdentityChromeDto identity = new StyleIdentityChromeDto();
            identity.ProviderId = 1;
            identity.ProviderName = "Chrome";
            identity.Id = 789123;

            style.Identity = identity;

            // (B) save instruction set

            INotificationCommandFactory notificationFactory = _registry.Resolve<INotificationCommandFactory>();

            ICommand<InstructionSetDto, IdentityContextDto<InstructionSetDto>> saveInstructionSet = notificationFactory.CreateInstructionSetSaveCommand();

            InstructionSetDto instructionSet = new InstructionSetDto();

            instructionSet.Vehicle = new VehicleIdentificationDto();
            instructionSet.Vehicle.Vin = Vin;

            instructionSet.SpecificationProvider = new SpecificationProviderDto();
            instructionSet.SpecificationProvider.Id = 1;
            instructionSet.SpecificationProvider.Name = "Inventory Load";

            instructionSet.InstructionSet = new InstructionSet();

            instructionSet.InstructionSet.Version = 1.0m;

            StyleCode code = new StyleCode();

            code.Code = "ABC123";

            instructionSet.InstructionSet.StyleCode = code;

            Option option = new Option();

            option.Code = "DEF456";

            instructionSet.InstructionSet.Options = new OptionCollection();

            instructionSet.InstructionSet.Options.Add(option);

            instructionSet.CountInformation = 3;
            instructionSet.CountCategories = 2;
            instructionSet.CountOptions = 0;

            // (C) run them together

            ICommandTransaction transaction = notificationFactory.CreateTransactionCommand();

            Command<StyleDto, IdentityContextDto<StyleDto>> styleCommand = new Command<StyleDto, IdentityContextDto<StyleDto>>(
                saveStyle,
                new IdentityContextDto<StyleDto>
                {
                    Arguments = style,
                    Identity = new IdentityDto
                    {
                        AuthorityName = "CAS",
                        Name = "admin"
                    }
                });

            Command<InstructionSetDto, IdentityContextDto<InstructionSetDto>> instructionCommand = new Command<InstructionSetDto, IdentityContextDto<InstructionSetDto>>(
                saveInstructionSet,
                new IdentityContextDto<InstructionSetDto>
                {
                    Arguments = instructionSet,
                    Identity = new IdentityDto
                    {
                        AuthorityName = "CAS",
                        Name = "admin"
                    }
                });

            Chain chain = new Chain(instructionCommand, styleCommand);

            CommandBatch batch = new CommandBatch(
                styleCommand,
                chain,
                instructionCommand);

            transaction.Execute(batch);
        }

        protected XmlDocument GetTestStyleDocument()
        {
            XmlDocument document = new XmlDocument();

            document.AppendChild(document.CreateElement("Style"));

            return document;
        }

        protected XmlDocument GetTestReferenceDocument()
        {
            XmlDocument document = new XmlDocument();

            document.AppendChild(document.CreateElement("Reference"));

            return document;
        }

        class Chain : CommandChain<StyleDto, IdentityContextDto<InstructionSetDto>>
        {
            public Chain(ICommandParameter<IdentityContextDto<InstructionSetDto>> parameter, ICommandResult<StyleDto> result)
                : base(parameter, result)
            {
            }

            public override void Execute()
            {
                Parameter.Parameter.Arguments.StyleKey = Result.Result.Key;
            }
        }
    }
}