using System;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects.Information;
using NUnit.Framework;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Commands
{
    [TestFixture]
    public class InformationSaveCommandTest
    {
        const string Vin = "1G1ZE5EB4AF110623";

        private IRegistry _registry;

        [SetUp]
        public void SetUp()
        {
            _registry = RegistryFactory.GetRegistry();

            _registry.Register<IDataSessionManager, DataSessionManager>(
                ImplementationScope.Shared);

            _registry.Register<ICache, MemoryCache>(
                ImplementationScope.Shared);

            _registry.Register<Client.DomainModel.Clients.Module>();

            _registry.Register<Notification.Module>();
        }

        [TearDown]
        public void TearDown()
        {
            _registry.Dispose();
        }

        [Test]
        public void Test()
        {
            INotificationCommandFactory factory = _registry.Resolve<INotificationCommandFactory>();

            ICommand<InformationKeyDto, IdentityContextDto<InformationDto>> command = factory.CreateInformationSaveCommand();

            InformationDto information = new InformationDto();

            information.Vehicle = new VehicleIdentificationDto();
            information.Vehicle.Vin = Vin;

            information.SpecificationProvider = new SpecificationProviderDto();
            information.SpecificationProvider.Id = 1;
            information.SpecificationProvider.Name = "Inventory Load";

            information.Information = new Information();

            information.Information.Head = new InformationHead();

            information.Information.Head.Sender = new Sender();
            information.Information.Head.Sender.Id = 1;
            information.Information.Head.Sender.Name = "Inventory Load";
            information.Information.Head.Sent = DateTime.Now;

            information.Information.Body = new InformationBody();

            information.Information.Body.VehicleSpecification = new VehicleSpecification();
            information.Information.Body.VehicleSpecification.Vin = Vin;
            information.Information.Body.VehicleSpecification.Version = 1.0m;
            information.Information.Body.VehicleSpecification.IsFleet = false;

            information.Information.Body.VehicleSpecification.Description = new VehicleDescription();
            information.Information.Body.VehicleSpecification.Description.ModelYear = 2010;
            information.Information.Body.VehicleSpecification.Description.Make = "Chevrolet";
            information.Information.Body.VehicleSpecification.Description.Model = "Malibu";
            information.Information.Body.VehicleSpecification.Description.Trim = "LX";

            VehicleColor color = new VehicleColor();

            color.Description = "Green";

            information.Information.Body.VehicleSpecification.Colors = new VehicleColorCollection();
            information.Information.Body.VehicleSpecification.Colors.Add(color);

            VehicleOption option = new VehicleOption();

            option.Name = "AM/FM Radio";

            information.Information.Body.VehicleSpecification.Options = new VehicleOptionCollection();
            information.Information.Body.VehicleSpecification.Options.Add(option);

            InformationKeyDto key = command.Execute(new IdentityContextDto<InformationDto>
            {
                Arguments = information,
                Identity = new IdentityDto
                {
                    AuthorityName = "CAS",
                    Name = "admin"
                }
            });

            Assert.IsNotNull(key);
        }
    }
}