using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects;
using NUnit.Framework;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Commands
{
    [TestFixture]
    public class InstructionSetFetchCommandTest
    {
        private const string Vin = "1G1ZE5EB4AF110623";

        private IRegistry _registry;

        [SetUp]
        public void SetUp()
        {
            _registry = RegistryFactory.GetRegistry();

            _registry.Register<IDataSessionManager, DataSessionManager>(
                ImplementationScope.Shared);

            _registry.Register<ICache, MemoryCache>(
                ImplementationScope.Shared);

            _registry.Register<Client.DomainModel.Module>();

            _registry.Register<Notification.Module>();
        }

        [TearDown]
        public void TearDown()
        {
            _registry.Dispose();
        }

        [Test]
        public void Test()
        {
            INotificationCommandFactory factory = _registry.Resolve<INotificationCommandFactory>();

            ICommand<InstructionSetDto, IdentityContextDto<InstructionSetCriteriaDto>> command = factory.CreateInstructionSetFetchCommand();

            InstructionSetCriteriaDto input = new InstructionSetCriteriaDto();

            input.SpecificationProvider = new SpecificationProviderDto();
            input.SpecificationProvider.Id = 1;
            input.SpecificationProvider.Name = "Chrome";

            input.Vehicle = new VehicleIdentificationDto();
            input.Vehicle.Vin = Vin;

            IdentityContextDto<InstructionSetCriteriaDto> context = new IdentityContextDto<InstructionSetCriteriaDto>
            {
                Identity = new IdentityDto
                {
                    AuthorityName = "CAS",
                    Name = "admin"
                },
                Arguments = input
            };

            InstructionSetDto output = command.Execute(context);

            Assert.IsNotNull(output);
        }
    }
}