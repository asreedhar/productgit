using System.Xml;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects.Vehicle;
using NUnit.Framework;
using VehicleDescription=
    FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects.Vehicle.VehicleDescription;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Commands
{
    [TestFixture]
    public class SpecificationFetchCommandTest
    {
        const string Vin = "1G1ZE5EB4AF110623";

        private IRegistry _registry;

        [SetUp]
        public void SetUp()
        {
            _registry = RegistryFactory.GetRegistry();

            _registry.Register<IDataSessionManager, DataSessionManager>(
                ImplementationScope.Shared);

            _registry.Register<ICache, MemoryCache>(
                ImplementationScope.Shared);

            _registry.Register<Client.DomainModel.Clients.Module>();

            _registry.Register<Client.DomainModel.Vehicles.Module>();

            _registry.Register<Specification.Module>();
        }

        [TearDown]
        public void TearDown()
        {
            _registry.Dispose();
        }

        [Test]
        public void TestFetchDealer()
        {
            IBroker broker = Broker.Get("admin", "CAS", "CAS", 100147);

            VehicleIdentification vehicle = _registry.Resolve<IVehicleRepository>().Identification("1G1ZE5EB4AF110623");

            ISpecificationCommandFactory factory = _registry.Resolve<ISpecificationCommandFactory>();

            ICommand<SpecificationFetchResultsDto, IdentityContextDto<SpecificationFetchArgumentsDto>> command = factory.CreateSpecificationFetchCommand();

            SpecificationFetchArgumentsDto criteria = new SpecificationFetchArgumentsDto();

            criteria.Broker = new BrokerIdentificationDto();
            criteria.Broker.Handle = broker.Handle;
            criteria.Broker.Id = broker.Id;

            criteria.Vehicle = new VehicleIdentificationDto();
            criteria.Vehicle.Vin = vehicle.Vin;
            criteria.Vehicle.Id = vehicle.Id;

            SpecificationFetchResultsDto output = command.Execute(
                new IdentityContextDto<SpecificationFetchArgumentsDto>()
                {
                    Arguments = criteria,
                    Identity = new IdentityDto
                    {
                        AuthorityName = "CAS",
                        Name = "admin"
                    }
                });

            Assert.IsNotNull(output);

            Assert.IsNotNull(output.Specification);
        }

        [Test]
        public void TestFetchSystem()
        {
            ISpecificationCommandFactory factory = _registry.Resolve<ISpecificationCommandFactory>();

            ICommand<SpecificationFetchResultsDto, IdentityContextDto<SpecificationFetchArgumentsDto>> command = factory.CreateSpecificationFetchCommand();

            SpecificationFetchArgumentsDto criteria = new SpecificationFetchArgumentsDto();

            criteria.Vehicle = new VehicleIdentificationDto();
            criteria.Vehicle.Vin = Vin;

            command.Execute(
                new IdentityContextDto<SpecificationFetchArgumentsDto>()
                {
                    Arguments = criteria,
                    Identity = new IdentityDto
                    {
                        AuthorityName = "CAS",
                        Name = "admin"
                    }
                });
        }

        [Test]
        public void TestSaveDealer()
        {
            IBroker broker = Broker.Get("admin", "CAS", "CAS", 100147);

            VehicleIdentification vehicle = _registry.Resolve<IVehicleRepository>().Identification("1G1ZE5EB4AF110623");

            ISpecificationCommandFactory factory = _registry.Resolve<ISpecificationCommandFactory>();

            ICommand<SpecificationDto, IdentityContextDto<SpecificationDto>> command = factory.CreateSpecificationSaveCommand();

            SpecificationDto input = new SpecificationDto();

            input.Dealer = new BrokerIdentificationDto();
            input.Dealer.Handle = broker.Handle;
            input.Dealer.Id = broker.Id;

            input.Vehicle = new VehicleIdentificationDto();
            input.Vehicle.Vin = vehicle.Vin;
            input.Vehicle.Id = vehicle.Id;

            input.BuildSpecification = GetTestBuildSpecification(Vin);
            input.VehicleSpecification = GetTestVehicleSpecification(Vin);

            input.Style = new StyleDto();
            input.Style.Document = new StyleProviderDocumentDto();
            input.Style.Document.Document = GetTestStyleDocument();
            input.Style.Document.Version = 1;

            input.Style.Reference = new StyleProviderDocumentDto();
            input.Style.Reference.Document = GetTestReferenceDocument();
            input.Style.Reference.Version = 2;

            input.Style.Summary = new StyleSummaryDto();
            input.Style.Summary.BodyStyleName = "B";
            input.Style.Summary.DivisionName = "D";
            input.Style.Summary.ManufacturerName = "M";
            input.Style.Summary.ModelName = "O";
            input.Style.Summary.ModelYear = 2010;
            input.Style.Summary.StyleName = "S";
            input.Style.Summary.SubdivisionName= "U";
            input.Style.Summary.TrimName = "T";

            StyleIdentityChromeDto identity = new StyleIdentityChromeDto();
            identity.ProviderId = 1;
            identity.ProviderName = "Chrome";
            identity.Id = 123456;

            input.Style.Identity = identity;

            command.Execute(
                new IdentityContextDto<SpecificationDto>()
                {
                    Arguments = input,
                    Identity = new IdentityDto
                    {
                        AuthorityName = "CAS",
                        Name = "admin"
                    }
                });
        }

        protected XmlDocument GetTestStyleDocument()
        {
            XmlDocument document = new XmlDocument();

            document.AppendChild(document.CreateElement("Style"));

            return document;
        }

        protected XmlDocument GetTestReferenceDocument()
        {
            XmlDocument document = new XmlDocument();

            document.AppendChild(document.CreateElement("Reference"));

            return document;
        }

        protected XmlDocument GetTestBuildSpecification(string vin)
        {
            XmlDocument document = new XmlDocument();

            XmlNode node = document.AppendChild(document.CreateElement("Build"));

            XmlAttribute attr = node.Attributes.Append(document.CreateAttribute("VIN"));

            attr.Value = vin;

            return document;
        }

        protected VehicleSpecification GetTestVehicleSpecification(string vin)
        {
            VehicleSpecification specification = new VehicleSpecification();

            specification.Vin = vin;
            specification.Version = 1.0m;
            specification.IsFleet = false;

            specification.Description = new VehicleDescription();
            specification.Description.BodyStyle = "Wibble";
            specification.Description.DriveTrain = new VehicleDriveTrain();
            specification.Description.DriveTrain.DriveTrainAxle = VehicleDriveTrainAxle.Front;
            specification.Description.DriveTrain.DriveTrainType = VehicleDriveTrainType.TwoWheelDrive;
            specification.Description.DriveTrain.Name = "Wobble";

            specification.Description.Engine = new VehicleEngine();
            specification.Description.Engine.Capacity = 4;
            specification.Description.Engine.CylinderCount = 8;
            specification.Description.Engine.CylinderLayout = VehicleEngineCylinderLayout.Flat;
            specification.Description.Engine.EngineType = VehicleEngineType.Piston;
            specification.Description.Engine.Name = "Flibble";

            specification.Description.Model = new VehicleModel();
            specification.Description.Model.Division = "D";
            specification.Description.Model.Manufacturer = "M";
            specification.Description.Model.Model = "O";
            specification.Description.Model.ModelYear = 2010;
            specification.Description.Model.Subdivision = "S";

            specification.Description.PassengerCapacity = 8;
            specification.Description.PassengerDoors = 4;
            specification.Description.StyleCode = "ABC123";
            specification.Description.StyleName = "Manual [NATL]";

            specification.Description.Transmission = new VehicleTransmission();
            specification.Description.Transmission.GearCount = 8;
            specification.Description.Transmission.Name = "Automatic 8 Speed";
            specification.Description.Transmission.TransmissionType = VehicleTransmissionType.Automatic;

            specification.Description.Trim = "LX";

            return specification;
        }
    }
}