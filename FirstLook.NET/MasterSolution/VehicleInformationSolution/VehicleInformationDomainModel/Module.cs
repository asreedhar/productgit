using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleInformation.DomainModel
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<Chrome.Module>();

            registry.Register<Customization.Module>();

            registry.Register<Notification.Module>();

            registry.Register<Specification.Module>();
        }
    }
}
