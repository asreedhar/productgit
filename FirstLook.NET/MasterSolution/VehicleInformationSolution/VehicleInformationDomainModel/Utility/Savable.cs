using System;
using System.ComponentModel;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;

namespace FirstLook.VehicleInformation.DomainModel.Utility
{
    public class Savable
    {
        private bool _new, _deleted, _dirty;

        public bool IsNew
        {
            get { return _new; }
            set { _new = value; }
        }

        public bool IsDeleted
        {
            get { return _deleted; }
            set { _deleted = value; }
        }

        public bool IsDirty
        {
            get { return _dirty; }
            set { _dirty = value; }
        }

        public Savable()
        {
            IsDirty = true;
            IsNew = true;
        }

        public void MarkNew()
        {
            IsNew = true;
            IsDeleted = false;
            MarkDirty();
        }

        public void MarkOld()
        {
            IsNew = false;
            MarkClean();
        }

        public void MarkDeleted()
        {
            IsDeleted = true;
            MarkDirty();
        }

        public void MarkDirty()
        {
            IsDirty = true;
        }

        public void MarkClean()
        {
            IsDirty = false;
        }

        public void PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            MarkDirty();
        }
    }

    public interface ISavable
    {
        AuditRow AuditRow { get; }

        bool IsNew { get; }

        bool IsDeleted { get; }

        bool IsDirty { get; }

        void MarkDeleted();

        void OnSaved(object sender, SavedEventArgs e);
    }

    public class SavedEventArgs : EventArgs
    {
        private readonly AuditRow _auditRow;

        public SavedEventArgs(AuditRow auditRow)
        {
            _auditRow = auditRow;
        }

        public AuditRow AuditRow
        {
            get { return _auditRow; }
        }
    }
}