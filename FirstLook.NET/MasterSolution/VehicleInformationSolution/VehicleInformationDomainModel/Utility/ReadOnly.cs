using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;

namespace FirstLook.VehicleInformation.DomainModel.Utility
{
    public class ReadOnly
    {
        private readonly List<string> _propertyNames;

        public ReadOnly(params string[] propertyNames)
        {
            _propertyNames = new List<string>();

            foreach (string propertyName in propertyNames)
            {
                _propertyNames.Add(propertyName);
            }
        }

        public void OnPropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            if (_propertyNames.Contains(e.PropertyName))
            {
                string message = string.Format(
                    CultureInfo.InvariantCulture,
                    "Property '{0}' is read-only",
                    e.PropertyName);

                throw new NotSupportedException(message);
            }
        }

        public static void Immutable(object sender, PropertyChangingEventArgs e)
        {
            throw new NotSupportedException("Object is read-only");
        }
    }
}