using System;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Gateways;

namespace FirstLook.VehicleInformation.DomainModel.Utility
{
    public abstract class MapperBase
    {
        private readonly AuditRowGateway _auditRowGateway = new AuditRowGateway();

        protected AuditRowGateway AuditRowGateway
        {
            get { return _auditRowGateway; }
        }

        protected AuditRow Insert()
        {
            return AuditRowGateway.Insert(DateTime.Now, true, false, false);
        }

        protected AuditRow Update(AuditRow row)
        {
            return AuditRowGateway.Insert(row, DateTime.Now, false, true, false);
        }

        protected AuditRow Delete(AuditRow row)
        {
            return AuditRowGateway.Insert(row, DateTime.Now, false, false, true);
        }
    }
}
