using System;
using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Commands
{
    public class CustomizeStandardInfo : ICustomizeStandardInfo, ICustomizationVisitor<Standard>
    {
        private readonly ICustomize<Standard> _customize;

        private Standard _entity;

        private StandardDto _info;

        public CustomizeStandardInfo(ICustomize<Standard> customize)
        {
            _customize = customize;
        }

        public Standard Entity
        {
            set { _entity = value; }
        }

        public void Apply(Standard standard, StandardDto info)
        {
            _entity = standard;

            _info = info;

            _customize.Apply(this);
        }

        public void Visit(ChangeName<Standard> customization)
        {
            if (customization.Entity.Equals(_entity))
            {
                _info.Name = customization.Name;
            }
        }

        public void Visit(ChangeDescription<Standard> customization)
        {
            throw new NotSupportedException();
        }

        public void Visit(ChangeLocation<Standard> customization)
        {
            throw new NotSupportedException();
        }

        public void Visit(ChangePosition<Standard> customization)
        {
            throw new NotSupportedException();
        }

        public void Visit(Associate<Standard> customization)
        {
            throw new NotSupportedException();
        }

        public void Visit(Hide<Standard> customization)
        {
            throw new NotSupportedException();
        }

        public void Visit(Merge<Standard> customization)
        {
            throw new NotSupportedException();
        }

        public void Visit(Package<Standard> customization)
        {
            throw new NotSupportedException();
        }
    }
}
