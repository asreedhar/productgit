using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Commands
{
    public interface ICustomizeStandardInfo
    {
        void Apply(Standard option, StandardDto info);
    }
}
