using System;
using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Commands
{
    public class CustomizeOptionInfo : ICustomizationVisitor<Option>, ICustomizeOptionInfo
    {
        private Option _entity;

        private OptionDto _info;

        private readonly ICustomize<Option> _customize;

        public CustomizeOptionInfo(ICustomize<Option> customize)
        {
            _customize = customize;
        }

        public Option Entity
        {
            set { _entity = value; }
        }

        public void Apply(Option option, OptionDto info)
        {
            _entity = option;

            _info = info;

            _customize.Apply(this);
        }
        
        public void Visit(ChangeName<Option> customization)
        {
            if (customization.Entity.Equals(_entity))
            {
                _info.Name = customization.Name;
            }
        }

        public void Visit(ChangeDescription<Option> customization)
        {
            if (customization.Entity.Equals(_entity))
            {
                _info.Description = customization.Description;
            }
        }

        public void Visit(ChangeLocation<Option> customization)
        {
            throw new NotSupportedException();
        }

        public void Visit(ChangePosition<Option> customization)
        {
            throw new NotSupportedException();
        }

        public void Visit(Associate<Option> customization)
        {
            // throw new NotImplementedException();
        }

        public void Visit(Hide<Option> customization)
        {
            throw new NotSupportedException();
        }

        public void Visit(Merge<Option> customization)
        {
            throw new NotSupportedException();
        }

        public void Visit(Package<Option> customization)
        {
            // throw new NotImplementedException();
        }
    }
}
