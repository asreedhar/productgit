using System.Collections.Generic;
using FirstLook.Common.Core.Memento;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Commands
{
    public class CustomizeChain<TEntity> : ICustomize<TEntity>
        where TEntity : IOriginator<TEntity>
    {
        private readonly List<ICustomize<TEntity>> _customizers = new List<ICustomize<TEntity>>();

        public CustomizeChain(params ICustomize<TEntity>[] customizers)
        {
            foreach (ICustomize<TEntity> customize in customizers)
            {
                Add(customize);
            }
        }

        public void Add(ICustomize<TEntity> customize)
        {
            _customizers.Add(customize);
        }

        public void Apply(ICustomizationVisitor<TEntity> visitor)
        {
            foreach (ICustomize<TEntity> customize in _customizers)
            {
                customize.Apply(visitor);
            }
        }
    }
}
