using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Commands
{
    public interface ICustomizeClassification
    {
        void Apply(IHeader header, ClassificationHeaderDto classification);
    }
}