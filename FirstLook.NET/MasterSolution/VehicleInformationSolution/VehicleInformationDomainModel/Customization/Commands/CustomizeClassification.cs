using System;
using FirstLook.Common.Core.Memento;
using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Commands
{
    public class CustomizeClassification<TEntity> : ICustomizeClassification, ICustomizationVisitor<TEntity>
        where TEntity : class, IOriginator<TEntity>, IHeader
    {
        private readonly ICustomize<TEntity> _customize;

        private ClassificationHeaderDto _classification;

        private TEntity _header;

        public CustomizeClassification(ICustomize<TEntity> customize)
        {
            _customize = customize;
        }

        public void Apply(IHeader header, ClassificationHeaderDto classification)
        {
            TEntity entity = header as TEntity;

            if (header is TEntity)
            {
                Entity = entity;

                _classification = classification;

                _customize.Apply(this);
            }
        }

        public TEntity Entity
        {
            set { _header = value; }
        }

        public void Visit(ChangeName<TEntity> customization)
        {
            if (customization.Entity.Equals(_header))
            {
                _classification.Name = customization.Name;
            }
        }

        public void Visit(ChangeDescription<TEntity> customization)
        {
            throw new NotSupportedException();
        }

        public void Visit(ChangeLocation<TEntity> customization)
        {
            if (customization.Entity.Equals(_header))
            {
                _classification.Location = (LocationDto) ((byte) customization.Location);
            }
        }

        public void Visit(ChangePosition<TEntity> customization)
        {
            if (customization.Entity.Equals(_header))
            {
                _classification.Position = customization.Position;
            }
        }

        public void Visit(Associate<TEntity> customization)
        {
            throw new NotSupportedException();
        }

        public void Visit(Hide<TEntity> customization)
        {
            if (customization.Entity.Equals(_header))
            {
                _classification.Visible = false;
            }
        }

        public void Visit(Merge<TEntity> customization)
        {
            if (customization.Entity.Equals(_header))
            {
                _classification.MergeInfo.ChildOf = delegate(MergeInfo parent)
                      {
                          TEntity entity = parent.Self as TEntity;

                          return entity != null
                                     ? customization.Parent.Equals(entity)
                                     : false;
                      };
            }

            if (customization.Parent.Equals(_header))
            {
                _classification.MergeInfo.ParentOf = delegate(MergeInfo child)
                      {
                          TEntity entity = child.Self as TEntity;

                          return entity != null
                                     ? customization.Entity.Equals(entity)
                                     : false;
                      };
            }
        }

        public void Visit(Package<TEntity> customization)
        {
            throw new NotSupportedException();
        }
    }
}