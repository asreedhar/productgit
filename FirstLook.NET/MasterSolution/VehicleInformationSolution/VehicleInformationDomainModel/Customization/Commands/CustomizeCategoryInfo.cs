using System;
using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Commands
{
    public class CustomizeCategoryInfo : ICustomizeCategoryInfo, ICustomizationVisitor<Category>
    {
        private readonly ICustomize<Category> _customize;

        public CustomizeCategoryInfo(ICustomize<Category> customize)
        {
            _customize = customize;
        }

        private Category _entity;

        private CategoryDto _info;

        public Category Entity
        {
            set { _entity = value; }
        }

        public void Apply(Category category, CategoryDto info)
        {
            _entity = category;

            _info = info;

            _customize.Apply(this);
        }

        public void Visit(ChangeName<Category> customization)
        {
            if (customization.Entity.Equals(_entity))
            {
                _info.Name = customization.Name;
            }
        }

        public void Visit(ChangeDescription<Category> customization)
        {
            throw new NotSupportedException();
        }

        public void Visit(ChangeLocation<Category> customization)
        {
            throw new NotSupportedException();
        }

        public void Visit(ChangePosition<Category> customization)
        {
            throw new NotSupportedException();
        }

        public void Visit(Associate<Category> customization)
        {
            throw new NotSupportedException();
        }

        public void Visit(Hide<Category> customization)
        {
            throw new NotSupportedException();
        }

        public void Visit(Merge<Category> customization)
        {
            throw new NotSupportedException();
        }

        public void Visit(Package<Category> customization)
        {
            throw new NotSupportedException();
        }
    }
}
