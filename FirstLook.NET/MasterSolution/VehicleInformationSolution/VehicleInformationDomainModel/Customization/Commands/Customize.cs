using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Common.Core.Memento;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;
using Style=FirstLook.VehicleInformation.DomainModel.Customization.Model.Style;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Commands
{
    public abstract class Customize<TEntity> : ICustomize<TEntity>
        where TEntity : IOriginator<TEntity>
    {
        private ICustomizationRepository _repository;

        protected ICustomizationRepository Repository
        {
            get
            {
                if (_repository == null)
                {
                    _repository = RegistryFactory.GetRegistry().Resolve<ICustomizationRepository>();
                }

                return _repository;
            }
        }

        [Flags]
        private enum Mode
        {
            System,
            Dealer,
            Style
        }

        private readonly Mode _mode;

        private readonly Style _style;

        private readonly IBroker _broker;

        protected Customize()
        {
            _mode = Mode.System;
        }

        protected Customize(IBroker broker)
        {
            _mode = Mode.Dealer;

            _broker = broker;
        }

        protected Customize(Style style)
        {
            _mode = Mode.System | Mode.Style;

            _style = style;
        }

        protected Customize(IBroker broker, Style style)
        {
            _mode = Mode.Dealer | Mode.Style;

            _broker = broker;

            _style = style;
        }

        private IList<ChangeDescription<TEntity>> _descriptions;

        protected virtual IList<ChangeDescription<TEntity>> Descriptions
        {
            get
            {
                if (_descriptions == null)
                {
                    switch (_mode)
                    {
                        case Mode.System:
                            _descriptions = Repository.Fetch<ChangeDescription<TEntity>, TEntity>();
                            break;
                        case Mode.System | Mode.Style:
                            _descriptions = Repository.Fetch<ChangeDescription<TEntity>, TEntity>(_style);
                            break;
                        case Mode.Dealer:
                            _descriptions = Repository.Fetch<ChangeDescription<TEntity>, TEntity>(_broker);
                            break;
                        case Mode.Dealer | Mode.Style:
                            _descriptions = Repository.Fetch<ChangeDescription<TEntity>, TEntity>(_broker, _style);
                            break;
                    }
                }

                return _descriptions;
            }
        }

        private IList<ChangeName<TEntity>> _names;

        protected virtual IList<ChangeName<TEntity>> Names
        {
            get
            {
                if (_names == null)
                {
                    switch (_mode)
                    {
                        case Mode.System:
                            _names = Repository.Fetch<ChangeName<TEntity>, TEntity>();
                            break;
                        case Mode.System | Mode.Style:
                            _names = Repository.Fetch<ChangeName<TEntity>, TEntity>(_style);
                            break;
                        case Mode.Dealer:
                            _names = Repository.Fetch<ChangeName<TEntity>, TEntity>(_broker);
                            break;
                        case Mode.Dealer | Mode.Style:
                            _names = Repository.Fetch<ChangeName<TEntity>, TEntity>(_broker, _style);
                            break;
                    }
                }

                return _names;
            }
        }

        private IList<ChangeLocation<TEntity>> _locations;

        protected virtual IList<ChangeLocation<TEntity>> Locations
        {
            get
            {
                if (_locations == null)
                {
                    switch (_mode)
                    {
                        case Mode.System:
                            _locations = Repository.Fetch<ChangeLocation<TEntity>, TEntity>();
                            break;
                        case Mode.System | Mode.Style:
                            _locations = Repository.Fetch<ChangeLocation<TEntity>, TEntity>(_style);
                            break;
                        case Mode.Dealer:
                            _locations = Repository.Fetch<ChangeLocation<TEntity>, TEntity>(_broker);
                            break;
                        case Mode.Dealer | Mode.Style:
                            _locations = Repository.Fetch<ChangeLocation<TEntity>, TEntity>(_broker, _style);
                            break;
                    }
                }

                return _locations;
            }
        }

        private IList<ChangePosition<TEntity>> _positions;

        protected virtual IList<ChangePosition<TEntity>> Positions
        {
            get
            {
                if (_positions == null)
                {
                    switch (_mode)
                    {
                        case Mode.System:
                            _positions = Repository.Fetch<ChangePosition<TEntity>, TEntity>();
                            break;
                        case Mode.System | Mode.Style:
                            _positions = Repository.Fetch<ChangePosition<TEntity>, TEntity>(_style);
                            break;
                        case Mode.Dealer:
                            _positions = Repository.Fetch<ChangePosition<TEntity>, TEntity>(_broker);
                            break;
                        case Mode.Dealer | Mode.Style:
                            _positions = Repository.Fetch<ChangePosition<TEntity>, TEntity>(_broker, _style);
                            break;
                    }
                }

                return _positions;
            }
        }

        private IList<Hide<TEntity>> _hides;

        protected virtual IList<Hide<TEntity>> Hides
        {
            get
            {
                if (_hides == null)
                {
                    switch (_mode)
                    {
                        case Mode.System:
                            _hides = Repository.Fetch<Hide<TEntity>, TEntity>();
                            break;
                        case Mode.System | Mode.Style:
                            _hides = Repository.Fetch<Hide<TEntity>, TEntity>(_style);
                            break;
                        case Mode.Dealer:
                            _hides = Repository.Fetch<Hide<TEntity>, TEntity>(_broker);
                            break;
                        case Mode.Dealer | Mode.Style:
                            _hides = Repository.Fetch<Hide<TEntity>, TEntity>(_broker, _style);
                            break;
                    }
                }

                return _hides;
            }
        }

        private IList<Merge<TEntity>> _merges;

        protected virtual IList<Merge<TEntity>> Merges
        {
            get
            {
                if (_merges == null)
                {
                    switch (_mode)
                    {
                        case Mode.System:
                            _merges = Repository.Fetch<Merge<TEntity>, TEntity>();
                            break;
                        case Mode.System | Mode.Style:
                            _merges = Repository.Fetch<Merge<TEntity>, TEntity>(_style);
                            break;
                        case Mode.Dealer:
                            _merges = Repository.Fetch<Merge<TEntity>, TEntity>(_broker);
                            break;
                        case Mode.Dealer | Mode.Style:
                            _merges = Repository.Fetch<Merge<TEntity>, TEntity>(_broker, _style);
                            break;
                    }
                }

                return _merges;
            }
        }

        private IList<Package<TEntity>> _packages;

        protected virtual IList<Package<TEntity>> Packages
        {
            get
            {
                if (_packages == null)
                {
                    switch (_mode)
                    {
                        case Mode.System:
                            _packages = Repository.Fetch<Package<TEntity>, TEntity>();
                            break;
                        case Mode.System | Mode.Style:
                            _packages = Repository.Fetch<Package<TEntity>, TEntity>(_style);
                            break;
                        case Mode.Dealer:
                            _packages = Repository.Fetch<Package<TEntity>, TEntity>(_broker);
                            break;
                        case Mode.Dealer | Mode.Style:
                            _packages = Repository.Fetch<Package<TEntity>, TEntity>(_broker, _style);
                            break;
                    }
                }

                return _packages;
            }
        }

        public void Apply(ICustomizationVisitor<TEntity> visitor)
        {
            if (ChangeDescription)
            {
                foreach (ICustomization<TEntity> customization in Descriptions)
                {
                    customization.Accept(visitor);
                }
            }

            if (ChangeName)
            {
                foreach (ICustomization<TEntity> customization in Names)
                {
                    customization.Accept(visitor);
                }
            }

            if (ChangeLocation)
            {
                foreach (ICustomization<TEntity> customization in Locations)
                {
                    customization.Accept(visitor);
                }
            }

            if (ChangePosition)
            {
                foreach (ICustomization<TEntity> customization in Positions)
                {
                    customization.Accept(visitor);
                }
            }

            if (Hide)
            {
                foreach (ICustomization<TEntity> customization in Hides)
                {
                    customization.Accept(visitor);
                }
            }

            if (Merge)
            {
                foreach (ICustomization<TEntity> customization in Merges)
                {
                    customization.Accept(visitor);
                }
            }

            if (Package)
            {
                foreach (ICustomization<TEntity> customization in Packages)
                {
                    customization.Accept(visitor);
                }
            }
        }

        protected virtual bool ChangeDescription { get { return false; } }

        protected virtual bool ChangeName { get { return false; } }

        protected virtual bool ChangeLocation { get { return false; } }

        protected virtual bool ChangePosition { get { return false; } }

        protected virtual bool Hide { get { return false; } }

        protected virtual bool Merge { get { return false; } }

        protected virtual bool Package { get { return false; } }
    }

    public class CustomizeCategoryHeader : Customize<CategoryHeader>
    {
        protected override bool ChangeName
        {
            get { return true; }
        }

        protected override bool ChangeLocation
        {
            get { return true; }
        }

        protected override bool ChangePosition
        {
            get { return true; }
        }

        protected override bool Merge
        {
            get { return true; }
        }
    }

    public class CustomizeOptionHeader : Customize<OptionHeader>
    {
        protected override bool ChangeName
        {
            get { return true; }
        }

        protected override bool ChangeLocation
        {
            get { return true; }
        }

        protected override bool Hide
        {
            get { return true; }
        }
    }

    public class CustomizeStandardHeader : Customize<StandardHeader>
    {
        protected override bool ChangeName
        {
            get { return true; }
        }

        protected override bool ChangeLocation
        {
            get { return true; }
        }

        protected override bool Hide
        {
            get { return true; }
        }
    }

    public class CustomizeCategory : Customize<Category>
    {
        public CustomizeCategory()
        {
        }

        public CustomizeCategory(IBroker broker)
            : base(broker)
        {
        }

        public CustomizeCategory(Style style) : base(style)
        {
        }

        public CustomizeCategory(IBroker broker, Style style)
            : base(broker, style)
        {
        }

        protected override bool ChangeName
        {
            get { return true; }
        }
    }

    public class CustomizeOption : Customize<Option>
    {
        public CustomizeOption()
        {
        }

        public CustomizeOption(IBroker broker)
            : base(broker)
        {
        }

        public CustomizeOption(Style style) : base(style)
        {
        }

        public CustomizeOption(IBroker broker, Style style)
            : base(broker, style)
        {
        }

        protected override bool ChangeDescription
        {
            get { return true; }
        }

        protected override bool ChangeName
        {
            get { return true; }
        }

        protected override bool Package
        {
            get { return true; }
        }
    }

    public class CustomizeStandard : Customize<Standard>
    {
        public CustomizeStandard()
        {
        }

        public CustomizeStandard(IBroker broker)
            : base(broker)
        {
        }

        public CustomizeStandard(Style style) : base(style)
        {
        }

        public CustomizeStandard(IBroker broker, Style style)
            : base(broker, style)
        {
        }

        protected override bool ChangeName
        {
            get { return true; }
        }
    }
}
