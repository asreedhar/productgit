using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Commands
{
    public interface ICustomizeOptionInfo
    {
        void Apply(Option option, OptionDto info);
    }
}
