using FirstLook.Common.Core.Memento;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Commands
{
    public interface ICustomize<TEntity> where TEntity : IOriginator<TEntity>
    {
        void Apply(ICustomizationVisitor<TEntity> visitor);
    }
}