﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleInformation.DomainModel.Customization
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<Repositories.Module>();
        }
    }
}
