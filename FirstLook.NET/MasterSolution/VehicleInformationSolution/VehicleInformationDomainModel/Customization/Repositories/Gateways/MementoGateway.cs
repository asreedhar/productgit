using System.Data;
using FirstLook.Client.DomainModel.Clients.Repositories.Utility;
using FirstLook.Common.Core.Memento;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Gateways
{
    public class MementoGateway : AuditingGateway
    {
        public IMementoEntity<Category> Insert(IMemento<Category> memento)
        {
            CategoryMemento category = (CategoryMemento) memento;

            IMementoEntity<Category> entity = category as IMementoEntity<Category>;

            ICustomizationStore store = Resolve<ICustomizationStore>();

            if (entity == null)
            {
                store.Memento_Category_Exists(
                    delegate(IDataReader reader)
                        {
                            if (reader.Read())
                            {
                                entity = new CategoryMementoEntity(
                                    reader.GetInt32(reader.GetOrdinal("CategoryID")),
                                    ReadMemento(reader));
                            }
                        },
                    category.Id);

                if (entity == null)
                {
                    MementoEntity m = NewMemento(MementoType.Category);

                    store.Memento_Category_Insert(
                        delegate(IDataReader reader)
                            {
                                if (reader.Read())
                                {
                                    entity = new CategoryMementoEntity(
                                        reader.GetInt32(reader.GetOrdinal("CategoryID")),
                                        m);
                                }
                            },
                        m.Id,
                        category.Id);
                }
            }

            return entity;
        }

        public IMementoEntity<Option> Insert(IMemento<Option> memento)
        {
            OptionMemento option = (OptionMemento) memento;

            IMementoEntity<Option> entity = option as IMementoEntity<Option>;

            ICustomizationStore store = Resolve<ICustomizationStore>();

            if (entity == null)
            {
                store.Memento_Option_Exists(
                    delegate(IDataReader reader)
                        {
                            if (reader.Read())
                            {
                                entity = new OptionMementoEntity(
                                    reader.GetInt32(reader.GetOrdinal("StyleID")),
                                    reader.GetInt32(reader.GetOrdinal("Sequence")),
                                    reader.GetString(reader.GetOrdinal("OptionCode")),
                                    ReadMemento(reader));
                            }
                        },
                    option.StyleId,
                    option.Sequence,
                    option.Code);

                if (entity == null)
                {
                    MementoEntity m = NewMemento(MementoType.Option);

                    store.Memento_Option_Insert(
                        delegate(IDataReader reader)
                            {
                                if (reader.Read())
                                {
                                    entity = new OptionMementoEntity(
                                        reader.GetInt32(reader.GetOrdinal("StyleID")),
                                        reader.GetInt32(reader.GetOrdinal("Sequence")),
                                        reader.GetString(reader.GetOrdinal("OptionCode")),
                                        m);
                                }
                            },
                        m.Id,
                        option.StyleId,
                        option.Sequence,
                        option.Code);
                }
            }

            return entity;
        }

        public IMementoEntity<Standard> Insert(IMemento<Standard> memento)
        {
            StandardMemento standard = (StandardMemento)memento;

            IMementoEntity<Standard> entity = standard as IMementoEntity<Standard>;

            ICustomizationStore store = Resolve<ICustomizationStore>();

            if (entity == null)
            {
                store.Memento_Standard_Exists(
                    delegate(IDataReader reader)
                    {
                        if (reader.Read())
                        {
                            entity = new StandardMementoEntity(
                                reader.GetInt32(reader.GetOrdinal("StyleID")),
                                reader.GetInt32(reader.GetOrdinal("Sequence")),
                                ReadMemento(reader));
                        }
                    },
                    standard.StyleId,
                    standard.Sequence);

                if (entity == null)
                {
                    MementoEntity m = NewMemento(MementoType.Standard);

                    store.Memento_Standard_Insert(
                        delegate(IDataReader reader)
                        {
                            if (reader.Read())
                            {
                                entity = new StandardMementoEntity(
                                    reader.GetInt32(reader.GetOrdinal("StyleID")),
                                    reader.GetInt32(reader.GetOrdinal("Sequence")),
                                    m);
                            }
                        },
                        m.Id,
                        standard.StyleId,
                        standard.Sequence);
                }
            }

            return entity;
        }

        public IMementoEntity<OptionHeader> Insert(IMemento<OptionHeader> memento)
        {
            OptionHeaderMemento header = (OptionHeaderMemento) memento;

            IMementoEntity<OptionHeader> entity = header as IMementoEntity<OptionHeader>;

            ICustomizationStore store = Resolve<ICustomizationStore>();

            if (entity == null)
            {
                store.Memento_OptionHeader_Exists(
                    delegate(IDataReader reader)
                    {
                        if (reader.Read())
                        {
                            entity = new OptionHeaderMementoEntity(
                                reader.GetInt32(reader.GetOrdinal("OptionHeaderID")),
                                ReadMemento(reader));
                        }
                    },
                    header.Id);

                if (entity == null)
                {
                    MementoEntity m = NewMemento(MementoType.OptionHeader);

                    store.Memento_OptionHeader_Insert(
                        delegate(IDataReader reader)
                        {
                            if (reader.Read())
                            {
                                entity = new OptionHeaderMementoEntity(
                                    reader.GetInt32(reader.GetOrdinal("OptionHeaderID")),
                                    m);
                            }
                        },
                        m.Id,
                        header.Id);
                }
            }

            return entity;
        }

        public IMementoEntity<CategoryHeader> Insert(IMemento<CategoryHeader> memento)
        {
            CategoryHeaderMemento header = (CategoryHeaderMemento)memento;

            IMementoEntity<CategoryHeader> entity = header as IMementoEntity<CategoryHeader>;

            ICustomizationStore store = Resolve<ICustomizationStore>();

            if (entity == null)
            {
                store.Memento_CategoryHeader_Exists(
                    delegate(IDataReader reader)
                    {
                        if (reader.Read())
                        {
                            entity = new CategoryHeaderMementoEntity(
                                reader.GetInt32(reader.GetOrdinal("CategoryHeaderID")),
                                ReadMemento(reader));
                        }
                    },
                    header.Id);

                if (entity == null)
                {
                    MementoEntity m = NewMemento(MementoType.CategoryHeader);

                    store.Memento_CategoryHeader_Insert(
                        delegate(IDataReader reader)
                        {
                            if (reader.Read())
                            {
                                entity = new CategoryHeaderMementoEntity(
                                    reader.GetInt32(reader.GetOrdinal("CategoryHeaderID")),
                                    m);
                            }
                        },
                        m.Id,
                        header.Id);
                }
            }

            return entity;
        }

        public IMementoEntity<StandardHeader> Insert(IMemento<StandardHeader> memento)
        {
            StandardHeaderMemento header = (StandardHeaderMemento)memento;

            IMementoEntity<StandardHeader> entity = header as IMementoEntity<StandardHeader>;

            ICustomizationStore store = Resolve<ICustomizationStore>();

            if (entity == null)
            {
                store.Memento_StandardHeader_Exists(
                    delegate(IDataReader reader)
                    {
                        if (reader.Read())
                        {
                            entity = new StandardHeaderMementoEntity(
                                reader.GetInt32(reader.GetOrdinal("StandardHeaderID")),
                                ReadMemento(reader));
                        }
                    },
                    header.Id);

                if (entity == null)
                {
                    MementoEntity m = NewMemento(MementoType.StandardHeader);

                    store.Memento_StandardHeader_Insert(
                        delegate(IDataReader reader)
                        {
                            if (reader.Read())
                            {
                                entity = new StandardHeaderMementoEntity(
                                    reader.GetInt32(reader.GetOrdinal("StandardHeaderID")),
                                    m);
                            }
                        },
                        m.Id,
                        header.Id);
                }
            }

            return entity;
        }

        private MementoEntity NewMemento(MementoType mementoType)
        {
            return CustomizationStoreHelper.InsertMemento(
                Insert(),
                mementoType);
        }

        protected MementoEntity ReadMemento(IDataReader exists)
        {
            return new MementoEntity(
                exists.GetInt32(exists.GetOrdinal("MementoID")),
                ReadAuditRow(exists, "MX_"),
                (MementoType) exists.GetByte(exists.GetOrdinal("MementoTypeID")));
        }
    }
}
