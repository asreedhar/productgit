using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Repositories.Utility;
using FirstLook.Common.Core.Memento;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;
using FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Gateways
{
    public class CustomizationsGateway : AuditingGateway
    {
        public IList<TCustomization> Fetch<TCustomization, TEntity>()
            where TCustomization : ICustomization<TEntity>
            where TEntity : IOriginator<TEntity>
        {
            return Fetch<TCustomization, TEntity>(null);
        }

        public IList<TCustomization> Fetch<TCustomization, TEntity>(IBroker broker)
        {
            ICustomizationStore store = RegistryFactory.GetRegistry().Resolve<ICustomizationStore>();

            Owner owner = broker == null ? Owner.System : Owner.Dealer;

            IList<TCustomization> list = null;

            store.Customizations_Exists(
                delegate(IDataReader reader)
                {
                    if (reader.Read())
                    {
                        int id = reader.GetInt32(reader.GetOrdinal("CustomizationsID"));

                        list = new CustomizationsEntity<TCustomization>(owner, id);
                    }
                    else
                    {
                        list = new Customizations<TCustomization>(owner);
                    }
                },
                broker);

            return list;
        }

        public CustomizationsEntity<TCustomization> Insert<TCustomization, TEntity>(Customizations<TCustomization> customizations)
            where TCustomization : ICustomization<TEntity>
            where TEntity : IOriginator<TEntity>
        {
            return CustomizationStoreHelper.InsertCustomizations<TCustomization>(
                Insert(),
                customizations.Owner);
        }
    }
}
