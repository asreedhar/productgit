using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Serializers;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;
using FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Gateways
{
    public interface ICustomizationStore
    {
        void Customizations_Exists(Action<IDataReader> f, IBroker broker);
        void Customizations_Fetch(Action<IDataReader> f, int customizationsId, CustomizationType customizationType, MementoType mementoType, Style style);
        void Customizations_Insert(Action<IDataReader> f, AuditRow auditRow, Owner owner);
        void Customizations_Customization_Insert(AuditRow auditRow, int customizationsId, int customizationId);
        void Customizations_Client_Insert(AuditRow auditRow, int customizationsId, int clientId);
        void Customization_Insert(Action<IDataReader> f, AuditRow auditRow, int customizationsId, CustomizationType customizationType);
        void Customization_Associate(AuditRow auditRow, int customizationId, int mementoId, int relationId);
        void Customization_ChangeDescription(AuditRow auditRow, int customizationId, int mementoId, string description);
        void Customization_ChangeName(AuditRow auditRow, int customizationId, int mementoId, string name);
        void Customization_ChangeLocation(AuditRow auditRow, int customizationId, int mementoId, Location location);
        void Customization_ChangePosition(AuditRow auditRow, int customizationId, int mementoId, int position);
        void Customization_Hide(AuditRow auditRow, int customizationId, int mementoId);
        void Customization_Merge(AuditRow auditRow, int customizationId, int mementoId, int parentId);
        void Customization_Package(AuditRow auditRow, int customizationId, int mementoId);
        void Memento_Insert(Action<IDataReader> f, AuditRow auditRow, MementoType mementoType);
        void Memento_Category_Exists(Action<IDataReader> f, int id);
        void Memento_Category_Insert(Action<IDataReader> f, int mementoId, int id);
        void Memento_CategoryHeader_Exists(Action<IDataReader> f, int id);
        void Memento_CategoryHeader_Insert(Action<IDataReader> f, int mementoId, int id);
        void Memento_Option_Exists(Action<IDataReader> f, int styleId, int sequence, string optionCode);
        void Memento_Option_Insert(Action<IDataReader> f, int mementoId, int styleId, int sequence, string optionCode);
        void Memento_OptionHeader_Exists(Action<IDataReader> f, int id);
        void Memento_OptionHeader_Insert(Action<IDataReader> f, int mementoId, int id);
        void Memento_Standard_Exists(Action<IDataReader> f, int styleId, int sequence);
        void Memento_Standard_Insert(Action<IDataReader> f, int mementoId, int styleId, int sequence);
        void Memento_StandardHeader_Exists(Action<IDataReader> f, int id);
        void Memento_StandardHeader_Insert(Action<IDataReader> f, int mementoId, int id);
    }

    public class DbCustomizationStore : SessionDataStore, ICustomizationStore
    {
        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        public void Customizations_Exists(Action<IDataReader> f, IBroker broker)
        {
            const string exists = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customizations_Exists.txt";

            Parameter o = (broker == null)
                  ? new Parameter("OwnerID", 1, DbType.Int32)
                  : new Parameter("OwnerID", 2, DbType.Int32);

            Parameter d = (broker == null)
                  ? new Parameter("ClientID", DBNull.Value, DbType.Int32)
                  : new Parameter("ClientID", broker.Id, DbType.Int32);

            Query(f, exists, o, d);
        }

        public void Customizations_Insert(Action<IDataReader> f, AuditRow auditRow, Owner owner)
        {
            const string insert = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customizations_Insert.txt";

            NonQuery(insert,
                new Parameter("AuditRowID", auditRow.Id, DbType.Int32),
                new Parameter("OwnerID", (byte) owner, DbType.Byte));

            const string fetch = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customizations_Fetch.txt";

            Query(f, fetch);
        }

        public void Customizations_Customization_Insert(AuditRow auditRow, int customizationsId, int customizationId)
        {
            const string insert = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customizations_Customization_Insert.txt";

            NonQuery(insert,
                new Parameter("AuditRowID", auditRow.Id, DbType.Int32),
                new Parameter("CustomizationsID", customizationsId, DbType.Int32),
                new Parameter("CustomizationID", customizationId, DbType.Int32));
        }

        public void Customizations_Client_Insert(AuditRow auditRow, int customizationsId, int clientId)
        {
            const string insert = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customizations_Client_Insert.txt";

            NonQuery(insert,
                new Parameter("AuditRowID", auditRow.Id, DbType.Int32),
                new Parameter("CustomizationsID", customizationsId, DbType.Int32),
                new Parameter("ClientID", clientId, DbType.Int32));
        }

        public void Customization_Insert(Action<IDataReader> f, AuditRow auditRow, int customizationsId, CustomizationType customizationType)
        {
            const string insert = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customization_Insert.txt";

            NonQuery(insert,
                new Parameter("AuditRowID", auditRow.Id, DbType.Int32),
                new Parameter("CustomizationsID", customizationsId, DbType.Int32),
                new Parameter("CustomizationTypeID", (byte) customizationType, DbType.Byte));

            const string fetch = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customization_Fetch.txt";

            Query(f, fetch);
        }

        public void Customization_Associate(AuditRow auditRow, int customizationId, int mementoId, int relationId)
        {
            const string insert = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customization_Associate_Insert.txt";

            NonQuery(insert,
                new Parameter("AuditRowID", auditRow.Id, DbType.Int32),
                new Parameter("CustomizationID", customizationId, DbType.Int32),
                new Parameter("MementoID", mementoId, DbType.Int32),
                new Parameter("RelationID", relationId, DbType.Int32));
        }

        public void Customization_ChangeDescription(AuditRow auditRow, int customizationId, int mementoId, string description)
        {
            const string insert = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customization_ChangeDescription_Insert.txt";

            NonQuery(insert,
                new Parameter("AuditRowID", auditRow.Id, DbType.Int32),
                new Parameter("CustomizationID", customizationId, DbType.Int32),
                new Parameter("MementoID", mementoId, DbType.Int32),
                new Parameter("Description", description, DbType.String));
        }

        public void Customization_ChangeName(AuditRow auditRow, int customizationId, int mementoId, string name)
        {
            const string insert = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customization_ChangeName_Insert.txt";

            NonQuery(insert,
                new Parameter("AuditRowID", auditRow.Id, DbType.Int32),
                new Parameter("CustomizationID", customizationId, DbType.Int32),
                new Parameter("MementoID", mementoId, DbType.Int32),
                new Parameter("Name", name, DbType.String));
        }

        public void Customization_ChangeLocation(AuditRow auditRow, int customizationId, int mementoId, Location location)
        {
            const string insert = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customization_ChangeLocation_Insert.txt";

            NonQuery(insert,
                new Parameter("AuditRowID", auditRow.Id, DbType.Int32),
                new Parameter("CustomizationID", customizationId, DbType.Int32),
                new Parameter("MementoID", mementoId, DbType.Int32),
                new Parameter("LocationID", (byte) location, DbType.Byte));
        }

        public void Customization_ChangePosition(AuditRow auditRow, int customizationId, int mementoId, int position)
        {
            const string insert = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customization_ChangePosition_Insert.txt";

            NonQuery(insert,
                new Parameter("AuditRowID", auditRow.Id, DbType.Int32),
                new Parameter("CustomizationID", customizationId, DbType.Int32),
                new Parameter("MementoID", mementoId, DbType.Int32),
                new Parameter("Position", position, DbType.Int32));
        }

        public void Customization_Hide(AuditRow auditRow, int customizationId, int mementoId)
        {
            const string insert = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customization_Hide_Insert.txt";

            NonQuery(insert,
                new Parameter("AuditRowID", auditRow.Id, DbType.Int32),
                new Parameter("CustomizationID", customizationId, DbType.Int32),
                new Parameter("MementoID", mementoId, DbType.Int32));
        }

        public void Customization_Merge(AuditRow auditRow, int customizationId, int mementoId, int parentId)
        {
            const string insert = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customization_Merge_Insert.txt";

            NonQuery(insert,
                new Parameter("AuditRowID", auditRow.Id, DbType.Int32),
                new Parameter("CustomizationID", customizationId, DbType.Int32),
                new Parameter("MementoID", mementoId, DbType.Int32),
                new Parameter("ParentID", parentId, DbType.Int32));
        }

        public void Customization_Package(AuditRow auditRow, int customizationId, int mementoId)
        {
            const string insert = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customization_Package_Insert.txt";

            NonQuery(insert,
                new Parameter("AuditRowID", auditRow.Id, DbType.Int32),
                new Parameter("CustomizationID", customizationId, DbType.Int32),
                new Parameter("MementoID", mementoId, DbType.Int32));
        }

        public void Memento_Insert(Action<IDataReader> f, AuditRow auditRow, MementoType mementoType)
        {
            const string insert = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Memento_Insert.txt";

            NonQuery(insert,
                new Parameter("AuditRowID", auditRow.Id, DbType.Int32),
                new Parameter("MementoTypeID", (byte) mementoType, DbType.Byte));

            const string fetch = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Memento_Fetch.txt";

            Query(f, fetch);
        }

        public void Memento_Category_Exists(Action<IDataReader> f, int id)
        {
            const string exists = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Memento_Category_Exists.txt";

            Query(f, exists, new Parameter("CategoryID", id, DbType.Int32));
        }

        public void Memento_Category_Insert(Action<IDataReader> f, int mementoId, int id)
        {
            const string insert = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Memento_Category_Insert.txt";

            NonQuery(insert,
                new Parameter("MementoID", mementoId, DbType.Int32),
                new Parameter("CategoryID", id, DbType.Int32));

            Memento_Category_Exists(f, id);
        }

        public void Memento_CategoryHeader_Exists(Action<IDataReader> f, int id)
        {
            const string exists = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Memento_CategoryHeader_Exists.txt";

            Query(f, exists, new Parameter("CategoryHeaderID", id, DbType.Int32));
        }

        public void Memento_CategoryHeader_Insert(Action<IDataReader> f, int mementoId, int id)
        {
            const string insert = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Memento_CategoryHeader_Insert.txt";

            NonQuery(insert,
                new Parameter("MementoID", mementoId, DbType.Int32),
                new Parameter("CategoryHeaderID", id, DbType.Int32));

            Memento_CategoryHeader_Exists(f, id);
        }

        public void Memento_Option_Exists(Action<IDataReader> f, int styleId, int sequence, string optionCode)
        {
            const string exists = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Memento_Option_Exists.txt";

            Query(f, exists,
                new Parameter("StyleID", styleId, DbType.Int32),
                new Parameter("Sequence", sequence, DbType.Int32),
                new Parameter("OptionCode", optionCode, DbType.String));
        }

        public void Memento_Option_Insert(Action<IDataReader> f, int mementoId, int styleId, int sequence, string optionCode)
        {
            const string insert = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Memento_Option_Insert.txt";

            NonQuery(insert,
                new Parameter("MementoID", mementoId, DbType.Int32),
                new Parameter("StyleID", styleId, DbType.Int32),
                new Parameter("Sequence", sequence, DbType.Int32),
                new Parameter("OptionCode", optionCode, DbType.String));

            Memento_Option_Exists(f, styleId, sequence, optionCode);
        }

        public void Memento_OptionHeader_Exists(Action<IDataReader> f, int id)
        {
            const string exists = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Memento_OptionHeader_Exists.txt";

            Query(f, exists, new Parameter("OptionHeaderID", id, DbType.Int32));
        }

        public void Memento_OptionHeader_Insert(Action<IDataReader> f, int mementoId, int id)
        {
            const string insert = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Memento_OptionHeader_Insert.txt";

            NonQuery(insert,
                new Parameter("MementoID", mementoId, DbType.Int32),
                new Parameter("OptionHeaderID", id, DbType.Int32));

            Memento_OptionHeader_Exists(f, id);
        }

        public void Memento_Standard_Exists(Action<IDataReader> f, int styleId, int sequence)
        {
            const string exists = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Memento_Standard_Exists.txt";

            Query(f, exists,
                new Parameter("StyleID", styleId, DbType.Int32),
                new Parameter("Sequence", sequence, DbType.Int32));
        }

        public void Memento_Standard_Insert(Action<IDataReader> f, int mementoId, int styleId, int sequence)
        {
            const string insert = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Memento_Standard_Insert.txt";

            NonQuery(insert,
                new Parameter("MementoID", mementoId, DbType.Int32),
                new Parameter("StyleID", styleId, DbType.Int32),
                new Parameter("Sequence", sequence, DbType.Int32));

            Memento_Standard_Exists(f, styleId, sequence);
        }

        public void Memento_StandardHeader_Exists(Action<IDataReader> f, int id)
        {
            const string exists = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Memento_StandardHeader_Exists.txt";

            Query(f, exists, new Parameter("StandardHeaderID", id, DbType.Int32));
        }

        public void Memento_StandardHeader_Insert(Action<IDataReader> f, int mementoId, int id)
        {
            const string insert = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Memento_StandardHeader_Insert.txt";

            NonQuery(insert,
                new Parameter("MementoID", mementoId, DbType.Int32),
                new Parameter("StandardHeaderID", id, DbType.Int32));

            Memento_StandardHeader_Exists(f, id);
        }

        public void Customizations_Fetch(Action<IDataReader> f, int customizationsId, CustomizationType customizationType, MementoType mementoType, Style style)
        {
            string fetch = string.Empty;

            switch (customizationType)
            {
                case CustomizationType.ChangeName:
                    switch (mementoType)
                    {
                        case MementoType.Category:
                            fetch = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customizations_ChangeName_Category_Fetch.txt";
                            break;
                        case MementoType.Option:
                            fetch = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customizations_ChangeName_Option_Fetch.txt";
                            break;
                        case MementoType.Standard:
                            fetch = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customizations_ChangeName_Standard_Fetch.txt";
                            break;
                        case MementoType.CategoryHeader:
                            fetch = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customizations_ChangeName_CategoryHeader_Fetch.txt";
                            break;
                        case MementoType.OptionHeader:
                            fetch = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customizations_ChangeName_OptionHeader_Fetch.txt";
                            break;
                        case MementoType.StandardHeader:
                            fetch = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customizations_ChangeName_StandardHeader_Fetch.txt";
                            break;
                    }
                    break;
                case CustomizationType.ChangeDescription:
                    switch (mementoType)
                    {
                        case MementoType.Option:
                            fetch = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customizations_ChangeDescription_Option_Fetch.txt";
                            break;
                    }
                    break;
                case CustomizationType.ChangeLocation:
                    switch (mementoType)
                    {
                        case MementoType.CategoryHeader:
                            fetch = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customizations_ChangeLocation_CategoryHeader_Fetch.txt";
                            break;
                        case MementoType.OptionHeader:
                            fetch = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customizations_ChangeLocation_OptionHeader_Fetch.txt";
                            break;
                        case MementoType.StandardHeader:
                            fetch = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customizations_ChangeLocation_StandardHeader_Fetch.txt";
                            break;
                    }
                    break;
                case CustomizationType.ChangePosition:
                    switch (mementoType)
                    {
                        case MementoType.CategoryHeader:
                            fetch = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customizations_ChangePosition_CategoryHeader_Fetch.txt";
                            break;
                    }
                    break;
                case CustomizationType.Hide:
                    switch (mementoType)
                    {
                        case MementoType.CategoryHeader:
                            fetch = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customizations_Hide_CategoryHeader_Fetch.txt";
                            break;
                        case MementoType.OptionHeader:
                            fetch = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customizations_Hide_OptionHeader_Fetch.txt";
                            break;
                        case MementoType.StandardHeader:
                            fetch = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customizations_Hide_StandardHeader_Fetch.txt";
                            break;
                    }
                    break;
                case CustomizationType.Merge:
                    switch (mementoType)
                    {
                        case MementoType.CategoryHeader:
                            fetch = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customizations_Merge_CategoryHeader_Fetch.txt";
                            break;
                    }
                    break;
                case CustomizationType.Package:
                    switch (mementoType)
                    {
                        case MementoType.Option:
                            fetch = "FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Resources.CustomizationRepository_Customizations_Package_Option_Fetch.txt";
                            break;
                    }
                    break;
            }

            if (string.IsNullOrEmpty(fetch))
            {
                throw new NotSupportedException();
            }

            List<Parameter> parameters = new List<Parameter>();

            parameters.Add(new Parameter("CustomizationTypeID", (byte) customizationType, DbType.Byte));

            parameters.Add(new Parameter("MementoTypeID", (byte) mementoType, DbType.Byte));

            parameters.Add(new Parameter("CustomizationsID", customizationsId, DbType.Int32));

            parameters.Add(new Parameter("Date", DateTime.Now, DbType.DateTime));

            if (style != null)
            {
                parameters.Add(new Parameter("StyleID", style.Id, DbType.Int32));
            }

            Query(f, fetch, parameters.ToArray());
        }
    }

    public static class CustomizationStoreHelper
    {
        public static CustomizationsEntity<T> InsertCustomizations<T>(AuditRow auditRow, Owner owner)
        {
            ICustomizationStore store = RegistryFactory.GetRegistry().Resolve<ICustomizationStore>();

            CustomizationsEntity<T> entity = null;

            store.Customizations_Insert(
                delegate(IDataReader reader)
                    {
                        if (reader.Read())
                        {
                            entity = new CustomizationsEntity<T>(
                                (Owner) reader.GetByte(reader.GetOrdinal("OwnerID")),
                                reader.GetInt32(reader.GetOrdinal("CustomizationsID")));
                        }
                    },
                auditRow, owner);

            return entity;
        }

        public static CustomizationEntity InsertCustomization(AuditRow auditRow, int customizationsId, CustomizationType customizationType)
        {
            ICustomizationStore store = RegistryFactory.GetRegistry().Resolve<ICustomizationStore>();

            CustomizationEntity entity = null;

            store.Customization_Insert(
                delegate(IDataReader reader)
                    {
                        if (reader.Read())
                        {
                            entity = new CustomizationEntity(
                                reader.GetInt32(reader.GetOrdinal("CustomizationID")),
                                GetAuditRow(reader, "XX_"),
                                (CustomizationType) reader.GetByte(reader.GetOrdinal("CustomizationTypeID")));
                        }
                    },
                auditRow, customizationsId, customizationType);

            return entity;
        }

        public static AuditRow GetAuditRow(IDataRecord record, string prefix)
        {
            return new AuditRowSerializer(prefix).Deserialize(record);
        }

        public static MementoEntity InsertMemento(AuditRow auditRow, MementoType mementoType)
        {
            ICustomizationStore store = RegistryFactory.GetRegistry().Resolve<ICustomizationStore>();

            MementoEntity entity = null;

            store.Memento_Insert(
                delegate(IDataReader reader)
                    {
                        if (reader.Read())
                        {
                            entity = new MementoEntity(
                                reader.GetInt32(reader.GetOrdinal("MementoID")),
                                auditRow,
                                (MementoType)reader.GetByte(reader.GetOrdinal("MementoTypeID")));
                        }
                    },
                auditRow, mementoType);

            return entity;
        }

        public static CategoryMementoEntity InsertMemento_Category(MementoEntity memento, CategoryMemento category)
        {
            ICustomizationStore store = RegistryFactory.GetRegistry().Resolve<ICustomizationStore>();

            CategoryMementoEntity entity = null;

            store.Memento_Category_Insert(
                delegate(IDataReader reader)
                    {
                        if (reader.Read())
                        {
                            entity = new CategoryMementoEntity(
                                reader.GetInt32(reader.GetOrdinal("CategoryID")),
                                memento);
                        }
                    },
                memento.Id, category.Id);

            return entity;
        }
    }
}
