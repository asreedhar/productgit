using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Utility;
using FirstLook.Common.Core.Memento;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;
using FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities;
using FirstLook.VehicleInformation.DomainModel.Utility;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Gateways
{
    public class CustomizationGateway : AuditingGateway
    {
        public ChangeNameEntity<TEntity> Save<TEntity>(int id, IMementoEntity<TEntity> memento, ChangeName<TEntity> customization)
            where TEntity : IOriginator<TEntity>
        {
            ChangeNameEntity<TEntity> entity = customization as ChangeNameEntity<TEntity>;

            CustomizationEntity c;

            AuditRow i;

            Save(entity, id, CustomizationType.ChangeName, out c, out i);

            ICustomizationStore store = RegistryFactory.GetRegistry().Resolve<ICustomizationStore>();

            store.Customization_ChangeName(
                    i,
                    c.Id,
                    memento.Entity.Id,
                    customization.Name);

            if (entity != null)
            {
                entity.OnSaved(this, new SavedEventArgs(i));
            }
            else
            {
                entity = new ChangeNameEntity<TEntity>(
                    c,
                    i,
                    memento,
                    customization.Name);
            }

            return entity;
        }

        public ChangeDescriptionEntity<TEntity> Save<TEntity>(int id, IMementoEntity<TEntity> memento, ChangeDescription<TEntity> customization)
            where TEntity : IOriginator<TEntity>
        {
            ChangeDescriptionEntity<TEntity> entity = customization as ChangeDescriptionEntity<TEntity>;

            CustomizationEntity c;

            AuditRow i;

            Save(entity, id, CustomizationType.ChangeDescription, out c, out i);

            ICustomizationStore store = RegistryFactory.GetRegistry().Resolve<ICustomizationStore>();

            store.Customization_ChangeDescription(
                    i,
                    c.Id,
                    memento.Entity.Id,
                    customization.Description);

            if (entity != null)
            {
                entity.OnSaved(this, new SavedEventArgs(i));
            }
            else
            {
                entity = new ChangeDescriptionEntity<TEntity>(
                    c,
                    i,
                    memento,
                    customization.Description);
            }

            return entity;
        }

        public ChangeLocationEntity<TEntity> Save<TEntity>(int id, IMementoEntity<TEntity> memento, ChangeLocation<TEntity> customization)
            where TEntity : IOriginator<TEntity>
        {
            ChangeLocationEntity<TEntity> entity = customization as ChangeLocationEntity<TEntity>;

            CustomizationEntity c;

            AuditRow i;

            Save(entity, id, CustomizationType.ChangeLocation, out c, out i);

            ICustomizationStore store = RegistryFactory.GetRegistry().Resolve<ICustomizationStore>();

            store.Customization_ChangeLocation(
                    i,
                    c.Id,
                    memento.Entity.Id,
                    customization.Location);

            if (entity != null)
            {
                entity.OnSaved(this, new SavedEventArgs(i));
            }
            else
            {
                entity = new ChangeLocationEntity<TEntity>(
                    c,
                    i,
                    memento,
                    customization.Location);
            }

            return entity;
        }

        public ChangePositionEntity<TEntity> Save<TEntity>(int id, IMementoEntity<TEntity> memento, ChangePosition<TEntity> customization)
            where TEntity : IOriginator<TEntity>
        {
            ChangePositionEntity<TEntity> entity = customization as ChangePositionEntity<TEntity>;

            CustomizationEntity c;

            AuditRow i;

            Save(entity, id, CustomizationType.ChangePosition, out c, out i);

            ICustomizationStore store = RegistryFactory.GetRegistry().Resolve<ICustomizationStore>();

            store.Customization_ChangePosition(
                    i,
                    c.Id,
                    memento.Entity.Id,
                    customization.Position);
            
            if (entity != null)
            {
                entity.OnSaved(this, new SavedEventArgs(i));
            }
            else
            {
                entity = new ChangePositionEntity<TEntity>(
                    c,
                    i,
                    memento,
                    customization.Position);
            }

            return entity;
        }

        public HideEntity<TEntity> Save<TEntity>(int id, IMementoEntity<TEntity> memento, Hide<TEntity> customization)
            where TEntity : IOriginator<TEntity>
        {
            HideEntity<TEntity> entity = customization as HideEntity<TEntity>;

            CustomizationEntity c;

            AuditRow i;

            Save(entity, id, CustomizationType.Hide, out c, out i);

            ICustomizationStore store = RegistryFactory.GetRegistry().Resolve<ICustomizationStore>();

            store.Customization_Hide(
                    i,
                    c.Id,
                    memento.Entity.Id);

            if (entity != null)
            {
                entity.OnSaved(this, new SavedEventArgs(i));
            }
            else
            {
                entity = new HideEntity<TEntity>(
                    c,
                    i,
                    memento);
            }

            return entity;
        }

        public MergeEntity<TEntity> Save<TEntity>(int id, IMementoEntity<TEntity> memento, IMementoEntity<TEntity> parent, Merge<TEntity> customization)
            where TEntity : IOriginator<TEntity>
        {
            MergeEntity<TEntity> entity = customization as MergeEntity<TEntity>;

            CustomizationEntity c;

            AuditRow i;

            Save(entity, id, CustomizationType.Merge, out c, out i);

            ICustomizationStore store = RegistryFactory.GetRegistry().Resolve<ICustomizationStore>();

            store.Customization_Merge(
                    i,
                    c.Id,
                    memento.Entity.Id,
                    parent.Entity.Id);

            if (entity != null)
            {
                entity.OnSaved(this, new SavedEventArgs(i));
            }
            else
            {
                entity = new MergeEntity<TEntity>(
                    c,
                    i,
                    memento,
                    parent);
            }

            return entity;
        }

        public PackageEntity<TEntity> Save<TEntity>(int id, IMementoEntity<TEntity> memento, Package<TEntity> customization)
            where TEntity : IOriginator<TEntity>
        {
            PackageEntity<TEntity> entity = customization as PackageEntity<TEntity>;

            CustomizationEntity c;

            AuditRow i;

            Save(entity, id, CustomizationType.Package, out c, out i);

            ICustomizationStore store = RegistryFactory.GetRegistry().Resolve<ICustomizationStore>();

            store.Customization_Package(
                    i,
                    c.Id,
                    memento.Entity.Id);

            if (entity != null)
            {
                entity.OnSaved(this, new SavedEventArgs(i));
            }
            else
            {
                entity = new PackageEntity<TEntity>(
                    c,
                    i,
                    memento);
            }

            return entity;
        }

        private void Save<TEntity>(ICustomizationEntity<TEntity> entity, int id, CustomizationType type, out CustomizationEntity c, out AuditRow i)
            where TEntity : IOriginator<TEntity>
        {
            ICustomizationStore store = RegistryFactory.GetRegistry().Resolve<ICustomizationStore>();

            AuditRow j = null;

            if (entity == null)
            {
                c = CustomizationStoreHelper.InsertCustomization(
                    Insert(),
                    id,
                    type);

                i = Insert();

                j = Insert();
            }
            else
            {
                c = entity.Customization;

                if (entity.IsDeleted)
                {
                    i = Delete(entity.AuditRow);

                    j = Delete(entity.Customization.AuditRow);
                }
                else
                {
                    i = Update(entity.AuditRow);
                }
            }

            if (j != null)
            {
                store.Customizations_Customization_Insert(
                    j,
                    id,
                    c.Id);
            }
        }
    }
}
