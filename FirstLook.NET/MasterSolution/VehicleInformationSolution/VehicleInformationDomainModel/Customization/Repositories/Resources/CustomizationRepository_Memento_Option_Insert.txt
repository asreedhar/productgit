
INSERT INTO Customization.Memento_Option
        ( MementoID ,
          StyleID ,
          Sequence ,
          OptionCode
        )
VALUES  ( @MementoID,
          @StyleID,
          @Sequence,
          @OptionCode
        )
