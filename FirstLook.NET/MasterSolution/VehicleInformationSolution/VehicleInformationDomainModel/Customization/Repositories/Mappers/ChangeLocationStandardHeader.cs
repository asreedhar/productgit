using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;
using FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities;
using Style=FirstLook.VehicleInformation.DomainModel.Customization.Model.Style;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Mappers
{
    public class ChangeLocationStandardHeader : CustomizationMapper<ChangeLocation<StandardHeader>, StandardHeader>
    {
        public override IList<ChangeLocation<StandardHeader>> Fetch(IBroker broker, Style style)
        {
            throw new NotSupportedException();
        }

        public override IList<ChangeLocation<StandardHeader>> Fetch(Style style)
        {
            throw new NotSupportedException();
        }

        protected override ChangeLocation<StandardHeader> Save(CustomizationsEntity<ChangeLocation<StandardHeader>> customizations, ChangeLocation<StandardHeader> customization)
        {
            IMementoEntity<StandardHeader> memento = MementoGateway.Insert(customization.Entity);

            return CustomizationGateway.Save(
                customizations.Id,
                memento,
                customization);
        }

        protected override CustomizationType CustomizationType
        {
            get { return CustomizationType.ChangeLocation; }
        }

        protected override MementoType MementoType
        {
            get { return MementoType.StandardHeader; }
        }

        protected override Action<IDataReader> Read(IList<ChangeLocation<StandardHeader>> list)
        {
            const string customizationPrefix = "CX_", mementoPrefix = "MX_";

            return delegate(IDataReader reader)
                       {
                           while (reader.Read())
                           {
                               ChangeLocationEntity<StandardHeader> item = new ChangeLocationEntity<StandardHeader>(
                                   GetCustomizationEntity(reader),
                                   GetAuditRow(reader, customizationPrefix),
                                   new StandardHeaderMementoEntity(
                                       reader.GetInt32(reader.GetOrdinal("StandardHeaderID")),
                                       GetMementoEntity(reader, mementoPrefix)),
                                   (Location)reader.GetByte(reader.GetOrdinal("LocationID")));
                               list.Add(item);
                           }
                       };
        }
    }
}