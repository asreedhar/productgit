using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Common.Core.Memento;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Mappers
{
    public interface ICustomizationMapper<TCustomization, TEntity>
        where TCustomization : ICustomization<TEntity>
        where TEntity : IOriginator<TEntity>
    {
        IList<TCustomization> Fetch();

        IList<TCustomization> Fetch(IBroker broker);

        IList<TCustomization> Fetch(Style style);

        IList<TCustomization> Fetch(IBroker broker, Style style);

        IList<TCustomization> Save(IList<TCustomization> list);
    }
}