using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;
using FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities;
using Style=FirstLook.VehicleInformation.DomainModel.Customization.Model.Style;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Mappers
{
    public class HideCategoryHeader : CustomizationMapper<Hide<CategoryHeader>, CategoryHeader>
    {
        public override IList<Hide<CategoryHeader>> Fetch(IBroker broker, Style style)
        {
            throw new NotSupportedException();
        }

        public override IList<Hide<CategoryHeader>> Fetch(Style style)
        {
            throw new NotSupportedException();
        }

        protected override Hide<CategoryHeader> Save(CustomizationsEntity<Hide<CategoryHeader>> customizations, Hide<CategoryHeader> customization)
        {
            IMementoEntity<CategoryHeader> memento = MementoGateway.Insert(customization.Entity);

            return CustomizationGateway.Save(
                customizations.Id,
                memento,
                customization);
        }

        protected override CustomizationType CustomizationType
        {
            get { return CustomizationType.Hide; }
        }

        protected override MementoType MementoType
        {
            get { return MementoType.CategoryHeader; }
        }

        protected override Action<IDataReader> Read(IList<Hide<CategoryHeader>> list)
        {
            const string customizationPrefix = "CX_", mementoPrefix = "MX_";

            return delegate(IDataReader reader)
                       {
                           while (reader.Read())
                           {
                               HideEntity<CategoryHeader> item = new HideEntity<CategoryHeader>(
                                   GetCustomizationEntity(reader),
                                   GetAuditRow(reader, customizationPrefix),
                                   new CategoryHeaderMementoEntity(
                                       reader.GetInt32(reader.GetOrdinal("CategoryHeaderID")),
                                       GetMementoEntity(reader, mementoPrefix)));
                               list.Add(item);
                           }
                       };
        }
    }
}