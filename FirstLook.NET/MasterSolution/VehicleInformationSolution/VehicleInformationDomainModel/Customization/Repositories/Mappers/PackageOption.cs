using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;
using FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Mappers
{
    public class PackageOption : CustomizationMapper<Package<Option>, Option>
    {
        public override IList<Package<Option>> Fetch()
        {
            throw new NotSupportedException();
        }

        public override IList<Package<Option>> Fetch(IBroker broker)
        {
            throw new NotSupportedException();
        }

        protected override Package<Option> Save(CustomizationsEntity<Package<Option>> customizations, Package<Option> customization)
        {
            IMementoEntity<Option> memento = MementoGateway.Insert(customization.Entity);

            return CustomizationGateway.Save(
                customizations.Id,
                memento,
                customization);
        }

        protected override CustomizationType CustomizationType
        {
            get { return CustomizationType.Package; }
        }

        protected override MementoType MementoType
        {
            get { return MementoType.Option; }
        }

        protected override Action<IDataReader> Read(IList<Package<Option>> list)
        {
            const string customizationPrefix = "CX_", mementoPrefix = "MX_";

            return delegate(IDataReader reader)
                       {
                           while (reader.Read())
                           {
                               PackageEntity<Option> item = new PackageEntity<Option>(
                                   GetCustomizationEntity(reader),
                                   GetAuditRow(reader, customizationPrefix),
                                   new OptionMementoEntity(
                                       reader.GetInt32(reader.GetOrdinal("StyleID")),
                                       reader.GetInt32(reader.GetOrdinal("Sequence")),
                                       reader.GetString(reader.GetOrdinal("OptionCode")),
                                       GetMementoEntity(reader, mementoPrefix)));
                               list.Add(item);
                           }
                       };
        }
    }
}
