using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;
using FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities;
using Style=FirstLook.VehicleInformation.DomainModel.Customization.Model.Style;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Mappers
{
    public class ChangeNameCategory : CustomizationMapper<ChangeName<Category>, Category>
    {
        public override IList<ChangeName<Category>> Fetch(IBroker broker, Style style)
        {
            throw new NotSupportedException();
        }

        public override IList<ChangeName<Category>> Fetch(Style style)
        {
            throw new NotSupportedException();
        }

        protected override ChangeName<Category> Save(CustomizationsEntity<ChangeName<Category>> customizations, ChangeName<Category> customization)
        {
            IMementoEntity<Category> memento = MementoGateway.Insert(customization.Entity);

            return CustomizationGateway.Save(
                customizations.Id,
                memento,
                customization);
        }

        protected override CustomizationType CustomizationType
        {
            get { return CustomizationType.ChangeName; }
        }

        protected override MementoType MementoType
        {
            get { return MementoType.Category; }
        }

        protected override Action<IDataReader> Read(IList<ChangeName<Category>> list)
        {
            const string customizationPrefix = "CX_", mementoPrefix = "MX_";

            return delegate(IDataReader reader)
                       {
                           while (reader.Read())
                           {
                               ChangeNameEntity<Category> item = new ChangeNameEntity<Category>(
                                   GetCustomizationEntity(reader),
                                   GetAuditRow(reader, customizationPrefix),
                                   new CategoryMementoEntity(
                                       reader.GetInt32(reader.GetOrdinal("CategoryID")),
                                       GetMementoEntity(reader, mementoPrefix)),
                                   reader.GetString(reader.GetOrdinal("Name")));
                               list.Add(item);
                           }
                       };
        }
    }
}