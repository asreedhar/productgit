using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;
using FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities;
using Style=FirstLook.VehicleInformation.DomainModel.Customization.Model.Style;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Mappers
{
    public class HideStandardHeader : CustomizationMapper<Hide<StandardHeader>, StandardHeader>
    {
        public override IList<Hide<StandardHeader>> Fetch(IBroker broker, Style style)
        {
            throw new NotSupportedException();
        }

        public override IList<Hide<StandardHeader>> Fetch(Style style)
        {
            throw new NotSupportedException();
        }

        protected override Hide<StandardHeader> Save(CustomizationsEntity<Hide<StandardHeader>> customizations, Hide<StandardHeader> customization)
        {
            IMementoEntity<StandardHeader> memento = MementoGateway.Insert(customization.Entity);

            return CustomizationGateway.Save(
                customizations.Id,
                memento,
                customization);
        }

        protected override CustomizationType CustomizationType
        {
            get { return CustomizationType.Hide; }
        }

        protected override MementoType MementoType
        {
            get { return MementoType.StandardHeader; }
        }

        protected override Action<IDataReader> Read(IList<Hide<StandardHeader>> list)
        {
            const string customizationPrefix = "CX_", mementoPrefix = "MX_";

            return delegate(IDataReader reader)
                       {
                           while (reader.Read())
                           {
                               HideEntity<StandardHeader> item = new HideEntity<StandardHeader>(
                                   GetCustomizationEntity(reader),
                                   GetAuditRow(reader, customizationPrefix),
                                   new StandardHeaderMementoEntity(
                                       reader.GetInt32(reader.GetOrdinal("StandardHeaderID")),
                                       GetMementoEntity(reader, mementoPrefix)));
                               list.Add(item);
                           }
                       };
        }
    }
}