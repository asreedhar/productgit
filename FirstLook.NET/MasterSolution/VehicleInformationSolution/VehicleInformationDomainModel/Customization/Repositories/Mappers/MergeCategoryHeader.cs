using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;
using FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities;
using Style=FirstLook.VehicleInformation.DomainModel.Customization.Model.Style;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Mappers
{
    public class MergeCategoryHeader : CustomizationMapper<Merge<CategoryHeader>, CategoryHeader>
    {
        public override IList<Merge<CategoryHeader>> Fetch(IBroker broker, Style style)
        {
            throw new NotSupportedException();
        }

        public override IList<Merge<CategoryHeader>> Fetch(Style style)
        {
            throw new NotSupportedException();
        }

        protected override Merge<CategoryHeader> Save(CustomizationsEntity<Merge<CategoryHeader>> customizations, Merge<CategoryHeader> customization)
        {
            IMementoEntity<CategoryHeader> memento = MementoGateway.Insert(customization.Entity);

            IMementoEntity<CategoryHeader> parent = MementoGateway.Insert(customization.Parent);

            return CustomizationGateway.Save(
                customizations.Id,
                memento,
                parent,
                customization);
        }

        protected override CustomizationType CustomizationType
        {
            get { return CustomizationType.Merge; }
        }

        protected override MementoType MementoType
        {
            get { return MementoType.CategoryHeader; }
        }

        protected override Action<IDataReader> Read(IList<Merge<CategoryHeader>> list)
        {
            const string customizationPrefix = "CX_", mementoPrefix = "MX_", parentPrefixA = "PP_", parentPrefixB = "PX_";

            return delegate(IDataReader reader)
                       {
                           while (reader.Read())
                           {
                               MergeEntity<CategoryHeader> item = new MergeEntity<CategoryHeader>(
                                   GetCustomizationEntity(reader),
                                   GetAuditRow(reader, customizationPrefix),
                                   new CategoryHeaderMementoEntity(
                                       reader.GetInt32(reader.GetOrdinal("CategoryHeaderID")),
                                       GetMementoEntity(reader, string.Empty, mementoPrefix)),
                                   new CategoryHeaderMementoEntity(
                                       reader.GetInt32(reader.GetOrdinal(parentPrefixA + "CategoryHeaderID")),
                                       GetMementoEntity(reader, parentPrefixA, parentPrefixB))
                                   );
                               list.Add(item);
                           }
                       };
        }
    }
}