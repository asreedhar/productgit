using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;
using FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities;
using Style=FirstLook.VehicleInformation.DomainModel.Customization.Model.Style;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Mappers
{
    public class ChangeNameOptionHeader : CustomizationMapper<ChangeName<OptionHeader>, OptionHeader>
    {
        public override IList<ChangeName<OptionHeader>> Fetch(IBroker broker, Style style)
        {
            throw new NotSupportedException();
        }

        public override IList<ChangeName<OptionHeader>> Fetch(Style style)
        {
            throw new NotSupportedException();
        }

        protected override ChangeName<OptionHeader> Save(CustomizationsEntity<ChangeName<OptionHeader>> customizations, ChangeName<OptionHeader> customization)
        {
            IMementoEntity<OptionHeader> memento = MementoGateway.Insert(customization.Entity);

            return CustomizationGateway.Save(
                customizations.Id,
                memento,
                customization);
        }

        protected override CustomizationType CustomizationType
        {
            get { return CustomizationType.ChangeName; }
        }

        protected override MementoType MementoType
        {
            get { return MementoType.OptionHeader; }
        }

        protected override Action<IDataReader> Read(IList<ChangeName<OptionHeader>> list)
        {
            const string customizationPrefix = "CX_", mementoPrefix = "MX_";

            return delegate(IDataReader reader)
                       {
                           while (reader.Read())
                           {
                               ChangeNameEntity<OptionHeader> item = new ChangeNameEntity<OptionHeader>(
                                   GetCustomizationEntity(reader),
                                   GetAuditRow(reader, customizationPrefix),
                                   new OptionHeaderMementoEntity(
                                       reader.GetInt32(reader.GetOrdinal("OptionHeaderID")),
                                       GetMementoEntity(reader, mementoPrefix)),
                                   reader.GetString(reader.GetOrdinal("Name")));
                               list.Add(item);
                           }
                       };
        }
    }
}