using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;
using FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities;
using Style=FirstLook.VehicleInformation.DomainModel.Customization.Model.Style;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Mappers
{
    public class ChangePositionCategoryHeader : CustomizationMapper<ChangePosition<CategoryHeader>, CategoryHeader>
    {
        public override IList<ChangePosition<CategoryHeader>> Fetch(IBroker broker, Style style)
        {
            throw new NotSupportedException();
        }

        public override IList<ChangePosition<CategoryHeader>> Fetch(Style style)
        {
            throw new NotSupportedException();
        }

        protected override ChangePosition<CategoryHeader> Save(CustomizationsEntity<ChangePosition<CategoryHeader>> customizations, ChangePosition<CategoryHeader> customization)
        {
            IMementoEntity<CategoryHeader> memento = MementoGateway.Insert(customization.Entity);

            return CustomizationGateway.Save(
                customizations.Id,
                memento,
                customization);
        }

        protected override CustomizationType CustomizationType
        {
            get { return CustomizationType.ChangePosition; }
        }

        protected override MementoType MementoType
        {
            get { return MementoType.CategoryHeader; }
        }

        protected override Action<IDataReader> Read(IList<ChangePosition<CategoryHeader>> list)
        {
            const string customizationPrefix = "CX_", mementoPrefix = "MX_";

            return delegate(IDataReader reader)
                       {
                           while (reader.Read())
                           {
                               ChangePositionEntity<CategoryHeader> item = new ChangePositionEntity<CategoryHeader>(
                                   GetCustomizationEntity(reader),
                                   GetAuditRow(reader, customizationPrefix),
                                   new CategoryHeaderMementoEntity(
                                       reader.GetInt32(reader.GetOrdinal("CategoryHeaderID")),
                                       GetMementoEntity(reader, mementoPrefix)),
                                   reader.GetInt32(reader.GetOrdinal("Position")));
                               list.Add(item);
                           }
                       };
        }
    }
}