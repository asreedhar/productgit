using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;
using FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities;
using Style=FirstLook.VehicleInformation.DomainModel.Customization.Model.Style;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Mappers
{
    public class ChangeNameStandardHeader : CustomizationMapper<ChangeName<StandardHeader>, StandardHeader>
    {
        public override IList<ChangeName<StandardHeader>> Fetch(IBroker broker, Style style)
        {
            throw new NotSupportedException();
        }

        public override IList<ChangeName<StandardHeader>> Fetch(Style style)
        {
            throw new NotSupportedException();
        }

        protected override ChangeName<StandardHeader> Save(CustomizationsEntity<ChangeName<StandardHeader>> customizations, ChangeName<StandardHeader> customization)
        {
            IMementoEntity<StandardHeader> memento = MementoGateway.Insert(customization.Entity);

            return CustomizationGateway.Save(
                customizations.Id,
                memento,
                customization);
        }

        protected override CustomizationType CustomizationType
        {
            get { return CustomizationType.ChangeName; }
        }

        protected override MementoType MementoType
        {
            get { return MementoType.StandardHeader; }
        }

        protected override Action<IDataReader> Read(IList<ChangeName<StandardHeader>> list)
        {
            const string customizationPrefix = "CX_", mementoPrefix = "MX_";

            return delegate(IDataReader reader)
                       {
                           while (reader.Read())
                           {
                               ChangeNameEntity<StandardHeader> item = new ChangeNameEntity<StandardHeader>(
                                   GetCustomizationEntity(reader),
                                   GetAuditRow(reader, customizationPrefix),
                                   new StandardHeaderMementoEntity(
                                       reader.GetInt32(reader.GetOrdinal("StandardHeaderID")),
                                       GetMementoEntity(reader, mementoPrefix)),
                                   reader.GetString(reader.GetOrdinal("Name")));
                               list.Add(item);
                           }
                       };
        }
    }
}