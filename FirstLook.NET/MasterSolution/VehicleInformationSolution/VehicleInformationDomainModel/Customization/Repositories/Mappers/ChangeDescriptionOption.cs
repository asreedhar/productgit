using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;
using FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Mappers
{
    public class ChangeDescriptionOption : CustomizationMapper<ChangeDescription<Option>, Option>
    {
        public override IList<ChangeDescription<Option>> Fetch()
        {
            throw new NotSupportedException();
        }

        public override IList<ChangeDescription<Option>> Fetch(IBroker broker)
        {
            throw new NotSupportedException();
        }

        protected override ChangeDescription<Option> Save(CustomizationsEntity<ChangeDescription<Option>> customizations, ChangeDescription<Option> customization)
        {
            IMementoEntity<Option> memento = MementoGateway.Insert(
                customization.Entity);

            return CustomizationGateway.Save(
                customizations.Id,
                memento,
                customization);
        }

        protected override CustomizationType CustomizationType
        {
            get { return CustomizationType.ChangeDescription; }
        }

        protected override MementoType MementoType
        {
            get { return MementoType.Option; }
        }

        protected override Action<IDataReader> Read(IList<ChangeDescription<Option>> list)
        {
            const string customizationPrefix = "CX_", mementoPrefix = "MX_";

            return delegate(IDataReader reader)
                       {
                           while (reader.Read())
                           {
                               ChangeDescriptionEntity<Option> item = new ChangeDescriptionEntity<Option>(
                                   GetCustomizationEntity(reader),
                                   GetAuditRow(reader, customizationPrefix),
                                   new OptionMementoEntity(
                                       reader.GetInt32(reader.GetOrdinal("StyleID")),
                                       reader.GetInt32(reader.GetOrdinal("Sequence")),
                                       reader.GetString(reader.GetOrdinal("OptionCode")),
                                       GetMementoEntity(reader, mementoPrefix)),
                                   reader.GetString(reader.GetOrdinal("Description")));
                               list.Add(item);
                           }
                       };
        }
    }
}
