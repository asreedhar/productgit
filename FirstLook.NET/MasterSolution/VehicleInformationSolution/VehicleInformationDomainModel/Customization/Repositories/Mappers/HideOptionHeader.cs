using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;
using FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities;
using Style=FirstLook.VehicleInformation.DomainModel.Customization.Model.Style;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Mappers
{
    public class HideOptionHeader : CustomizationMapper<Hide<OptionHeader>, OptionHeader>
    {
        public override IList<Hide<OptionHeader>> Fetch(IBroker broker, Style style)
        {
            throw new NotSupportedException();
        }

        public override IList<Hide<OptionHeader>> Fetch(Style style)
        {
            throw new NotSupportedException();
        }

        protected override Hide<OptionHeader> Save(CustomizationsEntity<Hide<OptionHeader>> customizations, Hide<OptionHeader> customization)
        {
            IMementoEntity<OptionHeader> memento = MementoGateway.Insert(customization.Entity);

            return CustomizationGateway.Save(
                customizations.Id,
                memento,
                customization);
        }

        protected override CustomizationType CustomizationType
        {
            get { return CustomizationType.Hide; }
        }

        protected override MementoType MementoType
        {
            get { return MementoType.OptionHeader; }
        }

        protected override Action<IDataReader> Read(IList<Hide<OptionHeader>> list)
        {
            const string customizationPrefix = "CX_", mementoPrefix = "MX_";

            return delegate(IDataReader reader)
                       {
                           while (reader.Read())
                           {
                               HideEntity<OptionHeader> item = new HideEntity<OptionHeader>(
                                   GetCustomizationEntity(reader),
                                   GetAuditRow(reader, customizationPrefix),
                                   new OptionHeaderMementoEntity(
                                       reader.GetInt32(reader.GetOrdinal("OptionHeaderID")),
                                       GetMementoEntity(reader, mementoPrefix)));
                               list.Add(item);
                           }
                       };
        }
    }
}