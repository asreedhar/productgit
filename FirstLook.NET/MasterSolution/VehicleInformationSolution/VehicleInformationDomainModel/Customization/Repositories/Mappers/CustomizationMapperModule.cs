using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Mappers
{
    public class CustomizationMapperModule : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<
                ICustomizationMapper<ChangeDescription<Option>, Option>,
                ChangeDescriptionOption>(ImplementationScope.Shared);

            registry.Register<
                ICustomizationMapper<ChangeLocation<CategoryHeader>, CategoryHeader>,
                ChangeLocationCategoryHeader>(ImplementationScope.Shared);

            registry.Register<
                ICustomizationMapper<ChangeLocation<OptionHeader>, OptionHeader>,
                ChangeLocationOptionHeader>(ImplementationScope.Shared);

            registry.Register<
                ICustomizationMapper<ChangeLocation<StandardHeader>, StandardHeader>,
                ChangeLocationStandardHeader>(ImplementationScope.Shared);

            registry.Register<
                ICustomizationMapper<ChangeName<Category>, Category>,
                ChangeNameCategory>(ImplementationScope.Shared);

            registry.Register<
                ICustomizationMapper<ChangeName<Option>, Option>,
                ChangeNameOption>(ImplementationScope.Shared);

            registry.Register<
                ICustomizationMapper<ChangeName<Standard>, Standard>,
                ChangeNameStandard>(ImplementationScope.Shared);

            registry.Register<
                ICustomizationMapper<ChangeName<CategoryHeader>, CategoryHeader>,
                ChangeNameCategoryHeader>(ImplementationScope.Shared);

            registry.Register<
                ICustomizationMapper<ChangeName<OptionHeader>, OptionHeader>,
                ChangeNameOptionHeader>(ImplementationScope.Shared);

            registry.Register<
                ICustomizationMapper<ChangeName<StandardHeader>, StandardHeader>,
                ChangeNameStandardHeader>(ImplementationScope.Shared);

            registry.Register<
                ICustomizationMapper<ChangePosition<CategoryHeader>, CategoryHeader>,
                ChangePositionCategoryHeader>(ImplementationScope.Shared);

            registry.Register<
                ICustomizationMapper<Hide<CategoryHeader>, CategoryHeader>,
                HideCategoryHeader>(ImplementationScope.Shared);

            registry.Register<
                ICustomizationMapper<Hide<OptionHeader>, OptionHeader>,
                HideOptionHeader>(ImplementationScope.Shared);

            registry.Register<
                ICustomizationMapper<Hide<StandardHeader>, StandardHeader>,
                HideStandardHeader>(ImplementationScope.Shared);

            registry.Register<
                ICustomizationMapper<Merge<CategoryHeader>, CategoryHeader>,
                MergeCategoryHeader>(ImplementationScope.Shared);

            registry.Register<
                ICustomizationMapper<Package<Option>, Option>,
                PackageOption>(ImplementationScope.Shared);
        }
    }
}
