using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;
using FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Mappers
{
    public class ChangeNameStandard : CustomizationMapper<ChangeName<Standard>, Standard>
    {
        public override IList<ChangeName<Standard>> Fetch()
        {
            throw new NotSupportedException();
        }

        public override IList<ChangeName<Standard>> Fetch(IBroker broker)
        {
            throw new NotSupportedException();
        }

        protected override ChangeName<Standard> Save(CustomizationsEntity<ChangeName<Standard>> customizations, ChangeName<Standard> customization)
        {
            IMementoEntity<Standard> memento = MementoGateway.Insert(customization.Entity);

            return CustomizationGateway.Save(
                customizations.Id,
                memento,
                customization);
        }

        protected override CustomizationType CustomizationType
        {
            get { return CustomizationType.ChangeName; }
        }

        protected override MementoType MementoType
        {
            get { return MementoType.Standard; }
        }

        protected override Action<IDataReader> Read(IList<ChangeName<Standard>> list)
        {
            const string customizationPrefix = "CX_", mementoPrefix = "MX_";

            return delegate(IDataReader reader)
                       {
                           while (reader.Read())
                           {
                               ChangeNameEntity<Standard> item = new ChangeNameEntity<Standard>(
                                   GetCustomizationEntity(reader),
                                   GetAuditRow(reader, customizationPrefix),
                                   new StandardMementoEntity(
                                       reader.GetInt32(reader.GetOrdinal("StyleID")),
                                       reader.GetInt32(reader.GetOrdinal("Sequence")),
                                       GetMementoEntity(reader, mementoPrefix)),
                                   reader.GetString(reader.GetOrdinal("Name")));
                               list.Add(item);
                           }
                       };
        }
    }
}
