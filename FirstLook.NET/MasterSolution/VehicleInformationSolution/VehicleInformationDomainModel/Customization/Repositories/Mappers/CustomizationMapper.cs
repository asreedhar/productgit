using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Common.Core.Memento;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;
using FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities;
using FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Gateways;
using FirstLook.VehicleInformation.DomainModel.Utility;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Mappers
{
    public abstract class CustomizationMapper<TCustomization, TEntity> : MapperBase, ICustomizationMapper<TCustomization, TEntity>
        where TCustomization : ICustomization<TEntity>
        where TEntity : IOriginator<TEntity>
    {
        private readonly CustomizationGateway _customizationGateway = new CustomizationGateway();

        private readonly CustomizationsGateway _customizationsGateway = new CustomizationsGateway();

        private readonly MementoGateway _mementoGateway = new MementoGateway();

        public CustomizationGateway CustomizationGateway
        {
            get { return _customizationGateway; }
        }

        public CustomizationsGateway CustomizationsGateway
        {
            get { return _customizationsGateway; }
        }

        public MementoGateway MementoGateway
        {
            get { return _mementoGateway; }
        }

        public virtual IList<TCustomization> Fetch()
        {
            IList<TCustomization> list = CustomizationsGateway.Fetch<TCustomization, TEntity>();

            CustomizationsEntity<TCustomization> entity = list as CustomizationsEntity<TCustomization>;

            if (entity != null)
            {
                ICustomizationStore store = RegistryFactory.GetRegistry().Resolve<ICustomizationStore>();

                store.Customizations_Fetch(
                    Read(list),
                    entity.Id,
                    CustomizationType,
                    MementoType,
                    null);
            }

            return list;
        }

        public virtual IList<TCustomization> Fetch(IBroker broker)
        {
            IList<TCustomization> list = CustomizationsGateway.Fetch<TCustomization, TEntity>(broker);

            CustomizationsEntity<TCustomization> entity = list as CustomizationsEntity<TCustomization>;

            if (entity != null)
            {
                ICustomizationStore store = RegistryFactory.GetRegistry().Resolve<ICustomizationStore>();

                store.Customizations_Fetch(
                    Read(list),
                    entity.Id,
                    CustomizationType,
                    MementoType,
                    null);
            }

            return list;
        }

        public virtual IList<TCustomization> Fetch(Style style)
        {
            IList<TCustomization> list = CustomizationsGateway.Fetch<TCustomization, TEntity>();

            CustomizationsEntity<TCustomization> entity = list as CustomizationsEntity<TCustomization>;

            if (entity != null)
            {
                ICustomizationStore store = RegistryFactory.GetRegistry().Resolve<ICustomizationStore>();

                store.Customizations_Fetch(
                    Read(list),
                    entity.Id,
                    CustomizationType,
                    MementoType,
                    style);
            }

            return list;
        }

        public virtual IList<TCustomization> Fetch(IBroker broker, Style style)
        {
            IList<TCustomization> list = CustomizationsGateway.Fetch<TCustomization, TEntity>(broker);

            CustomizationsEntity<TCustomization> entity = list as CustomizationsEntity<TCustomization>;

            if (entity != null)
            {
                ICustomizationStore store = RegistryFactory.GetRegistry().Resolve<ICustomizationStore>();

                store.Customizations_Fetch(
                    Read(list),
                    entity.Id,
                    CustomizationType,
                    MementoType,
                    style);
            }

            return list;
        }

        public IList<TCustomization> Save(IList<TCustomization> customizations)
        {
            CustomizationsEntity<TCustomization> entity = customizations as CustomizationsEntity<TCustomization>;

            if (entity != null)
            {
                if (entity.IsDirty)
                {
                    foreach (TCustomization item in entity.DeletedList)
                    {
                        ISavable savable = item as ISavable;

                        if (savable != null)
                        {
                            if (savable.IsDeleted)
                            {
                                Save(entity, item);
                            }
                        }
                    }

                    entity.DeletedList.Clear();

                    // use a for loop so we can modify the array ...

                    for (int i = 0, l = entity.Count; i < l; i++)
                    {
                        entity[i] = Save(entity, entity[i]);
                    }
                }
            }
            else
            {
                Customizations<TCustomization> list = customizations as Customizations<TCustomization>;

                if (list != null)
                {
                    entity = CustomizationsGateway.Insert<TCustomization, TEntity>(list);

                    foreach (TCustomization item in customizations)
                    {
                        entity.Add(Save(entity, item));
                    }
                }
                else
                {
                    throw new NotSupportedException();
                }
            }

            return entity;
        }

        protected abstract TCustomization Save(
            CustomizationsEntity<TCustomization> customizations,
            TCustomization customization);

        protected abstract CustomizationType CustomizationType { get; }

        protected abstract MementoType MementoType { get; }

        protected abstract Action<IDataReader> Read(IList<TCustomization> list);

        protected static MementoEntity GetMementoEntity(IDataReader reader, string auditPrefix)
        {
            return GetMementoEntity(reader, string.Empty, auditPrefix);
        }

        protected static MementoEntity GetMementoEntity(IDataReader reader, string prefix, string auditPrefix)
        {
            return new MementoEntity(
                reader.GetInt32(reader.GetOrdinal(prefix + "MementoID")),
                GetAuditRow(reader, auditPrefix),
                (MementoType)reader.GetByte(reader.GetOrdinal(prefix + "MementoTypeID")));
        }

        protected static AuditRow GetAuditRow(IDataReader reader, string prefix)
        {
            return CustomizationStoreHelper.GetAuditRow(reader, prefix);
        }

        protected static CustomizationEntity GetCustomizationEntity(IDataReader reader)
        {
            return new CustomizationEntity(
                reader.GetInt32(reader.GetOrdinal("CustomizationID")),
                GetAuditRow(reader, "XX_"),
                (CustomizationType)reader.GetByte(reader.GetOrdinal("CustomizationTypeID")));
        }
    }
}