using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;
using FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities;
using Style=FirstLook.VehicleInformation.DomainModel.Customization.Model.Style;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Mappers
{
    public class ChangeLocationOptionHeader : CustomizationMapper<ChangeLocation<OptionHeader>, OptionHeader>
    {
        public override IList<ChangeLocation<OptionHeader>> Fetch(IBroker broker, Style style)
        {
            throw new NotSupportedException();
        }

        public override IList<ChangeLocation<OptionHeader>> Fetch(Style style)
        {
            throw new NotSupportedException();
        }

        protected override ChangeLocation<OptionHeader> Save(CustomizationsEntity<ChangeLocation<OptionHeader>> customizations, ChangeLocation<OptionHeader> customization)
        {
            IMementoEntity<OptionHeader> memento = MementoGateway.Insert(customization.Entity);

            return CustomizationGateway.Save(
                customizations.Id,
                memento,
                customization);
        }

        protected override CustomizationType CustomizationType
        {
            get { return CustomizationType.ChangeLocation; }
        }

        protected override MementoType MementoType
        {
            get { return MementoType.OptionHeader; }
        }

        protected override Action<IDataReader> Read(IList<ChangeLocation<OptionHeader>> list)
        {
            const string customizationPrefix = "CX_", mementoPrefix = "MX_";

            return delegate(IDataReader reader)
                       {
                           while (reader.Read())
                           {
                               ChangeLocationEntity<OptionHeader> item = new ChangeLocationEntity<OptionHeader>(
                                   GetCustomizationEntity(reader),
                                   GetAuditRow(reader, customizationPrefix),
                                   new OptionHeaderMementoEntity(
                                       reader.GetInt32(reader.GetOrdinal("OptionHeaderID")),
                                       GetMementoEntity(reader, mementoPrefix)),
                                   (Location)reader.GetByte(reader.GetOrdinal("LocationID")));
                               list.Add(item);
                           }
                       };
        }
    }
}