using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;
using FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities;
using Style=FirstLook.VehicleInformation.DomainModel.Customization.Model.Style;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Mappers
{
    public class ChangeLocationCategoryHeader : CustomizationMapper<ChangeLocation<CategoryHeader>, CategoryHeader>
    {
        public override IList<ChangeLocation<CategoryHeader>> Fetch(IBroker broker, Style style)
        {
            throw new NotSupportedException();
        }

        public override IList<ChangeLocation<CategoryHeader>> Fetch(Style style)
        {
            throw new NotSupportedException();
        }

        protected override ChangeLocation<CategoryHeader> Save(CustomizationsEntity<ChangeLocation<CategoryHeader>> customizations, ChangeLocation<CategoryHeader> customization)
        {
            IMementoEntity<CategoryHeader> memento = MementoGateway.Insert(customization.Entity);

            return CustomizationGateway.Save(
                customizations.Id,
                memento,
                customization);
        }

        protected override CustomizationType CustomizationType
        {
            get { return CustomizationType.ChangeLocation; }
        }

        protected override MementoType MementoType
        {
            get { return MementoType.CategoryHeader; }
        }

        protected override Action<IDataReader> Read(IList<ChangeLocation<CategoryHeader>> list)
        {
            const string customizationPrefix = "CX_", mementoPrefix = "MX_";

            return delegate(IDataReader reader)
                       {
                           while (reader.Read())
                           {
                               ChangeLocationEntity<CategoryHeader> item = new ChangeLocationEntity<CategoryHeader>(
                                   GetCustomizationEntity(reader),
                                   GetAuditRow(reader, customizationPrefix),
                                   new CategoryHeaderMementoEntity(
                                       reader.GetInt32(reader.GetOrdinal("CategoryHeaderID")),
                                       GetMementoEntity(reader, mementoPrefix)),
                                   (Location) reader.GetByte(reader.GetOrdinal("LocationID")));
                               list.Add(item);
                           }
                       };
        }
    }
}