using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;
using FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Mappers
{
    public class ChangeNameOption : CustomizationMapper<ChangeName<Option>, Option>
    {
        public override IList<ChangeName<Option>> Fetch()
        {
            throw new NotSupportedException();
        }

        public override IList<ChangeName<Option>> Fetch(IBroker broker)
        {
            throw new NotSupportedException();
        }

        protected override ChangeName<Option> Save(CustomizationsEntity<ChangeName<Option>> customizations, ChangeName<Option> customization)
        {
            IMementoEntity<Option> memento = MementoGateway.Insert(customization.Entity);

            return CustomizationGateway.Save(
                customizations.Id,
                memento,
                customization);
        }

        protected override CustomizationType CustomizationType
        {
            get { return CustomizationType.ChangeName; }
        }

        protected override MementoType MementoType
        {
            get { return MementoType.Option; }
        }

        protected override Action<IDataReader> Read(IList<ChangeName<Option>> list)
        {
            const string customizationPrefix = "CX_", mementoPrefix = "MX_";

            return delegate(IDataReader reader)
                       {
                           while (reader.Read())
                           {
                               ChangeNameEntity<Option> item = new ChangeNameEntity<Option>(
                                   GetCustomizationEntity(reader),
                                   GetAuditRow(reader, customizationPrefix),
                                   new OptionMementoEntity(
                                       reader.GetInt32(reader.GetOrdinal("StyleID")),
                                       reader.GetInt32(reader.GetOrdinal("Sequence")),
                                       reader.GetString(reader.GetOrdinal("OptionCode")),
                                       GetMementoEntity(reader, mementoPrefix)),
                                   reader.GetString(reader.GetOrdinal("Name")));
                               list.Add(item);
                           }
                       };
        }
    }
}
