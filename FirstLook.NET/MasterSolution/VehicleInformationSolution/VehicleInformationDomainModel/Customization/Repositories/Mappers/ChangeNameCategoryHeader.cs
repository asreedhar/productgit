using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;
using FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities;
using Style=FirstLook.VehicleInformation.DomainModel.Customization.Model.Style;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Mappers
{
    public class ChangeNameCategoryHeader : CustomizationMapper<ChangeName<CategoryHeader>, CategoryHeader>
    {
        public override IList<ChangeName<CategoryHeader>> Fetch(IBroker broker, Style style)
        {
            throw new NotSupportedException();
        }

        public override IList<ChangeName<CategoryHeader>> Fetch(Style style)
        {
            throw new NotSupportedException();
        }

        protected override ChangeName<CategoryHeader> Save(CustomizationsEntity<ChangeName<CategoryHeader>> customizations, ChangeName<CategoryHeader> customization)
        {
            IMementoEntity<CategoryHeader> memento = MementoGateway.Insert(customization.Entity);

            return CustomizationGateway.Save(
                customizations.Id,
                memento,
                customization);
        }

        protected override CustomizationType CustomizationType
        {
            get { return CustomizationType.ChangeName; }
        }

        protected override MementoType MementoType
        {
            get { return MementoType.CategoryHeader; }
        }

        protected override Action<IDataReader> Read(IList<ChangeName<CategoryHeader>> list)
        {
            const string customizationPrefix = "CX_", mementoPrefix = "MX_";

            return delegate(IDataReader reader)
                       {
                           while (reader.Read())
                           {
                               ChangeNameEntity<CategoryHeader> item = new ChangeNameEntity<CategoryHeader>(
                                   GetCustomizationEntity(reader),
                                   GetAuditRow(reader, customizationPrefix),
                                   new CategoryHeaderMementoEntity(
                                       reader.GetInt32(reader.GetOrdinal("CategoryHeaderID")),
                                       GetMementoEntity(reader, mementoPrefix)),
                                   reader.GetString(reader.GetOrdinal("Name")));
                               list.Add(item);
                           }
                       };
        }
    }
}