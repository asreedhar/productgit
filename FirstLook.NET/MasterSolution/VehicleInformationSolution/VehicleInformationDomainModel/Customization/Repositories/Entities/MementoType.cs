namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities
{
    public enum MementoType : byte
    {
        NotDefined,
        CategoryHeader,
        OptionHeader,
        StandardHeader,
        Category,
        Option,
        Standard
    }
}
