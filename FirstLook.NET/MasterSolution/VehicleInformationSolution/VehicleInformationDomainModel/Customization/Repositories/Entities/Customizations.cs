using System.Collections.Generic;
using System.ComponentModel;
using FirstLook.VehicleInformation.DomainModel.Utility;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities
{
    public class Customizations<T> : BindingList<T>
    {
        private readonly Owner _owner;

        public Customizations(Owner owner)
        {
            _owner = owner;
        }

        public Owner Owner
        {
            get { return _owner; }
        }
    }

    public class CustomizationsEntity<T> : Customizations<T>
    {
        private readonly int _id;

        public CustomizationsEntity(Owner owner, int id) : base(owner)
        {
            _id = id;
        }

        public int Id
        {
            get { return _id; }
        }

        private List<T> _deletedList;

        protected internal List<T> DeletedList
        {
            get
            {
                if (_deletedList == null)
                {
                    _deletedList = new List<T>();
                }
                return _deletedList;
            }
        }

        private void DeleteChild(T child)
        {
            ISavable savable = child as ISavable;

            if (savable != null)
            {
                savable.MarkDeleted();
            }
            
            DeletedList.Add(child);
        }

        protected override void RemoveItem(int index)
        {
            T child = this[index];
            base.RemoveItem(index);
            DeleteChild(child);
        }

        protected override void ClearItems()
        {
            while (Count > 0) RemoveItem(0);

            base.ClearItems();
        }

        protected override void SetItem(int index, T item)
        {
            T child = default(T);
            if (!ReferenceEquals(this[index], item))
                child = this[index];
            base.SetItem(index, item);
            if (child != null)
                DeleteChild(child);
        }

        public bool IsDirty
        {
            get
            {
                // true if we have any deleted non-new items the list is dirty

                foreach (T item in DeletedList)
                {
                    ISavable savable = item as ISavable;

                    if (savable != null)
                    {
                        if (!savable.IsNew)
                        {
                            return true;
                        }
                    }
                }

                // true if we have any new (non ISavable) or dirty objects

                foreach (T item in this)
                {
                    ISavable savable = item as ISavable;

                    if (savable != null)
                    {
                        if (savable.IsDirty)
                        {
                            return true;
                        }
                    }
                    else
                    {
                        return true;
                    }
                }

                // else

                return false;
            }
        }
    }
}
