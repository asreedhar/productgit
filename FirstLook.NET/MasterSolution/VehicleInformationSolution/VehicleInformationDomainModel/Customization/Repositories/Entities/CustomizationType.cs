namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities
{
    public enum CustomizationType
    {
        NotDefined,
        Associate,
        ChangeDescription,
        ChangeName,
        ChangeLocation,
        ChangePosition,
        Hide,
        Merge,
        Package
    }
}
