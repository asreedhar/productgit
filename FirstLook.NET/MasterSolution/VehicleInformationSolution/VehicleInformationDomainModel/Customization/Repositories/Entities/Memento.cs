using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Common.Core.Memento;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities
{
    public class MementoEntity
    {
        private readonly int _id;
        private readonly AuditRow _auditRow;
        private readonly MementoType _type;

        public MementoEntity(int id, AuditRow auditRow, MementoType type)
        {
            _id = id;
            _auditRow = auditRow;
            _type = type;
        }

        public int Id
        {
            get { return _id; }
        }

        public AuditRow AuditRow
        {
            get { return _auditRow; }
        }

        public MementoType Type
        {
            get { return _type; }
        }
    }

    public interface IMementoEntity<TEntity> : IMemento<TEntity>
        where TEntity : IOriginator<TEntity>
    {
        MementoEntity Entity { get; }
    }

    public class CategoryHeaderMementoFactory : IMementoFactory<CategoryHeader>
    {
        public IMemento<CategoryHeader> For(CategoryHeader item)
        {
            return new CategoryHeaderMemento(item.Id);
        }
    }

    public class CategoryHeaderMemento : IMemento<CategoryHeader>
    {
        private readonly int _id;

        public CategoryHeaderMemento(int id)
        {
            _id = id;
        }

        public int Id
        {
            get { return _id; }
        }

        public bool Equals(CategoryHeader other)
        {
            if (other == null) return false;

            return other.Id == Id;
        }
    }

    public class CategoryHeaderMementoEntity : CategoryHeaderMemento, IMementoEntity<CategoryHeader>
    {
        private readonly MementoEntity _entity;

        public CategoryHeaderMementoEntity(int id, MementoEntity memento) : base(id)
        {
            _entity = memento;
        }

        public MementoEntity Entity
        {
            get { return _entity; }
        }
    }

    public class OptionHeaderMementoFactory : IMementoFactory<OptionHeader>
    {
        public IMemento<OptionHeader> For(OptionHeader item)
        {
            return new OptionHeaderMemento(item.Id);
        }
    }

    public class OptionHeaderMemento : IMemento<OptionHeader>
    {
        private readonly int _id;

        public OptionHeaderMemento(int id)
        {
            _id = id;
        }

        public int Id
        {
            get { return _id; }
        }

        public bool Equals(OptionHeader other)
        {
            if (other == null) return false;
            return other.Id == Id;
        }
    }

    public class OptionHeaderMementoEntity : OptionHeaderMemento, IMementoEntity<OptionHeader>
    {
        private readonly MementoEntity _entity;

        public OptionHeaderMementoEntity(int id, MementoEntity memento)
            : base(id)
        {
            _entity = memento;
        }

        public MementoEntity Entity
        {
            get { return _entity; }
        }
    }

    public class StandardHeaderMementoFactory : IMementoFactory<StandardHeader>
    {
        public IMemento<StandardHeader> For(StandardHeader item)
        {
            return new StandardHeaderMemento(item.Id);
        }
    }

    public class StandardHeaderMemento : IMemento<StandardHeader>
    {
        private readonly int _id;

        public StandardHeaderMemento(int id)
        {
            _id = id;
        }

        public int Id
        {
            get { return _id; }
        }

        public bool Equals(StandardHeader other)
        {
            if (other == null) return false;
            return other.Id == Id;
        }
    }

    public class StandardHeaderMementoEntity : StandardHeaderMemento, IMementoEntity<StandardHeader>
    {
        private readonly MementoEntity _entity;

        public StandardHeaderMementoEntity(int id, MementoEntity memento)
            : base(id)
        {
            _entity = memento;
        }

        public MementoEntity Entity
        {
            get { return _entity; }
        }
    }

    public class CategoryMementoFactory : IMementoFactory<Category>
    {
        public IMemento<Category> For(Category item)
        {
            return new CategoryMemento(item.Id);
        }
    }

    public class CategoryMemento : IMemento<Category>
    {
        private readonly int _id;

        public CategoryMemento(int id)
        {
            _id = id;
        }

        public int Id
        {
            get { return _id; }
        }

        public bool Equals(Category other)
        {
            if (other == null) return false;
            return other.Id == Id;
        }
    }

    public class CategoryMementoEntity : CategoryMemento, IMementoEntity<Category>
    {
        private readonly MementoEntity _entity;

        public CategoryMementoEntity(int id, MementoEntity memento)
            : base(id)
        {
            _entity = memento;
        }

        public MementoEntity Entity
        {
            get { return _entity; }
        }
    }

    public class OptionMementoFactory : IMementoFactory<Option>
    {
        public IMemento<Option> For(Option item)
        {
            return new OptionMemento(item.StyleId, item.Sequence, item.Code);
        }
    }

    public class OptionMemento : IMemento<Option>
    {
        private readonly int _styleId;
        private readonly int _sequence;
        private readonly string _code;

        public OptionMemento(int styleId, int sequence, string code)
        {
            _styleId = styleId;
            _sequence = sequence;
            _code = code;
        }

        public int StyleId
        {
            get { return _styleId; }
        }

        public int Sequence
        {
            get { return _sequence; }
        }

        public string Code
        {
            get { return _code; }
        }

        public bool Equals(Option other)
        {
            if (other == null) return false;

            return StyleId == other.StyleId &&
                   Sequence == other.Sequence &&
                   Equals(Code, other.Code);
        }
    }

    public class OptionMementoEntity : OptionMemento, IMementoEntity<Option>
    {
        private readonly MementoEntity _entity;

        public OptionMementoEntity(int styleId, int sequence, string code, MementoEntity memento)
            : base(styleId, sequence, code)
        {
            _entity = memento;
        }

        public MementoEntity Entity
        {
            get { return _entity; }
        }
    }

    public class StandardMementoFactory : IMementoFactory<Standard>
    {
        public IMemento<Standard> For(Standard item)
        {
            return new StandardMemento(item.StyleId, item.Sequence);
        }
    }

    public class StandardMemento : IMemento<Standard>
    {
        private readonly int _styleId;
        private readonly int _sequence;

        public StandardMemento(int styleId, int sequence)
        {
            _styleId = styleId;
            _sequence = sequence;
        }

        public int StyleId
        {
            get { return _styleId; }
        }

        public int Sequence
        {
            get { return _sequence; }
        }

        public bool Equals(Standard other)
        {
            if (other == null) return false;
            return other.StyleId == StyleId && other.Sequence == Sequence;
        }
    }

    public class StandardMementoEntity : StandardMemento, IMementoEntity<Standard>
    {
        private readonly MementoEntity _entity;

        public StandardMementoEntity(int styleId, int sequence, MementoEntity memento)
            : base(styleId, sequence)
        {
            _entity = memento;
        }

        public MementoEntity Entity
        {
            get { return _entity; }
        }
    }

    public class MementoFactoryModule : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IMementoFactory<CategoryHeader>, CategoryHeaderMementoFactory>(ImplementationScope.Shared);
            registry.Register<IMementoFactory<OptionHeader>, OptionHeaderMementoFactory>(ImplementationScope.Shared);
            registry.Register<IMementoFactory<StandardHeader>, StandardHeaderMementoFactory>(ImplementationScope.Shared);
            registry.Register<IMementoFactory<Category>, CategoryMementoFactory>(ImplementationScope.Shared);
            registry.Register<IMementoFactory<Option>, OptionMementoFactory>(ImplementationScope.Shared);
            registry.Register<IMementoFactory<Standard>, StandardMementoFactory>(ImplementationScope.Shared);
        }
    }
}
