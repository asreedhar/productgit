using System;
using FirstLook.Common.Core.Memento;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities
{
    public static class TypeHelper
    {
        public static CustomizationType ToCommandType<TEntity>(Type type)
            where TEntity : IOriginator<TEntity>
        {
            if (type == typeof(Associate<TEntity>))
            {
                return CustomizationType.Associate;
            }

            if (type == typeof(ChangeDescription<TEntity>))
            {
                return CustomizationType.ChangeDescription;
            }

            if (type == typeof(ChangeName<TEntity>))
            {
                return CustomizationType.ChangeName;
            }

            if (type == typeof(ChangeLocation<TEntity>))
            {
                return CustomizationType.ChangeLocation;
            }

            if (type == typeof(ChangePosition<TEntity>))
            {
                return CustomizationType.ChangePosition;
            }

            if (type == typeof(Hide<TEntity>))
            {
                return CustomizationType.Hide;
            }

            if (type == typeof(Merge<TEntity>))
            {
                return CustomizationType.Merge;
            }

            if (type == typeof(Package<TEntity>))
            {
                return CustomizationType.Package;
            }

            return CustomizationType.NotDefined;
        }

        public static MementoType ToEntityType(Type type)
        {
            if (type == typeof(Category))
            {
                return MementoType.Category;
            }

            if (type == typeof(CategoryHeader))
            {
                return MementoType.CategoryHeader;
            }

            if (type == typeof(Option))
            {
                return MementoType.Option;
            }

            if (type == typeof(OptionHeader))
            {
                return MementoType.OptionHeader;
            }

            if (type == typeof(Standard))
            {
                return MementoType.Standard;
            }

            if (type == typeof(StandardHeader))
            {
                return MementoType.StandardHeader;
            }

            return MementoType.NotDefined;
        }
    }
}
