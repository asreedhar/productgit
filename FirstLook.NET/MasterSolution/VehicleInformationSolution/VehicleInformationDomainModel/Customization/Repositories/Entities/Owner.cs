namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities
{
    public enum Owner : byte
    {
        NotDefined,
        System,
        Dealer
    }
}
