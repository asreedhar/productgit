using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Common.Core.Memento;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;
using FirstLook.VehicleInformation.DomainModel.Utility;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities
{
    public class CustomizationEntity
    {
        private readonly int _id;
        private readonly AuditRow _auditRow;
        private readonly CustomizationType _type;

        public CustomizationEntity(int id, AuditRow auditRow, CustomizationType type)
        {
            _id = id;
            _auditRow = auditRow;
            _type = type;
        }

        public int Id
        {
            get { return _id; }
        }

        public AuditRow AuditRow
        {
            get { return _auditRow; }
        }

        public CustomizationType Type
        {
            get { return _type; }
        }
    }

    public interface ICustomizationEntity<TEntity> : ICustomization<TEntity>, ISavable
        where TEntity : IOriginator<TEntity>
    {
        CustomizationEntity Customization { get; }
    }

    public class ChangeNameEntity<TEntity> : ChangeName<TEntity>, ICustomizationEntity<TEntity>
        where TEntity : IOriginator<TEntity>
    {
        private readonly CustomizationEntity _customization;
        
        public CustomizationEntity Customization
        {
            get { return _customization; }
        }

        public ChangeNameEntity()
        {
            base.PropertyChanged += _savable.PropertyChanged;
        }

        public ChangeNameEntity(CustomizationEntity customization, AuditRow auditRow, IMemento<TEntity> entity, string name)
            : this()
        {
            base.Entity = entity;
            base.Name = name;
            _auditRow = auditRow;
            _customization = customization;
            _savable.MarkOld();
        }

        #region ISavable

        private readonly Savable _savable = new Savable();

        private AuditRow _auditRow;

        public AuditRow AuditRow
        {
            get { return _auditRow; }
        }

        public bool IsNew
        {
            get { return _savable.IsNew; }
        }

        public bool IsDeleted
        {
            get { return _savable.IsDeleted; }
        }

        public bool IsDirty
        {
            get { return _savable.IsDirty; }
        }

        public void MarkDeleted()
        {
            _savable.MarkDeleted();
        }

        public void OnSaved(object sender, SavedEventArgs e)
        {
            _auditRow = e.AuditRow;

            _savable.MarkOld();
        }

        #endregion
    }

    public class ChangeDescriptionEntity<TEntity> : ChangeDescription<TEntity>, ICustomizationEntity<TEntity>
        where TEntity : IOriginator<TEntity>
    {
        private readonly CustomizationEntity _customization;

        public CustomizationEntity Customization
        {
            get { return _customization; }
        }

        public ChangeDescriptionEntity()
        {
            base.PropertyChanged += _savable.PropertyChanged;
        }

        public ChangeDescriptionEntity(CustomizationEntity customization, AuditRow auditRow, IMemento<TEntity> entity, string description)
            : this()
        {
            base.Entity = entity;
            base.Description = description;
            _auditRow = auditRow;
            _customization = customization;
            _savable.MarkOld();
        }

        #region ISavable

        private readonly Savable _savable = new Savable();

        private AuditRow _auditRow;

        public AuditRow AuditRow
        {
            get { return _auditRow; }
        }

        public bool IsNew
        {
            get { return _savable.IsNew; }
        }

        public bool IsDeleted
        {
            get { return _savable.IsDeleted; }
        }

        public bool IsDirty
        {
            get { return _savable.IsDirty; }
        }

        public void MarkDeleted()
        {
            _savable.MarkDeleted();
        }

        public void OnSaved(object sender, SavedEventArgs e)
        {
            _auditRow = e.AuditRow;

            _savable.MarkOld();
        }

        #endregion
    }

    public class ChangeLocationEntity<TEntity> : ChangeLocation<TEntity>, ICustomizationEntity<TEntity>
        where TEntity : IOriginator<TEntity>
    {
        private readonly CustomizationEntity _customization;

        public CustomizationEntity Customization
        {
            get { return _customization; }
        }

        public ChangeLocationEntity()
        {
            base.PropertyChanged += _savable.PropertyChanged;
        }

        public ChangeLocationEntity(CustomizationEntity customization, AuditRow auditRow, IMemento<TEntity> entity, Location location)
            : this()
        {
            base.Entity = entity;
            base.Location = location;
            _auditRow = auditRow;
            _customization = customization;
            _savable.MarkOld();
        }

        #region ISavable

        private readonly Savable _savable = new Savable();

        private AuditRow _auditRow;

        public AuditRow AuditRow
        {
            get { return _auditRow; }
        }

        public bool IsNew
        {
            get { return _savable.IsNew; }
        }

        public bool IsDeleted
        {
            get { return _savable.IsDeleted; }
        }

        public bool IsDirty
        {
            get { return _savable.IsDirty; }
        }

        public void MarkDeleted()
        {
            _savable.MarkDeleted();
        }

        public void OnSaved(object sender, SavedEventArgs e)
        {
            _auditRow = e.AuditRow;

            _savable.MarkOld();
        }

        #endregion
    }

    public class ChangePositionEntity<TEntity> : ChangePosition<TEntity>, ICustomizationEntity<TEntity>
        where TEntity : IOriginator<TEntity>
    {
        private readonly CustomizationEntity _customization;

        public CustomizationEntity Customization
        {
            get { return _customization; }
        }

        public ChangePositionEntity()
        {
            base.PropertyChanged += _savable.PropertyChanged;
        }

        public ChangePositionEntity(CustomizationEntity customization, AuditRow auditRow, IMemento<TEntity> entity, int position)
            : this()
        {
            base.Entity = entity;
            base.Position = position;
            _auditRow = auditRow;
            _customization = customization;
            _savable.MarkOld();
        }

        #region ISavable

        private readonly Savable _savable = new Savable();

        private AuditRow _auditRow;

        public AuditRow AuditRow
        {
            get { return _auditRow; }
        }

        public bool IsNew
        {
            get { return _savable.IsNew; }
        }

        public bool IsDeleted
        {
            get { return _savable.IsDeleted; }
        }

        public bool IsDirty
        {
            get { return _savable.IsDirty; }
        }

        public void MarkDeleted()
        {
            _savable.MarkDeleted();
        }

        public void OnSaved(object sender, SavedEventArgs e)
        {
            _auditRow = e.AuditRow;

            _savable.MarkOld();
        }

        #endregion
    }

    public class PackageEntity<TEntity> : Package<TEntity>, ICustomizationEntity<TEntity>
        where TEntity : IOriginator<TEntity>
    {
        private readonly CustomizationEntity _customization;

        public CustomizationEntity Customization
        {
            get { return _customization; }
        }

        public PackageEntity()
        {
            base.PropertyChanged += _savable.PropertyChanged;
        }

        public PackageEntity(CustomizationEntity customization, AuditRow auditRow, IMemento<TEntity> entity)
            : this()
        {
            base.Entity = entity;
            _auditRow = auditRow;
            _customization = customization;
            _savable.MarkOld();
        }

        #region ISavable

        private readonly Savable _savable = new Savable();

        private AuditRow _auditRow;

        public AuditRow AuditRow
        {
            get { return _auditRow; }
        }

        public bool IsNew
        {
            get { return _savable.IsNew; }
        }

        public bool IsDeleted
        {
            get { return _savable.IsDeleted; }
        }

        public bool IsDirty
        {
            get { return _savable.IsDirty; }
        }

        public void MarkDeleted()
        {
            _savable.MarkDeleted();
        }

        public void OnSaved(object sender, SavedEventArgs e)
        {
            _auditRow = e.AuditRow;

            _savable.MarkOld();
        }

        #endregion
    }

    public class AssociateEntity<TEntity> : Associate<TEntity>, ICustomizationEntity<TEntity>
        where TEntity : IOriginator<TEntity>
    {
        private readonly CustomizationEntity _customization;

        public CustomizationEntity Customization
        {
            get { return _customization; }
        }

        public AssociateEntity()
        {
            base.PropertyChanged += _savable.PropertyChanged;
        }

        public AssociateEntity(CustomizationEntity customization, AuditRow auditRow, IMemento<TEntity> entity, IMemento<TEntity> relation)
            : this()
        {
            base.Entity = entity;
            base.Relation = relation;
            _auditRow = auditRow;
            _customization = customization;
            _savable.MarkOld();
        }

        #region ISavable

        private readonly Savable _savable = new Savable();

        private AuditRow _auditRow;

        public AuditRow AuditRow
        {
            get { return _auditRow; }
        }

        public bool IsNew
        {
            get { return _savable.IsNew; }
        }

        public bool IsDeleted
        {
            get { return _savable.IsDeleted; }
        }

        public bool IsDirty
        {
            get { return _savable.IsDirty; }
        }

        public void MarkDeleted()
        {
            _savable.MarkDeleted();
        }

        public void OnSaved(object sender, SavedEventArgs e)
        {
            _auditRow = e.AuditRow;

            _savable.MarkOld();
        }

        #endregion
    }

    public class HideEntity<TEntity> : Hide<TEntity>, ICustomizationEntity<TEntity>
        where TEntity : IOriginator<TEntity>
    {
        private readonly CustomizationEntity _customization;

        public CustomizationEntity Customization
        {
            get { return _customization; }
        }

        public HideEntity()
        {
            base.PropertyChanged += _savable.PropertyChanged;
        }

        public HideEntity(CustomizationEntity customization, AuditRow auditRow, IMemento<TEntity> entity)
            : this()
        {
            base.Entity = entity;
            _auditRow = auditRow;
            _customization = customization;
            _savable.MarkOld();
        }

        #region ISavable

        private readonly Savable _savable = new Savable();

        private AuditRow _auditRow;

        public AuditRow AuditRow
        {
            get { return _auditRow; }
        }

        public bool IsNew
        {
            get { return _savable.IsNew; }
        }

        public bool IsDeleted
        {
            get { return _savable.IsDeleted; }
        }

        public bool IsDirty
        {
            get { return _savable.IsDirty; }
        }

        public void MarkDeleted()
        {
            _savable.MarkDeleted();
        }

        public void OnSaved(object sender, SavedEventArgs e)
        {
            _auditRow = e.AuditRow;

            _savable.MarkOld();
        }

        #endregion
    }

    public class MergeEntity<TEntity> : Merge<TEntity>, ICustomizationEntity<TEntity>
        where TEntity : IOriginator<TEntity>
    {
        private readonly CustomizationEntity _customization;

        public CustomizationEntity Customization
        {
            get { return _customization; }
        }

        public MergeEntity()
        {
            base.PropertyChanged += _savable.PropertyChanged;
        }

        public MergeEntity(CustomizationEntity customization, AuditRow auditRow, IMemento<TEntity> entity, IMemento<TEntity> parent)
            : this()
        {
            base.Entity = entity;
            base.Parent = parent;
            _auditRow = auditRow;
            _customization = customization;
            _savable.MarkOld();
        }

        #region ISavable

        private readonly Savable _savable = new Savable();

        private AuditRow _auditRow;

        public AuditRow AuditRow
        {
            get { return _auditRow; }
        }

        public bool IsNew
        {
            get { return _savable.IsNew; }
        }

        public bool IsDeleted
        {
            get { return _savable.IsDeleted; }
        }

        public bool IsDirty
        {
            get { return _savable.IsDirty; }
        }

        public void MarkDeleted()
        {
            _savable.MarkDeleted();
        }

        public void OnSaved(object sender, SavedEventArgs e)
        {
            _auditRow = e.AuditRow;

            _savable.MarkOld();
        }

        #endregion
    }
}
