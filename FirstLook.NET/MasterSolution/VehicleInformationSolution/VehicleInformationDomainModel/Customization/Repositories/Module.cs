﻿using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;
using FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Entities;
using FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Gateways;
using FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Mappers;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ICustomizationRepository, CustomizationRepository>(ImplementationScope.Shared);

            registry.Register<ICustomizationStore, DbCustomizationStore>(ImplementationScope.Shared);

            registry.Register<CustomizationMapperModule>();

            registry.Register<MementoFactoryModule>();
        }
    }
}
