using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Memento;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Customization.Model;
using FirstLook.VehicleInformation.DomainModel.Customization.Repositories.Mappers;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Repositories
{
    public class CustomizationRepository : RepositoryBase, ICustomizationRepository
    {
        protected override string DatabaseName
        {
            get { return "Vehicle"; }
        }

        public IList<TCustomization> Fetch<TCustomization, TEntity>()
            where TCustomization : ICustomization<TEntity>
            where TEntity : IOriginator<TEntity>
        {
            return DoInSession(() => Mapper<TCustomization, TEntity>().Fetch());
        }

        public IList<TCustomization> Fetch<TCustomization, TEntity>(Style style)
            where TCustomization : ICustomization<TEntity>
            where TEntity : IOriginator<TEntity>
        {
            return DoInSession(() => Mapper<TCustomization, TEntity>().Fetch(style));
        }

        public IList<TCustomization> Fetch<TCustomization, TEntity>(IBroker broker)
            where TCustomization : ICustomization<TEntity>
            where TEntity : IOriginator<TEntity>
        {
            return DoInSession(() => Mapper<TCustomization, TEntity>().Fetch(broker));
        }

        public IList<TCustomization> Fetch<TCustomization, TEntity>(IBroker broker, Style style)
            where TCustomization : ICustomization<TEntity>
            where TEntity : IOriginator<TEntity>
        {
            return DoInSession(() => Mapper<TCustomization, TEntity>().Fetch(broker, style));
        }

        public IList<TCustomization> Save<TCustomization, TEntity>(IPrincipal principal, IList<TCustomization> list)
            where TCustomization : ICustomization<TEntity>
            where TEntity : IOriginator<TEntity>
        {
            return DoInTransaction(
                delegate(IDataSession session)
                    {
                        Audit audit = session.Items["Audit"] as Audit;

                        if (audit == null)
                        {
                            audit = Resolve<IRepository>().Create(principal);

                            session.Items["Audit"] = audit;
                        }
                    },
                () => Mapper<TCustomization, TEntity>().Save(list));
        }

        private static ICustomizationMapper<TCustomization, TEntity> Mapper<TCustomization, TEntity>()
            where TCustomization : ICustomization<TEntity>
            where TEntity : IOriginator<TEntity>
        {
            return RegistryFactory.GetRegistry().Resolve<ICustomizationMapper<TCustomization, TEntity>>();
        }

        protected override void OnBeginTransaction(IDataSession session)
        {
            // nope
        }
    }
}