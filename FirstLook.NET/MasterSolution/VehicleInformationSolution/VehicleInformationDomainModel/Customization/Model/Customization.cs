using FirstLook.Common.Core;
using FirstLook.Common.Core.Memento;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Model
{
    public abstract class Customization<TEntity> : PropertyChangeSupport, ICustomization<TEntity>
        where TEntity : IOriginator<TEntity>
    {
        private IMemento<TEntity> _entity;

        public virtual IMemento<TEntity> Entity
        {
            get { return _entity; }
            set
            {
                if (!Equals(_entity,value))
                {
                    Assignment(delegate { _entity = value; }, "Entity");
                }
            }
        }

        public abstract void Accept(ICustomizationVisitor<TEntity> visitor);
    }

    public class ChangeName<TEntity> : Customization<TEntity>
        where TEntity : IOriginator<TEntity>
    {
        private string _name;

        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (!Equals(_name,value))
                {
                    Assignment(delegate { _name = value; }, "Name");
                }
            }
        }

        public override void Accept(ICustomizationVisitor<TEntity> visitor)
        {
            visitor.Visit(this);
        }
    }

    public class ChangeDescription<TEntity> : Customization<TEntity>
        where TEntity : IOriginator<TEntity>
    {
        private string _description;

        public virtual string Description
        {
            get { return _description; }
            set
            {
                if (!Equals(_description, value))
                {
                    Assignment(delegate { _description = value; }, "Description");
                }
            }
        }

        public override void Accept(ICustomizationVisitor<TEntity> visitor)
        {
            visitor.Visit(this);
        }
    }

    public class ChangeLocation<TEntity> : Customization<TEntity>
        where TEntity : IOriginator<TEntity>
    {
        private Location _location;

        public virtual Location Location
        {
            get { return _location; }
            set
            {
                if (!Equals(_location, value))
                {
                    Assignment(delegate { _location = value; }, "Location");
                }
            }
        }

        public override void Accept(ICustomizationVisitor<TEntity> visitor)
        {
            visitor.Visit(this);
        }
    }

    public class ChangePosition<TEntity> : Customization<TEntity>
        where TEntity : IOriginator<TEntity>
    {
        private int _position;

        public virtual int Position
        {
            get { return _position; }
            set
            {
                if (!Equals(_position, value))
                {
                    Assignment(delegate { _position = value; }, "Position");
                }
            }
        }

        public override void Accept(ICustomizationVisitor<TEntity> visitor)
        {
            visitor.Visit(this);
        }
    }

    public class Package<TEntity> : Customization<TEntity>
        where TEntity : IOriginator<TEntity>
    {
        public override void Accept(ICustomizationVisitor<TEntity> visitor)
        {
            visitor.Visit(this);
        }
    }

    public class Associate<TEntity> : Customization<TEntity>
        where TEntity : IOriginator<TEntity>
    {
        private IMemento<TEntity> _relation;

        public virtual IMemento<TEntity> Relation
        {
            get { return _relation; }
            set
            {
                if (!Equals(_relation, value))
                {
                    Assignment(delegate { _relation = value; }, "Relation");
                }
            }
        }

        public override void Accept(ICustomizationVisitor<TEntity> visitor)
        {
            visitor.Visit(this);
        }
    }

    public class Hide<TEntity> : Customization<TEntity>
        where TEntity : IOriginator<TEntity>
    {
        public override void Accept(ICustomizationVisitor<TEntity> visitor)
        {
            visitor.Visit(this);
        }
    }

    public class Merge<TEntity> : Customization<TEntity>
        where TEntity : IOriginator<TEntity>
    {
        private IMemento<TEntity> _parent;

        public virtual IMemento<TEntity> Parent
        {
            get { return _parent; }
            set
            {
                if (!Equals(_parent, value))
                {
                    Assignment(delegate { _parent = value; }, "Parent");
                }
            }
        }

        public override void Accept(ICustomizationVisitor<TEntity> visitor)
        {
            visitor.Visit(this);
        }
    }
}
