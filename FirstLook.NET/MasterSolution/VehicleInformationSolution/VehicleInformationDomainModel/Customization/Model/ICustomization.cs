using FirstLook.Common.Core.Memento;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Model
{
    public interface ICustomization<TEntity>
        where TEntity : IOriginator<TEntity>
    {
        IMemento<TEntity> Entity { get; set; }

        void Accept(ICustomizationVisitor<TEntity> visitor);
    }
}