namespace FirstLook.VehicleInformation.DomainModel.Customization.Model
{
    public enum Location : byte
    {
        NotDefined = 0,
        Vehicle    = 1,
        Interior   = 2,
        Exterior   = 3
    }
}
