using System.Collections.Generic;
using FirstLook.Common.Core.Memento;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Model
{
    static class CustomizationHelper<TEntity>
        where TEntity : IOriginator<TEntity>
    {
        public static void Apply(IList<TEntity> values, IList<ICustomization<TEntity>> customizations, ICustomizationVisitor<TEntity> visitor)
        {
            foreach (TEntity value in values)
            {
                visitor.Entity = value;

                foreach (ICustomization<TEntity> customization in customizations)
                {
                    customization.Accept(visitor);
                }
            }
        }
    }
}