using FirstLook.Common.Core.Memento;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Model
{
    public interface ICustomizationVisitor<TEntity>
        where TEntity : IOriginator<TEntity>
    {
        TEntity Entity { set; }

        void Visit(ChangeName<TEntity> customization);

        void Visit(ChangeDescription<TEntity> customization);

        void Visit(ChangeLocation<TEntity> customization);

        void Visit(ChangePosition<TEntity> customization);

        void Visit(Associate<TEntity> customization);

        void Visit(Hide<TEntity> customization);

        void Visit(Merge<TEntity> customization);

        void Visit(Package<TEntity> customization);
    }
}
