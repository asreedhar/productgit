using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Common.Core.Memento;

namespace FirstLook.VehicleInformation.DomainModel.Customization.Model
{
    public interface ICustomizationRepository
    {
        IList<TCustomization> Fetch<TCustomization, TEntity>()
            where TEntity : IOriginator<TEntity>
            where TCustomization : ICustomization<TEntity>;

        IList<TCustomization> Fetch<TCustomization, TEntity>(Style style)
            where TEntity : IOriginator<TEntity>
            where TCustomization : ICustomization<TEntity>;

        IList<TCustomization> Fetch<TCustomization, TEntity>(IBroker broker)
            where TEntity : IOriginator<TEntity>
            where TCustomization : ICustomization<TEntity>;

        IList<TCustomization> Fetch<TCustomization, TEntity>(IBroker broker, Style style)
            where TEntity : IOriginator<TEntity>
            where TCustomization : ICustomization<TEntity>;

        IList<TCustomization> Save<TCustomization, TEntity>(IPrincipal principal, IList<TCustomization> customizations)
            where TEntity : IOriginator<TEntity>
            where TCustomization : ICustomization<TEntity>;
    }
}
