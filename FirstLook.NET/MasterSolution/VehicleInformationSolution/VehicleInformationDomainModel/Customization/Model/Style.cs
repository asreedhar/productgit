namespace FirstLook.VehicleInformation.DomainModel.Customization.Model
{
    public class Style
    {
        private readonly int _id;

        public Style(int id)
        {
            _id = id;
        }

        public int Id
        {
            get { return _id; }
        }
    }
}
