namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData
{
    public class ConsumerInformation
    {
        private ConsumerInformationHeader header;
        private int id;
        private string label;
        private string value;

        public ConsumerInformation(ConsumerInformationHeader header, int id, string label, string value)
        {
            this.header = header;
            this.id = id;
            this.label = label;
            this.value = value;
        }

        public ConsumerInformationHeader Header
        {
            get { return header; }
        }

        public int Id
        {
            get { return id; }
        }

        public string Label
        {
            get { return label; }
        }

        public string Value
        {
            get { return value; }
        }
    }
}