namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData
{
    public interface IConditionVisitor
    {
        void VisitAndCondition(AndCondition condition);

        void VisitNotCondition(NotCondition condition);

        void VisitOption(OptionCondition condition);

        void VisitOrCondition(OrCondition condition);
    }
}