namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration.Serialization {
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Tools.Console", "1.0.0.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://xml.firstlook.biz/VSD/Build")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="http://xml.firstlook.biz/VSD/Build", IsNullable=false)]
    public partial class BuildSpecification {
        
        private Style styleField;
        
        private Build buildField;
        
        private string vinField;
        
        private decimal versionField;
        
        private bool versionFieldSpecified;
        
        /// <remarks/>
        public Style Style {
            get {
                return this.styleField;
            }
            set {
                this.styleField = value;
            }
        }
        
        /// <remarks/>
        public Build Build {
            get {
                return this.buildField;
            }
            set {
                this.buildField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Vin {
            get {
                return this.vinField;
            }
            set {
                this.vinField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal Version {
            get {
                return this.versionField;
            }
            set {
                this.versionField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool VersionSpecified {
            get {
                return this.versionFieldSpecified;
            }
            set {
                this.versionFieldSpecified = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Tools.Console", "1.0.0.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://xml.firstlook.biz/VSD/Build")]
    public partial class Style {
        
        private int idField;
        
        private int versionField;
        
        /// <remarks/>
        public int Id {
            get {
                return this.idField;
            }
            set {
                this.idField = value;
            }
        }
        
        /// <remarks/>
        public int Version {
            get {
                return this.versionField;
            }
            set {
                this.versionField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Tools.Console", "1.0.0.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://xml.firstlook.biz/VSD/Build")]
    public partial class BuildStandard {
        
        private int standardSequenceField;
        
        private BuildStandardState standardStateField;
        
        /// <remarks/>
        public int StandardSequence {
            get {
                return this.standardSequenceField;
            }
            set {
                this.standardSequenceField = value;
            }
        }
        
        /// <remarks/>
        public BuildStandardState StandardState {
            get {
                return this.standardStateField;
            }
            set {
                this.standardStateField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Tools.Console", "1.0.0.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://xml.firstlook.biz/VSD/Build")]
    public enum BuildStandardState {
        
        /// <remarks/>
        Add,
        
        /// <remarks/>
        Remove,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Tools.Console", "1.0.0.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://xml.firstlook.biz/VSD/Build")]
    public partial class BuildCategory {
        
        private int categoryIdField;
        
        private BuildCategoryState categoryStateField;
        
        /// <remarks/>
        public int CategoryId {
            get {
                return this.categoryIdField;
            }
            set {
                this.categoryIdField = value;
            }
        }
        
        /// <remarks/>
        public BuildCategoryState CategoryState {
            get {
                return this.categoryStateField;
            }
            set {
                this.categoryStateField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Tools.Console", "1.0.0.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://xml.firstlook.biz/VSD/Build")]
    public enum BuildCategoryState {
        
        /// <remarks/>
        Add,
        
        /// <remarks/>
        Package,
        
        /// <remarks/>
        Remove,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Tools.Console", "1.0.0.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://xml.firstlook.biz/VSD/Build")]
    public partial class BuildOption {
        
        private string optionCodeField;
        
        private BuildOptionStateCollection optionStateField;
        
        /// <remarks/>
        public string OptionCode {
            get {
                return this.optionCodeField;
            }
            set {
                this.optionCodeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public BuildOptionStateCollection OptionStates {
            get {
                return this.optionStateField;
            }
            set {
                this.optionStateField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Tools.Console", "1.0.0.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://xml.firstlook.biz/VSD/Build")]
    public enum BuildOptionState {
        
        /// <remarks/>
        Off,
        
        /// <remarks/>
        On,
        
        /// <remarks/>
        Require,
        
        /// <remarks/>
        Include,
        
        /// <remarks/>
        OrRequire,
        
        /// <remarks/>
        OrInclude,
        
        /// <remarks/>
        Exclude,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Tools.Console", "1.0.0.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://xml.firstlook.biz/VSD/Build")]
    public partial class Build {
        
        private BuildLogicOperator lastOperatorField;
        
        private BuildState stateField;
        
        private BuildOptionCollection optionField;
        
        private BuildCategoryCollection categoryField;
        
        private BuildStandardCollection standardField;
        
        private BuildOptionCollection selectionsField;
        
        /// <remarks/>
        public BuildLogicOperator LastOperator {
            get {
                return this.lastOperatorField;
            }
            set {
                this.lastOperatorField = value;
            }
        }
        
        /// <remarks/>
        public BuildState State {
            get {
                return this.stateField;
            }
            set {
                this.stateField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Option")]
        public BuildOptionCollection Options {
            get {
                return this.optionField;
            }
            set {
                this.optionField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Category")]
        public BuildCategoryCollection Categories {
            get {
                return this.categoryField;
            }
            set {
                this.categoryField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Standard")]
        public BuildStandardCollection Standards {
            get {
                return this.standardField;
            }
            set {
                this.standardField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Selections")]
        public BuildOptionCollection Selections {
            get {
                return this.selectionsField;
            }
            set {
                this.selectionsField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Tools.Console", "1.0.0.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://xml.firstlook.biz/VSD/Build")]
    public enum BuildLogicOperator {
        
        /// <remarks/>
        R,
        
        /// <remarks/>
        M,
        
        /// <remarks/>
        O,
        
        /// <remarks/>
        I,
        
        /// <remarks/>
        N,
        
        /// <remarks/>
        X,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Tools.Console", "1.0.0.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://xml.firstlook.biz/VSD/Build")]
    public enum BuildState {
        
        /// <remarks/>
        Normal,
        
        /// <remarks/>
        Query,
    }
    
    public class BuildOptionStateCollection : System.Collections.CollectionBase {
        
        public BuildOptionState this[int idx] {
            get {
                return ((BuildOptionState)(base.InnerList[idx]));
            }
            set {
                base.InnerList[idx] = value;
            }
        }
        
        public int Add(BuildOptionState value) {
            return base.InnerList.Add(value);
        }
        
        public void Remove(BuildOptionState value) {
            base.InnerList.Remove(value);
        }
        
        public bool Contains(BuildOptionState value) {
            return base.InnerList.Contains(value);
        }
    }
    
    public class BuildOptionCollection : System.Collections.CollectionBase {
        
        public BuildOption this[int idx] {
            get {
                return ((BuildOption)(base.InnerList[idx]));
            }
            set {
                base.InnerList[idx] = value;
            }
        }
        
        public int Add(BuildOption value) {
            return base.InnerList.Add(value);
        }
        
        public void Remove(BuildOption value) {
            base.InnerList.Remove(value);
        }
        
        public bool Contains(BuildOption value) {
            return base.InnerList.Contains(value);
        }
    }
    
    public class BuildCategoryCollection : System.Collections.CollectionBase {
        
        public BuildCategory this[int idx] {
            get {
                return ((BuildCategory)(base.InnerList[idx]));
            }
            set {
                base.InnerList[idx] = value;
            }
        }
        
        public int Add(BuildCategory value) {
            return base.InnerList.Add(value);
        }
        
        public void Remove(BuildCategory value) {
            base.InnerList.Remove(value);
        }
        
        public bool Contains(BuildCategory value) {
            return base.InnerList.Contains(value);
        }
    }
    
    public class BuildStandardCollection : System.Collections.CollectionBase {
        
        public BuildStandard this[int idx] {
            get {
                return ((BuildStandard)(base.InnerList[idx]));
            }
            set {
                base.InnerList[idx] = value;
            }
        }
        
        public int Add(BuildStandard value) {
            return base.InnerList.Add(value);
        }
        
        public void Remove(BuildStandard value) {
            base.InnerList.Remove(value);
        }
        
        public bool Contains(BuildStandard value) {
            return base.InnerList.Contains(value);
        }
    }
}
