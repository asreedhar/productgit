using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration
{
    public class ConfigurationEngineState : IEnumerable<OptionState>
    {
        private readonly LogicOperator _logicOperator;
        private readonly ConfigurationEngineResult _engineResult;
        private readonly OptionState[] _optionStates;
        private readonly OptionEncodings _optionSelections;

        public ConfigurationEngineState(LogicOperator logicOperator, ConfigurationEngineResult configurationEngineResult, OptionState[] optionStates, OptionEncodings optionSelections)
        {
            _logicOperator = logicOperator;

            _engineResult = configurationEngineResult;

            _optionStates = optionStates;

            _optionSelections = optionSelections;
        }

        public LogicOperator LogicOperator
        {
            get { return _logicOperator; }
        }

        public ConfigurationEngineResult EngineResult
        {
            get { return _engineResult; }
        }

        public OptionEncodings OptionSelections
        {
            get { return _optionSelections; }
        }

        public int Count
        {
            get
            {
                return _optionStates.Length;
            }
        }

        public OptionState this[int index]
        {
            get
            {
                return _optionStates[index];
            }
        }

        public void CopyTo(OptionState[] destination)
        {
            Array.Copy(_optionStates, destination, _optionStates.Length);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            if (EngineResult == ConfigurationEngineResult.Normal)
            {
                sb.AppendFormat("N");
            }
            else
            {
                sb.AppendFormat("Q");
            }

            sb.Append('|');

            switch (LogicOperator)
            {
                case LogicOperator.NotDefined:
                    sb.Append('-');
                    break;
                case LogicOperator.Require:
                    sb.Append('R');
                    break;
                case LogicOperator.MutualRequire:
                    sb.Append('M');
                    break;
                case LogicOperator.Include:
                    sb.Append('I');
                    break;
                case LogicOperator.OrRequire:
                    sb.Append('O');
                    break;
                case LogicOperator.OrInclude:
                    sb.Append('N');
                    break;
                case LogicOperator.Exclude:
                    sb.Append('X');
                    break;
            }

            sb.Append('|');

            sb.Append(OptionStateHelper.ToString(_optionStates));

            sb.Append('|');

            foreach (OptionEncoding optionSelection in _optionSelections)
            {
                sb.Append(optionSelection.Index).Append(",");
            }

            if (sb[sb.Length-1] == ',')
            {
                sb.Length -= 1;
            }

            return sb.ToString();
        }

        public static bool TryParse(string value, OptionEncodings encodings, out ConfigurationEngineState configurationState)
        {
            if (string.IsNullOrEmpty(value))
            {
                configurationState = null;

                return false;
            }

            // fields

            ConfigurationEngineResult configurationEngineResult = ConfigurationEngineResult.Normal;

            LogicOperator logicOperator = LogicOperator.NotDefined;

            OptionState[] optionStates = new OptionState[0];

            OptionEncodings optionSelections = OptionEncodings.Empty;

            // text is "state|operator|states|selections"

            bool error = false;

            int start = 0, index = 0;

            int[] minimumLength = new int[] {1, 1, 1, 0};

            for (int i = 0; i < 4; i++)
            {
                string text;

                if (i < 3)
                {
                    index = value.IndexOf('|', start);

                    if (index == -1)
                    {
                        error = true;

                        break;
                    }

                    int length = index - start;

                    text = value.Substring(start, length);
                }
                else
                {
                    text = value.Substring(start);
                }

                if (text.Length < minimumLength[i])
                {
                    error = true;

                    break;
                }

                switch(i)
                {
                    case 0:
                        configurationEngineResult = ParseEngineResult(text);
                        break;
                    case 1:
                        logicOperator = ParseLogicOperator(text);
                        break;
                    case 2:
                        optionStates = OptionStateHelper.FromString(text);
                        break;
                    case 3:
                        optionSelections = ParseOptionSelections(text, encodings);
                        break;
                }

                start = index + 1;
            }

            if (error)
            {
                configurationState = null;

                return false;
            }

            configurationState = new ConfigurationEngineState(
                logicOperator,
                configurationEngineResult,
                optionStates,
                optionSelections);

            return true;
        }

        static ConfigurationEngineResult ParseEngineResult(string text)
        {
            return text[0] == 'N'
                       ? ConfigurationEngineResult.Normal
                       : ConfigurationEngineResult.Query;
        }

        static LogicOperator ParseLogicOperator(string text)
        {
            LogicOperator logicOperator = LogicOperator.NotDefined;

            switch (text[0])
            {
                case '-':
                    logicOperator = LogicOperator.NotDefined;
                    break;
                case 'R':
                    logicOperator = LogicOperator.Require;
                    break;
                case 'M':
                    logicOperator = LogicOperator.MutualRequire;
                    break;
                case 'I':
                    logicOperator = LogicOperator.Include;
                    break;
                case 'O':
                    logicOperator = LogicOperator.OrRequire;
                    break;
                case 'N':
                    logicOperator = LogicOperator.OrInclude;
                    break;
                case 'X':
                    logicOperator = LogicOperator.Exclude;
                    break;
            }

            return logicOperator;
        }

        static OptionEncodings ParseOptionSelections(string text, OptionEncodings encodings)
        {
            List<OptionEncoding> selections = new List<OptionEncoding>();

            string[] values = text.Split(new char[] { ',' });

            foreach (string value in values)
            {
                if (!string.IsNullOrEmpty(value))
                {
                    int index = int.Parse(value);

                    selections.Add(encodings[index]);
                }
            }

            return new OptionEncodings(selections);
        }

        public ConfigurationEngineState Transition(LogicOperator newLogicOperator, ConfigurationEngineResult newConfigurationEngineResult, OptionState[] newOptionStates)
        {
            return new ConfigurationEngineState(
                newLogicOperator,
                newConfigurationEngineResult,
                newOptionStates,
                OptionSelections);
        }

        public ConfigurationEngineState Selection(OptionEncoding option, OptionState[] newOptionStates)
        {
            newOptionStates[option.Index] |= OptionState.On;

            return new ConfigurationEngineState(
                _logicOperator,
                _engineResult,
                newOptionStates,
                _optionSelections.Select(option));
        }

        public ConfigurationEngineState Deselection(OptionEncoding option)
        {
            return new ConfigurationEngineState(
                _logicOperator,
                _engineResult,
                _optionStates,
                _optionSelections.Remove(option));
        }

        #region IEnumerable<OptionState> Members

        IEnumerator<OptionState> IEnumerable<OptionState>.GetEnumerator()
        {
            return new List<OptionState>(_optionStates).GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _optionStates.GetEnumerator();
        }

        #endregion

        
    }
}
