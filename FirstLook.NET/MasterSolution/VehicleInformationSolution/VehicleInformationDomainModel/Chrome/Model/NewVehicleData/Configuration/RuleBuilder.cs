using System;
using System.Collections.Generic;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration
{
    public class RuleBuilder : IConditionVisitor
    {
        private readonly List<OptionEncoding> _optionEncodings = new List<OptionEncoding>();

        private readonly Dictionary<string, bool> _exists = new Dictionary<string, bool>();

        private IRule _root;

        public IRule Root
        {
            get { return _root; }
        }

        public OptionEncodings OptionEncodings
        {
            get
            {
                return new OptionEncodings(_optionEncodings);
            }
        }

        public void Register(OptionEncodings options)
        {
            _optionEncodings.Clear();

            _optionEncodings.AddRange(options);
        }

        public void Register(IEnumerable<Option> options)
        {
            foreach (Option option in options)
            {
                int requirement = (option.OptionKind == null) ? 0 : option.OptionKind.Id;

                _optionEncodings.Add(new OptionEncoding(_optionEncodings.Count, option.Code, option.GroupName, requirement, option.Availability));

                _exists.Add(option.Code, true);
            }
        }

        public virtual IRule NewRule(Operator op, IRule parent)
        {
            IRule rule;

            switch (op)
            {
                case Operator.And:
                    rule = new AndRule();
                    break;
                case Operator.Not:
                    rule = new NotRule();
                    break;
                case Operator.Or:
                    rule = new OrRule();
                    break;
                default:
                    throw new ArgumentOutOfRangeException("op");
            }

            if (_root == null)
            {
                _root = rule;
            }

            if (parent != null)
            {
                parent.Add(rule);
            }

            return rule;
        }

        public virtual void NewVariable(string code, IRule parent)
        {
            OptionEncoding optionEncoding = null;

            foreach (OptionEncoding value in _optionEncodings)
            {
                if (Equals(value.OptionCode, code))
                {
                    optionEncoding = value;

                    break;
                }
            }

            if (optionEncoding == null)
            {
                optionEncoding = new OptionEncoding(_optionEncodings.Count, code, string.Empty, 0, Availability.Any);

                _optionEncodings.Add(optionEncoding);
            }

            IRule rule = new OptionRule(optionEncoding.Index);

            parent.Add(rule);
        }

        public static IRule ToRule(OptionEncodings encodings, ICondition condition)
        {
            RuleBuilder r = new RuleBuilder();

            r.Register(encodings);

            condition.Accept(r);

            return r.Root;
        }

        #region IConditionVisitor Members

        private IRule _parent;

        protected void VisitAndCondition(AndCondition and)
        {
            IRule rule = _parent;

            _parent = NewRule(Operator.And, _parent);

            foreach (ICondition condition in and.Conditions)
            {
                condition.Accept(this);
            }

            _parent = rule;
        }

        protected void VisitNotCondition(NotCondition not)
        {
            IRule rule = _parent;

            _parent = NewRule(Operator.Not, _parent);

            not.Accept(this);

            _parent = rule;
        }

        protected void VisitOption(OptionCondition option)
        {
            if (_exists.ContainsKey(option.Code))
            {
                if (_parent == null)
                {
                    _parent = NewRule(Operator.Or, null);
                }

                NewVariable(option.Code, _parent);
            }
        }

        protected void VisitOrCondition(OrCondition or)
        {
            IRule rule = _parent;

            _parent = NewRule(Operator.Or, _parent);

            foreach (ICondition condition in or.Conditions)
            {
                condition.Accept(this);
            }

            _parent = rule;
        }

        void IConditionVisitor.VisitAndCondition(AndCondition condition)
        {
            VisitAndCondition(condition);
        }

        void IConditionVisitor.VisitNotCondition(NotCondition condition)
        {
            VisitNotCondition(condition);
        }

        void IConditionVisitor.VisitOption(OptionCondition condition)
        {
            VisitOption(condition);
        }

        void IConditionVisitor.VisitOrCondition(OrCondition condition)
        {
            VisitOrCondition(condition);
        }

        #endregion
    }
}