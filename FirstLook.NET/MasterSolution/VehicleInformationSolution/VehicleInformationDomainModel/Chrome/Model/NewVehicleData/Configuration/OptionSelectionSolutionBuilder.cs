using System;
using System.Collections.Generic;
using FirstLook.Common.Core.Collections;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration
{
    public class OptionSelectionSolutionBuilder
    {
        private readonly Solver _solver = new Solver();

        public IRandomAccess<IRandomAccess<OptionSelection>> Process(IEnumerable<Option> options, ICondition condition)
        {
            _solver.Register(options);

            ICondition collapse = condition.Collapse();

            collapse.Accept(_solver);

            return _solver.Solve();
        }

        class Solver : RuleBuilder
        {
            private readonly List<OptionEncoding> _variables = new List<OptionEncoding>();

            public override void NewVariable(string code, IRule parent)
            {
                base.NewVariable(code, parent);

                if (!_variables.Exists(delegate(OptionEncoding value) { return Equals(code, value.OptionCode); }))
                {
                    _variables.Add(OptionEncodings[code]);
                }
            }

            public IRandomAccess<IRandomAccess<OptionSelection>> Solve()
            {
                RandomAccess<IRandomAccess<OptionSelection>> solutions = new RandomAccess<IRandomAccess<OptionSelection>>();

                if (OptionEncodings.Count == 0 || _variables.Count == 0)
                {
                    return solutions; // HACK: cough, splutter (bad data from Chrome)
                }

                int l = _variables.Count;

                OptionState[] z = new OptionState[OptionEncodings.Count];

                OptionState[] a = new OptionState[l];

                for (int i = 0; i <= l; i++)
                {
                    for (int j = 0; j < l; j++)
                    {
                        a[j] = (j < l - i) ? OptionState.Off : OptionState.On;
                    }

                    // for all zero's and all one's there are no permutations so the
                    // while loop will finish before the chance to evaluate the array

                    if (i == 0 || i == l)
                    {
                        CopyTo(a, z);

                        Evaluate(a, z, solutions);
                    }

                    while (true)
                    {
                        // max(j) where a[j] < a[j+1]

                        int j = l - 2;

                        while (j >= 0)
                        {
                            if (a[j] < a[j + 1])
                            {
                                break;
                            }

                            j--;
                        }

                        if (j < 0)
                        {
                            break; // no more permutations
                        }

                        // max(k) where a[j] < a[k]

                        int k = l - 1;

                        while (k > j)
                        {
                            if (a[j] < a[k])
                            {
                                break;
                            }

                            k--;
                        }

                        // swap a[j] and a[k]

                        OptionState t = a[j];

                        a[j] = a[k];

                        a[k] = t;

                        // reverse array between a[j+1] and a[l-1]

                        Array.Reverse(a, j + 1, l - (j + 1));

                        // try it out!

                        CopyTo(a, z);

                        Evaluate(a, z, solutions);
                    }
                }

                return solutions;
            }

            private void CopyTo(OptionState[] src, OptionState[] dst)
            {
                // reset dst

                for (int i = 0, l = dst.Length; i < l; i++)
                {
                    dst[i] = OptionState.Off;
                }

                // copy src to dst

                for (int i = 0, l = src.Length; i < l; i++)
                {
                    OptionEncoding variable = _variables[i];

                    dst[variable.Index] = src[i];
                }
            }

            private void Evaluate(OptionState[] a, OptionState[] z, IRandomAccess<IRandomAccess<OptionSelection>> solutions)
            {
                if (Root.Evaluate(z))
                {
                    RandomAccess<OptionSelection> solution = new RandomAccess<OptionSelection>();

                    for (int i = 0, l = a.Length; i < l; i++)
                    {
                        OptionSelection selection = new OptionSelection(
                            _variables[i].OptionCode,
                            a[i] == OptionState.On);

                        solution.Add(selection);
                    }

                    solutions.Add(solution);
                }
            }
        }
    }
}