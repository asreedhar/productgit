namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration
{
    public interface IRule
    {
        void Add(IRule rule);

        bool Evaluate(OptionState[] values);
    }
}