namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration
{
    public enum Operator
    {
        And,
        Not,
        Or
    }
}