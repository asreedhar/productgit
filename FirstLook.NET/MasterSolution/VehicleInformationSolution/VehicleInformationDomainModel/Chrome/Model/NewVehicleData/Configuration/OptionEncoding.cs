using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration
{
    public class OptionEncoding : Encoding<OptionEncoding>
    {
        private readonly Availability _availability;
        private readonly string _optionCode;
        private readonly string _optionGroup;
        private readonly int _optionRequirement;

        public OptionEncoding(int index, string optionCode, string optionGroup, int optionRequirement, Availability availability) : base(index)
        {
            _optionCode = optionCode;
            _optionGroup = optionGroup;
            _optionRequirement = optionRequirement;
            _availability = availability;
        }

        public string OptionCode
        {
            get { return _optionCode; }
        }

        public string OptionGroup
        {
            get { return _optionGroup; }
        }

        public int OptionRequirement
        {
            get { return _optionRequirement; }
        }

        public Availability Availability
        {
            get { return _availability; }
        }
    }
}