namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration
{
    public abstract class Encoding<T> where T : Encoding<T>
    {
        private readonly int _index;

        protected Encoding(int index)
        {
            _index = index;
        }

        public int Index
        {
            get { return _index; }
        }
    }
}
