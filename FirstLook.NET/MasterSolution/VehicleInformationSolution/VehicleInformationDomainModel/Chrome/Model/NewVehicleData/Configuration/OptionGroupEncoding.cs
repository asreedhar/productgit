using System.Collections.Generic;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration
{
    public class OptionGroupEncoding
    {
        private readonly Dictionary<string, int> _optionGroupMap = new Dictionary<string, int>();
        private readonly Dictionary<int, int> _optionRequirementMap = new Dictionary<int, int>();

        private readonly int[][] _optionGroups;
        private readonly int[][] _optionRequirements;

        private readonly OptionEncodings _options;

        public OptionGroupEncoding(OptionEncodings options)
        {
            _options = options;

            Dictionary<string,List<OptionEncoding>> groups = new Dictionary<string, List<OptionEncoding>>();

            foreach (OptionEncoding option in options)
            {
                if (string.IsNullOrEmpty(option.OptionGroup))
                {
                    continue;
                }

                if (groups.ContainsKey(option.OptionGroup))
                {
                    groups[option.OptionGroup].Add(option);
                }
                else
                {
                    List<OptionEncoding> group = new List<OptionEncoding>();

                    group.Add(option);

                    groups.Add(option.OptionGroup, group);
                }
            }

            _optionGroups = new int[groups.Count][];

            int i = 0;

            foreach (KeyValuePair<string, List<OptionEncoding>> group in groups)
            {
                _optionGroups[i] = new int[group.Value.Count];

                _optionGroupMap.Add(group.Key, i);

                int j = 0;

                foreach (OptionEncoding encoding in group.Value)
                {
                    _optionGroups[i][j++] = encoding.Index;
                }

                i++;
            }

            Dictionary<int, List<OptionEncoding>> requirements = new Dictionary<int, List<OptionEncoding>>();

            foreach (OptionEncoding option in options)
            {
                if (option.OptionRequirement == 0)
                {
                    continue;
                }

                if (requirements.ContainsKey(option.OptionRequirement))
                {
                    requirements[option.OptionRequirement].Add(option);
                }
                else
                {
                    List<OptionEncoding> requirement = new List<OptionEncoding>();

                    requirement.Add(option);

                    requirements.Add(option.OptionRequirement, requirement);
                }
            }

            _optionRequirements = new int[requirements.Count][];

            i = 0;

            foreach (KeyValuePair<int, List<OptionEncoding>> requirement in requirements)
            {
                _optionRequirements[i] = new int[requirement.Value.Count];

                _optionRequirementMap.Add(requirement.Key, i);

                int j = 0;

                foreach (OptionEncoding encoding in requirement.Value)
                {
                    _optionRequirements[i][j++] = encoding.Index;
                }

                i++;
            }
        }

        public bool IsValid(OptionState[] optionStates)
        {
            foreach (int[] optionGroup in _optionGroups)
            {
                int i = 0;

                foreach (int optionGroupItem in optionGroup)
                {
                    OptionState optionState = optionStates[optionGroupItem];

                    if (IsSet(optionState, OptionState.Exclude))
                    {
                        continue;
                    }

                    if (IsSet(optionState, OptionState.On, OptionState.Include, OptionState.Require))
                    {
                        if (++i > 1)
                        {
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        public int ConflictingSelection(ConfigurationEngineState engineState, OptionEncoding option)
        {
            if (string.IsNullOrEmpty(option.OptionGroup))
            {
                return -1;
            }

            int[] optionGroup = _optionGroups[_optionGroupMap[option.OptionGroup]];

            foreach (int member in optionGroup)
            {
                if (member == option.Index)
                {
                    continue;
                }

                OptionState optionState = engineState[member];

                if (IsSet(optionState, OptionState.Exclude))
                {
                    continue;
                }

                if (IsSet(optionState, OptionState.On, OptionState.Include, OptionState.Require))
                {
                    return member;
                }
            }

            return -1;
        }

        public IEnumerable<OptionEncoding> DefaultSelections()
        {
            List<OptionEncoding> selections = new List<OptionEncoding>();
 
            foreach (int[] optionRequirementCandidates in _optionRequirements)
            {
                if (optionRequirementCandidates.Length == 1)
                {
                    int candidate = optionRequirementCandidates[0];

                    int i = FindGroup(candidate);

                    if (i != -1)
                    {
                        int[] optionGroup = _optionGroups[i];

                        if (optionGroup.Length == 1)
                        {
                            selections.Add(_options[candidate]);
                        }
                    }
                    else
                    {
                        selections.Add(_options[candidate]);
                    }
                }
            }

            return selections;
        }

        static bool IsSet(OptionState value, params OptionState[] flags)
        {
            foreach (OptionState flag in flags)
            {
                if ((value & flag) == flag)
                {
                    return true;
                }
            }

            return false;
        }

        int FindGroup(int option)
        {
            OptionEncoding encoding = _options[option];

            if (string.IsNullOrEmpty(encoding.OptionGroup))
            {
                return -1;
            }

            return _optionGroupMap[encoding.OptionGroup];
        }
    }
}
