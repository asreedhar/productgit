using System.Collections.Generic;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration
{
    public class StandardEncodings : Encodings<StandardEncoding,Standard>
    {
        public static readonly StandardEncodings Empty = new StandardEncodings(new StandardEncoding[0]);

        public StandardEncodings(IEnumerable<Standard> standards)
            : base(standards)
        {
        }

        public StandardEncodings(ICollection<StandardEncoding> values)
            : base(values)
        {
        }

        protected override StandardEncoding Map(int index, Standard value)
        {
            return new StandardEncoding(index, value.Sequence);
        }

        public StandardEncoding Find(int sequence)
        {
            foreach (StandardEncoding value in this)
            {
                if (Equals(value.Sequence, sequence))
                {
                    return value;
                }
            }

            return null;
        }

        public override StandardEncoding this[Standard standard]
        {
            get
            {
                foreach (StandardEncoding value in this)
                {
                    if (Equals(value.Sequence, standard.Sequence))
                    {
                        return value;
                    }
                }

                return null;
            }
        }
    }
}
