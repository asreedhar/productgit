using System.Collections;
using System.Collections.Generic;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration
{
    public abstract class Encodings<T,V> : IEnumerable<T> where T : Encoding<T>
    {
        private readonly T[] _values;

        protected Encodings()
        {
        }
        
        protected Encodings(IEnumerable<V> values)
        {
            List<T> list = new List<T>();

            int i = 0;

            foreach (V value in values)
            {
                list.Add(Map(i++, value));
            }

            _values = list.ToArray();
        }

        protected Encodings(ICollection<T> values)
        {
            _values = new T[values.Count];
                
            values.CopyTo(_values, 0);
        }

        protected abstract T Map(int index, V value);

        protected T[] Values
        {
            get { return _values; }
        }

        public bool Contains(T encoding)
        {
            foreach (T value in _values)
            {
                if (Equals(value.Index, encoding.Index))
                {
                    return true;
                }
            }

            return false;
        }

        public T this[int index]
        {
            get
            {
                return _values[index];
            }
        }

        public abstract T this[V value] { get; }

        public int Count
        {
            get
            {
                return _values.Length;
            }
        }

        #region IEnumerable<OptionEncoding> Members

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return new List<T>(_values).GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _values.GetEnumerator();
        }

        #endregion
    }
}
