namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration
{
    public class CategoryEncoding : Encoding<CategoryEncoding>
    {
        private readonly int _id;

        public CategoryEncoding(int index, int id)
            : base(index)
        {
            _id = id;
        }

        public int Id
        {
            get { return _id; }
        }
    }
}
