using System;
using System.Collections.Generic;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using Bit = FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration.OptionStateHelper;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration
{
    public class ConfigurationEngine
    {
        private readonly OrderRuleEncodings _orderRules;

        private readonly OptionEncodings _optionEncodings;

        public ConfigurationEngine(OptionEncodings options, IEnumerable<OrderRule> orderRules)
        {
            _optionEncodings = options;

            _orderRules = new OrderRuleEncodings(orderRules, options);
        }

        public ConfigurationEngine(IEnumerable<Option> options, IEnumerable<OrderRule> orderRules)
        {
            _optionEncodings = new OptionEncodings(options);

            _orderRules = new OrderRuleEncodings(orderRules, _optionEncodings);
        }

        public ConfigurationEngineState Start()
        {
            OptionState[] startState = new OptionState[_optionEncodings.Count];

            return new ConfigurationEngineState(
                LogicOperator.NotDefined,
                ConfigurationEngineResult.Normal,
                startState,
                OptionEncodings.Empty);
        }

        public ConfigurationEngineState Transition(ConfigurationEngineState engineState, OptionEncoding option, OptionState optionState)
        {
            OptionState[] optionStates = new OptionState[engineState.Count];

            engineState.CopyTo(optionStates);

            ConfigurationEngineState newEngineState = engineState;

            if (newEngineState.EngineResult == ConfigurationEngineResult.Query)
            {
                switch (newEngineState.LogicOperator)
                {
                    case LogicOperator.OrInclude:
                        newEngineState = OrInclude(option, optionStates, newEngineState);
                        break;
                    case LogicOperator.OrRequire:
                        newEngineState = OrRequire(option, optionStates, newEngineState);
                        break;
                }
            }
            else
            {
                if (optionState == OptionState.Off)
                {
                    newEngineState = Deselect(newEngineState, option, optionStates, OptionState.On);
                }
                else
                {
                    // TODO: Validation of selection

                    newEngineState = newEngineState.Selection(option, optionStates);

                    newEngineState = Select(optionStates, newEngineState, option);
                }
            }

            return newEngineState;
        }

        protected ConfigurationEngineState Select(OptionState[] optionStates, ConfigurationEngineState engineState, OptionEncoding targetOptionCode)
        {
            ConfigurationEngineState newEngineState = engineState;

            foreach (OrderRuleEncoding orderRule in _orderRules)
            {
                if (!orderRule.Condition.Evaluate(optionStates))
                {
                    continue;
                }
                
                if (orderRule.Option.Index == targetOptionCode.Index)
                {
                    newEngineState = Evaluate(orderRule, optionStates, engineState);
                }
                
                if (newEngineState.EngineResult == ConfigurationEngineResult.Query)
                {
                    break;
                }
            }

            return newEngineState;
        }

        private ConfigurationEngineState Evaluate(OrderRuleEncoding orderRule, OptionState[] optionStates, ConfigurationEngineState engineState)
        {
            ConfigurationEngineState newEngineState = engineState;

            switch (orderRule.LogicOperator)
            {
                case LogicOperator.Require:
                case LogicOperator.MutualRequire:
                    newEngineState = Require(orderRule, optionStates, engineState);
                    break;
                case LogicOperator.OrRequire:
                    newEngineState = OrRequire(orderRule, optionStates, engineState);
                    break;
                case LogicOperator.Include:
                    newEngineState = Include(orderRule, optionStates, engineState);
                    break;
                case LogicOperator.OrInclude:
                    newEngineState = OrInclude(orderRule, optionStates, engineState);
                    break;
                case LogicOperator.Exclude:
                    newEngineState = Exclude(orderRule, optionStates, engineState);
                    break;
            }

            return newEngineState;
        }

        ConfigurationEngineState Require(OrderRuleEncoding rule, OptionState[] optionStates, ConfigurationEngineState engineState)
        {
            ConfigurationEngineState newEngineState = engineState;

            bool[] existing = new bool[rule.Operand.Count];

            int i = 0;

            foreach (OptionEncoding operand in rule.Operand)
            {
                // If an Operand has been excluded by another Option Code, then
                // the Option Code that has excluded it must be deselected, which
                // will turn off all of its other logic rules.

                newEngineState = Deselect(newEngineState, operand, optionStates, OptionState.Exclude);

                // All options listed in the Operand field must also be selected.

                existing[i++] = OptionStateHelper.IsSet(optionStates[operand.Index], OptionState.Require);

                optionStates[operand.Index] |= OptionState.Require;
            }

            newEngineState = newEngineState.Transition(
                LogicOperator.Require,
                ConfigurationEngineResult.Normal,
                optionStates);

            // Any configuration logic associated with the Operands must also be enforced.

            i = 0;

            foreach (OptionEncoding operand in rule.Operand)
            {
                if (!existing[i++])
                {
                    newEngineState = Select(optionStates, newEngineState, operand);

                    if (newEngineState.EngineResult == ConfigurationEngineResult.Query)
                    {
                        break;
                    }
                }
            }

            return newEngineState;
        }

        static ConfigurationEngineState OrRequire(OrderRuleEncoding rule, OptionState[] optionStates, ConfigurationEngineState engineState)
        {
            // One of the options listed in the Operand field must also
            // be selected by the user. The user must be presented with
            // all possible choices, excluding options that have already
            // been excluded by another Option Code.

            int choiceCount = 0, excludedCount = 0, onCount = 0 ;

            foreach (OptionEncoding operand in rule.Operand)
            {
                OptionState state = optionStates[operand.Index];

                if (OptionStateHelper.IsOn(state))
                {
                    onCount++;
                }
                else if (OptionStateHelper.IsExcluded(state))
                {
                    excludedCount++;
                }
                else
                {
                    choiceCount++;
                }
            }

            // If any one of the Operands has already been selected by
            // another Option Code, then the rule is satisfied.

            ConfigurationEngineState newEngineState = engineState;

            if (onCount == 0)
            {
                if (choiceCount > 0)
                {
                    // TODO: IF CHOICECOUNT == 1 THEN SELECT UNLESS NO_HELP

                    foreach (OptionEncoding operand in rule.Operand)
                    {
                        OptionState state = optionStates[operand.Index];

                        if (OptionStateHelper.IsExcluded(state) || OptionStateHelper.IsOn(state))
                        {
                            continue;
                        }

                        optionStates[operand.Index] |= OptionState.OrRequire;
                    }
                }
                else
                {
                    // If all Operands have been excluded by other Option Codes,
                    // then the user should be presented with all Operands; upon
                    // the user�s choice, the Option Code that excluded the chosen
                    // Operand should be deselected and all of its other logic
                    // rules should be turned off.

                    if (excludedCount > 0)
                    {
                        foreach (OptionEncoding operand in rule.Operand)
                        {
                            OptionState state = optionStates[operand.Index];

                            if (OptionStateHelper.IsExcluded(state))
                            {
                                optionStates[operand.Index] |= OptionState.OrRequire;
                            }
                        }
                    }
                    else
                    {
                        throw new NotSupportedException("Incomplete Specification: 0 Choices and 0 Exclusions");
                    }
                }
            }

            newEngineState = newEngineState.Transition(
                        LogicOperator.OrRequire,
                        ConfigurationEngineResult.Query,
                        optionStates);

            return newEngineState;
        }

        ConfigurationEngineState OrRequire(OptionEncoding selection, OptionState[] optionStates, ConfigurationEngineState engineState)
        {
            for (int i = 0, l = optionStates.Length; i < l; i++)
            {
                if (i == selection.Index)
                {
                    optionStates[i] |= OptionState.On;
                }
                else
                {
                    optionStates[i] &= ~OptionState.OrRequire;
                }
            }

            ConfigurationEngineState newEngineState = engineState.Transition(
                        LogicOperator.OrRequire,
                        ConfigurationEngineResult.Normal,
                        optionStates);

            return Select(optionStates, newEngineState, selection);
        }

        static ConfigurationEngineState Include(OrderRuleEncoding rule, OptionState[] optionStates, ConfigurationEngineState engineState)
        {
            // When the option in the Option Code field is selected, all options
            // listed in the Operand field should be included.

            foreach (OptionEncoding operand in rule.Operand)
            {
                OptionState state = optionStates[operand.Index];

                // Included Operands can be excluded by other options 
                // without negating the Include rule on the originating
                // Option Code.

                if (OptionStateHelper.IsExcluded(state))
                {
                    continue;
                }

                // If an Operand has been required by a separate Option Code,
                // the Operand should still be included by the original Option
                // Code�s I rule, and the price of the Operand should not be
                // included in the total price of the vehicle. Option Codes
                // selected by an I rule will also supersede user-selected options,
                // and their prices should not be factored into the total price
                // of the vehicle.

                optionStates[operand.Index] |= OptionState.Include;

                // An Operand included by the Option Code should not have any of
                // its own configuration rules enforced by the I rule.
                // --> No Call To Solve
            }

            ConfigurationEngineState newEngineState = engineState.Transition(
                LogicOperator.Include,
                ConfigurationEngineResult.Normal,
                optionStates);

            return newEngineState;
        }

        ConfigurationEngineState OrInclude(OrderRuleEncoding rule, OptionState[] optionStates, ConfigurationEngineState engineState)
        {
            // The user must be presented with all possible choices, excluding
            // Operands that have already been excluded by another Option
            // Code�s logic rules.

            int choiceCount = 0, onCount = 0;

            foreach (OptionEncoding operand in rule.Operand)
            {
                OptionState state = optionStates[operand.Index];

                if (!OptionStateHelper.IsExcluded(state))
                {
                    if (OptionStateHelper.IsOn(state))
                    {
                        onCount++;
                    }
                    else
                    {
                        choiceCount++;
                    }
                }
            }

            ConfigurationEngineState newEngineState = engineState;

            if (onCount > 0 || choiceCount == 1)
            {
                foreach (OptionEncoding operand in rule.Operand)
                {
                    OptionState state = optionStates[operand.Index];

                    if (OptionStateHelper.IsOn(state) || OptionStateHelper.IsOff(state))
                    {
                        optionStates[operand.Index] |= OptionState.OrInclude;

                        // TODO: SELECT UNLESS NO_HELP

                        newEngineState = OrInclude(operand, optionStates, newEngineState);

                        break;
                    }
                }
            }
            else
            {
                if (choiceCount > 1)
                {
                    foreach (OptionEncoding operand in rule.Operand)
                    {
                        OptionState state = optionStates[operand.Index];

                        if (OptionStateHelper.IsOn(state) || OptionStateHelper.IsExcluded(state))
                        {
                            continue;
                        }

                        optionStates[operand.Index] |= OptionState.OrInclude;
                    }

                    newEngineState = engineState.Transition(
                        LogicOperator.OrInclude,
                        ConfigurationEngineResult.Query,
                        optionStates);
                }
                else
                {
                    throw new NotSupportedException("Incomplete Specification: 0 Choices");
                }
            }
            
            return newEngineState;
        }

        ConfigurationEngineState OrInclude(OptionEncoding selection, OptionState[] optionStates, ConfigurationEngineState engineState)
        {
            // If the Operand selected by an N rule has been selected by
            // another Option Code�s R or O rule, then the N rule will
            // override the original R or O rule.

            for (int i = 0, l = optionStates.Length; i < l; i++)
            {
                if (i == selection.Index)
                {
                    optionStates[i] |= OptionState.On;
                }
                else
                {
                    optionStates[i] &= ~OptionState.OrInclude;
                }
            }

            ConfigurationEngineState newEngineState = engineState.Transition(
                LogicOperator.OrInclude,
                ConfigurationEngineResult.Normal,
                optionStates);

            // An Operand included by the Option Code should have all
            // of its own configuration rules enforced by the N rule.

            return Select(optionStates, newEngineState, selection);
        }

        ConfigurationEngineState Exclude(OrderRuleEncoding rule, OptionState[] optionStates, ConfigurationEngineState engineState)
        {
            // When the option in the Option Code field is selected, all
            // options listed in the Operand field should be excluded or
            // otherwise marked as not selectable.

            ConfigurationEngineState newEngineState = engineState;

            foreach (OptionEncoding operand in rule.Operand)
            {
                // If an Operand has been selected by another Option
                // Code�s R or O rule, then the other Option Code must
                // be deselected (along with all of its logic rules) in
                // order to complete the original Option Code�s X rule.

                newEngineState = Deselect(newEngineState, operand, optionStates, OptionState.Require);

                newEngineState = Deselect(newEngineState, operand, optionStates, OptionState.OrRequire);

                optionStates[operand.Index] |= OptionState.Exclude;
            }

            newEngineState = engineState.Transition(
                LogicOperator.Exclude,
                ConfigurationEngineResult.Normal,
                optionStates);

            return newEngineState;
        }

        ConfigurationEngineState Deselect(ConfigurationEngineState engineState, OptionEncoding option, OptionState[] optionStates, OptionState optionState)
        {
            if (!OptionStateHelper.IsSet(optionStates[option.Index], optionState))
            {
                return engineState;
            }

            ConfigurationEngineState state = Start();

            foreach (OptionEncoding selection in engineState.OptionSelections)
            {
                ConfigurationEngineState rollback = state;

                state = Transition(state, selection, OptionState.On);

                while (state.EngineResult == ConfigurationEngineResult.Query)
                {
                    OptionState flag = state.LogicOperator == LogicOperator.OrInclude
                                           ? OptionState.OrInclude
                                           : OptionState.OrRequire;

                    bool found = false;

                    for (int i = 0, l = state.Count; i < l && !found; i++)
                    {
                        if (OptionStateHelper.IsSet(state[i], flag))
                        {
                            if (OptionStateHelper.IsSet(optionStates[i], OptionState.On))
                            {
                                state = Transition(state, _optionEncodings[i], OptionState.On);

                                found = true;
                            }
                        }
                    }

                    if (!found)
                    {
                        throw new NotSupportedException("Question has no answer");
                    }
                }

                if (OptionStateHelper.IsSet(state[option.Index], optionState))
                {
                    state = rollback.Deselection(selection);
                }
            }

            // update the selections with the corrected state ...

            for (int i = 0, l = state.Count; i < l; i++)
            {
                optionStates[i] = state[i];
            }

            return state;
        }

        public OptionEncodings OptionEncodings
        {
            get { return _optionEncodings; }
        }

        #region Inner Classes

        class OrderRuleEncodings : IEnumerable<OrderRuleEncoding>
        {
            private readonly OrderRuleEncoding[] _rules;

            public OrderRuleEncodings(IEnumerable<OrderRule> orderRules, OptionEncodings encodings)
            {
                List<OrderRuleEncoding> list = new List<OrderRuleEncoding>();

                foreach (OrderRule orderRule in orderRules)
                {
                    list.Add(new OrderRuleEncoding(orderRule, encodings));
                }

                _rules = new OrderRuleEncoding[list.Count];

                list.CopyTo(_rules);
            }

            #region IEnumerable<OrderRuleEncoding> Members

            IEnumerator<OrderRuleEncoding> IEnumerable<OrderRuleEncoding>.GetEnumerator()
            {
                return new List<OrderRuleEncoding>(_rules).GetEnumerator();
            }

            #endregion

            #region IEnumerable Members

            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return _rules.GetEnumerator();
            }

            #endregion
        }

        class OrderRuleEncoding
        {
            private readonly LogicOperator _logicOperator;
            private readonly OptionEncoding _option;
            private readonly OptionEncodings _operand;
            private readonly IRule _condition;

            public OrderRuleEncoding(OrderRule orderRule, OptionEncodings encodings)
            {
                _logicOperator = orderRule.LogicOperator;

                _option = encodings[orderRule.Option.Code];

                List<OptionEncoding> list = new List<OptionEncoding>();

                foreach (Option item in orderRule.Operand)
                {
                    list.Add(encodings[item.Code]);
                }

                _operand = new OptionEncodings(list);

                _condition = (orderRule.IsConditional && orderRule.Condition != null)
                    ? RuleBuilder.ToRule(encodings, orderRule.Condition)
                    : TrueRule.Instance;
            }

            public LogicOperator LogicOperator
            {
                get { return _logicOperator; }
            }

            public OptionEncoding Option
            {
                get { return _option; }
            }

            public OptionEncodings Operand
            {
                get { return _operand; }
            }

            public IRule Condition
            {
                get { return _condition; }
            }
        }

        class TrueRule : IRule
        {
            public static readonly TrueRule Instance = new TrueRule();

            public void Add(IRule rule)
            {
                return;
            }

            public bool Evaluate(OptionState[] values)
            {
                return true;
            }
        }

        #endregion
    }
}
