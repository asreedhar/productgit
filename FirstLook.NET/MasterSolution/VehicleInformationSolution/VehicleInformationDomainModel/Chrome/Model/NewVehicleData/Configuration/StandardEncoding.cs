namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration
{
    public class StandardEncoding : Encoding<StandardEncoding>
    {
        private readonly int _sequence;

        public StandardEncoding(int index, int sequence) : base(index)
        {
            _sequence = sequence;
        }

        public int Sequence
        {
            get { return _sequence; }
        }
    }
}
