namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration
{
    public class OptionSetEncoding
    {
        private readonly int[] variables;

        public OptionSetEncoding(OptionEncodings options)
        {
            variables = new int[options.Count];

            // look at groups
        }

        public int this[int index]
        {
            get
            {
                return variables[index];
            }
            set
            {
                variables[index] = value;
            }
        }

        public int Count
        {
            get
            {
                return variables.Length;
            }
        }
    }
}
