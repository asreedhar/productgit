namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration
{
    public class ConfigurationEngineTransition
    {
        private readonly ConfigurationEngineState startState;
        private readonly ConfigurationEngineState endState;

        public ConfigurationEngineTransition(ConfigurationEngineState startState, ConfigurationEngineState endState)
        {
            this.startState = startState;
            this.endState = endState;
        }

        public ConfigurationEngineState StartState
        {
            get { return startState; }
        }

        public ConfigurationEngineState EndState
        {
            get { return endState; }
        }
    }
}
