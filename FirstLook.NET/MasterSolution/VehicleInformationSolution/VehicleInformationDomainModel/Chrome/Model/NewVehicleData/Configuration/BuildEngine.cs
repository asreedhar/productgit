using System.Collections;
using System.Collections.Generic;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration
{
    public class BuildEngine
    {
        static IEnumerable<Category> Collect(IEnumerable<Standard> standards, IEnumerable<Option> options)
        {
            List<Category> categories = new List<Category>();

            foreach (Standard standard in standards)
            {
                foreach (StandardAction action in standard.Actions)
                {
                    Category category = action.Category;

                    if (!categories.Exists(delegate(Category value) { return value.Id == category.Id; }))
                    {
                        categories.Add(category);
                    }
                }
            }

            foreach (Option option in options)
            {
                foreach (OptionAction action in option.Actions)
                {
                    Category category = action.Category;

                    if (!categories.Exists(delegate(Category value) { return value.Id == category.Id; }))
                    {
                        categories.Add(category);
                    }
                }
            }

            return categories;
        }

        private readonly StandardEncodings _standards;

        private readonly CategoryEncodings _categories;

        private readonly OptionEncodings _options;

        private readonly ActionEncodings<StandardEncoding, Standard> _standardActions;

        private readonly ActionEncodings<OptionEncoding, Option> _optionActions;

        private readonly ConfigurationEngine _engine;

        private readonly OptionGroupEncoding _optionGroups;

        public BuildEngine(IEnumerable<Standard> standards, IEnumerable<Option> options, IEnumerable<OrderRule> orderRules)
        {
            _categories = new CategoryEncodings(Collect(standards, options));

            _standards = new StandardEncodings(standards);

            _options = new OptionEncodings(options);

            _standardActions = new ActionEncodings<StandardEncoding, Standard>(_categories, _standards, standards);

            _optionActions = new ActionEncodings<OptionEncoding, Option>(_categories, _options, options);

            _engine = new ConfigurationEngine(_options, orderRules);

            _optionGroups = new OptionGroupEncoding(_options);
        }

        public StandardEncodings Standards
        {
            get { return _standards; }
        }

        public CategoryEncodings Categories
        {
            get { return _categories; }
        }

        public OptionEncodings Options
        {
            get { return _options; }
        }

        public OptionGroupEncoding OptionGroups
        {
            get { return _optionGroups; }
        }

        public BuildEngineState Start()
        {
            Action[] categories = new Action[Categories.Count];

            foreach (ActionEncoding action in _standardActions)
            {
                action.Apply(categories);
            }

            Action[] standards = new Action[Standards.Count];

            for (int i = 0, l = Standards.Count; i < l; i++)
            {
                standards[i] = Action.Add;
            }

            return new BuildEngineState(
                _engine.Start(),
                new Actions(categories),
                new Actions(standards));
        }

        public BuildEngineState Transition(BuildEngineState buildState, OptionEncoding option, OptionState optionState)
        {
            ConfigurationEngineState newEngineState = _engine.Transition(
                buildState.EngineState,
                option,
                optionState);

            Action[] categories = new Action[_categories.Count];

            foreach (ActionEncoding action in _standardActions)
            {
                action.Apply(categories);
            }

            for (int i = 0, l = _options.Count; i < l; i++)
            {
                OptionEncoding encoding = _options[i];

                if (OptionStateHelper.IsOn(newEngineState[encoding.Index]))
                {
                    _optionActions[encoding.Index].Apply(categories);
                }
            }

            Action[] standards = new Action[_standards.Count];

            for (int i = 0, l = _standards.Count; i < l; i++)
            {
                standards[i] = (_standardActions[i].IsTrue(categories))
                   ? Action.Add
                   : Action.Remove;
            }

            return new BuildEngineState(
                newEngineState,
                new Actions(categories),
                new Actions(standards));
        }

        #region Inner Classes

        class ActionEncodings<TEncoding, TCategoryActor> : IEnumerable<ActionEncoding>
            where TEncoding : Encoding<TEncoding>
            where TCategoryActor : ICategoryActor<TCategoryActor>
        {
            private readonly ActionEncoding[] _encodings; // one for every standard or option

            public ActionEncodings(
                Encodings<CategoryEncoding, Category> categories,
                Encodings<TEncoding, TCategoryActor> encodings,
                IEnumerable<TCategoryActor> values)
            {
                _encodings = new ActionEncoding[encodings.Count];

                foreach (TCategoryActor value in values)
                {
                    _encodings[encodings[value].Index] = new ActionEncoding(categories, value.Actions);
                }
            }

            public ActionEncoding this[int index]
            {
                get
                {
                    return _encodings[index];
                }
            }

            #region IEnumerable<ActionEncoding> Members

            public IEnumerator<ActionEncoding> GetEnumerator()
            {
                foreach (ActionEncoding encoding in _encodings)
                {
                    yield return encoding;
                }
            }

            #endregion

            #region IEnumerable Members

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            #endregion
        }

        class ActionEncoding
        {
            private readonly Action[] _actions;

            public ActionEncoding(Encodings<CategoryEncoding, Category> encodings, IEnumerable<ICategoryAction> actions)
            {
                _actions = new Action[encodings.Count];

                foreach (ICategoryAction action in actions)
                {
                    CategoryEncoding encoding = encodings[action.Category];

                    _actions[encoding.Index] = action.Action;
                }
            }

            public void Apply(Action[] actions)
            {
                for (int i = 0, l = _actions.Length; i < l; i++)
                {
                    if (_actions[i] != Action.NotDefined)
                    {
                        actions[i] = _actions[i];
                    }
                }
            }

            public bool IsTrue(Action[] actions)
            {
                for (int i = 0, l = _actions.Length; i < l; i++)
                {
                    if (_actions[i] == Action.NotDefined)
                    {
                        continue;
                    }

                    if (actions[i] != _actions[i])
                    {
                        return false;
                    }
                }

                return true;
            }
        }

        #endregion
    }
}
