using System.Text;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration
{
    public static class OptionStateHelper
    {
        public static OptionState[] FromString(string text)
        {
            OptionState[] states = new OptionState[text.Length];

            for (int i = 0, l = text.Length; i < l; i++)
            {
                states[i] = FromChar(text[i]);
            }
            
            return states;
        }

        public static OptionState FromChar(char c)
        {
            OptionState state = OptionState.Off;

            switch (c)
            {
                case '0':
                    state = OptionState.Off;
                    break;
                case '1':
                    state = OptionState.On;
                    break;
                case 'R':
                    state = OptionState.Require;
                    break;
                case 'r':
                    state = OptionState.Require | OptionState.On;
                    break;
                case 'I':
                    state = OptionState.Include;
                    break;
                case 'i':
                    state = OptionState.Include | OptionState.On;
                    break;
                case 'O':
                    state = OptionState.OrRequire;
                    break;
                case 'o':
                    state = OptionState.OrRequire | OptionState.On;
                    break;
                case 'N':
                    state = OptionState.OrInclude;
                    break;
                case 'n':
                    state = OptionState.OrInclude | OptionState.On;
                    break;
                case 'X':
                    state = OptionState.Exclude;
                    break;
                case 'x':
                    state = OptionState.Exclude | OptionState.On;
                    break;
                case '!': // 6 Flags
                    state = OptionState.On | OptionState.Require | OptionState.Include | OptionState.OrRequire | OptionState.OrInclude | OptionState.Exclude;
                    break;
                case '#': // 5 Flags
                    state = OptionState.Require | OptionState.Include | OptionState.OrRequire | OptionState.OrInclude | OptionState.Exclude;
                    break;
                case '%':
                    state = OptionState.On | OptionState.Include | OptionState.OrRequire | OptionState.OrInclude | OptionState.Exclude;
                    break;
                case '&':
                    state = OptionState.On | OptionState.Require | OptionState.OrRequire | OptionState.OrInclude | OptionState.Exclude;
                    break;
                case '+':
                    state = OptionState.On | OptionState.Require | OptionState.Include | OptionState.OrInclude | OptionState.Exclude;
                    break;
                case '-':
                    state = OptionState.On | OptionState.Require | OptionState.Include | OptionState.OrRequire | OptionState.Exclude;
                    break;
                case '*':
                    state = OptionState.On | OptionState.Require | OptionState.Include | OptionState.OrRequire | OptionState.OrInclude;
                    break;
                case 'a': // 3 Flags
                    state = OptionState.Exclude | OptionState.On | OptionState.Include;
                    break;
                case 'b':
                    state = OptionState.Exclude | OptionState.On | OptionState.Require;
                    break;
                case 'c':
                    state = OptionState.Exclude | OptionState.On | OptionState.OrInclude;
                    break;
                case 'd':
                    state = OptionState.Exclude | OptionState.On | OptionState.OrRequire;
                    break;
                case 'A': // 2 Flags
                    state = OptionState.Exclude | OptionState.Include;
                    break;
                case 'B':
                    state = OptionState.Exclude | OptionState.Require;
                    break;
                case 'C':
                    state = OptionState.Exclude | OptionState.OrInclude;
                    break;
                case 'D':
                    state = OptionState.Exclude | OptionState.OrRequire;
                    break;
                
            }

            return state;
        }

        public static string ToString(OptionState[] states)
        {
            StringBuilder sb = new StringBuilder();

            foreach (OptionState state in states)
            {
                sb.Append(ToChar(state));
            }
            
            return sb.ToString();
        }

        public static char ToChar(OptionState state)
        {
            // count the number of bits

            int b = 0, u = 0;

            int n = (int) state;

            for (; n != 0; n >>= 1)
            {
                b += n & 1;
            }

            // map to a character

            char c;

            if (state == OptionState.Off)
            {
                c = '0';
            }
            else
            {
                // 6 Flags

                if (IsSet(state, OptionState.On|OptionState.Require|OptionState.Include|OptionState.OrRequire|OptionState.OrInclude|OptionState.Exclude))
                {
                    c = '!';
                    u = 6;
                }

                // 5 Flags

                else if (IsSet(state, OptionState.Require | OptionState.Include | OptionState.OrRequire | OptionState.OrInclude | OptionState.Exclude))
                {
                    c = '#'; u = 5;
                }
                else if (IsSet(state, OptionState.On | OptionState.Include | OptionState.OrRequire | OptionState.OrInclude | OptionState.Exclude))
                {
                    c = '%'; u = 5;
                }
                else if (IsSet(state, OptionState.On | OptionState.Require | OptionState.OrRequire | OptionState.OrInclude | OptionState.Exclude))
                {
                    c = '&'; u = 5;
                }
                else if (IsSet(state, OptionState.On | OptionState.Require | OptionState.Include | OptionState.OrInclude | OptionState.Exclude))
                {
                    c = '+'; u = 5;
                }
                else if (IsSet(state, OptionState.On | OptionState.Require | OptionState.Include | OptionState.OrRequire | OptionState.Exclude))
                {
                    c = '-'; u = 5;
                }
                else if (IsSet(state, OptionState.On | OptionState.Require | OptionState.Include | OptionState.OrRequire | OptionState.OrInclude))
                {
                    c = '*'; u = 5;
                }
                
                // 3 Flags

                // exclude + on + flag

                else if (IsSet(state, OptionState.Exclude | OptionState.On | OptionState.Include))
                {
                    c = 'a'; u = 3;
                }
                else if (IsSet(state, OptionState.Exclude | OptionState.On | OptionState.Require))
                {
                    c = 'b'; u = 3;
                }
                else if (IsSet(state, OptionState.Exclude | OptionState.On | OptionState.OrInclude))
                {
                    c = 'c'; u = 3;
                }
                else if (IsSet(state, OptionState.Exclude | OptionState.On | OptionState.OrRequire))
                {
                    c = 'd'; u = 3;
                }

                // 2 Flags

                // flag + on

                else if (IsSet(state, OptionState.Require | OptionState.On))
                {
                    c = 'r'; u = 2;
                }
                else if (IsSet(state, OptionState.Include | OptionState.On))
                {
                    c = 'i'; u = 2;
                }
                else if (IsSet(state, OptionState.OrRequire | OptionState.On))
                {
                    c = 'o'; u = 2;
                }
                else if (IsSet(state, OptionState.OrInclude | OptionState.On))
                {
                    c = 'n'; u = 2;
                }
                else if (IsSet(state, OptionState.Exclude | OptionState.On))
                {
                    c = 'x'; u = 2;
                }

                // flag + exclude

                else if (IsSet(state, OptionState.Exclude | OptionState.Include))
                {
                    c = 'A'; u = 2;
                }
                else if (IsSet(state, OptionState.Exclude | OptionState.Require))
                {
                    c = 'B'; u = 2;
                }
                else if (IsSet(state, OptionState.Exclude | OptionState.OrInclude))
                {
                    c = 'C'; u = 2;
                }
                else if (IsSet(state, OptionState.Exclude | OptionState.OrRequire))
                {
                    c = 'D'; u = 2;
                }

                // 1 Flag

                else if (IsSet(state, OptionState.On))
                {
                    c = '1'; u = 1;
                }
                else if (IsSet(state, OptionState.Require))
                {
                    c = 'R'; u = 1;
                }
                else if (IsSet(state, OptionState.Include))
                {
                    c = 'I'; u = 1;
                }
                else if (IsSet(state, OptionState.OrRequire))
                {
                    c = 'O'; u = 1;
                }
                else if (IsSet(state, OptionState.OrInclude))
                {
                    c = 'N'; u = 1;
                }
                else if (IsSet(state, OptionState.Exclude))
                {
                    c = 'X'; u = 1;
                }

                // Not Specified (SAD FACE)

                else
                {
                    c = '?';
                }
            }

            if (b != u)
            {
                c = '?';
            }

            return c;
        }

        public static bool IsOn(OptionState state)
        {
            return IsSet(state, OptionState.On, OptionState.Include, OptionState.Require);
        }

        public static bool IsOff(OptionState state)
        {
            return state == OptionState.Off;
        }

        public static bool IsExcluded(OptionState state)
        {
            return IsSet(state, OptionState.Exclude);
        }

        public static bool IsSet(OptionState value, params OptionState[] flags)
        {
            foreach (OptionState flag in flags)
            {
                if ((value & flag) == flag)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
