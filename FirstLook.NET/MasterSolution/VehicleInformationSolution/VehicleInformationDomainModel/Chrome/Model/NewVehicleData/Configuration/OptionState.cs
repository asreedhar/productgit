using System;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration
{
    [Flags]
    public enum OptionState
    {
        Off         = 0,
        On          = 2 << 0,
        Require     = 2 << 1,
        Include     = 2 << 2,
        OrRequire   = 2 << 3,
        OrInclude   = 2 << 4,
        Exclude     = 2 << 5
    }
}
