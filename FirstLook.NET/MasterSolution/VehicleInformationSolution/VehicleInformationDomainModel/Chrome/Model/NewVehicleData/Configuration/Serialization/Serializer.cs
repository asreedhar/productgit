using System.IO;
using System.Xml;
using System.Xml.Serialization;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration.Serialization
{
    public static class Serializer
    {
        public static XmlDocument Serialize(string vin, int styleId, int styleVersion, BuildEngine engine, BuildEngineState engineState)
        {
            BuildSpecification specification = new BuildSpecification();
            specification.Vin = vin;
            specification.Version = 1.0m;

            specification.Style = new Style();
            specification.Style.Id = styleId;
            specification.Style.Version = styleVersion;

            Build theBuild = new Build();

            switch (engineState.EngineState.LogicOperator)
            {
                case LogicOperator.Exclude:
                    theBuild.LastOperator = BuildLogicOperator.X;
                    break;
                case LogicOperator.Include:
                    theBuild.LastOperator = BuildLogicOperator.I;
                    break;
                case LogicOperator.MutualRequire:
                    theBuild.LastOperator = BuildLogicOperator.M;
                    break;
                case LogicOperator.OrInclude:
                    theBuild.LastOperator = BuildLogicOperator.N;
                    break;
                case LogicOperator.OrRequire:
                    theBuild.LastOperator = BuildLogicOperator.O;
                    break;
                case LogicOperator.Require:
                    theBuild.LastOperator = BuildLogicOperator.R;
                    break;
            }

            switch (engineState.EngineState.EngineResult)
            {
                case ConfigurationEngineResult.Normal:
                    theBuild.State = BuildState.Normal;
                    break;
                case ConfigurationEngineResult.Query:
                    theBuild.State = BuildState.Query;
                    break;
            }

            theBuild.Categories = new BuildCategoryCollection();

            for (int i = 0, l = engineState.CategoryStates.Count; i < l; i++)
            {
                CategoryEncoding category = engine.Categories[i];

                Action action = engineState.CategoryStates[i];

                BuildCategory item = new BuildCategory();

                item.CategoryId = category.Id;

                switch (action)
                {
                    case Action.Add:
                        item.CategoryState = BuildCategoryState.Add;
                        break;
                    case Action.Package:
                        item.CategoryState = BuildCategoryState.Package;
                        break;
                    case Action.Remove:
                        item.CategoryState = BuildCategoryState.Remove;
                        break;
                }
                
                theBuild.Categories.Add(item);
            }

            theBuild.Standards = new BuildStandardCollection();

            for (int i = 0, l = engineState.StandardStates.Count; i < l; i++)
            {
                StandardEncoding standard = engine.Standards[i];

                Action action = engineState.StandardStates[i];

                BuildStandard item = new BuildStandard();

                item.StandardSequence = standard.Sequence;

                switch (action)
                {
                    case Action.Add:
                        item.StandardState = BuildStandardState.Add;
                        break;
                    case Action.Remove:
                        item.StandardState = BuildStandardState.Remove;
                        break;
                }

                theBuild.Standards.Add(item);
            }

            theBuild.Options = new BuildOptionCollection();

            for (int i = 0, l = engineState.EngineState.Count; i < l; i++)
            {
                OptionState optionState = engineState.EngineState[i];

                OptionEncoding option = engine.Options[i];

                BuildOption item = new BuildOption();

                item.OptionCode = option.OptionCode;

                item.OptionStates = new BuildOptionStateCollection();

                if (OptionStateHelper.IsSet(optionState, OptionState.Exclude))
                {
                    item.OptionStates.Add(BuildOptionState.Exclude);
                }
                if (OptionStateHelper.IsSet(optionState, OptionState.Include))
                {
                    item.OptionStates.Add(BuildOptionState.Include);
                }
                if (optionState == OptionState.Off)
                {
                    item.OptionStates.Add(BuildOptionState.Off);
                }
                if (OptionStateHelper.IsSet(optionState, OptionState.On))
                {
                    item.OptionStates.Add(BuildOptionState.On);
                }
                if (OptionStateHelper.IsSet(optionState, OptionState.OrInclude))
                {
                    item.OptionStates.Add(BuildOptionState.OrInclude);
                }
                if (OptionStateHelper.IsSet(optionState, OptionState.OrRequire))
                {
                    item.OptionStates.Add(BuildOptionState.OrRequire);
                }
                if (OptionStateHelper.IsSet(optionState, OptionState.Require))
                {
                    item.OptionStates.Add(BuildOptionState.Require);
                }

                theBuild.Options.Add(item);
            }

            theBuild.Selections = new BuildOptionCollection();

            foreach (OptionEncoding optionSelection in engineState.EngineState.OptionSelections)
            {
                BuildOption option = new BuildOption();

                option.OptionCode = optionSelection.OptionCode;

                theBuild.Selections.Add(option);
            }

            specification.Build = theBuild;

            return DoSerialize(specification);
        }

        public static BuildEngineState Deserialize(BuildEngine engine, XmlDocument document)
        {
            BuildSpecification specification = DoDeserialize(document);

            BuildEngineState engineState = engine.Start();

            foreach (BuildOption option in specification.Build.Selections)
            {
                OptionEncoding encoding = engine.Options[option.OptionCode];

                if (encoding != null)
                {
                    engineState = engine.Transition(
                        engineState,
                        encoding,
                        OptionState.On);
                }
            }

            return engineState;
        }

        internal static XmlDocument DoSerialize(BuildSpecification build)
        {
            XmlSerializer ser = new XmlSerializer(typeof(BuildSpecification));
            
            XmlDocument document = new XmlDocument();

            using (MemoryStream ms = new MemoryStream())
            {
                ser.Serialize(ms, build);

                ms.Seek(0, SeekOrigin.Begin);

                document.Load(ms);
            }

            return document;
        }

        internal static BuildSpecification DoDeserialize(XmlDocument document)
        {
            XmlSerializer ser = new XmlSerializer(typeof(BuildSpecification));

            using (XmlNodeReader reader = new XmlNodeReader(document))
            {
                return (BuildSpecification) ser.Deserialize(reader);
            }
        }
    }
}
