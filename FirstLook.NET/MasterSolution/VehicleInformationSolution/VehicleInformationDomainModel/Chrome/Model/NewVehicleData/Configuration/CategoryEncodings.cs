using System.Collections.Generic;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration
{
    public class CategoryEncodings : Encodings<CategoryEncoding, Category>
    {
        public static readonly CategoryEncodings Empty = new CategoryEncodings(new CategoryEncoding[0]);

        public CategoryEncodings(IEnumerable<Category> standards)
            : base(standards)
        {
        }

        public CategoryEncodings(ICollection<CategoryEncoding> values)
            : base(values)
        {
        }

        protected override CategoryEncoding Map(int index, Category value)
        {
            return new CategoryEncoding(index, value.Id);
        }

        public CategoryEncoding Find(int id)
        {
            foreach (CategoryEncoding value in this)
            {
                if (Equals(value.Id, id))
                {
                    return value;
                }
            }

            return null;
        }

        public override CategoryEncoding this[Category category]
        {
            get
            {
                foreach (CategoryEncoding value in this)
                {
                    if (Equals(value.Id, category.Id))
                    {
                        return value;
                    }
                }

                return null;
            }
        }
    }
}
