using System.Text;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration
{
    public class BuildEngineState
    {
        private readonly ConfigurationEngineState _engineState;
        private readonly Actions _categoryStates;
        private readonly Actions _standardStates;

        public BuildEngineState(ConfigurationEngineState engineState, Actions categoryStates, Actions standardStates)
        {
            _engineState = engineState;
            _categoryStates = categoryStates;
            _standardStates = standardStates;
        }

        public ConfigurationEngineState EngineState
        {
            get { return _engineState; }
        }

        public Actions CategoryStates
        {
            get { return _categoryStates; }
        }

        public Actions StandardStates
        {
            get { return _standardStates; }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            // text is "engine$category$standard"

            sb.Append(_engineState).Append('$').Append(_categoryStates).Append('$').Append(_standardStates);

            return sb.ToString();
        }

        public static bool TryParse(string value, BuildEngine engine, out BuildEngineState buildState)
        {
            if (string.IsNullOrEmpty(value))
            {
                buildState = null;

                return false;
            }

            // fields

            ConfigurationEngineState engineState = null;

            Actions categoryStates= null;

            Actions standardStates = null;

            // text is "engine$category$standard"

            bool error = false;

            int start = 0, index = 0;

            int[] minimumLength = new int[] { 6, 0, 0 };

            for (int i = 0; i < 3; i++)
            {
                string text;

                if (i < 2)
                {
                    index = value.IndexOf('$', start);

                    if (index == -1)
                    {
                        error = true;

                        break;
                    }

                    int length = index - start;

                    text = value.Substring(start, length);
                }
                else
                {
                    text = value.Substring(start);
                }

                if (text.Length < minimumLength[i])
                {
                    error = true;

                    break;
                }

                switch (i)
                {
                    case 0:
                        error |= !ConfigurationEngineState.TryParse(text, engine.Options, out engineState);
                        break;
                    case 1:
                        error |= !Actions.TryParse(text, out categoryStates);
                        break;
                    case 2:
                        error |= !Actions.TryParse(text, out standardStates);
                        break;
                }

                start = index + 1;
            }

            if (error)
            {
                buildState = null;

                return false;
            }

            buildState = new BuildEngineState(
                engineState,
                categoryStates,
                standardStates);

            return true;
        }
    }
}
