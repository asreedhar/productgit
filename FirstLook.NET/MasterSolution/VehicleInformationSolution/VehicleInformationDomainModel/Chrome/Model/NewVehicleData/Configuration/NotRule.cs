using System;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration
{
    public class NotRule : IRule
    {
        private IRule _rule;

        public void Add(IRule rule)
        {
            if (_rule == null)
            {
                _rule = rule;
            }
            else
            {
                throw new ArgumentException("Already Set", "rule");
            }
        }

        public bool Evaluate(OptionState[] values)
        {
            return !_rule.Evaluate(values);
        }
    }
}