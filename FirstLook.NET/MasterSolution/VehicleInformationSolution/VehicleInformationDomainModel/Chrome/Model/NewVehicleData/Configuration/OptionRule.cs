using System;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration
{
    public class OptionRule : IRule
    {
        private readonly int _index;

        public OptionRule(int index)
        {
            _index = index;
        }

        public void Add(IRule rule)
        {
            throw new NotSupportedException();
        }

        public bool Evaluate(OptionState[] values)
        {
            return (values[_index] & OptionState.On) == OptionState.On;
        }
    }
}