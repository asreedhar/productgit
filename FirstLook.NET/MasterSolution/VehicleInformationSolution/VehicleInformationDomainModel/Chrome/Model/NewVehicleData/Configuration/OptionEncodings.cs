using System;
using System.Collections.Generic;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration
{
    public class OptionEncodings : Encodings<OptionEncoding, Option>
    {
        public static readonly OptionEncodings Empty = new OptionEncodings(new OptionEncoding[0]);

        public OptionEncodings(IEnumerable<Option> options)
            : base(options)
        {
        }

        public OptionEncodings(ICollection<OptionEncoding> options)
            : base(options)
        {
        }

        protected override OptionEncoding Map(int index, Option value)
        {
            int requirement = (value.OptionKind == null) ? 0 : value.OptionKind.Id;

            return new OptionEncoding(index, value.Code, value.GroupName, requirement, value.Availability);
        }

        public override OptionEncoding this[Option option]
        {
            get
            {
                return this[option.Code];
            }
        }

        public OptionEncoding this[string optionCode]
        {
            get
            {
                foreach (OptionEncoding value in this)
                {
                    if (Equals(value.OptionCode, optionCode))
                    {
                        return value;
                    }
                }
                return null;
            }
        }

        public OptionEncodings Select(OptionEncoding encoding)
        {
            List<OptionEncoding> values = new List<OptionEncoding>(this);

            values.Add(encoding);

            return new OptionEncodings(values);
        }

        public OptionEncodings Remove(OptionEncoding encoding)
        {
            List<OptionEncoding> values = new List<OptionEncoding>(
                Array.FindAll(Values,
                    delegate(OptionEncoding value)
                        {
                            return value.Index == encoding.Index;
                        }));

            return new OptionEncodings(values);
        }
    }
}
