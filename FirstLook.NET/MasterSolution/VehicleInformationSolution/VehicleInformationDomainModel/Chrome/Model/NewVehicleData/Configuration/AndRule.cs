using System.Collections.Generic;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration
{
    public class AndRule : IRule
    {
        private readonly List<IRule> _rules = new List<IRule>();

        public void Add(IRule rule)
        {
            _rules.Add(rule);
        }

        public bool Evaluate(OptionState[] values)
        {
            foreach (IRule rule in _rules)
            {
                if (!rule.Evaluate(values))
                {
                    return false;
                }
            }

            return true;
        }
    }
}