using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData
{
    public class Actions : IEnumerable<Action>
    {
        private readonly Action[] _values;

        public Actions(Action[] values)
        {
            _values = values;
        }

        public int Count
        {
            get
            {
                return _values.Length;
            }
        }

        public Action this[int index]
        {
            get
            {
                return _values[index];
            }
        }

        public void CopyTo(Action[] destination)
        {
            Array.Copy(_values, destination, _values.Length);
        }

        #region IEnumerable<Action> Members

        public IEnumerator<Action> GetEnumerator()
        {
            foreach (Action value in _values)
            {
                yield return value;
            }
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            foreach (Action value in _values)
            {
                char c = '?';

                switch (value)
                {
                    case Action.NotDefined:
                        c = 'N';
                        break;
                    case Action.Add:
                        c = 'C';
                        break;
                    case Action.Package:
                        c = 'P';
                        break;
                    case Action.Remove:
                        c = 'D';
                        break;
                }

                sb.Append(c);
            }

            return sb.ToString();
        }

        public static bool TryParse(string text, out Actions actions)
        {
            Action[] values = new Action[text.Length];

            for (int i = 0, l = text.Length; i < l; i++)
            {
                char c = text[i];

                Action a;

                switch (c)
                {
                    case 'N':
                        a = Action.NotDefined;
                        break;
                    case 'C':
                        a = Action.Add;
                        break;
                    case 'P':
                        a = Action.Package;
                        break;
                    case 'D':
                        a = Action.Remove;
                        break;
                    default:
                        actions = null;
                        return false;
                }

                values[i] = a;
            }

            actions = new Actions(values);

            return true;
        }
    }
}
