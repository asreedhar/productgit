///////////////////////////////////////////////////////////
//  IHeader.cs
//  Implementation of the Interface IHeader
//  Generated by Enterprise Architect
//  Created on:      21-Mar-2010 4:14:03 PM
//  Original author: swenmouth
///////////////////////////////////////////////////////////




namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData {
	public interface IHeader {

		int Id{
			get;
		}

		string Name{
			get;
		}
	}//end IHeader

}//end namespace NewVehicleData