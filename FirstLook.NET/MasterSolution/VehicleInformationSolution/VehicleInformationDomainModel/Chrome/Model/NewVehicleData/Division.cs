///////////////////////////////////////////////////////////
//  Division.cs
//  Implementation of the Class Division
//  Generated by Enterprise Architect
//  Created on:      21-Mar-2010 4:14:03 PM
//  Original author: swenmouth
///////////////////////////////////////////////////////////


namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData {
	public class Division {

		private readonly int id;
		private readonly string name;
		private readonly Manufacturer manufacturer;

		/// 
		/// <param name="manufacturer"></param>
		/// <param name="name"></param>
		/// <param name="id"></param>
        public Division(string name, int id, Manufacturer manufacturer)
        {
		    this.id = id;
		    this.name = name;
		    this.manufacturer = manufacturer;
		}

		public int Id{
			get{
				return id;
			}
		}

		public Manufacturer Manufacturer{
			get{
				return manufacturer;
			}
		}

		public string Name{
			get{
				return name;
			}
		}

	}//end Division

}//end namespace NewVehicleData