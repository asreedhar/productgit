namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData
{
    public class TechnicalSpecification
    {
        private TechnicalSpecificationHeader header;
        private int id;
        private string label;
        private string value;
        private ICondition condition;

        public TechnicalSpecification(TechnicalSpecificationHeader header, int id, string label, string value,
                                      ICondition condition)
        {
            this.header = header;
            this.id = id;
            this.label = label;
            this.value = value;
            this.condition = condition;
        }

        public TechnicalSpecificationHeader Header
        {
            get { return header; }
        }

        public int Id
        {
            get { return id; }
        }

        public string Label
        {
            get { return label; }
        }

        public string Value
        {
            get { return value; }
        }

        public ICondition Condition
        {
            get { return condition; }
        }
    }
}