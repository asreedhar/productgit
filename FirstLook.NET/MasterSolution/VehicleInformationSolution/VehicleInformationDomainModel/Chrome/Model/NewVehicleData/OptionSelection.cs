namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData
{
    public class OptionSelection
    {
        private readonly string optionCode;
        private readonly bool selected;

        public OptionSelection(string optionCode, bool selected)
        {
            this.optionCode = optionCode;
            this.selected = selected;
        }

        public string OptionCode
        {
            get { return optionCode; }
        }

        public bool Selected
        {
            get { return selected; }
        }
    }
}