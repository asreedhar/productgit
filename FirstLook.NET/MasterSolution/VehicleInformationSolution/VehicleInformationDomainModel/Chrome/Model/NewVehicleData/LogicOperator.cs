///////////////////////////////////////////////////////////
//  LogicOperator.cs
//  Implementation of the Class LogicOperator
//  Generated by Enterprise Architect
//  Created on:      21-Mar-2010 4:14:04 PM
//  Original author: swenmouth
///////////////////////////////////////////////////////////




namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData {
	public enum LogicOperator
	{

		NotDefined = 0,
        Require = 1,
        MutualRequire = 2,
        OrRequire = 3,
        Include = 4,
        OrInclude = 5,
        Exclude = 6

	}//end LogicOperator

}//end namespace NewVehicleData