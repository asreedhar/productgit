namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData
{
    public class ConsumerInformationHeader : IHeader
    {
        private readonly int id;
        private readonly string name;

        public ConsumerInformationHeader(int id, string name)
        {
            this.id = id;
            this.name = name;
        }

        public int Id
        {
            get { return id; }
        }

        public string Name
        {
            get { return name; }
        }
    }
}