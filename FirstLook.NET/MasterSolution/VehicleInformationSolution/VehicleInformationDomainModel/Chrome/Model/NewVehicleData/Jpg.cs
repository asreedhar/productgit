namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData
{
    public class Jpg
    {
        private readonly string relativePath;

        public Jpg(string relativePath)
        {
            this.relativePath = relativePath;
        }

        public string RelativePath
        {
            get { return relativePath; }
        }
    }
}