using System.Xml;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData
{
    public class StyleSnapshot
    {
        private readonly XmlDocument _style;
        private readonly short _styleVersion;
        private readonly XmlDocument _reference;
        private readonly short _referenceVersion;

        public StyleSnapshot(XmlDocument reference, short referenceVersion, XmlDocument style, short styleVersion)
        {
            _reference = reference;
            _referenceVersion = referenceVersion;
            _style = style;
            _styleVersion = styleVersion;
        }

        public short StyleVersion
        {
            get { return _styleVersion; }
        }

        public short ReferenceVersion
        {
            get { return _referenceVersion; }
        }

        public XmlDocument Style
        {
            get { return _style; }
        }

        public XmlDocument Reference
        {
            get { return _reference; }
        }
    }
}