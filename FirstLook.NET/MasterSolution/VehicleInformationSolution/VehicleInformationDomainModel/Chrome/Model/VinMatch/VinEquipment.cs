///////////////////////////////////////////////////////////
//  VinEquipment.cs
//  Implementation of the Class VinEquipment
//  Generated by Enterprise Architect
//  Created on:      21-Mar-2010 4:14:08 PM
//  Original author: swenmouth
///////////////////////////////////////////////////////////


namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.VinMatch {
	public class VinEquipment {

		private Category category;
		private VinAvailability availability;

		/// 
		/// <param name="availability"></param>
		/// <param name="category"></param>
		public VinEquipment(VinAvailability availability, Category category){
		    this.availability = availability;
		    this.category = category;
		}

		public VinAvailability Availability{
			get{
				return availability;
			}
		}

		public Category Category{
			get{
				return category;
			}
		}

	}//end VinEquipment

}//end namespace VinMatch