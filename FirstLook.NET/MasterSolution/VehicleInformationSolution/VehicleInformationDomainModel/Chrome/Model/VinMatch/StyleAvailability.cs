///////////////////////////////////////////////////////////
//  StyleAvailability.cs
//  Implementation of the Class StyleAvailability
//  Generated by Enterprise Architect
//  Created on:      21-Mar-2010 4:14:07 PM
//  Original author: swenmouth
///////////////////////////////////////////////////////////




namespace FirstLook.VehicleInformation.DomainModel.Chrome.Model.VinMatch {
	public enum StyleAvailability
	{

		NotDefined = 0,
		Standard = 1,
		Optional = 2

	}//end StyleAvailability

}//end namespace VinMatch