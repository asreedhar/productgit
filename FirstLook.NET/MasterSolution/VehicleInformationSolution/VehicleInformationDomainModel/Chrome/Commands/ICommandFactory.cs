﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects.Envelopes;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands
{
    public interface ICommandFactory
    {
        ICommand<CategoryResultsDto, CategoryArgumentsDto> CreateCategoryCommand();

        ICommand<CategoryIdentificationResultsDto, CategoryIdentificationArgumentsDto> CreateCategoryIdentificationCommand();

        ICommand<ColorOptionsResultsDto, ColorOptionsArgumentsDto> CreateColorOptionsCommand();

        ICommand<ConfigurationInitializeResultsDto, ConfigurationInitializeArgumentsDto> CreateConfigurationInitializeCommand();

        ICommand<ConfigurationUpdateResultsDto, ConfigurationUpdateArgumentsDto> CreateConfigurationUpdateCommand();

        ICommand<EncodingsResultsDto, EncodingsArgumentsDto> CreateEncodingsCommand();

        ICommand<NullDto, IdentityContextDto<SaveSpecificationArgumentsDto>> CreateSaveSpecificationCommand();

        ICommand<StyleResultsDto, IdentityContextDto<StyleArgumentsDto>> CreateStyleCommand();

        ICommand<VinPatternResultsDto, VinPatternArgumentsDto> CreateVinPatternCommand();
    }
}
