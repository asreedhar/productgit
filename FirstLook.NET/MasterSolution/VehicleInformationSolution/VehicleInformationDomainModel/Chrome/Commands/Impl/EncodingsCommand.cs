using System;
using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.Impl
{
    public class EncodingsCommand : ICommand<EncodingsResultsDto, EncodingsArgumentsDto>
    {
        public EncodingsResultsDto Execute(EncodingsArgumentsDto parameters)
        {
            INewVehicleDataRepository repository = RegistryFactory.GetRegistry().Resolve<INewVehicleDataRepository>();

            Style style = repository.Fetch(parameters.VehiclePrototype.StyleId);

            if (style == null)
            {
                throw new Exception("fuckity fuck fuck");
            }

            BuildEngine buildEngine = new BuildEngine(style.Standards, style.Options, style.OrderRules);

            EncodingsDto encodings = new EncodingsDto();

            List<EncodingDto> standards = new List<EncodingDto>();

            foreach (StandardEncoding standard in buildEngine.Standards)
            {
                standards.Add(new EncodingDto(standard.Index, standard.Sequence));
            }

            encodings.Standards = standards;

            List<EncodingDto> categories = new List<EncodingDto>();

            foreach (CategoryEncoding category in buildEngine.Categories)
            {
                categories.Add(new EncodingDto(category.Index, category.Id));
            }

            encodings.Categories = categories;

            List<EncodingDto> options = new List<EncodingDto>();

            foreach (OptionEncoding option in buildEngine.Options)
            {
                options.Add(new EncodingDto(option.Index, option.OptionCode));
            }

            encodings.Options = options;

            return new EncodingsResultsDto {Encodings = encodings};
        }
    }
}
