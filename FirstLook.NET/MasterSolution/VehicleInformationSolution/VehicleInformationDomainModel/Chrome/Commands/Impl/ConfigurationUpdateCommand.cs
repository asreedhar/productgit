using System;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.Builders;
using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.Impl
{
    public class ConfigurationUpdateCommand : ICommand<ConfigurationUpdateResultsDto, ConfigurationUpdateArgumentsDto>
    {
        public ConfigurationUpdateResultsDto Execute(ConfigurationUpdateArgumentsDto parameters)
        {
            INewVehicleDataRepository repository = RegistryFactory.GetRegistry().Resolve<INewVehicleDataRepository>();

            Style style = repository.Fetch(parameters.VehicleBuild.Prototype.StyleId);

            if (style == null)
            {
                throw new Exception("fuckity fuck fuck");
            }

            OptionCollectionDto choices = new OptionCollectionDto();

            BuildEngine buildEngine = new BuildEngine(style.Standards, style.Options, style.OrderRules);

            BuildEngineState buildEngineState;

            if (!BuildEngineState.TryParse(parameters.VehicleBuild.State, buildEngine, out buildEngineState))
            {
                throw new Exception("there's no state when she's gone ...");
            }

            BuildEngineState nextBuildEngineState = buildEngine.Transition(
                buildEngineState,
                buildEngine.Options[parameters.OptionCode],
                parameters.Selected ? OptionState.On : OptionState.Off);

            ConfigurationEngineState nextState = nextBuildEngineState.EngineState;

            if (nextState.EngineResult == ConfigurationEngineResult.Query)
            {
                for (int i = 0, l = nextState.Count; i < l; i++)
                {
                    OptionState optionState = nextState[i];

                    bool choice = false;

                    choice |= ((optionState & OptionState.OrInclude) == OptionState.OrInclude);

                    choice |= ((optionState & OptionState.OrRequire) == OptionState.OrRequire);

                    if (choice)
                    {
                        OptionEncoding encoding = buildEngine.Options[i];

                        foreach (Option option in style.Options)
                        {
                            if (Equals(option.Code, encoding.OptionCode))
                            {
                                choices.Add(Mapper.ToInfo(option));

                                break;
                            }
                        }
                    }
                }
            }

            return new ConfigurationUpdateResultsDto
                       {
                           NextState = nextBuildEngineState.ToString(),
                           Choices = choices
                       };
        }
    }
}
