using System;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.Builders;
using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration;
using Action=FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Action;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.Impl
{
    public class CategoryIdentificationCommand : ICommand<CategoryIdentificationResultsDto, CategoryIdentificationArgumentsDto>
    {
        public CategoryIdentificationResultsDto Execute(CategoryIdentificationArgumentsDto parameters)
        {
            OptionCollectionDto candidates = new OptionCollectionDto();

            INewVehicleDataRepository repository = RegistryFactory.GetRegistry().Resolve<INewVehicleDataRepository>();

            Style style = repository.Fetch(parameters.VehicleBuild.Prototype.StyleId);

            if (style == null)
            {
                throw new Exception("fuckity fuck fuck");
            }

            BuildEngine buildEngine = new BuildEngine(style.Standards, style.Options, style.OrderRules);

            BuildEngineState buildEngineState;

            if (!BuildEngineState.TryParse(parameters.VehicleBuild.State, buildEngine, out buildEngineState))
            {
                throw new Exception("there's no state when she's gone ...");
            }

            ConfigurationEngineState state = buildEngineState.EngineState;

            if (parameters.Addition)
            {

                SelectCandidates(style, Action.Add, state, buildEngine.Options, candidates, parameters.CategoryId);

                // we still need code that determines when the selected options satisfy
                // a package

                if (candidates.Count == 0)
                {
                    SelectCandidates(style, Action.Package, state, buildEngine.Options, candidates, parameters.CategoryId);
                }
            }
            else // candidates are currently selected options that add this category
            {
                foreach (Option option in style.Options)
                {
                    OptionState optionState = state[buildEngine.Options[option.Code].Index];

                    if ((optionState & OptionState.On) == OptionState.On)
                    {
                        foreach (OptionAction action in option.Actions)
                        {
                            if (action.Category.Id == parameters.CategoryId)
                            {
                                switch (action.Action)
                                {
                                    case Action.Add:
                                    case Action.Package:
                                        candidates.Add(Mapper.ToInfo(option));
                                        break;
                                }

                                break;
                            }
                        }
                    }
                }
            }

            return new CategoryIdentificationResultsDto {Candidates = candidates};
        }

        private static void SelectCandidates(Style style, Action operation, ConfigurationEngineState state, OptionEncodings encodings, OptionCollectionDto candidates, int categoryId)
        {
            foreach (Option option in style.Options)
            {
                OptionState optionState = state[encodings[option.Code].Index];

                bool isOn = (optionState & OptionState.On) == OptionState.On;

                if (!isOn)
                {
                    foreach (OptionAction action in option.Actions)
                    {
                        if (action.Category.Id == categoryId)
                        {
                            if (action.Action == operation)
                            {
                                candidates.Add(Mapper.ToInfo(option));
                            }

                            break;
                        }
                    }
                }
            }
        }
    }
}