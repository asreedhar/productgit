﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects.Envelopes;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.Impl
{
    public class CommandFactory : ICommandFactory
    {
        public ICommand<CategoryResultsDto, CategoryArgumentsDto> CreateCategoryCommand()
        {
            return new CategoryCommand();
        }

        public ICommand<CategoryIdentificationResultsDto, CategoryIdentificationArgumentsDto> CreateCategoryIdentificationCommand()
        {
            return new CategoryIdentificationCommand();
        }

        public ICommand<ColorOptionsResultsDto, ColorOptionsArgumentsDto> CreateColorOptionsCommand()
        {
            return new ColorOptionsCommand();
        }

        public ICommand<ConfigurationInitializeResultsDto, ConfigurationInitializeArgumentsDto> CreateConfigurationInitializeCommand()
        {
            return new ConfigurationInitializeCommand();
        }

        public ICommand<ConfigurationUpdateResultsDto, ConfigurationUpdateArgumentsDto> CreateConfigurationUpdateCommand()
        {
            return new ConfigurationUpdateCommand();
        }

        public ICommand<EncodingsResultsDto, EncodingsArgumentsDto> CreateEncodingsCommand()
        {
            return new EncodingsCommand();
        }

        public ICommand<NullDto, IdentityContextDto<SaveSpecificationArgumentsDto>> CreateSaveSpecificationCommand()
        {
            return new SaveSpecificationCommand();
        }

        public ICommand<StyleResultsDto, IdentityContextDto<StyleArgumentsDto>> CreateStyleCommand()
        {
            return new StyleCommand();
        }

        public ICommand<VinPatternResultsDto, VinPatternArgumentsDto> CreateVinPatternCommand()
        {
            return new VinPatternCommand();
        }
    }
}
