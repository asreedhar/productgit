using System;
using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.Builders;
using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.Impl
{
    public class CategoryCommand : ICommand<CategoryResultsDto, CategoryArgumentsDto>
    {
        public CategoryResultsDto Execute(CategoryArgumentsDto parameters)
        {
            INewVehicleDataRepository repository = RegistryFactory.GetRegistry().Resolve<INewVehicleDataRepository>();

            Style style = repository.Fetch(parameters.VehicleBuild.Prototype.StyleId);

            if (style == null)
            {
                throw new Exception("fuckity fuck fuck");
            }

            CategoryResultsDto result = new CategoryResultsDto();

            CategoryHeaderListBuilder builder = new CategoryHeaderListBuilder();

            foreach (Option option in style.Options)
            {
                if (parameters.VehicleBuild.Prototype.IsFleet)
                {
                    if (option.Availability == Availability.Fleet || option.Availability == Availability.Any)
                    {
                        Add(builder, option);
                    }
                }
                else
                {
                    if (option.Availability == Availability.Any || option.Availability == Availability.Retail)
                    {
                        Add(builder, option);
                    }
                }
            }

            BuildEngine buildEngine = new BuildEngine(style.Standards, style.Options, style.OrderRules);

            BuildEngineState buildEngineState;

            if (BuildEngineState.TryParse(parameters.VehicleBuild.State, buildEngine, out buildEngineState))
            {
                ConfigurationEngineState engineState = buildEngineState.EngineState;

                for (int i = 0, l = engineState.Count; i < l; i++)
                {
                    OptionState optionState = engineState[i];

                    if (optionState == OptionState.Off)
                    {
                        continue;
                    }

                    if ((optionState & OptionState.Exclude) == OptionState.Exclude)
                    {
                        continue;
                    }

                    string optionCode = buildEngine.Options[i].OptionCode;

                    foreach (Option option in style.Options)
                    {
                        if (Equals(option.Code, optionCode))
                        {
                            Apply(builder, option.Actions);

                            break;
                        }
                    }
                }
            }

            builder.CopyTo(result.CategoryHeaders);

            return result;
        }

        private static void Add(CategoryHeaderListBuilder builder, Option option)
        {
            foreach (OptionAction action in option.Actions)
            {
                builder.Add(action.Category);
            }
        }

        private static void Apply(CategoryHeaderListBuilder builder, IEnumerable<ICategoryAction> actions)
        {
            foreach (ICategoryAction action in actions)
            {
                builder.Apply(action);
            }
        }
    }
}