using System.Collections.Generic;
using FirstLook.Common.Core.Collections;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.Builders
{
    internal class CategoryHeaderListBuilder
    {
        private readonly List<CategoryHeaderDto> _headers = new List<CategoryHeaderDto>();

        public void Add(Category category)
        {
            CategoryHeaderDto header = GetCategoryHeader(category.Header);

            if (string.IsNullOrEmpty(category.GroupName))
            {
                AppendToCollection(header, category);
            }
            else
            {
                AppendToSet(header, category);
            }
        }

        private CategoryHeaderDto GetCategoryHeader(IHeader header)
        {
            foreach (CategoryHeaderDto item in _headers)
            {
                if (item.Id == header.Id)
                {
                    return item;
                }
            }

            CategoryHeaderDto newHeader = new CategoryHeaderDto();

            newHeader.Id = header.Id;

            newHeader.Name = header.Name;

            _headers.Add(newHeader);

            return newHeader;
        }

        private void AppendToCollection(CategoryHeaderDto header, Category category)
        {
            foreach (CategoryCollectionDto collection in header.Categories)
            {
                if (string.IsNullOrEmpty(collection.GroupName))
                {
                    foreach (CategoryDto info in collection.Items)
                    {
                        if (info.Id == category.Id)
                        {
                            return;
                        }
                    }

                    collection.Add(Mapper.ToInfo(category));

                    return;
                }
            }

            CategoryCollectionDto newCollection = new CategoryCollectionDto();

            newCollection.Add(Mapper.ToInfo(category));

            header.Categories.Add(newCollection);
        }

        private void AppendToSet(CategoryHeaderDto header, Category category)
        {
            foreach (CategoryCollectionDto collection in header.Categories)
            {
                if (!string.IsNullOrEmpty(collection.GroupName))
                {
                    if (collection.GroupName.Equals(category.GroupName))
                    {
                        foreach (CategoryDto info in collection.Items)
                        {
                            if (info.Id == category.Id)
                            {
                                return;
                            }
                        }

                        collection.Add(Mapper.ToInfo(category));

                        return;
                    }
                }
            }

            CategoryCollectionDto newCollection = new CategoryCollectionDto();

            newCollection.GroupName = category.GroupName;

            newCollection.Add(Mapper.ToInfo(category));

            header.Categories.Add(newCollection);
        }

        public void Apply(ICategoryAction action)
        {
            foreach (CategoryHeaderDto header in _headers)
            {
                foreach (CategoryCollectionDto collection in header.Categories)
                {
                    CategoryDto info = collection.Apply(action);

                    if (info != null)
                    {
                        break;
                    }
                }
            }
        }

        public void CopyTo(IRandomAccess<CategoryHeaderDto> categoryHeaders)
        {
            foreach (CategoryHeaderDto header in _headers)
            {
                categoryHeaders.Add(header);
            }
        }
    }
}