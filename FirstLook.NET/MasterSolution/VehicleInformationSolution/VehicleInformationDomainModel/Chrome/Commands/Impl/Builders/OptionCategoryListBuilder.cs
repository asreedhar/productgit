using System.Collections.Generic;
using FirstLook.Common.Core.Collections;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration;
using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.Builders
{
    internal class OptionCategoryListBuilder
    {
        private readonly List<OptionSelectionDto> _selections = new List<OptionSelectionDto>();

        private void AppendSelection(OptionDto option, bool automatic)
        {
            OptionSelectionDto selection = new OptionSelectionDto();

            selection.Option = option;

            selection.Automatic = automatic;

            _selections.Add(selection);
        }

        private readonly List<RequirementDto> _requirements = new List<RequirementDto>();

        public void AddRequirement(OptionKind optionKind)
        {
            RequirementDto requirement = new RequirementDto();
            requirement.Id = optionKind.Id;
            requirement.Name = optionKind.Name;
            requirement.Status = RequirementStatusDto.NotSatisfied;
            _requirements.Add(requirement);
        }

        private readonly List<OptionCategoryDto> _categories = new List<OptionCategoryDto>();

        public void Add(Option option)
        {
            OptionCategoryDto category = IsPackage(option)
                                          ? GetPackageHeader()
                                          : GetHeader(option.Header, option.GroupName);

            if (string.IsNullOrEmpty(option.GroupName) || "0".Equals(option.GroupName))
            {
                AppendToCollection(option, category);
            }
            else
            {
                AppendToSet(option, category);
            }
        }

        private bool IsPackage(Option option)
        {
            bool isPackage = (option.OptionKind.Id == 21);

            if (!isPackage)
            {
                bool onlyPackageItems = true;

                foreach (OptionAction action in option.Actions)
                {
                    onlyPackageItems &= action.Action == Action.Package;
                }

                isPackage = onlyPackageItems && option.Actions.Count > 1;
            }

            if (!isPackage)
            {
                foreach (OptionCategoryDto category in _categories)
                {
                    if (category.Id == 1255)
                    {
                        foreach (OptionCollectionDto collection in category.Options)
                        {
                            if (!string.IsNullOrEmpty(collection.GroupName))
                            {
                                if (Equals(collection.GroupName, option.GroupName))
                                {
                                    isPackage = true;
                                }
                            }
                        }
                    }
                }
            }

            return isPackage;
        }

        private OptionCategoryDto GetPackageHeader()
        {
            foreach (OptionCategoryDto category in _categories)
            {
                if (category.Id == 1255) // yay magic numbers from chrome !!!
                {
                    return category;
                }
            }

            OptionCategoryDto newCategory = new OptionCategoryDto();

            newCategory.Id = 1255;

            newCategory.Name = "PACKAGE";

            _categories.Add(newCategory);

            return newCategory;
        }

        private OptionCategoryDto GetHeader(IHeader header, string groupName)
        {
            // go find the group: do not split radio groups up (app will select wrong things)

            foreach (OptionCategoryDto category in _categories)
            {
                foreach (OptionCollectionDto collection in category.Options)
                {
                    if (!string.IsNullOrEmpty(collection.GroupName))
                    {
                        if (Equals(collection.GroupName, groupName))
                        {
                            return category;
                        }
                    }
                }
            }

            // no group, see if the category already exists

            foreach (OptionCategoryDto category in _categories)
            {
                if (category.Name.Equals(header.Name)) // why not id?
                {
                    return category;
                }
            }

            // nope, let's build a new category

            OptionCategoryDto newCategory = new OptionCategoryDto();

            newCategory.Id = header.Id;

            newCategory.Name = header.Name;

            _categories.Add(newCategory);

            return newCategory;
        }

        private void AppendToCollection(Option option, OptionCategoryDto category)
        {
            foreach (OptionCollectionDto collection in category.Options)
            {
                if (string.IsNullOrEmpty(collection.GroupName))
                {
                    collection.Add(Mapper.ToInfo(option));

                    return;
                }
            }

            OptionCollectionDto newCollection = new OptionCollectionDto();

            newCollection.Add(Mapper.ToInfo(option));

            category.Options.Add(newCollection);
        }

        private void AppendToSet(Option option, OptionCategoryDto category)
        {
            foreach (OptionCollectionDto collection in category.Options)
            {
                if (!string.IsNullOrEmpty(collection.GroupName))
                {
                    if (collection.GroupName.Equals(option.GroupName))
                    {
                        collection.Add(Mapper.ToInfo(option));

                        return;
                    }
                }
            }

            OptionCollectionDto newCollection = new OptionCollectionDto();

            newCollection.GroupName = option.GroupName;

            if (option.OptionKind != null && option.OptionKind.Id != 0)
            {
                newCollection.Requirement.Id = option.OptionKind.Id;

                newCollection.Requirement.Name = option.OptionKind.Name;

                if (_requirements.Exists(delegate(RequirementDto info) { return info.Id == option.OptionKind.Id; }))
                {
                    newCollection.Requirement.Required = true;
                }
            }
            
            newCollection.Add(Mapper.ToInfo(option));

            category.Options.Add(newCollection);
        }

        public void CopyTo(IRandomAccess<OptionCategoryDto> destination)
        {
            foreach (OptionCategoryDto category in _categories)
            {
                destination.Add(category);
            }
        }

        public void CopyTo(IRandomAccess<OptionSelectionDto> destination)
        {
            foreach (OptionSelectionDto selection in _selections)
            {
                destination.Add(selection);
            }
        }

        public void Select(OptionEncodings encodings, ConfigurationEngineState state)
        {
            foreach (OptionEncoding optionSelection in state.OptionSelections)
            {
                OptionDto option = Find(optionSelection.OptionCode);

                if (option == null)
                {
                    continue; // fleet vehicle
                }

                AppendSelection(option, false);

                // TODO: Selection Consequences
            }

            for (int i = 0, l = state.Count; i < l; i++)
            {
                OptionDto option = Find(encodings[i].OptionCode);

                if (option == null)
                {
                    continue; // fleet vehicle
                }

                OptionState optionState = state[i];

                if (OptionStateHelper.IsSet(optionState, OptionState.Exclude))
                {
                    option.Selected = false;

                    option.Enabled = OptionStateHelper.IsSet(optionState, OptionState.OrInclude, OptionState.OrRequire);
                }
                else
                {
                    if (OptionStateHelper.IsSet(optionState, OptionState.On))
                    {
                        option.Selected = true;
                    }

                    if (OptionStateHelper.IsSet(optionState, OptionState.Include, OptionState.Require))
                    {
                        option.Selected = true;

                        option.Enabled = false;
                    }
                }
            }
        }

        OptionDto Find(string code)
        {
            foreach (OptionCategoryDto category in _categories)
            {
                foreach (OptionCollectionDto collection in category.Options)
                {
                    OptionDto option = collection.Find(code);

                    if (option != null)
                    {
                        return option;
                    }
                }
            }

            return null;
        }

        public void PriceSelections(IRandomAccess<Option> options, IRandomAccess<Price> prices)
        {
            IRandomAccess<Option> selections = GetSelections(options);

            foreach (OptionCategoryDto category in _categories)
            {
                foreach (OptionCollectionDto collection in category.Options)
                {
                    foreach (OptionDto option in collection.Items)
                    {
                        foreach (Price price in prices)
                        {
                            if (Equals(price.Option.Code, option.Code))
                            {
                                if (price.Condition == null)
                                {
                                    option.Invoice = price.Invoice;
                                    option.Msrp = price.Msrp;
                                    break;
                                }

                                if (price.Condition.Evaluate(selections))
                                {
                                    option.Invoice = price.Invoice;
                                    option.Msrp = price.Msrp;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        private RandomAccess<Option> GetSelections(IEnumerable<Option> options)
        {
            RandomAccess<Option> selections = new RandomAccess<Option>();

            foreach (OptionSelectionDto selection in _selections)
            {
                AppendSelection(options, selections, selection.Option.Code);

                foreach (OptionSelectionConsequenceDto consequence in selection.Consequences)
                {
                    if (consequence.Effect == OptionSelectionEffectDto.Add)
                    {
                        AppendSelection(options,selections, consequence.Option.Code);
                    }
                    else if (consequence.Effect == OptionSelectionEffectDto.Remove)
                    {
                        foreach (Option option in selections)
                        {
                            if (Equals(option.Code, consequence.Option.Code))
                            {
                                // selections.Remove(option);
                            }
                        }
                    }
                }
            }

            return selections;
        }

        private static void AppendSelection(IEnumerable<Option> options, IRandomAccess<Option> selections, string optionCode)
        {
            foreach (Option option in options)
            {
                if (Equals(option.Code, optionCode))
                {
                    selections.Add(option);

                    break;
                }
            }
        }
    }
}