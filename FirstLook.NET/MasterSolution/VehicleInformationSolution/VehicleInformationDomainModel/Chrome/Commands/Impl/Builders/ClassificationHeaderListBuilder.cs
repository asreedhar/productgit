using System.Collections.Generic;
using FirstLook.Common.Core.Collections;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration;
using FirstLook.VehicleInformation.DomainModel.Customization.Commands;
using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.Builders
{
    internal class ClassificationHeaderListBuilder
    {
        private readonly List<ClassificationHeaderDto> _classifications = new List<ClassificationHeaderDto>();

        private ICustomizeClassification _categoryHeaderCustomization;
        private ICustomizeClassification _optionHeaderCustomization;
        private ICustomizeClassification _standardHeaderCustomization;
        private ICustomizeCategoryInfo _categoryCustomization;
        private ICustomizeOptionInfo _optionCustomization;
        private ICustomizeStandardInfo _standardCustomization;

        public ICustomizeClassification CategoryHeaderCustomization
        {
            get { return _categoryHeaderCustomization; }
            set { _categoryHeaderCustomization = value; }
        }

        public ICustomizeClassification OptionHeaderCustomization
        {
            get { return _optionHeaderCustomization; }
            set { _optionHeaderCustomization = value; }
        }

        public ICustomizeClassification StandardHeaderCustomization
        {
            get { return _standardHeaderCustomization; }
            set { _standardHeaderCustomization = value; }
        }

        public ICustomizeCategoryInfo CategoryCustomization
        {
            get { return _categoryCustomization; }
            set { _categoryCustomization = value; }
        }

        public ICustomizeOptionInfo OptionCustomization
        {
            get { return _optionCustomization; }
            set { _optionCustomization = value; }
        }

        public ICustomizeStandardInfo StandardCustomization
        {
            get { return _standardCustomization; }
            set { _standardCustomization = value; }
        }

        public void Add(Option option)
        {
            IHeader header = option.Header;

            int i = 0;

            foreach (OptionAction action in option.Actions)
            {
                if (action.Action == Action.Add || action.Action == Action.Package)
                {
                    if (i++ == 0)
                    {
                        header = action.Category.Header;
                    }
                }

                Add(action.Category);
            }

            if (IsPackage(option))
            {
                header = new OptionHeader("PACKAGE", 1255);
            }

            ClassificationHeaderDto classification = GetHeader(header, option.GroupName, OptionHeaderCustomization);

            if (classification.Position == 0)
            {
                classification.Position = int.MinValue; // START OF LIST
            }

            if (string.IsNullOrEmpty(option.GroupName) || "0".Equals(option.GroupName))
            {
                AppendToCollection(classification.Options, option);
            }
            else
            {
                AppendToSet(classification.Options, option);
            }
        }

        public void Add(Standard standard)
        {
            IHeader header = standard.Header;

            int i = 0;

            foreach (StandardAction action in standard.Actions)
            {
                if (action.Action == Action.Add || action.Action == Action.Package)
                {
                    if (i++ == 0)
                    {
                        header = action.Category.Header;
                    }
                }

                Add(action.Category);
            }

            ClassificationHeaderDto classification = GetHeader(header, string.Empty, StandardHeaderCustomization);
            
            if (classification.Position == 0)
            {
                classification.Position = int.MaxValue; // END OF LIST
            }

            AppendToCollection(classification.Standards, standard);
        }

        public void Add(Category category)
        {
            IHeader header = category.Header;

            ClassificationHeaderDto classification = GetHeader(header, category.GroupName, CategoryHeaderCustomization);

            if (string.IsNullOrEmpty(category.GroupName))
            {
                AppendToCollection(classification.Categories, category);
            }
            else
            {
                AppendToSet(classification.Categories, category);
            }
        }

        public void Select(BuildEngine buildEngine, BuildEngineState buildEngineState)
        {
            // options

            ConfigurationEngineState state = buildEngineState.EngineState;

            for (int i = 0, l = state.Count; i < l; i++)
            {
                OptionDto option = FindOption(buildEngine.Options[i].OptionCode);

                if (option == null)
                {
                    continue; // fleet vehicle
                }

                OptionState optionState = state[i];

                if (OptionStateHelper.IsSet(optionState, OptionState.Exclude))
                {
                    option.Selected = false;

                    option.Enabled = OptionStateHelper.IsSet(optionState, OptionState.OrInclude, OptionState.OrRequire);
                }
                else
                {
                    if (OptionStateHelper.IsSet(optionState, OptionState.On))
                    {
                        option.Selected = true;
                    }

                    if (OptionStateHelper.IsSet(optionState, OptionState.Include, OptionState.Require))
                    {
                        option.Selected = true;

                        option.Enabled = false;
                    }
                }
            }

            // standards

            for (int i = 0, l = buildEngineState.StandardStates.Count; i < l; i++)
            {
                StandardDto standard = FindStandard(buildEngine.Standards[i].Sequence);

                if (standard == null)
                {
                    continue; // wtf?
                }

                StandardEncoding encoding = buildEngine.Standards.Find(standard.Sequence);

                Action action = buildEngineState.StandardStates[encoding.Index];

                standard.Enabled = (action == Action.Add || action == Action.Package);
            }

            // categories

            for (int i = 0, l = buildEngineState.CategoryStates.Count; i < l; i++)
            {
                CategoryDto category = FindCategory(buildEngine.Categories[i].Id);

                if (category == null)
                {
                    continue; // wtf?
                }

                CategoryEncoding encoding = buildEngine.Categories.Find(category.Id);

                Action action = buildEngineState.CategoryStates[encoding.Index];

                if (action == Action.Add || action == Action.Package)
                {
                    category.Selected = true;
                }
                else
                {
                    category.Selected = false;
                }

                if (action == Action.Package || action == Action.Remove)
                {
                    category.Enabled = false;
                }
            }
        }

        public void CopyTo(IRandomAccess<ClassificationHeaderDto> classificationHeaders)
        {
            foreach (ClassificationHeaderDto header in _classifications)
            {
                if (header != null)
                {
                    classificationHeaders.Add(header);
                }
            }
        }

        #region Implementation

        static bool IsPackage(Option option)
        {
            bool isPackage = (option.OptionKind.Id == 21);

            if (!isPackage)
            {
                bool onlyPackageItems = true;

                foreach (OptionAction action in option.Actions)
                {
                    onlyPackageItems &= action.Action == Action.Package;
                }

                isPackage = onlyPackageItems && option.Actions.Count > 1;
            }

            return isPackage;
        }

        OptionDto FindOption(string code)
        {
            foreach (ClassificationHeaderDto classification in _classifications)
            {
                foreach (OptionCollectionDto collection in classification.Options)
                {
                    OptionDto option = collection.Find(code);

                    if (option != null)
                    {
                        return option;
                    }
                }
            }

            return null;
        }

        StandardDto FindStandard(int sequence)
        {
            foreach (ClassificationHeaderDto classification in _classifications)
            {
                foreach (StandardDto standard in classification.Standards)
                {
                    if (standard.Sequence == sequence)
                    {
                        return standard;
                    }
                }
            }

            return null;
        }

        CategoryDto FindCategory(int id)
        {
            foreach (ClassificationHeaderDto classification in _classifications)
            {
                foreach (CategoryCollectionDto collection in classification.Categories)
                {
                    foreach (CategoryDto category in collection.Items)
                    {
                        if (category.Id == id)
                        {
                            return category;
                        }
                    }
                }
            }

            return null;
        }

        ClassificationHeaderDto GetHeader(IHeader header, string groupName, ICustomizeClassification customization)
        {
            // go find the group: do not split radio groups up (app will select wrong things)

            foreach (ClassificationHeaderDto classification in _classifications)
            {
                foreach (OptionCollectionDto options in classification.Options)
                {
                    if (!string.IsNullOrEmpty(options.GroupName))
                    {
                        if (Equals(options.GroupName, groupName))
                        {
                            return classification;
                        }
                    }
                }

                foreach (CategoryCollectionDto category in classification.Categories)
                {
                    if (!string.IsNullOrEmpty(category.GroupName))
                    {
                        if (Equals(category.GroupName, groupName))
                        {
                            return classification;
                        }
                    }
                }
            }

            // no group, perhaps header is a child of an existing classification

            MergeInfo info = new MergeInfo(header);

            foreach (ClassificationHeaderDto classification in _classifications)
            {
                if (classification.MergeInfo.ParentOf(info))
                {
                    return classification;
                }
            }

            // no group, see if the classification already exists

            foreach (ClassificationHeaderDto classification in _classifications)
            {
                if (classification.MergeInfo.Self.Equals(header))
                {
                    return classification;
                }
            }

            // nope, let's build a new category

            ClassificationHeaderDto newClassification = new ClassificationHeaderDto();

            newClassification.Id = header.Id;

            newClassification.Name = header.Name;

            newClassification.MergeInfo.Self = header;

            customization.Apply(header, newClassification);

            _classifications.Add(newClassification);

            // see if the classification is the parent of an existing entry

            for (int i = 0; i < _classifications.Count; i++)
            {
                ClassificationHeaderDto classification = _classifications[i];

                if (newClassification.MergeInfo.ParentOf(classification.MergeInfo))
                {
                    foreach (CategoryCollectionDto category in classification.Categories)
                    {
                        newClassification.Categories.Add(category);
                    }

                    foreach (OptionCollectionDto option in classification.Options)
                    {
                        newClassification.Options.Add(option);
                    }

                    foreach (StandardDto standard in classification.Standards)
                    {
                        newClassification.Standards.Add(standard);
                    }

                    _classifications.RemoveAt(i--);
                }
            }

            // done !

            return newClassification;
        }

        void AppendToCollection(IRandomAccess<OptionCollectionDto> options, Option option)
        {
            OptionDto info = Mapper.ToInfo(option);

            OptionCustomization.Apply(option, info);

            foreach (OptionCollectionDto collection in options)
            {
                if (string.IsNullOrEmpty(collection.GroupName))
                {
                    collection.Add(info);

                    return;
                }
            }

            OptionCollectionDto newCollection = new OptionCollectionDto();

            newCollection.Add(info);

            options.Add(newCollection);
        }

        void AppendToSet(IRandomAccess<OptionCollectionDto> options, Option option)
        {
            OptionDto info = Mapper.ToInfo(option);

            OptionCustomization.Apply(option, info);

            foreach (OptionCollectionDto collection in options)
            {
                if (!string.IsNullOrEmpty(collection.GroupName))
                {
                    if (collection.GroupName.Equals(option.GroupName))
                    {
                        collection.Add(info);

                        return;
                    }
                }
            }

            OptionCollectionDto newCollection = new OptionCollectionDto();

            newCollection.GroupName = option.GroupName;

            if (option.OptionKind != null && option.OptionKind.Id != 0)
            {
                newCollection.Requirement.Id = option.OptionKind.Id;

                newCollection.Requirement.Name = option.OptionKind.Name;

                //if (_requirements.Exists(delegate(RequirementInfo info) { return info.Id == option.OptionKind.Id; }))
                //{
                //    newCollection.Required = true;
                //}
            }

            newCollection.Add(info);

            options.Add(newCollection);
        }

        void AppendToCollection(IRandomAccess<CategoryCollectionDto> categories, Category category)
        {
            CategoryDto newInfo = Mapper.ToInfo(category);

            CategoryCustomization.Apply(category, newInfo);

            foreach (CategoryCollectionDto collection in categories)
            {
                if (string.IsNullOrEmpty(collection.GroupName))
                {
                    foreach (CategoryDto info in collection.Items)
                    {
                        if (info.Id == category.Id)
                        {
                            return;
                        }
                    }

                    collection.Add(newInfo);

                    return;
                }
            }

            CategoryCollectionDto newCollection = new CategoryCollectionDto();

            newCollection.Add(newInfo);

            categories.Add(newCollection);
        }

        static void AppendToSet(IRandomAccess<CategoryCollectionDto> categories, Category category)
        {
            foreach (CategoryCollectionDto collection in categories)
            {
                if (!string.IsNullOrEmpty(collection.GroupName))
                {
                    if (collection.GroupName.Equals(category.GroupName))
                    {
                        foreach (CategoryDto info in collection.Items)
                        {
                            if (info.Id == category.Id)
                            {
                                return;
                            }
                        }

                        collection.Add(Mapper.ToInfo(category));

                        return;
                    }
                }
            }

            CategoryCollectionDto newCollection = new CategoryCollectionDto();

            newCollection.GroupName = category.GroupName;

            newCollection.Add(Mapper.ToInfo(category));

            categories.Add(newCollection);
        }

        void AppendToCollection(IRandomAccess<StandardDto> standards, Standard standard)
        {
            StandardDto info = Mapper.ToInfo(standard);

            _standardCustomization.Apply(standard, info);

            standards.Add(info);
        }

        #endregion
    }
}