using System;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.Builders
{
    internal static class Mapper
    {
        public static OptionDto ToInfo(Option option)
        {
            OptionDto info = new OptionDto();
            info.Code = option.Code;
            info.Description = option.Description;
            info.Name = option.Name;
            info.Selected = false;
            info.Enabled = true;

            if (option.OptionKind != null)
            {
                info.RequirementId = option.OptionKind.Id;
                info.RequirementName = option.OptionKind.Name;
            }

            return info;
        }

        public static CategoryDto ToInfo(Category category)
        {
            CategoryDto info = new CategoryDto();
            info.Id = category.Id;
            info.Name = category.Name;
            return info;
        }

        public static StandardDto ToInfo(Standard standard)
        {
            StandardDto info = new StandardDto();
            info.Name = standard.Name;
            info.Sequence = standard.Sequence;
            return info;
        }

        public static ColorComponentDto ToInfo(ColorComponent component)
        {
            ColorComponentDto info = new ColorComponentDto();
            info.Code = String.IsNullOrEmpty(component.ManufacturerCode)
                            ? component.Code
                            : component.ManufacturerCode;
            info.RgbHex = component.RgbHex;
            info.Description = component.Description;
            info.GenericDescription = component.GenericDescription;
            return info;
        }
    }
}