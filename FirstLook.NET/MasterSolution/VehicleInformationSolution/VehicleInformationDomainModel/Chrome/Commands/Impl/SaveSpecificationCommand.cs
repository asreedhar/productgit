using System;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration.Serialization;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects.Vehicle;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Specification.Commands;
using Action=FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Action;
using Style=FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Style;
using StyleDto=FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects.StyleDto;
using VehicleDescription=
    FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects.Vehicle.VehicleDescription;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.Impl
{
    public class SaveSpecificationCommand : ICommand<NullDto, IdentityContextDto<SaveSpecificationArgumentsDto>>
    {
        public NullDto Execute(IdentityContextDto<SaveSpecificationArgumentsDto> parameters)
        {
            SaveSpecificationArgumentsDto arguments = parameters.Arguments;

            IBroker broker = Broker.Get(parameters.Identity.Name, parameters.Identity.AuthorityName, arguments.Broker.Handle);

            IResolver resolver = RegistryFactory.GetResolver();

            IVehicleRepository vehicleRepository = resolver.Resolve<IVehicleRepository>();

            VehicleIdentification vehicle = vehicleRepository.Identification(arguments.Vehicle.Vin);

            INewVehicleDataRepository newVehicleDataRepository = resolver.Resolve<INewVehicleDataRepository>();

            VehicleBuildDto vehicleBuild = arguments.VehicleBuild;

            int styleId = vehicleBuild.Prototype.StyleId;

            Style style = newVehicleDataRepository.Fetch(styleId);

            StyleSnapshot snapshot = newVehicleDataRepository.Freeze(styleId);

            BuildEngine buildEngine = new BuildEngine(style.Standards, style.Options, style.OrderRules);

            BuildEngineState buildEngineState;

            if (!BuildEngineState.TryParse(vehicleBuild.State, buildEngine, out buildEngineState))
            {
                throw new Exception("there's no state when she's gone ...");
            }

            ISpecificationCommandFactory factory = resolver.Resolve<ISpecificationCommandFactory>();

            ICommand<SpecificationDto, IdentityContextDto<SpecificationDto>> command = factory.CreateSpecificationSaveCommand();

            SpecificationDto specification = Specification(
                broker,
                vehicle,
                style,
                snapshot,
                buildEngine,
                buildEngineState);

            command.Execute(
                new IdentityContextDto<SpecificationDto>
                {
                    Identity = parameters.Identity,
                    Arguments = specification
                });

            return new NullDto();
        }

        private SpecificationDto Specification(
            IBroker broker,
            VehicleIdentification vehicle,
            Style style,
            StyleSnapshot snapshot,
            BuildEngine engine,
            BuildEngineState state)
        {
            SpecificationDto specification = new SpecificationDto
            {
                Dealer = new BrokerIdentificationDto { Id = broker.Id, Handle = broker.Handle },
                Vehicle = new VehicleIdentificationDto { Vin = vehicle.Vin, Id = vehicle.Id },
                BuildSpecification = Serializer.Serialize(vehicle.Vin, style.Id, 1, engine, state),
                VehicleSpecification = VehicleSpecification(vehicle.Vin, style, engine, state),
                Style = new StyleDto
                {
                    Document = new StyleProviderDocumentDto
                    {
                        Document = snapshot.Style,
                        Version = snapshot.StyleVersion
                    },
                    Reference = new StyleProviderDocumentDto
                    {
                        Document = snapshot.Reference,
                        Version = snapshot.ReferenceVersion
                    },
                    Summary = new StyleSummaryDto
                    {
                        BodyStyleName = style.BodyStyles[0].Name,
                        DivisionName = style.Model.Subdivision.Division.Name,
                        ManufacturerName = style.Model.Subdivision.Division.Manufacturer.Name,
                        ModelName = style.Model.Name,
                        ModelYear = style.Model.ModelYear,
                        StyleName = style.NameWithoutTrim,
                        SubdivisionName = style.Model.Subdivision.Name,
                        TrimName = style.Trim
                    }
                }
            };

            StyleIdentityChromeDto identity = new StyleIdentityChromeDto
            {
                ProviderId = 1,
                ProviderName = "Chrome",
                Id = style.Id
            };

            specification.Style.Identity = identity;

            return specification;
        }

        protected VehicleSpecification VehicleSpecification(
            string vin,
            Style style,
            BuildEngine engine,
            BuildEngineState state)
        {
            VehicleSpecification specification = new VehicleSpecification
            {
                Vin = vin,
                Version = 1.0m,
                IsFleet = false,
                Description = new VehicleDescription
                {
                    BodyStyle = "Wibble",
                    DriveTrain = new VehicleDriveTrain
                    {
                        DriveTrainAxle = VehicleDriveTrainAxle.Front,
                        DriveTrainType = VehicleDriveTrainType.TwoWheelDrive,
                        Name = "Wobble"
                    },
                    Engine = new VehicleEngine
                    {
                        Capacity = 4,
                        CylinderCount = 8,
                        CylinderLayout = VehicleEngineCylinderLayout.Flat,
                        EngineType = VehicleEngineType.Piston,
                        Name = "Flibble"
                    },
                    Model = new VehicleModel
                    {
                        Division = "D",
                        Manufacturer = "M",
                        Model = "O",
                        ModelYear = 2010,
                        Subdivision = "S"
                    },
                    PassengerCapacity = 8,
                    PassengerDoors = 4,
                    StyleCode = style.FullCode,
                    StyleName = style.Name,
                    Transmission = new VehicleTransmission
                    {
                        GearCount = 8,
                        Name = "Automatic 8 Speed",
                        TransmissionType = VehicleTransmissionType.Automatic
                    },
                    Trim = style.Trim
                },
                Options = new VehicleOptionCollection()
            };

            for (int i = 0, l = state.EngineState.Count; i < l; i++)
            {
                OptionState optionState = state.EngineState[i];

                if (OptionStateHelper.IsSet(optionState, OptionState.Exclude))
                {
                    continue;
                }

                if (optionState == OptionState.Off)
                {
                    continue;
                }

                OptionEncoding encoding = engine.Options[i];

                foreach (Option option in style.Options)
                {
                    if (Equals(option.Code, encoding.OptionCode))
                    {
                        VehicleOption item = new VehicleOption
                        {
                            OptionCode = option.Code,
                            Name = option.Name,
                            SupplementalDescription = option.Description
                        };

                        foreach (ICategoryAction action in option.Actions)
                        {
                            if (action.Action == Action.Remove)
                            {
                                continue;
                            }

                            if (item.Features == null)
                            {
                                item.Features = new Int32Collection();
                            }

                            item.Features.Add(action.Category.Id);
                        }

                        specification.Options.Add(item);

                        break;
                    }
                }
            }

            specification.Standards = new VehicleStandardCollection();

            for (int i = 0, l = state.StandardStates.Count; i < l; i++)
            {
                if (state.StandardStates[i] == Action.Remove)
                {
                    continue;
                }

                StandardEncoding encoding = engine.Standards[i];

                foreach (Standard standard in style.Standards)
                {
                    if (standard.Sequence == encoding.Sequence)
                    {
                        VehicleStandard item = new VehicleStandard {Name = standard.Name};

                        foreach (ICategoryAction action in standard.Actions)
                        {
                            if (action.Action == Action.Remove)
                            {
                                continue;
                            }

                            if (item.Features == null)
                            {
                                item.Features = new Int32Collection();
                            }

                            item.Features.Add(action.Category.Id);
                        }

                        specification.Standards.Add(item);

                        break;
                    }
                }
            }

            return specification;
        }
    }
}