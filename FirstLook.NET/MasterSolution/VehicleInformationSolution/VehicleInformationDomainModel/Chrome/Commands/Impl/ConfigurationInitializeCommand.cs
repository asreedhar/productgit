using System;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.Impl
{
    public class ConfigurationInitializeCommand : ICommand<ConfigurationInitializeResultsDto, ConfigurationInitializeArgumentsDto>
    {
        public ConfigurationInitializeResultsDto Execute(ConfigurationInitializeArgumentsDto parameters)
        {
            INewVehicleDataRepository repository = RegistryFactory.GetRegistry().Resolve<INewVehicleDataRepository>();

            Style style = repository.Fetch(parameters.VehiclePrototype.StyleId);

            if (style == null)
            {
                throw new Exception("fuckity fuck fuck");
            }

            BuildEngine buildEngine = new BuildEngine(style.Standards, style.Options, style.OrderRules);

            BuildEngineState state = buildEngine.Start();

            foreach (OptionEncoding selection in buildEngine.OptionGroups.DefaultSelections())
            {
                if (Option.IsAllowed(selection.Availability, parameters.VehiclePrototype.IsFleet))
                {
                    state = buildEngine.Transition(state, selection, OptionState.On);
                }
            }

            return new ConfigurationInitializeResultsDto {State = state.ToString()};
        }
    }
}
