using System;
using FirstLook.Common.Core.Collections;
using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Configuration;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.Impl
{
    public class ColorOptionsCommand : ICommand<ColorOptionsResultsDto, ColorOptionsArgumentsDto>
    {
        public ColorOptionsResultsDto Execute(ColorOptionsArgumentsDto parameters)
        {
            INewVehicleDataRepository repository = RegistryFactory.GetRegistry().Resolve<INewVehicleDataRepository>();

            Style style = repository.Fetch(parameters.StyleId);

            if (style == null)
            {
                throw new Exception("fuckity fuck fuck");
            }

            // create object

            RandomAccess<OptionCollectionDto> result = new RandomAccess<OptionCollectionDto>();

            // add the options

            foreach (Color color in style.Colors)
            {
                bool match = true;

                if (color.ExteriorOne != null)
                {
                    match &= (Equals(GetCode(color.ExteriorOne), parameters.ExteriorCodeOne));
                }

                if (color.ExteriorTwo != null)
                {
                    match &= (Equals(GetCode(color.ExteriorTwo), parameters.ExteriorCodeTwo));
                }

                if (color.Interior != null)
                {
                    match &= (Equals(GetCode(color.Interior), parameters.InteriorCode));
                }

                if (match)
                {
                    OptionSelectionSolutionBuilder builder = new OptionSelectionSolutionBuilder();

                    IRandomAccess<IRandomAccess<OptionSelection>> solutions = builder.Process(style.Options, color.Condition);

                    foreach (IRandomAccess<OptionSelection> solution in solutions)
                    {
                        OptionCollectionDto collection = new OptionCollectionDto();

                        bool skipSolution = false;

                        foreach (OptionSelection selection in solution)
                        {
                            foreach (Option option in style.Options)
                            {
                                if (Equals(option.Code, selection.OptionCode))
                                {
                                    if (!option.IsAllowed(parameters.IsFleet))
                                    {
                                        if (selection.Selected)
                                        {
                                            skipSolution = true;

                                            break;
                                        }
                                    }

                                    OptionDto info = new OptionDto
                                    {
                                        Code = option.Code,
                                        Description = option.Description,
                                        Name = option.Name
                                    };

                                    if (option.OptionKind != null)
                                    {
                                        info.RequirementId = option.OptionKind.Id;
                                        info.RequirementName = option.OptionKind.Name;
                                    }

                                    info.Selected = selection.Selected;
                                    
                                    collection.Add(info);

                                    break;
                                }
                            }

                            if (skipSolution) break;
                        }

                        if (!skipSolution && !Contains(result, collection))
                            result.Add(collection);
                    }
                }
            }

            return new ColorOptionsResultsDto{Result = result};
        }

        public bool Contains(IRandomAccess<OptionCollectionDto> list, OptionCollectionDto value)
        {
            foreach (OptionCollectionDto item in list)
            {
                if (item != null)
                {
                    if (item.Equals(value))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private static string GetCode(ColorComponent color)
        {
            return string.IsNullOrEmpty(color.ManufacturerCode)
                       ? color.Code
                       : color.ManufacturerCode;
        }
    }
}