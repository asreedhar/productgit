namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects
{
    public class OptionRequirementDto
    {
        private int _id;
        private string _name;
        private bool _required;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public bool Required
        {
            get { return _required; }
            set { _required = value; }
        }
    }
}
