using FirstLook.Common.Core.Collections;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects
{
    public class OptionCandidateDto
    {
        private string _state = string.Empty;
        private IRandomAccess<OptionCollectionDto> _candidate;

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }

        public IRandomAccess<OptionCollectionDto> Candidate
        {
            get { return _candidate; }
            set { _candidate = value; }
        }
    }
}
