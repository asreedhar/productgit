using FirstLook.Common.Core.Collections;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects
{
    public class CategoryHeaderDto
    {
        private int _id;
        private string _name;
        private readonly IRandomAccess<CategoryCollectionDto> _categories = new RandomAccess<CategoryCollectionDto>();

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public IRandomAccess<CategoryCollectionDto> Categories
        {
            get { return _categories; }
        }
    }
}