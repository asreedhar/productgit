using System.Collections.Generic;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects
{
    public class CategoryCollectionDto
    {
        private readonly IList<CategoryDto> _items = new List<CategoryDto>();

        private string _groupName = string.Empty;

        public string GroupName
        {
            get { return _groupName; }
            set { _groupName = value; }
        }

        public void Add(CategoryDto option)
        {
            _items.Add(option);
        }

        public int Count
        {
            get { return _items.Count; }
        }

        public CategoryDto this[int index]
        {
            get { return _items[index]; }
        }

        public IList<CategoryDto> Items
        {
            get { return _items; }
        }

        public CategoryDto Apply(ICategoryAction action)
        {
            foreach (CategoryDto item in _items)
            {
                if (item.Id == action.Category.Id)
                {
                    switch (action.Action)
                    {
                        case Action.Add:
                        case Action.Package:
                            item.Selected = true;
                            break;
                        case Action.Remove:
                            item.Selected = false;
                            break;
                    }

                    return item;
                }
            }

            return null;
        }
    }
}