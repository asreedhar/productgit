using System;
using System.Collections.Generic;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects
{
    public class OptionSelectionChangeDto
    {
        private readonly LogicOperator _logicOperator;
        private readonly IEnumerable<String> _optionCodes;

        public OptionSelectionChangeDto(LogicOperator logicOperator, IEnumerable<string> optionCodes)
        {
            _logicOperator = logicOperator;
            _optionCodes = optionCodes;
        }

        public LogicOperator LogicOperator
        {
            get { return _logicOperator; }
        }

        public IEnumerable<string> OptionCodes
        {
            get { return _optionCodes; }
        }
    }
}