using FirstLook.Common.Core.Collections;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects
{
    public class ConsumerInformationHeaderDto
    {
        private int _id;
        private string _name;
        private readonly RandomAccess<ConsumerInformationDto> _consumerInformation = new RandomAccess<ConsumerInformationDto>();

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public IRandomAccess<ConsumerInformationDto> ConsumerInformation {
            get { return _consumerInformation; }
        }
    }
}