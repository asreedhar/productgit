using System;
using FirstLook.Common.Core.Collections;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects
{
    internal class MergeInfo
    {
        private static readonly Predicate<MergeInfo> False = delegate { return false; };

        public MergeInfo()
        {
        }

        public MergeInfo(IHeader self)
        {
            _self = self;
        }

        private IHeader _self;
        private Predicate<MergeInfo> _childOf = False;
        private Predicate<MergeInfo> _parentOf = False;

        public IHeader Self
        {
            get { return _self; }
            set { _self = value; }
        }

        public Predicate<MergeInfo> ChildOf
        {
            get { return _childOf; }
            set { _childOf = value; }
        }

        public Predicate<MergeInfo> ParentOf
        {
            get { return _parentOf; }
            set { _parentOf = value; }
        }
    }

    public class ClassificationHeaderDto
    {
        private int _id;
        private string _name;
        private int _position;
        private bool _visible = true;
        private LocationDto _location;
        private readonly MergeInfo _mergeInfo = new MergeInfo();
        private readonly RandomAccess<CategoryCollectionDto> _categories = new RandomAccess<CategoryCollectionDto>();
        private readonly RandomAccess<StandardDto> _standards = new RandomAccess<StandardDto>();
        private readonly IRandomAccess<OptionCollectionDto> _options = new RandomAccess<OptionCollectionDto>();

        internal MergeInfo MergeInfo
        {
            get { return _mergeInfo; }
        }

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public LocationDto Location
        {
            get { return _location; }
            set { _location = value; }
        }

        public int Position
        {
            get { return _position; }
            set { _position = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public bool Visible
        {
            get { return _visible; }
            set { _visible = value; }
        }

        public IRandomAccess<CategoryCollectionDto> Categories
        {
            get { return _categories; }
        }

        public IRandomAccess<StandardDto> Standards
        {
            get { return _standards; }
        }

        public IRandomAccess<OptionCollectionDto> Options
        {
            get { return _options; }
        }
    }
}
