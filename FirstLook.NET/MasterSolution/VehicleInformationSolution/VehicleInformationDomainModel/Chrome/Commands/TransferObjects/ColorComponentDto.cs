using System;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects
{
    public class ColorComponentDto : IEquatable<ColorComponent>
    {
        private string _code;
        private string _description;
        private string _genericDescription;
        private string _rgbHex;

        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public string GenericDescription
        {
            get { return _genericDescription; }
            set { _genericDescription = value; }
        }

        public string RgbHex
        {
            get { return _rgbHex; }
            set { _rgbHex = value; }
        }

        public bool Equals(ColorComponent other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.Code, _code) && Equals(other.Description, _description) && Equals(other.GenericDescription, _genericDescription) && Equals(other.RgbHex, _rgbHex);
        }
    }
}