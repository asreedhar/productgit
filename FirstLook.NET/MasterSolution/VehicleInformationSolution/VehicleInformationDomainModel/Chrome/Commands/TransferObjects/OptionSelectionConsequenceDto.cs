namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects
{
    public class OptionSelectionConsequenceDto
    {
        private OptionDto _option;
        private OptionSelectionEffectDto _effect;

        public OptionDto Option
        {
            get { return _option; }
            set { _option = value; }
        }

        public OptionSelectionEffectDto Effect
        {
            get { return _effect; }
            set { _effect = value; }
        }
    }
}