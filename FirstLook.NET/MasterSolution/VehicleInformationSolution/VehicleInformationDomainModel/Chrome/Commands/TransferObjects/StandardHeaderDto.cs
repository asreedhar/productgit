using FirstLook.Common.Core.Collections;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects
{
    public class StandardHeaderDto
    {
        private int _id;
        private string _name;
        private readonly IRandomAccess<StandardDto> _standards = new RandomAccess<StandardDto>();

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public IRandomAccess<StandardDto> Standards
        {
            get { return _standards; }
        }
    }
}
