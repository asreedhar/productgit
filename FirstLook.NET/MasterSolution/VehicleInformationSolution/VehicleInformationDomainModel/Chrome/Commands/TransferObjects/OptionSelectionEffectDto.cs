namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects
{
    public enum OptionSelectionEffectDto
    {
        NotDefined,
        Add,
        Remove
    }
}