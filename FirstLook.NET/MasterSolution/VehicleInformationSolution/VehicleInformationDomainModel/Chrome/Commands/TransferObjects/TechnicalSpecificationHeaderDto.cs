using FirstLook.Common.Core.Collections;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects
{
    public class TechnicalSpecificationHeaderDto
    {
        private int _id;
        private string _name;
        private readonly IRandomAccess<TechnicalSpecificationDto> _technicalSpecifications = new RandomAccess<TechnicalSpecificationDto>();

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public IRandomAccess<TechnicalSpecificationDto> TechnicalSpecifications
        {
            get { return _technicalSpecifications; }
        }
    }
}