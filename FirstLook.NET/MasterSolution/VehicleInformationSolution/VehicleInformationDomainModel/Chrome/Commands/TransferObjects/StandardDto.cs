namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects
{
    public class StandardDto
    {
        private int _sequence;
        private string _name;
        private bool _enabled = true;

        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public int Sequence
        {
            get { return _sequence; }
            set { _sequence = value; }
        }
    }
}
