namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects
{
    public class TechnicalSpecificationDto
    {
        private int _id;
        private string _label;
        private string _value;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Label
        {
            get { return _label; }
            set { _label = value; }
        }

        public string Value
        {
            get { return _value; }
            set { this._value = value; }
        }
    }
}