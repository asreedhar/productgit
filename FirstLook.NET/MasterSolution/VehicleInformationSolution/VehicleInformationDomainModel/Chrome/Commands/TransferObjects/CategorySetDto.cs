using System.Collections;
using System.Collections.Generic;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects
{
    public class CategorySetDto : ICategoryCollection
    {
        private readonly IList<CategoryDto> _items = new List<CategoryDto>();

        private string _groupName;

        public string GroupName
        {
            get { return _groupName; }
            set { _groupName = value; }
        }

        /// 
        /// <param name="option"></param>
        public void Add(CategoryDto option)
        {
            _items.Add(option);
        }

        public int Count
        {
            get { return _items.Count; }
        }

        /// 
        /// <param name="index"></param>
        public CategoryDto Get(int index)
        {
            return _items[index];
        }

        public CategoryDto Apply(ICategoryAction action)
        {
            foreach (CategoryDto item in _items)
            {
                if (item.Id == action.Category.Id)
                {
                    switch (action.Action)
                    {
                        case Action.Add:
                        case Action.Package:
                            item.Selected = true;
                            break;
                        case Action.Remove:
                            item.Selected = false;
                            break;
                    }

                    return item;
                }
            }

            return null;
        }

        #region IEnumerable<OptionInfo> Members

        public IEnumerator<CategoryDto> GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}