using System;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects
{
    [Serializable]
    public class VehicleBuildDto
    {
        private VehiclePrototypeDto _prototype;
        private string _state;

        public VehiclePrototypeDto Prototype
        {
            get { return _prototype; }
            set { _prototype = value; }
        }

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }
    }
}
