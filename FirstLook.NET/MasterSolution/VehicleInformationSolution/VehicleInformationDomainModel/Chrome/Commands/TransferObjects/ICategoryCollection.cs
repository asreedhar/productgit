using System.Collections.Generic;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects
{
    public interface ICategoryCollection : IEnumerable<CategoryDto>
    {
        int Count { get; }

        void Add(CategoryDto category);

        CategoryDto Get(int index);

        CategoryDto Apply(ICategoryAction action);
    }
}