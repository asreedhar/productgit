namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects
{
    public enum LocationDto
    {
        NotDefined = 0,
        Vehicle = 1,
        Interior = 2,
        Exterior = 3
    }
}
