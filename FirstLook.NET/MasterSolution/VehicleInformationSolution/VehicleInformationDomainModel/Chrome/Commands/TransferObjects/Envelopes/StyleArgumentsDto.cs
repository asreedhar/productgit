﻿using System;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class StyleArgumentsDto
    {
        private VehicleBuildDto _vehicleBuild;
        private Guid _broker;
        private bool _hasBroker;

        public VehicleBuildDto VehicleBuild
        {
            get { return _vehicleBuild; }
            set { _vehicleBuild = value; }
        }

        public Guid Broker
        {
            get { return _broker; }
            set { _broker = value; }
        }

        public bool HasBroker
        {
            get { return _hasBroker; }
            set { _hasBroker = value; }
        }
    }
}
