﻿using System;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class CategoryIdentificationArgumentsDto
    {
        private VehicleBuildDto _vehicleBuild;
        private int _categoryId;
        private bool _addition;

        public VehicleBuildDto VehicleBuild
        {
            get { return _vehicleBuild; }
            set { _vehicleBuild = value; }
        }

        public int CategoryId
        {
            get { return _categoryId; }
            set { _categoryId = value; }
        }

        public bool Addition
        {
            get { return _addition; }
            set { _addition = value; }
        }
    }
}
