﻿using System;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class EncodingsArgumentsDto
    {
        private VehiclePrototypeDto _vehiclePrototype;

        public VehiclePrototypeDto VehiclePrototype
        {
            get { return _vehiclePrototype; }
            set { _vehiclePrototype = value; }
        }
    }
}
