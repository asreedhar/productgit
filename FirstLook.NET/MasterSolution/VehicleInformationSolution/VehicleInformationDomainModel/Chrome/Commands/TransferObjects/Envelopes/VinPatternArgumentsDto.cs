﻿using System;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class VinPatternArgumentsDto
    {
        private string _vinPattern;
        private string _country;

        public string VinPattern
        {
            get { return _vinPattern; }
            set { _vinPattern = value; }
        }

        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }
    }
}
