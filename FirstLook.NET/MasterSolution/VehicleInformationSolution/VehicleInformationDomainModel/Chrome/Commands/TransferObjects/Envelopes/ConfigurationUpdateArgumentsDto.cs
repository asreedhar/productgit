﻿using System;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ConfigurationUpdateArgumentsDto
    {
        private VehicleBuildDto _vehicleBuild;
        private string _optionCode;
        private bool _selected;

        public VehicleBuildDto VehicleBuild
        {
            get { return _vehicleBuild; }
            set { _vehicleBuild = value; }
        }

        public string OptionCode
        {
            get { return _optionCode; }
            set { _optionCode = value; }
        }

        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; }
        }
    }
}
