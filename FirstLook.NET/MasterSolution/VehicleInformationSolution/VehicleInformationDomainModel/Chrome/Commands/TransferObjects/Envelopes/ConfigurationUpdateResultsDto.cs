﻿using System;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ConfigurationUpdateResultsDto
    {
        private string _nextState;
        private OptionCollectionDto _choices = new OptionCollectionDto();

        public string NextState
        {
            get { return _nextState; }
            set { _nextState = value; }
        }

        public OptionCollectionDto Choices
        {
            get { return _choices; }
            set { _choices = value; }
        }
    }
}
