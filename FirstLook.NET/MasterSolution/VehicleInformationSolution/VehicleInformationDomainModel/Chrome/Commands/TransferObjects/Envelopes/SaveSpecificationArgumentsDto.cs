using System;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class SaveSpecificationArgumentsDto
    {
        private BrokerIdentificationDto _broker;
        private VehicleIdentificationDto _vehicle;
        private VehicleBuildDto _vehicleBuild;

        public BrokerIdentificationDto Broker
        {
            get { return _broker; }
            set { _broker = value; }
        }

        public VehicleIdentificationDto Vehicle
        {
            get { return _vehicle; }
            set { _vehicle = value; }
        }

        public VehicleBuildDto VehicleBuild
        {
            get { return _vehicleBuild; }
            set { _vehicleBuild = value; }
        }
    }
}
