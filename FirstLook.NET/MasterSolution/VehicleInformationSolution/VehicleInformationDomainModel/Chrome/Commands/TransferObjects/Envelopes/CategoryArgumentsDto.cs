﻿using System;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class CategoryArgumentsDto
    {
        private VehicleBuildDto _vehicleBuild;

        public VehicleBuildDto VehicleBuild
        {
            get { return _vehicleBuild; }
            set { _vehicleBuild = value; }
        }
    }
}
