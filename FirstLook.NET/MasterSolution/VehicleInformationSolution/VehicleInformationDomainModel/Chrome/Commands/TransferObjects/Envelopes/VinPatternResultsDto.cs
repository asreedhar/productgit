﻿using System;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class VinPatternResultsDto
    {
        private VinPatternDto _result;

        public VinPatternDto Result
        {
            get { return _result; }
            set { _result = value; }
        }
    }
}
