﻿using System;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class EncodingsResultsDto
    {
        private EncodingsDto _encodings;

        public EncodingsDto Encodings
        {
            get { return _encodings; }
            set { _encodings = value; }
        }
    }
}
