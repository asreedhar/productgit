﻿using System;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class CategoryIdentificationResultsDto
    {
        private OptionCollectionDto _candidates;

        public OptionCollectionDto Candidates
        {
            get { return _candidates; }
            set { _candidates = value; }
        }
    }
}
