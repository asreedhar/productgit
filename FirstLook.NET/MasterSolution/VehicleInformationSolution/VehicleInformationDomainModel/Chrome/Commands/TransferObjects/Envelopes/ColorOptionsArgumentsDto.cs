namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects.Envelopes
{
    public class ColorOptionsArgumentsDto
    {
        private int _styleId;
        private bool _isFleet;
        private string _exteriorCodeOne;
        private string _exteriorCodeTwo;
        private string _interiorCode;

        public bool IsFleet
        {
            get { return _isFleet; }
            set { _isFleet = value; }
        }

        public int StyleId
        {
            get { return _styleId; }
            set { _styleId = value; }
        }

        public string ExteriorCodeOne
        {
            get { return _exteriorCodeOne; }
            set { _exteriorCodeOne = value; }
        }

        public string ExteriorCodeTwo
        {
            get { return _exteriorCodeTwo; }
            set { _exteriorCodeTwo = value; }
        }

        public string InteriorCode
        {
            get { return _interiorCode; }
            set { _interiorCode = value; }
        }
    }
}