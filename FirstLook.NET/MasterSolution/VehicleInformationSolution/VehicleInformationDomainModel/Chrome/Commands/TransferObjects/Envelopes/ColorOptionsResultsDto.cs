﻿using System;
using FirstLook.Common.Core.Collections;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ColorOptionsResultsDto
    {
        private IRandomAccess<OptionCollectionDto> _result;

        public IRandomAccess<OptionCollectionDto> Result
        {
            get { return _result; }
            set { _result = value; }
        }
    }
}
