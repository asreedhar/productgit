using System;
using FirstLook.Common.Core.Collections;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class CategoryResultsDto
    {
        private readonly IRandomAccess<CategoryHeaderDto> _categoryHeaders = new RandomAccess<CategoryHeaderDto>();

        public IRandomAccess<CategoryHeaderDto> CategoryHeaders
        {
            get { return _categoryHeaders; }
        }
    }
}