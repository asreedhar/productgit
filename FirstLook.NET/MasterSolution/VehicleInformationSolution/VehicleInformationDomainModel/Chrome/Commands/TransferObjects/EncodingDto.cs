namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects
{
    public class EncodingDto
    {
        public EncodingDto(string id, string value)
        {
            _index = id;
            _value = value;
        }

        public EncodingDto(int id, int value)
        {
            _index = id.ToString();
            _value = value.ToString();
        }

        public EncodingDto(int id, string value)
        {
            _index = id.ToString();
            _value = value;
        }

        private string _index;
        private string _value;

        public string Index
        {
            get { return _index; }
            set { _index = value; }
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }
}
