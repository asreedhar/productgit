using System.Collections.Generic;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects
{
    public class EncodingsDto
    {
        private IEnumerable<EncodingDto> _standards;
        private IEnumerable<EncodingDto> _categories;
        private IEnumerable<EncodingDto> _options;

        public IEnumerable<EncodingDto> Standards
        {
            get { return _standards; }
            set { _standards = value; }
        }

        public IEnumerable<EncodingDto> Categories
        {
            get { return _categories; }
            set { _categories = value; }
        }

        public IEnumerable<EncodingDto> Options
        {
            get { return _options; }
            set { _options = value; }
        }
    }
}
