using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects
{
    public class ColorDto
    {
        private bool _asTwoTone;
        private ColorComponentDto _exteriorOne;
        private ColorComponentDto _exteriorTwo;
        private ColorComponentDto _interior;

        public bool AsTwoTone
        {
            get { return _asTwoTone; }
            set { _asTwoTone = value; }
        }

        public ColorComponentDto ExteriorOne
        {
            get { return _exteriorOne; }
            set { _exteriorOne = value; }
        }

        public ColorComponentDto ExteriorTwo
        {
            get { return _exteriorTwo; }
            set { _exteriorTwo = value; }
        }

        public ColorComponentDto Interior
        {
            get { return _interior; }
            set { _interior = value; }
        }

        public bool Equals(Color other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(_exteriorOne, other.ExteriorOne) // a.equals(b)
                   && Equals(_exteriorTwo, other.ExteriorTwo)
                   && Equals(_interior, other.Interior)
                   && Equals(_asTwoTone, other.AsTwoTone);
        }
    }
}