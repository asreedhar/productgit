﻿using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.Impl;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Commands
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ICommandFactory, CommandFactory>(ImplementationScope.Shared);
        }
    }
}
