﻿using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.VinMatch;
using FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.VinMatch;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<INewVehicleDataRepository, NewVehicleDataRepository>(
                ImplementationScope.Shared);

            registry.Register<INewVehicleDataStore, DbNewVehicleDataStore>(
                ImplementationScope.Shared);

            registry.Register<IVinMatchRepository, VinMatchRepository>(
                ImplementationScope.Shared);

            registry.Register<IVinMatchStore, DbVinMatchStore>(
                ImplementationScope.Shared);
        }
    }
}
