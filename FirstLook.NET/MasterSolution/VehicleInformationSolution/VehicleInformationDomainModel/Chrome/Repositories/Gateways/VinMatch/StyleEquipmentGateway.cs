using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.VinMatch;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.VinMatch
{
    public class StyleEquipmentGateway : GatewayBase
    {
        public StyleEquipment[] Fetch(int styleId)
        {
            string key = CreateCacheKey(styleId);

            List<StyleEquipment> values = Cache.Get(key) as List<StyleEquipment>;

            if (values == null)
            {
                values = new List<StyleEquipment>();

                CategoryGateway categories = new CategoryGateway();

                categories.Fetch(0); // pre-fetch (breaking code)

                IVinMatchStore store = Resolve<IVinMatchStore>();

                store.StyleEquipment(delegate(IDataReader reader)
                {
                    while (reader.Read())
                    {
                        string availabilityText = reader.GetString(reader.GetOrdinal("Availability"));

                        int categoryId = reader.GetInt32(reader.GetOrdinal("CategoryId"));

                        StyleAvailability availability = (StyleAvailability)Enum.Parse(
                                                                                 typeof(StyleAvailability),
                                                                                 availabilityText);

                        Category category = categories.Fetch(categoryId);

                        values.Add(new StyleEquipment(category, availability));
                    }

                    Cache.Add(
                        key,
                        values,
                        CacheConstants.NoAbsoluteExpiration,
                        CacheConstants.DefaultSlidingExpiration);
                }, styleId);
            }

            return values.ToArray();
        }
    }
}
