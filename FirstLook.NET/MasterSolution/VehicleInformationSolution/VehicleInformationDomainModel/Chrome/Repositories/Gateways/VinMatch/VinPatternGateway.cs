using System.Data;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.VinMatch;
using FirstLook.Common.Core.Memento;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.VinMatch
{
    public class VinPatternGateway : GatewayBase
    {
        public VinPattern Fetch(string vinPattern, string country)
        {
            string key = CreateCacheKey(vinPattern, country);

            VinPattern pattern = Cache.Get(key) as VinPattern;

            if (pattern == null)
            {
                IVinMatchStore store = RegistryFactory.GetRegistry().Resolve<IVinMatchStore>();

                store.VinPattern(delegate(IDataReader reader)
                {
                    if (reader.Read())
                    {
                        pattern = new VinPattern(
                            reader.GetString(reader.GetOrdinal("StyleName")),
                            reader.GetString(reader.GetOrdinal("Pattern")),
                            reader.GetInt32(reader.GetOrdinal("ModelYear")),
                            reader.GetString(reader.GetOrdinal("ModelName")),
                            reader.GetInt32(reader.GetOrdinal("Id")),
                            reader.GetString(reader.GetOrdinal("DivisionName")),
                            reader.GetString(reader.GetOrdinal("Country")));

                        Cache.Add(
                            key,
                            pattern,
                            CacheConstants.NoAbsoluteExpiration,
                            CacheConstants.DefaultSlidingExpiration);
                    }
                }, vinPattern, country);

            }

            return pattern;
        }
    }
}
