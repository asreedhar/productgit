using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.VinMatch;
using FirstLook.Common.Core.Memento;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.VinMatch
{
    public class VinEquipmentGateway : GatewayBase
    {
        public VinEquipment[] Fetch(int vinPatternId, string country)
        {
            string key = CreateCacheKey(vinPatternId, country);

            List<VinEquipment> values = Cache.Get(key) as List<VinEquipment>;

            if (values == null)
            {
                IVinMatchStore store = RegistryFactory.GetRegistry().Resolve<IVinMatchStore>();

                store.VinEquipment(delegate(IDataReader reader)
                {
                    values = new List<VinEquipment>();

                    CategoryGateway categories = new CategoryGateway();

                    while (reader.Read())
                    {
                       string availabilityText = reader.GetString(reader.GetOrdinal("Availability"));

                       int categoryId = reader.GetInt32(reader.GetOrdinal("CategoryId"));

                       VinAvailability availability = (VinAvailability) Enum.Parse(typeof(VinAvailability), availabilityText);

                       Category category = categories.Fetch(categoryId);

                       values.Add(new VinEquipment(availability, category));
                    }

                    Cache.Add(
                       key,
                       values,
                       CacheConstants.NoAbsoluteExpiration,
                       CacheConstants.DefaultSlidingExpiration);
                }, vinPatternId, country);
            }

            return values.ToArray();
        }
    }
}
