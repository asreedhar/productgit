using System.Collections.Generic;
using System.Data;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.VinMatch;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.VinMatch
{
    public class CategoryGateway : GatewayBase
    {
        private IDictionary<int, Category> Load()
        {
            string key = CreateCacheKey();

            IDictionary<int, Category> values = Cache.Get(key) as IDictionary<int, Category>;

            if (values == null)
            {
                IVinMatchStore store = RegistryFactory.GetRegistry().Resolve<IVinMatchStore>();

                store.Category(delegate(IDataReader reader)
                {
                    values = Load(reader);

                    Cache.Add(
                        key,
                        values,
                        CacheConstants.NoAbsoluteExpiration,
                        CacheConstants.DefaultSlidingExpiration);
                });
            }

            return values;
        }

        private static Dictionary<int, Category> Load(IDataReader reader)
        {
            Dictionary<int, Category> categories = new Dictionary<int, Category>();

            Dictionary<string, CategoryType> categoryTypes = new Dictionary<string, CategoryType>();

            while (reader.Read())
            {
                string categoryTypeText = DataRecord.GetString(reader, "CategoryType");

                if (!categoryTypes.ContainsKey(categoryTypeText))
                {
                    categoryTypes.Add(categoryTypeText, new CategoryType(categoryTypeText));
                }

                Category category = new Category(
                    reader.GetInt32(reader.GetOrdinal("Id")),
                    DataRecord.GetString(reader,"GroupName"),
                    DataRecord.GetString(reader, "Description"),
                    categoryTypes[categoryTypeText]);

                if (!categories.ContainsKey(category.Id))
                {
                    categories.Add(category.Id, category);
                }
            }

            return categories;
        }

        public Category Fetch(int categoryId)
        {
            IDictionary<int, Category> values = Load();

            if (values.ContainsKey(categoryId))
            {
                return values[categoryId];
            }

            return null;
        }
    }
}
