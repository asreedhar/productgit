using System;
using System.Data;
using FirstLook.Common.Core.Data;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.VinMatch
{
    public interface IVinMatchStore
    {
        void VinPattern(Action<IDataReader> f, string vinPattern, string country);

        void VinEquipment(Action<IDataReader> f, int vinPatternId, string country);

        void Style(Action<IDataReader> f, int vinPatternId, string country);

        void StyleEquipment(Action<IDataReader> f, int styleId);

        void Category(Action<IDataReader> f);
    }

    public class DbVinMatchStore : SimpleDataStore, IVinMatchStore
    {
        protected override System.Reflection.Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        protected override string DatabaseName
        {
            get { return "Chrome"; }
        }

        public void VinPattern(Action<IDataReader> f, string vinPattern, string country)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.VinMatch.VinPatternRepository_Fetch_VinPattern.txt";

            Query(f, queryName, new Parameter("VinPattern", vinPattern, DbType.String), new Parameter("Country", country, DbType.String));
        }

        public void VinEquipment(Action<IDataReader> f, int vinPatternId, string country)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.VinMatch.VinPatternRepository_Fetch_VinEquipment.txt";

            Query(f, queryName, new Parameter("VinPatternID", vinPatternId, DbType.Int32), new Parameter("Country", country, DbType.String));
        }

        public void Style(Action<IDataReader> f, int vinPatternId, string country)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.VinMatch.VinPatternRepository_Fetch_Styles.txt";

            Query(f, queryName, new Parameter("VinPatternID", vinPatternId, DbType.Int32), new Parameter("Country", country, DbType.String));
        }

        public void StyleEquipment(Action<IDataReader> f, int styleId)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.VinMatch.VinPatternRepository_Fetch_StyleEquipment.txt";

            Query(f, queryName, new Parameter("StyleId", styleId, DbType.Int32));
        }

        public void Category(Action<IDataReader> f)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.VinMatch.VinPatternRepository_Fetch_Category_Bulk.txt";

            Query(f, queryName);
        }
    }
}
