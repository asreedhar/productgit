using System.Collections.Generic;
using System.Data;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.VinMatch;
using FirstLook.Common.Core.Memento;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.VinMatch
{
    public class StyleGateway : GatewayBase
    {
        public Style[] Fetch(int vinPatternId, string country)
        {
            string key = CreateCacheKey(vinPatternId, country);

            List<Style> values = Cache.Get(key) as List<Style>;

            if (values == null)
            {
                IVinMatchStore store = RegistryFactory.GetRegistry().Resolve<IVinMatchStore>();

                store.Style(delegate(IDataReader reader)
                {
                    values = new List<Style>();

                    while (reader.Read())
                    {
                        Style style = new Style(
                            DataRecord.GetString(reader, "TrimName"),
                            DataRecord.GetString(reader, "SubdivisionName"),
                            DataRecord.GetString(reader, "StyleName"),
                            DataRecord.GetString(reader, "StyleNameWithoutTrim"),
                            reader.GetInt32(reader.GetOrdinal("ModelYear")),
                            DataRecord.GetString(reader, "ModelName"),
                            DataRecord.GetString(reader, "ManufacturerStyleCode"),
                            reader.GetInt32(reader.GetOrdinal("Id")),
                            reader.GetBoolean(reader.GetOrdinal("FleetOnly")),
                            DataRecord.GetString(reader, "DivisionName"),
                            DataRecord.GetString(reader, "Country"),
                            reader.GetBoolean(reader.GetOrdinal("AvailableInNewVehicleData")),
                            null);

                        values.Add(style);
                    }

                    Cache.Add(
                        key,
                        values,
                        CacheConstants.NoAbsoluteExpiration,
                        CacheConstants.DefaultSlidingExpiration);

                }, vinPatternId, country);
            }

            return values.ToArray();
        }
    }
}
