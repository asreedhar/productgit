using System.Collections.Generic;
using System.Data;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.Common.Core.Memento;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.NewVehicleData
{
    public class OptionGateway : GatewayBase
    {
        private readonly OptionHeaderGateway _headers = new OptionHeaderGateway();

        public Option Lookup(int styleId, string code, INewVehicleDataStore store)
        {
            IList<Option> values = Load(styleId);

            foreach (Option value in values)
            {
                if (Equals(value.Code, code))
                {
                    return value;
                }
            }

            return null;
        }

        public IList<Option> Fetch(int styleId)
        {
            return Load(styleId);
        }

        public IList<Option> Fetch(int styleId, INewVehicleDataStore store)
        {
            if (store == null) return Fetch(styleId);

            IList<Option> values = new List<Option>();

            store.Option(
                delegate(IDataReader reader)
                    {
                        while (reader.Read())
                        {
                            int id = reader.GetInt32(reader.GetOrdinal("OptionHeaderId"));

                            string description = string.Empty;

                            if (!reader.IsDBNull(reader.GetOrdinal("Description")))
                            {
                                description = reader.GetString(reader.GetOrdinal("Description"));
                            }

                            Option option = new Option(
                                new OptionKindGateway().Lookup(reader.GetInt32(reader.GetOrdinal("OptionKindId"))),
                                reader.GetString(reader.GetOrdinal("Name")),
                                description,
                                reader.GetInt32(reader.GetOrdinal("Sequence")),
                                styleId,
                                _headers.Fetch(id),
                                reader.GetString(reader.GetOrdinal("GroupName")),
                                reader.GetString(reader.GetOrdinal("Code")),
                                ModelGateway.GetAvailability(reader));

                            values.Add(option);
                        }
                    },
                styleId);

            return values;
        }

        public void Freeze(int styleId, DataSet dataSet)
        {
            DataTable table = dataSet.Tables.Add("Option");

            Resolve<INewVehicleDataStore>().Option(
                delegate(IDataReader reader)
                {
                    table.Load(reader);
                },
                styleId);
        }

        private IList<Option> Load(int styleId)
        {
            string key = CreateCacheKey(styleId);

            IList<Option> values = Cache.Get(key) as IList<Option>;

            if (values == null)
            {
                INewVehicleDataStore store = Resolve<INewVehicleDataStore>();

                values = Fetch(styleId, store);

                Cache.Add(
                    key,
                    values,
                    CacheConstants.NoAbsoluteExpiration,
                    CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
