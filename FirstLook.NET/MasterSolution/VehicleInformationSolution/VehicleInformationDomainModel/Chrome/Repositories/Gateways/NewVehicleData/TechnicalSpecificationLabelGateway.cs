using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.NewVehicleData
{
    public class TechnicalSpecificationLabelGateway : GatewayBase
    {
        public class Row
        {
            public int LabelId;
            public string Label;
            public int TypeId;
        }

        public Row Fetch(int id)
        {
            IDictionary<int, Row> values = Load();

            if (values.ContainsKey(id))
            {
                return values[id];
            }

            return null;
        }

        public void Freeze(DataSet dataSet)
        {
            DataTable table = dataSet.Tables.Add("TechnicalSpecificationLabel");

            Resolve<INewVehicleDataStore>().TechnicalSpecificationLabel(
                delegate(IDataReader reader) { table.Load(reader); });
        }

        private Dictionary<int, Row> Load()
        {
            string key = CreateCacheKey();

            Dictionary<int, Row> values = Cache.Get(key) as Dictionary<int, Row>;

            if (values == null)
            {
                values = new Dictionary<int, Row>();

                Resolve<INewVehicleDataStore>().TechnicalSpecificationLabel(
                    delegate(IDataReader reader)
                        {
                            while (reader.Read())
                            {
                                Row row = new Row();
                                row.Label = reader.GetString(reader.GetOrdinal("Label"));
                                row.LabelId = reader.GetInt32(reader.GetOrdinal("LabelID"));
                                row.TypeId = reader.GetInt32(reader.GetOrdinal("TypeID"));

                                values.Add(row.LabelId, row);
                            }
                        });

                Cache.Add(
                    key,
                    values,
                    CacheConstants.NoAbsoluteExpiration,
                    CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}