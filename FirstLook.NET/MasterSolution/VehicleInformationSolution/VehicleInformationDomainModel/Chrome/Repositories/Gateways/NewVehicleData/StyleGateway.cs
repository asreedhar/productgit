using System.Data;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Data;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.NewVehicleData
{
    public class StyleGateway : GatewayBase
    {
        public Style Fetch(int styleId)
        {
            string key = CreateCacheKey(styleId);

            Style style = Cache.Get(key) as Style;

            if (style == null)
            {
                INewVehicleDataStore store = Resolve<INewVehicleDataStore>();

                style = Fetch(styleId, store);

                Remember(key, style);
            }

            return style;
        }

        public Style Fetch(int styleId, INewVehicleDataStore store)
        {
            if (store == null) return Fetch(styleId);

            Style style = null;

            store.Style(
                delegate(IDataReader reader)
                    {
                        if (reader.Read())
                        {
                            style = new Style(
                                new JpgGateway().Fetch(styleId),
                                new ModelGateway().Fetch(reader.GetInt32(reader.GetOrdinal("ModelId"))),
                                reader.GetBoolean(reader.GetOrdinal("TrueBasePrice")),
                                DataRecord.GetString(reader, "Trim"),
                                reader.GetInt32(reader.GetOrdinal("Sequence")),
                                DataRecord.GetString(reader, "NameWithoutTrim"),
                                DataRecord.GetString(reader, "Name"),
                                reader.GetDecimal(reader.GetOrdinal("MSRP")),
                                reader.GetDecimal(reader.GetOrdinal("Invoice")),
                                DataRecord.GetString(reader, "FullCode"),
                                reader.GetDecimal(reader.GetOrdinal("DestinationCharge")),
                                DataRecord.GetString(reader, "Code"),
                                reader.GetInt32(reader.GetOrdinal("Id")));
                        }
                    },
                styleId);

            return style;
        }

        public void Freeze(int styleId, DataSet dataSet)
        {
            DataTable table = dataSet.Tables.Add("Style");

            Resolve<INewVehicleDataStore>().Style(
                reader => table.Load(reader),
                styleId);
        }
    }
}
