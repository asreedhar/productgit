using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Data;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Antlr;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.NewVehicleData
{
    public class TechnicalSpecificationGateway : GatewayBase
    {
        private readonly TechnicalSpecificationHeaderGateway _headers = new TechnicalSpecificationHeaderGateway();

        private readonly TechnicalSpecificationLabelGateway _labels = new TechnicalSpecificationLabelGateway();

        public IList<TechnicalSpecification> Fetch(int styleId)
        {
            string key = CreateCacheKey(styleId);

            IList<TechnicalSpecification> values = Cache.Get(key) as IList<TechnicalSpecification>;

            if (values == null)
            {
                INewVehicleDataStore store = Resolve<INewVehicleDataStore>();

                values = Fetch(styleId, store);

                values = new ReadOnlyCollection<TechnicalSpecification>(values);

                Cache.Add(
                    key,
                    values,
                    CacheConstants.NoAbsoluteExpiration,
                    CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }

        public IList<TechnicalSpecification> Fetch(int styleId, INewVehicleDataStore store)
        {
            if (store == null) return Fetch(styleId);

            IList<TechnicalSpecification> values = new List<TechnicalSpecification>();

            store.TechnicalSpecification(
                delegate(IDataReader reader)
                    {
                        while (reader.Read())
                        {
                            int labelId = reader.GetInt32(reader.GetOrdinal("LabelID"));

                            TechnicalSpecificationLabelGateway.Row label = _labels.Fetch(labelId);

                            string conditionText = DataRecord.GetString(reader, "Condition");

                            ICondition condition = ConditionHelper.GetCondition(conditionText);

                            TechnicalSpecification technicalSpecification = new TechnicalSpecification(
                                _headers.Fetch(label.TypeId),
                                label.LabelId,
                                label.Label,
                                reader.GetString(reader.GetOrdinal("Value")),
                                condition);

                            values.Add(technicalSpecification);
                        }
                    },
                styleId);

            return values;
        }

        public void Freeze(int styleId, DataSet dataSet)
        {
            DataTable table = dataSet.Tables.Add("TechnicalSpecification");

            Resolve<INewVehicleDataStore>().TechnicalSpecification(
                delegate(IDataReader reader)
                {
                    table.Load(reader);
                },
                styleId);
        }
    }
}