using System.Data;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.Common.Core.Memento;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.NewVehicleData
{
    public class ModelGateway : GatewayBase
    {
        public VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Model Fetch(int modelId)
        {
            string key = CreateCacheKey(modelId);

            VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Model value = Cache.Get(key) as VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Model;

            if (value == null)
            {
                Resolve<INewVehicleDataStore>().Model(
                    delegate(IDataReader reader)
                        {
                            if (reader.Read())
                            {
                                value = new VehicleInformation.DomainModel.Chrome.Model.NewVehicleData.Model(
                                    DataRecord.GetString(reader, "Name"),
                                    reader.GetInt32(reader.GetOrdinal("ModelYear")),
                                    reader.GetInt32(reader.GetOrdinal("Id")),
                                    DataRecord.GetString(reader, "Comment"),
                                    new SubdivisionGateway().Fetch(reader.GetInt32(reader.GetOrdinal("SubdivisionID"))),
                                    GetAvailability(reader));

                                Cache.Add(
                                    key,
                                    value,
                                    CacheConstants.NoAbsoluteExpiration,
                                    CacheConstants.DefaultSlidingExpiration);
                            }
                        },
                    modelId);
            }

            return value;
        }

        public static Availability GetAvailability(IDataRecord reader)
        {
            Availability availability = Availability.NotDefined;

            string s = reader.GetString(reader.GetOrdinal("Availability"));

            switch (s)
            {
                case " ":
                    availability = Availability.Any;
                    break;
                case "F":
                    availability = Availability.Fleet;
                    break;
                case "R":
                    availability = Availability.Retail;
                    break;
                case "P":
                    availability = Availability.Police;
                    break;
            }

            return availability;
        }

        public void Freeze(int modelId, DataSet dataSet)
        {
            DataTable table = dataSet.Tables.Add("Model");

            Resolve<INewVehicleDataStore>().Model(
                delegate(IDataReader reader)
                {
                    table.Load(reader);
                },
                modelId);
        }
    }
}
