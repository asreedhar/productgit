using System.Collections.Generic;
using System.Data;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.Common.Core.Memento;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.NewVehicleData
{
    public class ConsumerInformationHeaderGateway : GatewayBase
    {
        public ConsumerInformationHeader Fetch(int id)
        {
            IDictionary<int, ConsumerInformationHeader> values = Load();

            if (values.ContainsKey(id))
            {
                return values[id];
            }

            return null;
        }

        public void Freeze(DataSet dataSet)
        {
            DataTable table = dataSet.Tables.Add("ConsumerInformationHeader");

            Resolve<INewVehicleDataStore>().ConsumerInformationHeader(
                delegate(IDataReader reader) { table.Load(reader); });
        }

        private Dictionary<int, ConsumerInformationHeader> Load()
        {
            string key = CreateCacheKey();

            Dictionary<int, ConsumerInformationHeader> values = Cache.Get(key) as Dictionary<int, ConsumerInformationHeader>;

            if (values == null)
            {
                values = new Dictionary<int, ConsumerInformationHeader>();

                Resolve<INewVehicleDataStore>().ConsumerInformationHeader(
                    delegate(IDataReader reader)
                        {
                            while (reader.Read())
                            {
                                ConsumerInformationHeader header = new ConsumerInformationHeader(
                                    reader.GetInt32(reader.GetOrdinal("Id")),
                                    reader.GetString(reader.GetOrdinal("Name")));

                                values.Add(header.Id, header);
                            }
                        });

                Cache.Add(
                    key,
                    values,
                    CacheConstants.NoAbsoluteExpiration,
                    CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}