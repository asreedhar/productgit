using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.NewVehicleData
{
    public class CategoryActionGateway : GatewayBase
    {
        public enum FeatureType
        {
            NotDefined = 0,
            Standard = 1,
            Option = 2
        }

        public class CategoryAction
        {
            private readonly FeatureType _featureType;
            private readonly Action _action;
            private readonly int _categoryId;
            private readonly int _sequence;

            internal CategoryAction(FeatureType featureType, Action action, int categoryId, int sequence)
            {
                _featureType = featureType;
                _action = action;
                _categoryId = categoryId;
                _sequence = sequence;
            }

            public FeatureType FeatureType
            {
                get { return _featureType; }
            }

            public Action Action
            {
                get { return _action; }
            }

            public int CategoryId
            {
                get { return _categoryId; }
            }

            public int Sequence
            {
                get { return _sequence; }
            }
        }

        public void Load(int styleId)
        {
            string key = CreateCacheKey(styleId);

            List<CategoryAction> values = Cache.Get(key) as List<CategoryAction>;

            if (values == null)
            {
                INewVehicleDataStore store = Resolve<INewVehicleDataStore>();

                values = Load(styleId, store);

                Cache.Add(
                    key,
                    values,
                    CacheConstants.NoAbsoluteExpiration,
                    CacheConstants.DefaultSlidingExpiration);
            }
        }

        public List<CategoryAction> Load(int styleId, INewVehicleDataStore store)
        {
            if (store == null) store = Resolve<INewVehicleDataStore>();

            List<CategoryAction> values = new List<CategoryAction>();

            store.CategoryAction(
                delegate(IDataReader reader)
                    {
                        while (reader.Read())
                        {
                            string featureTypeText = reader.GetString(reader.GetOrdinal("FeatureType"));

                            FeatureType featureType = FeatureType.NotDefined;

                            switch (featureTypeText)
                            {
                                case "S":
                                    featureType = FeatureType.Standard;
                                    break;
                                case "O":
                                    featureType = FeatureType.Option;
                                    break;
                            }

                            string actionText = reader.GetString(reader.GetOrdinal("State"));

                            Action action = Action.NotDefined;

                            switch (actionText)
                            {
                                case "C":
                                    action = Action.Add;
                                    break;
                                case "P":
                                    action = Action.Package;
                                    break;
                                case "D":
                                    action = Action.Remove;
                                    break;
                            }

                            CategoryAction categoryAction = new CategoryAction(
                                featureType,
                                action,
                                reader.GetInt32(reader.GetOrdinal("CategoryID")),
                                reader.GetInt32(reader.GetOrdinal("Sequence")));

                            values.Add(categoryAction);
                        }
                    },
                styleId);

            return values;
        }

        public IList<CategoryAction> Fetch(int styleId, int sequence, FeatureType featureType)
        {
            string key = CreateCacheKey(styleId);

            List<CategoryAction> values = Cache.Get(key) as List<CategoryAction>;

            return Fetch(values, sequence, featureType);
        }

        public IList<CategoryAction> Fetch(int styleId, int sequence, FeatureType featureType, INewVehicleDataStore store)
        {
            List<CategoryAction> values = Load(styleId, store);
            
            return Fetch(values, sequence, featureType);
        }

        public static IList<CategoryAction> Fetch(List<CategoryAction> values, int sequence, FeatureType featureType)
        {
            if (values != null)
            {
                List<CategoryAction> actions = values.FindAll(
                    delegate(CategoryAction match)
                    {
                        return match.Sequence == sequence && match.FeatureType == featureType;
                    });

                return new ReadOnlyCollection<CategoryAction>(actions);
            }

            return new CategoryAction[0];
        }

        public void Freeze(int styleId, DataSet dataSet)
        {
            DataTable table = dataSet.Tables.Add("CategoryAction");

            RegistryFactory.GetRegistry().Resolve<INewVehicleDataStore>().CategoryAction(
                delegate(IDataReader reader)
                {
                    table.Load(reader);
                },
                styleId);
        }
    }
}