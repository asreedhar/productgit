using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.Common.Core.Memento;
using FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Antlr;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.NewVehicleData
{
    public class PriceGateway : GatewayBase
    {
        public IList<Price> Fetch(int styleId)
        {
            string key = CreateCacheKey(styleId);

            IList<Price> values = Cache.Get(key) as IList<Price>;

            if (values == null)
            {
                INewVehicleDataStore store = Resolve<INewVehicleDataStore>();

                values = Fetch(styleId, store);

                values = new ReadOnlyCollection<Price>(values);

                Cache.Add(
                    key,
                    values,
                    CacheConstants.NoAbsoluteExpiration,
                    CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }

        public IList<Price> Fetch(int styleId, INewVehicleDataStore store)
        {
            if (store == null) return Fetch(styleId);

            IList<Price> values = new List<Price>();

            store.Price(
                delegate(IDataReader reader)
                    {
                        OptionGateway options = new OptionGateway();

                        while (reader.Read())
                        {
                            string optionCode = reader.GetString(reader.GetOrdinal("OptionCode"));

                            Option option = options.Lookup(styleId, optionCode, store);

                            string conditionText = DataRecord.GetString(reader, "Condition");

                            ICondition condition = ConditionHelper.GetCondition(conditionText);

                            Price price = new Price(
                                option,
                                condition,
                                reader.GetDecimal(reader.GetOrdinal("MSRP")),
                                reader.GetDecimal(reader.GetOrdinal("Invoice")));

                            values.Add(price);
                        }

                    },
                styleId);

            return values;
        }

        public void Freeze(int styleId, DataSet dataSet)
        {
            DataTable table = dataSet.Tables.Add("Price");

            Resolve<INewVehicleDataStore>().Price(
                delegate(IDataReader reader)
                {
                    table.Load(reader);
                },
                styleId);
        }
    }
}
