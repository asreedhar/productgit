using System.Data;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.Common.Core.Memento;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.NewVehicleData
{
    public class ManufacturerGateway : GatewayBase
    {
        public Manufacturer Fetch(int manufacturerId)
        {
            string key = CreateCacheKey(manufacturerId);

            Manufacturer value = Cache.Get(key) as Manufacturer;

            if (value == null)
            {
                Resolve<INewVehicleDataStore>().Manufacturer(
                    delegate(IDataReader reader)
                        {
                            if (reader.Read())
                            {
                                value = new Manufacturer(
                                    reader.GetString(reader.GetOrdinal("Name")),
                                    reader.GetInt32(reader.GetOrdinal("Id")));

                                Cache.Add(
                                    key,
                                    value,
                                    CacheConstants.NoAbsoluteExpiration,
                                    CacheConstants.DefaultSlidingExpiration);
                            }
                        },
                    manufacturerId);
            }

            return value;
        }

        public void Freeze(int manufacturerId, DataSet dataSet)
        {
            DataTable table = dataSet.Tables.Add("Manufacturer");

            Resolve<INewVehicleDataStore>().Manufacturer(
                delegate(IDataReader reader)
                {
                    table.Load(reader);
                },
                manufacturerId);
        }
    }
}
