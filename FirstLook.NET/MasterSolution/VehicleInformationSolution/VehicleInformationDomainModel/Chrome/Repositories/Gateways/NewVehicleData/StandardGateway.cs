using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.Common.Core.Memento;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.NewVehicleData
{
    public class StandardGateway : GatewayBase
    {
        private readonly StandardHeaderGateway _headers = new StandardHeaderGateway();

        public IList<Standard> Fetch(int styleId)
        {
            string key = CreateCacheKey(styleId);

            IList<Standard> values = Cache.Get(key) as IList<Standard>;

            if (values == null)
            {
                INewVehicleDataStore store = Resolve<INewVehicleDataStore>();

                values = new ReadOnlyCollection<Standard>(Fetch(styleId, store));

                Cache.Add(
                    key,
                    values,
                    CacheConstants.NoAbsoluteExpiration,
                    CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }

        public IList<Standard> Fetch(int styleId, INewVehicleDataStore store)
        {
            if (store == null) return Fetch(styleId);

            List<Standard> values = new List<Standard>();

            store.Standard(
                delegate(IDataReader reader)
                    {
                        while (reader.Read())
                        {
                            int id = reader.GetInt32(reader.GetOrdinal("StandardHeaderId"));

                            Standard standard = new Standard(
                                _headers.Fetch(id),
                                reader.GetString(reader.GetOrdinal("Name")),
                                reader.GetInt32(reader.GetOrdinal("Sequence")),
                                styleId);

                            values.Add(standard);
                        }
                    },
                styleId);

            return values;
        }

        public void Freeze(int styleId, DataSet dataSet)
        {
            DataTable table = dataSet.Tables.Add("Standard");

            Resolve<INewVehicleDataStore>().Standard(
                delegate(IDataReader reader)
                {
                    table.Load(reader);
                },
                styleId);
        }
    }
}
