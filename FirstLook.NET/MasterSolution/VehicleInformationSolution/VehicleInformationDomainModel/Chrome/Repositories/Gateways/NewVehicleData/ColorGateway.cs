using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.Common.Core.Memento;
using FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Antlr;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.NewVehicleData
{
    public class ColorGateway : GatewayBase
    {
        public IList<Color> Fetch(int styleId)
        {
            string key = CreateCacheKey(styleId);

            IList<Color> values = Cache.Get(key) as IList<Color>;

            if (values == null)
            {
                INewVehicleDataStore store = Resolve<INewVehicleDataStore>();

                values = new ReadOnlyCollection<Color>(Fetch(styleId, store));

                Cache.Add(
                    key,
                    values,
                    CacheConstants.NoAbsoluteExpiration,
                    CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }

        public IList<Color> Fetch(int styleId, INewVehicleDataStore store)
        {
            if (store == null) return Fetch(styleId);

            IList<Color> values = new List<Color>();

            store.Color(
                delegate(IDataReader reader)
                    {
                        while (reader.Read())
                        {
                            Color color = new Color(
                                GetCondition(reader),
                                reader.GetString(reader.GetOrdinal("OrderCode")),
                                GetColorComponent(reader, true, "1"),
                                GetColorComponent(reader, true, "2"),
                                GetColorComponent(reader, false, "1"),
                                reader.GetBoolean(reader.GetOrdinal("AsTwoTone")));

                            values.Add(color);
                        }
                    },
                styleId);

            return values;
        }

        private static ColorComponent GetColorComponent(IDataRecord record, bool exterior, string suffix)
        {
            if (exterior)
            {
                string genericDescription = "GenericExtColor";
                string rgbHex = "Ext1RGBHex";
                string manufacturerCode = "Ext1ManCode";
                string manufacturerFullCode = "Ext1MfrFullCode";
                string description = "Ext1Desc";
                string code = "Ext1Code";

                if ("2".Equals(suffix))
                {
                    genericDescription = "GenericExt2Color";
                    rgbHex = "Ext2RGBHex";
                    manufacturerCode = "Ext2ManCode";
                    manufacturerFullCode = "Ext2MfrFullCode";
                    description = "Ext2Desc";
                    code = "Ext2Code";
                }

                return new ColorComponent(
                    record.GetString(record.GetOrdinal(genericDescription)),
                    record.GetString(record.GetOrdinal(rgbHex)),
                    record.GetString(record.GetOrdinal(manufacturerFullCode)),
                    record.GetString(record.GetOrdinal(manufacturerCode)),
                    record.GetString(record.GetOrdinal(description)),
                    record.GetString(record.GetOrdinal(code)));
            }

            return new ColorComponent(
                string.Empty,
                string.Empty,
                string.Empty,
                record.GetString(record.GetOrdinal("IntManCode")),
                record.GetString(record.GetOrdinal("IntDesc")),
                record.GetString(record.GetOrdinal("IntCode")));
        }

        private static ICondition GetCondition(IDataRecord reader)
        {
            string conditionText = reader.GetString(reader.GetOrdinal("Condition"));

            return ConditionHelper.GetCondition(conditionText);
        }

        public void Freeze(int styleId, DataSet dataSet)
        {
            DataTable table = dataSet.Tables.Add("Color");

            RegistryFactory.GetRegistry().Resolve<INewVehicleDataStore>().Color(
                delegate(IDataReader reader)
                {
                    table.Load(reader);
                },
                styleId);
        }
    }
}
