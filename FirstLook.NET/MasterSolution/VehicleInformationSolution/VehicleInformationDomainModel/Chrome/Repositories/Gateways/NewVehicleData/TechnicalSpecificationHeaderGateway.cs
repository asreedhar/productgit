using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.NewVehicleData
{
    public class TechnicalSpecificationHeaderGateway : GatewayBase
    {
        public TechnicalSpecificationHeader Fetch(int id)
        {
            IDictionary<int, TechnicalSpecificationHeader> values = Load();

            if (values.ContainsKey(id))
            {
                return values[id];
            }

            return null;
        }

        public void Freeze(DataSet dataSet)
        {
            DataTable table = dataSet.Tables.Add("TechnicalSpecificationHeader");

            Resolve<INewVehicleDataStore>().TechnicalSpecificationHeader(
                delegate(IDataReader reader) { table.Load(reader); });
        }

        private Dictionary<int, TechnicalSpecificationHeader> Load()
        {
            string key = CreateCacheKey();

            Dictionary<int, TechnicalSpecificationHeader> values =
                Cache.Get(key) as Dictionary<int, TechnicalSpecificationHeader>;

            if (values == null)
            {
                values = new Dictionary<int, TechnicalSpecificationHeader>();

                Resolve<INewVehicleDataStore>().TechnicalSpecificationHeader(
                    delegate(IDataReader reader)
                        {
                            while (reader.Read())
                            {
                                TechnicalSpecificationHeader header = new TechnicalSpecificationHeader(
                                    reader.GetInt32(reader.GetOrdinal("Id")),
                                    reader.GetString(reader.GetOrdinal("Name")));

                                values.Add(header.Id, header);
                            }
                        });

                Cache.Add(
                    key,
                    values,
                    CacheConstants.NoAbsoluteExpiration,
                    CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}