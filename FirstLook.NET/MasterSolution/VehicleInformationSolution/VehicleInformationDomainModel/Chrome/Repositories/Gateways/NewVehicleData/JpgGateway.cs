using System.Data;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.Common.Core.Memento;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.NewVehicleData
{
    public class JpgGateway : GatewayBase
    {
        public Jpg Fetch(int styleId)
        {
            string key = CreateCacheKey(styleId);

            Jpg value = Cache.Get(key) as Jpg;

            if (value != null)
            {
                return value;
            }

            Resolve<INewVehicleDataStore>().Jpg(
                delegate(IDataReader reader)
                    {
                        if (reader.Read())
                        {
                            value = new Jpg(
                                reader.GetString(reader.GetOrdinal("JPGName")));

                            Cache.Add(
                                key,
                                value,
                                CacheConstants.NoAbsoluteExpiration,
                                CacheConstants.DefaultSlidingExpiration);
                        }
                    },
                styleId);

            return value;
        }

        public void Freeze(int styleId, DataSet dataSet)
        {
            DataTable table = dataSet.Tables.Add("Jpg");

            Resolve<INewVehicleDataStore>().Jpg(
                delegate(IDataReader reader)
                {
                    table.Load(reader);
                },
                styleId);
        }
    }
}