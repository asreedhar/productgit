using System.Collections.Generic;
using System.Data;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.Common.Core.Memento;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.NewVehicleData
{
    public class StandardHeaderGateway : GatewayBase
    {
        public StandardHeader Fetch(int id)
        {
            IDictionary<int, StandardHeader> values = Load();

            if (values.ContainsKey(id))
            {
                return values[id];
            }

            return null;
        }

        public void Freeze(DataSet dataSet)
        {
            DataTable table = dataSet.Tables.Add("StandardHeader");

            Resolve<INewVehicleDataStore>().StandardHeader(
                delegate(IDataReader reader) { table.Load(reader); });
        }

        private Dictionary<int, StandardHeader> Load()
        {
            string key = CreateCacheKey();

            Dictionary<int, StandardHeader> values = Cache.Get(key) as Dictionary<int, StandardHeader>;

            if (values == null)
            {
                values = new Dictionary<int, StandardHeader>();

                Resolve<INewVehicleDataStore>().StandardHeader(
                    delegate(IDataReader reader)
                        {
                            while (reader.Read())
                            {
                                StandardHeader header = new StandardHeader(
                                    reader.GetString(reader.GetOrdinal("Name")),
                                    reader.GetInt32(reader.GetOrdinal("Id")));

                                values.Add(header.Id, header);
                            }
                        });

                Cache.Add(
                    key,
                    values,
                    CacheConstants.NoAbsoluteExpiration,
                    CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}