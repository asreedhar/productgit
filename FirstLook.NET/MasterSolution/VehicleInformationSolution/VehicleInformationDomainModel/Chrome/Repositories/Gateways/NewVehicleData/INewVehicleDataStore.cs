using System;
using System.Data;
using FirstLook.Common.Core.Data;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.NewVehicleData
{
    public interface INewVehicleDataStore
    {
        void BodyStyles(Action<IDataReader> f, int styleId);

        void Category(Action<IDataReader> f);

        void CategoryHeader(Action<IDataReader> f);

        void CategoryAction(Action<IDataReader> f, int styleId);

        void Color(Action<IDataReader> f, int styleId);

        void ConsumerInformation(Action<IDataReader> f, int styleId);

        void ConsumerInformationLabel(Action<IDataReader> f);

        void ConsumerInformationHeader(Action<IDataReader> f);

        void Division(Action<IDataReader> f, int divisionId);

        void Jpg(Action<IDataReader> f, int styleId);

        void Manufacturer(Action<IDataReader> f, int manufacturerId);

        void Model(Action<IDataReader> f, int modelId);

        void Option(Action<IDataReader> f, int styleId);

        void OptionHeader(Action<IDataReader> f);

        void OptionKind(Action<IDataReader> f);

        void OptionKind(Action<IDataReader> f, int styleId);

        void OrderRule(Action<IDataReader> f, int styleId);

        void Price(Action<IDataReader> f, int styleId);

        void Standard(Action<IDataReader> f, int styleId);

        void StandardHeader(Action<IDataReader> f);

        void Style(Action<IDataReader> f, int styleId);

        void Subdivision(Action<IDataReader> f, int subdivisionId);

        void TechnicalSpecification(Action<IDataReader> f, int styleId);

        void TechnicalSpecificationLabel(Action<IDataReader> f);

        void TechnicalSpecificationHeader(Action<IDataReader> f);
    }

    public class DbNewVehicleDataStore : SimpleDataStore, INewVehicleDataStore
    {
        protected override System.Reflection.Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        protected override string DatabaseName
        {
            get { return "Chrome"; }
        }

        public void BodyStyles(Action<IDataReader> f, int styleId)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.NewVehicleData.StyleRepository_Fetch_BodyStyles.txt";

            Query(f, queryName, new Parameter("StyleId", styleId, DbType.Int32));
        }

        public void CategoryAction(Action<IDataReader> f, int styleId)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.NewVehicleData.StyleRepository_Fetch_CategoryAction.txt";

            Query(f, queryName, new Parameter("StyleId", styleId, DbType.Int32));
        }

        public void CategoryHeader(Action<IDataReader> f)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.NewVehicleData.StyleRepository_Fetch_CategoryHeader.txt";

            Query(f, queryName);
        }

        public void Category(Action<IDataReader> f)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.NewVehicleData.StyleRepository_Fetch_Category_Bulk.txt";

            Query(f, queryName);
        }

        public void Color(Action<IDataReader> f, int styleId)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.NewVehicleData.StyleRepository_Fetch_Color.txt";

            Query(f, queryName, new Parameter("StyleId", styleId, DbType.Int32));
        }

        public void ConsumerInformation(Action<IDataReader> f, int styleId)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.NewVehicleData.StyleRepository_Fetch_ConsumerInformation.txt";

            Query(f, queryName, new Parameter("StyleId", styleId, DbType.Int32));
        }

        public void ConsumerInformationLabel(Action<IDataReader> f)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.NewVehicleData.StyleRepository_Fetch_ConsumerInformationLabel.txt";

            Query(f, queryName);
        }

        public void ConsumerInformationHeader(Action<IDataReader> f)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.NewVehicleData.StyleRepository_Fetch_ConsumerInformationHeader.txt";

            Query(f, queryName);
        }

        public void Division(Action<IDataReader> f, int divisionId)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.NewVehicleData.StyleRepository_Fetch_Division.txt";

            Query(f, queryName, new Parameter("DivisionId", divisionId, DbType.Int32));
        }

        public void Jpg(Action<IDataReader> f, int styleId)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.NewVehicleData.StyleRepository_Fetch_JPGs.txt";

            Query(f, queryName, new Parameter("StyleId", styleId, DbType.Int32));
        }

        public void Manufacturer(Action<IDataReader> f, int manufacturerId)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.NewVehicleData.StyleRepository_Fetch_Manufacturer.txt";

            Query(f, queryName, new Parameter("ManufacturerId", manufacturerId, DbType.Int32));
        }

        public void Model(Action<IDataReader> f, int modelId)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.NewVehicleData.StyleRepository_Fetch_Model.txt";

            Query(f, queryName, new Parameter("ModelId", modelId, DbType.Int32));
        }

        public void Option(Action<IDataReader> f, int styleId)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.NewVehicleData.StyleRepository_Fetch_Option_Bulk.txt";

            Query(f, queryName, new Parameter("StyleId", styleId, DbType.Int32));
        }

        public void OptionHeader(Action<IDataReader> f)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.NewVehicleData.StyleRepository_Fetch_OptionHeader.txt";

            Query(f, queryName);
        }

        public void OptionKind(Action<IDataReader> f)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.NewVehicleData.StyleRepository_Fetch_OptionKind_Single.txt";

            Query(f, queryName);
        }

        public void OptionKind(Action<IDataReader> f, int styleId)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.NewVehicleData.StyleRepository_Fetch_OptionKind_Bulk.txt";

            Query(f, queryName, new Parameter("StyleId", styleId, DbType.Int32));
        }

        public void OrderRule(Action<IDataReader> f, int styleId)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.NewVehicleData.StyleRepository_Fetch_OrderRules.txt";

            Query(f, queryName, new Parameter("StyleId", styleId, DbType.Int32));
        }

        public void Price(Action<IDataReader> f, int styleId)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.NewVehicleData.StyleRepository_Fetch_Prices.txt";

            Query(f, queryName, new Parameter("StyleId", styleId, DbType.Int32));
        }

        public void Standard(Action<IDataReader> f, int styleId)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.NewVehicleData.StyleRepository_Fetch_Standards.txt";

            Query(f, queryName, new Parameter("StyleId", styleId, DbType.Int32));
        }

        public void StandardHeader(Action<IDataReader> f)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.NewVehicleData.StyleRepository_Fetch_StandardHeaders.txt";

            Query(f, queryName);
        }

        public void Style(Action<IDataReader> f, int styleId)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.NewVehicleData.StyleRepository_Fetch_Style.txt";

            Query(f, queryName, new Parameter("StyleId", styleId, DbType.Int32));
        }

        public void Subdivision(Action<IDataReader> f, int subdivisionId)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.NewVehicleData.StyleRepository_Fetch_Subdivision.txt";

            Query(f, queryName, new Parameter("SubdivisionId", subdivisionId, DbType.Int32));
        }

        public void TechnicalSpecification(Action<IDataReader> f, int styleId)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.NewVehicleData.StyleRepository_Fetch_TechnicalSpecification.txt";

            Query(f, queryName, new Parameter("StyleId", styleId, DbType.Int32));
        }

        public void TechnicalSpecificationLabel(Action<IDataReader> f)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.NewVehicleData.StyleRepository_Fetch_TechnicalSpecificationLabel.txt";

            Query(f, queryName);
        }

        public void TechnicalSpecificationHeader(Action<IDataReader> f)
        {
            const string queryName = "FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Resources.NewVehicleData.StyleRepository_Fetch_TechnicalSpecificationHeader.txt";

            Query(f, queryName);
        }
    }

    public class DataSetNewVehicleDataStore : INewVehicleDataStore
    {
        private readonly DataSet _style;
        private readonly DataSet _reference;

        public DataSetNewVehicleDataStore(DataSet reference, DataSet style)
        {
            _reference = reference;
            _style = style;
        }

        public void BodyStyles(Action<IDataReader> f, int styleId)
        {
            f(_style.Tables["BodyStyle"].CreateDataReader());
        }

        public void Category(Action<IDataReader> f)
        {
            f(_reference.Tables["Category"].CreateDataReader());
        }

        public void CategoryHeader(Action<IDataReader> f)
        {
            f(_reference.Tables["CategoryHeader"].CreateDataReader());
        }

        public void CategoryAction(Action<IDataReader> f, int styleId)
        {
            f(_style.Tables["CategoryAction"].CreateDataReader());
        }

        public void Color(Action<IDataReader> f, int styleId)
        {
            f(_style.Tables["Color"].CreateDataReader());
        }

        public void ConsumerInformation(Action<IDataReader> f, int styleId)
        {
            f(_style.Tables["ConsumerInformation"].CreateDataReader());
        }

        public void ConsumerInformationLabel(Action<IDataReader> f)
        {
            f(_reference.Tables["ConsumerInformationLabel"].CreateDataReader());
        }

        public void ConsumerInformationHeader(Action<IDataReader> f)
        {
            f(_reference.Tables["ConsumerInformationHeader"].CreateDataReader());
        }

        public void Division(Action<IDataReader> f, int divisionId)
        {
            f(_style.Tables["Style"].CreateDataReader());
        }

        public void Jpg(Action<IDataReader> f, int styleId)
        {
            f(_style.Tables["Jpg"].CreateDataReader());
        }

        public void Manufacturer(Action<IDataReader> f, int manufacturerId)
        {
            f(_style.Tables["Manufacturer"].CreateDataReader());
        }

        public void Model(Action<IDataReader> f, int modelId)
        {
            f(_style.Tables["Model"].CreateDataReader());
        }

        public void Option(Action<IDataReader> f, int styleId)
        {
            f(_style.Tables["Option"].CreateDataReader());
        }

        public void OptionHeader(Action<IDataReader> f)
        {
            f(_reference.Tables["OptionHeader"].CreateDataReader());
        }

        public void OptionKind(Action<IDataReader> f)
        {
            f(_reference.Tables["OptionKind"].CreateDataReader());
        }

        public void OptionKind(Action<IDataReader> f, int styleId)
        {
            f(_style.Tables["OptionKind"].CreateDataReader());
        }

        public void OrderRule(Action<IDataReader> f, int styleId)
        {
            f(_style.Tables["OrderRule"].CreateDataReader());
        }

        public void Price(Action<IDataReader> f, int styleId)
        {
            f(_style.Tables["Price"].CreateDataReader());
        }

        public void Standard(Action<IDataReader> f, int styleId)
        {
            f(_style.Tables["Standard"].CreateDataReader());
        }

        public void StandardHeader(Action<IDataReader> f)
        {
            f(_reference.Tables["StandardHeader"].CreateDataReader());
        }

        public void Style(Action<IDataReader> f, int styleId)
        {
            f(_style.Tables["Style"].CreateDataReader());
        }

        public void Subdivision(Action<IDataReader> f, int subdivisionId)
        {
            f(_style.Tables["Subdivision"].CreateDataReader());
        }

        public void TechnicalSpecification(Action<IDataReader> f, int styleId)
        {
            f(_style.Tables["TechnicalSpecification"].CreateDataReader());
        }

        public void TechnicalSpecificationLabel(Action<IDataReader> f)
        {
            f(_reference.Tables["TechnicalSpecificationLabel"].CreateDataReader());
        }

        public void TechnicalSpecificationHeader(Action<IDataReader> f)
        {
            f(_reference.Tables["TechnicalSpecificationHeader"].CreateDataReader());
        }
    }
}
