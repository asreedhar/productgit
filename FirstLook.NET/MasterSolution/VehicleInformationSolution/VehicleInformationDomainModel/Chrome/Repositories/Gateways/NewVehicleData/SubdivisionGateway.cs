using System.Data;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.NewVehicleData
{
    public class SubdivisionGateway : GatewayBase
    {
        public Subdivision Fetch(int subdivisionId)
        {
            string key = CreateCacheKey(subdivisionId);

            Subdivision value = Cache.Get(key) as Subdivision;

            if (value == null)
            {
                Resolve<INewVehicleDataStore>().Subdivision(
                    delegate(IDataReader reader)
                        {
                            if (reader.Read())
                            {
                                value = new Subdivision(
                                    new DivisionGateway().Fetch(reader.GetInt32(reader.GetOrdinal("DivisionId"))),
                                    reader.GetString(reader.GetOrdinal("Name")),
                                    reader.GetInt32(reader.GetOrdinal("ModelYear")),
                                    reader.GetInt32(reader.GetOrdinal("Id")));

                                Cache.Add(
                                    key,
                                    value,
                                    CacheConstants.NoAbsoluteExpiration,
                                    CacheConstants.DefaultSlidingExpiration);
                            }
                        },
                    subdivisionId);
            }

            return value;
        }

        public void Freeze(int subdivisionId, DataSet dataSet)
        {
            DataTable table = dataSet.Tables.Add("Subdivision");

            Resolve<INewVehicleDataStore>().Subdivision(
                delegate(IDataReader reader)
                    {
                        table.Load(reader);
                    },
                subdivisionId);
        }
    }
}
