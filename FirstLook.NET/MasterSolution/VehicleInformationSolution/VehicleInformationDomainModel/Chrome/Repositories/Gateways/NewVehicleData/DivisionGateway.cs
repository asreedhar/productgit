using System.Data;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.Common.Core.Memento;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.NewVehicleData
{
    public class DivisionGateway : GatewayBase
    {
        public Division Fetch(int divisionId)
        {
            string key = CreateCacheKey(divisionId);

            Division division = Cache.Get(key) as Division;

            if (division == null)
            {
                Resolve<INewVehicleDataStore>().Division(
                    delegate(IDataReader reader)
                        {
                            if (reader.Read())
                            {
                                division = new Division(
                                    reader.GetString(reader.GetOrdinal("Name")),
                                    reader.GetInt32(reader.GetOrdinal("Id")),
                                    new ManufacturerGateway().Fetch(reader.GetInt32(reader.GetOrdinal("ManufacturerId"))));

                                Cache.Add(
                                    key,
                                    division,
                                    CacheConstants.NoAbsoluteExpiration,
                                    CacheConstants.DefaultSlidingExpiration);
                            }
                        },
                    divisionId);
            }

            return division;
        }

        public void Freeze(int divisionId, DataSet dataSet)
        {
            DataTable table = dataSet.Tables.Add("Division");

            Resolve<INewVehicleDataStore>().Division(
                delegate(IDataReader reader)
                {
                    table.Load(reader);
                },
                divisionId);
        }
    }
}
