using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.Common.Core.Memento;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.NewVehicleData
{
    public class ConsumerInformationGateway : GatewayBase
    {
        private readonly ConsumerInformationHeaderGateway _headers = new ConsumerInformationHeaderGateway();

        private readonly ConsumerInformationLabelGateway _labels = new ConsumerInformationLabelGateway();

        public IList<ConsumerInformation> Fetch(int styleId)
        {
            string key = CreateCacheKey(styleId);

            IList<ConsumerInformation> values = Cache.Get(key) as IList<ConsumerInformation>;

            if (values == null)
            {
                INewVehicleDataStore store = Resolve<INewVehicleDataStore>();

                values = Fetch(styleId, store);

                values = new ReadOnlyCollection<ConsumerInformation>(values);

                Cache.Add(
                    key,
                    values,
                    CacheConstants.NoAbsoluteExpiration,
                    CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }

        public IList<ConsumerInformation> Fetch(int styleId, INewVehicleDataStore store)
        {
            if (store == null) return Fetch(styleId);

            IList<ConsumerInformation> values = new List<ConsumerInformation>();

            store.ConsumerInformation(
                delegate(IDataReader reader)
                    {
                        while (reader.Read())
                        {
                            ConsumerInformationLabelGateway.Row label = _labels.Fetch(
                                reader.GetInt32(reader.GetOrdinal("LabelID")));

                            ConsumerInformation consumerInformation = new ConsumerInformation(
                                _headers.Fetch(label.TypeId),
                                label.LabelId,
                                label.Label,
                                reader.GetString(reader.GetOrdinal("Value")));

                            values.Add(consumerInformation);
                        }
                    },
                styleId);

            return values;
        }

        public void Freeze(int styleId, DataSet dataSet)
        {
            DataTable table = dataSet.Tables.Add("ConsumerInformation");

            Resolve<INewVehicleDataStore>().ConsumerInformation(
                delegate(IDataReader reader)
                {
                    table.Load(reader);
                },
                styleId);
        }
    }
}