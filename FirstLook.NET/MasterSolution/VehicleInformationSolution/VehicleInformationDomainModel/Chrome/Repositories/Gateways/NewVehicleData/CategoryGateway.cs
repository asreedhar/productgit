using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.NewVehicleData
{
    public class CategoryGateway : GatewayBase
    {
        private readonly CategoryHeaderGateway _headers = new CategoryHeaderGateway();

        public Category Fetch(int categoryId)
        {
            string key = CreateCacheKey();

            Dictionary<int, Category> values = Cache.Get(key) as Dictionary<int, Category>;

            if (values == null)
            {
                INewVehicleDataStore store = Resolve<INewVehicleDataStore>();

                values = Fetch(store);

                Cache.Add(
                    key,
                    values,
                    CacheConstants.NoAbsoluteExpiration,
                    CacheConstants.DefaultSlidingExpiration);
            }

            if (values.ContainsKey(categoryId))
            {
                return values[categoryId];
            }

            return null;
        }

        public Category Fetch(int categoryId, INewVehicleDataStore store)
        {
            Dictionary<int, Category> values = Fetch(store);

            if (values.ContainsKey(categoryId))
            {
                return values[categoryId];
            }

            return null;
        }

        public void Freeze(DataSet dataSet)
        {
            DataTable table = dataSet.Tables.Add("Category");

            Resolve<INewVehicleDataStore>().Category(
                delegate(IDataReader reader)
                    {
                        table.Load(reader);
                    });
        }

        private Dictionary<int, Category> Fetch(INewVehicleDataStore store)
        {
            Dictionary<int, Category> values = new Dictionary<int, Category>();

            store.Category(
                delegate(IDataReader reader)
                {
                    while (reader.Read())
                    {
                        CategoryHeader header = _headers.Fetch(reader.GetOrdinal("CategoryHeaderID"));

                        string groupName = reader.IsDBNull(reader.GetOrdinal("GroupName"))
                                               ? string.Empty
                                               : reader.GetString(reader.GetOrdinal("GroupName"));

                        Category category = new Category(
                            groupName,
                            reader.GetString(reader.GetOrdinal("Name")),
                            reader.GetInt32(reader.GetOrdinal("Id")),
                            header);

                        if (!((IDictionary<int, Category>)values).ContainsKey(category.Id))
                        {
                            ((IDictionary<int, Category>)values).Add(category.Id, category);
                        }
                    }
                });

            return values;
        }
    }
}
