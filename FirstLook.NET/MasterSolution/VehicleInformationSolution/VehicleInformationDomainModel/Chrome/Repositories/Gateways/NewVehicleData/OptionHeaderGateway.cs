using System.Collections.Generic;
using System.Data;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.Common.Core.Memento;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.NewVehicleData
{
    public class OptionHeaderGateway : GatewayBase
    {
        public OptionHeader Fetch(int id)
        {
            IDictionary<int, OptionHeader> values = Load();

            if (values.ContainsKey(id))
            {
                return values[id];
            }

            return null;
        }

        public void Freeze(DataSet dataSet)
        {
            DataTable table = dataSet.Tables.Add("OptionHeader");

            Resolve<INewVehicleDataStore>().OptionHeader(
                delegate(IDataReader reader) { table.Load(reader); });
        }

        private Dictionary<int, OptionHeader> Load()
        {
            string key = CreateCacheKey();

            Dictionary<int, OptionHeader> values = Cache.Get(key) as Dictionary<int, OptionHeader>;

            if (values == null)
            {
                values = new Dictionary<int, OptionHeader>();

                Resolve<INewVehicleDataStore>().OptionHeader(
                    delegate(IDataReader reader)
                        {
                            while (reader.Read())
                            {
                                OptionHeader header = new OptionHeader(
                                    reader.GetString(reader.GetOrdinal("Name")),
                                    reader.GetInt32(reader.GetOrdinal("Id")));

                                values.Add(header.Id, header);
                            }
                        });

                Cache.Add(
                    key,
                    values,
                    CacheConstants.NoAbsoluteExpiration,
                    CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}