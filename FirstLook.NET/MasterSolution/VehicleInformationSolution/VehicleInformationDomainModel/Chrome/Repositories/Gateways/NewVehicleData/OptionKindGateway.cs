using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.Common.Core.Memento;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.NewVehicleData
{
    public class OptionKindGateway : GatewayBase
    {
        public OptionKind Lookup(int optionKindId)
        {
            IDictionary<int, OptionKind> values = Load();

            if (values.ContainsKey(optionKindId))
            {
                return values[optionKindId];
            }

            return null;
        }
        
        public IList<OptionKind> Fetch(int styleId)
        {
            string key = CreateCacheKey(styleId);

            IList<OptionKind> values = Cache.Get(key) as IList<OptionKind>;

            if (values == null)
            {
                INewVehicleDataStore store = Resolve<INewVehicleDataStore>();

                values = new ReadOnlyCollection<OptionKind>(Fetch(styleId, store));

                Cache.Add(
                    key,
                    values,
                    CacheConstants.NoAbsoluteExpiration,
                    CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }

        public IList<OptionKind> Fetch(int styleId, INewVehicleDataStore store)
        {
            if (store == null) return Fetch(styleId);

            List<OptionKind> values = new List<OptionKind>();

            store.OptionKind(
                delegate(IDataReader reader)
                    {
                        while (reader.Read())
                        {
                            OptionKind kind = Lookup(reader.GetInt32(reader.GetOrdinal("Id")));

                            values.Add(kind);
                        }
                    },
                styleId);

            return values;
        }

        public void Freeze(int styleId, DataSet dataSet)
        {
            DataTable table = dataSet.Tables.Add("OptionKind");

            Resolve<INewVehicleDataStore>().OptionKind(
                delegate(IDataReader reader)
                    {
                        table.Load(reader);
                    },
                styleId);
        }

        public void Freeze(DataSet dataSet)
        {
            DataTable table = dataSet.Tables.Add("OptionKind");

            Resolve<INewVehicleDataStore>().OptionKind(
                delegate(IDataReader reader)
                    {
                        table.Load(reader);
                    });
        }

        private IDictionary<int,OptionKind> Load()
        {
            string key = CreateCacheKey();

            IDictionary<int, OptionKind> values = Cache.Get(key) as IDictionary<int, OptionKind>;

            if (values == null)
            {
                values = new Dictionary<int, OptionKind>();

                Resolve<INewVehicleDataStore>().OptionKind(
                    delegate(IDataReader reader)
                        {
                            while (reader.Read())
                            {
                                OptionKind kind = new OptionKind(
                                    reader.GetString(reader.GetOrdinal("Name")),
                                    reader.GetInt32(reader.GetOrdinal("Id")));

                                values.Add(kind.Id, kind);
                            }
                        });

                Cache.Add(
                    key,
                    values,
                    CacheConstants.NoAbsoluteExpiration,
                    CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}
