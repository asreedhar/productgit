using System.Collections.Generic;
using System.Data;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.Common.Core.Memento;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.NewVehicleData
{
    public class CategoryHeaderGateway : GatewayBase
    {
        public CategoryHeader Fetch(int id)
        {
            Dictionary<int, CategoryHeader> values = Load();

            if (values.ContainsKey(id))
            {
                return values[id];
            }

            return null;
        }

        public void Freeze(DataSet dataSet)
        {
            DataTable table = dataSet.Tables.Add("CategoryHeader");

            Resolve<INewVehicleDataStore>().CategoryHeader(
                delegate(IDataReader reader) { table.Load(reader); });
        }

        private Dictionary<int, CategoryHeader> Load()
        {
            string key = CreateCacheKey();

            Dictionary<int, CategoryHeader> values = Cache.Get(key) as Dictionary<int, CategoryHeader>;

            if (values == null)
            {
                values = new Dictionary<int, CategoryHeader>();

                Resolve<INewVehicleDataStore>().CategoryHeader(
                    delegate(IDataReader reader)
                        {
                            while (reader.Read())
                            {
                                CategoryHeader header = new CategoryHeader(
                                    reader.GetString(reader.GetOrdinal("Name")),
                                    reader.GetInt32(reader.GetOrdinal("Id")));

                                values.Add(header.Id, header);
                            }
                        });

                Cache.Add(
                    key,
                    values,
                    CacheConstants.NoAbsoluteExpiration,
                    CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }
    }
}