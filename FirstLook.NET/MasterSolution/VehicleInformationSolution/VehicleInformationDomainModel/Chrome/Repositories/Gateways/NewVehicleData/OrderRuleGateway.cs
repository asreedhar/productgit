using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;
using FirstLook.Common.Core.Memento;
using FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Antlr;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.NewVehicleData
{
    public class OrderRuleGateway : GatewayBase
    {
        public IList<OrderRule> Fetch(int styleId)
        {
            string key = CreateCacheKey(styleId);

            IList<OrderRule> values = Cache.Get(key) as IList<OrderRule>;

            if (values == null)
            {
                INewVehicleDataStore store = Resolve<INewVehicleDataStore>();

                values = Fetch(styleId, store);

                values = new ReadOnlyCollection<OrderRule>(values);

                Cache.Add(
                    key,
                    values,
                    CacheConstants.NoAbsoluteExpiration,
                    CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }

        public IList<OrderRule> Fetch(int styleId, INewVehicleDataStore store)
        {
            if (store == null) return Fetch(styleId);

            IList<OrderRule> values = new List<OrderRule>();

            store.OrderRule(
                delegate(IDataReader reader)
                    {
                        OptionGateway lookup = new OptionGateway();

                        while (reader.Read())
                        {
                            string conditionText = reader.GetString(reader.GetOrdinal("Condition"));

                            ICondition condition = ConditionHelper.GetCondition(conditionText);

                            string logicOperatorText = reader.GetString(reader.GetOrdinal("LogicOperator"));

                            LogicOperator logicOperator = GetLogicOperator(logicOperatorText);

                            string optionCode = reader.GetString(reader.GetOrdinal("OptionCode"));

                            Option option = lookup.Lookup(styleId, optionCode, store);

                            if (option == null)
                            {
                                throw new Exception("fuckity fuxck fuck");
                            }

                            string operand = reader.GetString(reader.GetOrdinal("Operand"));

                            string[] operands = operand.Split(new char[] {','});

                            OrderRule rule = new OrderRule(
                                option,
                                logicOperator,
                                reader.GetBoolean(reader.GetOrdinal("IsConditional")),
                                condition);

                            foreach (string code in operands)
                            {
                                Option value = lookup.Lookup(styleId, code, store);

                                if (value != null)
                                {
                                    rule.Operand.Add(value); // the spec says ignore imaginary options
                                }
                            }

                            if (rule.Operand.Count > 0)
                            {
                                values.Add(rule);
                            }
                        }
                    },
                styleId);

            return values;
        }

        public void Freeze(int styleId, DataSet dataSet)
        {
            DataTable table = dataSet.Tables.Add("OrderRule");

            Resolve<INewVehicleDataStore>().OrderRule(
                delegate(IDataReader reader)
                {
                    table.Load(reader);
                },
                styleId);
        }

        private static LogicOperator GetLogicOperator(string st)
        {
            LogicOperator op = LogicOperator.NotDefined;

            switch (st)
            {
                case "R":
                    op = LogicOperator.Require;
                    break;
                case "M":
                    op = LogicOperator.MutualRequire;
                    break;
                case "O":
                    op = LogicOperator.OrRequire;
                    break;
                case "I":
                    op = LogicOperator.Include;
                    break;
                case "N":
                    op = LogicOperator.OrInclude;
                    break;
                case "X":
                    op = LogicOperator.Exclude;
                    break;
            }

            return op;
        }
    }
}
