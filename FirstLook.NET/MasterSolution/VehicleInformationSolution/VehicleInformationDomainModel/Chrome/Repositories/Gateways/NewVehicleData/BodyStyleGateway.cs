using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Gateways.NewVehicleData
{
    public class BodyStyleGateway : GatewayBase
    {
        public IList<BodyStyle> Fetch(int styleId)
        {
            string key = CreateCacheKey(styleId);

            IList<BodyStyle> values = Cache.Get(key) as IList<BodyStyle>;

            if (values == null)
            {
                INewVehicleDataStore store = Resolve<INewVehicleDataStore>();

                values = new ReadOnlyCollection<BodyStyle>(Fetch(styleId, store));

                Cache.Add(
                    key,
                    values,
                    CacheConstants.NoAbsoluteExpiration,
                    CacheConstants.DefaultSlidingExpiration);
            }

            return values;
        }

        public IList<BodyStyle> Fetch(int styleId, INewVehicleDataStore store)
        {
            if (store == null) return Fetch(styleId);

            List<BodyStyle> values = new List<BodyStyle>();

            store.BodyStyles(
                delegate(IDataReader reader)
                    {
                        while (reader.Read())
                        {
                            BodyStyle bodyStyle = new BodyStyle(
                                reader.GetBoolean(reader.GetOrdinal("IsPrimary")),
                                reader.GetString(reader.GetOrdinal("Name")));

                            values.Add(bodyStyle);
                        }
                    },
                styleId);

            return values;
        }

        public void Freeze(int styleId, DataSet dataSet)
        {
            DataTable table = dataSet.Tables.Add("BodyStyle");

            RegistryFactory.GetRegistry().Resolve<INewVehicleDataStore>().BodyStyles(
                delegate(IDataReader reader)
                    {
                        table.Load(reader);
                    },
                styleId);
        }
    }
}