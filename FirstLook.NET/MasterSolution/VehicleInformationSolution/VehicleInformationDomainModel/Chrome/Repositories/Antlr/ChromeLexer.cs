// $ANTLR 3.2 Sep 23, 2009 12:02:23 C:\\Users\\swenmouth\\Documents\\Chrome.g 2010-03-22 11:54:14

// The variable 'variable' is assigned but its value is never used.
#pragma warning disable 168, 219
// Unreachable code detected.
#pragma warning disable 162


using System;
using Antlr.Runtime;
using IList 		= System.Collections.IList;
using ArrayList 	= System.Collections.ArrayList;
using Stack 		= Antlr.Runtime.Collections.StackList;


namespace  FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Antlr 
{
public partial class ChromeLexer : Lexer {
    public const int AND = 5;
    public const int EOF = -1;
    public const int RPAREN = 9;
    public const int LPAREN = 8;
    public const int CODE = 7;
    public const int NOT = 6;
    public const int OR = 4;

    // delegates
    // delegators

    public ChromeLexer() 
    {
		InitializeCyclicDFAs();
    }
    public ChromeLexer(ICharStream input)
		: this(input, null) {
    }
    public ChromeLexer(ICharStream input, RecognizerSharedState state)
		: base(input, state) {
		InitializeCyclicDFAs(); 

    }
    
    override public string GrammarFileName
    {
    	get { return "C:\\Users\\swenmouth\\Documents\\Chrome.g";} 
    }

    // $ANTLR start "LPAREN"
    public void mLPAREN() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = LPAREN;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\swenmouth\\Documents\\Chrome.g:45:2: ( '(' )
            // C:\\Users\\swenmouth\\Documents\\Chrome.g:45:4: '('
            {
            	Match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "LPAREN"

    // $ANTLR start "RPAREN"
    public void mRPAREN() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = RPAREN;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\swenmouth\\Documents\\Chrome.g:47:2: ( ')' )
            // C:\\Users\\swenmouth\\Documents\\Chrome.g:47:4: ')'
            {
            	Match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "RPAREN"

    // $ANTLR start "AND"
    public void mAND() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = AND;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\swenmouth\\Documents\\Chrome.g:49:2: ( '&' )
            // C:\\Users\\swenmouth\\Documents\\Chrome.g:49:4: '&'
            {
            	Match('&'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "AND"

    // $ANTLR start "OR"
    public void mOR() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = OR;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\swenmouth\\Documents\\Chrome.g:51:2: ( ',' )
            // C:\\Users\\swenmouth\\Documents\\Chrome.g:51:4: ','
            {
            	Match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "OR"

    // $ANTLR start "NOT"
    public void mNOT() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = NOT;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\swenmouth\\Documents\\Chrome.g:53:2: ( '!' )
            // C:\\Users\\swenmouth\\Documents\\Chrome.g:53:4: '!'
            {
            	Match('!'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "NOT"

    // $ANTLR start "CODE"
    public void mCODE() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = CODE;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\swenmouth\\Documents\\Chrome.g:55:2: ( (~ ( LPAREN | RPAREN | AND | OR | NOT ) )+ )
            // C:\\Users\\swenmouth\\Documents\\Chrome.g:55:4: (~ ( LPAREN | RPAREN | AND | OR | NOT ) )+
            {
            	// C:\\Users\\swenmouth\\Documents\\Chrome.g:55:4: (~ ( LPAREN | RPAREN | AND | OR | NOT ) )+
            	int cnt1 = 0;
            	do 
            	{
            	    int alt1 = 2;
            	    int LA1_0 = input.LA(1);

            	    if ( ((LA1_0 >= '\u0000' && LA1_0 <= ' ') || (LA1_0 >= '\"' && LA1_0 <= '%') || LA1_0 == '\'' || (LA1_0 >= '*' && LA1_0 <= '+') || (LA1_0 >= '-' && LA1_0 <= '\uFFFF')) )
            	    {
            	        alt1 = 1;
            	    }


            	    switch (alt1) 
            		{
            			case 1 :
            			    // C:\\Users\\swenmouth\\Documents\\Chrome.g:55:5: ~ ( LPAREN | RPAREN | AND | OR | NOT )
            			    {
            			    	if ( (input.LA(1) >= '\u0000' && input.LA(1) <= ' ') || (input.LA(1) >= '\"' && input.LA(1) <= '%') || input.LA(1) == '\'' || (input.LA(1) >= '*' && input.LA(1) <= '+') || (input.LA(1) >= '-' && input.LA(1) <= '\uFFFF') ) 
            			    	{
            			    	    input.Consume();

            			    	}
            			    	else 
            			    	{
            			    	    MismatchedSetException mse = new MismatchedSetException(null,input);
            			    	    Recover(mse);
            			    	    throw mse;}


            			    }
            			    break;

            			default:
            			    if ( cnt1 >= 1 ) goto loop1;
            		            EarlyExitException eee1 =
            		                new EarlyExitException(1, input);
            		            throw eee1;
            	    }
            	    cnt1++;
            	} while (true);

            	loop1:
            		;	// Stops C# compiler whining that label 'loop1' has no statements


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "CODE"

    override public void mTokens() // throws RecognitionException 
    {
        // C:\\Users\\swenmouth\\Documents\\Chrome.g:1:8: ( LPAREN | RPAREN | AND | OR | NOT | CODE )
        int alt2 = 6;
        int LA2_0 = input.LA(1);

        if ( (LA2_0 == '(') )
        {
            alt2 = 1;
        }
        else if ( (LA2_0 == ')') )
        {
            alt2 = 2;
        }
        else if ( (LA2_0 == '&') )
        {
            alt2 = 3;
        }
        else if ( (LA2_0 == ',') )
        {
            alt2 = 4;
        }
        else if ( (LA2_0 == '!') )
        {
            alt2 = 5;
        }
        else if ( ((LA2_0 >= '\u0000' && LA2_0 <= ' ') || (LA2_0 >= '\"' && LA2_0 <= '%') || LA2_0 == '\'' || (LA2_0 >= '*' && LA2_0 <= '+') || (LA2_0 >= '-' && LA2_0 <= '\uFFFF')) )
        {
            alt2 = 6;
        }
        else 
        {
            NoViableAltException nvae_d2s0 =
                new NoViableAltException("", 2, 0, input);

            throw nvae_d2s0;
        }
        switch (alt2) 
        {
            case 1 :
                // C:\\Users\\swenmouth\\Documents\\Chrome.g:1:10: LPAREN
                {
                	mLPAREN(); 

                }
                break;
            case 2 :
                // C:\\Users\\swenmouth\\Documents\\Chrome.g:1:17: RPAREN
                {
                	mRPAREN(); 

                }
                break;
            case 3 :
                // C:\\Users\\swenmouth\\Documents\\Chrome.g:1:24: AND
                {
                	mAND(); 

                }
                break;
            case 4 :
                // C:\\Users\\swenmouth\\Documents\\Chrome.g:1:28: OR
                {
                	mOR(); 

                }
                break;
            case 5 :
                // C:\\Users\\swenmouth\\Documents\\Chrome.g:1:31: NOT
                {
                	mNOT(); 

                }
                break;
            case 6 :
                // C:\\Users\\swenmouth\\Documents\\Chrome.g:1:35: CODE
                {
                	mCODE(); 

                }
                break;

        }

    }


	private void InitializeCyclicDFAs()
	{
	}

 
    
}
}