
grammar Chrome;

options {
	language=CSharp2;
	output=AST;
	ASTLabelType=CommonTree;
}

@parser::namespace { FirstLook.Classification.Repositories.Antlr }

@lexer::namespace { FirstLook.Classification.Repositories.Antlr }

program
	: condition EOF
	;

condition
	: expression
	;

expression
	: logical_or_expression
	;

logical_or_expression
	: logical_and_expression (OR logical_and_expression)*
	;

logical_and_expression
	: unary_expression (AND unary_expression)*
	;

unary_expression
	: NOT unary_expression
	| CODE
	| parenthesized_expression
	;

parenthesized_expression
	: LPAREN expression RPAREN
	;

LPAREN
	: '(';
RPAREN
	: ')';
AND
	: '&';
OR
	: ',';
NOT
	: '!';
CODE
	: (~(LPAREN|RPAREN|AND|OR|NOT))+;
