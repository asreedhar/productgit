using System.IO;
using Antlr.Runtime;
using Antlr.Runtime.Tree;
using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;

namespace FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Antlr
{
    public static class ConditionHelper
    {
        public static ICondition GetCondition(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return null;
            }

            using (StringReader sr = new StringReader(text))
            {
                ANTLRReaderStream ar = new ANTLRReaderStream(sr);
                ChromeLexer lexer = new ChromeLexer(ar);
                CommonTokenStream tokens = new CommonTokenStream(lexer);
                ChromeParser parser = new ChromeParser(tokens);
                CommonTree t = (CommonTree)parser.program().Tree;
                CommonTreeNodeStream nodes = new CommonTreeNodeStream(t);
                ChromeTree tree = new ChromeTree(nodes);
                tree.program();
                return tree.Condition;
            }
        }
    }
}
