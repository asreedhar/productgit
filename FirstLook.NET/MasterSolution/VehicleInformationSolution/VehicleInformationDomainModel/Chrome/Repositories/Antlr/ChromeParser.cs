// $ANTLR 3.2 Sep 23, 2009 12:02:23 C:\\Users\\swenmouth\\Documents\\Chrome.g 2010-03-22 11:54:13

// The variable 'variable' is assigned but its value is never used.
#pragma warning disable 168, 219
// Unreachable code detected.
#pragma warning disable 162


using System;
using Antlr.Runtime;
using IList 		= System.Collections.IList;
using ArrayList 	= System.Collections.ArrayList;
using Stack 		= Antlr.Runtime.Collections.StackList;



using Antlr.Runtime.Tree;

namespace  FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Antlr 
{
public partial class ChromeParser : Parser
{
    public static readonly string[] tokenNames = new string[] 
	{
        "<invalid>", 
		"<EOR>", 
		"<DOWN>", 
		"<UP>", 
		"OR", 
		"AND", 
		"NOT", 
		"CODE", 
		"LPAREN", 
		"RPAREN"
    };

    public const int AND = 5;
    public const int EOF = -1;
    public const int RPAREN = 9;
    public const int LPAREN = 8;
    public const int CODE = 7;
    public const int NOT = 6;
    public const int OR = 4;

    // delegates
    // delegators



        public ChromeParser(ITokenStream input)
    		: this(input, new RecognizerSharedState()) {
        }

        public ChromeParser(ITokenStream input, RecognizerSharedState state)
    		: base(input, state) {
            InitializeCyclicDFAs();

             
        }
        
    protected ITreeAdaptor adaptor = new CommonTreeAdaptor();

    public ITreeAdaptor TreeAdaptor
    {
        get { return this.adaptor; }
        set {
    	this.adaptor = value;
    	}
    }

    override public string[] TokenNames {
		get { return ChromeParser.tokenNames; }
    }

    override public string GrammarFileName {
		get { return "C:\\Users\\swenmouth\\Documents\\Chrome.g"; }
    }


    public class program_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "program"
    // C:\\Users\\swenmouth\\Documents\\Chrome.g:14:1: program : condition EOF ;
    public ChromeParser.program_return program() // throws RecognitionException [1]
    {   
        ChromeParser.program_return retval = new ChromeParser.program_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        IToken EOF2 = null;
        ChromeParser.condition_return condition1 = default(ChromeParser.condition_return);


        CommonTree EOF2_tree=null;

        try 
    	{
            // C:\\Users\\swenmouth\\Documents\\Chrome.g:15:2: ( condition EOF )
            // C:\\Users\\swenmouth\\Documents\\Chrome.g:15:4: condition EOF
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_condition_in_program53);
            	condition1 = condition();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, condition1.Tree);
            	EOF2=(IToken)Match(input,EOF,FOLLOW_EOF_in_program55); 
            		EOF2_tree = (CommonTree)adaptor.Create(EOF2);
            		adaptor.AddChild(root_0, EOF2_tree);


            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "program"

    public class condition_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "condition"
    // C:\\Users\\swenmouth\\Documents\\Chrome.g:18:1: condition : expression ;
    public ChromeParser.condition_return condition() // throws RecognitionException [1]
    {   
        ChromeParser.condition_return retval = new ChromeParser.condition_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        ChromeParser.expression_return expression3 = default(ChromeParser.expression_return);



        try 
    	{
            // C:\\Users\\swenmouth\\Documents\\Chrome.g:19:2: ( expression )
            // C:\\Users\\swenmouth\\Documents\\Chrome.g:19:4: expression
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_expression_in_condition66);
            	expression3 = expression();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, expression3.Tree);

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "condition"

    public class expression_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "expression"
    // C:\\Users\\swenmouth\\Documents\\Chrome.g:22:1: expression : logical_or_expression ;
    public ChromeParser.expression_return expression() // throws RecognitionException [1]
    {   
        ChromeParser.expression_return retval = new ChromeParser.expression_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        ChromeParser.logical_or_expression_return logical_or_expression4 = default(ChromeParser.logical_or_expression_return);



        try 
    	{
            // C:\\Users\\swenmouth\\Documents\\Chrome.g:23:2: ( logical_or_expression )
            // C:\\Users\\swenmouth\\Documents\\Chrome.g:23:4: logical_or_expression
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_logical_or_expression_in_expression77);
            	logical_or_expression4 = logical_or_expression();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, logical_or_expression4.Tree);

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "expression"

    public class logical_or_expression_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "logical_or_expression"
    // C:\\Users\\swenmouth\\Documents\\Chrome.g:26:1: logical_or_expression : logical_and_expression ( OR logical_and_expression )* ;
    public ChromeParser.logical_or_expression_return logical_or_expression() // throws RecognitionException [1]
    {   
        ChromeParser.logical_or_expression_return retval = new ChromeParser.logical_or_expression_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        IToken OR6 = null;
        ChromeParser.logical_and_expression_return logical_and_expression5 = default(ChromeParser.logical_and_expression_return);

        ChromeParser.logical_and_expression_return logical_and_expression7 = default(ChromeParser.logical_and_expression_return);


        CommonTree OR6_tree=null;

        try 
    	{
            // C:\\Users\\swenmouth\\Documents\\Chrome.g:27:2: ( logical_and_expression ( OR logical_and_expression )* )
            // C:\\Users\\swenmouth\\Documents\\Chrome.g:27:4: logical_and_expression ( OR logical_and_expression )*
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_logical_and_expression_in_logical_or_expression88);
            	logical_and_expression5 = logical_and_expression();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, logical_and_expression5.Tree);
            	// C:\\Users\\swenmouth\\Documents\\Chrome.g:27:27: ( OR logical_and_expression )*
            	do 
            	{
            	    int alt1 = 2;
            	    int LA1_0 = input.LA(1);

            	    if ( (LA1_0 == OR) )
            	    {
            	        alt1 = 1;
            	    }


            	    switch (alt1) 
            		{
            			case 1 :
            			    // C:\\Users\\swenmouth\\Documents\\Chrome.g:27:28: OR logical_and_expression
            			    {
            			    	OR6=(IToken)Match(input,OR,FOLLOW_OR_in_logical_or_expression91); 
            			    		OR6_tree = (CommonTree)adaptor.Create(OR6);
            			    		adaptor.AddChild(root_0, OR6_tree);

            			    	PushFollow(FOLLOW_logical_and_expression_in_logical_or_expression93);
            			    	logical_and_expression7 = logical_and_expression();
            			    	state.followingStackPointer--;

            			    	adaptor.AddChild(root_0, logical_and_expression7.Tree);

            			    }
            			    break;

            			default:
            			    goto loop1;
            	    }
            	} while (true);

            	loop1:
            		;	// Stops C# compiler whining that label 'loop1' has no statements


            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "logical_or_expression"

    public class logical_and_expression_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "logical_and_expression"
    // C:\\Users\\swenmouth\\Documents\\Chrome.g:30:1: logical_and_expression : unary_expression ( AND unary_expression )* ;
    public ChromeParser.logical_and_expression_return logical_and_expression() // throws RecognitionException [1]
    {   
        ChromeParser.logical_and_expression_return retval = new ChromeParser.logical_and_expression_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        IToken AND9 = null;
        ChromeParser.unary_expression_return unary_expression8 = default(ChromeParser.unary_expression_return);

        ChromeParser.unary_expression_return unary_expression10 = default(ChromeParser.unary_expression_return);


        CommonTree AND9_tree=null;

        try 
    	{
            // C:\\Users\\swenmouth\\Documents\\Chrome.g:31:2: ( unary_expression ( AND unary_expression )* )
            // C:\\Users\\swenmouth\\Documents\\Chrome.g:31:4: unary_expression ( AND unary_expression )*
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_unary_expression_in_logical_and_expression106);
            	unary_expression8 = unary_expression();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, unary_expression8.Tree);
            	// C:\\Users\\swenmouth\\Documents\\Chrome.g:31:21: ( AND unary_expression )*
            	do 
            	{
            	    int alt2 = 2;
            	    int LA2_0 = input.LA(1);

            	    if ( (LA2_0 == AND) )
            	    {
            	        alt2 = 1;
            	    }


            	    switch (alt2) 
            		{
            			case 1 :
            			    // C:\\Users\\swenmouth\\Documents\\Chrome.g:31:22: AND unary_expression
            			    {
            			    	AND9=(IToken)Match(input,AND,FOLLOW_AND_in_logical_and_expression109); 
            			    		AND9_tree = (CommonTree)adaptor.Create(AND9);
            			    		adaptor.AddChild(root_0, AND9_tree);

            			    	PushFollow(FOLLOW_unary_expression_in_logical_and_expression111);
            			    	unary_expression10 = unary_expression();
            			    	state.followingStackPointer--;

            			    	adaptor.AddChild(root_0, unary_expression10.Tree);

            			    }
            			    break;

            			default:
            			    goto loop2;
            	    }
            	} while (true);

            	loop2:
            		;	// Stops C# compiler whining that label 'loop2' has no statements


            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "logical_and_expression"

    public class unary_expression_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "unary_expression"
    // C:\\Users\\swenmouth\\Documents\\Chrome.g:34:1: unary_expression : ( NOT unary_expression | CODE | parenthesized_expression );
    public ChromeParser.unary_expression_return unary_expression() // throws RecognitionException [1]
    {   
        ChromeParser.unary_expression_return retval = new ChromeParser.unary_expression_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        IToken NOT11 = null;
        IToken CODE13 = null;
        ChromeParser.unary_expression_return unary_expression12 = default(ChromeParser.unary_expression_return);

        ChromeParser.parenthesized_expression_return parenthesized_expression14 = default(ChromeParser.parenthesized_expression_return);


        CommonTree NOT11_tree=null;
        CommonTree CODE13_tree=null;

        try 
    	{
            // C:\\Users\\swenmouth\\Documents\\Chrome.g:35:2: ( NOT unary_expression | CODE | parenthesized_expression )
            int alt3 = 3;
            switch ( input.LA(1) ) 
            {
            case NOT:
            	{
                alt3 = 1;
                }
                break;
            case CODE:
            	{
                alt3 = 2;
                }
                break;
            case LPAREN:
            	{
                alt3 = 3;
                }
                break;
            	default:
            	    NoViableAltException nvae_d3s0 =
            	        new NoViableAltException("", 3, 0, input);

            	    throw nvae_d3s0;
            }

            switch (alt3) 
            {
                case 1 :
                    // C:\\Users\\swenmouth\\Documents\\Chrome.g:35:4: NOT unary_expression
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	NOT11=(IToken)Match(input,NOT,FOLLOW_NOT_in_unary_expression124); 
                    		NOT11_tree = (CommonTree)adaptor.Create(NOT11);
                    		adaptor.AddChild(root_0, NOT11_tree);

                    	PushFollow(FOLLOW_unary_expression_in_unary_expression126);
                    	unary_expression12 = unary_expression();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, unary_expression12.Tree);

                    }
                    break;
                case 2 :
                    // C:\\Users\\swenmouth\\Documents\\Chrome.g:36:4: CODE
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	CODE13=(IToken)Match(input,CODE,FOLLOW_CODE_in_unary_expression131); 
                    		CODE13_tree = (CommonTree)adaptor.Create(CODE13);
                    		adaptor.AddChild(root_0, CODE13_tree);


                    }
                    break;
                case 3 :
                    // C:\\Users\\swenmouth\\Documents\\Chrome.g:37:4: parenthesized_expression
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_parenthesized_expression_in_unary_expression136);
                    	parenthesized_expression14 = parenthesized_expression();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, parenthesized_expression14.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "unary_expression"

    public class parenthesized_expression_return : ParserRuleReturnScope
    {
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "parenthesized_expression"
    // C:\\Users\\swenmouth\\Documents\\Chrome.g:40:1: parenthesized_expression : LPAREN expression RPAREN ;
    public ChromeParser.parenthesized_expression_return parenthesized_expression() // throws RecognitionException [1]
    {   
        ChromeParser.parenthesized_expression_return retval = new ChromeParser.parenthesized_expression_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        IToken LPAREN15 = null;
        IToken RPAREN17 = null;
        ChromeParser.expression_return expression16 = default(ChromeParser.expression_return);


        CommonTree LPAREN15_tree=null;
        CommonTree RPAREN17_tree=null;

        try 
    	{
            // C:\\Users\\swenmouth\\Documents\\Chrome.g:41:2: ( LPAREN expression RPAREN )
            // C:\\Users\\swenmouth\\Documents\\Chrome.g:41:4: LPAREN expression RPAREN
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	LPAREN15=(IToken)Match(input,LPAREN,FOLLOW_LPAREN_in_parenthesized_expression147); 
            		LPAREN15_tree = (CommonTree)adaptor.Create(LPAREN15);
            		adaptor.AddChild(root_0, LPAREN15_tree);

            	PushFollow(FOLLOW_expression_in_parenthesized_expression149);
            	expression16 = expression();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, expression16.Tree);
            	RPAREN17=(IToken)Match(input,RPAREN,FOLLOW_RPAREN_in_parenthesized_expression151); 
            		RPAREN17_tree = (CommonTree)adaptor.Create(RPAREN17);
            		adaptor.AddChild(root_0, RPAREN17_tree);


            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "parenthesized_expression"

    // Delegated rules


	private void InitializeCyclicDFAs()
	{
	}

 

    public static readonly BitSet FOLLOW_condition_in_program53 = new BitSet(new ulong[]{0x0000000000000000UL});
    public static readonly BitSet FOLLOW_EOF_in_program55 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_expression_in_condition66 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_logical_or_expression_in_expression77 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_logical_and_expression_in_logical_or_expression88 = new BitSet(new ulong[]{0x0000000000000012UL});
    public static readonly BitSet FOLLOW_OR_in_logical_or_expression91 = new BitSet(new ulong[]{0x00000000000001C0UL});
    public static readonly BitSet FOLLOW_logical_and_expression_in_logical_or_expression93 = new BitSet(new ulong[]{0x0000000000000012UL});
    public static readonly BitSet FOLLOW_unary_expression_in_logical_and_expression106 = new BitSet(new ulong[]{0x0000000000000022UL});
    public static readonly BitSet FOLLOW_AND_in_logical_and_expression109 = new BitSet(new ulong[]{0x00000000000001C0UL});
    public static readonly BitSet FOLLOW_unary_expression_in_logical_and_expression111 = new BitSet(new ulong[]{0x0000000000000022UL});
    public static readonly BitSet FOLLOW_NOT_in_unary_expression124 = new BitSet(new ulong[]{0x00000000000001C0UL});
    public static readonly BitSet FOLLOW_unary_expression_in_unary_expression126 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_CODE_in_unary_expression131 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_parenthesized_expression_in_unary_expression136 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_LPAREN_in_parenthesized_expression147 = new BitSet(new ulong[]{0x00000000000001C0UL});
    public static readonly BitSet FOLLOW_expression_in_parenthesized_expression149 = new BitSet(new ulong[]{0x0000000000000200UL});
    public static readonly BitSet FOLLOW_RPAREN_in_parenthesized_expression151 = new BitSet(new ulong[]{0x0000000000000002UL});

}
}