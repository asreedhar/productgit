
tree grammar ChromeTree;

options {
	language=CSharp;
	tokenVocab=Chrome;
	ASTLabelType=CommonTree;
}

@namespace { FirstLook.Classification.Repositories.Antlr }

@treeparser::header {
using FirstLook.Classification.Model.Chrome.NewVehicleData;
}

@members {
	private ICondition _condition;
	public ICondition Condition {
		get { return _condition; }
	}
}

program
	: condition EOF
	;

condition
	: e=expression { _condition = $e.value; }
	;

expression returns [ICondition value]
	: l=logical_or_expression { $value = $l.value; }
	;

logical_or_expression returns [OrCondition value]
@init {
	$value = new OrCondition();
}
	: a=logical_and_expression { $value.Conditions.Add($a.value); }
		(OR b=logical_and_expression { $value.Conditions.Add($b.value); } )*
	;

logical_and_expression returns [AndCondition value]
@init {
	$value = new AndCondition();
}
	: a=unary_expression { $value.Conditions.Add($a.value); }
		(AND b=unary_expression { $value.Conditions.Add($b.value); } )*
	;

unary_expression returns [ICondition value]
	: NOT u=unary_expression { $value = new NotCondition($u.value); }
	| c=CODE { $value = new OptionCondition($c.text); }
	| p=parenthesized_expression { $value = $p.value; }
	;

parenthesized_expression returns [ICondition value]
	: LPAREN e=expression RPAREN { $value = $e.value; }
	;
