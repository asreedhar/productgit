// $ANTLR 3.2 Sep 23, 2009 12:02:23 C:\\Users\\swenmouth\\Documents\\ChromeTree.g 2010-03-22 12:28:50


using FirstLook.VehicleInformation.DomainModel.Chrome.Model.NewVehicleData;


using System;
using Antlr.Runtime;
using Antlr.Runtime.Tree;using IList 		= System.Collections.IList;
using ArrayList 	= System.Collections.ArrayList;
using Stack 		= Antlr.Runtime.Collections.StackList;


namespace  FirstLook.VehicleInformation.DomainModel.Chrome.Repositories.Antlr 
{
public class ChromeTree : TreeParser 
{
    public static readonly string[] tokenNames = new string[] 
	{
        "<invalid>", 
		"<EOR>", 
		"<DOWN>", 
		"<UP>", 
		"OR", 
		"AND", 
		"NOT", 
		"CODE", 
		"LPAREN", 
		"RPAREN"
    };

    public const int AND = 5;
    public const int EOF = -1;
    public const int RPAREN = 9;
    public const int LPAREN = 8;
    public const int CODE = 7;
    public const int NOT = 6;
    public const int OR = 4;

    // delegates
    // delegators



        public ChromeTree(ITreeNodeStream input)
    		: this(input, new RecognizerSharedState()) {
        }

        public ChromeTree(ITreeNodeStream input, RecognizerSharedState state)
    		: base(input, state) {
            InitializeCyclicDFAs();

             
       }
        

    override public string[] TokenNames {
		get { return ChromeTree.tokenNames; }
    }

    override public string GrammarFileName {
		get { return "C:\\Users\\swenmouth\\Documents\\ChromeTree.g"; }
    }


    	private ICondition _condition;
    	public ICondition Condition {
    		get { return _condition; }
    	}



    // $ANTLR start "program"
    // C:\\Users\\swenmouth\\Documents\\ChromeTree.g:23:1: program : condition EOF ;
    public void program() // throws RecognitionException [1]
    {   
        try 
    	{
            // C:\\Users\\swenmouth\\Documents\\ChromeTree.g:24:2: ( condition EOF )
            // C:\\Users\\swenmouth\\Documents\\ChromeTree.g:24:4: condition EOF
            {
            	PushFollow(FOLLOW_condition_in_program58);
            	condition();
            	state.followingStackPointer--;

            	Match(input,EOF,FOLLOW_EOF_in_program60); 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return ;
    }
    // $ANTLR end "program"


    // $ANTLR start "condition"
    // C:\\Users\\swenmouth\\Documents\\ChromeTree.g:27:1: condition : e= expression ;
    public void condition() // throws RecognitionException [1]
    {   
        ICondition e = null;


        try 
    	{
            // C:\\Users\\swenmouth\\Documents\\ChromeTree.g:28:2: (e= expression )
            // C:\\Users\\swenmouth\\Documents\\ChromeTree.g:28:4: e= expression
            {
            	PushFollow(FOLLOW_expression_in_condition73);
            	e = expression();
            	state.followingStackPointer--;

            	 _condition = e; 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return ;
    }
    // $ANTLR end "condition"


    // $ANTLR start "expression"
    // C:\\Users\\swenmouth\\Documents\\ChromeTree.g:31:1: expression returns [ICondition value] : l= logical_or_expression ;
    public ICondition expression() // throws RecognitionException [1]
    {   
        ICondition value = null;

        OrCondition l = null;


        try 
    	{
            // C:\\Users\\swenmouth\\Documents\\ChromeTree.g:32:2: (l= logical_or_expression )
            // C:\\Users\\swenmouth\\Documents\\ChromeTree.g:32:4: l= logical_or_expression
            {
            	PushFollow(FOLLOW_logical_or_expression_in_expression92);
            	l = logical_or_expression();
            	state.followingStackPointer--;

            	 value =  l; 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return value;
    }
    // $ANTLR end "expression"


    // $ANTLR start "logical_or_expression"
    // C:\\Users\\swenmouth\\Documents\\ChromeTree.g:35:1: logical_or_expression returns [OrCondition value] : a= logical_and_expression ( OR b= logical_and_expression )* ;
    public OrCondition logical_or_expression() // throws RecognitionException [1]
    {   
        OrCondition value = null;

        AndCondition a = null;

        AndCondition b = null;



        	value =  new OrCondition();

        try 
    	{
            // C:\\Users\\swenmouth\\Documents\\ChromeTree.g:39:2: (a= logical_and_expression ( OR b= logical_and_expression )* )
            // C:\\Users\\swenmouth\\Documents\\ChromeTree.g:39:4: a= logical_and_expression ( OR b= logical_and_expression )*
            {
            	PushFollow(FOLLOW_logical_and_expression_in_logical_or_expression116);
            	a = logical_and_expression();
            	state.followingStackPointer--;

            	 value.Conditions.Add(a); 
            	// C:\\Users\\swenmouth\\Documents\\ChromeTree.g:40:3: ( OR b= logical_and_expression )*
            	do 
            	{
            	    int alt1 = 2;
            	    int LA1_0 = input.LA(1);

            	    if ( (LA1_0 == OR) )
            	    {
            	        alt1 = 1;
            	    }


            	    switch (alt1) 
            		{
            			case 1 :
            			    // C:\\Users\\swenmouth\\Documents\\ChromeTree.g:40:4: OR b= logical_and_expression
            			    {
            			    	Match(input,OR,FOLLOW_OR_in_logical_or_expression123); 
            			    	PushFollow(FOLLOW_logical_and_expression_in_logical_or_expression127);
            			    	b = logical_and_expression();
            			    	state.followingStackPointer--;

            			    	 value.Conditions.Add(b); 

            			    }
            			    break;

            			default:
            			    goto loop1;
            	    }
            	} while (true);

            	loop1:
            		;	// Stops C# compiler whining that label 'loop1' has no statements


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return value;
    }
    // $ANTLR end "logical_or_expression"


    // $ANTLR start "logical_and_expression"
    // C:\\Users\\swenmouth\\Documents\\ChromeTree.g:43:1: logical_and_expression returns [AndCondition value] : a= unary_expression ( AND b= unary_expression )* ;
    public AndCondition logical_and_expression() // throws RecognitionException [1]
    {   
        AndCondition value = null;

        ICondition a = null;

        ICondition b = null;



        	value =  new AndCondition();

        try 
    	{
            // C:\\Users\\swenmouth\\Documents\\ChromeTree.g:47:2: (a= unary_expression ( AND b= unary_expression )* )
            // C:\\Users\\swenmouth\\Documents\\ChromeTree.g:47:4: a= unary_expression ( AND b= unary_expression )*
            {
            	PushFollow(FOLLOW_unary_expression_in_logical_and_expression154);
            	a = unary_expression();
            	state.followingStackPointer--;

            	 value.Conditions.Add(a); 
            	// C:\\Users\\swenmouth\\Documents\\ChromeTree.g:48:3: ( AND b= unary_expression )*
            	do 
            	{
            	    int alt2 = 2;
            	    int LA2_0 = input.LA(1);

            	    if ( (LA2_0 == AND) )
            	    {
            	        alt2 = 1;
            	    }


            	    switch (alt2) 
            		{
            			case 1 :
            			    // C:\\Users\\swenmouth\\Documents\\ChromeTree.g:48:4: AND b= unary_expression
            			    {
            			    	Match(input,AND,FOLLOW_AND_in_logical_and_expression161); 
            			    	PushFollow(FOLLOW_unary_expression_in_logical_and_expression165);
            			    	b = unary_expression();
            			    	state.followingStackPointer--;

            			    	 value.Conditions.Add(b); 

            			    }
            			    break;

            			default:
            			    goto loop2;
            	    }
            	} while (true);

            	loop2:
            		;	// Stops C# compiler whining that label 'loop2' has no statements


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return value;
    }
    // $ANTLR end "logical_and_expression"


    // $ANTLR start "unary_expression"
    // C:\\Users\\swenmouth\\Documents\\ChromeTree.g:51:1: unary_expression returns [ICondition value] : ( NOT u= unary_expression | c= CODE | p= parenthesized_expression );
    public ICondition unary_expression() // throws RecognitionException [1]
    {   
        ICondition value = null;

        CommonTree c = null;
        ICondition u = null;

        ICondition p = null;


        try 
    	{
            // C:\\Users\\swenmouth\\Documents\\ChromeTree.g:52:2: ( NOT u= unary_expression | c= CODE | p= parenthesized_expression )
            int alt3 = 3;
            switch ( input.LA(1) ) 
            {
            case NOT:
            	{
                alt3 = 1;
                }
                break;
            case CODE:
            	{
                alt3 = 2;
                }
                break;
            case LPAREN:
            	{
                alt3 = 3;
                }
                break;
            	default:
            	    NoViableAltException nvae_d3s0 =
            	        new NoViableAltException("", 3, 0, input);

            	    throw nvae_d3s0;
            }

            switch (alt3) 
            {
                case 1 :
                    // C:\\Users\\swenmouth\\Documents\\ChromeTree.g:52:4: NOT u= unary_expression
                    {
                    	Match(input,NOT,FOLLOW_NOT_in_unary_expression185); 
                    	PushFollow(FOLLOW_unary_expression_in_unary_expression189);
                    	u = unary_expression();
                    	state.followingStackPointer--;

                    	 value =  new NotCondition(u); 

                    }
                    break;
                case 2 :
                    // C:\\Users\\swenmouth\\Documents\\ChromeTree.g:53:4: c= CODE
                    {
                    	c=(CommonTree)Match(input,CODE,FOLLOW_CODE_in_unary_expression198); 
                    	 value =  new OptionCondition(((c != null) ? c.Text : null)); 

                    }
                    break;
                case 3 :
                    // C:\\Users\\swenmouth\\Documents\\ChromeTree.g:54:4: p= parenthesized_expression
                    {
                    	PushFollow(FOLLOW_parenthesized_expression_in_unary_expression207);
                    	p = parenthesized_expression();
                    	state.followingStackPointer--;

                    	 value =  p; 

                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return value;
    }
    // $ANTLR end "unary_expression"


    // $ANTLR start "parenthesized_expression"
    // C:\\Users\\swenmouth\\Documents\\ChromeTree.g:57:1: parenthesized_expression returns [ICondition value] : LPAREN e= expression RPAREN ;
    public ICondition parenthesized_expression() // throws RecognitionException [1]
    {   
        ICondition value = null;

        ICondition e = null;


        try 
    	{
            // C:\\Users\\swenmouth\\Documents\\ChromeTree.g:58:2: ( LPAREN e= expression RPAREN )
            // C:\\Users\\swenmouth\\Documents\\ChromeTree.g:58:4: LPAREN e= expression RPAREN
            {
            	Match(input,LPAREN,FOLLOW_LPAREN_in_parenthesized_expression224); 
            	PushFollow(FOLLOW_expression_in_parenthesized_expression228);
            	e = expression();
            	state.followingStackPointer--;

            	Match(input,RPAREN,FOLLOW_RPAREN_in_parenthesized_expression230); 
            	 value =  e; 

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return value;
    }
    // $ANTLR end "parenthesized_expression"

    // Delegated rules


	private void InitializeCyclicDFAs()
	{
	}

 

    public static readonly BitSet FOLLOW_condition_in_program58 = new BitSet(new ulong[]{0x0000000000000000UL});
    public static readonly BitSet FOLLOW_EOF_in_program60 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_expression_in_condition73 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_logical_or_expression_in_expression92 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_logical_and_expression_in_logical_or_expression116 = new BitSet(new ulong[]{0x0000000000000012UL});
    public static readonly BitSet FOLLOW_OR_in_logical_or_expression123 = new BitSet(new ulong[]{0x00000000000001C0UL});
    public static readonly BitSet FOLLOW_logical_and_expression_in_logical_or_expression127 = new BitSet(new ulong[]{0x0000000000000012UL});
    public static readonly BitSet FOLLOW_unary_expression_in_logical_and_expression154 = new BitSet(new ulong[]{0x0000000000000022UL});
    public static readonly BitSet FOLLOW_AND_in_logical_and_expression161 = new BitSet(new ulong[]{0x00000000000001C0UL});
    public static readonly BitSet FOLLOW_unary_expression_in_logical_and_expression165 = new BitSet(new ulong[]{0x0000000000000022UL});
    public static readonly BitSet FOLLOW_NOT_in_unary_expression185 = new BitSet(new ulong[]{0x00000000000001C0UL});
    public static readonly BitSet FOLLOW_unary_expression_in_unary_expression189 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_CODE_in_unary_expression198 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_parenthesized_expression_in_unary_expression207 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_LPAREN_in_parenthesized_expression224 = new BitSet(new ulong[]{0x00000000000001C0UL});
    public static readonly BitSet FOLLOW_expression_in_parenthesized_expression228 = new BitSet(new ulong[]{0x0000000000000200UL});
    public static readonly BitSet FOLLOW_RPAREN_in_parenthesized_expression230 = new BitSet(new ulong[]{0x0000000000000002UL});

}
}