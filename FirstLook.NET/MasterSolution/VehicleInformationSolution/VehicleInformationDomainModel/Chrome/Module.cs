﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleInformation.DomainModel.Chrome
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<Commands.Module>();

            registry.Register<Repositories.Module>();
        }
    }
}
