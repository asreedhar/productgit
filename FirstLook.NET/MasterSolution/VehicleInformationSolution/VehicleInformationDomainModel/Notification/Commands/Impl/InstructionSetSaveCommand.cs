using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects.Instruction;
using FirstLook.VehicleInformation.DomainModel.Notification.Model;
using FirstLook.VehicleInformation.DomainModel.Specification.Model;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Commands.Impl
{
    [Serializable]
    public class InstructionSetSaveCommand : ICommand<InstructionSetDto, IdentityContextDto<InstructionSetDto>>
    {
        public InstructionSetDto Execute(IdentityContextDto<InstructionSetDto> parameters)
        {
            InstructionSetDto arguments = parameters.Arguments;

            IRegistry registry = RegistryFactory.GetRegistry();

            IVehicleRepository vehicleRepository = registry.Resolve<IVehicleRepository>();

            INotificationRepository repository = registry.Resolve<INotificationRepository>();

            SpecificationProvider provider = new SpecificationProvider(
                arguments.SpecificationProvider.Id,
                arguments.SpecificationProvider.Name);

            VehicleIdentification vehicle = vehicleRepository.Identification(arguments.Vehicle.Vin);

            SpecificationInstructionSet instructions = repository.Fetch(
                vehicle,
                provider);

            if (instructions == null)
            {
                instructions = new SpecificationInstructionSet();

                instructions.SpecificationProvider = provider;

                instructions.Vehicle = vehicle;
            }

            // (b) instructions

            instructions.Document = new XmlDocument();

            Serialize(instructions.Document, arguments.InstructionSet);

            // (c) summary

            instructions.CountInformation = arguments.CountInformation;
            instructions.CountCategories = arguments.CountCategories;
            instructions.CountOptions = arguments.CountOptions;

            // (d) associations

            if (arguments.StyleKey != null)
            {
                instructions.Style = new StyleKey(arguments.StyleKey.Id);
            }

            // save

            instructions = repository.Save(
                Principal.Get(parameters.Identity.Name, parameters.Identity.AuthorityName),
                instructions);

            // marshall

            return Mapper.Map(instructions);
        }

        private static void Serialize(XmlDocument document, InstructionSet instructions)
        {
            const string ns = @"http://xml.firstlook.biz/VSD/Instruction";

            if (string.IsInterned(ns) == null)
            {
                string.Intern(ns);
            }

            document.NameTable.Add(ns);

            XmlSerializer serializer = new XmlSerializer(typeof(InstructionSet));

            using (MemoryStream stream = new MemoryStream())
            {
                serializer.Serialize(stream, instructions);

                stream.Flush();

                stream.Seek(0, SeekOrigin.Begin);

                document.Load(stream);
            }
        }
    }
}