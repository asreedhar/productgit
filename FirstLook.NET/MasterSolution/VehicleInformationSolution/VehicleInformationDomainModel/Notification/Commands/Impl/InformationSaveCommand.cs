using System.IO;
using System.Xml;
using System.Xml.Serialization;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects.Information;
using FirstLook.VehicleInformation.DomainModel.Notification.Model;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Commands.Impl
{
    public class InformationSaveCommand : ICommand<InformationKeyDto, IdentityContextDto<InformationDto>>
    {
        public InformationKeyDto Execute(IdentityContextDto<InformationDto> parameters)
        {
            InformationDto arguments = parameters.Arguments;

            IResolver registry = RegistryFactory.GetResolver();

            IVehicleRepository vehicleRepository = registry.Resolve<IVehicleRepository>();

            VehicleIdentification vehicle = vehicleRepository.Identification(arguments.Vehicle.Vin);

            INotificationRepository notificationRepository = registry.Resolve<INotificationRepository>();

            SpecificationInformation information = new SpecificationInformation
            {
                SpecificationProvider = new SpecificationProvider(
                    arguments.SpecificationProvider.Id,
                    arguments.SpecificationProvider.Name),
                Vehicle = vehicle,
                Document = new XmlDocument()
            };

            Serialize(information.Document, arguments.Information);

            information = notificationRepository.Save(
                Principal.Get(parameters.Identity.Name, parameters.Identity.AuthorityName),
                information);

            InformationKeyDto key = new InformationKeyDto {Id = information.Id};

            return key;
        }

        private static void Serialize(XmlDocument document, Information information)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                new XmlSerializer(typeof (Information)).Serialize(stream, information);

                stream.Flush();

                stream.Seek(0, SeekOrigin.Begin);

                document.Load(stream);
            }
        }
    }
}