using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Notification.Model;
using FirstLook.VehicleInformation.DomainModel.Specification.Model;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Commands.Impl
{
    public class SpecificationAssociationCommand : ICommand<NullDto, IdentityContextDto<SpecificationAssociationDto>>
    {
        public NullDto Execute(IdentityContextDto<SpecificationAssociationDto> parameters)
        {
            SpecificationAssociationDto arguments = parameters.Arguments;

            IRegistry registry = RegistryFactory.GetRegistry();

            INotificationRepository repository = registry.Resolve<INotificationRepository>();

            AssociationToSpecification association = new AssociationToSpecification();

            association.SpecificationInstructionSet = new SpecificationInstructionSetKey(
                arguments.InstructionSetKey.Id,
                arguments.InstructionSetKey.DocumentVersion);

            association.Specification = new SpecificationKey(
                arguments.SpecificationKey.Id,
                arguments.SpecificationKey.Version);

            repository.Save(
                Principal.Get(parameters.Identity.Name, parameters.Identity.AuthorityName),
                association);

            return new NullDto();
        }
    }
}