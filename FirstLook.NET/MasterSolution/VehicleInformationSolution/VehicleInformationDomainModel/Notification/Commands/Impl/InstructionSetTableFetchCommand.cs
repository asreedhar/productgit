using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Notification.Model;
using FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Commands.Impl
{
    [Serializable]
    public class InstructionSetTableFetchCommand : ICommand<InstructionSetTableDto, IdentityContextDto<VehicleIdentificationDto>>
    {
        public InstructionSetTableDto Execute(IdentityContextDto<VehicleIdentificationDto> parameters)
        {
            VehicleIdentificationDto arguments = parameters.Arguments;

            IResolver resolver = RegistryFactory.GetResolver();

            INotificationRepository repository = resolver.Resolve<INotificationRepository>();

            VehicleIdentification identification = resolver.Resolve<IVehicleRepository>().Identification(arguments.Vin);

            IList<SpecificationInstructionSetSummary> list = repository.Fetch(identification);

            InstructionSetTableDto table = new InstructionSetTableDto();

            table.Vehicle = new VehicleIdentificationDto();

            table.Vehicle.Vin = arguments.Vin;

            foreach (SpecificationInstructionSetSummary item in list)
            {
                InstructionSetTableRowDto row = new InstructionSetTableRowDto();

                row.StyleIsIdentified = item.StyleIsIdentified;
                row.CountInformation = item.CountInformation;
                row.CountCategories = item.CountCategories;
                row.CountOptions = item.CountOptions;

                row.SpecificationProvider = new SpecificationProviderDto();
                row.SpecificationProvider.Id = item.SpecificationProvider.Id;
                row.SpecificationProvider.Name = item.SpecificationProvider.Name;

                row.StyleSummary = new StyleSummaryDto();

                Specification.Commands.Mapper.Map(item.StyleSummary, row.StyleSummary);

                table.Rows.Add(row);
            }

            return table;
        }
    }
}