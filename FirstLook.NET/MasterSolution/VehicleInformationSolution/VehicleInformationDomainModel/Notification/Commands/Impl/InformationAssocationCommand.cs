using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Notification.Model;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Commands.Impl
{
    public class InformationAssocationCommand : ICommand<NullDto, IdentityContextDto<InformationAssociationDto>>
    {
        public NullDto Execute(IdentityContextDto<InformationAssociationDto> parameters)
        {
            InformationAssociationDto arguments = parameters.Arguments;

            IRegistry registry = RegistryFactory.GetRegistry();

            INotificationRepository repository = registry.Resolve<INotificationRepository>();

            AssociationToInformation association = new AssociationToInformation();

            association.SpecificationInstructionSet = new SpecificationInstructionSetKey(
                arguments.InstructionSetKey.Id,
                arguments.InstructionSetKey.DocumentVersion);

            association.SpecificationInformation = new SpecificationInformationKey(
                arguments.InformationKey.Id);

            repository.Save(
                Principal.Get(parameters.Identity.Name, parameters.Identity.AuthorityName),
                association);
            
            return new NullDto();
        }
    }
}