using System;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Notification.Model;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Commands.Impl
{
    [Serializable]
    public class InstructionSetFetchCommand : ICommand<InstructionSetDto, IdentityContextDto<InstructionSetCriteriaDto>>
    {
        public InstructionSetDto Execute(IdentityContextDto<InstructionSetCriteriaDto> parameters)
        {
            InstructionSetCriteriaDto arguments = parameters.Arguments;

            IResolver resolver = RegistryFactory.GetResolver();

            INotificationRepository repository = resolver.Resolve<INotificationRepository>();

            SpecificationProvider provider = new SpecificationProvider(
                arguments.SpecificationProvider.Id,
                arguments.SpecificationProvider.Name);

            VehicleIdentification vehicle = resolver.Resolve<IVehicleRepository>().Identification(arguments.Vehicle.Vin);

            SpecificationInstructionSet instructions = repository.Fetch(vehicle, provider);

            if (instructions == null)
            {
                return null;
            }

            return Mapper.Map(instructions);
        }
    }
}