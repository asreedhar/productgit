using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Notification.Commands.Impl;
using FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Notification.Model;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Commands
{
    public class NotificationCommandFactory : CommandFactory, INotificationCommandFactory
    {
        public ICommand<InstructionSetTableDto, IdentityContextDto<VehicleIdentificationDto>> CreateInstructionSetTableFetchCommand()
        {
            return new InstructionSetTableFetchCommand();
        }

        public ICommand<InformationKeyDto, IdentityContextDto<InformationDto>> CreateInformationSaveCommand()
        {
            return new InformationSaveCommand();
        }

        public ICommand<InstructionSetDto, IdentityContextDto<InstructionSetDto>> CreateInstructionSetSaveCommand()
        {
            return new InstructionSetSaveCommand();
        }

        public ICommand<NullDto, IdentityContextDto<InformationAssociationDto>> CreateInformationAssocationCommand()
        {
            return new InformationAssocationCommand();
        }

        public ICommand<NullDto, IdentityContextDto<SpecificationAssociationDto>> CreateSpecificationAssocationCommand()
        {
            return new SpecificationAssociationCommand();
        }

        public ICommand<InstructionSetDto, IdentityContextDto<InstructionSetCriteriaDto>> CreateInstructionSetFetchCommand()
        {
            return new InstructionSetFetchCommand();
        }

        public override ICommandTransaction CreateTransactionCommand()
        {
            IRegistry registry = RegistryFactory.GetRegistry();

            INotificationRepository repository = registry.Resolve<INotificationRepository>();

            return new CommandTransaction(repository.TransactionFactory);
        }
    }
}