﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Commands
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<INotificationCommandFactory, NotificationCommandFactory>(ImplementationScope.Shared);
        }
    }
}
