using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Commands
{
    public interface INotificationCommandFactory : ICommandFactory
    {
        // 1. enumerate instruction set summary information in a table

        ICommand<InstructionSetTableDto, IdentityContextDto<VehicleIdentificationDto>> CreateInstructionSetTableFetchCommand();

        // 2. save information

        ICommand<InformationKeyDto, IdentityContextDto<InformationDto>> CreateInformationSaveCommand();

        // 3. create instruction set from information

        ICommand<InstructionSetDto, IdentityContextDto<InstructionSetDto>> CreateInstructionSetSaveCommand();

        // 4. associate instruction set with x, y, z

        ICommand<NullDto, IdentityContextDto<InformationAssociationDto>> CreateInformationAssocationCommand();

        ICommand<NullDto, IdentityContextDto<SpecificationAssociationDto>> CreateSpecificationAssocationCommand();

        // 5. load instruction set

        ICommand<InstructionSetDto, IdentityContextDto<InstructionSetCriteriaDto>> CreateInstructionSetFetchCommand();
    }
}