﻿using System;
using System.Xml;
using System.Xml.Serialization;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects.Instruction;
using FirstLook.VehicleInformation.DomainModel.Notification.Model;
using FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Commands
{
    public static class Mapper
    {
        public static InstructionSetDto Map(SpecificationInstructionSet instructions)
        {
            InstructionSetDto dto = new InstructionSetDto();

            dto.Vehicle = new VehicleIdentificationDto();
            dto.Vehicle.Vin = instructions.Vehicle.Vin;

            dto.SpecificationProvider = new SpecificationProviderDto();
            dto.SpecificationProvider.Id = instructions.SpecificationProvider.Id;
            dto.SpecificationProvider.Name = instructions.SpecificationProvider.Name;

            dto.InstructionSet = DeserialzeInstructions(instructions.Document);
            
            dto.CountInformation = instructions.CountInformation;
            dto.CountCategories = instructions.CountCategories;
            dto.CountOptions = instructions.CountOptions;

            if (instructions.Style != null)
            {
                dto.StyleKey = new StyleKeyDto();
                dto.StyleKey.Id = instructions.Style.Id;
            }

            return dto;
        }

        private static InstructionSet DeserialzeInstructions(XmlNode document)
        {
            using (XmlNodeReader reader = new XmlNodeReader(document))
            {
                const string ns = @"http://xml.firstlook.biz/VSD/Instruction";

                if (String.IsInterned(ns) == null)
                {
                    String.Intern(ns);
                }

                reader.NameTable.Add(ns);

                XmlSerializer ser = new XmlSerializer(typeof(InstructionSet));

                return (InstructionSet)ser.Deserialize(reader);
            }
        }
    }
}
