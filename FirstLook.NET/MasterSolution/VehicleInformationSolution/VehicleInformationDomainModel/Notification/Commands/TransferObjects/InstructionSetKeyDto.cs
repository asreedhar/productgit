using System;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects
{
    [Serializable]
    public class InstructionSetKeyDto
    {
        private int _id;
        private short _documentVersion;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public short DocumentVersion
        {
            get { return _documentVersion; }
            set { _documentVersion = value; }
        }
    }
}