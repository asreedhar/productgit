using System;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects
{
    [Serializable]
    public class InstructionSetSpecificationDto
    {
        private VehicleIdentificationDto _vehicle;
        private BrokerIdentificationDto _dealer;
        private SpecificationProviderDto _specificationProvider;
        private short _instructionSetVersion;
        private short _specificationVersion;

        // instructionsetkeydto
        // specificationkeydto

        public VehicleIdentificationDto Vehicle
        {
            get { return _vehicle; }
            set { _vehicle = value; }
        }

        public BrokerIdentificationDto Dealer
        {
            get { return _dealer; }
            set { _dealer = value; }
        }

        public SpecificationProviderDto SpecificationProvider
        {
            get { return _specificationProvider; }
            set { _specificationProvider = value; }
        }

        public short InstructionSetVersion
        {
            get { return _instructionSetVersion; }
            set { _instructionSetVersion = value; }
        }

        public short SpecificationVersion
        {
            get { return _specificationVersion; }
            set { _specificationVersion = value; }
        }
    }
}