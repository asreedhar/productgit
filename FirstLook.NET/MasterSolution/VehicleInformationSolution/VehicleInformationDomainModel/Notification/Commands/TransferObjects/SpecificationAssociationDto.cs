using System;
using FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects
{
    [Serializable]
    public class SpecificationAssociationDto
    {
        private SpecificationKeyDto _specificationKey;
        private InstructionSetKeyDto _instructionSetKey;

        public SpecificationKeyDto SpecificationKey
        {
            get { return _specificationKey; }
            set { _specificationKey = value; }
        }

        public InstructionSetKeyDto InstructionSetKey
        {
            get { return _instructionSetKey; }
            set { _instructionSetKey = value; }
        }
    }
}