using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects.Information
{
    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable()]
    [DebuggerStepThrough()]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/SpecificationMessage")]
    [XmlRoot(Namespace = "http://xml.firstlook.biz/VSD/SpecificationMessage", IsNullable = false)]
    public partial class Information
    {
        private InformationHead headField;

        private InformationBody bodyField;

        private decimal versionField;

        private bool versionFieldSpecified;

        /// <remarks/>
        public InformationHead Head
        {
            get { return headField; }
            set { headField = value; }
        }

        /// <remarks/>
        public InformationBody Body
        {
            get { return bodyField; }
            set { bodyField = value; }
        }

        /// <remarks/>
        [XmlAttribute()]
        public decimal Version
        {
            get { return versionField; }
            set { versionField = value; }
        }

        /// <remarks/>
        [XmlIgnore()]
        public bool VersionSpecified
        {
            get { return versionFieldSpecified; }
            set { versionFieldSpecified = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable()]
    [DebuggerStepThrough()]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/SpecificationMessage")]
    public partial class InformationHead
    {
        private Sender senderField;

        private DateTime sentField;

        /// <remarks/>
        public Sender Sender
        {
            get { return senderField; }
            set { senderField = value; }
        }

        /// <remarks/>
        public DateTime Sent
        {
            get { return sentField; }
            set { sentField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable()]
    [DebuggerStepThrough()]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/SpecificationMessage")]
    public partial class Sender
    {
        private int idField;

        private string nameField;

        private decimal versionField;

        /// <remarks/>
        public int Id
        {
            get { return idField; }
            set { idField = value; }
        }

        /// <remarks/>
        public string Name
        {
            get { return nameField; }
            set { nameField = value; }
        }

        /// <remarks/>
        public decimal Version
        {
            get { return versionField; }
            set { versionField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable()]
    [DebuggerStepThrough()]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/SpecificationMessage")]
    public partial class VehicleOption
    {
        private string optionCodeField;

        private string nameField;

        private bool isStandardField;

        private bool isStandardFieldSpecified;

        /// <remarks/>
        public string OptionCode
        {
            get { return optionCodeField; }
            set { optionCodeField = value; }
        }

        /// <remarks/>
        public string Name
        {
            get { return nameField; }
            set { nameField = value; }
        }

        /// <remarks/>
        [XmlAttribute()]
        public bool IsStandard
        {
            get { return isStandardField; }
            set { isStandardField = value; }
        }

        /// <remarks/>
        [XmlIgnore()]
        public bool IsStandardSpecified
        {
            get { return isStandardFieldSpecified; }
            set { isStandardFieldSpecified = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable()]
    [DebuggerStepThrough()]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/SpecificationMessage")]
    public partial class VehicleColor
    {
        private VehicleColorItem colorItemField;

        private bool colorItemFieldSpecified;

        private int colorRankField;

        private bool colorRankFieldSpecified;

        private string codeField;

        private string descriptionField;

        /// <remarks/>
        public VehicleColorItem ColorItem
        {
            get { return colorItemField; }
            set { colorItemField = value; }
        }

        /// <remarks/>
        [XmlIgnore()]
        public bool ColorItemSpecified
        {
            get { return colorItemFieldSpecified; }
            set { colorItemFieldSpecified = value; }
        }

        /// <remarks/>
        public int ColorRank
        {
            get { return colorRankField; }
            set { colorRankField = value; }
        }

        /// <remarks/>
        [XmlIgnore()]
        public bool ColorRankSpecified
        {
            get { return colorRankFieldSpecified; }
            set { colorRankFieldSpecified = value; }
        }

        /// <remarks/>
        public string Code
        {
            get { return codeField; }
            set { codeField = value; }
        }

        /// <remarks/>
        public string Description
        {
            get { return descriptionField; }
            set { descriptionField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable()]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/SpecificationMessage")]
    public enum VehicleColorItem
    {
        /// <remarks/>
        Interior,

        /// <remarks/>
        Exterior,
    }

    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable()]
    [DebuggerStepThrough()]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/SpecificationMessage")]
    public partial class VehicleDescription
    {
        private int modelYearField;

        private bool modelYearFieldSpecified;

        private string makeField;

        private string modelField;

        private string styleCodeField;

        private string styleNameField;

        private string bodyStyleField;

        private string trimField;

        private string engineField;

        private string transmissionField;

        private string driveTrainField;

        private string fuelTypeField;

        private string wheelBaseField;

        /// <remarks/>
        public int ModelYear
        {
            get { return modelYearField; }
            set { modelYearField = value; }
        }

        /// <remarks/>
        [XmlIgnore()]
        public bool ModelYearSpecified
        {
            get { return modelYearFieldSpecified; }
            set { modelYearFieldSpecified = value; }
        }

        /// <remarks/>
        public string Make
        {
            get { return makeField; }
            set { makeField = value; }
        }

        /// <remarks/>
        public string Model
        {
            get { return modelField; }
            set { modelField = value; }
        }

        /// <remarks/>
        public string StyleCode
        {
            get { return styleCodeField; }
            set { styleCodeField = value; }
        }

        /// <remarks/>
        public string StyleName
        {
            get { return styleNameField; }
            set { styleNameField = value; }
        }

        /// <remarks/>
        public string BodyStyle
        {
            get { return bodyStyleField; }
            set { bodyStyleField = value; }
        }

        /// <remarks/>
        public string Trim
        {
            get { return trimField; }
            set { trimField = value; }
        }

        /// <remarks/>
        public string Engine
        {
            get { return engineField; }
            set { engineField = value; }
        }

        /// <remarks/>
        public string Transmission
        {
            get { return transmissionField; }
            set { transmissionField = value; }
        }

        /// <remarks/>
        public string DriveTrain
        {
            get { return driveTrainField; }
            set { driveTrainField = value; }
        }

        /// <remarks/>
        public string FuelType
        {
            get { return fuelTypeField; }
            set { fuelTypeField = value; }
        }

        /// <remarks/>
        public string WheelBase
        {
            get { return wheelBaseField; }
            set { wheelBaseField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable()]
    [DebuggerStepThrough()]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/SpecificationMessage")]
    public partial class VehicleSpecification
    {
        private VehicleDescription descriptionField;

        private VehicleColorCollection colorsField;

        private VehicleOptionCollection optionsField;

        private string vinField;

        private bool isFleetField;

        private decimal versionField;

        /// <remarks/>
        public VehicleDescription Description
        {
            get { return descriptionField; }
            set { descriptionField = value; }
        }

        /// <remarks/>
        [XmlElement("Color")]
        public VehicleColorCollection Colors
        {
            get { return colorsField; }
            set { colorsField = value; }
        }

        /// <remarks/>
        [XmlElement("Option")]
        public VehicleOptionCollection Options
        {
            get { return optionsField; }
            set { optionsField = value; }
        }

        /// <remarks/>
        [XmlAttribute()]
        public string Vin
        {
            get { return vinField; }
            set { vinField = value; }
        }

        /// <remarks/>
        [XmlAttribute()]
        public bool IsFleet
        {
            get { return isFleetField; }
            set { isFleetField = value; }
        }

        /// <remarks/>
        [XmlAttribute()]
        public decimal Version
        {
            get { return versionField; }
            set { versionField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable()]
    [DebuggerStepThrough()]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/SpecificationMessage")]
    public partial class InformationBody
    {
        private VehicleSpecification vehicleSpecificationField;

        /// <remarks/>
        public VehicleSpecification VehicleSpecification
        {
            get { return vehicleSpecificationField; }
            set { vehicleSpecificationField = value; }
        }
    }

    public class VehicleColorCollection : CollectionBase
    {
        public VehicleColor this[int idx]
        {
            get { return ((VehicleColor) (base.InnerList[idx])); }
            set { base.InnerList[idx] = value; }
        }

        public int Add(VehicleColor value)
        {
            return base.InnerList.Add(value);
        }

        public void Remove(VehicleColor value)
        {
            base.InnerList.Remove(value);
        }

        public bool Contains(VehicleColor value)
        {
            return base.InnerList.Contains(value);
        }
    }

    public class VehicleOptionCollection : CollectionBase
    {
        public VehicleOption this[int idx]
        {
            get { return ((VehicleOption) (base.InnerList[idx])); }
            set { base.InnerList[idx] = value; }
        }

        public int Add(VehicleOption value)
        {
            return base.InnerList.Add(value);
        }

        public void Remove(VehicleOption value)
        {
            base.InnerList.Remove(value);
        }

        public bool Contains(VehicleOption value)
        {
            return base.InnerList.Contains(value);
        }
    }
}