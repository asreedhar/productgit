using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects
{
    [Serializable]
    public class InstructionSetTableDto
    {
        private VehicleIdentificationDto _vehicle;

        private List<InstructionSetTableRowDto> _rows = new List<InstructionSetTableRowDto>();

        public List<InstructionSetTableRowDto> Rows
        {
            get { return _rows; }
            set { _rows = value; }
        }

        public VehicleIdentificationDto Vehicle
        {
            get { return _vehicle; }
            set { _vehicle = value; }
        }
    }
}
