using System;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects
{
    [Serializable]
    public class InformationDto
    {
        private SpecificationProviderDto _specificationProvider;
        private VehicleIdentificationDto _vehicle;
        private Information.Information _information;

        public Information.Information Information
        {
            get { return _information; }
            set { _information = value; }
        }

        public SpecificationProviderDto SpecificationProvider
        {
            get { return _specificationProvider; }
            set { _specificationProvider = value; }
        }

        public VehicleIdentificationDto Vehicle
        {
            get { return _vehicle; }
            set { _vehicle = value; }
        }
    }
}