using System;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects.Instruction;
using FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects
{
    [Serializable]
    public class InstructionSetDto
    {
        private InstructionSetKeyDto _key;
        private VehicleIdentificationDto _vehicle;
        private SpecificationProviderDto _specificationProvider;
        private InstructionSet _instructionSet;
        private StyleKeyDto _styleKey;

        public InstructionSetKeyDto Key
        {
            get { return _key; }
            set { _key = value; }
        }

        public StyleKeyDto StyleKey
        {
            get { return _styleKey; }
            set { _styleKey = value; }
        }

        public VehicleIdentificationDto Vehicle
        {
            get { return _vehicle; }
            set { _vehicle = value; }
        }

        public SpecificationProviderDto SpecificationProvider
        {
            get { return _specificationProvider; }
            set { _specificationProvider = value; }
        }

        public InstructionSet InstructionSet
        {
            get { return _instructionSet; }
            set { _instructionSet = value; }
        }

        #region Summary Information

        private short _countInformation;
        private short _countCategories;
        private short _countOptions;

        public short CountInformation
        {
            get { return _countInformation; }
            set { _countInformation = value; }
        }

        public short CountCategories
        {
            get { return _countCategories; }
            set { _countCategories = value; }
        }

        public short CountOptions
        {
            get { return _countOptions; }
            set { _countOptions = value; }
        }

        #endregion
    }
}