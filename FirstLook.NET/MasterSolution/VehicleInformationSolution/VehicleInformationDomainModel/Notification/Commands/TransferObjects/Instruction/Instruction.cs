using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects.Instruction
{
    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable()]
    [DebuggerStepThrough()]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/Instruction")]
    [XmlRoot(Namespace = "http://xml.firstlook.biz/VSD/Instruction", IsNullable = false)]
    public partial class InstructionSet
    {
        private StyleCode styleCodeField;

        private BodyStyle bodyStyleField;

        private ColorComponentCollection colorComponentField;

        private CategoryCollection categoryField;

        private OptionCollection optionField;

        private decimal versionField;

        private bool versionFieldSpecified;

        /// <remarks/>
        public StyleCode StyleCode
        {
            get { return styleCodeField; }
            set { styleCodeField = value; }
        }

        /// <remarks/>
        public BodyStyle BodyStyle
        {
            get { return bodyStyleField; }
            set { bodyStyleField = value; }
        }

        /// <remarks/>
        [XmlElement("ColorComponent")]
        public ColorComponentCollection ColorComponents
        {
            get { return colorComponentField; }
            set { colorComponentField = value; }
        }

        /// <remarks/>
        [XmlElement("Category")]
        public CategoryCollection Categories
        {
            get { return categoryField; }
            set { categoryField = value; }
        }

        /// <remarks/>
        [XmlElement("Option")]
        public OptionCollection Options
        {
            get { return optionField; }
            set { optionField = value; }
        }

        /// <remarks/>
        [XmlAttribute()]
        public decimal Version
        {
            get { return versionField; }
            set { versionField = value; }
        }

        /// <remarks/>
        [XmlIgnore()]
        public bool VersionSpecified
        {
            get { return versionFieldSpecified; }
            set { versionFieldSpecified = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable()]
    [DebuggerStepThrough()]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/Instruction")]
    public partial class StyleCode
    {
        private string codeField;

        /// <remarks/>
        public string Code
        {
            get { return codeField; }
            set { codeField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable()]
    [DebuggerStepThrough()]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/Instruction")]
    public partial class Option
    {
        private string codeField;

        /// <remarks/>
        public string Code
        {
            get { return codeField; }
            set { codeField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable()]
    [DebuggerStepThrough()]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/Instruction")]
    public partial class Category
    {
        private int idField;

        /// <remarks/>
        public int Id
        {
            get { return idField; }
            set { idField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable()]
    [DebuggerStepThrough()]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/Instruction")]
    public partial class ColorComponent
    {
        private string codeField;

        /// <remarks/>
        public string Code
        {
            get { return codeField; }
            set { codeField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable()]
    [DebuggerStepThrough()]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/Instruction")]
    public partial class BodyStyle
    {
        private string nameField;

        /// <remarks/>
        public string Name
        {
            get { return nameField; }
            set { nameField = value; }
        }
    }

    public class ColorComponentCollection : CollectionBase
    {
        public ColorComponent this[int idx]
        {
            get { return ((ColorComponent) (base.InnerList[idx])); }
            set { base.InnerList[idx] = value; }
        }

        public int Add(ColorComponent value)
        {
            return base.InnerList.Add(value);
        }

        public void Remove(ColorComponent value)
        {
            base.InnerList.Remove(value);
        }

        public bool Contains(ColorComponent value)
        {
            return base.InnerList.Contains(value);
        }
    }

    public class CategoryCollection : CollectionBase
    {
        public Category this[int idx]
        {
            get { return ((Category) (base.InnerList[idx])); }
            set { base.InnerList[idx] = value; }
        }

        public int Add(Category value)
        {
            return base.InnerList.Add(value);
        }

        public void Remove(Category value)
        {
            base.InnerList.Remove(value);
        }

        public bool Contains(Category value)
        {
            return base.InnerList.Contains(value);
        }
    }

    public class OptionCollection : CollectionBase
    {
        public Option this[int idx]
        {
            get { return ((Option) (base.InnerList[idx])); }
            set { base.InnerList[idx] = value; }
        }

        public int Add(Option value)
        {
            return base.InnerList.Add(value);
        }

        public void Remove(Option value)
        {
            base.InnerList.Remove(value);
        }

        public bool Contains(Option value)
        {
            return base.InnerList.Contains(value);
        }
    }
}