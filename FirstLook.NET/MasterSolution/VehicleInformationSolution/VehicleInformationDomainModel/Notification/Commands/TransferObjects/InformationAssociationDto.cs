using System;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects
{
    [Serializable]
    public class InformationAssociationDto
    {
        private InformationKeyDto _informationKey;
        private InstructionSetKeyDto _instructionSetKey;

        public InformationKeyDto InformationKey
        {
            get { return _informationKey; }
            set { _informationKey = value; }
        }

        public InstructionSetKeyDto InstructionSetKey
        {
            get { return _instructionSetKey; }
            set { _instructionSetKey = value; }
        }
    }
}