using System;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects
{
    [Serializable]
    public class InstructionSetCriteriaDto
    {
        private VehicleIdentificationDto _vehicle;
        private SpecificationProviderDto _specificationProvider;

        public VehicleIdentificationDto Vehicle
        {
            get { return _vehicle; }
            set { _vehicle = value; }
        }

        public SpecificationProviderDto SpecificationProvider
        {
            get { return _specificationProvider; }
            set { _specificationProvider = value; }
        }
    }
}