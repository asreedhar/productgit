using System;
using FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects
{
    [Serializable]
    public class InstructionSetTableRowDto
    {
        private SpecificationProviderDto _specificationProvider;
        private StyleSummaryDto _styleSummary;
        private bool _styleIsIdentified;
        private int _countInformation;
        private int _countCategories;
        private int _countOptions;

        public SpecificationProviderDto SpecificationProvider
        {
            get { return _specificationProvider; }
            set { _specificationProvider = value; }
        }

        public StyleSummaryDto StyleSummary
        {
            get { return _styleSummary; }
            set { _styleSummary = value; }
        }

        public bool StyleIsIdentified
        {
            get { return _styleIsIdentified; }
            set { _styleIsIdentified = value; }
        }

        public int CountInformation
        {
            get { return _countInformation; }
            set { _countInformation = value; }
        }

        public int CountCategories
        {
            get { return _countCategories; }
            set { _countCategories = value; }
        }

        public int CountOptions
        {
            get { return _countOptions; }
            set { _countOptions = value; }
        }
    }
}