using System;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Commands.TransferObjects
{
    [Serializable]
    public class InformationKeyDto
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
    }
}