﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleInformation.DomainModel.Notification
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<Repositories.Module>();

            registry.Register<Commands.Module>();
        }
    }
}
