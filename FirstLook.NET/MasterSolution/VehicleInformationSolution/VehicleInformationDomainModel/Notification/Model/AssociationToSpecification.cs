using System;
using FirstLook.Common.Core;
using FirstLook.VehicleInformation.DomainModel.Specification.Model;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Model
{
    [Serializable]
    public class AssociationToSpecification : PropertyChangeSupport
    {
        private ISpecificationInstructionSetKey _specificationInstructionSet;

        public ISpecificationInstructionSetKey SpecificationInstructionSet
        {
            get { return _specificationInstructionSet; }
            set
            {
                if (!Equals(_specificationInstructionSet, value))
                {
                    Assignment(delegate { _specificationInstructionSet = value; }, "SpecificationInstructionSet");
                }
            }
        }

        private ISpecificationKey _specification;

        public ISpecificationKey Specification
        {
            get { return _specification; }
            set
            {
                if (!Equals(_specification, value))
                {
                    Assignment(delegate { _specification = value; }, "Specification");
                }
            }
        }
    }
}