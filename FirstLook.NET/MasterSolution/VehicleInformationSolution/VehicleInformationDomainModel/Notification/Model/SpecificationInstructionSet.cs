using System;
using System.Xml;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core;
using FirstLook.VehicleInformation.DomainModel.Specification.Model;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Model
{
    public interface ISpecificationInstructionSetKey
    {
        int Id { get; }

        short DocumentVersion { get; }
    }

    [Serializable]
    public class SpecificationInstructionSetKey : ISpecificationInstructionSetKey
    {
        private readonly int _id;
        private readonly short _documentVersion;

        public SpecificationInstructionSetKey(int id, short documentVersion)
        {
            _documentVersion = documentVersion;
            _id = id;
        }

        public int Id
        {
            get { return _id; }
        }

        public short DocumentVersion
        {
            get { return _documentVersion; }
        }
    }

    [Serializable]
    public class SpecificationInstructionSet : PropertyChangeSupport, ISpecificationInstructionSetKey
    {
        private SpecificationProvider _specificationProvider;
        private VehicleIdentification _vehicle;
        private XmlDocument _document;
        private short _countInformation;
        private short _countCategories;
        private short _countOptions;

        public virtual int Id
        {
            get { return 0; }
        }

        public virtual short DocumentVersion
        {
            get { return 0; }
        }

        public SpecificationProvider SpecificationProvider
        {
            get { return _specificationProvider; }
            set
            {
                if (!Equals(_specificationProvider, value))
                {
                    Assignment(delegate { _specificationProvider = value; }, "SpecificationProvider");
                }
            }
        }

        public VehicleIdentification Vehicle
        {
            get { return _vehicle; }
            set
            {
                if (!Equals(_vehicle, value))
                {
                    Assignment(delegate { _vehicle = value; }, "Vehicle");
                }
            }
        }

        public XmlDocument Document
        {
            get { return _document; }
            set
            {
                if (!AreSame(_document, value))
                {
                    Assignment(delegate { _document = value; }, "Document");
                }
            }
        }

        public short CountInformation
        {
            get { return _countInformation; }
            set
            {
                if (!Equals(_countInformation, value))
                {
                    Assignment(delegate { _countInformation = value; }, "CountInformation");
                }
            }
        }

        public short CountCategories
        {
            get { return _countCategories; }
            set
            {
                if (!Equals(_countCategories, value))
                {
                    Assignment(delegate { _countCategories = value; }, "CountCategories");
                }
            }
        }

        public short CountOptions
        {
            get { return _countOptions; }
            set
            {
                if (!Equals(_countOptions, value))
                {
                    Assignment(delegate { _countOptions = value; }, "CountOptions");
                }
            }
        }

        #region Associations

        private IStyleKey _style;
        
        public IStyleKey Style
        {
            get { return _style; }
            set
            {
                if (!Equals(_style, value))
                {
                    Assignment(delegate { _style = value; }, "Style");
                }
            }
        }

        #endregion
    }
}