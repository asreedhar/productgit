using System;
using FirstLook.Common.Core;
using FirstLook.VehicleInformation.DomainModel.Specification.Model;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Model
{
    [Serializable]
    public class SpecificationInstructionSetSummary : PropertyChangeSupport
    {
        private SpecificationProvider _specificationProvider;
        private StyleSummary _styleSummary;
        private bool _styleIsIdentified;
        private int _countInformation;
        private int _countCategories;
        private int _countOptions;

        public SpecificationProvider SpecificationProvider
        {
            get { return _specificationProvider; }
            set
            {
                if (!Equals(_specificationProvider, value))
                {
                    Assignment(delegate { _specificationProvider = value; }, "SpecificationProvider");
                }
            }
        }

        public StyleSummary StyleSummary
        {
            get { return _styleSummary; }
            set
            {
                if (!Equals(_styleSummary, value))
                {
                    Assignment(delegate { _styleSummary = value; }, "StyleSummary");
                }
            }
        }

        public bool StyleIsIdentified
        {
            get { return _styleIsIdentified; }
            set
            {
                if (!Equals(_styleIsIdentified, value))
                {
                    Assignment(delegate { _styleIsIdentified = value; }, "StyleIsIdentified");
                }
            }
        }

        public int CountInformation
        {
            get { return _countInformation; }
            set
            {
                if (!Equals(_countInformation, value))
                {
                    Assignment(delegate { _countInformation = value; }, "CountInformation");
                }
            }
        }

        public int CountCategories
        {
            get { return _countCategories; }
            set
            {
                if (!Equals(_countCategories, value))
                {
                    Assignment(delegate { _countCategories = value; }, "CountCategories");
                }
            }
        }

        public int CountOptions
        {
            get { return _countOptions; }
            set
            {
                if (!Equals(_countOptions, value))
                {
                    Assignment(delegate { _countOptions = value; }, "CountOptions");
                }
            }
        }
    }
}
