using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Data;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Model
{
    public interface INotificationRepository
    {
        IList<SpecificationInstructionSetSummary> Fetch(VehicleIdentification vehicle);

        SpecificationInstructionSet Fetch(VehicleIdentification vehicle, SpecificationProvider provider);

        SpecificationInstructionSet Save(IPrincipal principal, SpecificationInstructionSet instructionSet);

        SpecificationInformation Save(IPrincipal principal, SpecificationInformation information);

        void Save(IPrincipal principal, AssociationToInformation association);

        void Save(IPrincipal principal, AssociationToSpecification association);

        StyleChange Save(IPrincipal principal, StyleChange styleChange);

        ITransactionFactory TransactionFactory { get; }
    }
}