using System;
using FirstLook.Common.Core;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Model
{
    [Serializable]
    public class AssociationToInformation : PropertyChangeSupport
    {
        private ISpecificationInstructionSetKey _specificationInstructionSet;

        public ISpecificationInstructionSetKey SpecificationInstructionSet
        {
            get { return _specificationInstructionSet; }
            set
            {
                if (!Equals(_specificationInstructionSet, value))
                {
                    Assignment(delegate { _specificationInstructionSet = value; }, "SpecificationInstructionSet");
                }
            }
        }

        private ISpecificationInformationKey _specificationInformation;

        public ISpecificationInformationKey SpecificationInformation
        {
            get { return _specificationInformation; }
            set
            {
                if (!Equals(_specificationInformation, value))
                {
                    Assignment(delegate { _specificationInformation = value; }, "SpecificationInformation");
                }
            }
        }
    }
}