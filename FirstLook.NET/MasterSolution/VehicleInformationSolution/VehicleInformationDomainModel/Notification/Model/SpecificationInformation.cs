using System;
using System.Xml;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Model
{
    public interface ISpecificationInformationKey
    {
        int Id { get; }
    }

    [Serializable]
    public class SpecificationInformationKey : ISpecificationInformationKey
    {
        private readonly int _id;

        public SpecificationInformationKey(int id)
        {
            _id = id;
        }

        public int Id
        {
            get { return _id; }
        }
    }

    [Serializable]
    public class SpecificationInformation : PropertyChangeSupport, ISpecificationInformationKey
    {
        private SpecificationProvider _specificationProvider;
        private VehicleIdentification _vehicle;
        private XmlDocument _document;

        public virtual int Id
        {
            get { return 0; }
        }

        public SpecificationProvider SpecificationProvider
        {
            get { return _specificationProvider; }
            set
            {
                if (!Equals(_specificationProvider, value))
                {
                    Assignment(delegate { _specificationProvider = value; }, "SpecificationProvider");
                }
            }
        }

        public VehicleIdentification Vehicle
        {
            get { return _vehicle; }
            set
            {
                if (!Equals(_vehicle, value))
                {
                    Assignment(delegate { _vehicle = value; }, "Vehicle");
                }
            }
        }

        public XmlDocument Document
        {
            get { return _document; }
            set
            {
                if (!AreSame(_document, value))
                {
                    Assignment(delegate { _document = value; }, "Document");
                }
            }
        }
    }
}