using System;
using System.Xml;
using FirstLook.Common.Core;
using FirstLook.VehicleInformation.DomainModel.Specification.Model;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Model
{
    [Serializable]
    public class StyleChange : PropertyChangeSupport
    {
        private StyleProvider _styleProvider;
        private XmlDocument _document;

        public StyleProvider StyleProvider
        {
            get { return _styleProvider; }
            set
            {
                if (!Equals(_styleProvider, value))
                {
                    Assignment(delegate { _styleProvider = value; }, "SpecificationProvider");
                }
            }
        }

        public XmlDocument Document
        {
            get { return _document; }
            set
            {
                if (!Equals(_document, value))
                {
                    Assignment(delegate { _document = value; }, "Document");
                }
            }
        }
    }
}