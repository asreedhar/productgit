using System;
using FirstLook.Common.Core;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Model
{
    [Serializable]
    public class SpecificationProvider : PropertyChangeSupport, IEquatable<SpecificationProvider>
    {
        private byte _id;
        private string _name;

        public SpecificationProvider()
        {
        }

        public SpecificationProvider(byte id, string name)
        {
            _id = id;
            _name = name;
        }

        public byte Id
        {
            get { return _id; }
            set
            {
                if (!Equals(_id, value))
                {
                    Assignment(delegate { _id = value; }, "Id");
                }
            }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (!Equals(_name, value))
                {
                    Assignment(delegate { _name = value; }, "Name");
                }
            }
        }

        public bool Equals(SpecificationProvider other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other._id == _id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (SpecificationProvider)) return false;
            return Equals((SpecificationProvider) obj);
        }

        public override int GetHashCode()
        {
            return _id;
        }
    }
}