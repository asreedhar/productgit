using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;
using FirstLook.VehicleInformation.DomainModel.Notification.Model;
using FirstLook.VehicleInformation.DomainModel.Notification.Repositories.Mappers;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Repositories
{
    public class NotificationRepository : RepositoryBase, INotificationRepository
    {
        private readonly SpecificationInstructionSetMapper _mapperInstructionSet;

        private readonly SpecificationInformationMapper _mapperInformation;

        private readonly StyleChangeMapper _mapperChange;

        public NotificationRepository()
        {
            _mapperInstructionSet = new SpecificationInstructionSetMapper();

            _mapperInformation = new SpecificationInformationMapper();

            _mapperChange = new StyleChangeMapper();
        }

        protected override string DatabaseName
        {
            get { return "Vehicle"; }
        }

        public IList<SpecificationInstructionSetSummary> Fetch(VehicleIdentification vehicle)
        {
            return DoInSession(() => _mapperInstructionSet.Fetch(vehicle));
        }

        public SpecificationInstructionSet Fetch(VehicleIdentification vehicle, SpecificationProvider provider)
        {
            return DoInSession(() => _mapperInstructionSet.Fetch(vehicle, provider));
        }

        public SpecificationInstructionSet Save(IPrincipal principal, SpecificationInstructionSet instructionSet)
        {
            return DoInTransaction(
                delegate(IDataSession session)
                {
                    Audit audit = session.Items["Audit"] as Audit;

                    if (audit == null)
                    {
                        audit = Resolve<IRepository>().Create(principal);

                        session.Items["Audit"] = audit;
                    }
                },
                () => _mapperInstructionSet.Save(instructionSet));
        }

        public SpecificationInformation Save(IPrincipal principal, SpecificationInformation information)
        {
            return DoInTransaction(
                delegate(IDataSession session)
                {
                    Audit audit = session.Items["Audit"] as Audit;

                    if (audit == null)
                    {
                        audit = Resolve<IRepository>().Create(principal);

                        session.Items["Audit"] = audit;
                    }
                },
                () => _mapperInformation.Save(information));
        }

        public void Save(IPrincipal principal, AssociationToInformation association)
        {
            DoInTransaction(
                delegate(IDataSession session)
                {
                    Audit audit = session.Items["Audit"] as Audit;

                    if (audit == null)
                    {
                        audit = Resolve<IRepository>().Create(principal);

                        session.Items["Audit"] = audit;
                    }
                },
                delegate
                {
                    _mapperInstructionSet.Save(association);

                    return new NullDto();
                });
        }

        public void Save(IPrincipal principal, AssociationToSpecification association)
        {
            DoInTransaction(
                delegate(IDataSession session)
                {
                    Audit audit = session.Items["Audit"] as Audit;

                    if (audit == null)
                    {
                        audit = Resolve<IRepository>().Create(principal);

                        session.Items["Audit"] = audit;
                    }
                },
                delegate
                {
                    _mapperInstructionSet.Save(association);

                    return new NullDto();
                });
        }

        public StyleChange Save(IPrincipal principal, StyleChange styleChange)
        {
            return DoInTransaction(
                delegate(IDataSession session)
                {
                    Audit audit = session.Items["Audit"] as Audit;

                    if (audit == null)
                    {
                        audit = Resolve<IRepository>().Create(principal);

                        session.Items["Audit"] = audit;
                    }
                },
                () => _mapperChange.Save(styleChange));
        }

        protected override void OnBeginTransaction(IDataSession session)
        {
            // nope ...
        }
    }
}