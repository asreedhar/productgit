using System;
using System.Xml;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.VehicleInformation.DomainModel.Notification.Model;
using FirstLook.VehicleInformation.DomainModel.Utility;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Repositories.Entities
{
    [Serializable]
    public class SpecificationInformationEntity : SpecificationInformation, ISavable
    {
        private static readonly ReadOnly ReadOnlyProperties = new ReadOnly(
            "SpecificationProvider",
            "Vehicle",
            "Document");

        private readonly int _id;

        public override int Id
        {
            get { return _id; }
        }

        public SpecificationInformationEntity()
        {
            ListenToEvents();
        }

        public SpecificationInformationEntity(int id, AuditRow auditRow,
                                              SpecificationProvider specificationProvider,
                                              VehicleIdentification vehicle,
                                              XmlDocument document
            )
        {
            SpecificationProvider = specificationProvider;
            Vehicle = vehicle;
            Document = document;

            _id = id;
            _auditRow = auditRow;
            _savable.MarkOld();

            ListenToEvents();
        }

        private void ListenToEvents()
        {
            base.PropertyChanging += ReadOnlyProperties.OnPropertyChanging;

            base.PropertyChanged += _savable.PropertyChanged;
        }

        #region ISavable

        private readonly Savable _savable = new Savable();

        private AuditRow _auditRow;

        public AuditRow AuditRow
        {
            get { return _auditRow; }
        }

        public bool IsNew
        {
            get { return _savable.IsNew; }
        }

        public bool IsDeleted
        {
            get { return _savable.IsDeleted; }
        }

        public bool IsDirty
        {
            get { return _savable.IsDirty; }
        }

        public void MarkDeleted()
        {
            _savable.MarkDeleted();
        }

        public void OnSaved(object sender, SavedEventArgs e)
        {
            _auditRow = e.AuditRow;

            _savable.MarkOld();
        }

        #endregion
    }
}