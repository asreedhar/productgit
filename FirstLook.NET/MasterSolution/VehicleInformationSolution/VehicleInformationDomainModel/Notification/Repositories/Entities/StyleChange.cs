using System;
using System.Xml;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.VehicleInformation.DomainModel.Notification.Model;
using FirstLook.VehicleInformation.DomainModel.Specification.Model;
using FirstLook.VehicleInformation.DomainModel.Utility;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Repositories.Entities
{
    [Serializable]
    public class StyleChangeEntity : StyleChange, ISavable
    {
        private readonly int _id;

        public int Id
        {
            get { return _id; }
        }

        public StyleChangeEntity()
        {
            base.PropertyChanged += _savable.PropertyChanged;
        }

        public StyleChangeEntity(int id, AuditRow auditRow,
                                 StyleProvider styleProvider,
                                 XmlDocument document) : this()
        {
            StyleProvider = styleProvider;
            Document = document;

            _id = id;
            _auditRow = auditRow;
            _savable.MarkOld();

            base.PropertyChanging += ReadOnly.Immutable;
        }

        #region ISavable

        private readonly Savable _savable = new Savable();

        private AuditRow _auditRow;

        public AuditRow AuditRow
        {
            get { return _auditRow; }
        }

        public bool IsNew
        {
            get { return _savable.IsNew; }
        }

        public bool IsDeleted
        {
            get { return _savable.IsDeleted; }
        }

        public bool IsDirty
        {
            get { return _savable.IsDirty; }
        }

        public void MarkDeleted()
        {
            _savable.MarkDeleted();
        }

        public void OnSaved(object sender, SavedEventArgs e)
        {
            _auditRow = e.AuditRow;

            _savable.MarkOld();
        }

        #endregion
    }
}