using System;
using System.ComponentModel;
using System.Xml;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.VehicleInformation.DomainModel.Notification.Model;
using FirstLook.VehicleInformation.DomainModel.Specification.Model;
using FirstLook.VehicleInformation.DomainModel.Utility;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Repositories.Entities
{
    public class SpecificationInstructionSetIdentity
    {
        private readonly int _id;
        private readonly AuditRow _auditRow;

        public SpecificationInstructionSetIdentity(AuditRow auditRow, int id)
        {
            _auditRow = auditRow;
            _id = id;
        }

        public int Id
        {
            get { return _id; }
        }

        public AuditRow AuditRow
        {
            get { return _auditRow; }
        }
    }

    public class AssociationToStyleEntity : IStyleKey
    {
        private readonly int _id;
        private readonly AuditRow _auditRow;

        public AssociationToStyleEntity(int id, AuditRow auditRow)
        {
            _auditRow = auditRow;
            _id = id;
        }

        public int Id
        {
            get { return _id; }
        }

        public AuditRow AuditRow
        {
            get { return _auditRow; }
        }

        public bool Equals(IStyleKey other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other.Id == _id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (IStyleKey)) return false;
            return Equals((IStyleKey) obj);
        }

        public override int GetHashCode()
        {
            return _id;
        }
    }

    [Serializable]
    public class SpecificationInstructionSetEntity : SpecificationInstructionSet, ISavable
    {
        private static readonly ReadOnly ReadOnlyProperties = new ReadOnly(
            "SpecificationProvider",
            "Vehicle");

        private readonly SpecificationInstructionSetIdentity _identity;

        private short _documentVersionCounter;
        private short _documentVersion;

        public SpecificationInstructionSetIdentity Identity
        {
            get { return _identity; }
        }

        public override int Id
        {
            get { return _identity.Id; }
        }

        public override short DocumentVersion
        {
            get
            {
                return _documentVersion;
            }
        }

        public SpecificationInstructionSetEntity()
        {
            ListenToEvents();
        }

        public SpecificationInstructionSetEntity(AuditRow auditRow, SpecificationInstructionSetIdentity identity,
                                                 SpecificationProvider specificationProvider,
                                                 VehicleIdentification vehicle,
                                                 XmlDocument document,
                                                 short documentVersion,
                                                 short countInformation,
                                                 short countCategories,
                                                 short countOptions,
                                                 IStyleKey style)
        {
            SpecificationProvider = specificationProvider;
            Vehicle = vehicle;
            Document = document;
            CountInformation = countInformation;
            CountCategories = countCategories;
            CountOptions = countOptions;

            Style = style;

            _identity = identity;
            _documentVersion = documentVersion;
            _auditRow = auditRow;
            _savable.MarkOld();

            ListenToEvents();
        }

        private void ListenToEvents()
        {
            base.PropertyChanging += ReadOnlyProperties.OnPropertyChanging;

            base.PropertyChanged += OnPropertyChanged;

            base.PropertyChanged += _savable.PropertyChanged;
        }

        void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (Equals(e.PropertyName, "Document"))
            {
                if (_documentVersionCounter++ == 0)
                {
                    _documentVersion++;
                }
            }
            else if (Equals(e.PropertyName, "Style"))
            {
                if (_documentVersionCounter == 0)
                {
                    throw new NotSupportedException("Style change must be preceded by change in document");
                }
            }
        }

        #region ISavable

        private readonly Savable _savable = new Savable();

        private AuditRow _auditRow;

        public AuditRow AuditRow
        {
            get { return _auditRow; }
        }

        public bool IsNew
        {
            get { return _savable.IsNew; }
        }

        public bool IsDeleted
        {
            get { return _savable.IsDeleted; }
        }

        public bool IsDirty
        {
            get { return _savable.IsDirty; }
        }

        public void MarkDeleted()
        {
            _savable.MarkDeleted();
        }

        public void OnSaved(object sender, SavedEventArgs e)
        {
            _auditRow = e.AuditRow;

            _savable.MarkOld();
        }

        #endregion
    }
}