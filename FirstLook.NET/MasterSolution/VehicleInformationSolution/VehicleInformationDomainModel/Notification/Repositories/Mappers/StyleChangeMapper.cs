using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.VehicleInformation.DomainModel.Notification.Model;
using FirstLook.VehicleInformation.DomainModel.Notification.Repositories.Entities;
using FirstLook.VehicleInformation.DomainModel.Notification.Repositories.Gateways;
using FirstLook.VehicleInformation.DomainModel.Utility;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Repositories.Mappers
{
    public class StyleChangeMapper : MapperBase
    {
        private readonly StyleChangeGateway _gateway;

        public StyleChangeMapper()
        {
            _gateway = new StyleChangeGateway();
        }

        public StyleChangeEntity Save(StyleChange change)
        {
            StyleChangeEntity entity = change as StyleChangeEntity;

            if (entity != null)
            {
                AuditRow auditRow = Insert();

                StyleChangeGateway.Row row = _gateway.Insert(
                    auditRow.Id,
                    change.StyleProvider.Id,
                    change.Document);

                entity = new StyleChangeEntity(
                    row.Id,
                    auditRow,
                    change.StyleProvider,
                    change.Document);
            }

            return entity;
        }
    }
}