using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.VehicleInformation.DomainModel.Notification.Model;
using FirstLook.VehicleInformation.DomainModel.Notification.Repositories.Entities;
using FirstLook.VehicleInformation.DomainModel.Notification.Repositories.Gateways;
using FirstLook.VehicleInformation.DomainModel.Utility;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Repositories.Mappers
{
    public class SpecificationInformationMapper : MapperBase
    {
        private readonly SpecificationInformationGateway _gatewayInformation;

        public SpecificationInformationMapper()
        {
            _gatewayInformation = new SpecificationInformationGateway();
        }

        public SpecificationInformationEntity Save(SpecificationInformation information)
        {
            SpecificationInformationEntity entity = information as SpecificationInformationEntity;

            if (entity == null)
            {
                AuditRow auditRow = Insert();

                SpecificationProvider provider = information.SpecificationProvider;

                SpecificationInformationGateway.Row row = _gatewayInformation.Insert(
                    auditRow.Id,
                    provider.Id,
                    information.Vehicle.Id,
                    information.Document);

                entity = new SpecificationInformationEntity(
                    row.Id,
                    auditRow,
                    provider,
                    information.Vehicle,
                    information.Document);
            }

            return entity;
        }
    }
}