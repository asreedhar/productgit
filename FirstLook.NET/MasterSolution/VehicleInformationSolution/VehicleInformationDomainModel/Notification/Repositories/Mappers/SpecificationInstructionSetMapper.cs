using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.VehicleInformation.DomainModel.Notification.Model;
using FirstLook.VehicleInformation.DomainModel.Notification.Repositories.Entities;
using FirstLook.VehicleInformation.DomainModel.Notification.Repositories.Gateways;
using FirstLook.VehicleInformation.DomainModel.Specification.Model;
using FirstLook.VehicleInformation.DomainModel.Specification.Repositories.Gateways;
using FirstLook.VehicleInformation.DomainModel.Specification.Repositories.Mappers;
using FirstLook.VehicleInformation.DomainModel.Utility;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Repositories.Mappers
{
    public class SpecificationInstructionSetMapper : MapperBase
    {
        private readonly SpecificationInstructionSetSpecificationGateway _gatewayInstructionSetSpecification;

        private readonly SpecificationInstructionSetInformationGateway _gatewayInstructionSetInformation;

        private readonly SpecificationInstructionSetStyleGateway _gatewayInstructionSetStyle;

        private readonly SpecificationInstructionSetBodyGateway _gatewayInstructionSetBody;

        private readonly SpecificationInstructionSetGateway _gatewayInstructionSet;

        private readonly SpecificationProviderGateway _gatewayProvider;

        private readonly StyleSummaryGateway _gatewayStyleSummary;

        private readonly StyleMapper _mapperStyle;

        public SpecificationInstructionSetMapper()
        {
            _gatewayInstructionSetSpecification = new SpecificationInstructionSetSpecificationGateway();

            _gatewayInstructionSetInformation = new SpecificationInstructionSetInformationGateway();

            _gatewayInstructionSetStyle = new SpecificationInstructionSetStyleGateway();

            _gatewayInstructionSetBody = new SpecificationInstructionSetBodyGateway();

            _gatewayInstructionSet = new SpecificationInstructionSetGateway();

            _gatewayStyleSummary = new StyleSummaryGateway();

            _gatewayProvider = new SpecificationProviderGateway();

            _mapperStyle = new StyleMapper();
        }

        public IList<SpecificationInstructionSetSummary> Fetch(VehicleIdentification vehicle)
        {
            IList<SpecificationInstructionSetGateway.SummaryRow> rows = _gatewayInstructionSet.Fetch(
                vehicle.Id);

            IList<SpecificationInstructionSetSummary> items = new List<SpecificationInstructionSetSummary>();

            foreach (SpecificationInstructionSetGateway.SummaryRow row in rows)
            {
                SpecificationInstructionSetSummary item = new SpecificationInstructionSetSummary();

                if (row.StyleId > 0)
                {
                    StyleSummaryGateway.Row summaryRow = _gatewayStyleSummary.Fetch(row.StyleId);

                    StyleSummary summary = new StyleSummary
                    {
                        ManufacturerName = summaryRow.ManufacturerName,
                        DivisionName = summaryRow.DivisionName,
                        SubdivisionName = summaryRow.SubdivisionName,
                        ModelName = summaryRow.ModelName,
                        StyleName = summaryRow.StyleName,
                        TrimName = summaryRow.TrimName,
                        BodyStyleName = summaryRow.BodyStyleName,
                        ModelYear = summaryRow.ModelYear
                    };

                    item.StyleSummary = summary;

                    item.StyleIsIdentified = true;
                }

                SpecificationProviderGateway.Row providerRow = _gatewayProvider.Fetch(row.SpecificationProviderId);

                SpecificationProvider provider = new SpecificationProvider(
                    providerRow.Id,
                    providerRow.Name);

                item.SpecificationProvider = provider;

                item.CountInformation = row.CountInformation;
                item.CountCategories = row.CountCategories;
                item.CountOptions = row.CountOptions;

                items.Add(item);
            }

            return items;
        }

        public SpecificationInstructionSetEntity Fetch(VehicleIdentification vehicle, SpecificationProvider provider)
        {
            SpecificationInstructionSetGateway.Row head = _gatewayInstructionSet.Fetch(vehicle.Id, provider.Id);

            if (head == null)
            {
                return null;
            }

            SpecificationInstructionSetBodyGateway.Row body = _gatewayInstructionSetBody.Fetch(head.Id);

            SpecificationInstructionSetStyleGateway.Row styleRow = _gatewayInstructionSetStyle.Fetch(
                body.Id,
                body.DocumentVersion);

            IStyle style = null;

            if (styleRow != null)
            {
                style = _mapperStyle.Fetch(new StyleKey(styleRow.StyleId));
            }

            SpecificationInstructionSetEntity entity = new SpecificationInstructionSetEntity(
                AuditRowGateway.Fetch(body.AuditRowId),
                new SpecificationInstructionSetIdentity(
                    AuditRowGateway.Fetch(head.AuditRowId),
                    head.Id),
                provider,
                vehicle,
                body.Document,
                body.DocumentVersion,
                body.CountInformation,
                body.CountCategories,
                body.CountOptions,
                style);

            return entity;
        }

        public SpecificationInstructionSetEntity Save(SpecificationInstructionSet instructionSet)
        {
            SpecificationProvider provider = instructionSet.SpecificationProvider;

            VehicleIdentification vehicle = instructionSet.Vehicle;

            SpecificationInstructionSetEntity entity = instructionSet as SpecificationInstructionSetEntity;

            IStyleKey style = instructionSet.Style;

            if (style != null && style.Id == 0)
            {
                style = null;
            }

            if (entity == null)
            {
                entity = Fetch(vehicle, provider);

                if (entity != null)
                {
                    entity.Document = instructionSet.Document;
                    entity.Style = style;

                    entity.CountInformation = entity.CountInformation;
                    entity.CountCategories = entity.CountCategories;
                    entity.CountOptions = entity.CountOptions;
                }
            }

            if (entity == null)
            {
                AuditRow headAudit = Insert();

                SpecificationInstructionSetGateway.Row head = _gatewayInstructionSet.Insert(
                    headAudit.Id,
                    vehicle.Id,
                    provider.Id);

                AuditRow bodyAudit = Insert();

                SpecificationInstructionSetBodyGateway.Row body = _gatewayInstructionSetBody.Insert(
                    bodyAudit.Id,
                    head.Id,
                    instructionSet.Document,
                    instructionSet.DocumentVersion,
                    style != null,
                    instructionSet.CountInformation,
                    instructionSet.CountCategories,
                    instructionSet.CountOptions);

                if (style != null)
                {
                    AuditRow styleAudit = Insert();

                    _gatewayInstructionSetStyle.Insert(
                        styleAudit.Id,
                        body.Id,
                        body.DocumentVersion,
                        style.Id);
                }

                entity = new SpecificationInstructionSetEntity(
                    bodyAudit,
                    new SpecificationInstructionSetIdentity(
                        AuditRowGateway.Fetch(head.AuditRowId),
                        head.Id),
                    provider,
                    vehicle,
                    body.Document,
                    body.DocumentVersion,
                    body.CountInformation,
                    body.CountCategories,
                    body.CountOptions,
                    style);
            }
            else
            {
                if (entity.IsDirty)
                {
                    AuditRow bodyAudit = Update(entity.AuditRow);

                    _gatewayInstructionSetBody.Insert(
                        bodyAudit.Id,
                        entity.Id,
                        entity.Document,
                        entity.DocumentVersion,
                        style != null,
                        entity.CountInformation,
                        entity.CountCategories,
                        entity.CountOptions);

                    if (style != null)
                    {
                        AuditRow styleAudit = Insert();

                        _gatewayInstructionSetStyle.Insert(
                            styleAudit.Id,
                            entity.Id,
                            entity.DocumentVersion,
                            style.Id);
                    }

                    entity.OnSaved(this, new SavedEventArgs(bodyAudit));
                }
            }

            return entity;
        }

        public void Save(AssociationToInformation association)
        {
            SpecificationInstructionSetInformationGateway.Row row = _gatewayInstructionSetInformation.Fetch(
                association.SpecificationInstructionSet.Id,
                association.SpecificationInstructionSet.DocumentVersion,
                association.SpecificationInformation.Id);

            if (row == null)
            {
                AuditRow audit = Insert();

                _gatewayInstructionSetInformation.Insert(
                    audit.Id,
                    association.SpecificationInstructionSet.Id,
                    association.SpecificationInstructionSet.DocumentVersion,
                    association.SpecificationInformation.Id);
            }
        }

        public void Save(AssociationToSpecification association)
        {
            SpecificationInstructionSetSpecificationGateway.Row row = _gatewayInstructionSetSpecification.Fetch(
                association.SpecificationInstructionSet.Id,
                association.SpecificationInstructionSet.DocumentVersion,
                association.Specification.Id,
                association.Specification.Version);

            if (row == null)
            {
                AuditRow audit = Insert();

                _gatewayInstructionSetSpecification.Insert(
                    audit.Id,
                    association.SpecificationInstructionSet.Id,
                    association.SpecificationInstructionSet.DocumentVersion,
                    association.Specification.Id,
                    association.Specification.Version);
            }
        }
    }
}