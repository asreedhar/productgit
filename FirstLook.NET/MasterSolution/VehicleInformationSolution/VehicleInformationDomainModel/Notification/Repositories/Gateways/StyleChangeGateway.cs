using System.Data;
using System.Xml;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Repositories.Gateways
{
    public class StyleChangeGateway : GatewayBase
    {
        public class Row
        {
            public int AuditRowId;
            public int Id;
            public byte StyleProviderId;
            public XmlDocument Document;
        }

        public Row Insert(int auditRowId, int styleProviderId, XmlDocument document)
        {
            Row row = null;

            Resolve<INotificationStore>().StyleChange_Insert(
                delegate(IDataReader reader)
                    {
                        if (reader.Read())
                        {
                            row = new Row();
                            row.AuditRowId = reader.GetInt32(reader.GetOrdinal("AuditRowId"));
                            row.Document = DataRecord.GetXmlDocument(reader, "Document");
                            row.Id = reader.GetInt32(reader.GetOrdinal("Id"));
                            row.StyleProviderId = reader.GetByte(reader.GetOrdinal("StyleProviderId"));
                        }
                    },
                auditRowId,
                styleProviderId,
                document);

            return row;
        }
    }
}