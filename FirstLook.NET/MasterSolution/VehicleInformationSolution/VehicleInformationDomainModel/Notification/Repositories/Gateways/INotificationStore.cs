using System;
using System.Data;
using System.Reflection;
using System.Xml;
using FirstLook.Common.Core.Data;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Repositories.Gateways
{
    public interface INotificationStore
    {
        void SpecificationInformation_Insert(Action<IDataReader> action, int auditRowId, byte specificationProviderId, int vehicleId, XmlDocument document);

        void SpecificationInstructionSet_Fetch(Action<IDataReader> action, int vehicleId);

        void SpecificationInstructionSet_Fetch(Action<IDataReader> action, int vehicleId, byte specificationProviderId);

        void SpecificationInstructionSet_Insert(Action<IDataReader> action, int auditRowId, byte specificationProviderId, int vehicleId);

        void SpecificationInstructionSet_Body_Fetch(Action<IDataReader> action, int id);

        void SpecificationInstructionSet_Body_Insert(int auditRowId, int id, XmlDocument document, short documentVersion, bool isStyleSpecified, int countInformation, int countCategories, int countOptions);

        void SpecificationInstructionSet_Style_Fetch(Action<IDataReader> action, int id, short documentVersion);

        void SpecificationInstructionSet_Style_Insert(int auditRowId, int id, short documentVersion, int styleId);

        void SpecificationInstructionSet_Specification_Fetch(Action<IDataReader> action, int specificationInstructionSetId, short documentVersion, int specificationId, short specificationVersion);

        void SpecificationInstructionSet_Specification_Insert(int auditRowId, int specificationInstructionSetId, short documentVersion, int specificationId, short specificationVersion);

        void SpecificationInstructionSet_Information_Fetch(Action<IDataReader> action, int specificationInstructionSetId, short documentVersion, object informationId);

        void SpecificationInstructionSet_Information_Insert(int auditRowId, int specificationInstructionSetId, short documentVersion, int informationId);

        void SpecificationProvider_Fetch(Action<IDataReader> action, int id);

        void StyleChange_Insert(Action<IDataReader> action, int auditRowId, int styleProviderId, XmlDocument document);
    }

    public class NotificationStore : SessionDataStore, INotificationStore
    {
        private const string Prefix = "FirstLook.VehicleInformation.DomainModel.Notification.Repositories.Resources.";
        
        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        public void SpecificationInformation_Insert(Action<IDataReader> action, int auditRowId, byte specificationProviderId, int vehicleId, XmlDocument document)
        {
            const string insert = Prefix + ".SpecificationInformationGateway_Insert.txt";

            NonQuery(insert,
                new Parameter("SpecificationProviderID", specificationProviderId, DbType.Byte),
                new Parameter("VehicleID", vehicleId, DbType.Int32),
                new Parameter("Document", DataRecord.ToXml(document), DbType.Xml),
                new Parameter("AuditRowID", auditRowId, DbType.Int32));

            const string fetch = Prefix + ".SpecificationInformationGateway_Fetch_Identity.txt";

            Query(action, fetch);
        }

        public void SpecificationInstructionSet_Fetch(Action<IDataReader> action, int vehicleId)
        {
            const string fetch = Prefix + ".SpecificationInstructionSetGateway_Fetch_Summary.txt";

            Query(action, fetch, new Parameter("VehicleID", vehicleId, DbType.Int32));
        }

        public void SpecificationInstructionSet_Fetch(Action<IDataReader> action, int vehicleId, byte specificationProviderId)
        {
            const string fetch = Prefix + ".SpecificationInstructionSetGateway_Fetch_NaturalKey.txt";

            Query(action, fetch,
                new Parameter("SpecificationProviderID", specificationProviderId, DbType.Byte),
                new Parameter("VehicleID", vehicleId, DbType.Int32));
        }

        public void SpecificationInstructionSet_Insert(Action<IDataReader> action, int auditRowId, byte specificationProviderId, int vehicleId)
        {
            const string insert = Prefix + ".SpecificationInstructionSetGateway_Insert.txt";

            NonQuery(insert,
                new Parameter("SpecificationProviderID", specificationProviderId, DbType.Byte),
                new Parameter("VehicleID", vehicleId, DbType.Int32),
                new Parameter("AuditRowID", auditRowId, DbType.Int32));

            const string fetch = Prefix + ".SpecificationInstructionSetGateway_Fetch_Identity.txt";

            Query(action, fetch);
        }

        public void SpecificationInstructionSet_Body_Fetch(Action<IDataReader> action, int id)
        {
            const string fetch = Prefix + ".SpecificationInstructionSetGateway_Body_Fetch_Id.txt";

            Query(action, fetch,
                new Parameter("SpecificationInstructionSetID", id, DbType.Int32));
        }

        public void SpecificationInstructionSet_Body_Insert(int auditRowId, int id, XmlDocument document, short documentVersion, bool isStyleSpecified, int countInformation, int countCategories, int countOptions)
        {
            const string insert = Prefix + ".SpecificationInstructionSetGateway_Body_Insert.txt";

            NonQuery(insert,
                new Parameter("SpecificationInstructionSetID", id, DbType.Byte),
                new Parameter("Document", DataRecord.ToXml(document), DbType.Xml),
                new Parameter("DocumentVersion", documentVersion, DbType.Int16),
                new Parameter("SummaryStyleIsIdentified", isStyleSpecified, DbType.Boolean),
                new Parameter("SummaryCountInformation", countInformation, DbType.Int32),
                new Parameter("SummaryCountCategories", countCategories, DbType.Int32),
                new Parameter("SummaryCountOptions", countOptions, DbType.Int32),
                new Parameter("AuditRowID", auditRowId, DbType.Int32));
        }

        public void SpecificationInstructionSet_Style_Fetch(Action<IDataReader> action, int id, short documentVersion)
        {
            const string fetch = Prefix + ".SpecificationInstructionSetGateway_Style_Fetch.txt";

            Query(action, fetch,
                  new Parameter("SpecificationInstructionSetID", id, DbType.Int32),
                  new Parameter("DocumentVersion", documentVersion, DbType.Int16));
        }

        public void SpecificationInstructionSet_Style_Insert(int auditRowId, int id, short documentVersion, int styleId)
        {
            const string insert = Prefix + ".SpecificationInstructionSetGateway_Style_Insert.txt";

            NonQuery(insert,
                     new Parameter("SpecificationInstructionSetID", id, DbType.Int32),
                     new Parameter("DocumentVersion", documentVersion, DbType.Int16),
                     new Parameter("StyleID", styleId, DbType.Boolean),
                     new Parameter("AuditRowID", auditRowId, DbType.Int32));
        }

        public void SpecificationInstructionSet_Specification_Fetch(Action<IDataReader> action, int specificationInstructionSetId, short documentVersion, int specificationId, short specificationVersion)
        {
            const string fetch = Prefix + ".SpecificationInstructionSetGateway_Specification_Fetch.txt";

            Query(action, fetch,
                  new Parameter("SpecificationInstructionSetID", specificationInstructionSetId, DbType.Int32),
                  new Parameter("DocumentVersion", documentVersion, DbType.Int16),
                  new Parameter("SpecificationID", specificationId, DbType.Int32),
                  new Parameter("SpecificationVersion", specificationVersion, DbType.Int16));
        }

        public void SpecificationInstructionSet_Specification_Insert(int auditRowId, int specificationInstructionSetId, short documentVersion, int specificationId, short specificationVersion)
        {
            const string insert = Prefix + ".SpecificationInstructionSetGateway_Specification_Insert.txt";

            NonQuery(insert,
                     new Parameter("SpecificationInstructionSetID", specificationInstructionSetId, DbType.Int32),
                     new Parameter("DocumentVersion", documentVersion, DbType.Int16),
                     new Parameter("SpecificationID", specificationId, DbType.Int32),
                     new Parameter("SpecificationVersion", specificationVersion, DbType.Int16),
                     new Parameter("AuditRowID", auditRowId, DbType.Int32));
        }

        public void SpecificationInstructionSet_Information_Fetch(Action<IDataReader> action, int specificationInstructionSetId, short documentVersion, object informationId)
        {
            const string fetch = Prefix + ".SpecificationInstructionSetGateway_SpecificationInformation_Fetch.txt";

            Query(action, fetch,
                  new Parameter("SpecificationInstructionSetID", specificationInstructionSetId, DbType.Int32),
                  new Parameter("DocumentVersion", documentVersion, DbType.Int16),
                  new Parameter("SpecificationInformationID", informationId, DbType.Int32));
        }

        public void SpecificationInstructionSet_Information_Insert(int auditRowId, int specificationInstructionSetId, short documentVersion, int informationId)
        {
            const string insert = Prefix + ".SpecificationInstructionSetGateway_SpecificationInformation_Insert.txt";

            NonQuery(insert,
                     new Parameter("SpecificationInstructionSetID", specificationInstructionSetId, DbType.Int32),
                     new Parameter("DocumentVersion", documentVersion, DbType.Int16),
                     new Parameter("SpecificationInformationID", informationId, DbType.Int32),
                     new Parameter("AuditRowID", auditRowId, DbType.Int32));
        }

        public void SpecificationProvider_Fetch(Action<IDataReader> action, int id)
        {
            const string fetch = Prefix + ".SpecificationProviderGateway_Fetch_Id.txt";

            Query(action, fetch,
                  new Parameter("SpecificationProviderID", id, DbType.Int32));
        }

        public void StyleChange_Insert(Action<IDataReader> action, int auditRowId, int styleProviderId, XmlDocument document)
        {
            const string insert = Prefix + ".StyleChangeGateway_Insert.txt";

            NonQuery(insert,
                     new Parameter("StyleProviderID", styleProviderId, DbType.Int32),
                     new Parameter("Document", DataRecord.ToXml(document), DbType.Xml),
                     new Parameter("AuditRowID", auditRowId, DbType.Int32));

            const string fetch = Prefix + ".StyleChangeGateway_Fetch_Identity.txt";

            Query(action, fetch);
        }
    }
}
