using System.Data;
using FirstLook.Common.Core;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Repositories.Gateways
{
    public class SpecificationProviderGateway : GatewayBase
    {
        public class Row
        {
            public byte Id;
            public string Name;
        }

        public Row Fetch(int id)
        {
            string key = CreateCacheKey(id);

            Row row = Cache.Get(key) as Row;

            if (row == null)
            {
                Resolve<INotificationStore>().SpecificationProvider_Fetch(
                    delegate(IDataReader reader)
                        {
                            if (reader.Read())
                            {
                                row = new Row();
                                row.Id = reader.GetByte(reader.GetOrdinal("SpecificationProviderId"));
                                row.Name = reader.GetString(reader.GetOrdinal("Name"));
                            }
                        },
                    id);

                Remember(key, row);
            }

            return row;
        }
    }
}