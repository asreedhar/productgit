using System.Collections.Generic;
using System.Data;
using System.Xml;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Repositories.Gateways
{
    public class SpecificationInstructionSetGateway : GatewayBase
    {
        public class Row
        {
            public int Id;
            public byte SpecificationProviderId;
            public int VehicleId;
            public int AuditRowId;
        }

        public class SummaryRow
        {
            public byte SpecificationProviderId;
            public int StyleId;
            public int CountInformation;
            public int CountCategories;
            public int CountOptions;
        }

        public IList<SummaryRow> Fetch(int vehicleId)
        {
            string key = CreateCacheKey(vehicleId);

            IList<SummaryRow> rows = Cache.Get(key) as IList<SummaryRow>;

            if (rows == null)
            {
                rows = new List<SummaryRow>();

                Resolve<INotificationStore>().SpecificationInstructionSet_Fetch(
                    delegate(IDataReader reader)
                    {
                        while (reader.Read())
                        {
                            rows.Add(Summary(reader));
                        }
                    },
                    vehicleId);

                Remember(key, rows);
            }

            return rows;
        }

        public Row Fetch(int vehicleId, byte specificationProviderId)
        {
            string key = CreateCacheKey(vehicleId, specificationProviderId);

            Row row = Cache.Get(key) as Row;

            if (row == null)
            {
                Resolve<INotificationStore>().SpecificationInstructionSet_Fetch(
                    delegate(IDataReader reader)
                    {
                        if (reader.Read())
                        {
                            row = Fetch(reader);
                        }
                    },
                    vehicleId,
                    specificationProviderId);

                Remember(key, row);
            }

            return row;
        }

        public Row Insert(int auditRowId, int vehicleId, byte specificationProviderId)
        {
            Row row = null;

            Resolve<INotificationStore>().SpecificationInstructionSet_Insert(
                delegate(IDataReader reader)
                {
                    if (reader.Read())
                    {
                        row = Fetch(reader);
                    }
                },
                auditRowId,
                specificationProviderId,
                vehicleId);

            return row;
        }

        private static SummaryRow Summary(IDataRecord record)
        {
            SummaryRow row = new SummaryRow();
            row.CountInformation = record.GetInt32(record.GetOrdinal("CountInformation"));
            row.CountCategories = record.GetInt32(record.GetOrdinal("CountCategories"));
            row.CountOptions = record.GetInt32(record.GetOrdinal("CountOptions"));
            row.SpecificationProviderId = record.GetByte(record.GetOrdinal("SpecificationProviderId"));
            row.StyleId = record.GetInt32(record.GetOrdinal("StyleId"));
            return row;
        }

        private static Row Fetch(IDataRecord record)
        {
            Row row = new Row();
            row.AuditRowId = record.GetInt32(record.GetOrdinal("AuditRowId"));
            row.Id = record.GetInt32(record.GetOrdinal("SpecificationInstructionSetId"));
            row.SpecificationProviderId = record.GetByte(record.GetOrdinal("SpecificationProviderId"));
            row.VehicleId = record.GetInt32(record.GetOrdinal("VehicleId"));
            return row;
        }
    }

    public class SpecificationInstructionSetBodyGateway : GatewayBase
    {
        public class Row
        {
            public int Id;
            public XmlDocument Document;
            public short DocumentVersion;
            public short CountInformation;
            public short CountCategories;
            public short CountOptions;
            public int AuditRowId;
        }

        public Row Fetch(int id)
        {
            string key = CreateCacheKey(id);

            Row row = Cache.Get(key) as Row;

            if (row == null)
            {
                Resolve<INotificationStore>().SpecificationInstructionSet_Body_Fetch(
                    delegate(IDataReader reader)
                    {
                        if (reader.Read())
                        {
                            row = Fetch(reader);
                        }
                    },
                    id);

                Remember(key, row);
            }

            return row;
        }

        public Row Insert(int auditRowId, int id, XmlDocument document, short documentVersion, bool isStyleSpecified, short countInformation, short countCategories, short countOptions)
        {
            Resolve<INotificationStore>().SpecificationInstructionSet_Body_Insert(
                auditRowId,
                id,
                document,
                documentVersion,
                isStyleSpecified,
                countInformation,
                countCategories,
                countOptions);

            Row row = new Row();
            row.AuditRowId = auditRowId;
            row.Document = document;
            row.DocumentVersion = documentVersion;
            row.Id = id;
            row.CountInformation = countInformation;
            row.CountCategories = countCategories;
            row.CountOptions = countOptions;

            return row;
        }

        private static Row Fetch(IDataRecord record)
        {
            Row row = new Row();
            row.AuditRowId = record.GetInt32(record.GetOrdinal("AuditRowId"));
            row.Document = DataRecord.GetXmlDocument(record, "Document");
            row.DocumentVersion = record.GetInt16(record.GetOrdinal("DocumentVersion"));
            row.Id = record.GetInt32(record.GetOrdinal("SpecificationInstructionSetId"));
            row.CountInformation = record.GetInt16(record.GetOrdinal("SummaryCountInformation"));
            row.CountCategories = record.GetInt16(record.GetOrdinal("SummaryCountCategories"));
            row.CountOptions = record.GetInt16(record.GetOrdinal("SummaryCountOptions"));
            return row;
        }
    }

    public class SpecificationInstructionSetStyleGateway : GatewayBase
    {
        public class Row
        {
            public int Id;
            public short DocumentVersion;
            public int StyleId;
            public int AuditRowId;
        }

        public Row Fetch(int id, short documentVersion)
        {
            string key = CreateCacheKey(id, documentVersion);

            Row row = Cache.Get(key) as Row;

            if (row == null)
            {
                Resolve<INotificationStore>().SpecificationInstructionSet_Style_Fetch(
                    delegate(IDataReader reader)
                    {
                        if (reader.Read())
                        {
                            row = Fetch(reader);
                        }
                    },
                    id,
                    documentVersion);

                Remember(key, row);
            }

            return row;
        }

        public Row Insert(int auditRowId, int id, short documentVersion, int styleId)
        {
            Resolve<INotificationStore>().SpecificationInstructionSet_Style_Insert(
                auditRowId,
                id,
                documentVersion,
                styleId);

            Row row = new Row();
            row.AuditRowId = auditRowId;
            row.DocumentVersion = documentVersion;
            row.Id = id;
            row.StyleId = styleId;

            return row;
        }

        private static Row Fetch(IDataRecord record)
        {
            Row row = new Row();
            row.AuditRowId = record.GetInt32(record.GetOrdinal("AuditRowId"));
            row.DocumentVersion = record.GetInt16(record.GetOrdinal("DocumentVersion"));
            row.Id = record.GetInt32(record.GetOrdinal("SpecificationInstructionSetId"));
            row.StyleId = record.GetInt32(record.GetOrdinal("StyleId"));
            return row;
        }
    }

    public class SpecificationInstructionSetSpecificationGateway : GatewayBase
    {
        public class Row
        {
            public int SpecificationInstructionSetId;
            public short DocumentVersion;
            public int SpecificationId;
            public short SpecificationVersion;
            public int AuditRowId;
        }

        public Row Fetch(
            int specificationInstructionSetId,
            short documentVersion,
            int specificationId,
            short specificationVersion)
        {
            string key = CreateCacheKey(specificationInstructionSetId, documentVersion, specificationId, specificationVersion);

            Row row = Cache.Get(key) as Row;

            if (row == null)
            {
                Resolve<INotificationStore>().SpecificationInstructionSet_Specification_Fetch(
                    delegate(IDataReader reader)
                    {
                        if (reader.Read())
                        {
                            row = Fetch(reader);
                        }
                    },
                    specificationInstructionSetId,
                    documentVersion,
                    specificationId,
                    specificationVersion);

                Remember(key, row);
            }

            return row;
        }

        public Row Insert(int auditRowId, int specificationInstructionSetId, short documentVersion, int specificationId, short specificationVersion)
        {
            Resolve<INotificationStore>().SpecificationInstructionSet_Specification_Insert(
                auditRowId,
                specificationInstructionSetId,
                documentVersion,
                specificationId,
                specificationVersion);

            Row row = new Row();
            row.AuditRowId = auditRowId;
            row.SpecificationInstructionSetId = specificationInstructionSetId;
            row.DocumentVersion = documentVersion;
            row.SpecificationId = specificationId;
            row.SpecificationVersion = specificationVersion;

            return row;
        }

        private static Row Fetch(IDataRecord record)
        {
            Row row = new Row();
            row.AuditRowId = record.GetInt32(record.GetOrdinal("AuditRowId"));
            row.SpecificationInstructionSetId = record.GetInt32(record.GetOrdinal("SpecificationInstructionSetId"));
            row.DocumentVersion = record.GetInt16(record.GetOrdinal("DocumentVersion"));
            row.SpecificationId = record.GetInt32(record.GetOrdinal("SpecificationId"));
            row.SpecificationVersion = record.GetInt16(record.GetOrdinal("SpecificationVersion"));
            return row;
        }
    }

    public class SpecificationInstructionSetInformationGateway : GatewayBase
    {
        public class Row
        {
            public int SpecificationInstructionSetId;
            public short DocumentVersion;
            public int SpecificationInformationId;
            public int AuditRowId;
        }

        public Row Fetch(int specificationInstructionSetId, short documentVersion, int informationId)
        {
            string key = CreateCacheKey(specificationInstructionSetId, documentVersion, informationId);

            Row row = Cache.Get(key) as Row;

            if (row == null)
            {
                Resolve<INotificationStore>().SpecificationInstructionSet_Information_Fetch(
                    delegate(IDataReader reader)
                        {
                            if (reader.Read())
                            {
                                row = Fetch(reader);
                            }
                        },
                    specificationInstructionSetId,
                    documentVersion,
                    informationId);

                Remember(key, row);
            }

            return row;
        }

        public Row Insert(int auditRowId, int specificationInstructionSetId, short documentVersion, int informationId)
        {
            Resolve<INotificationStore>().SpecificationInstructionSet_Information_Insert(
                auditRowId,
                specificationInstructionSetId,
                documentVersion,
                informationId);

            Row row = new Row();
            row.AuditRowId = auditRowId;
            row.SpecificationInstructionSetId = specificationInstructionSetId;
            row.DocumentVersion = documentVersion;
            row.SpecificationInformationId = informationId;

            return row;
        }

        private static Row Fetch(IDataRecord record)
        {
            Row row = new Row();
            row.AuditRowId = record.GetInt32(record.GetOrdinal("AuditRowId"));
            row.DocumentVersion = record.GetInt16(record.GetOrdinal("DocumentVersion"));
            row.SpecificationInformationId = record.GetInt32(record.GetOrdinal("SpecificationInformationId"));
            row.SpecificationInstructionSetId = record.GetInt32(record.GetOrdinal("SpecificationInstructionSetId"));
            return row;
        }
    }
}
