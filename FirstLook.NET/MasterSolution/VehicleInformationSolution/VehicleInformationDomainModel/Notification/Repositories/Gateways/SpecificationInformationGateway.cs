using System.Data;
using System.Xml;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Repositories.Gateways
{
    public class SpecificationInformationGateway : GatewayBase
    {
        public class Row
        {
            public int Id;
            public byte SpecificationProviderId;
            public int VehicleId;
            public XmlDocument Document;
            public int AuditRowId;
        }

        public Row Insert(int auditRowId, byte specificationProviderId, int vehicleId, XmlDocument document)
        {
            Row row = null;

            Resolve<INotificationStore>().SpecificationInformation_Insert(
                delegate(IDataReader reader)
                {
                    if (reader.Read())
                    {
                        row = Fetch(reader);
                    }
                },
                auditRowId,
                specificationProviderId,
                vehicleId,
                document);

            return row;
        }

        private static Row Fetch(IDataRecord record)
        {
            Row row = new Row();
            row.AuditRowId = record.GetInt32(record.GetOrdinal("AuditRowID"));
            row.Document = DataRecord.GetXmlDocument(record, "Document");
            row.Id = record.GetInt32(record.GetOrdinal("SpecificationInformationID"));
            row.SpecificationProviderId = record.GetByte(record.GetOrdinal("SpecificationProviderID"));
            row.VehicleId = record.GetInt32(record.GetOrdinal("VehicleID"));
            return row;
        }
    }
}