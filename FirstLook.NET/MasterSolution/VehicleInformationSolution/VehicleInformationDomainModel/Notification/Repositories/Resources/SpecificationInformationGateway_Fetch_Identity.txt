
SELECT  SpecificationInformationID ,
        SpecificationProviderID ,
        VehicleID ,
        Document ,
        AuditRowID
FROM    Notification.SpecificationInformation
WHERE   SpecificationInformationID = @@IDENTITY
