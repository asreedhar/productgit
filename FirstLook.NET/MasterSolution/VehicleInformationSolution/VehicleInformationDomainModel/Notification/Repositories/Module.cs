﻿using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Notification.Model;

namespace FirstLook.VehicleInformation.DomainModel.Notification.Repositories
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<INotificationRepository, NotificationRepository>(ImplementationScope.Shared);
        }
    }
}
