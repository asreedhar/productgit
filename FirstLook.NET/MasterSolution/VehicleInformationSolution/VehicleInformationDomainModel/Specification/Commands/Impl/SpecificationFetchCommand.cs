using System;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleInformation.DomainModel.Specification.Model;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Commands.Impl
{
    [Serializable]
    public class SpecificationFetchCommand : ICommand<SpecificationFetchResultsDto, IdentityContextDto<SpecificationFetchArgumentsDto>>
    {
        public SpecificationFetchResultsDto Execute(IdentityContextDto<SpecificationFetchArgumentsDto> parameters)
        {
            SpecificationFetchArgumentsDto arguments = parameters.Arguments;

            IBroker broker = null;

            if (arguments.Broker != null && !arguments.Broker.Handle.Equals(Guid.Empty))
            {
                broker = Broker.Get(
                    parameters.Identity.Name,
                    parameters.Identity.AuthorityName,
                    arguments.Broker.Handle);
            }
            

            IRegistry registry = RegistryFactory.GetRegistry();

            IVehicleRepository vehicleRepository = registry.Resolve<IVehicleRepository>();

            VehicleIdentification identification = vehicleRepository.Identification(arguments.Vehicle.Vin);

            ISpecificationRepository specificationRepository = registry.Resolve<ISpecificationRepository>();

            Model.Specification specification;

            if (broker == null)
            {
                specification = specificationRepository.Fetch(identification);
            }
            else
            {
                specification = specificationRepository.Fetch(
                    identification,
                    broker);
            }

            if (specification == null)
            {
                return null;
            }

            return new SpecificationFetchResultsDto
            {
                Arguments = arguments,
                Specification = Mapper.Map(broker, specification)
            };
        }
    }
}