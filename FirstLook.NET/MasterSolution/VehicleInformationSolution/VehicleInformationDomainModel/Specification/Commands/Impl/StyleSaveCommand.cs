using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Specification.Model;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Commands.Impl
{
    public class StyleSaveCommand : ICommand<StyleDto, IdentityContextDto<StyleDto>>
    {
        public StyleDto Execute(IdentityContextDto<StyleDto> parameters)
        {
            StyleDto arguments = parameters.Arguments;

            IRegistry registry = RegistryFactory.GetRegistry();

            ISpecificationRepository repository = registry.Resolve<ISpecificationRepository>();

            IStyle style = null;

            if (arguments.Key != null)
            {
                style = repository.Fetch(new StyleKey(arguments.Key.Id));
            }
            
            style = Mapper.Map(arguments, style);

            style = repository.Save(
                Principal.Get(parameters.Identity.Name, parameters.Identity.AuthorityName),
                style);

            return Mapper.Map(style);
        }
    }
}