using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects.Vehicle;
using FirstLook.VehicleInformation.DomainModel.Specification.Model;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Commands.Impl
{
    [Serializable]
    public class SpecificationSaveCommand : ICommand<SpecificationDto, IdentityContextDto<SpecificationDto>>
    {
        public SpecificationDto Execute(IdentityContextDto<SpecificationDto> parameters)
        {
            SpecificationDto arguments = parameters.Arguments;

            IBroker broker = Broker.Get(parameters.Identity.Name, parameters.Identity.AuthorityName, arguments.Dealer.Handle);

            IResolver resolver = RegistryFactory.GetResolver();

            IVehicleRepository vehicleRepository = resolver.Resolve<IVehicleRepository>();

            VehicleIdentification vehicle = vehicleRepository.Identification(arguments.Vehicle.Vin);

            ISpecificationRepository repository = resolver.Resolve<ISpecificationRepository>();

            Model.Specification specification;

            if (arguments.Dealer == null)
            {
                specification = repository.Fetch(vehicle);
            }
            else
            {
                specification = repository.Fetch(vehicle, broker);
            }

            // create new object if insert

            if (specification == null)
            {
                specification = new Model.Specification();

                specification.Vehicle = vehicle;

                if (broker != null)
                {
                    specification.Dealer = broker;
                }
            }

            // a) style

            IStyle style = Mapper.Map(arguments.Style, specification.Style);

            specification.Style = style;

            // b) vehicle document

            XmlDocument vehicleDocument = new XmlDocument();

            Serialize(vehicleDocument, arguments.VehicleSpecification);

            specification.VehicleDocument = vehicleDocument;

            // c) build document

            specification.BuildDocument = arguments.BuildSpecification;

            // save

            specification = repository.Save(
                Principal.Get(parameters.Identity.Name, parameters.Identity.AuthorityName),
                specification);

            // produce DTO from result

            return Mapper.Map(specification.Dealer, specification);
        }

        private static void Serialize(XmlDocument vehicleDocument, VehicleSpecification specification)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                new XmlSerializer(typeof(VehicleSpecification)).Serialize(stream, specification);

                stream.Flush();

                stream.Seek(0, SeekOrigin.Begin);

                vehicleDocument.Load(stream);
            }
        }
    }
}