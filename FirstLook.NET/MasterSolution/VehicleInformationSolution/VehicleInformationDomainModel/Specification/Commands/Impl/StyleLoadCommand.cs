using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Specification.Model;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Commands.Impl
{
    public class StyleLoadCommand : ICommand<StyleDto, IdentityContextDto<StyleKeyDto>>
    {
        public StyleDto Execute(IdentityContextDto<StyleKeyDto> parameters)
        {
            IRegistry registry = RegistryFactory.GetRegistry();

            ISpecificationRepository repository = registry.Resolve<ISpecificationRepository>();

            IStyle style = repository.Fetch(new StyleKey(parameters.Arguments.Id));

            if (style == null)
            {
                return null;
            }

            return Mapper.Map(style);
        }
    }
}