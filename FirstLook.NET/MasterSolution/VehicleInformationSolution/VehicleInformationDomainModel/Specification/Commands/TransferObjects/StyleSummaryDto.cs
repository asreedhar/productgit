using System;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects
{
    [Serializable]
    public class StyleSummaryDto
    {
        private int _modelYear = 1980;

        private string _manufacturerName = string.Empty;
        private string _divisionName = string.Empty;
        private string _subDivisionName = string.Empty;
        private string _modelName = string.Empty;
        private string _styleName = string.Empty;
        private string _trimName = string.Empty;
        private string _bodyStyleName = string.Empty;

        public int ModelYear
        {
            get { return _modelYear; }
            set { _modelYear = value; }
        }

        public string ManufacturerName
        {
            get { return _manufacturerName; }
            set { _manufacturerName = value; }
        }

        public string DivisionName
        {
            get { return _divisionName; }
            set { _divisionName = value; }
        }

        public string SubdivisionName
        {
            get { return _subDivisionName; }
            set { _subDivisionName = value; }
        }

        public string ModelName
        {
            get { return _modelName; }
            set { _modelName = value; }
        }

        public string StyleName
        {
            get { return _styleName; }
            set { _styleName = value; }
        }

        public string TrimName
        {
            get { return _trimName; }
            set { _trimName = value; }
        }

        public string BodyStyleName
        {
            get { return _bodyStyleName; }
            set { _bodyStyleName = value; }
        }
    }
}
