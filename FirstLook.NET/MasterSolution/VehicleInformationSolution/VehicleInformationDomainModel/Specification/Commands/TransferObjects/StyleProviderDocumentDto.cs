using System;
using System.Xml;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects
{
    [Serializable]
    public class StyleProviderDocumentDto
    {
        private XmlDocument _document;
        private int _version;

        public XmlDocument Document
        {
            get { return _document; }
            set { _document = value; }
        }

        public int Version
        {
            get { return _version; }
            set { _version = value; }
        }
    }
}