using System;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects
{
    [Serializable]
    public class SpecificationCriteriaDto
    {
        private VehicleIdentificationDto _vehicle;
        private BrokerIdentificationDto _dealer;

        /// <summary>
        /// Is Nullable.
        /// </summary>
        public BrokerIdentificationDto Dealer
        {
            get { return _dealer; }
            set { _dealer = value; }
        }

        /// <summary>
        /// Not Nullable
        /// </summary>
        public VehicleIdentificationDto Vehicle
        {
            get { return _vehicle; }
            set { _vehicle = value; }
        }
    }
}
