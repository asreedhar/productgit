using System;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects
{
    [Serializable]
    public abstract class StyleIdentityDto
    {
        private int _providerId;
        private string _providerName;

        public int ProviderId
        {
            get { return _providerId; }
            set { _providerId = value; }
        }

        public string ProviderName
        {
            get { return _providerName; }
            set { _providerName = value; }
        }
    }

    [Serializable]
    public class StyleIdentityAutoDataDto : StyleIdentityDto
    {
        private string _acode;

        public string Acode
        {
            get { return _acode; }
            set { _acode = value; }
        }
    }

    [Serializable]
    public class StyleIdentityChromeDto : StyleIdentityDto
    {
        private int _id;
        private bool _isMappingUpdated;
        private bool _isDeleted;
        private bool _isUpdated;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public bool IsMappingUpdated
        {
            get { return _isMappingUpdated; }
            set { _isMappingUpdated = value; }
        }

        public bool IsDeleted
        {
            get { return _isDeleted; }
            set { _isDeleted = value; }
        }

        public bool IsUpdated
        {
            get { return _isUpdated; }
            set { _isUpdated = value; }
        }
    }
}
