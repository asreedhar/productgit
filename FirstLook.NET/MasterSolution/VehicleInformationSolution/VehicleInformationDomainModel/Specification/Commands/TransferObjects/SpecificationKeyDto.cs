using System;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects
{
    [Serializable]
    public class SpecificationKeyDto
    {
        private int _id;
        private short _version;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public short Version
        {
            get { return _version; }
            set { _version = value; }
        }
    }
}