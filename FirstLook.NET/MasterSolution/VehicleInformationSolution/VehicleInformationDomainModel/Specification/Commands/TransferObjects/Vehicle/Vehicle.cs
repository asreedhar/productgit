using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects.Vehicle
{
    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/Vehicle")]
    [XmlRoot(Namespace = "http://xml.firstlook.biz/VSD/Vehicle", IsNullable = false)]
    public class VehicleSpecification
    {
        private VehicleDescription descriptionField;

        private VehicleColorCollection colorsField;

        private VehicleOptionCollection optionsField;

        private VehicleStandardCollection standardsField;

        private VehiclePriceCollection pricesField;

        private string vinField;

        private bool isFleetField;

        private bool isFleetFieldSpecified;

        private decimal versionField;

        private bool versionFieldSpecified;

        /// <remarks/>
        public VehicleDescription Description
        {
            get { return descriptionField; }
            set { descriptionField = value; }
        }

        /// <remarks/>
        [XmlElement("Colors")]
        public VehicleColorCollection Colors
        {
            get { return colorsField; }
            set { colorsField = value; }
        }

        /// <remarks/>
        [XmlElement("Options")]
        public VehicleOptionCollection Options
        {
            get { return optionsField; }
            set { optionsField = value; }
        }

        /// <remarks/>
        [XmlElement("Standards")]
        public VehicleStandardCollection Standards
        {
            get { return standardsField; }
            set { standardsField = value; }
        }

        /// <remarks/>
        [XmlElement("Prices")]
        public VehiclePriceCollection Prices
        {
            get { return pricesField; }
            set { pricesField = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public string Vin
        {
            get { return vinField; }
            set { vinField = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public bool IsFleet
        {
            get { return isFleetField; }
            set { isFleetField = value; }
        }

        /// <remarks/>
        [XmlIgnore]
        public bool IsFleetSpecified
        {
            get { return isFleetFieldSpecified; }
            set { isFleetFieldSpecified = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public decimal Version
        {
            get { return versionField; }
            set { versionField = value; }
        }

        /// <remarks/>
        [XmlIgnore]
        public bool VersionSpecified
        {
            get { return versionFieldSpecified; }
            set { versionFieldSpecified = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/Vehicle")]
    public class VehicleDescription
    {
        private VehicleModel modelField;

        private string styleCodeField;

        private string styleNameField;

        private string bodyStyleField;

        private string trimField;

        private int passengerCapacityField;

        private int passengerDoorsField;

        private VehicleTransmission transmissionField;

        private VehicleEngine engineField;

        private VehicleDriveTrain driveTrainField;

        /// <remarks/>
        public VehicleModel Model
        {
            get { return modelField; }
            set { modelField = value; }
        }

        /// <remarks/>
        public string StyleCode
        {
            get { return styleCodeField; }
            set { styleCodeField = value; }
        }

        /// <remarks/>
        public string StyleName
        {
            get { return styleNameField; }
            set { styleNameField = value; }
        }

        /// <remarks/>
        public string BodyStyle
        {
            get { return bodyStyleField; }
            set { bodyStyleField = value; }
        }

        /// <remarks/>
        public string Trim
        {
            get { return trimField; }
            set { trimField = value; }
        }

        /// <remarks/>
        public int PassengerCapacity
        {
            get { return passengerCapacityField; }
            set { passengerCapacityField = value; }
        }

        /// <remarks/>
        public int PassengerDoors
        {
            get { return passengerDoorsField; }
            set { passengerDoorsField = value; }
        }

        /// <remarks/>
        public VehicleTransmission Transmission
        {
            get { return transmissionField; }
            set { transmissionField = value; }
        }

        /// <remarks/>
        public VehicleEngine Engine
        {
            get { return engineField; }
            set { engineField = value; }
        }

        /// <remarks/>
        public VehicleDriveTrain DriveTrain
        {
            get { return driveTrainField; }
            set { driveTrainField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/Vehicle")]
    public class VehicleModel
    {
        private int modelYearField;

        private string modelField;

        private string subdivisionField;

        private string divisionField;

        private string manufacturerField;

        /// <remarks/>
        public int ModelYear
        {
            get { return modelYearField; }
            set { modelYearField = value; }
        }

        /// <remarks/>
        public string Model
        {
            get { return modelField; }
            set { modelField = value; }
        }

        /// <remarks/>
        public string Subdivision
        {
            get { return subdivisionField; }
            set { subdivisionField = value; }
        }

        /// <remarks/>
        public string Division
        {
            get { return divisionField; }
            set { divisionField = value; }
        }

        /// <remarks/>
        public string Manufacturer
        {
            get { return manufacturerField; }
            set { manufacturerField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/Vehicle")]
    public class VehicleStandard
    {
        private string nameField;

        private Int32Collection featuresField;

        /// <remarks/>
        public string Name
        {
            get { return nameField; }
            set { nameField = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public Int32Collection Features
        {
            get { return featuresField; }
            set { featuresField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/Vehicle")]
    public class VehiclePrice
    {
        private VehiclePriceType priceTypeField;

        private decimal amountField;

        /// <remarks/>
        public VehiclePriceType PriceType
        {
            get { return priceTypeField; }
            set { priceTypeField = value; }
        }

        /// <remarks/>
        public decimal Amount
        {
            get { return amountField; }
            set { amountField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/Vehicle")]
    public enum VehiclePriceType
    {
        /// <remarks/>
        Invoice,

        /// <remarks/>
        Msrp,

        /// <remarks/>
        TotalOptionInvoice,

        /// <remarks/>
        TotalOptionMsrp,
    }

    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/Vehicle")]
    public class VehicleOption
    {
        private string optionCodeField;

        private string nameField;

        private string supplementalDescriptionField;

        private VehiclePrice priceField;

        private Int32Collection featuresField;

        /// <remarks/>
        public string OptionCode
        {
            get { return optionCodeField; }
            set { optionCodeField = value; }
        }

        /// <remarks/>
        public string Name
        {
            get { return nameField; }
            set { nameField = value; }
        }

        /// <remarks/>
        public string SupplementalDescription
        {
            get { return supplementalDescriptionField; }
            set { supplementalDescriptionField = value; }
        }

        /// <remarks/>
        public VehiclePrice Price
        {
            get { return priceField; }
            set { priceField = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public Int32Collection Features
        {
            get { return featuresField; }
            set { featuresField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/Vehicle")]
    public class VehicleColor
    {
        private VehicleColorItem colorItemField;

        private int colorRankField;

        private string codeField;

        private string descriptionField;

        private string genericDescriptionField;

        private string rgbHexField;

        /// <remarks/>
        public VehicleColorItem ColorItem
        {
            get { return colorItemField; }
            set { colorItemField = value; }
        }

        /// <remarks/>
        public int ColorRank
        {
            get { return colorRankField; }
            set { colorRankField = value; }
        }

        /// <remarks/>
        public string Code
        {
            get { return codeField; }
            set { codeField = value; }
        }

        /// <remarks/>
        public string Description
        {
            get { return descriptionField; }
            set { descriptionField = value; }
        }

        /// <remarks/>
        public string GenericDescription
        {
            get { return genericDescriptionField; }
            set { genericDescriptionField = value; }
        }

        /// <remarks/>
        public string RgbHex
        {
            get { return rgbHexField; }
            set { rgbHexField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/Vehicle")]
    public enum VehicleColorItem
    {
        /// <remarks/>
        Interior,

        /// <remarks/>
        Exterior,
    }

    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/Vehicle")]
    public class VehicleDriveTrain
    {
        private VehicleDriveTrainType driveTrainTypeField;

        private VehicleDriveTrainAxle driveTrainAxleField;

        private string nameField;

        /// <remarks/>
        public VehicleDriveTrainType DriveTrainType
        {
            get { return driveTrainTypeField; }
            set { driveTrainTypeField = value; }
        }

        /// <remarks/>
        public VehicleDriveTrainAxle DriveTrainAxle
        {
            get { return driveTrainAxleField; }
            set { driveTrainAxleField = value; }
        }

        /// <remarks/>
        public string Name
        {
            get { return nameField; }
            set { nameField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/Vehicle")]
    public enum VehicleDriveTrainType
    {
        /// <remarks/>
        [XmlEnum("Two Wheel Drive")] TwoWheelDrive,

        /// <remarks/>
        [XmlEnum("Four Wheel Drive")] FourWheelDrive,

        /// <remarks/>
        [XmlEnum("All Wheel Drive")] AllWheelDrive,
    }

    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/Vehicle")]
    public enum VehicleDriveTrainAxle
    {
        /// <remarks/>
        Front,

        /// <remarks/>
        Rear,
    }

    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/Vehicle")]
    public class VehicleEngine
    {
        private VehicleEngineType engineTypeField;

        private int capacityField;

        private int cylinderCountField;

        private VehicleEngineCylinderLayout cylinderLayoutField;

        private string nameField;

        /// <remarks/>
        public VehicleEngineType EngineType
        {
            get { return engineTypeField; }
            set { engineTypeField = value; }
        }

        /// <remarks/>
        public int Capacity
        {
            get { return capacityField; }
            set { capacityField = value; }
        }

        /// <remarks/>
        public int CylinderCount
        {
            get { return cylinderCountField; }
            set { cylinderCountField = value; }
        }

        /// <remarks/>
        public VehicleEngineCylinderLayout CylinderLayout
        {
            get { return cylinderLayoutField; }
            set { cylinderLayoutField = value; }
        }

        /// <remarks/>
        public string Name
        {
            get { return nameField; }
            set { nameField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/Vehicle")]
    public enum VehicleEngineType
    {
        /// <remarks/>
        Electric,

        /// <remarks/>
        Piston,

        /// <remarks/>
        Rotary,
    }

    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/Vehicle")]
    public enum VehicleEngineCylinderLayout
    {
        /// <remarks/>
        Flat,

        /// <remarks/>
        Inline,

        /// <remarks/>
        Straight,

        /// <remarks/>
        V,
    }

    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/Vehicle")]
    public class VehicleTransmission
    {
        private VehicleTransmissionType transmissionTypeField;

        private string nameField;

        private int gearCountField;

        /// <remarks/>
        public VehicleTransmissionType TransmissionType
        {
            get { return transmissionTypeField; }
            set { transmissionTypeField = value; }
        }

        /// <remarks/>
        public string Name
        {
            get { return nameField; }
            set { nameField = value; }
        }

        /// <remarks/>
        public int GearCount
        {
            get { return gearCountField; }
            set { gearCountField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("Tools.Console", "1.0.0.0")]
    [Serializable]
    [XmlType(Namespace = "http://xml.firstlook.biz/VSD/Vehicle")]
    public enum VehicleTransmissionType
    {
        /// <remarks/>
        Automatic,

        /// <remarks/>
        CVT,

        /// <remarks/>
        Manual,
    }

    public class VehicleColorCollection : CollectionBase
    {
        public VehicleColor this[int idx]
        {
            get { return ((VehicleColor) (base.InnerList[idx])); }
            set { base.InnerList[idx] = value; }
        }

        public int Add(VehicleColor value)
        {
            return base.InnerList.Add(value);
        }

        public void Remove(VehicleColor value)
        {
            base.InnerList.Remove(value);
        }

        public bool Contains(VehicleColor value)
        {
            return base.InnerList.Contains(value);
        }
    }

    public class VehicleOptionCollection : CollectionBase
    {
        public VehicleOption this[int idx]
        {
            get { return ((VehicleOption) (base.InnerList[idx])); }
            set { base.InnerList[idx] = value; }
        }

        public int Add(VehicleOption value)
        {
            return base.InnerList.Add(value);
        }

        public void Remove(VehicleOption value)
        {
            base.InnerList.Remove(value);
        }

        public bool Contains(VehicleOption value)
        {
            return base.InnerList.Contains(value);
        }
    }

    public class VehicleStandardCollection : CollectionBase
    {
        public VehicleStandard this[int idx]
        {
            get { return ((VehicleStandard) (base.InnerList[idx])); }
            set { base.InnerList[idx] = value; }
        }

        public int Add(VehicleStandard value)
        {
            return base.InnerList.Add(value);
        }

        public void Remove(VehicleStandard value)
        {
            base.InnerList.Remove(value);
        }

        public bool Contains(VehicleStandard value)
        {
            return base.InnerList.Contains(value);
        }
    }

    public class VehiclePriceCollection : CollectionBase
    {
        public VehiclePrice this[int idx]
        {
            get { return ((VehiclePrice) (base.InnerList[idx])); }
            set { base.InnerList[idx] = value; }
        }

        public int Add(VehiclePrice value)
        {
            return base.InnerList.Add(value);
        }

        public void Remove(VehiclePrice value)
        {
            base.InnerList.Remove(value);
        }

        public bool Contains(VehiclePrice value)
        {
            return base.InnerList.Contains(value);
        }
    }

    public class Int32Collection : CollectionBase
    {
        public int this[int idx]
        {
            get { return ((int) (base.InnerList[idx])); }
            set { base.InnerList[idx] = value; }
        }

        public int Add(int value)
        {
            return base.InnerList.Add(value);
        }

        public void Remove(int value)
        {
            base.InnerList.Remove(value);
        }

        public bool Contains(int value)
        {
            return base.InnerList.Contains(value);
        }
    }
}