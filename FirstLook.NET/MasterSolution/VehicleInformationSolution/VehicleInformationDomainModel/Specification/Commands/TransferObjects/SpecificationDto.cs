using System;
using System.Xml;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects.Vehicle;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects
{
    [Serializable]
    public class SpecificationDto
    {
        private short _version;
        private BrokerIdentificationDto _dealer;
        private VehicleIdentificationDto _vehicle;
        private StyleDto _style;
        private VehicleSpecification _vehicleSpecification;
        private XmlDocument _buildSpecification;

        public short Version
        {
            get { return _version; }
            set { _version = value; }
        }

        public BrokerIdentificationDto Dealer
        {
            get { return _dealer; }
            set { _dealer = value; }
        }

        public StyleDto Style
        {
            get { return _style; }
            set { _style = value; }
        }

        public VehicleIdentificationDto Vehicle
        {
            get { return _vehicle; }
            set { _vehicle = value; }
        }

        public VehicleSpecification VehicleSpecification
        {
            get { return _vehicleSpecification; }
            set { _vehicleSpecification = value; }
        }

        public XmlDocument BuildSpecification
        {
            get { return _buildSpecification; }
            set { _buildSpecification = value; }
        }
    }
}