using System;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects
{
    [Serializable]
    public class SpecificationIdentityDto
    {
        private VehicleIdentificationDto _vehicle;
        private BrokerIdentificationDto _dealer;
        private short _version;

        public VehicleIdentificationDto Vehicle
        {
            get { return _vehicle; }
            set { _vehicle = value; }
        }

        public BrokerIdentificationDto Dealer
        {
            get { return _dealer; }
            set { _dealer = value; }
        }

        public short Version
        {
            get { return _version; }
            set { _version = value; }
        }
    }
}