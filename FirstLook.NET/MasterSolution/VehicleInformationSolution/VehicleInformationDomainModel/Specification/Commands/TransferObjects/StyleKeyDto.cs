using System;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects
{
    [Serializable]
    public class StyleKeyDto
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
    }
}