﻿using System;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class SpecificationFetchResultsDto
    {
        private SpecificationFetchArgumentsDto _arguments;

        public SpecificationFetchArgumentsDto Arguments
        {
            get { return _arguments; }
            set { _arguments = value; }
        }

        private SpecificationDto _specification;

        public SpecificationDto Specification
        {
            get { return _specification; }
            set { _specification = value; }
        }
    }
}
