﻿using System;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class SpecificationFetchArgumentsDto
    {
        private VehicleIdentificationDto _vehicle;
        private BrokerIdentificationDto _broker;

        public VehicleIdentificationDto Vehicle
        {
            get { return _vehicle; }
            set { _vehicle = value; }
        }

        public BrokerIdentificationDto Broker
        {
            get { return _broker; }
            set { _broker = value; }
        }
    }
}
