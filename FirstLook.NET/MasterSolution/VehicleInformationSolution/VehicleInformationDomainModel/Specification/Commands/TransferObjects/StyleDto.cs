using System;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects
{
    [Serializable]
    public class StyleDto
    {
        private StyleKeyDto _key;
        private StyleIdentityDto _identity;
        private StyleSummaryDto _summary;
        private StyleProviderDocumentDto _document;
        private StyleProviderDocumentDto _reference;

        public StyleKeyDto Key
        {
            get { return _key; }
            set { _key = value; }
        }

        public StyleIdentityDto Identity
        {
            get { return _identity; }
            set { _identity = value; }
        }

        public StyleSummaryDto Summary
        {
            get { return _summary; }
            set { _summary = value; }
        }

        public StyleProviderDocumentDto Document
        {
            get { return _document; }
            set { _document = value; }
        }

        public StyleProviderDocumentDto Reference
        {
            get { return _reference; }
            set { _reference = value; }
        }
    }
}