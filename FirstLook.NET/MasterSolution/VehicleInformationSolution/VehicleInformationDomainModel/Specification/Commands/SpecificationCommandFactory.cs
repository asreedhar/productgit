using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Specification.Commands.Impl;
using FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleInformation.DomainModel.Specification.Model;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Commands
{
    public class SpecificationCommandFactory : CommandFactory, ISpecificationCommandFactory
    {
        public ICommand<SpecificationFetchResultsDto, IdentityContextDto<SpecificationFetchArgumentsDto>> CreateSpecificationFetchCommand()
        {
            return new SpecificationFetchCommand();
        }

        public ICommand<SpecificationDto, IdentityContextDto<SpecificationDto>> CreateSpecificationSaveCommand()
        {
            return new SpecificationSaveCommand();
        }

        public ICommand<StyleDto, IdentityContextDto<StyleKeyDto>> CreateStyleLoadCommand()
        {
            return new StyleLoadCommand();
        }

        public ICommand<StyleDto, IdentityContextDto<StyleDto>> CreateStyleSaveCommand()
        {
            return new StyleSaveCommand();
        }

        public override ICommandTransaction CreateTransactionCommand()
        {
            IRegistry registry = RegistryFactory.GetRegistry();

            ISpecificationRepository repository = registry.Resolve<ISpecificationRepository>();

            return new CommandTransaction(repository.TransactionFactory);
        }
    }
}