using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects.Envelopes;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Commands
{
    public interface ISpecificationCommandFactory : ICommandFactory
    {
        // 1. load a specification (how do we merge specifications and instruction sets?)

        ICommand<SpecificationFetchResultsDto, IdentityContextDto<SpecificationFetchArgumentsDto>> CreateSpecificationFetchCommand();

        // 2. save a specification

        ICommand<SpecificationDto, IdentityContextDto<SpecificationDto>> CreateSpecificationSaveCommand();

        // 3. load a style (NEW)

        ICommand<StyleDto, IdentityContextDto<StyleKeyDto>> CreateStyleLoadCommand();

        // 4. save a style (NEW)

        ICommand<StyleDto, IdentityContextDto<StyleDto>> CreateStyleSaveCommand();
    }
}