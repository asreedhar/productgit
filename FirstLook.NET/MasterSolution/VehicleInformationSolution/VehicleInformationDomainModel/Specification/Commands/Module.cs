﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Commands
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ISpecificationCommandFactory, SpecificationCommandFactory>(ImplementationScope.Shared);
        }
    }
}
