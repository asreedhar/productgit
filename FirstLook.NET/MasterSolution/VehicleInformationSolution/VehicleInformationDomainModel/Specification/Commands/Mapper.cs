﻿using System;
using System.Xml;
using System.Xml.Serialization;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Specification.Commands.TransferObjects.Vehicle;
using FirstLook.VehicleInformation.DomainModel.Specification.Model;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Commands
{
    public static class Mapper
    {
        public static SpecificationDto Map(IBroker broker, Model.Specification specification)
        {
            SpecificationDto dto = Map(specification);

            if (broker != null)
            {
                dto.Dealer = new BrokerIdentificationDto();

                dto.Dealer.Handle = broker.Handle;
            }

            return dto;
        }

        public static SpecificationDto Map(BrokerIdentificationDto dealer, Model.Specification specification)
        {
            SpecificationDto dto = Map(specification);

            if (dealer != null)
            {
                dto.Dealer = new BrokerIdentificationDto();

                dto.Dealer.Handle = dealer.Handle;
            }

            return dto;
        }

        private static SpecificationDto Map(Model.Specification specification)
        {
            SpecificationDto dto = new SpecificationDto();

            dto.Version = specification.Version;

            dto.Style = Map(specification.Style);

            dto.Vehicle = new VehicleIdentificationDto();

            dto.Vehicle.Vin = specification.Vehicle.Vin;

            if (specification.VehicleDocument != null)
            {
                XmlSerializer serializer = new XmlSerializer(typeof(VehicleSpecification));

                using (XmlNodeReader reader = new XmlNodeReader(specification.VehicleDocument))
                {
                    dto.VehicleSpecification = (VehicleSpecification) serializer.Deserialize(reader);
                }
            }
            
            dto.BuildSpecification = specification.BuildDocument;

            return dto;
        }

        public static void Map(StyleSummary summary, StyleSummaryDto summaryDto)
        {
            summary.ManufacturerName = summaryDto.ManufacturerName;
            summary.DivisionName = summaryDto.DivisionName;
            summary.SubdivisionName = summaryDto.SubdivisionName;
            summary.ModelName = summaryDto.ModelName;
            summary.StyleName = summaryDto.StyleName;
            summary.TrimName = summaryDto.TrimName;
            summary.BodyStyleName = summaryDto.BodyStyleName;
            summary.ModelYear = summaryDto.ModelYear;
        }

        public static StyleDto Map(IStyle style)
        {
            StyleDto dto = new StyleDto();

            dto.Document = new StyleProviderDocumentDto();

            dto.Document.Document = style.Document.Document;

            dto.Document.Version = style.Document.Version;

            dto.Reference = new StyleProviderDocumentDto();

            dto.Reference.Document = style.ReferenceDocument.Document;

            dto.Reference.Version = style.ReferenceDocument.Version;

            if (style.StyleProvider == StyleProvider.Chrome)
            {
                ChromeStyle chrome = (ChromeStyle) style;

                StyleIdentityChromeDto identity = new StyleIdentityChromeDto();

                identity.Id = chrome.ChromeStyleId;
                identity.ProviderId = StyleProvider.Chrome.Id;
                identity.ProviderName = StyleProvider.Chrome.Name;

                if (chrome.Status != null)
                {
                    identity.IsDeleted = chrome.Status.IsDeleted;
                    identity.IsUpdated = chrome.Status.IsUpdated;
                    identity.IsMappingUpdated = chrome.Status.IsMappingUpdated;
                }

                dto.Identity = identity;
            }

            dto.Key = new StyleKeyDto();

            dto.Key.Id = style.Id;

            return dto;
        }

        public static IStyle Map(StyleDto styleDto, IStyle style)
        {
            StyleIdentityDto identityDto = styleDto.Identity;

            if (identityDto.ProviderId == StyleProvider.Chrome.Id)
            {
                StyleIdentityChromeDto chromeDto = (StyleIdentityChromeDto) identityDto;

                ChromeStyle chromeStyle;

                if (style == null)
                {
                    style = chromeStyle = new ChromeStyle();

                    chromeStyle.Status = new ChromeStyleStatus();
                }
                else
                {
                    if (style.StyleProvider.Id != StyleProvider.Chrome.Id)
                    {
                        throw new ArgumentException();
                    }

                    chromeStyle = (ChromeStyle) style;
                }

                chromeStyle.StyleProvider = StyleProvider.Chrome;

                chromeStyle.ChromeStyleId = chromeDto.Id;

                chromeStyle.Status.IsDeleted = chromeDto.IsDeleted;
                chromeStyle.Status.IsUpdated = chromeDto.IsUpdated;
                chromeStyle.Status.IsMappingUpdated = chromeDto.IsMappingUpdated;
            }
            else
            {
                throw new NotSupportedException();
            }
            
            // a) style

            if (style.Document == null)
            {
                style.Document = new StyleProviderDocument();
            }

            StyleProviderDocument sd = style.Document;

            sd.Document = styleDto.Document.Document;

            sd.Version = styleDto.Document.Version;

            // b) style reference

            if (style.ReferenceDocument == null)
            {
                style.ReferenceDocument = new StyleProviderDocument();
            }

            StyleProviderDocument rd = style.ReferenceDocument;

            rd.Document = styleDto.Reference.Document;

            rd.Version = styleDto.Reference.Version;

            // c) style summary

            if (style.StyleSummary == null)
            {
                style.StyleSummary = new StyleSummary();
            }

            StyleSummaryDto summaryDto = styleDto.Summary;

            Map(style.StyleSummary, summaryDto);

            return style;
        }
    }
}
