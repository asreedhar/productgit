using System;
using System.Xml;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.VehicleInformation.DomainModel.Specification.Model;
using FirstLook.VehicleInformation.DomainModel.Utility;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Repositories.Entities
{
    /// <summary>
    /// Once saved, this class cannot be updated.  Any old, dirty objects when saved should throw
    /// an exception.
    /// </summary>
    [Serializable]
    public class StyleProviderDocumentEntity : StyleProviderDocument, ISavable
    {
        private readonly int _id;

        private readonly StyleProviderDocumentType _documentType;

        public int Id
        {
            get { return _id; }
        }

        public StyleProviderDocumentType DocumentType
        {
            get { return _documentType; }
        }

        public StyleProviderDocumentEntity()
        {
            base.PropertyChanged += _savable.PropertyChanged;
        }

        public StyleProviderDocumentEntity(int id, AuditRow auditRow, StyleProviderDocumentType documentType,
                                           int version, XmlDocument document)
        {
            Version = version;
            Document = document;

            _id = id;
            _documentType = documentType;

            _auditRow = auditRow;
            _savable.MarkOld();
        }

        #region ISavable

        private readonly Savable _savable = new Savable();

        private AuditRow _auditRow;

        public AuditRow AuditRow
        {
            get { return _auditRow; }
        }

        public bool IsNew
        {
            get { return _savable.IsNew; }
        }

        public bool IsDeleted
        {
            get { return _savable.IsDeleted; }
        }

        public bool IsDirty
        {
            get { return _savable.IsDirty; }
        }

        public void MarkDeleted()
        {
            _savable.MarkDeleted();
        }

        public void OnSaved(object sender, SavedEventArgs e)
        {
            _auditRow = e.AuditRow;

            _savable.MarkOld();
        }

        #endregion
    }
}