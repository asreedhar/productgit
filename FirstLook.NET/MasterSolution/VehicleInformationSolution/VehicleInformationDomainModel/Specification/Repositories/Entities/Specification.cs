using System;
using System.ComponentModel;
using System.Xml;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.VehicleInformation.DomainModel.Specification.Model;
using FirstLook.VehicleInformation.DomainModel.Utility;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Repositories.Entities
{
    [Serializable]
    public class SpecificationIdentity
    {
        private readonly int _id;
        private readonly AuditRow _auditRow;
        private readonly SpecificationType _type;

        public SpecificationIdentity(int id, AuditRow auditRow, SpecificationType type)
        {
            _id = id;
            _auditRow = auditRow;
            _type = type;
        }

        public int Id
        {
            get { return _id; }
        }

        public AuditRow AuditRow
        {
            get { return _auditRow; }
        }

        public SpecificationType Type
        {
            get { return _type; }
        }
    }

    public class SpecificationEntity : Model.Specification, ISavable
    {
        private static readonly ReadOnly ReadOnlyProperties = new ReadOnly(
            "Dealer",
            "Vehicle");

        private readonly SpecificationIdentity _identity;

        private int _versionCounter;

        private short _version;

        public SpecificationIdentity Identity
        {
            get { return _identity; }
        }

        public override int Id
        {
            get { return _identity.Id; }
        }

        public override short Version
        {
            get { return _version; }
        }

        public SpecificationEntity()
        {
            ListenToEvents();
        }

        public SpecificationEntity(SpecificationIdentity identity, AuditRow auditRow,
                                   short version,
                                   IBroker broker,
                                   VehicleIdentification vehicle,
                                   IStyle style,
                                   XmlDocument vehicleDocument,
                                   XmlDocument buildDocument)
        {
            Dealer = broker;
            Vehicle = vehicle;
            Style = style;
            VehicleDocument = vehicleDocument;
            BuildDocument = buildDocument;

            _identity = identity;
            _version = version;
            _auditRow = auditRow;
            _savable.MarkOld();

            ListenToEvents();
        }
        
        private void ListenToEvents()
        {
            base.PropertyChanging += ReadOnlyProperties.OnPropertyChanging;

            base.PropertyChanged += _savable.PropertyChanged;

            base.PropertyChanged += MyPropertyChanged;
        }

        void MyPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (_versionCounter++ == 0)
            {
                _version += 1;
            }
        }

        #region ISavable

        private readonly Savable _savable = new Savable();

        private AuditRow _auditRow;

        public AuditRow AuditRow
        {
            get { return _auditRow; }
        }

        public bool IsNew
        {
            get { return _savable.IsNew; }
        }

        public bool IsDeleted
        {
            get { return _savable.IsDeleted; }
        }

        public bool IsDirty
        {
            get { return _savable.IsDirty; }
        }

        public void MarkDeleted()
        {
            _savable.MarkDeleted();
        }

        public void OnSaved(object sender, SavedEventArgs e)
        {
            _auditRow = e.AuditRow;

            _savable.MarkOld();
        }

        #endregion
    }
}