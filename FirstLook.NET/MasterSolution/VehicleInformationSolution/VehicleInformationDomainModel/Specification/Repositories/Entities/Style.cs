using System;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.VehicleInformation.DomainModel.Specification.Model;
using FirstLook.VehicleInformation.DomainModel.Utility;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Repositories.Entities
{
    [Serializable]
    public class StyleIdentity
    {
        private readonly int _id;
        private readonly AuditRow _auditRow;

        public StyleIdentity(AuditRow auditRow, int id)
        {
            _auditRow = auditRow;
            _id = id;
        }

        public int Id
        {
            get { return _id; }
        }

        public AuditRow AuditRow
        {
            get { return _auditRow; }
        }
    }

    [Serializable]
    public class ChromeStyleEntity : ChromeStyle, ISavable
    {
        private readonly StyleIdentity _identity;

        public StyleIdentity Identity
        {
            get { return _identity; }
        }

        public override int Id
        {
            get
            {
                return Identity.Id;
            }
        }

        public ChromeStyleEntity()
        {
            base.PropertyChanged += _savable.PropertyChanged;
        }

        public ChromeStyleEntity(StyleIdentity identity, AuditRow auditRow,
                                 StyleProvider styleProvider,
                                 StyleSummary styleSummary,
                                 StyleProviderDocument document,
                                 StyleProviderDocument referenceDocument,
                                 int chromeStyleId,
                                 ChromeStyleStatus status)
            : this()
        {
            StyleProvider = styleProvider;
            StyleSummary = styleSummary;
            Document = document;
            ReferenceDocument = referenceDocument;

            ChromeStyleId = chromeStyleId;
            Status = status;

            _auditRow = auditRow;
            _identity = identity;
            _savable.MarkOld();

            base.PropertyChanging += ReadOnly.Immutable;
        }

        #region ISavable

        private readonly Savable _savable = new Savable();

        private AuditRow _auditRow;

        public AuditRow AuditRow
        {
            get { return _auditRow; }
        }

        public bool IsNew
        {
            get { return _savable.IsNew; }
        }

        public bool IsDeleted
        {
            get { return _savable.IsDeleted; }
        }

        public bool IsDirty
        {
            get { return _savable.IsDirty; }
        }

        public void MarkDeleted()
        {
            _savable.MarkDeleted();
        }

        public void OnSaved(object sender, SavedEventArgs e)
        {
            _auditRow = e.AuditRow;

            _savable.MarkOld();
        }

        #endregion
    }

    public class ChromeStyleStatusEntity : ChromeStyleStatus, ISavable
    {
        private readonly int _styleId;

        public int StyleId
        {
            get { return _styleId; }
        }

        public ChromeStyleStatusEntity()
        {
            base.PropertyChanged += _savable.PropertyChanged;
        }

        public ChromeStyleStatusEntity(int styleId, AuditRow auditRow,
                                       bool isMappingUpdated,
                                       bool isDeleted,
                                       bool isUpdated)
            : this()
        {
            IsMappingUpdated = isMappingUpdated;
            IsDeleted = isDeleted;
            IsUpdated = isUpdated;

            _styleId = styleId;
            _auditRow = auditRow;
            _savable.MarkOld();
        }

        #region ISavable

        private readonly Savable _savable = new Savable();

        private AuditRow _auditRow;

        AuditRow ISavable.AuditRow
        {
            get { return _auditRow; }
        }

        bool ISavable.IsNew
        {
            get { return _savable.IsNew; }
        }

        bool ISavable.IsDeleted
        {
            get { return _savable.IsDeleted; }
        }

        bool ISavable.IsDirty
        {
            get { return _savable.IsDirty; }
        }

        void ISavable.MarkDeleted()
        {
            _savable.MarkDeleted();
        }

        void ISavable.OnSaved(object sender, SavedEventArgs e)
        {
            _auditRow = e.AuditRow;

            _savable.MarkOld();
        }

        #endregion
    }
}