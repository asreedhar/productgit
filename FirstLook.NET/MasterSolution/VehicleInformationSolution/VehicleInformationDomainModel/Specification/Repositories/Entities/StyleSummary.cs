using System;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.VehicleInformation.DomainModel.Specification.Model;
using FirstLook.VehicleInformation.DomainModel.Utility;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Repositories.Entities
{
    [Serializable]
    public class StyleSummaryEntity : StyleSummary, ISavable
    {
        private readonly int _id;

        public int Id
        {
            get { return _id; }
        }

        public StyleSummaryEntity()
        {
            base.PropertyChanged += _savable.PropertyChanged;
        }

        public StyleSummaryEntity(AuditRow auditRow, int id,
                                  string manufacturerName,
                                  string divisionName,
                                  string subdivisionName,
                                  string modelName,
                                  string styleName,
                                  string trimName,
                                  string bodyStyleName,
                                  int modelYear)
            : this()
        {
            ManufacturerName = manufacturerName;
            DivisionName = divisionName;
            SubdivisionName = subdivisionName;
            ModelName = modelName;
            StyleName = styleName;
            TrimName = trimName;
            BodyStyleName = bodyStyleName;
            ModelYear = modelYear;

            _auditRow = auditRow;
            _id = id;
            _savable.MarkOld();
        }

        #region ISavable

        private readonly Savable _savable = new Savable();

        private AuditRow _auditRow;

        public AuditRow AuditRow
        {
            get { return _auditRow; }
        }

        public bool IsNew
        {
            get { return _savable.IsNew; }
        }

        public bool IsDeleted
        {
            get { return _savable.IsDeleted; }
        }

        public bool IsDirty
        {
            get { return _savable.IsDirty; }
        }

        public void MarkDeleted()
        {
            _savable.MarkDeleted();
        }

        public void OnSaved(object sender, SavedEventArgs e)
        {
            _auditRow = e.AuditRow;

            _savable.MarkOld();
        }

        #endregion
    }
}