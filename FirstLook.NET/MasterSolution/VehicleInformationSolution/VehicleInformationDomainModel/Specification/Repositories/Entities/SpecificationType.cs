using System;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Repositories.Entities
{
    [Serializable]
    public enum SpecificationType : byte
    {
        NotDefined,
        System,
        Dealer
    }
}