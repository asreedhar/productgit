using System;
using FirstLook.VehicleInformation.DomainModel.Specification.Model;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Repositories.Entities
{
    [Serializable]
    public class StyleProviderDocumentType : IEquatable<StyleProviderDocumentType>
    {
        public static readonly StyleProviderDocumentType ChromeStyle = new StyleProviderDocumentType(
            1, "Chrome Style", StyleProvider.Chrome);

        public static readonly StyleProviderDocumentType ChromeReference = new StyleProviderDocumentType(
            2, "Chrome Reference", StyleProvider.Chrome);

        public static StyleProviderDocumentType Get(int id)
        {
            switch (id)
            {
                case 1:
                    return ChromeStyle;
                case 2:
                    return ChromeReference;
            }

            throw new ArgumentOutOfRangeException("id");
        }

        private readonly int _id;
        private readonly StyleProvider _styleProvider;
        private readonly string _name;

        public StyleProviderDocumentType(int id, string name, StyleProvider styleProvider)
        {
            _id = id;
            _name = name;
            _styleProvider = styleProvider;
        }

        public int Id
        {
            get { return _id; }
        }

        public StyleProvider StyleProvider
        {
            get { return _styleProvider; }
        }

        public string Name
        {
            get { return _name; }
        }

        public bool Equals(StyleProviderDocumentType other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other._id == _id && Equals(other._styleProvider, _styleProvider) && Equals(other._name, _name);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (StyleProviderDocumentType)) return false;
            return Equals((StyleProviderDocumentType) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int result = _id;
                result = (result*397) ^ (_styleProvider != null ? _styleProvider.GetHashCode() : 0);
                result = (result*397) ^ (_name != null ? _name.GetHashCode() : 0);
                return result;
            }
        }
    }
}