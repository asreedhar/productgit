using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.VehicleInformation.DomainModel.Specification.Model;
using FirstLook.VehicleInformation.DomainModel.Specification.Repositories.Entities;
using FirstLook.VehicleInformation.DomainModel.Specification.Repositories.Gateways;
using FirstLook.VehicleInformation.DomainModel.Utility;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Repositories.Mappers
{
    public class SpecificationMapper : MapperBase
    {
        private readonly SpecificationGateway _gatewayHead;
        private readonly SpecificationBodyGateway _gatewayBody;
        private readonly SpecificationDealerGateway _gatewayLink;
        private readonly StyleMapper _mapper;

        public SpecificationMapper()
        {
            _gatewayHead = new SpecificationGateway();
            _gatewayBody = new SpecificationBodyGateway();
            _gatewayLink = new SpecificationDealerGateway();

            _mapper = new StyleMapper();
        }

        public SpecificationEntity Fetch(VehicleIdentification vehicle)
        {
            SpecificationGateway.Row head = _gatewayHead.Fetch(vehicle.Id);

            if (head == null)
            {
                return null;
            }

            return Fetch(vehicle, null, head);
        }

        public SpecificationEntity Fetch(VehicleIdentification vehicle, IBroker broker)
        {
            SpecificationGateway.Row head = _gatewayHead.Fetch(
                vehicle.Id,
                broker.Id);

            if (head == null)
            {
                return null;
            }

            return Fetch(vehicle, broker, head);
        }

        public SpecificationEntity Save(Model.Specification specification)
        {
            // natural key value-objects

            VehicleIdentification vehicle = specification.Vehicle;

            // calculate specification type

            IBroker broker = specification.Dealer;

            SpecificationType type = (broker == null)
                                         ? SpecificationType.System
                                         : SpecificationType.Dealer;

            // save style first of all

            IStyle style = _mapper.Save(specification.Style);

            // see if it's an entity
            
            SpecificationEntity entity = specification as SpecificationEntity;

            if (entity == null)
            {
                switch (type)
                {
                    case SpecificationType.System:
                        entity = Fetch(vehicle);
                        break;
                    case SpecificationType.Dealer:
                        entity = Fetch(vehicle, broker);
                        break;
                }
                
                if (entity != null)
                {
                    entity.VehicleDocument = specification.VehicleDocument;
                    entity.BuildDocument = specification.BuildDocument;
                    entity.Style = style;
                }
            }

            if (entity == null) // insert
            {
                AuditRow headAuditRow = Insert();

                SpecificationGateway.Row head = _gatewayHead.Insert(
                    headAuditRow.Id,
                    type,
                    vehicle.Id);

                AuditRow bodyAuditRow = Insert();

                SpecificationBodyGateway.Row body = _gatewayBody.Insert(
                    bodyAuditRow.Id,
                    head.Id,
                    specification.Version,
                    style.Id,
                    specification.VehicleDocument,
                    specification.BuildDocument);

                if (broker != null)
                {
                    AuditRow linkAuditRow = Insert();

                    _gatewayLink.Insert(
                        linkAuditRow.Id,
                        head.Id,
                        broker.Id);
                }

                entity = new SpecificationEntity(
                    new SpecificationIdentity(
                        head.Id,
                        headAuditRow,
                        head.Type),
                    bodyAuditRow,
                    body.Version,
                    broker,
                    vehicle,
                    style,
                    body.VehicleDocument,
                    body.BuildDocument);
            }
            else
            {
                if (entity.IsDirty)
                {
                    AuditRow newAuditRow = Update(entity.AuditRow);

                    _gatewayBody.Insert(
                        newAuditRow.Id,
                        entity.Id,
                        entity.Version,
                        entity.Style.Id,
                        entity.VehicleDocument,
                        entity.BuildDocument);

                    entity.OnSaved(this, new SavedEventArgs(newAuditRow));
                }
            }

            return entity;
        }

        private SpecificationEntity Fetch(VehicleIdentification vehicle, IBroker broker, SpecificationGateway.Row head)
        {
            SpecificationBodyGateway.Row body = _gatewayBody.Fetch(head.Id);

            AuditRow headAuditRow = AuditRowGateway.Fetch(head.AuditRowId);

            AuditRow bodyAuditRow = AuditRowGateway.Fetch(body.AuditRowId);

            IStyle style = _mapper.Fetch(new StyleKey(body.StyleId));

            return new SpecificationEntity(
                new SpecificationIdentity(
                    head.Id,
                    headAuditRow,
                    head.Type),
                bodyAuditRow,
                body.Version,
                broker,
                vehicle,
                style,
                body.VehicleDocument,
                body.BuildDocument);
        }
    }
}