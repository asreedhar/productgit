using System;
using System.IO;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.VehicleInformation.DomainModel.Specification.Model;
using FirstLook.VehicleInformation.DomainModel.Specification.Repositories.Entities;
using FirstLook.VehicleInformation.DomainModel.Specification.Repositories.Gateways;
using FirstLook.VehicleInformation.DomainModel.Utility;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Repositories.Mappers
{
    public class StyleMapper : MapperBase
    {
        private readonly StyleGateway _gatewayStyle;
        private readonly StyleChromeGateway _gatewayStyleChrome;
        private readonly StyleChromeStatusGateway _gatewayStyleChromeStatus;
        private readonly StyleSummaryGateway _gatewayStyleSummary;
        private readonly StyleProviderDocumentGateway _gatewayStyleProviderDocument;

        public StyleMapper()
        {
            _gatewayStyle = new StyleGateway();

            _gatewayStyleChrome = new StyleChromeGateway();

            _gatewayStyleChromeStatus = new StyleChromeStatusGateway();

            _gatewayStyleSummary = new StyleSummaryGateway();

            _gatewayStyleProviderDocument = new StyleProviderDocumentGateway();
        }

        public IStyle Fetch(IStyleKey key)
        {
            StyleGateway.Row head = _gatewayStyle.Fetch(key.Id);

            if (head == null)
            {
                return null;
            }

            StyleProvider provider = StyleProvider.Get(head.StyleProviderId);

            IStyle style;

            if (provider == StyleProvider.Chrome)
            {
                StyleChromeGateway.Row body = _gatewayStyleChrome.Fetch(key.Id);

                StyleChromeStatusGateway.Row status = _gatewayStyleChromeStatus.Fetch(key.Id);

                StyleSummaryGateway.Row summary = _gatewayStyleSummary.Fetch(key.Id);

                if (body == null || summary == null)
                {
                    throw new InvalidDataException();
                }

                StyleProviderDocumentGateway.Row reference =
                    _gatewayStyleProviderDocument.Fetch(body.ChromeReferenceDocumentId);

                StyleProviderDocumentType referenceType =
                    StyleProviderDocumentType.Get(reference.StyleProviderDocumentTypeId);

                StyleProviderDocumentGateway.Row document =
                    _gatewayStyleProviderDocument.Fetch(body.ChromeStyleDocumentId);

                StyleProviderDocumentType documentType =
                    StyleProviderDocumentType.Get(document.StyleProviderDocumentTypeId);

                style = Fetch(head, provider, body, summary, reference, document, documentType, referenceType, status);
            }
            else
            {
                throw new NotSupportedException();
            }

            return style;
        }

        public IStyle Save(IStyle istyle)
        {
            StyleProvider provider = istyle.StyleProvider;

            if (provider == StyleProvider.Chrome)
            {
                ChromeStyle style = (ChromeStyle) istyle;

                ChromeStyleEntity entity = style as ChromeStyleEntity;

                if (entity == null)
                {
                    StyleChromeGateway.Row body = _gatewayStyleChrome.Fetch(
                        style.ChromeStyleId,
                        style.Document.Version,
                        style.ReferenceDocument.Version);

                    if (body != null)
                    {
                        entity = (ChromeStyleEntity) Fetch(new StyleKey(body.Id));

                        entity.Status.IsMappingUpdated = style.Status.IsMappingUpdated;
                        entity.Status.IsDeleted = style.Status.IsDeleted;
                        entity.Status.IsUpdated = style.Status.IsUpdated;
                    }
                }

                if (entity == null)
                {
                    AuditRow documentAudit = Insert();

                    StyleProviderDocumentType documentType = StyleProviderDocumentType.ChromeStyle;

                    StyleProviderDocumentGateway.Row document = _gatewayStyleProviderDocument.Insert(
                        documentAudit.Id,
                        documentType.Id,
                        style.Document.Document,
                        style.Document.Version);

                    AuditRow referenceAudit = Insert();

                    StyleProviderDocumentType referenceType = StyleProviderDocumentType.ChromeReference;

                    StyleProviderDocumentGateway.Row reference = _gatewayStyleProviderDocument.Insert(
                        referenceAudit.Id,
                        referenceType.Id,
                        style.ReferenceDocument.Document,
                        style.ReferenceDocument.Version);

                    AuditRow headAudit = Insert();

                    StyleGateway.Row head = _gatewayStyle.Insert(
                        headAudit.Id,
                        provider.Id);

                    AuditRow bodyAudit = Insert();

                    StyleChromeGateway.Row body = _gatewayStyleChrome.Insert(
                        bodyAudit.Id,
                        head.Id,
                        style.ChromeStyleId,
                        document.Id,
                        reference.Id);

                    AuditRow statusAudit = Insert();

                    StyleChromeStatusGateway.Row status = _gatewayStyleChromeStatus.Insert(
                        statusAudit.Id,
                        head.Id,
                        style.Status.IsMappingUpdated,
                        style.Status.IsDeleted,
                        style.Status.IsUpdated);

                    AuditRow summaryAudit = Insert();

                    StyleSummary styleSummary = style.StyleSummary;

                    StyleSummaryGateway.Row summary = _gatewayStyleSummary.Insert(
                        summaryAudit.Id,
                        head.Id,
                        styleSummary.ManufacturerName,
                        styleSummary.DivisionName,
                        styleSummary.SubdivisionName,
                        styleSummary.ModelName,
                        styleSummary.StyleName,
                        styleSummary.TrimName,
                        styleSummary.BodyStyleName,
                        styleSummary.ModelYear);

                    entity = Fetch(head, provider, body, summary, reference, document, documentType, referenceType,
                                   status);
                }
                else
                {
                    ChromeStyleStatusEntity status = (ChromeStyleStatusEntity) entity.Status;

                    ISavable savable = status;

                    if (savable.IsDirty)
                    {
                        AuditRow statusAudit = Update(savable.AuditRow);

                        _gatewayStyleChromeStatus.Insert(
                            statusAudit.Id,
                            entity.Id,
                            status.IsMappingUpdated,
                            status.IsDeleted,
                            status.IsUpdated);

                        savable.OnSaved(this, new SavedEventArgs(statusAudit));
                    }
                }

                return entity;
            }

            throw new NotSupportedException();
        }


        private ChromeStyleEntity Fetch(
            StyleGateway.Row head,
            StyleProvider provider,
            StyleChromeGateway.Row body,
            StyleSummaryGateway.Row summary,
            StyleProviderDocumentGateway.Row reference,
            StyleProviderDocumentGateway.Row document,
            StyleProviderDocumentType documentType,
            StyleProviderDocumentType referenceType,
            StyleChromeStatusGateway.Row status)
        {
            ChromeStyleEntity style = new ChromeStyleEntity(
                new StyleIdentity(
                    AuditRowGateway.Fetch(head.AuditRowId),
                    body.Id),
                AuditRowGateway.Fetch(body.AuditRowId),
                provider,
                new StyleSummaryEntity(
                    AuditRowGateway.Fetch(summary.AuditRowId),
                    summary.Id,
                    summary.ManufacturerName,
                    summary.DivisionName,
                    summary.SubdivisionName,
                    summary.ModelName,
                    summary.StyleName,
                    summary.TrimName,
                    summary.BodyStyleName,
                    summary.ModelYear),
                new StyleProviderDocumentEntity(
                    document.Id,
                    AuditRowGateway.Fetch(document.AuditRowId),
                    documentType,
                    document.DocumentVersion,
                    document.Document
                    ),
                new StyleProviderDocumentEntity(
                    reference.Id,
                    AuditRowGateway.Fetch(reference.AuditRowId),
                    referenceType,
                    reference.DocumentVersion,
                    reference.Document
                    ),
                body.ChromeStyleId,
                new ChromeStyleStatusEntity(
                    status.Id,
                    AuditRowGateway.Fetch(status.AuditRowId),
                    status.IsMappingUpdated,
                    status.IsDeleted,
                    status.IsUpdated));
            return style;
        }
    }
}