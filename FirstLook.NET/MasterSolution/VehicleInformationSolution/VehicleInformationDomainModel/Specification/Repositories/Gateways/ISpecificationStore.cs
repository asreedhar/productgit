using System;
using System.Data;
using System.Reflection;
using System.Xml;
using FirstLook.Common.Core.Data;
using FirstLook.VehicleInformation.DomainModel.Specification.Repositories.Entities;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Repositories.Gateways
{
    public interface ISpecificationStore
    {
        void Specification_Fetch(Action<IDataReader> f, int vehicleId);

        void Specification_Fetch(Action<IDataReader> f, int vehicleId, int clientId);

        void Specification_Insert(Action<IDataReader> f,
                                  int auditRowId,
                                  SpecificationType type,
                                  int vehicleId);

        void Specification_Body_Fetch(Action<IDataReader> f, int id);

        void Specification_Body_Insert(int auditRowId,
                                      int id,
                                      short version,
                                      int styleId,
                                      XmlDocument vehicleDocument,
                                      XmlDocument buildDocument);

        void Specification_Client_Insert(int auditRowId, int id, int clientId);

        void Style_Fetch(Action<IDataReader> f, int id);

        void Style_Insert(Action<IDataReader> f, int auditRowId, int styleProviderId);

        void Style_Chrome_Fetch(Action<IDataReader> f, int id);

        void Style_Chrome_Fetch(Action<IDataReader> f, int chromeStyleId, int styleDocumentVersion, int referenceDocumentVersion);

        void Style_Chrome_Insert(int auditRowId, int id, int chromeStyleId, int styleDocumentId, int referenceDocumentId);

        void Style_Chrome_Status_Fetch(Action<IDataReader> f, int id);

        void Style_Chrome_Status_Insert(int auditRowId, int id, bool isMappingUpdated, bool isDeleted, bool isUpdated);

        void Style_Summary_Fetch(Action<IDataReader> f, int id);

        void Style_Summary_Insert(int auditRowId,
                                  int id,
                                  string manufacturerName,
                                  string divisionName,
                                  string subdivisionName,
                                  string modelName,
                                  string styleName,
                                  string trimName,
                                  string bodyStyleName,
                                  int modelYear);

        void StyleProviderDocument_Fetch(Action<IDataReader> f, int id);

        void StyleProviderDocument_Insert(Action<IDataReader> f, int auditRowId, int styleProviderDocumentTypeId, XmlDocument document, int documentVersion);
    }

    public class DbSpecificationStore : SessionDataStore, ISpecificationStore
    {
        private const string Prefix = "FirstLook.VehicleInformation.DomainModel.Specification.Repositories.Resources";

        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        public void Specification_Fetch(Action<IDataReader> f, int vehicleId)
        {
            const string fetch = Prefix + ".SpecificationGateway_Fetch.txt";

            Query(f, fetch, new Parameter("VehicleID", vehicleId, DbType.Int32));
        }

        public void Specification_Fetch(Action<IDataReader> f, int vehicleId, int clientId)
        {
            const string fetch = Prefix + ".SpecificationGateway_Fetch_Dealer.txt";

            Query(f, fetch,
                new Parameter("VehicleID", vehicleId, DbType.Int32),
                new Parameter("ClientID", clientId, DbType.Int32));
        }

        public void Specification_Insert(Action<IDataReader> f, int auditRowId, SpecificationType type, int vehicleId)
        {
            const string insert = Prefix + ".SpecificationGateway_Insert.txt";

            NonQuery(insert,
                new Parameter("SpecificationTypeID", (byte)type, DbType.Int32),
                new Parameter("VehicleID", vehicleId, DbType.Int32),
                new Parameter("AuditRowID", auditRowId, DbType.Int32));

            const string fetch = Prefix + ".SpecificationGateway_Fetch_Identity.txt";

            Query(f, fetch);
        }

        public void Specification_Body_Fetch(Action<IDataReader> f, int id)
        {
            const string fetch = Prefix + ".SpecificationGateway_Body_Fetch_Id.txt";

            Query(f, fetch, new Parameter("SpecificationID", id, DbType.Int32));
        }

        public void Specification_Body_Insert(int auditRowId, int id, short version, int styleId, XmlDocument vehicleDocument, XmlDocument buildDocument)
        {
            const string insert = Prefix + ".SpecificationGateway_Body_Insert.txt";

            NonQuery(insert,
                new Parameter("SpecificationID", id, DbType.Int32),
                new Parameter("SpecificationVersion", version, DbType.Byte),
                new Parameter("StyleID", styleId, DbType.Int32),
                new Parameter("VehicleDocument", DataRecord.ToXml(vehicleDocument), DbType.Xml),
                new Parameter("BuildDocument", DataRecord.ToXml(buildDocument), DbType.Xml),
                new Parameter("AuditRowID", auditRowId, DbType.Int32));
        }

        public void Specification_Client_Insert(int auditRowId, int id, int clientId)
        {
            const string insert = Prefix + ".SpecificationGateway_Client_Insert.txt";

            NonQuery(insert,
                new Parameter("SpecificationID", id, DbType.Int32),
                new Parameter("ClientID", clientId, DbType.Int32),
                new Parameter("AuditRowID", auditRowId, DbType.Int32));
        }

        public void Style_Fetch(Action<IDataReader> f, int id)
        {
            const string fetch = Prefix + ".StyleGateway_Fetch_Id.txt";

            Query(f, fetch, new Parameter("StyleID", id, DbType.Int32));
        }

        public void Style_Insert(Action<IDataReader> f, int auditRowId, int styleProviderId)
        {
            const string insert = Prefix + ".StyleGateway_Insert.txt";

            NonQuery(insert,
                new Parameter("StyleProviderID", (byte) styleProviderId, DbType.Byte),
                new Parameter("AuditRowID", auditRowId, DbType.Int32));

            const string fetch = Prefix + ".StyleGateway_Fetch_Identity.txt";

            Query(f, fetch);
        }

        public void Style_Chrome_Fetch(Action<IDataReader> f, int id)
        {
            const string fetch = Prefix + ".StyleGateway_Chrome_Fetch_Id.txt";

            Query(f, fetch, new Parameter("StyleID", id, DbType.Int32));
        }

        public void Style_Chrome_Fetch(Action<IDataReader> f, int chromeStyleId, int styleDocumentVersion, int referenceDocumentVersion)
        {
            const string fetch = Prefix + ".StyleGateway_Chrome_Fetch_NaturalKey.txt";

            Query(f, fetch,
                new Parameter("ChromeStyleID", chromeStyleId, DbType.Int32),
                new Parameter("DocumentVersion", chromeStyleId, DbType.Int32),
                new Parameter("ReferenceDocumentVersion", chromeStyleId, DbType.Int32));
        }

        public void Style_Chrome_Insert(int auditRowId, int id, int chromeStyleId, int styleDocumentId, int referenceDocumentId)
        {
            const string insert = Prefix + ".StyleGateway_Chrome_Insert.txt";

            NonQuery(insert,
                new Parameter("StyleID", id, DbType.Int32),
                new Parameter("ChromeStyleID", chromeStyleId, DbType.Int32),
                new Parameter("ChromeStyleDocumentID", styleDocumentId, DbType.Int32),
                new Parameter("ChromeReferenceDocumentID", referenceDocumentId, DbType.Int32),
                new Parameter("AuditRowID", auditRowId, DbType.Int32));
        }

        public void Style_Chrome_Status_Fetch(Action<IDataReader> f, int id)
        {
            const string fetch = Prefix + ".StyleGateway_Chrome_Status_Fetch.txt";

            Query(f, fetch, new Parameter("StyleID", id, DbType.Int32));
        }

        public void Style_Chrome_Status_Insert(int auditRowId, int id, bool isMappingUpdated, bool isDeleted, bool isUpdated)
        {
            const string insert = Prefix + ".StyleGateway_Chrome_Status_Insert.txt";

            NonQuery(insert,
                     new Parameter("StyleID", id, DbType.Int32),
                     new Parameter("IsMappingUpdated", isMappingUpdated, DbType.Boolean),
                     new Parameter("IsDeleted", isDeleted, DbType.Boolean),
                     new Parameter("IsUpdated", isUpdated, DbType.Boolean),
                     new Parameter("AuditRowID", auditRowId, DbType.Int32));
        }

        public void Style_Summary_Fetch(Action<IDataReader> f, int id)
        {
            const string fetch = Prefix + ".StyleGateway_Summary_Fetch.txt";

            Query(f, fetch, new Parameter("StyleID", id, DbType.Int32));
        }

        public void Style_Summary_Insert(int auditRowId, int id, string manufacturerName, string divisionName, string subdivisionName, string modelName, string styleName, string trimName, string bodyStyleName, int modelYear)
        {
            const string insert = Prefix + ".StyleGateway_Summary_Insert.txt";

            NonQuery(insert,
                     new Parameter("StyleID", id, DbType.Int32),
                     new Parameter("Manufacturer", manufacturerName, DbType.String),
                     new Parameter("Division", divisionName, DbType.String),
                     new Parameter("Subdivision", subdivisionName, DbType.String),
                     new Parameter("Model", modelName, DbType.String),
                     new Parameter("Style", styleName, DbType.String),
                     new Parameter("Trim", trimName, DbType.String),
                     new Parameter("BodyStyle", bodyStyleName, DbType.String),
                     new Parameter("ModelYear", modelYear, DbType.Int16),
                     new Parameter("AuditRowID", auditRowId, DbType.Int32));
        }

        public void StyleProviderDocument_Fetch(Action<IDataReader> f, int id)
        {
            const string fetch = Prefix + ".StyleProviderDocumentGateway_Fetch_Id.txt";

            Query(f, fetch, new Parameter("StyleProviderDocumentID", id, DbType.Int32));
        }

        public void StyleProviderDocument_Insert(Action<IDataReader> f, int auditRowId, int styleProviderDocumentTypeId, XmlDocument document, int documentVersion)
        {
            const string insert = Prefix + ".StyleProviderDocumentGateway_Insert.txt";

            NonQuery(insert,
                     new Parameter("StyleProviderDocumentTypeID", styleProviderDocumentTypeId, DbType.Byte),
                     new Parameter("Document", DataRecord.ToXml(document), DbType.Xml),
                     new Parameter("DocumentVersion", documentVersion, DbType.Int32),
                     new Parameter("AuditRowID", auditRowId, DbType.Int32));

            const string fetch = Prefix + ".StyleProviderDocumentGateway_Fetch_Identity.txt";

            Query(f, fetch);
        }
    }
}