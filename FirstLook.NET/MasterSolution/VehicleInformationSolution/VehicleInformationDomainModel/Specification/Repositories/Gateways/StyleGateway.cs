using System.Data;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Repositories.Gateways
{
    public class StyleGateway : GatewayBase
    {
        public class Row
        {
            public int Id;
            public byte StyleProviderId;
            public int AuditRowId;
        }

        public Row Fetch(int id)
        {
            string key = CreateCacheKey(id);

            Row row = Cache.Get(key) as Row;

            if (row == null)
            {
                ISpecificationStore store = Resolve<ISpecificationStore>();

                store.Style_Fetch(
                    delegate(IDataReader reader)
                        {
                            if (reader.Read())
                            {
                                row = Fetch(reader);
                            }
                        },
                    id);

                Remember(key, row);
            }

            return row;
        }

        public Row Insert(int auditRowId, int styleProviderId)
        {
            ISpecificationStore store = Resolve<ISpecificationStore>();

            Row row = null;

            store.Style_Insert(
                delegate(IDataReader reader)
                    {
                        if (reader.Read())
                        {
                            row = Fetch(reader);
                        }
                    },
                auditRowId,
                styleProviderId);

            return row;
        }

        private static Row Fetch(IDataRecord record)
        {
            Row row = new Row();
            row.AuditRowId = record.GetInt32(record.GetOrdinal("AuditRowId"));
            row.Id = record.GetInt32(record.GetOrdinal("StyleId"));
            row.StyleProviderId = record.GetByte(record.GetOrdinal("StyleProviderId"));
            return row;
        }
    }

    public class StyleChromeGateway : GatewayBase
    {
        public class Row
        {
            public int Id;
            public int ChromeStyleId;
            public int ChromeStyleDocumentId;
            public int ChromeReferenceDocumentId;
            public int AuditRowId;
        }

        public Row Fetch(int id)
        {
            string key = CreateCacheKey(id);

            Row row = Cache.Get(key) as Row;

            if (row == null)
            {
                ISpecificationStore store = Resolve<ISpecificationStore>();

                store.Style_Chrome_Fetch(
                    delegate(IDataReader reader)
                        {
                            if (reader.Read())
                            {
                                row = Fetch(reader);
                            }
                        },
                    id);

                Remember(key, row);
            }

            return row;
        }

        public Row Fetch(int chromeStyleId, int chromeStyleVersion, int chromeReferenceVersion)
        {
            ISpecificationStore store = Resolve<ISpecificationStore>();

            Row row = null;

            store.Style_Chrome_Fetch(
                delegate(IDataReader reader)
                    {
                        if (reader.Read())
                        {
                            row = Fetch(reader);
                        }
                    },
                chromeStyleId,
                chromeStyleVersion,
                chromeReferenceVersion);

            Remember(row);

            return row;
        }

        public Row Insert(int auditRowId, int id, int chromeStyleId, int styleDocumentId, int referenceDocumentId)
        {
            ISpecificationStore store = Resolve<ISpecificationStore>();

            store.Style_Chrome_Insert(
                auditRowId,
                id,
                chromeStyleId,
                styleDocumentId,
                referenceDocumentId);

            Row row = new Row();
            row.AuditRowId = auditRowId;
            row.ChromeReferenceDocumentId = referenceDocumentId;
            row.ChromeStyleDocumentId = styleDocumentId;
            row.ChromeStyleId = chromeStyleId;
            row.Id = id;

            return row;
        }

        private static Row Fetch(IDataRecord record)
        {
            Row row = new Row();
            row.AuditRowId = record.GetInt32(record.GetOrdinal("AuditRowId"));
            row.ChromeReferenceDocumentId = record.GetInt32(record.GetOrdinal("ChromeReferenceDocumentId"));
            row.ChromeStyleDocumentId = record.GetInt32(record.GetOrdinal("ChromeStyleDocumentId"));
            row.ChromeStyleId = record.GetInt32(record.GetOrdinal("ChromeStyleId"));
            row.Id = record.GetInt32(record.GetOrdinal("StyleID"));
            return row;
        }

        private void Remember(Row row)
        {
            if (row != null)
            {
                string key = CreateCacheKey(row.Id);

                Remember(key, row);
            }
        }
    }

    public class StyleChromeStatusGateway : GatewayBase
    {
        public class Row
        {
            public int Id;
            public bool IsMappingUpdated;
            public bool IsUpdated;
            public bool IsDeleted;
            public int AuditRowId;
        }

        public Row Fetch(int id)
        {
            string key = CreateCacheKey(id);

            Row row = Cache.Get(key) as Row;

            if (row == null)
            {
                ISpecificationStore store = Resolve<ISpecificationStore>();

                store.Style_Chrome_Status_Fetch(
                    delegate(IDataReader reader)
                        {
                            if (reader.Read())
                            {
                                row = Fetch(reader);
                            }
                        },
                    id);

                Remember(key, row);
            }

            return row;
        }

        public Row Insert(int auditRowId, int id, bool isMappingUpdated, bool isDeleted, bool isUpdated)
        {
            ISpecificationStore store = Resolve<ISpecificationStore>();

            store.Style_Chrome_Status_Insert(
                auditRowId,
                id,
                isMappingUpdated,
                isDeleted,
                isUpdated);

            Row row = new Row();
            row.AuditRowId = auditRowId;
            row.Id = id;
            row.IsDeleted = isDeleted;
            row.IsMappingUpdated = isMappingUpdated;
            row.IsUpdated = isUpdated;
            
            return row;
        }

        private static Row Fetch(IDataRecord record)
        {
            Row row = new Row();
            row.AuditRowId = record.GetInt32(record.GetOrdinal("AuditRowId"));
            row.Id = record.GetInt32(record.GetOrdinal("StyleID"));
            row.IsDeleted = record.GetBoolean(record.GetOrdinal("IsDeleted"));
            row.IsMappingUpdated = record.GetBoolean(record.GetOrdinal("IsMappingUpdated"));
            row.IsUpdated = record.GetBoolean(record.GetOrdinal("IsUpdated"));
            return row;
        }
    }

    public class StyleSummaryGateway : GatewayBase
    {
        public class Row
        {
            public int Id;
            public string ManufacturerName;
            public string DivisionName;
            public string SubdivisionName;
            public string ModelName;
            public string StyleName;
            public string TrimName;
            public string BodyStyleName;
            public int ModelYear;
            public int AuditRowId;
        }

        public Row Fetch(int id)
        {
            string key = CreateCacheKey(id);

            Row row = Cache.Get(key) as Row;

            if (row == null)
            {
                ISpecificationStore store = Resolve<ISpecificationStore>();

                store.Style_Summary_Fetch(
                    delegate(IDataReader reader)
                        {
                            if (reader.Read())
                            {
                                row = Fetch(reader);
                            }
                        },
                    id);

                Remember(key, row);
            }

            return row;
        }


        public Row Insert(int auditRowId, int id, string manufacturerName, string divisionName, string subdivisionName,
                          string modelName, string styleName, string trimName, string bodyStyleName, int modelYear)
        {
            ISpecificationStore store = Resolve<ISpecificationStore>();

            store.Style_Summary_Insert(
                auditRowId,
                id,
                manufacturerName,
                divisionName,
                subdivisionName,
                modelName,
                styleName,
                trimName,
                bodyStyleName,
                modelYear);

            Row row = new Row();

            row.AuditRowId = auditRowId;
            row.BodyStyleName = bodyStyleName;
            row.DivisionName = divisionName;
            row.Id = id;
            row.ManufacturerName = manufacturerName;
            row.ModelName = modelName;
            row.ModelYear = modelYear;
            row.StyleName = styleName;
            row.TrimName = trimName;

            return row;
        }

        private static Row Fetch(IDataRecord record)
        {
            Row row = new Row();
            row.AuditRowId = record.GetInt32(record.GetOrdinal("AuditRowId"));
            row.BodyStyleName = DataRecord.GetString(record, "BodyStyle");
            row.DivisionName = DataRecord.GetString(record, "Division");
            row.Id = record.GetInt32(record.GetOrdinal("StyleID"));
            row.ManufacturerName = DataRecord.GetString(record, "Manufacturer");
            row.ModelName = DataRecord.GetString(record, "Model");
            row.ModelYear = record.GetInt16(record.GetOrdinal("ModelYear"));
            row.StyleName = DataRecord.GetString(record, "Style");
            row.TrimName = DataRecord.GetString(record, "Trim");
            return row;
        }
    }
}