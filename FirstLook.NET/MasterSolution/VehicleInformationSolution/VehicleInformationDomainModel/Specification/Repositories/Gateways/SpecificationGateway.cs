using System.Data;
using System.Xml;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;
using FirstLook.VehicleInformation.DomainModel.Specification.Repositories.Entities;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Repositories.Gateways
{
    public class SpecificationGateway : GatewayBase
    {
        public class Row
        {
            public int Id;
            public SpecificationType Type;
            public int VehicleId;
            public int AuditRowId;
        }

        public Row Fetch(int vehicleId)
        {
            string key = CreateCacheKey(vehicleId);

            Row row = Cache.Get(key) as Row;

            if (row == null)
            {
                ISpecificationStore store = Resolve<ISpecificationStore>();

                store.Specification_Fetch(
                    delegate(IDataReader reader)
                        {
                            if (reader.Read())
                            {
                                row = Fetch(reader);
                            }
                        },
                    vehicleId);

                Remember(key, row);
            }

            return row;
        }

        public Row Fetch(int vehicleId, int dealerId)
        {
            string key = CreateCacheKey(vehicleId, dealerId);

            Row row = Cache.Get(key) as Row;

            if (row == null)
            {
                ISpecificationStore store = Resolve<ISpecificationStore>();

                store.Specification_Fetch(
                    delegate(IDataReader reader)
                        {
                            if (reader.Read())
                            {
                                row = Fetch(reader);
                            }
                        },
                    vehicleId,
                    dealerId);

                Remember(key, row);
            }

            return row;
        }

        public Row Insert(int auditRowId, SpecificationType type, int vehicleId)
        {
            ISpecificationStore store = Resolve<ISpecificationStore>();

            Row row = null;

            store.Specification_Insert(
                delegate(IDataReader reader)
                    {
                        if (reader.Read())
                        {
                            row = Fetch(reader);
                        }
                    },
                auditRowId,
                type,
                vehicleId);

            return row;
        }

        private static Row Fetch(IDataRecord reader)
        {
            Row row = new Row();
            row.AuditRowId = reader.GetInt32(reader.GetOrdinal("AuditRowId"));
            row.Id = reader.GetInt32(reader.GetOrdinal("SpecificationID"));
            row.Type = (SpecificationType) reader.GetByte(reader.GetOrdinal("SpecificationTypeId"));
            row.VehicleId = reader.GetInt32(reader.GetOrdinal("VehicleId"));
            return row;
        }
    }

    public class SpecificationBodyGateway : GatewayBase
    {
        public class Row
        {
            public int Id;
            public short Version;
            public int StyleId;
            public XmlDocument VehicleDocument;
            public XmlDocument BuildDocument;
            public int AuditRowId;
        }

        public Row Fetch(int id)
        {
            string key = CreateCacheKey(id);

            Row row = Cache.Get(key) as Row;

            if (row == null)
            {
                ISpecificationStore store = Resolve<ISpecificationStore>();

                store.Specification_Body_Fetch(
                    delegate(IDataReader reader)
                        {
                            if (reader.Read())
                            {
                                row = new Row();
                                row.AuditRowId = reader.GetInt32(reader.GetOrdinal("AuditRowId"));
                                row.BuildDocument = DataRecord.GetXmlDocument(reader, "BuildDocument");
                                row.Id = reader.GetInt32(reader.GetOrdinal("SpecificationID"));
                                row.StyleId = reader.GetInt32(reader.GetOrdinal("StyleId"));
                                row.VehicleDocument = DataRecord.GetXmlDocument(reader, "VehicleDocument");
                                row.Version = reader.GetInt16(reader.GetOrdinal("SpecificationVersion"));
                            }
                        },
                    id);

                Remember(key, row);
            }

            return row;
        }

        public Row Insert(int auditRowId, int id, short version, int styleId, XmlDocument vehicleDocument,
                          XmlDocument buildDocument)
        {
            string key = CreateCacheKey(id);

            Forget(key);

            Resolve<ISpecificationStore>().Specification_Body_Insert(
                auditRowId,
                id,
                version,
                styleId,
                vehicleDocument,
                buildDocument);

            Row row = new Row();
            row.AuditRowId = auditRowId;
            row.BuildDocument = buildDocument;
            row.Id = id;
            row.StyleId = styleId;
            row.VehicleDocument = vehicleDocument;
            row.Version = version;

            return row;
        }
    }

    public class SpecificationDealerGateway : GatewayBase
    {
        public class Row
        {
            public int Id;
            public int DealerId;
            public int AuditRowId;
        }

        public Row Insert(int auditRowId, int id, int dealerId)
        {
            Resolve<ISpecificationStore>().Specification_Client_Insert(
                auditRowId,
                id,
                dealerId);

            Row row = new Row();
            row.Id = id;
            row.DealerId = dealerId;
            row.AuditRowId = auditRowId;

            return row;
        }
    }
}