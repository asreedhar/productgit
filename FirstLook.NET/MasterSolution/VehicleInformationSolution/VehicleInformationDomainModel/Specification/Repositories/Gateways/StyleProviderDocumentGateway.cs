using System.Data;
using System.Xml;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Repositories.Gateways
{
    public class StyleProviderDocumentGateway : GatewayBase
    {
        public class Row
        {
            public int Id;
            public int StyleProviderDocumentTypeId;
            public XmlDocument Document;
            public int DocumentVersion;
            public int AuditRowId;
        }

        public Row Fetch(int id)
        {
            string key = CreateCacheKey(id);

            Row row = Cache.Get(key) as Row;

            if (row == null)
            {
                ISpecificationStore store = Resolve<ISpecificationStore>();

                store.StyleProviderDocument_Fetch(
                    delegate(IDataReader reader)
                        {
                            if (reader.Read())
                            {
                                row = Fetch(reader);
                            }
                        },
                    id);

                Remember(key, row);
            }

            return row;
        }

        public Row Insert(int auditRowId, int styleProviderDocumentTypeId, XmlDocument document, int documentVersion)
        {
            ISpecificationStore store = Resolve<ISpecificationStore>();

            Row row = null;

            store.StyleProviderDocument_Insert(
                delegate(IDataReader reader)
                {
                    if (reader.Read())
                    {
                        row = Fetch(reader);
                    }
                },
                auditRowId,
                styleProviderDocumentTypeId,
                document,
                documentVersion);

            return row;
        }

        private static Row Fetch(IDataRecord record)
        {
            Row row = new Row();
            row.AuditRowId = record.GetInt32(record.GetOrdinal("AuditRowId"));
            row.Document = DataRecord.GetXmlDocument(record, "Document");
            row.DocumentVersion = record.GetInt32(record.GetOrdinal("DocumentVersion"));
            row.Id = record.GetInt32(record.GetOrdinal("StyleProviderDocumentID"));
            row.StyleProviderDocumentTypeId = record.GetByte(record.GetOrdinal("StyleProviderDocumentTypeId"));
            return row;
        }
    }
}