using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;
using FirstLook.VehicleInformation.DomainModel.Specification.Model;
using FirstLook.VehicleInformation.DomainModel.Specification.Repositories.Mappers;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Repositories
{
    public class SpecificationRepository : RepositoryBase, ISpecificationRepository
    {
        private readonly SpecificationMapper _specificationMapper;

        private readonly StyleMapper _styleMapper;

        public SpecificationRepository()
        {
            _specificationMapper = new SpecificationMapper();

            _styleMapper = new StyleMapper();
        }

        protected override string DatabaseName
        {
            get { return "Vehicle"; }
        }

        public Model.Specification Fetch(VehicleIdentification vehicle)
        {
            return DoInSession(() => _specificationMapper.Fetch(vehicle));
        }

        public Model.Specification Fetch(VehicleIdentification vehicle, IBroker broker)
        {
            return DoInSession(() => _specificationMapper.Fetch(vehicle, broker));
        }

        public Model.Specification Save(IPrincipal principal, Model.Specification specification)
        {
            return DoInTransaction(
                delegate(IDataSession session)
                {
                    Audit audit = session.Items["Audit"] as Audit;

                    if (audit == null)
                    {
                        audit = Resolve<IRepository>().Create(principal);

                        session.Items["Audit"] = audit;
                    }
                },
                () => _specificationMapper.Save(specification));
        }

        public IStyle Fetch(IStyleKey key)
        {
            return DoInSession(() => _styleMapper.Fetch(key));
        }

        public IStyle Save(IPrincipal principal, IStyle style)
        {
            return DoInTransaction(
                delegate(IDataSession session)
                {
                    Audit audit = session.Items["Audit"] as Audit;

                    if (audit == null)
                    {
                        audit = Resolve<IRepository>().Create(principal);

                        session.Items["Audit"] = audit;
                    }
                },
                () => _styleMapper.Save(style));
        }

        protected override void OnBeginTransaction(IDataSession session)
        {
            // suck ass
        }
    }
}