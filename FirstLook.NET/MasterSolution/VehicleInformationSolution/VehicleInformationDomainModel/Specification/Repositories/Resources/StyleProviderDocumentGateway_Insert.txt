
INSERT INTO Specification.StyleProviderDocument
        ( StyleProviderDocumentTypeID ,
          Document ,
          DocumentVersion ,
          AuditRowID
        )
VALUES  ( @StyleProviderDocumentTypeID,
          @Document,
          @DocumentVersion,
          @AuditRowID
        )
