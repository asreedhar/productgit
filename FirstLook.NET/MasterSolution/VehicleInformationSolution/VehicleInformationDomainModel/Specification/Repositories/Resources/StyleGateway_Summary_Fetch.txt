
SELECT  S.StyleID ,
        S.Manufacturer ,
        S.Division ,
        S.Subdivision ,
        S.Model ,
        S.Style ,
        S.Trim ,
        S.BodyStyle ,
        S.ModelYear ,
        S.AuditRowID
FROM    Specification.StyleSummary S
JOIN    Audit.AuditRow A ON A.AuditRowID = S.AuditRowID
WHERE   S.StyleID = @StyleID
AND     GETDATE() BETWEEN A.ValidFrom AND A.ValidUpTo
