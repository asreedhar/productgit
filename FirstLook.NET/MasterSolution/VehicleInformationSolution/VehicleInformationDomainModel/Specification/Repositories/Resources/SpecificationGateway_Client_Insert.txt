
INSERT INTO Specification.Specification_Client
        ( SpecificationID ,
          ClientID ,
          AuditRowID
        )
VALUES  ( @SpecificationID,
          @ClientID,
          @AuditRowID
        )
