﻿using FirstLook.Common.Core.Registry;
using FirstLook.VehicleInformation.DomainModel.Specification.Model;
using FirstLook.VehicleInformation.DomainModel.Specification.Repositories.Gateways;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Repositories
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ISpecificationRepository, SpecificationRepository>(ImplementationScope.Shared);

            registry.Register<ISpecificationStore, DbSpecificationStore>(ImplementationScope.Shared);
        }
    }
}
