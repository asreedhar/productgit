﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.VehicleInformation.DomainModel.Specification
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<Repositories.Module>();

            registry.Register<Commands.Module>();
        }
    }
}
