using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Data;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Model
{
    public interface ISpecificationRepository
    {
        Specification Fetch(VehicleIdentification vehicle);

        Specification Fetch(VehicleIdentification vehicle, IBroker broker);

        Specification Save(IPrincipal principal, Specification specification);

        IStyle Fetch(IStyleKey key);

        IStyle Save(IPrincipal principal, IStyle style);

        ITransactionFactory TransactionFactory { get; }
    }
}