using System;
using System.Xml;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Model
{
    public interface ISpecificationKey : IEquatable<ISpecificationKey>
    {
        int Id { get; }

        short Version { get; }
    }

    public class SpecificationKey : ISpecificationKey
    {
        private readonly int _id;
        private readonly short _version;

        public SpecificationKey(int id, short version)
        {
            _id = id;
            _version = version;
        }

        public int Id
        {
            get { return _id; }
        }

        public short Version
        {
            get { return _version; }
        }

        public bool Equals(ISpecificationKey other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other.Id == _id && other.Version == _version;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (ISpecificationKey)) return false;
            return Equals((ISpecificationKey) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (_id*397) ^ _version.GetHashCode();
            }
        }
    }

    [Serializable]
    public class Specification : PropertyChangeSupport, ISpecificationKey
    {
        private IBroker _dealer;
        private VehicleIdentification _vehicle;
        private IStyle _style;
        private XmlDocument _vehicleDocument;
        private XmlDocument _buildDocument;

        public virtual int Id
        {
            get { return 0; }
        }

        public virtual short Version
        {
            get { return 0; }
        }

        public IBroker Dealer
        {
            get { return _dealer; }
            set
            {
                if (!Equals(_dealer, value))
                {
                    Assignment(delegate { _dealer = value; }, "Dealer");
                }
            }
        }

        public VehicleIdentification Vehicle
        {
            get { return _vehicle; }
            set
            {
                if (!Equals(_vehicle, value))
                {
                    Assignment(delegate { _vehicle = value; }, "Vehicle");
                }
            }
        }

        public IStyle Style
        {
            get { return _style; }
            set
            {
                if (!Equals(_style, value))
                {
                    Assignment(delegate { _style = value; }, "Style");
                }
            }
        }

        public XmlDocument VehicleDocument
        {
            get { return _vehicleDocument; }
            set
            {
                if (!AreSame(_vehicleDocument, value))
                {
                    Assignment(delegate { _vehicleDocument = value; }, "VehicleDocument");
                }
            }
        }

        public XmlDocument BuildDocument
        {
            get { return _buildDocument; }
            set
            {
                if (!AreSame(_buildDocument, value))
                {
                    Assignment(delegate { _buildDocument = value; }, "BuildDocumnt");
                }
            }
        }

        public bool Equals(ISpecificationKey other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other.Id == Id && other.Version == Version;
        }
    }
}