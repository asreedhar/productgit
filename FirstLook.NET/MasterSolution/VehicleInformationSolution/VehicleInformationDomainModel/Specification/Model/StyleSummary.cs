using System;
using FirstLook.Common.Core;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Model
{
    [Serializable]
    public class StyleSummary : PropertyChangeSupport
    {
        private string _manufacturerName;
        private string _divisionName;
        private string _subdivisionName;
        private string _modelName;
        private string _styleName;
        private string _trimName;
        private string _bodyStyleName;
        private int _modelYear;

        public string ManufacturerName
        {
            get { return _manufacturerName; }
            set
            {
                if (!Equals(_manufacturerName, value))
                {
                    Assignment(delegate { _manufacturerName = value; }, "ManufacturerName");
                }
            }
        }

        public string DivisionName
        {
            get { return _divisionName; }
            set
            {
                if (!Equals(_divisionName, value))
                {
                    Assignment(delegate { _divisionName = value; }, "DivisionName");
                }
            }
        }

        public string SubdivisionName
        {
            get { return _subdivisionName; }
            set
            {
                if (!Equals(_subdivisionName, value))
                {
                    Assignment(delegate { _subdivisionName = value; }, "SubdivisionName");
                }
            }
        }

        public string ModelName
        {
            get { return _modelName; }
            set
            {
                if (!Equals(_modelName, value))
                {
                    Assignment(delegate { _modelName = value; }, "ModelName");
                }
            }
        }

        public string StyleName
        {
            get { return _styleName; }
            set
            {
                if (!Equals(_styleName, value))
                {
                    Assignment(delegate { _styleName = value; }, "StyleName");
                }
            }
        }

        public string TrimName
        {
            get { return _trimName; }
            set
            {
                if (!Equals(_trimName, value))
                {
                    Assignment(delegate { _trimName = value; }, "TrimName");
                }
            }
        }

        public string BodyStyleName
        {
            get { return _bodyStyleName; }
            set
            {
                if (!Equals(_bodyStyleName, value))
                {
                    Assignment(delegate { _bodyStyleName = value; }, "BodyStyleName");
                }
            }
        }

        public int ModelYear
        {
            get { return _modelYear; }
            set
            {
                if (!Equals(_modelYear, value))
                {
                    Assignment(delegate { _modelYear = value; }, "ModelYear");
                }
            }
        }
    }
}