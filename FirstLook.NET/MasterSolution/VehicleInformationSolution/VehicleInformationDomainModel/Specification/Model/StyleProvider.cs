using System;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Model
{
    [Serializable]
    public sealed class StyleProvider : IEquatable<StyleProvider>
    {
        public static readonly StyleProvider Chrome = new StyleProvider(1, "Chrome");

        public static StyleProvider Get(byte id)
        {
            switch (id)
            {
                case 1:
                    return Chrome;
            }

            throw new NotSupportedException();
        }

        private readonly byte _id;
        private readonly string _name;

        private StyleProvider(byte id, string name)
        {
            _id = id;
            _name = name;
        }

        public byte Id
        {
            get { return _id; }
        }

        public string Name
        {
            get { return _name; }
        }

        public bool Equals(StyleProvider other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other._id == _id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (StyleProvider)) return false;
            return Equals((StyleProvider) obj);
        }

        public override int GetHashCode()
        {
            return _id;
        }
    }
}