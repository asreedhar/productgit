using System;
using System.Xml;
using FirstLook.Common.Core;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Model
{
    [Serializable]
    public class StyleProviderDocument : PropertyChangeSupport, IEquatable<StyleProviderDocument>
    {
        private int _version;
        private XmlDocument _document;

        public int Version
        {
            get { return _version; }
            set
            {
                if (!Equals(_version, value))
                {
                    Assignment(delegate { _version = value; }, "Version");
                }
            }
        }

        public XmlDocument Document
        {
            get { return _document; }
            set
            {
                if (!AreSame(_document, value))
                {
                    Assignment(delegate { _document = value; }, "Document");
                }
            }
        }

        public bool Equals(StyleProviderDocument other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other._version == _version && AreSame(other._document, _document);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (StyleProviderDocument)) return false;
            return Equals((StyleProviderDocument) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (_version*397) ^ (_document != null ? _document.GetHashCode() : 0);
            }
        }
    }
}