using System;
using FirstLook.Common.Core;

namespace FirstLook.VehicleInformation.DomainModel.Specification.Model
{
    public interface IStyleKey : IEquatable<IStyleKey>
    {
        int Id { get; }
    }

    public class StyleKey : IStyleKey
    {
        private readonly int _id;

        public int Id
        {
            get { return _id; }
        }

        public StyleKey(int id)
        {
            _id = id;
        }

        public bool Equals(IStyleKey other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other.Id == _id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (IStyleKey)) return false;
            return Equals((IStyleKey) obj);
        }

        public override int GetHashCode()
        {
            return _id;
        }
    }

    public interface IStyle : IStyleKey
    {
        StyleSummary StyleSummary { get; set; }

        StyleProvider StyleProvider { get; set; }

        StyleProviderDocument Document { get; set; }

        StyleProviderDocument ReferenceDocument { get; set; }
    }

    [Serializable]
    public abstract class Style : PropertyChangeSupport, IStyle
    {
        public static readonly int NotDefined = 0;

        private StyleProvider _styleProvider;
        private StyleSummary _styleSummary;
        private StyleProviderDocument _document;
        private StyleProviderDocument _referenceDocument;

        public virtual int Id
        {
            get { return NotDefined; }
        }

        public StyleProvider StyleProvider
        {
            get { return _styleProvider; }
            set
            {
                if (!Equals(_styleProvider, value))
                {
                    Assignment(delegate { _styleProvider = value; }, "StyleProvider");
                }
            }
        }

        public StyleSummary StyleSummary
        {
            get { return _styleSummary; }
            set
            {
                if (!Equals(_styleSummary, value))
                {
                    Assignment(delegate { _styleSummary = value; }, "StyleSummary");
                }
            }
        }

        public StyleProviderDocument Document
        {
            get { return _document; }
            set
            {
                if (!Equals(_document, value))
                {
                    Assignment(delegate { _document = value; }, "Document");
                }
            }
        }

        public StyleProviderDocument ReferenceDocument
        {
            get { return _referenceDocument; }
            set
            {
                if (!Equals(_referenceDocument, value))
                {
                    Assignment(delegate { _referenceDocument = value; }, "ReferenceDocument");
                }
            }
        }

        public virtual bool Equals(IStyleKey other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other.Id == Id;
        }
    }

    [Serializable]
    public class ChromeStyle : Style
    {
        private int _chromeStyleId;
        private ChromeStyleStatus _status;
        
        public int ChromeStyleId
        {
            get { return _chromeStyleId; }
            set
            {
                if (!Equals(_chromeStyleId, value))
                {
                    Assignment(delegate { _chromeStyleId = value; }, "ChromeStyleId");
                }
            }
        }

        public ChromeStyleStatus Status
        {
            get { return _status; }
            set
            {
                if (!Equals(_status, value))
                {
                    Assignment(delegate { _status = value; }, "Status");
                }
            }
        }

        public override bool Equals(IStyleKey other)
        {
            ChromeStyle style = other as ChromeStyle;
            if (style != null)
            {
                return _chromeStyleId == style.ChromeStyleId;
            }
            return base.Equals(other);
        }
    }

    [Serializable]
    public class ChromeStyleStatus : PropertyChangeSupport
    {
        private bool _isMappingUpdated;
        private bool _isDeleted;
        private bool _isUpdated;

        public bool IsMappingUpdated
        {
            get { return _isMappingUpdated; }
            set
            {
                if (!Equals(_isMappingUpdated, value))
                {
                    Assignment(delegate { _isMappingUpdated = value; }, "IsMappingUpdated");
                }
            }
        }

        public bool IsDeleted
        {
            get { return _isDeleted; }
            set
            {
                if (!Equals(_isDeleted, value))
                {
                    Assignment(delegate { _isDeleted = value; }, "IsDeleted");
                }
            }
        }

        public bool IsUpdated
        {
            get { return _isUpdated; }
            set
            {
                if (!Equals(_isUpdated, value))
                {
                    Assignment(delegate { _isUpdated = value; }, "IsUpdated");
                }
            }
        }
    }
}