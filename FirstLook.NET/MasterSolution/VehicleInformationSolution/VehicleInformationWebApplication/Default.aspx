﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="FirstLook.VehicleInformation.WebApplication._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Default Page</title>
    <script src="Public/Scripts/Lib/jquery/jquery-ui-1.8.2.custom/js/jquery-1.4.2.min.js" type="text/javascript" charset="utf-8"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <li>Pages
                <ul>
                    <li>
                        <label>Broker: <input type="text" maxlength="36" name="broker" id="broker" value="A21C1068-A001-4AD9-803E-47E60635BDF9" /></label>
                        <label>VIN: <select name="vin" id="vin">
                            <optgroup label="Ford">
                                <option value="1FTPW14V67FB81010" selected="selected">2007 Ford F150 Lariat</option>
                                <option value="1FAHP33N58W102268">2008 Ford Focus SES</option>
                            </optgroup>
                            <optgroup label="Honda">
                                <option value="1HGCP36828A035489">2008 Honda Accord EX-L</option>
                                <option value="1HGFA16838L113964">2008 Honda Civic EX</option>
                            </optgroup>
                            <optgroup label="BMW">
                                <option value="WBAWL73558PX52052">2008 BMW 335 i</option>
                                <option value="WBSWD93549PY43898">2009 BMW M3</option>
                            </optgroup>
                            <optgroup label="Cadillac">
                                <option value="1GYFK23259R265402">2009 Cadillac Escalade </option>
                                <option value="1G6DL67A380141007">2008 Cadillac STS V8</option>
                            </optgroup>
                        </select></label>
                        <a href="javascript:void(0);" id="style_link">Style</a>
                        <script language="javascript" type="text/javascript">
                            $('#style_link').bind('click', function(evt) {
                                var v = $('#vin').val(),
                                    b = $('#broker').val();
                                window.location.href = 'Pages/Chrome/Style.aspx?v=' + v + '&b=' + b;
                                evt.preventDefault();
                                return false;
                            });
                        </script>
                    </li>
                </ul>
            </li>
            <li>Services
                <ul>
                    <li>
                        <asp:HyperLink runat="server" ID="ChromeService" NavigateUrl="~/Services/Chrome.asmx" Text="Chrome" />
                    </li>
                </ul>
            </li>
        </div>
    </form>
</body>
</html>
