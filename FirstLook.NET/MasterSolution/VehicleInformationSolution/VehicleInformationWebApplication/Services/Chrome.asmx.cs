using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.Common.Core.Collections;
using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.Impl;
using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects;
using FirstLook.VehicleInformation.DomainModel.Chrome.Commands.TransferObjects.Envelopes;

namespace FirstLook.VehicleInformation.WebApplication.Services
{
    /// <summary>
    /// Summary description for Chrome
    /// </summary>
    [WebService(Namespace = "http://services.firstlook.biz/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    [ScriptService]
    public class Chrome : WebService
    {
        #region WebMethods

        [WebMethod]
        public IEnumerable<StyleDto> GetStyles(string vin)
        {
            VinPatternCommand vinPatternCommand = new VinPatternCommand();

            return vinPatternCommand.Execute(
                new VinPatternArgumentsDto
                {
                    Country = "US",
                    VinPattern = vin
                }).Result.Styles;
        }

        [WebMethod]
        public StyleResultsDto GetStyleDetails(string bodyStyle, bool isFleet, int styleId, Guid broker)
        {
            VehiclePrototypeDto prototype = GetVehiclePrototype(bodyStyle, isFleet, styleId);

            IdentityContextDto<StyleArgumentsDto> arguments = GetStyleCommand(prototype, broker);

            return new StyleCommand().Execute(arguments);
        }

        [WebMethod]
        public string GetImageUrl(string bodyStyle, bool isFleet, int styleId)
        {
            VehiclePrototypeDto prototype = GetVehiclePrototype(bodyStyle, isFleet, styleId);

            IdentityContextDto<StyleArgumentsDto> arguments = GetStyleCommand(prototype, null);

            StyleResultsDto results = new StyleCommand().Execute(arguments);

            return "http://swenmouth02:9090/stock_images/" + results.ImageRelativePath; // TODO: Fix my location please, PM
        }

        [WebMethod]
        public IEnumerable<ClassificationHeaderDto> GetClassificationHeaders(string bodyStyle, bool isFleet, int styleId, Guid broker)
        {
            VehiclePrototypeDto prototype = GetVehiclePrototype(bodyStyle, isFleet, styleId);

            IdentityContextDto<StyleArgumentsDto> arguments = GetStyleCommand(prototype, broker);

            return new StyleCommand().Execute(arguments).ClassificationHeaders;
        }

        [WebMethod]
        public IEnumerable<ColorDto> GetColors(string bodyStyle, bool isFleet, int styleId)
        {
            VehiclePrototypeDto prototype = GetVehiclePrototype(bodyStyle, isFleet, styleId);

            IdentityContextDto<StyleArgumentsDto> arguments = GetStyleCommand(prototype, null);

            return new StyleCommand().Execute(arguments).Colors;
        }

        [WebMethod]
        public IRandomAccess<OptionCollectionDto> GetColorOptions(string exteriorCodeOne, string exteriorCodeTwo, string interiorCode, int styleId, bool isFleet)
        {
            exteriorCodeOne = NullableStringHelper(exteriorCodeOne);
            exteriorCodeTwo = NullableStringHelper(exteriorCodeTwo);
            interiorCode = NullableStringHelper(interiorCode);

            ColorOptionsArgumentsDto configuration = GetColorConfiguration(exteriorCodeOne, exteriorCodeTwo, interiorCode, styleId, isFleet);

            ColorOptionsCommand categoryCommand = new ColorOptionsCommand();

            return categoryCommand.Execute(configuration).Result;
        }

        [WebMethod]
        public string GetVehicleInitialState(string bodyStyle, bool isFleet, int styleId)
        {
            VehiclePrototypeDto prototype = GetVehiclePrototype(bodyStyle, isFleet, styleId);

            return GetVehicleInitialState(prototype);
        }

        [WebMethod]
        public string UpdateVehicleState(string bodyStyle, bool isFleet, int styleId, string prevState, string optionCode, bool selected)
        {
            VehiclePrototypeDto prototype = GetVehiclePrototype(bodyStyle, isFleet, styleId);

            VehicleBuildDto build = GetVehicleBuild(prototype, prevState);

            ConfigurationUpdateCommand command = new ConfigurationUpdateCommand();

            return command.Execute(
                new ConfigurationUpdateArgumentsDto
                {
                    OptionCode = optionCode,
                    Selected = selected,
                    VehicleBuild = build
                }).NextState;
        }

        [WebMethod]
        public OptionCollectionDto CategoryUpdateState(string bodyStyle, bool isFleet, int styleId, string prevState, int categoryId, bool selected)
        {
            VehiclePrototypeDto prototype = GetVehiclePrototype(bodyStyle, isFleet, styleId);

            VehicleBuildDto build = GetVehicleBuild(prototype, prevState);

            CategoryIdentificationResultsDto results = new CategoryIdentificationCommand().Execute(
                new CategoryIdentificationArgumentsDto
                {
                    Addition = selected,
                    CategoryId = categoryId,
                    VehicleBuild = build
                });

            List<string> options = new List<string>();

            for (int i = 0, l = results.Candidates.Count; i < l; i++)
            {
                options.Add(results.Candidates.Get(i).Code);
            }

            return results.Candidates;
        }

        [WebMethod]
        public void UpdateBuild(Guid broker, string vin, string bodyStyle, bool isFleet, int styleId, string state)
        {
            VehiclePrototypeDto prototype = GetVehiclePrototype(bodyStyle, isFleet, styleId);

            VehicleBuildDto build = GetVehicleBuild(prototype, state);

            SaveSpecificationCommand command = new SaveSpecificationCommand();

            command.Execute(new IdentityContextDto<SaveSpecificationArgumentsDto>
            {
                Arguments = new SaveSpecificationArgumentsDto
                {
                    Broker = new BrokerIdentificationDto { Handle = broker },
                    Vehicle = new VehicleIdentificationDto { Vin = vin },
                    VehicleBuild = build
                },
                Identity = null
            });
        }

        [WebMethod]
        public EncodingsDto GetEncodings(string bodyStyle, bool isFleet, int styleId)
        {
            VehiclePrototypeDto prototype = GetVehiclePrototype(bodyStyle, isFleet, styleId);

            EncodingsCommand encodingsCommand = new EncodingsCommand();

            return encodingsCommand.Execute(new EncodingsArgumentsDto{VehiclePrototype = prototype}).Encodings;
        }

        #endregion

        #region HelperMethods

        private static IdentityContextDto<StyleArgumentsDto> GetStyleCommand(VehiclePrototypeDto prototype, Guid? broker)
        {
            return new IdentityContextDto<StyleArgumentsDto>
            {
                Arguments = new StyleArgumentsDto
                {
                    Broker = broker.GetValueOrDefault(),
                    HasBroker = broker.HasValue,
                    VehicleBuild = GetVehicleBuild(prototype)
                },
                Identity = new IdentityDto
                {
                    Name = HttpContext.Current.User.Identity.Name,
                    AuthorityName = HttpContext.Current.User.Identity.AuthenticationType
                }
            };
        }

        private static VehicleBuildDto GetVehicleBuild(VehiclePrototypeDto prototype)
        {
            return GetVehicleBuild(prototype, GetVehicleInitialState(prototype));
        }

        private static VehicleBuildDto GetVehicleBuild(VehiclePrototypeDto prototype, string state)
        {
            return new VehicleBuildDto
            {
                Prototype = prototype,
                State = state
            };
        }

        private static VehiclePrototypeDto GetVehiclePrototype(string bodyStyle, bool isFleet, int styleId)
        {
            return new VehiclePrototypeDto
            {
                BodyStyle = bodyStyle,
                IsFleet = isFleet,
                StyleId = styleId
            };
        }

        private static string GetVehicleInitialState(VehiclePrototypeDto prototype)
        {
            return new ConfigurationInitializeCommand().Execute(
                new ConfigurationInitializeArgumentsDto
                {
                    VehiclePrototype = prototype
                }).State;
        }

        private static ColorOptionsArgumentsDto GetColorConfiguration(string exteriorCodeOne, string exteriorCodeTwo, string interiorCode, int styleId, bool isFleet)
        {
            return new ColorOptionsArgumentsDto
            {
                ExteriorCodeOne = exteriorCodeOne,
                ExteriorCodeTwo = exteriorCodeTwo,
                InteriorCode = interiorCode,
                StyleId = styleId,
                IsFleet = isFleet
            };
        }

        private static string NullableStringHelper(string value)
        {
            return value == "null" ? "" : value;
        }
        #endregion
    }
}
