﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Style.aspx.cs" Inherits="FirstLook.VehicleInformation.WebApplication.Pages.Chrome.Style" Theme="Usual"
 %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" Theme="Usual">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<link rel="apple-touch-icon" href="../../Public/Icons/apple-touch-icon.png" />
	<!-- ========================== -->
	<!-- = apple specific headers = -->
	<!-- ========================== -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
	<meta name="format-detection" content="telephone=no" />
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="HandheldFriendly" content="true" />
    <!-- ========================== -->
	<!-- = apple specific headers = -->
	<!-- ========================== -->
    <title>Lot Loading</title>
    <script src="../../Public/Scripts/Lib/LABjs-1.0.2rc1/LAB.js" type="text/javascript" charset="utf-8"></script>    
    <script src="../../Public/Scripts/Lib/modernizer/modernizr-1.5.min.js" type="text/javascript" charset="utf-8"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="nav_header">
        </div>
        
        <ul id="location_selection" class="tabs wrapper">
	        <li class="vehicle"><a href="#vehicle">Vehicle</a></li>
	        <li class="interior"><a href="#interior">Interior</a></li>
	        <li class="exterior"><a href="#exterior">Exterior</a></li>
        </ul>
        
        <div id="main_content">
            <div id="header">
			    <h1>
			        <asp:Literal ID="DescriptionLiteral" runat="server" />
			    </h1>
			    <dl class="vin">
				    <dt>VIN:</dt>
				    <dd><asp:Literal ID="VinLiteral" runat="server" /></dd>
			    </dl>
			    <dl class="mileage">
				    <dt>Mileage:</dt>
				    <dd><asp:Literal ID="MileageLiteral" runat="server" /></dd>
			    </dl>
			    <dl class="stock_num">
				    <dt>Stock #:</dt>
				    <dd><asp:Literal ID="StockNumLiteral" runat="server" /></dd>
			    </dl>
		    </div>
		
		    <fieldset id="add_option_by_code" class="wrapper">
			    <legend>Add Option By OEM Option Code</legend>
			    <input type="text" id="add_option_code" value="" />			
			    <label for="add_option" class="button">
			    <input type="submit" value="Add Option" id="add_option" />
			    </label>
		    </fieldset>
    		
		    <fieldset id="display_controls">
			    <span>Show: </span>
			    <ul class="input_list">
				    <li id="show_standards">
					    <label for="show_standards_checkbox">
						    <input type="checkbox" id="show_standards_checkbox" />
						    Standards
					    </label>
				    </li>
				    <li id="show_features">
					    <label for="show_features_checkbox">
						    <input type="checkbox" checked="checked" id="show_features_checkbox" />
						    Features
					    </label>
				    </li>
				    <li id="show_options">
					    <label>
						    <input type="checkbox" checked="checked" />
						    Options
					    </label>
				    </li>
				    <li id="show_add_by_option">
					    <label>
						    <input type="checkbox" />
						    Show Add By OEM Option Code
					    </label>
				    </li>
			    </ul>
		    </fieldset>
    		
		    <div id="options" class="wrapper">
    			
			    <div id="vehicle" class="card">			    	
			    	<div class="fieldset_wrapper">
			    	
				    <span class="image">
					    <a href="" target="_blank">
						    <img class="landscape" alt="" src="../../Public/Images/_blank.gif" />
					    </a>
				    </span>
				    
				    <fieldset id="select_style" class="wrapper">
					    <legend>Select Style</legend>
		    			
					    <select name="style_select" id="style_select">
						    <option>Select a Style</option>
					    </select>
				    </fieldset>
    				
				    <fieldset id="colors" class="wrapper">
					    <legend>Colors</legend>
    					
					    <p class="color_selector">
						    <label>Exterior Color<span class="option_code"></span></label>
						    <span class="sample"><span></span></span>
						    <select name="exterior_color_1" id="exterior_color_1" >
							    <option>Select an Exterior Color</option>
						    </select>
					    </p>
    					
					    <p class="color_selector">
						    <label>Second Exterior Color<span class="option_code"></span></label>
						    <span class="sample"><span></span></span>
						    <select name="exterior_color_2" id="exterior_color_2" >
							    <option>Select an Exterior Color</option>
						    </select>
					    </p>
    					
					    <p class="color_selector">
						    <label>Interior Color<span class="option_code"></span></label>
						    <span class="sample"><span></span></span>
						    <select name="interior_color" id="interior_color">
							    <option>Select an Interior Color</option>
						    </select>
					    </p>
				    </fieldset>
    				
				    <fieldset id="key_information">
					    <legend>Key Information</legend>
					    <br />
					    <ul class="input_list columned wrapper">
						    <li>
							    <label>
								    Mileage
								    <input type="text" name="mileage" value="" id="mileage" />
							    </label>
						    </li>
						    <li>
							    <label>
								    <input type="checkbox" name="serviced_here" id="non_smoker" />
								    Serviced Here
							    </label>
						    </li>
						    <li>
							    <label>
								    <input type="checkbox" name="non_smoker" id="Checkbox1" />
								    Non-Smoker
							    </label>
						    </li>
						    <li>
							    <label>
								    <input type="checkbox" name="non_smoker" id="Checkbox2" />
								    Non-Smoker
							    </label>
						    </li>
					    </ul>
				    </fieldset>
    				</div>
			    </div>
    			
			    <div id="interior" class="card">
			    </div>
    			
			    <div id="exterior" class="card">
			    </div>
    			
			    <div id="dialog" class="card">
			    </div>
    			
		    </div>
    		
		    <div id="overview">
			    <h3>Vehicle Build Summary</h3>
			    <ul class="options color">
				    <li>
					    <h6>Exterior Color</h6>
					    <span class="sample"><span></span></span> 
					    Color Name
					    <span class="option_code"></span>
				    </li>
				    <li>
					    <h6>Interior Color</h6>
					    <span class="sample"><span></span></span> 
					    Color Name
					    <span class="option_code"></span>
				    </li>
			    </ul>
			    <div id="build_options">
				    <h4>OEM Options</h4>
				    <ol class="options">
				        <li style="display: none"></li>
				    </ol>
			    </div>
			    <div id="build_features" >		
				    <h4>Generic Options</h4>
				    <ul class="tags">
				        <li style="display: none"></li>
				    </ul>
			    </div>
			    <div id="build_standards">
				    <h4>Standard Equipment</h4>
				    <ul class="tags">
				        <li style="display: none"></li> 
				    </ul>
			    </div>			
		    </div>
    		
		    <fieldset id="controls" class="wrapper">
			    <label for="save"><input type="submit" name="save" value="save" id="save" /></label>
		    </fieldset>
		
		</div>
		
		<script type="text/javascript" charset="utf-8">
		    $LAB.script("../../Public/Scripts/Lib/jquery/jquery-ui-1.8.2.custom/js/jquery-1.4.2.min.js")
				.script("../../Public/Scripts/App/Chrome/Style/Application.js");

		    if (typeof JSON === "undefined") {
		        $LAB.script("../../Public/Scripts/Lib/json/json2.min.js");
		    }
		</script>
	    
    </form>
</body>
</html>
