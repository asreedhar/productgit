var WindowResizer = function(defaults) {
	for(prop in defaults) {
		this[prop] = defaults[prop];
	}
};
WindowResizer.prototype = {
	init: function() {
		this.bindEvents();
		this.setDefaults();
	},
	bindEvents: function() {
		$(window).bind("resize", this, this.onResize);
		this.get$TabLinks().bind("click", this, this.onTabClick);
	},
	setDefaults: function() {
		$(window).trigger({ type: "resize", data: this });
		this.get$TabLinks().first().trigger({ type: "click", data: this });
	},
	onResize: function(e) {
		if (e.data && e.data.sizes) {
			var width = $(e.target).width();
			var doc = document.body, classes;
			for (prop in e.data.sizes) {
				$(doc).removeClass(e.data.sizes[prop]);
				if (width > prop) {
					classes = e.data.sizes[prop];
				};
			}
			$(doc).addClass(classes);
		};
	},
	get$Cards: function() {
		return this.get$Cache("Cards", "#options .card");
	},
	get$TabLinks: function() {
		return this.get$Cache("TabLinks", "#location_selection a");
	},
	onTabClick: function(e) {
		var href = e.target.href;
		if (href.indexOf("#") != -1) {
			e.data.get$TabLinks().each(function() {
				$(this.parentNode).removeClass("active");
			})
			e.data.get$Cards().hide();
			$(e.target.parentNode).addClass("active")
			$(href.substr(href.lastIndexOf("#"), href.length)).show();
		}
		e.preventDefault();
	},
	get$Cache: function(key, selector) {
		var privateKey = "_"+key;
		if (!!!this[privateKey]) {
			this[privateKey] = $(selector);
		};
		return this[privateKey]
	}
};