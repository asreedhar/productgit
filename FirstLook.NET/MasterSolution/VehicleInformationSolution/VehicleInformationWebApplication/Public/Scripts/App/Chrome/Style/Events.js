App.Events = {
    "Styles": {
        fetchComplete: function(event, data) {
            if ( !! !data) {
                throw new Error("Missing Data");
            }
            var styleTemplateBuilder = new StyleTemplateBuilder(data);
            styleTemplateBuilder.clean();
            styleTemplateBuilder.init();
            Page.init();
        },
        onChange: function(event) {
            var id = $(event.target).val();
            var style = $.grep(this.Items,
            function(a) {
                return a.Id == id;
            });
            if ( !! style && !!style[0]) {
                $(style[0]).trigger("fetch");
            }
        }
    },
    "Style": {
        fetchComplete: function(event, data) {
        	var image = new VehicleImage();
        	image.getData = $.proxy(this.getData, this);
        	$(image).trigger("fetch");
        	var colors = new Colors();
        	colors.getData = $.proxy(this.getData, this);
        	$(colors).trigger("fetch");
        	var encodings = new Encodings();
        	encodings.getData = $.proxy(this.getData, this);
        	$(encodings).trigger("fetch");
        	
        	App.State.Style = this;
			
            var vehicle = data;
            var builder = new VehicleTemplateBuilder(vehicle);
            builder.clean();
            builder.init();
        }
    },
    "VehicleImage": {
    	fetchComplete: function(event, data) {
    		var image = data;
    		var builder = new ImageTempalteBuilder(image);
    		builder.clean();
    		builder.init();
    	}
    },
    "Colors": {
    	fetchComplete: function(event, data) {
    		var colors = data;
    		var builder = new ColorTemplateBuilder(colors);
    		builder.clean();
    		builder.init();
    	},
    	onColorChange: function(event, data) {
    		var colorChange = new ColorStateChange(data.exteriorCodeOne,data.exteriorCodeTwo,data.interiorCode);
    		colorChange.FleetOnly = App.State.Style.FleetOnly;
    		colorChange.Id = App.State.Style.Id;
    		
    		$(colorChange).trigger("fetch");
    	}
    },
    "BuildState": {
    	create: function(event, data) {
    		App.State.BuildState = this;
    	},
    	fetchComplete: function(event, data) {
    		VehicleTemplateBuilder.update_selections();
    		VehicleTemplateBuilder.update_summary();
    	},
        saveState: function(event, data) {
            var buildStateSave = new BuildStateSave("", false, App.State.Style.Id, App.State.BuildState.StateString, $.getUrlVar("b"));
            $(buildStateSave).trigger("fetch");
        }
    },
    "OptionStateChange": {
    	fetchComplete: function(event, data) {
    		VehicleTemplateBuilder.update_selections();
    		if (data.EngineResult === EngineResult.Query) {
    			var builder = new BuildDialogBuilder(data);
    			builder.clean();
    			builder.init();
    		};
    		VehicleTemplateBuilder.update_summary();
    	}
    },
    "FeatureStateChange": {
    	fetchComplete: function(event, data) {
    		var optionsChanged = data;
    		if (optionsChanged.Count === 0) {
    			return;
    		};
    		if (optionsChanged.Count === 1) {
    			var optionStateChange = new OptionStateChange(optionsChanged.Items[0].Code, !!this.Selected);
    			optionStateChange.FleetOnly = App.State.Style.FleetOnly;
    			optionStateChange.Id = App.State.Style.Id;
    			
    			optionStateChange.StateString = App.State.BuildState.StateString;
    			
    			$(optionStateChange).trigger("fetch");
    		} else {    			
    			var mockBuildState = {
    				OptionsToChoose: []
    			}
    			var key, vehicle = App.State.Vehicle;
    			for (var i=0; i < optionsChanged.Items.length; i++) {
    				key = vehicle.OptionsKey[optionsChanged.Items[i].Code];
    				mockBuildState.OptionsToChoose.push(key);
    			};
    			var builder = new BuildDialogBuilder(mockBuildState);
    			builder.clean();
    			builder.init();
    		}
    		VehicleTemplateBuilder.update_summary();
        }
    },
    "ColorStateChange": {
    	fetchComplete: function(event, data) {
    		// Only Prompt for user desicion, PM
    	}
    },
    "Vehicle": {
    	create: function(event, data) {
    		App.State.Vehicle = this;
    	},
    	optionChanged: function(event, data) {
    		var buildStateChange = OptionStateChange.create(data);
    		buildStateChange.FleetOnly = App.State.Style.FleetOnly;
    		buildStateChange.Id = App.State.Style.Id;
    					
			buildStateChange.StateString = App.State.BuildState.StateString;
			
    		$(buildStateChange).trigger("fetch");
    	},
    	featureChanged: function(event, data) {
    		var buildStateChange = FeatureStateChange.create(data);
    		buildStateChange.FleetOnly = App.State.Style.FleetOnly;
    		buildStateChange.Id = App.State.Style.Id;
    		
    		buildStateChange.StateString = App.State.BuildState.StateString;
    		
    		$(buildStateChange).trigger("fetch");
    	}
    },
	"Encodings": {
		create: function(event, data) {
			App.State.Encodings = this; 
		},
		fetchComplete: function (event, data) {
			var initialState = new BuildState();
        	initialState.getData = $.proxy(this.getData, this);
        	$(initialState).trigger("fetch");
		}
	}
};
