$LAB.script("../../Public/Scripts/Lib/WindowResizer.js")
    .script("../../Public/Scripts/App/Chrome/Style/Services.js")
    .script("../../Public/Scripts/App/Chrome/Style/Events.js")
    .script("../../Public/Scripts/App/Chrome/Style/Classes.js")
    .script("../../Public/Scripts/App/Chrome/Style/Templates.js")
    .script("../../Public/Scripts/Lib/iscroll.js")
    .script("../../Public/Scripts/App/Chrome/Style/Mappers.js").wait(function() {
        $LAB.script("../../Public/Scripts/App/Chrome/Style/Configuration.js").wait(function() {
            $LAB.script("../../Public/Scripts/App/Chrome/Style/Commands.js").script("../../Public/Scripts/App/Chrome/Style/Runtime.js");
	});
});

var App = function() {};
App.prototype = {
    init: function() {
        // $("button, input:submit").button();

        if ( !! this.Sizes) {
            this.Sizes.init();
        }
        this.init$bindEvents();

        this.init$triggerDefaults();
    },
    init$bindEvents: function() {
        var hide_sections = function(e) {
            var action = !!e.target.checked ? "show": "hide";
            if (e.data) {
                $(e.data)[action]();
                $(e.target.parentNode)
                .removeClass("checked unchecked")
                .addClass( !! e.target.checked ? "checked": "unchecked");
            }
        };
        var add_option_from_text = function(e) {
            var text = $("#add_option_code").val();
            var inputs = $.grep($("#options label"),
            function(a) {
                return $(a).text().toLowerCase().indexOf(text.toLowerCase()) !== -1;
            });        
            $(inputs).children("input").attr("checked", function() { return !!!this.checked; }).trigger("click");
            $("#add_option_code").text("");
            e.preventDefault();
        };
        $("#show_standards input").bind("click", "div.standards, #build_standards", hide_sections);
        $("#show_features input").bind("click", "fieldset.features_section, #build_features", hide_sections);
        $("#show_options input").bind("click", "fieldset.options_section, #build_options, #msrp_slider, #msrp_slider_value", hide_sections);
        $("#show_add_by_option input").bind("click", "fieldset#add_option_by_code", hide_sections);
        $("#add_option_code").bind("keypress",
        function(e) {
            if (e.which == 13) {
                $("#add_option").trigger("click");
                e.preventDefault();
            };
        });
        $("#add_option").bind("click", add_option_from_text);

        function saveBuild(evt) {
            var state = (((App || {}).State || {}).BuildState || {});
            $(state).trigger("saveState");
            evt.preventDefault();
        }

        $("#save").bind("click", saveBuild);
    },
    init$triggerDefaults: function() {
        var checkboxes = $("#show_standards input, #show_features input, #show_options input, #show_add_by_option input");
        checkboxes.each(function() {
            $(this).trigger("click");
            this.checked = !this.checked;
        });
    }
};

App.State = {
    Vehicle: null,
    Encodings: null,
    BuildState: null
};
