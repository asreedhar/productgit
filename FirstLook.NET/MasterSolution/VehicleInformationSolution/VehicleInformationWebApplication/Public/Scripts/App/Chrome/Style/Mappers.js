App.DataMapper = {
    "Vehicle": function(json) {
        var result = new Vehicle();
        result.option_count = 0;
        var addOptions = function(classification) {
            var item,
            l;
            for (var i = classification.Options.length - 1; i >= 0; i--) {
                classification.Options[i].Classification = classification.Name;
                item = OptionGroup.create(classification.Options[i]);
                l = result.Options.length;
                for (var j = classification.Options[i].Items.length - 1; j >= 0; j--) {
                    // Needed for fast lookup, PM
                    result.OptionsKey[classification.Options[i].Items[j].Code] = new KeyValue(l, item.Items.length);
                    item.Items.push(Option.create(classification.Options[i].Items[j]));
                    result.option_count++;
                };
                result.Options.push(item);
            };
        };
        var addFeatures = function(classification) {
            var item;
            for (var i = classification.Categories.length - 1; i >= 0; i--) {
                classification.Categories[i].Classification = classification.Name;
                item = FeatureGroup.create(classification.Categories[i]);
                var l = result.Features.length;
                for (var j = classification.Categories[i].Items.length - 1; j >= 0; j--) {
                    // Needed for fast lookup and to decouple the map from state object, PM
                    result.FeatureKey[classification.Categories[i].Items[j].Id] = new KeyValue(l, item.Items.length);
                    item.Items.push(Feature.create(classification.Categories[i].Items[j]));
                };
                result.Features.push(item);
            };
        };
        var addStandards = function(classification) {
            var item;
            for (var i = classification.Standards.length - 1; i >= 0; i--) {
                item = classification.Standards[i];
                item.Classification = classification.Name;
                result.Standards.push(Standard.create(item));
            };
        };
        var hiddenClassifications = [10510, 1002, 1089, 23, 21, 28, 1096, 1104, 1107, 1114, 
        1118, 1004, 1119, 1120, 1005, 1006, 1122, 1123, 1124, 1125, 10565, 1007, 1008, 10562, 
        1130, 1134, 1135, 1136, 1137, 1012, 1013, 1149, 1018, 1150, 1019, 1020, 10592, 1021, 
        1022, 10183, 1151, 10108, 10191, 1154, 1155, 1156, 10590, 1157, 1158, 1163, 64, 1174, 
        1175, 10207, 10094, 1178, 1180, 1181, 1182, 1183, 10101, 1184, 1185, 10571, 1188, 1191, 
        1203, 1204, 10115, 1205, 1207, 10489, 1212, 1213, 1214, 10563, 1217, 1218, 1219, 1031, 
        1227, 1228, 10559, 1242, 1245, 1246, 1247, 1248, 10319, 10669, 10569, 1252, 1253, 1254, 
        10630, 1258, 10197, 1260, 1261, 10509, 1263, 10426, 1275, 1276, 1032, 1277, 1033, 1034, 
        1035, 1280, 1281, 1036, 1282, 1283, 1284, 1038, 1039, 1040, 1285, 1041, 1286, 1290, 10344, 
        10095, 1299, 1300, 1304, 10098, 1309, 1310, 1311, 10112, 1316, 1325, 1326, 10116, 1327, 
        1328, 1329, 10261, 10258, 69, 1330, 1331, 1338, 1342, 1346, 1047, 10561, 10593, 22, 1350, 
        10097, 1049, 10196, 1351, 41, 1359, 1367, 1051, 1052, 10568, 10564, 10567, 1369, 1370, 1371, 
        10320, 10158, 1374, 1375, 10566, 10321, 10195, 10365, 1381, 1056, 10184, 79, 1399, 73, 
        10109, 1157, 1166, 1167, 1168, 1169, 1244, 1245, 1246, 10206, 10210];
        hiddenClassifications = [];
        for (var i = json.length - 1; i >= 0; i--) {
        	if ($.inArray(json[i].Id, hiddenClassifications) !== -1) 
        		continue;
        	
            if (json[i].Location == Organization.Exterior) {
                result.Classifications.Exterior.push(json[i].Name);
            } else if (json[i].Location == Organization.Interior) {
                result.Classifications.Interior.push(json[i].Name);
            } else {
                result.Classifications.Vehicle.push(json[i].Name);
            }
            addOptions(json[i]);
            addFeatures(json[i]);
            addStandards(json[i]);
        };
        $(result).trigger("create");
        return result;
    },
    "Styles": function(json) {
        var styles = new Styles();
        styles.Items = [];
        for (var i in json) {
            styles.Items.push(Style.create(json[i]));
        }
        return styles;
    },
    "Style": function(json) {
        var result = new Style();
        for (var i in json) {
            if (result[i] !== undefined) {
                result[i] = json[i];
            };
        }
        result.FleetOnly = false;
        result.Broker = $.getUrlVar('b');
        /// TODO
        $(result).trigger("create");
        return result;
    },
    "Standard": function(json) {
        return new Standard(json.Name, json.Classification, json.Sequence);
    },
    "FeatureGroup": function(json) {
        return new FeatureGroup(json.GroupName, json.Classification);
    },
    "Feature": function(json) {
        return new Feature(json.Id, json.Name);
    },
    "OptionGroup": function(json) {
        return new OptionGroup(json.GroupName, json.Classification);
    },
    "Option": function(json) {
        return new Option(json.Code, json.Name, json.Description, json.Selected, json.RequirementName, json.RequirementId);
    },
    "VehicleImage": function(json) {
        return new VehicleImage(json);
    },
    "Colors": function(json) {
        var colors = new Colors(),
        ext_1_list = [],
        ext_2_list = [],
        int_1_list = [],
        color,
        ext_1,
        ext_2,
        int_1;
        function add_unique_to_list(item, unique_list, classification) {
            if ( !! item && !!item.Code && $.inArray(item.Code, unique_list) === -1) {
                unique_list.push(item.Code);
                item.Classification = classification;
                colors.Items.push(Color.create(item));
            }
        }
        for (var i = json.length - 1; i >= 0; i--) {
            ext_1 = json[i].ExteriorOne;
            ext_2 = json[i].ExteriorTwo;
            int_1 = json[i].Interior;
            add_unique_to_list(ext_1, ext_1_list, "ExteriorOne");
            add_unique_to_list(ext_2, ext_2_list, "ExteriorTwo");
            add_unique_to_list(int_1, int_1_list, "Interior");
        };
        return colors;
    },
    "Color": function(json) {
        return new Color(json.Code, json.Description, json.Classification, json.RgbHex);
    },
    "BuildState": function(json) {
        var command = new App.Command.DecodeBuildState();
        command.StateString = json;
        command.Execute();
        var result = command.Result;
        result.StateString = json;

        $(result).trigger("create");
        return result;
    },
    "OptionStateChange": function(json) {
        return new OptionStateChange(json.Code, json.Selected);
    },
    "FeatureStateChange": function(json) {
        return new FeatureStateChange(json.Id, json.Selected);
    },
    "ColorStateChange": function(json) {

        },
    "Encodings": function(json) {
        var result = new Encodings(),
        i,
        item;
        for (i = json.Options.length - 1; i >= 0; i--) {
            item = json.Options[i];
            result.Options[item.Index] = item.Value;
        }
        for (i = json.Categories.length - 1; i >= 0; i--) {
            item = json.Categories[i];
            result.Categories[item.Index] = item.Value;
        }
        for (i = json.Standards.length - 1; i >= 0; i--) {
            item = json.Standards[i];
            result.Standards[item.Index] = item.Value;
        }
        $(result).trigger("create");
        return result;
    }
};

$.extend({
  getUrlVars: function(){
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
      hash = hashes[i].split('=');
      vars.push(hash[0]);
      vars[hash[0]] = hash[1];
    }
    return vars;
  },
  getUrlVar: function(name){
    return $.getUrlVars()[name];
  }
});
