/**
 * namespace App.Command
 */

if (typeof(App) === 'undefined') {
    var App = {};
}
if (typeof(App.Command) === 'undefined') {
    App.Command = {};
}

 (function() {
    /**
     * DecodeBuildState
     */

    var DecodeBuildState = function() {
        this.StateString = "";
        this.Result = null;
    };

    DecodeBuildState.prototype.Execute = function Execute() {
        var str = this.StateString,
        isQuery,
        result = new BuildState();
        var split_string = str.split("|");

        result.EngineResult = split_string[0];
        result.LogicOperator = split_string[1];
        result.OptionState = split_string[2];
        split_string = split_string[3].split("$");
        result.OptionSelections = split_string[0];
        result.CategoryState = split_string[1];
        result.StandardState = split_string[2];

        isQuery = result.EngineResult === EngineResult.Query;

        this.parseOptions(result.OptionState, isQuery);
        this.parseFeatures(result.CategoryState);

        if (isQuery) {
            result.OptionsToChoose = this.OptionsToChoose;
            if (result.OptionsToChoose.length === 0) {
                result.EngineResult = EngineResult.Normal;
                console.log("False EngineResult.Query Detected: " + str);
            };
        };


        this.Result = result;
    };

    (function(obj) {

        function parsing_curry(character, type, selected) {
        	if (typeof selected === "undefined") {
        		throw new Error([character, " is incorrectly defined as ", selected]);
        	};
            return function(str, list, list_key, encoding) {
                var offset = 0,
                should_loop = true,
                id,
                i,
                key,
                result = [],
                should_be;
                while (should_loop) {
                    i = str.indexOf(character, offset);
                    if (i !== -1) {
                        id = encoding[i];
                        key = list_key[id];
                        if ( !! !key && !!console) {
                        	should_be = type.toEnum(selected).join(", ");
                        	if (should_be !== "Off") {
                        		console.log(id, ": Exists in Encoding but not in Vehicle");
                            	console.log(type === Action ? "Action" : "StateEncoded"," as ", character," Should be", should_be);
                        	};                            
                        } else {
                            result.push(key);
                            if ( !! !list[key.GroupId].Items[key.ItemId] && !!console) {
                                console.log(id, key.GroupId, key.ItemId);
                            } else {
                                list[key.GroupId].Items[key.ItemId].Selected = selected;
                            }

                        }
                        offset = i + 1;
                    } else {
                        should_loop = false;
                    }
                }
                return result;
            };
        };
        var s = SelectionState;
        var a = Action;
        
        var option_state_map = {
        	"1": s.On,
        	"0": s.Off,
        	"R": s.Require,
        	"r": s.Require | s.On,
        	"I": s.Include,
        	"i": s.Include | s.On,
        	"o": s.OrRequire | s.On,
        	"X": s.Exclude,
        	"x": s.Exclude | s.On,
        	"!": s.On | s.Require | s.Include | s.OrRequire | s.OrInclude | s.Exclude,
        	"#": s.Require | s.Include | s.OrRequire | s.OrInclude | s.Exclude,
        	"%": s.On | s.Include | s.OrRequire | s.OrInclude | s.Exclude,
        	"&": s.On | s.Require | s.OrRequire | s.OrInclude | s.Exclude,
        	"+": s.On | s.Require | s.Include | s.OrInclude | s.Exclude,
        	"-": s.On | s.Require | s.Include | s.OrRequire | s.Exclude,
        	"*": s.On | s.Require | s.Include | s.OrRequire | s.OrInclude,
        	"a": s.Exclude | s.On | s.Include,
        	"b": s.Exclude | s.On | s.Require,
        	"c": s.Exclude | s.On | s.OrInclude,
        	"d": s.Exclude | s.On | s.OrRequire,
        	"A": s.Exclude | s.Include,
        	"B": s.Exclude | s.Require,
        	"C": s.Exclude | s.OrInclude,
        	"D": s.Exclude | s.OrRequire
        };
        
        var options_choice_state_map = {
        	"O": s.OrRequire,
        	"N": s.OrInclude
        };
        
        var feature_state_map = {
        	"N": a.NotDefined,
        	"C": a.Add,
        	"P": a.Package,
        	"D": a.Remove
        };
        
        var prop;
        var prefix = "parse_option_";
        for (prop in option_state_map) {
        	obj[prefix+prop] = parsing_curry(prop, s, option_state_map[prop]);
        }
        
        prefix = "parse_choice_option_";
        for (prop in options_choice_state_map) {
        	obj[prefix+prop] = parsing_curry(prop, s, options_choice_state_map[prop]);
        }
        
        prefix = "parse_feature_";
        for (prop in feature_state_map) {
        	obj[prefix+prop] = parsing_curry(prop, a, feature_state_map[prop]);
        }    

        obj.parseOptions = function parseOptions(str, isQuery) {
            var list = App.State.Vehicle.Options;
            var list_key = App.State.Vehicle.OptionsKey;
            var encoding = App.State.Encodings.Options;
            
            var prop;
            var prefix = "parse_option_";
            var prefix_choice = "parse_choice_option_";
            
            if (isQuery) {
            	this.OptionsToChoose = [];
            };
            
            var t;
            var i = 0;
            
            for (prop in this) {
            	if (prop.indexOf(prefix) === 0) {
            		this[prop](str, list, list_key, encoding);
            	} else 
            	if (isQuery && prop.indexOf(prefix_choice) === 0) {
            		t = this[prop](str, list, list_key, encoding);
            		for (i = t.length - 1; i >= 0; i--){
            			this.OptionsToChoose.push(t[i]);
            		};            		
            	}
            }
        };

        obj.parseFeatures = function parseFeatures(str) {
            var list = App.State.Vehicle.Features;
            var list_key = App.State.Vehicle.FeatureKey;
            var encoding = App.State.Encodings.Categories;
            
            var prop;
            var prefix = "parse_feature_";
            for (prop in this) {
            	if (prop.indexOf(prefix) === 0) {
            		this[prop](str, list, list_key, encoding);
            	};
            }
        };

        // ===================
        // = Public Commands =
        // ===================
        App.Command.DecodeBuildState = DecodeBuildState;

    })(DecodeBuildState.prototype);

}).apply(App.Command);