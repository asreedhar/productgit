App.EventBinder = function(obj) {
	if (obj instanceof Styles) {
		$(obj).bind("fetchComplete", App.Events.Styles.fetchComplete);
	}
	if (obj instanceof Style) {
		$(obj).bind("fetchComplete", App.Events.Style.fetchComplete);
	}
	if (obj instanceof VehicleImage) {
		$(obj).bind("fetchComplete", App.Events.VehicleImage.fetchComplete);
	}
	if (obj instanceof Colors) {
		$(obj).bind("fetchComplete", App.Events.Colors.fetchComplete);
		$(obj).bind("onColorChange", App.Events.Colors.onColorChange);
	}
	if (obj instanceof BuildState) {
		$(obj).bind("create", App.Events.BuildState.create);
		$(obj).bind("fetchComplete", App.Events.BuildState.fetchComplete);
        $(obj).bind("saveState", App.Events.BuildState.saveState);
	}
	if (obj instanceof ColorStateChange) {
		$(obj).bind("fetchComplete", App.Events.ColorStateChange.fetchComplete);
	}
	if (obj instanceof OptionStateChange) {
		$(obj).bind("fetchComplete", App.Events.OptionStateChange.fetchComplete);
	}
	if (obj instanceof FeatureStateChange) {
		$(obj).bind("fetchComplete", App.Events.FeatureStateChange.fetchComplete);
	}
	if (obj instanceof Vehicle) {
		$(obj).bind("create", App.Events.Vehicle.create);
		$(obj).bind("optionChanged", App.Events.Vehicle.optionChanged);
		$(obj).bind("featureChanged", App.Events.Vehicle.featureChanged);
	}
	if (obj instanceof Encodings) {
		$(obj).bind("create", App.Events.Encodings.create);
		$(obj).bind("fetchComplete", App.Events.Encodings.fetchComplete);
	}
};

// ====================
// = Bind DataMappers =
// ====================
(function() {
    function BindDataMapper(target, source) {
        target.create = source;
    };

    BindDataMapper(Vehicle, App.DataMapper.Vehicle);
    BindDataMapper(Styles, App.DataMapper.Styles);
    BindDataMapper(Style, App.DataMapper.Style);
    BindDataMapper(Standard, App.DataMapper.Standard);
    BindDataMapper(Feature, App.DataMapper.Feature);
    BindDataMapper(Option, App.DataMapper.Option);
    BindDataMapper(Colors, App.DataMapper.Colors);
    BindDataMapper(Color, App.DataMapper.Color);
    BindDataMapper(OptionGroup, App.DataMapper.OptionGroup);
    BindDataMapper(FeatureGroup, App.DataMapper.FeatureGroup);
    BindDataMapper(BuildState, App.DataMapper.BuildState);
    BindDataMapper(Encodings, App.DataMapper.Encodings);
    BindDataMapper(OptionStateChange, App.DataMapper.OptionStateChange);
    BindDataMapper(FeatureStateChange, App.DataMapper.FeatureStateChange);
    if (document.documentElement.clientWidth >= 980) {
    	BindDataMapper(VehicleImage, App.DataMapper.VehicleImage);
    };
})();

// =======================
// = Bind Service Fetchs =
// =======================
 (function() {
    function BindFetch(target, source) {
        for (var m in source) {
            target.prototype[m] = source[m];
        }
        $(target).bind("fetch", target.prototype.fetch);
    };
    
    BindFetch(Styles, App.Services.Styles);
    BindFetch(Style, App.Services.Classification);    
    BindFetch(Colors, App.Services.Colors);
    BindFetch(BuildState, App.Services.InitialState);
    BindFetch(BuildStateSave, App.Services.SaveBuildState);
    BindFetch(OptionStateChange, App.Services.UpdateState);
    BindFetch(FeatureStateChange, App.Services.UpdateCategoryState);
    BindFetch(ColorStateChange, App.Services.UpdateColorState);
    BindFetch(Encodings, App.Services.Encodings);
    
    if (document.documentElement.clientWidth >= 980) {
    	BindFetch(VehicleImage, App.Services.ImageUrl);
    };
    
})();

$.ajaxSetup(App.Services.Default.AjaxSetup);
