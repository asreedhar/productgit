var Organization = {
    "Interior": 2,
    "Exterior": 3,
    "Vehicle": 0
};
var Vehicle = function() {
	this.OptionsKey = {};
	this.FeatureKey = {};
	this.Options = [];
	this.Colors = [];
	this.Classificatoin = {
		Interior: [],
		Exterior: [],
		Vehicle: []
	};
	this.Features = [];
	this.Standards = [];
	App.EventBinder(this);
};
Vehicle.prototype = {
    Colors: [],
    Classifications: {
        Interior: [],
        Exterior: [],
        Vehicle: []
    },
    Options: [],
    Options: {},
    Features: [],
    Standards: []
};
var KeyValue = function(group_id, item_id) {
	this.GroupId = group_id;
	this.ItemId = item_id;
};
KeyValue.prototype = {
	Group: -1,
	Item: -1,
	Dom: null
};

var VehicleImage = function(src) {
	this.Src = src;
	App.EventBinder(this);
};
VehicleImage.prototype = {
	Src: ""
};
var Style = function() {
	App.EventBinder(this);
};
Style.prototype = {
    Id: -1,
    AvailableInNvd: false,
    Country: "",
    DivisionName: "",
    FleetOnly: false,
    ModelName: "",
    ModelYear: "",
    Name: "",
    NameWithoutTrim: "",
    SubdivisionName: "",
    TrimName: "",
    Broker: ""
};
var Styles = function(vin) {
    this.Vin = vin;
    this.Items = [];
    App.EventBinder(this);
};
Styles.prototype = {
    Vin: "",
    Items: []
};
var Standard = function(name, classification, sequence) {
    this.Name = name;
    this.Classification = classification;
    this.Sequence = sequence;
    App.EventBinder(this);
};
Standard.prototype = {
    Name: "",
    Classification: "",
    Sequence: -1
};
var FeatureGroup = function(name, classification) {
	this.Name = name;
	this.Items = [];
	this.Classification = classification;
	App.EventBinder(this);
};
FeatureGroup.prototype = {
	Name: "",
	Classificatoin: "",
	Item: []
};
var Feature = function(id, name) {
    this.Id = id;
    this.Name = name;
    App.EventBinder(this);
};
Feature.prototype = {
    Name: "",
    Id: -1
};
var OptionGroup = function(name, classification) {
	this.Name = name;
	this.Items = [];
	this.Classification = classification;
	App.EventBinder(this);
};
OptionGroup.prototype = {
	Name: "",
	Item: [],
	Classification: ""
};
var Option = function(code, name, description, requirementName, requirementId) {
    this.Code = code;
    this.Name = name;    
    this.Description = description;    
    this.Selected = SelectionState.Off;
    this.RequirementName = requirementName || null;
    this.RequirementId = requirementId || -1;
    App.EventBinder(this);
};
Option.prototype = {
    Code: "",
    Name: "",
    Description: "",
    Selected: false,
    RequirementName: "",
	RequirementId: -1
};
var Colors = function() {
	this.Items = [];
	App.EventBinder(this);
};
Colors.prototype = {
	Items: []
};
var Color = function(code, name, classification, rgb) {
    this.Code = code;
    this.Name = name;
    this.Classification = classification;
    this.RGB = rgb;
    App.EventBinder(this);
};
Color.prototype = {
    RGB: "000000",
    Name: "Name",
    Code: "code",
    Classification: ""
};
var BuildState = function(option_state, category_state, standard_state) {
	this.OptionState = option_state;
	this.CategoryState = category_state;
	this.StandardState = standard_state;
	this.OptionsToChoose = [];
	App.EventBinder(this);
};
BuildState.prototype = {
	EngineResult: "",
	LogicOperator: "",
	OptionSelections: "",
	OptionState: "",
	CategoryState: "",
	StandardState: "",
	OptionsToChoose: [],
	StateString: ""
};
var BuildStateSave = function  (bodyStyle, isFleet, styleId, state, broker, inventoryId) {
    this.BodyStyle = bodyStyle | "";
    this.FleetOnly = isFleet || false;
    this.Id = styleId || -1;
    this.StateString = state || "";
    this.Broker = broker || "";
    this.InventoryId = inventoryId || -1;
    App.EventBinder(this);
};
var OptionStateChange = function(optionCode, selected) {
	this.OptionCode = optionCode;
	this.Selected = selected;
	App.EventBinder(this);
};

OptionStateChange.prototype = {
	OptionCode: "optionCode",
	Selected: false,
	StateString: ""
};
var FeatureStateChange = function(id, selected) {
	this.CategoryId = id;
	this.Selected = selected;
	App.EventBinder(this);
};
FeatureStateChange.prototype = {
	CategoryId: -1,
	Selected: false,
	StateString: ""
};

var ColorStateChange = function(exteriorCodeOne, exteriorCodeTwo, interiorCode) {
	this.ExteriorCodeOne = exteriorCodeOne;
	this.ExteriorCodeTwo = exteriorCodeTwo;
	this.InteriorCode = interiorCode;
	App.EventBinder(this);
};

var EngineResult = {
	Normal: "N",
	Query: "Q"
};

var Encodings = function() {
	this.Categories = [];
	this.Standards = [];
	this.Options = [];
	App.EventBinder(this);
};
Encodings.prototype = {
	Standards: [],
	Categories: [],
	Options: []
};

var EnumFactory = function(array) {
	var Enum = {};
	for (var i=0,l=array.length; i < l; i++) {
		Enum[array[i]] = 1<<i;
	};
	
	function valueFor(state) {
		var result;
		for (var i = state.length - 1; i >= 0; i--){
			result |= Enum[state[i]];
		};
		return result;
	}
	
	function toEnum(state) {
		var result = [];
		var state = state.toString(2);
		var value, prop;
		for (var i = state.length - 1, offset = i; i >= 0; i--){
			if (state[i]==="1") {
				value = 1<<(offset - i);
				for (prop in Enum) {
					if (Enum[prop] === value) {
						result.push(prop);
					}
				}
			};
		};
		return result;
	}
	
	Enum.isSet = EnumFactory.isSet;
	Enum.valueFor = valueFor;
	Enum.toEnum = toEnum;
	
	return Enum;
};
EnumFactory.isSet = function (state, value) {
		var flags = [];
		if (arguments.length > 2) {
			for (var i = arguments.length - 1; i >= 1; i--){
				flags.push(arguments[i]);
			};
		} else {
			flags.push(value);
		}
		for (var i = flags.length - 1; i >= 0; i--){
			if ((state & flags[i]) === flags[i]) {
				return true;
			}
		};
		return false;
	};

var Action = EnumFactory(["NotDefined","Add","Package","Remove"]);
var SelectionState = EnumFactory(["Off","On","Require","Include","OrRequire","OrInclude","Exclude"]);
