$LAB.script("../../Public/Scripts/Lib/jquery/jquery-ui-1.8.2.custom/js/jquery-ui-1.8.2.custom.min.js");

// ==========================
// = Style Tempalte Builder =
// ==========================
var StyleTemplateBuilder = function (data) {
	this.data = data;
	this.$el = $("#style_select");
};
StyleTemplateBuilder.prototype = {
	init: function () {
		if (!!this.data) {
			this.build();
		}
	},
	build: function () {
		var opt, i, l;
		for (i = 0, l = this.data.Items.length; i < l; i++) {
			opt = $("<option/>").attr("value", this.data.Items[i].Id);
			opt.text(this.data.Items[i].Name);
			this.$el.append(opt);
		}
		this.$el.bind("change", $.proxy(App.Events.Styles.onChange, this.data));				
		if (this.data.Items.length === 1) {
			this.$el.attr("selectedIndex", 1);
			this.$el.trigger("change");
		};
	},
	clean: function () {
		$("option:gt(1)",this.$el).remove();
	}
};

// =================================
// = VehicleImage Template Builder =
// =================================
var ImageTempalteBuilder = function (data) {
	this.data = data;
	this.$el = $(".image");
};
ImageTempalteBuilder.prototype = {
	init: function () {
		if (!!this.data) {
			this.build();
		}
	},
	build: function () {
		$("a", this.$el).attr("href", this.data.Src);
		$("img", this.$el).attr("src", this.data.Src);
	},
	clean: function () {
		$("a", this.$el).attr("href", "");
		$("img", this.$el).attr("href", "");
	}
};

// =========================
// = Vehicle Color Builder =
// =========================
var ColorTemplateBuilder = function (data) {
	this.data = data;
	this.$el = $("#colors");
};
ColorTemplateBuilder.prototype = {
	init: function () {
		if (!!this.data) {
			this.build();
		}
	},
	build: function () {
		var exterior1 = $.grep(this.data.Items, function (a) { return a.Classification === "ExteriorOne"; }),
		exterior2 = $.grep(this.data.Items, function (a) { return a.Classification === "ExteriorTwo"; }),
		interior = $.grep(this.data.Items, function (a) { return a.Classification === "Interior"; });
		this.ext_1_$el = $("#exterior_color_1"),
		this.ext_2_$el = $("#exterior_color_2"),
		this.int_1_$el = $("#interior_color");
		function onChange(event) {
			var el = event.target,
			color = $(el.options[el.selectedIndex]).data(),
			sample = $(".sample span", $(el).parent(".color_selector"));
			sample.css("backgroundColor", "#" + color.RGB);
			
			this["onColorChange"]();
		}
		function populate_color(el, colors) {
			var opt, i, l;
			for (i = 0, l = colors.length; i < l; i++) {
				opt = $("<option />").attr("value", colors[i].Code);
				opt.text(colors[i].Name);
				opt.data(colors[i]);
				$(el).append(opt);
			}
		}
		populate_color(this.ext_1_$el, exterior1);
		if (!!exterior2.length) {
			populate_color(this.ext_2_$el, exterior2);
		} else {
			$(this.ext_2_$el).parents("p").hide();
		}
		populate_color(this.int_1_$el, interior);
		$(this.ext_1_$el).bind("change", $.proxy(onChange, this));
		$(this.ext_2_$el).bind("change", $.proxy(onChange, this));
		$(this.int_1_$el).bind("change", $.proxy(onChange, this));
	},
	clean: function () {
		var ex_1 = $("#exterior_color_1"),
		ex_2 = $("#exterior_color_2"),
		in_1 = $("#interior_color");
		
		$("option:gt(1)", ex_1).remove();
		$("option:gt(1)", ex_2).remove();
		$("option:gt(1)", in_1).remove();
		$(ex_1, ex_2, in_1).unbind();
	},	
	onColorChange: function () {
		var colors = {
			"exteriorCodeOne": this.ext_1_$el.get(0).selectedIndex > 0 ? this.ext_1_$el.val() : null,
			"exteriorCodeTwo": this.ext_2_$el.get(0).selectedIndex > 0 ? this.ext_2_$el.val() : null,
			"interiorCode": this.int_1_$el.get(0).selectedIndex > 0 ? this.int_1_$el.val() : null
		};
		
		$(this.data).trigger("onColorChange", colors);
	}
};

// ==========================
// = VehicleTemplateBuilder =
// ==========================
var VehicleTemplateBuilder = function (data) {
	this.data = data;
};
VehicleTemplateBuilder.prototype = {
	init: function () {
		if (!!this.data) {
			this.build();
		}
		
		setTimeout(App.prototype.init$triggerDefaults, 0); // Cough, cough, hack, hack
	},
	build: function () {
		
		this.build_vehicle();
		this.build_interior();
		this.build_exterior();
		
		this.bind_build_clicks();
		
		var options = [];
		for (var i=0,l=this.data.Options.length; i < l; i++) {
			for (var j=0, k=this.data.Options[i].Items.length; j < k; j++) {
				options.push("(" + this.data.Options[i].Items[j].Code + ") " +this.data.Options[i].Items[j].Name);
			};
		};

        $("#add_option_code").autocomplete({
            source: options
        });
	},
	clean: function () {
		var els = $("fieldset.attribute_group, div.standards");
		els.unbind();
		els.remove();
	},	
	build_vehicle: function () {
		var el = $("#vehicle"),
		classifications = this.data.Classifications.Vehicle;
		this.build_card(el, classifications);
	},
	build_interior: function () {
		var el = $("#interior"),
		classifications = this.data.Classifications.Interior;
		this.build_card(el, classifications);
	},
	build_exterior: function () {
		var el = $("#exterior"),
		classifications = this.data.Classifications.Exterior;
		this.build_card(el, classifications);
	},
	build_card: function (el, classifications) {
		this.build_classifications(el, classifications);
		for (var i=0; i < classifications.length; i++) {
			this.build_standards(el, classifications[i]);
			this.build_features(el, classifications[i]);
			this.build_options(el, classifications[i]);
		}
	},
	build_classifications: function (wrapper, classifications) {
		var fieldset, legend, i, l, div;
		
		for (i = 0, l = classifications.length; i < l; i++) {
			div = $("<div />").addClass("fieldset_wrapper");
			fieldset = $("<fieldset/>").attr("id",classifications[i].replace(/\W/g,"_"));
			fieldset.addClass("attribute_group");
			legend = $("<legend />").text(classifications[i]);	
			fieldset.append(legend);
			div.append(fieldset);
			$(wrapper).append(div);
		};
	},
	build_standards: function (wrapper, filter) {
		var standards = $.grep(this.data.Standards, function (a) { return a.Classification.toLowerCase() === filter.toLowerCase(); });
		if (!!!standards.length) {
			return;
		}
		var template = $("<div/>").addClass("standards");
		template.append("<h5>Standard Equipment</h5>");
		var list = $("<ul/>").addClass("columned wrapper");
		var item;
		var force_long = standards.length === 1;
		var is_long = false;
		for (var i = standards.length - 1; i >= 0; i--){		
			is_long = standards[i].Name.length > 150;
			if (is_long) {
				item = $("<li/>").text(standards[i].Name.substr(0,150) + "...").addClass("standard");
				item.attr("title", standards[i].Name);
			} else {
				item = $("<li/>").text(standards[i].Name).addClass("standard");
			}
			if (force_long || is_long) {
				item.addClass("long");
			} 
			list.append(item);
		}
		template.append(list);
		$("#"+filter.replace(/\W/g,"_"),wrapper).append(template);
	},
	build_features: function (wrapper, filter) {
		var features = $.grep(this.data.Features, function (a) { return a.Classification.toLowerCase() === filter.toLowerCase(); });
		if (!!!features.length) {
			return;
		}
		var template = $("<fieldset/>").addClass("features_section");
		var legend = $("<legend/>").text("Generic Options");
		var list = $("<ul/>").addClass("input_list columned wrapper");
		var item, label, input, is_group, feature_name, group, feature, features_store;
		features_store = App.State.Vehicle.FeatureKey;
		function render_checkboxes(group, list) {
			var feature, item, label, input;
			for (var i=0; i < group.Items.length; i++) {
				feature = group.Items[i];
				item = $("<li/>");
				label = $("<label/>");
				input = $("<input/>").attr("type", "checkbox").attr("name", feature.Name).addClass("feature");
				input.data("Feature", feature);
				label.append(input);
				features_store[feature.Id].Dom = input.get(0);
				label.append(" "+feature.Name);
				item.append(label);
				list.append(item);
			}
		}
		function render_radio(group, list) {
			var feature, item, label, input;
			for (var i=0; i < group.Items.length; i++) {
				feature = group.Items[i];
				item = $("<li/>");
				label = $("<label/>");
				input = $("<input/>").attr("type", "radio").attr("name", group.Name).addClass("feature");
				input.data("Feature", feature);
				label.append(input);
				features_store[feature.Id].Dom = input.get(0);
				label.append(" "+feature.Name);
				item.append(label);
				list.append(item);
			}
		}
		function render_select_box (group, list) {
			var feature, item, label, select, opt;
			item = $("<li />");
			label = $("<label />");
			label.append(group.Name);
			select = $("<select />").attr("name", group.Name).addClass("option");
			opt = $("<option />").text("Please select one");
			select.append(opt);
			for (var i = group.Items.length - 1; i >= 0; i--){
				feature = group.Items[i];
				opt = $("<option />").val(feature.Name).text(feature.Name);
				opt.data("Feature", feature);
				select.append(opt);
				features_store[feature.Id].Dom = select.get(0);
			}
			label.append(select);
			item.append(label);
			list.append(item);
		}
		for (var i = features.length - 1; i >= 0; i--){
			group = features[i];
			is_group = !!group.Name && group.Items.length > 1;
			if (is_group) {
				// if (group.Items.length > 2) {
				// 	render_select_box(group, list);
				// } else {
					render_radio(group, list);
				// }
			} else {
				render_checkboxes(group, list);
			}
		}		
		template.append(legend);
		template.append(list);
		$("#"+filter.replace(/\W/g,"_"),wrapper).append(template);
	},
	build_options: function (wrapper, filter) {
		var options;
		
		if (!!filter) {
			options = $.grep(this.data.Options, function (a) {return a.Classification.toLowerCase() === filter.toLowerCase(); } );
		} else {
			options = this.data.Options;
		}
		
		if (!!!options.length) {
			return;		
		}
		var template = $("<fieldset/>").addClass("options_section");
		var legend = $("<legend/>").text("OEM Options");
		var list = $("<ul/>").addClass("input_list columned wrapper");
		var item, label, input, option_code, feature_name, is_group, option_name, group, option, option_store;
		option_store = App.State.Vehicle.OptionsKey;
		function render_checkboxes(group, list) {
			var option, item, label, input, option_code, name, is_long = false;
			for (var i=0; i < group.Items.length; i++) {
				option = group.Items[i];
				item = $("<li/>");
				label = $("<label/>");
				input = $("<input/>").attr("type", "checkbox").attr("name", option.Code).addClass("option");
				input.data("Option", option);
				label.append(input);
				option_store[option.Code].Dom = input.get(0);
				option_code = $("<span />").addClass("option_code").text(" (" + option.Code +") ");
				label.append(option_code);
				is_long = option.Name.length > 90;
				feature_name = $("<span />").addClass("feature_name").text(option.Name.toLowerCase());
				if (is_long) {
					item.addClass("long");
				};
				label.append(feature_name);
				item.attr("title", option.Description);
				item.append(label);
				list.append(item);
			}
		}
		function render_radio(group, list) {
			var option, item, label, input, option_code, is_long = false;
			for (var i=0; i < group.Items.length; i++) {
				option = group.Items[i];
				item = $("<li/>");
				label = $("<label/>");
				input = $("<input/>").attr("type", "radio").attr("name", group.Name).addClass("option");
				input.data("Option", option);
				label.append(input);
				option_store[option.Code].Dom = input.get(0);
				option_code = $("<span />").addClass("option_code").text(" (" + option.Code +") ");
				label.append(option_code);
				feature_name = $("<span />").addClass("feature_name").text(option.Name.toLowerCase());
				is_long = option.Name.length > 90;
				if (is_long) {
					item.addClass("long");
				};
				label.append(feature_name);
				item.attr("title", option.Description);
				item.append(label);
				list.append(item);
			}
		}
		function render_select_box (group, list) {
			var option, item, label, select, opt;
			item = $("<li />");
			label = $("<label />");
			label.append(group.Items[0].RequirementName);
			select = $("<select />").attr("name", group.Name).addClass("option");
			opt = $("<option />").text("Please select one");
			select.append(opt);
			for (var i = group.Items.length - 1; i >= 0; i--){
				option = group.Items[i];
				opt = $("<option />").val(option.Code).text(option.Name);
				opt.data("Option", option);
				select.append(opt);
				option_store[option.Code].Dom = select.get(0);
			}
			label.append(select);
			item.append(label);
			list.append(item);
		}
		for (var i = options.length - 1; i >= 0; i--){
			group = options[i];
			is_group = !!group.Name && group.Items.length > 1;
			if (is_group) {
				// if (group.Items.length > 2) {
				// 	render_select_box(group, list);
				// } else {
					render_radio(group, list);
				// }
			} else {
				render_checkboxes(group, list);
			}
		}
		template.append(legend);
		template.append(list);
		$("#"+filter.replace(/\W/g,"_"),wrapper).append(template);
	},
	options_sort: function (a,b) {
		return a.RequirementId > b.RequirementId;
	},
	feature_sort: function (a,b) {
		return a.Name > b.Name;
	},
	bind_build_clicks: function () {
		// TODO move out of tempalte
		var list_builder = function (list_selector, input_selector) {
				var list = $(list_selector).first(), 
				item_builder = function () {
					list.append($("<li/>").text($(this).parents("label").text()));
				};
				$("li", list).remove();
				$(input_selector).each(item_builder);
		};
		
		
		var option_builder = function(list_selector, input_selector) {
			var list = $(list_selector).first();
			var li;
			var h6;
			var code;
			var parent;
			item_builder = function() {
				li = $("<li />");
				parent = $(this).parents("label");
				h6 = $("<h6 />").text($("span.feature_name", parent).text())
				code = $("<span />").text($("span.option_code", parent).text()).addClass("option_code")
				li.append(h6).append(code);
				list.append(li);
			};
			$("li", list).remove();
			
			
			$(input_selector).each(item_builder);
			
		};
		
		var defaultBulder = function() {
			option_builder("#build_options ol", "input.option:checked");
			list_builder("#build_features ul", "input.feature:checked");
		};
		var hasTouch = "createTouch" in document;
		var builder_click = function (e) {
			var element = e.target;
			if (!hasTouch) {
				$("#options fieldset.attribute_group").each(function () { 
					if (!!!$(element).data("ForceVisible") &&
						$("input:checked", element).length === $("input", element).length) { 
						$("li, fieldset, h5",element).hide();
					}
				});
			};
			var option = $(element).data("Option");
			var feature = $(element).data("Feature");
			if (!!option) {
				this.onOptionChanged(option.Code, element.checked);
			};
			if (!!feature) {
				this.onFeatureChanged(feature.Id, element.checked);
			};
		};		
		$("#options input").bind("click", $.proxy(builder_click, this));
		if (!hasTouch) {
			$("#options fieldset.attribute_group legend").bind("click", function (e) {
				var isVisible = false;
				$("li,fieldset,h5",this.parentNode).each(function () {
					isVisible |= $(this).is(":visible");
				});
				$("li,fieldset,h5",this.parentNode)[!!isVisible ? "hide" : "show" ]();
				if (isVisible) {
					$(this.parentNode).data("ForceVisible", true);
				} else {
					$(this.parentNode).removeData("ForceVisible");
				}
				e.stopPropagation();
			});
		};
		
		VehicleTemplateBuilder.update_summary = defaultBulder; // Lame
	},
	onOptionChanged: function(code, selected) {
		$(this.data).trigger("optionChanged", {"Code": code, "Selected": selected});
	},
	onFeatureChanged: function(id, selected) {
		$(this.data).trigger("featureChanged", {"Id": id, "Selected": selected});
	}
};
VehicleTemplateBuilder.update_selections = function () {
	var vehicle = App.State.Vehicle,
	i, j, l, gl, option, dom, should_select, should_deselect, change_enable, should_disable;
	for (i = 0, l = vehicle.Options.length; i < l; i++) {
		for (j = 0, gl = vehicle.Options[i].Items.length; j < gl; j++) {
			option = vehicle.Options[i].Items[j];			
			should_select = SelectionState.isSet(option.Selected, SelectionState.On, SelectionState.Include, SelectionState.Require);
			should_deselect = SelectionState.isSet(option.Selected, SelectionState.Exclude, SelectionState.Off); // May Need additional Logic, PM						
			if (should_select || should_deselect) {
				dom = vehicle.OptionsKey[option.Code].Dom;
				if ($(dom).is("input")) {
					$(dom).attr("checked", should_select);
				} else
				if ($(dom).is("select")) {
					var select = $(dom).get(0);
					for (var k = select.options.length - 1; k >= 0; k--){
						if ($(select.options[k]).val() == option.Code) {
							$(select.options[k]).attr("selected", should_select);
						}
					};
					
				}
			}
			
			change_enable = false;
			if (SelectionState.isSet(option.Selected, SelectionState.Exclude)) {
				change_enable = true;
				should_enable = SelectionState.isSet(option.Selected, SelectionState.OrInclude, SelectionState.OrRequire);				
			} else if (SelectionState.isSet(option.Selected, SelectionState.Include, SelectionState.Require)) {
				change_enable = true;
				should_enable = false;
			}
			
			if (change_enable) {
				$(dom).attr("enabled", should_enable)
			};
		}
	}
	
	should_select = false;
	should_deselect = false;
	
	for (i = 0, l = vehicle.Features.length; i < l; i++) {
		for (j = 0, gl = vehicle.Features[i].Items.length; j < gl; j++) {
			option = vehicle.Features[i].Items[j];		
			should_select = Action.isSet(option.Selected, Action.Add, Action.Package);
			dom = vehicle.FeatureKey[option.Id].Dom;
			if ($(dom).is("input")) {
				$(dom).attr("checked", should_select);
			} else
			if ($(dom).is("option")) {
				$(dom).attr("selected", should_select);
			}
		}
	}
};

// ======================
// = BuildDialogBuilder =
// ======================
var BuildDialogBuilder = function(data) {
	this.data = data;
};
BuildDialogBuilder.prototype = {
	init: function() {
		if (!!this.data) { // requires OptionsToChoose
			this.build();
		};
	},
	build: function() {
		var vehicle = App.State.Vehicle;
		var groups = {};
		var wrapper = $("#dialog");
		var option, group, item;
		
		for (var i=0; i < this.data.OptionsToChoose.length; i++) {
			group = vehicle.Options[this.data.OptionsToChoose[i].GroupId];
			item = group.Items[this.data.OptionsToChoose[i].ItemId];
			if (!groups.hasOwnProperty(group.Classification)) {
				groups[group.Classification] = [];
			};
			groups[group.Classification].push(item);
		};
		
		
		var option, input, label, li, option_code, feature_name;
		for (var prop in groups) {
			var fieldset = $("<fieldset />").addClass("options_section");	
			var legend = $("<legend />").text(prop);
			var list = $("<ul />").addClass("wrapper");	
			for (var j=0; j < groups[prop].length; j++) {
				option = groups[prop][j];
				li = $("<li />");
				label = $("<label />");
				input = $("<input/>").attr("type", "radio").attr("name", option.Code).addClass("option");
				option_code = $("<span />").addClass("option_code").text(" (" + option.Code +") ");
				label.append(input);
				label.append(option_code);
				feature_name = $("<span />").addClass("feature_name").text(option.Name.toLowerCase());
				label.append(feature_name);
				li.attr("title", option.Description);
				li.append(label);
				list.append(li);
			};
			fieldset.append(legend);
			fieldset.append(list);
			wrapper.append(fieldset);	
		};
		
		this.bind_clicks();
		
		wrapper.dialog({
			title: "One of the following is required for your selecion",
			modal: true,
			width: 600
		}); /// Update Text, Needs Button to turn off engine
	},
	bind_clicks: function() {
		var onClick = function(event) {
			var el = event.target;
			var json = {
				"Code": el.name,
				"Selected": el.checked,
				"Type": Option
			};
			$(App.State.Vehicle).trigger("optionChanged", json);
			
			this.clean();
		};
		$("input","#dialog").bind("click", $.proxy(onClick, this));
	},
	clean: function() {
		$("#dialog").dialog("destroy");
		$("input","#dialog").unbind();		
		$("fieldset","#dialog").remove();
	}
};