App.Services = {
    "Default": {
        AjaxSetup: {
            type: "POST",
            dataType: "json",
            processData: false,
            contentType: "application/json; charset=utf-8",
            error: function(xhr) {
            	console.log(xhr.responseText);
            	alert([":( Fuck! Fuck! Fuck :(", "\r\n", xhr.responseText]);
            }
        }
    },
    "Styles": {
        AjaxSetup: {
            url: "../../Services/Chrome.asmx/GetStyles"
        },
        fetch: function() {
            this.AjaxSetup.data = this.getData();
            var onFetchSuccess = function(json) {
            	var styles = Styles.create(json.d);
                $(this).trigger("fetchComplete", [styles]);
            };
            this.AjaxSetup.context = this;
            this.AjaxSetup.success = onFetchSuccess;
            $.ajax(this.AjaxSetup);
        },
        getData: function() {
            return '{"vin":"' + this.Vin + '"}';
        }
    },
    "ImageUrl": {
        AjaxSetup: {
            url: "../../Services/Chrome.asmx/GetImageUrl"
        },
        fetch: function() {
            this.AjaxSetup.data = this.getData();
            var onFetchSuccess = function(json) {
            	var image = VehicleImage.create(json.d);
            	$(this).trigger("fetchComplete", [image]);
            };
            this.AjaxSetup.context = this;
            this.AjaxSetup.success = onFetchSuccess;
            $.ajax(this.AjaxSetup);
        },
        getData: function() {
        	return '{"bodyStyle": "", "isFleet": ' + this.FleetOnly + ', "styleId": ' + this.Id + '}';
        }
    },
    "Colors": {
    	AjaxSetup: {
            url: "../../Services/Chrome.asmx/GetColors"
        },
        fetch: function() {
            this.AjaxSetup.data = this.getData();
            var onFetchSuccess = function(json) {
            	var colors = Colors.create(json.d);
            	$(this).trigger("fetchComplete", colors);
            };
            this.AjaxSetup.context = this;
            this.AjaxSetup.success = onFetchSuccess;
            $.ajax(this.AjaxSetup);
        },
        getData: function() {
        	return '{"bodyStyle": "", "isFleet": ' + this.FleetOnly + ', "styleId": ' + this.Id + '}';
        }
    },
	"Encodings": {
    	AjaxSetup: {
            url: "../../Services/Chrome.asmx/GetEncodings"
        },
        fetch: function() {
            this.AjaxSetup.data = this.getData();
            var onFetchSuccess = function(json) {
            	var encodings = Encodings.create(json.d);
            	$(this).trigger("fetchComplete", colors);
            };
            this.AjaxSetup.context = this;
            this.AjaxSetup.success = onFetchSuccess;
            $.ajax(this.AjaxSetup);
        },
        getData: function() {
        	return '{"bodyStyle": "", "isFleet": ' + this.FleetOnly + ', "styleId": ' + this.Id + '}';
        }
    },
	"Classification": {
        AjaxSetup: {
            url: "../../Services/Chrome.asmx/GetClassificationHeaders"
        },
        fetch: function() {
            this.AjaxSetup.data = this.getData();
            var onFetchSuccess = function(json) {
                var vehicle = Vehicle.create(json.d);
                $(this).trigger("fetchComplete", [vehicle]);
            };
            this.AjaxSetup.context = this;
            this.AjaxSetup.success = onFetchSuccess;
            $.ajax(this.AjaxSetup);
        },
        getData: function() {
            return '{"bodyStyle": "", "isFleet": ' + this.FleetOnly + ', "styleId": ' + this.Id + ', "broker": "' + this.Broker + '"}';
        }
    },
    "InitialState": {
    	AjaxSetup: {
    		url: "../../Services/Chrome.asmx/GetVehicleInitialState"
    	},
    	fetch: function() {
    		this.AjaxSetup.data = this.getData();
            var onFetchSuccess = function(json) {
                var buildState = BuildState.create(json.d);
                $(this).trigger("fetchComplete", [buildState]);
            };
            this.AjaxSetup.context = this;
            this.AjaxSetup.success = onFetchSuccess;
            $.ajax(this.AjaxSetup);
    	},
    	getData: function() {
    		return '{"bodyStyle": "", "isFleet": ' + this.FleetOnly + ', "styleId": ' + this.Id + '}';
    	}
    },
    "SaveBuildState": {
    	AjaxSetup: {
    		url: "../../Services/Chrome.asmx/UpdateBuild"
    	},
    	fetch: function() {
    		this.AjaxSetup.data = this.getData();
            var onFetchSuccess = function(json) {
            };
            this.AjaxSetup.context = this;
            this.AjaxSetup.success = onFetchSuccess;
            $.ajax(this.AjaxSetup);
    	},
    	getData: function() {
    		return '{"bodyStyle": "", "isFleet": ' + this.FleetOnly + ', "styleId": ' + this.Id + ', "state": "' + this.StateString + '", "broker":"' + this.Broker + '", "inventoryId": ' +this.InventoryId+ '}';
    	}
    },
    "UpdateState": {
    	AjaxSetup: {
    		url: "../../Services/Chrome.asmx/UpdateVehicleState"
    	},
    	fetch: function() {
    		this.AjaxSetup.data = this.getData();
            var onFetchSuccess = function(json) {
                var buildState = BuildState.create(json.d);
                $(this).trigger("fetchComplete", [buildState]);
            };
            this.AjaxSetup.context = this;
            this.AjaxSetup.success = onFetchSuccess;
            $.ajax(this.AjaxSetup);
    	},
    	getData: function() {
    		return '{"bodyStyle": "", "isFleet": ' + this.FleetOnly + ', "styleId": ' + this.Id + ', "prevState": "' + this.StateString + '" ,"optionCode": "' + this.OptionCode + '", "selected": ' + this.Selected +'}';
    	}
    },
    "UpdateCategoryState": {
    	AjaxSetup: {
    		url: "../../Services/Chrome.asmx/CategoryUpdateState"
    	},
    	fetch: function() {
    		this.AjaxSetup.data = this.getData();
            var onFetchSuccess = function(json) {
                $(this).trigger("fetchComplete", [json]);
            };
            this.AjaxSetup.context = this;
            this.AjaxSetup.success = onFetchSuccess;
            $.ajax(this.AjaxSetup);
    	},
    	getData: function() {
    		return '{"bodyStyle": "", "isFleet": ' + this.FleetOnly + ', "styleId": ' + this.Id + ', "prevState": "' + this.StateString + '" ,"categoryId": "' + this.CategoryId + '", "selected": ' + this.Selected +'}';
    	}
    },
    "UpdateColorState": {
    	AjaxSetup: {
    		url: "../../Services/Chrome.asmx/GetColorOptions"
    	},
    	fetch: function() {
    		this.AjaxSetup.data = this.getData();
            var onFetchSuccess = function(json) {
                $(this).trigger("fetchComplete", [json]);
            };
            this.AjaxSetup.context = this;
            this.AjaxSetup.success = onFetchSuccess;
            $.ajax(this.AjaxSetup);
    	},
    	getData: function() {
    		return '{"exteriorCodeOne": "' + this.ExteriorCodeOne + '", "exteriorCodeTwo": "'+ this.ExteriorCodeTwo +'", "interiorCode": "'+this.InteriorCode+'", "styleId": '+this.Id+', "isFleet": '+this.FleetOnly+' }';
    	}
    }
};
