var Page = new App();
Page.Sizes = new WindowResizer({
    "sizes": {
        0: "option_column_1",
        481: "option_column_2",
        769: "side_by_side option_column_1",
        980: "side_by_side option_column_2",
        1024: "side_by_side option_column_3"
    }
});

var SimpleScroll = function(element) {
	this.element = element;
};
SimpleScroll.prototype = {
	init: function() {
		var isTouch = "createTouch" in document;
		var isIphone = (/iphone/gi).test(navigator.appVersion);
		var isIpad = (/ipad/gi).test(navigator.appVersion);
		var has3d = ('WebKitCSSMatrix' in window && 'm11' in new WebKitCSSMatrix());
		if (has3d && isTouch) {
			this.setElementPosition = function(value) {
				var y = typeof value === "undefined" ? 0 : value;				
				this.lastY += y,
				maxHeight = this.element.clientHeight - this.clientHeight;
				if (this.lastY < 0) {
					this.lastY = 0;
				} else if (this.lastY > maxHeight) {
					this.lastY = maxHeight;
				}
				this.element.style.webkitTransform = "translate3d(0," + (-1*this.lastY) + "px,0)";
				if (this.lastY > 58) {
					this.secondBar.style.webkitTransform = "translate3d(0," + (this.lastY-58) + "px,0)";
				} else {
					this.secondBar.style.webkitTransform = "translate3d(0,0,0)";
				}
			};
		} else if (isTouch) {
			this.setElementPosition = function(value) {
				var y = typeof value === "undefined" ? 0 : value;
				this.element.style.webkitTransform = "translate(0px," + y + "px)";
			};
		}
		
		this.lastY = 0;
		this.startValue = 0;
		
		if (isTouch && isIphone) {
			document.documentElement.addEventListener("touchstart", $.proxy(this.onTouchStart, this));
			document.documentElement.addEventListener("touchmove", $.proxy(this.onTouchMove, this));
			
			this.clientHeight = document.documentElement.clientHeight;
			this.secondBar = document.getElementById('display_controls');
			this.element.style.webkitTransition = "-webkit-transform 50ms cubic-bezier(0.0, 0.0, 0.25, 1.0);";
		}
		
		
		if (isIpad) {
			$(document.body).addClass("ipad");
		};
		if (isIphone) {
			$(document.body).addClass("iphone");
		};
	},
	onTouchStart: function(e) {
		this.startValue = e.targetTouches[0].clientY;
	},
	onTouchMove: function(e) {
		e.preventDefault();
		var start = this.startValue,
		end = e.targetTouches[0].clientY,
		delta;
		
		delta = start - end;
		
		this.setElementPosition(delta*0.15);
	}
};

var scroller;

var styles = new Styles($.getUrlVar('v'));

$(styles).trigger("fetch");

new SimpleScroll(document.getElementById('main_content')).init();
