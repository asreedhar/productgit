﻿/* Shared AssemblyInfo. Add as link file to all projects.
 * Needs to be autogenerated by build in future to change the version number
 */
using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Resources;

[assembly: AssemblyCompany("Firstlook, LLC")]
[assembly: AssemblyCopyright("Copyright © Firstlook, LLC 2007")]
[assembly: AssemblyVersion(FirstLook.Internal.FirstLookAssemblyInfo.Version)]
[assembly: AssemblyFileVersion(FirstLook.Internal.FirstLookAssemblyInfo.Version)]
[assembly: AssemblyTrademark("")]

[assembly: ComVisible(false)]
#if !NOT_CLS_COMPLIANT
[assembly: CLSCompliant(true)]
#endif

[assembly: AssemblyCulture("")]
[assembly: NeutralResourcesLanguage("en-US")]
[assembly: AssemblyConfiguration("")]

namespace FirstLook.Internal
{
    internal static class FirstLookAssemblyInfo
    {
        public const string Version = "1.0.0.0";
    }
}