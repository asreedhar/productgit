﻿using System;
using FirstLook.AmazonCloudSearchService.DomainModel.Utility;

namespace FirstLook.AmazonCloudSearchService.DomainModel.Exceptions
{
    public class FirstlookGeneralException : Exception
    {
        public FirstlookGeneralException(string message)
            : base(message)
        {
            this.SetCode((int)ErrorCodeEnumeration.GeneralFirstlookError);
        }

        public FirstlookGeneralException(string message, string devMsg)
            : base(message)
        {
            this.SetCode((int)ErrorCodeEnumeration.GeneralFirstlookError);
            this.SetDevMsg(devMsg);
        }
    }
}
