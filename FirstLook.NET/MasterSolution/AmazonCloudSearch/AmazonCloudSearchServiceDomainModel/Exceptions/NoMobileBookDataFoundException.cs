﻿using System;
using FirstLook.AmazonCloudSearchService.DomainModel.Utility;

namespace FirstLook.AmazonCloudSearchService.DomainModel.Exceptions
{
   [Serializable]
   public class NoMobileBookDataFoundException:Exception
    {
        public NoMobileBookDataFoundException(string message): base(message)
        {
            this.SetCode((int)ErrorCodeEnumeration.NoMobileBookFound);
        }
    }
}
