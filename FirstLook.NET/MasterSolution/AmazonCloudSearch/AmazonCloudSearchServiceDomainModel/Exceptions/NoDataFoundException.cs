﻿using System;
using FirstLook.AmazonCloudSearchService.DomainModel.Utility;

namespace FirstLook.AmazonCloudSearchService.DomainModel.Exceptions
{
    [Serializable]
    public class NoDataFoundException : Exception //also extended by
    {
        public NoDataFoundException(string message) : base(message)
        {
            this.SetCode((int)ErrorCodeEnumeration.NoDataFound);
        }
    }
}