﻿using System;

namespace FirstLook.AmazonCloudSearchService.DomainModel.Exceptions
{
    class ExceptionFilters
    {
        public static Exception InvalidInputEquipmentOptions(ArgumentNullException ex, string equipmentId)
        {
            if (ex.Message == "VehicleConfiguration")
            {
                return new InvalidInputException("Equipment", "Invalid Value(s)", "Could not find option for Equipment ID = " + equipmentId);
            }
            return ex;
        }

        public static Exception InvalidInputTrimId(ArgumentNullException ex, string trimId)
        {
            if (ex.Message == "UVC is null")
            {
                return new InvalidInputException("trim", "Invalid Value(s)", "Could not find vehicle for TrimId = " + trimId);
            }
            return ex;
        }
    }
}
