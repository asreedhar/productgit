﻿using System;
using FirstLook.AmazonCloudSearchService.DomainModel.Utility;

namespace FirstLook.AmazonCloudSearchService.DomainModel.Exceptions
{
    public class DupApprOrInvException : Exception
    {
        public DupApprOrInvException(string message)
            : base(message)
        {
            this.SetCode((int)ErrorCodeEnumeration.AppraisalOrInventoryExists);
        }
    }
}
