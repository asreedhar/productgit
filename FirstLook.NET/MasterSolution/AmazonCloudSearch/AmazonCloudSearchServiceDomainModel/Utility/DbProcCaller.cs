using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace FirstLook.AmazonCloudSearchService.DomainModel.Utility
{
    public class DbProcCaller
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public IDbConnection Connection = null;
        public IDataReader Reader = null;

        ~DbProcCaller()
        {
            if ((Reader != null) && (Reader.IsClosed == false))
            {
                Reader.Close();
            }

            if ((Connection != null) && (Connection.State == ConnectionState.Open))
            {
                Connection.Close();
            }
        }

        /**
         * Please call close after you are done (in a finally clause)
         */
        public void CallProc(string dbName, string procName, Dictionary<string, object> parameters)
        {
            Connection = ProfiledDbConnectionFactory.GetConnection(ConfigurationManager.ConnectionStrings[dbName].ConnectionString);
            var command = Connection.CreateCommand();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = procName;

            foreach (string key in parameters.Keys)
            {
                command.Parameters.AddWithValue(command, key, parameters[key]);
            }

            try
            {
                Connection.Open();
                Reader = command.ExecuteReader();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                throw;
            }
        }

        public void Close()
        {
            if ((Reader != null) && (Reader.IsClosed == false))
            {
                Reader.Close();
            }

            if ((Connection != null) && (Connection.State == ConnectionState.Open))
            {
                Connection.Close();
            }

            Reader = null;
            Connection = null;
        }
    }
}