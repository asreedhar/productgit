﻿using System.Data;

namespace FirstLook.AmazonCloudSearchService.DomainModel.Utility
{
    public static class DbCommandExtensions
    {
        public static IDbDataParameter CreateWithValue(this IDbCommand cmd, string name, object value)
        {
            var param = cmd.CreateParameter();
            param.ParameterName = name;
            param.Value = value;
            return param;
        }

        public static void AddWithValue(this IDataParameterCollection c, IDbCommand cmd, string name, object value)
        {
            var param = cmd.CreateWithValue(name, value);
            cmd.Parameters.Add(param);
        }
    }
}
