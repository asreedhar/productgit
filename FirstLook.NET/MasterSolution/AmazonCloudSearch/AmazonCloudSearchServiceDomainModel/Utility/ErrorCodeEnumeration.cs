namespace FirstLook.AmazonCloudSearchService.DomainModel.Utility
{
    public enum ErrorCodeEnumeration
    {
        UnknownError = 0,
        MissingInput = 1,
        InvalidInput = 2,
        DealerHasNoAccessToValuation = 3,
        NotAuthToOrderReport = 4,
        ErrorRetrievingReport = 5,
        ReportingServiceDown = 6, 
        ReportingServiceHasNoReportForVin = 7,
        BookValuesChecksumDontMatch = 8,
        InvalidCommandConfig = 9,
        ReplayEquipActionsDontMatch = 10,
        NoError = 11,
        NoAuctionData = 12,
        UserAccessDeniedToResource = 13,
        BookMappingYearNotFound = 14,
        NoDataFound = 15,
        AppraisalOrInventoryExists = 16,
        GeneralFirstlookError = 17,
        NoMobileBookFound = 18
    }

}