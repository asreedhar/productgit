﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.AmazonCloudSearchService.DomainModel.AmazonCloudSearchListing.Commands.TransferObjects.Envelopes;
using FirstLook.AmazonCloudSearchService.DomainModel.AmazonCloudSearchListing.Model;

namespace FirstLook.AmazonCloudSearchService.DomainModel.AmazonCloudSearchListing.Commands
{
    public static class Mapper
    {

        public static VehicleSearchListingResultDto Map(SearchResponse data,SearchResponse analyticsData)
        {
            System.Diagnostics.Debug.Write(DateTime.Now);
            List<VehicleListingResultDto> customers = data.Hits.Hit.Select(p => new VehicleListingResultDto()
            {

                AdvertiserAddressCity = p.Field.AdvertiserAddressCity,
                AdvertiserAddressCountry = p.Field.AdvertiserAddressCountry,
                AdvertiserAddressCounty = p.Field.AdvertiserAddressCounty,
                AdvertiserAddressLine1 = p.Field.AdvertiserAddressLine1,
                AdvertiserAddressLocation = p.Field.AdvertiserAddressLocation,
                AdvertiserAddressPostalcode = p.Field.AdvertiserAddressPostalcode,
                AdvertiserAddressState = p.Field.AdvertiserAddressState,
                AdvertiserName = p.Field.AdvertiserName,
                BodyStyle = p.Field.BodyStyle,
                CandidateChromecStyles = p.Field.CandidateChromecStyles.Select(s => Convert.ToInt32(s)).ToList(),
                //DatePublished=Convert.ToDateTime(p.Field.DatePublished),
                DrivetrainDescription = p.Field.DrivetrainDescription,
                DrivetrainType = p.Field.DrivetrainType,
                EngineCylinderCount = Convert.ToInt32(p.Field.EngineCylinderCount),
                EngineCylinderLayout = p.Field.EngineCylinderLayout,
                EngineDescription = p.Field.EngineDescription,
                EngineDisplacementInliters = Convert.ToDouble(p.Field.EngineDisplacementInliters),
                EngineFuelType = p.Field.EngineFuelType,
                ExteriorItems = p.Field.ExteriorItems,
                ExteriorOneColorGenericName = p.Field.ExteriorOneColorGenericName,
                ExteriorOneColorManufacturerName = p.Field.ExteriorOneColorManufacturerName,
                ExteriorOneColorRgbHex = p.Field.ExteriorOneColorRgbHex,
                FuelEconomyCity = Convert.ToDouble(p.Field.FuelEconomyCity),
                FuelEconomyHighway = Convert.ToDouble(p.Field.FuelEconomyHighway),
                InteriorColorManufacturerName = p.Field.InteriorColorManufacturerName,
                InteriorItems = p.Field.InteriorItems,
                IsPublicationOnline = Convert.ToInt32(p.Field.IsPublicationOnline) == 1,
                Make = p.Field.Make,
                Manufacturer = p.Field.Manufacturer,
                Model = p.Field.Model,
                ModelYear = Convert.ToInt32(p.Field.ModelYear),
                OdometerReading = Convert.ToInt32(p.Field.OdometerReading),
                PassengerCapacity = Convert.ToInt32(p.Field.PassengerCapacity),
                PassengerDoors = Convert.ToInt32(p.Field.PassengerDoors),
                Price = Convert.ToDouble(p.Field.Price),
                SafetyItems = p.Field.SafetyItems,
                StockNumber = p.Field.StockNumber,
                Style = p.Field.Style,
                TechnologyItems = p.Field.TechnologyItems,
                TransmissionDescription = p.Field.TransmissionDescription,
                TransmissionGearSelectionMechanism = p.Field.TransmissionGearSelectionMechanism,
                TransmissionType = p.Field.TransmissionType,
                Trim = p.Field.Trim,
                UnderTheHoodItems = p.Field.UnderTheHoodItems,
                VehicleId = Convert.ToInt32(p.Field.VehicleId),
                Vin = p.Field.Vin
            }).ToList();
            
            return new VehicleSearchListingResultDto()
            {
                TimeTaken = data.Status.Time,
                Cursor = data.Hits.Cursor,
                RecordsCount = data.Hits.Found,
                FacetInfo = data.Facets,
                Listings = customers,
                MaxMileage = analyticsData.Hits.Hit.DefaultIfEmpty().Max(vl => vl == null ? 0 : Convert.ToInt32(vl.Field.OdometerReading)),
                MinMileage = analyticsData.Hits.Hit.DefaultIfEmpty().Max(vl => vl == null ? 0 : Convert.ToInt32(vl.Field.OdometerReading)),
                AvgMileage = (float)analyticsData.Hits.Hit.DefaultIfEmpty().Average(vl => vl == null ? 0 : Convert.ToInt32(vl.Field.OdometerReading)),
                MinPrice = analyticsData.Hits.Hit.DefaultIfEmpty().Min(vl => vl == null ? 0 : Convert.ToDouble(vl.Field.Price)),
                MaxPrice = analyticsData.Hits.Hit.DefaultIfEmpty().Max(vl => vl == null ? 0 : Convert.ToDouble(vl.Field.Price)),
                AvgPrice = analyticsData.Hits.Hit.DefaultIfEmpty().Average(vl => vl == null ? 0 : Convert.ToDouble(vl.Field.Price)),
            };
        }
    }
}
