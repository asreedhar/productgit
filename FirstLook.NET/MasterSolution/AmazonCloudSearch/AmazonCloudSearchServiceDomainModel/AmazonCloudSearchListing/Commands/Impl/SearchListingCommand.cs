﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using FirstLook.AmazonCloudSearchService.DomainModel.AmazonCloudSearchListing.Commands.TransferObjects.Envelopes;
using FirstLook.AmazonCloudSearchService.DomainModel.Exceptions;
using FirstLook.CloudSearch.DomainModel.Search.Commands.TransferObjects;
using FirstLook.CloudSearch.DomainModel.Search.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using Newtonsoft.Json;

namespace FirstLook.AmazonCloudSearchService.DomainModel.AmazonCloudSearchListing.Commands.Impl
{
    public class SearchListingCommand : ICommand<VehicleSearchListingResultDto, VehicleSearchListingArgumentDto>
    {
       
        public VehicleSearchListingResultDto Execute(VehicleSearchListingArgumentDto parameters)
        {
            var factory = RegistryFactory.GetResolver().Resolve<FirstLook.CloudSearch.DomainModel.Search.Commands.ICommandFactory>();
            var command = factory.CreateCloudSearchCommand();                    

            SearchOperation operation1 = SearchOperation.CreateSearchOperation(() => parameters.ModelYear, parameters.ModelYear, SearchOperator.EqualsTo, SearchOperator.EqualsTo);
            SearchOperation operation2 = SearchOperation.CreateSearchOperation(() => parameters.Make, parameters.Make, SearchOperator.EqualsTo, SearchOperator.EqualsTo);
            SearchOperation operation3 = SearchOperation.CreateSearchOperation(() => parameters.Model, parameters.Model, SearchOperator.EqualsTo, SearchOperator.EqualsTo);
            SearchOperation operation4 = SearchOperation.CreateSearchOperation(() => parameters.ExteriorOneColorGenericName, parameters.ExteriorOneColorGenericName, SearchOperator.EqualsTo, SearchOperator.EqualsTo);
            SearchOperation operation5 = SearchOperation.CreateSearchOperation(() => parameters.Trim, parameters.Trim, SearchOperator.EqualsTo, SearchOperator.EqualsTo);
            SearchOperation operation6 = SearchOperation.CreateSearchOperation(() => parameters.AdvertiserAddressLocation, parameters.MaxDistance, SearchOperator.GreaterThanEqualsTo,radiusSaerch: new RadiusSearch() { SourceLatitude = parameters.UserLat, SourceLongitude = parameters.UserLon });
            SearchOperation operation7= SearchOperation.CreateSearchOperation(() => parameters.StockNumber, parameters.StockNumber, SearchOperator.EqualsTo);


            SearchOperation searchOperationResult = operation1 & operation2 & operation3 & operation4 & operation5 & operation6 & operation7;

            CloudSearchCommandResult cloudSearchCommandResult = command.Execute(new CloudSearchCommandInput() { SearchQuery = searchOperationResult.SearchOperationString, Cursor = parameters.Cursor, Size = parameters.Size, Facet = GetFacetString(parameters.Facets) });
            Model.SearchResponse data = JsonConvert.DeserializeObject<Model.SearchResponse>(cloudSearchCommandResult.JsonData);
                        
            CloudSearchCommandResult cloudSearchCommandResultAnalysisResult = command.Execute(new CloudSearchCommandInput() { SearchQuery = searchOperationResult.SearchOperationString, Cursor = "", Size = data.Hits.Found});
            Model.SearchResponse analyticsData = JsonConvert.DeserializeObject<Model.SearchResponse>(cloudSearchCommandResultAnalysisResult.JsonData);

            return Mapper.Map(data, analyticsData);
            
        }

        private string GetFacetString(Dictionary<string, List<object>> FacetPropertis)
        {
            if (FacetPropertis == null || FacetPropertis.Count == 0)
                return null;

            Faceting facetList = new Faceting();

            foreach (string key in FacetPropertis.Keys)
            {
                Type myType = typeof(VehicleSearchListingArgumentDto);
                // Get the PropertyInfo object by passing the property name.
                MemberInfo[] PropInfo = myType.GetMember(key);
                if (PropInfo == null || PropInfo.Length == 0)
                    throw new InvalidInputException("Facet column not found");

                facetList.AddFacet(new Facet() { FacetName = GetDescription(PropInfo), Constraints = FacetPropertis[key] });
            }

            return facetList.ToString();
        }

        private string GetDescription(MemberInfo[] members)
        {
            object[] attrs = members[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attrs == null || attrs.Length == 0) return null;
            DescriptionAttribute desc = attrs[0] as DescriptionAttribute;
            if (desc == null) return null;
            return desc.Description;
        }
       
    }
}
