﻿
namespace FirstLook.AmazonCloudSearchService.DomainModel.AmazonCloudSearchListing.Commands.Impl
{
    public class CommandFactory : ICommandFactory
    {
        public SearchListingCommand CreateSearchListingCommand()
        {
            return new SearchListingCommand();
        }

    }
}
