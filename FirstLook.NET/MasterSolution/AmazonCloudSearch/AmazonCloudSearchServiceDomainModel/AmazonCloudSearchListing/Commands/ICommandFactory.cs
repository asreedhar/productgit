﻿using FirstLook.AmazonCloudSearchService.DomainModel.AmazonCloudSearchListing.Commands.Impl;

namespace FirstLook.AmazonCloudSearchService.DomainModel.AmazonCloudSearchListing.Commands
{
    public interface ICommandFactory
    {
        SearchListingCommand CreateSearchListingCommand();
    }
}
