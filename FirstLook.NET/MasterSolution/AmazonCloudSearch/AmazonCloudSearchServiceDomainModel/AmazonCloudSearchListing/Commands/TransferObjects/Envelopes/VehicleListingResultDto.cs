﻿using System;
using System.Collections.Generic;

namespace FirstLook.AmazonCloudSearchService.DomainModel.AmazonCloudSearchListing.Commands.TransferObjects.Envelopes
{
    public class VehicleListingResultDto
    {

        public int? VehicleId { get; set; }


        public string Vin { get; set; }


        public string AdvertiserName { get; set; }


        public string AdvertiserAddressLine1 { get; set; }


        public string AdvertiserAddressCity { get; set; }


        public string AdvertiserAddressCounty { get; set; }


        public string AdvertiserAddressState { get; set; }


        public string AdvertiserAddressPostalcode { get; set; }


        public string AdvertiserAddressCountry { get; set; }


        public string AdvertiserAddressLocation { get; set; }


        public string StockNumber { get; set; }


        public double? Price { get; set; }


        public int? OdometerReading { get; set; }


        public List<int> CandidateChromecStyles { get; set; }


        public string Manufacturer { get; set; }


        public string Make { get; set; }


        public string Model { get; set; }


        public int? ModelYear { get; set; }


        public string Trim { get; set; }


        public string Style { get; set; }


        public string BodyStyle { get; set; }


        public string TransmissionDescription { get; set; }


        public string TransmissionType { get; set; }


        public List<string> TransmissionGearSelectionMechanism { get; set; }


        public string EngineDescription { get; set; }


        public List<string> EngineFuelType { get; set; }


        public int? EngineCylinderCount { get; set; }


        public string EngineCylinderLayout { get; set; }


        public double? EngineDisplacementInliters { get; set; }


        public string DrivetrainDescription { get; set; }


        public string DrivetrainType { get; set; }


        public string ExteriorOneColorManufacturerName { get; set; }


        public string ExteriorOneColorGenericName { get; set; }


        public string ExteriorOneColorRgbHex { get; set; }


        public string InteriorColorManufacturerName { get; set; }


        public double? FuelEconomyCity { get; set; }


        public double? FuelEconomyHighway { get; set; }


        public int? PassengerCapacity { get; set; }


        public int? PassengerDoors { get; set; }


        public List<string> InteriorItems { get; set; }


        public List<string> ExteriorItems { get; set; }


        public List<string> SafetyItems { get; set; }


        public List<string> TechnologyItems { get; set; }


        public List<string> UnderTheHoodItems { get; set; }


        public bool? IsPublicationOnline { get; set; }


        public DateTime? DatePublished { get; set; }
    }
}
