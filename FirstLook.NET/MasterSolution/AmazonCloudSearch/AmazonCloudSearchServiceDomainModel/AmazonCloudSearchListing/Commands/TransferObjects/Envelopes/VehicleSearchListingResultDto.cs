﻿using System.Collections.Generic;
using FirstLook.AmazonCloudSearchService.DomainModel.AmazonCloudSearchListing.Model;


namespace FirstLook.AmazonCloudSearchService.DomainModel.AmazonCloudSearchListing.Commands.TransferObjects.Envelopes
{
    public class VehicleSearchListingResultDto
    {
        public List<VehicleListingResultDto> Listings { get; set; }
        public long TimeTaken { get; set; }
        public Dictionary<string, FacetBuckets> FacetInfo { get; set; }
        public string Cursor { get; set; }
        public long RecordsCount { get; set; }
        public double? MinPrice { get; set; }
        public double? MaxPrice { get; set; }
        public int? MinMileage { get; set; }
        public int? MaxMileage { get; set; }
        public double AvgPrice { get; set; }
        public float AvgMileage { get; set; }
    }
}
