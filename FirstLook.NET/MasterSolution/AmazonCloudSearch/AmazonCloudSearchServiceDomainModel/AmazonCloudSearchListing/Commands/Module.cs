﻿using FirstLook.AmazonCloudSearchService.DomainModel.AmazonCloudSearchListing.Commands.Impl;
using FirstLook.Common.Core.Registry;

namespace FirstLook.AmazonCloudSearchService.DomainModel.AmazonCloudSearchListing.Commands

{
    public class Module : IModule
    {
        #region IModule Members

        /// <summary>
        /// Add pertinent interfaces and impelementing classes to the registry.
        /// </summary>
        public void Configure(IRegistry registry)
        {
            registry.Register<ICommandFactory,CommandFactory>(ImplementationScope.Shared);
        }

        #endregion

    }
}
