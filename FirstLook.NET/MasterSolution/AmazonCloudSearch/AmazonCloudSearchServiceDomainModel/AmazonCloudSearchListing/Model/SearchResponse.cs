﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.AmazonCloudSearchService.DomainModel.AmazonCloudSearchListing.Model 
{
    [DataContract]
    public class SearchResponse
    {
        [DataMember(Name = "status")]
        public Status Status { get; set; }

        [DataMember(Name = "hits")]
        public Hits Hits { get; set; }

        [DataMember(Name = "facets")]
        public Dictionary<string, FacetBuckets> Facets { get; set; }
    }

    [DataContract]
    public class FacetBuckets
    {
        [DataMember(Name = "buckets")]
        public List<Buckets> buckets { get; set; }

       
    }
}
