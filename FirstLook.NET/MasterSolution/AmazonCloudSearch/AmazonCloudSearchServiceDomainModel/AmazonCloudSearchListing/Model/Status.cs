﻿using System.Runtime.Serialization;

namespace FirstLook.AmazonCloudSearchService.DomainModel.AmazonCloudSearchListing.Model 
{
    [DataContract]
    public class Status
    {
        [DataMember(Name = "rid", EmitDefaultValue = false)] 
        public string Rid { get; set; }

        [DataMember(Name = "time-ms", EmitDefaultValue = false)]
        public int Time { get; set; }
    }
}
