﻿using System.Runtime.Serialization;

namespace FirstLook.AmazonCloudSearchService.DomainModel.AmazonCloudSearchListing.Model
{
    [DataContract]
    public class Buckets
    {
        [DataMember(Name = "value")]
        public string Value { get; set; }

        [DataMember(Name = "count")]
        public long Count { get; set; }
    }
}
