﻿using System.Runtime.Serialization;

namespace FirstLook.AmazonCloudSearchService.DomainModel.AmazonCloudSearchListing.Model 
    
{
    [DataContract]
   public class Hit
    {
       [DataMember(Name = "id", EmitDefaultValue = false)] 
       public string Id { get; set; }

       [DataMember(Name = "fields", EmitDefaultValue = false)] 
       public Fields Field{ get; set; }
    }
}
