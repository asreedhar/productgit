﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.AmazonCloudSearchService.DomainModel.AmazonCloudSearchListing.Model
{
    [DataContract]
    public class Fields
    {
        [DataMember(Name = "vehicleid", EmitDefaultValue = false)]
        public string VehicleId { get; set; }

        [DataMember(Name = "vin", EmitDefaultValue = false)]
        public string Vin { get; set; }

        [DataMember(Name = "advertiser_name", EmitDefaultValue = false)]
        public string AdvertiserName { get; set; }

        [DataMember(Name = "advertiser_address_line1", EmitDefaultValue = false)]
        public string AdvertiserAddressLine1 { get; set; }

        [DataMember(Name = "advertiser_address_line2", EmitDefaultValue = false)]
        public string AdvertiserAddressLine2 { get; set; }
        
        [DataMember(Name = "advertiser_address_city", EmitDefaultValue = false)]        
        public string AdvertiserAddressCity { get; set; }

        [DataMember(Name = "advertiser_address_county", EmitDefaultValue = false)]        
        public string AdvertiserAddressCounty { get; set; }

        [DataMember(Name = "advertiser_address_state", EmitDefaultValue = false)]
        public string AdvertiserAddressState { get; set; }

        [DataMember(Name = "advertiser_address_postalcode", EmitDefaultValue = false)]
        public string AdvertiserAddressPostalcode { get; set; }

        [DataMember(Name = "advertiser_address_country", EmitDefaultValue = false)]
        public string AdvertiserAddressCountry { get; set; }

        [DataMember(Name = "advertiser_address_location", EmitDefaultValue = false)]
        public string AdvertiserAddressLocation { get; set; }

        [DataMember(Name = "advertiser_address_email", EmitDefaultValue = false)]
        public string AdvertiserAddressEmail { get; set; }

        [DataMember(Name = "advertiser_address_fax", EmitDefaultValue = false)]
        public string AdvertiserAddressFax { get; set; }

        [DataMember(Name = "advertiser_address_phone", EmitDefaultValue = false)]
        public string AdvertiserAddressPhone { get; set; }

        [DataMember(Name = "advertiser_address_website", EmitDefaultValue = false)]
        public string AdvertiserAddressWebsite { get; set; }

        [DataMember(Name = "air_compressor", EmitDefaultValue = false)]
        public string AirCompressor { get; set; }
        

        [DataMember(Name = "stock_number", EmitDefaultValue = false)]
        public string StockNumber { get; set; }

        [DataMember(Name = "price", EmitDefaultValue = false)]
        public string Price { get; set; }

        [DataMember(Name = "odometer_reading", EmitDefaultValue = false)]
        public string OdometerReading { get; set; }

        [DataMember(Name = "candidate_chromec_styles")]
        public List<string> CandidateChromecStyles { get; set; }

        [DataMember(Name = "manufacturer")]
        public string Manufacturer { get; set; }

        [DataMember(Name = "make")]
        public string Make { get; set; }

        [DataMember(Name = "model")]
        public string Model { get; set; }

        [DataMember(Name = "model_year")]
        public string ModelYear { get; set; }//Expression<Func<T>> 

        [DataMember(Name = "trim")]
        public string Trim { get; set; }

        [DataMember(Name = "style")]
        public string Style { get; set; }

        [DataMember(Name = "body_style")]
        public string BodyStyle { get; set; }

        [DataMember(Name = "transmission_description")]
        public string TransmissionDescription { get; set; }

        [DataMember(Name = "transmission_type")]
        public string TransmissionType { get; set; }

        [DataMember(Name = "transmission_gear_selection_mechanism")]
        public List<string> TransmissionGearSelectionMechanism { get; set; }

        [DataMember(Name = "engine_description")]
        public string EngineDescription { get; set; }

        [DataMember(Name = "engine_fuel_type")]
        public List<string> EngineFuelType { get; set; }

        [DataMember(Name = "engine_cylinder_count")]
        public string EngineCylinderCount { get; set; }

        [DataMember(Name = "engine_cylinder_layout")]
        public string EngineCylinderLayout { get; set; }

        [DataMember(Name = "engine_displacement_inliters")]
        public string EngineDisplacementInliters { get; set; }

        [DataMember(Name = "drivetrain_description")]
        public string DrivetrainDescription { get; set; }

        [DataMember(Name = "drivetrain_type")]
        public string DrivetrainType { get; set; }

        [DataMember(Name = "exterior_one_color_manufacturer_name")]
        public string ExteriorOneColorManufacturerName { get; set; }

        [DataMember(Name = "exterior_one_color_generic_name")]
        public string ExteriorOneColorGenericName { get; set; }

        [DataMember(Name = "exterior_one_color_rgb_hex")]
        public string ExteriorOneColorRgbHex { get; set; }

        [DataMember(Name = "interior_color_manufacturer_name")]
        public string InteriorColorManufacturerName { get; set; }

        [DataMember(Name = "fuel_economy_city")]
        public string FuelEconomyCity { get; set; }

        [DataMember(Name = "fuel_economy_highway")]
        public string FuelEconomyHighway { get; set; }

        [DataMember(Name = "passenger_capacity")]
        public string PassengerCapacity { get; set; }

        [DataMember(Name = "passenger_doors")]
        public string PassengerDoors { get; set; }

        [DataMember(Name = "interior_items")]
        public List<string> InteriorItems { get; set; }

        [DataMember(Name = "exterior_items")]
        public List<string> ExteriorItems { get; set; }

        [DataMember(Name = "safety_items")]
        public List<string> SafetyItems { get; set; }

        [DataMember(Name = "technology_items")]
        public List<string> TechnologyItems { get; set; }

        [DataMember(Name = "under_the_hood_items")]
        public List<string> UnderTheHoodItems { get; set; }

        [DataMember(Name = "is_publication_online")]
        public string IsPublicationOnline { get; set; }

        [DataMember(Name = "date_published")]
        public string DatePublished { get; set; }

        [DataMember(Name="drivetrain_axle")]
        public string DrivetrainAxle { get; set; }

        [DataMember(Name = "engine_displacement_cubic_inches")]
        public string EngineDisplacementCubicInches { get; set; }

        [DataMember(Name = "exterior_two_color_generic_name")]
        public string ExteriorTwoColorGenericName { get; set; }

        [DataMember(Name = "exterior_two_color_manufacturer_name")]
        public string ExteriorTwoColorManufacturerName { get; set; }

        [DataMember(Name = "exterior_two_color_rgb_hex")]
        public string ExteriorTwoColorRgbHex { get; set; }

        [DataMember(Name = "fuel_economy_combined")]
        public string FuelEconomyCombined { get; set; }

        [DataMember(Name = "interior_color_rgb_hex")]
        public string InteriorColorRgbHex { get; set; }

        [DataMember(Name = "market_class")]
        public string MarketClass { get; set; }

        [DataMember(Name = "stock_type")]
        public string StockType { get; set; }
        
        [DataMember(Name = "transmission_gear_count")]
        public string TransmissionGearCount { get; set; }
        

        
    }
}
