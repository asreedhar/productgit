$(document).ready(function () {
	
    var
	fl = {},
	reqs,
	silentLogging = true//true if you prefer console logs over alerts
    ; //end local vars
    
    //console.log handling
    fl.log = function(msg){
	if(silentLogging){ console.log(msg); }
	else { alert(msg); }
    };
    
    $('ul.service_testers a').click( function(e){
	reqs[ this.getAttribute('data-method') ]();
    } )
    
    reqs = new RequestObject()
    
    //constructor - uses event delegation to rout form input changes into one event for the request object to listen for
    function FormChangeHandler(formId){
	    var $_formObj = $('#'+formId),
	    topThis = this;
	    
	    $_formObj.on('change','select, input, textarea',  function(e){
		$(topThis).trigger( {
			type:'formChange',
			inputId: e.target.id,
			inputValue: e.target.value
		} );
	    } );
	    
	    //initialize values stored by triggering 'fake' change event
	    this.initVals = function(){
		$('select, input, textarea', $_formObj).change();
	    }
    }
    
    //constructor
    function RequestObject() {

	var
		formObj,
		inputs = {
			dealersVal:null,
			mileageVal:null,
			vinVal:null,
			booksVal:null
		}
	;//end internal var decs
	
	formObj = new FormChangeHandler('WebServiceTest');
	
	//inputs obj will init values when formObj initializes
	$(formObj).on('formChange', function(e){
		var propName = e.inputId + 'Val';
		inputs[propName] = e.inputValue;
	} );
	
	
		
	//Public methods (defs hoisted from below) - Done this way for convenience of having an API menu
	this.dealersGET = dealersGET;
	this.dealersBooksGET = dealersBooksGET;
	this.booksGET = booksGET;
	this.searchtypesGET = searchtypesGET;
	this.auctionreferenceGET = auctionreferenceGET;
	this.distancesGET = distancesGET;
	this.naaaAuctionGET = naaaAuctionGET;
	this.autocheckreportPOST = autocheckreportPOST;
	this.carfaxreportPOST = carfaxreportPOST;
	this.autocheckreportGET = autocheckreportGET;
	this.carfaxreportGET = carfaxreportGET;
	this.carfaxreportGET = carfaxreportGET;
	this.listingsGET = listingsGET;
	this.hierarchyGET = hierarchyGET;
	this.valuationsEquipmentActionsGET = valuationsEquipmentActionsGET;
	this.appraisalsPOST = appraisalsPOST;
	this.bookValuationsPOST = bookValuationsPOST;
	
	
	//internal  methods
	
	
	
	function validate(){
		var len = arguments.length,
		hasError = false,
		inputsAreValid = true,
		inputFails = ['No value  has been set for form inputs with the following IDs:'];
		
		for(var i = 0; i < len; i++){
			var
				inputId = arguments[i],
				thisInput = inputs[ inputId + 'Val' ]
			;
			
			if(thisInput !== undefined){
				if(!thisInput){
					inputsAreValid = false;
					inputFails.push(inputId);
				}
			}
			else {
				var errorMessage = 'Request Object validate function error: There appears to be no form input with the id \"' + inputId + '\"';
				
				inputsAreValid = false;
				hasError = true;
				
				throw new Error(errorMessage);
				break;
			}
			
		}
		
		if(hasError) {
			fl.log( errorMessage );
		}
		else if(!inputsAreValid){
			fl.log( inputFails.join('\n') );
		}
		
		return inputsAreValid;
	}

	function getRequest(url, ajaxOpts) {
	    var defaultObj = {
		type: 'GET',
		dataType: 'json',
		error: function (xhr, status, err) { throw new Error('RequestObject Error:\n\n' + err); }
	    };
	    if(!ajaxOpts){ ajaxOpts = {} }
	    if(!ajaxOpts.url){
		ajaxOpts.url=url;
	    }
	    $.ajax( $.extend(defaultObj, ajaxOpts) );
	}
	
	function postRequest(url, ajaxOpts){
		
	        if(!ajaxOpts){ ajaxOpts = {} }
		ajaxOpts.type = 'POST';
		getRequest(url, ajaxOpts);
	}
	
	//End internal methods
	
	//public method definitions
	
	function dealersGET() {

	    var url = "dealers";

	    getRequest(url, {
		success: function (data) {
			var optionsArr = [];
			
			$("#resultBox").val(JSON.stringify(data));

			var $_dealers = $('#dealers');
			
			$_dealers.empty();

			$.each(data.dealers, function (index, value) {
				optionsArr.push(
					'<option value="'+value.dealer+'">'+value.desc+'</option>'
				);
			} );
			
			$_dealers.html(optionsArr.join(''));
			
			formObj.initVals(); //resets the initVals
		}
	    } );
	}

	function dealersBooksGET() {

	    var url = "books/dealers/";

	    url = url + inputs.dealersVal;

	    getRequest(url, {
		success: function (data) {
		    $("#resultBox").val(JSON.stringify(data));

		    $('#dealersbooks option').each(function (index, option) {
			$(option).remove();
		    });

		    var dealersbooks = $('#dealersbooks');

		    $.each(data.books, function (index, value) {
			dealersbooks.append(
			    $('<option></option>').val(value).html(value)
			);
		    });
		}
	    } );
	}


	function booksGET() {

	    var url = "books";

	    getRequest( url, {
		success: function (data) {
		    $("#resultBox").val(JSON.stringify(data));

		    $('#books option').each(function (index, option) {
			$(option).remove();
		    });

		    var books = $('#books');

		    $.each(data.books, function (index, value) {
			books.append(
			$('<option></option>').val(value.book).html(value.desc)
		    );
		    });

		}
	    } );
	}

	function searchtypesGET() {

	    var url = "listings/searchTypes";

	    getRequest( url, {
		success: function (data) {
		    $("#resultBox").val(JSON.stringify(data));

		    $('#searchtypes option').each(function (index, option) {
			$(option).remove();
		    });

		    var searchtypes = $('#searchtypes');

		    $.each(data.listings, function (index, value) {
			searchtypes.append(
			$('<option></option>').val(value).html(value)
		    );
		    });

		}
	    });
	}

	function auctionreferenceGET() {

	    var url = "auctions/naaa/reference";

	    getRequest( url, {
		success: function (data) {
		    $("#resultBox").val(JSON.stringify(data));

		    $('#areas option').each(function (index, option) {
			$(option).remove();
		    });

		    $('#periods option').each(function (index, option) {
			$(option).remove();
		    });

		    var areas = $('#areas');

		    var periods = $('#periods');

		    $.each(data.areas, function (key, value) {
			areas.append(
			$('<option></option>').val(value.id).html(value.name)
		    );
		    });

		    $.each(data.periods, function (key, value) {
			periods.append(
			$('<option></option>').val(value.id).html(value.name)
		    );
		    });

		}
	    });
	}

	function distancesGET() {

	    var url = "listings/distances";

	    getRequest( url, {
		success: function (data) {
		    $("#resultBox").val(JSON.stringify(data));

		    $('#distances option').each(function (index, option) {
			$(option).remove();
		    });

		    var distances = $('#distances');

		    $.each(data.distances, function (index, value) {
			distances.append(
			$('<option></option>').val(value).html(value)
		    );
		    });
		}
	    });
	}

	function naaaAuctionGET() {

	    //This needs reworking
	    var queryString = ['?'];

	    if (inputs.areasVal) {
		queryString.push("area=" + inputs.areasVal);
	    }

	    if (inputs.periodsVal) {
		queryString.push("period=" + inputs.periodsVal);
	    }

	    if (inputs.mileage) {
		queryString.push("mileage=" + inputs.mileage);
	    }
	    
	    queryString.join('&');

	    if(validate('vin') === false){ return false; }

	    var url = "auctions/naaa/vins/" + inputs.vinVal + queryString;

	    getRequest( url, {
		success: function (data) {
		    $("#resultBox").val(JSON.stringify(data));
		}
	    });
	}

	function autocheckreportPOST() {
	    
	    if( !validate('vin','dealers') ){ return false; }

	    var url = "reports/autocheck/dealers/" + inputs.dealersVal + "/vins/" + inputs.vinVal;

	    postRequest( url, {
		success: function (data) {
		    $("#resultBox").val(JSON.stringify(data));
		}
	    });
	}

	function carfaxreportPOST() {

	    if( !validate('vin','dealers') ){ return false; }

	    var url = "reports/carfax/dealers/" + inputs.dealersVal + "/vins/" + inputs.vinVal;

	    postRequest( url, {
		success: function (data) {
		    $("#resultBox").val(JSON.stringify(data));
		}
	    } );
	}

	function autocheckreportGET() {
		
	    if( !validate('vin','dealers') ){ return false; }

	    var url = "reports/autocheck/dealers/" + inputs.dealersVal + "/vins/" + inputs.vinVal;

	    getRequest( url, {
		success: function (data) {
		    $("#resultBox").val(JSON.stringify(data));
		}
	    });
	}

	function carfaxreportGET() {
	    
	    if( !validate('vin','dealers') ){ return false; }

	    var url = "reports/carfax/dealers/" + inputs.dealersVal + "/vins/" + inputs.vinVal;

	    $.ajax({
		success: function (data) {
		    $("#resultBox").val(JSON.stringify(data));
		}
	    });
	}

	function listingsGET() {
	
	    if( !validate('vin','dealers') ){ return false; }

	    var url = "listings/dealers/" + inputs.dealersVal + "/vins/" + inputs.vinVal;

	    getRequest( url, {
		success: function (data) {
		    $("#resultBox").val(JSON.stringify(data));
		}
	    });
	}

	function hierarchyGET() {

	    var url = "vehicles/hierarchy";

	    getRequest( url, {
		success: function (data) {
		    $("#resultBox").val(JSON.stringify(data));
		}
	    });
	}

	function valuationsEquipmentActionsGET() {

	    var url = "valuations/equipmentActions";

	    getRequest( url, {
		success: function (data) {
		    $("#resultBox").val(JSON.stringify(data));
		}
	    } );
	}

	function appraisalsPOST() {
		
	    if( validate('vin','dealers') === false ){ return false; }

	    var appraisalRequest = {
		"appraisalAmount": 10000,
		"appraisalType": "T",
		"color": "red",
		"targetGrossProfit": 500,
		"estimatedRecon": 250,
		"targetSellingPrice": 10750,
		"bookValuationRequests": [
			{
			    "request": {
				"book": "kbb",
				"mileage": 1234,
				"trimId": 100,
				"returnId": "0c9a3e30-6cbd-11e2-bcfd-0800200c9a66",
				"equipmentSelected": [1, 2, 3],
				"equipmentExplicitActions": [
					{ "action": "R", "equipment": 5 },
					{ "action": "A", "equipment": 2 },
					{ "action": "R", "equipment": 6 }
				    ]
			    },
			    "checkSum": "abc123"
			}
		]
	    };

	    var url = "appraisals/dealers/" + inputs.dealersVal + "/vins/" + inputs.vinVal;


	    postRequest( url, {
		data: JSON.stringify(appraisalRequest),
		success: function (data) {
		    $("#resultBox").val(JSON.stringify(data));
		}
	    } );
	    
	}

	function bookValuationsPOST() {

	    var url = '/valuations/dealers/' + inputs.dealersVal

	    postRequest( url, {
		data: {
		    bookValuationRequests: [
			{
			    book: 'kbb',
			    mileage: 10000,
			    trimId: 12,
			    returnId: 'a12b',
			    equipmentSelected: [
					5,
					7
				],
			    equipmentExplicitActions: [
					{
					    action: 'setThing',
					    equipment: 5
					},
					{
					    action: 'setOtherThing',
					    equipment: 6
					}
				]
			}
		    ]
		},
		success:function(data){
			console.log('Success Data: \n\n' + JSON.stringify(data));
		}
	    } );
	}

    }

});//end doc ready