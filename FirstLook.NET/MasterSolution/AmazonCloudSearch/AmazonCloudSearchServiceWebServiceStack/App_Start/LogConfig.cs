﻿namespace FirstLook.AmazonCloudSearchService.WebServiceStack.App_Start
{
    internal static class LogConfig
    {
        public static void RegisterLogging()
        {
            log4net.Config.XmlConfigurator.Configure();
        }
    }
}