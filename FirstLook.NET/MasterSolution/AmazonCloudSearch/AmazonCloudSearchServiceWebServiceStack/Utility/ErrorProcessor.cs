﻿using System;
using System.Collections.Generic;
using FirstLook.AmazonCloudSearchService.DomainModel.Exceptions;
using ServiceStack.ServiceHost;
using ServiceStack.Common.Web;
using System.Net;
using System.Data.SqlClient;
//using FirstLook.VehicleHistoryReport.DomainModel.Carfax;
using FirstLook.AmazonCloudSearchService.DomainModel.Utility;
//using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck;
using System.Collections;
namespace FirstLook.AmazonCloudSearchService.WebServiceStack.Utility
{
    class ErrorMgr
    {
        string nl = Environment.NewLine;
        ServiceSettings ss = new ServiceSettings();
        List<Func<Exception, IDictionary<string, object>>> handlers = new List<Func<Exception, IDictionary<string, object>>>();
        Dictionary<Type, Func<Exception, IDictionary<string, object>>> exceptionHandlers = new Dictionary<Type, Func<Exception, IDictionary<string, object>>>();
        bool showStackTrace = false;
        public ErrorMgr()
        {
            Add(typeof(UserAccessException), HandlerUserAccessException);
            Add(typeof(MissingInputException), HandlerMissingInputException);
            Add(typeof(InvalidInputException), HandlerInvalidInputException);
            Add(typeof(NoDataFoundException), HandleNoDataFoundException);
            Add(typeof(DupApprOrInvException), HandleDupApprOrInvException);
            //Add(typeof(CarfaxException), HandlerCarFaxException);
            //Add(typeof(AutoCheckException), HandlerAutoCheckException);
            Add(typeof(FirstlookGeneralException), HandlerFirstlookGeneralException);
            Add(typeof(SqlException), HandlerSQLException);
            Add(typeof(SystemException), DefHandlerFunc);
            Add(typeof(Exception), DefHandlerFunc);// empty key == default
            Add(typeof(NoMobileBookDataFoundException), HandleNoMobileBookDataFoundException);
            showStackTrace = this.Setting("Debug") == "true";   
        }

        /// <summary>
        /// Add a handler for a particular exception type
        /// </summary>
        /// <param name="exceptionType">The Exception</param>
        /// <param name="fConvert">The conversion function</param>
        /// <returns></returns>
        public ErrorMgr Add(Type exceptionType, Func<Exception, IDictionary<string, object>> fConvert)
        {
            exceptionHandlers.Add(exceptionType, fConvert);
            return this;
        }

        public IDictionary<string, object> MissingField(string fldName, string devMsg)
        {
            int errCode = (int)ErrorCodeEnumeration.MissingInput;
            var msg = ss.Setting("err.MissingRequestField");
            msg = (msg == null) ? "" : string.Format(msg, fldName);
            string errDescriptionURL = (errCode > 0) ? "http://api.firstlook.biz/docs/errors/" + errCode : "";
            return new Dictionary<string, object> {
                {   "status", (int)HttpStatusCode.BadRequest },
                {   "code", errCode },
                {   "property", fldName},
                {   "message", msg },
                {   "developerMessage", devMsg},
                {   "moreInfo", errDescriptionURL }
            };
        }// missingField

        public IDictionary<string, object> ExceptionToError(Exception ex, string devMsg)
        {
            var handler = FindHandler(ex);
            if (handler == null)
                handler = FindHandler(null);
            IDictionary<string, object> ret = handler(ex);
            if (!string.IsNullOrEmpty(devMsg)) 
                ret["developerMessage"] = devMsg;
            return ret;
        }


        /**
         * 
         * called when an internal error has occurred to get a user-friendly message that says the system has some internal problem
         * maybe try again later.
         * 
         * */
        string DefaultInternalErrorMessage()
        {
            return ss.Setting("err.DefaultMessage");
        }

        public string GetDefaultErrorMessage()
        {
            return DefaultInternalErrorMessage() ?? this.GetString("err.DefaultMessage");
        }

        Func<Exception, IDictionary<string, object>> FindHandler(Exception ex)
        {
            var exType = ex == null ? typeof(Exception) : ex.GetType();
            var handler = exceptionHandlers.ContainsKey(exType) ? exceptionHandlers[exType] : null;
            return handler;
        }

        /// <summary>
        /// Creates a dev message including relevant parts of the exception
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        string CreateDevMessage(Exception ex)
        {
            var data = dataAsString(ex.Data) + Environment.NewLine;

            return !showStackTrace
                ? string.Format(@"{0}" + nl, ex.Message)
                : string.Format(@"{0}" + nl + nl + "{1}" + nl + nl + @"STACK TRACE:{2}" + nl + nl + @"DATA:{3}" + nl + nl + "@{4}", ex.Message, ex.Source, ex.StackTrace, data, ex.TargetSite.ToString());
        }

        /// <summary>
        /// Creates a dev message including relevant parts of the exception
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        string CreateDevMessage(Exception ex, string devMsg)
        {
            var data = dataAsString(ex.Data) + Environment.NewLine;

            return !showStackTrace
                ? string.Format(@"{0}" + nl + nl + "{1}" + nl, ex.Message, devMsg)
                : string.Format(@"{0}" + nl + nl + "{1}" + nl + nl + "{2}" + nl + nl + @"STACK TRACE:{3}" + nl + nl + @"DATA:{4}" + nl + nl + "@{5}", ex.Message, devMsg, ex.Source, ex.StackTrace, data, ex.TargetSite.ToString());
        }

        private object dataAsString(IDictionary data)
        {          
            string s = "";
            foreach (var k in data.Keys)
                s += k.ToString() + "=" + data[k].ToString();
            return "{"+s +"}";
        }

        /// <summary>
        ///  The default handler for exceptions when none is specified
        /// </summary>
        IDictionary<string, object> DefHandlerFunc(Exception ex)
        {
            var code = ex.Code();
            var msg = this.GetErrorString(ex.Code());            
            return new Dictionary<string, object>{
                    {   "status", 500 },
                    {   "code", code },
                    {   "message",  msg ?? GetDefaultErrorMessage() },
                    {   "developerMessage",  "ExceptionType:" + (ex.GetType().Name.ToString() ) + "" + nl +CreateDevMessage(ex)}
            };
        }

        /// <summary>
        ///  The handler for SQL exceptions
        /// </summary>
        IDictionary<string, object> HandlerSQLException(Exception ex)
        {
            SqlException sqe = (SqlException)ex;
            var code = ex.Data["code"] ?? 0;
            var errMsg = nl+@"Database Error:";
            if (sqe.Server != null)
                errMsg += nl + @"Server: " + sqe.Server;
            if (sqe.Procedure != null)
                errMsg += nl + @"Procedure: " + sqe.Procedure;
            errMsg += nl + @"State: " + sqe.State;
            errMsg += nl + @"ErrorCode: " + sqe.ErrorCode;
            return new Dictionary<string, object>{
                    {   "status", 500 },
                    {   "message",  GetDefaultErrorMessage() },
                    {   "code",                                                     code},
                    {   "developerMessage", (!showStackTrace ? "" : errMsg )+ nl+CreateDevMessage(ex)}
            };
        }

        ///// <summary>
        /////  The handler for CarfaxException exceptions
        ///// </summary>
        //IDictionary<string, object> HandlerCarFaxException(Exception ex)
        //{
        //    var cfe = (CarfaxException)ex;
        //    var status = 400;
        //    var msg = GetDefaultErrorMessage();
        //    var devMsg = "Carfax error: " + cfe.ResponseCode.ToString() + nl  + CreateDevMessage(ex);
        //    var code = ex.Data.Contains("code") ? (int)ex.Data["code"] :  0;
        //    switch (cfe.ResponseCode)
        //    {

        //        case CarfaxResponseCode.Failure_InvalidVin:
        //            code = (int)ErrorCodeEnumeration.InvalidInput;
        //            msg = this.GetErrorString(code, "Vin", "Invalid Value");
        //            break;

        //        case CarfaxResponseCode.Failure_AccountStatus:
        //            devMsg = "Carfax error: " + cfe.ResponseCode.ToString() + "\n" + devMsg;
        //            code = (int)ErrorCodeEnumeration.NotAuthToOrderReport;
        //            msg = this.GetErrorString(code, "Vin");
        //            status = 403;
        //            break;
        //        case CarfaxResponseCode.Failure_BadPassword:
        //            devMsg = "Carfax error: " + cfe.ResponseCode.ToString() + "\n" + devMsg;
        //            code = (int)ErrorCodeEnumeration.NotAuthToOrderReport;
        //            msg = this.GetErrorString(code, "Vin");
        //            status = 403;
        //            break;
        //        case CarfaxResponseCode.Failure_BadUserName:
        //            devMsg = "Carfax error: " + cfe.ResponseCode.ToString() + "\n" + devMsg;
        //            code = (int)ErrorCodeEnumeration.NotAuthToOrderReport;
        //            msg = this.GetErrorString(code, "Vin");
        //            status = 403;
        //            break;
        //        case CarfaxResponseCode.Failure_NoData:
        //            status = 404;
        //            code = (int)ErrorCodeEnumeration.ReportingServiceHasNoReportForVin;
        //            msg = this.GetErrorString(code, "Carfax","");
        //            break;
        //        case CarfaxResponseCode.Failure_TransactionError:
        //            status = 404;
        //            code = 5;
        //            break;
        //        case CarfaxResponseCode.Failure_NoCodeSupplied:
        //            status = 404;
        //            code = 5;
        //            break;
        //        case CarfaxResponseCode.Failure_ServiceUnavailable:
        //            status = 404;
        //            code = 5;
        //            break;
        //        case CarfaxResponseCode.Undefined:
        //            status = 404;
        //            code = 5;
        //            break;
        //    }
        //    return new Dictionary<string, object>{
        //            {   "status", status },
        //            {   "code", code},
        //            {   "message",  msg },
        //            {   "developerMessage", devMsg}
        //    };
        //}


        ///// <summary>
        /////  The handler for AutoCheck exceptions
        ///// </summary>
        //IDictionary<string, object> HandlerAutoCheckException(Exception ex)
        //{
        //    var ace = (AutoCheckException)ex;
        //    var status = 400;
        //    var msg = GetDefaultErrorMessage();
        //    var devMsg = "AutoCheck error: " + ace.ResponseCode.ToString() + 
        //        "\nReport Id:"+ ace.ReportId +
        //        "\n" + CreateDevMessage(ex); 
        //    var code = ex.Data.Contains("code") ? (int)ex.Data["code"] : 0;

        //    switch (ace.ResponseCode)
        //    {
        //        case AutoCheckResponseCode.Undefined:
        //            code = (int)ErrorCodeEnumeration.NotAuthToOrderReport;
        //            msg = this.GetErrorString(
        //                (int)ErrorCodeEnumeration.ReportingServiceDown, 
        //                "AutoCheck", 
        //                "Vin"
        //                );
        //            break;

        //        case AutoCheckResponseCode.Forbidden:
        //            devMsg = "AutoCheck error: " + ace.ResponseCode.ToString() + "\n" + devMsg;
        //            code = (int)ErrorCodeEnumeration.NotAuthToOrderReport;
        //             msg = this.GetErrorString(code, "AutoCheck");
        //           status = 403;
        //            break;

        //        case AutoCheckResponseCode.Success:
        //            status = 200;
        //            msg = "";
        //            code = (int)ErrorCodeEnumeration.NoError;
        //            break;

        //        case AutoCheckResponseCode.Error:
        //            status = 500;
        //            code = (int)ErrorCodeEnumeration.UnknownError;
        //            break;

        //    }
        //    return new Dictionary<string, object>{
        //            {   "status", status },
        //            {   "code", code},
        //            {   "message",  msg },
        //            {   "developerMessage", devMsg}
        //    };
        //}

        /// <summary>
        ///  The handler for MissingInputException exceptions
        /// </summary>
        IDictionary<string, object> HandlerMissingInputException(Exception ex)
        {
            var fmie = (MissingInputException)ex;
            var errMsg = this.GetErrorString((int)ErrorCodeEnumeration.MissingInput, fmie.InputField.ToString());

            return new Dictionary<string, object>{
                {   "status", 400 },
                {   "message",  errMsg },
                {   "code", (int)ErrorCodeEnumeration.MissingInput},
                {   "property", fmie.InputField.ToString()},
                {   "developerMessage", errMsg + "\n" + CreateDevMessage(ex)}
            };
        }

        /// <summary>
        ///  The handler for InvalidInputException exceptions
        /// </summary>
        IDictionary<string, object> HandlerInvalidInputException(Exception ex)
        {
            var fiie = (InvalidInputException)ex;
            var errMsg = this.GetErrorString((int)ErrorCodeEnumeration.InvalidInput, fiie.InputField,  fiie.InputValue);
            return new Dictionary<string, object>{
                {   "status", 400 },
                {   "message",  errMsg },
                 {   "property", fiie.InputField},
                {   "code", (int)ErrorCodeEnumeration.InvalidInput},
                {   "developerMessage", errMsg + nl + CreateDevMessage(ex)}
            };
        }

        /// <summary>
        ///  The handler for UserAccessException exceptions
        /// </summary>
        IDictionary<string, object> HandlerUserAccessException(Exception ex)
        {
            var fiie = (UserAccessException)ex;
            var errMsg = this.GetErrorString((int)ErrorCodeEnumeration.UserAccessDeniedToResource, fiie.Resource.ToString(), fiie.Reason);
            return new Dictionary<string, object>{
                {   "status", 403 },
                {   "message",  errMsg },
                {   "code", (int)ErrorCodeEnumeration.UserAccessDeniedToResource},
                {   "developerMessage",errMsg + nl + CreateDevMessage(ex)}
            };
        }

        /// <summary>
        ///  The handler for NoDataFound exceptions
        /// </summary>
        IDictionary<string, object> HandleNoDataFoundException(Exception ex)
        {
            var code = ex.Code();
            var msg = this.GetErrorString(ex.Code());
            var devMsg = "No Data Found."+ nl + CreateDevMessage(ex);
            var jsonDict = new Dictionary<string, object>{
                    { "status", 404 },
                    { "code", code },
                    { "message",  msg },
                    { "developerMessage", devMsg }
            };
            return jsonDict;
        }
        /// <summary>
        ///  The handler for NoMobileBookDataFound exceptions
        /// </summary>
        IDictionary<string, object> HandleNoMobileBookDataFoundException(Exception ex)
        {
            var code = ex.Code();
            var msg = this.GetErrorString(ex.Code());
            var devMsg = "No Data Found." + nl + CreateDevMessage(ex);
            var jsonDict = new Dictionary<string, object>{
                    { "status", 404 },
                    { "code", code },
                    { "message",  msg },
                    { "developerMessage", devMsg }
            };
            return jsonDict;
        }

        /// <summary>
        ///  The handler for DupAppraisalOrInv exceptions
        /// </summary>
        IDictionary<string, object> HandleDupApprOrInvException(Exception ex)
        {
            var devMsg = CreateDevMessage(ex);
            var jsonDict = new Dictionary<string, object>{
                    { "status", 409 },
                    { "code", ex.Code() },
                    { "message",  ex.Message },
                    { "developerMessage", devMsg }
            };
            return jsonDict;
        }

        /// <summary>
        ///  The handler for Firstlook General exceptions
        /// </summary>
        IDictionary<string, object> HandlerFirstlookGeneralException(Exception ex)
        {
            var devMsg = "";
            if (string.IsNullOrEmpty(ex.DevMsg()))
            {
                devMsg = CreateDevMessage(ex);
            }
            else
            {
                devMsg = CreateDevMessage(ex, ex.DevMsg());
            }

            var jsonDict = new Dictionary<string, object>{
                    { "status", 400 },
                    { "code", ex.Code() },
                    { "message",  ex.Message },
                    { "developerMessage", devMsg }
            };
            return jsonDict;
        }


    }// end ErrorMgr

    public static class ErrorMgrMixin
    {
        static ErrorMgr emgr = new ErrorMgr();

        static public void Add(this object caller, Exception ex, Func<Exception, IDictionary<string, object>> fConvert)
        {
            emgr.Add(ex, fConvert);
        }
        static public IDictionary<string, object> MissingField(this object caller, string fldName, string devMsg)// missingField
        {
            return emgr.MissingField(fldName, devMsg);
        }

        static public IDictionary<string, object> ExceptionToError(this object caller, Exception ex, string devMsg)
        {
            return emgr.ExceptionToError(ex, devMsg);
        }
        /// <summary>
        /// We use the status from the first error in list or 400 if none present
        /// </summary>
        /// <param name="caller">
        /// The target of this mixin.
        /// </param>
        /// <param name="errMsgs">
        /// List of error objects
        /// </param>
        /// 
        /// <returns></returns>
        static public IHttpResult HandleError(this object caller, List<IDictionary<string, object>> errMsgs)
        {
            IDictionary<string, List<IDictionary<string, object>>> outerErrMsg = new Dictionary<string, List<IDictionary<string, object>>>();
            outerErrMsg["errors"] = errMsgs;
            // get status from 1st message
            var status = (errMsgs[0]["status"]);
            if (errMsgs[0]["status"] != null)
                errMsgs[0].Remove("status");
            //clear the null code and null status
            removeStatusAndCode(errMsgs);
            // if its there use it else use 400
            var httpStatus = status == null ? 400 : Convert.ToInt32(status);
            // create an ENUM from our httpStatus
            HttpStatusCode httpCode = (HttpStatusCode)Enum.ToObject(typeof(HttpStatusCode), httpStatus);
            // create result w/ errors messages and statusCode
            IHttpResult result = new HttpResult(outerErrMsg, httpCode);
            return result;
        }

        internal static void removeStatusAndCode(List<IDictionary<string, object>> errMsgs)
        {
            foreach (IDictionary<string, object> errMsg in errMsgs)
            {
                if (errMsg.ContainsKey("code") && errMsg["code"] == null)
                    errMsg.Remove("code");
                errMsg.Remove("status");
            }
        }

        static public IHttpResult HandleError(this object caller, IDictionary<string, object> err)
        {
            List<IDictionary<string, object>> l = new List<IDictionary<string, object>>();
            l.Add(err);
            return HandleError(caller, l);
        }
    }
}