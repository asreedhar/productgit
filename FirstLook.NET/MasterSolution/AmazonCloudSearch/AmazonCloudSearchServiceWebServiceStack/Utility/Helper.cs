﻿using System.Linq;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using System.Configuration;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text.RegularExpressions;

namespace FirstLook.AmazonCloudSearchService.WebServiceStack.Utility
{
    public class Helper
    {
        public static IdentityContextDto<T> WrapInContext<T>(T arguments, string userName)
        {
            return new IdentityContextDto<T>
            {
                Arguments = arguments,
                Identity = new IdentityDto
                {
                    AuthorityName = "CAS",
                    Name = userName
                }
            };
        }

        //Pulled this out here so we can change the compression algorithm acrossed the board without hitting every service.
        public static IHttpResult Compress<T>(ServiceStack.ServiceInterface.Service service, T result) where T : class
        {
            if (service.RequestContext.CompressionType != null)
            {
                return (IHttpResult)service.RequestContext.ToOptimizedResult(result);
            }

            //if no compression type is supported just return the uncompressed result.
            return new HttpResult
                       {
                           Response = result
                       };
        }


        
        //Does two things. Gives us one place to change header messages across the services. And pulls in values we might want to change at run-time from web.config
        //usage: Helper.setHeadersFromConf(result, string[]{ "cacheControlHour","My-Unique-Header:Yay!" } );
        //Note that you can reference keys or just drop in literal header values (literals determined by presence of a colon)
        public static IHttpResult SetStaticHeaders(IHttpResult resultObj, string[] headerKeys)
        {
            var pattern = new Regex(@"{{(?<keyMatch>[^}{]*)}}");

            var headerContent = new Dictionary<string, string>
            {
                //note: colons in keys would be bad because that's how it sniffs out literal header values
				{ "cacheControlMin","Cache-Control:max-age={{cacheControlMinimumSecs}}, must-revalidate" }, //anything less than 3600
                { "cacheControlHour","Cache-Control:max-age={{cacheControlHourSecs}}, must-revalidate" }, //3600
                { "cacheControlDay","Cache-Control:max-age={{cacheControlDaySecs}},  must-revalidate" }, //86400
                { "cacheControlZero","Cache-Control:max-age=0,  must-revalidate" }, //0
                { "cacheControlNoCache","Cache-Control:no-cache" },
                { "referenceDataTrue","Reference-Data:true" }
            };

            var headerConf = (NameValueCollection) ConfigurationManager.GetSection("servicesConfig");

            foreach(string key in headerKeys)
            {
                string val = key.Split(':').Length == 2 ? key : headerContent[key];

                val = (from Match match in pattern.Matches(headerContent[key]) 
                       select match.Groups["keyMatch"].Value)
                       .Aggregate(val, (current, headerConfKey) => current.Replace("{{" + headerConfKey + "}}", headerConf[headerConfKey]));

                string[] header = val.Split(':');

                resultObj.Headers.Add(header[0], header[1]);
            }
            return resultObj;
        }

        internal static TResult ExecuteCommand<TResult, TParameter>(ICommand<TResult, TParameter> command, TParameter args)
        {
            var executor = RegistryFactory.GetResolver().Resolve<ICommandExecutor>();

            return executor.Execute(command, args).Result;

        }
    }
}