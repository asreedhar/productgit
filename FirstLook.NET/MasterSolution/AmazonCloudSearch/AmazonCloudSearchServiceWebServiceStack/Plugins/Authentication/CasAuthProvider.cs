﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Security.Principal;
using System.Threading;
using System.Web;
using CASWrapper;
using FirstLook.Common.Security.CentralAuthenticationService.Client.Configuration;
using ServiceStack.ServiceInterface;
using ServiceStack.ServiceInterface.Auth;
using ServiceStack.Common.Web;
using System.Net;
using ServiceStack.WebHost.Endpoints.Extensions;


namespace FirstLook.AmazonCloudSearchService.WebServiceStack.Plugins.Authentication
{
    public class CasAuthProvider : BasicAuthProvider
    {
        private static readonly log4net.ILog Log4net = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const string CasWebServiceUrl = "CasWebServiceUrl";

        public override bool TryAuthenticate(IServiceBase authService, string userName, string password)
        {
            //Add here your custom auth logic (database calls etc)
            //Return true if credentials are valid, otherwise false
            
            var serviceUrl = GetCasServiceUrl();
            
            var wrapper = new CasServiceWrapper(serviceUrl);
            bool isAuthenticated = false;
            try
            {
                if (Log4net.IsInfoEnabled)
                {
                    Log4net.InfoFormat("Authenticating. UserName = '{0}'", userName);
                }

                // We don't actually need to create a ticket. 
                // We just need to know if the credentials are authentic.
                wrapper.CreateTicket(userName, password, out isAuthenticated);

            }
            catch (Exception ex)
            {
                Log4net.Error("Failed while communicating with the CAS SOAP service.", ex);
                throw;// return false;
            }


            if (Log4net.IsInfoEnabled)
            {
                Log4net.InfoFormat("Finished Authenticating. UserName = '{0}', IsAuthenticated = {1}", userName,
                               isAuthenticated ? "true" : "false");
            }
            return isAuthenticated;
        }


        public override void OnAuthenticated(IServiceBase authService, IAuthSession session, IOAuthTokens tokens, Dictionary<string, string> authInfo)
        {
            ////Fill the IAuthSession with data which you want to retrieve in the app eg:
            session.IsAuthenticated = true;

            // Since we are not using CAS authentication, we need to set the Thread's principal.  This is used by various commands to get the user name
            // (i.e. search.UpdateUser = Thread.CurrentPrincipal.Identity.Name;).
            var genPricipal = new GenericPrincipal(new GenericIdentity(session.UserAuthName), new string[] { });
            Thread.CurrentPrincipal = genPricipal;

            ////Important: You need to save the session!
            authService.SaveSession(session, SessionExpiry);
        }

        private static string GetCasServiceUrl()
        {
            var url = ConfigurationManager.AppSettings[CasWebServiceUrl];
            if (!string.IsNullOrEmpty(url))
                return url;

            var configuration =
                (CentralAuthenticationServiceConfigurationSection)ConfigurationManager.GetSection("centralAuthenticationService")
                ?? new CentralAuthenticationServiceConfigurationSection();

            return configuration.ValidateUrl;
        }
    }
}