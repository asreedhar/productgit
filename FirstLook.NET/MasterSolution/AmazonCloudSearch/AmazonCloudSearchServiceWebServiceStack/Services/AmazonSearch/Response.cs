﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using FirstLook.AmazonCloudSearchService.WebServiceStack.Services.AmazonSearch.Model;
using FirstLook.CloudSearch.DomainModel.Search.Model;
using FirstLook.AmazonCloudSearchService.DomainModel.AmazonCloudSearchListing.Model;

namespace FirstLook.AmazonCloudSearchService.WebServiceStack.Services.AmazonSearch
{
    [DataContract]
    public class Response
    {
        [DataMember(Name = "listings")]
        public List<VehicleListing> Listings { get; set; }

        [DataMember(Name = "priceMin")]
        public double? MinPrice { get; set; }

        [DataMember(Name = "priceMax")]
        public double? MaxPrice { get; set; }

        [DataMember(Name = "mileageMin")]
        public int? MinMileage { get; set; }

        [DataMember(Name = "mileageMax")]
        public int? MaxMileage { get; set; }

        [DataMember(Name = "priceAvg")]
        public double AvgPrice { get; set; }

        [DataMember(Name = "mileageAvg")]
        public float AvgMileage { get; set; }

        [DataMember(Name = "facets")]
        public Dictionary<string, FacetBuckets> FacetDetails { get; set; }

        [DataMember(Name = "cursor")]
        public string Cursor { get; set; }
    }
    //[DataContract]
    //public class FacetBuckets
    //{
    //    [DataMember(Name = "buckets")]
    //    public List<Buckets> buckets { get; set; }


    //}
}