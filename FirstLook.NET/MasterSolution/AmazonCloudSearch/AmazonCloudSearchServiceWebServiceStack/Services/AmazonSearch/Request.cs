﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using ServiceStack.ServiceHost;

namespace FirstLook.AmazonCloudSearchService.WebServiceStack.Services.AmazonSearch
{
    public class Request
    {
        [Route("/{ver}/amazonsearch/vehicles/listings")]
        [Route("/amazonsearch/vehicles/listings")]
        public class AmazonCloudSearch
        {
            
            public int? VehicleId; // { get; set; }

            public string Vin { get; set; }

            public  string AdvertiserName { get; set; }

            public  string AdvertiserAddressLine1 { get; set; }

            public  string AdvertiserAddressCity { get; set; }

            public  string AdvertiserAddressCounty { get; set; }

            public  string AdvertiserAddressState { get; set; }

            public  string AdvertiserAddressPostalcode { get; set; }

            public  string AdvertiserAddressCountry { get; set; }

            public  string AdvertiserAddressLocation { get; set; }

            public  string StockNumber { get; set; }

            public  int? Price { get; set; }

            public  int? OdometerReading { get; set; }

            public  List<int> CandidateChromecStyles { get; set; }

            public  string Manufacturer { get; set; }

            public  string Make{ get; set; }

            public  string Model { get; set; }

            public  int? ModelYear { get; set; }
    
            public  string Trim { get; set; }

            public  string Style { get; set; }

            public  string BodyStyle { get; set; }

            public  string TransmissionDescription { get; set; }

            public string TransmissionType { get; set; }

            public  List<string> TransmissionGearSelectionMechanism { get; set; }

            public  string EngineDescription { get; set; }

            public  List<string> EngineFuelType { get; set; }

            public  int? EngineCylinderCount { get; set; }

            public  string EngineCylinderLayout { get; set; }

            public  float? EngineDisplacementInliters { get; set; }

            public string DrivetrainDescription { get; set; }

            public  string DrivetrainType { get; set; }

            public  string ExteriorOneColorManufacturerName{ get; set; }

            public  string ExteriorOneColorGenericName{ get; set; }

            public  string ExteriorOneColorRgbHex{ get; set; }

            public  string InteriorColorManufacturerName{ get; set; }

            public  int? FuelEconomyCity{ get; set; }

            public  int? FuelEconomyHighway{ get; set; }

            public  int? PassengerCapacity{ get; set; }

            public  int? PassengerDoors{ get; set; }

            public  List<string> InteriorItems{ get; set; }
         
            public  List<string> ExteriorItems{ get; set; }
            
            public  List<string> SafetyItems{ get; set; }
         
            public  List<string> TechnologyItems{ get; set; }

            public  List<string> UnderTheHoodItems{ get; set; }

            public  bool? IsPublicationOnline{ get; set; }

            public  DateTime? DatePublished{ get; set; }

            public string Cursor { get; set; }

            public int Size { get; set; }

            public Dictionary<string,List<object>> Facets{get;set;}

            public string Ver { get; set; }

            public float UserLat { get; set; }

            public float UserLon { get; set; }

            public int MinDistance { get; set; }

            public int MaxDistance { get; set; }
        }
    }
}