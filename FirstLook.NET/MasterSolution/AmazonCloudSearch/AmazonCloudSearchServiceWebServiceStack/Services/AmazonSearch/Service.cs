﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Reflection;
using FirstLook.CloudSearch.DomainModel.Search.Model;
using FirstLook.CloudSearch.DomainModel.Search.Serializers;
using FirstLook.Common.Core.Registry;
using FirstLook.AmazonCloudSearchService.DomainModel.AmazonCloudSearchListing.Commands;
using FirstLook.AmazonCloudSearchService.DomainModel.AmazonCloudSearchListing.Commands.TransferObjects.Envelopes;
using FirstLook.AmazonCloudSearchService.DomainModel.Exceptions;
using FirstLook.AmazonCloudSearchService.WebServiceStack.Services.AmazonSearch.Model;
using FirstLook.AmazonCloudSearchService.WebServiceStack.Utility;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;

namespace FirstLook.AmazonCloudSearchService.WebServiceStack.Services.AmazonSearch
{
    [Authenticate]
    public class Service : ServiceStack.ServiceInterface.Service
    {
        readonly VersionMapper<Request.AmazonCloudSearch> _searchMapper = new VersionMapper<Request.AmazonCloudSearch>("v1");

        public Service()
        {
            _searchMapper.Add("v1", "post", Post_v1);
        }

        public object Post(Request.AmazonCloudSearch request)
        {
            try
            {
                return _searchMapper.GetServiceFunction(request.Ver, "post")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }


        public IHttpResult Post_v1(Request.AmazonCloudSearch request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            var command = factory.CreateSearchListingCommand();

            VehicleSearchListingArgumentDto vehicleArgumentDTO = new VehicleSearchListingArgumentDto()
            {
                ModelYear = request.ModelYear,
                Make = request.Make,
                Model = request.Model,
                ExteriorOneColorGenericName = request.ExteriorOneColorGenericName,
                Trim=request.Trim,
                Facets = request.Facets,
                Size = request.Size,
                UserLat=request.UserLat,
                UserLon=request.UserLon,
                MaxDistance=request.MaxDistance,
                MinDistance=request.MinDistance
            };
            string oldETag = this.RequestContext.GetHeader("If-None-Match");

            VehicleSearchListingResultDto vehicleListingDTO = command.Execute(vehicleArgumentDTO);
            

            AmazonSearch.Response response = new AmazonSearch.Response()
            {
                AvgMileage = vehicleListingDTO.AvgMileage,
                AvgPrice = vehicleListingDTO.AvgPrice,
                Listings = Map(vehicleListingDTO),
                MaxMileage = vehicleListingDTO.MaxMileage,
                MaxPrice = vehicleListingDTO.MaxPrice,
                MinMileage = vehicleListingDTO.MinMileage,
                MinPrice = vehicleListingDTO.MinPrice,
                Cursor = vehicleListingDTO.Cursor,
                FacetDetails = vehicleListingDTO.FacetInfo
            };
            IHttpResult result =handle304(oldETag, response, true);

            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                Helper.SetStaticHeaders(result, new string[] { "referenceDataTrue", "cacheControlDay" });
            }

            return result;
        }

        

        private List<VehicleListing> Map(VehicleSearchListingResultDto vehicleListingDTO)
        {
            List<VehicleListing> Listing = vehicleListingDTO.Listings.Select(p => new VehicleListing()
            {

                AdvertiserAddressCity = p.AdvertiserAddressCity,
                AdvertiserAddressCountry = p.AdvertiserAddressCountry,
                AdvertiserAddressCounty = p.AdvertiserAddressCounty,
                AdvertiserAddressLine1 = p.AdvertiserAddressLine1,
                AdvertiserAddressLocation = p.AdvertiserAddressLocation,
                AdvertiserAddressPostalcode = p.AdvertiserAddressPostalcode,
                AdvertiserAddressState = p.AdvertiserAddressState,
                AdvertiserName = p.AdvertiserName,
                BodyStyle = p.BodyStyle,
                CandidateChromecStyles = p.CandidateChromecStyles,
                DatePublished = p.DatePublished,
                DrivetrainDescription = p.DrivetrainDescription,
                DrivetrainType = p.DrivetrainType,
                EngineCylinderCount = p.EngineCylinderCount,
                EngineCylinderLayout = p.EngineCylinderLayout,
                EngineDescription = p.EngineDescription,
                EngineDisplacementInliters = p.EngineDisplacementInliters,
                EngineFuelType = p.EngineFuelType,
                ExteriorItems = p.ExteriorItems,
                ExteriorOneColorGenericName = p.ExteriorOneColorGenericName,
                ExteriorOneColorManufacturerName = p.ExteriorOneColorManufacturerName,
                ExteriorOneColorRgbHex = p.ExteriorOneColorRgbHex,
                FuelEconomyCity = p.FuelEconomyCity,
                FuelEconomyHighway = p.FuelEconomyHighway,
                InteriorColorManufacturerName = p.InteriorColorManufacturerName,
                InteriorItems = p.InteriorItems,
                IsPublicationOnline = p.IsPublicationOnline,
                Make = p.Make,
                Manufacturer = p.Manufacturer,
                Model = p.Model,
                ModelYear = p.ModelYear,
                OdometerReading = p.OdometerReading,
                PassengerCapacity = p.PassengerCapacity,
                PassengerDoors = p.PassengerDoors,
                Price = p.Price,
                SafetyItems = p.SafetyItems,
                StockNumber = p.StockNumber,
                Style = p.Style,
                TechnologyItems = p.TechnologyItems,
                TransmissionDescription = p.TransmissionDescription,
                TransmissionGearSelectionMechanism = p.TransmissionGearSelectionMechanism,
                TransmissionType = p.TransmissionType,
                Trim = p.Trim,
                UnderTheHoodItems = p.UnderTheHoodItems,
                VehicleId = p.VehicleId,
                Vin = p.Vin
            }).ToList();
            return Listing;
        }


        private List<VehicleListing> Map(List<DeserializeSearchData> SearchResults)
        {
            List<VehicleListing> vehicleListings = new List<VehicleListing>();

            foreach (DeserializeSearchData sr in SearchResults)
            {
                List<int> candidateChromecStyles = new List<int>();

                sr.CandidateChromecStyles.ForEach(ccs => candidateChromecStyles.Add(Convert.ToInt32(ccs)));

                vehicleListings.Add(new VehicleListing()
                {
                    AdvertiserAddressCity = sr.AdvertiserAddressCity != null && sr.AdvertiserAddressCity.Count > 0 ? sr.AdvertiserAddressCity[0] : null,
                    AdvertiserAddressCountry = sr.AdvertiserAddressCountry != null && sr.AdvertiserAddressCountry.Count > 0 ? sr.AdvertiserAddressCountry[0] : null,
                    AdvertiserAddressCounty = sr.AdvertiserAddressCounty != null && sr.AdvertiserAddressCounty.Count > 0 ? sr.AdvertiserAddressCounty[0] : null,
                    AdvertiserAddressLine1 = sr.AdvertiserAddressLine1 != null && sr.AdvertiserAddressLine1.Count > 0 ? sr.AdvertiserAddressLine1[0] : null,
                    AdvertiserAddressLocation = sr.AdvertiserAddressLocation != null && sr.AdvertiserAddressLocation.Count > 0 ? sr.AdvertiserAddressLocation[0] : null,
                    AdvertiserAddressPostalcode = sr.AdvertiserAddressPostalcode != null && sr.AdvertiserAddressPostalcode.Count > 0 ? sr.AdvertiserAddressPostalcode[0] : null,
                    AdvertiserAddressState = sr.AdvertiserAddressState != null && sr.AdvertiserAddressState.Count > 0 ? sr.AdvertiserAddressState[0] : null,
                    AdvertiserName = sr.AdvertiserName != null && sr.AdvertiserName.Count > 0 ? sr.AdvertiserName[0] : null,
                    BodyStyle = sr.BodyStyle != null && sr.BodyStyle.Count > 0 ? sr.BodyStyle[0] : null,
                    CandidateChromecStyles = candidateChromecStyles,
                    //DatePublished = sr.DatePublished != null && sr.DatePublished.Count > 0 ? (DateTime?)Convert.ToDateTime(sr.DatePublished[0]) : null,
                    DrivetrainDescription = sr.DrivetrainDescription != null && sr.DrivetrainDescription.Count > 0 ? sr.DrivetrainDescription[0] : null,
                    DrivetrainType = sr.DrivetrainType != null && sr.DrivetrainType.Count > 0 ? sr.DrivetrainType[0] : null,
                    EngineCylinderCount = sr.EngineCylinderCount != null && sr.EngineCylinderCount.Count > 0 ? (int?)Convert.ToInt32(sr.EngineCylinderCount[0]) : null,
                    EngineCylinderLayout = sr.EngineCylinderLayout != null && sr.EngineCylinderLayout.Count > 0 ? sr.EngineCylinderLayout[0] : null,
                    EngineDescription = sr.EngineDescription != null && sr.EngineDescription.Count > 0 ? sr.EngineDescription[0] : null,
                    EngineDisplacementInliters = sr.EngineDisplacementInliters != null && sr.EngineDisplacementInliters.Count > 0 ? (float?)Convert.ToDecimal(sr.EngineDisplacementInliters[0]) : null,
                    EngineFuelType = sr.EngineFuelType != null && sr.EngineFuelType.Count > 0 ? sr.EngineFuelType : null,
                    ExteriorItems = sr.ExteriorItems != null && sr.ExteriorItems.Count > 0 ? sr.ExteriorItems : null,
                    ExteriorOneColorGenericName = sr.ExteriorOneColorGenericName != null && sr.ExteriorOneColorGenericName.Count > 0 ? sr.ExteriorOneColorGenericName[0] : null,
                    ExteriorOneColorManufacturerName = sr.ExteriorOneColorManufacturerName != null && sr.ExteriorOneColorManufacturerName.Count > 0 ? sr.ExteriorOneColorManufacturerName[0] : null,
                    ExteriorOneColorRgbHex = sr.ExteriorOneColorRgbHex != null && sr.ExteriorOneColorRgbHex.Count > 0 ? sr.ExteriorOneColorRgbHex[0] : null,
                    FuelEconomyCity = sr.FuelEconomyCity != null && sr.FuelEconomyCity.Count > 0 ? (int?)Convert.ToInt32(sr.FuelEconomyCity[0]) : null,
                    FuelEconomyHighway = sr.FuelEconomyHighway != null && sr.FuelEconomyHighway.Count > 0 ? (int?)Convert.ToInt32(sr.FuelEconomyHighway[0]) : null,
                    InteriorColorManufacturerName = sr.InteriorColorManufacturerName != null && sr.InteriorColorManufacturerName.Count > 0 ? sr.InteriorColorManufacturerName[0] : null,
                    InteriorItems = sr.InteriorItems != null && sr.InteriorItems.Count > 0 ? sr.InteriorItems : null,
                    IsPublicationOnline = sr.IsPublicationOnline != null && sr.IsPublicationOnline.Count > 0 ? (bool?)(sr.IsPublicationOnline[0] == "1" ? true : false) : null,
                    Make = sr.Make != null && sr.Make.Count > 0 ? sr.Make[0] : null,
                    Manufacturer = sr.Manufacturer != null && sr.Manufacturer.Count > 0 ? sr.Manufacturer[0] : null,
                    Model = sr.Model != null && sr.Model.Count > 0 ? sr.Model[0] : null,
                    ModelYear = sr.ModelYear != null && sr.ModelYear.Count > 0 ? (int?)Convert.ToInt32(sr.ModelYear[0]) : null,
                    OdometerReading = sr.OdometerReading != null && sr.OdometerReading.Count > 0 ? (int?)Convert.ToInt32(sr.OdometerReading[0]) : null,
                    PassengerCapacity = sr.PassengerCapacity != null && sr.PassengerCapacity.Count > 0 ? (int?)Convert.ToInt32(sr.PassengerCapacity[0]) : null,
                    PassengerDoors = sr.PassengerDoors != null && sr.PassengerDoors.Count > 0 ? (int?)Convert.ToInt32(sr.PassengerDoors[0]) : null,
                    Price = sr.Price != null && sr.Price.Count > 0 ? (int?)Convert.ToInt32(sr.Price[0]) : null,
                    SafetyItems = sr.SafetyItems != null && sr.SafetyItems.Count > 0 ? sr.SafetyItems : null,
                    StockNumber = sr.StockNumber != null && sr.StockNumber.Count > 0 ? sr.StockNumber[0] : null,
                    Style = sr.Style != null && sr.Style.Count > 0 ? sr.Style[0] : null,
                    TechnologyItems = sr.TechnologyItems != null && sr.TechnologyItems.Count > 0 ? sr.TechnologyItems : null,
                    TransmissionDescription = sr.TransmissionDescription != null && sr.TransmissionDescription.Count > 0 ? sr.TransmissionDescription[0] : null,
                    TransmissionGearSelectionMechanism = sr.TransmissionGearSelectionMechanism != null && sr.TransmissionGearSelectionMechanism.Count > 0 ? sr.TransmissionGearSelectionMechanism : null,
                    TransmissionType = sr.TransmissionType != null && sr.TransmissionType.Count > 0 ? sr.TransmissionType[0] : null,
                    Trim = sr.Trim != null && sr.Trim.Count > 0 ? sr.Trim[0] : null,
                    UnderTheHoodItems = sr.UnderTheHoodItems != null && sr.UnderTheHoodItems.Count > 0 ? sr.UnderTheHoodItems : null,
                    VehicleId = sr.VehicleId != null && sr.VehicleId.Count > 0 ? (int?)Convert.ToInt32(sr.VehicleId[0]) : null,
                    Vin = sr.Vin != null && sr.Vin.Count > 0 ? sr.Vin[0] : null
                });
            }
            return vehicleListings;
        }

        private string GetFacetString(Dictionary<string, List<object>> FacetPropertis)
        {
            if (FacetPropertis == null || FacetPropertis.Count == 0)
                return null;

            Faceting facetList = new Faceting();

            foreach (string key in FacetPropertis.Keys)
            {
                Type myType = typeof(SearchRequestEntity);
                // Get the PropertyInfo object by passing the property name.
                MemberInfo[] PropInfo = myType.GetMember(key);
                if (PropInfo == null || PropInfo.Length == 0)
                    throw new InvalidInputException("Facet column not found");

                facetList.AddFacet(new Facet() { FacetName = GetDescription(PropInfo), Constraints = FacetPropertis[key] });
            }

            return facetList.ToString();
        }

        private string GetDescription(MemberInfo[] members)
        {
            object[] attrs = members[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attrs == null || attrs.Length == 0) return null;
            DescriptionAttribute desc = attrs[0] as DescriptionAttribute;
            if (desc == null) return null;
            return desc.Description;
        }

        IHttpResult handle304(string oldETag, object response, bool compress)
        {
            string newETag = ETagHelper.CreateETagHash(response);
            IHttpResult result = new HttpResult();
            if (oldETag != null && oldETag.Equals(newETag))
            {
                //  ETags match so no need to zip and send the whole response - just headers
                result.StatusCode = HttpStatusCode.NotModified;
            }
            else
            {
                // ETag does not match so we need to send to updated resource
                if (compress)
                    result = Helper.Compress(this, response);
                else
                    result = new HttpResult
                    {
                        Response = response
                    };

                ETagHelper.AddETagHeader(result, response);
                result.StatusCode = HttpStatusCode.OK;
            }
            return result;
        }

    }
}