﻿
namespace FirstLook.FirstLookServices.WebServiceStack.Services.AmazonSearch.Model
{
    public class Bucket
    {
        public string BucketValue { get; set; }
        public int Count { get; set; }
    }
}