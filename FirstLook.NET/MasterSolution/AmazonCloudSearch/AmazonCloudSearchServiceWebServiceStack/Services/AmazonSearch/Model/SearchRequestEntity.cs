﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace FirstLook.AmazonCloudSearchService.WebServiceStack.Services.AmazonSearch.Model
{
    public class SearchRequestEntity
    {
        [Description("vehicleid")]
        public static int? VehicleId;// { get; set; }

        [Description("vin")]
        public static string Vin;// { get; set; }

        [Description("advertiser_name")]
        public static string AdvertiserName;// { get; set; }

        [Description("advertiser_address_line1")]
        public static string AdvertiserAddressLine1;// { get; set; }

        [Description("advertiser_address_city")]
        public static string AdvertiserAddressCity;// { get; set; }

        [Description("advertiser_address_county")]
        public static string AdvertiserAddressCounty;// { get; set; }

        [Description("advertiser_address_state")]
        public static string AdvertiserAddressState;// { get; set; }

        [Description("advertiser_address_postalcode")]
        public static string AdvertiserAddressPostalcode;// { get; set; }

        [Description("advertiser_address_country")]
        public static string AdvertiserAddressCountry;// { get; set; }

        [Description("advertiser_address_location")]
        public static string AdvertiserAddressLocation;// { get; set; }

        [Description("stock_number")]
        public static string StockNumber;// { get; set; }

        [Description("price")]
        public static int? Price;// { get; set; }

        [Description("odometer_reading")]
        public static int? OdometerReading;// { get; set; }

        [Description("candidate_chromec_styles")]
        public static List<int> CandidateChromecStyles;// { get; set; }

        [Description("manufacturer")]
        public static string Manufacturer;// { get; set; }

        [Description("make")]
        public static string Make;//{ get; set; }

        [Description("model")]
        public static string Model;// { get; set; }

        [Description("model_year")]
        public static int ModelYear;// { get; set; }Expression<Func<T>> 

        [Description("trim")]
        public static string Trim;// { get; set; }

        [Description("style")]
        public static string Style;// { get; set; }

        [Description("body_style")]
        public static string BodyStyle;// { get; set; }

        [Description("transmission_description")]
        public static string TransmissionDescription;// { get; set; }

        [Description("transmission_type")]
        public string TransmissionType;// { get; set; }

        [Description("transmission_gear_selection_mechanism")]
        public static List<string> TransmissionGearSelectionMechanism;// { get; set; }

        [Description("engine_description")]
        public static string EngineDescription;// { get; set; }

        [Description("engine_fuel_type")]
        public static List<string> EngineFuelType;// { get; set; }

        [Description("engine_cylinder_count")]
        public static int? EngineCylinderCount;// { get; set; }

        [Description("engine_cylinder_layout")]
        public static string EngineCylinderLayout;// { get; set; }

        [Description("engine_displacement_inliters")]
        public static float? EngineDisplacementInliters;// { get; set; }

        [Description("drivetrain_description")]
        public string DrivetrainDescription;// { get; set; }

        [Description("drivetrain_type")]
        public static string DrivetrainType;// { get; set; }

        [Description("exterior_one_color_manufacturer_name")]
        public static string ExteriorOneColorManufacturerName;//{ get; set; }

        [Description("exterior_one_color_generic_name")]
        public static string ExteriorOneColorGenericName;//{ get; set; }

        [Description("exterior_one_color_rgb_hex")]
        public static string ExteriorOneColorRgbHex;//{ get; set; }

        [Description("interior_color_manufacturer_name")]
        public static string InteriorColorManufacturerName;//{ get; set; }

        [Description("fuel_economy_city")]
        public static int? FuelEconomyCity;//{ get; set; }

        [Description("fuel_economy_highway")]
        public static int? FuelEconomyHighway;//{ get; set; }

        [Description("passenger_capacity")]
        public static int? PassengerCapacity;//{ get; set; }

        [Description("passenger_doors")]
        public static int? PassengerDoors;//{ get; set; }

        [Description("interior_items")]
        public static List<string> InteriorItems;//{ get; set; }

        [Description("exterior_items")]
        public static List<string> ExteriorItems;//{ get; set; }

        [Description("safety_items")]
        public static List<string> SafetyItems;//{ get; set; }

        [Description("technology_items")]
        public static List<string> TechnologyItems;//{ get; set; }

        [Description("under_the_hood_items")]
        public static List<string> UnderTheHoodItems;//{ get; set; }

        [Description("is_publication_online")]
        public static bool? IsPublicationOnline;//{ get; set; }

        [Description("date_published")]
        public static DateTime? DatePublished;//{ get; set; }
    }
}