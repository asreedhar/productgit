﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.AmazonCloudSearchService.WebServiceStack.Services.AmazonSearch.Model
{
    public class VehicleListing
    {
        [DataMember(Name = "vehicleid")]
        public int? VehicleId { get; set; }

        [DataMember(Name = "vin")]
        public string Vin { get; set; }

        [DataMember(Name = "advertiser_name")]
        public string AdvertiserName { get; set; }

        [DataMember(Name = "advertiser_address_line1")]
        public string AdvertiserAddressLine1 { get; set; }

        [DataMember(Name = "advertiser_address_city")]
        public string AdvertiserAddressCity { get; set; }

        [DataMember(Name = "advertiser_address_county")]
        public string AdvertiserAddressCounty { get; set; }

        [DataMember(Name = "advertiser_address_state")]
        public string AdvertiserAddressState { get; set; }

        [DataMember(Name = "advertiser_address_postalcode")]
        public string AdvertiserAddressPostalcode { get; set; }

        [DataMember(Name = "advertiser_address_country")]
        public string AdvertiserAddressCountry { get; set; }

        [DataMember(Name = "advertiser_address_location")]
        public string AdvertiserAddressLocation { get; set; }

        [DataMember(Name = "stock_number")]
        public string StockNumber { get; set; }

        [DataMember(Name = "price")]
        public double? Price { get; set; }

        [DataMember(Name = "odometer_reading")]
        public int? OdometerReading { get; set; }

        [DataMember(Name = "candidate_chromec_styles")]
        public List<int> CandidateChromecStyles { get; set; }

        [DataMember(Name = "manufacturer")]
        public string Manufacturer { get; set; }

        [DataMember(Name = "make")]
        public string Make { get; set; }

        [DataMember(Name = "model")]
        public string Model { get; set; }

        [DataMember(Name = "model_year")]
        public int? ModelYear { get; set; }

        [DataMember(Name = "trim")]
        public string Trim { get; set; }

        [DataMember(Name = "style")]
        public string Style { get; set; }

        [DataMember(Name = "body_style")]
        public string BodyStyle { get; set; }

        [DataMember(Name = "transmission_description")]
        public string TransmissionDescription { get; set; }

        [DataMember(Name = "transmission_type")]
        public string TransmissionType { get; set; }

        [DataMember(Name = "transmission_gear_selection_mechanism")]
        public List<string> TransmissionGearSelectionMechanism { get; set; }

        [DataMember(Name = "engine_description")]
        public string EngineDescription { get; set; }

        [DataMember(Name = "engine_fuel_type")]
        public List<string> EngineFuelType { get; set; }

        [DataMember(Name = "engine_cylinder_count")]
        public int? EngineCylinderCount { get; set; }

        [DataMember(Name = "engine_cylinder_layout")]
        public string EngineCylinderLayout { get; set; }

        [DataMember(Name = "engine_displacement_inliters")]
        public double? EngineDisplacementInliters { get; set; }

        [DataMember(Name = "drivetrain_description")]
        public string DrivetrainDescription { get; set; }

        [DataMember(Name = "drivetrain_type")]
        public string DrivetrainType { get; set; }

        [DataMember(Name = "exterior_one_color_manufacturer_name")]
        public string ExteriorOneColorManufacturerName { get; set; }

        [DataMember(Name = "exterior_one_color_generic_name")]
        public string ExteriorOneColorGenericName { get; set; }

        [DataMember(Name = "exterior_one_color_rgb_hex")]
        public string ExteriorOneColorRgbHex { get; set; }

        [DataMember(Name = "interior_color_manufacturer_name")]
        public string InteriorColorManufacturerName { get; set; }

        [DataMember(Name = "fuel_economy_city")]
        public double? FuelEconomyCity { get; set; }

        [DataMember(Name = "fuel_economy_highway")]
        public double? FuelEconomyHighway { get; set; }

        [DataMember(Name = "passenger_capacity")]
        public int? PassengerCapacity { get; set; }

        [DataMember(Name = "passenger_doors")]
        public int? PassengerDoors { get; set; }

        [DataMember(Name = "interior_items")]
        public List<string> InteriorItems { get; set; }

        [DataMember(Name = "exterior_items")]
        public List<string> ExteriorItems { get; set; }

        [DataMember(Name = "safety_items")]
        public List<string> SafetyItems { get; set; }

        [DataMember(Name = "technology_items")]
        public List<string> TechnologyItems { get; set; }

        [DataMember(Name = "under_the_hood_items")]
        public List<string> UnderTheHoodItems { get; set; }

        [DataMember(Name = "is_publication_online")]
        public bool? IsPublicationOnline { get; set; }

        [DataMember(Name = "date_published")]
        public DateTime? DatePublished { get; set; }
    }
}