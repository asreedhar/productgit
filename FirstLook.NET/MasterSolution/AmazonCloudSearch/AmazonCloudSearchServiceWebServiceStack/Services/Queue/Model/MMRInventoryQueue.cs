﻿using System.Runtime.Serialization;
using FirstLook.FirstLookServices.WebServiceStack.Services.Appraisals;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Queue.Model
{
    [DataContract]
    public class MMRInventoryQueue:MMRVehicle
    {
        [DataMember]
        public int InventoryId { get; set; }
        [DataMember]
        public int Mileage { get; set; }
        [DataMember]
        public int VehicleYear { get; set; }
    }
}