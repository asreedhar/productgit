﻿using System;
using System.Net;
using FirstLook.FirstLookServices.WebServiceStack.Utility;
using FirstLook.QueueProcessor.DomainModel.BookQueue.Model;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model;
using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Queue
{
    public class Service : ServiceStack.ServiceInterface.Service
    {
        readonly VersionMapper<Request.MMRQueueProcessor> _queueProcessorMapper = new VersionMapper<Request.MMRQueueProcessor>("v1");
        readonly VersionMapper<Request.MMRPushDataInQueue> _queuePushDataMapper = new VersionMapper<Request.MMRPushDataInQueue>("v1");
        readonly VersionMapper<Request.MMRPurgeData> _queuePurgeDataMapper = new VersionMapper<Request.MMRPurgeData>("v1");
        readonly VersionMapper<Request.MMRAveragePrice> _queueMMRAvgPriceDataMapper = new VersionMapper<Request.MMRAveragePrice>("v1");

        public Service()
        {
            _queueProcessorMapper.Add("v1", "post", Post_v1);
            _queuePushDataMapper.Add("v1", "post", Post_v1);
            _queuePurgeDataMapper.Add("v1", "post", Post_v1);
            _queueMMRAvgPriceDataMapper.Add("v1", "get", Get_v1);
        }

        public object Get(Request.MMRAveragePrice request)
        {
            try
            {
                return _queueMMRAvgPriceDataMapper.GetServiceFunction(request.Ver, "get")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);

                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public object Post(Request.MMRQueueProcessor request)
        {
            try
            {
                return _queueProcessorMapper.GetServiceFunction(request.Ver, "post")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public object Post(Request.MMRPushDataInQueue request)
        {
            try
            {
                return _queuePushDataMapper.GetServiceFunction(request.Ver, "post")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public object Post(Request.MMRPurgeData request)
        {
            try
            {
                return _queuePurgeDataMapper.GetServiceFunction(request.Ver, "post")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public object Get_v1(Request.MMRAveragePrice request)
        {
            string oldETag = this.RequestContext.GetHeader("If-None-Match");

            ProcessManhaimQueue _queue = new ProcessManhaimQueue();
            MMRInventoryRequest req=new MMRInventoryRequest();

            decimal avgPrice=_queue.UpdateAveragePrice(req);

            IHttpResult result = Handle304(oldETag, avgPrice, true);
            //throw new Exception();
            return result;
        }

        public IHttpResult Post_v1(Request.MMRQueueProcessor request)
        {
            string oldETag = this.RequestContext.GetHeader("If-None-Match");

            ProcessManhaimQueue _queue = new ProcessManhaimQueue();
            MMRInventoryRequest req = new MMRInventoryRequest();

            _queue.UpdateMMRAveragePriceFromQueue(request.RecordCount);
            
            IHttpResult result = Helper.Compress(this, "");

            result.StatusCode = HttpStatusCode.OK;

            result = Helper.SetStaticHeaders(result, new[] { "cacheControlNoCache" });

            return result;
        }

        public IHttpResult Post_v1(Request.MMRPushDataInQueue request)
        {
            string oldETag = this.RequestContext.GetHeader("If-None-Match");

            ProcessManhaimQueue _queue = new ProcessManhaimQueue();
            MMRInventoryRequest req = new MMRInventoryRequest();

            _queue.PushPeriodicDataToQueue(request.Days);

            IHttpResult result = Helper.Compress(this, "");

            result.StatusCode = HttpStatusCode.OK;

            result = Helper.SetStaticHeaders(result, new[] { "cacheControlNoCache" });

            return result;
        }

        public IHttpResult Post_v1(Request.MMRPurgeData request)
        {
            string oldETag = this.RequestContext.GetHeader("If-None-Match");

            ProcessManhaimQueue _queue = new ProcessManhaimQueue();
            MMRInventoryRequest req = new MMRInventoryRequest();

            if ((QueueStatus) request.Status == QueueStatus.Error)
            {
                _queue.PurgeFailureQueue(request.Days);
            }
            else
            {
                _queue.PurgeSuccessQueue(request.Days);
            }

            IHttpResult result = Helper.Compress(this, "");

            result.StatusCode = HttpStatusCode.OK;

            result = Helper.SetStaticHeaders(result, new[] { "cacheControlNoCache" });

            return result;
        }
        
        IHttpResult Handle304(string oldETag, object response, bool compress)
        {
            string newETag = ETagHelper.CreateETagHash(response);

            IHttpResult result = new HttpResult();

            if (oldETag != null && oldETag.Equals(newETag))
            {
                //  ETags match so no need to zip and send the whole response - just headers
                result.StatusCode = HttpStatusCode.NotModified;
            }
            else
            {
                // ETag does not match so we need to send to updated resource
                // 
                if (compress)
                    result = Helper.Compress(this, response);
                else
                    result = new HttpResult
                    {
                        Response = response
                    };

                Helper.SetStaticHeaders(result, new[] { "cacheControlHour" });

                ETagHelper.AddETagHeader(result, response);

                result.StatusCode = HttpStatusCode.OK;
            }

            return result;
        }
    }
}