using AutoMapper;
using FirstLook.FirstLookServices.DomainModel.VehicleConfig;
using FirstLook.FirstLookServices.DomainModel.VehicleConfig.Internal;
using FirstLook.FirstLookServices.WebServiceStack.Services.VehicleConfig;

namespace FirstLook.FirstLookServices.WebServiceStack
{
    /// <summary>
    /// For any AutoMapper scenarios where it can't figure out the map on its own, register the mappings.
    /// </summary>
    internal static class ExplicitAutoMapperProjections
    {
        public static void Init()
        {
            VehicleConfigMaps();
            Mapper.AssertConfigurationIsValid();
        }

        public static void Reset()
        {
            Mapper.Reset();
        }

        private static void VehicleConfigMaps()
        {
            Mapper.CreateMap<IEquipmentRule, EquipmentRule>()
                .ForMember(dest => dest.Equipment, opt => opt.MapFrom(src => src.Equipment))
                .ForMember(dest => dest.Rule, opt => opt.MapFrom(src => src.Rule));

            Mapper.CreateMap<IEquipment, Equipment>()
                .ForMember(dest => dest.Default, opt => opt.MapFrom(src => src.Default))
                .ForMember(dest => dest.Desc, opt => opt.MapFrom(src => src.Desc))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.SortOrder, opt => opt.MapFrom(src => src.SortOrder));

            Mapper.CreateMap<IVehicleConfig, VehicleConfig>()
                .ForMember(dest => dest.Book, opt => opt.MapFrom(src => src.Book.Desc));

            Mapper.CreateMap<IVehicleConfig, VehicleConfig>();
            
        }
    }
}