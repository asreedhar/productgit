﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Net;
using FirstLook.AmazonCloudSearchService.WebServiceStack.Plugins.Authentication;
using FirstLook.AmazonCloudSearchService.WebServiceStack.Utility;
using Funq;
using ServiceStack.Common.Web;
using ServiceStack.ServiceInterface;
using ServiceStack.ServiceInterface.Auth;
using ServiceStack.Text;
using ServiceStack.WebHost.Endpoints;

namespace FirstLook.AmazonCloudSearchService.WebServiceStack
{
    public class AWSCloudSearchServicesAppHost : AppHostBase
    {
        //Tell Service Stack the name of your application and where to find your web services
        public AWSCloudSearchServicesAppHost() : base("AWSCloudSearchServices", typeof(AWSCloudSearchServicesAppHost).Assembly) { }

        public override void Configure(Container container)
        {
            //Prevent __type attribute from being serialized on json response
            JsConfig.ExcludeTypeInfo = true;
            JsConfig.IncludeNullValues = true;

            JsConfig.ThrowOnDeserializationError = true;

            var headerConf = (NameValueCollection)ConfigurationManager.GetSection("servicesConfig");

            string url = headerConf.Get("WebHostUrl");

            SetConfig(new EndpointHostConfig
            {
                DefaultContentType = ContentType.Json,
                DebugMode = true,
                WebHostUrl = url
            });

            //Handle Unhandled Exceptions occurring outside of Services
            //Stop the output on 'Forbidden'
            this.ExceptionHandler = (req, res, operationName, ex) =>
            {
                if (ex.Message == "Invalid UserName or Password")
                {
                    res.Write("");
                    res.StatusCode = (int)HttpStatusCode.Unauthorized;
                }
                else
                {
                    IDictionary<string, object> error = this.ExceptionToError(ex, null);
                    IDictionary<string, List<IDictionary<string, object>>> responseObj = new Dictionary<string, List<IDictionary<string, object>>>();
                    List<IDictionary<string, object>> errorList = new List<IDictionary<string, object>>();
                    if (error != null && error.Values.Count > 0)
                    {
                        if (error.ContainsKey("status"))
                        {
                            res.StatusCode = (int)error["status"];
                            error.Remove("status");
                        }
                    }
                    responseObj["errors"] = errorList;
                    errorList.Add(error);
                    res.Write(responseObj.SerializeAndFormat());
                }
            };

            this.ServiceExceptionHandler = (req, ex) =>
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            };

            Plugins.Add(new AuthFeature(() => new AuthUserSession(), new IAuthProvider[]
                                                                         {
                                                                             new CasAuthProvider()
                                                                         }) { HtmlRedirect = null });

        }
    }

}