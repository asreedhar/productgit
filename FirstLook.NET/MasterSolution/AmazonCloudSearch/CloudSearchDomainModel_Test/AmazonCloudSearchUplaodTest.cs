﻿using System;
using System.Collections.Generic;
using FirstLook.CloudSearch.DomainModel.DataUpload.Command.Impl;
using FirstLook.CloudSearch.DomainModel.DataUpload.Model;
using FirstLook.Common.Core.Registry;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FirstLook.CloudSearch.DomainModel_Test
{
    /// <summary>
    /// Summary description for AmazonS3ReadTest
    /// </summary>
    [TestClass]
    public class AmazonCloudSearchUplaodTest
    {
        public AmazonCloudSearchUplaodTest()
        {
            //
            // TODO: Add constructor logic here
            //

            IRegistry registry =RegistryFactory.GetRegistry();

            var commandFactory = registry.Register<FirstLook.CloudSearch.DomainModel.DataUpload.Command.Module>();
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestAmazonReadCommand()
        {
            ReadS3DataCommand readS3DataCommand = new ReadS3DataCommand();
            Dictionary<string, string> files = readS3DataCommand.Execute("");

            Assert.IsTrue(files.Count>0);
        }
        
        [TestMethod]
        public void TestUploadAmazonSearch()
        {
            UploadToCloudSearchCommand uploadToCloudSearchCommand = new UploadToCloudSearchCommand();
            System.Diagnostics.Debug.Print("Start:" + DateTime.Now);
            List<List<CloudSearchDocument>> cloudSearchDocuments = uploadToCloudSearchCommand.Execute(AmazonCloudSearchDataOperationType.AddData);
            System.Diagnostics.Debug.Print("End:" + DateTime.Now);
            Assert.IsNotNull(cloudSearchDocuments);
        }
    }
}
