﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;

public class Foo
{
    [Description("The first property.")]
    public int property1;

    [Description("The second property.")]
    public string property2;
}

class Program
{
    static string GetDescription<T>(Expression<Func<T>> expr)
    {
        var mexpr = expr.Body as MemberExpression;
        if (mexpr == null) return null;
        if (mexpr.Member == null) return null;
        object[] attrs = mexpr.Member.GetCustomAttributes(typeof(DescriptionAttribute), false);
        if (attrs == null || attrs.Length == 0) return null;
        DescriptionAttribute desc = attrs[0] as DescriptionAttribute;
        if (desc == null) return null;
        return desc.Description;
    }

    static void Main(string[] args)
    {
        Foo foo = new Foo();
        Console.WriteLine(GetDescription(() => foo.property1));
        Console.WriteLine(GetDescription(() => foo.property2));
    }
}