﻿using System.Collections.Generic;
using FirstLook.AmazonCloudSearchService.DomainModel.AmazonCloudSearchListing.Commands.Impl;
using FirstLook.AmazonCloudSearchService.DomainModel.AmazonCloudSearchListing.Commands.TransferObjects.Envelopes;
using FirstLook.CloudSearch.DomainModel.Search.Commands.Impl;
using FirstLook.CloudSearch.DomainModel.Search.Commands.TransferObjects;
using FirstLook.CloudSearch.DomainModel.Search.Model;
using FirstLook.Common.Core.Registry;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FirstLook.CloudSearch.DomainModel_Test
{
    [TestClass]
    public class AmazonCloudSearchTest
    {
       
        public AmazonCloudSearchTest()
        {
            // Code that runs on application startup      
            IRegistry registry = RegistryFactory.GetRegistry();
            registry.Register<FirstLook.CloudSearch.DomainModel.Module>();
        }

        [TestMethod]
        public void TestAmazonCloudSearch()
        {
            SearchQueryFilterOperation f11 = new SearchQueryFilterOperation();
            SearchQueryValueFilter f1 = new SearchQueryValueFilter() {Field="model_year",Value=null,IsNumeric=true};
            SearchQueryValueFilter f2 = new SearchQueryValueFilter() { Field = "make", Value = "Toyota" };
            //SearchQueryValueFilter f3 = new SearchQueryValueFilter() { Field = "make", Value = "bmw" ,SearchOperator=SearchOperator.Radius,UserLatitude="abc",UserLongitude="lmn",DocumentField="location_address" };
            List<SearchQueryValueFilter> searchQueryFilters=new List<SearchQueryValueFilter>();
            searchQueryFilters.Add(f1);
            searchQueryFilters.Add(f2);
            //searchQueryFilters.Add(f3);
            f11.addFilters(f1);
            f11.addFilters(f2);
           // f11.addFilters(f3);
            Assert.IsNotNull(f11.ToString());
        }
        
        [TestMethod]
        public void TestCloudSearchCommand()
        {
            SearchOperation operation1 = SearchOperation.CreateSearchOperation(() => SearchEntity.ModelYear, 2011, SearchOperator.EqualsTo);
            SearchOperation operation2 = SearchOperation.CreateSearchOperation(() => SearchEntity.Make, "Toyota", SearchOperator.EqualsTo);
            SearchOperation operation3 = SearchOperation.CreateSearchOperation(() => SearchEntity.AdvertiserAddressLocation, 500, SearchOperator.Range, 1500, new RadiusSearch() { SourceLatitude = 42.257622F, SourceLongitude = -84.423366F },true);

            SearchOperation result = operation1 & operation2 & operation3;

            CloudSearchCommand command = new CloudSearchCommand();
            command.Execute(new CloudSearchCommandInput() { SearchQuery = result.SearchOperationString, ReturnFields = operation1.ReturnFields + operation1.ReturnFields+operation3.ReturnFields });
        }

        [TestMethod]
        public void TestCloudSearchSerializationCommand()
        {
            SearchOperation operation1 = SearchOperation.CreateSearchOperation(() => SearchEntity.ModelYear, null, SearchOperator.EqualsTo);
            SearchOperation operation2 = SearchOperation.CreateSearchOperation(() => SearchEntity.Make, "Toyota", SearchOperator.EqualsTo);
            SearchOperation operation3 = SearchOperation.CreateSearchOperation(() => SearchEntity.Model, null, SearchOperator.EqualsTo);
            SearchOperation operation4 = SearchOperation.CreateSearchOperation(() => SearchEntity.ExteriorOneColorGenericName, null, SearchOperator.EqualsTo);

            SearchOperation searchOperationResult = operation1 & operation2 & operation3 & operation4;


            CloudSearchCommand command = new CloudSearchCommand();
            CloudSearchCommandResult resultData = command.Execute(new CloudSearchCommandInput() { SearchQuery = searchOperationResult.SearchOperationString });

            //DeserializeSearchData desrializeObj = new DeserializeSearchData();
            //List<DeserializeSearchData> data = desrializeObj.SerializeSearchData(resultData.JsonData);

        }

        [TestMethod]
        public void TestAmazonCloudSearchFLDomainModel()
        {
            VehicleSearchListingArgumentDto vehicleArgumentDTO = new VehicleSearchListingArgumentDto();//{ ModelYear=(int)request.ModelYear,Model=request.Model,Make=request.Make,ExteriorOneColorGenericName=request.ExteriorOneColorGenericName};
            vehicleArgumentDTO.ModelYear = 2011;
            vehicleArgumentDTO.Make = "Toyota";
            vehicleArgumentDTO.Model = "RAV4";
            vehicleArgumentDTO.ExteriorOneColorGenericName = "White";
            //vehicleArgumentDTO.Facets = request.Facets;
            vehicleArgumentDTO.Size = 100;
            var command = new SearchListingCommand();      

            VehicleSearchListingResultDto vehicleListingDTO = command.Execute(vehicleArgumentDTO);
            Assert.AreEqual("abc", "abc");
        }
    }
}
