﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Amazon.S3;
using Amazon.S3.Model;
using System.Configuration;
using System.Web;
using System.IO;

namespace AmazonCloudSearchTest
{
    class Program
    {
        static void Main(string[] args)
        {
            string bucketName = ConfigurationManager.AppSettings["Bucket"];
            string keyName = ConfigurationManager.AppSettings["S3FileName"];
            string basepath = ConfigurationManager.AppSettings["filebasepath"];

            try
            {

                using (AmazonS3Client s3Client = new AmazonS3Client(Amazon.RegionEndpoint.USEast1))
                {
                    ListObjectsRequest listRequest = new ListObjectsRequest() { BucketName = bucketName, Delimiter = "/", Prefix = basepath };
                    ListObjectsResponse listResponse = s3Client.ListObjects(listRequest);

                    foreach (S3Object entry in listResponse.S3Objects)
                    {
                        //  string key = HttpUtility.UrlEncode("/home/elasticdump/advertisement_1_0.2014-12-19.mapping.json.gz");
                        GetObjectRequest request = new GetObjectRequest
                        {
                            BucketName = bucketName,
                            Key = entry.Key
                        };

                        GetPreSignedUrlRequest request1 = new GetPreSignedUrlRequest
                        {
                            BucketName = bucketName,
                            Key = "advertisement_1_0.2015-01-07.data.json.gz",
                            Expires = DateTime.Now.AddMinutes(3600)
                        };

                        string presignedUrl = s3Client.GetPreSignedURL(request1);
                        GetObjectResponse response = s3Client.GetObject(request);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            TestPlease tp = new TestPlease();
            //tp.ReadAndProcessLargeFile(@"C:\temp\advertisement_1_0.2014-12-19.data.json.txt");


        }
    }
    public class TestPlease
    {

        
        public void GetResult()
        {
            string bucketName = ConfigurationManager.AppSettings["Bucket"];
            string keyName = ConfigurationManager.AppSettings["S3FileName"];
            string basepath = ConfigurationManager.AppSettings["filebasepath"];

            try
            {

                using (AmazonS3Client s3Client = new AmazonS3Client(Amazon.RegionEndpoint.USEast1))
                {
                    ListObjectsRequest listRequest = new ListObjectsRequest() { BucketName = bucketName, Delimiter = "/", Prefix = basepath };
                    ListObjectsResponse listResponse = s3Client.ListObjects(listRequest);

                    foreach (S3Object entry in listResponse.S3Objects)
                    {
                        //  string key = HttpUtility.UrlEncode("/home/elasticdump/advertisement_1_0.2014-12-19.mapping.json.gz");
                        GetObjectRequest request = new GetObjectRequest
                        {
                            BucketName = bucketName,
                            Key = entry.Key
                        };

                        GetPreSignedUrlRequest request1 = new GetPreSignedUrlRequest
                        {
                            BucketName = bucketName,
                            Key = "/home/elasticdump/advertisement_1_0.2014-12-19.mapping.json.gz",
                            Expires = DateTime.Now.AddMinutes(3600)
                        };

                        string presignedUrl = s3Client.GetPreSignedURL(request1);
                        GetObjectResponse response = s3Client.GetObject(request);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
