﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.CloudSearch.DomainModel
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<Search.Commands.Module>();
           
        }
    }
}
