﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text.RegularExpressions;
using System.Threading;
using Amazon;
using Amazon.CloudSearchDomain;
using Amazon.CloudSearchDomain.Model;
using Amazon.S3;
using Amazon.S3.Model;
using FirstLook.CloudSearch.DomainModel.DataUpload.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;

namespace FirstLook.CloudSearch.DomainModel.DataUpload.Command.Impl
{
    public class UploadToCloudSearchCommand : ICommand<List<List<CloudSearchDocument>>, string>
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public IQueue RequestQueue { get; internal set; }
        private ManualResetEvent Event;
        private int TotalThreads;
        #region ICommand<CloudSearchDocument,CloudSearch> Members

        public List<List<CloudSearchDocument>> Execute(string type)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            var commandFactory = resolver.Resolve<ICommandFactory>();

            var readS3Command = commandFactory.CreateReadS3DataCommand();

            Dictionary<string, string> s3files = readS3Command.Execute("");

            List<List<CloudSearchDocument>> cloudSearchDocs = new List<List<CloudSearchDocument>>();

            //Change Scalling to Large
            //string cloudSearchDomainName = ConfigurationManager.AppSettings["DomainName"];

            //Change Scalling to Large
            //ScalingParameters scallingParam=new ScalingParameters(){DesiredInstanceType=PartitionInstanceType.SearchM1Large,DesiredReplicationCount=0};
            //UpdateScalingParametersRequest updateScallingrequest = new UpdateScalingParametersRequest() { DomainName = ConfigurationManager.AppSettings["DocumentEndPoint"], ScalingParameters = scallingParam };

            //AmazonCloudSearchConfig cloudSearchConfig = new AmazonCloudSearchConfig() { RegionEndpoint = RegionEndpoint.USEast1, ServiceURL = ConfigurationManager.AppSettings["DocumentEndPoint"] };

            //AmazonCloudSearchClient awscloudSearchClient = new AmazonCloudSearchClient(cloudSearchConfig);
            //UpdateScalingParametersResponse updateScalingResponse=awscloudSearchClient.UpdateScalingParameters(updateScallingrequest);

            try
            {
                foreach (string s3key in s3files.Keys)
                {
                    byte[] buffer = new byte[Helper.MaxBatchSize];
                    string filedata = "";
                    using (FileStream fs = File.Open(s3files[s3key], FileMode.Open, FileAccess.Read))
                    using (BufferedStream bs = new BufferedStream(fs))
                    {
                        var memoryStream = new MemoryStream(buffer);
                        var stream = new StreamReader(memoryStream);
                        while (bs.Read(buffer, 0, Helper.MaxBatchSize) != 0)
                        {
                            RequestQueue = resolver.Resolve<IQueue>();

                            filedata = filedata + System.Text.Encoding.Default.GetString(buffer);

                            if (filedata.StartsWith(",")) { filedata = filedata.Substring(1); }

                            List<string> data = Regex.Split(filedata, "\r\n").ToList();
                            filedata = data[data.Count - 1];
                            data.RemoveAt(data.Count - 1);
                            data.RemoveAll(dt => dt == "[" || dt == "]");

                            string datatoupload = "[" + string.Join("", data) + "]";

                            if (datatoupload!=null)
                                RequestQueue.Push(datatoupload);//push string to Queue for uploading.

                            //If queue size is that of Thread then start processing upload
                            if (RequestQueue.Count() == Helper.ThreadSize)
                            {
                                Event = new ManualResetEvent(false);
                                TotalThreads = 0;
                                while (RequestQueue.Count() > 0)
                                {
                                    string request = RequestQueue.Pop();                                    
                                    ThreadPool.QueueUserWorkItem(new WaitCallback(ProcessUpload), request);
                                }

                                Event.WaitOne();
                            }
                        }
                        //Process pending requestss
                        while (RequestQueue.Count() > 0)
                        {
                            string request = RequestQueue.Pop();
                            ThreadPool.QueueUserWorkItem(new WaitCallback(ProcessUpload), request);
                        }

                        Event.WaitOne();
                    }
                    //delete file
                    new FileInfo(s3files[s3key]).Delete();

                    MoveAmazonObject(s3key);
                }
                                
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("Error Uploading file: {0}", ex.Message));
                throw ex;
            }
           
            return cloudSearchDocs;
        }

        #endregion


        private void ProcessUpload(object request)
        {
            string s3Data = Convert.ToString(request);
            try
            {
                List<AmazonS3Document> data = new List<AmazonS3Document>();
                //Convert string to Json.
                DataContractJsonSerializer json_serializer = new DataContractJsonSerializer(typeof(List<AmazonS3Document>));
                data = json_serializer.ReadObject(GenerateStreamFromString(s3Data)) as List<AmazonS3Document>;
                //Convert S3 object to Object that needs to be uploaded.
                List<CloudSearchDocument> cloudSearchDocuments = Mapper.Map(data, AmazonCloudSearchDataOperationType.AddData);

                MemoryStream stream2 = new MemoryStream();
                //Convert documents to Json
                DataContractJsonSerializer dataContractJsonSerializer = new DataContractJsonSerializer(typeof(List<CloudSearchDocument>));
                dataContractJsonSerializer.WriteObject(stream2, cloudSearchDocuments);

                stream2.Flush();
                stream2.Position = 0;

                //Start uploading
                AmazonCloudSearchDomainConfig amazonCloudSearchDomainConfig = new AmazonCloudSearchDomainConfig();
                amazonCloudSearchDomainConfig.RegionEndpoint = RegionEndpoint.USEast1;
                amazonCloudSearchDomainConfig.ServiceURL = Helper.DocumentEndPoint;                

                AmazonCloudSearchDomainClient client = new AmazonCloudSearchDomainClient(amazonCloudSearchDomainConfig);
                UploadDocumentsRequest documentRequest = new UploadDocumentsRequest();

                documentRequest.Documents = stream2;
                documentRequest.ContentType = ContentType.ApplicationJson;
                UploadDocumentsResponse response = client.UploadDocuments(documentRequest);

            }           
            catch (DocumentServiceException ex)
            {
                Log.Error(string.Format("Error while uploading file: {0}. Data {1}", ex.Message, s3Data));
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                //RequestQueue.Push(s3Data);
            }
            catch (AmazonCloudSearchDomainException ex)
            {
                Log.Error(string.Format("Error while uploading file: {0}. Data {1}", ex.Message, s3Data));
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("Error while processing Upload file: {0}. Data {1}", ex.Message, s3Data));
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
            finally
            {
                TotalThreads++;
                if (RequestQueue.Count()==0)
                {
                    Event.Set();
                }
            }
        }

        private void MoveAmazonObject(string key)
        {
            try
            {
                using (AmazonS3Client s3Client = new AmazonS3Client(RegionEndpoint.USEast1))
                {


                    CopyObjectRequest copyRequest = new CopyObjectRequest()
                    {
                        SourceBucket = Helper.BucketName,
                        DestinationBucket = Helper.BucketName,
                        SourceKey = key,
                        DestinationKey = Helper.GetArchiveS3Key(key)
                    };
                    CopyObjectResponse copyResponse = s3Client.CopyObject(copyRequest);

                    DeleteObjectRequest deleteRequest = new DeleteObjectRequest()
                    {
                        BucketName = Helper.BucketName,
                        Key = key
                    };

                    DeleteObjectResponse deleteResponse = s3Client.DeleteObject(deleteRequest);

                }
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null && (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") || amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    Log.Error("Error copying Appraisal photos from S3 Bucket - " + Helper.BucketName + ":  Incorrect AWS Credentials");
                    throw new Exception(amazonS3Exception.Message);//UserAccessException("AWS", "Forbidden", "Incorrect AWS Credentials.");
                }
                else
                {
                    Log.Error("Exception copying Appraisal photos from S3 Bucket - " + Helper.BucketName, amazonS3Exception);
                    throw new Exception(amazonS3Exception.Message);//FirstlookGeneralException("Errorcode " + amazonS3Exception.ErrorCode + " when writing to S3 Bucket: " + bucketName + " - Error message = " + amazonS3Exception.Message);
                }
            }
        }

        private Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }
}
