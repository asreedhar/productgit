﻿
namespace FirstLook.CloudSearch.DomainModel.DataUpload.Command.Impl
{
    public class CommandFactory : ICommandFactory
    {

        #region ICommandFactory Members

        public ReadS3DataCommand CreateReadS3DataCommand()
        {
            return new ReadS3DataCommand();
        }

        public UploadToCloudSearchCommand CreateUploadToCloudSearchCommand()
        {
            return new UploadToCloudSearchCommand();
        }

        #endregion
    }
}
