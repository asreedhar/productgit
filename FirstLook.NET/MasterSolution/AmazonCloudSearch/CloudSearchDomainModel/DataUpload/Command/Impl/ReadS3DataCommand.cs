﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Threading;
using Amazon.S3;
using Amazon.S3.Model;
using FirstLook.Common.Core.Command;

namespace FirstLook.CloudSearch.DomainModel.DataUpload.Command.Impl
{
    public class ReadS3DataCommand : ICommand<Dictionary<string, string>, string>
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private ManualResetEvent Event;

        private const string GzipExtension = ".gz";
        private const string TextFileExtension = ".txt";
        private const string TempFileExtension = ".tmp";

        List<S3File> s3Files = new List<S3File>();
        #region ICommand<CloudSearch,string> Members

        public Dictionary<string, string> Execute(string parameters)
        {
            Dictionary<string, string> files = new Dictionary<string, string>();

            try
            {
                using (AmazonS3Client s3Client = new AmazonS3Client(Amazon.RegionEndpoint.USEast1))
                {
                    ListObjectsRequest listRequest = new ListObjectsRequest() { BucketName = Helper.BucketName };
                    ListObjectsResponse listResponse = s3Client.ListObjects(listRequest);
                    
                    Event = new ManualResetEvent(false);

                    foreach (S3Object entry in listResponse.S3Objects)
                    {
                        if (!entry.Key.StartsWith(Helper.BasePath) && entry.Key.EndsWith("data.json.gz") && entry.Key.EndsWith("advertisement_1_0.2015-01-19.data.json.gz"))
                        {
                            s3Files.Add(new S3File() { key = entry.Key, FileSize = entry.Size });
                        }
                    }
                }

                foreach (var s3File in s3Files)
                {
                    files.Add(s3File.key, OutputFilePath(s3File.key));
                    ThreadPool.QueueUserWorkItem(new WaitCallback(GetFilesFromS3), s3File);
                    Event.WaitOne();
                }

                Event = new ManualResetEvent(false);
                foreach (var s3file in s3Files)
                {
                    ThreadPool.QueueUserWorkItem(new WaitCallback(DecpmpressgzipFiles), s3file.key);
                    Event.WaitOne();
                }

                DeleteFiles(GzipExtension);

                return files;

            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null && (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") || amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    Log.Error("Error reading Appraisal to S3 Bucket - " + Helper.BucketName + ":  Incorrect AWS Credentials");
                    //throw new UserAccessException("AWS", "Forbidden", "Incorrect AWS Credentials.");
                    
                    throw new Exception();
                }
                else
                {
                    Log.Error("Exception reading Appraisal to S3 Bucket - " + Helper.BucketName, amazonS3Exception);
                    //throw new FirstlookGeneralException("Errorcode " + amazonS3Exception.ErrorCode + " when writing to S3 Bucket: " + bucketName + " - Error message = " + amazonS3Exception.Message);
                    throw new Exception();
                }
                Console.WriteLine(amazonS3Exception.Message);
            }

        }

        #endregion

        private void GetFilesFromS3(object key)
        {
            try
            {

                S3File s3File = (S3File)(key);
                Log.Info(string.Format("Downloading file {0} at {1}",key,DateTime.Now));
                using (AmazonS3Client s3Client = new AmazonS3Client(Amazon.RegionEndpoint.USEast1))
                {
                    int startByteRange = 0;

                    HttpStatusCode currentStatus;
                    long maxBuffer = 0;
                    //retrive files partially
                    do
                    {
                        maxBuffer= (startByteRange)*Helper.MaxBatchSize;
                        ByteRange byteRange = new ByteRange(startByteRange * Helper.MaxBatchSize + 1, maxBuffer > s3File.FileSize?s3File.FileSize-1:maxBuffer);

                        GetObjectRequest request = new GetObjectRequest
                        {
                            BucketName = Helper.BucketName,
                            Key = s3File.key,
                            ByteRange = byteRange
                        };

                        using (GetObjectResponse response = s3Client.GetObject(request))
                        {

                            using (Stream responseStream = response.ResponseStream)
                            {
                                if (!File.Exists(FilePath(s3File.key)))
                                {
                                    response.WriteResponseStreamToFile(FilePath(s3File.key));
                                }
                            }
                           
                            currentStatus = response.HttpStatusCode;

                        }
                        startByteRange++;
                    } while (currentStatus == System.Net.HttpStatusCode.PartialContent && maxBuffer < s3File.FileSize);

                }
                Log.Info(string.Format("Downloading completed for file {0} at {1}", key, DateTime.Now));
                //MergeFilesToSingleFile(Helper.LocalPath, TempFileExtension, FilePath(s3File.key));

            }
            catch (Exception ex)
            {
                Log.Error(string.Format("Error Downloading S3 file:{0}. {1}",key , ex.Message));
                throw ex;
            }
            finally
            {
                if (GetTotalFilesInBase(GzipExtension) == s3Files.Count)
                {
                    Event.Set();
                }
            }

        }

        private void DecpmpressgzipFiles(object key)
        {
            string s3key = Convert.ToString(key);
            Log.Info(string.Format("Decompressing file {0} at {1}", key, DateTime.Now));
            using (GZipStream instream = new GZipStream(File.OpenRead(FilePath(s3key)), CompressionMode.Decompress))// ArgumentException...
            {
                using (FileStream outputStream = new FileStream(OutputFilePath(s3key), FileMode.Append, FileAccess.Write))
                {
                    int bufferSize = 8192, bytesRead = 0;
                    byte[] buffer = new byte[bufferSize];
                    while ((bytesRead = instream.Read(buffer, 0, bufferSize)) > 0)
                    {
                        outputStream.Write(buffer, 0, bytesRead);
                    }
                }
            }
            Log.Info(string.Format("Decompressing completed file {0} at {1}", key, DateTime.Now));
            if (GetTotalFilesInBase(TextFileExtension) == s3Files.Count)
            {
                Event.Set();
            }
        }

        private string FilePath(string key)
        {
            return string.Format(@"{0}\{1}", Helper.LocalPath, key.Replace(Helper.BasePath, ""));
        }
        private string OutputFilePath(string key)
        {
            return string.Format(@"{0}\{1}", Helper.LocalPath, key.Replace(Helper.BasePath, "").Replace(GzipExtension, TextFileExtension));
        }
        private string TempFilePath(string key, int count)
        {
            return string.Format(@"{0}\{1}", Helper.LocalPath, key.Replace(Helper.BasePath, "") + Convert.ToString(count) + TempFileExtension);
        }

        private int GetTotalFilesInBase(string extension)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(Helper.LocalPath);

            return directoryInfo.GetFiles("*" + extension).Length;
        }

        private void DeleteFiles(string extension)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(Helper.LocalPath);

            foreach (FileInfo file in directoryInfo.GetFiles("*" + extension))
            {
                file.Delete();
            }

        }

        private void MergeFilesToSingleFile(string dirPath, string filePattern, string destFile)
        {
            List<string> files = Directory.GetFiles(dirPath, "*" + filePattern).ToList();
            
            using (var destStream = File.Create(destFile))
            {
                foreach (string filePath in files)
                {
                    using (var sourceStream = File.OpenRead(filePath))
                        sourceStream.CopyTo(destStream);

                    new FileInfo(filePath).Delete();
                }
            }
        }
    }

    class S3File
    {
        public string key { get; set; }
        public long FileSize { get; set; }
    }
}
