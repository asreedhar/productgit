﻿using FirstLook.CloudSearch.DomainModel.DataUpload.Command.Impl;

namespace FirstLook.CloudSearch.DomainModel.DataUpload.Command
{
    public interface ICommandFactory
    {
        ReadS3DataCommand CreateReadS3DataCommand();
        UploadToCloudSearchCommand CreateUploadToCloudSearchCommand();
    }
}
