﻿using FirstLook.CloudSearch.DomainModel.DataUpload.Command.Impl;
using FirstLook.CloudSearch.DomainModel.DataUpload.Model;
using FirstLook.Common.Core.Registry;

namespace FirstLook.CloudSearch.DomainModel.DataUpload.Command
{
    public class Module : IModule
    {
        #region IModule Members

        /// <summary>
        /// Add pertinent interfaces and impelementing classes to the registry.
        /// </summary>
        public void Configure(IRegistry registry)
        {
            registry.Register<IQueue, MemoryQueue>(ImplementationScope.Shared);
            registry.Register<ICommandFactory, CommandFactory>(ImplementationScope.Shared);
        }

        #endregion

    }
}
