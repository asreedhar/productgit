﻿using System;
using System.Configuration;

namespace FirstLook.CloudSearch.DomainModel.DataUpload.Command
{
    public static class Helper
    {
        public static string BucketName { get { return ConfigurationManager.AppSettings["Bucket"]; } }
        public static string KeyName { get { return ConfigurationManager.AppSettings["S3FileName"]; } }
        public static string BasePath { get { return ConfigurationManager.AppSettings["filebasepath"]; } }
        public static string LocalPath { get { return ConfigurationManager.AppSettings["localpath"]; } }
        public static int MaxBatchSize { get { return Convert.ToInt32(ConfigurationManager.AppSettings["MaxDocumentBatchSize"]); } }
        public static int ThreadSize { get { return Convert.ToInt32(ConfigurationManager.AppSettings["ThreadSize"]); } }
        public static string DocumentEndPoint { get { return ConfigurationManager.AppSettings["DocumentEndPoint"]; } }
        public static string SearchEndPoint { get { return ConfigurationManager.AppSettings["SearchEndPoint"]; } }

        public static string PartialDownloadBufferSize { get { return ConfigurationManager.AppSettings["PartialDownloadFileSize"]; } }
        

        public static string GetArchiveS3Key(string filename) { 
            return string.Format("{0}{1}",ConfigurationManager.AppSettings["S3DocArchiveFolder"],filename); 
        }

    }
}
