﻿using System.Runtime.Serialization;

namespace FirstLook.CloudSearch.DomainModel.DataUpload.Model
{
    [DataContract]
    public class ProductEquipmentItem
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "tier")]
        public int Tier { get; set; }
    }
}
