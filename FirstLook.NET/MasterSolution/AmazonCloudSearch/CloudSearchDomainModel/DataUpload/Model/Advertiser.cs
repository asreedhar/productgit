﻿using System.Runtime.Serialization;

namespace FirstLook.CloudSearch.DomainModel.DataUpload.Model
{
    [DataContract]
   public class Advertiser
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "address")]
        public Address Address { get; set; }

        [DataMember(Name="emailAddress")]
        public string EmailAddress { get; set; }

        [DataMember(Name="faxNumber")]
        public string FaxNumber { get; set; }

        [DataMember(Name="phoneNumber")]
        public string PhoneNumber { get; set; }

        [DataMember(Name="websiteUrl")]
        public string WebsiteUrl { get; set; }

    }
}
