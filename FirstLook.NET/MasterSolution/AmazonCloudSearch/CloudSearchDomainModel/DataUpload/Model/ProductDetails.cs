﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.CloudSearch.DomainModel.DataUpload.Model
{
    [DataContract]
    public class ProductDetails
    {
        [DataMember(Name = "candidateChromeStyles")]
        public List<int> CandidateChromeStyles { get; set; }

        [DataMember(Name = "manufacturer")]
        public string Manufacturer { get; set; }

        [DataMember(Name="marketClass")]
        public string MarketClass { get; set; }

        [DataMember(Name = "make")]
        public string Make { get; set; }

        [DataMember(Name = "model")]
        public string Model { get; set; }

        [DataMember(Name = "modelYear")]
        public int? ModelYear { get; set; }

        [DataMember(Name = "trim")]
        public string Trim { get; set; }
        
        [DataMember(Name = "style")]
        public string Style { get; set; }

        [DataMember(Name = "bodyStyle")]
        public string BodyStyle { get; set; }
        
        [DataMember(Name = "transmissionDescription")]
        public string TransmissionDescription { get; set; }

        [DataMember(Name = "transmissionType")]
        public string TransmissionType { get; set; }

        [DataMember(Name = "transmissionGearSelectionMechanism")]
        public List<string> TransmissionGearSelectionMechanism { get; set; }

        [DataMember(Name="transmissionGearCount")]
        public byte? TransmissionGearCount { get; set; }

        [DataMember(Name = "engineDescription")]
        public string EngineDescription { get; set; }

        [DataMember(Name = "engineFuelType")]
        public List<string> EngineFuelType { get; set; }

        [DataMember(Name = "engineCylinderCount")]
        public byte? EngineCylinderCount { get; set; }

        [DataMember(Name = "engineCylinderLayout")]
        public string EngineCylinderLayout { get; set; }

        [DataMember(Name = "engineDisplacementInLiters")]
        public double? EngineDisplacementInLiters { get; set; }

        [DataMember(Name="engineDisplacementInCubicInches")]
        public double? EngineDisplacementInCubicInches { get; set; }

        [DataMember(Name = "drivetrainDescription")]
        public string DriveTrainDescription { get; set; }

        [DataMember(Name = "drivetrainType")]
        public string DriveTrainType { get; set; }

        [DataMember(Name="airCompressor")]
        public string AirCompressor { get; set; }

        [DataMember(Name = "exteriorOneColorManufacturerName")]
        public string ExteriorOneColorManufacturerName { get; set; }

        [DataMember(Name = "exteriorOneColorGenericName")]
        public string ExteriorOneColorGenericName { get; set; }

        [DataMember(Name = "exteriorOneColorRgbHex")]
        public string ExteriorOneColorRgbHex { get; set; }

        [DataMember(Name="exteriorTwoColorGenericName")]
        public string ExteriorTwoColorGenericName { get; set; }

        [DataMember(Name="exteriorTwoColorManufacturerName")]
        public string ExteriorTwoColorManufacturerName { get; set; }

        [DataMember(Name = "exteriorTwoColorRgbHex")]
        public string ExteriorTwoColorRgbHex { get; set; }

        [DataMember(Name = "interiorColorManufacturerName")]
        public string InteriorColorManufacturerName { get; set; }

        [DataMember(Name = "fuelEconomyCity")]
        public double? FuelEconomyCity { get; set; }

        [DataMember(Name = "fuelEconomyHighway")]
        public double? FuelEconomyHighway { get; set; }

        [DataMember(Name="fuelEconomyCombined")]
        public double? FuelEconomyCombined { get; set; }

        [DataMember(Name = "passengerCapacity")]
        public byte? PassengerCapacity { get; set; }

        [DataMember(Name = "passengerDoors")]
        public byte? PassengerDoors { get; set; }

        [DataMember(Name="drivetrainAxle")]
        public string DriveTrainAxle { get; set; }

        [DataMember(Name="interiorColorRgbHex")]
        public string InteriorColorRgbHex { get; set; }


    }
}
