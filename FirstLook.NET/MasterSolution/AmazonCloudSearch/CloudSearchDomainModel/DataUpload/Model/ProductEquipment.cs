﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.CloudSearch.DomainModel.DataUpload.Model
{
    [DataContract]
    public class ProductEquipment
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "ordinal")]
        public string Ordinal { get; set; }

        [DataMember(Name = "items")]
        public List<ProductEquipmentItem> ProductEquipmentList { get; set; } 
    }
}
