﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace FirstLook.CloudSearch.DomainModel.DataUpload.Model
{
    [DataContract]
    public class PublicationEventLog
    {
        [DataMember(Name = "creationTime")]
        public string CreationTimeJson { get; set; }

        [DataMember(Name = "lastWriteTime")]
        public string LastWriteTimeJson { get; set; }

        [DataMember(Name = "events")]
        public List<PublicationEvents> PublicationEventList { get; set; }


        [IgnoreDataMember]
        public DateTime CreationTime
        {
            // Replace "o" with whichever DateTime format specifier you need.
            get { return DateTime.Parse(CreationTimeJson, null, DateTimeStyles.RoundtripKind); }
            set { CreationTimeJson = value.ToString("mm/dd/yyyy"); }
        }

        [IgnoreDataMember]
        public DateTime LastWriteTime
        {
            // Replace "o" with whichever DateTime format specifier you need.
            get { return DateTime.Parse(LastWriteTimeJson, null, DateTimeStyles.RoundtripKind); }
            set { LastWriteTimeJson = value.ToString("mm/dd/yyyy"); }
        }
    }
}
