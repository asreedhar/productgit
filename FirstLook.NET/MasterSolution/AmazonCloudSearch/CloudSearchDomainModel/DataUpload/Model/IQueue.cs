

namespace FirstLook.CloudSearch.DomainModel.DataUpload.Model
{
    /// <summary>
    /// Interface that defines a string queue.
    /// </summary>
    public interface IQueue
    {
        /// <summary>
        /// Push a string onto the queue. Must be thread safe.
        /// </summary>
        /// <param name="BookRequest">string to push onto the queue.</param>
        void Push(string data);

        /// <summary>
        /// Pop a string off the queue. Must be thread safe.
        /// </summary>
        /// <returns>string popped off the queue.</returns>
        string Pop();

        /// <summary>
        /// Get the number of entries on the queue. Must be thread safe.
        /// </summary>
        /// <returns>Queue size.</returns>
        int Count();
    }
}
