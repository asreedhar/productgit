﻿using System;
using System.Globalization;
using System.Runtime.Serialization;

namespace FirstLook.CloudSearch.DomainModel.DataUpload.Model
{
    [DataContract]
    public class PublicationEvents
    {
        [DataMember(Name = "eventTime")]
        public string EventTimeJson { get; set; }

        [DataMember(Name = "eventData")]
        public string EventData { get; set; }

        [DataMember(Name = "eventSource")]
        public string EventSource { get; set; }


        [IgnoreDataMember]
        public DateTime EventTime
        {
            // Replace "o" with whichever DateTime format specifier you need.
            get { return DateTime.Parse(EventTimeJson, null, DateTimeStyles.RoundtripKind); }
            set { EventTimeJson = value.ToString("mm/dd/yyyy"); }
        }

    }
}
