using System.Collections.Generic;



namespace FirstLook.CloudSearch.DomainModel.DataUpload.Model
{
    /// <summary>
    /// In-memory string queue.
    /// </summary>
    public class MemoryQueue : IQueue
    {
        /// <summary>
        /// string queue.
        /// </summary>
        static private readonly Queue<string> _queue = new Queue<string>();

        /// <summary>
        /// Push a string onto the queue. Thread safe.
        /// </summary>
        /// <param name="string">string to push onto the queue.</param>
        public void Push(string request)
        {
            lock (_queue)
            {
                _queue.Enqueue(request);
            }
        }

        /// <summary>
        /// Pop a string off the queue. Thread safe.
        /// </summary>
        /// <returns>string popped off the queue. Null if the queue is empty.</returns>
        public string Pop()
        {
            string request = null;
            lock (_queue)
            {
                if (_queue.Count > 0)
                {
                    request = _queue.Dequeue();
                }
            }
            return request;
        }

        /// <summary>
        /// Get the number of entries on the queue. Thread safe.
        /// </summary>
        /// <returns>Queue size.</returns>
        public int Count()
        {
            int count;

            lock (_queue)
            {
                count = _queue.Count;
            }
            return count;
        }
    }
}
