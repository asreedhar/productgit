﻿
using System.Collections.Generic;
using System.Linq;
using System;

namespace FirstLook.CloudSearch.DomainModel.DataUpload.Model
{
    public static class Mapper
    {
        public static List<CloudSearchDocument> Map(List<AmazonS3Document> amazonS3Documents,string type)
        {
            List<CloudSearchDocument> CloudSearchDocuments = new List<CloudSearchDocument>();
            foreach (AmazonS3Document amazonS3Document in amazonS3Documents)
            {
                List<string> ExteriorItems = new List<string>();
                List<string> InteriorItems = new List<string>();
                List<string> SafetyItems = new List<string>();
                List<string> UnderTheHoodItems = new List<string>();
                List<string> TechnologyItems = new List<string>();

                List<ProductEquipment> productEquipment = new List<ProductEquipment>();
                CloudSearchFields cloudSearchFields = null;

                if (amazonS3Document.Vehicle != null)
                {
                    if (amazonS3Document.Vehicle.ProductEquipment != null)
                    {
                        productEquipment = amazonS3Document.Vehicle.ProductEquipment.Where(pe => pe.Name == "Exterior").ToList();
                        if (productEquipment != null && productEquipment.Count>0)
                            productEquipment.FirstOrDefault().ProductEquipmentList.ForEach(pel => ExteriorItems.Add(pel.Name));

                        productEquipment = amazonS3Document.Vehicle.ProductEquipment.Where(pe => pe.Name == "Interior").ToList();
                        if (productEquipment != null && productEquipment.Count > 0)
                            productEquipment.FirstOrDefault().ProductEquipmentList.ForEach(pel => InteriorItems.Add(pel.Name));

                        productEquipment = amazonS3Document.Vehicle.ProductEquipment.Where(pe => pe.Name == "Safety").ToList();
                        if (productEquipment != null && productEquipment.Count > 0)
                            productEquipment.FirstOrDefault().ProductEquipmentList.ForEach(pel => SafetyItems.Add(pel.Name));

                        productEquipment = amazonS3Document.Vehicle.ProductEquipment.Where(pe => pe.Name == "Under the Hood").ToList();
                        if (productEquipment != null && productEquipment.Count > 0)
                            productEquipment.FirstOrDefault().ProductEquipmentList.ForEach(pel => UnderTheHoodItems.Add(pel.Name));

                        productEquipment = amazonS3Document.Vehicle.ProductEquipment.Where(pe => pe.Name == "Technology").ToList();
                        if (productEquipment != null && productEquipment.Count > 0)
                            productEquipment.FirstOrDefault().ProductEquipmentList.ForEach(pel => TechnologyItems.Add(pel.Name));
                    }
                    
                    cloudSearchFields = new CloudSearchFields()
                                                         {
                                                             AdvertiserAddressCity = amazonS3Document.Vehicle.Advertiser!=null && amazonS3Document.Vehicle.Advertiser.Address!=null ? amazonS3Document.Vehicle.Advertiser.Address.City:null,
                                                             AdvertiserAddressCountry = amazonS3Document.Vehicle.Advertiser!=null && amazonS3Document.Vehicle.Advertiser.Address!=null ? amazonS3Document.Vehicle.Advertiser.Address.Country:null,
                                                             AdvertiserAddressCounty = amazonS3Document.Vehicle.Advertiser!=null && amazonS3Document.Vehicle.Advertiser.Address!=null ?amazonS3Document.Vehicle.Advertiser.Address.County:null,
                                                             AdvertiserAddressLine1 = amazonS3Document.Vehicle.Advertiser!=null && amazonS3Document.Vehicle.Advertiser.Address!=null ?amazonS3Document.Vehicle.Advertiser.Address.Line1:null,
                                                             AdvertiserAddressLocation = amazonS3Document.Vehicle.Advertiser!=null && amazonS3Document.Vehicle.Advertiser.Address!=null ?amazonS3Document.Vehicle.Advertiser.Address.Location:null,
                                                             AdvertiserAddressPostalcode = amazonS3Document.Vehicle.Advertiser!=null && amazonS3Document.Vehicle.Advertiser.Address!=null ?amazonS3Document.Vehicle.Advertiser.Address.PostalCode:null,
                                                             AdvertiserAddressState = amazonS3Document.Vehicle.Advertiser!=null && amazonS3Document.Vehicle.Advertiser.Address!=null ?amazonS3Document.Vehicle.Advertiser.Address.State:null,
                                                             AdvertiserName = amazonS3Document.Vehicle.Advertiser!=null ?amazonS3Document.Vehicle.Advertiser.Name:null,
                                                             BodyStyle = amazonS3Document.Vehicle.ProductDetails!=null? amazonS3Document.Vehicle.ProductDetails.BodyStyle:null,
                                                             CandidateChromecStyles =  amazonS3Document.Vehicle.ProductDetails!=null? amazonS3Document.Vehicle.ProductDetails.CandidateChromeStyles:null,
                                                             DatePublished = null,
                                                             DrivetrainDescription =  amazonS3Document.Vehicle.ProductDetails!=null? amazonS3Document.Vehicle.ProductDetails.DriveTrainDescription:null,
                                                             DrivetrainType =  amazonS3Document.Vehicle.ProductDetails!=null? amazonS3Document.Vehicle.ProductDetails.DriveTrainType:null,
                                                             EngineCylinderCount =  amazonS3Document.Vehicle.ProductDetails!=null? amazonS3Document.Vehicle.ProductDetails.EngineCylinderCount:null,
                                                             EngineCylinderLayout = amazonS3Document.Vehicle.ProductDetails!=null?amazonS3Document.Vehicle.ProductDetails.EngineCylinderLayout:null,
                                                             EngineDescription = amazonS3Document.Vehicle.ProductDetails!=null?amazonS3Document.Vehicle.ProductDetails.EngineDescription:null,
                                                             EngineFuelType = amazonS3Document.Vehicle.ProductDetails!=null?amazonS3Document.Vehicle.ProductDetails.EngineFuelType:null,
                                                             EngineDisplacementInliters = amazonS3Document.Vehicle.ProductDetails!=null?amazonS3Document.Vehicle.ProductDetails.EngineDisplacementInLiters:null,
                                                             ExteriorItems = ExteriorItems,
                                                             ExteriorOneColorGenericName = amazonS3Document.Vehicle.ProductDetails!=null?amazonS3Document.Vehicle.ProductDetails.ExteriorOneColorGenericName:null,
                                                             ExteriorOneColorManufacturerName = amazonS3Document.Vehicle.ProductDetails!=null?amazonS3Document.Vehicle.ProductDetails.ExteriorOneColorManufacturerName:null,
                                                             ExteriorOneColorRgbHex = amazonS3Document.Vehicle.ProductDetails!=null?amazonS3Document.Vehicle.ProductDetails.ExteriorOneColorRgbHex:null  ,
                                                             FuelEconomyCity = amazonS3Document.Vehicle.ProductDetails!=null?amazonS3Document.Vehicle.ProductDetails.FuelEconomyCity:null,
                                                             FuelEconomyHighway = amazonS3Document.Vehicle.ProductDetails!=null?amazonS3Document.Vehicle.ProductDetails.FuelEconomyHighway:null,
                                                             InteriorColorManufacturerName = amazonS3Document.Vehicle.ProductDetails!=null?amazonS3Document.Vehicle.ProductDetails.InteriorColorManufacturerName:null,
                                                             InteriorItems = InteriorItems,
                                                             IsPublicationOnline = amazonS3Document.Vehicle.PublicationInformation.IsOnline,
                                                             Make = amazonS3Document.Vehicle.ProductDetails!=null?amazonS3Document.Vehicle.ProductDetails.Make:null,
                                                             Manufacturer = amazonS3Document.Vehicle.ProductDetails!=null?amazonS3Document.Vehicle.ProductDetails.Manufacturer:null,
                                                             Model = amazonS3Document.Vehicle.ProductDetails!=null?amazonS3Document.Vehicle.ProductDetails.Model:null,
                                                             ModelYear = amazonS3Document.Vehicle.ProductDetails!=null?amazonS3Document.Vehicle.ProductDetails.ModelYear:null,
                                                             OdometerReading = amazonS3Document.Vehicle.ProductInformation!=null?amazonS3Document.Vehicle.ProductInformation.OdometerReading:null,
                                                             PassengerCapacity = amazonS3Document.Vehicle.ProductDetails!=null?amazonS3Document.Vehicle.ProductDetails.PassengerCapacity:null,
                                                             PassengerDoors = amazonS3Document.Vehicle.ProductDetails!=null?amazonS3Document.Vehicle.ProductDetails.PassengerDoors:null,
                                                             Price = amazonS3Document.Vehicle.ProductInformation!=null?amazonS3Document.Vehicle.ProductInformation.Price:null,
                                                             SafetyItems = SafetyItems,
                                                             StockNumber = amazonS3Document.Vehicle.ProductInformation!=null?amazonS3Document.Vehicle.ProductInformation.StockNumber:null,
                                                             Style = amazonS3Document.Vehicle.ProductDetails!=null?amazonS3Document.Vehicle.ProductDetails.Style:null,
                                                             TechnologyItems = TechnologyItems,
                                                             TransmissionDescription = amazonS3Document.Vehicle.ProductDetails!=null?amazonS3Document.Vehicle.ProductDetails.TransmissionDescription:null,
                                                             TransmissionGearSelectionMechanism = amazonS3Document.Vehicle.ProductDetails!=null?amazonS3Document.Vehicle.ProductDetails.TransmissionGearSelectionMechanism:null,
                                                             TransmissionType = amazonS3Document.Vehicle.ProductDetails!=null?amazonS3Document.Vehicle.ProductDetails.TransmissionType:null,
                                                             Trim = amazonS3Document.Vehicle.ProductDetails!=null?amazonS3Document.Vehicle.ProductDetails.Trim:null,
                                                             UnderTheHoodItems = UnderTheHoodItems,
                                                             VehicleId = amazonS3Document.Vehicle.VehicleId,
                                                             Vin = amazonS3Document.Vehicle.Vin,
                                                             AdvertiserAddressLine2 = amazonS3Document.Vehicle.Advertiser != null && amazonS3Document.Vehicle.Advertiser.Address != null ? amazonS3Document.Vehicle.Advertiser.Address.Line2 : null,
                                                             AdvertiserAddressEmail= amazonS3Document.Vehicle.Advertiser != null ? amazonS3Document.Vehicle.Advertiser.EmailAddress : null,
                                                             AdvertiserAddressFax = amazonS3Document.Vehicle.Advertiser != null ? amazonS3Document.Vehicle.Advertiser.FaxNumber : null,
                                                             AdvertiserAddressPhone = amazonS3Document.Vehicle.Advertiser != null ? amazonS3Document.Vehicle.Advertiser.PhoneNumber : null,
                                                             AdvertiserAddressWebsite = amazonS3Document.Vehicle.Advertiser != null ? amazonS3Document.Vehicle.Advertiser.WebsiteUrl : null,
                                                             AirCompressor = amazonS3Document.Vehicle.ProductDetails != null ? amazonS3Document.Vehicle.ProductDetails.AirCompressor : null,
                                                             DrivetrainAxle = amazonS3Document.Vehicle.ProductDetails != null ? amazonS3Document.Vehicle.ProductDetails.DriveTrainAxle : null,
                                                             EngineDisplacementCubicInches = amazonS3Document.Vehicle.ProductDetails != null ? amazonS3Document.Vehicle.ProductDetails.EngineDisplacementInCubicInches : null,
                                                             ExteriorTwoColorGenericName = amazonS3Document.Vehicle.ProductDetails != null ? amazonS3Document.Vehicle.ProductDetails.ExteriorTwoColorGenericName : null,
                                                             ExteriorTwoColorManufacturerName = amazonS3Document.Vehicle.ProductDetails != null ? amazonS3Document.Vehicle.ProductDetails.ExteriorTwoColorManufacturerName : null,
                                                             ExteriorTwoColorRgbHex = amazonS3Document.Vehicle.ProductDetails != null ? amazonS3Document.Vehicle.ProductDetails.ExteriorTwoColorRgbHex : null,
                                                             FuelEconomyCombined = amazonS3Document.Vehicle.ProductDetails != null ? amazonS3Document.Vehicle.ProductDetails.FuelEconomyCombined : null,
                                                             TransmissionGearCount = amazonS3Document.Vehicle.ProductDetails != null ? amazonS3Document.Vehicle.ProductDetails.TransmissionGearCount : null,
                                                             StockType = amazonS3Document.Vehicle.ProductInformation != null ? amazonS3Document.Vehicle.ProductInformation.StockType : null

                                                         };
                }
                CloudSearchDocuments.Add(new CloudSearchDocument()
                {
                    Id = amazonS3Document.Id,
                    Type = type,
                    SearchDocumentFields = cloudSearchFields
                });
            }
            return CloudSearchDocuments;
        }
        
    }
}
