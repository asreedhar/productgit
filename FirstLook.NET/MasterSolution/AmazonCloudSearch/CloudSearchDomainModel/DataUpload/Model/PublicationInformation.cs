﻿using System;
using System.Globalization;
using System.Runtime.Serialization;

namespace FirstLook.CloudSearch.DomainModel.DataUpload.Model
{
    [DataContract]
   public class PublicationInformation
    {
        [DataMember(Name = "isOnline")]
        public bool IsOnline { get; set; }

        [DataMember(Name = "datePublished")]
        public string DatePublishedJson { get; set; }

        [DataMember(Name="dateWithdrawn")]
        public string DateWithdrawn { get; set; }

        
        [IgnoreDataMember]        
        public DateTime? DatePublished
        {
            // Replace "o" with whichever DateTime format specifier you need.
            get { return DateTime.Parse(DatePublishedJson, null, DateTimeStyles.RoundtripKind); }
            //set { DatePublishedJson = value.ToString("mm/dd/yyyy"); }
        }

    }
}
