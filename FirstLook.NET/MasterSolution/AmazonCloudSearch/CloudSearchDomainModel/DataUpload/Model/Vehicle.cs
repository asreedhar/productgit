﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.CloudSearch.DomainModel.DataUpload.Model
{
    [DataContract]
    public class Vehicle
    {
        [DataMember(Name = "vehicleId")]
        public int VehicleId { get; set; }

        [DataMember(Name = "vin")]
        public string Vin { get; set; }

        [DataMember(Name = "text")]
        public string Text { get; set; }

        [DataMember(Name = "advertiser")]
        public Advertiser Advertiser { get; set; }

        [DataMember(Name="images")]
        public string images { get; set; }

        [DataMember(Name = "productInformation")]
        public ProductInformation ProductInformation { get; set; }

        [DataMember(Name = "productDetails")]
        public ProductDetails ProductDetails { get; set; }

        [DataMember(Name = "productEquipment")]
        public List<ProductEquipment> ProductEquipment { get; set; }

        [DataMember(Name = "publicationInformation")]
        public PublicationInformation PublicationInformation { get; set; }

        [DataMember(Name = "publicationEventLog")]
        public PublicationEventLog PublicationEventLog { get; set; }
    }
}
