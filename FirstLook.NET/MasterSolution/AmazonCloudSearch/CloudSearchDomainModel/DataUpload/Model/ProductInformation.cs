﻿using System.Runtime.Serialization;

namespace FirstLook.CloudSearch.DomainModel.DataUpload.Model
{
    [DataContract]
    public class ProductInformation
    {
        [DataMember(Name = "stockNumber")]
        public string StockNumber { get; set; }
        
        [DataMember(Name="stockType")]
        public string StockType { get; set; }

        [DataMember(Name = "price")]
        public double? Price { get; set; }

        [DataMember(Name = "odometerReading")]
        public int? OdometerReading { get; set; }
    }
}
