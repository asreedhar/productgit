﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.CloudSearch.DomainModel.DataUpload.Model
{
    [DataContract]
    public class CloudSearchFields
    {
        [DataMember(Name = "vehicleid", EmitDefaultValue = false)]
        public int? VehicleId { get; set; }

        [DataMember(Name = "vin", EmitDefaultValue = false)]
        public string Vin { get; set; }

        [DataMember(Name = "advertiser_name", EmitDefaultValue = false)]
        public string AdvertiserName { get; set; }

        [DataMember(Name = "advertiser_address_line1", EmitDefaultValue = false)]
        public string AdvertiserAddressLine1 { get; set; }

        [DataMember(Name = "advertiser_address_city", EmitDefaultValue = false)]
        public string AdvertiserAddressCity { get; set; }

        [DataMember(Name = "advertiser_address_county", EmitDefaultValue = false)]
        public string AdvertiserAddressCounty { get; set; }

        [DataMember(Name = "advertiser_address_state", EmitDefaultValue = false)]
        public string AdvertiserAddressState { get; set; }

        [DataMember(Name = "advertiser_address_postalcode", EmitDefaultValue = false)]
        public string AdvertiserAddressPostalcode { get; set; }

        [DataMember(Name = "advertiser_address_country", EmitDefaultValue = false)]
        public string AdvertiserAddressCountry { get; set; }

        [DataMember(Name = "advertiser_address_location", EmitDefaultValue = false)]
        public string AdvertiserAddressLocation { get; set; }

        [DataMember(Name = "stock_number", EmitDefaultValue = false)]
        public string StockNumber { get; set; }

        [DataMember(Name = "price", EmitDefaultValue = false)]
        public double? Price { get; set; }

        [DataMember(Name = "odometer_reading", EmitDefaultValue = false)]
        public int? OdometerReading { get; set; }

        [DataMember(Name = "candidate_chromec_styles", EmitDefaultValue = false)]
        public List<int> CandidateChromecStyles { get; set; }

        [DataMember(Name = "manufacturer", EmitDefaultValue = false)]
        public string Manufacturer { get; set; }

        [DataMember(Name = "make", EmitDefaultValue = false)]
        public string Make { get; set; }

        [DataMember(Name = "model", EmitDefaultValue = false)]
        public string Model { get; set; }

        [DataMember(Name = "model_year", EmitDefaultValue = false)]
        public int? ModelYear { get; set; }

        [DataMember(Name = "trim", EmitDefaultValue = false)]
        public string Trim { get; set; }

        [DataMember(Name = "style", EmitDefaultValue = false)]
        public string Style { get; set; }

        [DataMember(Name = "body_style", EmitDefaultValue = false)]
        public string BodyStyle { get; set; }

        [DataMember(Name = "transmission_description", EmitDefaultValue = false)]
        public string TransmissionDescription { get; set; }

        [DataMember(Name = "transmission_type", EmitDefaultValue = false)]
        public string TransmissionType { get; set; }

        [DataMember(Name = "transmission_gear_selection_mechanism", EmitDefaultValue = false)]
        public List<string> TransmissionGearSelectionMechanism { get; set; }

        [DataMember(Name = "engine_description", EmitDefaultValue = false)]
        public string EngineDescription { get; set; }

        [DataMember(Name = "engine_fuel_type", EmitDefaultValue = false)]
        public List<string> EngineFuelType { get; set; }

        [DataMember(Name = "engine_cylinder_count", EmitDefaultValue = false)]
        public int? EngineCylinderCount { get; set; }

        [DataMember(Name = "engine_cylinder_layout", EmitDefaultValue = false)]
        public string EngineCylinderLayout { get; set; }

        [DataMember(Name = "engine_displacement_inliters", EmitDefaultValue = false)]
        public double? EngineDisplacementInliters { get; set; }

        [DataMember(Name = "drivetrain_description", EmitDefaultValue = false)]
        public string DrivetrainDescription { get; set; }

        [DataMember(Name = "drivetrain_type", EmitDefaultValue = false)]
        public string DrivetrainType { get; set; }

        [DataMember(Name = "exterior_one_color_manufacturer_name", EmitDefaultValue = false)]
        public string ExteriorOneColorManufacturerName { get; set; }

        [DataMember(Name = "exterior_one_color_generic_name", EmitDefaultValue = false)]
        public string ExteriorOneColorGenericName { get; set; }

        [DataMember(Name = "exterior_one_color_rgb_hex", EmitDefaultValue = false)]
        public string ExteriorOneColorRgbHex { get; set; }

        [DataMember(Name = "interior_color_manufacturer_name", EmitDefaultValue = false)]
        public string InteriorColorManufacturerName { get; set; }

        [DataMember(Name = "fuel_economy_city", EmitDefaultValue = false)]
        public double? FuelEconomyCity { get; set; }

        [DataMember(Name = "fuel_economy_highway", EmitDefaultValue = false)]
        public double? FuelEconomyHighway { get; set; }

        [DataMember(Name = "passenger_capacity", EmitDefaultValue = false)]
        public int? PassengerCapacity { get; set; }

        [DataMember(Name = "passenger_doors", EmitDefaultValue = false)]
        public int? PassengerDoors { get; set; }

        [DataMember(Name = "interior_items", EmitDefaultValue = false)]
        public List<string> InteriorItems { get; set; }

        [DataMember(Name = "exterior_items", EmitDefaultValue = false)]
        public List<string> ExteriorItems { get; set; }

        [DataMember(Name = "safety_items", EmitDefaultValue = false)]
        public List<string> SafetyItems { get; set; }

        [DataMember(Name = "technology_items", EmitDefaultValue = false)]
        public List<string> TechnologyItems { get; set; }

        [DataMember(Name = "under_the_hood_items", EmitDefaultValue = false)]
        public List<string> UnderTheHoodItems { get; set; }

        [DataMember(Name = "is_publication_online", EmitDefaultValue = false)]
        public bool? IsPublicationOnline { get; set; }

        [DataMember(Name = "date_published", EmitDefaultValue = false)]
        public DateTime? DatePublished { get; set; }
        //NEw Props
        [DataMember(Name = "advertiser_address_line2", EmitDefaultValue = false)]
        public string AdvertiserAddressLine2 { get; set; }

        [DataMember(Name = "advertiser_address_email", EmitDefaultValue = false)]
        public string AdvertiserAddressEmail { get; set; }

        [DataMember(Name = "advertiser_address_fax", EmitDefaultValue = false)]
        public string AdvertiserAddressFax { get; set; }


        [DataMember(Name = "advertiser_address_phone", EmitDefaultValue = false)]
        public string AdvertiserAddressPhone { get; set; }

        [DataMember(Name = "advertiser_address_website", EmitDefaultValue = false)]
        public string AdvertiserAddressWebsite { get; set; }

        [DataMember(Name = "air_compressor", EmitDefaultValue = false)]
        public string AirCompressor { get; set; }

        [DataMember(Name = "drivetrain_axle", EmitDefaultValue = false)]
        public string DrivetrainAxle { get; set; }

        [DataMember(Name = "engine_displacement_cubic_inches", EmitDefaultValue = false)]
        public double? EngineDisplacementCubicInches { get; set; }

        [DataMember(Name = "exterior_two_color_generic_name", EmitDefaultValue = false)]
        public string ExteriorTwoColorGenericName { get; set; }

        [DataMember(Name = "exterior_two_color_manufacturer_name", EmitDefaultValue = false)]
        public string ExteriorTwoColorManufacturerName { get; set; }

        [DataMember(Name = "exterior_two_color_rgb_hex", EmitDefaultValue = false)]
        public string ExteriorTwoColorRgbHex { get; set; }

        [DataMember(Name = "fuel_economy_combined", EmitDefaultValue = false)]
        public double? FuelEconomyCombined { get; set; }

        [DataMember(Name = "interior_color_rgb_hex", EmitDefaultValue = false)]
        public string InteriorColorRgbHex { get; set; }
              

        [DataMember(Name = "transmission_gear_count", EmitDefaultValue = false)]
        public int? TransmissionGearCount { get; set; }

        [DataMember(Name = "stock_type", EmitDefaultValue = false)]
        public string StockType { get; set; }


    }
}
