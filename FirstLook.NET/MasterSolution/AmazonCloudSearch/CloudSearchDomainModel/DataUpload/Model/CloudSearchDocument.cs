﻿using System.Runtime.Serialization;

namespace FirstLook.CloudSearch.DomainModel.DataUpload.Model
{
    [DataContract]
    public class CloudSearchDocument
    {
        [DataMember(Name = "id", Order = 2, EmitDefaultValue = false)]
        public string Id { get; set; }

        [DataMember(Name = "type", Order = 1, EmitDefaultValue = false)]
        public string Type { get; set; }

        [DataMember(Name = "fields", Order = 3, EmitDefaultValue = false)]
        public CloudSearchFields SearchDocumentFields { get; set; }

    }
}
