﻿
namespace FirstLook.CloudSearch.DomainModel.DataUpload.Model
{
    public static class AmazonCloudSearchDataOperationType
    {
        public  const string AddData = "add";
        public  const string RemoveData = "delete";
    }
}
