﻿using System.Runtime.Serialization;

namespace FirstLook.CloudSearch.DomainModel.DataUpload.Model
{
    [DataContract]
    public class AmazonS3Document
    {
        [DataMember(Name = "_index")]
        public string Index { get; set; }

        [DataMember(Name = "_type")]
        public string Type { get; set; }

        [DataMember(Name = "_id")]
        public string Id { get; set; }

        [DataMember(Name = "_score")]
        public int Score { get; set; }

        [DataMember(Name = "_source")]
        public Vehicle Vehicle { get; set; }
    }
}
