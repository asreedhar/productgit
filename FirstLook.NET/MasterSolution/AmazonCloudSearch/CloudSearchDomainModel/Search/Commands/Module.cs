﻿using FirstLook.CloudSearch.DomainModel.Search.Commands.Impl;
using FirstLook.Common.Core.Registry;

namespace FirstLook.CloudSearch.DomainModel.Search.Commands
{
    public class Module : IModule
    {
        #region IModule Members

        /// <summary>
        /// Add pertinent interfaces and impelementing classes to the registry.
        /// </summary>
        public void Configure(IRegistry registry)
        {
            registry.Register<ICommandFactory, CommandFactory>(ImplementationScope.Shared);
        }

        #endregion

    }
}
