﻿using Amazon.CloudSearchDomain;
namespace FirstLook.CloudSearch.DomainModel.Search.Commands.TransferObjects
{
    public class CloudSearchCommandInput
    {
        public string SearchQuery { get; set; }
        public string Facet { get; set; }
        public string Cursor { get; set; }
        public int Size { get; set; }
        public string SearchEndPoint { get; set; }
        public QueryParser QueryParser { get; set; }
        public string ReturnFields { get; set; }
    }
}
