﻿using System.Net;

namespace FirstLook.CloudSearch.DomainModel.Search.Commands.TransferObjects
{
    public class CloudSearchCommandResult
    {
        public string JsonData { get; set; }
        public HttpStatusCode StatusCode { get; set; }       
    }
}

