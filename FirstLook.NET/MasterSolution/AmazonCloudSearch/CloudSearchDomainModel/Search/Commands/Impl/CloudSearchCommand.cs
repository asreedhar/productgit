﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using FirstLook.CloudSearch.DomainModel.Search.Commands.TransferObjects;
using FirstLook.Common.Core.Command;

namespace FirstLook.CloudSearch.DomainModel.Search.Commands.Impl
{
    public class CloudSearchCommand : ICommand<CloudSearchCommandResult, CloudSearchCommandInput>
    {
        public  CloudSearchCommandResult Execute(CloudSearchCommandInput parameters)
        {   
            try
            {  
                string url = "";
                string cursor = String.IsNullOrEmpty(parameters.Cursor) ? "initial" :parameters.Cursor;
                if (String.IsNullOrEmpty(parameters.ReturnFields))
                {
                    url = string.Format("{0}{1}{2}&q.parser={3}&cursor={4}&size={5}", ConfigurationManager.AppSettings["SearchUrl"], parameters.SearchQuery, parameters.Facet, "structured", cursor, parameters.Size);
                }
                else
                {
                    string returnFields = parameters.ReturnFields.EndsWith(",") ? parameters.ReturnFields.Substring(0, parameters.ReturnFields.Length - 1) : parameters.ReturnFields;
                    url = string.Format("{0}{1}{2}&q.parser={3}&cursor={4}&size={5}&return={6}", ConfigurationManager.AppSettings["SearchUrl"], parameters.SearchQuery, parameters.Facet, "structured", cursor, parameters.Size,returnFields);
                }
                HttpWebRequest request =(HttpWebRequest) WebRequest.Create(url);
                request.Accept = "gzip";
                HttpWebResponse webresponse = (HttpWebResponse)request.GetResponse();

                Stream dataStream = webresponse.GetResponseStream();

                StreamReader reader = new StreamReader(dataStream);

                string responseFromServer = reader.ReadToEnd();
                                
                return new CloudSearchCommandResult() { JsonData = responseFromServer, StatusCode = webresponse.StatusCode};//, FacetInfo = facetInfo };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       
    }
}
