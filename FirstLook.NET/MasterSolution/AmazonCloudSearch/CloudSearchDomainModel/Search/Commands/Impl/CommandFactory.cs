﻿
namespace FirstLook.CloudSearch.DomainModel.Search.Commands.Impl
{
    public class CommandFactory : ICommandFactory
    {        
        public CloudSearchCommand CreateCloudSearchCommand()
        {
            return new CloudSearchCommand();
        }
    }
}
