﻿using FirstLook.CloudSearch.DomainModel.Search.Commands.Impl;

namespace FirstLook.CloudSearch.DomainModel.Search.Commands
{
    public interface ICommandFactory
    {
        CloudSearchCommand CreateCloudSearchCommand();
       
    }
}
