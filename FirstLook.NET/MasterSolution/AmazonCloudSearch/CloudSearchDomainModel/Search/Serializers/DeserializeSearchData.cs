﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;

namespace FirstLook.CloudSearch.DomainModel.Search.Serializers
{
    [DataContract]
    public class DeserializeSearchData
    {
        [DataMember(Name = "vehicleid", EmitDefaultValue = false)]
        public List<string> VehicleId { get; set; }

        [DataMember(Name = "vin", EmitDefaultValue = false)]
        public List<string> Vin { get; set; }

        [DataMember(Name = "advertiser_name", EmitDefaultValue = false)]
        public List<string> AdvertiserName { get; set; }

        [DataMember(Name = "advertiser_address_line1", EmitDefaultValue = false)]
        public List<string> AdvertiserAddressLine1 { get; set; }

        [DataMember(Name = "advertiser_address_city", EmitDefaultValue = false)]
        public List<string> AdvertiserAddressCity { get; set; }

        [DataMember(Name = "advertiser_address_county", EmitDefaultValue = false)]
        public List<string> AdvertiserAddressCounty { get; set; }

        [DataMember(Name = "advertiser_address_state", EmitDefaultValue = false)]
        public List<string> AdvertiserAddressState { get; set; }

        [DataMember(Name = "advertiser_address_postalcode", EmitDefaultValue = false)]
        public List<string> AdvertiserAddressPostalcode { get; set; }

        [DataMember(Name = "advertiser_address_country", EmitDefaultValue = false)]
        public List<string> AdvertiserAddressCountry { get; set; }

        [DataMember(Name = "advertiser_address_location", EmitDefaultValue = false)]
        public List<string> AdvertiserAddressLocation { get; set; }

        [DataMember(Name = "stock_number", EmitDefaultValue = false)]
        public List<string> StockNumber { get; set; }

        [DataMember(Name = "price", EmitDefaultValue = false)]
        public List<string> Price { get; set; }

        [DataMember(Name = "odometer_reading", EmitDefaultValue = false)]
        public List<int> OdometerReading { get; set; }

        [DataMember(Name = "candidate_chromec_styles", EmitDefaultValue = false)]
        public List<int> CandidateChromecStyles { get; set; }

        [DataMember(Name = "manufacturer", EmitDefaultValue = false)]
        public List<string> Manufacturer { get; set; }

        [DataMember(Name = "make", EmitDefaultValue = false)]
        public List<string> Make { get; set; }

        [DataMember(Name = "model", EmitDefaultValue = false)]
        public List<string> Model { get; set; }

        [DataMember(Name = "model_year", EmitDefaultValue = false)]
        public List<string> ModelYear { get; set; }

        [DataMember(Name = "trim", EmitDefaultValue = false)]
        public List<string> Trim { get; set; }

        [DataMember(Name = "style", EmitDefaultValue = false)]
        public List<string> Style { get; set; }

        [DataMember(Name = "body_style", EmitDefaultValue = false)]
        public List<string> BodyStyle { get; set; }

        [DataMember(Name = "transmission_description", EmitDefaultValue = false)]
        public List<string> TransmissionDescription { get; set; }

        [DataMember(Name = "transmission_type", EmitDefaultValue = false)]
        public List<string> TransmissionType { get; set; }

        [DataMember(Name = "transmission_gear_selection_mechanism", EmitDefaultValue = false)]
        public List<string> TransmissionGearSelectionMechanism { get; set; }

        [DataMember(Name = "engine_description", EmitDefaultValue = false)]
        public List<string> EngineDescription { get; set; }

        [DataMember(Name = "engine_fuel_type", EmitDefaultValue = false)]
        public List<string> EngineFuelType { get; set; }

        [DataMember(Name = "engine_cylinder_count", EmitDefaultValue = false)]
        public List<string> EngineCylinderCount { get; set; }

        [DataMember(Name = "engine_cylinder_layout", EmitDefaultValue = false)]
        public List<string> EngineCylinderLayout { get; set; }

        [DataMember(Name = "engine_displacement_inliters", EmitDefaultValue = false)]
        public List<float> EngineDisplacementInliters { get; set; }

        [DataMember(Name = "drivetrain_description", EmitDefaultValue = false)]
        public List<string> DrivetrainDescription { get; set; }

        [DataMember(Name = "drivetrain_type", EmitDefaultValue = false)]
        public List<string> DrivetrainType { get; set; }

        [DataMember(Name = "exterior_one_color_manufacturer_name", EmitDefaultValue = false)]
        public List<string> ExteriorOneColorManufacturerName { get; set; }

        [DataMember(Name = "exterior_one_color_generic_name", EmitDefaultValue = false)]
        public List<string> ExteriorOneColorGenericName { get; set; }

        [DataMember(Name = "exterior_one_color_rgb_hex", EmitDefaultValue = false)]
        public List<string> ExteriorOneColorRgbHex { get; set; }

        [DataMember(Name = "interior_color_manufacturer_name", EmitDefaultValue = false)]
        public List<string> InteriorColorManufacturerName { get; set; }

        [DataMember(Name = "fuel_economy_city", EmitDefaultValue = false)]
        public List<string> FuelEconomyCity { get; set; }

        [DataMember(Name = "fuel_economy_highway", EmitDefaultValue = false)]
        public List<string> FuelEconomyHighway { get; set; }

        [DataMember(Name = "passenger_capacity", EmitDefaultValue = false)]
        public List<string> PassengerCapacity { get; set; }

        [DataMember(Name = "passenger_doors", EmitDefaultValue = false)]
        public List<string> PassengerDoors { get; set; }

        [DataMember(Name = "interior_items", EmitDefaultValue = false)]
        public List<string> InteriorItems { get; set; }

        [DataMember(Name = "exterior_items", EmitDefaultValue = false)]
        public List<string> ExteriorItems { get; set; }

        [DataMember(Name = "safety_items", EmitDefaultValue = false)]
        public List<string> SafetyItems { get; set; }

        [DataMember(Name = "technology_items", EmitDefaultValue = false)]
        public List<string> TechnologyItems { get; set; }

        [DataMember(Name = "under_the_hood_items", EmitDefaultValue = false)]
        public List<string> UnderTheHoodItems { get; set; }

        [DataMember(Name = "is_publication_online", EmitDefaultValue = false)]
        public List<string> IsPublicationOnline { get; set; }

        [DataMember(Name = "date_published", EmitDefaultValue = false)]
        public List<string> DatePublished { get; set; }

        public List<DeserializeSearchData> SerializeSearchData(List<string> jsonRecords)
        {
            DataContractJsonSerializer json_serializer = new DataContractJsonSerializer(typeof(DeserializeSearchData));
            List<DeserializeSearchData> deserializedData=new List<DeserializeSearchData>();
            foreach (string jsonRecord in jsonRecords)
            {
                using (MemoryStream stream = new MemoryStream(Encoding.Unicode.GetBytes(jsonRecord)))
                {                                  
                    deserializedData.Add(json_serializer.ReadObject(stream) as DeserializeSearchData);
                }
            }

            return deserializedData;
        }
    }
}
