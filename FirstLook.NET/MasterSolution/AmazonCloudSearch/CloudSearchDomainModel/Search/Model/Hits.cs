﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.CloudSearch.DomainModel.Search.Model
{
    [DataContract]
    public class Hits
    {
        [DataMember(Name = "found", EmitDefaultValue = false)] 
        public int Found { get; set; }

        [DataMember(Name = "start", EmitDefaultValue = false)] 
        public int Start { get; set; }

        [DataMember(Name = "cursor", EmitDefaultValue = false)] 
        public string Cursor { get; set; }

        [DataMember(Name = "hit", EmitDefaultValue = false)] 
        public List<Hit> Hit { get; set; }
    }
}
