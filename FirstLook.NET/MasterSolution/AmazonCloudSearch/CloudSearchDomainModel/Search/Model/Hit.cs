﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace FirstLook.CloudSearch.DomainModel.Search.Model
{
    [DataContract]
   public class Hit
    {
       [DataMember(Name = "id", EmitDefaultValue = false)] 
       public string Id { get; set; }

       [DataMember(Name = "fields", EmitDefaultValue = false)] 
       public Fields Fields{ get; set; }
    }
}
