﻿using System.Collections.Generic;
using System.Text;

/// <summary>
/// Summary description for Faceting
/// </summary>
/// 
namespace FirstLook.CloudSearch.DomainModel.Search.Model
{
    public class Faceting
    {
        public List<Facet> FacetList;

        public Faceting()
        {
            FacetList = new List<Facet>();
        }

        public void AddFacet(Facet facet)
        {
            FacetList.Add(facet);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in FacetList)
            {
                sb.Append(item);
            }
            return sb.ToString();
        }

    }
}