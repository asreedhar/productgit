﻿using System.Collections.Generic;
using System;
using System.ComponentModel;
using System.Linq.Expressions;

namespace FirstLook.CloudSearch.DomainModel.Search.Model
{
    public class SearchOperation
    {      

        public SearchQueryValueFilter Operand1 { get; internal set; }
        public string SearchOperationString { get; internal set; }
        public string ReturnFields { get; internal set; }
         
        public static SearchOperation  CreateSearchOperation<T>(Expression<Func<T>> operand1,object value1,SearchOperator operation1,object rangeValue1=null,RadiusSearch radiusSaerch=null,bool isReturn=false){
            SearchOperation SearchOperation = new SearchOperation();

            if(operation1== SearchOperator.Range && rangeValue1==null)
                throw new Exception("Must specify Range value for Range operation");

            string fieldDescription = GetDescription(operand1);
            SearchOperation.Operand1 = new SearchQueryValueFilter() {
                    Field = fieldDescription, 
                    Value =  Convert.ToString(value1),// value1.ToString(), 
                    ValueTwo = rangeValue1 !=null? rangeValue1.ToString():null, 
                    SearchOperator = operation1,
                    IsExclude=operation1==SearchOperator.Not,
                    IsNumeric = IsNumericField(operand1.GetType().ToString()),
                    RadiusSearch=radiusSaerch,
                    IsReturn = isReturn
            };
            SearchOperation.SearchOperationString = SearchOperation.Operand1.ToString();
            if (isReturn) { SearchOperation.ReturnFields = SearchOperation.ReturnFields + fieldDescription + ","; }
            return SearchOperation;
        }
        
        public static SearchOperation operator |(SearchOperation c1, SearchOperation c2)
        {
            SearchQueryFilterOperation filterOperation = new SearchQueryFilterOperation() { operation=SearchQueryFilterOperation.FilterOperation.or };

            if (c1.Operand1 != null)
                filterOperation.addFilters(c1.Operand1);
            else
                filterOperation.SearchExpression = c1.SearchOperationString;

            if (c2.Operand1 != null)
                filterOperation.addFilters(c2.Operand1);
            else
                filterOperation.SearchExpression = c2.SearchOperationString;
                      
            
            return new SearchOperation(){SearchOperationString= filterOperation.ToString()};
        }

        public static SearchOperation operator &(SearchOperation c1, SearchOperation c2)
        {
            SearchQueryFilterOperation filterOperation = new SearchQueryFilterOperation() {operation = SearchQueryFilterOperation.FilterOperation.and };

            if (c1.Operand1 != null)
                filterOperation.addFilters(c1.Operand1);
            else
                filterOperation.SearchExpression = c1.SearchOperationString;

            if (c2.Operand1 != null)
                filterOperation.addFilters(c2.Operand1);
            else
                filterOperation.SearchExpression = c2.SearchOperationString;
                    
            
            return new SearchOperation() { SearchOperationString = filterOperation.ToString() };
        }

        private static string GetDescription<T>(Expression<Func<T>> expr)
        {
            var mexpr = expr.Body as MemberExpression;
            if (mexpr == null) return null;
            if (mexpr.Member == null) return null;
            object[] attrs = mexpr.Member.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attrs == null || attrs.Length == 0) return null;
            DescriptionAttribute desc = attrs[0] as DescriptionAttribute;
            if (desc == null) return null;
            return desc.Description;
        }

        private static bool IsNumericField(string returnType)
        {
            string dataType=returnType.ToUpper();
            if (dataType.Contains("INT") || dataType.Contains("DOUBLE") || dataType.Contains("DECIMAL") || dataType.Contains("SINGLE"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public enum SearchOperator
    {
        Range,
        Not,
        GreaterThan,
        LessThan,
        GreaterThanEqualsTo,
        LessThanEqualsTo,
        EqualsTo,
        NotEqualsTo,
        Radius
    }
}
