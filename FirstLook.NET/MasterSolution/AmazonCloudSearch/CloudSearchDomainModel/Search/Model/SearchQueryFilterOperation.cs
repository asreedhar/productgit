﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FirstLook.CloudSearch.DomainModel.Search.Model
{
    /// <summary>
    /// Summary description for SearchQueryFilterOperation
    /// </summary>
    public class SearchQueryFilterOperation : ISearchQueryFilter
    {

        public List<ISearchQueryFilter> filters {get;set;}
        public string SearchExpression { get; set; }
        public FilterOperation operation { get; set; }

        public SearchQueryFilterOperation()
        {
            operation = FilterOperation.and;
            filters = new List<ISearchQueryFilter>();
        }

        public void addFilters(ISearchQueryFilter filter)
        {
            filters.Add(filter);
        }
        
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            bool isTrue = false;
            foreach (ISearchQueryFilter f in filters)
            {
                if (filters.Count == 2 && f.ToString() == null)
                {
                    isTrue = true;
                }
            }
            if (!isTrue || !String.IsNullOrEmpty(SearchExpression))
            {
                sb.Append("(");
                sb.Append(operation);
            }
          
            
            if (!String.IsNullOrEmpty(SearchExpression))
                sb.Append(SearchExpression);

            foreach (ISearchQueryFilter f in filters)
            {
                sb.Append(" ");
                sb.Append(f);
            }
            if (!isTrue || !String.IsNullOrEmpty(SearchExpression))
            {
                sb.Append(")");
            }
                  
            
            return sb.ToString();
        }

        public enum FilterOperation
        {
            and, or
        }

    }
}