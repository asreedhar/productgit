﻿
/// <summary>
/// Summary description for Range
/// </summary>
/// 
namespace FirstLook.CloudSearch.DomainModel.Search.Model
{
    public class Range
    {
        public string value1 { get; set; }
        public string value2 { get; set; }
    }
}