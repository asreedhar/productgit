﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// Summary description for Facet
/// </summary>
namespace FirstLook.CloudSearch.DomainModel.Search.Model
{
    public class Facet
    {
        public String FacetName { get; set; }
        public List<object> Constraints { get; set; }
        public List<Range> RangeList { get; set; }

        public Facet()
        {
            Constraints = new List<object>();
            RangeList = new List<Range>();
        }

        public void AddContraints(string contraint)
        {
            Constraints.Add(contraint);
        }

        public void AddContraints(Range range)
        {
            RangeList.Add(range);
        }


        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            if (FacetName != null)
            {
                sb.Append("&facet.");
                sb.Append(FacetName);
                sb.Append("=");
            }
            if (Constraints != null && Constraints.Count > 0)
            {
                sb.Append("{ buckets :[");
                var last = Constraints.Last();
                foreach (var item in Constraints)
                {
                    sb.Append("\"");
                    sb.Append(item);
                    sb.Append("\"");
                    if (!item.Equals(last))
                    {
                        sb.Append(",");
                    }
                }
                sb.Append("]}");
            }
            else if (RangeList != null && RangeList.Count > 0)
            {
                sb.Append("{ buckets :[");
                var last = RangeList.Last();
                foreach (var item in RangeList)
                {
                    sb.Append("\"");
                    sb.Append("[");
                    sb.Append(item.value1);
                    sb.Append(",");
                    sb.Append(item.value2);
                    sb.Append("]");
                    sb.Append("\"");
                    if (!item.Equals(last))
                    {
                        sb.Append(",");
                    }
                }
                sb.Append("]}");
            }
            else
            {
                sb.Append("{}");
            }

            return sb.ToString();
        }





        //public override string ToString()
        //{
        //    StringBuilder sb = new StringBuilder();
        //    if (FacetName != null)
        //    {
        //        sb.Append("{");
        //        sb.Append(FacetName);
        //        sb.Append(":");
        //        if (Constraints != null && Constraints.Count > 0)
        //        {
        //            sb.Append("{ buckets :[");
        //            var last = Constraints.Last();
        //            foreach (var item in Constraints)
        //            {
        //                sb.Append("\"");
        //                sb.Append(item);
        //                sb.Append("\"");
        //                if (!item.Equals(last))
        //                {
        //                    sb.Append(",");
        //                }
        //            }
        //            sb.Append("]}");
        //        }
        //        else if (RangeList != null && RangeList.Count > 0)
        //        {
        //            sb.Append("{ buckets :[");
        //            var last = RangeList.Last();
        //            foreach (var item in RangeList)
        //            {
        //                sb.Append("\"");
        //                sb.Append("[");
        //                sb.Append(item.value1);
        //                sb.Append(",");
        //                sb.Append(item.value2);
        //                sb.Append("]");
        //                sb.Append("\"");
        //                if (!item.Equals(last))
        //                {
        //                    sb.Append(",");
        //                }
        //            }
        //            sb.Append("]}");
        //        }
        //        else
        //        {
        //            sb.Append("{}");
        //        }
        //        sb.Append("}");
        //    }

        //    return sb.ToString();
        //}
    }
}