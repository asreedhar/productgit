﻿using System.Text;
using System;

namespace FirstLook.CloudSearch.DomainModel.Search.Model
{
    /// <summary>
    /// Summary description for SearchQueryValueFilter
    /// </summary>
    public class SearchQueryValueFilter : ISearchQueryFilter
    {
        public string Field { get; set; }
        public string Value { get; set; }
        public string ValueTwo { get; set; }
        public bool IsNumeric { get; set; }
        public bool IsExclude { get; set; }
        public bool IsReturn { get; set; }
        public SearchOperator SearchOperator { get; set; }
        public RadiusSearch RadiusSearch { get; set; }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            if (Value != null && Value != "")
            {
                if (IsExclude && RadiusSearch == null)
                {
                    str.Append("(not ");
                }
                if (RadiusSearch != null)
                {
                    if (Field == null)
                        throw new Exception("Field can not be null for Radius search");

                    BoundingBox box = RadiusSearch.GetBoundingBoxToSearch(RadiusSearch.SourceLatitude, RadiusSearch.SourceLongitude, (float)Convert.ToDecimal(Value), RadiusUnit.Miles);
                    if (ValueTwo != null)
                    {
                        str.Append("(or ");
                    }
                    str.Append(Field);
                    str.Append(":[");
                    str.Append("'" + box.MaxPoint.Latitude + "," + box.MaxPoint.Longitude + "',");
                    str.Append("'" + box.MinPoint.Latitude + "," + box.MinPoint.Longitude + "'");
                    str.Append("] ");

                    if (ValueTwo != null)
                    {
                        box = RadiusSearch.GetBoundingBoxToSearch(RadiusSearch.SourceLatitude, RadiusSearch.SourceLongitude, (float)Convert.ToDecimal(ValueTwo), RadiusUnit.Miles);

                        str.Append(Field);
                        str.Append(":[");
                        str.Append("'" + box.MaxPoint.Latitude + "," + box.MaxPoint.Longitude + "',");
                        str.Append("'" + box.MinPoint.Latitude + "," + box.MaxPoint.Longitude + "'");
                        str.Append("]");
                        str.Append(")");
                    }
                    return str.ToString();
                }


                if (Field != null && IsNumeric)
                {
                    if (SearchOperator == SearchOperator.Range)
                    {
                        str.Append("(range field=");
                        str.Append(RadiusSearch == null ? Field : "distance");
                        str.Append("  [");
                        str.Append(Value);
                        str.Append(",");
                        str.Append(ValueTwo);
                        str.Append("])");
                    }

                    else if (SearchOperator == SearchOperator.GreaterThanEqualsTo)
                    {
                        str.Append(RadiusSearch == null ? Field : "distance");
                        str.Append(":");
                        str.Append("[");
                        str.Append(Value);
                        str.Append(",}");
                    }
                    else if (SearchOperator == SearchOperator.LessThanEqualsTo)
                    {
                        str.Append(RadiusSearch == null ? Field : "distance");
                        str.Append(":");
                        str.Append("{,");
                        str.Append(Value);
                        str.Append("]");
                    }
                    else if (SearchOperator == SearchOperator.GreaterThan)
                    {
                        str.Append(RadiusSearch == null ? Field : "distance");
                        str.Append(":");
                        str.Append("{");
                        str.Append(Value);
                        str.Append(",}");
                    }
                    else if (SearchOperator == SearchOperator.LessThan)
                    {
                        str.Append(RadiusSearch == null ? Field : "distance");
                        str.Append(":");
                        str.Append("{,");
                        str.Append(Value);
                        str.Append("}");
                    }
                    else
                    {

                        str.Append("(term field%3D");
                        str.Append(RadiusSearch == null ? Field : "distance");
                        str.Append(" ");
                        str.Append(Value);
                        str.Append(")");

                    }
                }
                else
                {
                    if (Field != null && SearchOperator == SearchOperator.EqualsTo)
                    {
                        str.Append(Field);
                        str.Append(":");

                    }
                    if (Value != null && Value != "")
                    {
                        str.Append("'");
                        str.Append(Value);
                        str.Append("'");
                    }
                }


                if (IsExclude)
                {
                    str.Append(")");
                }

                return str.ToString();
            }
            else
            {
                return null;
            }
        }

    }
}