﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace FirstLook.CloudSearch.DomainModel.Search.Model
{
    [DataContract]
    public class Fields
    {
        [Description("vehicleid")]
        public int? VehicleId { get; set; }

        [Description("vin")]
        public string Vin { get; set; }

        [Description("advertiser_name")]
        public string AdvertiserName { get; set; }

        [Description("advertiser_address_line1")]
        public string AdvertiserAddressLine1 { get; set; }

        [Description("advertiser_address_city")]
        public string AdvertiserAddressCity { get; set; }

        [Description("advertiser_address_county")]
        public string AdvertiserAddressCounty { get; set; }

        [Description("advertiser_address_state")]
        public string AdvertiserAddressState { get; set; }

        [Description("advertiser_address_postalcode")]
        public string AdvertiserAddressPostalcode { get; set; }

        [Description("advertiser_address_country")]
        public string AdvertiserAddressCountry { get; set; }

        [Description("advertiser_address_location")]
        public string AdvertiserAddressLocation { get; set; }

        [Description("stock_number")]
        public string StockNumber { get; set; }

        [Description("price")]
        public int? Price { get; set; }

        [Description("odometer_reading")]
        public int? OdometerReading { get; set; }

        [Description("candidate_chromec_styles")]
        public List<int> CandidateChromecStyles { get; set; }

        [Description("manufacturer")]
        public string Manufacturer{ get; set; }

        [Description("make")]
        public string Make { get; set; }

        [Description("model")]
        public string Model { get; set; }

        [Description("model_year")]
        public int ModelYear { get; set; }//Expression<Func<T>> 

        [Description("trim")]
        public string Trim { get; set; }

        [Description("style")]
        public string Style { get; set; }

        [Description("body_style")]
        public string BodyStyle { get; set; }

        [Description("transmission_description")]
        public string TransmissionDescription { get; set; }

        [Description("transmission_type")]
        public string TransmissionType { get; set; }

        [Description("transmission_gear_selection_mechanism")]
        public List<string> TransmissionGearSelectionMechanism { get; set; }

        [Description("engine_description")]
        public string EngineDescription { get; set; }

        [Description("engine_fuel_type")]
        public List<string> EngineFuelType { get; set; }

        [Description("engine_cylinder_count")]
        public int? EngineCylinderCount { get; set; }

        [Description("engine_cylinder_layout")]
        public string EngineCylinderLayout { get; set; }

        [Description("engine_displacement_inliters")]
        public double? EngineDisplacementInliters { get; set; }

        [Description("drivetrain_description")]
        public string DrivetrainDescription { get; set; }

        [Description("drivetrain_type")]
        public string DrivetrainType { get; set; }

        [Description("exterior_one_color_manufacturer_name")]
        public string ExteriorOneColorManufacturerName { get; set; }

        [Description("exterior_one_color_generic_name")]
        public string ExteriorOneColorGenericName { get; set; }

        [Description("exterior_one_color_rgb_hex")]
        public string ExteriorOneColorRgbHex { get; set; }

        [Description("interior_color_manufacturer_name")]
        public string InteriorColorManufacturerName { get; set; }

        [Description("fuel_economy_city")]
        public double? FuelEconomyCity { get; set; }

        [Description("fuel_economy_highway")]
        public int? FuelEconomyHighway { get; set; }

        [Description("passenger_capacity")]
        public int? PassengerCapacity { get; set; }

        [Description("passenger_doors")]
        public int? PassengerDoors { get; set; }

        [Description("interior_items")]
        public List<string> InteriorItems { get; set; }

        [Description("exterior_items")]
        public List<string> ExteriorItems { get; set; }

        [Description("safety_items")]
        public List<string> SafetyItems { get; set; }

        [Description("technology_items")]
        public List<string> TechnologyItems { get; set; }

        [Description("under_the_hood_items")]
        public List<string> UnderTheHoodItems { get; set; }

        [Description("is_publication_online")]
        public bool? IsPublicationOnline { get; set; }

        [Description("date_published")]
        public DateTime? DatePublished { get; set; }

    }
}
