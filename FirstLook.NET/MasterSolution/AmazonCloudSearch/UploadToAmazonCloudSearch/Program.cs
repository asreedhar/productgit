﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.CloudSearch.DomainModel.DataUpload.Command.Impl;
using FirstLook.CloudSearch.DomainModel.DataUpload.Model;
using FirstLook.Common.Core.Registry;

namespace UploadToAmazonCloudSearch
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // Code that runs on application startup      
                IRegistry registry = RegistryFactory.GetRegistry();
                registry.Register<FirstLook.CloudSearch.DomainModel.DataUpload.Command.Module>();
                UploadToCloudSearchCommand uploadToCloudSearchCommand = new UploadToCloudSearchCommand();
                List<List<CloudSearchDocument>> cloudSearchDocuments = uploadToCloudSearchCommand.Execute(AmazonCloudSearchDataOperationType.AddData);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                Console.ReadLine();
            }

        }
    }
}
