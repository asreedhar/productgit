﻿using System.Collections.Generic;
using NUnit.Framework;
using VehicleTemplatingDomainModel;


namespace VehicleTemplatingDomainModelTests
{
    [TestFixture]
    public class TemplateTests
    {
        [Test]
        public void DataPointKeyOverridesText()
        {
            var key = DynamicDataPointKeys.Make;
            var prompt = "Include Make?";
            var text = "this should be ignored";

            var datapoint = new DataPoint(key, text, prompt, false);

            Assert.AreEqual(string.Empty, datapoint.Text);
            Assert.IsNotNull(datapoint.Key);
            Assert.AreEqual(key, datapoint.Key);
        }

        [Test]
        public Template CanCreateTemplate()
        {
            string name = "Main Template";
            decimal height = 11.0m;
            decimal width = 8.5m;
            string units = "in";
            TemplateTypes type = TemplateTypes.WindowSticker;
            string backgroundImage = string.Empty;

            var templateTO = new TemplateTO(Constants.UnassignedId, name, height, width, units, type, backgroundImage, new List<ContentArea>());

            var template = Template.CreateTemplate(templateTO);

            Assert.IsNotNull(template);
            Assert.IsNotNull(template.ContentAreas);
            Assert.IsTrue(template.ContentAreas.Count == 0);
            Assert.AreEqual(name, template.Name);
            Assert.AreEqual(height, template.Height);
            Assert.AreEqual(width, template.Width);
            Assert.AreEqual(units, template.Unit);
            Assert.AreEqual(type, template.Type);

            return template;
        }

        [Test]
        public void CanCreateTemplateWithContentAreas()
        {
            var template = CanCreateTemplate();
            var key = DynamicDataPointKeys.Make;
            var prompt = "Include Make?";

            //var dynamicDataPoint = new DynamicDataPoint(key, prompt);
            var datapoint = new DataPoint(key, null, prompt, false);
            
            var font = new Font(FontFamily.Arial_Black, FontAlign.left, 1.0m, true, true, true, 1.5m);
            var topLeft = new Point(0.0m, 100.0m);
            var bottomRight = new Point(100.0m, 200.0m);
            var printSizes = new CalculatedPrintSizes(24.0m, 22, 1.222m, 1.333m, 1.444m);

            Assert.AreEqual(FontFamily.Arial_Black, font.Family);
            Assert.AreEqual(FontAlign.left, font.Align);
            Assert.AreEqual(1.0m, font.LineHeight);
            Assert.AreEqual(true, font.Italics);
            Assert.AreEqual(true, font.Underline);
            Assert.AreEqual(true, font.Bold);

            Assert.AreEqual(0.0m, topLeft.X);
            Assert.AreEqual(100.0m, topLeft.Y);

            Assert.AreEqual(100.0m, bottomRight.X);
            Assert.AreEqual(200.0m, bottomRight.Y);

            Assert.AreEqual(24.0m, printSizes.LineHeight);
            Assert.AreEqual(22, printSizes.CharacterAverage);
            Assert.AreEqual(1.222m, printSizes.EmSize);
            Assert.AreEqual(1.333m, printSizes.EnSize);
            Assert.AreEqual(1.444m, printSizes.BulletSize);

            var contentArea = new ContentArea(datapoint, font, topLeft, bottomRight, printSizes,1);

            Assert.IsNotNull(contentArea);
            Assert.AreEqual(Constants.UnassignedId, contentArea.Id);
            Assert.IsNotNull(contentArea.DataPoint);
            Assert.IsNotNull(contentArea.Font);
            Assert.IsNotNull(contentArea.TopLeft);
            Assert.IsNotNull(contentArea.BottomRight);
            Assert.IsNotNull(contentArea.PrintSizes);

            template.ContentAreas.Add(contentArea);

            Assert.AreEqual(1, template.ContentAreas.Count);

        }
    }

    [TestFixture]
    public class URLShorteningTests
    {
        [Test]
        public void URLShorteningTests_1()
        {
            IURLShortener shortener = new URLShortener();
            var shortURL = shortener.Shorten( "https://www.google.com/maps/preview/place/Banff,+AB,+Canada/@51.176934,-115.5683895,14z/data=!3m1!4b1!4m2!3m1!1s0x5370ca45910c4afd:0xcaafaebedaac9463" );
            Assert.GreaterOrEqual( shortURL.Length, 0 );
        }
    }
}