﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Autofac;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using NUnit.Framework;
using VehicleTemplatingDomainModel;


namespace VehicleTemplatingDomainModel_IntegrationTests
{
    [TestFixture]
    public class IntegrationTests
    {
        //private string oh = "6DB5C338-B5C3-DC11-9377-0014221831B0";   // Windy City Chevy
        //private string oh = "684E7529-3041-DD11-9F48-0014221831B0";   // BMW North Scottsdale
        //private string oh = "B7B2C338-B5C3-DC11-9377-0014221831B0";   // All Star Chevrolet
        private string oh = "339F3752-3F10-DF11-9976-0022198DB286";     // All Star Chevrolet

        private static Template CreateTemplate(string name, decimal height, decimal width, string units, TemplateTypes type)
        {
            var templateTO = new TemplateTO(Constants.UnassignedId, name, height, width, units, type, string.Empty, new List<ContentArea>());

            var template = Template.CreateTemplate(templateTO);

            return template;
        }

        private Template CreateTemplate()
        {
            string name = "Main Template " + Guid.NewGuid();
            decimal height = 11.0m;
            decimal width = 8.5m;
            string units = "in";
            TemplateTypes type = TemplateTypes.WindowSticker;

            var template = CreateTemplate(name, height, width, units, type);

            Assert.IsNotNull(template);
            Assert.IsNotNull(template.ContentAreas);
            Assert.IsTrue(template.ContentAreas.Count == 0);
            Assert.AreEqual(name, template.Name);
            Assert.AreEqual(height, template.Height);
            Assert.AreEqual(width, template.Width);
            Assert.AreEqual(units, template.Unit);
            Assert.AreEqual(type, template.Type);

            return template;
        }

        private Template CreateTemplateWithContentAreas()
        {
            var template = CreateTemplate();

            var key = DynamicDataPointKeys.Make;
            var prompt = "Include Make?";

            //var dynamicDataPoint = new DynamicDataPoint(key, prompt);
            var dataPoint = new DataPoint(key, null, prompt, false);
//            Assert.AreEqual(Enum.GetName(typeof(DynamicDataPointKeys), key), dataPoint.Text);
//            Assert.AreEqual(Constants.UnassignedId, dataPoint.Id);

            var font = new Font(FontFamily.Arial_Black, FontAlign.left, 1.0m, true, true, true, 1.5m);
            var topLeft = new Point(0.0m, 100.0m);
            var bottomRight = new Point(100.0m, 200.0m);
            var printSizes = new CalculatedPrintSizes(24.0m, 22, 1.222m, 1.333m, 1.444m);

            Assert.AreEqual(FontFamily.Arial_Black, font.Family);
            Assert.AreEqual(FontAlign.left, font.Align);
            Assert.AreEqual(1.0m, font.LineHeight);
            Assert.AreEqual(true, font.Italics);
            Assert.AreEqual(true, font.Underline);
            Assert.AreEqual(true, font.Bold);

            Assert.AreEqual(0.0m, topLeft.X);
            Assert.AreEqual(100.0m, topLeft.Y);

            Assert.AreEqual(100.0m, bottomRight.X);
            Assert.AreEqual(200.0m, bottomRight.Y);

            Assert.AreEqual(24.0m, printSizes.LineHeight);
            Assert.AreEqual(22, printSizes.CharacterAverage);
            Assert.AreEqual(1.222m, printSizes.EmSize);
            Assert.AreEqual(1.333m, printSizes.EnSize);
            Assert.AreEqual(1.444m, printSizes.BulletSize);

            var contentArea = new ContentArea(dataPoint, font, topLeft, bottomRight, printSizes,1);

            Assert.IsNotNull(contentArea);
            Assert.AreEqual(Constants.UnassignedId, contentArea.Id);
            Assert.IsNotNull(contentArea.DataPoint);
            Assert.IsNotNull(contentArea.Font);
            Assert.IsNotNull(contentArea.TopLeft);
            Assert.IsNotNull(contentArea.BottomRight);
            Assert.IsNotNull(contentArea.PrintSizes);

            template.ContentAreas.Add(contentArea);

            Assert.AreEqual(1, template.ContentAreas.Count);


            return template;

        }


        [Test]
        public void CanCreateTemplate()
        {
            ResetTestConditions();
            CreateTemplate();
        }

        [Test]
        public void CanCreateTemplateWithContentAreas()
        {
            ResetTestConditions();
            CreateTemplateWithContentAreas();
        }

        [Test]
        public void CanCreateAndSaveTemplateWithContentAreas()
        {

            ResetTestConditions();
            var template = CreateTemplateWithContentAreas();
            template.Save(oh, "dhillis");

            Debug.WriteLine("Post Save TemplateId:" + template.TemplateId);

            Assert.IsTrue(template.TemplateId != Constants.UnassignedId);
        }

        [Test]
        public void CanGetTemplatesForOwner()
        {

            ResetTestConditions();
            var template = CreateTemplateWithContentAreas();
            template.Name = "Window Sticker " + DateTime.Now;
            template.Save(oh, "dhillis");
            
            var ownersTemplates = Template.GetTemplateInfoForOwnerByTemplateType(oh, TemplateTypes.WindowSticker);
            Assert.IsTrue(ownersTemplates.Count > 0);

            foreach (var templateInfo in ownersTemplates)
            {
                var t = Template.GetTemplate(templateInfo.Id);
                Assert.IsNotNull(t);
                break;
            }
        }

        [Test]
        public void CanUpdateTemplate()
        {

            ResetTestConditions();
            var template = CreateTemplateWithContentAreas();
            string newName = "Template (updated) " + DateTime.Now;
            template.Name = newName;
            template.Save(oh, "dhillis");

            var ownersTemplates = Template.GetTemplateInfoForOwnerByTemplateType(oh, TemplateTypes.WindowSticker);
            Assert.IsTrue(ownersTemplates.Count > 0);


            foreach (var templateInfo in ownersTemplates)
            {
                var t = Template.GetTemplate(templateInfo.Id);


                if (t.Name == newName)
                {
                    t.Name += " changed!";
                    t.ContentAreas[0].Font.Size = 2.3m;

                    t.Save(oh, "dhillis");
                    t = Template.GetTemplate(t.TemplateId);
                    Assert.AreEqual(newName + " changed!", t.Name);
                    Assert.IsNotNull(t);
                    break;
                }
            }
        }

        [Test]
        public void CanDeleteWindowStickerTemplates()
        {
            ResetTestConditions();

            var ownersTemplates = Template.GetTemplateInfoForOwnerByTemplateType(oh, TemplateTypes.WindowSticker);
            Assert.IsTrue(ownersTemplates.Count > 0);

            foreach (var templateInfo in ownersTemplates)
            {
                var t = Template.GetTemplate(templateInfo.Id);
                t.Delete();
            }

            ownersTemplates = Template.GetTemplateInfoForOwnerByTemplateType(oh, TemplateTypes.WindowSticker);
            Assert.IsTrue(ownersTemplates.Count == 0);
        }

        [Test]
        public void CanDeleteBuyersguideTemplates()
        {
            ResetTestConditions();

            var ownersTemplates = Template.GetTemplateInfoForOwnerByTemplateType(oh, TemplateTypes.BuyersGuide);
            Assert.IsTrue(ownersTemplates.Count > 0);

            foreach (var templateInfo in ownersTemplates)
            {
                var t = Template.GetTemplate(templateInfo.Id);
                t.Delete();
            }

            ownersTemplates = Template.GetTemplateInfoForOwnerByTemplateType(oh, TemplateTypes.BuyersGuide);
            Assert.IsTrue(ownersTemplates.Count == 0);
        }


        [Test]
        public void CanGetBuyersGuideTemplatesForOwner()
        {

            ResetTestConditions();
            var template = CreateTemplateWithContentAreas();
            template.Name = "BG- " + DateTime.Now;
            template.Type = TemplateTypes.BuyersGuide;
            template.Save(oh, "dhillis");

            var ownersTemplates = Template.GetTemplateInfoForOwnerByTemplateType(oh, TemplateTypes.BuyersGuide);
            Assert.IsTrue(ownersTemplates.Count > 0);

            foreach (var templateInfo in ownersTemplates)
            {
                var t = Template.GetTemplate(templateInfo.Id);
                Assert.IsNotNull(t);
                break;
            }
        }


        [Test]
        public void CanUpdateTemplateAsSystemTemplate()
        {

            ResetTestConditions();
            var template = CreateTemplateWithContentAreas();
            string newName = "Template - " + DateTime.Now;
            template.Name = newName;
            template.Save(oh, "dhillis");

            Assert.IsTrue(template.TemplateId != Constants.UnassignedId);

            // get the template
            template = Template.GetTemplate(template.TemplateId);

            template.Name = template.Name += " SYSTEM";
            template.SaveAsSystemTemplate("dhillis");

            // get the template
            template = Template.GetTemplate(template.TemplateId);

            Assert.IsNotNull(template);
            Assert.AreEqual(newName + " SYSTEM", template.Name);

        }




        #region Reset Test Conditions

        private static void ResetTestConditions()
        {
            ResetRegistryWithLiveDataAccess();
        }

        private static void ResetRegistryWithLiveDataAccess()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<VehicleTemplatingDataAccess>().As<IVehicleTemplatingDataAccess>();
            builder.RegisterType<NullLogger>().As<ILogger>();
            Registry.RegisterContainer(builder.Build());
        }

        #endregion  

    }
}