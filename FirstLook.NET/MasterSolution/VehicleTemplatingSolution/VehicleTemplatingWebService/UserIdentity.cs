using System;
using System.Web.Services.Protocols;

namespace FirstLook.VehicleTemplating.VehicleTemplatingWebService
{
    /// <summary>
    /// Identity of the calling user.
    /// </summary>
    [Serializable]
    public class UserIdentity : SoapHeader
    {
        /// <summary>
        /// Username of the caller.
        /// </summary>
        public string UserName { get; set; }
    }
}