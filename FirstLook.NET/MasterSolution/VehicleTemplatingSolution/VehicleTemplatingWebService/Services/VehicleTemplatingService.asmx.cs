﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Web.Services;
using log4net;
using VehicleTemplatingDomainModel;

namespace FirstLook.VehicleTemplating.VehicleTemplatingWebService.Services
{
    /// <summary>
    /// Summary description for VehicleTemplatingService
    /// </summary>
    [WebService(Namespace = "http://services.firstlook.biz/VehicleTemplatingWebService")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class VehicleTemplatingService : WebService
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(VehicleTemplatingService).FullName);

        #region Web Methods

        [WebMethod]
        public List<TemplateInfo> GetWindowStickerTemplateInfoForOwner(string ownerHandle)
        {
            var templateList = Template.GetTemplateInfoForOwnerByTemplateType(ownerHandle, TemplateTypes.WindowSticker);
            return templateList;
        }

        [WebMethod]
        public List<TemplateInfo> GetBuyersGuideTemplateInfoForOwner(string ownerHandle)
        {
            var templateList = Template.GetTemplateInfoForOwnerByTemplateType(ownerHandle, TemplateTypes.BuyersGuide);
            return templateList;
        }

        [WebMethod]
        public List<TemplateInfo> GetSharedWindowStickerTemplateInfo()
        {
            var templateList = Template.GetSharedTemplateInfoByTemplateType(TemplateTypes.WindowSticker);
            return templateList;
        }

        [WebMethod]
        public List<TemplateInfo> GetOrphanedBuyersGuideTemplateInfo()
        {
            var templateList = Template.GetOrphanedTemplateInfoByTemplateType(TemplateTypes.BuyersGuide);
            return templateList;
        }

        [WebMethod]
        public List<TemplateInfo> GetOrphanedWindowStickerTemplateInfo()
        {
            var templateList = Template.GetOrphanedTemplateInfoByTemplateType(TemplateTypes.WindowSticker);
            return templateList;
        }

        [WebMethod]
        public List<TemplateInfo> GetSharedBuyersGuideTemplateInfo()
        {
            var templateList = Template.GetSharedTemplateInfoByTemplateType(TemplateTypes.BuyersGuide);
            return templateList;
        }

        [WebMethod]
        public TemplateTO GetTemplate(int templateId)
        {
            Log.DebugFormat("GetTemplate {0}", templateId);

            var template = Template.GetTemplate(templateId);
            return template.AsTransferObject();
        }

        /// <summary>
        /// Saves a template for a particular owner.
        /// </summary>
        /// <param name="templateTO"></param>
        /// <param name="ownerHandle"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        [WebMethod]
        public TemplateTO SaveTemplate(TemplateTO templateTO, string ownerHandle, string userName)
        {
            Log.WarnFormat("Template Saved called {0}", userName);

            var template = Template.CreateTemplate(templateTO);
            template.Save(ownerHandle, userName);

            return template.AsTransferObject();
        }

        /// <summary>
        /// Saves a system wide template.
        /// </summary>
        /// <param name="templateTO"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        [WebMethod]
        public TemplateTO SaveSystemTemplate(TemplateTO templateTO, string userName)
        {
            Log.WarnFormat("Template Saved System called {0}", userName);

            var template = Template.CreateTemplate(templateTO);
            template.SaveAsSystemTemplate(userName);

            return template.AsTransferObject();
        }

        [WebMethod]
        public byte[] GeneratePdf(VehicleData vehicleData, TemplateTO template, string ownerHandle)
        {
            var backgroundImageVirtualRoot = ConfigurationManager.AppSettings["BackgroundImageVirtualRoot"];

            var printBuilder = new VehicleTemplatingWebControls.PrintVehicleTemplateBuilder(vehicleData, template, backgroundImageVirtualRoot);
            return printBuilder.BuildVehicleTemplate(ownerHandle);
        }

        [WebMethod]
        public string GenerateHtml(VehicleData vehicleData, TemplateTO template)
        {
            string backgroundImageVirtualRoot = ConfigurationManager.AppSettings["BackgroundImageVirtualRoot"];

            var printBuilder = new VehicleTemplatingWebControls.PrintVehicleTemplateBuilder(vehicleData, template, backgroundImageVirtualRoot);
            return printBuilder.BuildVehicleHtml();
        }

        [WebMethod]
        public string GenerateMultiHtml(List<VehicleData> vehicleDataList, TemplateTO template)
        {
            string backgroundImageVirtualRoot = ConfigurationManager.AppSettings["BackgroundImageVirtualRoot"];

            List<IVehicleData> vehicleList = new List<IVehicleData>(vehicleDataList.Cast<IVehicleData>());

            var printBuilder = new VehicleTemplatingWebControls.PrintMultiVehicleTemplateBuider(vehicleList, template, backgroundImageVirtualRoot);

            return printBuilder.BuildVehicleHtml();
        }

        #endregion
    }
}
