﻿using System;
using Autofac;
using Autofac.Integration.Web;
using FirstLook.Common.Core.IOC;
using VehicleTemplatingDomainModel;

namespace FirstLook.VehicleTemplating.VehicleTemplatingWebService
{
    public class Global : System.Web.HttpApplication, IContainerProviderAccessor
    {
        private static IContainerProvider _containerProvider;

        protected void Application_Start(object sender, EventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();

            var builder = new ContainerBuilder();
            new VehicleTemplatingDomainRegister().Register(builder);
            IContainer container = builder.Build();            
            Registry.RegisterContainer(container);
            _containerProvider = new ContainerProvider(container);
        }

        public IContainerProvider ContainerProvider
        {
            get { return _containerProvider; }
        }
    }
}