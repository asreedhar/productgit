﻿using System;
using System.Web.Script.Services;
using System.Web.Services;
using VehicleTemplatingWebServiceClient.VehicleTemplatingService;

namespace VehicleTemplatingWebApplication.Services
{
    /// <summary>
    /// Summary description for Template
    /// </summary>
    [WebService(Namespace = "http://xml.firstlook.biz/")] /// FIXME
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [ScriptService]
    public class Template : WebService
    {

        private VehicleTemplatingService _templatingService;
        private VehicleTemplatingService TemplatingService
        {
            get
            {
                if (_templatingService == null)
                {
                    _templatingService = new VehicleTemplatingService();
                }
                return _templatingService;
            }
        }


        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public TemplateTO Get(int templateId)
        {
            if (templateId == -1)
            {
                return new TemplateTO();
            }
            return TemplatingService.GetTemplate(templateId);
        }

        [WebMethod]
        public TemplateTO Save(TemplateTO template, string ownerHandle)
        {
            return TemplatingService.SaveTemplate(template, ownerHandle, User.Identity.Name);
        }

        [WebMethod]
        public TemplateTO SystemSave(TemplateTO template)
        {
            return TemplatingService.SaveSystemTemplate(template, User.Identity.Name);
        }

        [WebMethod]
        public byte[] GeneratePdf(VehicleData vehicleData, TemplateTO template)
        {
            return TemplatingService.GeneratePdf(vehicleData, template, "");
        }

    }
}
