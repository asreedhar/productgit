﻿using System;
using System.Web;
using Autofac.Integration.Web;
using Autofac;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Core.Registry;
using VehicleTemplatingDomainModel;

namespace VehicleTemplatingWebApplication
{
    public class Global : HttpApplication, IContainerProviderAccessor
    {
        private static IContainerProvider _containerProvider;

        protected void Application_Start(object sender, EventArgs e)
        {
            var builder = new ContainerBuilder();

            new VehicleTemplatingDomainRegister().Register(builder);

            IContainer container = builder.Build();
            Registry.RegisterContainer(container);
            _containerProvider = new ContainerProvider(container);

            //Added for fault reporting (health montioring) integration

            IRegistry registry = RegistryFactory.GetRegistry();

            registry.Register<FirstLook.Fault.DomainModel.Module>();

            registry.Register<IDataSessionManager, DataSessionManager>(ImplementationScope.Shared);
        }

        #region Implementation of IContainerProviderAccessor

        public IContainerProvider ContainerProvider
        {
            get { return _containerProvider; }
        }

        #endregion
    }
}