/**
* namespace App
*/

if (typeof (App) === 'undefined') {
    var App = {};
}

(function () {
    var sampleVehicleData = {
        AgeInDays:10,
        Make:"Make",
        Model:"Model",
        ModelYear:1985,
        Series:"Series",
        Price:10000.0,
        UnitCost:10000.0,
        Mileage:50.0,
        StockNumber:"#StockNum",
        Vin:"19UYA31581L000000",
        Engine:"Engine",
        FuelType:"FuelType",
        Transmission:"Transmission",
        Class:"Class",
        Trim:"Trim",
        BodyStyle:"BodyStyle",
        Description:"Description",
        ExteriorColor:"ExteriorColor",
        InteriorColor:"InteriorColor",
        Drivetrain:"Drivetrain",
        Certified:false,
        Equipment:["Equipment0","Equipment1"],
        Packages:["Package0","Package1"],
        MpgCity:"MpgCity",
        MpgHwy:"MpgHwy",
        CertifiedID:"CertifiedID",
        KBBBookValue:10000.0,
        NADABookValue:10000.0,
        QRCodeURL:"QRCodeURL",
        MSRP:15000.0
    };

    this.State = {};
    this.Events = {
        "DOM.beforeFetchDOM": function () {
            $("#font-content-area, #layout-content-area, #content-content-area").hide();
        },
        "DOM.afterFetchDOM": DOMBinder,
        "Grid.generate": function (evt, dom_evt) {
            if (typeof (dom_evt) !== "undefined") {
                dom_evt.stopPropagation();
                dom_evt.preventDefault();
            };

            var els = App.$DOM;
            var page_width = els.PageWidth.val();
            var page_height = els.PageHeight.val();
            var grid_x = els.GridX.val();
            var grid_y = els.GridY.val();
            var page_lower_right = PrintValues.PrintPoint.fromString([page_width, page_height].join(","));
            var zero_mark = new PrintValues.PrintPoint(0, 0, page_lower_right.Unit);
            var page = new PrintValues.PagePreview(App.DOM.PagePreview, zero_mark, page_lower_right);
            var grid_lower_right = PrintValues.PrintPoint.fromString([grid_x, grid_y].join(","));
            var grid = new PrintValues.GenerateGridCommand(zero_mark, grid_lower_right, page);

            App.State.Page = page;

            $(App).trigger("State.Page");

            grid.execute();

            $(App).trigger("Grid.afterGenerate");
        },
        "Grid.samplePDF": function (evt, dom_evt) {
            Services.GeneratePDF.generate({ "vehicleData": sampleVehicleData, "template": App.State.sampleTemplate });
        },
        "Grid.afterGenerate": function (evt, data) {
            $(App).trigger("Grid.reportSize", data);
            $(App).trigger("Font.changeFontSize", data);
            $(App).trigger("Font.changeFont", data);
        },
        "Grid.reportSize": function (evt) {
            size_report = new PrintValues.GenerateSizeReport(App.DOM.PagePreview, App.State.Page);
            size_report.execute();
        },
        "Grid.reportMousePosition": function (evt, dom_evt) {
            var printPoint = PrintValues.PrintPoint.fromEvent(dom_evt, App.DOM.PagePreview);
            App.$DOM.CurrentPosition.text(printPoint.toString());
        },
        "Page.startCreateDataHolder": function (evt, dom_evt) {
            if (!$(dom_evt.target).hasClass("print_area")) {
                App.State.startPoint = PrintValues.PrintPoint.fromEvent(dom_evt, App.DOM.PagePreview);

                $(App).trigger("State.startPointSet");
            }
        },
        "Page.endCreateDataHolder": function (evt, dom_evt) {
            if (typeof App.State.startPoint !== "undefined"
            && !$(dom_evt.target).hasClass("print_area")) {
                App.State.endPoint = PrintValues.PrintPoint.fromEvent(dom_evt, App.DOM.PagePreview);

                $(App).trigger("State.endPointSet");

                var printArea = new PrintValues.PrintArea(App.State.startPoint, App.State.endPoint);

                var size = printArea.pxSize();
                if (size.width < 10 || size.width < 10) {
                    App.State.startPoint = undefined;
                    App.State.endPoint = undefined;
                    // // de-select content area
                    // $("#font-content-area, #layout-content-area, #content-content-area").hide();
                    // $("#generate_preview, #grid_setup, #page_setup").show();
                    return;
                }


                printArea.appendTo(App.DOM.PagePreview);

                if (App.State.Template !== undefined) {
                    App.State.Template.add_content_area_via_print_area(printArea);
                }
            }
        },
        "Page.clearCreateDataHolder": function (evt, dom_evt) {
            if (dom_evt.target.id === "page_preview") {
                delete App.State.startPoint;
                delete App.State.endPoint;
            };
        },
        "Page.GenerateTemplate": function (evt) {
            if (typeof App.State.sampleTemplate === "undefined") {
                return;
            };
            var template = App.State.sampleTemplate;

            App.$DOM.GridX.val(((template.grid || {}).width || 0.25) + template.Unit);
            App.$DOM.GridY.val(((template.grid || {}).height || 0.25) + template.Unit);
            App.$DOM.PageWidth.val(template.Width + template.Unit);
            App.$DOM.PageHeight.val(template.Height + template.Unit);

            //If we have a background image from the database and it hasn't been set by a manual upload
            if (template.BackgroundImage !== null && App.$DOM.BackgroundImage.attr("src") === "") {
                App.$DOM.BackgroundImage.attr("src", "/resources/templating/BackgroundImageHandler.ashx" + template.BackgroundImage);
            }

            //If user has manually uploaded new photo, set template.BackgroundImage so the new one can be saved.
            if ($(".backgroundImageFilename").val() !== "") {
                template.BackgroundImage = $(".backgroundImageFilename").val();
            }

            App.State.Template = new PrintValues.Template(template, App.DOM.PagePreview);

            $(App).trigger("Grid.generate");
        },
        "Page.PopulateDataPoints": function (evt) {
            if (typeof App.State.DataKeys === "undefined") {
                return;
            };

            var select = App.$DOM.DataPoints;
            var opt,
            text;
            for (var i = 0, l = App.State.DataKeys.length; i <= l; i++) {
                text = App.State.DataKeys[i - 1];
                if (text === "") continue;
                opt = $("<option />").val(i - 1).text(text);
                select.append(opt);
            };

        },
        "Page.confirmSave": function (evt, dom_evt) {
            if (typeof dom_evt !== "undefined") {
                dom_evt.preventDefault();
                dom_evt.stopPropagation();
            }

            var shared = '';
            var matches = window.location.search.match(/[\?\&]shared=([^\&]*)/);
            if (matches && matches.length > 1) {
                shared = matches[1];
            }
            if (shared === "True") {

                $("#dialog-confirm").dialog("option", {
                    buttons: {
                        "Accept": function () {
                            $(App).trigger("Page.saveChanges");
                            $(this).dialog("close");
                        },
                        Cancel: function () {
                            $(this).dialog("close");
                        }
                    }
                });
                $("#dialog-confirm").dialog("open");
            } else {
                $(App).trigger("Page.saveChanges");
            }
        },
        "Page.saveChanges": function () {
            var ownerHandle = window.location.search.match(/[\?\&]ownerHandle=([^\&]*)/)[1];
            Services.UpdateTemplate.update({ "template": App.State.sampleTemplate, "ownerHandle": ownerHandle });
        },
        "State.staticText": function () {
            App.$DOM.SampleText.text(App.State.staticText.text);
            var ul = App.$DOM.SampleList;
            function populateList(item, idx) {
                ul.append($("<li />").text(item));
            };
            $.map(App.State.staticText.list, populateList);
        },
        "State.sampleTemplate": function (evt) {
            $(App).trigger("Page.GenerateTemplate");
        },
        "State.sampleVehicle": function (evt) {
            if (typeof App.State.Template !== "undefined") {
                for (var item in App.State.Template.content_areas) App.State.Template.content_areas[item].applyContentData();
            }
        },
        "State.DataKeys": function () {
            $(App).trigger("Page.PopulateDataPoints");
        },
        "State.selectedContentArea": function () {
            $("#font-content-area, #layout-content-area, #content-content-area").show();
            $("#generate_preview, #grid_setup, #page_setup").hide();
        },

        "State.UpdateUIFieldSet": function() {
            // show/hide correct UI field set
            $(".data_point____all").hide();
            var dp_opt = $("#data_points > :selected");
            var selector = ".data_point__";
            if (dp_opt.val() == "26")
                selector += "QRCodeURL";
            else
                selector += "DEFAULT";
            $(selector).show();
        },

        "State.selectedPreviewContentDOM": PopulatePrintValues,

        "Template.changeName": generateTemplateChangeEvent("nameChange"),
        "Template.isBuyersGuideChange": generateTemplateChangeEvent("isBuyersGuideChange"),
        "Font.changeFont": generateStyleChangeEvent("fontChange"),
        "Font.changeFontSize": generateStyleChangeEvent("fontSizeChange"),
        "Font.changeLineHeight": generateStyleChangeEvent("fontLineHeightChange"),
        "Font.changeAlign": generateStyleChangeEvent("alignChange"),
        "Font.changeItalic": generateStyleChangeEvent("fontItalicChange"),
        "Font.changeBold": generateStyleChangeEvent("fontBoldChange"),
        "Font.changeUnderline": generateStyleChangeEvent("fontUnderlineChange"),
        "Font.changeColumns": generateStyleChangeEvent("columnChange"),
        "Position.changeX": generateStyleChangeEvent("xChange"),
        "Position.changeY": generateStyleChangeEvent("yChange"),
        "Position.changeWidth": generateStyleChangeEvent("widthChange"),
        "Position.changeHeight": generateStyleChangeEvent("heightChange"),
        "Data.changeDataPoint": generateStyleChangeEvent("dataPointChange"),
        "Data.changeErrorCorrectionLevel": generateStyleChangeEvent("errorCorrectionLevelChange"),
        "Data.changeStaticText": generateStyleChangeEvent("staticTextChange"),
        "Data.changePrompt": generateStyleChangeEvent("promptChange"),
        "Data.remove": generateStyleChangeEvent("remove")
    };
    function generateStyleChangeEvent(eventName) {
        return function (evt, dom_evt) {
            if (has_selected_content_area()) {
                $(App.State.selectedContentArea).trigger(eventName, dom_evt);
                PopulatePrintValues();
            } /* else {
                // return UI to state it was in before any content area was selected
            } */
        };
    }
    function has_selected_content_area() {
        return (typeof App.State.selectedContentArea !== "undefined"
        && typeof App.State.selectedPreviewContentDOM !== "undefined");
    }

    function simpleNumberStepper(evt) {
        if (evt.which == 40 || evt.which == 38) {
            var value = $(evt.target).val();
            var increment = evt.which === 38;
            var regexp = /(^[0-9\-\.]*)(\d)(.*$)/;
            function incrementLastDidgit(m, m1, m2, m3) {
                var add = increment ? 1 : -1;
                var v = parseInt(m2, 10);
                var adj = v + add;
                var is_zero = adj <= 0;
                var move_up = adj === 10 && increment && m1 !== "";
                var is_float = m.indexOf(".") !== -1;

                if (!is_float) {
                    return [parseFloat(m1 + m2) + add, m3].join("");
                } else if (is_zero) {
                    return [parseFloat(m1), m3].join("");
                }
                if (move_up) {
                    return [parseFloat(m1), m3].join("").replace(regexp, incrementLastDidgit);
                } else {
                    return [m1, (v + add), m3].join("");
                }
            }
            value = value.replace(regexp, incrementLastDidgit);
            $(evt.target).val(value);
            evt.preventDefault();
            evt.stopPropagation();
            $(evt.target).trigger("change");
        } else if (evt.which === 39) {
            var value = $(evt.target).val();
            if (value.indexOf(".") !== -1) {
                $(evt.target).val(value.replace(/(^[0-9\.]*)(\w*$)/, "$10$2"));
            } else {
                $(evt.target).val(value.replace(/(^[0-9]*)(\w*$)/, "$1.0$2"));
            }

            evt.preventDefault();
            evt.stopPropagation();
            $(evt.target).trigger("change");
        }
    }

    function PopulatePrintValues() {
        var printLineHeight = new PrintValues.FontScale(App.DOM.SampleHeight);
        var printCharacterAvg = new PrintValues.FontScale(App.DOM.SampleText);
        var printCharacterList = new PrintValues.FontScale(App.DOM.SampleList);
        var printEmSize = new PrintValues.FontScale(App.DOM.SampleEm);
        var printEnSize = new PrintValues.FontScale(App.DOM.SampleEn);
        var printBulletSize = new PrintValues.FontScale(App.DOM.SampleBullet);

        var point;
        point = printLineHeight.print_size().BottomRight;
        App.$DOM.PrintLineHeight.text(point.Y / 26 + point.Unit);

        App.$DOM.PrintCharacterAvg.text(printCharacterAvg.ratio().toString());

        point = printEmSize.print_size().BottomRight;
        App.$DOM.PrintEmSize.text(point.X / 10 + point.Unit);

        point = printEnSize.print_size().BottomRight;
        App.$DOM.PrintEnSize.text(point.X / 10 + point.Unit);

        point = printBulletSize.print_size().BottomRight;
        App.$DOM.PrintBulletSize.text(point.X + point.Unit);
    };

    function generateTemplateChangeEvent(eventName) {
        var returnFunc = function (evt, dom_evt) {
            $(App.State.Template).trigger(eventName, dom_evt);
        }

        return returnFunc;
    }

    for (var prop in this.Events) {
        if (this.Events[prop].constructor == Array) {
            for (var i = 0; i < this.Events[prop].length; i++) {
                $(App).bind(prop, this.Events[prop][i]);
            };
        } else {
            $(App).bind(prop, this.Events[prop]);
        }
    }

    function DOMBinder() {
        function triggerAppEvent(name) {
            return function (evt) {
                $(App).trigger(name, evt);
            };
        };

        App.$DOM.TemplateName.bind("change", triggerAppEvent("Template.changeName"));
        App.$DOM.IsBuyersGuide.bind("click", triggerAppEvent("Template.isBuyersGuideChange"));
        App.$DOM.GenerateButton.bind("click", triggerAppEvent("Grid.generate"));
        App.$DOM.SamplePDFButton.bind("click", triggerAppEvent("Grid.samplePDF"));
        App.$DOM.PagePreview.bind("mousemove", triggerAppEvent("Grid.reportMousePosition"));
        App.$DOM.PagePreview.bind("mousedown", triggerAppEvent("Page.startCreateDataHolder"));
        App.$DOM.PagePreview.bind("mouseout", triggerAppEvent("Page.clearCreateDataHolder"));
        App.$DOM.PagePreview.bind("mouseup", triggerAppEvent("Page.endCreateDataHolder"));
        App.$DOM.SaveButton.bind("click", triggerAppEvent("Page.confirmSave"));
        App.$DOM.FontSelection.bind("change", triggerAppEvent("Font.changeFont"));
        App.$DOM.FontSize.bind("change", triggerAppEvent("Font.changeFontSize"));
        App.$DOM.LineHeight.bind("change", triggerAppEvent("Font.changeLineHeight"));
        App.$DOM.Align.bind("change", triggerAppEvent("Font.changeAlign"));
        App.$DOM.IsItalic.bind("change", triggerAppEvent("Font.changeItalic"));
        App.$DOM.IsBold.bind("change", triggerAppEvent("Font.changeBold"));
        App.$DOM.IsUnderline.bind("change", triggerAppEvent("Font.changeUnderline"));
        App.$DOM.Columns.bind('change', triggerAppEvent("Font.changeColumns"));
        App.$DOM.X.bind("change", triggerAppEvent("Position.changeX"));
        App.$DOM.Y.bind("change", triggerAppEvent("Position.changeY"));
        App.$DOM.Width.bind("change", triggerAppEvent("Position.changeWidth"));
        App.$DOM.Height.bind("change", triggerAppEvent("Position.changeHeight"));
        App.$DOM.Size.bind("change", (function() {
            var evt_w = triggerAppEvent("Position.changeWidth");
            var evt_h = triggerAppEvent("Position.changeHeight");
            return function (evt) {
                App.$DOM.Width.val(App.$DOM.Size.val());
                App.$DOM.Height.val(App.$DOM.Size.val());
                evt_w( evt );
                evt_h( evt );
            };
        })());
        App.$DOM.DataPoints.bind("change", (function () {
            var eventHandler = triggerAppEvent("Data.changeDataPoint")
            return function (evt) {
                var dp_opt = $("#data_points > :selected");
                if (dp_opt.val() == "26") {
                    // pre-fill size from smallest known dimension, and update larger dimension
                    App.$DOM.Size.val( Math.min( parseFloat(App.$DOM.Width.val()), parseFloat(App.$DOM.Height.val()))+"in" );
                    App.$DOM.Size.trigger("change",{});
                }
                // update UI fields
                $(App).trigger("State.UpdateUIFieldSet");
                // any actions that would have taken place before my modifications
                eventHandler(evt);
            }
        })());
        App.$DOM.QrCodeErrorCorrectionLevel.bind("change", triggerAppEvent("Data.changeErrorCorrectionLevel"));
        App.$DOM.StaticText.bind("change", triggerAppEvent("Data.changeStaticText"));
        App.$DOM.Prompt.bind("change", triggerAppEvent("Data.changePrompt"));
        App.$DOM.RemovePrintArea.bind("click", triggerAppEvent("Data.remove"));

        $("input:text").bind("keydown", simpleNumberStepper);
    };
    $(App).trigger("DOM.beforeFetchDOM");
    /**
    * namespace DOM
    */
    this.$DOM = {
        GenerateButton: $("#generate_preview"),
        SamplePDFButton: $("#sample_pdf"),
        TemplateName: $("#template_name"),
        IsBuyersGuide: $("#is_buyers_guide"),
        BackgroundImage: $(".backgroundImage"),
        PageWidth: $("#page_width"),
        PageHeight: $("#page_height"),
        GridX: $("#grid_x"),
        GridY: $("#grid_y"),
        PagePreview: $("#page_preview"),
        CurrentPosition: $("#current_position"),
        SampleText: $("#sample_text"),
        SampleList: $("#sample_list"),
        SampleBullet: $("#sample_bullet"),
        SampleEm: $("#sample_em"),
        SampleEn: $("#sample_en"),
        SampleHeight: $("#sample_height"),
        FontAreaReport: $("#character_per_area"),
        MergeDataArea: $("#merge_data"),
        PrintLineHeight: $("#print_line_height"),
        PrintCharacterAvg: $("#print_character_avg"),
        PrintEmSize: $("#print_em_size"),
        PrintEnSize: $("#print_en_size"),
        PrintBulletSize: $("#print_bullet_size"),
        SaveButton: $("#save_button"),
        // =========================
        // = Content Area Controls =
        // =========================
        DataPoints: $("#data_points"),
        StaticText: $("#static_text"),
        UrlSource: $("#url_source"),
        StaticUrl: $("#static_url"),
        QrCodeErrorCorrectionLevel: $("#qr_code_error_correction_level"),
        Prompt: $("#prompt"),
        Align: $("#align"),
        LineHeight: $("#line_height"),
        FontSelection: $("#font_selection"),
        FontSize: $("#font_size"),
        IsItalic: $("#is_italic"),
        IsBold: $("#is_bold"),
        IsUnderline: $("#is_underline"),
        SeperateFontForLabel: $("#seperate_font_for_label"),
        X: $("#x"),
        Y: $("#y"),
        Width: $("#width"),
        Height: $("#height"),
        Size: $("#size"),
        Columns: $("#columns"),
        RemovePrintArea: $("#remove_data_point")
    };

    this.DOM = {};

    for (var prop in this.$DOM) {
        this.DOM[prop] = this.$DOM[prop].get(0);
    }

    $(App).trigger("DOM.afterFetchDOM");

    /**
    * namespace Services
    */

    if (typeof (Services) == 'undefined') {
        var Services = {};
    }

    (function () {
        var urls = {
            static_text: "/templating/Services/static_text.json",
            sample_template: "/templating/Services/Template.asmx/Get",
            sample_template_save: "/templating/Services/Template.asmx/Save",
            system_template_save: "/templating/Services/Template.asmx/SystemSave",
            sample_vehicle: "/CouchDb/inventory/",
            generate_pdf: "/templating/Services/Template.asmx/GeneratePdf"
        };

        var Fetch = function (url, variable_to_set, cacheable) {
            this.url = url;
            this.variable_to_set = variable_to_set;
            this.cacheable = cacheable;
        };
        /**
        * fetch
        * @param 
        */
        Fetch.prototype.fetch = function (_data) {
            var GET = {
                url: this.url,
                type: "GET",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: $.proxy(this.onComplete, this),
                "data": _data || {}
            };
            $.ajax(GET);
        };
        /**
        * onComplete
        * @param data
        */
        Fetch.prototype.onComplete = function (data) {
            /// Don't Overwrite Values
            if (typeof App.State[this.variable_to_set] === "undefined") {
                App.State[this.variable_to_set] = data.d || data;
                $(App).trigger("State." + this.variable_to_set);
            };
        };

        Fetch.prototype.onFailure = function () {
            throw "Failed to fetch " + this.variable_to_set;
        };

        var Update = function (url, variable_to_update) {
            this.url = url;
            this.variable_to_update = variable_to_update;
        };
        Update.prototype.update = function (data) {
            if (typeof data !== "undefined") {
                this.data = data;
            }
            var PUT = {
                url: this.url,
                type: "POST",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: this.get_data(),
                success: $.proxy(this.onSuccess, this),
                error: $.proxy(this.onError, this)
            };
            $.ajax(PUT);
        };
        Update.prototype.get_data = function () {
            return JSON.stringify(this.data) || {};
        };
        Update.prototype.onSuccess = function (data) {
            alert("Saved Successfully");
        };
        Update.prototype.onError = function (req, reason, error) {
            alert(reason);
        }

        new Fetch(urls.static_text, "staticText", true).fetch();

        var templateId = { "templateId": window.location.search.match(/[\?\&]template=(\d*)\&?/)[1] };

        if (!!templateId.templateId) {
            new Fetch(urls.sample_template, "sampleTemplate", false).fetch(templateId);
        } else {

            App.State.sampleTemplate = {
                "__type": "VehicleTemplatingWebServiceClient.VehicleTemplatingService.TemplateTO",
                "TemplateId": -1,
                "Height": 11,
                "Width": 8.5,
                "Unit": "in",
                "Name": "",
                "Type": 0,
                "BackgroundImage": $(".backgroundImageFilename").val(),
                "ContentAreas": []
            };
            $(App).trigger("State.sampleTemplate");
        }

        this.SampleVehicle = new Fetch(urls.sample_vehicle, "sampleVehicle");

        this.UpdateTemplate = new Update(urls.sample_template_save);
        this.UpdateSystemTemplate = new Update(urls.system_template_save);

        this.GeneratePDF = {
            generate: function (data) {
                $.ajax({
                    url: urls.generate_pdf,
                    type: "POST",
                    data: data,
                    success: function (data) {
                        alert("Saved Successfully");
                    },
                    error: function (req, reason, error) {
                        alert(reason);
                    }
                });
            }
        };

    }).apply(Services);


    function getValueForVehicle(vehicle, value_key) {
        if (typeof App.State.knownDataPoints !== "undefined") {
            var well_known_key = App.State.knownDataPoints[value_key];
            if (typeof well_known_key !== "undefined") {
                if (typeof well_known_key.values !== "undefined") {
                    var values = [];
                    for (var i = 0, l = well_known_key.values.length; i < l; i++) {
                        values.push(vehicle[well_known_key.values[i]]);
                    };
                    return values.join(" ");
                };
            };
        };

        if (typeof (vehicle[value_key]) !== "undefined") {
            return vehicle[value_key];
        }

        throw "What did you want to do with " + value_key;
    }

    var getValueForVehicleInState = function (value_key) {
        if (typeof App.State.sampleVehicle !== "undefined") {
            return getValueForVehicle(App.State.sampleVehicle, value_key);
        } else if (value_key === "Equipment") {
            var sample_list = [];
            while (sample_list.length <= 100) {
                sample_list.push("<<equipment>>");
            }
            return sample_list;
        } else if (value_key === "Packages") {
            var sample_list = [];
            while (sample_list.length <= 100) {
                sample_list.push("<<packages>>");
            }
            return sample_list;
        } else {
            return "<<" + value_key + ">>";
        }
    };

    App.State.DataKeys = PrintValues.DataKeys;
    $(this).trigger("State.DataKeys");

    this.Services = Services;

    this.getValueForVehicle = getValueForVehicleInState;
    this.PopulatePrintValues = PopulatePrintValues;

    var shared = '';
    var matches = window.location.search.match(/[\?\&]shared=([^\&]*)/);
    if (matches && matches.length > 1) {
        shared = matches[1];
    }

    if (shared === "True") {
        $("#sharedwarning").show();
    }
    $("#dialog-confirm").dialog({
        resizable: false,
        autoOpen: false,
        modal: true,
        draggable: false,
        closeText: ""
    });

}).apply(App);
