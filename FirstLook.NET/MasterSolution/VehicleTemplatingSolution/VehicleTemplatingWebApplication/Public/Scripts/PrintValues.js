/**
 * namespace PrintValues
 */
if (typeof(PrintValues) === 'undefined') {
    var PrintValues = {};
}

 (function() {
    
    /**
     * Holds Print Size and element for a Template Preview
     * @author Paul Monson
     * @version 0.1
     * @requires PrintArea
     */
    
    function PagePreview(el, a, b) {
        this.PrintArea = new PrintArea(a, b);
        this.Element = el;
    };

    function PrintPoint(x, y, unit) {
        this.X = parseFloat(x);
        // convert to int
        this.Y = parseFloat(y);
        // convert to int
        this.Unit = unit;
        this.validate();
    }
    PrintPoint.validateRegex = /^([0-9\.]*)(?:mm|in|pt)?\s?[\,xX\u00D7]\s?([0-9\.]*)(mm|in|pt)$/;
    PrintPoint.fromString = function(str) {
        var match = this.validateRegex.exec(str);
        if ( !! match && match.length >= 3) {
            return new PrintPoint(match[1], match[2], match[3]);
        } else {
            throw str + " is not valid point";
        }
    };
    PrintPoint.fromEvent = function(evt, el) {
        var x = evt.pageX - el.offsetLeft,
        y = evt.pageY - el.offsetTop,
        unit = "px";
        if (typeof App.State.Scale !== "undefined") {
            // possible problem, with type, PM
            x = App.State.Scale.convertPx(x);
            y = App.State.Scale.convertPx(y);
            unit = App.State.Scale.Unit;
        };
        return new PrintPoint(x, y, unit);
    };
    PrintPoint.prototype = {
        units: {
            "mm": "",
            "in": "",
            "pt": ""
        },
        validate: function() {
            if (! (typeof this.X === "number")
            || !(typeof this.Y === "number")
            || !(this.Unit in this.units)) {
                throw "not a valid PrintPoint";
            };
        },
        toString: function() {
            return [this.X + this.Unit, this.Y + this.Unit].join(" \u00D7 ");
        }
    };

    function PrintArea(a, b) {
        this.set_from_points(a, b);
        this.validate();
        this.Unit = a.Unit;
    };
    PrintArea.className = "print_area";
    PrintArea.prototype = {
        set_from_points: function(a, b) {
            var tl_x = a.X < b.X ? a.X: b.X,
            tl_y = a.Y < b.Y ? a.Y: b.Y,
            br_x = a.X >= b.X ? a.X: b.X,
            br_y = a.Y >= b.Y ? a.Y: b.Y;

            this.TopLeft = new PrintPoint(tl_x, tl_y, a.Unit);
            this.BottomRight = new PrintPoint(br_x, br_y, b.Unit);
        },
        validate: function() {
            if (! (this.TopLeft instanceof PrintPoint)
            || !(this.BottomRight instanceof PrintPoint)) {
                throw "not a valid PrintArea";
            };
            if (this.TopLeft.Unit !== this.BottomRight.Unit) {
                throw "A and B must have same units";
            };
        },
        appendTo: function(parent) {
            var el = document.createElement("div");
            this.Element = el;
            $(el).addClass(PrintArea.className);
            this.setStyle();
            parent.appendChild(el);

            return el;
        },
        setStyle: function() {
            $(this.Element).css({
                "position": "absolute",
                "top": this.TopLeft.Y + this.Unit,
                "left": this.TopLeft.X + this.Unit,
                "width": this.BottomRight.X - this.TopLeft.X + this.Unit,
                "height": this.BottomRight.Y - this.TopLeft.Y + this.Unit,
                "z-index": 5
            });
        },
        toString: function() {
            return this.getWidth().toString() + " \u00D7 " + this.getHeight().toString();
        },
        getWidth: function() {
            var width = this.BottomRight.X - this.TopLeft.X;
            return new PrintValue(width, this.Unit);
        },
        getHeight: function() {
            var height = this.BottomRight.Y - this.TopLeft.Y;
            return new PrintValue(height, this.Unit);
        },
        toTemplatePoints: function() {
            return {
                "TopLeft": {
                    "X": this.TopLeft.X,
                    "Y": this.TopLeft.Y
                },
                "BottomRight": {
                    "X": this.BottomRight.X,
                    "Y": this.BottomRight.Y
                }
            };
        },
        size: function() {
            var value = (this.BottomRight.X - this.TopLeft.X) * (this.BottomRight.Y - this.TopLeft.Y);
            return new PrintValue(value, this.Unit, value + "sq " + this.Unit);
        },
        pxSize: function() {
            if (App.State.Scale !== undefined) {
                return {
                    "width": App.State.Scale.convertPrint(this.BottomRight.X - this.TopLeft.X),
                    "height": App.State.Scale.convertPrint(this.BottomRight.Y - this.TopLeft.Y)
                };
            }
        }
    };

    function Scale(value, unit) {
        this.Value = value;
        this.Unit = unit;
    };
    Scale.prototype = {
        convertPx: function(x, decimals) {
            if (typeof decimals !== "number") {
                decimals = 1e3;
            } else if (decimals == -1) {
                decimals = 1e10;
            }
            return (Math.round(x / this.Value * decimals) / decimals);
        },
        convertPrint: function(x) {
            return Math.round(x * this.Value);
        },
        toString: function() {
            return this.Value + "dp" + this.Unit.toLowerCase();
        }
    };

    function FontScale(el) {
        this.Element = el;
    }
    FontScale.prototype = {
        characters: function() {
            return $(this.Element).text().length;
        },
        print_size: function() {
            var x = App.State.Scale.convertPx($(this.Element).outerWidth(true)),
            y = App.State.Scale.convertPx($(this.Element).outerHeight(true)),
            unit = App.State.Scale.Unit;

            return new PrintArea(new PrintPoint(0, 0, unit), new PrintPoint(x, y, unit));
        },
        ratio: function() {
            var print_area = this.print_size();
            var value = Math.floor(this.characters() / print_area.size());
            return new PrintValue(value, " characters per sq " + print_area.Unit);
        }
    };

    function PrintValue(value, unit, string) {
        this.Value = value;
        this.Unit = unit;
        if (string !== undefined) {
            this.String = string;
        }
    }
    PrintValue.prototype = {
        valueOf: function() {
            return this.Value;
        },
        toString: function() {
            return this.String || this.Value + this.Unit;
        }
    };

    function FontSize(str) {
        this.String = str;
        this.fromString(str);
        this.validate();
    };
    FontSize.validateRegex = /^([0-9\.]*)(pt)$/;
    FontSize.prototype = {
        validate: function() {
            if (typeof this.String === "undefined"
            || typeof this.Value !== "number"
            || this.Value < 6
            // Min
            || this.Value > 60
            // Max
            || typeof this.Unit === "undefined") {
                throw "not valid FontSize";
            }
        },
        fromString: function(str) {
            var match = FontSize.validateRegex.exec(str);
            if ( !! match && !!match[1] && !!match[2]) {
                this.Value = parseFloat(match[1]);
                this.Unit = match[2];
            } else {
                throw str + " is not a valid FontSize";
            }
        },
        applyToElement: function(el) {
            el.style.fontSize = this.toString();
        },
        toString: function() {
            return this.String;
        },
        valueOf: function() {
            return this.Value;
        }
    };

    function Template(data, el) {
        this.Data = data;
        this._contentAreas = data.ContentAreas;
        this.Unit = data.Unit;
        this.Element = el;
        if (typeof this._contentAreas !== "undefined") {
            this.apply_content_areas();
        };
        this.applyData();
        for (var _event in this.events) {
            $(this).bind(_event, this.events[_event]);
        }
    }
    Template.prototype = {
        apply_content_areas: function() {
            this.ContentAreas = [];
            for (var i = 0; i < this._contentAreas.length; i++) {
                this.add_content_area(this._contentAreas[i]);
            };
        },
        add_content_area: function(content_area) {
            var template_content_area = new TemplateContentArea(this.Element, content_area, this.Unit);
            $(template_content_area).bind("remove", $.proxy((function(el) {
                return function() {
                    this.remove_content_area(el);
                };
            })(template_content_area), this));
            template_content_area.apply_data_to_DOM();
            this.ContentAreas.push(template_content_area);
        },
        add_content_area_via_print_area: function(print_area) {
            var points = print_area.toTemplatePoints();
            var data_point = {
                "DataPoint": {},
                "TopLeft": points.TopLeft,
                "BottomRight": points.BottomRight,
                "Font": { "LineHeight": 1.2, "Size": 12, "Align": 1 },
                "ExtraDataJSON": null
            };
            var template_content_area = new TemplateContentArea(this.Element, data_point,
            this.Unit, print_area);
            this._contentAreas.push(data_point);
            $(template_content_area).bind("remove", $.proxy((function(el) {
                return function() {
                    this.remove_content_area(el);
                };
            })(template_content_area), this));
            template_content_area.apply_data_to_DOM();
            this.ContentAreas.push(template_content_area);
        },
        remove_content_area: function(el) {
            var index = $.inArray(el, this.ContentAreas);
            this._contentAreas.splice(index, 1);
            this.ContentAreas.splice(index, 1);
        },
        applyData: function() {
                       App.$DOM.TemplateName.val(this.Data.Name);
                       App.$DOM.IsBuyersGuide.attr("checked", this.Data.Type == 1);
                   },
        events: {
                    "nameChange": function(evt, dom_evt) {
                        this.Data.Name = $(dom_evt.target).val();
                        this.applyData();
                    },
                    "isBuyersGuideChange": function(evt, dom_evt) {
                        this.Data.Type = $(dom_evt.target).is(":checked") ? 1 : 0;
                        this.applyData();
                    }
        }
    };
    var TemplateContentArea = function(parent_element, data, unit, printArea) {
        this.ParentElement = parent_element;
        this.Data = data || {};
        this.Unit = unit;
        if (printArea instanceof PrintArea) {
            this.Area = printArea;
        } else {
            this.Area = this.create_print_area();
        }

        if (this.Data.DataPoint === undefined) {
            this.Data.DataPoint = {};
        }
    };
    TemplateContentArea.prototype = {
        create_print_area: function() {
            var content_area = this.Data;
            var start_point = new PrintPoint(content_area.TopLeft.X, content_area.TopLeft.Y, this.Unit);
            var end_point = new PrintPoint(content_area.BottomRight.X, content_area.BottomRight.Y, this.Unit);
            var area = new PrintArea(start_point, end_point);

            area.appendTo(this.ParentElement);

            return area;
        },
        apply_data_to_DOM: function() {
            var events = {
                "applyFont": $.proxy(this.applyFontContent, this),
                "applyColumn": $.proxy(this.applyColumnContent, this),
                "applyData": $.proxy(this.applyContentData, this),
                "applyPosition": $.proxy(this.applyPrintPoint, this),
                "remove": $.proxy(this.removeTemplateContentArea, this)
            };

            for (var eventName in events) {
                $(this.Area.Element).bind(eventName, events[eventName]);
            }

            this.applyContentData();
            this.applyFontContent();
            this.applyPrintPoint();

            $(this.Area.Element).bind("click", $.proxy(function(evt) {
                this.showEditContentArea();
                $(App).trigger("State.UpdateUIFieldSet");
            },
            this));
        },
        applyContentData: function() {
            var content_area = this.Data;
            var area = this.Area;
            var content_text;

            $(area.Element).empty();

            if (!!(content_area.DataPoint||{}).Text) {
                $(area.Element).append(content_area.DataPoint.Text.replace(/\n/, "<br />"));
            } else if (!!(content_area.DataPoint||{}).Key) {
                if (typeof App.getValueForVehicle === "function") {
                    content_text = App.getValueForVehicle(getStringForEnum(content_area.DataPoint.Key, DataKeys));
                } else {
                    content_text = "<<" + getStringForEnum(content_area.DataPoint.Key, DataKeys) + ">>";
                }

                if (content_text instanceof Array) {
                    var ul = $("<ul />");
                    var appendListItem = function(item, idx) {
                        ul.append($("<li />").text(item));
                    };
                    $.map(content_text, appendListItem);
                    $(area.Element).append(ul);
                } else {
                    $(area.Element).text(content_text.toString());
                }
            }

            if (typeof (content_area.DataPoint||{}).Prompt !== "undefined") {
                $(area.Element).addClass("prompted_value");
            };

            this.applyColumnContent();
        },
        applyFontContent: function() {
            var content_area = this.Data;
            var area = this.Area;
            var content_font;
            var css = {};

            if (typeof content_area.Font === "undefined") {
                content_area.Font = {};
            };

            content_font = content_area.Font;

            if (typeof content_font.Family !== "undefined") {
                css.fontFamily = getStringForEnum(content_font.Family, FontFamily);
            }
            if (typeof content_font.Size !== "undefined") {
                css.fontSize = content_font.Size + "pt";
            }
            if (typeof content_font.LineHeight !== "undefined") {
                css.lineHeight = content_font.LineHeight;
            };
            if (typeof content_font.Align !== "undefined") {
                css.textAlign = getStringForEnum(content_font.Align, FontAlign);
            };
            if (typeof content_font.Italics !== "undefined") {
                css.fontStyle = !!content_font.Italics ? "italic": "normal";
            };
            if (typeof content_font.Bold !== "undefined") {
                css.fontWeight = !!content_font.Bold ? "bold": "normal";
            };
            if (typeof content_font.Underline !== "undefined") {
                css.textDecoration = !!content_font.Underline ? "underline": "none";
            };
            $(area.Element).css(css);

            this.calculatePrintValues();

            this.applyColumnContent();
        },
        calculatePrintValues: function() {
            if (App.State.Scale === undefined) return;

            var css = {
                "fontFamily": this.Area.Element.style.fontFamily,
                "fontSize": this.Area.Element.style.fontSize,
                "lineHeight": this.Area.Element.style.lineHeight,
                "textAlign": this.Area.Element.style.textAlign,
                "fontStyle": this.Area.Element.style.fontStyle,
                "fontWeight": this.Area.Element.style.fontWeight,
                "textDecoration": this.Area.Element.style.textDecoration
            };
            $([App.DOM.SampleText, App.DOM.SampleEm, App.DOM.SampleEn, App.DOM.SampleHeight, App.DOM.SampleBullet]).css(css);

            if (this.Data.Font === undefined) {
                this.Data.Font = {};
            };

            this.Data.PrintSizes = {};

            var printLineHeight = new FontScale(App.DOM.SampleHeight);
            var printCharacterAvg = new FontScale(App.DOM.SampleText);
            var printCharacterList = new FontScale(App.DOM.SampleList);
            var printEmSize = new FontScale(App.DOM.SampleEm);
            var printEnSize = new FontScale(App.DOM.SampleEn);
            var printBulletSize = new FontScale(App.DOM.SampleBullet);

            var point;
            point = printLineHeight.print_size().BottomRight;
            this.Data.PrintSizes.LineHeight = point.Y / 26;

            this.Data.PrintSizes.CharacterAvg = printCharacterAvg.ratio().valueOf();

            point = printEmSize.print_size().BottomRight;
            this.Data.PrintSizes.EmSize = point.X / 10;

            point = printEnSize.print_size().BottomRight;
            this.Data.PrintSizes.EnSize = point.X / 10;

            point = printBulletSize.print_size().BottomRight;
            this.Data.PrintSizes.BulletSize = point.X;
        },
        applyColumnContent: function() {
            if (App.State.Scale === undefined) return;

            var content_font = (this.Data|| {}).Font || {};
            var print_sizes = (this.Data||{}).PrintSizes || {};
            var area = this.Area;
            $("ul.col", area.Element).remove();
            $("ul", area.Element).show();
            if (typeof this.Data.Columns !== "undefined" && this.Data.Columns > 1) {
                var columns = this.Data.Columns;
                var width = area.getWidth();
                var column_width = width.valueOf() / this.Data.Columns;
                var column_height = area.getHeight().valueOf();
                var avg_character_width = ((0.07 * print_sizes.EmSize) + (0.93 * print_sizes.EnSize));
                var line_height = print_sizes.LineHeight;
                var bullet_width = print_sizes.BulletSize;
                
                column_width -= bullet_width;

                var column_width = Math.round((column_width * 1e2)) / 1e2;

                var characters_per_line = Math.floor(column_width / avg_character_width);
                var num_lines = Math.floor(column_height / line_height);

                var list_count = 1;
                var line_num = 1;
                var items = [];
                for (var i = 0; i < columns; i++) {
                    items.push($("<ul class='col col_" + i + "' />"));
                };
                function eachLi(idx, item) {
                    if (list_count > columns) return;
                    var length = $(item).text().length;
                    if (length > characters_per_line) {
                        line_num += Math.floor(length / characters_per_line);
                    };

                    line_num++;

                    if (list_count > columns) return;

                    items[list_count - 1].append($(item).clone());
                    $(item).attr("rel", list_count);

                    if (line_num > list_count * num_lines) {
                        list_count++;
                    };
                }

                $("li", area.Element).each(eachLi);
                $("ul", area.Element).hide();

                for (i in items) {
                    var item = items[i];
                    item.css({
                        "position": "absolute",
                        "left": (i*column_width) + width.Unit,
                        "width": column_width + width.Unit
                    });
                    $(area.Element).append(item);
                };
            };
        },
        applyPrintPoint: function() {
		var line_height = ((this.Data.Font.PrintSizes || {}).LineHeight || {});

            this.Area.TopLeft.X = this.Data.TopLeft.X;
            this.Area.TopLeft.Y = this.Data.TopLeft.Y;
            this.Area.BottomRight.X = this.Data.BottomRight.X;
            this.Area.BottomRight.Y = this.Data.BottomRight.Y;
		
		/*****************************************************************
		 * This will ensure the line is exactly cut off at a line height * 
		 *****************************************************************/
	    /*if (line_height > 0) {
		    this.area.bottom_right.y -= (this.data.points.bottom_right.y - this.data.points.top_left.y)%line_height;
	    } */

            this.Area.setStyle();

            this.applyColumnContent();
        },
        showEditContentArea: function(evt) {
            if (typeof this._EditContentArea === "undefined") {
                this._EditContentArea = new TemplateEditContentArea(this.Data, this.Unit);
            }

            $(".print_area").removeClass("selected");
            $(this.Area.Element).addClass("selected");

            App.State.selectedPreviewContentDOM = this.Area.Element;
            this.applyFontContent();
            $(App).trigger("State.selectedPreviewContentDOM");

            $(this._EditContentArea).trigger("loadData");

            $(this.Area.Element).draggable(
            {
                containment: this.ParentElement,
                stop: $.proxy(function(event, ui){
                    var x = App.State.Scale.convertPx(ui.position.left);
                    var y = App.State.Scale.convertPx(ui.position.top);
                    App.$DOM.X.val(x + App.State.Scale.Unit);
                    App.$DOM.Y.val(y + App.State.Scale.Unit);
                    $(this._EditContentArea).trigger("locationDragged", {left:x, top:y});
                },
                this)
            });

        },
        removeTemplateContentArea: function() {
            $(this).trigger("remove");

            $(this.Area.Element).unbind("remove", this.removeTemplateContentArea); //remove dom event as causing stack overflow
            $(this.Area.Element).remove(); 

            delete this.Area;

            $(App).trigger("State.deselectedPreviewContentDOM");
        }
    };

    var TemplateEditContentArea = function(data, unit) {
        this.Data = data;
        this.Unit = unit;
        $(this).unbind();
        for (var _event in this.events) {
            $(this).bind(_event, this.events[_event]);
        }
    };
    TemplateEditContentArea.prototype = {
        events: {
            "remove": function(evt, dom_evt) {
                if (dom_evt !== undefined) {
                    dom_evt.preventDefault();
                    dom_evt.stopPropagation();
                }

                $(App.State.selectedPreviewContentDOM).trigger("remove");
                $(App).trigger("State.deselectedContentArea");
            },
            "loadData": function() {
                this.loadDataPoints();
                this.dummyLoad("StaticText", this.Data.DataPoint.Text);
                this.dummyLoad("Prompt", this.Data.DataPoint.Prompt);

                var font = this.Data.Font || {};
                this.dummyLoad("Align", getStringForEnum(font.Align, FontAlign), "left");
                this.dummyLoad("LineHeight", font.LineHeight, 1.2);
                this.dummyLoad("FontSelection", getStringForEnum(font.Family, FontFamily), "Arial");
                this.dummyLoad("FontSize", font.Size, "12pt");
                this.dummyCheckedLoad("IsItalic", font.Italics, false);
                this.dummyCheckedLoad("IsBold", font.Bold, false);
                this.dummyCheckedLoad("IsUnderline", font.Underline, false);

                var points = this.Data || {
                    TopLeft: "",
                    BottomRight: ""
                };
                this.dummyLoad("X", points.TopLeft.X + this.Unit);
                this.dummyLoad("Y", points.TopLeft.Y + this.Unit);
                this.dummyLoad("Columns", this.Data.Columns || 1);
                function roundFloats(val) {
                    return Math.round(val * 1e5) / 1e5;
                }
                this.dummyLoad("Width", roundFloats(points.BottomRight.X - points.TopLeft.X) + this.Unit);
                this.dummyLoad("Height", roundFloats(points.BottomRight.Y - points.TopLeft.Y) + this.Unit);

                if( this.Data.ExtraDataJSON ) {
                    var xdj = JSON.parse( this.Data.ExtraDataJSON );
                    if( xdj ) {
                        this.dummyLoad("QrCodeErrorCorrectionLevel", xdj.ErrorCorrectionLevel);
                        // pre-fill size from smallest known dimension, and update larger dimension
                        this.dummyLoad("Size", roundFloats(points.BottomRight.Y - points.TopLeft.Y) + this.Unit); // take from height; should be the same as width anyway
                    }
                }

                App.State.selectedContentArea = this;
                $(App).trigger("State.selectedContentArea");
            },
            "fontChange": function(evt, dom_evt) {
                this.ensure_font_object();
                this.Data.Font.Family = getIntForEnum($(dom_evt.target).val() || "Arial", FontFamily); 
                $(App.State.selectedPreviewContentDOM).trigger("applyFont");
            },
            "fontSizeChange": function(evt, dom_evt) {
                this.ensure_font_object();
                this.Data.Font.Size = parseFloat($(dom_evt.target).val()) || 12;
                $(App.State.selectedPreviewContentDOM).trigger("applyFont");
            },
            "fontLineHeightChange": function(evt, dom_evt) {
                this.ensure_font_object();
                this.Data.Font.LineHeight = parseFloat($(dom_evt.target).val()) || 1.2;
                $(App.State.selectedPreviewContentDOM).trigger("applyFont");
            },
            "alignChange": function(evt, dom_evt) {
                this.ensure_font_object();
                this.Data.Font.Align = getIntForEnum($(dom_evt.target).val() || "left", FontAlign);
                $(App.State.selectedPreviewContentDOM).trigger("applyFont");
            },
            "fontItalicChange": function(evt, dom_evt) {
                this.ensure_font_object();
                this.Data.Font.Italics = $(dom_evt.target).is(":checked");
                $(App.State.selectedPreviewContentDOM).trigger("applyFont");
            },
            "fontBoldChange": function(evt, dom_evt) {
                this.ensure_font_object();
                this.Data.Font.Bold = $(dom_evt.target).is(":checked");
                $(App.State.selectedPreviewContentDOM).trigger("applyFont");
            },
            "fontUnderlineChange": function(evt, dom_evt) {
                this.ensure_font_object();
                this.Data.Font.Underline = $(dom_evt.target).is(":checked");
                $(App.State.selectedPreviewContentDOM).trigger("applyFont");
            },
            "locationDragged": function(evt, position) {
                this.ensure_points_object();
                this.setX(position.left);
                this.setY(position.top);
                $(App.State.selectedPreviewContentDOM).trigger("applyPosition");
            },
            "xChange": function(evt, dom_evt) {
                this.ensure_points_object();
                var value = parseFloat($(dom_evt.target).val());
                this.setX(value);
                $(App.State.selectedPreviewContentDOM).trigger("applyPosition");
            },
            "yChange": function(evt, dom_evt) {
                this.ensure_points_object();
                var value = parseFloat($(dom_evt.target).val());
                this.setY(value);
                $(App.State.selectedPreviewContentDOM).trigger("applyPosition");
            },
            "widthChange": function(evt, dom_evt) {
                this.ensure_points_object();
                var value = parseFloat($(dom_evt.target).val()) + this.Data.TopLeft.X;
                this.Data.BottomRight.X = value;
                $(App.State.selectedPreviewContentDOM).trigger("applyPosition");
            },
            "heightChange": function(evt, dom_evt) {
                this.ensure_points_object();
                var value = parseFloat($(dom_evt.target).val()) + this.Data.TopLeft.Y;
                this.Data.BottomRight.Y = value;
                $(App.State.selectedPreviewContentDOM).trigger("applyPosition");
            },
            "dataPointChange": function(evt, dom_evt) {
                var value = $(dom_evt.target).val();
                if (value !== "static_text") {
                    this.Data.DataPoint.Key = parseInt(value, 10);
                };
                if( value == "26" ) {
                    // changed Data Point to QRCodeURL from some other value
                    // create a new internal "extra data" structure
                    this.Data.ExtraDataJSON = JSON.stringify({
                        ErrorCorrectionLevel: "L" // default error correction level
                    });
                    // update the UI to match this state
                    this.dummyLoad("QrCodeErrorCorrectionLevel", "L");
                } else {
                    this.Data.ExtraDataJSON = null;
                }
                this.loadDataPoints();
                $(App.State.selectedPreviewContentDOM).trigger("applyData");
            },
            "staticTextChange": function(evt, dom_evt) {
                var value = $(dom_evt.target).val();
                if (value !== "") {
                    this.Data.DataPoint.Text = value;
                    if (typeof this.Data.DataPoint.Key !== "undefined") {
                        delete this.Data.DataPoint.Key;
                    }
                } else {
                    delete this.Data.DataPoint.Text;
                }
                this.loadDataPoints();
                $(App.State.selectedPreviewContentDOM).trigger("applyData");
            },
            "promptChange": function(evt, dom_evt) {
                var value = $(dom_evt.target).val();
                if (value !== "") {
                    this.Data.DataPoint.Prompt = value;
                } else {
                    delete this.Data.DataPoint.Prompt;
                }
                this.loadDataPoints();
                $(App.State.selectedPreviewContentDOM).trigger("applyData");
            },
            "columnChange": function(evt, dom_evt) {
                this.ensure_font_object();
                var value = $(dom_evt.target).val();
                if (value !== "") {
                    this.Data.Columns = parseInt(value, 10);
                    $(App.State.selectedPreviewContentDOM).trigger("applyFont");
                } else {
                    this.Data.Columns = 1;
                }
            },
            "errorCorrectionLevelChange": function(evt, dom_evt) {
                var value = $(dom_evt.target).val();
                var xdj;
                if( this.Data.ExtraDataJSON )
                    xdj = JSON.parse( this.Data.ExtraDataJSON );
                else
                    xdj = {};
                xdj.ErrorCorrectionLevel = value;
                this.Data.ExtraDataJSON = JSON.stringify( xdj );
            }
        },
        ensure_font_object: function() {
            if (typeof this.Data.Font === "undefined") {
                this.Data.Font = {};
            };
        },
        ensure_points_object: function() {
            if (typeof this.Data === "undefined") {
                this.Data= {
                    TopLeft: {
                        X: 0,
                        Y: 0
                    },
                    BottomRight: {
                        X: 0,
                        Y: 0
                    }
                };
            };
        },
        hasValue: function(v) {
             if (typeof v === "string") return v !== ""; 
            return typeof v !== "undefined";
        },
        loadDataPoints: function() {
            if (this.hasValue(this.Data.DataPoint.Text)) {
                App.$DOM.DataPoints.val("static_text");
            } else {
                if (this.hasValue(this.Data.DataPoint.Key)) {
                    App.$DOM.DataPoints.val(this.Data.DataPoint.Key);
                } else {
                    App.$DOM.DataPoints.attr("selectedIndex", 0);
                }
            }
        },
        loadStaticText: function() {
            App.$DOM.StaticText.val(this.Data.DataPoint.Text || "");
        },
        dummyLoad: function(dom_key, val, def) {
            App.$DOM[dom_key].val(val || def || "");
        },
        dummyCheckedLoad: function(dom_key, val, def) {
            App.$DOM[dom_key].attr("checked", val || def);
        },
        setX: function(value){
            var delta = value - this.Data.TopLeft.X;
            this.Data.TopLeft.X = value;
            this.Data.BottomRight.X += delta;
        },
        setY: function(value){
             var delta = value - this.Data.TopLeft.Y;
             this.Data.TopLeft.Y = value;
             this.Data.BottomRight.Y += delta;
        }
    };


    // ============
    // = Commands =
    // ============
    function GenerateSizeReport(el, page) {
        this.El = el;
        this.Height = $(el).height();
        this.Width = $(el).width();
        if (typeof page !== "undefined") {
            this.Page = page;
        };
        this.validate();
    };
    GenerateSizeReport.prototype = {
        validate: function() {
            if (typeof this.El === "undefined"
            || typeof this.Height !== "number"
            || typeof this.Width !== "number"
            || !(this.Page instanceof PagePreview)) {
                throw "not a valid Report Size";
            };
        },
        execute: function() {
            this.cleanReport();
            this.createReport();
            this.appendReport();
        },
        createReport: function() {
            var frag = document.createDocumentFragment(),
            list = document.createElement("p"),
            height = document.createElement("span"),
            width = document.createElement("span"),
            ratio = document.createElement("span"),
            px_height = $(this.El).innerHeight(),
            px_width = $(this.El).innerWidth(),
            height_text = document.createTextNode(px_height + "px"),
            width_text = document.createTextNode(px_width + "px");

            App.State.Scale = new Scale(px_width / this.Page.PrintArea.BottomRight.X, this.Page.PrintArea.Unit);

            $(App.State).trigger("scaleSet");

            ratio.appendChild(document.createTextNode(App.State.Scale.toString()));
            ratio.className = "scale";
            height.appendChild(height_text);
            height.className = "height";
            width.appendChild(width_text);
            width.className = "width";
            list.appendChild(height);
            list.appendChild(document.createTextNode(" \u00D7 "));
            list.appendChild(width);
            list.appendChild(document.createTextNode(" at "));
            list.appendChild(ratio);
            list.className = "size_list";

            frag.appendChild(list);

            this.Report = frag;
        },
        appendReport: function() {
            $(this.El).before(this.Report);
            delete this.Report;
        },
        cleanReport: function() {
            $("p.size_list").remove();
        }
    };

    function GenerateGridCommand(a, b, page) {
        this.Size = new PrintArea(a, b);
        if (page instanceof PagePreview) {
            this._Page = page;
        };
        this.validate();
    };
    GenerateGridCommand.prototype = {
        gridClassName: "overlay_grid",
        validate: function() {
            if (this.Size.Unit !== this.getPage().PrintArea.Unit) {
                throw "Units must match";
            };
        },
        execute: function() {
            var page = this.getPage(),
            x = page.PrintArea.BottomRight.X,
            y = page.PrintArea.BottomRight.Y,
            unit = page.PrintArea.Unit;

            this.cleanGrid(page.Element);
            this.createGrid(x, y, unit);
            $(page.Element).width(x + unit).height(y + unit);
            this.appendGrid(page.Element);
        },
        createGrid: function(max_x, max_y, unit) {
            var frag = document.createDocumentFragment(),
            table = document.createElement("table"),
            tableBody = document.createElement("tbody"),
            x_cells = max_x / this.Size.BottomRight.X,
            y_cells = max_y / this.Size.BottomRight.Y,
            height = this.Size.BottomRight.Y + unit,
            width = this.Size.BottomRight.X + unit,
            max_hieght = max_y + unit,
            max_width = max_x + unit,
            row,
            cell;

            $(table).attr("cellspacing", 0).addClass(this.gridClassName);
            $(table).height(max_hieght).width(max_width);
            for (var i = 0; i < y_cells; i++) {
                row = document.createElement("tr");
                $(row).height(height);
                for (var j = 0; j < x_cells; j++) {
                    cell = document.createElement("td");
                    $(cell).width(width);
                    row.appendChild(cell);
                };
                tableBody.appendChild(row);
            };
            table.appendChild(tableBody);
            frag.appendChild(table);

            this.TableFragment = frag;
        },
        appendGrid: function(el) {
            el.appendChild(this.TableFragment);
            delete this.TableFragment;
        },
        cleanGrid: function(el) {
            $("table." + this.gridClassName, el).remove();
        },
        getPage: function() {
            if (typeof this._Page === "undefined") {
                this._Page = new PagePreview();
            };
            return this._Page;
        }
    };

    function GenerateFontSizeReport(el, fontScale) {
        this.Element = el;
        this.FontScale = fontScale;
    }
    GenerateFontSizeReport.prototype = {
        execute: function() {
            $(this.Element).text(this.FontScale.ratio().toString());
        }
    };

    function getStringForEnum (value, _enum) {
	    if (isNaN(value)) {
		    return value;
	    } else {
		    return _enum[value];
	    }
    }

    function getIntForEnum(value, _enum) {
       for (var i=0, l = _enum.length; i < l; i++) {
           if (value === _enum[i]) 
               return i;
       }
    }

    var FontFamily = [
	    "Arial",
	    "Arial Black",
	    "Comic Sans MS",
	    "Courier New",
	    "Georgia",
	    "Impact",
	    "Lucidia Console",
	    "Lucidia Grande",
	    "Tahoma",
	    "Time New Roman",
	    "Trebuchet",
	    "Verdana"
    ]

    var FontAlign = [
    	"center",
        "left",
        "right"
    ];

    var DataKeys = [
    	"",
        "Year",
        "Make",
        "Model",
        "Trim",
        "VehicleDescription",
        "ExteriorColor",
        "InteriorColor",
        "Mileage",
        "StockNumber",
        "Transmission",
        "Engine",
        "DriveTrain",
        "Certified",
        "BodyStyle",
        "Vin",
        "Series",
        "Price",
        "Equipment",
        "AgeInDays",
        "MpgCity",
        "MpgHwy",
        "CertifiedID",
        "KBBBookValue",
        "Packages",
        "NADABookValue",
        "QRCodeURL",
        "MaxShowroomDirectURL",
        "MaxShowroomSMSCode",
        "MaxShowroomSMSPhoneNumber",
        "MSRP"
    ];

    var KnownPrompts = [
       "No Warranty",
       "Limited Warranty",
       "Full Warranty",
       "Service Contract"
    ];

    this.PagePreview = PagePreview;
    this.PrintPoint = PrintPoint;
    this.PrintArea = PrintArea;
    this.Scale = Scale;
    this.FontScale = FontScale;
    this.PrintValue = PrintValue;
    this.FontSize = FontSize;
    this.FontFamily = FontFamily;
    this.FontAlign = FontAlign;
    this.DataKeys = DataKeys;
    this.Template = Template;

    this.GenerateSizeReport = GenerateSizeReport;
    this.GenerateGridCommand = GenerateGridCommand;
    this.GenerateFontSizeReport = GenerateFontSizeReport;
}).apply(PrintValues);
