﻿
var TemplateManagerHome = function() {};

TemplateManagerHome.prototype = {
    
    /// Initializes client-side page functionality on page load
    onReady: function() {
        
        this.initTabs();
        this.setSelectedTab();
        this.initActionLinks();
        this.initNewWindowLinks();
        $("#Container").css("visibility", "visible");
        
    },
    
    /// Initalizes share, copy, remove and delete links
    initActionLinks: function() {
        
        $("ul.actions a.share, ul.actions a.copy").bind("click", function(e) {
        
            // Reset business unit IDs field
            var bidsField = $("input:hidden.bids")
            $(bidsField).val("");
        
            // Get postback javascript
            var action = $(this).attr("href");
            
            // Get dataset type (buyer's guide or window sticker)
            var dataType = $(this).parents("table").attr("id").indexOf("BuyersGuide") > -1 ? "BuyersGuide" : "WindowSticker";
            
            // Get requested action
            var actionType = $(this).hasClass("share") ? "Share" : "Copy";
            
            // Assign corresponding modal
            var modal = $("#" + actionType + "TemplateModal");
            
            // Setup modal dialog
            $(modal).dialog({ buttons: [ 
                                          { text: actionType + " Template",
                                            click: function() {
                                              
                                                  // Hide error message
                                                  $(modal).find("p.error").hide();
                                                  
                                                  // Validate business unit IDs and continue
                                                  if ($(modal).find("input:text").val() === "") {
                                                      $(modal).find("p.error").show();
                                                  } else {
                                                      $(bidsField).val($(modal).find("input:text").val());
                                                      eval(action);
                                                  }
                                                  
                                              }
                                          },
                                          { text: "Cancel",
                                              click: function() {
                                                  // Hide error, reset textbox and close dialog
                                                  $(modal).find("p.error").hide();
                                                  $(modal).find("input:text").val("");
                                                  $(this).dialog("close");
                                              }
                                          }
                                       ], closeText: "", dialogClass: "modal", draggable: false, modal: true, resizable: false 
                            });
            
            // Prevent postback on click
            e.preventDefault();
            e.stopPropagation();
        });
        
        $("ul.actions a.remove").bind("click", function(e) {
            
            var removeMode = $(this).hasClass("removeAll") ? "removeAll" : "remove";
            var confirmMessage;
            
            if (removeMode == "removeAll") {
                confirmMessage = "Warning: This action will remove this template's association with ALL dealers.";
            } else {
                confirmMessage = "Warning: This action will remove this dealer's association with this template.";
            }
            
            if (!confirm( confirmMessage )) {
                e.preventDefault();
                e.stopPropagation();
            }
        
        });
        
        $("ul.actions a.delete").bind("click", function(e) {
        
            if (!confirm("Warning: This action will permanently delete this template.")) {
                e.preventDefault();
                e.stopPropagation();
            }
        
        });
        
    },
    
    initNewWindowLinks: function() {
    
        $("a.newWindow").live("click", function() {
        
            var href = $(this).attr("href");
            var windowName = $(this).attr("name");
            var options = $(this).attr("options");
            
            window.open(href, windowName, options);
            
            return false;
        
        });
    
    },
    
    initTabs: function() {
        
        // Setup tab click handler
        $("#Tabset a").bind("click", { pageObj: this }, function(e) {
            if ($(this).parents("li").hasClass("selected")) return;
            //$("input.tabIndex").val( $(this).parents("li").index() );
            $.cookie("SelectedTab", $(this).parents("li").index());
            e.data.pageObj.setSelectedTab();
        });
        
    },
    
    setSelectedTab: function() {
        
        var selectedTab = $.cookie("SelectedTab");//$("input.tabIndex").val();
        selectedTab = selectedTab === "-1" || selectedTab === null ? 0 : parseInt( selectedTab, 10 );
        //$("input.tabIndex").val( selectedTab );
        $.cookie("SelectedTab", selectedTab);
        
        $("#Tabset ul li").removeClass( "selected" );
        $("#TabContent > div").hide();
        
        $("#Tabset ul li:eq(" + selectedTab + ")").addClass( "selected" );
        $("#TabContent" + selectedTab).show();
        
    }

};


var Page;
$(document).ready(function() {
	Page = new TemplateManagerHome();
	Page.onReady();
  $("#DealerBuyersGuidesTable, #DealerWindowStickersTable").tablesorter({
      sortList: [[1,0]],
      headers: { 2: {sorter: false } }
  });
});