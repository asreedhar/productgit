﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.DomainModel.Oltp;
using VehicleTemplatingDomainModel;
using VehicleTemplatingWebServiceClient.VehicleTemplatingService;
using TemplateInfo=VehicleTemplatingWebServiceClient.VehicleTemplatingService.TemplateInfo;

namespace VehicleTemplatingWebApplication.Pages.Admin
{
    public partial class Home : Page
    {
        private VehicleTemplatingService _templatingService;
        private VehicleTemplatingService TemplatingService
        {
            get
            {
                if (_templatingService == null)
                {
                    _templatingService = new VehicleTemplatingService();
                }
                return _templatingService;
            }
        }

        private string OwnerHandle
        {
            get
            {
                return Request.Params["OwnerHandle"];
            }
        }

        private IList<TemplateInfo> _windowStickers;
        private IList<TemplateInfo> WindowStickers
        {
            get
            {
                if (_windowStickers == null)
                {
                    _windowStickers = TemplatingService.GetWindowStickerTemplateInfoForOwner(OwnerHandle);
                }
                return _windowStickers;
            }
        }

        private IList<TemplateInfo> _buyersGuide;
        private IList<TemplateInfo> Buyersguide
        {
            get
            {
                if (_buyersGuide == null)
                {
                    _buyersGuide = TemplatingService.GetBuyersGuideTemplateInfoForOwner(OwnerHandle);
                }
                return _buyersGuide;
            }
        }

        private IList<TemplateInfo> _sharedWindowStickers;
        private IList<TemplateInfo> SharedWindowStickers
        {
            get
            {
                if (_sharedWindowStickers == null)
                {
                    _sharedWindowStickers = TemplatingService.GetSharedWindowStickerTemplateInfo();
                }
                return _sharedWindowStickers;
            }
        }

        private IList<TemplateInfo> _sharedBuyersGuide;
        private IList<TemplateInfo> SharedBuyersGuide
        {
            get
            {
                if (_sharedBuyersGuide == null)
                {
                    _sharedBuyersGuide = TemplatingService.GetSharedBuyersGuideTemplateInfo();
                }
                return _sharedBuyersGuide;
            }
        }

        private IList<TemplateInfo> _orphanedWindowStickers;
        private IList<TemplateInfo> OrphanedWindowStickers
        {
            get
            {
                if (_orphanedWindowStickers == null)
                {
                    _orphanedWindowStickers = TemplatingService.GetOrphanedWindowStickerTemplateInfo();
                }
                return _orphanedWindowStickers;
            }
        }

        private IList<TemplateInfo> _orphanedBuyersGuide;
        private IList<TemplateInfo> OrphanedBuyersGuide
        {
            get
            {
                if (_orphanedBuyersGuide == null)
                {
                    _orphanedBuyersGuide = TemplatingService.GetOrphanedBuyersGuideTemplateInfo();
                }
                return _orphanedBuyersGuide;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // Templates by dealer
            BuyersGuideRepeater.Visible = Buyersguide.Count >= 0;
            WindowStickerRepeater.Visible = WindowStickers.Count >= 0;
            BuyersGuideRepeater.DataSource = Buyersguide;
            WindowStickerRepeater.DataSource = WindowStickers;

            // Shared templates
            SharedBuyersGuideRepeater.Visible = SharedBuyersGuide.Count >= 0;
            SharedWindowStickerRepeater.Visible = SharedWindowStickers.Count >= 0;
            SharedBuyersGuideRepeater.DataSource = SharedBuyersGuide;
            SharedWindowStickerRepeater.DataSource = SharedWindowStickers;

            // Orphaned templates
            OrphanedBuyersGuideRepeater.Visible = OrphanedBuyersGuide.Count >= 0;
            OrphanedWindowStickerRepeater.Visible = OrphanedWindowStickers.Count >= 0;
            OrphanedBuyersGuideRepeater.DataSource = OrphanedBuyersGuide;
            OrphanedWindowStickerRepeater.DataSource = OrphanedWindowStickers;

            Page.PreRenderComplete += Page_PreRenderComplete;

        }

        private void Page_PreRenderComplete(object sender, EventArgs e)
        {
            if (BuyersGuideRepeater.Visible)
            {
                BuyersGuideRepeater.DataBind();
            }
            if (WindowStickerRepeater.Visible)
            {
                WindowStickerRepeater.DataBind();
            }

            if (SharedBuyersGuideRepeater.Visible)
            {
                SharedBuyersGuideRepeater.DataBind();
            }
            if (SharedWindowStickerRepeater.Visible)
            {
                SharedWindowStickerRepeater.DataBind();
            }

            if (OrphanedBuyersGuideRepeater.Visible)
            {
                OrphanedBuyersGuideRepeater.DataBind();
            }
            if (OrphanedWindowStickerRepeater.Visible)
            {
                OrphanedWindowStickerRepeater.DataBind();
            }

            // Set Dealer Name in Templates By Dealer tab
            DealerNameLiteral.Text = Owner.GetOwner(Request["ownerHandle"]).Name;

            DisplayManagerActionStatus();

        }

        private void DisplayManagerActionStatus()
        {
            if (Session["ManagerActionStatus"] == null) return;

            var statusReport = Session["ManagerActionStatus"];

            BulkActionStatusPanel.Visible = true;
            BulkActionStatusRepeater.DataSource = statusReport;
            BulkActionStatusRepeater.DataBind();

            Session.Remove("ManagerActionStatus");

        }

        private string UriForTemplate(TemplateInfo templateInfo)
        {
            return "~/Pages/Admin/ModifyTemplate.aspx?template=" + templateInfo.Id + "&ownerHandle=" + OwnerHandle + "&shared=" + templateInfo.Shared;
        }

        protected void WindowStickerRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var repeater = sender as Repeater;
            if (repeater == null) return;

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                HyperLink hyperLink = e.Item.FindControl("WindowStickerTemplateHyperLink") as HyperLink;
                TemplateInfo templateInfo = e.Item.DataItem as TemplateInfo;

                PopulateHyperLink(templateInfo, hyperLink);
                PopulateTemplateId(e.Item);

                // TODO: Wire this up:
                Image sharedTemplateCheckmark = e.Item.FindControl("SharedWindowStickerCheckmark") as Image;
                if (sharedTemplateCheckmark != null && (bool) templateInfo.Shared)
                {
                    sharedTemplateCheckmark.Visible = true;
                }
            }

            if (e.Item.ItemType == ListItemType.Footer && repeater.Items.Count < 1)
            {
                Label lblFooter = (Label)e.Item.FindControl("RepeaterEmptyLabel");
                lblFooter.Visible = true;
            }
        }

        protected void BuyersGuideRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var repeater = sender as Repeater;
            if (repeater == null) return;

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem) {

                HyperLink hyperLink = e.Item.FindControl("BuyersGuideHyperLink") as HyperLink;
                TemplateInfo templateInfo = e.Item.DataItem as TemplateInfo;

                PopulateHyperLink(templateInfo, hyperLink);
                PopulateTemplateId(e.Item);

                // TODO: Wire this up:
                Image sharedTemplateCheckmark = e.Item.FindControl("SharedBuyersGuideCheckmark") as Image;
                if (sharedTemplateCheckmark != null && (bool)templateInfo.Shared)
                {
                    sharedTemplateCheckmark.Visible = true;
                }
            }

            if (e.Item.ItemType == ListItemType.Footer && repeater.Items.Count < 1)
            {
                Label lblFooter = (Label) e.Item.FindControl("RepeaterEmptyLabel");
                lblFooter.Visible = true;
            }
        }

        private static void PopulateTemplateId(RepeaterItem item)
        {
            TemplateInfo templateInfo = item.DataItem as TemplateInfo;

            // Avoid null reference error on tempalteInfo
            if (templateInfo == null) return;

            LinkButton shareBtn;
            LinkButton copyBtn;
            LinkButton removeBtn;
            LinkButton deleteBtn;

            if (item.ClientID.Contains("WindowStickerRepeater"))
            {
                shareBtn = item.FindControl("ShareStickerLink") as LinkButton;
                copyBtn = item.FindControl("CopyStickerLink") as LinkButton;
                removeBtn = item.FindControl("RemoveStickerLink") as LinkButton;
                deleteBtn = item.FindControl("DeleteStickerLink") as LinkButton;
            }
            else
            {
                shareBtn = item.FindControl("ShareBuyersGuideLink") as LinkButton;
                copyBtn = item.FindControl("CopyBuyersGuideLink") as LinkButton;
                removeBtn = item.FindControl("RemoveBuyersGuideLink") as LinkButton;
                deleteBtn = item.FindControl("DeleteBuyersGuideLink") as LinkButton;
                
            }

            if (shareBtn != null) { shareBtn.Attributes.Add("templateid", templateInfo.Id.ToString()); }
            if (copyBtn != null) { copyBtn.Attributes.Add("templateid", templateInfo.Id.ToString()); }
            if (removeBtn != null) { removeBtn.Attributes.Add("templateid", templateInfo.Id.ToString()); }
            if (deleteBtn != null) { deleteBtn.Attributes.Add("templateid", templateInfo.Id.ToString()); }

        }

        private void PopulateHyperLink(TemplateInfo templateInfo, HyperLink hyperLink)
        {
            if (hyperLink != null && templateInfo != null)
            {
                hyperLink.NavigateUrl = UriForTemplate(templateInfo);
                hyperLink.Text = templateInfo.Name;
            }
        }

        protected void NewWindowStickerHyperLink_PreRender(object sender, EventArgs e)
        {
            NewWindowStickerHyperLink.NavigateUrl += "?template=-1&ownerHandle=" + OwnerHandle;
        }

        protected void CopyTemplate_Click(object sender, EventArgs e)
        {
            LinkButton button = sender as LinkButton;

            if (button == null || string.IsNullOrEmpty(button.Attributes["templateid"])) return;

            var templateID = int.Parse(button.Attributes["templateid"]);
            var businessuUnitIds = BusinessUnitIdsHidden.Value.Replace(" ", string.Empty).Split(new[] { ',' });

            Template template = Template.GetTemplate(templateID);

            // Prepare list of action status reports
            List<ManagerActionStatus> statusReport = new List<ManagerActionStatus>();

            foreach (string bid in businessuUnitIds)
            {
                try
                {
                    template.TemplateId = -1;
                    template.Save(Owner.GetOwner(int.Parse(bid)).Handle, Thread.CurrentPrincipal.Identity.Name);
                    statusReport.Add(new ManagerActionStatus(bid, "Success"));
                }
                catch
                {
                    // Make sure we're not tracking empty strings returned from Split()
                    if (!string.IsNullOrEmpty(bid))
                    {
                        statusReport.Add(new ManagerActionStatus(bid, "Failed"));
                    }

                    continue;
                }

            }

            Session["ManagerActionStatus"] = statusReport;
            Response.Redirect(Request.Path + "?" + Request.QueryString);
        }

        protected void ShareTemplate_Click(object sender, EventArgs e)
        {
            LinkButton button = sender as LinkButton;
            if (button == null || string.IsNullOrEmpty(button.Attributes["templateid"])) return;

            var templateID = int.Parse(button.Attributes["templateid"]);
            var businessuUnitIds = BusinessUnitIdsHidden.Value.Replace(" ", string.Empty).Split(new char[] { ',' });

            Template template = Template.GetTemplate(templateID);

            // Prepare list of action status reports
            List<ManagerActionStatus> statusReport = new List<ManagerActionStatus>();

            foreach (string bid in businessuUnitIds)
            {
                try
                {
                    template.Share(Owner.GetOwner(int.Parse(bid)).Id);
                    statusReport.Add(new ManagerActionStatus(bid, "Success"));
                }
                catch
                {
                    if (!string.IsNullOrEmpty(bid))
                    {
                        statusReport.Add(new ManagerActionStatus(bid, "Failed"));
                    }

                    return;
                }


            }

            Session["ManagerActionStatus"] = statusReport;
            Response.Redirect(Request.Path + "?" + Request.QueryString);
        }

        protected void DeleteTemplate_Click(object sender, EventArgs e)
        {
            LinkButton button = sender as LinkButton;

            if (button == null || string.IsNullOrEmpty(button.Attributes["templateid"])) return;

            var templateID = int.Parse(button.Attributes["templateid"]);

            Template template = Template.GetTemplate(templateID);
            template.Delete();


            Response.Redirect(Request.Path + "?" + Request.QueryString);
        }

        protected void RemoveTemplate_Click(object sender, EventArgs e)
        {
            LinkButton button = sender as LinkButton;

            if (button == null || string.IsNullOrEmpty(button.Attributes["templateid"])) return;

            var templateID = int.Parse(button.Attributes["templateid"]);

            Template template = Template.GetTemplate(templateID);
            template.Remove(Owner.GetOwner(Request["ownerHandle"]).Id);

            Response.Redirect(Request.Path + "?" + Request.QueryString);
        }

        protected void RemoveTemplateFromAll_Click(object sender, EventArgs e)
        {
            LinkButton button = sender as LinkButton;

            if (button == null || string.IsNullOrEmpty(button.Attributes["templateid"])) return;

            var templateID = int.Parse(button.Attributes["templateid"]);

            Template template = Template.GetTemplate(templateID);
            template.Remove();

            Response.Redirect(Request.Path + "?" + Request.QueryString);
        }

        public class ManagerActionStatus
        {
            public ManagerActionStatus(string bid, string message)
            {
                Buid = bid;
                StatusMessage = message;
            }

            public string Buid { get; set; }
            public string StatusMessage { get; set; }
        }

    }
}
