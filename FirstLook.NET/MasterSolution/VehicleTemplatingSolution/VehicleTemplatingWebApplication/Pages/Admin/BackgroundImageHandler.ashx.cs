﻿using System.Collections.Specialized;
using System.Web;
using System.Web.Services;

namespace VehicleTemplatingWebApplication.Pages.Admin
{
    /// <summary>
    /// BackgroundImageHandler class is used to serve 
    /// dynamic background images to the Template Manager.
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class BackgroundImageHandler : IHttpHandler
    {
        private string _buid;

        public void ProcessRequest(HttpContext context)
        {
            HttpRequest request = context.Request;
            HttpResponse response = context.Response;

            NameValueCollection qs = request.QueryString;

            // get business unit id
            _buid = request["bu"];

            string fileName = qs["img"];
            string filePath = string.Concat(TemplateManagerUtilities.GetDocumentRoot(), _buid, @"\", fileName);

            response.Clear();
            response.ContentType = TemplateManagerUtilities.GetContentType(request.QueryString);
            response.WriteFile(filePath);
            response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

    }
}
