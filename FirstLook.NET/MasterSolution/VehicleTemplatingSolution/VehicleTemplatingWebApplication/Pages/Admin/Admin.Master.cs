﻿using System;
using FirstLook.DomainModel.Oltp;

namespace VehicleTemplatingWebApplication.Pages.Admin
{
    public partial class Admin : System.Web.UI.MasterPage
    {
        private Owner _owner;
        private Owner Owner
        {
            get
            {
                if (_owner == null)
                {
                    int dealerId = -1;
                    if (Int32.TryParse(Request.QueryString["dealerId"], out dealerId))
                    {
                        _owner = Owner.GetOwner(dealerId);
                    }


                }

                return _owner;
            }
        }

        private string _ownerHandle;
        private string OwnerHandle
        {
            get
            {
                if (string.IsNullOrEmpty(_ownerHandle))
                {
                    _ownerHandle = !string.IsNullOrEmpty(Request.QueryString["ownerHandle"])
                                       ? Request.QueryString["ownerHandle"]
                                       : Owner.Handle;
                }

                return _ownerHandle;
            }
        }

        private string TemplateId
        {
            get
            {
                return Request.Params["template"];
            }
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["dealerId"]))
            {
                if (!string.IsNullOrEmpty(TemplateId))
                {
                    Response.Redirect("~/Pages/Admin/ModifyTemplate.aspx" + "?template=" + TemplateId + "&ownerHandle=" + OwnerHandle); 
                }
                Response.Redirect("~/Pages/Admin/Home.aspx" + "?ownerHandle=" + OwnerHandle);
            }
        }
    }
}
