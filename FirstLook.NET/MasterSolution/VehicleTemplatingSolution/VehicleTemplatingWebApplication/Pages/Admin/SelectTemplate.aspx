﻿<%@ Page Title="Select Template | Template Manager" Language="C#" AutoEventWireup="true" CodeBehind="SelectTemplate.aspx.cs" Inherits="VehicleTemplatingWebApplication.Pages.Admin.CreateTemplate"
    MasterPageFile="~/Pages/Admin/Admin.Master" %>

<asp:Content ID="HeadContent" runat="server" ContentPlaceHolderID="HeadContentPlaceHolder">
    <link rel="stylesheet" type="text/css" href="../../Public/Css/Admin.Main.debug.css" />
    <link href="../../Public/Css/WindowStickerDefault.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="BodyContentPlaceHolder">
    <div class="pageTitle">
        <h1>Select Template</h1>
    </div>
    <asp:Repeater ID="BuyersGuideRepeater" runat="server" OnItemDataBound="BuyersGuideRepeater_ItemDataBound">
        <HeaderTemplate>
            <h2>Buyers Guide</h2>
            <ul>
        </HeaderTemplate>
        <ItemTemplate>
            <li>
                <asp:HyperLink ID="BuyersGuideHyperLink" runat="server" />
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul>
            <hr />
        </FooterTemplate>
    </asp:Repeater>
    <asp:Repeater ID="WindowStickerRepeater" runat="server" OnItemDataBound="WindowStickerRepeater_ItemDataBound">
        <HeaderTemplate>
            <h2>Window Sticker</h2>
            <ul>
        </HeaderTemplate>
        <ItemTemplate>
            <li>
                <asp:HyperLink ID="WindowStickerTemplateHyperLink" runat="server" />
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul>
            <hr />
        </FooterTemplate>
    </asp:Repeater>
    <asp:HyperLink ID="NewWindowStickerHyperLink" runat="server" NavigateUrl="ModifyTemplate.aspx" OnPreRender="NewWindowStickerHyperLink_PreRender">
          New Window Sticker
    </asp:HyperLink>
    
    <script src="../../Public/Scripts/jquery-1.4.2.min.js" type="text/javascript"></script>  
    <script type="text/javascript">
        
        var TemplateManager = function() {};
        
        TemplateManager.prototype = {
            
            onReady: function() {
                
                $("#Container").css("visibility", "visible");
                
            }
            
        };
        
        var Page = new TemplateManager();
        $(document).ready(function() {
            
            Page.onReady.call( Page );
            
        });
        
    </script>
</asp:Content>
