﻿<%@ Page Language="C#" MasterPageFile="~/Pages/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="VehicleTemplatingWebApplication.Pages.Admin.Home" Title="Home | Template Manager" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">

    <input type="hidden" id="BusinessUnitIdsHidden" runat="server" class="bids" />
    <input type="hidden" id="BulkActionsStatusHidden" runat="server" class="status" />
    
    <asp:Panel ID="BulkActionStatusPanel" runat="server" Visible="false">
    
        <asp:Repeater ID="BulkActionStatusRepeater" runat="server">
            <HeaderTemplate>
                <ul id="BulkActionStatusList">
            </HeaderTemplate>
            <FooterTemplate>
                </ul>
            </FooterTemplate>
            <ItemTemplate>
                <li class='<%# Eval("StatusMessage").ToString().ToLower() %>'><strong><%# Eval("Buid")%>:</strong> <%# Eval("StatusMessage")%></li>
            </ItemTemplate>
        </asp:Repeater>
    
    </asp:Panel>
    
    <a href="Help.html" id="HelpLink" class="newWindow" name="Help" options="width=500,height=700,location=no,scrollbars=yes,toolbar=no">Help</a>

    <div id="Tabset" class="clearfix">
        <ul>
            <li><a href="#">Templates by Dealer</a></li>
            <li><a href="#">Shared Templates</a></li>
            <li><a href="#">Orphaned Templates</a></li>
        </ul>
    </div>
    
    <div id="TabContent" class="clearfix">
        
        <div id="TabContent0" class="clearfix">
        
            <h3 class="clearfix">
                <span class="floatLeft">
                    Templates for
                    <asp:Literal ID="DealerNameLiteral" runat="server">this dealer</asp:Literal>
                </span>
                <asp:HyperLink ID="NewWindowStickerHyperLink" runat="server" NavigateUrl="ModifyTemplate.aspx" OnPreRender="NewWindowStickerHyperLink_PreRender">Create New Template &gt;</asp:HyperLink>
            </h3>
            
            <asp:Repeater ID="BuyersGuideRepeater" runat="server" OnItemDataBound="BuyersGuideRepeater_ItemDataBound">
                <HeaderTemplate>
                    <h4 class="caption">Buyers' Guides</h4>
                    <table id="DealerBuyersGuidesTable" class="dataTable">
                        <thead>
                        <tr>
                            <th style="width:45px">Shared?</th>
                            <th>Template Name</th>
                            <th style="width:400px">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                </HeaderTemplate>                
                <ItemTemplate>
                    <tr>
                        <td style="text-align:center"><asp:Image ID="SharedBuyersGuideCheckmark" runat="server" ImageUrl="~/Public/Images/checkmark.gif" Visible="false" /></td>
                        <td><asp:HyperLink ID="BuyersGuideHyperLink" runat="server" CssClass="name" /></td>
                        <td>
                            <ul class="actions">
                                <li><asp:LinkButton ID="ShareBuyersGuideLink" runat="server" CssClass="share" OnClick="ShareTemplate_Click" title="Associates this template with the specified business units (Master Template)" Text="Share with ..." /></li>
                                <li><asp:LinkButton ID="CopyBuyersGuideLink" runat="server" CssClass="copy" OnClick="CopyTemplate_Click" title="Makes a deep copy of this template to the specified business units (Copy Template)" Text="Copy to ..." /></li>
                                <li><asp:LinkButton ID="RemoveBuyersGuideLink" runat="server" CssClass="remove" OnClick="RemoveTemplate_Click" title="Removes the association between this template and this dealer" Text="Remove from this dealer" /></li>
                            </ul>
                        </td>
                    </tr>
                    
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                    </table>
                    <asp:Label ID="RepeaterEmptyLabel" runat="server" CssClass="empty" Visible="false">
                        There are no buyer's guides associated with this dealer.
                    </asp:Label>
                </FooterTemplate>
            </asp:Repeater>
            
            <asp:Repeater ID="WindowStickerRepeater" runat="server" OnItemDataBound="WindowStickerRepeater_ItemDataBound">
                <HeaderTemplate>
                    <h4 class="caption">Window Stickers</h4>
                    <table id="DealerWindowStickersTable" class="dataTable">
                        <thead>
                        <tr>
                            <th>Shared?</th>
                            <th>Template Name</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td style="text-align:center; width:45px"><asp:Image ID="SharedWindowStickerCheckmark" runat="server" ImageUrl="~/Public/Images/checkmark.gif" Visible="false" /></td>
                        <td><asp:HyperLink ID="WindowStickerTemplateHyperLink" runat="server" CssClass="name" /></td>
                        <td style="width:400px">
                            <ul class="actions">
                                <li><asp:LinkButton ID="ShareStickerLink" runat="server" CssClass="share" OnClick="ShareTemplate_Click" title="Associates this template with the specified business units (Master Template)" Text="Share with ..." /></li>
                                <li><asp:LinkButton ID="CopyStickerLink" runat="server" CssClass="copy" OnClick="CopyTemplate_Click" title="Makes a deep copy of this template to the specified business units (Copy Template)" Text="Copy to ..." /></li>
                                <li><asp:LinkButton ID="RemoveStickerLink" runat="server" CssClass="remove" OnClick="RemoveTemplate_Click" title="Removes the association between this template and this dealer" Text="Remove from this dealer" /></li>
                            </ul>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                    </table>
                    <asp:Label ID="RepeaterEmptyLabel" runat="server" CssClass="empty" Visible="false">
                        There are no window stickers associated with this dealer.
                    </asp:Label>
                </FooterTemplate>
            </asp:Repeater>
            
        </div>
        
        <div id="TabContent1" class="clearfix">
        
            <h3>Shared Templates</h3>
            
            <asp:Repeater ID="SharedBuyersGuideRepeater" runat="server" OnItemDataBound="BuyersGuideRepeater_ItemDataBound">
                <HeaderTemplate>
                    <table id="SharedBuyersGuidesTable" class="dataTable">
                        <caption>Buyers' Guides</caption>
                        <colgroup>
                            <col style="width:300px" />
                            <col style="width:*" />
                            <col style="width:400px" />
                        </colgroup>
                        <tr>
                            <th>Template Name</th>
                            <th>Dealers</th>
                            <th>Actions</th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><asp:HyperLink ID="BuyersGuideHyperLink" runat="server" CssClass="name" /></td>
                        <td><%# Eval("Dealers") %></td>
                        <td>
                            <ul class="actions">
                                <li><asp:LinkButton ID="ShareBuyersGuideLink" runat="server" CssClass="share" OnClick="ShareTemplate_Click" title="Associates this template with the specified business units (Master Template)" Text="Share with ..." /></li>
                                <li><asp:LinkButton ID="CopyBuyersGuideLink" runat="server" CssClass="copy" OnClick="CopyTemplate_Click" title="Makes a deep copy of this template to the specified business units (Copy Template)" Text="Copy to ..." /></li>
                                <li><asp:LinkButton ID="RemoveBuyersGuideLink" runat="server" CssClass="remove removeAll" OnClick="RemoveTemplateFromAll_Click" title="Removes the association between this template and ALL dealers" Text="Remove from all dealers" /></li>
                            </ul>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                    <asp:Label ID="RepeaterEmptyLabel" runat="server" CssClass="empty" Visible="false">
                        There are no shared buyer's guides to display.
                    </asp:Label>
                </FooterTemplate>
            </asp:Repeater>
            
            <asp:Repeater ID="SharedWindowStickerRepeater" runat="server" OnItemDataBound="WindowStickerRepeater_ItemDataBound">
                <HeaderTemplate>
                    <table id="SharedWindowStickersTable" class="dataTable">
                        <caption>Window Stickers</caption>
                        <colgroup>
                            <col style="width:300px" />
                            <col style="width:*" />
                            <col style="width:400px" />
                        </colgroup>
                        <tr>
                            <th>Template Name</th>
                            <th>Dealers</th>
                            <th>Actions</th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><asp:HyperLink ID="WindowStickerTemplateHyperLink" runat="server" CssClass="name" /></td>
                        <td><%# Eval("Dealers") %></td>
                        <td>
                            <ul class="actions">
                                <li><asp:LinkButton ID="ShareStickerLink" runat="server" CssClass="share" OnClick="ShareTemplate_Click" title="Associates this template with the specified business units (Master Template)" Text="Share with ..." /></li>
                                <li><asp:LinkButton ID="CopyStickerLink" runat="server" CssClass="copy" OnClick="CopyTemplate_Click" title="Makes a deep copy of this template to the specified business units (Copy Template)" Text="Copy to ..." /></li>
                                <li><asp:LinkButton ID="RemoveStickerLink" runat="server" CssClass="remove removeAll" OnClick="RemoveTemplateFromAll_Click" title="Removes the association between this template and ALL dealers" Text="Remove from all dealers" /></li>
                            </ul>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                    <asp:Label ID="RepeaterEmptyLabel" runat="server" CssClass="empty" Visible="false">
                        There are no shared window stickers to display.
                    </asp:Label>
                </FooterTemplate>
            </asp:Repeater>
            
        </div>
        
        <div id="TabContent2" class="clearfix">
        
            <h3>Orphaned Templates</h3>
            
            <asp:Repeater ID="OrphanedBuyersGuideRepeater" runat="server" OnItemDataBound="BuyersGuideRepeater_ItemDataBound">
                <HeaderTemplate>
                    <table id="OrphanedBuyersGuidesTable" class="dataTable">
                        <caption>Buyers' Guides</caption>
                        <colgroup>
                            <col style="width:*" />
                            <col style="width:400px" />
                        </colgroup>
                        <tr>
                            <th>Template Name</th>
                            <th>Actions</th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><asp:HyperLink ID="BuyersGuideHyperLink" runat="server" CssClass="name" /></td>
                        <td>
                            <ul class="actions">
                                <li><asp:LinkButton ID="ShareBuyersGuideLink" runat="server" CssClass="share" OnClick="ShareTemplate_Click" title="Associates this template with the specified business units (Master Template)" Text="Share with ..." /></li>
                                <li><asp:LinkButton ID="CopyBuyersGuideLink" runat="server" CssClass="copy" OnClick="CopyTemplate_Click" title="Makes a deep copy of this template to the specified business units (Copy Template)" Text="Copy to ..." /></li>
                                <li><asp:LinkButton ID="DeleteBuyersGuideLink" runat="server" CssClass="delete" OnClick="DeleteTemplate_Click" title="Permanently delete this template" Text="Delete" /></li>
                            </ul>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                    <asp:Label ID="RepeaterEmptyLabel" runat="server" CssClass="empty" Visible="false">
                        There are no orphaned buyer's guides to display.
                    </asp:Label>
                </FooterTemplate>
            </asp:Repeater>
            
            <asp:Repeater ID="OrphanedWindowStickerRepeater" runat="server" OnItemDataBound="WindowStickerRepeater_ItemDataBound">
                <HeaderTemplate>
                    <table id="OrphanedWindowStickersTable" class="dataTable">
                        <caption>Window Stickers</caption>
                        <colgroup>
                            <col style="width:*" />
                            <col style="width:400px" />
                        </colgroup>
                        <tr>
                            <th>Template Name</th>
                            <th>Actions</th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><asp:HyperLink ID="WindowStickerTemplateHyperLink" runat="server" CssClass="name" /></td>
                        <td>
                            <ul class="actions">
                                <li><asp:LinkButton ID="ShareStickerLink" runat="server" CssClass="share" OnClick="ShareTemplate_Click" title="Associates this template with the specified business units (Master Template)" Text="Share with ..." /></li>
                                <li><asp:LinkButton ID="CopyStickerLink" runat="server" CssClass="copy" OnClick="CopyTemplate_Click" title="Makes a deep copy of this template to the specified business units (Copy Template)" Text="Copy to ..." /></li>
                                <li><asp:LinkButton ID="DeleteStickerLink" runat="server" CssClass="delete" OnClick="DeleteTemplate_Click" title="Permanently delete this template" Text="Delete" /></li>
                            </ul>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                    <asp:Label ID="RepeaterEmptyLabel" runat="server" CssClass="empty" Visible="false">
                        There are no orphaned window stickers to display.
                    </asp:Label>
                </FooterTemplate>
            </asp:Repeater>
        
        </div>
        
    </div>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../Public/Scripts/jquery.cookie.js"></script>
    <script type="text/javascript" src="/merchandising/Public/Scripts/lib/jquery.tablesorter.min.js"></script>
    <script type="text/javascript" src="../../Public/Scripts/Admin.Home.debug.js"></script>

    <!-- Business Unit Entry modals -->
    <div id="ShareTemplateModal" style="display:none;">
        <h1>Share Template</h1>
        <p>This action will add associations between this template and the business units entered into the textbox below. To copy a whole new version of this template, click 'Cancel' and choose 'Copy to ...' from the actions list.</p>
        <div class="fieldLabelPair">
            <label for="BusinessUnitIds1">Business Unit IDs:</label>
            <input type="text" id="BusinessUnitIds1" />
            <p class="example">(Example: 16575, 29156, 20154)</p>
            <p class="error">Please enter one or more business unit IDs. (comma-delimited)</p>
        </div>
    </div>
    
    <div id="CopyTemplateModal" style="display:none;">
        <h1>Copy Template</h1>
        <p>This action will create a new copy of this template and associate it with the business units entered into the textbox below. To simply associate more dealers with this template, click 'Cancel' and choose 'Share with ...' from the action list.</p>
        <div class="fieldLabelPair">
            <label for="BusinessUnitIds2">Business Unit IDs:</label>
            <input type="text" id="BusinessUnitIds2" />
            <p class="example">(Example: 16575, 29156, 20154)</p>
            <p class="error">Please enter one or more business unit IDs. (comma-delimited)</p>
        </div>
    </div>

</asp:Content>
