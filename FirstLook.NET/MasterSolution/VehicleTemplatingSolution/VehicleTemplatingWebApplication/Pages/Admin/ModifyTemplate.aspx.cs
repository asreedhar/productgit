﻿using System;
using System.IO;
using System.Web.UI;

using FirstLook.DomainModel.Oltp;

namespace VehicleTemplatingWebApplication.Pages.Admin
{
    public partial class ModifyTemplate : Page
    {
        const string BackgroundHttpPath = "/resources/templating/BackgroundImageHandler.ashx";

        protected void ImageUploadButton_Click(object sender, EventArgs e)
        {
            string docRoot = TemplateManagerUtilities.GetDocumentRoot();

            // Get business unit ID from owner handle
            string buid = Owner.GetOwner(Request["ownerHandle"]).OwnerEntityId.ToString();

            // Check for business unit folder in merchandising document root
            string buDirectory = string.Concat(docRoot, buid);
            if (!Directory.Exists(buDirectory)) { Directory.CreateDirectory(buDirectory); }

            // Create filename
            string fileExtension = TemplateManagerUtilities.GetFileExtension(background_image);
            string fileName = string.Concat(DateTime.UtcNow.ToFileTimeUtc().ToString(), ".", fileExtension);

            UploadMessagingLabel.Visible = false;

            if (string.IsNullOrEmpty(fileExtension))
            {
                UploadMessagingLabel.Text = TemplateManagerUtilities.UiMessages["unsupportedImageType"];
                UploadMessagingLabel.Visible = true;
                return;
            }

            // Save uploaded file to server
            try
            {
                // file-system: save image
                string path = string.Concat(buDirectory, @"\", fileName);
                background_image.PostedFile.SaveAs(path);

                // http access for templating admin and save value to DB
                string query = string.Concat("?bu=" + buid, "&img=", fileName);
                BackgroundImageFilename.Value = query;
                BackgroundImage.ImageUrl = string.Concat(BackgroundHttpPath, query);

                //BackgroundImageFilename.Value = string.Concat("/stickers/" + buid, @"/", fileName);
                //BackgroundImage.ImageUrl = string.Concat("/stickers/" + buid, @"/", fileName);

            }
            catch
            {
                throw new Exception("Error saving file to file system.");
            }

        }

    }
}
