﻿<%@ Page Title="Modify Template | Template Manager" Language="C#" AutoEventWireup="true" CodeBehind="ModifyTemplate.aspx.cs" Inherits="VehicleTemplatingWebApplication.Pages.Admin.ModifyTemplate" MasterPageFile="~/Pages/Admin/Admin.Master" %>

<asp:Content ID="HeadContent" runat="server" ContentPlaceHolderID="HeadContentPlaceHolder">
    <link href="../../Public/Css/WindowStickerDefault.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #Content { position:static !important; } /* Required for bugfix (Case 15423) */
        .ui-dialog { height: 120px !important; }
    </style>
</asp:Content>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="BodyContentPlaceHolder">
    <div class="globalmessage error" id="sharedwarning" style="display: none;">
	    <p class="error">This is a shared template. Any changes you make will affect multiple dealerships.</p>
	</div>

	<div id="dialog-confirm">
	    <h1>Save Shared Template</h1>
	    <p>You are about to save changes that will affect multiple dealerships. Are you sure you want to proceed?</p>
	</div>

    <div class="pageTitle">
	    <h1>Modify Template</h1>
    </div>
    
    <div id="main" class="clearfix">		
    	
	<div id="page_preview" class="arial">
	    <p id="sample_text"></p>
		<p id="sample_em">mmmmmmmmmm</p>
		<p id="sample_en">nnnnnnnnnn</p>
		<p id="sample_height">A<br />B<br />C<br />D<br />E<br />F<br />G<br />H<br />I<br />J<br />K<br />L<br />M<br />N<br />O<br />P<br />Q<br />R<br />S<br />T<br />U<br />V<br />W<br />X<br />Y<br />Z</p>
		<ul id="sample_bullet"><li></li></ul>
		<ul id="sample_list"></ul>
		<asp:Image id="BackgroundImage" runat="server" Width="8.5in" Height="11in" CssClass="backgroundImage" />
		<input type="hidden" id="BackgroundImageFilename" runat="server" class="backgroundImageFilename" />
		
	</div>
	
	<fieldset id="template_setup" class="vform">
	    <legend>Template Setup</legend>
	    <p>
	        <label for="template_name">Template Name:</label>
	        <input type="text" name="template_name" id="template_name" style="width:250px;" />
	    </p>
	    <p class="checkbox">
	        <label for="is_buyers_guide">Is Buyers Guide</label>
	        <input type="checkbox" name="is_buyers_guide" id="is_buyers_guide" />
	    </p>
	</fieldset>
	
	<fieldset id="page_setup" class="vform">
		<legend>Page Setup</legend>
		<p>
			<label for="page_width">Page Width:</label>
			<input type="text" name="page_width" value="8.5in" id="page_width" style="width:50px;" />
		</p>
		<p>
			<label for="page_height">Page Height:</label>
			<input type="text" name="page_height" value="11in" id="page_height" style="width:50px;" />
		</p>
		<p>
			<label for="background_image">Background Image (BMP/GIF/JPG)</label>
			<input type="file" name="background_image" id="background_image" runat="server" />
			<asp:Button ID="ImageUploadButton" runat="server" Text="Upload" OnClick="ImageUploadButton_Click" Enabled="false" />
			<asp:Label ID="UploadMessagingLabel" runat="server" Visible="false" ForeColor="#cc3333"></asp:Label>
		</p>
	</fieldset>
	
	<fieldset id="grid_setup" class="vform">
		<legend>Grid Setup</legend>
		<p>
			<label for="grid_x:">Grid X:</label>
			<input type="text" name="grid_x" value="0.25in" id="grid_x" style="width:50px;" />
		</p>
		<p>
			<label for="grid_y:">Grid Y:</label>
			<input type="text" name="grid_Y" value="0.25in" id="grid_y" style="width:50px;" />
		</p>
	</fieldset>
	
	<fieldset class="controls">
		<button id="generate_preview" name="generate_preview">Generate Preview</button>
		<button style="display:none" id="sample_pdf" name="sample_pdf">Download Sample PDF</button>
	</fieldset>
	
	
	<div id="merge_data" class="vform">
		<div id="content-content-area">
			<p>
				<button id="remove_data_point">Remove Print Area</button>
			</p>
			<p>
				<label for="data_points">Data Point</label>
				<select name="Data Points" id="data_points">
					<option>Select an Option</option>
					<option value="static_text">enter static text</option>
				</select>
			</p>
			<p class="static_text data_point____all data_point__DEFAULT">
				<label for="static_text">Static Text</label>
				<textarea id="static_text" name="static_text" rows="5" style="width:250px;"></textarea>
			</p>
			<span class="hidden_de_scoped_features" style="display:none">
	            <p class="url_source data_point____all data_point__QRCodeURL" style="display:none">
	            	<label>URL Source</label>
	        		<label><input type="radio" name="url_source" value="static"> Custom Static URL</label>
	        		<label><input type="radio" name="url_source" value="standard_dynamic" checked="checked"> Website PDF URL</label>
	            </p>
	            <p class="static_url data_point____all data_point__QRCodeURL" style="display:none">
	            	<label for="static_url">Static URL</label>
	            	<input type="text" id="static_url" name="static_url" style="width:250px;">
	            </p>
	        </span>
			<p class="qr_code_error_correction_level data_point____all data_point__QRCodeURL" style="display:none">
				<label for="qr_code_error_correction_level">Error Correction Level</label>
                <select name="qr_code_error_correction_level" id="qr_code_error_correction_level">
                    <option value="L">Level L (up to 7% damage) (Low)</option>
                    <option value="M">Level M (up to 15% damage) (Medium)</option>
                    <option value="Q">Level Q (up to 25% damage) (Quartile)</option>
                    <option value="H">Level H (up to 30% damage) (High)</option>
                </select>
			</p>
			<p class="prompt data_point____all data_point__DEFAULT">
				<label for="prompt">Prompt</label>
                <select name="prompt" id="prompt">
                    <option value=""></option>
                    <option value="No Warranty">No Warranty</option>
                    <option value="Limited Warranty">Limited Warranty</option>
                    <option value="Full Warranty">Full Warranty</option>
                    <option value="Service Contract">Service Contract</option>
                    <option value="KBB Book Value">KBB Book Value</option>
                </select>
			</p>
			<p class="data_point____all data_point__DEFAULT">
				<label for="align">Align</label>
				<select name="align" id="align">
					<option value="left">left</option>
					<option value="right">right</option>
					<option value="center">center</option>
				</select>
			</p>
			<div class="hr"><hr /></div>
            <p class="data_point____all data_point__DEFAULT" style="display: none">
				<label for="label_align">Align</label>
				<select name="label_align" id="label_align">
					<option value="left">left</option>
					<option value="right">right</option>
					<option value="center">center</option>
				</select>
			</p>
			<p class="data_point____all data_point__DEFAULT" style="display: none">
				<label for="label_vertical_align">Vertical Align</label>
				<select name="label_vertical_align" id="label_vertical_align">
					<option value="left">above</option>
					<option value="right">inline</option>
					<option value="center">below</option>
				</select>
			</p>
		</div>
		<div id="font-content-area">
			<p class="data_point____all data_point__DEFAULT">
				<label for="line_hieght">Line Height</label>
				<input type="text" name="line_height" value="1.2" id="line_height" style="width:50px;" />
			</p>
			<p class="data_point____all data_point__DEFAULT">
				<label for="font_selection">Font</label>
				<select name="font_selection" id="font_selection">
					<option value="Arial">Arial</option>
					<option value="Arial Black">Arial Black</option>
					<option value="Comic Sans MS">Comic Sans MS</option>
					<option value="Courier New">Courier New</option>
					<option value="Georgia">Georgia</option>
					<option value="Impact">Impact</option>
					<option value="Lucida Console">Lucida Console</option>
					<option value="Lucida Grande">Lucida Grande</option>
					<option value="Tahoma">Tahoma</option>
					<option value="Time New Roman">Times New Roman</option>
					<option value="Trebuchet">Trebuchet</option>
					<option value="Verdana">Verdana</option>
				</select>
			</p>
			<p class="data_point____all data_point__DEFAULT">
				<label for="font_size">Font Size</label>
				<input type="text" name="font_size" value="12pt" id="font_size" style="width:50px;" />
			</p>
			<p class="checkbox data_point____all data_point__DEFAULT">
				<input type="checkbox" name="is_italic" id="is_italic" />
				<label for="is_italic">italic</label>
			</p>
			<p class="checkbox data_point____all data_point__DEFAULT">
				<input type="checkbox" name="is_bold" id="is_bold" />
				<label for="is_bold">bold</label>
			</p>
			<p class="checkbox data_point____all data_point__DEFAULT">
				<input type="checkbox" name="is_underline" id="is_underline" />
				<label for="is_underline">underline</label>
			</p>
		</div>
		<div id="layout-content-area">
			<p>
				<label for="x">X</label>
				<input type="text" name="x" value="" id="x" style="width:80px;" />
			</p>
			<p>
				<label for="y">Y</label>
				<input type="text" name="y" value="" id="y" style="width:80px;" />
			</p>
			<p class="data_point____all data_point__DEFAULT">
				<label for="width">Width</label>
				<input type="text" name="width" value="" id="width" style="width:80px;" />
			</p>
			<p class="data_point____all data_point__DEFAULT">
				<label for="height">Height</label>
				<input type="text" name="height" value="" id="height" style="width:80px;" />
			</p>
			<p class="data_point____all data_point__QRCodeURL">
				<label for="size">Size (Square)</label>
				<input type="text" name="size" value="" id="size" style="width:80px;" />
			</p>
			<p class="data_point____all data_point__DEFAULT">
				<label for="columns">Columns</label>
				<input type="text" name="columns" value="1" id="columns" style="width:50px;" />
			</p>
		</div>
	</div>
	
	<dl id="print_calculations">
		<dt>Line Height</dt>
		<dd id="print_line_height"></dd>
		<dt>Characters</dt>
		<dd id="print_character_avg"></dd>
		<dt>Em Size</dt>
		<dd id="print_em_size"></dd>
		<dt>En Size</dt>
		<dd id="print_en_size"></dd>
		<dt>Bullet Size</dt>
		<dd id="print_bullet_size"></dd>
	</dl>
	
	<fieldset class="controls">
		<legend>Actions</legend>
		<p>
			<button id="save_button">Save</button>
		</p>
		
	</fieldset>
	
    </div>
    
    <div id="footer">
	    <p>Current Position: <span id="current_position"></span></p>
    </div>
    
    <script src="../../Public/Scripts/jquery-1.4.2.min.js" type="text/javascript"></script>   
    <script src="../../Public/Scripts/jquery-ui-1.8.17.min.js" type="text/javascript"></script>
    <script type="text/javascript">!window.JSON && document.write('<script src="../../Public/Scripts/json2.js"><\/script>')</script>
    <script src="../../Public/Scripts/plugins.js" type="text/javascript"></script>
    <script src="../../Public/Scripts/PrintValues.js" type="text/javascript"></script>
    <script src="../../Public/Scripts/Application.js" type="text/javascript"></script>    
    <script type="text/javascript">
        
        var TemplateManager = function() {};
        
        TemplateManager.prototype = {
            
            onReady: function() {
                
                // Enable 'Upload' button when file selected
                $("#page_setup input[type=file]").bind("change", function() { $(this).next("input").removeAttr("disabled"); });
                $("#Container").css("visibility", "visible");
                
            }
            
        };
        
        var Page = new TemplateManager();
        $(document).ready(function() {
            
            Page.onReady.call( Page );
            
        });
        
    </script>
</asp:Content>
