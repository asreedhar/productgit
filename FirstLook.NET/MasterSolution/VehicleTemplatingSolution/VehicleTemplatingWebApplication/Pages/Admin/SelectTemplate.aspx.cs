﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using VehicleTemplatingWebServiceClient.VehicleTemplatingService;

namespace VehicleTemplatingWebApplication.Pages.Admin
{
    public partial class CreateTemplate : Page
    {
        private VehicleTemplatingService _templatingService;
        private VehicleTemplatingService TemplatingService
        {
            get
            {
                if (_templatingService == null)
                {
                    _templatingService = new VehicleTemplatingService();
                }
                return _templatingService;
            }
        } 

        private string OwnerHandle
        {
            get
            {
                return Request.Params["OwnerHandle"];
            }
        }

        private IList<TemplateInfo> _windowStickers = null;
        private IList<TemplateInfo> WindowStickers
        {
            get
            {
                if (_windowStickers == null)
                {
                    _windowStickers = TemplatingService.GetWindowStickerTemplateInfoForOwner(OwnerHandle);
                }
                return _windowStickers;
            }
        }

        private IList<TemplateInfo> _buyersGuide = null;
        private IList<TemplateInfo> Buyersguide
        {
            get
            {
                if (_buyersGuide == null)
                {
                    _buyersGuide = TemplatingService.GetBuyersGuideTemplateInfoForOwner(OwnerHandle);
                }
                return _buyersGuide;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            BuyersGuideRepeater.Visible = Buyersguide.Count >= 0;
            WindowStickerRepeater.Visible = WindowStickers.Count >= 0;

            BuyersGuideRepeater.DataSource = Buyersguide;
            WindowStickerRepeater.DataSource = WindowStickers;

            Page.PreRenderComplete += Page_PreRenderComplete;
        }

        private void Page_PreRenderComplete(object sender, EventArgs e)
        {
            if (BuyersGuideRepeater.Visible)
            {
                BuyersGuideRepeater.DataBind();
            }
            if (WindowStickerRepeater.Visible)
            {
                WindowStickerRepeater.DataBind();
            }
        }

        private string UriForTemplate(TemplateInfo templateInfo)
        {
            return "~/Pages/Admin/ModifyTemplate.aspx?template=" + templateInfo.Id + "&ownerHandle=" + OwnerHandle;
        }

        protected void WindowStickerRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            HyperLink hyperLink = e.Item.FindControl("WindowStickerTemplateHyperLink") as HyperLink;
            TemplateInfo templateInfo = e.Item.DataItem as TemplateInfo;
            PopulateHyperLink(templateInfo, hyperLink);
        }

        private void PopulateHyperLink(TemplateInfo templateInfo, HyperLink hyperLink)
        {
            if (hyperLink != null && templateInfo != null)
            {
                hyperLink.NavigateUrl = UriForTemplate(templateInfo);
                hyperLink.Text = templateInfo.Name;
            }
        }

        protected void BuyersGuideRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            HyperLink hyperLink = e.Item.FindControl("BuyersGuideHyperLink") as HyperLink;
            TemplateInfo templateInfo = e.Item.DataItem as TemplateInfo;
            PopulateHyperLink(templateInfo, hyperLink);
        }

        protected void NewWindowStickerHyperLink_PreRender(object sender, EventArgs e)
        {
            NewWindowStickerHyperLink.NavigateUrl += "?template=-1&ownerHandle=" + OwnerHandle;
        }
    }
}
