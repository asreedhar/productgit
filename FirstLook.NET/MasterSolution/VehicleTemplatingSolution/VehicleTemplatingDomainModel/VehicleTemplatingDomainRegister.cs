﻿
using Autofac;
using FirstLook.Common.Core.IOC;

namespace VehicleTemplatingDomainModel
{
    public class VehicleTemplatingDomainRegister : IRegistryModule
    {
        public void Register(ContainerBuilder builder)
        {
            builder.RegisterType<VehicleTemplatingDataAccess>().As<IVehicleTemplatingDataAccess>();
        }
    }
}
