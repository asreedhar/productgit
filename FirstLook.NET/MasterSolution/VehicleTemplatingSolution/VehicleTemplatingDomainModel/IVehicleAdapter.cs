namespace VehicleTemplatingDomainModel
{
    public interface IVehicleAdapter {

        VehicleData Adapt(string ownerHandle, string vehicleHandle);
    }
}