
namespace VehicleTemplatingDomainModel
{
    public interface IDataPoint {
        DynamicDataPointKeys ?  Key { get; }
        string Text{get;}
        string Prompt{get;}
        bool Selected { get; set; }
    }
}