using System;

namespace VehicleTemplatingDomainModel
{
    [Serializable]
    public class DataPoint : IDataPoint
    {
        private DynamicDataPointKeys? _key;
        private string _text;
        private string _prompt;
        private bool _selected;

        public DataPoint()
        {

        }

        public DataPoint(DynamicDataPointKeys key, string text, string prompt, bool selected)
        {
            _key = key;
            _text = text;
            _prompt = prompt;
            _selected = selected;
        }

        internal DataPoint(int id, DynamicDataPointKeys key, string text, string prompt, bool selected)
        {
            _key = key;
            _text = text;
            _prompt = prompt;
            _selected = selected;
        }

        public DynamicDataPointKeys? Key
        {
            get { return _key; }
            set { _key = value; }
        }

        public string Text
        {
            get
            {
                if (_key.HasValue && _key != DynamicDataPointKeys.Undefined)
                {
                    return string.Empty;
                }

                return _text;
            }
            set
            {
                _text = value;
            }
        }

        public string Prompt
        {
            get { return _prompt; }
            set { _prompt = value; }
        }

        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; }
        }
    }
}