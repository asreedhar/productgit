using System;
using System.Collections.Generic;

namespace VehicleTemplatingDomainModel
{
    public interface IVehicleData {

        int AgeInDays { get; }
        string Make { get; }
        string Model { get; }
        int ModelYear { get; }
        string Series { get; }
        decimal Price { get; }
        decimal UnitCost { get; }
        int Mileage { get; }
        string StockNumber { get; }
        string Vin { get; }
        string Engine { get; }
        string FuelType { get; }
        string Transmission { get; }
        string Class { get; }
        string Trim { get; }
        string BodyStyle { get; }
        string Description { get; }
        string ExteriorColor { get; }
        string InteriorColor { get; }
        string Drivetrain { get; }
        bool Certified { get; }
        List<string> Equipment { get; }
        List<string> Packages { get; }
        string MpgCity { get; }
        string MpgHwy { get; }
        string CertifiedID { get; }
        decimal KBBBookValue { get; }
        decimal NADABookValue { get; }
        decimal MSRP { get; }
    }
}