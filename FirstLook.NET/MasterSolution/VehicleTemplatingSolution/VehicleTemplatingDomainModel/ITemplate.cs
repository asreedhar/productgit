using System.Collections.Generic;

namespace VehicleTemplatingDomainModel
{
    public interface ITemplate
    {
        int TemplateId { get; set; }
        string Name { get; set; }
        decimal Height { get; set; }
        decimal Width { get; set; }
        string Unit { get; set; }
        TemplateTypes Type { get; set; }
        List<ContentArea> ContentAreas { get; set; }
        string BackgroundImage { get; set; }
        string FileName { get; set; }
        bool IsExternal { get; }

        byte[] ProcessExternalPdf(string ownerHandle, IVehicleData vehicle);
    }
}