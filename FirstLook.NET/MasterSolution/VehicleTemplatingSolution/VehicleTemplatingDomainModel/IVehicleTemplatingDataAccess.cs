using System.Collections.Generic;

namespace VehicleTemplatingDomainModel
{
    public interface IVehicleTemplatingDataAccess
    {
        List<TemplateInfo> GetTemplateInfoForOwner(string ownerHandle, TemplateTypes type);
        List<TemplateInfo> GetSharedTemplateInfo(TemplateTypes type);
        List<TemplateInfo> GetOrphanedTemplateInfo(TemplateTypes type);
        TemplateTO GetTemplate(int templateId);
        bool SaveTemplate(string ownerHandle, Template template, string user);
        bool SaveSystemTemplate(Template template, string user);
        bool DeleteTemplate(Template template);
        bool ShareTemplate(Template template, int oid);
        bool RemoveTemplate(Template template, int? oid);
    }
}