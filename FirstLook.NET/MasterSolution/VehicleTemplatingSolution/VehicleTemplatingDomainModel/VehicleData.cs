using System;
using System.Collections.Generic;

namespace VehicleTemplatingDomainModel
{
    [Serializable]
    public class VehicleData : IVehicleData
    {
        public int AgeInDays { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public int ModelYear { get; set; }
        public string Series { get; set; }
        public decimal Price { get; set; }
        public decimal UnitCost { get; set; }
        public int Mileage { get; set; }
        public string StockNumber { get; set; }
        public string Vin { get; set; }
        public string Engine { get; set; }
        public string FuelType { get; set; }
        public string Transmission { get; set; }
        public string Class { get; set; }
        public string Trim { get; set; }
        public string BodyStyle { get; set; }
        public string Description { get; set; }
        public string ExteriorColor { get; set; }
        public string InteriorColor { get; set; }
        public string Drivetrain { get; set; }
        public bool Certified { get; set; }
        public List<string> Equipment { get; set; }
        public List<string> Packages { get; set; }
        public string MpgCity { get; set; }
        public string MpgHwy { get; set; }
        public string CertifiedID { get; set; }
        public decimal KBBBookValue{ get; set; }
        public decimal NADABookValue { get; set; }
        public decimal MSRP { get; set; }
    }
}