﻿using System;
using System.Net;
using System.IO;

namespace VehicleTemplatingDomainModel
{
    public interface IURLShortener
    {
        string Shorten( string longURL );
        string ProviderPrefix();
    }

    public class URLShortener : IURLShortener
    {
        public URLShortener()
        {
        }

        public string Shorten( string longURL )
        {
            var url_template = "http://tinyurl.com/api-create.php?url={0}";
            var request = WebRequest.Create( String.Format( url_template, longURL ));
            
            var response = request.GetResponse();
            string text;
            using( var reader = new StreamReader( response.GetResponseStream() ))
            {
                text = reader.ReadToEnd();
            }
            return text;
        }

        public string ProviderPrefix()
        {
            return "http://tinyurl.com/";
        }
    }
}
