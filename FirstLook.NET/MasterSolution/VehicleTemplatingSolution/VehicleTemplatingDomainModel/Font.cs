using System;

namespace VehicleTemplatingDomainModel
{
    [Serializable]
    public class Font
    {
        public Font()
        {
        }

        public Font(FontFamily family, FontAlign align, decimal lineHeight, bool italics, bool underline, bool bold, decimal size)
        {
            Family = family;
            Align = align;
            LineHeight = lineHeight;
            Italics = italics;
            Underline = underline;
            Bold = bold;
            Size = size;
        }
        
        public FontFamily Family{get;set;}
        public FontAlign Align { get;  set; }
        public decimal LineHeight { get;  set; }
        public bool Italics { get;  set; }
        public bool Underline { get;  set; }
        public bool Bold { get;  set; }
        public decimal Size { get; set; }
    }
}