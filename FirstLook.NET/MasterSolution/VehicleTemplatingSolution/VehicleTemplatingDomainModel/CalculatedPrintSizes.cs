using System;

namespace VehicleTemplatingDomainModel
{
    [Serializable]
    public class CalculatedPrintSizes
    {
        public CalculatedPrintSizes()
        {
        }

        public CalculatedPrintSizes(decimal lineHeight, int characterAverage,decimal emSize, decimal enSize, decimal bulletSize)
        {
            LineHeight = lineHeight;
            CharacterAverage = characterAverage;
            EmSize = emSize;
            EnSize = enSize;
            BulletSize = bulletSize;
        }

        public decimal LineHeight { get;  set; }
        public int CharacterAverage { get;  set; }
        public decimal EmSize { get;  set; }
        public decimal EnSize { get;  set; }
        public decimal BulletSize { get;  set; }

    }
}