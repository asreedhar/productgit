using System;

namespace VehicleTemplatingDomainModel
{
    [Serializable]
    public enum TemplateTypes 
    {
        /*
         * These values are persisted in the WindowSticker.DataPointKey table.
         * Do not change these values without changing the same rows in the table.
         */

        WindowSticker = 1,
        BuyersGuide = 2
    }
}