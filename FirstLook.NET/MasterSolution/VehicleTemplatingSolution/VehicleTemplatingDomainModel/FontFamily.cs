
using System;

namespace VehicleTemplatingDomainModel
{
    [Serializable]
    public enum FontFamily
    {
        /*
         * These values are persisted in the WindowSticker.FontFamily table.
         * Do not change these values without changing the same rows in the table.
         */

        // ReSharper disable InconsistentNaming
        Arial = 1,
        Arial_Black = 2,
        Comic_Sans_MS = 3,
        Courier_New = 4,
        Georgia = 5,
        Impact = 6,
        Lucida_Console = 7,
        Lucida_Grande = 8,
        Tahoma = 9,
        Time_New_Roman = 10,
        Trebuchet = 11,
        Verdana = 12
        // ReSharper restore InconsistentNaming
    }
}