namespace VehicleTemplatingDomainModel
{
    public enum DynamicDataPointKeys {
        
        Undefined = 0,
        Year = 1,
        Make =2,
        Model = 3,
        Trim = 4,
        /// <summary>
        /// (Year, Make, Model, Trim, & Body Style)
        /// </summary>
        VehicleDescription = 5,
        ExteriorColor = 6,
        InteriorColor = 7,
        Mileage = 8,
        StockNumber = 9,
        Transmission = 10,
        Engine = 11,
        Drivetrain = 12,
        Certified = 13,
        BodyStyle = 14,
        Vin = 15,
        Series = 16,
        Price = 17,
        Equipment = 18,
        AgeInDays = 19,
        MpgCity = 20,
        MpgHwy = 21,
        CertifiedID = 22,
        KBBBookValue = 23,
        Packages = 24,
        NADABookValue = 25,
        QRCodeURL = 26,
        MaxShowroomDirectURL = 27,
        MaxShowroomSMSCode = 28,
        MaxShowroomSMSPhoneNumber = 29,
        MSRP = 30,
    }
}