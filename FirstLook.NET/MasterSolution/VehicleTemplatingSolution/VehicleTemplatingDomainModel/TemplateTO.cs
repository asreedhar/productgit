using System;
using System.Collections.Generic;

namespace VehicleTemplatingDomainModel
{
    [Serializable]
    public class TemplateTO : ITemplate
    {
        public TemplateTO()
        {
            // this is here for the Json serializer (Newtonsoft.Json)
        }

        public TemplateTO(int templateId, string name, decimal height, decimal width, string units, TemplateTypes type, string backgroundImage, List<ContentArea> contentAreas, String fileName = "", String path = "")
        {
            TemplateId = templateId;
            Name = name;
            Height = height;
            Width = width;
            Unit = units;
            Type = type;
            BackgroundImage = backgroundImage;
            ContentAreas = contentAreas;
            FileName = fileName;
        }

        public int TemplateId { get; set; }
        public decimal Height { get; set; }
        public decimal Width { get; set; }
        public string Unit { get; set; }
        public string Name { get; set; }
        public string BackgroundImage { get; set; }
        public TemplateTypes Type { get; set; }
        public List<ContentArea> ContentAreas { get; set; }
        public string FileName { get; set; }
        public bool IsExternal { get { return (Template.CreateTemplate(this)).IsExternal; } }

        public byte[] ProcessExternalPdf(string ownerHandle, IVehicleData vehicle)
        {
            return (Template.CreateTemplate(this)).ProcessExternalPdf(ownerHandle, vehicle);
        }
    }
}