﻿
using System;

namespace VehicleTemplatingDomainModel
{
    [Serializable]
    public class TemplateInfo
    {
        public TemplateInfo() { }

        public TemplateInfo(int id, string name) : this(id, name, null, string.Empty) { }
        public TemplateInfo(int id, string name, bool? shared) : this(id, name, shared, string.Empty) { }
        public TemplateInfo(int id, string name, string dealers) : this(id, name, null, dealers) { }

        public TemplateInfo(int id, string name, bool? shared, string dealers)
        {
            Id = id;
            Name = name;
            Shared = shared;
            Dealers = dealers;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool? Shared { get; set; }
        public string Dealers { get; set; }
    }
}
