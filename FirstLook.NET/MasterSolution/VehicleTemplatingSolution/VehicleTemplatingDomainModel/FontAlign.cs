
using System;

namespace VehicleTemplatingDomainModel
{
    [Serializable]
    public enum FontAlign
    {
        /*
         * These values are persisted in the WindowSticker.FontAlign table.
         * Do not change these values without changing the same rows in the table.
         */
        center = 1,
        left = 2,
        right = 3

    }
}