
using System;

namespace VehicleTemplatingDomainModel
{
    [Serializable]
    public class ContentArea
    {
        public ContentArea()
        {

        }

        internal ContentArea(int id, DataPoint dataPoint, Font font, Point topLeft, Point bottomRight, CalculatedPrintSizes printSizes, int columns)
        {
            Id = id;
            DataPoint = dataPoint;
            Font = font;
            TopLeft = topLeft;
            BottomRight = bottomRight;
            PrintSizes = printSizes;
            Columns = columns;
        }


        public ContentArea(DataPoint dataPoint, Font font, Point topLeft, Point bottomRight, CalculatedPrintSizes printSizes, int columns):this(Constants.UnassignedId, dataPoint, font, topLeft, bottomRight, printSizes, columns)
        {
        }

        public int Id { get; set; }
        public DataPoint DataPoint { get; set; }
        public Font Font{ get; set; }
        public Point TopLeft { get; set; }
        public Point BottomRight { get; set; }
        public CalculatedPrintSizes PrintSizes { get; set; }
        public int Columns { get; set; }
        public string ExtraDataJSON { get; set; }

        internal bool HasValidDataPoint
        {
            get { return HasValidDynamicDataPoint || HasValidStaticTextDataPoint; }
        }
        
        internal bool HasValidDynamicDataPoint
        {
            get
            {
                return 
                    DataPoint != null && 
                    DataPoint.Key.HasValue &&
                    !DataPoint.Key.Equals(DynamicDataPointKeys.Undefined);
            }
        }

        internal bool HasValidStaticTextDataPoint
        {
            get
            {
                return DataPoint != null && ! string.IsNullOrEmpty(DataPoint.Text);
            }
        }
    }
}