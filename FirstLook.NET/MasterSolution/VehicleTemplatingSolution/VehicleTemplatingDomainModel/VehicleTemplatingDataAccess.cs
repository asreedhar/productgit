using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using FirstLook.Common.Data;

namespace VehicleTemplatingDomainModel
{
    public class VehicleTemplatingDataAccess : IVehicleTemplatingDataAccess
    {
        private const string WINDOW_STICKER_DATABASE = "IMT";

        private static TemplateTO GetTemplate(IDataRecord record)
        {
            var templateId = record.GetInt32(record.GetOrdinal("TemplateId"));
            var templateName = record.GetString(record.GetOrdinal("TemplateName"));
            var height = record.GetDecimal(record.GetOrdinal("Height"));
            var width = record.GetDecimal(record.GetOrdinal("Width"));
            var units = record["Units"].ToString();
            var templateType = (TemplateTypes) record.GetByte(record.GetOrdinal("TemplateTypeId"));

            string backgroundImage = null;
            if (!record.IsDBNull(record.GetOrdinal("BackgroundImage")))
                backgroundImage = record.GetString(record.GetOrdinal("BackgroundImage"));

            return new TemplateTO( templateId, templateName, height, width, units, templateType, backgroundImage, new List<ContentArea>() );

        }

        private static ContentArea GetContentArea(IDataRecord record)
        {
            int contentAreaId = record.GetInt32(record.GetOrdinal("ContentAreaId"));
            decimal topLeftX = record.GetDecimal(record.GetOrdinal("TopLeftX"));
            decimal topLeftY = record.GetDecimal(record.GetOrdinal("TopLeftY"));
            decimal bottomRightX = record.GetDecimal(record.GetOrdinal("BottomRightX"));
            decimal bottomRightY = record.GetDecimal(record.GetOrdinal("BottomRightY"));
            string fontFamily = record.GetString(record.GetOrdinal("FontFamily"));
            string fontAlign = record.GetString(record.GetOrdinal("FontAlign"));
            decimal lineHeight = record.GetDecimal(record.GetOrdinal("lineHeight"));
            int calculatedCharacterAverage = record.GetInt16(record.GetOrdinal("CalculatedCharacterAverage"));
            decimal calculatedEmSize = record.GetDecimal(record.GetOrdinal("CalculatedEmSize"));
            decimal calculatedEnSize = record.GetDecimal(record.GetOrdinal("CalculatedEnSize"));
            decimal calculatedLineHeight = record.GetDecimal(record.GetOrdinal("CalculatedLineHeight"));
            decimal calculatedBulletSize = record.GetDecimal(record.GetOrdinal("CalculatedBulletSize"));
            bool italics = record.GetBoolean(record.GetOrdinal("Italics"));
            bool underline = record.GetBoolean(record.GetOrdinal("Underline"));
            bool bold = record.GetBoolean(record.GetOrdinal("Bold"));
            int columns = record.GetByte(record.GetOrdinal("Columns"));
            decimal fontSize = record.GetDecimal(record.GetOrdinal("FontSize"));
            // nullable column
            int ordinal_ExtraDataJSON = record.GetOrdinal("ExtraDataJSON");
            string ExtraDataJSON = null;
            if( !record.IsDBNull( ordinal_ExtraDataJSON ))
                ExtraDataJSON = record.GetString( ordinal_ExtraDataJSON );

            // validate the values from the dataaccess
            if (! Enum.IsDefined(typeof(FontFamily), fontFamily.Replace(" ", "_")))
                throw new ApplicationException("Encountered an unsupported value for FontFamily: " + fontFamily);
                                
            if (!Enum.IsDefined(typeof(FontAlign), fontAlign))
                throw new ApplicationException("Encountered an unsupported value for FontAlign: " + fontAlign);

            var family = (FontFamily)Enum.Parse(typeof(FontFamily), fontFamily.Replace(" ", "_"));
            var align = (FontAlign)Enum.Parse(typeof(FontAlign), fontAlign);
                                
            // construct the point objects for this content area
            var topLeft = new Point(topLeftX, topLeftY);
            var bottomRight = new Point(bottomRightX, bottomRightY);

            var font = new Font(family, align, lineHeight, italics, underline, bold, fontSize);

            var printSizes = new CalculatedPrintSizes(calculatedLineHeight, calculatedCharacterAverage, calculatedEmSize,
                                                      calculatedEnSize, calculatedBulletSize);

            var dataPoint = GetDataPoint(record);

            var contentArea = new ContentArea(contentAreaId, dataPoint, font, topLeft, bottomRight, printSizes, columns);
            contentArea.ExtraDataJSON = ExtraDataJSON;
            return contentArea;
        }

        private static DataPoint GetDataPoint(IDataRecord record)
        {
            DataPoint dataPoint = null;

            if (!record.IsDBNull(record.GetOrdinal("DataPointId")))
            {   
                int dataPointId = record.GetInt32(record.GetOrdinal("DataPointId"));

                // only try to get a datapoint if we have a datapoint id
                string dataPointPrompt = null;
                if (!record.IsDBNull(record.GetOrdinal("DataPointPrompt")))
                    dataPointPrompt = record.GetString(record.GetOrdinal("DataPointPrompt"));

                string dataPointText = null;
                if (!record.IsDBNull(record.GetOrdinal("DataPointText")))
                    dataPointText = record.GetString(record.GetOrdinal("DataPointText"));

                string dataPointKey = null;
                if (!record.IsDBNull(record.GetOrdinal("DataPointKey")))
                    dataPointKey = record.GetString(record.GetOrdinal("DataPointKey"));

                if (dataPointKey == null && dataPointText == null)
                    throw new ApplicationException("Invalid datapoint values returned.  Key and Text were both null.");

                if (dataPointKey!= null && dataPointText != null)
                    throw new ApplicationException("Invalid datapoint values returned.  Both Key and Text were non-null.");

                DynamicDataPointKeys key = DynamicDataPointKeys.Undefined;

                if (dataPointKey != null)
                {
                    key = (DynamicDataPointKeys) Enum.Parse(typeof (DynamicDataPointKeys), dataPointKey);
                }

                dataPoint = new DataPoint(dataPointId, key, dataPointText, dataPointPrompt, false);                    
            }

            return dataPoint;
        }

        #region Implemnation of IVehicleTemplatingDataAccess
        public TemplateTO GetTemplate(int templateId)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(WINDOW_STICKER_DATABASE))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandText = "WindowSticker.Template#Fetch";
                    command.CommandType = CommandType.StoredProcedure;
                    command.AddParameterWithValue("TemplateId", DbType.Int32, false, templateId);
                    var reader = command.ExecuteReader();

                    if (reader.Read())
                    {
                        // construct a template
                        var template = GetTemplate(reader);

                        if (reader.NextResult())
                        {
                            while (reader.Read())
                            {
                                // add the contentareas to the template
                                var contentArea = GetContentArea(reader);
                                template.ContentAreas.Add(contentArea);
                            }

                            return template;
                        }
                        
                        throw new ApplicationException(
                            "Second resultset was not found after executing WindowSticker.Template#Fetch.");
                    }
                }
            }

            return null;
        }

        public List<TemplateInfo> GetTemplateInfoForOwner(string ownerHandle, TemplateTypes type)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(WINDOW_STICKER_DATABASE))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandText = "WindowSticker.TemplateInfo#FetchByOwner_Type";
                    command.CommandType = CommandType.StoredProcedure;
                    command.AddParameterWithValue("OwnerHandle", DbType.String, false, ownerHandle);
                    command.AddParameterWithValue("TemplateTypeId", DbType.Int32, false, (int)type);
                    var reader = command.ExecuteReader();

                    var templateInfoList = new List<TemplateInfo>();

                    while (reader.Read())
                    {
                        var templateId = reader.GetInt32(reader.GetOrdinal("TemplateId"));
                        var templateName = reader.GetString(reader.GetOrdinal("TemplateName"));
                        var shared = reader.GetBoolean(reader.GetOrdinal("Shared"));
                        templateInfoList.Add(new TemplateInfo(templateId, templateName, shared));
                    }

                    return templateInfoList;
                }
            }
        }

        public List<TemplateInfo> GetSharedTemplateInfo(TemplateTypes type)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(WINDOW_STICKER_DATABASE))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandText = "WindowSticker.TemplateInfo#FetchSharedByType";
                    command.CommandType = CommandType.StoredProcedure;
                    command.AddParameterWithValue("TemplateTypeId", DbType.Int32, false, (int)type);
                    var reader = command.ExecuteReader();

                    var templateInfoList = new List<TemplateInfo>();

                    while (reader.Read())
                    {
                        var templateId = reader.GetInt32(reader.GetOrdinal("TemplateId"));
                        var templateName = reader.GetString(reader.GetOrdinal("TemplateName"));
                        var dealers = reader.GetString(reader.GetOrdinal("Dealers"));
                        templateInfoList.Add(new TemplateInfo(templateId, templateName, true, dealers));
                    }

                    return templateInfoList;
                }
            }
        }

        public List<TemplateInfo> GetOrphanedTemplateInfo(TemplateTypes type)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(WINDOW_STICKER_DATABASE))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandText = "WindowSticker.TemplateInfo#FetchOrphanedByType";
                    command.CommandType = CommandType.StoredProcedure;
                    command.AddParameterWithValue("TemplateTypeId", DbType.Int32, false, (int)type);
                    var reader = command.ExecuteReader();

                    var templateInfoList = new List<TemplateInfo>();

                    while (reader.Read())
                    {
                        var templateId = reader.GetInt32(reader.GetOrdinal("TemplateId"));
                        var templateName = reader.GetString(reader.GetOrdinal("TemplateName"));
                        templateInfoList.Add(new TemplateInfo(templateId, templateName));
                    }

                    return templateInfoList;
                }
            }
        }

        public bool SaveTemplate(string ownerHandle, Template template, string user)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(WINDOW_STICKER_DATABASE))
            {
                connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.Transaction = transaction;

                        int templateId = SaveTemplate(command, template, ownerHandle, user);

                        DeleteTemplateContents(command, template);

                        bool hasValidContentArea = false;
                        foreach (var contentArea in template.ContentAreas)
                        {
                            if ( contentArea.HasValidDataPoint)
                            {
                                hasValidContentArea = true;
                                break;
                            }
                        }

                        if (hasValidContentArea)
                        {
                            SaveContentAreas(template, command, templateId);
                        }
                        else
                        {
                            throw new ApplicationException(
                                "The template did not have any valid content areas.  Save was not successful.");
                        }
                    }

                    transaction.Commit();
                }
            }

            return true;            
        }

        private static void DeleteTemplateContents(IDataCommand command, Template template)
        {
            command.CommandText = "WindowSticker.Template#DeleteContents";
            command.CommandType = CommandType.StoredProcedure;
            command.AddParameterWithValue("TemplateId", DbType.Int32, false, template.TemplateId);
            command.ExecuteNonQuery();

            // clear our command for reuse
            command.Parameters.Clear();
            command.CommandText = string.Empty;
        }

        private static int SaveTemplate(IDataCommand command, ITemplate template, string ownerHandle, string user)
        {
            bool isNew = template.TemplateId == Constants.UnassignedId;
            command.CommandText = isNew ? "WindowSticker.Template#Insert" : "WindowSticker.Template#Update";
            command.CommandType = CommandType.StoredProcedure;

            bool isSystemTemplate = ownerHandle == null;
            if (isSystemTemplate)
                command.AddParameterWithValue("OwnerHandle", DbType.String, true, DBNull.Value);
            else
                command.AddParameterWithValue("OwnerHandle", DbType.String, true, ownerHandle);

            if (!isNew)
                command.AddParameterWithValue("TemplateId", DbType.Int32, false, template.TemplateId);
                        
            command.AddParameterWithValue("TemplateTypeId", DbType.Byte, false, template.Type);
            command.AddParameterWithValue("Height", DbType.Decimal, false, template.Height);
            command.AddParameterWithValue("Width", DbType.Decimal, false, template.Width);
            command.AddParameterWithValue("Units", DbType.String, false, template.Unit);
            command.AddParameterWithValue("Name", DbType.String, false, template.Name);
            command.AddParameterWithValue("User", DbType.String, false, user);
            command.AddParameterWithValue("SystemTemplate", DbType.Boolean, false, isSystemTemplate);

            if (!string.IsNullOrEmpty(template.BackgroundImage))
            {
                command.AddParameterWithValue("BackgroundImage", DbType.String, true, template.BackgroundImage);
            }

            if (isNew)
                command.AddOutParameter("TemplateId", DbType.Int32);

            command.ExecuteNonQuery();
            var templateId = isNew
                                 ? (int)((IDataParameter) command.Parameters["TemplateId"]).Value
                                 : template.TemplateId;

            if (isNew) template.TemplateId = templateId;

            // clear our command for reuse
            command.Parameters.Clear();
            command.CommandText = string.Empty;
            return templateId;
        }

        private static void SaveContentAreas(ITemplate template, IDataCommand command, int templateId)
        {
            foreach (var contentArea in template.ContentAreas)
            {
                if (contentArea.DataPoint != null && ( contentArea.HasValidDataPoint))
                {
                    command.CommandText = "WindowSticker.ContentArea#Insert";
                    command.AddParameterWithValue("TemplateId", DbType.Int32, false, templateId);
                    command.AddParameterWithValue("TopLeftX", DbType.Decimal, false, contentArea.TopLeft.X);
                    command.AddParameterWithValue("TopLeftY", DbType.Decimal, false, contentArea.TopLeft.Y);
                    command.AddParameterWithValue("BottomRightX", DbType.Decimal, false, contentArea.BottomRight.X);
                    command.AddParameterWithValue("BottomRightY", DbType.Decimal, false, contentArea.BottomRight.Y);
                    command.AddParameterWithValue("FontFamilyId", DbType.Byte, false, (byte)contentArea.Font.Family);
                    command.AddParameterWithValue("FontAlignId", DbType.Byte, false, (byte)contentArea.Font.Align);
                    command.AddParameterWithValue("FontSize", DbType.Decimal, false, contentArea.Font.Size);
                    command.AddParameterWithValue("LineHeight", DbType.Decimal, false, contentArea.Font.LineHeight);
                    command.AddParameterWithValue("CalculatedCharacterAverage", DbType.Int32, false, contentArea.PrintSizes.CharacterAverage);
                    command.AddParameterWithValue("CalculatedEmSize", DbType.Decimal, false, contentArea.PrintSizes.EmSize);
                    command.AddParameterWithValue("CalculatedEnSize", DbType.Decimal, false, contentArea.PrintSizes.EnSize);
                    command.AddParameterWithValue("CalculatedLineHeight", DbType.Decimal, false, contentArea.PrintSizes.LineHeight);
                    command.AddParameterWithValue("CalculatedBulletSize", DbType.Decimal, false, contentArea.PrintSizes.BulletSize);
                    command.AddParameterWithValue("Italics", DbType.Boolean, false, contentArea.Font.Italics);
                    command.AddParameterWithValue("Underline", DbType.Boolean, false, contentArea.Font.Underline);
                    command.AddParameterWithValue("Bold", DbType.Boolean, false, contentArea.Font.Bold);
                    command.AddParameterWithValue("Columns", DbType.Byte, false, contentArea.Columns);
                    command.AddParameterWithValue("ExtraDataJSON", DbType.String, false, contentArea.ExtraDataJSON);
                    command.AddOutParameter("ContentAreaId", DbType.Int32);

                    command.ExecuteNonQuery();
                    var contentAreaId = ((IDataParameter)command.Parameters["ContentAreaId"]).Value;

                    // clear our command for reuse
                    command.Parameters.Clear();
                    command.CommandText = string.Empty;

                    command.CommandText = "WindowSticker.DataPoint#Insert";
                    command.AddParameterWithValue("ContentAreaId", DbType.Int32, false, contentAreaId);

                    if (contentArea.HasValidDynamicDataPoint)
                    {
                        command.AddParameterWithValue("DataPointKeyId", DbType.Byte, true,
                                                      (byte) contentArea.DataPoint.Key.Value);
                        command.AddParameterWithValue("Text", DbType.String, true, DBNull.Value);
                    }
                    else 
                    {
                        string staticText = contentArea.HasValidStaticTextDataPoint
                                                ? contentArea.DataPoint.Text
                                                : string.Empty;

                        command.AddParameterWithValue("DataPointKeyId", DbType.Byte, true, DBNull.Value);
                        command.AddParameterWithValue("Text", DbType.String, true, staticText);    
                    }
                                
                    command.AddParameterWithValue("Prompt", DbType.String, true, contentArea.DataPoint.Prompt);
                    command.AddOutParameter("DataPointId", DbType.Int32);
                    command.ExecuteNonQuery();
                }

                // clear our command for reuse
                command.Parameters.Clear();
                command.CommandText = string.Empty;
            }
        }

        public bool SaveSystemTemplate(Template template, string user)
        {
            return SaveTemplate(null, template, user);
        }

        public bool DeleteTemplate(Template template)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(WINDOW_STICKER_DATABASE))
            {
                connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.Transaction = transaction;

                        command.CommandText = "WindowSticker.Template#Delete";
                        command.CommandType = CommandType.StoredProcedure;
                        command.AddParameterWithValue("TemplateId", DbType.Int32, false, template.TemplateId);
                        command.ExecuteNonQuery();
                    }

                    transaction.Commit();
                }
            }

            return true;            

        }

        public bool ShareTemplate(Template template, int oid)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(WINDOW_STICKER_DATABASE))
            {
                connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.Transaction = transaction;

                        command.CommandText = "WindowSticker.Template#Share";
                        command.CommandType = CommandType.StoredProcedure;
                        command.AddParameterWithValue("TemplateId", DbType.Int32, false, template.TemplateId);
                        command.AddParameterWithValue("OwnerId", DbType.Int32, false, oid);
                        command.AddParameterWithValue("User", DbType.String, false, Thread.CurrentPrincipal.Identity.Name);
                        command.ExecuteNonQuery();
                    }

                    transaction.Commit();
                }
            }

            return true;
        }

        public bool RemoveTemplate(Template template, int? oid)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(WINDOW_STICKER_DATABASE))
            {
                connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.Transaction = transaction;

                        command.CommandText = "WindowSticker.Template#Remove";
                        command.CommandType = CommandType.StoredProcedure;
                        command.AddParameterWithValue("TemplateId", DbType.Int32, false, template.TemplateId);
                        command.AddParameterWithValue("OwnerId", DbType.Int32, false, oid);
                        command.ExecuteNonQuery();
                    }

                    transaction.Commit();
                }
            }

            return true;
        }

        #endregion

    }
}
