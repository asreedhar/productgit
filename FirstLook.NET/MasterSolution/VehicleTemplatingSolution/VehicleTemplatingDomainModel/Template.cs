using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.util;
using FirstLook.Common.Core.IOC;
using FirstLook.DomainModel.Oltp;
using iTextSharp.text.pdf;

namespace VehicleTemplatingDomainModel
{
    [Serializable]
    public class Template : ITemplate
    {
        private int _templateId;

        private static readonly List<TemplateTO> NonTemplatedPDFs = new List<TemplateTO>
        {
            {new TemplateTO(1000000, "Wisconsin Buyer's Guide", 0m, 0m, "in", TemplateTypes.BuyersGuide, null, new List<ContentArea>(), "WisconsinBuyersGuide.pdf")}
        };

        private Template(TemplateTO templateTO)
        {
            TemplateId = templateTO.TemplateId;
            Name = templateTO.Name;
            Height = templateTO.Height;
            Width = templateTO.Width;
            Unit = templateTO.Unit;
            Type = templateTO.Type;
            BackgroundImage = templateTO.BackgroundImage;
            ContentAreas = templateTO.ContentAreas;
            FileName = templateTO.FileName;
        }

        public int TemplateId 
        {
            get { return _templateId; }
            set { _templateId = value;}
        }

        public decimal Height { get; set; }
        public decimal Width { get; set; }
        public string Unit { get; set; }
        public string Name { get; set; }
        public TemplateTypes Type { get; set; }
        public string BackgroundImage { get; set; }
        public List<ContentArea> ContentAreas { get; set; }
        public byte[] StockPdf { get; set; }
        public string FileName { get; set; }

        public Boolean IsExternal
        {
            get { return NonTemplatedPDFs.Exists(t => t.TemplateId == TemplateId); }
        }

        public bool Save(string ownerHandle, string user)
        {
            return DataAccess.SaveTemplate(ownerHandle, this, user);
        }

        public bool SaveAsSystemTemplate(string user)
        {
            return DataAccess.SaveSystemTemplate(this, user);
        }

        public bool Delete()
        {
            if (TemplateId == Constants.UnassignedId || IsExternal) return true;

            return DataAccess.DeleteTemplate(this);
        }

        public bool Remove() { return Remove(null); }
        public bool Remove(int? oid)
        {
            if (TemplateId == Constants.UnassignedId || IsExternal) return true;

            return DataAccess.RemoveTemplate(this, oid);
        }

        public bool Share(int oid)
        {
            if (TemplateId == Constants.UnassignedId || IsExternal) return true;

            return DataAccess.ShareTemplate(this, oid);
        }

        private static IVehicleTemplatingDataAccess DataAccess
        {
            get { return Registry.Resolve<IVehicleTemplatingDataAccess>(); }
        }

        public TemplateTO AsTransferObject()
        {
            return new TemplateTO(TemplateId, Name, Height, Width, Unit, Type, BackgroundImage, ContentAreas, FileName);
        }

        public byte[] ProcessExternalPdf(string ownerHandle, IVehicleData vehicle)
        {
            byte[] externalPdf = null;

            if (IsExternal)
            {
                using (var stream = new MemoryStream())
                {
                    switch (TemplateId)
                    {
                        case 1000000: // Wisconsin Buyer's Guide
                            var reader = new PdfReader(Assembly.GetExecutingAssembly()
                                .GetManifestResourceStream(
                                    "VehicleTemplatingDomainModel.ExternalPdfs.WisconsinBuyersGuide.pdf"));

                            var stamp = new PdfStamper(reader, stream);
                            var acrofields = stamp.AcroFields;

                            var dealerName = String.Empty;

                            var owner = Owner.GetOwner(ownerHandle);
                            if (owner != null)
                            {
                                var businessUnit = BusinessUnitFinder.Instance().Find(owner.OwnerEntityId);
                                dealerName = businessUnit != null ? businessUnit.ShortName : "";
                            }

                            acrofields.SetField("YearMakeAndModel",
                                String.Format("{0} {1} {2}", vehicle.ModelYear, vehicle.Make, vehicle.Model));
                            acrofields.SetField("VIN", vehicle.Vin);
                            acrofields.SetField("DealershipName", dealerName);

                            var driveTrain = vehicle.Drivetrain;
                            switch (vehicle.Drivetrain)
                            {
                                case "Four Wheel Drive":
                                case "4x4":
                                case "All Wheel Drive":
                                    driveTrain = "4WDorAWD";
                                    break;
                                case "Front Wheel Drive":
                                    driveTrain = "FWD";
                                    break;
                                case "Rear Wheel Drive":
                                    driveTrain = "RWD";
                                    break;
                            }
                            acrofields.SetField("DriveTypeGroup", driveTrain);

                            //acrofields.SetField("Engine", vehicle.Engine);
                            if (vehicle.Transmission.Contains("A/T") || vehicle.Transmission.Contains("Automatic"))
                                acrofields.SetField("TransmissionGroup", "Automatic");
                            else if (vehicle.Transmission.Contains("M/T") || vehicle.Transmission.Contains("Manual"))
                                acrofields.SetField("TransmissionGroup", "Manual");

                            acrofields.SetField("Price", vehicle.Price.ToString("C"));
                            acrofields.SetField("StockNumber", vehicle.StockNumber);

                            if (vehicle.Mileage > 0)
                            {
                                acrofields.SetField("OdometerReadingGroup", "OdometerRead"); // set the check box
                                acrofields.SetField("OdometerMileage", vehicle.Mileage.ToString("N0"));
                                acrofields.SetField("OdometerReading_OdometerReadGroup", "Actual");
                            }

                            stamp.Close();
                            stream.Flush();
                            externalPdf = stream.ToArray();

                            break;
                    }
                }
            }

            return externalPdf;
        }




        #region Factory Methods

        public static List<TemplateInfo> GetTemplateInfoForOwnerByTemplateType(string ownerHandle, TemplateTypes templateType)
        {
            var templates = DataAccess.GetTemplateInfoForOwner(ownerHandle, templateType);

            // Add non templated PDFs
            templates.AddRange(
                NonTemplatedPDFs.Where(t => t.Type == templateType)
                    .Select(t => new TemplateInfo(t.TemplateId, t.Name, false)));

            return templates;
        }

        public static List<TemplateInfo> GetSharedTemplateInfoByTemplateType(TemplateTypes templateType)
        {
            return DataAccess.GetSharedTemplateInfo(templateType);
        }

        public static List<TemplateInfo> GetOrphanedTemplateInfoByTemplateType(TemplateTypes templateType)
        {
            return DataAccess.GetOrphanedTemplateInfo(templateType);
        }

        public static Template CreateTemplate(TemplateTO templateTO)
        {
            return new Template(templateTO);
        }

        public static Template GetTemplate(int templateId)
        {
            TemplateTO templateTO;

            templateTO = NonTemplatedPDFs.Exists(t => t.TemplateId == templateId)
                ? NonTemplatedPDFs.Single(t => t.TemplateId == templateId)
                : DataAccess.GetTemplate(templateId);

            return new Template(templateTO);
        }
        #endregion
    }
}