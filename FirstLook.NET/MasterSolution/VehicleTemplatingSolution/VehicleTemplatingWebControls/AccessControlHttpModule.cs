﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace VehicleTemplatingWebControls
{
    class AccessControlHttpModule : IHttpModule
    {
        #region Implementation of IHttpModule

        public void Init(HttpApplication context)
        {
            context.PostAcquireRequestState += PostAcquireRequestState;
        }

        public void Dispose()
        {
            // Nothing Here
        }

        #endregion

        private void PostAcquireRequestState(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

    }
}
