﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VehicleTemplatingDomainModel;

namespace VehicleTemplatingWebControls
{
    public class PrintMultiVehicleTemplateBuider : PrintVehicleTemplateBuilder
    {
        protected List<IVehicleData> VehicleList { get; set; }

        public PrintMultiVehicleTemplateBuider(IVehicleData vehicle, ITemplate template, string backgroundImageVirtualRoot):base(vehicle, template, backgroundImageVirtualRoot)
        {

            VehicleList = new List<IVehicleData>();
            VehicleList.Add(vehicle);

            Vehicle = vehicle;
            BackgroundImageVirtualRoot = backgroundImageVirtualRoot;
            URLShortener = new URLShortener();

        }
        public PrintMultiVehicleTemplateBuider(List<IVehicleData> vehicleList, ITemplate template, string backgroundImageVirtualRoot)
            : base(vehicleList[0], template, backgroundImageVirtualRoot) // change base to have different constructor
        {
            VehicleList = vehicleList;
            Template = template;
            BackgroundImageVirtualRoot = backgroundImageVirtualRoot;
            URLShortener = new URLShortener();
        }

        protected override void BuildHeadStyle(StringBuilder html)
        {
            // cut and past of base WITHOUT absolute positioning in body

            html.AppendLine(@"<style media='print' type='text/css'>");
            html.AppendFormat("@page {{ size: {0} {1}; margin: 0; padding: 0; }}",
                                  GetPrintValue(Template.Width),
                                  GetPrintValue(Template.Height));

            html.AppendLine();
            html.AppendLine(@"body { font-size: 12pt; line-height: 1.2; font-family: Arial; overflow: hidden; margin: 0; padding: 0; top: 0; left: 0; }");
            html.AppendLine(@"ul, ol { padding-left: 2em; margin: 0; }");
            html.AppendLine(@"ol { list-style-type: decimal; }");
            html.AppendFormat(".{0} {{ overflow: hidden; padding: 0; margin: 0; z-index: 5; position: absolute; }}", SR.PrintAreaClass);
            html.AppendLine();
            html.AppendLine(@"</style>");
            
        }

        protected override void BuildBody(StringBuilder html)
        {
            // start body
            html.AppendLine(@"<body>");

            // loop through vehicles
            foreach (var vehicle in VehicleList)
            {
                // set the member level vehicle data for backward compatibility
                Vehicle = vehicle;

                // regeneral all QRCode/SMS for each vehicle
                _GenericURL_Shortened = null;
                _QRCodeURL_ShortenedUppercased = null;
                _SMSURL_Alias = null;


                // start <DIV> for vehicle
                html.AppendFormat("<div style='width: {0}; height: {1};'>",
                                  GetPrintValue(Template.Width),
                                  GetPrintValue(Template.Height));

                // get vehicle htlm
                BuildVehicle(html);

                // end <DIV> for vehicle (page break if implementing multiply templates?
                html.AppendLine(@"</div>");

            }

            // end body
            html.AppendLine(@"</body>");
            
        }


    }
}
