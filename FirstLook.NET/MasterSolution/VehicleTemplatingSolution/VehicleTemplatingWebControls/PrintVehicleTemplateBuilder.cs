﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.Utilities;
using VehicleTemplatingDomainModel;
using Newtonsoft.Json.Linq;
using System.Configuration;

namespace VehicleTemplatingWebControls
{
    public class PrintVehicleTemplateBuilder
    {
        #region Constructor

        /// <summary>
        /// Construct a PrintVehicleTempalteBuilder Object
        /// </summary>
        /// <param name="vehicle"></param>
        /// <param name="template"></param>
        /// <param name="backgroundImageVirtualRoot"></param>
        public PrintVehicleTemplateBuilder(IVehicleData vehicle, ITemplate template, string backgroundImageVirtualRoot)
        {
            Vehicle = vehicle;
            Template = template;
            BackgroundImageVirtualRoot = backgroundImageVirtualRoot;
            URLShortener = new URLShortener();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Vehicle to build a printable template for
        /// </summary>
        public IVehicleData Vehicle { get; protected set; }


        /// <summary>
        /// Template to build for a vehicle
        /// </summary>
        public ITemplate Template { get; protected set; }

        /// <summary>
        /// Virtual root of the background image directory
        /// </summary>
        public string BackgroundImageVirtualRoot { get; protected set; }

        #endregion

        #region Public Methods
        /// <summary>
        /// Returns the ByteStream for a PDF of the Vehicle Tempalte
        /// </summary>
        /// <returns></returns>
        public byte[] BuildVehicleTemplate(string ownerHandle)
        {
            byte[] retBytes;

            if (Template.IsExternal)
                retBytes = Template.ProcessExternalPdf(ownerHandle, Vehicle);
            else
            {
                string html = BuildHtml();
                retBytes = PdfHelper.GeneratePdf(html);
            }

            return retBytes;
        }

        public string BuildVehicleHtml()
        {
            return BuildHtml();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Format the decimal value as a CSS printable dimension
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        protected string GetPrintValue(decimal d)
        {
            return string.Format("{0}{1}", d, Template.Unit);
        }

        /// <summary>
        /// Format the Enum FontFamily as a String for CSS
        /// </summary>
        /// <param name="ff"></param>
        /// <returns></returns>
        private static string GetFontFamily(FontFamily ff)
        {
            string value = ff.ToString().Replace('_', ' ');
            if (value.Contains(" "))
            {
                value = string.Format("\"{0}\"", value);
            }
            return value;
        }

        /// <summary>
        /// Return the font size in pt formats
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        private static string GetFontSize(decimal size)
        {
            return string.Format("{0}pt", size);
        }

        /// <summary>
        /// Generate the HTML for the PDF Generation
        /// </summary>
        /// <returns></returns>
        protected virtual string BuildHtml()
        {
            StringBuilder html = new StringBuilder();

            BuildDocType(html);

            html.AppendLine(@"<html lang='en'>");

            BuildHead(html);

            BuildBody(html);

            html.AppendLine(@"</html>");

            return html.ToString();
        }

        /// <summary>
        /// Add DocType to the Buffer
        /// </summary>
        /// <param name="html"></param>
        protected static void BuildDocType(StringBuilder html)
        {
            html.AppendLine(@"<!DOCTYPE html>"); /// HTML 5 Decloration, PM
        }

        /// <summary>
        /// Add the HTML head to the Buffer
        /// </summary>
        /// <param name="html"></param>
        protected void BuildHead(StringBuilder html)
        {
            html.AppendLine(@"<head>");
            html.AppendLine(@"<meta name='generator' content='VehicleTemplatingWebControls.PrintVehicleTemplateBuilder' />");
            html.AppendLine(@"<meta charset='utf-8'/>");
            html.AppendFormat("<title>{0}</title>", Template.Name);

            BuildHeadStyle(html);

            html.AppendLine(@"</head>");
        }

        /// <summary>
        /// Set Defealt Styles and Page Size in Global Styles
        /// </summary>
        /// <param name="html"></param>
        protected virtual void BuildHeadStyle(StringBuilder html)
        {
            html.AppendLine(@"<style media='print' type='text/css'>");
            html.AppendFormat("@page {{ size: {0} {1}; margin: 0; padding: 0; }}",
                                  GetPrintValue(Template.Width),
                                  GetPrintValue(Template.Height));

            html.AppendLine();
            html.AppendLine(@"body { font-size: 12pt; line-height: 1.2; font-family: Arial; overflow: hidden; margin: 0; padding: 0; position: absolute; top: 0; left: 0; }");
            html.AppendLine(@"ul, ol { padding-left: 2em; margin: 0; }");
            html.AppendLine(@"ol { list-style-type: decimal; }");
            html.AppendFormat(".{0} {{ overflow: hidden; padding: 0; margin: 0; z-index: 5; position: absolute; }}", SR.PrintAreaClass);
            html.AppendLine();
            html.AppendLine(@"</style>");
        }

        /// <summary>
        /// Add Body to the Buffer
        /// </summary>
        /// <param name="html"></param>
        protected virtual void BuildBody(StringBuilder html)
        {
            html.AppendFormat("<body style='width: {0}; height: {1};'>",
                              GetPrintValue(Template.Width),
                              GetPrintValue(Template.Height));

            BuildVehicle(html);


            html.AppendLine(@"</body>");
        }


        /// <summary>
        /// Add an individual vehicle to the body
        /// </summary>
        /// <param name="html"></param>
        protected virtual void BuildVehicle(StringBuilder html)
        {

            if (!string.IsNullOrEmpty(BackgroundImageVirtualRoot) && !string.IsNullOrEmpty(Template.BackgroundImage))
                html.AppendFormat("<img src='{0}{1}' style='width:{3}{2};height:{4}{2}' />", BackgroundImageVirtualRoot, Template.BackgroundImage, Template.Unit,
                         Template.Width, Template.Height);

            if (Template.ContentAreas != null)
            {
                foreach (ContentArea contentArea in Template.ContentAreas)
                {
                    if (contentArea.DataPoint.Prompt == null || contentArea.DataPoint.Selected)
                    {
                        BuildPrintArea(html, contentArea);
                    }
                }
            }

        }


        /// <summary>
        /// Add A ContentArea to the Buffer
        /// </summary>
        /// <param name="html"></param>
        /// <param name="contentArea"></param>
        private void BuildPrintArea(StringBuilder html, ContentArea contentArea)
        {
            html.AppendFormat("<div class='{0}' style='{1}'>", SR.PrintAreaClass, BuildPrintAreaStyle(contentArea));

            DataPoint dataPoint = contentArea.DataPoint;

            if (dataPoint.Key.HasValue && dataPoint.Key != DynamicDataPointKeys.Undefined)
            {   // it is a dynamic data point
                BuildDynamicContent(html, contentArea, dataPoint);
            }
            else
            {   // it must be static text
                BuildStaticContent(html, dataPoint);
            }

            html.AppendLine(@"</div>");
        }

        /// <summary>
        /// Return the CSS Style String for a Content Area
        /// </summary>
        /// <param name="contentArea"></param>
        /// <returns></returns>
        private string BuildPrintAreaStyle(ContentArea contentArea)
        {
            StringBuilder sb = new StringBuilder();

            decimal width = contentArea.BottomRight.X - contentArea.TopLeft.X;
            decimal height = contentArea.BottomRight.Y - contentArea.TopLeft.Y;

            // do not adjust height if the content area is not for text
            if (contentArea.DataPoint.Key != DynamicDataPointKeys.QRCodeURL)
            {
                decimal lineHeight = contentArea.PrintSizes.LineHeight;
                decimal lineHeightAdjustment = height % lineHeight;

                if (height != lineHeightAdjustment) // Exacly Single Line, PM
                {
                    height -= lineHeightAdjustment;
                }

                if (height < lineHeight)
                {
                    height = lineHeight;
                }
            }

            // Apply the Print Position
            sb.AppendFormat("top: {0}; left: {1}; width: {2}; height: {3}; ",
                            GetPrintValue(contentArea.TopLeft.Y),
                            GetPrintValue(contentArea.TopLeft.X),
                            GetPrintValue(width),
                            GetPrintValue(height));

            // Apply the Font Formating
            sb.AppendFormat("font-family: {0}; font-size: {1}; line-height: {2}; text-align: {3}; ",
                GetFontFamily(contentArea.Font.Family),
                GetFontSize(contentArea.Font.Size),
                contentArea.Font.LineHeight,
                contentArea.Font.Align);


            // Apply the Text Styling
            if (contentArea.Font.Italics)
            {
                sb.Append(@"font-style: italic; ");
            }

            if (contentArea.Font.Bold)
            {
                sb.Append(@"font-weight: bold; ");
            }

            if (contentArea.Font.Underline)
            {
                sb.Append(@"text-decoration: underline; ");
            }

            return sb.ToString();
        }

        /// <summary>
        /// Add a Static ContentArea to the Buffer
        /// </summary>
        /// <param name="html"></param>
        /// <param name="dataPoint"></param>
        private static void BuildStaticContent(StringBuilder html, IDataPoint dataPoint)
        {
            BuildContentString(html, dataPoint.Text);
        }

        /// <summary>
        /// Add a DynamicDataPoint to the Buffer
        /// with Content from the Vehicle
        /// </summary>
        /// <param name="html"></param>
        /// <param name="contentArea"></param>
        /// <param name="dataPoint"></param>
        private void BuildDynamicContent(StringBuilder html, ContentArea contentArea, IDataPoint dataPoint)
        {
            if (Vehicle == null)
            {
                throw new NullReferenceException("Vehicle can not be NULL");
            }

            switch (dataPoint.Key)
            {
                case DynamicDataPointKeys.Year:
                    BuildContentString(html, Vehicle.ModelYear.ToString());
                    break;
                case DynamicDataPointKeys.Make:
                    BuildContentString(html, Vehicle.Make);
                    break;
                case DynamicDataPointKeys.Model:
                    BuildContentString(html, Vehicle.Model);
                    break;
                case DynamicDataPointKeys.VehicleDescription:
                    BuildContentString(html, Vehicle.Description);
                    break;
                case DynamicDataPointKeys.ExteriorColor:
                    BuildContentString(html, Vehicle.ExteriorColor);
                    break;
                case DynamicDataPointKeys.InteriorColor:
                    BuildContentString(html, Vehicle.InteriorColor);
                    break;
                case DynamicDataPointKeys.Mileage:
                    BuildContentString(html, string.Format(SR.IntegerFormat, Vehicle.Mileage));
                    break;
                case DynamicDataPointKeys.StockNumber:
                    BuildContentString(html, Vehicle.StockNumber);
                    break;
                case DynamicDataPointKeys.Transmission:
                    BuildContentString(html, Vehicle.Transmission);
                    break;
                case DynamicDataPointKeys.Engine:
                    BuildContentString(html, Vehicle.Engine);
                    break;
                case DynamicDataPointKeys.Drivetrain:
                    BuildContentString(html, Vehicle.Drivetrain);
                    break;
                case DynamicDataPointKeys.Certified:
                    BuildContentString(html, Vehicle.Certified ? "X" : string.Empty);
                    break;
                case DynamicDataPointKeys.BodyStyle:
                    BuildContentString(html, Vehicle.BodyStyle);
                    break;
                case DynamicDataPointKeys.Vin:
                    BuildContentString(html, Vehicle.Vin);
                    break;
                case DynamicDataPointKeys.Series:
                    BuildContentString(html, Vehicle.Series);
                    break;
                case DynamicDataPointKeys.Price:
                    BuildContentString(html,
                                       Vehicle.Price != default(decimal)
                                           ? string.Format(SR.DollarFormat, Vehicle.Price)
                                           : string.Empty);
                    break;
                case DynamicDataPointKeys.Trim:
                    BuildContentString(html, Vehicle.Trim);
                    break;
                case DynamicDataPointKeys.Equipment:
                    BuildContentList(html, contentArea, Vehicle.Equipment, false);
                    break;
                case DynamicDataPointKeys.MpgCity:
                    BuildContentString(html, Vehicle.MpgCity);
                    break;
                case DynamicDataPointKeys.MpgHwy:
                    BuildContentString(html, Vehicle.MpgHwy);
                    break;
                case DynamicDataPointKeys.CertifiedID:
                    BuildContentString( html, Vehicle.CertifiedID );
                    break;
                case DynamicDataPointKeys.KBBBookValue:
                    BuildContentString(html, Vehicle.KBBBookValue > 0 ? Vehicle.KBBBookValue.ToString() : string.Empty);
                    break;
                case DynamicDataPointKeys.NADABookValue:
                    BuildContentString(html, Vehicle.NADABookValue > 0 ? Vehicle.NADABookValue.ToString("$#,##0") : string.Empty);
                    break;
                case DynamicDataPointKeys.Packages:
                    BuildContentList(html, contentArea, Vehicle.Packages, true);
                    break;
                case DynamicDataPointKeys.QRCodeURL:
                    BuildQRCodeURLImageContent(html, QRCodeURL(), contentArea, dataPoint);
                    break;
                case DynamicDataPointKeys.MaxShowroomDirectURL:
                    BuildContentString(html, GenericURL());
                    break;
                case DynamicDataPointKeys.MaxShowroomSMSCode:
                    BuildContentString(html, SMSCode());
                    break;
                case DynamicDataPointKeys.MaxShowroomSMSPhoneNumber:
                    BuildContentString(html, SMSPhoneNumber());
                    break;
                case DynamicDataPointKeys.MSRP:
                    BuildContentString(html,
                                       Vehicle.MSRP != default(decimal)
                                           ? string.Format(SR.DollarFormat, Vehicle.MSRP)
                                           : string.Empty);
                    break;
            }
        }

        /// <summary>
        /// Add a list of Content To the Buffer
        /// </summary>
        /// <param name="html"></param>
        /// <param name="contentArea"></param>
        /// <param name="equipment"></param>
        /// <param name="withSpacing"> </param>
        private void BuildContentList(StringBuilder html, ContentArea contentArea, IEnumerable<string> equipment, bool withSpacing)
        {
            // Make sure columns are at least '1' to avoid divide by zero error
            int columns = contentArea.Columns == 0 ? 1 : contentArea.Columns;

            decimal width = contentArea.BottomRight.X - contentArea.TopLeft.X;
            decimal columnHeight = contentArea.BottomRight.Y - contentArea.TopLeft.Y;
            decimal columnWidth = width / columns;

            columnWidth = Math.Round(columnWidth * 1e2m) / 1e2m;

            int numLines = Int32Helper.ToInt32(Math.Floor(columnHeight/contentArea.PrintSizes.LineHeight));

            int listCount = 1;
            int lineNum = 1;
            StringBuilder[] items = new StringBuilder[columns];

            for (int i = 0; i < columns; i++)
            {
                items[i] = new StringBuilder();
                items[i].AppendFormat("<ul style='position: absolute; left: {0}; width: {1}'>",
                                      GetPrintValue(i * columnWidth),
                                      GetPrintValue(columnWidth - contentArea.PrintSizes.BulletSize));
                items[i].AppendLine();
            }

            foreach (string s in equipment)
            {
                if (listCount > columns) continue;

                decimal e = 0;
                foreach (char c in s)
                {
                    e += char.IsUpper(c) ? contentArea.PrintSizes.EmSize : contentArea.PrintSizes.EnSize;
                }

                int d = Int32Helper.ToInt32(Math.Ceiling(e / columnWidth));

                lineNum += d;

                if (listCount > columns) continue;

                if (withSpacing)
                {
                    items[listCount - 1].AppendFormat("<li rel='{1}'><p>{0}</p></li>", s, d);
                }
                else
                {
                    items[listCount - 1].AppendFormat("<li rel='{1}'>{0}</li>", s, d);
                }

                // Possible Truncation of List Text, PM
                items[listCount - 1].AppendLine();

                if (lineNum >= numLines)
                {
                    listCount++;
                    lineNum = 1;
                }
            }

            foreach (StringBuilder sb in items)
            {
                sb.AppendLine(@"</ul>"); // Close List

                html.Append(sb.ToString()); // Append List to html
            }
        }

      
        /// <summary>
        /// Add a string Content to the Buffer
        /// </summary>
        /// <param name="html"></param>
        /// <param name="text"></param>
        private static void BuildContentString(StringBuilder html, string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return; // I'm not sure about the viability of this, it should be safe but it we also probably should check before we call this, PM
            }

            html.AppendLine(text);
        }

        // these data are all derived from Vehicle and/or Template, and never depend on any specific data points
        protected IURLShortener URLShortener;
        
        protected string _GenericURL_Shortened;
        protected string _QRCodeURL_ShortenedUppercased;
        protected string _SMSURL_Alias;

        private string GenericURL()
        {
            if( _GenericURL_Shortened == null )
            {
                var GenericURLTemplate = ConfigurationManager.AppSettings["GenericURLTemplate"];
                var GenericURL = String.Format( GenericURLTemplate, Vehicle.Vin, Template.TemplateId );
                _GenericURL_Shortened = URLShortener.Shorten( GenericURL );
            }
            return _GenericURL_Shortened;
        }

        private string QRCodeURL()
        {
            if( _QRCodeURL_ShortenedUppercased == null )
            {
                var QRCodeURLTemplate = ConfigurationManager.AppSettings["QRCodeURLTemplate"];
                var QRCodeURL = String.Format( QRCodeURLTemplate, Vehicle.Vin, Template.TemplateId );
                var QRCodeURL_Shortened = URLShortener.Shorten( QRCodeURL );
                _QRCodeURL_ShortenedUppercased = QRCodeURL_Shortened.ToUpper();
            }
            return _QRCodeURL_ShortenedUppercased;
        }

        private string SMSCode()
        {
            if( _SMSURL_Alias == null )
            {
                var SMSURLTemplate = ConfigurationManager.AppSettings["SMSURLTemplate"];
                var SMSURL = String.Format( SMSURLTemplate, Vehicle.Vin, Template.TemplateId );
                var SMSURL_Shortened = URLShortener.Shorten( SMSURL );
                _SMSURL_Alias = SMSURL_Shortened.Substring( URLShortener.ProviderPrefix().Length );
            }
            return _SMSURL_Alias;
        }

        private string SMSPhoneNumber()
        {
            return ConfigurationManager.AppSettings["SMSPhoneNumber"];
        }

        /// <summary>
        /// Builds a "QR"-encoded URL as a PNG image in the dimensions of the content area;
        /// Then creates an html string that represents the PNG image data as a 64-bit string
        /// And inserts it inline into the result HTML, using CSS
        /// </summary>
        private static void BuildQRCodeURLImageContent(StringBuilder html, string QRCodeURL_Shortened, ContentArea contentArea, IDataPoint dataPoint)
        {
            dynamic ExtraDataJSON = JObject.Parse( contentArea.ExtraDataJSON );
            string error_correction_level = ExtraDataJSON.ErrorCorrectionLevel; // only present in content areas with data point = QRCodeURL (26)
            
            var img_src = GoogleChartTools_Infographics_QRCodes_URL( QRCodeURL_Shortened, error_correction_level );
            var img_alt = "";
            var img_style = "width:100%;height:100%;";
            var img_html = String.Format( "<img style=\"{0}\" alt=\"{1}\" src=\"{2}\" />", img_style, img_alt, img_src );

            html.AppendLine( img_html );
        }

        private static string GoogleChartTools_Infographics_QRCodes_URL( string data_utf8_url_enc, string error_correction_level_code )
        {
            int size_px = 177; // allows for version 40 QR codes (maximum data) while still within google's hard limits for pixel sizes
            int margin_rows = 0; // no margin, visual margin handled by template layout
            string url_template = "https://chart.googleapis.com/chart?cht=qr&chs={0}x{0}&choe=UTF-8&chld={1}|{2}&chl={3}";
            return String.Format( url_template, size_px, error_correction_level_code, margin_rows, data_utf8_url_enc );
        }

        #endregion

        #region Internal Static Resources
        static internal class SR
        {
            /// <summary>
            /// CSS Class used for the each ContentArea
            /// </summary>
            public static string PrintAreaClass = "print_area";

            /// <summary>
            /// Standard Formating for Integer Values
            /// </summary>
            public static string IntegerFormat = "{0:0,0}";

            /// <summary>
            /// Standard Formating for Dollar Values
            /// </summary>
            public static string DollarFormat = "{0:c}";
        }
        #endregion
    }
}
