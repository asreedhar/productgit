﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using NUnit.Framework;
using VehicleTemplatingDomainModel;
using FirstLook.Common.Core.Extensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using VehicleData=VehicleTemplatingWebServiceTests.VehicleTemplatingService.VehicleData;


namespace VehicleTemplatingWebServiceTests
{

    [TestFixture]
    public class VehicleTemplatingWebServiceTests
    {
        private VehicleTemplatingService.VehicleTemplatingService _client;

        [SetUp]
        public void Setup()
        {
            _client = new VehicleTemplatingService.VehicleTemplatingService();            
        }

        /*[Test]
        public void CanGetTemplateInfoForOwner()
        {
            var templateInfoJson = _client.GetWindowStickerTemplateInfoForOwnerAsJson("684E7529-3041-DD11-9F48-0014221831B0");
            Debug.WriteLine("json: " + templateInfoJson);
            var templateInfo = templateInfoJson.FromJson<List<TemplateInfo>>();
        }*/

        /*[Test]
        public void CanGetTemplate()
        {
            var templateInfoJson = _client.GetWindowStickerTemplateInfoForOwnerAsJson("684E7529-3041-DD11-9F48-0014221831B0");
            Debug.WriteLine("json: " + templateInfoJson);
            var templateInfo = templateInfoJson.FromJson<List<TemplateInfo>>();

            if (templateInfo.Count > 0)
            {
                var templateJson = _client.GetTemplateAsJson(templateInfo[0].Id);

                Debug.WriteLine("template Json: " + templateJson);

                var template = templateJson.FromJson<TemplateTO>();

                Assert.IsNotNull(template);
            }
        }*/

        /*[Test]
        public void CanGetEditSaveTemplate()
        {
            var templateInfoJson = _client.GetWindowStickerTemplateInfoForOwnerAsJson("684E7529-3041-DD11-9F48-0014221831B0");
            Debug.WriteLine("json: " + templateInfoJson);
            var templateInfo = templateInfoJson.FromJson<List<TemplateInfo>>();

            var newname = "Template ABC~" + Guid.NewGuid();

            if (templateInfo.Count > 0)
            {
                var templateId = templateInfo[0].Id;

                var templateJson = _client.GetTemplateAsJson(templateId);

                Debug.WriteLine("template Json: " + templateJson);

                var template = templateJson.FromJson<TemplateTO>();

                template.Name = newname;

                _client.SaveTemplate(template.ToJson(), "684E7529-3041-DD11-9F48-0014221831B0");

                var updatedTemplate = _client.GetTemplateAsJson(templateId).FromJson<TemplateTO>();
                Assert.AreEqual(newname, updatedTemplate.Name);
            }
        }*/

        [Test]
        public void CanCallGeneratePdf()
        {
            // VehicleData data = new VehicleData();
            // TemplateTO template = new TemplateTO();

            // VehicleTemplatingService.VehicleData serviceData = (VehicleTemplatingService.VehicleData) data;

            VehicleTemplatingService.VehicleData data = null;
            VehicleTemplatingService.TemplateTO template = null;

            _client.GeneratePdf(data, template);
        }

        [Test]
        public void TryGenerateSamplePDF()
        {
            var vehicle_data_json = "{\"AgeInDays\":10,\"Make\":\"Make\",\"Model\":\"Model\",\"ModelYear\":1985,\"Series\":\"Series\",\"Price\":10000,\"UnitCost\":10000,\"Mileage\":50,\"StockNumber\":\"#StockNum\",\"Vin\":\"19UYA31581L000000\",\"Engine\":\"Engine\",\"FuelType\":\"FuelType\",\"Transmission\":\"Transmission\",\"Class\":\"Class\",\"Trim\":\"Trim\",\"BodyStyle\":\"BodyStyle\",\"Description\":\"Description\",\"ExteriorColor\":\"ExteriorColor\",\"InteriorColor\":\"InteriorColor\",\"Drivetrain\":\"Drivetrain\",\"Certified\":false,\"Equipment\":[\"Equipment0\",\"Equipment1\"],\"Packages\":[\"Package0\",\"Package1\"],\"MpgCity\":\"MpgCity\",\"MpgHwy\":\"MpgHwy\",\"CertifiedID\":\"CertifiedID\",\"KBBBookValue\":10000,\"NADABookValue\":10000,\"QRCodeURL\":\"QRCodeURL\"}";
            var template_json = "{\"__type\":\"VehicleTemplatingWebServiceClient.VehicleTemplatingService.TemplateTO\",\"TemplateId\":3046,\"Height\":11,\"Width\":8.5,\"Unit\":\"in\",\"Name\":\"HIGH ROLLER\",\"BackgroundImage\":null,\"Type\":0,\"ContentAreas\":[{\"Id\":199751,\"DataPoint\":{\"Key\":26,\"Text\":\"\",\"Prompt\":null,\"Selected\":false},\"Font\":{\"Family\":0,\"Align\":1,\"LineHeight\":1.2,\"Italics\":false,\"Underline\":false,\"Bold\":false,\"Size\":12},\"TopLeft\":{\"X\":0,\"Y\":0},\"BottomRight\":{\"X\":5,\"Y\":5},\"PrintSizes\":{\"LineHeight\":0.198,\"CharacterAverage\":0,\"EmSize\":0.135,\"EnSize\":0.083,\"BulletSize\":0.406},\"Columns\":0,\"ExtraDataJSON\":\"{\\\"ErrorCorrectionLevel\\\":\\\"L\\\"}\"}]}";

            VehicleTemplatingService.VehicleData vehicle_data = JsonConvert.DeserializeObject<VehicleTemplatingService.VehicleData>( vehicle_data_json );
            VehicleTemplatingService.TemplateTO template = JsonConvert.DeserializeObject<VehicleTemplatingService.TemplateTO>( template_json );
            
            byte[] pdf_bin = _client.GeneratePdf( vehicle_data, template );

            var desktopFolder = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            var fullFileName = System.IO.Path.Combine(desktopFolder, "TryGenerateSamplePDF.pdf");
            var fs = new System.IO.FileStream(fullFileName, System.IO.FileMode.Create);
            fs.Write( pdf_bin, 0, pdf_bin.Length );
        }


    }
}
