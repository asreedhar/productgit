﻿using System.ComponentModel;
using System.Web.Mvc;

namespace Max.Selling.WebService.Internal
{
    public class SkipNullBinder : DefaultModelBinder
    {
        protected override void SetProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor, object value)
        {
            if(value == null)
                return;

            base.SetProperty(controllerContext, bindingContext, propertyDescriptor, value);
        }
    }
}
