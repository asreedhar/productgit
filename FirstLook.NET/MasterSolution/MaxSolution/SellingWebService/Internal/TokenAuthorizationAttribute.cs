﻿using System;
using System.Security.Principal;
using System.Web.Mvc;
using Max.Selling.Domain;

namespace Max.Selling.WebService.Internal
{
    public class TokenAuthorizationAttribute : FilterAttribute, IAuthorizationFilter
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string _tokenParam;

        public TokenAuthorizationAttribute() : this("token")
        {
        }

        public TokenAuthorizationAttribute(string tokenParam)
        {
            _tokenParam = tokenParam;
        }

        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if(!filterContext.RouteData.Values.ContainsKey(_tokenParam))
                throw new InvalidOperationException("no token found in route");

            filterContext.HttpContext.User = new GenericPrincipal(new GenericIdentity(string.Empty), new string[] { }); 
            string token = filterContext.RouteData.Values[_tokenParam] as string;

            try
            {
                var ticket = SellingTicket.FromString(token);

                if (!ticket.IsExpired)
                {
                    if(ticket.UserName.IsDemo())
                        filterContext.HttpContext.User = new GenericPrincipal(new GenericIdentity(ticket.UserName), new string[] { Roles.Demo });
                    else
                        filterContext.HttpContext.User = new GenericPrincipal(new GenericIdentity(ticket.UserName), new string[] {});
                }
            }
            catch (Exception) //bad tokens throw exception when we try to decrypt them
            {
                Log.Info("Bad Token");
            }

            if(!filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                Log.Info("Ticket Expired");

                //send a 401 and abort
                filterContext.Result = new HttpUnauthorizedResult();
                
            }
              
        }
    }
}
