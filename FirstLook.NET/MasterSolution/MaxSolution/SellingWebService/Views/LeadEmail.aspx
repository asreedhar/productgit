﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Max.Selling.WebService.ViewModels.LeadEmailViewModel>" %>
To: <%= Model.CustomerFirstName %> <%= Model.CustomerLastName %>
<%= Model.CustomerPhoneNumber %>
<%= Model.CustomerEmail %>

Dear <%= Model.CustomerFirstName %>,
Here is the information on the <%= Model.Make%> <%=Model.Model %> you requested from <%= Model.DealerName%>.

If you have any questions, please call us <%= !string.IsNullOrEmpty(Model.SalesmanPhoneNumber) ? Model.SalesmanPhoneNumber : Model.DealerPhoneNumber %>
