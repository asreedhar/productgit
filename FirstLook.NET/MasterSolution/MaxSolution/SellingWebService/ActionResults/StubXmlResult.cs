﻿using System.Text;
using System.Web.Mvc;
using System.IO;

namespace Max.Selling.WebService.ActionResults
{
    internal class StubXmlResult : ActionResult
    {
        private const string XmlResourceName = "Max.Selling.WebService.ActionResults.StubResults.xml";
        private static string xml;

        static StubXmlResult()
        {
            using (TextReader reader = new StreamReader(typeof(StubXmlResult).Assembly.GetManifestResourceStream(XmlResourceName)))
                xml = reader.ReadToEnd();
        }

        public override void ExecuteResult(ControllerContext context)
        {
            context.HttpContext.Response.ContentType = "text/xml";
            context.HttpContext.Response.ContentEncoding = Encoding.UTF8;
            context.HttpContext.Response.Write(xml);
        }
    }
}
