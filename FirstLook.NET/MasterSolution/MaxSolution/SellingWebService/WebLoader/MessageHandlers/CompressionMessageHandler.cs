﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Max.Selling.WebService.WebLoader.MessageHandlers
{
    public class CompressionMessageHandler : DelegatingHandler
    {
        public CompressionMessageHandler()
        {
        }

        public CompressionMessageHandler(HttpMessageHandler innerHandler)
            : base(innerHandler)
        {
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var inner = base.SendAsync(request, cancellationToken);
            return inner.ContinueWith(t =>
                                          {
                                              var response = t.Result;
                                              if (!IsContentCompressable(response.Content))
                                                  return response;

                                              var compressionType = DetermineCompressionTypeFromRequest(request);
                                              if (compressionType == CompressionType.None)
                                                  return response;

                                              response.Content = new CompressedContent(response.Content, compressionType);

                                              return response;
                                          }
                );
        }

        private static bool IsContentCompressable(HttpContent content)
        {
            if(content == null)
                return false;
            
            var contentType = content.Headers.ContentType;
            if(contentType == null)
                return false;

            var mediaType = contentType.MediaType;
            return mediaType.StartsWith("text/", StringComparison.InvariantCultureIgnoreCase)
                   || mediaType.EndsWith("/json", StringComparison.InvariantCultureIgnoreCase)
                   || mediaType.EndsWith("/xml", StringComparison.InvariantCultureIgnoreCase);
        }

        private static CompressionType DetermineCompressionTypeFromRequest(HttpRequestMessage request)
        {
            return request.Headers.AcceptEncoding
                .OrderByDescending(ae => ae.Quality)
                .Select(ae => ParseEncoding(ae.Value))
                .FirstOrDefault(ct => ct != CompressionType.None);
        }

        private static CompressionType ParseEncoding(string value)
        {
            var cmp = StringComparer.InvariantCultureIgnoreCase;
            if(cmp.Equals(value, "gzip"))
                return CompressionType.Gzip;
            if(cmp.Equals(value, "deflate"))
                return CompressionType.Deflate;
            return CompressionType.None;
        }

        private enum CompressionType
        {
            None = 0,
            Gzip,
            Deflate
        }

        private class CompressedContent : HttpContent
        {
            private readonly HttpContent _innerContent;
            private readonly CompressionType _compressionType;

            public CompressedContent(HttpContent innerContent, CompressionType compressionType)
            {
                _innerContent = innerContent;
                _compressionType = compressionType;

                CopyHeaders();

                Headers.ContentEncoding.Add(_compressionType.ToString().ToLowerInvariant());
            }

            private void CopyHeaders()
            {
                foreach (var header in _innerContent.Headers)
                    Headers.TryAddWithoutValidation(header.Key, header.Value);
            }

            protected override Task SerializeToStreamAsync(Stream output, TransportContext context)
            {
                var outer = GetCompressionStreamDecorator(_compressionType, output);
                return _innerContent.CopyToAsync(outer)
                    .ContinueWith(t =>
                                      {
                                          outer.Dispose();
                                          if (t.IsFaulted)
                                              throw new IOException("Failed to compress inner stream.", t.Exception);
                                      });
            }

            private static Stream GetCompressionStreamDecorator(CompressionType compressionType, Stream output)
            {
                switch(compressionType)
                {
                    case CompressionType.Gzip:
                        return new GZipStream(output, CompressionMode.Compress, true);
                    case CompressionType.Deflate:
                        return new DeflateStream(output, CompressionMode.Compress, true);
                    default:
                        throw new InvalidOperationException();
                }
            }

            protected override bool TryComputeLength(out long length)
            {
                length = -1;
                return false;
            }
        }
    }
}