﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FirstLook.Common.Core.Extensions;
using FirstLook.Common.Core.Logging;
using FirstLook.Merchandising.DomainModel.Core.Authentication;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using MvcMiniProfiler;
using ILog = FirstLook.Common.Core.Logging.ILog;

namespace Max.Selling.WebService.WebLoader.MessageHandlers
{
    public class BasicAuthenticationMessageHandler : DelegatingHandler
    {
        private readonly IUserServices _userServices;
        private static readonly ILog Log = LoggerFactory.GetLogger<BasicAuthenticationMessageHandler>();
        
        public BasicAuthenticationMessageHandler(IUserServices userServices)
        {
            _userServices = userServices;
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            MiniProfiler.Start();

            Log.Debug("Begin SendAsync Authentication message");

            var credential = ParseAuthorizationHeader(request.Headers.Authorization);
			Log.DebugFormat("Credentials found: {0}", credential.Username); // don't log the password
            var isAuthorized = _userServices.AuthenticateAndSetCurrent(credential);

            Log.DebugFormat("is authorized returned: {0}", isAuthorized.ToJson());

            if(!isAuthorized || _userServices.Current == null || !_userServices.Current.IsInRole("MiniProfiler"))
                MiniProfiler.Stop(true);

            return isAuthorized 
                ? base.SendAsync(request, cancellationToken) 
                : CreateUnauthorizedResponse();
        }

        private static BasicCredential ParseAuthorizationHeader(AuthenticationHeaderValue authorization)
        {
			var badCredentials = new EmptyCredentials();

            Log.DebugFormat("Parse auth headers: {0}", authorization.ToJson());
            if (authorization == null
                || !StringComparer.InvariantCultureIgnoreCase.Equals(authorization.Scheme, "Basic"))
            {
                Log.DebugFormat("No auth or failed auth scheme. Return null.");
                return badCredentials;
            }

            var bytes = ConvertBase64ToBytes(authorization);
            if (bytes == null) return badCredentials;

            var str = Encoding.ASCII.GetString(bytes);
            
            Log.DebugFormat("GetString from bytes: {0}", str);

            var colonPos = str.IndexOf(':');
            if (colonPos < 0)
                return badCredentials;

            var username = str.Substring(0, colonPos);
            var password = str.Substring(colonPos + 1);

            if (username.Length <= 0)
                return badCredentials;

            Log.DebugFormat("U:{0}", username); // don't log the password
            // Should we do some data validation on username? Maybe a regex?

            return new BasicCredential(username, password);
        }

        private static byte[] ConvertBase64ToBytes(AuthenticationHeaderValue authorization)
        {
            try
            {
                return Convert.FromBase64String(authorization.Parameter);
            }
            catch (FormatException)
            {
                return null;
            }
        }

        public Task<HttpResponseMessage> CreateUnauthorizedResponse()
        {
            var responseMsg = new HttpResponseMessage(HttpStatusCode.Unauthorized);
            responseMsg.Headers.Add("WWW-Authenticate", "Basic realm=\"Mobile Web Loader\"");

            Log.Warn("Creating the Unauthorized response message.");

            var tcs = new TaskCompletionSource<HttpResponseMessage>();
            tcs.SetResult(responseMsg);
            return tcs.Task;
        }

	    public class EmptyCredentials : BasicCredential
	    {
			public EmptyCredentials() : base (string.Empty, string.Empty)
			{}
	    }
    }
}