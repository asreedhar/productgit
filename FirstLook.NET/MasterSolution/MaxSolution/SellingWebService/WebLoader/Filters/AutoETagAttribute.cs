﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Max.Selling.WebService.WebLoader.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class AutoETagAttribute : FilterAttribute, IActionFilter
    {
        public Task<HttpResponseMessage> ExecuteActionFilterAsync(HttpActionContext actionContext, CancellationToken cancellationToken, 
            Func<Task<HttpResponseMessage>> continuation)
        {
            return continuation()
                .ContinueWith(
                    msgTask =>
                        {
                            var tcs = new TaskCompletionSource<Context>();

                            var response = msgTask.Result;
                            if (response == null || (int) response.StatusCode < 200 || (int) response.StatusCode >= 300)
                            {
                                tcs.SetResult(new Context(response, null));
                            }
                            else
                            {
                                ETagHelper
                                    .CalculateSignature(response.Content)
                                    .ContinueWith(t => tcs.SetResult(new Context(response, t.Result)),
                                                  TaskContinuationOptions.ExecuteSynchronously);
                            }

                            return tcs.Task;
                        })
                .Unwrap()
                .ContinueWith(
                    contextTask =>
                        {
                            var context = contextTask.Result;
                            var response = context.Response;
                            var request = actionContext.Request;

                            if (context.Sig == null)
                                return response;

                            ETagHelper.SetETagHeader(response, context.Sig);

                            return ETagHelper.IfNoneMatch(context.Sig, request) 
                                ? response 
                                : NotModifiedResponse(context.Sig, response, request);
                        }, TaskContinuationOptions.ExecuteSynchronously);
        }

        private static HttpResponseMessage NotModifiedResponse(string sig, HttpResponseMessage originalResponse,
                                                               HttpRequestMessage originalRequest)
        {
            var content = new ByteArrayContent(new byte[0]);
            content.Headers.ContentType = originalResponse.Content.Headers.ContentType;

            var response = originalRequest.CreateResponse(HttpStatusCode.NotModified);
            ETagHelper.SetETagHeader(response, sig);
            response.Headers.CacheControl = new CacheControlHeaderValue {Private = true};
            response.Content = content;

            return response;
        }

        private class Context
        {
            private readonly HttpResponseMessage _response;
            private readonly string _sig;

            public Context(HttpResponseMessage response, string sig)
            {
                _response = response;
                _sig = sig;
            }

            public HttpResponseMessage Response
            {
                get { return _response; }
            }

            public string Sig
            {
                get { return _sig; }
            }
        }
    }
}