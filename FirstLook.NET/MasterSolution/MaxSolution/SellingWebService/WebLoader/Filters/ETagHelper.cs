using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web.Http;

namespace Max.Selling.WebService.WebLoader.Filters
{
    public static class ETagHelper
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static Task<string> CalculateSignature(HttpContent content)
        {
            return content.ReadAsByteArrayAsync()
                .ContinueWith(t =>
                                  {
                                      using (var hashAlg = MD5.Create())
                                      {
                                          return "\"" + Convert.ToBase64String(hashAlg.ComputeHash(t.Result)) + "\"";
                                      }
                                  }
                );
        }

        public static Task<string> CalculateSignature<T>(T obj)
        {
            var content = new ObjectContent(typeof (T), obj, new JsonMediaTypeFormatter());
            return CalculateSignature((HttpContent)content);
        }

        public static bool IfNoneMatch(string sig, HttpRequestMessage requestMsg)
        {
            var cmp = StringComparer.Ordinal;
            var etagsToCheck = requestMsg.Headers.IfNoneMatch;
            return etagsToCheck == null || 
                !etagsToCheck.Any(t => !t.IsWeak && 
                    (cmp.Equals(sig, t.Tag) || cmp.Equals("\"*\"", t.Tag)));
        }

        public static bool IfMatch(string sig, HttpRequestMessage requestMsg, bool ifMatchHeaderIsRequired = false)
        {
            var etagsToCheck = requestMsg.Headers.Contains("If-Match") ? requestMsg.Headers.IfMatch : null;
            if(etagsToCheck == null)
            {
                if (ifMatchHeaderIsRequired)
                {
                    Log.Error("Missing If-Match Etag");
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                                                        {
                                                            ReasonPhrase = "If-Match header is required"
                                                        });
                }
                return true;
            }

            var cmp = StringComparer.Ordinal;
            return etagsToCheck.Any(t => !t.IsWeak && (cmp.Equals(t.Tag, "\"*\"") || cmp.Equals(t.Tag, sig)));
        }

        public static void SetETagHeader(HttpResponseMessage response, string sig)
        {
            var etag = new EntityTagHeaderValue(sig, false);
            response.Headers.ETag = etag;
            response.Headers.CacheControl = new CacheControlHeaderValue { Private = true };
        }
    }
}