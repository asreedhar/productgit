﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.Core.Authentication;
using FirstLook.Merchandising.DomainModel.Core.Authorization;

namespace Max.Selling.WebService.WebLoader.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public class AuthorizeDealerAttribute : FilterAttribute, IAuthorizationFilter
    {
        private readonly IAuthorizeDealer _authorizeDealer;
        private readonly ICurrentPrincipal _currentPrincipal;

        public AuthorizeDealerAttribute(IAuthorizeDealer authorizeDealer, ICurrentPrincipal currentPrincipal)
        {
            _authorizeDealer = authorizeDealer;
            _currentPrincipal = currentPrincipal;
        }

        public AuthorizeDealerAttribute()
            : this(Registry.Resolve<IAuthorizeDealer>(), Registry.Resolve<ICurrentPrincipal>())
        {
        }

        private string _dealerParameter = "dealer";
        /// <summary>
        /// The name of the route parameter that contains the dealer code.
        /// </summary>
        public string DealerParameter
        {
            get { return _dealerParameter; }
            set
            {
                if(string.IsNullOrEmpty(value))
                    throw new ArgumentNullException("value");
                _dealerParameter = value;
            }
        }

        private DealerUpgradeList _assertDealerUpgrades = DealerUpgradeList.Empty;
        public string AssertDealerUpgrades
        {
            get { return _assertDealerUpgrades.Original; }
            set { _assertDealerUpgrades = new DealerUpgradeList(value); }
        }

        public bool AssertDealerHasWebLoaderUpgrade { get; set; }

        public Task<HttpResponseMessage> ExecuteAuthorizationFilterAsync(HttpActionContext actionContext, CancellationToken cancellationToken, 
            Func<Task<HttpResponseMessage>> continuation)
        {
            var dealerCode = GetDealerCodeFromRoute(actionContext, DealerParameter);
            var principal = _currentPrincipal.Current;

            var result = _authorizeDealer.IsAuthorized(principal, dealerCode, _assertDealerUpgrades.List, AssertDealerHasWebLoaderUpgrade);

            if (result.IsAuthorized)
                return continuation();
            
            if (result.NotFound)
                return CreateErrorResponse(actionContext, result, HttpStatusCode.NotFound);
            
            return CreateErrorResponse(actionContext, result, HttpStatusCode.Forbidden);
        }

        private static string GetDealerCodeFromRoute(HttpActionContext actionContext, string dealerCodeRouteParameter)
        {
            object dealerCode;
            actionContext.ControllerContext.RouteData.Values.TryGetValue(dealerCodeRouteParameter, out dealerCode);
            return dealerCode != null ? dealerCode.ToString() : null;
        }

        private static Task<HttpResponseMessage> CreateHttpForbiddenResponse(HttpActionContext actionContext, AuthorizationResult authMsg)
        {
            var tcs = new TaskCompletionSource<HttpResponseMessage>();
            tcs.SetResult(actionContext.Request.CreateResponse(HttpStatusCode.Forbidden, authMsg,
                                                               actionContext.ControllerContext.Configuration));
            return tcs.Task;
        }

        private static Task<HttpResponseMessage> CreateErrorResponse(HttpActionContext actionContext, AuthorizationResult authMsg, HttpStatusCode httpStatusCode)
        {
            var tcs = new TaskCompletionSource<HttpResponseMessage>();
            var response = actionContext.Request.CreateResponse(httpStatusCode, authMsg,
                                                                actionContext.ControllerContext.Configuration);
            response.ReasonPhrase = authMsg.Message;
            tcs.SetResult(response);
            return tcs.Task;
        }
    }
}