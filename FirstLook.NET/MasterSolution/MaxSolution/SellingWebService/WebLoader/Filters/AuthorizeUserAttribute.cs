﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.Core.Authentication;
using FirstLook.Merchandising.DomainModel.Core.Authorization;

namespace Max.Selling.WebService.WebLoader.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public class AuthorizeUserAttribute : FilterAttribute, IAuthorizationFilter
    {
        private readonly IAuthorizeUser _authorizeUser;
        private readonly ICurrentPrincipal _currentPrincipal;

        public AuthorizeUserAttribute(IAuthorizeUser authorizeUser, ICurrentPrincipal currentPrincipal)
        {
            _authorizeUser = authorizeUser;
            _currentPrincipal = currentPrincipal;
        }

        public AuthorizeUserAttribute()
            : this(Registry.Resolve<IAuthorizeUser>(), Registry.Resolve<ICurrentPrincipal>())
        {
        }

        public string AssertAllRoles
        {
            get { return _assertAllRoles.Original; }
            set { _assertAllRoles = new RoleList(value); }
        }
        private RoleList _assertAllRoles = RoleList.Empty;

        public string AssertAnyRoles
        {
            get { return _assertAnyRoles.Original; }
            set { _assertAnyRoles = new RoleList(value); }
        }
        private RoleList _assertAnyRoles = RoleList.Empty;
        
        public Task<HttpResponseMessage> ExecuteAuthorizationFilterAsync(
            HttpActionContext actionContext, CancellationToken cancellationToken, Func<Task<HttpResponseMessage>> continuation)
        {
            var principal = _currentPrincipal.Current;
            var authMsg = _authorizeUser.IsAuthorized(principal, _assertAllRoles.List, _assertAnyRoles.List);
            
            return authMsg.IsAuthorized 
                ? continuation() 
                : CreateHttpForbiddenResponse(actionContext, authMsg);
        }

        private static Task<HttpResponseMessage> CreateHttpForbiddenResponse(HttpActionContext actionContext, AuthorizationResult authMsg)
        {
            var tcs = new TaskCompletionSource<HttpResponseMessage>();
            tcs.SetResult(actionContext.Request.CreateResponse(HttpStatusCode.Forbidden, authMsg,
                                                               actionContext.ControllerContext.Configuration));
            return tcs.Task;
        }
    }
}