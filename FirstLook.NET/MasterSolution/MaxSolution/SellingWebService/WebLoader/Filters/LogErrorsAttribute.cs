﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Core.Logging;
using FirstLook.Merchandising.DomainModel.Core.Authorization;

namespace Max.Selling.WebService.WebLoader.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class LogErrorsAttribute : FilterAttribute, IExceptionFilter
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<LogErrorsAttribute>();

        private static readonly Task Completed = GetCompletedVoidTask();

        private readonly IUserServices _userServices;

        public LogErrorsAttribute(IUserServices services)
        {
            _userServices = services;
        }

        public LogErrorsAttribute()
            : this(Registry.Resolve<IUserServices>())
        {
        }

        private static Task GetCompletedVoidTask()
        {
            var tcs = new TaskCompletionSource<int>();
            tcs.SetResult(0);
            return tcs.Task;
        }

        public Task ExecuteExceptionFilterAsync(HttpActionExecutedContext context, CancellationToken cancellationToken)
        {
            if(!Log.IsErrorEnabled)
                return Completed;

            var b = new StringBuilder();
            Exception exception = null;

            b.Append("Unhandled Exception in WebAPI stack.");
            var user = _userServices.Current;
            b.AppendFormat("\r\n    Identity: {0}", user != null ? user.UserName : "No Current User");
            if(context != null)
            {
                exception = context.Exception;
                if(context.Request != null)
                {
                    b.AppendFormat("\r\n    Request Uri: {0}", context.Request.RequestUri);
                    b.AppendFormat("\r\n    Request Method: {0}", context.Request.Method);
                    b.AppendFormat("\r\n    Request Headers:");
                    var headers = GetCombinedHeaders(context.Request).OrderBy(kvp => kvp.Key).ToList();
                    foreach(var header in headers)
                    {
                        if(StringComparer.InvariantCultureIgnoreCase.Equals("Authorization", header.Key))
                        {
                            b.AppendFormat("\r\n        Authorization: redacted.");
                        }
                        else
                        {
                            b.AppendFormat("\r\n        {0}: {1}", header.Key, string.Join(", ", header.Value));
                        }
                    }
                }
            }

            if (exception != null)
                Log.Error(b.ToString(), exception);
            else
                Log.Error(b.ToString());

            return Completed;
        }

        private IEnumerable<KeyValuePair<string, IEnumerable<string>>> GetCombinedHeaders(HttpRequestMessage request)
        {
            if (request.Headers != null)
                foreach (var header in request.Headers)
                    yield return header;

            if (request.Content != null && request.Content.Headers != null)
                foreach (var header in request.Content.Headers)
                    yield return header;
        }
    }
}