namespace Max.Selling.WebService.WebLoader.Model
{
    public interface IVehicleDataLoader
    {
        VehicleData Load(string dealerCode, int inventoryId);
    }
}