﻿namespace Max.Selling.WebService.WebLoader.Model
{
    public class VehicleRepository : IVehicleRepository
    {
        private readonly IVehicleDataLoader _vehicleDataLoader;
        private IVehicleUpdater _vehicleUpdater;

        public VehicleRepository(IVehicleDataLoader vehicleDataLoader, IVehicleUpdater vehicleUpdater)
        {
            _vehicleDataLoader = vehicleDataLoader;
            _vehicleUpdater = vehicleUpdater;
        }

        public IVehicle GetVehicle(string dealerCode, int inventoryId)
        {
            return new Vehicle(dealerCode, inventoryId, _vehicleDataLoader, _vehicleUpdater);
        }
    }
}