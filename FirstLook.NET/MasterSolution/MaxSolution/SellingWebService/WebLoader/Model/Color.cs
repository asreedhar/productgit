using System.Web;

namespace Max.Selling.WebService.WebLoader.Model
{
    public class Color
    {
        public string Code { get; private set; }
        public string Desc { get; private set; }

        public static Color ParseColor1(string colorString)
        {
            var vals = HttpUtility.ParseQueryString(colorString);
            var color = new Color {Code = vals["c"], Desc = vals["d"]};
            return color;
        }

        public static Color ParseColor2(string colorString)
        {
            var vals = HttpUtility.ParseQueryString(colorString);
            if (vals["c2"] == null) return null;
            var color2 = new Color {Code = vals["c2"], Desc = vals["d2"]};
            return color2;
        }

    }
}