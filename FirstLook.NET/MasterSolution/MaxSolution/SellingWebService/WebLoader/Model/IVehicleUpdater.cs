using System;

namespace Max.Selling.WebService.WebLoader.Model
{
    public interface IVehicleUpdater
    {
        void Update(string dealerCode, int inventoryId, ViewModels.Vehicle vehicleModel, Func<string, bool> isPropertyDefined);
    }
}