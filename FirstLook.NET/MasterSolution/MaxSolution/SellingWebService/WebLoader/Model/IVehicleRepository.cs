﻿namespace Max.Selling.WebService.WebLoader.Model
{
    public interface IVehicleRepository
    {
        IVehicle GetVehicle(string businessUnitCode, int inventoryId);
    }
}