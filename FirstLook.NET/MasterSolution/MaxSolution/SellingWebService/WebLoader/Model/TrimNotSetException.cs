﻿using System;

namespace Max.Selling.WebService.WebLoader.Model
{
    public class TrimNotSetException : Exception
    {
        public TrimNotSetException(string message):base(message)
        {
            
        }
    }
}