﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Chrome;
using Max.Selling.WebService.WebLoader.ViewModels;

namespace Max.Selling.WebService.WebLoader.Model
{
    public class Vehicle : IVehicle
    {
        private readonly IVehicleDataLoader _loader;
        private readonly IVehicleUpdater _updater;

        private Lazy<VehicleData> _data;
        private readonly string _dealerCode;
        private readonly int _inventoryId;

        public Vehicle(string dealerCode, int inventoryId, IVehicleDataLoader loader, IVehicleUpdater updater)
        {
            _dealerCode = dealerCode;
            _inventoryId = inventoryId;
            _loader = loader;
            _updater = updater;
            ResetCache();
        }

        public VehicleData Data
        {
            get { return _data.Value; }
        }

        public void ResetCache()
        {
            _data = new Lazy<VehicleData>(() => _loader.Load(_dealerCode, _inventoryId));
        }

        public ViewModels.Vehicle ToViewModel()
        {
            var data = Data;

            return new ViewModels.Vehicle
            {
                Id = data.InventoryData.InventoryID,
                Vin = data.InventoryData.VIN,
                StockNum = data.InventoryData.StockNumber,
                Make = data.InventoryData.Make,
                Model = data.InventoryData.Model,
                Year = TryParseYear(data.InventoryData.Year),
                Trim = Ref.FromStyle(data.StyleId, data.Style),
                AvailableTrimIds = GetTrimIds(data.VinPattern),
                EquipmentCompleted = !(data.InventoryData.NeedsInterior || data.InventoryData.NeedsExterior),
                PhotosCompleted = !data.InventoryData.NeedsPhotos,
                Engine = data.GetEquipmentRef(e => e.Engine),
                Transmission = data.GetEquipmentRef(e => e.Transmission),
                Drivetrain = data.GetEquipmentRef(e => e.Drivetrain),
                ExtColor = data.GetExtColorRef(),
                IntColor = data.GetIntColorRef(),
                Equipment = data.GetSelectedEquipment(),
                Options = data.GetSelectedOptions()
            };
        }

        private static List<string> GetTrimIds(IVinPattern vinPattern)
        {
            return vinPattern.ChromeStyleIds
                .Select(styleId => "1_" + styleId.ToString(CultureInfo.InvariantCulture))
                .ToList();
        }

        private static int TryParseYear(string year)
        {
            int y;
            int.TryParse(year, out y);
            return y;
        }

        public void Update(ViewModels.Vehicle vehicleModel, Func<string, bool> isPropertyDefined)
        {
            ResetCache();
            _updater.Update(_dealerCode, _inventoryId, vehicleModel, isPropertyDefined);
        }
    }
}