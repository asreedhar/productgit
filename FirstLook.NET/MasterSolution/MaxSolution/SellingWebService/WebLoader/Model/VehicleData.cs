﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Chrome;
using FirstLook.Merchandising.DomainModel.Chrome.Model;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.VehicleVinData;
using FirstLook.Merchandising.DomainModel.Vehicles;
using Max.Selling.WebService.WebLoader.ViewModels;

namespace Max.Selling.WebService.WebLoader.Model
{
    public class VehicleData
    {
        public VehicleData(IInventoryData inventoryData,
            PrimaryEquipmentConfiguration primaryEquipment, PrimaryEquipmentConfiguration standardEquipment,
            DetailedEquipmentCollection detailedEquipment, GenericEquipmentCollection selectedEquipment,
            IVinPattern vinPattern, int? styleId, Style style, SharedChromeData chromeData)
        {
            InventoryData = inventoryData;
            PrimaryEquipment = primaryEquipment;
            StandardPrimaryEquipment = standardEquipment;
            DetailedEquipment = detailedEquipment;
            VinPattern = vinPattern;
            StyleId = styleId;
            Style = style;
            SharedChromeData = chromeData;
            SelectedEquipment = selectedEquipment;
        }

        public IInventoryData InventoryData { get; private set; }
        private PrimaryEquipmentConfiguration PrimaryEquipment { get; set; }
        private PrimaryEquipmentConfiguration StandardPrimaryEquipment { get; set; }
        private DetailedEquipmentCollection DetailedEquipment { get; set; }
        private GenericEquipmentCollection SelectedEquipment { get; set; }

        public IVinPattern VinPattern { get; private set; }
        public int? StyleId { get; private set; }
        public Style Style { get; private set; }
        private SharedChromeData SharedChromeData { get; set; }

        public Ref GetEquipmentRef(Func<PrimaryEquipmentConfiguration, EquipmentObject> getEquipmentProp)
        {
            var primaryEquipmentObject = PrimaryEquipment == null ? null : getEquipmentProp(PrimaryEquipment);
            var standardEquipmentObject = StandardPrimaryEquipment == null ? null : getEquipmentProp(StandardPrimaryEquipment);

            var primaryEquipment = primaryEquipmentObject == null || primaryEquipmentObject.TypeID == -1
                                       ? (int?)null
                                       : primaryEquipmentObject.TypeID;
            var standardEquipment = standardEquipmentObject == null || standardEquipmentObject.TypeID == -1
                                        ? (int?)null
                                        : standardEquipmentObject.TypeID;

            var equipment = primaryEquipment ?? standardEquipment;
            if (equipment == null)
                return null;

            Category cat;
            var desc = SharedChromeData.Categories.TryGetValue(equipment.Value, out cat)
                           ? cat.UserFriendlyName
                           : "No Description " + equipment.Value;

            return new Ref
            {
                Id = equipment.Value.ToString(CultureInfo.InvariantCulture),
                Desc = desc
            };
        }

        public ExtColorRef GetExtColorRef()
        {
            var color = Style == null
                            ? null
                            : Style
                                  .GetExteriorColors()
                                  .FirstOrDefault(InventoryData.Ext1Code, InventoryData.Ext1Desc,
                                                  InventoryData.Ext2Code, InventoryData.Ext2Desc);
            if (color != null)
            {
                return new ColorHelper(color).ToExtRef();
            }

            if (!string.IsNullOrEmpty(InventoryData.Ext1Desc) || !string.IsNullOrEmpty(InventoryData.Ext2Desc))
            {
                return new ColorHelper(InventoryData.Ext1Code, InventoryData.Ext1Desc,
                                          InventoryData.Ext2Code, InventoryData.Ext2Desc).ToExtRef();
            }

            return null;
        }

        public Ref GetIntColorRef()
        {
            var color = Style == null
                            ? null
                            : Style
                                  .GetInteriorColors()
                                  .FirstOrDefault(InventoryData.IntCode, InventoryData.IntDesc);
            if (color != null)
            {
                return new ColorHelper(color).ToIntRef();
            }

            if (!string.IsNullOrEmpty(InventoryData.IntDesc))
            {
                return new ColorHelper(InventoryData.IntCode, InventoryData.IntDesc).ToIntRef();
            }

            return null;
        }

        public List<Ref> GetSelectedEquipment()
        {
            var primaryEquipmentCategoryIds = GetPrimaryEquipmentCategoryIds();
            return SelectedEquipment
                .Cast<GenericEquipment>()
                .Where(ge => !primaryEquipmentCategoryIds.Contains(ge.Category.CategoryID))
                .Select(ge =>
                        new Ref
                        {
                            Id = ge.Category.CategoryID.ToString(CultureInfo.InvariantCulture),
                            Desc = LookupCategoryUserFriendlyName(ge.Category.CategoryID)
                        })
                .ToList();
        }

        private HashSet<int> GetPrimaryEquipmentCategoryIds()
        {
            var cmp = StringComparer.InvariantCultureIgnoreCase;
            var categoryHeaderIds = new HashSet<int>(
                SharedChromeData.CategoryHeaders.Values
                    .Where(ch => cmp.Equals(ch._CategoryHeader, "Engine") 
                        || cmp.Equals(ch._CategoryHeader, "Transmission") 
                        || cmp.Equals(ch._CategoryHeader, "Drivetrain")
                        || cmp.Equals(ch._CategoryHeader, "Fuel"))
                    .Select(ch => ch.CategoryHeaderID));

            return new HashSet<int>(
                SharedChromeData.Categories.Values
                    .Where(c => categoryHeaderIds.Contains(c.CategoryHeaderID.GetValueOrDefault()))
                    .Select(c => c.CategoryID));
        }

        private string LookupCategoryUserFriendlyName(int id)
        {
            Category category;
            string name = null;
            if (SharedChromeData.Categories.TryGetValue(id, out category))
                name = category.UserFriendlyName;
            if (string.IsNullOrEmpty(name))
                name = "Category " + id.ToString(CultureInfo.InvariantCulture);
            return name;
        }

        public List<OptionRef> GetSelectedOptions()
        {
            return DetailedEquipment
                .Cast<DetailedEquipment>()
                .Select(de => new OptionRef
                {
                    Id = de.OptionCode,
                    Code = de.OptionCode,
                    Desc = string.IsNullOrEmpty(de.OptionText)
                               ? string.Format("Option {0}", de.OptionCode)
                               : de.OptionText,
                    DetailedDesc = TrimOptionTextPrefixIfNecessary(de)
                })
                .ToList();
        }

        private static string TrimOptionTextPrefixIfNecessary(DetailedEquipment de)
        {
            var desc = (de.OptionText ?? "").Trim();
            var detailedDesc = (de.Description ?? "").Trim();
            if (detailedDesc.StartsWith(desc + ": "))
                detailedDesc = detailedDesc.Substring(desc.Length + 2);

            detailedDesc = detailedDesc.Trim();

            return detailedDesc.Length <= 0
                       ? null
                       : detailedDesc;
        }
    }
}