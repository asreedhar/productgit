﻿using System;

namespace Max.Selling.WebService.WebLoader.Model
{
    public interface IVehicle
    {
        ViewModels.Vehicle ToViewModel();
        void Update(ViewModels.Vehicle vehicleModel, Func<string, bool> isPropertyDefined);
    }
}