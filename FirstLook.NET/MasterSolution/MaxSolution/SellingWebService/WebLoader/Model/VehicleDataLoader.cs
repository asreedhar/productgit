﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;
using FirstLook.Merchandising.DomainModel.Chrome;
using FirstLook.Merchandising.DomainModel.Chrome.Mapper;
using FirstLook.Merchandising.DomainModel.Chrome.Model;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using FirstLook.Merchandising.DomainModel.DataAccess;
using FirstLook.Merchandising.DomainModel.VehicleVinData;
using FirstLook.Merchandising.DomainModel.Vehicles;
using System.Linq;
using MvcMiniProfiler;

namespace Max.Selling.WebService.WebLoader.Model
{
    internal class VehicleDataLoader : IVehicleDataLoader
    {
        private readonly IUserServices _userServices;
        private readonly IDealerServices _dealerServices;
        private readonly ISharedChromeDataRepository _sharedChromeDataRepository;
        private readonly IChromeMapper _chromeMapper;
        private readonly IVinPatternRepository _vinPatternRepository;
        private readonly IStyleRepository _styleRepository;
        private readonly IInventoryDataAccess _inventoryDataAccess;

        public VehicleDataLoader(
            IUserServices userServices,
            IDealerServices dealerServices,
            ISharedChromeDataRepository sharedChromeDataRepository,
            IChromeMapper chromeMapper,
            IVinPatternRepository vinPatternRepository,
            IStyleRepository styleRepository,
            IInventoryDataAccess inventoryDataAccess)
        {
            _userServices = userServices;
            _dealerServices = dealerServices;
            _sharedChromeDataRepository = sharedChromeDataRepository;
            _chromeMapper = chromeMapper;
            _vinPatternRepository = vinPatternRepository;
            _styleRepository = styleRepository;
            _inventoryDataAccess = inventoryDataAccess;
        }

        public VehicleData Load(string dealerCode, int inventoryId)
        {
            var dealer = _dealerServices.GetDealerByCode(dealerCode);
            var user = _userServices.Current;

            IInventoryData inventoryData;
            PrimaryEquipmentConfiguration primaryEquipment;
            GenericEquipmentCollection genericEquipment;
            DetailedEquipmentCollection detailedEquipment;
            try
            {
                inventoryData = _inventoryDataAccess.Fetch(dealer.Id, inventoryId);

                using (MiniProfiler.Current.Step("Vehicle Load: FetchOrCreateDetailed"))
                {
                    VehicleConfiguration.FetchOrCreateDetailed(dealer.Id, inventoryId, user.UserName);
                }

                // TODO: Maybe remove this call if we can get generics populated reliably another way
                primaryEquipment = PrimaryEquipmentConfiguration.FetchVehicleConfig(dealer.Id, inventoryId)
                                   ?? PrimaryEquipmentConfiguration.Default;

                genericEquipment = GenericEquipmentCollection.FetchSelections(dealer.Id, inventoryId);
                detailedEquipment = DetailedEquipmentCollection.FetchSelections(dealer.Id, inventoryId);
            }
            catch (InventoryException ex)
            {
                if (Regex.IsMatch(ex.Message, @"Inventory\s+Item\s+not\s+found\."))
                    ThrowVehicleNotFound();

                throw;
            }
            catch (SqlException ex)
            {
                if (Regex.IsMatch(ex.Message, @"Not\s+an\s+InventoryId"))
                    ThrowVehicleNotFound();

                throw;
            }

            var sharedChromeData = _sharedChromeDataRepository.GetData();

            var vinPatternId = inventoryData == null
                                   ? null
                                   : _chromeMapper.LookupVinPatternIdByVin(inventoryData.VIN);
            var vinPattern = vinPatternId.HasValue
                                 ? _vinPatternRepository.GetVinPatternById(vinPatternId.Value)
                                 : null;

            var styleId = GetStyleId(inventoryData, vinPattern != null ? vinPattern.ChromeStyleIds : new int[]{});

            var style = styleId.HasValue
                            ? _styleRepository.GetStyleById(styleId.Value)
                            : null;

            var standardEquipment = style == null ? null : style.GetPrimaryEquipmentConfiguration(sharedChromeData);

            return new VehicleData(inventoryData, primaryEquipment, standardEquipment, detailedEquipment,
                                   genericEquipment, vinPattern, styleId, style, sharedChromeData);
        }

        private static void ThrowVehicleNotFound()
        {
            throw new HttpResponseException(
                new HttpResponseMessage(HttpStatusCode.NotFound) { ReasonPhrase = "Vehicle Not Found" });
        }

        private static int? GetStyleId(IInventoryData data, IEnumerable<int> availableChromeStyles)
        {
            int? chromeStyleId =  data.ChromeStyleId.GetValueOrDefault(-1) == -1
                       ? null
                       : data.ChromeStyleId;

            if(!chromeStyleId.HasValue)
                return chromeStyleId;

            //the selected chrome has to be one of the available ones
            if (availableChromeStyles.Contains(chromeStyleId.Value))
                return chromeStyleId;

            return null;
        }
    }
}