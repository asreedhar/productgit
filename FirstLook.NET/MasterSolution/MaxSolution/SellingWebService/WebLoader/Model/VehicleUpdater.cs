﻿using System;
using System.Linq;
using System.Web;
using FirstLook.LotIntegration.DomainModel.Tasks;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using FirstLook.Merchandising.DomainModel.DataAccess;
using FirstLook.Merchandising.DomainModel.DataSource;
using FirstLook.Merchandising.DomainModel.VehicleVinData;
using FirstLook.Merchandising.DomainModel.Vehicles;
using Max.Selling.WebService.WebLoader.ViewModels;
using Merchandising.Messages;
using Merchandising.Messages.AutoApprove;
using System.Collections.Generic;

namespace Max.Selling.WebService.WebLoader.Model
{
    //@TODO for all the cases check if the trim is set or not.  Throw an exception if trim is not set and the user is trying to add option/set color etc.
    public class VehicleUpdater : IVehicleUpdater
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IQueueFactory _queueFactory;
        private readonly IAdMessageSender _adMessageSender;
        private readonly IUserServices _userServices;
        private readonly IDealerServices _dealerServices;
        private readonly IInventoryDataAccessCache _inventoryDataAccessCache;

        public VehicleUpdater (IQueueFactory queueFactory, IAdMessageSender adMessageSender, 
            IUserServices userServices, IDealerServices dealerServices, IInventoryDataAccessCache inventoryDataAccessCache)
        {
            _queueFactory = queueFactory;
            _adMessageSender = adMessageSender;
            _userServices = userServices;
            _dealerServices = dealerServices;
            _inventoryDataAccessCache = inventoryDataAccessCache;
        }

        public void Update(string dealerCode, int inventoryId, ViewModels.Vehicle vehicleModel, Func<string, bool> isPropertyDefined)
        {
            Log.DebugFormat("Saving Data, Dealer: {0}, Inv: {1}", dealerCode, inventoryId);

            var dealer = _dealerServices.GetDealerByCode(dealerCode);
            var user = _userServices.Current;

            var context = new Context(this, dealer, user, vehicleModel, inventoryId, isPropertyDefined);

            context.InvalidateCache();
            
            context.SaveTrim();
            context.SaveEquipmentCompleted();
            context.SavePhotosCompleted();
            context.SaveIntColor();
            context.SaveExtColor();
            context.SaveEngine();
            context.SaveDriveTrain();
            context.SaveTransmission();
            context.SaveOptions();
            context.SaveEquipment();
        }

        private class Context
        {
            private VehicleConfiguration _vehicleConfiguration;
            private bool _trimSetForFirstTime;

            private readonly VehicleUpdater _parent;
            private readonly IDealer _dealer;
            private readonly IUser _user;
            private readonly ViewModels.Vehicle _vehicleModel;
            private readonly int _inventoryId;
            private readonly Func<string, bool> _isPropertyDefined;

            public Context(VehicleUpdater parent, IDealer dealer, IUser user, ViewModels.Vehicle vehicleModel, int inventoryId, Func<string, bool> isPropertyDefined)
            {
                _parent = parent;
                _dealer = dealer;
                _user = user;
                _vehicleModel = vehicleModel;
                _inventoryId = inventoryId;
                _isPropertyDefined = isPropertyDefined;
            }

            public void SaveTrim()
            {
                if (!ShouldSaveTrim()) return;
                _vehicleConfiguration = VehicleConfiguration.FetchOrCreateDetailed(_dealer.Id, _inventoryId, _user.UserName);
                _trimSetForFirstTime = _vehicleConfiguration.ChromeStyleID <= 0;

                int chromeStyleid = int.Parse(_vehicleModel.Trim.Id.Split('_').ToArray()[1]);
                if (_vehicleConfiguration.ChromeStyleID != chromeStyleid || _trimSetForFirstTime)
                {
                    SetChromeStyleId(chromeStyleid);
                    ClearVehicleInformation();
                    UpdateVehicleConfiguration();
                    
                    SendAutoApproveMessage();
                }
            }

            public void SaveEngine()
            {
                if (!ShouldSaveEngine()) return;
                var engineId = int.Parse(_vehicleModel.Engine.Id);
                UpsertVehicleConfiguration.UpdateEngine(_inventoryId, _dealer.Id, engineId);
            }

            public void SaveTransmission()
            {
                if (!ShouldSaveTransmission()) return;
                var transmittionId = int.Parse(_vehicleModel.Transmission.Id);
                UpsertVehicleConfiguration.UpdateTrans(_inventoryId, _dealer.Id, transmittionId);
            }

            public void SaveDriveTrain()
            {
                if (!ShouldSaveDrivetrain()) return;
                var drivetrainId = int.Parse(_vehicleModel.Drivetrain.Id);
                UpsertVehicleConfiguration.UpdateDrivetrain(_inventoryId, _dealer.Id, drivetrainId);
            }

            private GenericChoiceCollection GetAvailableEquipment(int trim)
            {
                var equipmentDataSource = new GenericChoicesDataSource();
                var orphanDataSource = new OrphanedLotOptionsDataSource();

                var chromeEquipment = equipmentDataSource.Fetch(trim, 0);

                foreach (GenericChoice orphan in orphanDataSource.Fetch(trim, _dealer.Id, _vehicleModel.Id))
                {
                    chromeEquipment.Add(orphan);
                }

                return chromeEquipment;
            }

            private static GenericChoice GetGenericChoice(GenericChoiceCollection choiceCollection, Ref equipment)
            {
                return choiceCollection.Cast<GenericChoice>().FirstOrDefault(genericChoice => genericChoice.ID == int.Parse(equipment.Id));
            }

            public void SaveEquipment()
            {
                if (!ShouldSaveEquipment()) return;
                var availableEquipment =
                    GetAvailableEquipment(int.Parse(_vehicleModel.Trim.Id.Replace(@"1_", string.Empty)));

                _vehicleConfiguration = VehicleConfiguration.FetchOrCreateDetailed(_dealer.Id, _inventoryId, _user.UserName);

                List<int> genericIds = _vehicleConfiguration.VehicleOptions.generics.GetCategoryIdList();
                genericIds.Sort();
                string origEquipmentKey = GenerateKey(genericIds);

                _vehicleConfiguration.VehicleOptions.generics.Clear();

                foreach (var equipment in _vehicleModel.Equipment)
                {
                    var genericChoice = GetGenericChoice(availableEquipment, equipment);
                    var isStandard = genericChoice != null && genericChoice.IsStandard;

                    var newEquipment = new GenericEquipment
                                           {
                                               Category =
                                                   {Description = equipment.Desc, CategoryID = int.Parse(equipment.Id)},
                                               IsStandard = isStandard
                                           };

                    _vehicleConfiguration.VehicleOptions.generics.Add(newEquipment);
                }

                foreach (GenericChoice genericChoice in availableEquipment)
                {
                    if (_vehicleModel.Equipment.All(r => int.Parse(r.Id) != genericChoice.ID)) continue;

                    var newEquipment = new GenericEquipment
                                           {
                                               Category =
                                                   {
                                                       Description = genericChoice.Description,
                                                       CategoryID = genericChoice.ID
                                                   },
                                               IsStandard = genericChoice.IsStandard
                                           };

                    _vehicleConfiguration.VehicleOptions.generics.Add(newEquipment);
                }

                List<int> updatedGenericIds = _vehicleConfiguration.VehicleOptions.generics.GetCategoryIdList();
                updatedGenericIds.Sort();
                string newEquipmentKey = GenerateKey(updatedGenericIds);

                //TODO: Figure out why these are always different.
                //  When I PUT the same options back to a vehicle the updatedGenericIds list is one less than the originalGenericsList
                //  We should figure out why that is.

                if (!origEquipmentKey.Equals(newEquipmentKey))
                    SendAutoApproveMessage();

                _vehicleConfiguration.Save(_dealer.Id, _user.UserName);
            }

            private bool ShouldSaveEquipment()
            {
                return _vehicleModel.Equipment != null;
            }

            public void SaveEquipmentCompleted()
            {
                if (!ShouldSaveEquipmentCompleted()) return;
                var statuses = StepStatusCollection.Fetch(_dealer.Id, _inventoryId);
                var equipmentStatusCode = _vehicleModel.EquipmentCompleted.HasValue &&
                                          _vehicleModel.EquipmentCompleted.Value
                                              ? ActivityStatusCodes.Complete
                                              : ActivityStatusCodes.Incomplete;
                statuses.SetStatus(StepStatusTypes.ExteriorEquipment, equipmentStatusCode).SetStatus(
                    StepStatusTypes.InteriorEquipment, equipmentStatusCode);
                statuses.Save(HttpContext.Current.User.Identity.Name);
            }

            private bool ShouldSaveEquipmentCompleted()
            {
                return _vehicleModel.EquipmentCompleted.HasValue;
            }

            public void SavePhotosCompleted()
            {
                if (!ShouldSavePhotosCompleted()) return;
                var statuses = StepStatusCollection.Fetch(_dealer.Id, _inventoryId);
                var equipmentStatusCode = _vehicleModel.PhotosCompleted.HasValue && _vehicleModel.PhotosCompleted.Value
                                              ? ActivityStatusCodes.Complete
                                              : ActivityStatusCodes.Incomplete;
                statuses.SetStatus(StepStatusTypes.Photos, equipmentStatusCode);
                statuses.Save(HttpContext.Current.User.Identity.Name);
            }

            private bool ShouldSavePhotosCompleted()
            {
                return _vehicleModel.PhotosCompleted.HasValue;
            }

            private void SetChromeStyleId(int chromeStyleId)
            {
                _vehicleConfiguration.SetChromeStyleID(_dealer.Id, _inventoryId, chromeStyleId);
            }

            private void ClearVehicleInformation()
            {
                ClearColors();
                ClearPackages();
                ClearKeyInformation();
                ClearCondition();
            }

            private void ClearColors()
            {
                _vehicleConfiguration.ExteriorColor1 = string.Empty;
                _vehicleConfiguration.ExteriorColor2 = string.Empty;
                _vehicleConfiguration.ExteriorColorCode = string.Empty;
                _vehicleConfiguration.InteriorColor = string.Empty;
                _vehicleConfiguration.InteriorColorCode = string.Empty;
            }

            private void ClearPackages()
            {
                _vehicleConfiguration.VehicleOptions.options.Clear();
                var packages = DetailedEquipmentCollection.FetchSelections(_dealer.Id, _inventoryId);
                packages.Clear();
                packages.Save(_dealer.Id, _inventoryId);
            }

            private void ClearKeyInformation()
            {
                _vehicleConfiguration.KeyInformationList.Clear();
            }

            private void ClearCondition()
            {
                _vehicleConfiguration.VehicleCondition = string.Empty;
            }

            private void UpdateVehicleConfiguration()
            {
                _vehicleConfiguration.Save(_dealer.Id, _user.UserName);
            }

            private void SendAutoApproveMessage()
            {
                ValidateTrim();
                _parent._adMessageSender.SendAutoApproval(new AutoApproveMessage(_dealer.Id, _inventoryId, _user.UserName), GetType().FullName);
            }

            private bool ShouldSaveTrim()
            {
                return (_vehicleModel.Trim != null && !string.IsNullOrWhiteSpace(_vehicleModel.Trim.Id));
            }

            private bool ShouldSaveEngine()
            {
                return _vehicleModel.Engine != null && !string.IsNullOrWhiteSpace(_vehicleModel.Engine.Id);
            }

            private bool ShouldSaveTransmission()
            {
                return _vehicleModel.Transmission != null && !string.IsNullOrWhiteSpace(_vehicleModel.Transmission.Id);
            }

            public void SaveIntColor()
            {
                if (!ShouldSaveIntColor()) return;
                _vehicleConfiguration = VehicleConfiguration.FetchOrCreateDetailed(_dealer.Id, _inventoryId, _user.UserName);
                ValidateTrim();

                var intColor = Color.ParseColor1(_vehicleModel.IntColor.Id);
                _vehicleConfiguration.InteriorColorCode = intColor.Code;
                _vehicleConfiguration.InteriorColor = string.IsNullOrEmpty(intColor.Code) ? "" : intColor.Desc;
                _vehicleConfiguration.Save(_dealer.Id, _user.UserName);
            }


            public void SaveExtColor()
            {
                if (!ShouldSaveExtColor()) return;
                _vehicleConfiguration = VehicleConfiguration.FetchOrCreateDetailed(_dealer.Id, _inventoryId, _user.UserName);
                ValidateTrim();

                var ext1Color = Color.ParseColor1(_vehicleModel.ExtColor.Id);
                var ext2Color = Color.ParseColor2(_vehicleModel.ExtColor.Id);

                _vehicleConfiguration.ExteriorColor1 = ext1Color.Desc;
                _vehicleConfiguration.ExteriorColorCode = ext1Color.Code;

                if (ext2Color != null)
                {
                    _vehicleConfiguration.ExteriorColor2 = ext2Color.Desc;
                    _vehicleConfiguration.ExteriorColorCode2 = ext2Color.Code;
                }

                _vehicleConfiguration.Save(_dealer.Id, _user.UserName);
            }


            private bool ShouldSaveDrivetrain()
            {
                return _vehicleModel.Drivetrain != null && !string.IsNullOrWhiteSpace(_vehicleModel.Drivetrain.Id);
            }

            private bool ShouldSaveIntColor()
            {
                return _vehicleModel.IntColor != null && !string.IsNullOrWhiteSpace(_vehicleModel.IntColor.Id);
            }

            private bool ShouldSaveExtColor()
            {
                return _vehicleModel.ExtColor != null && !string.IsNullOrWhiteSpace(_vehicleModel.ExtColor.Id);
            }

            public void SaveOptions()
            {
                if (!ShouldSaveOptions()) return;

                _vehicleConfiguration = VehicleConfiguration.FetchOrCreateDetailed(_dealer.Id, _inventoryId, _user.UserName);
                ValidateTrim();

                List<string> originalOptionCodes = _vehicleConfiguration.VehicleOptions.options.GetOptionCodes(",").Split(new[] {','},StringSplitOptions.RemoveEmptyEntries).ToList();
                originalOptionCodes.Sort();
                string originalOptionKey = GenerateKey(originalOptionCodes);

                _vehicleConfiguration.VehicleOptions.options.Clear();
                var optionsAvailableForTrim =
                    EquipmentCollection.FetchUnfilteredOptions(_vehicleConfiguration.ChromeStyleID);
                foreach (var option in _vehicleModel.Options)
                {
                    if (!optionsAvailableForTrim.Contains(option.Id)) return;
                    var optionToAdd = new DetailedEquipment(optionsAvailableForTrim.GetByCode(option.Id));
                    _vehicleConfiguration.VehicleOptions.options.Add(optionToAdd);
                }

                List<string> newOptionCodes = _vehicleConfiguration.VehicleOptions.options.GetOptionCodes(",").Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                newOptionCodes.Sort();
                string newOptionKey = GenerateKey(newOptionCodes);

                if (!originalOptionKey.Equals(newOptionKey))
                    SendAutoApproveMessage();

                _vehicleConfiguration.VehicleOptions.options.Save(_dealer.Id, _inventoryId);
            }

            //@TODO : Custom exception and test
            private void ValidateTrim()
            {
                if (_vehicleConfiguration.ChromeStyleID > 0) return;
                throw new TrimNotSetException("This operation requires the trim to be set.");
            }

            private bool ShouldSaveOptions()
            {
                return _vehicleModel.Options != null;
            }

            public void InvalidateCache()
            {
                _parent._inventoryDataAccessCache.Clear(_dealer.Id, _inventoryId);
            }
        }

        public static string GenerateKey(object sourceObject)
        {
            if (sourceObject == null)
            {
                throw new ArgumentNullException("Null as parameter is not allowed");
            }
            string hashString;

            //We determine if the passed object is really serializable.
            try
            {
                hashString = ComputeHash(ObjectToByteArray(sourceObject));
            }
            catch (System.Reflection.AmbiguousMatchException ame)
            {
                throw new ApplicationException("Could not definitly decide if object is serializable.", ame);
            }

            return hashString;
        }


        private static string ComputeHash(byte[] objectAsBytes)
        {
            System.Security.Cryptography.MD5 md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] result = md5.ComputeHash(objectAsBytes);

            // Build the final string by converting each byte
            // into hex and appending it to a StringBuilder
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                sb.Append(result[i].ToString("X2"));
            }
            return sb.ToString();
        }

        private static readonly object locker = new object();

        private static byte[] ObjectToByteArray(object objectToSerialize)
        {
            using (System.IO.MemoryStream fs = new System.IO.MemoryStream())
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                lock (locker)
                {
                    formatter.Serialize(fs, objectToSerialize);
                }
                return fs.ToArray();
            }
        }
    }
}