﻿using System.Text;
using System.Web;
using FirstLook.Merchandising.DomainModel.Chrome;

namespace Max.Selling.WebService.WebLoader.ViewModels
{
    public class ColorHelper
    {
        private readonly string _code1;
        private readonly string _desc1;
        private readonly string _simple1;
        private readonly string _code2;
        private readonly string _desc2;
        private readonly string _simple2;

        public ColorHelper(IExteriorColor c)
        {
            _code1 = string.IsNullOrEmpty(c.Ext1MfgCode) ? c.Ext1Code ?? "" : c.Ext1MfgCode;
            _desc1 = c.Ext1Desc ?? "";
            _simple1 = c.Ext1Simple ?? "";

            _code2 = string.IsNullOrEmpty(c.Ext2MfgCode) ? c.Ext2Code ?? "" : c.Ext2MfgCode;
            _desc2 = c.Ext2Desc ?? "";
            _simple2 = c.Ext2Simple ?? "";
        }

        public ColorHelper(IInteriorColor c)
        {
            _code1 = string.IsNullOrEmpty(c.MfgCode) ? c.Code ?? "" : c.MfgCode;
            _desc1 = c.Desc ?? "";
            _simple1 = "";

            _code2 = "";
            _desc2 = "";
            _simple2 = "";
        }

        public ColorHelper(string code1, string desc1, string code2 = "", string desc2 = "")
        {
            _code1 = code1 ?? "";
            _desc1 = desc1 ?? "";
            _simple1 = "";

            _code2 = code2 ?? "";
            _desc2 = desc2 ?? "";
            _simple2 = "";
        }

        public string ToId()
        {
            var b = new StringBuilder(100);
            
            if(!string.IsNullOrEmpty(_code1))
            {
                if(b.Length > 0)
                    b.Append("&");
                b.Append("c=");
                b.Append(HttpUtility.UrlEncode(_code1, Encoding.UTF8));
            }
            if (!string.IsNullOrEmpty(_desc1))
            {
                if (b.Length > 0)
                    b.Append("&");
                b.Append("d=");
                b.Append(HttpUtility.UrlEncode(_desc1, Encoding.UTF8));
            }

            if (!string.IsNullOrEmpty(_code2))
            {
                if (b.Length > 0)
                    b.Append("&");
                b.Append("c2=");
                b.Append(HttpUtility.UrlEncode(_code2, Encoding.UTF8));
            }
            if (!string.IsNullOrEmpty(_desc2))
            {
                if (b.Length > 0)
                    b.Append("&");
                b.Append("d2=");
                b.Append(HttpUtility.UrlEncode(_desc2, Encoding.UTF8));
            }

            return b.ToString();
        }

        public string ToDesc()
        {
            var middle = !string.IsNullOrEmpty(_desc1) && !string.IsNullOrEmpty(_desc2)
                             ? " with "
                             : "";

            return _desc1 + middle + _desc2;
        }

        public string ToSimple()
        {
            var middle = !string.IsNullOrEmpty(_simple1) && !string.IsNullOrEmpty(_simple2)
                             ? " / "
                             : "";

            return _simple1 + middle + _simple2;
        }

        public ExtColorRef ToExtRef()
        {
            var simple = ToSimple();
            if(simple == "")
                simple = null;
            return new ExtColorRef

                {
                    Id = ToId(),
                    Desc = ToDesc(),
                    Simple = simple
                };
        }

        public Ref ToIntRef()
        {
            return new Ref
                {
                    Id = ToId(),
                    Desc = ToDesc()
                };
        }
    }
}