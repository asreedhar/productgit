﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Max.Selling.WebService.WebLoader.ViewModels
{
    public class VehicleSummary
    {
        public int Id { get; set; }
        public string StockNum { get; set; }
        public string Vin { get; set; }
        public int PhotoCount { get; set; }
        public int Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Ref Trim { get; set; }
        public List<string> AvailableTrimIds { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ExtColorRef ExtColor { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? LowPhotos { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? NoPhotos { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? NoEquipment { get; set; }
    }
}