namespace Max.Selling.WebService.WebLoader.ViewModels
{
    public class TrimRef
    {
        public string Id { get; set; }
        public string Desc { get; set; }
    }
}