﻿using System.Collections.Generic;

namespace Max.Selling.WebService.WebLoader.ViewModels
{
    public class Trim
    {
        public string Id { get; set; }
        public int Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Desc { get; set; }
        public List<SpecificEquipment> AvailableEngines { get; set; }
        public List<SpecificEquipment> AvailableTransmissions { get; set; }
        public List<SpecificEquipment> AvailableDrivetrains { get; set; }
        public List<IntColor> AvailableIntColors { get; set; }
        public List<ExtColor> AvailableExtColors { get; set; }
        public List<Option> AvailableOptions { get; set; }
        public List<Equipment> AvailableEquipment { get; set; }
    }
}