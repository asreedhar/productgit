﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Max.Selling.WebService.WebLoader.ViewModels
{
    public class Vehicle
    {
        public int Id { get; set; }
        public string StockNum { get; set; }
        public string Vin { get; set; }
        public int Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? EquipmentCompleted { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? PhotosCompleted { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Ref Trim { get; set; }

        public List<string> AvailableTrimIds { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Ref Engine { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Ref Transmission { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Ref Drivetrain { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Ref IntColor { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ExtColorRef ExtColor { get; set; }

        public List<OptionRef> Options { get; set; }
        public List<Ref> Equipment { get; set; }
    }
}