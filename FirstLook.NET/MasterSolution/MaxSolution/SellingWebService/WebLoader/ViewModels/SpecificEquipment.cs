using Newtonsoft.Json;

namespace Max.Selling.WebService.WebLoader.ViewModels
{
    public class SpecificEquipment
    {
        public string Id { get; set; }
        public string Desc { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? Standard { get; set; }
    }
}