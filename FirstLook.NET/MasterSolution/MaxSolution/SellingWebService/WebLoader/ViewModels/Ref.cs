﻿using System.Globalization;
using FirstLook.Merchandising.DomainModel.Chrome.Model;

namespace Max.Selling.WebService.WebLoader.ViewModels
{
    public class Ref
    {
        public string Id { get; set; }
        public string Desc { get; set; }

        public static Ref FromStyle(int? styleId, Style style)
        {
            if (styleId == null)
                return null;

            if (style == null)
                return new Ref
                {
                    Id = "1_" + styleId.Value.ToString(CultureInfo.InvariantCulture),
                    Desc = ""
                };

            return new Ref
            {
                Id = style.CountryCode.ToString(CultureInfo.InvariantCulture) + "_" +
                     style.StyleID.ToString(CultureInfo.InvariantCulture),
                Desc = style.GetStyleDesc()
            };
        }
    }
}