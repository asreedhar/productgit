namespace Max.Selling.WebService.WebLoader.ViewModels
{
    public class Equipment
    {
        public string Id { get; set; }
        public string Type { get; set; }
        public string Category { get; set; }
        public bool? Standard { get; set; }
        public string Desc { get; set; }
    }
}