namespace Max.Selling.WebService.WebLoader.ViewModels
{
    public class ExtColor
    {
        public string Id { get; set; }
        public string Desc { get; set; }
        public string Simple { get; set; }
    }
}