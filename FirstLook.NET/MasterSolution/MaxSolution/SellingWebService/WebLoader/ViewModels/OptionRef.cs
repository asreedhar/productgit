using Newtonsoft.Json;

namespace Max.Selling.WebService.WebLoader.ViewModels
{
    public class OptionRef : Ref
    {
        public string Code { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string DetailedDesc { get; set; }
    }
}