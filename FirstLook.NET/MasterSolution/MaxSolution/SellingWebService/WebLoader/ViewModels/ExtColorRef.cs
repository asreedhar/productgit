﻿using Newtonsoft.Json;

namespace Max.Selling.WebService.WebLoader.ViewModels
{
    public class ExtColorRef : Ref
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Simple { get; set; }
    }
}