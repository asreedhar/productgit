using System;
using Newtonsoft.Json;

namespace Max.Selling.WebService.WebLoader.ViewModels
{
    public class Option
    {
        public string Id { get; set; }
        public string Code { get; set; }
        public string Desc { get; set; }
        
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Decimal? MSRP { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string DetailedDesc { get; set; }
    }
}