﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Max.Selling.WebService.ViewModels
{
    public class LeadEmailViewModel
    {
        public string CustomerFirstName { get; set; }
        public string CustomerLastName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerPhoneNumber { get; set; }

        public int Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Trim { get; set; }

        public string SalesmanPhoneNumber { get; set; }

        public string DealerName { get; set; }
        public string DealerPhoneNumber { get; set; }
    }
}
