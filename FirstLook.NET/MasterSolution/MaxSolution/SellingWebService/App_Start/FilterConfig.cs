﻿using System.Web.Http.Filters;
using Max.Selling.WebService.WebLoader.Filters;

namespace Max.Selling.WebService.App_Start
{
    internal static class FilterConfig
    {
        public static void RegisterGlobalFilters(HttpFilterCollection filters)
        {
            filters.Add(new LogErrorsAttribute());
        }
    }
}