﻿using System.Collections.ObjectModel;
using System.Net.Http;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using Max.Selling.WebService.WebLoader.MessageHandlers;

namespace Max.Selling.WebService.App_Start
{
    internal static class MessageHandlerConfig
    {
        public static void Register(Collection<DelegatingHandler> handlers)
        {
            var userServices = Registry.Resolve<IUserServices>();

            handlers.Add(new CompressionMessageHandler());
            handlers.Add(new BasicAuthenticationMessageHandler(userServices));
        }
    }
}