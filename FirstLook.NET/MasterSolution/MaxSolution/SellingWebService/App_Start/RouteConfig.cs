﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace Max.Selling.WebService.App_Start
{
    internal static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapHttpRoute("MiniProfiler", "MiniProfiler.aspx", new {controller = "MiniProfiler"});
            routes.MapHttpRoute("Version", "Version.aspx", new {controller = "Version"});

            SetupWebloaderRoutes(routes);
            SetupSellingRoutes(routes);
        }

        private static void SetupWebloaderRoutes(RouteCollection routes)
        {
            const string baseRoute = "wl.aspx/";

            routes.MapHttpRoute("Webloader_Dealers", baseRoute + "dealers", new { controller = "WebLoaderDealers" });
            routes.MapHttpRoute("Webloader_Trim", baseRoute + "trims/{trim}", new { controller = "WebLoaderTrim" });
            routes.MapHttpRoute("Webloader_Vehicle", baseRoute + "dealers/{dealer}/vehicles/{vehicle}",
                                new { controller = "WebLoaderVehicle" });
            routes.MapHttpRoute("Webloader_Vehicle_Photos", baseRoute + "dealers/{dealer}/vehicles/{vehicle}/photos",
                                new { controller = "WebLoaderVehiclePhotos" });
            routes.MapHttpRoute("Webloader_Vehicle_Thumb", baseRoute + "dealers/{dealer}/vehicles/{vehicle}/thumb.jpg",
                                new { controller = "WebLoaderVehicleThumb" });
            routes.MapHttpRoute("Webloader_Vehicles", baseRoute + "dealers/{dealer}/vehicles",
                                new { controller = "WebLoaderVehicles" });
        }

        private static void SetupSellingRoutes(RouteCollection routes)
        {
            //not using wildcard on iis6 so we need aspx in the routes
            routes.MapRoute(
                "Schema",
                "api.aspx/Inventory/{version}/{document}",
                new { controller = "Selling", action = "InventorySchema" }
                );

            routes.MapRoute(
                "CreateToken",
                "api.aspx/Token/Create",
                new { controller = "Login", action = "CreateTicket" }
                );

            routes.MapRoute(
                "Dealers",
                "api.aspx/{token}/Dealers",
                new { controller = "Selling", action = "GetDealers" }
                );

            routes.MapRoute(
                "Inventory",
                "api.aspx/{token}/Inventory/{businessUnit}",
                new { controller = "Selling", action = "Inventory" }
                );

            routes.MapRoute(
                "Photos",
                "api.aspx/{token}/Photos/{businessUnit}/{vin}",
                new { controller = "Selling", action = "Photos" }
                );

            routes.MapRoute(
                "DealerPhotos",
                "api.aspx/{token}/Photos/{businessUnit}",
                new { controller = "Selling", action = "DealerPhotos" }
                );

            routes.MapRoute(
                "Customer",
                "api.aspx/{token}/Customer/{businessUnit}/{vin}",
                new { controller = "Selling", action = "Customer" }
                );

            routes.MapRoute(
                "Default", // Route name
                "{controller}.aspx/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
                );
        }
    }
}