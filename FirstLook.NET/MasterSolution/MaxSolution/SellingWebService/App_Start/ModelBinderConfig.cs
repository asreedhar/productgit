﻿using System.Web.Mvc;
using Max.Selling.WebService.Internal;
using Max.Selling.WebService.ViewModels;

namespace Max.Selling.WebService.App_Start
{
    internal static class ModelBinderConfig
    {
        public static void Register(ModelBinderDictionary binders)
        {
            binders.Add(typeof (CustomerViewModel), new SkipNullBinder());
        }
    }
}