﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Dependencies;
using Autofac;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.PhotoServices;
using FirstLook.DomainModel.Oltp;
using FirstLook.LotIntegration.DomainModel;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Chrome;
using FirstLook.Merchandising.DomainModel.Chrome.Mapper;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using FirstLook.Merchandising.DomainModel.DataAccess;
using Max.Selling.WebService.WebLoader.Model;
using Merchandising.Messages;

namespace Max.Selling.WebService.App_Start
{
    internal static class IOCConfig
    {
        public static void RegisterTypes(HttpConfiguration webApiConfig)
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new CoreModule());
            builder.RegisterModule(new OltpModule());
            builder.RegisterModule(new PhotoServicesModule());
            builder.RegisterModule(new MerchandisingMessagesModule());
            builder.RegisterModule( new LotIntegrationServiceModule() );
            new MerchandisingDomainRegistry().Register(builder);

            // Register controllers
            builder.RegisterType<Controllers.WebLoaderVehiclePhotosController>().AsSelf();
            builder.RegisterType<Controllers.WebLoaderDealersController>().AsSelf();
            builder.RegisterType<Controllers.WebLoaderVehiclesController>().AsSelf();
            builder.RegisterType<Controllers.WebLoaderVehicleController>().AsSelf();
            builder.RegisterType<Controllers.WebLoaderTrimController>().AsSelf();
            builder.RegisterType<Controllers.WebLoaderVehicleThumbController>().AsSelf();

            // Register WebLoader domain dependencies.
            builder.Register(c => new VehicleDataLoader(
                                      c.Resolve<IUserServices>(), c.Resolve<IDealerServices>(),
                                      c.Resolve<ISharedChromeDataRepository>(),
                                      c.Resolve<IChromeMapper>(), c.Resolve<IVinPatternRepository>(),
                                      c.Resolve<IStyleRepository>(),
                                      c.Resolve<IInventoryDataAccess>()))
                .As<IVehicleDataLoader>();
            builder.Register(c => new VehicleUpdater(
                                      c.Resolve<IQueueFactory>(), 
                                      c.Resolve<IAdMessageSender>(),
                                      c.Resolve<IUserServices>(), 
                                      c.Resolve<IDealerServices>(),
                                      c.Resolve<IInventoryDataAccessCache>()))
                .As<IVehicleUpdater>();
            builder.Register(c => new VehicleRepository(
                                      c.Resolve<IVehicleDataLoader>(),
                                      c.Resolve<IVehicleUpdater>()))
                .As<IVehicleRepository>();

            var container = builder.Build();

            Registry.RegisterContainer(container);
            webApiConfig.DependencyResolver = new WebapiAutofacDependencyResolver(container);
        }

        // Adapted from AutoFac source -- there is a package for this but it requires a more 
        // recent version of AutoFac than I want to upgrade to at this time.
        private class WebapiAutofacDependencyResolver : IDependencyResolver
        {
            private readonly IContainer _container;
            private readonly IDependencyScope _rootScope;
            private static readonly string Tag = typeof (WebapiAutofacDependencyResolver).ToString();

            public WebapiAutofacDependencyResolver(IContainer container)
            {
                _container = container;
                _rootScope = new WebapiAutofacDependencyScope(_container);
            }

            public void Dispose()
            {
                _rootScope.Dispose();
            }

            public object GetService(Type serviceType)
            {
                return _rootScope.GetService(serviceType);
            }

            public IEnumerable<object> GetServices(Type serviceType)
            {
                return _rootScope.GetServices(serviceType);
            }

            public IDependencyScope BeginScope()
            {
                return new WebapiAutofacDependencyScope(_container.BeginLifetimeScope(Tag));
            }
        }
        
        // Adapted from AutoFac source -- there is a package for this but it requires a more 
        // recent version of AutoFac than I want to upgrade to at this time.
        private class WebapiAutofacDependencyScope : IDependencyScope
        {
            private readonly ILifetimeScope _scope;

            public WebapiAutofacDependencyScope(ILifetimeScope scope)
            {
                _scope = scope;
            }

            public void Dispose()
            {
                _scope.Dispose();
            }

            public object GetService(Type serviceType)
            {
                return _scope.ResolveOptional(serviceType);
            }

            public IEnumerable<object> GetServices(Type serviceType)
            {
                if (!_scope.IsRegistered(serviceType))
                    return Enumerable.Empty<object>();

                var enumerableServiceType = typeof (IEnumerable<>).MakeGenericType(serviceType);
                return (IEnumerable<object>)_scope.Resolve(enumerableServiceType);
            }
        }
    }
}