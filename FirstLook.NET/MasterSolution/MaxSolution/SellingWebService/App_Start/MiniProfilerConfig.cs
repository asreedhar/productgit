﻿using System.Configuration;
using MvcMiniProfiler;
using MvcMiniProfiler.Storage;

namespace Max.Selling.WebService.App_Start
{
    internal static class MiniProfilerConfig
    {
         public static void ConfigureStorage()
         {
             MiniProfiler.Settings.Storage = new SqlServerStorage(
                 ConfigurationManager.ConnectionStrings["Merchandising"].ConnectionString);
             MiniProfiler.Settings.SuppressMiniProfilerIdsInHeader = true;
         }
    }
}