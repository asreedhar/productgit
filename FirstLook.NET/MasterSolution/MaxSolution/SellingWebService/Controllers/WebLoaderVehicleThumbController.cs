﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Web.Http;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using FirstLook.Merchandising.DomainModel.DataAccess;
using Max.Selling.WebService.WebLoader.Filters;
using FirstLook.Common.Core.PhotoServices;

namespace Max.Selling.WebService.Controllers
{
    [AutoETag]
    [AuthorizeUser]
    [AuthorizeDealer(AssertDealerUpgrades = "Merchandising", AssertDealerHasWebLoaderUpgrade = true)]
    public class WebLoaderVehicleThumbController : ApiController
    {
        private IInventoryDataAccessCache _inventory;
        private IDealerServices _dealerServices;
        private IPhotoServices _photoServices;

        public WebLoaderVehicleThumbController(IInventoryDataAccessCache inventory, IDealerServices dealerServices, IPhotoServices photoServices)
        {
            _inventory = inventory;
            _dealerServices = dealerServices;
            _photoServices = photoServices;
        }

        public HttpResponseMessage Get(string dealer, int vehicle, int? w = null)
        {
            var businessUnitId = _dealerServices.GetDealerByCode(dealer).Id;
            var inventory = _inventory.Fetch(businessUnitId, vehicle);

            var result = _photoServices.GetMainThumbnailUrl(dealer, inventory.VIN);
            var image = LoadFromUrl(result.Url);
            var stream = Resize(image, w);
            stream.Seek(0, SeekOrigin.Begin);

            var content = new StreamContent(stream);
            content.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");

            return new HttpResponseMessage(HttpStatusCode.OK) {Content = content};
        }

        private static Image LoadFromUrl(string url)
        {
            WebClient client = new WebClient();
            Stream stream = client.OpenRead(url);
            var bitMap = new Bitmap(stream);
            return bitMap;
        }


        private static void Save(Image source, Stream stream, ImageFormat sourceFormat)
        {
            if (sourceFormat.Equals(ImageFormat.Jpeg))
                source.Save(stream, ImageFormat.Jpeg);
            else if (sourceFormat.Equals(ImageFormat.Png))
                source.Save(stream, ImageFormat.Png);
            else
                source.Save(stream, ImageFormat.Jpeg);
        }

        private static Stream Resize(Image source, int? w)
        {
            MemoryStream stream = new MemoryStream();

            if(!w.HasValue || w.Value >= source.Width) //nothing to do and we can't make it bigger
            {
                Save(source, stream, source.RawFormat);
                return stream;
            }

            double ratio = w.Value / (double)source.Width;            

            int targetWidth = (int)Math.Round(source.Width * ratio, 0);
            int targetHeight = (int)Math.Round(source.Height * ratio, 0);

            using (Image target = new Bitmap(targetWidth, targetHeight))
            using (Graphics g = Graphics.FromImage(target))
            {
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.DrawImage(source, 0, 0, targetWidth, targetHeight);

                Save(target, stream, source.RawFormat);
            }

            return stream;
        }

      
    }
}