﻿using System;
using System.IO;
using System.Text;
using System.Web.Mvc;
using FirstLook.Common.Core.Command;
using Max.Selling.Common;
using Max.Selling.Domain;
using Max.Selling.WebService.ActionResults;
using Max.Selling.WebService.Internal;
using Max.Selling.WebService.ViewModels;
using CommandFactory = Max.Selling.Domain.CommandFactory;
using ICommandFactory = Max.Selling.Domain.ICommandFactory;

namespace Max.Selling.WebService.Controllers
{
    public class SellingController : Controller
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private ICommandFactory _commandFactory;

        public SellingController() : this(new CommandFactory())
        {
        }

        public SellingController(ICommandFactory commandFactory)
        {
            _commandFactory = commandFactory;
        }

        [HttpGet]
        [TokenAuthorization]
        [CompressionFilter]
        public ActionResult GetDealers()
        {
            Log.DebugFormat("GetDealers called for {0}", HttpContext.User.Identity.Name);

            FetchDealersCommand command = _commandFactory.CreateDealersCommand(HttpContext.User);
            AbstractCommand.DoRun(command);

            if(command.ErrorInfo.HasError)
                Log.ErrorFormat("GetDealers returned error {0}", command.ErrorInfo.Message);

            return new ContentResult() { Content = command.Xml, ContentEncoding = Encoding.UTF8, ContentType = "text/xml" };
        }


        [HttpGet]
        [TokenAuthorization]
        [CompressionFilter]
        public ActionResult Inventory(int businessUnit)
        {
            Log.DebugFormat("Inventory called for {0} with businessUnit = {1}", HttpContext.User.Identity.Name, businessUnit);

            InventoryCommand command = _commandFactory.CreateInventoryCommand(businessUnit);
            AbstractCommand.DoRun(command);

            return new ContentResult() { Content = command.InventoryXml, ContentEncoding = Encoding.UTF8, ContentType = "text/xml" };
        }


        [HttpGet]
        public ActionResult InventorySchema(string version, string document)
        {
            string schema = string.Empty;
            using(TextReader reader = new StreamReader(typeof(Resources).Assembly.GetManifestResourceStream(Resources.GetEmbeddedSchemaResource(new Version(version)))))
                schema = reader.ReadToEnd();

            return new ContentResult() { Content = schema, ContentEncoding = Encoding.UTF8, ContentType = "text/xml" };
        }

        [HttpGet]
        [TokenAuthorization]
        [CompressionFilter]
        public ActionResult Photos(int businessUnit, string vin)
        {
            Log.DebugFormat("Photos called for {0} with businessUnit = {1} Vin = {2}", HttpContext.User.Identity.Name, businessUnit, vin);

            PhotosCommand command = _commandFactory.CreatePhotosCommand(businessUnit, vin);
            AbstractCommand.DoRun(command);

            return new ContentResult() { Content = command.Xml, ContentEncoding = Encoding.UTF8, ContentType = "text/xml" };
        }

        [HttpGet]
        [TokenAuthorization]
        [CompressionFilter]
        public ActionResult DealerPhotos(int businessUnit)
        {
            Log.DebugFormat("DealerPhotos called for {0} with businessUnit = {1}", HttpContext.User.Identity.Name, businessUnit);

            DealerPhotosCommand command = _commandFactory.CreateDealerPhotosCommand(businessUnit);
            AbstractCommand.DoRun(command);

            return new ContentResult() { Content = command.Xml, ContentEncoding = Encoding.UTF8, ContentType = "text/xml" };
        }

        [HttpPost]
        [TokenAuthorization]
        public ActionResult Customer(int businessUnit, string vin, CustomerViewModel customer)
        {
            Log.DebugFormat("Customer called for {0} with businessUnit = {1} Vin = {2}", HttpContext.User.Identity.Name, businessUnit, vin);

            LeadCommand leadCommand = new LeadCommand(businessUnit, vin)
                                      {
                                          Address = customer.Address,
                                          Email = customer.Email,
                                          City = customer.City,
                                          FirstName = customer.FirstName,
                                          LastName = customer.LastName,
                                          PhoneNumber = customer.PhoneNumber,
                                          State = customer.State,
                                          ZipCode = customer.ZipCode
                                      };

            AbstractCommand.DoRun(leadCommand);

            EmailCommand emailCommand = new EmailCommand(HttpContext.User.Identity.Name, businessUnit, vin, leadCommand.ErrorInfo);
            AbstractCommand.DoRun(emailCommand);

            LeadEmailViewModel emailModel = new LeadEmailViewModel() {
                CustomerFirstName = leadCommand.FirstName,
                CustomerLastName = leadCommand.LastName,
                CustomerEmail = leadCommand.Email,
                CustomerPhoneNumber = leadCommand.PhoneNumber,
                Year = emailCommand.Year,
                Make = emailCommand.Make,
                Model = emailCommand.Model,
                Trim = emailCommand.Trim,
                SalesmanPhoneNumber = emailCommand.UserOfficePhoneExtension,
                DealerName = emailCommand.DealerName,
                DealerPhoneNumber = emailCommand.DealerPhoneNumber
            };

            ErrorInformation pdfErrorInfo = ErrorInformation.NoError;
            if (!emailCommand.ErrorInfo.HasError) 
            {   //send email
                Log.Debug("Generating email from view");
                string emailBody = RenderViewToString("~/Views/LeadEmail.aspx", emailModel);
                string subject = string.Format("{0} {1} {2}", emailModel.Year, emailModel.Make, emailModel.Model);
                pdfErrorInfo = PdfMailer.SendMail(emailModel.CustomerEmail, emailCommand.UserEmail, subject, emailBody, emailModel.DealerName, emailCommand.Pdf);
            }

            pdfErrorInfo = emailCommand.ErrorInfo.ProcessNewError(pdfErrorInfo);
            return new ContentResult() { Content = pdfErrorInfo.CreateStatusXml(), ContentEncoding = Encoding.UTF8, ContentType = "text/xml" }; ;
        }

        protected string RenderViewToString<T>(string viewPath, T model)
        {
            using (var writer = new StringWriter())
            {
                var view = new WebFormView(new ControllerContext(), viewPath);
                var vdd = new ViewDataDictionary<T>(model);
                var viewCxt = new ViewContext(ControllerContext, view, vdd, new TempDataDictionary(), writer);
                viewCxt.View.Render(viewCxt, writer);
                return writer.ToString();
            }
        } 
    }
}
