﻿using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Max.Selling.WebService.WebLoader.Filters;
using MvcMiniProfiler;

namespace Max.Selling.WebService.Controllers
{
    public class MiniProfilerController : ApiController
    {
        [AuthorizeUser(AssertAllRoles = "MiniProfiler")]
        public HttpResponseMessage Get()
        {
            return new HttpResponseMessage(HttpStatusCode.OK)
                       {
                           Content = new StringContent(
                               "<html><body>" +
                               MiniProfiler.RenderIncludes() +
                               "</body></html>",
                               Encoding.UTF8, "text/html")
                       };
        }
    }
}