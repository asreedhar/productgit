﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Core.Messaging;
using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using FirstLook.Merchandising.DomainModel.DataAccess;
using Max.Selling.WebService.WebLoader.Filters;
using Merchandising.Messages;
using Merchandising.Messages.WebLoader;

namespace Max.Selling.WebService.Controllers
{
    [AuthorizeUser]
    [AuthorizeDealer(AssertDealerUpgrades = "Merchandising", AssertDealerHasWebLoaderUpgrade = true)]
    public class WebLoaderVehiclePhotosController : ApiController
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private IFileStoreFactory _factory;
        private IUserServices _user;
        private IWebLoaderPhotoRepository _webLoaderRepository;
        private IQueue<PhotoBatchUploadMessage> _writeQueue;

        public WebLoaderVehiclePhotosController(IWebLoaderPhotoRepository webLoaderRepository, IFileStoreFactory fileStoreFactory, IQueueFactory queueFactory, IUserServices userServices)
        {
            _factory = fileStoreFactory;
            _user = userServices;
            _webLoaderRepository = webLoaderRepository;
            _writeQueue = queueFactory.CreateWebLoaderAbsolutePosition();
        }

        private static void WriteImage(IFileStorage storage, string key, byte[] imageBuffer)
        {
            Log.DebugFormat("Writing image {0} to storage", key);
            using (var stream = new MemoryStream())
            {
                stream.Write(imageBuffer, 0, imageBuffer.Length);
                stream.Flush();
                stream.Position = 0;
                storage.Write(key, stream);
            }
        }

        private void SendAndOrderPhotos(string dealer, Guid batch, bool @lock)
        {
            Log.DebugFormat("All photos in the batch are recorded in our table. Sending PhotoUploadMessage.");
            var photos = _webLoaderRepository.OrderPhotos(batch);
            var photoUploads = new List<PhotoUploadMessage>();

            // It looks like this could get bigger than 64KB. Might want to send one message per photo instead
            // of doing it this way.
            foreach (var photo in photos)
            {
                photoUploads.Add(new PhotoUploadMessage(
                                    dealer,
                                    @lock,
                                    photo.Vin,
                                    photo.StockNumber,
                                    photo.InventoryId,
                                    photo.Position,
                                    photo.Taken.ToUniversalTime().ToString("yyyyMMddTHHmmssZ"),
                                    photo.Handle,
                                    photo.Handle + ".jpg",
                                    _user.Current.UserName,
                                    _user.Current.FirstName,
                                    _user.Current.LastName));
            }

            var json = photoUploads.ToArray().ToJson();
            Log.DebugFormat("Message payload has length {0}. Content is '{1}'", json.Length, json);

            _writeQueue.SendMessage(new PhotoBatchUploadMessage(batch, photoUploads.ToArray()), GetType().FullName);
        }

        public HttpResponseMessage Post(string dealer, int vehicle, Guid batch, int batchCount, Guid rnd, string t, bool @lock = false)
        {
            // Setup some thread-context items
            log4net.ThreadContext.Properties["businessUnitId"] = dealer;
            log4net.ThreadContext.Properties["inventoryId"] = vehicle;
            log4net.ThreadContext.Properties["batchId"] = rnd;
            log4net.ThreadContext.Properties["timestamp"] = t;
            log4net.ThreadContext.Properties["batchCount"] = batchCount;

            Log.DebugFormat("WebLoaderPhoto Received: dealer = {0}, inv = {1}, batch = {2}, randomFileName={3}, timestamp = {3}, batchCount = {4}",
                dealer, vehicle, batch, rnd, t, batchCount);

            //if we have seen this guid for this vehicle return
            if (_webLoaderRepository.Exists(vehicle, rnd))
            {
                // this seems bad - what if they are re-trying?  we should also check S3 and see if the byte count matches.
                // or maybe we inserted the record to the db but failed to upload the file to s3 the first time, etc...
                Log.WarnFormat("We already have a record for a file with guid (filename) {0} for vehicle {1}. Returning http status {2}", rnd, vehicle, HttpStatusCode.NoContent);

                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }

            try
            {
                byte[] imageBuffer = null;
                var task = Request.Content.ReadAsByteArrayAsync().ContinueWith(x =>
                {
                    imageBuffer = x.Result;
                });
                task.Wait();

                if (imageBuffer == null || imageBuffer.Length == 0)
                {
                    throw new ApplicationException("Zero-byte image received.");
                }

                // Save image to s3 with the GUID value as the file name
                var fileStore = _factory.WebLoaderApiPhotos();
                string fileName = rnd + ".jpg";

                WriteImage(fileStore, fileName, imageBuffer);

                // TODO: We really need to get the position from the caller. Not having it forces us to "batch" the photos.
                DateTime tTime = DateTime.ParseExact(t, "yyyyMMddTHHmmssZ", CultureInfo.InvariantCulture);
                int insertBatchCount = _webLoaderRepository.Insert(vehicle, -1, tTime.ToLocalTime(), batch, rnd, _user.Current.UserName);

                Log.DebugFormat("Photo Count Recorded: inv = {0}, guid = {1}, count = {2}", vehicle, rnd, insertBatchCount);
                if (insertBatchCount >= batchCount) //send the batch message if we reached the total count
                {
                    SendAndOrderPhotos(dealer, batch, @lock);
                }
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("WebLoaderVehiclePhotosController.Post", ex);
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }

            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }
    }
}