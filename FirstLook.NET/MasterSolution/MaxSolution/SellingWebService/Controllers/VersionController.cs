﻿using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using FirstLook.Internal;
using Max.Selling.WebService.WebLoader.Filters;

namespace Max.Selling.WebService.Controllers
{
    public class VersionController : ApiController
    {
        [AuthorizeUser(AssertAllRoles = "Administrator")]
        public HttpResponseMessage Get()
        {
            return new HttpResponseMessage(HttpStatusCode.OK)
                       {
                           Content = new StringContent(
                               typeof(FirstLookAssemblyInfo).Assembly.GetName().Version.ToString(),
                               Encoding.UTF8,
                               "text/plain")
                       };
        }
    }
}