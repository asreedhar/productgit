﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text.RegularExpressions;
using System.Web.Http;
using Max.Selling.WebService.WebLoader.Filters;
using Max.Selling.WebService.WebLoader.Model;
using Newtonsoft.Json.Linq;
using Vehicle = Max.Selling.WebService.WebLoader.ViewModels.Vehicle;
using System.Linq;

namespace Max.Selling.WebService.Controllers
{
    [AuthorizeUser]
    [AuthorizeDealer(AssertDealerUpgrades = "Merchandising", AssertDealerHasWebLoaderUpgrade = true)]
    public class WebLoaderVehicleController : ApiController
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IVehicleRepository _vehicleRepository;

        public WebLoaderVehicleController(IVehicleRepository vehicleRepository)
        {
            _vehicleRepository = vehicleRepository;
        }

        [AutoETag]
        public Vehicle Get(string dealer, string vehicle)
        {
            Log.DebugFormat("Vehicle GET: Dealer: {0}, Inv: {1}", dealer, vehicle);

            var inventoryId = ParseVehicleId(vehicle);

            return _vehicleRepository.GetVehicle(dealer, inventoryId).ToViewModel();
        }

        public HttpResponseMessage Put(HttpRequestMessage request, string dealer, string vehicle)
        {
            var json = request.Content.ReadAsStringAsync().Result;

            var inventoryId = ParseVehicleId(vehicle);

            Log.DebugFormat("Vehicle Put: Dealer: {0}, Inv: {1}, Json: {2}", dealer, vehicle, json);

            var inputModel = JObject.Parse(json);
            var inputVehicle = inputModel.ToObject<Vehicle>();
            var veh = _vehicleRepository.GetVehicle(dealer, inventoryId);

            CheckForConflicts(veh, request);

            UpdateVehicleInDatabase(inputModel, veh, inputVehicle);

            return ReturnCurrentVehicleFromDatabase(veh);
        }

        private static void CheckForConflicts(IVehicle veh, HttpRequestMessage request)
        {
            var sig = ETagHelper.CalculateSignature(veh.ToViewModel()).Result;

            if (ETagHelper.IfMatch(sig, request, true)) return;

            Log.ErrorFormat("Etags for put vehicle did not match! sig: {0}, passedSig: {1}", sig, request.Headers.IfMatch.First().Tag);
            var msg = new HttpResponseMessage(HttpStatusCode.PreconditionFailed);
            throw new HttpResponseException(msg);
        }

        private static HttpResponseMessage ReturnCurrentVehicleFromDatabase(IVehicle veh)
        {
            var response = new HttpResponseMessage(HttpStatusCode.OK)
                               {
                                   Content = new ObjectContent(typeof (Vehicle), veh.ToViewModel(),
                                                               new JsonMediaTypeFormatter())
                               };
            var sig = ETagHelper.CalculateSignature(response.Content).Result;
            ETagHelper.SetETagHeader(response, sig);
            return response;
        }

        private static void UpdateVehicleInDatabase(IDictionary<string, JToken> inputModel, IVehicle veh, Vehicle inputVehicle)
        {
            try
            {
                veh.Update(inputVehicle, name => IsPropertyDefined(inputModel, name));
            }
            catch (TrimNotSetException)
            {
                throw new HttpResponseException(
                    new HttpResponseMessage(HttpStatusCode.BadRequest));
            }
        }

        private static int ParseVehicleId(string vehicle)
        {
            int inventoryId;
            if (!Regex.IsMatch(vehicle, @"^-?\d{1,20}$") || !int.TryParse(vehicle, out inventoryId))
                throw new HttpResponseException(
                    new HttpResponseMessage(HttpStatusCode.BadRequest) {ReasonPhrase = "Invalid Vehicle Id"});
            return inventoryId;
        }

        private static bool IsPropertyDefined(IDictionary<string, JToken> model, string name)
        {
            return model.ContainsKey(name);
        }
    }
}