﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using FirstLook.Merchandising.DomainModel.Chrome;
using FirstLook.Merchandising.DomainModel.DataSource;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.Merchandising.DomainModel.VehicleVinData;
using Max.Selling.WebService.WebLoader.Filters;
using Max.Selling.WebService.WebLoader.ViewModels;
using Engine = FirstLook.Merchandising.DomainModel.VehicleVinData.Engine;
using System.Linq;

namespace Max.Selling.WebService.Controllers
{
    [AutoETag]
    [AuthorizeUser]
    public class WebLoaderTrimController : ApiController
    {
        private const string CountryCode = @"1_";

        private IStyleRepository _repository;

        public WebLoaderTrimController(IStyleRepository styleRepository)
        {
            _repository = styleRepository;
        }

        private static string CountryCodeId(int id)
        {
            return string.Format("{0}{1}", CountryCode, id);
        }

        private static void AddEquipment(List<Equipment> equipment, GenericChoiceCollection collection, string type)
        {
            foreach (GenericChoice exterior in collection)
                equipment.Add(new Equipment() { Category = exterior.Header, Desc = exterior.Description, Id = exterior.ID.ToString(), Standard = exterior.IsStandard, Type = type });
        }

        private static List<Equipment> GetEquipment(int trim)
        {
            var list = new List<Equipment>();
            var equipmentDataSource = new GenericChoicesDataSource();
            
            var exteriorEquipment = equipmentDataSource.Fetch(trim, 2);
            AddEquipment(list, exteriorEquipment, "Exterior");
            
            var internalEquipment = equipmentDataSource.Fetch(trim, 1);
            AddEquipment(list, internalEquipment, "Interior");
            
            return list;
        }

        private static List<SpecificEquipment> GetEngines(int trim)
        {
            var list = new List<SpecificEquipment>();
            var engines = EngineCollection.GetEngines(trim);

            foreach(Engine engine in engines)
                list.Add(new SpecificEquipment { Desc = engine.Description, Id = engine.CategoryId.ToString(), Standard = engine.IsStandard });

            return list;
        }

        private static List<SpecificEquipment> GetTransmissions(int trim)
        {
            var list = new List<SpecificEquipment>();
            var transmissions = TransmissionCollection.GetTransmissions(trim);

            foreach (Transmission transmission in transmissions)
                list.Add(new SpecificEquipment { Desc = transmission.Description, Id = transmission.CategoryId.ToString(), Standard = transmission.IsStandard });

            return list;
        }

        private static List<SpecificEquipment> GetDriveTrains(int trim)
        {
            var list = new List<SpecificEquipment>();
            var driveTrains = DrivetrainCollection.GetDrivetrains(trim);

            foreach (Drivetrain driveTrain in driveTrains)
                list.Add(new SpecificEquipment { Desc = driveTrain.Description, Id = driveTrain.CategoryId.ToString(), Standard = driveTrain.IsStandard });

            return list;
        }
        
        private static List<Option> GetOptions(int trim)
        {
            return EquipmentCollection.FetchOptional(trim, false)
                .Cast<VehicleDataAccess.Equipment>()
                .Select(option => new Option
                                      {
                                          Desc = option.Description,
                                          Id = option.OptionCode,
                                          Code = option.OptionCode,
                                          MSRP = option.MaxMSRP,
                                          DetailedDesc = string.IsNullOrEmpty(option.ExtDescription)
                                                             ? null
                                                             : option.ExtDescription
                                      })
                .ToList();
        }

        private static List<IntColor> GetInteriorColors(int trim)
        {
            var list = new List<IntColor>();
            var interiorColors = AvailableChromeColors.FetchInterior(trim);

            foreach (ChromeColor color in interiorColors)
            {
                string id = string.Format("c={0}&d={1}", color.ColorCode, Regex.Replace(color.Description, @"\s+", "+"));

                list.Add(new IntColor() { Desc = color.Description, Id = id });
            }

            return list;
        }

        private static List<ExtColor> GetExteriorColor(int trim)
        {
            var list = new List<ExtColor>();
            var exteriorColors = AvailableChromeColors.FetchExterior(trim);

            foreach (ColorPair color in exteriorColors)
            {
                var id = string.Format("c={0}&d={1}", color.First.ColorCode, Regex.Replace(color.First.Description, @"\s+", "+"));
                if(color.Second != null && !string.IsNullOrWhiteSpace(color.Second.ColorCode))
                {
                    id += string.Format("&c2={0}&d2={1}", color.Second.ColorCode,
                                        Regex.Replace(color.Second.Description, @"\s+", "+"));
                }
                list.Add(new ExtColor() { Desc = color.PairDescription, Id = id, Simple = color.First.GenericColor });
            }

            return list;
        }
        

        public Trim Get(string trim)
        {
            trim = trim.Replace(CountryCode, string.Empty);
            int trimValue = int.Parse(trim);

            var trimModel = new Trim();
            trimModel.AvailableEquipment = GetEquipment(trimValue);
            trimModel.AvailableEngines = GetEngines(trimValue);
            trimModel.AvailableTransmissions = GetTransmissions(trimValue);
            trimModel.AvailableDrivetrains = GetDriveTrains(trimValue);
            trimModel.AvailableOptions = GetOptions(trimValue);
            trimModel.AvailableExtColors = GetExteriorColor(trimValue);
            trimModel.AvailableIntColors = GetInteriorColors(trimValue);

            trimModel.Id = CountryCodeId(trimValue);

            var repositoryStyle = _repository.GetStyleById(new[] {trimValue}).Single();
            trimModel.Desc = repositoryStyle.StyleName;
            trimModel.Make = repositoryStyle.Model.ManufacturerName;
            trimModel.Model = repositoryStyle.Model.ModelName;
            trimModel.Year = repositoryStyle.Model.ModelYear ?? 0;

            return trimModel;
        }
    }
}