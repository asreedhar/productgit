﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using Max.Selling.WebService.WebLoader.Filters;
using Max.Selling.WebService.WebLoader.ViewModels;

namespace Max.Selling.WebService.Controllers
{
    [AutoETag]
    [AuthorizeUser]
    public class WebLoaderDealersController : ApiController
    {
        private readonly IUserServices _userServices;

        public WebLoaderDealersController(IUserServices userServices)
        {
            _userServices = userServices;
        }

        public List<Dealer> Get()
        {
            var comparer = StringComparer.CurrentCultureIgnoreCase;
            return 
                _userServices.Current
                .GetDealers()
                .Where(d => d.HasWebLoader && d.HasUpgrade(Upgrade.Merchandising))
                .OrderBy(d => d.Name, comparer).ThenBy(d => d.Code, comparer)
                .Select(d => new Dealer {Id = d.Code, Desc = d.Name})
                .ToList();
        }
    }
}