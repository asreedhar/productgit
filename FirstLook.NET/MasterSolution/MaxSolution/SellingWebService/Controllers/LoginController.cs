﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using FirstLook.Common.Core.Command;
using Max.Selling.Domain;
using CommandFactory = Max.Selling.Domain.CommandFactory;
using ICommandFactory = Max.Selling.Domain.ICommandFactory;

namespace Max.Selling.WebService.Controllers
{
    public class LoginController : Controller
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private ICommandFactory _commandFactory;

        public LoginController() : this(new CommandFactory())
        {
        }

        public LoginController(ICommandFactory commandFactory)
        {
            _commandFactory = commandFactory;
        }

        [HttpPost]
        public ActionResult CreateTicket(string userName, string password)
        {
            Log.DebugFormat("CreateTicket called for {0}", userName);

            var tokenCommand = _commandFactory.CreateTokenCommand(userName, password);
            AbstractCommand.DoRun(tokenCommand);

            if(tokenCommand.ErrorInfo.HasError)
                Log.Error("CreateTicket is returning with error");

            return new ContentResult() { Content = tokenCommand.Xml, ContentEncoding = Encoding.UTF8, ContentType = "text/xml" };
        }
    }
}
