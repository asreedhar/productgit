﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Http;
using FirstLook.Merchandising.DomainModel.Chrome;
using FirstLook.Merchandising.DomainModel.Chrome.Mapper;
using FirstLook.Merchandising.DomainModel.Chrome.Model;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using FirstLook.Merchandising.DomainModel.Workflow;
using Max.Selling.WebService.WebLoader.Filters;
using Max.Selling.WebService.WebLoader.ViewModels;

namespace Max.Selling.WebService.Controllers
{
    [AutoETag]
    [AuthorizeUser]
    [AuthorizeDealer(AssertDealerUpgrades = "Merchandising", AssertDealerHasWebLoaderUpgrade = true)]
    public class WebLoaderVehiclesController : ApiController
    {
        private readonly IDealerServices _dealerServices;
        private readonly IInventoryDataRepository _inventoryDataRepository;
        private readonly IChromeMapper _chromeMapper;
        private readonly IVinPatternRepository _vinPatternRepository;
        private readonly IStyleRepository _styleRepository;

        public WebLoaderVehiclesController(IDealerServices dealerServices, IInventoryDataRepository inventoryDataRepository,
            IChromeMapper chromeMapper, IVinPatternRepository vinPatternRepository, IStyleRepository styleRepository)
        {
            _dealerServices = dealerServices;
            _inventoryDataRepository = inventoryDataRepository;
            _chromeMapper = chromeMapper;
            _vinPatternRepository = vinPatternRepository;
            _styleRepository = styleRepository;
        }

        public List<VehicleSummary> Get(string dealer)
        {
            var dealerObj = _dealerServices.GetDealerByCode(dealer);

            var inventory = ToList(_inventoryDataRepository.GetAllInventory(dealerObj.Id));

            var vinPatterns = GetVinPatternRecordsForInventory(inventory);

            var styles = GetActiveStylesForInventory(inventory);

            return inventory
                .Select(i => new VehicleSummary
                                 {
                                     Id = i.InventoryID,
                                     StockNum = i.StockNumber ?? "",
                                     Vin = i.VIN ?? "",
                                     PhotoCount = i.PhotoCount,
                                     Year = TryParseYear(i.Year),
                                     Make = i.Make ?? "",
                                     Model = i.Model ?? "",
                                     LowPhotos = WorkflowFilters.LowPhotos(i),
                                     NoPhotos = WorkflowFilters.NoPhotos(i),
                                     NoEquipment = WorkflowFilters.EquipmentNeeded(i),
                                     AvailableTrimIds = GetTrimIdsForVin(vinPatterns, i.VIN),
                                     Trim = GetActiveTrim(styles, i, GetTrimIdsForVinInteger(vinPatterns, i.VIN)),
                                     ExtColor = GetActiveExt1Color(styles, i, GetTrimIdsForVinInteger(vinPatterns, i.VIN))
                                 })
                .ToList();
        }

        private Dictionary<int, IVinPattern> GetVinPatternRecordsForInventory(IEnumerable<IInventoryData> inventory)
        {
            return _vinPatternRepository.GetVinPatternById(
                inventory
                    .Select(i => _chromeMapper.LookupVinPatternIdByVin(i.VIN))
                    .Where(vpid => vpid.HasValue)
                    .Select(vpid => vpid.GetValueOrDefault())
                    )
                .ToDictionary(vp => vp.VINPatternID);
        }

        private IList<int> GetTrimIdsForVinInteger(IDictionary<int, IVinPattern> vinPatterns, string vin)
        {
            var vinPatternId = _chromeMapper.LookupVinPatternIdByVin(vin);
            if (vinPatternId.HasValue)
            {
                IVinPattern vinPattern;
                vinPatterns.TryGetValue(vinPatternId.Value, out vinPattern);
                if (vinPattern != null)
                    return vinPattern.ChromeStyleIds;
            }

            return new List<int>();
        }

        private List<string> GetTrimIdsForVin(IDictionary<int, IVinPattern> vinPatterns, string vin)
        {
            var result = new List<string>();
            var vinPatternId = _chromeMapper.LookupVinPatternIdByVin(vin);
            if(vinPatternId.HasValue)
            {
                IVinPattern vinPattern;
                vinPatterns.TryGetValue(vinPatternId.Value, out vinPattern);
                if(vinPattern != null)
                    result.AddRange(vinPattern.ChromeStyleIds.Select(styleId => "1_" + styleId.ToString(CultureInfo.InvariantCulture)));
            }
            return result;
        }

        private Dictionary<int, Style> GetActiveStylesForInventory(IEnumerable<IInventoryData> inventory)
        {
            return _styleRepository.GetStyleById(
                inventory
                    .Select(i => i.ChromeStyleId.GetValueOrDefault(-1))
                    .Where(styleId => styleId != -1))
                .ToDictionary(s => s.StyleID);
        }

        private static Ref GetActiveTrim(IDictionary<int, Style> styles, IInventoryData item, IEnumerable<int> availableChromeStyles)
        {
            var styleId = GetStyleId(item, availableChromeStyles);
            var style = GetStyle(styles, styleId);
            return Ref.FromStyle(styleId, style);
        }

        private static ExtColorRef GetActiveExt1Color(IDictionary<int, Style> styles, IInventoryData data, IEnumerable<int> availableChromeStyles)
        {
            var color = GetColor(styles, data, availableChromeStyles);
            if (color != null)
            {
                return new ColorHelper(color).ToExtRef();
            }

            if (!string.IsNullOrEmpty(data.Ext1Desc) || !string.IsNullOrEmpty(data.Ext2Desc))
            {
                return new ColorHelper(data.Ext1Code, data.Ext1Desc, data.Ext2Code, data.Ext2Desc).ToExtRef();
            }
            
            return null;
        }

        private static IExteriorColor GetColor(IDictionary<int, Style> styles, IInventoryData data, IEnumerable<int> availableChromeStyles)
        {
            var styleId = GetStyleId(data, availableChromeStyles);
            var style = GetStyle(styles, styleId);
            return style == null
                       ? null
                       : style.GetExteriorColors().FirstOrDefault(data.Ext1Code, data.Ext1Desc, data.Ext2Code, data.Ext2Desc);
        }

        private static Style GetStyle(IDictionary<int, Style> styles, int? styleId)
        {
            if (styleId == null)
                return null;

            Style style;
            styles.TryGetValue(styleId.Value, out style);
            return style;
        }

        private static int? GetStyleId(IInventoryData data, IEnumerable<int> availableChromeStyles)
        {
            int? chromeStyleId = data.ChromeStyleId.GetValueOrDefault(-1) == -1
                       ? null
                       : data.ChromeStyleId;

            if (!chromeStyleId.HasValue)
                return chromeStyleId;

            //the selected chrome has to be one of the available ones
            if (availableChromeStyles.Contains(chromeStyleId.Value))
                return chromeStyleId;

            return null;
        }

        private static IList<IInventoryData> ToList(IEnumerable<IInventoryData> inventory)
        {
            var list = inventory as IList<IInventoryData>;
            return list ?? inventory.ToList();
        }

        private static int TryParseYear(string year)
        {
            int result;
            int.TryParse(year, out result);
            return result;
        }
    }
}