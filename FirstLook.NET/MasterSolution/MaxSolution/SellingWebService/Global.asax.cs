﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Max.Selling.WebService.App_Start;
using MvcMiniProfiler;

namespace Max.Selling.WebService
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            MiniProfilerConfig.ConfigureStorage();
            LogConfig.RegisterLogging();
            IOCConfig.RegisterTypes(GlobalConfiguration.Configuration);
            AreaRegistration.RegisterAllAreas();
            ModelBinderConfig.Register(ModelBinders.Binders);
            MessageHandlerConfig.Register(GlobalConfiguration.Configuration.MessageHandlers);
            FilterConfig.RegisterGlobalFilters(GlobalConfiguration.Configuration.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        protected void Application_EndRequest()
        {
            MiniProfiler.Stop();
        }
    }
}