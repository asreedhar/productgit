﻿using System;
using System.Xml;

namespace Max.Selling.Common
{
    public class ErrorInformation
    {
        public static readonly ErrorInformation NoError = new ErrorInformation();

        private const string OK = "ok";

        public string Message { get; private set; }
        internal ErrorCode ErrorCode { get; private set; }
        public bool HasError { get; private set; }

        public ErrorInformation()
        {
            Message = OK;
            ErrorCode = ErrorCode.NoError;
            HasError = false;
        }

        public ErrorInformation(string message) : this(ErrorCode.GeneralError, message)
        {
        }

        public ErrorInformation(Exception e): this(ErrorCode.GeneralError, e.Message)
        {
        }

        public ErrorInformation(ErrorCode code, string message)
        {
            Message = message;
            ErrorCode = code;
            HasError = true;
        }

        public ErrorInformation ProcessNewError(ErrorInformation newError)
        {
            return HasError ? this : newError;
        }

        public XmlNode CreateErrorNode(XmlDocument doc)
        {
            var errorNode = doc.CreateElement("status");
            var attribute = doc.CreateAttribute("error");
            attribute.Value = HasError.ToString().ToLower();

            var errorCodeAttribute = doc.CreateAttribute("errorcode");
            errorCodeAttribute.Value = ((int) ErrorCode).ToString();

            errorNode.InnerText = Message;
            errorNode.Attributes.Append(attribute);
            errorNode.Attributes.Append(errorCodeAttribute);

            return errorNode;
        }

        public string CreateStatusXml()
        {
            var doc = new XmlDocument();
            doc.AppendChild(CreateErrorNode(doc));
            return doc.InnerXml;
        }

    }
}
