﻿namespace Max.Selling.Common
{
    public enum ErrorCode
    {
        NoError = 0,
        GeneralError = 1
    }
}
