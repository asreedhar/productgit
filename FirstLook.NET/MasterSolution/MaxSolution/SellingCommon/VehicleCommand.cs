﻿using System;
using System.Data;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Data;

namespace Max.Selling.Common
{
    public class VehicleCommand : AbstractCommand
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private ErrorInformation _errorInfo;

        private int _inventoryID;
        private int _inventoryType;
        private int _businessUnitID;
        private string _vin;

        public VehicleCommand(int businessUnitID, string vin)
        {
            _businessUnitID = businessUnitID;
            _vin = vin;
        }

        protected override void Run()
        {
            _errorInfo = ErrorInformation.NoError;

            try
            {
                using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection("IMT"))
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    using (IDataCommand command = new DataCommand((connection.CreateCommand())))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "Marketing.GetInventoryID";
                        command.AddParameterWithValue("businessUnitID", DbType.Int32, false, _businessUnitID);
                        command.AddParameterWithValue("vin", DbType.String, false, _vin);

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            if(!reader.Read())
                                throw new InvalidOperationException("No VIN found");
                            
                            _inventoryID = reader.GetInt32(reader.GetOrdinal("InventoryID"));
                            _inventoryType = reader.GetByte(reader.GetOrdinal("InventoryType"));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Failed to get InventoryID for BusinessUnitID = {0}, VIN = {1}", _businessUnitID, _vin), e);
                _errorInfo = new ErrorInformation(e);
            }
        }

        public ErrorInformation ErrorInfo
        {
            get
            {
                return _errorInfo;
            }
        }

        public int InventoryID
        {
            get
            {
                return _inventoryID;
            }
        }

        public bool IsUsed
        {
            get { return _inventoryType == 2; }
        }
    }
}
