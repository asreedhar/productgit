﻿using System;

namespace Max.WebImport.Domain.TransferObjects.V_1_0
{
    [Serializable]
    public class Status
    {
        public static Status Ok = new Status() { Code = 100, Description = "Ok" };
        public static Status Unauthorized = new Status() { Code = 400, Description = "Unauthorized" };
        public static Status PermissionDenied = new Status() { Code = 401, Description = "Permission Denied" };
        public static Status InvalidParameter = new Status() { Code = 402, Description = "Invalid Parameter" };
        public static Status InternalError = new Status() { Code = 500, Description = "Internal Error" };
        public static Status NewCarsNotSupported = new Status() { Code = 403, Description = "New car pricing not supported" };

        public int Code { get; set; }
        public string Description { get; set;}
    }
}
