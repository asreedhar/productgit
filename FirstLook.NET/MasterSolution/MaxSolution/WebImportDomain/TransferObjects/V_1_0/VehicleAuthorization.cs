﻿using System;

namespace Max.WebImport.Domain.TransferObjects.V_1_0
{
    [Serializable]
    public class VehicleAuthorization
    {
        public int BusinessUnitID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

    }
}
