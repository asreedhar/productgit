﻿using System;
using System.Data;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Data;
using Max.Selling.Common;

namespace Max.WebImport.Domain
{
    public class ProviderAuthorizationCommand : AbstractCommand
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string _userName;
        private string _password;
        
        public ProviderAuthorizationCommand(string userName, string password)
        {
            _userName = userName;
            _password = password;
            Authorized = false;
            ProviderID = -1;
            ProviderName = String.Empty;
            ErrorInfo = ErrorInformation.NoError;
        }

        public bool Authorized { get; private set; }
        public int ProviderID { get; private set; }
        public string ProviderName { get; private set; }
        public ErrorInformation ErrorInfo { get; private set; }

        protected override void Run()
        {
            try
            {
                string dataPassword = string.Empty;

                using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection("IMT"))
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    using (IDataCommand command = new DataCommand((connection.CreateCommand())))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "WebImport.Provider#Fetch";
                        command.AddParameterWithValue("userName", DbType.String, false, _userName);

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                dataPassword = reader.GetString(reader.GetOrdinal("Password"));
                                ProviderID = reader.GetInt32(reader.GetOrdinal("ProviderID"));
                                ProviderName = reader.GetString(reader.GetOrdinal("ProviderName"));
                            }
                        }
                    }
                }

                Authorized = (_password == dataPassword);
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Failed to get Account information for UserName = {0}", _userName), e);
                ErrorInfo = new ErrorInformation(e);
            }
        }
    }
}
