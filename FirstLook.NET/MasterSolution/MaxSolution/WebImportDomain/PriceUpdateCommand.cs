﻿using System;
using FirstLook.Common.Core.Command;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel;
using Max.Selling.Common;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;

namespace Max.WebImport.Domain
{
    public class PriceUpdateCommand : AbstractCommand
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const int MinPrice = 0;
        private const int MaxPrice = 999999;

        private const int WebServiceSource = 5;

        private int _businessUnitID;
        private int _inventoryID;
        private int _newPrice;
        private string _providerName;

        public PriceUpdateCommand(int businessUnitID, int inventoryID, int newPrice, string providerName)
        {
            _businessUnitID = businessUnitID;
            _inventoryID = inventoryID;
            _newPrice = newPrice;
            _providerName = providerName;
            ErrorInfo = ErrorInformation.NoError;
        }

        public ErrorInformation ErrorInfo { get; private set; }

        public static bool IsPriceValid(int newPrice)
        {
            return newPrice >= MinPrice && newPrice <= MaxPrice;
        }

        protected override void Run()
        {
            try
            {
                var owner = FirstLook.DomainModel.Oltp.Owner.GetOwner(_businessUnitID);
                string vehicleHandle = string.Format("1{0}", _inventoryID);
                var inventory = Inventory.GetInventory(owner.Handle, vehicleHandle);

                if (inventory.ListPrice.HasValue)
                {
                    InventoryRepriceCommand.Execute(
                        _providerName,
                        _inventoryID,
                        inventory.ListPrice.Value,
                        _newPrice,
                        false,
                        WebServiceSource);
                }   
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Failed to update price for BusinessUnitID = {0}, InventoryID = {1}", _businessUnitID, _inventoryID), e);
                ErrorInfo = new ErrorInformation(e);
            }

        }
    }
}
