﻿using System;
using System.Data;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Data;
using Max.Selling.Common;

namespace Max.WebImport.Domain
{
    public class AccountCommand : AbstractCommand
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private int _businessUnitID;
        private ActionType _actionType;
        private int _providerID;

        public AccountCommand(int businessUnitID, ActionType actionType, int providerID)
        {
            _businessUnitID = businessUnitID;
            _actionType = actionType;
            _providerID = providerID;

            Authorized = false;
            ErrorInfo = ErrorInformation.NoError;
        }

        public bool Authorized { get; private set; }
        public ErrorInformation ErrorInfo { get; private set; }

        protected override void Run()
        {
            try
            {
                int dataProviderID = -1;
                bool dataActive = false;

                using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection("IMT"))
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    using (IDataCommand command = new DataCommand((connection.CreateCommand())))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "WebImport.Account#Fetch";
                        command.AddParameterWithValue("businessUnitID", DbType.Int32, false, _businessUnitID);
                        command.AddParameterWithValue("actionType", DbType.Int32, false, _actionType);

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                dataProviderID = reader.GetInt32(reader.GetOrdinal("ProviderID"));
                                dataActive = reader.GetBoolean(reader.GetOrdinal("Active"));
                            }
                        }
                    }
                }

                Authorized = (dataProviderID == _providerID && dataActive);
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Failed to get Provider information for BusinessUnitID = {0}, ActionType = {1}", _businessUnitID, _actionType), e);
                ErrorInfo = new ErrorInformation(e);
            }
        }

    }
}
