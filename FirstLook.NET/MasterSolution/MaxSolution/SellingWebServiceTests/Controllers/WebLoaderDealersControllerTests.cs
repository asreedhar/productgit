﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using Max.Selling.WebService.Controllers;
using Moq;
using NUnit.Framework;

namespace SellingWebServiceTests.Controllers
{
    [TestFixture]
    public class WebLoaderDealersControllerTests
    {
        private Mock<IUser> _userMock;
        private IUser _user;
        private Mock<IUserServices> _userServicesMock;
        private IUserServices _userServices;

        [SetUp]
        public void Setup()
        {
            _userMock = new Mock<IUser>();
            _user = _userMock.Object;

            _userServicesMock = new Mock<IUserServices>();
            _userServices = _userServicesMock.Object;
        }

        private static IDealer MakeDealer(int id, string code, string name, bool hasMerchandisingUpgrade = true, bool hasWebLoaderEnabled = true)
        {
            var dealerMock = new Mock<IDealer>();
            dealerMock.Setup(d => d.Id).Returns(id);
            dealerMock.Setup(d => d.Code).Returns(code);
            dealerMock.Setup(d => d.Name).Returns(name);
            if (hasMerchandisingUpgrade)
                dealerMock.Setup(d => d.HasUpgrade(Upgrade.Merchandising)).Returns(true);
            if (hasWebLoaderEnabled)
                dealerMock.Setup(d => d.HasWebLoader).Returns(true);
            return dealerMock.Object;
        }
        
        private IEnumerable<IDealer> MakeDealers()
        {
            return new[]
                       {
                           MakeDealer(105239, "WINDYCIT05", "Windy City BMW"),
                           MakeDealer(101732, "ALLSTARC01", "All Star Chevrolet"),
                           MakeDealer(103569, "VOSSCHEV02", "Voss Chevrolet")
                       };
        }

        [Test]
        public void Get_should_return_list_of_dealers_the_current_user_has_access_to()
        {
            _userServicesMock.Setup(us => us.Current).Returns(_user);
            _userMock.Setup(u => u.GetDealers()).Returns(MakeDealers);

            var controller = new WebLoaderDealersController(_userServices);

            var result = controller.Get();

            Assert.That(result.Select(d => d.Id).ToArray(), Is.EquivalentTo(new[] { "WINDYCIT05", "ALLSTARC01", "VOSSCHEV02" }));
        }

        [Test]
        public void Get_should_return_dealer_list_that_populates_the_Name_property()
        {
            _userServicesMock.Setup(us => us.Current).Returns(_user);
            _userMock.Setup(u => u.GetDealers()).Returns(MakeDealers);

            var controller = new WebLoaderDealersController(_userServices);

            var result = controller.Get();

            Assert.That(result.Select(d => d.Desc).ToArray(), Is.EquivalentTo(new[] { "Windy City BMW", "All Star Chevrolet", "Voss Chevrolet" }));
        }

        [Test]
        public void Get_should_return_list_of_dealers_ordered_by_name()
        {
            _userServicesMock.Setup(us => us.Current).Returns(_user);
            _userMock.Setup(u => u.GetDealers()).Returns(MakeDealers);

            var controller = new WebLoaderDealersController(_userServices);

            var result = controller.Get();

            Assert.That(result.Select(d => d.Id).ToArray(), Is.EqualTo(new[] { "ALLSTARC01", "VOSSCHEV02", "WINDYCIT05" }));
        }

        [Test]
        public void Get_should_return_only_dealers_that_have_the_Merchandising_upgrade()
        {
            _userServicesMock.Setup(us => us.Current).Returns(_user);
            _userMock.Setup(u => u.GetDealers())
                .Returns(new[]
                       {
                           MakeDealer(105239, "WINDYCIT05", "Windy City BMW", false),
                           MakeDealer(101732, "ALLSTARC01", "All Star Chevrolet"),
                           MakeDealer(103569, "VOSSCHEV02", "Voss Chevrolet")
                       });

            var controller = new WebLoaderDealersController(_userServices);

            var result = controller.Get();

            Assert.That(result.Select(d => d.Id).ToArray(), Is.EqualTo(new[] { "ALLSTARC01", "VOSSCHEV02" }));
        }

        [Test]
        public void Get_should_return_only_dealers_that_have_the_WebLoader_upgrade()
        {
            _userServicesMock.Setup(us => us.Current).Returns(_user);
            _userMock.Setup(u => u.GetDealers())
                .Returns(new[]
                       {
                           MakeDealer(105239, "WINDYCIT05", "Windy City BMW"),
                           MakeDealer(101732, "ALLSTARC01", "All Star Chevrolet", hasWebLoaderEnabled: false),
                           MakeDealer(103569, "VOSSCHEV02", "Voss Chevrolet")
                       });

            var controller = new WebLoaderDealersController(_userServices);

            var result = controller.Get();

            Assert.That(result.Select(d => d.Id).ToArray(), Is.EqualTo(new[] { "VOSSCHEV02", "WINDYCIT05" }));
        }
    }
}