﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using Core.Messaging;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using FirstLook.Merchandising.DomainModel.DataAccess;
using FirstLook.Merchandising.DomainModel.Vehicles;
using Max.Selling.WebService.Controllers;
using Merchandising.Messages;
using Merchandising.Messages.WebLoader;
using Moq;
using NUnit.Framework;

namespace SellingWebServiceTests.Controllers
{
    [TestFixture]
    public class WebLoaderVehiclePhotosControllerTests
    {

        [Test]
        public void FileStoredAndMessageSent()
        {
            var queueFactoryMock = new Mock<IQueueFactory>();
            var fileStorageFactoryMock = new Mock<IFileStoreFactory>();
            var userServicesMock = new Mock<IUserServices>();
            var userMock = new Mock<IUser>();
            var photoRepositoryoMock = new Mock<IWebLoaderPhotoRepository>();

            //setup userServiceMock
            userMock.Setup(x => x.FirstName).Returns("Brock");
            userMock.Setup(x => x.LastName).Returns("Reeve");
            userMock.Setup(x => x.UserName).Returns("breeve");
            userServicesMock.Setup(x => x.Current).Returns(userMock.Object);

            //testing params
            string date = DateTime.UtcNow.ToString("yyyyMMddTHHmmssZ");
            Guid guid = Guid.NewGuid();
            byte[] bits = new byte[] { 1 };
            string dealerCode = "WINDYCIT05";
            int inventoryId = 20771387;
            int businessUnitid = 105239;
            string vin = "5UXZV4C58CL000014";


            //photoRepositorystep
            photoRepositoryoMock.Setup(x => x.Exists(inventoryId, guid)).Returns(false);
            //photoRepositoryoMock.Setup(x => x.Insert(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<Guid>(), It.IsAny<Guid>(), It.IsAny<String>())).Returns(1);

            //setup dealerServices
            var dealerMock = new Mock<IDealer>();
            dealerMock.Setup(x => x.Id).Returns(businessUnitid);

            //setup queues mocks
            var fileUploadQueueMock = new Mock<IQueue<PhotoUploadMessage>>();
            fileUploadQueueMock.Setup(x => x.SendMessage(It.IsAny<PhotoUploadMessage>(), It.IsAny<String>()))
                .Callback((PhotoUploadMessage m, string sentFrom) =>
                              {
                                  //assert message sent is corrent
                                  Assert.IsTrue(m.FirstName == "Brock");
                                  Assert.IsTrue(m.LastName == "Reeve");
                                  Assert.IsTrue(m.User == "breeve");
                                  Assert.IsTrue(m.T == date);
                                  Assert.IsTrue(m.Guid == guid);
                                  Assert.IsTrue(m.S3FileName == (guid + ".jpg"));
                                  Assert.IsTrue(m.Dealercode == dealerCode);
                                  Assert.IsTrue(m.Vin == vin);
                                  Assert.IsTrue(m.Lock);
                              }
                            );
            queueFactoryMock.Setup(x => x.CreateWebLoaderPhotoUpload()).Returns(fileUploadQueueMock.Object);
            
            //Set up File Storage mocks
            var fileStore = new Mock<IFileStorage>();
            fileStore.Setup(x => x.Write(It.IsAny<string>(), It.IsAny<Stream>()))
                .Callback((string filename, Stream stream) =>
                              {
                                  /*assert bits are equal in stream*/
                                  BinaryReader reader = new BinaryReader(stream);
                                  byte[] bytes = reader.ReadBytes((int)stream.Length);
                                  Assert.IsTrue(bytes.Length == 1);
                                  Assert.IsTrue(bytes[0] == bits[0]);
                              });
            fileStorageFactoryMock.Setup(x => x.WebLoaderApiPhotos()).Returns(fileStore.Object);

            var controller = new WebLoaderVehiclePhotosController(photoRepositoryoMock.Object, fileStorageFactoryMock.Object, queueFactoryMock.Object, userServicesMock.Object);
            controller.Request = new HttpRequestMessage();
            controller.Request.Content = new ByteArrayContent(bits);
            var result = controller.Post(dealerCode, inventoryId, Guid.NewGuid(), 1, guid, date, true);

            Assert.IsTrue(result.StatusCode == HttpStatusCode.NoContent);
        }
    }
}
