﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.Merchandising.DomainModel.Chrome;
using FirstLook.Merchandising.DomainModel.Chrome.Mapper;
using FirstLook.Merchandising.DomainModel.Chrome.Model;
using FirstLook.Merchandising.DomainModel.Core;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using Max.Selling.WebService.Controllers;
using Moq;
using NUnit.Framework;

namespace SellingWebServiceTests.Controllers
{
    [TestFixture]
    public class WebLoaderVehiclesControllerTests
    {
        private Mock<IDealerServices> _dealerServicesMock;
        private IDealerServices _dealerServices;
        private Mock<IInventoryDataRepository> _inventoryDataRepositoryMock;
        private IInventoryDataRepository _inventoryDataRepository;
        private Mock<IDealer> _dealerMock;
        private IDealer _dealer;
        private Mock<IChromeMapper> _chromeMapperMock;
        private IChromeMapper _chromeMapper;
        private Mock<IVinPatternRepository> _vinPatternRepositoryMock;
        private IVinPatternRepository _vinPatternRepository;
        private Dictionary<int, IVinPattern> _vinPatterns;
        private Mock<IStyleRepository> _styleRepositoryMock;
        private IStyleRepository _styleRepository;
        private Dictionary<int, Style> _styles;

        [SetUp]
        public void Setup()
        {
            _dealerMock = new Mock<IDealer>();
            _dealer = _dealerMock.Object;
            _dealerMock.Setup(d => d.Id).Returns(105239);

            _dealerServicesMock = new Mock<IDealerServices>();
            _dealerServices = _dealerServicesMock.Object;
            _dealerServicesMock.Setup(ds => ds.GetDealerByCode("windycit05")).Returns(_dealer);

            _chromeMapperMock = new Mock<IChromeMapper>();
            _chromeMapper = _chromeMapperMock.Object;
            _chromeMapperMock.Setup(cm => cm.LookupVinPatternIdByVin("1HGEJ6571WL983856")).Returns(16781);
            _chromeMapperMock.Setup(cm => cm.LookupVinPatternIdByVin("4T1BG22K31U983888")).Returns(18919);
            _chromeMapperMock.Setup(cm => cm.LookupVinPatternIdByVin("KNAGD128145983904")).Returns(30247);
            _chromeMapperMock.Setup(cm => cm.LookupVinPatternIdByVin("WDBBA48D4JA083852")).Returns(3);

            _vinPatterns = new Dictionary<int, IVinPattern>
                {
                    {16781, MakeVinPattern(16781, new[] {105308})},
                    {18919, MakeVinPattern(18919, new[] {4735, 4734, 4733, 4726, 4725, 4724, 4723, 4722, 4721, 4720, 4719, 4718})},
                    {30247, MakeVinPattern(30247, new[] {262102, 262104})},
                    {3, MakeVinPattern(3, new[] {332735})},
                };

            _vinPatternRepositoryMock = new Mock<IVinPatternRepository>();
            _vinPatternRepository = _vinPatternRepositoryMock.Object;
            _vinPatternRepositoryMock.Setup(vpr => vpr.GetVinPatternById(It.IsAny<IEnumerable<int>>()))
                .Returns<IEnumerable<int>>(ids => 
                    ids
                        .Distinct()
                        .Where(id => _vinPatterns.ContainsKey(id))
                        .Select(id => _vinPatterns[id]));

            _styleRepositoryMock = new Mock<IStyleRepository>();
            _styleRepository = _styleRepositoryMock.Object;
            _styles = new Dictionary<int, Style>
                {
                    {105308, MakeStyle(105308, "4dr Sdn Manual", "LX")},
                    {262104, MakeStyle(262104, "4dr Sdn Auto V6", "EX")}
                };
            _styleRepositoryMock.Setup(r => r.GetStyleById(It.IsAny<IEnumerable<int>>()))
                .Returns<IEnumerable<int>>(
                    ids => ids.Select(id => _styles.ContainsKey(id) ? _styles[id] : null).Where(sty => sty != null));

            _inventoryDataRepositoryMock = new Mock<IInventoryDataRepository>();
            _inventoryDataRepository = _inventoryDataRepositoryMock.Object;
            _inventoryDataRepositoryMock.Setup(idr => idr.GetAllInventory(105239))
                .Returns<int>(dealerId => MakeInventoryItems());
        }

        private static IEnumerable<IInventoryData> MakeInventoryItems()
        {
            return new[]
                       {
                           MakeInventory(5, "P1255", "1HGEJ6571WL983856", 37, "1998", "Honda", "Civic", styleId: 105308),
                           MakeInventory(15, "S555-22", "4T1BG22K31U983888", 3, "2001", "Toyota", "Camry", styleId: -1),
                           MakeInventory(25, "T5533", "KNAGD128145983904", 0, "2004", "Kia", "Optima", styleId: 262104)
                       };
        }

        private static IVinPattern MakeVinPattern(int id, IList<int> styleIds)
        {
            var mock = new Mock<IVinPattern>();
            mock.Setup(vp => vp.VINPatternID).Returns(id);
            mock.Setup(vp => vp.ChromeStyleIds).Returns(styleIds);
            return mock.Object;
        }

        private static Style MakeStyle(int styleId, string styleNameWOTrim, string trim)
        {
            return new Style(1, styleId, styleNameWOTrim: styleNameWOTrim, trim: trim, colors: new Color[0]);
        }

        private static IInventoryData MakeInventory(int inventoryId = 0, string stockNum = "", string vin = "",
            int photoCount = 0,
            string year = "2000", string make = "Ford", string model = "F-150",
            bool needsPhotos = false, int desiredPhotoCount = 5, bool needsInterior = false, bool needsExterior = false,
            int? styleId = null, string ext1Desc = null, string ext1Code = null)
        {
            var mock = new Mock<IInventoryData>();
            mock.Setup(d => d.InventoryID).Returns(inventoryId);
            mock.Setup(d => d.StockNumber).Returns(stockNum);
            mock.Setup(d => d.VIN).Returns(vin);
            mock.Setup(d => d.PhotoCount).Returns(photoCount);
            mock.Setup(d => d.Year).Returns(year);
            mock.Setup(d => d.Make).Returns(make);
            mock.Setup(d => d.Model).Returns(model);
            mock.Setup(d => d.NeedsPhotos).Returns(needsPhotos);
            mock.Setup(d => d.DesiredPhotoCount).Returns(desiredPhotoCount);
            mock.Setup(d => d.NeedsInterior).Returns(needsInterior);
            mock.Setup(d => d.NeedsExterior).Returns(needsExterior);
            mock.Setup(d => d.ChromeStyleId).Returns(styleId);
            mock.Setup(d => d.Ext1Desc).Returns(ext1Desc);
            mock.Setup(d => d.Ext1Code).Returns(ext1Code);

            return mock.Object;
        }

        [Test]
        public void Get_should_return_inventory_items_with_Id_populated()
        {
            var controller = new WebLoaderVehiclesController(_dealerServices, _inventoryDataRepository, _chromeMapper, _vinPatternRepository, _styleRepository);

            var result = controller.Get("windycit05");

            Assert.That(result.Select(i => i.Id).ToArray(), Is.EquivalentTo(new[] {5, 15, 25}));
        }

        [Test]
        public void Get_should_return_inventory_items_with_StockNum_populated()
        {
            var controller = new WebLoaderVehiclesController(_dealerServices, _inventoryDataRepository, _chromeMapper, _vinPatternRepository, _styleRepository);

            var result = controller.Get("windycit05");

            Assert.That(result.Select(i => i.StockNum).ToArray(), Is.EquivalentTo(new[] { "P1255", "S555-22", "T5533" }));
        }

        [Test]
        public void Get_should_not_return_null_StockNum()
        {
            _inventoryDataRepositoryMock.Setup(r => r.GetAllInventory(105239))
                .Returns(new[] {MakeInventory(stockNum: null)});

            var controller = new WebLoaderVehiclesController(_dealerServices, _inventoryDataRepository, _chromeMapper, _vinPatternRepository, _styleRepository);

            var result = controller.Get("windycit05");

            Assert.That(result.Single().StockNum, Is.EqualTo(""));
        }

        [Test]
        public void Get_should_return_inventory_items_with_Vin_populated()
        {
            var controller = new WebLoaderVehiclesController(_dealerServices, _inventoryDataRepository, _chromeMapper, _vinPatternRepository, _styleRepository);

            var result = controller.Get("windycit05");

            Assert.That(result.Select(i => i.Vin).ToArray(),
                        Is.EquivalentTo(new[] {"1HGEJ6571WL983856", "4T1BG22K31U983888", "KNAGD128145983904"}));
        }

        [Test]
        public void Get_should_not_return_null_Vin()
        {
            _inventoryDataRepositoryMock.Setup(r => r.GetAllInventory(105239))
                .Returns(new[] { MakeInventory(vin: null) });

            var controller = new WebLoaderVehiclesController(_dealerServices, _inventoryDataRepository, _chromeMapper, _vinPatternRepository, _styleRepository);

            var result = controller.Get("windycit05");

            Assert.That(result.Single().Vin, Is.EqualTo(""));
        }

        [Test]
        public void Get_should_return_inventory_items_with_PhotoCount_populated()
        {
            var controller = new WebLoaderVehiclesController(_dealerServices, _inventoryDataRepository, _chromeMapper, _vinPatternRepository, _styleRepository);

            var result = controller.Get("windycit05");

            Assert.That(result.Select(i => i.PhotoCount).ToArray(), Is.EquivalentTo(new[] { 0, 3, 37 }));
        }

        [Test]
        public void Get_should_return_inventory_items_with_Year_populated()
        {
            var controller = new WebLoaderVehiclesController(_dealerServices, _inventoryDataRepository, _chromeMapper, _vinPatternRepository, _styleRepository);

            var result = controller.Get("windycit05");

            Assert.That(result.Select(i => i.Year).ToArray(), Is.EquivalentTo(new[] { 1998, 2004, 2001 }));
        }

        [TestCase("abc", 0)]
        [TestCase("", 0)]
        [TestCase("2005", 2005)]
        public void Get_should_parse_Year_property_and_fallback_to_default_value(string inputYear, int expectedResult)
        {
            _inventoryDataRepositoryMock.Setup(r => r.GetAllInventory(105239))
                .Returns(new[] { MakeInventory(year: inputYear) });

            var controller = new WebLoaderVehiclesController(_dealerServices, _inventoryDataRepository, _chromeMapper, _vinPatternRepository, _styleRepository);

            var result = controller.Get("windycit05");

            Assert.That(result.Single().Year, Is.EqualTo(expectedResult));
        }

        [Test]
        public void Get_should_return_inventory_items_with_Make_populated()
        {
            var controller = new WebLoaderVehiclesController(_dealerServices, _inventoryDataRepository, _chromeMapper, _vinPatternRepository, _styleRepository);

            var result = controller.Get("windycit05");

            Assert.That(result.Select(i => i.Make).ToArray(),
                        Is.EquivalentTo(new[] { "Honda", "Toyota", "Kia" }));
        }

        [Test]
        public void Get_should_not_return_null_Make()
        {
            _inventoryDataRepositoryMock.Setup(r => r.GetAllInventory(105239))
                .Returns(new[] { MakeInventory(make: null) });

            var controller = new WebLoaderVehiclesController(_dealerServices, _inventoryDataRepository, _chromeMapper, _vinPatternRepository, _styleRepository);

            var result = controller.Get("windycit05");

            Assert.That(result.Single().Make, Is.EqualTo(""));
        }

        [Test]
        public void Get_should_return_inventory_items_with_Model_populated()
        {
            var controller = new WebLoaderVehiclesController(_dealerServices, _inventoryDataRepository, _chromeMapper, _vinPatternRepository, _styleRepository);

            var result = controller.Get("windycit05");

            Assert.That(result.Select(i => i.Model).ToArray(),
                        Is.EquivalentTo(new[] { "Civic", "Camry", "Optima" }));
        }

        [Test]
        public void Get_should_not_return_null_Model()
        {
            _inventoryDataRepositoryMock.Setup(r => r.GetAllInventory(105239))
                .Returns(new[] { MakeInventory(model: null) });

            var controller = new WebLoaderVehiclesController(_dealerServices, _inventoryDataRepository, _chromeMapper, _vinPatternRepository, _styleRepository);

            var result = controller.Get("windycit05");

            Assert.That(result.Single().Model, Is.EqualTo(""));
        }

        [TestCase(true, 0, 5, false)]
        [TestCase(true, 3, 5, true)]
        [TestCase(true, 8, 5, false)]
        [TestCase(true, 5, 5, false)]
        [TestCase(false, 0, 5, false)]
        [TestCase(false, 1, 5, false)]
        public void LowPhotos_should_return_correct_values_given_specific_inventory_values(bool needsPhotos, int photoCount, int desiredPhotoCount, bool expectedLowPhotos)
        {
            _inventoryDataRepositoryMock.Setup(r => r.GetAllInventory(105239))
                .Returns(new[] { MakeInventory(needsPhotos: needsPhotos, photoCount: photoCount, desiredPhotoCount: desiredPhotoCount) });

            var controller = new WebLoaderVehiclesController(_dealerServices, _inventoryDataRepository, _chromeMapper, _vinPatternRepository, _styleRepository);

            var result = controller.Get("windycit05");

            Assert.That(result.Single().LowPhotos, Is.EqualTo(expectedLowPhotos));
        }

        [TestCase(true, 0, 5, true)]
        [TestCase(true, 3, 5, false)]
        [TestCase(true, 8, 5, false)]
        [TestCase(true, 5, 5, false)]
        [TestCase(false, 0, 5, false)]
        [TestCase(false, 1, 5, false)]
        public void NoPhotos_should_return_correct_values_given_specific_inventory_values(bool needsPhotos, int photoCount, int desiredPhotoCount, bool expectedLowPhotos)
        {
            _inventoryDataRepositoryMock.Setup(r => r.GetAllInventory(105239))
                .Returns(new[] { MakeInventory(needsPhotos: needsPhotos, photoCount: photoCount, desiredPhotoCount: desiredPhotoCount) });

            var controller = new WebLoaderVehiclesController(_dealerServices, _inventoryDataRepository, _chromeMapper, _vinPatternRepository, _styleRepository);

            var result = controller.Get("windycit05");

            Assert.That(result.Single().NoPhotos, Is.EqualTo(expectedLowPhotos));
        }

        [TestCase(false, false, false)]
        [TestCase(false, true, true)]
        [TestCase(true, false, true)]
        [TestCase(true, true, true)]
        public void NoEquipment_should_return_correct_values_given_specific_inventory_values(bool needsdInterior, bool needsExterior, bool expectedNoEquipement)
        {
            _inventoryDataRepositoryMock.Setup(r => r.GetAllInventory(105239))
                .Returns(new[] { MakeInventory(needsInterior: needsdInterior, needsExterior: needsExterior) });

            var controller = new WebLoaderVehiclesController(_dealerServices, _inventoryDataRepository, _chromeMapper, _vinPatternRepository, _styleRepository);

            var result = controller.Get("windycit05");

            Assert.That(result.Single().NoEquipment, Is.EqualTo(expectedNoEquipement));
        }

        [TestCase("1HGEJ6571WL983856", "1_105308")]
        [TestCase("4T1BG22K31U983888", "1_4735, 1_4734, 1_4733, 1_4726, 1_4725, 1_4724, 1_4723, 1_4722, 1_4721, 1_4720, 1_4719, 1_4718")]
        [TestCase("KNAGD128145983904", "1_262102, 1_262104")]
        public void AvailableTrimIds_should_return_expected_results(string vin, string expectedChromeStyleIdsCsv)
        {
            var expectedChromeStyleIds = expectedChromeStyleIdsCsv.Split(',').Select(s => s.Trim()).ToList();

            _inventoryDataRepositoryMock.Setup(r => r.GetAllInventory(105239))
                .Returns(new[] { MakeInventory(vin: vin) });

            var controller = new WebLoaderVehiclesController(_dealerServices, _inventoryDataRepository, _chromeMapper, _vinPatternRepository, _styleRepository);

            var result = controller.Get("windycit05");

            Assert.That(result.Single().AvailableTrimIds, Is.EquivalentTo(expectedChromeStyleIds));
        }

        [Test]
        public void AvailableTrimIds_should_return_empty_list_when_vin_not_matched()
        {
            _inventoryDataRepositoryMock.Setup(r => r.GetAllInventory(105239))
                .Returns(new[] { MakeInventory(vin: "1HGEJ6571_UNKNOWN") });

            var controller = new WebLoaderVehiclesController(_dealerServices, _inventoryDataRepository, _chromeMapper, _vinPatternRepository, _styleRepository);

            var result = controller.Get("windycit05");

            Assert.That(result.Single().AvailableTrimIds, Is.EquivalentTo(new string[0]));
        }

        [Test]
        public void AvailableTrimIds_should_return_empty_list_when_VinPattern_not_found()
        {
            _inventoryDataRepositoryMock.Setup(r => r.GetAllInventory(105239))
                .Returns(new[] { MakeInventory(vin: "1HGEJ6571WL983856") });
            _vinPatternRepositoryMock.Setup(vpr => vpr.GetVinPatternById(It.IsAny<IEnumerable<int>>())).Returns(Enumerable.Empty<IVinPattern>());

            var controller = new WebLoaderVehiclesController(_dealerServices, _inventoryDataRepository, _chromeMapper, _vinPatternRepository, _styleRepository);

            var result = controller.Get("windycit05");

            Assert.That(result.Single().AvailableTrimIds, Is.EquivalentTo(new string[0]));
        }

        [TestCase(105308, "1_105308", "1HGEJ6571WL983856")]
        [TestCase(262104, "1_262104", "KNAGD128145983904")]
        [TestCase(null, null, "")]
        [TestCase(-1, null, "")]
        public void Trim_Id_should_return_the_correct_string(int? styleIdOfInventory, string expectedTrimId, string vin)
        {
            _inventoryDataRepositoryMock.Setup(r => r.GetAllInventory(105239))
                .Returns(new[] { MakeInventory(styleId: styleIdOfInventory, vin: vin) });
            
            var controller = new WebLoaderVehiclesController(_dealerServices, _inventoryDataRepository, _chromeMapper, _vinPatternRepository, _styleRepository);

            var result = controller.Get("windycit05");

            var resultTrimId = result.Single().Trim == null ? null : result.Single().Trim.Id;

            Assert.That(resultTrimId, Is.EqualTo(expectedTrimId));
        }

        [TestCase(105308, "LX - 4dr Sdn Manual", "1HGEJ6571WL983856")]
        [TestCase(262104, "EX - 4dr Sdn Auto V6", "KNAGD128145983904")]
        [TestCase(null, null, "")]
        [TestCase(-1, null, "")]
        public void Trim_Desc_should_return_the_correct_string(int? styleIdOfInventory, string expectedTrimDesc, string vin)
        {
            _inventoryDataRepositoryMock.Setup(r => r.GetAllInventory(105239))
                .Returns(new[] { MakeInventory(styleId: styleIdOfInventory, vin: vin) });

            var controller = new WebLoaderVehiclesController(_dealerServices, _inventoryDataRepository, _chromeMapper, _vinPatternRepository, _styleRepository);

            var result = controller.Get("windycit05");

            var resultTrimDesc = result.Single().Trim == null ? null : result.Single().Trim.Desc;

            Assert.That(resultTrimDesc, Is.EqualTo(expectedTrimDesc));
        }

        // TODO: Check this.
        [TestCase("", "56")]
        [TestCase(null, "56")]
        [TestCase("", "")]
        [TestCase(null, null)]
        public void Ext1Color_should_be_null_when_BaseColor_is_empty_and_color_not_found(string baseColor, string exteriorColorCode)
        {
            _inventoryDataRepositoryMock.Setup(r => r.GetAllInventory(105239))
                .Returns(new[] { MakeInventory(styleId: 332735, ext1Desc: baseColor, ext1Code: exteriorColorCode) });

            _styles = new Dictionary<int, Style> {{332735, CreateStyleWithColors()}};

            var controller = new WebLoaderVehiclesController(_dealerServices, _inventoryDataRepository, _chromeMapper, _vinPatternRepository, _styleRepository);

            var result = controller.Get("windycit05");

            Assert.That(result.Single().ExtColor, Is.Null);
        }

        [TestCase("Super White", "040", "Super White", "White", "c=040&d=Super+White")]
        [TestCase("Super White", "FF", "Super White", "White", "c=040&d=Super+White")]
        [TestCase("super white", "FF", "Super White", "White", "c=040&d=Super+White")]
        [TestCase("Gold", "040", "Super White", "White", "c=040&d=Super+White")]
        [TestCase("super white", "", "Super White", "White", "c=040&d=Super+White")]
        [TestCase("Silver Sky Metallic", "", "Silver Sky Metallic", "Silver", "c=1D6&d=Silver+Sky+Metallic")]
        [TestCase("barcelona red metallic", "", "Barcelona Red Metallic", "Red", "c=3R3&d=Barcelona+Red+Metallic")]
        [TestCase("Azure blue", "", "Azure blue", null, "d=Azure+blue", Description = "Custom color")]
        [TestCase("Azure blue", "CUST_CODE", "Azure blue", null, "c=CUST_CODE&d=Azure+blue", Description = "Custom color")]
        public void Ext1Color_Desc_Simple_and_Id_should_be_expected_values_given_BaseColor_and_ExteriorColorCode(string ext1Desc, string ext1Code, 
            string expectedDesc, string expectedSimple, string expectedId)
        {
            _inventoryDataRepositoryMock.Setup(r => r.GetAllInventory(105239))
                .Returns(new[] { MakeInventory(styleId: 332735, ext1Desc: ext1Desc, ext1Code: ext1Code, vin: "WDBBA48D4JA083852") });

            _styles = new Dictionary<int, Style> { { 332735, CreateStyleWithColors() } };

            var controller = new WebLoaderVehiclesController(_dealerServices, _inventoryDataRepository, _chromeMapper, _vinPatternRepository, _styleRepository);

            var result = controller.Get("windycit05").Single();

            Assert.That(result.ExtColor.Desc, Is.EqualTo(expectedDesc), "Desc");
            Assert.That(result.ExtColor.Simple, Is.EqualTo(expectedSimple), "Simple");
            Assert.That(result.ExtColor.Id, Is.EqualTo(expectedId), "Id");
        }

        private static Style CreateStyleWithColors()
        {
            return new Style(
                styleID: 332735,
                colors: new[]
                {
                    // See style 332735
                    new Color(ext1Code: "00", ext1ManCode: "040", ext1Desc: "Super White", genericExtColor: "White"), 
                    new Color(ext1Code: "02", ext1ManCode: "1G3", ext1Desc: "Magnetic Gray Metallic", genericExtColor: "Gray"), 
                    new Color(ext1Code: "03", ext1ManCode: "202", ext1Desc: "Black", genericExtColor: "Black"), 
                    new Color(ext1Code: "04", ext1ManCode: "3R3", ext1Desc: "Barcelona Red Metallic", genericExtColor: "Red"), 
                    new Color(ext1Code: "08", ext1ManCode: "1D6", ext1Desc: "Silver Sky Metallic", genericExtColor: "Silver"), 
                    new Color(ext1Code: "0A", ext1ManCode: "4T3", ext1Desc: "Pyrite Mica", genericExtColor: "Gray"), 
                    new Color(ext1Code: "0N", ext1ManCode: "8S6", ext1Desc: "Nautical Blue Metallic", genericExtColor: "Blue"), 
                    new Color(ext1Code: "0P", ext1ManCode: "6V4", ext1Desc: "Spruce Mica", genericExtColor: "Green"), 
                    new Color(ext1Code: "0R", ext1ManCode: "3L5", ext1Desc: "Radiant Red", genericExtColor: "Red")
                });
        }
    }
}