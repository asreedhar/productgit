﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Max.Selling.Domain;
using Max.Selling.WebService.Controllers;
using NUnit.Framework;
using Moq;
using Max.Selling.Domain.Configuration;
using System.Xml;
using System.Web.Mvc;

namespace SellingWebServiceTests
{
    [TestFixture]
    public class LoginControllerTests
    {

        [Test]
        public void TestValidDemoLogin()
        {
            var userConfigurationElementMock = new Mock<IDemoDealerConfigurationSection>();
            var userConfigurationElement = new UserConfigurationElement(){Name = "maxdemo", Password = "max123"};
            userConfigurationElementMock.Setup(x => x.GetUserConfigurationElement(userConfigurationElement.Name)).Returns((string userName)
                => userConfigurationElement
            );

            var commandFactoryMock = new Mock<ICommandFactory>();
            commandFactoryMock.Setup(x => x.CreateTokenCommand(userConfigurationElement.Name, userConfigurationElement.Password)).Returns((string userName, string password) =>
                new DemoTokenCreationCommand(userName, password, userConfigurationElementMock.Object)
            );

            LoginController controller = new LoginController(commandFactoryMock.Object);
            var result = controller.CreateTicket("maxdemo", "max123") as ContentResult;

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(result.Content);

            var tokenElement = doc.SelectSingleNode("/token[@value]");
            var tokenAttribute = tokenElement.Attributes["value"].Value;
            Assert.IsTrue(tokenAttribute != string.Empty);

            var statusElement = doc.SelectSingleNode("//status[@error]");
            var statusAttribute = bool.Parse(statusElement.Attributes["error"].Value);
            Assert.IsTrue(statusAttribute == false);
        }
    }
}
