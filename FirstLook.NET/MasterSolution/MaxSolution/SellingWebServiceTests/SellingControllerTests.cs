﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Web.Mvc;
using System.Xml;
using FirstLook.DomainModel.Oltp;
using Max.Selling.Domain;
using Max.Selling.Domain.Configuration;
using Max.Selling.WebService.Controllers;
using Moq;
using NUnit.Framework;
using System.Web;
using System.Web.Routing;

namespace SellingWebServiceTests
{
    [TestFixture]
    public class SellingControllerTests
    {

        [Test]
        public void TestDemoGetDealers()
        {
            BusinessUnit unit = new BusinessUnit() { Name = "Hendrick BMW-MINI", Id = 100148 };
            var list = new List<BusinessUnit>();
            list.Add(unit);
            
            var businessUnitMock = new Mock<IDemoDealerConfigurationSection>();
            businessUnitMock.Setup(x => x.GetBusinessUnitFromUser("maxdemo")).Returns((string username) => list);

            DemoDealersCommand demoDealers = new DemoDealersCommand("maxdemo", businessUnitMock.Object);
            var commandFactoryMock = new Mock<ICommandFactory>();
            commandFactoryMock.Setup(x => x.CreateDealersCommand(It.IsAny<IPrincipal>())).Returns((IPrincipal currentUser) => demoDealers);

            var sellingController = GetSellingController(commandFactoryMock.Object, new GenericPrincipal(new GenericIdentity("maxdemo"), new string[] { Roles.Demo }));
            var result = sellingController.GetDealers() as ContentResult;

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(result.Content);

            var dealerName = doc.SelectSingleNode("/dealers/dealer/name").InnerText;
            var businessUnitId = int.Parse(doc.SelectSingleNode("/dealers/dealer/businessunitid").InnerText);
            Assert.IsTrue(unit.Name == dealerName);
            Assert.IsTrue(unit.Id == businessUnitId);

            var statusElement = doc.SelectSingleNode("//status[@error]");
            var statusAttribute = bool.Parse(statusElement.Attributes["error"].Value);
            Assert.IsTrue(statusAttribute == false);
        }

        [Test]
        public void TestGetInventory()
        {
            string inventoryDoc = "<salesaccelerator date=\"3/16/2011\" version=\"1.2\"></salesaccelerator>";
            int businessUnitId = 23;
            var inventoryAccessMock = new Mock<IInventoryDataAccess>();
            inventoryAccessMock.Setup(x => x.FetchDocs(businessUnitId)).Returns((int unit) => new string[] {inventoryDoc});

            var commandFactoryMock = new Mock<ICommandFactory>();
            commandFactoryMock.Setup(x => x.CreateInventoryCommand(businessUnitId)).Returns((int unit) => new InventoryCommand(unit, inventoryAccessMock.Object));

            var sellingController = GetSellingController(commandFactoryMock.Object, new GenericPrincipal(new GenericIdentity("breeve"), new string[] {}));
            var result = sellingController.Inventory(businessUnitId) as ContentResult;

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(result.Content);

            var salesAcceleratorElement = doc.SelectSingleNode("/inventory/item/salesaccelerator[@version]");
            var versionAttribute = salesAcceleratorElement.Attributes["version"].Value;
            Assert.IsTrue(versionAttribute == "1.2");

            var statusElement = doc.SelectSingleNode("//status[@error]");
            var statusAttribute = bool.Parse(statusElement.Attributes["error"].Value);
            Assert.IsTrue(statusAttribute == false);
        }


        private static SellingController GetSellingController(ICommandFactory commandFactory, IPrincipal user)
        {
            RequestContext requestContext = new RequestContext(new StubHttpContext(user), new RouteData());
            SellingController controller = new SellingController(commandFactory);
            
            controller.ControllerContext = new ControllerContext()
            {
                Controller = controller,
                RequestContext = requestContext
            };
            return controller;
        }


        private class StubHttpContext : HttpContextBase
        {
            private IPrincipal _user;

            public StubHttpContext(IPrincipal user)
            {
                _user = user;
            }

            public override IPrincipal User
            {
                get
                {
                    return _user;
                }
                set
                {
                    base.User = value;
                }
            }
        }

    }
}
