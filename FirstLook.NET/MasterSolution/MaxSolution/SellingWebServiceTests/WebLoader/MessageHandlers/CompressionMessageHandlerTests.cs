﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using Max.Selling.WebService.WebLoader.MessageHandlers;
using NUnit.Framework;

namespace SellingWebServiceTests.WebLoader.MessageHandlers
{
    [TestFixture]
    public class CompressionMessageHandlerTests
    {
        [Test]
        public void SendAsync_should_not_compress_inner_content_if_null()
        {
            var cancellationToken = CancellationToken.None;
            var requestMsg = new HttpRequestMessage();
            var inner = new EmptyResponseHandler();
            var handler = new CompressionMessageHandler(inner);
            var invoker = new HttpMessageInvoker(handler);

            var responseMsg = invoker.SendAsync(requestMsg, cancellationToken).Result;

            Assert.That(responseMsg.Content, Is.Null);
        }

        [Test]
        public void SendAsync_with_compressible_content_should_compress()
        {
            var cancellationToken = CancellationToken.None;
            var requestMsg = new HttpRequestMessage();
            requestMsg.Headers.AcceptEncoding.Add(new StringWithQualityHeaderValue("gzip"));
            var inner = new EmptyResponseHandler(() => new StringContent("Hello World!", Encoding.UTF8, "text/plain"));
            var handler = new CompressionMessageHandler(inner);
            var invoker = new HttpMessageInvoker(handler);

            var responseMsg = invoker.SendAsync(requestMsg, cancellationToken).Result;
            var content = responseMsg.Content;
            var bytes = content.ReadAsByteArrayAsync().Result;

            var result = GZipHelper(bytes);
            Assert.That(result, Is.EqualTo("Hello World!"));
        }

        [TestCase("text/plain", true)]
        [TestCase("application/json", true)]
        [TestCase("image/jpeg", false)]
        public void SendAsync_should_only_compress_certain_mime_types(string contentType, bool shouldBeCompressed)
        {
            var cancellationToken = CancellationToken.None;
            var requestMsg = new HttpRequestMessage();
            requestMsg.Headers.AcceptEncoding.Add(new StringWithQualityHeaderValue("gzip"));
            var inner = new EmptyResponseHandler(() => new StringContent("Hello World!", Encoding.UTF8, contentType));
            var handler = new CompressionMessageHandler(inner);
            var invoker = new HttpMessageInvoker(handler);

            var responseMsg = invoker.SendAsync(requestMsg, cancellationToken).Result;
            var content = responseMsg.Content;

            Assert.That(content.Headers.ContentEncoding.Any(enc => StringComparer.InvariantCultureIgnoreCase.Equals(enc, "gzip")), 
                Is.EqualTo(shouldBeCompressed));
        }

        [TestCase("gzip", true)]
        [TestCase("deflate", true)]
        [TestCase("sdch", false)]
        [TestCase("sdch,gzip", true)]
        public void SendAsync_should_only_compress_if_suitable_request_AcceptEncoding(string acceptEncoding, bool shouldBeCompressed)
        {
            var cancellationToken = CancellationToken.None;
            var requestMsg = new HttpRequestMessage();
            requestMsg.Headers.TryAddWithoutValidation("Accept-Encoding", acceptEncoding);
            var inner = new EmptyResponseHandler(() => new StringContent("Hello World!", Encoding.UTF8, "text/plain"));
            var handler = new CompressionMessageHandler(inner);
            var invoker = new HttpMessageInvoker(handler);

            var responseMsg = invoker.SendAsync(requestMsg, cancellationToken).Result;
            var content = responseMsg.Content;

            Assert.That(content.Headers.ContentEncoding.Any(enc => enc.Length > 0),
                Is.EqualTo(shouldBeCompressed));
        }

        private static string GZipHelper(byte[] bytes)
        {
            var compressed = new MemoryStream(bytes);
            using (var decompress = new GZipStream(compressed, CompressionMode.Decompress))
            {
                var uncompressed = new MemoryStream();
                decompress.CopyTo(uncompressed);
                return Encoding.UTF8.GetString(uncompressed.ToArray());
            }
        }
    }
}