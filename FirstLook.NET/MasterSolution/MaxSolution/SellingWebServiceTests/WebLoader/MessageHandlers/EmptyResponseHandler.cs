using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace SellingWebServiceTests.WebLoader.MessageHandlers
{
    public class EmptyResponseHandler : HttpMessageHandler
    {
        private readonly Func<HttpRequestMessage, HttpResponseMessage> _responseFactory;

        public EmptyResponseHandler(Func<HttpContent> contentFactory)
            : this(request =>
                       {
                           var content = contentFactory();
                           var response = request.CreateResponse(HttpStatusCode.OK);
                           response.Content = content;
                           return response;
                       })
        {
        }

        public EmptyResponseHandler(Func<HttpRequestMessage, HttpResponseMessage> responseFactory = null)
        {
            _responseFactory = responseFactory ?? (request => request.CreateResponse(HttpStatusCode.OK));
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var tcs = new TaskCompletionSource<HttpResponseMessage>();
            tcs.SetResult(_responseFactory(request));
            return tcs.Task;
        }
    }
}