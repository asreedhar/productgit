﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using FirstLook.Merchandising.DomainModel.Core.Authentication;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using Max.Selling.WebService.WebLoader.MessageHandlers;
using Moq;
using NUnit.Framework;

namespace SellingWebServiceTests.WebLoader.MessageHandlers
{
    [TestFixture]
    public class BasicAuthenticationMessageHandlerTests
    {
        private Mock<IUserServices> _userServicesMock;
        private IUserServices _userServices;

        [SetUp]
        public void Setup()
        {
            _userServicesMock = new Mock<IUserServices>();
            _userServices = _userServicesMock.Object;
        }

        [Test]
        public void SendAsync_should_return_401_when_Authorization_header_missing_from_request()
        {
            var cancellationToken = CancellationToken.None;
            var requestMsg = new HttpRequestMessage();
            
            var handlerUnderTest = new BasicAuthenticationMessageHandler(_userServices)
                              {
                                  InnerHandler = new EmptyResponseHandler()
                              };
            var invoker = new HttpMessageInvoker(handlerUnderTest);
            
            var response = invoker.SendAsync(requestMsg, cancellationToken).Result;

            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [TestCase("joe", "password5", HttpStatusCode.OK)]
        [TestCase("Joe_bad_username", "password5", HttpStatusCode.Unauthorized)]
        [TestCase("joe", "h@ck0r_p@$$w0rd", HttpStatusCode.Unauthorized)]
        public void SendAsync_should_return_OK_or_Unauthorized_when_basic_Authorization_header_with_username_and_password_are_sent(
            string username, string password, HttpStatusCode expectedStatusCode)
        {
            var cancellationToken = CancellationToken.None;
            var requestMsg = new HttpRequestMessage();
            requestMsg.Headers.Authorization = BuildAuthHeader("Basic", username, password);
            _userServicesMock.Setup(us => us.AuthenticateAndSetCurrent(new BasicCredential("joe", "password5"))).Returns(true);

            var handlerUnderTest = new BasicAuthenticationMessageHandler(_userServices)
            {
                InnerHandler = new EmptyResponseHandler()
            };
            var invoker = new HttpMessageInvoker(handlerUnderTest);

            var response = invoker.SendAsync(requestMsg, cancellationToken).Result;

            Assert.That(response.StatusCode, Is.EqualTo(expectedStatusCode));
        }

        [TestCase("joe", "password5", "joe")]
        [TestCase("Joe_bad_username", "password5", null)]
        [TestCase("joe", "h@ck0r_p@$$w0rd", null)]
        public void SendAsync_should_set_CurrentPrincipal_when_correct_username_and_password_are_sent(
            string username, string password, string expectedUsername)
        {
            var currentUsername = "previous_value";
            var cancellationToken = CancellationToken.None;
            var requestMsg = new HttpRequestMessage();
            requestMsg.Headers.Authorization = BuildAuthHeader("Basic", username, password);
            _userServicesMock.Setup(us => us.AuthenticateAndSetCurrent(It.IsAny<BasicCredential>()))
                .Returns<BasicCredential>(c =>
                                              {
                                                  var isAuthentic = c != null && c.Username == "joe" && c.Password == "password5";
                                                  currentUsername = isAuthentic ? c.Username : null;
                                                  return isAuthentic;
                                              });
            
            var handlerUnderTest = new BasicAuthenticationMessageHandler(_userServices)
            {
                InnerHandler = new EmptyResponseHandler()
            };
            var invoker = new HttpMessageInvoker(handlerUnderTest);

            invoker.SendAsync(requestMsg, cancellationToken).Wait();

            Assert.That(currentUsername, Is.EqualTo(expectedUsername));
        }

        [Test]
        public void SendAsync_should_WWW_Authenticate_header_when_authentication_fails()
        {
            var cancellationToken = CancellationToken.None;
            var requestMsg = new HttpRequestMessage();
            requestMsg.Headers.Authorization = BuildAuthHeader("Basic", "joe", "bad_password");
            _userServicesMock.Setup(us => us.AuthenticateAndSetCurrent(new BasicCredential("joe", "password5"))).Returns(true);

            var handlerUnderTest = new BasicAuthenticationMessageHandler(_userServices)
            {
                InnerHandler = new EmptyResponseHandler()
            };
            var invoker = new HttpMessageInvoker(handlerUnderTest);

            var result = invoker.SendAsync(requestMsg, cancellationToken).Result;

            Assert.That(result.Headers.WwwAuthenticate.Select(h => h.ToString()).Single(), Is.StringMatching(@"^\s*Basic\s+realm\s*=\s*\""[^\""]+\""\s*$"));
        }

        [TestCase("Basic", HttpStatusCode.OK)]
        [TestCase("BASIC", HttpStatusCode.OK, Description = "Should work with mixed case for scheme.")]
        [TestCase("basic", HttpStatusCode.OK, Description = "Should work with mixed case for scheme.")]
        [TestCase("NTLM", HttpStatusCode.Unauthorized)]
        public void SendAsync_should_return_OK_only_when_Authorization_scheme_is_Basic(
            string scheme, HttpStatusCode expectedStatusCode)
        {
            var cancellationToken = CancellationToken.None;
            var requestMsg = new HttpRequestMessage();
            requestMsg.Headers.Authorization = BuildAuthHeader(scheme, "joe", "password5");
            _userServicesMock.Setup(us => us.AuthenticateAndSetCurrent(new BasicCredential("joe", "password5"))).Returns(true);

            var handlerUnderTest = new BasicAuthenticationMessageHandler(_userServices)
            {
                InnerHandler = new EmptyResponseHandler()
            };
            var invoker = new HttpMessageInvoker(handlerUnderTest);

            var response = invoker.SendAsync(requestMsg, cancellationToken).Result;

            Assert.That(response.StatusCode, Is.EqualTo(expectedStatusCode));
        }

        [Test]
        public void SendAsync_should_return_Unauthorized_when_Authorization_header_parameter_is_invalid_base64()
        {
            var cancellationToken = CancellationToken.None;
            var requestMsg = new HttpRequestMessage();
            requestMsg.Headers.Authorization = new AuthenticationHeaderValue("Basic", "invalid_base_64");
            _userServicesMock.Setup(us => us.AuthenticateAndSetCurrent(new BasicCredential("joe", "password5"))).Returns(true);

            var handlerUnderTest = new BasicAuthenticationMessageHandler(_userServices)
            {
                InnerHandler = new EmptyResponseHandler()
            };
            var invoker = new HttpMessageInvoker(handlerUnderTest);

            var response = invoker.SendAsync(requestMsg, cancellationToken).Result;

            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public void SendAsync_should_return_Unauthorized_when_Authorization_header_decoded_parameter_does_not_contain_colon_char()
        {
            var cancellationToken = CancellationToken.None;
            var requestMsg = new HttpRequestMessage();
            requestMsg.Headers.Authorization = BuildAuthHeader("Basic", "joepassword5");
            _userServicesMock.Setup(us => us.AuthenticateAndSetCurrent(new BasicCredential("joe", "password5"))).Returns(true);

            var handlerUnderTest = new BasicAuthenticationMessageHandler(_userServices)
            {
                InnerHandler = new EmptyResponseHandler()
            };
            var invoker = new HttpMessageInvoker(handlerUnderTest);

            var response = invoker.SendAsync(requestMsg, cancellationToken).Result;

            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public void SendAsync_should_return_OK_with_blank_password()
        {
            var cancellationToken = CancellationToken.None;
            var requestMsg = new HttpRequestMessage();
            requestMsg.Headers.Authorization = BuildAuthHeader("Basic", "joe:");
            _userServicesMock.Setup(us => us.AuthenticateAndSetCurrent(new BasicCredential("joe", ""))).Returns(true);

            var handlerUnderTest = new BasicAuthenticationMessageHandler(_userServices)
            {
                InnerHandler = new EmptyResponseHandler()
            };
            var invoker = new HttpMessageInvoker(handlerUnderTest);

            var response = invoker.SendAsync(requestMsg, cancellationToken).Result;

            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
        }

        [Test]
        public void SendAsync_should_return_unauthorized_with_blank_username()
        {
            var cancellationToken = CancellationToken.None;
            var requestMsg = new HttpRequestMessage();
            requestMsg.Headers.Authorization = BuildAuthHeader("Basic", ":password");
            _userServicesMock.Setup(us => us.AuthenticateAndSetCurrent(new BasicCredential("", "password"))).Returns(true);

            var handlerUnderTest = new BasicAuthenticationMessageHandler(_userServices)
            {
                InnerHandler = new EmptyResponseHandler()
            };
            var invoker = new HttpMessageInvoker(handlerUnderTest);

            var response = invoker.SendAsync(requestMsg, cancellationToken).Result;

            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        private static AuthenticationHeaderValue BuildAuthHeader(string scheme, string paramToEncode)
        {
            return new AuthenticationHeaderValue(scheme, Convert.ToBase64String(Encoding.ASCII.GetBytes(paramToEncode)));
        }

        private static AuthenticationHeaderValue BuildAuthHeader(string scheme, string username, string password)
        {
            return new AuthenticationHeaderValue(scheme, 
                                                 Convert.ToBase64String(
                                                 Encoding.ASCII.GetBytes(
                                                 string.Format("{0}:{1}", username, password))));
        }
    }
}