﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using FirstLook.Merchandising.DomainModel.Core.Authentication;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using Max.Selling.WebService.WebLoader.Filters;
using Moq;
using NUnit.Framework;

namespace SellingWebServiceTests.WebLoader.Filters
{
    [TestFixture]
    public class AuthorizeUserAttributeTests
    {
        private Mock<IAuthorizeUser> _authorizeUserMock;
        private IAuthorizeUser _authorizeUser;
        private Mock<ICurrentPrincipal> _currentPrincipalMock;
        private ICurrentPrincipal _currentPrincipal;
        private Func<Task<HttpResponseMessage>> _continuation;
        private HttpActionContext _actionContext;
        private CancellationToken _cancellationToken;
        private GenericPrincipal _principal;
        private HttpControllerContext _controllerContext;

        [SetUp]
        public void Setup()
        {
            _authorizeUserMock = new Mock<IAuthorizeUser>();
            _authorizeUser = _authorizeUserMock.Object;

            _currentPrincipalMock = new Mock<ICurrentPrincipal>();
            _currentPrincipal = _currentPrincipalMock.Object;
            _principal = new GenericPrincipal(new GenericIdentity("pstephens"), new string[0]);

            _controllerContext = new HttpControllerContext {Request = new HttpRequestMessage(), Configuration = new HttpConfiguration()};
            _actionContext = new HttpActionContext {ControllerContext = _controllerContext};

            _cancellationToken = CancellationToken.None;
            _continuation = () => Task.Factory.StartNew(() => new HttpResponseMessage(HttpStatusCode.OK));
        }

        [Test]
        public void AssertAllRoles_should_be_passed_to_inner_IsAuthorized()
        {
            IList<string> list = null;
            _authorizeUserMock.Setup(
                au => au.IsAuthorized(It.IsAny<IPrincipal>(), It.IsAny<IList<string>>(), It.IsAny<IList<string>>()))
                .Returns(AuthorizationResult.Authorized)
                .Callback<IPrincipal, IList<string>, IList<String>>((p, all, any) => list = all);

            var filter = new AuthorizeUserAttribute(_authorizeUser, _currentPrincipal)
                             {
                                 AssertAllRoles = "Foo, Bar"
                             };

            var responseTask = filter.ExecuteAuthorizationFilterAsync(_actionContext, _cancellationToken, _continuation);
            responseTask.Wait();

            Assert.That(list, Is.EqualTo(new[] {"Foo", "Bar"}));
        }

        [Test]
        public void AssertAnyRoles_should_be_passed_to_inner_IsAuthorized()
        {
            IList<string> list = null;
            _authorizeUserMock.Setup(
                au => au.IsAuthorized(It.IsAny<IPrincipal>(), It.IsAny<IList<string>>(), It.IsAny<IList<string>>()))
                .Returns(AuthorizationResult.Authorized)
                .Callback<IPrincipal, IList<string>, IList<String>>((p, all, any) => list = any);

            var filter = new AuthorizeUserAttribute(_authorizeUser, _currentPrincipal)
            {
                AssertAnyRoles = " Admin, Reports"
            };

            var responseTask = filter.ExecuteAuthorizationFilterAsync(_actionContext, _cancellationToken, _continuation);
            responseTask.Wait();

            Assert.That(list, Is.EqualTo(new[] { "Admin", "Reports" }));
        }

        [Test]
        public void CurrentPrincipal_should_be_passed_to_inner_IsAuthorized()
        {
            _currentPrincipalMock.Setup(cp => cp.Current).Returns(_principal);
            _authorizeUserMock.Setup(
                au => au.IsAuthorized(It.IsAny<IPrincipal>(), It.IsAny<IList<string>>(), It.IsAny<IList<string>>()))
                .Returns(AuthorizationResult.Authorized);

            var filter = new AuthorizeUserAttribute(_authorizeUser, _currentPrincipal);

            var responseTask = filter.ExecuteAuthorizationFilterAsync(_actionContext, _cancellationToken, _continuation);
            responseTask.Wait();

            _authorizeUserMock.Verify(au => au.IsAuthorized(_principal, It.IsAny<IList<string>>(), It.IsAny<IList<string>>()), Times.Once());
        }

        [TestCase("Access Denied!", 403)]
        [TestCase(null, 200)] // null means authorization succeeded
        public void Should_return_appropriate_http_status_code_dependening_on_if_user_is_authorized(string authMsg, HttpStatusCode expectedResponseCode)
        {
            _currentPrincipalMock.Setup(cp => cp.Current).Returns(_principal);
            _authorizeUserMock.Setup(
                au => au.IsAuthorized(It.IsAny<IPrincipal>(), It.IsAny<IList<string>>(), It.IsAny<IList<string>>()))
                .Returns(new AuthorizationResult(authMsg));

            var filter = new AuthorizeUserAttribute(_authorizeUser, _currentPrincipal);

            var responseTask = filter.ExecuteAuthorizationFilterAsync(_actionContext, _cancellationToken, _continuation);
            var response = responseTask.Result;

            Assert.That(response.StatusCode, Is.EqualTo(expectedResponseCode));
        }

        [Test]
        public void Should_return_appropriate_content_dependening_on_if_user_is_authorized()
        {
            _currentPrincipalMock.Setup(cp => cp.Current).Returns(_principal);
            _authorizeUserMock.Setup(
                au => au.IsAuthorized(It.IsAny<IPrincipal>(), It.IsAny<IList<string>>(), It.IsAny<IList<string>>()))
                .Returns(new AuthorizationResult("Access Denied!"));

            var filter = new AuthorizeUserAttribute(_authorizeUser, _currentPrincipal);

            var responseTask = filter.ExecuteAuthorizationFilterAsync(_actionContext, _cancellationToken, _continuation);
            var response = responseTask.Result;

            Assert.That(
                ((AuthorizationResult) response.Content.ReadAsAsync(typeof (AuthorizationResult)).Result).Message,
                Is.EqualTo("Access Denied!"));
        }
    }
}