﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Routing;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Core.Authentication;
using FirstLook.Merchandising.DomainModel.Core.Authorization;
using Max.Selling.WebService.WebLoader.Filters;
using Moq;
using NUnit.Framework;

namespace SellingWebServiceTests.WebLoader.Filters
{
    [TestFixture]
    public class AuthorizeDealerAttributeTests
    {
        private Mock<IAuthorizeDealer> _authorizeDealerMock;
        private IAuthorizeDealer _authorizeDealer;
        private Mock<ICurrentPrincipal> _currentPrincipalMock;
        private ICurrentPrincipal _currentPrincipal;
        private Func<Task<HttpResponseMessage>> _continuation;
        private HttpActionContext _actionContext;
        private CancellationToken _cancellationToken;
        private GenericPrincipal _principal;
        private HttpControllerContext _controllerContext;
        private HttpRoute _route;

        [SetUp]
        public void Setup()
        {
            _authorizeDealerMock = new Mock<IAuthorizeDealer>();
            _authorizeDealer = _authorizeDealerMock.Object;

            _currentPrincipalMock = new Mock<ICurrentPrincipal>();
            _currentPrincipal = _currentPrincipalMock.Object;
            _principal = new GenericPrincipal(new GenericIdentity("pstephens"), new string[0]);

            _route = new HttpRoute();
            _controllerContext = new HttpControllerContext
                                     {
                                         Request = new HttpRequestMessage(),
                                         Configuration = new HttpConfiguration(),
                                         RouteData = new HttpRouteData(_route)
                                     };
            _actionContext = new HttpActionContext { ControllerContext = _controllerContext };

            _cancellationToken = CancellationToken.None;
            _continuation = () => Task.Factory.StartNew(() => new HttpResponseMessage(HttpStatusCode.OK));
        }

        [TestCase("dealer", "dealer", "WINDYCIT05", "WINDYCIT05")]
        [TestCase("foo", "dealer", "WINDYCIT05", null, Description = "Should not throw if route parameter is not defined.")]
        [TestCase("dealer", "dealer", 123, "123", Description = "Should accept non string values.")]
        [TestCase("dealer", "dealer", "", "", Description = "Should pass empty values through.")]
        public void DealerCode_should_be_passed_to_inner_IsAuthorized(string dealerCodeRouteParameter, string routeParameter, object routeValue, string expectedDealerCode)
        {
            _actionContext.ControllerContext.RouteData.Values[routeParameter] = routeValue;

            _authorizeDealerMock
                .Setup(ad => ad.IsAuthorized(It.IsAny<IPrincipal>(), It.IsAny<string>(), It.IsAny<IList<Upgrade>>(), It.IsAny<bool>()))
                .Returns(AuthorizationResult.Authorized);

            var filter = new AuthorizeDealerAttribute(_authorizeDealer, _currentPrincipal)
                             {DealerParameter = dealerCodeRouteParameter};

            var responseTask = filter.ExecuteAuthorizationFilterAsync(_actionContext, _cancellationToken, _continuation);
            responseTask.Wait();

            _authorizeDealerMock.Verify(
                ad => ad.IsAuthorized(It.IsAny<IPrincipal>(), expectedDealerCode, It.IsAny<IList<Upgrade>>(), It.IsAny<bool>()),
                Times.Once());
        }

        [Test]
        public void AssertDealerUpgrades_should_be_passed_to_inner_IsAuthorized()
        {
            IList<Upgrade> savedAssertDealerUpgrades = null;
            _authorizeDealerMock
                .Setup(ad => ad.IsAuthorized(It.IsAny<IPrincipal>(), It.IsAny<string>(), It.IsAny<IList<Upgrade>>(), It.IsAny<bool>()))
                .Returns(AuthorizationResult.Authorized)
                .Callback<IPrincipal, string, IList<Upgrade>, bool>((princ, dc, assertUpgrades, assertWebLoader) => savedAssertDealerUpgrades = assertUpgrades);

            var filter = new AuthorizeDealerAttribute(_authorizeDealer, _currentPrincipal)
                             {AssertDealerUpgrades = "Merchandising,MakeADeal"};

            var responseTask = filter.ExecuteAuthorizationFilterAsync(_actionContext, _cancellationToken, _continuation);
            responseTask.Wait();

            Assert.That(savedAssertDealerUpgrades, Is.EqualTo(new[] { Upgrade.Merchandising, Upgrade.MakeADeal }));
        }

        [Test]
        public void AssertDealerHasWebLoaderUpgrade_should_be_passed_to_inner_IsAuthorized()
        {
            _authorizeDealerMock
                .Setup(ad => ad.IsAuthorized(It.IsAny<IPrincipal>(), It.IsAny<string>(), It.IsAny<IList<Upgrade>>(), It.IsAny<bool>()))
                .Returns(AuthorizationResult.Authorized);

            var filter = new AuthorizeDealerAttribute(_authorizeDealer, _currentPrincipal) { AssertDealerHasWebLoaderUpgrade = true };

            var responseTask = filter.ExecuteAuthorizationFilterAsync(_actionContext, _cancellationToken, _continuation);
            responseTask.Wait();

            _authorizeDealerMock.Verify(
                ad => ad.IsAuthorized(It.IsAny<IPrincipal>(), It.IsAny<string>(), It.IsAny<IList<Upgrade>>(), true),
                Times.Once());
        }

        [Test]
        public void CurrentPrincipal_should_be_passed_to_inner_IsAuthorized()
        {
            _currentPrincipalMock.Setup(cp => cp.Current).Returns(_principal);
            _authorizeDealerMock
                .Setup(ad => ad.IsAuthorized(It.IsAny<IPrincipal>(), It.IsAny<string>(), It.IsAny<IList<Upgrade>>(), It.IsAny<bool>()))
                .Returns(AuthorizationResult.Authorized);

            var filter = new AuthorizeDealerAttribute(_authorizeDealer, _currentPrincipal);

            var responseTask = filter.ExecuteAuthorizationFilterAsync(_actionContext, _cancellationToken, _continuation);
            responseTask.Wait();

            _authorizeDealerMock.Verify(
                ad => ad.IsAuthorized(_principal, It.IsAny<string>(), It.IsAny<IList<Upgrade>>(), It.IsAny<bool>()),
                Times.Once());
        }

        [TestCase("Access Denied!", 403)]
        [TestCase(null, 200)] // null means authorization succeeded
        public void Should_return_appropriate_http_status_code_dependending_on_if_principal_is_authorized(string authMsg, HttpStatusCode expectedResponseCode)
        {
            _authorizeDealerMock
                .Setup(ad => ad.IsAuthorized(It.IsAny<IPrincipal>(), It.IsAny<string>(), It.IsAny<IList<Upgrade>>(), It.IsAny<bool>()))
                .Returns(new AuthorizationResult(authMsg));

            var filter = new AuthorizeDealerAttribute(_authorizeDealer, _currentPrincipal);

            var responseTask = filter.ExecuteAuthorizationFilterAsync(_actionContext, _cancellationToken, _continuation);
            var response = responseTask.Result;

            Assert.That(response.StatusCode, Is.EqualTo(expectedResponseCode));
        }

        [Test]
        public void Should_return_appropriate_content_dependening_on_if_dealer_is_authorized()
        {
            _authorizeDealerMock
                .Setup(ad => ad.IsAuthorized(It.IsAny<IPrincipal>(), It.IsAny<string>(), It.IsAny<IList<Upgrade>>(), It.IsAny<bool>()))
                .Returns(new AuthorizationResult("Forbidden!"));

            var filter = new AuthorizeDealerAttribute(_authorizeDealer, _currentPrincipal);

            var responseTask = filter.ExecuteAuthorizationFilterAsync(_actionContext, _cancellationToken, _continuation);
            var response = responseTask.Result;

            Assert.That(((AuthorizationResult) response.Content.ReadAsAsync(typeof (AuthorizationResult)).Result).Message,
                Is.EqualTo("Forbidden!"));
        }

        [Test]
        public void Should_return_NotFound_http_status_code_if_dealer_not_found()
        {
            _authorizeDealerMock
                .Setup(ad => ad.IsAuthorized(It.IsAny<IPrincipal>(), It.IsAny<string>(), It.IsAny<IList<Upgrade>>(), It.IsAny<bool>()))
                .Returns(new AuthorizationResult("Not Found!", true));

            var filter = new AuthorizeDealerAttribute(_authorizeDealer, _currentPrincipal);

            var responseTask = filter.ExecuteAuthorizationFilterAsync(_actionContext, _cancellationToken, _continuation);
            var response = responseTask.Result;

            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));
            Assert.That(((AuthorizationResult)response.Content.ReadAsAsync(typeof(AuthorizationResult)).Result).Message,
                Is.EqualTo("Not Found!"));
        }
    }
}