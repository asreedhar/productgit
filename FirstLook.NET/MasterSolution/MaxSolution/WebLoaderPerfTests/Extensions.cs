﻿using System.Text;
using Newtonsoft.Json.Linq;

namespace WebLoaderPerfTests
{
    public static class Extensions
    {
         public static byte[] ToBytes(this JToken json)
         {
             return new UTF8Encoding(false).GetBytes(json.ToString());
         }
    }
}