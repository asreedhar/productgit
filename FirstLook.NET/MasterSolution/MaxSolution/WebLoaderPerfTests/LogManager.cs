using System;
using System.Globalization;
using System.IO;
using System.Net;
using FirstLook.Merchandising.DomainModel.Core.IO;

namespace WebLoaderPerfTests
{
    public class LogManager : IDisposable
    {
        private readonly FileManager _files;
        private readonly DateTimeOffset _start;
        private readonly object _sync;
        private readonly FileStream _stream;
        private readonly CsvWriter _writer;

        public LogManager(FileManager files)
        {
            _files = files;
            _sync = new object();
            _start = DateTimeOffset.UtcNow;
            _stream = new FileStream(_files.GetPath(string.Format("perf-{0:yyyyMMdd-HHmmss}.csv", DateTime.Now)), 
                                     FileMode.Create, FileAccess.Write, FileShare.Read);
            _writer = new CsvWriter(_stream);
        }

        public void WritePerfLog(string cls, string url, DateTimeOffset startTime, DateTimeOffset stopTime, long? responseContentLength, HttpStatusCode? responseStatus, bool? wasCompressed = null, bool? wasPipelined = null, string error = "")
        {
            Console.WriteLine("Perf Log: {0} ElapsedMs: {1:0.0} Content-Length: {2} Status: {3} Response-Compressed: {4} Pipelined: {5}",
                url, (stopTime - startTime).TotalMilliseconds, responseContentLength, responseStatus, wasCompressed, wasPipelined);

            lock(_sync)
            {
                var startMs = (startTime - _start).TotalMilliseconds;
                var stopMs = (stopTime - _start).TotalMilliseconds;

                _writer.StartRecord();
                _writer.WriteValue(cls);
                _writer.WriteValue(startMs.ToString("0.0"));
                _writer.WriteValue(stopMs.ToString("0.0"));
                _writer.WriteValue(responseStatus.HasValue ? responseStatus.Value.ToString() : "");
                _writer.WriteValue(responseContentLength.HasValue ? responseContentLength.Value.ToString(CultureInfo.InvariantCulture) : "");
                _writer.WriteValue(wasCompressed.GetValueOrDefault(false) ? "yes" : "no");
                _writer.WriteValue(wasPipelined.GetValueOrDefault(false) ? "yes" : "no");
                _writer.WriteValue(url);
                _writer.WriteValue(error);
            }
        }

        public void WriteCsvHeader()
        {
            lock (_sync)
            {
                _writer.StartRecord();
                _writer.WriteValue("Class");
                _writer.WriteValue("StartMs");
                _writer.WriteValue("StopMs");
                _writer.WriteValue("Status");
                _writer.WriteValue("Content-Length");
                _writer.WriteValue("Compressed");
                _writer.WriteValue("Pipelined");
                _writer.WriteValue("Url");
                _writer.WriteValue("Error");
            }
        }
        
        public void Dispose()
        {
            try
            {
                _writer.Dispose();
            }
            finally
            {
                _stream.Dispose();
            }
        }
    }
}