using System;
using System.Collections.Generic;
using System.Threading;

namespace WebLoaderPerfTests
{
    public class ThreadManager
    {
        private int _activeTaskCount;
        private bool _done;
        private readonly object _sync = new object();
        private readonly Queue<Action> _queue = new Queue<Action>();
        private readonly Thread[] _threads;
        

        public ThreadManager(int requestCount)
        {
            _threads = new Thread[requestCount];
        }

        public void Enqueue(params Action[] tasks)
        {
            lock(_sync)
            {
                foreach(var task in tasks)
                    _queue.Enqueue(task);

                Monitor.PulseAll(_sync);
            }
        }

        public void StartThreads()
        {
            for(var i = 0; i < _threads.Length; ++i)
            {
                _threads[i] = new Thread(Worker);
                _threads[i].Start();
            }
        }

        public void WaitForCompletion()
        {
            foreach (var t in _threads)
            {
                t.Join();
            }
        }

        private void Worker()
        {
            while(true)
            {
                Action task;
                lock (_sync)
                {
                    while (_queue.Count <= 0 && !_done)
                        Monitor.Wait(_sync);
                    if (_done)
                        break;
                    task = _queue.Dequeue();
                    _activeTaskCount++;
                }

                try
                {
                    task();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("==== Unhandled Exception ====\r\n{0}", ex);
                }
                finally
                {
                    lock(_sync)
                    {
                        _activeTaskCount--;
                        if(_activeTaskCount <= 0 && _queue.Count <= 0)
                        {
                            _done = true;
                            Monitor.PulseAll(_sync);
                        }
                    }
                }
            }
        }
    }
}