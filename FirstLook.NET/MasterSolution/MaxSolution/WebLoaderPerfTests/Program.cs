﻿using System.Net;
using WebLoaderPerfTests.Properties;

namespace WebLoaderPerfTests
{
    class Program
    {
        static int Main()
        {
            ServicePointManager.ServerCertificateValidationCallback = (o, cert, chain, x) => true;

            return Settings.Default.AsyncClient 
                ? WebApiClientRunner.UseAsynchIO() 
                : HttpWebRequestRunner.UseSynchIO();
        }
    }
}
