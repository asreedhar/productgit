﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace WebLoaderPerfTests
{
    public class FileManager
    {
        private readonly string _cachePath;

        public FileManager(bool clearCache, string cachePath = "cache")
        {
            _cachePath = Path.GetFullPath(cachePath);
            if(!Directory.Exists(_cachePath))
            {
                Directory.CreateDirectory(_cachePath);
            }
            if(clearCache)
            {
                Console.WriteLine("Clearing cache directory '{0}'", _cachePath);
                foreach(var file in Directory.GetFiles(_cachePath, "*.*"))
                    File.Delete(file);
            }
        }

        public Task<string> ReadAsStringAsync(string name)
        {
            return ReadAsync(name, bytes => Encoding.UTF8.GetString(bytes));
        }

        public string ReadAsString(string name)
        {
            return Read(name, bytes => Encoding.UTF8.GetString(bytes));
        }

        public Task<T> ReadAsync<T>(string name, Func<byte[], T> transform)
        {
            var tcs = new TaskCompletionSource<T>();
            var path = Path.Combine(_cachePath, name);
            if(!File.Exists(path))
            {
                tcs.SetResult(default(T));
                return tcs.Task;
            }

            FileStream stream;
            try
            {
                stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read, 4096,
                                        FileOptions.Asynchronous | FileOptions.SequentialScan);
            }
            catch (Exception ex)
            {
                tcs.SetException(ex);
                return tcs.Task;
            }
            
            var bytes = new MemoryStream();
            var buffer = new byte[65536];

            Task<int>.Factory
                .FromAsync(stream.BeginRead, stream.EndRead, buffer, 0, buffer.Length, null)
                .ContinueWith(t => ProcessChunk(bytes, stream, buffer, t, tcs, transform));

            return tcs.Task;
        }

        private static void ProcessChunk<T>(MemoryStream bytes, Stream stream, byte[] buffer, Task<int> t, TaskCompletionSource<T> tcs, Func<byte[], T> transform)
        {
            if (t.IsFaulted)
            {
                tcs.TrySetException(new InvalidOperationException("Failed while reading file.", t.Exception));
                stream.Close();
                return;
            }

            if (t.Result == 0)
            {
                stream.Close();
                var result = transform(bytes.ToArray());
                tcs.SetResult(result);
            }
            else
            {
                bytes.Write(buffer, 0, t.Result);
                Task<int>.Factory
                    .FromAsync(stream.BeginRead, stream.EndRead, buffer, 0, buffer.Length, null)
                    .ContinueWith(t2 => ProcessChunk(bytes, stream, buffer, t2, tcs, transform));
            }
        }

        public T Read<T>(string name, Func<byte[], T> transform)
        {
            var path = Path.Combine(_cachePath, name);
            if (!File.Exists(path))
            {
                return default(T);
            }

            var bytes = File.ReadAllBytes(path);
            return transform(bytes);
        }

        public Task<byte[]> WriteAsync(string name, byte[] data)
        {
            return WriteAsync(name, data, d => d);
        }

        public Task<string> WriteAsync(string name, string data)
        {
            return WriteAsync(name, data, d => new UTF8Encoding(false).GetBytes(d));
        }

        public void Write(string name, string data)
        {
            Write(name, new UTF8Encoding(false).GetBytes(data));
        }

        public Task<T> WriteAsync<T>(string name, T data, Func<T, byte[]> transform)
        {
            return WriteAsync(name, transform(data), data);
        }

        public Task<T> WriteAsync<T>(string name, byte[] data, T passthru)
        {
            var path = Path.Combine(_cachePath, name);
            var stream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None, 4096,
                                        FileOptions.Asynchronous);
            return Task.Factory
                .FromAsync(stream.BeginWrite, stream.EndWrite, data, 0, data.Length, null)
                .ContinueWith(t =>
                                  {
                                      stream.Close();
                                      if(t.IsFaulted)
                                          throw new InvalidOperationException("Failed while writing file.", t.Exception);
                                      return passthru;
                                  });
        }

        public void Write(string name, byte[] data)
        {
            var path = Path.Combine(_cachePath, name);
            File.WriteAllBytes(path, data);
        }

        public void Delete(string name)
        {
            var path = Path.Combine(_cachePath, name);
            File.Delete(path);
        }

        public string GetPath(string name)
        {
            return Path.Combine(_cachePath, name);
        }
    }
}