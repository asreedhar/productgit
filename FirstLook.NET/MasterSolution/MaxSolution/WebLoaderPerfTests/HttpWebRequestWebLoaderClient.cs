using System;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;

namespace WebLoaderPerfTests
{
    internal class HttpWebRequestWebLoaderClient
    {
        private readonly FileManager _fileManager;
        private readonly LogManager _logManager;
        private readonly string _baseUrl;
        private readonly string _userName;
        private readonly string _password;
        private readonly bool _pipelined;
        private readonly bool _compressed;

        public HttpWebRequestWebLoaderClient(FileManager fileManager, LogManager logManager, string baseUrl, string userName, string password, bool pipelined, bool compressed)
        {
            _fileManager = fileManager;
            _logManager = logManager;
            _baseUrl = baseUrl;
            _userName = userName;
            _password = password;
            _pipelined = pipelined;
            _compressed = compressed;
        }

        public JArray GetDealers()
        {
            return GetResponse("dealers", ".json", "dealers",
                ReadAsString,
                (files, name) => files.ReadAsString(name),
                (files, name, content) => files.Write(name, content),
                JArray.Parse);
        }

        public JArray GetVehicles(string dealer)
        {
            return GetResponse(
                string.Format("dealers/{0}/vehicles", dealer),
                ".json", "vehicles",
                ReadAsString,
                (files, name) => files.ReadAsString(name),
                (files, name, content) => files.Write(name, content),
                JArray.Parse);
        }

        public JObject GetVehicle(string dealer, int id)
        {
            return GetResponse(
                string.Format("dealers/{0}/vehicles/{1}", dealer, id),
                ".json", "vehicle",
                ReadAsString,
                (files, name) => files.ReadAsString(name),
                (files, name, content) => files.Write(name, content),
                JObject.Parse);
        }

        public byte[] GetThumb(string dealer, int id)
        {
            return GetResponse(
                string.Format("dealers/{0}/vehicles/{1}/thumb.jpg", dealer, id),
                ".jpg", "thumb",
                ReadAsBytes,
                (files, name) => files.Read(name, b => b),
                (files, name, content) => files.Write(name, content),
                b => b);
        }

        public JObject GetTrim(string trim)
        {
            return GetResponse(
                string.Format("trims/{0}", trim),
                ".json", "trim",
                ReadAsString,
                (files, name) => files.ReadAsString(name),
                (files, name, content) => files.Write(name, content),
                JObject.Parse);
        }

        private TFinal GetResponse<TFinal, TIntermediate>(string relativeUrl,
                                                        string fileExtension,
                                                        string cls,
                                                        Func<HttpWebResponse, TIntermediate> calcIntermediateFromContent, 
                                                        Func<FileManager, string, TIntermediate> readFromFile,
                                                        Action<FileManager, string, TIntermediate> writeToFile,
                                                        Func<TIntermediate, TFinal> calcFinal)
        {
            var cleanedUrl = Regex.Replace(relativeUrl, "[/?=&]", "-");
            var etagFilename = cleanedUrl + "-etag.txt";
            var contentFilename = cleanedUrl + fileExtension;

            var request = CreateRequest(relativeUrl);

            var perf = new PerfAccumulator(_logManager, request.RequestUri.ToString(), _pipelined, _compressed, cls);

            Exception exception = null;
            HttpWebResponse response = null;
            var content = default(TIntermediate);
            try
            {
                var etag = ReadETag(etagFilename);
                ApplyETagToRequest(request, etag);

                response = BetterGetResponse(request);
                perf.SetResponse(response);
                EnsureSuccess(response, request);

                content = response.StatusCode == HttpStatusCode.NotModified
                                  ? readFromFile(_fileManager, contentFilename)
                                  : calcIntermediateFromContent(response);
            }
            catch(Exception ex)
            {
                exception = ex;
            }

            perf.Completed(exception);

            if(exception != null)
            {
                _fileManager.Delete(etagFilename);
                _fileManager.Delete(contentFilename);
            }
            else
            {
                WriteETag(etagFilename, response);
                writeToFile(_fileManager, contentFilename, content);
            }

            return calcFinal(content);
        }

        private static HttpWebResponse BetterGetResponse(HttpWebRequest request)
        {
            try
            {
                return (HttpWebResponse)request.GetResponse();
            }
            catch(WebException ex)
            {
                var response = (HttpWebResponse) ex.Response;
                if (response != null && response.StatusCode == HttpStatusCode.NotModified)
                    return response;

                throw;
            }
        }

        private HttpWebRequest CreateRequest(string relativeUrl)
        {
            var uri = new Uri(new Uri(_baseUrl), relativeUrl);

            var request = (HttpWebRequest) WebRequest.Create(uri);
            request.Method = "GET";

            request.PreAuthenticate = true;
            request.Credentials = new NetworkCredential(_userName, _password);

            if(_compressed)
                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            request.Pipelined = _pipelined;

            return request;
        }

        private string ReadAsString(HttpWebResponse response)
        {
            return Encoding.UTF8.GetString(ReadAsBytes(response));
        }

        private byte[] ReadAsBytes(HttpWebResponse response)
        {
            var tmp = new MemoryStream();
            using(var str = response.GetResponseStream())
            {
                if(str == null)
                    throw new Exception("No response content found.");
                str.CopyTo(tmp);
                return tmp.ToArray();
            }
        }

        private string ReadETag(string filename)
        {
            return _fileManager.ReadAsString(filename);
        }

        private void WriteETag(string filename, HttpWebResponse response)
        {
            var etag = response == null || response.Headers.Get("ETag") == null
                           ? null
                           : response.Headers.Get("ETag");

            if (etag == null)
            {
                _fileManager.Delete(filename);
            }

            _fileManager.Write(filename, etag);
        }

        private static void ApplyETagToRequest(HttpWebRequest request, string etag)
        {
            if (etag != null)
                request.Headers.Add("If-None-Match", etag);
        }

        private static void EnsureSuccess(HttpWebResponse response, HttpWebRequest request)
        {
            var status = (int)response.StatusCode;
            var success = (status == 304) || (status >= 200 && status < 300);
            if (!success)
                throw new InvalidOperationException(string.Format("Invalid http status for url '{0}': {1}",
                                                                  request.RequestUri,
                                                                  response.StatusCode));
        }
    }
}