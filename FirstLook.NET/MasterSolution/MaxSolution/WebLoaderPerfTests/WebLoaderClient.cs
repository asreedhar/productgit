﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace WebLoaderPerfTests
{
    public class WebLoaderClient
    {
        private readonly FileManager _files;
        private readonly LogManager _logger;
        private readonly string _baseUrl;
        private readonly string _userName;
        private readonly string _password;
        private readonly bool _shouldBePipelined;
        private readonly bool _acceptCompressed;

        public WebLoaderClient(FileManager files, LogManager logger, string baseUrl, string userName, string password, bool shouldBePipelined, bool acceptCompressed)
        {
            _files = files;
            _logger = logger;
            _baseUrl = baseUrl;
            _userName = userName;
            _password = password;
            _shouldBePipelined = shouldBePipelined;
            _acceptCompressed = acceptCompressed;
            ServicePointManager.DefaultConnectionLimit = 3;
        }

        private HttpClient CreateClient()
        {
            var webRequestHandler = new WebRequestHandler
                                        {
                                            AllowPipelining = _shouldBePipelined,
                                            AutomaticDecompression = _acceptCompressed ? DecompressionMethods.GZip : DecompressionMethods.None,
                                        };
            var client = HttpClientFactory.Create(webRequestHandler);
            client.BaseAddress = new Uri(_baseUrl);
            client.DefaultRequestHeaders.Authorization = CalcAuthorizationHeader();
            return client;
        }

        private AuthenticationHeaderValue CalcAuthorizationHeader()
        {
            return new AuthenticationHeaderValue("Basic", 
                Convert.ToBase64String(
                    Encoding.ASCII.GetBytes(
                        string.Format("{0}:{1}", _userName, _password))));
        }

        public Task<JArray> GetDealers()
        {
            return GetResponse("dealers", ".json", "dealers",
                c => c.ReadAsStringAsync(), 
                (files, name) => files.ReadAsStringAsync(name), 
                (files, name, content) => files.WriteAsync(name, content), 
                JArray.Parse);
        }

        public Task<JArray> GetVehicles(string dealer)
        {
            return GetResponse(string.Format("dealers/{0}/vehicles", dealer), ".json", "vehicles",
                c => c.ReadAsStringAsync(), 
                (files, name) => files.ReadAsStringAsync(name),
                (files, name, content) => files.WriteAsync(name, content),
                JArray.Parse);
        }

        public Task<JObject> GetVehicle(string dealer, int id)
        {
            return GetResponse(string.Format("dealers/{0}/vehicles/{1}", dealer, id), ".json", "vehicle",
                c => c.ReadAsStringAsync(),
                (files, name) => files.ReadAsStringAsync(name),
                (files, name, content) => files.WriteAsync(name, content),
                JObject.Parse);
        }

        public Task<byte[]> GetThumb(string dealer, int id)
        {
            return GetResponse(string.Format("dealers/{0}/vehicles/{1}/thumb.jpg?w=200", dealer, id), ".jpg", "thumb",
                c => c.ReadAsByteArrayAsync(),
                (files, name) => files.ReadAsync(name, t => t),
                (files, name, content) => files.WriteAsync(name, content),
                i => i);
        }

        public Task<JObject> GetTrim(string trim)
        {
            return GetResponse(string.Format("trims/{0}", trim), ".json", "trim",
                c => c.ReadAsStringAsync(), 
                (files, name) => files.ReadAsStringAsync(name),
                (files, name, content) => files.WriteAsync(name, content),
                JObject.Parse);
        }

        private Task<TFinal> GetResponse<TFinal, TIntermediate>(string relativeUrl,
                                                                string fileExtension,
                                                                string cls,
                                                                Func<HttpContent, Task<TIntermediate>> calcIntermediateFromContent, 
                                                                Func<FileManager, string, Task<TIntermediate>> readFromFile,
                                                                Func<FileManager, string, TIntermediate, Task> writeToFile,
                                                                Func<TIntermediate, TFinal> calcFinal)
        {
            var cleanedUrl = Regex.Replace(relativeUrl, "[/?=&]", "-");
            var etagFilename = cleanedUrl + "-etag.txt";
            var contentFilename = cleanedUrl + fileExtension;

            var request = new HttpRequestMessage(HttpMethod.Get, relativeUrl);

            var perf = new PerfAccumulator(_logger, request.RequestUri.ToString(), _shouldBePipelined, _acceptCompressed, cls);

            HttpResponseMessage response = null;

            return
                ReadETag(etagFilename)
                    .ContinueWith(t =>
                                      {
                                          ApplyETagToRequest(request, t.Result);
                                          return CreateClient().SendAsync(request);
                                      })
                    .Unwrap()
                    .ContinueWith(t =>
                                      {
                                          if(t.IsFaulted)
                                          {
                                              throw new InvalidOperationException("Failed while processing request",
                                                                                  t.Exception);
                                          }
                                          response = t.Result;
                                          perf.SetResponse(response);
                                          EnsureSuccess(response);
                                          return t.Result.StatusCode == HttpStatusCode.NotModified 
                                              ? readFromFile(_files, contentFilename) 
                                              : calcIntermediateFromContent(t.Result.Content);
                                      })
                    .Unwrap()
                    .ContinueWith(t =>
                                      {
                                          perf.Completed(t.IsFaulted ? t.Exception : null);
                                          
                                          if(t.IsFaulted)
                                          {
                                              _files.Delete(etagFilename);
                                              _files.Delete(contentFilename);
                                              var tcs = new TaskCompletionSource<TFinal>();
                                              tcs.SetException(
                                                  new InvalidOperationException("Failed while processing response",
                                                                                t.Exception));
                                              return tcs.Task;
                                          }

                                          var writeEtagTask = WriteETag(etagFilename, response);
                                          var writeContentTask = writeToFile(_files, contentFilename, t.Result);
                                          return Task.Factory.ContinueWhenAll(new[] { writeEtagTask, writeContentTask },
                                                                              t2 => calcFinal(t.Result), TaskContinuationOptions.ExecuteSynchronously);
                                      })
                    .Unwrap();
        }

        private Task<string> ReadETag(string filename)
        {
            return _files.ReadAsStringAsync(filename);
        }

        private Task WriteETag(string filename, HttpResponseMessage response)
        {
            var etag = response == null || response.Headers.ETag == null
                           ? null
                           : response.Headers.ETag.Tag;

            if(etag == null)
            {
                _files.Delete(filename);
                var tcs = new TaskCompletionSource<object>();
                tcs.SetResult(null);
                return tcs.Task;
            }

            return _files.WriteAsync(filename, etag);
        }

        private static void ApplyETagToRequest(HttpRequestMessage request, string etag)
        {
            if(etag != null)
                request.Headers.IfNoneMatch.Add(new EntityTagHeaderValue(etag, false));
        }

        private static void EnsureSuccess(HttpResponseMessage response)
        {
            var status = (int) response.StatusCode;
            var success = (status == 304) || (status >= 200 && status < 300);
            if (!success)
                throw new InvalidOperationException(string.Format("Invalid http status for url '{0}': {1}",
                                                                  response.RequestMessage.RequestUri,
                                                                  response.StatusCode));
        }
    }
}