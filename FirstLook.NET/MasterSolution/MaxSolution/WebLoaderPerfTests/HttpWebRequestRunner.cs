﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace WebLoaderPerfTests
{
    public static class HttpWebRequestRunner
    {
        public static int UseSynchIO()
        {
            var settings = Properties.Settings.Default;
            var fileManager = new FileManager(false);
            using (var logManager = new LogManager(fileManager))
            {
                logManager.WriteCsvHeader();

                var client = new HttpWebRequestWebLoaderClient(fileManager, logManager, settings.BaseUrl,
                                                               settings.UserName,
                                                               settings.Password, settings.Pipelined,
                                                               settings.Compressed);

                var threads = new ThreadManager(settings.RequestCount);

                threads.Enqueue(
                    () => ProcessDealers(client.GetDealers(), threads, client, fileManager));

                Console.WriteLine("Waiting for data...");
                try
                {
                    threads.StartThreads();
                    threads.WaitForCompletion();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Failed with exception: ");
                    Console.WriteLine(ex.ToString());
                    return 1;
                }

                Console.WriteLine("Finished.");
            }
            return 0;
        }

        private static void ProcessDealers(JToken dealers, ThreadManager threads, HttpWebRequestWebLoaderClient client, FileManager fileManager)
        {
            fileManager.Write("pretty-dealers.json", dealers.ToBytes());

            var dealerCode = dealers
                    .Where(d => (string)d.SelectToken("Desc") == "Windy City BMW")
                    .Select(d => (string)d.SelectToken("Id"))
                    .Single();

            threads.Enqueue(() => ProcessVehicles(client.GetVehicles(dealerCode), threads, client, dealerCode,
                                                  fileManager));
        }

        private static void ProcessVehicles(JArray vehicles, ThreadManager threads, HttpWebRequestWebLoaderClient client, string dealerCode, FileManager fileManager)
        {
            fileManager.Write("pretty-vehicles.json", vehicles.ToBytes());
            
            var processedTrims = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);

            foreach (var vehicle in vehicles)
            {
                var trim = (string)vehicle.SelectToken("Trim.Id");
                if (trim != null && processedTrims.Add(trim))
                {
                    threads.Enqueue(() => ProcessTrim(client.GetTrim(trim), fileManager, trim));
                }

                var id = (int)vehicle.SelectToken("Id");
                threads.Enqueue(() => ProcessVehicle(client.GetVehicle(dealerCode, id), dealerCode, fileManager, id));
                threads.Enqueue(() => client.GetThumb(dealerCode, id));
            }

        }

        private static void ProcessVehicle(JObject vehicle, string dealerCode, FileManager fileManager, int id)
        {
            fileManager.Write(
                string.Format("pretty-dealers-{0}-vehicles-{1}.json", dealerCode, id),
                vehicle.ToBytes());
        }

        private static void ProcessTrim(JObject trim, FileManager fileManager, string trimId)
        {
            fileManager.Write(
                string.Format("pretty-trims-{0}.json", trimId), trim.ToBytes());
        }
    }
}