﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace WebLoaderPerfTests
{
    public static class WebApiClientRunner
    {
        public static int UseAsynchIO()
        {
            TaskScheduler.UnobservedTaskException +=
                (o, e) => Console.WriteLine("====== Unobserved Exception: {0} ====", e);

            var settings = Properties.Settings.Default;
            var fileManager = new FileManager(false);
            using (var logManager = new LogManager(fileManager))
            {
                logManager.WriteCsvHeader();

                var client = new WebLoaderClient(fileManager, logManager, settings.BaseUrl, settings.UserName,
                                                 settings.Password, settings.Pipelined, settings.Compressed);

                var tasks = new TaskManager(settings.RequestCount);

                tasks.Enqueue(
                    () => client
                              .GetDealers()
                              .ContinueWith(t =>
                              {
                                  ProcessDealers(t.Result, tasks, client, fileManager);
                                  return fileManager.WriteAsync("pretty-dealers.json", t.Result,
                                                                d => d.ToBytes());
                              }).Unwrap());

                Console.WriteLine("Waiting for data...");
                try
                {
                    tasks.Task.Wait();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Failed with exception: ");
                    Console.WriteLine(ex.ToString());
                    return 1;
                }

                Console.WriteLine("Finished.");
            }

            return 0;
        }

        private static void ProcessDealers(IEnumerable<JToken> dealers, TaskManager tasks, WebLoaderClient client, FileManager files)
        {
            var dealerCode = dealers
                    .Where(d => (string)d.SelectToken("Desc") == "Windy City BMW")
                    .Select(d => (string)d.SelectToken("Id"))
                    .Single();

            tasks.Enqueue(
                () => client
                    .GetVehicles(dealerCode)
                    .ContinueWith(t =>
                    {
                        ProcessVehicles(t.Result, tasks, client, dealerCode, files);
                        return
                            files.WriteAsync(
                                string.Format("pretty-dealers-{0}-vehicles.json", dealerCode),
                                t.Result,
                                d => d.ToBytes());
                    }).Unwrap());
        }

        private static void ProcessVehicles(IEnumerable<JToken> vehicles, TaskManager tasks, WebLoaderClient client, string dealer, FileManager files)
        {
            var processedTrims = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);
            foreach (var vehicle in vehicles)
            {
                var trim = (string)vehicle.SelectToken("Trim.Id");
                if (trim != null && processedTrims.Add(trim))
                {
                    ProcessTrim(tasks, client, files, trim);
                }

                var id = (int)vehicle.SelectToken("Id");
                ProcessVehicle(tasks, client, dealer, files, id);
                ProcessThumb(tasks, client, dealer, id);
            }
        }

        private static void ProcessThumb(TaskManager tasks, WebLoaderClient client, string dealer, int id)
        {
            tasks.Enqueue(() => client.GetThumb(dealer, id));
        }

        private static void ProcessTrim(TaskManager tasks, WebLoaderClient client, FileManager files, string trim)
        {
            tasks.Enqueue(
                () => client
                          .GetTrim(trim)
                          .ContinueWith(t => files.WriteAsync(
                              string.Format("pretty-trims-{0}.json", trim), t.Result, d => d.ToBytes()))
                          .Unwrap());
        }

        private static void ProcessVehicle(TaskManager tasks, WebLoaderClient client, string dealer, FileManager files, int id)
        {
            tasks.Enqueue(
                () => client
                          .GetVehicle(dealer, id)
                          .ContinueWith(t => files.WriteAsync(
                              string.Format("pretty-dealers-{0}-vehicles-{1}.json", dealer, id),
                              t.Result,
                              d => d.ToBytes())));
        } 
    }
}