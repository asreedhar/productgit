using System;
using System.Net;
using System.Net.Http;

namespace WebLoaderPerfTests
{
    public class PerfAccumulator
    {
        private readonly LogManager _logger;
        private readonly string _requestUri;
        private readonly bool _pipeliningUsed;
        private readonly DateTimeOffset _start;
        private readonly string _cls;

        private long? _responseContentLength;
        private HttpStatusCode? _responseStatus;
        private readonly bool? _responseCompression;

        public PerfAccumulator(LogManager logger, string requestUri, bool pipeliningUsed, bool compressionUsed, string cls)
        {
            _logger = logger;
            _requestUri = requestUri;
            _pipeliningUsed = pipeliningUsed;
            _cls = cls;
            _responseCompression = compressionUsed;
            _start = DateTimeOffset.UtcNow;
        }

        public void SetResponse(HttpResponseMessage response)
        {
            var cmp = StringComparer.InvariantCultureIgnoreCase;

            if (response != null)
            {
                _responseStatus = response.StatusCode;
                var content = response.Content;
                if (content != null)
                {
                    _responseContentLength = content.Headers.ContentLength;
                }
            }
        }

        public void SetResponse(HttpWebResponse response)
        {
            var cmp = StringComparer.InvariantCultureIgnoreCase;

            if(response != null)
            {
                _responseStatus = response.StatusCode;
                
                var contentLength = response.Headers.Get("Content-Length");
                int contentLengthInt;
                if (contentLength != null && int.TryParse(contentLength, out contentLengthInt))
                    _responseContentLength = contentLengthInt;
            }
        }

        public void Completed(Exception ex = null)
        {
            var stop = DateTimeOffset.UtcNow;

            _logger.WritePerfLog(
                _cls,
                _requestUri,
                _start, stop,
                _responseContentLength, _responseStatus, _responseCompression,
                _pipeliningUsed, ex != null ? ex.Message : "");
        }
    }
}