using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebLoaderPerfTests
{
    public class TaskManager
    {
        private readonly object _lock = new object();
        private readonly Queue<Func<Task>> _queue = new Queue<Func<Task>>();
        private readonly Dictionary<int, Task> _tasks = new Dictionary<int, Task>();
        
        private readonly TaskCompletionSource<object> _tcs = new TaskCompletionSource<object>();
        private readonly int _taskLimit;
        private int _counter;

        public TaskManager(int taskLimit = 10)
        {
            _taskLimit = taskLimit;
        }

        public void Enqueue(params Func<Task>[] tasks)
        {
            lock(_lock)
            {
                foreach(var task in tasks)
                    _queue.Enqueue(task);
                CheckTasks();
            }
        }

        private void TaskCompleted(Task task, int id)
        {
            lock (_lock)
            {
                if(task.IsFaulted)
                    SignalFailure(task.Exception);

                _tasks.Remove(id);
                CheckTasks();
            }
        }

        private void CheckTasks()
        {
            if(_queue.Count <= 0 && _tasks.Count <= 0)
            {
                SignalComplete();
                return;
            }

            while(_tasks.Count < _taskLimit && _queue.Count > 0)
            {
                StartTask();
            }
        }

        private void StartTask()
        {
            var id = ++_counter;
            var taskFunc = _queue.Dequeue();
            var task = taskFunc();
            _tasks.Add(id, task);
            task.ContinueWith(t => TaskCompleted(t, id),
                              TaskContinuationOptions.ExecuteSynchronously);
        }


        private void SignalComplete()
        {
            _tcs.TrySetResult(null);
        }

        private void SignalFailure(Exception ex)
        {
            _tcs.TrySetException(ex);
        }

        public Task Task
        {
            get { return _tcs.Task; }
        }
    }
}