﻿using System.Web.Services;
using Max.Selling.Common;
using Max.WebImport.Domain;
using Max.WebImport.Domain.TransferObjects.V_1_0;
using FirstLook.Common.Core.Command;

namespace Max.Selling.WebImportServices.SoapServices.V_1_0
{
    /// <summary>
    /// Summary description for Services
    /// </summary>
    [WebService(Namespace = "http://max.firstlook.biz/maxservices/imports/soapservices")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class Pricing : WebService
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [WebMethod]
        public Status InternetPriceUpdate(VehicleAuthorization auth, string vin, int internetPrice)
        {
            Log.DebugFormat("InternetPriceUpdate called by UserName={0}, BusinessUnit={1}, Vin={2}, Price={3}", auth.UserName, auth.BusinessUnitID, vin, internetPrice);

            var providerAuthorizationCommand = new ProviderAuthorizationCommand(auth.UserName, auth.Password);
            AbstractCommand.DoRun(providerAuthorizationCommand);
            
            //Are they a valid provider with correct password
            if (!providerAuthorizationCommand.Authorized)
            {
                Log.Info("No Provider Found. Returning Unathorized.");
                return Status.Unauthorized;
            }

            var accountCommand = new AccountCommand(auth.BusinessUnitID, ActionType.Pricing, providerAuthorizationCommand.ProviderID);
            AbstractCommand.DoRun(accountCommand);
            
            //Does the provider have an account for businessUnit and can they update pricing
            if (!accountCommand.Authorized)
            {
                Log.InfoFormat("No account for BusinessUnit={0} authorizing provider={1}. Returning Permission Denied", auth.BusinessUnitID, providerAuthorizationCommand.ProviderID);
                return Status.PermissionDenied;
            }

            //Check if valid price
            if(!PriceUpdateCommand.IsPriceValid(internetPrice))
            {
                Log.InfoFormat("Invalid Price {0}", internetPrice);
                return Status.InvalidParameter;
            }

            var vehicleCommand = new VehicleCommand(auth.BusinessUnitID, vin);
            AbstractCommand.DoRun(vehicleCommand);
            if(vehicleCommand.ErrorInfo.HasError)
            {
                Log.Info("VehicleCommand Internal Error.");
                return Status.InternalError;
            }

            if(!vehicleCommand.IsUsed)
            {
                Log.Info("New cars not supported");
                return Status.NewCarsNotSupported;
            }

            var priceUpdateCommand = new PriceUpdateCommand(auth.BusinessUnitID, vehicleCommand.InventoryID, internetPrice, providerAuthorizationCommand.ProviderName);
            AbstractCommand.DoRun(priceUpdateCommand);

            if (priceUpdateCommand.ErrorInfo.HasError)
            {
                Log.Info("PriceUpdateCommand Internal Error.");
                return Status.InternalError;
            }

            Log.InfoFormat("InternetPriceUpdate updated UserName={0}, BusinessUnit={1}, Vin={2}, Price={3}", auth.UserName, auth.BusinessUnitID, vin, internetPrice);
            return Status.Ok;            
        }
    }
}
