﻿using System.Data.Entity.Infrastructure;
using System.Data.Objects;
using System.Data.Services;
using System.Data.Services.Common;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using DataWebServices.Properties;
using FirstLook.Common.Core.ServiceModel;

namespace DataWebServices.Reports.VehicleActivity
{
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    [ErrorHandlerBehavior]
    public class Service : DataService<ObjectContext>
    {
        public static void InitializeService(DataServiceConfiguration config)
        {
            config.UseVerboseErrors = Settings.Default.DataServices_UseVerboseErrors;
            
            config.SetEntitySetAccessRule("VehicleActivity", EntitySetRights.AllRead);
            config.SetEntitySetAccessRule("ActivityThreshold", EntitySetRights.AllRead);

            config.SetServiceOperationAccessRule("GetVehicleActivity", ServiceOperationRights.AllRead);
            
            config.DataServiceBehavior.MaxProtocolVersion = DataServiceProtocolVersion.V2;
        }

        protected override ObjectContext CreateDataSource()
        {
            var context = new Context();

            var objectContext = ((IObjectContextAdapter)context).ObjectContext;
            objectContext.ContextOptions.ProxyCreationEnabled = false;

            return objectContext;
        }

        [WebGet]
        public IQueryable<VehicleActivity> GetVehicleActivity(string memberName)
        {
            using (var buContext = new Data.BusinessUnit.Context())
            {
                var member = buContext.Member.Single(m => m.Login == memberName);

                var query = (IQueryable<VehicleActivity>) CurrentDataSource.CreateObjectSet<VehicleActivity>();

                if (!member.IsAdministrator())
                {
                    var allowedBusinessUnits =
                       buContext.BusinessUnitByMember
                            .Where(bu => bu.MemberID == member.MemberID)
                            .Select(bu => bu.BusinessUnitID)
                            .ToArray();
                    query = query.Where(va => allowedBusinessUnits.Contains(va.BusinessUnitID));
                }

                return query;
            }
        }
    }
}