﻿using System;

namespace DataWebServices.Reports.VehicleActivity
{
    public class VehicleActivity
    {
        public int BusinessUnitID { get; set; }
        public int InventoryID { get; set; }
        public string VIN { get; set; }
        public string StockNumber { get; set; }

        public int? Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Trim { get; set; }

        public int? Mileage { get; set; }
        public int? AgeInDays { get; set; }
        public byte? InventoryType { get; set; }

        public decimal? InternetPrice { get; set; }
        public decimal? PctAvgMarketPrice { get; set; }
        public int? AvgMarketPrice { get; set; }

        public int DestinationID { get; set; }

        public DateTime? ApprovalDate { get; set; }

        public float? CTR { get; set; }
        public float? CTRSinceLastReprice { get; set; }
        public int? SearchCount { get; set; }
        public int? DetailsPageViewCount { get; set; }
        public int? SearchPageViewsSinceLastReprice { get; set; }
        public int? DetailPageViewsSinceLastReprice { get; set; }
        public int? ActionCount { get; set; }

        public float LowActivityThreshold { get; set; }
        public float HighActivityThreshold { get; set; }
        public int MinSearchCountThreshold { get; set; }
    }
}