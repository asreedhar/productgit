using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using DataWebServices.Data.BusinessUnit;
using DataWebServices.Properties;

namespace DataWebServices.Reports.VehicleActivity
{
    public class Context : DbContext
    {
        public Context()
            : base("name=Merchandising")
        {
            ((IObjectContextAdapter) this).ObjectContext.CommandTimeout = Settings.Default.DatabaseTimeout_Seconds;
        }

        public DbSet<VehicleActivity> VehicleActivity { get; set; }
        public DbSet<ActivityThreshold> ActivityThreshold { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<VehicleActivity>().ToTable("VehicleActivity", "Reports");
            modelBuilder.Entity<VehicleActivity>().HasKey(a => new {a.BusinessUnitID, a.VIN, a.StockNumber, a.DestinationID});

            modelBuilder.Entity<ActivityThreshold>().ToTable("VehicleActivity#Thresholds", "Reports");
            modelBuilder.Entity<ActivityThreshold>().HasKey(a => new {a.BusinessUnitID});

            base.OnModelCreating(modelBuilder);
        }
    }
}