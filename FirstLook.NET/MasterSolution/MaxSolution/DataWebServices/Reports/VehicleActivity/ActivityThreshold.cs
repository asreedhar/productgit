﻿namespace DataWebServices.Reports.VehicleActivity
{
    public class ActivityThreshold
    {
        public int BusinessUnitID { get; set; }

        public double LowActivityThreshold { get; set; }
        public double HighActivityThreshold { get; set; }
        public int MinSearchCountThreshold { get; set; }
    }
}