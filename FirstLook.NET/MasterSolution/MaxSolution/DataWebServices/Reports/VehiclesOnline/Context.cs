using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using DataWebServices.Properties;

namespace DataWebServices.Reports.VehiclesOnline
{
    public class Context : DbContext
    {
        public Context()
            : base("name=Merchandising")
        {
            ((IObjectContextAdapter) this).ObjectContext.CommandTimeout = Settings.Default.DatabaseTimeout_Seconds;
        }

        public DbSet<VehiclesOnline> VehiclesOnline { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<VehiclesOnline>().ToTable("VehiclesOnline", "Reports");
            modelBuilder.Entity<VehiclesOnline>().HasKey(a => new { a.BusinessUnitId, a.InventoryId });

            base.OnModelCreating(modelBuilder);
        }
    }
}