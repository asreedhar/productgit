﻿using System;

namespace DataWebServices.Reports.VehiclesOnline
{
    public class VehiclesOnline
    {
        public int BusinessUnitId { get; set; }
        public int InventoryId { get; set; }
        public string StockNumber { get; set; }
        public string Vin { get; set; }
        public DateTime InventoryReceivedDate { get; set; }
        public int VehicleYear { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Trim { get; set; }
        public byte InventoryType { get; set; }
        public bool Approved { get; set; }

        public bool HasAtData { get; set; }
        public bool AtOnline { get; set; }

        public bool HasCarsData { get; set; }
        public bool CarsOnline { get; set; }
    }
}