﻿namespace DataWebServices.ViewModels
{
    public class MaxAdFilterVm
    {
        public string DisplayName { get; set; }
        public int Count { get; set; }
        public string Url { get; set; }
    }
}