﻿using System.Collections.Generic;

namespace DataWebServices.ViewModels
{
    public class MaxAdSummaryVm
    {
        public MaxForSellingAndEmailVm MaxForSellingAndEmail { get; set; }
        public DashboardVm Dashboard { get; set; }
        public MaxAdFiltersVm Filters { get; set; }
        public int BusinessUnitId { get; set; }
        public byte MaxVersion { get; set; }
        public Dictionary<string,bool> Roles { get; set; }
    }
}