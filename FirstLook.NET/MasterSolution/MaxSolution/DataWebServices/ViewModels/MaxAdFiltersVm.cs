﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataWebServices.ViewModels
{
    public class MaxAdFiltersVm
    {
        public MaxAdFilterVm NoPrice { get; set; }

        public MaxAdFilterVm NotOnline { get; set; }

        public MaxAdFilterVm NoPhotos { get; set; }

        public MaxAdFilterVm NoEquipment { get; set; }

        public MaxAdFilterVm LowActivity { get; set; }

        public MaxAdFilterVm LowAdQuality { get; set; }

        public MaxAdFilterVm DueForRepricing { get; set; }

        public MaxAdFilterVm NoPriceComparison { get; set; }
    }
}