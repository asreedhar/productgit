﻿namespace DataWebServices.ViewModels
{
    public class DashboardVm
    {
        public bool IsEnabled { get; set; }
        public string Url { get; set; }
    }
}