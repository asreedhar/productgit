﻿namespace DataWebServices.ViewModels
{
    public class MaxForSellingAndEmailVm
    {
        public bool IsEnabled
        {
            get { return HasMaxForSellingAndEmail; }
        }
        public bool HasMaxForSellingAndEmail { get; set; }
        public bool HasWebsitePdf { get; set; }
        public bool HasWebSitePdfMarketListings { get; set; }
   
    }
}