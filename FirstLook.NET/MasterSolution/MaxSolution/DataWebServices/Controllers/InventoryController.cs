﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DataWebServices.ViewModels;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Commands;
using FirstLook.Merchandising.DomainModel.Dashboard;
using FirstLook.Merchandising.DomainModel.Dashboard.Entities;
using FirstLook.Merchandising.DomainModel.Templating;
using log4net;
using MAX.Entities;

namespace DataWebServices.Controllers
{
    public class InventoryController : Controller
    {
        private static readonly ILog Log = LogManager.GetLogger( typeof( InventoryController ).FullName );

        private IDashboardManager _manager;
        private int _businessUnitId;
        private string _userName;
        private MaxAdSummaryVm _maxAdSummary;

        private const string InventoryPageUrl = "/merchandising/workflow/inventory.aspx?usednew=2";
        private const string NotOnlineReportUrl = "/merchandising/Reports/VehicleOnlineStatusReport.aspx?usednew=2";
        private const string DashboardUrl = "/merchandising/dashboard.aspx";

        public InventoryController(IDashboardManager manager)
        {
            _manager = manager;
        }

        [HttpGet]
        public JsonResult GetActionNeeded( int businessUnitId, string userName )
        {
            try
            {
               _businessUnitId = businessUnitId;
               _userName = userName;

               CreateMaxAdSummary();
               PopulateDashboard();
               PopulateMaxForSellingAndEmail();
               PopulateFilters();
               PopulateUserRoles();

               return Json( _maxAdSummary, JsonRequestBehavior.AllowGet );

            }catch(Exception ex)
            {
                Log.ErrorFormat(
                    "Exception thrown for an MVC request BucketsData.  BusinessUnitId : {0} ,  Message : {1}, StackTrace : {2}",
                    businessUnitId,  ex.Message, ex.StackTrace);
                var data = new {errorMessage = ex.Message};

                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }

        private void CreateMaxAdSummary()
        {
            _maxAdSummary = new MaxAdSummaryVm
                                {
                                    BusinessUnitId = _businessUnitId,
                                    MaxVersion = _manager.GetMaxVersion(_businessUnitId)
                                };
        }


        private void PopulateDashboard ()
        {
            var dashboard = new DashboardVm{Url = string.Empty};
            dashboard.IsEnabled = _manager.IsDashboardEnabled(_businessUnitId);
            
            if(dashboard.IsEnabled)
                dashboard.Url = DashboardUrl;
            _maxAdSummary.Dashboard = dashboard;
        }

        private void PopulateMaxForSellingAndEmail ()
        {
            var maxSe = new MaxForSellingAndEmailVm
                            {
                                HasMaxForSellingAndEmail = _manager.SupportsMaxSe(_businessUnitId),
                                HasWebsitePdf = _manager.HasDealerUpgrade(_businessUnitId, Upgrade.WebSitePDF),
                                HasWebSitePdfMarketListings =
                                    _manager.HasDealerUpgrade(_businessUnitId, Upgrade.WebSitePDFMarketListings)
                            };
            _maxAdSummary.MaxForSellingAndEmail = maxSe;
        }

        private void PopulateFilters()
        {
            _maxAdSummary.Filters = new MaxAdFiltersVm();
            PopulateNotOnlineFilter();
            PopulateInventoryPageFilters();
        }

        private void PopulateUserRoles()
        {
            _maxAdSummary.Roles = _manager.GetRoles( _userName );
        }

        private void PopulateNotOnlineFilter ()
        {
            var notOnlineStatus = _manager.GetNotOnlineCount(_businessUnitId, VehicleType.Used);
            var notOnlineUrl = _maxAdSummary.Dashboard.IsEnabled ? DashboardUrl : NotOnlineReportUrl;
            var notOnlineFilter = new MaxAdFilterVm {Count = notOnlineStatus.NotOnlineCount, DisplayName = "Not Online", Url = notOnlineUrl};
            _maxAdSummary.Filters.NotOnline = notOnlineFilter;

        }

        private void PopulateInventoryPageFilters()
        {
            var thresholds = _manager.GetBucketAlertThresholds(_businessUnitId);
            var filterData = _manager.GetBuckets(_businessUnitId, VehicleType.Used);

            _maxAdSummary.Filters.LowActivity = CreateInventoryPageFilter(filterData, thresholds, WorkflowType.LowActivity);
            _maxAdSummary.Filters.LowAdQuality = CreateInventoryPageFilter(filterData, thresholds, WorkflowType.LowAdQualityAll);
            _maxAdSummary.Filters.NoEquipment = CreateInventoryPageFilter(filterData, thresholds, WorkflowType.EquipmentNeeded);
            _maxAdSummary.Filters.NoPhotos = CreateInventoryPageFilter(filterData, thresholds, WorkflowType.NoPhotos);
            _maxAdSummary.Filters.NoPrice = CreateInventoryPageFilter(filterData, thresholds, WorkflowType.NoPrice); 
            _maxAdSummary.Filters.DueForRepricing = CreateInventoryPageFilter(filterData, thresholds, WorkflowType.DueForRepricing);
            _maxAdSummary.Filters.NoPriceComparison = CreateInventoryPageFilter(filterData, thresholds, WorkflowType.NoFavourablePriceComparisons);
        }

        private MaxAdFilterVm CreateInventoryPageFilter(IEnumerable<Bucket> filtersData, IEnumerable<BucketAlertThreshold> thresholds, WorkflowType filterId)
        {
            if(filtersData == null || thresholds == null)
                return null;

            var threshold = thresholds.Where(t => t.Bucket == filterId).FirstOrDefault();
            if (threshold != null && !threshold.Active)
                return null;

            var filterData = filtersData.Single(bucket => bucket.BucketId == filterId);
            
            if(filterData == null)
                return null;
            var filterModel = new MaxAdFilterVm()
                                  {
                                      Count = filterData.BucketCount,
                                      DisplayName = filterData.BucketDescription,
                                      Url = GetUrl( filterId )
                                  };
            return filterModel;
        }

        private string GetUrl (WorkflowType filterId)
        {
            // most links in mini-dashboard link to dashboard
            string url = DashboardUrl;

            // BUGZID 23158: DueForPricing link should always link to inventory bucket
            if (filterId == WorkflowType.DueForRepricing || !_maxAdSummary.Dashboard.IsEnabled)
            {
                url = GetInventoryPageUrl(filterId);    
            }
            
            return url;
        }

        // return URL to specific bucket on inventory page e.g. Outdated Price in Ad
        private static string GetInventoryPageUrl(WorkflowType filterId)
        {
            return string.Format("{0}&filter={1}", InventoryPageUrl, filterId);
        }
    }
}
