﻿using System.Data.Entity.Infrastructure;
using System.Data.Objects;
using System.Data.Services;
using System.Data.Services.Common;
using System.ServiceModel;
using DataWebServices.Properties;
using FirstLook.Common.Core.ServiceModel;

namespace DataWebServices.Data.BusinessUnit
{
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    [ErrorHandlerBehavior]
    public class Service : DataService<ObjectContext>
    {
        public static void InitializeService(DataServiceConfiguration config)
        {
            config.UseVerboseErrors = Settings.Default.DataServices_UseVerboseErrors;
            
            config.SetEntitySetAccessRule("BusinessUnitByMember", EntitySetRights.AllRead);
            
            config.DataServiceBehavior.MaxProtocolVersion = DataServiceProtocolVersion.V2;
        }

        protected override ObjectContext CreateDataSource()
        {
            var context = new Context();

            var objectContext = ((IObjectContextAdapter)context).ObjectContext;
            objectContext.ContextOptions.ProxyCreationEnabled = false;

            return objectContext;
        }
    }
}