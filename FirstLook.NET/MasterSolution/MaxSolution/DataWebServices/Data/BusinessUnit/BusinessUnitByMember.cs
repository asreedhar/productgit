namespace DataWebServices.Data.BusinessUnit
{
    public class BusinessUnitByMember
    {
        public int MemberID { get; set; }
        public string Login { get; set; }
        
        public int BusinessUnitID { get; set; }
        public string BusinessUnit { get; set; }

        public int DealerGroupBusinessUnitID { get; set; }
        public string DealerGroupBusinessUnit { get; set; }
    }
}