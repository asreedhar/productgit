namespace DataWebServices.Data.BusinessUnit
{
    public class Member
    {
        public int MemberID { get; set; }
        public string Login { get; set; }
        public int? MemberType { get; set; }

        public bool IsAdministrator()
        {
            return MemberType.HasValue && MemberType.Value == 1;
        }
    }
}