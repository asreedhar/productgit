using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using DataWebServices.Properties;

namespace DataWebServices.Data.BusinessUnit
{
    public class Context : DbContext
    {
        public Context()
            : base("name=Merchandising")
        {
            ((IObjectContextAdapter) this).ObjectContext.CommandTimeout = Settings.Default.DatabaseTimeout_Seconds;
        }

        public DbSet<BusinessUnitByMember> BusinessUnitByMember { get; set; }
        public DbSet<Member> Member { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BusinessUnitByMember>().ToTable("BusinessUnitByMember", "Reports");
            modelBuilder.Entity<BusinessUnitByMember>().HasKey(
                a => new {a.MemberID, a.BusinessUnitID});

            modelBuilder.Entity<Member>().ToTable("Member", "Reports");
            modelBuilder.Entity<Member>().HasKey(a => new {a.MemberID});

            base.OnModelCreating(modelBuilder);
        }
    }
}