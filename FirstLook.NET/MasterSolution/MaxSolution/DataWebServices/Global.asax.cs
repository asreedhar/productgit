﻿using System;
using System.Data.Services;
using System.Reflection;
using System.ServiceModel.Activation;
using System.Web.Mvc;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Core.Logging;
using FirstLook.Common.PhotoServices;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel;

namespace DataWebServices
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static readonly ILog Log = LoggerFactory.GetLogger<MvcApplication>();

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.Add(new ServiceRoute("Data/BusinessUnit.svc", new DataServiceHostFactory(), typeof(Data.BusinessUnit.Service)));
            routes.Add(new ServiceRoute("Reports/VehicleActivity.svc", new DataServiceHostFactory(), typeof(Reports.VehicleActivity.Service)));
            routes.Add(new ServiceRoute("Reports/VehiclesOnline.svc", new DataServiceHostFactory(), typeof(Reports.VehiclesOnline.Service)));

            routes.MapRoute("ActionNeeded", "api.aspx/ActionNeeded/{businessUnitId}/{userName}", new { controller = "Inventory", action = "GetActionNeeded" });
        }

        protected void Application_Start()
        {
            // Setup logging
            log4net.Config.XmlConfigurator.Configure();
            Log.Info("Application Starting...");

            AppDomain.CurrentDomain.UnhandledException += LogAppDomainUnhandledException;

            AreaRegistration.RegisterAllAreas();

            RegisterRoutes(RouteTable.Routes);

            SetupIOC();
        }

        protected void Application_End()
        {
            Log.Info("Application Stopping...");
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var ex = Server.GetLastError();
            Log.Error("Unhandled exception", ex);
        }

        private static void LogAppDomainUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            if (e.IsTerminating)
                Log.Fatal("Unhandled exception. Application is terminating.", e.ExceptionObject as Exception);
            else
                Log.Error("Unhandled exception.", e.ExceptionObject as Exception);
        }

        private void SetupIOC()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new CoreModule());
            builder.RegisterModule(new OltpModule());
            builder.RegisterModule(new PhotoServicesModule());
            new MerchandisingDomainRegistry().Register(builder);

            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            builder.RegisterModelBinders(Assembly.GetExecutingAssembly());
            builder.RegisterModelBinderProvider();

            var container = builder.Build();

            Registry.RegisterContainer(container);

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}