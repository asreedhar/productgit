using System.Net.Http;
using System.Net.Http.Headers;
using Max.Selling.WebService.WebLoader.ViewModels;
using Newtonsoft.Json;

namespace SellingWebService.IntegrationTests.WebLoader
{
    public class WebLoaderTrimClient
    {
        private static string GetUrl(string trim)
        {
            return string.Format("{0}{1}/trims/{2}", Settings.MaxWebsiteBaseUrl, Settings.WebLoaderBaseUrl, trim);
        }

        private static void SetValidCredential(HttpRequestMessage request, string username, string password)
        {
            request.Headers.Authorization = new AuthenticationHeaderValue("Basic", CommonAuthTests.EncodeCredential(username, password));
        }

        public Trim GetTrim(string id, string username, string password)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, GetUrl(id));
            SetValidCredential(request, username, password);

            var client = new HttpClient();
            var response = client.SendAsync(request).Result;

            return (Trim)JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result, typeof(Trim));
        }
    }
}