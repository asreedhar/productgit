﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace SellingWebService.IntegrationTests.WebLoader
{
    public static class DataFinder
    {
        public static readonly string Merchandising = "Merchandising";
        public static readonly string Chrome = "Chrome";
        public static readonly string IMT = "IMT";

        public static DataTable ExecQuery(string database, string query)
        {
            var provider = GetDbFactory(database);

            using(var cn = GetOpenConnection(database))
            using(var cmd = cn.CreateCommand())
            {
                cmd.CommandText = query;

                var adapter = provider.CreateDataAdapter();
                if (adapter == null)
                    throw new InvalidOperationException("Failed to create adapter.");

                adapter.SelectCommand = (DbCommand)cmd;

                var table = new DataTable();
                adapter.Fill(table);
                
                return table;
            }
        }

        public static DbProviderFactory GetDbFactory(string database)
        {
            var connectionStringConfig = ConfigurationManager.ConnectionStrings[database];
            return DbProviderFactories.GetFactory(connectionStringConfig.ProviderName);
        }

        public static IDbConnection GetOpenConnection(string database)
        {
            var connectionStringConfig = ConfigurationManager.ConnectionStrings[database];
            var dbFactory = DbProviderFactories.GetFactory(connectionStringConfig.ProviderName);

            var cn = dbFactory.CreateConnection();
            if (cn == null)
                throw new InvalidOperationException("Failed to create connection.");

            var success = false;
            try
            {
                cn.ConnectionString = connectionStringConfig.ConnectionString;
                cn.Open();
                success = true;
                return cn;
            }
            finally
            {
                if (!success)
                    cn.Dispose();
            }
        }

        public static VehicleKey FindVehicleWithPhotos(string dealerCode, Func<int, bool> condition)
        {
            var photoCountsTask = MiniPhotoClient.GetPhotoCountsForDealership(dealerCode);
            return ExecQuery(Merchandising,
                String.Format(@"
                select BusinessUnitCode as DealerCode, InventoryID, VIN, '{0}' as UserName, '{1}' as Password
                from IMT.dbo.BusinessUnit BU
                    inner join FLDW.dbo.InventoryActive I on I.BusinessUnitID = BU.BusinessUnitID
                    inner join FLDW.dbo.Vehicle V on I.BusinessUnitID = V.BusinessUnitID and I.VehicleID = V.VehicleID
                    inner join IMT.dbo.DealerPreference DP on BU.BusinessUnitID = DP.BusinessUnitID
                where I.InventoryActive = 1
                    and BU.Active = 1
                    and BU.BusinessUnitTypeID = 4
                    and DP.GoLiveDate is not null
                    and BU.BusinessUnitCode = '{2}'
                ", Settings.AdminUser, Settings.StandardPassword, dealerCode))
                .Rows
                .Cast<DataRow>()
                .Where(
                    r => {
                            int cnt;
                            photoCountsTask.Result.TryGetValue((string) r["VIN"], out cnt);
                            return condition(cnt);
                        })
                .Select(VehicleKey.FromRow)
                .First();
        }

        public static VehicleKey FindVehicle(string filter = "", bool needsMerchandisingUpgrade = true, bool needsWebLoaderUpgrade = true, string having = "")
        {
            var b = new StringBuilder();
            b.AppendFormat(
                @"
                select top 1 BU.BusinessUnitCode as DealerCode, I.InventoryID, '{0}' as UserName, '{1}' as Password
                from IMT.dbo.BusinessUnit BU
                    inner join FLDW.dbo.InventoryActive I on I.BusinessUnitID = BU.BusinessUnitID
                    inner join IMT.dbo.DealerPreference DP on BU.BusinessUnitID = DP.BusinessUnitID
                    left join Merchandising.builder.OptionsConfiguration OC on I.BusinessUnitID = OC.BusinessUnitID and I.InventoryID = OC.InventoryID
                    left join FLDW.dbo.Vehicle V on I.BusinessUnitID = V.BusinessUnitID and I.VehicleID = V.VehicleID
                    cross apply Merchandising.chrome.GetVinPatternForVin(V.Vin) VP
                    inner join VehicleCatalog.Chrome.VINPatternStyleMapping VPSM on VP.VinPatternID = VPSM.VINPatternID and VPSM.CountryCode = 1
                where I.InventoryActive = 1
                    and BU.Active = 1
                    and BU.BusinessUnitTypeID = 4
                    and DP.GoLiveDate is not null
                ", Settings.AdminUser, Settings.StandardPassword);

            if (needsMerchandisingUpgrade)
                b.AppendLine(@"and exists (select 1 
                                from IMT.dbo.DealerUpgrade DU
                                where DealerUpgradeCD = 24 
                                    and EffectiveDateActive = 1
                                    and DU.BusinessUnitID = BU.BusinessUnitID)");
            if (needsWebLoaderUpgrade)
                b.AppendLine(@"and exists (select 1
                                from Merchandising.settings.Merchandising S
                                where WebLoaderEnabled = 1
                                    and S.BusinessUnitID = BU.BusinessUnitID)");
            if(!string.IsNullOrEmpty(filter))
            {
                b.Append(" and ");
                b.AppendLine(filter);
            }

            b.AppendLine("group by BU.BusinessUnitCode, I.InventoryID");
            if(!string.IsNullOrEmpty(having))
            {
                b.AppendFormat("having {0}\r\n", having);
            }
            
            var table = ExecQuery(Merchandising, b.ToString());
            return VehicleKey.FromTable(table);
        }

        public static DataRow GetDataForVehicle(VehicleKey key)
        {
            return ExecQuery(Merchandising,
                String.Format(@"
                    select I.*, V.VIN, V.VehicleYear, OC.*, NEM.*,
                        MMG.Make as MMG_Make, MMG.Model as MMG_Model,
                        DIV.DivisionName as DIV_DivisionName, STY.CFModelName as STY_CFModelName, STY.Trim as STY_Trim, STY.StyleNameWOTrim as STY_StyleNameWOTrim
                    from FLDW.dbo.InventoryActive I
                        inner join FLDW.dbo.Vehicle V on I.BusinessUnitID = V.BusinessUnitID and I.VehicleID = V.VehicleID
                        left join Merchandising.builder.OptionsConfiguration OC on I.BusinessUnitID = OC.BusinessUnitID and I.InventoryID = OC.InventoryID
                        inner join IMT.dbo.BusinessUnit BU on I.BusinessUnitID = BU.BusinessUnitID
                        left join Merchandising.Builder.NewEquipmentMapping NEM on I.BusinessUnitID = NEM.BusinessUnitID and I.InventoryID = NEM.InventoryID
                        left join IMT.dbo.MakeModelGrouping MMG on MMG.MakeModelGroupingId = V.MakeModelGroupingId
                        left join VehicleCatalog.Chrome.Styles STY on OC.ChromeStyleID = STY.StyleID and STY.CountryCode = 1
                        left join VehicleCatalog.Chrome.Models MDL on STY.ModelID = MDL.ModelID and MDL.CountryCode = 1
                        left join VehicleCatalog.Chrome.Divisions DIV on MDL.DivisionID = DIV.DivisionID and DIV.CountryCode = 1
                    where
                        BU.BusinessUnitCode = '{0}'
                        and I.InventoryID = {1}
                    ", key.DealerCode, key.Vehicle)
                ).Rows.Cast<DataRow>().Single();
        }

        public static IList<int> GetValidChromeStylesForVehicle(VehicleKey key)
        {
            return ExecQuery(Merchandising,
                string.Format(@"
                    select distinct VPSM.ChromeStyleID
                    from IMT.dbo.BusinessUnit BU
                        inner join FLDW.dbo.InventoryActive I on BU.BusinessUnitID = I.BusinessUnitID
                        inner join FLDW.dbo.Vehicle V on I.BusinessUnitID = V.BusinessUnitID and I.VehicleID = V.VehicleID
                        cross apply Merchandising.chrome.GetVinPatternForVin(V.Vin) VP
                        inner join VehicleCatalog.Chrome.VINPatternStyleMapping VPSM on VP.VinPatternID = VPSM.VINPatternID and VPSM.CountryCode = 1
                    where BU.BusinessUnitCode = '{0}'
                        and I.InventoryID = {1}
                    ", key.DealerCode, key.Vehicle))
                .Rows
                .Cast<DataRow>()
                .Select(r => (int)r["ChromeStyleID"])
                .ToList();
        }

        public class PrimaryEquipmentStandards
        {
            public int StyleId { get; set; }
            public int? StandardEngine { get; set; }
            public string StandardEngineDesc { get; set; }
            public int? StandardTransmission { get; set; }
            public string StandardTransmissionDesc { get; set; }
            public int? StandardDrivetrain { get; set; }
            public string StandardDrivetrainDesc { get; set; }
        }

        public static PrimaryEquipmentStandards GetPrimaryEquipmentStandardsForVehicle(VehicleKey key)
        {
            return ExecQuery(Merchandising,
                string.Format(@"
                    select S.StyleID, 
                        Engine as EngineId, C1.UserFriendlyName as EngineDesc, 
                        Transmission as TransmissionId, C2.UserFriendlyName as TransmissionDesc,
                        DriveTrain as DrivetrainId, C3.UserFriendlyName as DrivetrainDesc
                    from 
                    IMT.dbo.BusinessUnit BU
                    inner join Merchandising.builder.OptionsConfiguration OC on BU.BusinessUnitId = OC.BusinessUnitID
                    inner join VehicleCatalog.Chrome.Styles S on OC.ChromeStyleID = S.StyleID
                    left join 
                    (
                        select C.CategoryTypeFilter, C.CategoryID, ChromeStyleID
                        from VehicleCatalog.Chrome.StyleGenericEquipment SGE
                            inner join VehicleCatalog.Chrome.Categories C
                                on SGE.CategoryID = C.CategoryId
                        where SGE.StyleAvailability = 'Standard'
                            and SGE.CountryCode = 1
                            and C.CountryCode = 1
                    ) as Src
                    pivot
                    (
                        max(CategoryID)
                        for CategoryTypeFilter in (Engine, Transmission, Drivetrain)
                    ) as P
                    on S.StyleID = P.ChromeStyleID
                    left join VehicleCatalog.Chrome.Categories C1 on P.Engine = C1.CategoryID and C1.CountryCode = 1
                    left join VehicleCatalog.Chrome.Categories C2 on P.Transmission = C2.CategoryID and C2.CountryCode = 1
                    left join VehicleCatalog.Chrome.Categories C3 on P.Drivetrain = C3.CategoryID and C3.CountryCode = 1
                    where S.CountryCode = 1
                        and BU.BusinessUnitCode = '{0}'
                        and OC.InventoryID = {1}
                    ", key.DealerCode, key.Vehicle))
                .Rows
                .Cast<DataRow>()
                .Select(r => new PrimaryEquipmentStandards
                    {
                        StyleId = (int)r["StyleID"],
                        StandardEngine = (r["EngineId"] is DBNull) ? (int?)null : (int)r["EngineId"],
                        StandardEngineDesc = (r["EngineDesc"] is DBNull) ? null : (string)r["EngineDesc"],
                        StandardTransmission = (r["TransmissionId"] is DBNull) ? (int?)null : (int)r["TransmissionId"],
                        StandardTransmissionDesc = (r["TransmissionDesc"] is DBNull) ? null : (string)r["TransmissionDesc"],
                        StandardDrivetrain = (r["DrivetrainId"] is DBNull) ? (int?)null : (int)r["DrivetrainId"],
                        StandardDrivetrainDesc = (r["DrivetrainDesc"] is DBNull) ? null : (string)r["DrivetrainDesc"],
                    })
                .Single();
        }

        public static DataRow GetExteriorChromeColor(VehicleKey key)
        {
            var colors = ExecQuery(Merchandising,
                string.Format(@"
                    select OC.ExtColor1, OC.ExtColor2, OC.ExteriorColorCode, OC.ExteriorColorCode2, C.*                        
                    from IMT.dbo.BusinessUnit BU
                        inner join FLDW.dbo.InventoryActive I 
                            on BU.BusinessUnitID = I.BusinessUnitID
                        inner join Merchandising.builder.OptionsConfiguration OC 
                            on I.BusinessUnitID = OC.BusinessUnitID and I.InventoryID = OC.InventoryID
                        inner join VehicleCatalog.Chrome.Colors C
                            on C.StyleID = OC.ChromeStyleID
                    where BU.BusinessUnitCode = '{0}'
                        and I.InventoryID = {1}
                        and C.CountryCode = 1
                ", key.DealerCode, key.Vehicle)).Rows.Cast<DataRow>().ToArray();

            var cmp = StringComparer.InvariantCultureIgnoreCase;

            var matchByCode = colors.FirstOrDefault(
                c => cmp.Equals(GetColorCode(c["Ext1ManCode"], c["Ext1Code"]), GetColorCode(c["ExteriorColorCode"]))
                     && cmp.Equals(GetColorCode(c["Ext2ManCode"], c["Ext2Code"]), GetColorCode(c["ExteriorColorCode2"]))
                     && (!string.IsNullOrEmpty(GetColorCode(c["ExteriorColorCode"])) 
                        || !string.IsNullOrEmpty(GetColorCode(c["ExteriorColorCode2"]))));

            var matchByDesc = colors.FirstOrDefault(
                c => cmp.Equals(GetColorDesc(c["Ext1Desc"]), GetColorDesc(c["ExtColor1"]))
                     && cmp.Equals(GetColorDesc(c["Ext2Desc"]), GetColorDesc(c["ExtColor2"]))
                     && (!string.IsNullOrEmpty(GetColorDesc(c["ExtColor1"])) 
                        || !string.IsNullOrEmpty(GetColorDesc(c["ExtColor2"]))));

            return matchByCode ?? matchByDesc;
        }

        public static DataRow GetInteriorChromeColor(VehicleKey key)
        {
            var colors = ExecQuery(Merchandising,
                string.Format(@"
                    select OC.IntColor, OC.InteriorColorCode, C.*
                    from IMT.dbo.BusinessUnit BU
                        inner join FLDW.dbo.InventoryActive I
                            on BU.BusinessUnitID = I.BusinessUnitID
                        inner join Merchandising.builder.OptionsConfiguration OC 
                            on I.BusinessUnitID = OC.BusinessUnitID and I.InventoryID = OC.InventoryID
                        inner join VehicleCatalog.Chrome.Colors C
                            on C.StyleID = OC.ChromeStyleID
                    where BU.BusinessUnitCode = '{0}'
                        and I.InventoryID = {1}
                        and C.CountryCode = 1
                ", key.DealerCode, key.Vehicle)).Rows.Cast<DataRow>().ToArray();

            var cmp = StringComparer.InvariantCultureIgnoreCase;

            var matchByCode = colors.FirstOrDefault(
                c => cmp.Equals(GetColorCode(c["IntManCode"], c["IntCode"]), GetColorCode(c["InteriorColorCode"]))
                     && !string.IsNullOrEmpty(GetColorCode(c["InteriorColorCode"])));

            var matchByDesc = colors.FirstOrDefault(
                c => cmp.Equals(GetColorDesc(c["IntDesc"]), GetColorDesc(c["IntColor"]))
                     && !string.IsNullOrEmpty(GetColorDesc(c["IntColor"])));

            return matchByCode ?? matchByDesc;
        }

        public static string GetColorCode(object manCode, object code)
        {
            var manCodeStr = manCode is DBNull ? null : (string)manCode;
            var codeStr = code is DBNull ? null : (string)code;
            return string.IsNullOrEmpty(manCodeStr) ? codeStr ?? "" : manCodeStr;
        }

        public static string GetColorCode(object code)
        {
            return code is DBNull ? "" : (string) code ?? "";
        }

        public static string GetColorDesc(object desc)
        {
            return desc is DBNull ? "" : (string) desc ?? "";
        }

        public static string GetColorSimple(object simple)
        {
            return simple is DBNull ? "" : (string) simple ?? "";
        }
    }
}