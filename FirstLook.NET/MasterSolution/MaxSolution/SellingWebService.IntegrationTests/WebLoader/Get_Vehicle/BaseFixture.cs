﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using NUnit.Framework;

namespace SellingWebService.IntegrationTests.WebLoader.Get_Vehicle
{
    public class BaseFixture
    {
        static BaseFixture()
        {
            // Allow the test client to work with a server that has an invalid SSL Cert. i.e. Int-B
            ServicePointManager.ServerCertificateValidationCallback += (o, c, chain, policy) => true;
        }

        [TestFixtureSetUp]
        public void BaseFixtureSetup()
        {
            _validVehicle = DataFinder.FindVehicle("OC.ChromeStyleID > 0");
        }

        protected VehicleKey _validVehicle;

        protected string GetUrl()
        {
            return GetUrl(_validVehicle);
        }

        protected static string GetUrl(VehicleKey key)
        {
            return GetUrl(key.DealerCode, key.Vehicle);
        }

        protected static string GetUrl(string dealer, string vehicle)
        {
            var url = string.Format("{0}{1}/dealers/{2}/vehicles/{3}",
                                 Settings.MaxWebsiteBaseUrl,
                                 Settings.WebLoaderBaseUrl,
                                 dealer, vehicle);
            Console.WriteLine("Testing: {0}", url);

            return url;
        }

        protected void SetValidCredential(HttpRequestMessage request)
        {
            request.Headers.Authorization = new AuthenticationHeaderValue("Basic",
                CommonAuthTests.EncodeCredential(_validVehicle.UserName, _validVehicle.Password));
        }

        protected void SetValidCredential (WebRequest request)
        {
            string authInfo = _validVehicle.UserName + ":" + _validVehicle.Password;
            authInfo = Convert.ToBase64String( Encoding.Default.GetBytes( authInfo ) );
            request.Headers["Authorization"] = "Basic " + authInfo;
        }
    }
}