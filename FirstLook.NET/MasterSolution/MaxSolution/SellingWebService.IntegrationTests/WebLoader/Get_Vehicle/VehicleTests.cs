﻿using System.Data;
using System.Globalization;
using System.Net;
using System.Net.Http;
using NUnit.Framework;
using Newtonsoft.Json.Linq;

namespace SellingWebService.IntegrationTests.WebLoader.Get_Vehicle
{
    [TestFixture]
    public class VehicleTests : BaseFixture
    {
        private HttpResponseMessage _response;
        private string _url;
        private DataRow _fromDb;
        private JObject _vehicle;

        [TestFixtureSetUp]
        public void SetupFixture()
        {
            _url = GetUrl();
            _fromDb = DataFinder.GetDataForVehicle(_validVehicle);
            _response = GetResponse();
            _vehicle = JObject.Parse(_response.Content.ReadAsStringAsync().Result);
        }

        private HttpResponseMessage GetResponse()
        {
            var client = new HttpClient();

            var request = new HttpRequestMessage(HttpMethod.Get, _url);
            SetValidCredential(request);

            return client.SendAsync(request).Result;
        }

        [Test]
        public void Get_with_good_vehicle_id_should_return_200()
        {
            Assert.That(_response.StatusCode, Is.EqualTo(HttpStatusCode.OK), string.Format("Url: {0}", _url));
        }

        [Test]
        public void Get_with_good_vehicle_id_should_return_content_type_application_json()
        {
            Assert.That(_response.Content.Headers.ContentType.MediaType, Is.EqualTo("application/json"), string.Format("Url: {0}", _url));
        }

        [Test]
        public void Response_Id_should_match_the_request()
        {
            Assert.That((int) _vehicle.SelectToken("Id"),
                        Is.EqualTo(int.Parse(_validVehicle.Vehicle)),
                        string.Format("Url: {0}", _url));
        }

        [Test] 
        public void Response_Vin_should_match_the_db()
        {
            Assert.That((string) _vehicle.SelectToken("Vin"),
                        Is.EqualTo(_fromDb["VIN"]),
                        string.Format("Test Vin. Url: {0}", _url));
        }

        [Test]
        public void Response_StockNum_should_match_the_db()
        {
            Assert.That((string) _vehicle.SelectToken("StockNum"),
                        Is.EqualTo(_fromDb["StockNumber"]),
                        string.Format("Test StockNum. Url: {0}", _url));
        }

        [Test]
        public void Response_Make_should_match_the_db()
        {
            Assert.That((string)_vehicle.SelectToken("Make"), Is.EqualTo(_fromDb["DIV_DivisionName"]));
        }

        [Test]
        public void Response_Model_should_match_the_db()
        {
            Assert.That((string)_vehicle.SelectToken("Model"), Is.EqualTo(_fromDb["STY_CFModelName"]));
        }

        [Test]
        public void Response_Year_should_match_the_db()
        {
            Assert.That(_vehicle.SelectToken("Year").Type, Is.EqualTo(JTokenType.Integer));
            Assert.That((int)_vehicle.SelectToken("Year"), Is.EqualTo(_fromDb["VehicleYear"]));
        }

        [Test]
        public void Response_Trim_should_have_the_equivalent_Id_to_the_db()
        {
            Assert.That((string)_vehicle.SelectToken("Trim.Id"), 
                Is.EqualTo("1_" + ((int)_fromDb["ChromeStyleID"]).ToString(CultureInfo.InvariantCulture)));
        }

        [Test]
        public void Response_Trim_should_have_the_correct_description_from_the_db()
        {
            // It is a valid case that a chromestyleid might be set, but there is no Trim text.
            // In that case the Trim is "base"
            var styTrim = string.IsNullOrWhiteSpace(_fromDb["STY_Trim"].ToString()) ? "base" : _fromDb["STY_Trim"].ToString();
            Assert.That((string)_vehicle.SelectToken("Trim.Desc"),
                Is.EqualTo(styTrim + " - " + _fromDb["STY_StyleNameWOTrim"]));
        }
    }
}