﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using NUnit.Framework;
using Newtonsoft.Json.Linq;

namespace SellingWebService.IntegrationTests.WebLoader.Get_Vehicle
{
    [TestFixture]
    public class FindVehicleTests : BaseFixture
    {
        private JObject GetResponse(VehicleKey key)
        {
            var url = GetUrl(key);
            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            SetValidCredential(request);

            var response = client.SendAsync(request).Result;
            if (!response.IsSuccessStatusCode)
                throw new Exception("HTTP Request Failed: " + response.ToString());
                        
            var vehicle = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            return vehicle;
        }

        [Test]
        public void Vehicle_with_VinPattern_with_single_style_should_return_single_style()
        {
            RunVinPatternTests(string.Format("count(distinct VPSM.ChromeStyleID) = 1 and BusinessUnitCode = '{0}'", Settings.DefaultDealerCode), 
                list => Assert.That(list.Count, Is.EqualTo(1)));
        }

        [Test]
        public void Vehicle_with_VinPattern_with_multiple_styles_should_return_multiple_styles()
        {
            RunVinPatternTests(string.Format("count(distinct VPSM.ChromeStyleID) >= 3 and BusinessUnitCode = '{0}'", Settings.DefaultDealerCode),
                list => Assert.That(list.Count, Is.GreaterThanOrEqualTo(3)));
        }

        private void RunVinPatternTests(string having, Action<IList<string>> preAssert)
        {
            var vehicleWithSingleStyle = DataFinder.FindVehicle(having: having);
            var expectedTrimIds =
                DataFinder.GetValidChromeStylesForVehicle(vehicleWithSingleStyle)
                    .Select(id => "1_" + id.ToString(CultureInfo.InvariantCulture))
                    .ToList();
            preAssert(expectedTrimIds);
            var url = GetUrl(vehicleWithSingleStyle);
            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            SetValidCredential(request);

            var response = client.SendAsync(request).Result;
            if (!response.IsSuccessStatusCode)
                throw new Exception("HTTP Request Failed: " + response.ToString());
            
            var vehicle = JObject.Parse(response.Content.ReadAsStringAsync().Result);

            var trimIds = vehicle.SelectToken("AvailableTrimIds").Select(styleId => (string) styleId).ToList();
            Assert.That(trimIds, Is.EquivalentTo(expectedTrimIds));
        }

        [TestCase(true)]
        [TestCase(false)]
        public void EquipmentCompleted_should_contain_correct_value_from_database(bool expectedEquipmentCompleted)
        {
            //If it is a honda we default it to true. We need to exlude hondas from this query.
            var key = DataFinder.FindVehicle(
                (expectedEquipmentCompleted ? "" : "not ") + 
                @"exists (select 1 
                          from Merchandising.builder.VehicleStatus VS 
                          where VS.BusinessUnitID = I.BusinessUnitID 
                            and VS.InventoryID = I.InventoryID
                            and VS.StatusLevel >= 2
                            and VS.StatusTypeId in (1, 2))
                            AND not exists (select 1 from IMT.dbo.MakeModelGrouping mmg
                            where v.MakeModelGroupingID = mmg.MakeModelGroupingID and mmg.Make = 'HONDA')");
            
            var vehicle = GetResponse(key);

            var actualEquipmentCompleted = (bool) vehicle.SelectToken("EquipmentCompleted");
            Assert.That(actualEquipmentCompleted, Is.EqualTo(expectedEquipmentCompleted));
        }

        [TestCase(true)]
        [TestCase(false)]
        public void PhotosCompleted_should_contain_correct_value_from_database(bool expectedPhotosCompleted)
        {
            var key = DataFinder.FindVehicle(
                (expectedPhotosCompleted ? "" : "not ") +
                @"exists (select 1 
                          from Merchandising.builder.VehicleStatus VS 
                          where VS.BusinessUnitID = I.BusinessUnitID 
                            and VS.InventoryID = I.InventoryID
                            and VS.StatusLevel >= 2
                            and VS.StatusTypeId = 3)");

            var vehicle = GetResponse(key);

            var actualPhotosCompleted = (bool)vehicle.SelectToken("PhotosCompleted");
            Assert.That(actualPhotosCompleted, Is.EqualTo(expectedPhotosCompleted));
        }

        [Test]
        public void Negative_vehicle_id_should_load()
        {
            var vehicleWithNegativeId = DataFinder.FindVehicle(string.Format("I.InventoryId < 0"));

            var url = GetUrl(vehicleWithNegativeId);
            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            SetValidCredential(request);

            var response = client.SendAsync(request).Result;

            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            var vehicle = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            Assert.That((int)vehicle.SelectToken("Id"), Is.EqualTo(int.Parse(vehicleWithNegativeId.Vehicle)));
        }
    }
}