﻿using NUnit.Framework;

namespace SellingWebService.IntegrationTests.WebLoader.Get_Vehicle
{
    [TestFixture]
    public class AuthTests : BaseFixture
    {
        [Test]
        public void Get_with_no_Authorization_header_should_return_401()
        {
            CommonAuthTests.Get_with_no_Authorization_header_should_return_401(GetUrl());
        }

        [Test]
        public void Get_with_valid_user_but_bad_password_should_return_401()
        {
            CommonAuthTests.Get_with_valid_user_but_bad_password_should_return_401(GetUrl());
        }

        [Test]
        public void Get_with_bad_user_but_valid_password_should_return_401()
        {
            CommonAuthTests.Get_with_bad_user_but_valid_password_should_return_401(GetUrl());
        }

        [Test]
        public void Get_with_good_credentials_but_invalid_dealer_should_return_404()
        {
            CommonAuthTests.Get_with_good_credentials_but_invalid_dealer_should_return_404(
                GetUrl(Settings.InvalidDealerCode, _validVehicle.Vehicle));
        }

        [Test]
        public void Get_with_good_credentials_and_dealer_but_user_not_allowed_at_dealer_should_return_403()
        {
            CommonAuthTests.Get_with_good_credentials_and_dealer_but_user_not_allowed_at_dealer_should_return_403(GetUrl);
        }

        [Test]
        public void Get_with_good_credentials_and_dealer_but_dealer_doesnt_have_Merchandising_upgrade_should_return_403()
        {
            CommonAuthTests.Get_with_good_credentials_and_dealer_but_dealer_doesnt_have_Merchandising_upgrade_should_return_403(GetUrl);
        }

        [Test]
        public void Get_with_good_credential_and_dealer_but_dealer_doesnt_have_WebLoader_upgrade_should_return_403()
        {
            CommonAuthTests.Get_with_good_credential_and_dealer_but_dealer_doesnt_have_WebLoader_upgrade_should_return_403(GetUrl);
        }
    }
}