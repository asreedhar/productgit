﻿using System.Globalization;
using System.Net.Http;
using NUnit.Framework;
using Newtonsoft.Json.Linq;

namespace SellingWebService.IntegrationTests.WebLoader.Get_Vehicle
{
    [TestFixture]
    public class PrimaryEquipmentTests : BaseFixture
    {
        [Test]
        public void Vehicle_with_no_NewEquipmentMapping_record_should_return_appropriate_standards_for_Engine()
        {
            var vehicleWithoutNewEquipmentMapping = FindVehicleWithoutNewEquipmentMappingRecord("Engine");

            var standards = DataFinder.GetPrimaryEquipmentStandardsForVehicle(vehicleWithoutNewEquipmentMapping);

            var url = GetUrl(vehicleWithoutNewEquipmentMapping);
            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            SetValidCredential(request);

            var response = client.SendAsync(request).Result;
            var vehicle = JObject.Parse(response.Content.ReadAsStringAsync().Result);

            Assert.That((string)vehicle.SelectToken("Engine.Id"),
                Is.EqualTo(standards.StandardEngine.GetValueOrDefault(-1).ToString(CultureInfo.InvariantCulture)));
            Assert.That((string)vehicle.SelectToken("Engine.Desc"), Is.EqualTo(standards.StandardEngineDesc));
        }

        [Test]
        public void Vehicle_with_NewEquipmentMapping_record_and_Engine_neg_one_should_return_appropriate_standard_for_Engine()
        {
            var vehicleWithNewEquipmentMapping = FindVehicleWithNewEquipmentMappingRecord("EngineTypeCategoryID = -1");

            var standards = DataFinder.GetPrimaryEquipmentStandardsForVehicle(vehicleWithNewEquipmentMapping);

            var url = GetUrl(vehicleWithNewEquipmentMapping);
            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            SetValidCredential(request);

            var response = client.SendAsync(request).Result;
            var vehicle = JObject.Parse(response.Content.ReadAsStringAsync().Result);

            Assert.That((string)vehicle.SelectToken("Engine.Id"),
                Is.EqualTo(standards.StandardEngine.GetValueOrDefault(-1).ToString(CultureInfo.InvariantCulture)));
            Assert.That((string)vehicle.SelectToken("Engine.Desc"), Is.EqualTo(standards.StandardEngineDesc));
        }

        [Test]
        public void Vehicle_with_NewEquipmentMapping_record_and_Engine_equal_to_standard_should_return_appropriate_value_for_Engine()
        {
            var vehicleWithNewEquipmentMapping = FindVehicleWithNewEquipmentMappingRecord("CategoryTypeFilter = 'Engine' and EngineTypeCategoryID = C.CategoryID");

            var standards = DataFinder.GetPrimaryEquipmentStandardsForVehicle(vehicleWithNewEquipmentMapping);

            var url = GetUrl(vehicleWithNewEquipmentMapping);
            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            SetValidCredential(request);

            var response = client.SendAsync(request).Result;
            var vehicle = JObject.Parse(response.Content.ReadAsStringAsync().Result);

            Assert.That((string)vehicle.SelectToken("Engine.Id"),
                Is.EqualTo(standards.StandardEngine.GetValueOrDefault(-1).ToString(CultureInfo.InvariantCulture)));
            Assert.That((string)vehicle.SelectToken("Engine.Desc"), Is.EqualTo(standards.StandardEngineDesc));
        }

        [Test]
        public void Vehicle_with_NewEquipmentMapping_record_and_Engine_not_equal_to_standard_should_return_appropriate_value_for_Engine()
        {
            var vehicleWithNewEquipmentMapping = FindVehicleWithNewEquipmentMappingRecord(
                "CategoryTypeFilter = 'Engine' and EngineTypeCategoryID <> C.CategoryID and EngineTypeCategoryID <> -1");

            var fromDb = DataFinder.GetDataForVehicle(vehicleWithNewEquipmentMapping);

            var url = GetUrl(vehicleWithNewEquipmentMapping);
            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            SetValidCredential(request);

            var response = client.SendAsync(request).Result;
            var vehicle = JObject.Parse(response.Content.ReadAsStringAsync().Result);

            Assert.That((string)vehicle.SelectToken("Engine.Id"),
                Is.EqualTo(((int)fromDb["EngineTypeCategoryID"]).ToString(CultureInfo.InvariantCulture)));
        }

        [Test]
        public void Vehicle_with_no_NewEquipmentMapping_record_should_return_appropriate_standards_for_Transmission()
        {
            var vehicleWithoutNewEquipmentMapping = FindVehicleWithoutNewEquipmentMappingRecord("Transmission");

            var standards = DataFinder.GetPrimaryEquipmentStandardsForVehicle(vehicleWithoutNewEquipmentMapping);

            var url = GetUrl(vehicleWithoutNewEquipmentMapping);
            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            SetValidCredential(request);

            var response = client.SendAsync(request).Result;
            var vehicle = JObject.Parse(response.Content.ReadAsStringAsync().Result);

            Assert.That((string)vehicle.SelectToken("Transmission.Id"),
                Is.EqualTo(standards.StandardTransmission.GetValueOrDefault(-1).ToString(CultureInfo.InvariantCulture)));
            Assert.That((string)vehicle.SelectToken("Transmission.Desc"), Is.EqualTo(standards.StandardTransmissionDesc));
        }

        [Test]
        public void Vehicle_with_NewEquipmentMapping_record_and_Transmission_neg_one_should_return_appropriate_standard_for_Transmission()
        {
            var vehicleWithNewEquipmentMapping = FindVehicleWithNewEquipmentMappingRecord("TransmissionTypeCategoryID = -1");

            var standards = DataFinder.GetPrimaryEquipmentStandardsForVehicle(vehicleWithNewEquipmentMapping);

            var url = GetUrl(vehicleWithNewEquipmentMapping);
            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            SetValidCredential(request);

            var response = client.SendAsync(request).Result;
            var vehicle = JObject.Parse(response.Content.ReadAsStringAsync().Result);

            Assert.That((string)vehicle.SelectToken("Transmission.Id"),
                Is.EqualTo(standards.StandardTransmission.GetValueOrDefault(-1).ToString(CultureInfo.InvariantCulture)));
            Assert.That((string)vehicle.SelectToken("Transmission.Desc"), Is.EqualTo(standards.StandardTransmissionDesc));
        }

        [Test]
        public void Vehicle_with_NewEquipmentMapping_record_and_Transmission_equal_to_standard_should_return_appropriate_value_for_Transmission()
        {
            var vehicleWithNewEquipmentMapping = FindVehicleWithNewEquipmentMappingRecord("CategoryTypeFilter = 'Transmission' and TransmissionTypeCategoryID = C.CategoryID");

            var standards = DataFinder.GetPrimaryEquipmentStandardsForVehicle(vehicleWithNewEquipmentMapping);

            var url = GetUrl(vehicleWithNewEquipmentMapping);
            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            SetValidCredential(request);

            var response = client.SendAsync(request).Result;
            var vehicle = JObject.Parse(response.Content.ReadAsStringAsync().Result);

            Assert.That((string)vehicle.SelectToken("Transmission.Id"),
                Is.EqualTo(standards.StandardTransmission.GetValueOrDefault(-1).ToString(CultureInfo.InvariantCulture)));
            Assert.That((string)vehicle.SelectToken("Transmission.Desc"), Is.EqualTo(standards.StandardTransmissionDesc));
        }

        [Test]
        public void Vehicle_with_NewEquipmentMapping_record_and_Transmission_not_equal_to_standard_should_return_appropriate_value_for_Transmission()
        {
            var vehicleWithNewEquipmentMapping = FindVehicleWithNewEquipmentMappingRecord(
                "CategoryTypeFilter = 'Transmission' and TransmissionTypeCategoryID <> C.CategoryID and TransmissionTypeCategoryID <> -1");

            var fromDb = DataFinder.GetDataForVehicle(vehicleWithNewEquipmentMapping);

            var url = GetUrl(vehicleWithNewEquipmentMapping);
            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            SetValidCredential(request);

            var response = client.SendAsync(request).Result;
            var vehicle = JObject.Parse(response.Content.ReadAsStringAsync().Result);

            Assert.That((string)vehicle.SelectToken("Transmission.Id"),
                Is.EqualTo(((int)fromDb["TransmissionTypeCategoryID"]).ToString(CultureInfo.InvariantCulture)));
        }

        [Test]
        public void Vehicle_with_no_NewEquipmentMapping_record_should_return_appropriate_standards_for_Drivetrain()
        {
            var vehicleWithoutNewEquipmentMapping = FindVehicleWithoutNewEquipmentMappingRecord("Drivetrain");

            var standards = DataFinder.GetPrimaryEquipmentStandardsForVehicle(vehicleWithoutNewEquipmentMapping);

            var url = GetUrl(vehicleWithoutNewEquipmentMapping);
            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            SetValidCredential(request);

            var response = client.SendAsync(request).Result;
            var vehicle = JObject.Parse(response.Content.ReadAsStringAsync().Result);

            Assert.That((string)vehicle.SelectToken("Drivetrain.Id"),
                Is.EqualTo(standards.StandardDrivetrain.GetValueOrDefault(-1).ToString(CultureInfo.InvariantCulture)));
            Assert.That((string)vehicle.SelectToken("Drivetrain.Desc"), Is.EqualTo(standards.StandardDrivetrainDesc));
        }

        [Test]
        public void Vehicle_with_NewEquipmentMapping_record_and_Drivetrain_neg_one_should_return_appropriate_standard_for_Drivetrain()
        {
            var vehicleWithNewEquipmentMapping = FindVehicleWithNewEquipmentMappingRecord("DrivetrainCategoryID = -1");

            var standards = DataFinder.GetPrimaryEquipmentStandardsForVehicle(vehicleWithNewEquipmentMapping);

            var url = GetUrl(vehicleWithNewEquipmentMapping);
            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            SetValidCredential(request);

            var response = client.SendAsync(request).Result;
            var vehicle = JObject.Parse(response.Content.ReadAsStringAsync().Result);

            Assert.That((string)vehicle.SelectToken("Drivetrain.Id"),
                Is.EqualTo(standards.StandardDrivetrain.GetValueOrDefault(-1).ToString(CultureInfo.InvariantCulture)));
            Assert.That((string)vehicle.SelectToken("Drivetrain.Desc"), Is.EqualTo(standards.StandardDrivetrainDesc));
        }

        [Test]
        public void Vehicle_with_NewEquipmentMapping_record_and_Drivetrain_equal_to_standard_should_return_appropriate_value_for_Drivetrain()
        {
            var vehicleWithNewEquipmentMapping = FindVehicleWithNewEquipmentMappingRecord("CategoryTypeFilter = 'Drivetrain' and DrivetrainCategoryID = C.CategoryID");

            var standards = DataFinder.GetPrimaryEquipmentStandardsForVehicle(vehicleWithNewEquipmentMapping);

            var url = GetUrl(vehicleWithNewEquipmentMapping);
            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            SetValidCredential(request);

            var response = client.SendAsync(request).Result;
            var vehicle = JObject.Parse(response.Content.ReadAsStringAsync().Result);

            Assert.That((string)vehicle.SelectToken("Drivetrain.Id"),
                Is.EqualTo(standards.StandardDrivetrain.GetValueOrDefault(-1).ToString(CultureInfo.InvariantCulture)));
            Assert.That((string)vehicle.SelectToken("Drivetrain.Desc"), Is.EqualTo(standards.StandardDrivetrainDesc));
        }

        [Test]
        public void Vehicle_with_NewEquipmentMapping_record_and_Drivetrain_not_equal_to_standard_should_return_appropriate_value_for_Drivetrain()
        {
            var vehicleWithNewEquipmentMapping = FindVehicleWithNewEquipmentMappingRecord(
                "CategoryTypeFilter = 'Drivetrain' and DrivetrainCategoryID <> C.CategoryID and DrivetrainCategoryID <> -1");

            var fromDb = DataFinder.GetDataForVehicle(vehicleWithNewEquipmentMapping);

            var url = GetUrl(vehicleWithNewEquipmentMapping);
            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            SetValidCredential(request);

            var response = client.SendAsync(request).Result;
            var vehicle = JObject.Parse(response.Content.ReadAsStringAsync().Result);

            Assert.That((string)vehicle.SelectToken("Drivetrain.Id"),
                Is.EqualTo(((int)fromDb["DrivetrainCategoryID"]).ToString(CultureInfo.InvariantCulture)));
        }

        private static VehicleKey FindVehicleWithoutNewEquipmentMappingRecord(string ensureThereIsAStandardForEquipmentType)
        {
            return DataFinder.FindVehicle(
                string.Format(@"not exists (select 1 from Merchandising.builder.NewEquipmentMapping E 
                              where I.BusinessUnitID = E.BusinessUnitID 
                                  and I.InventoryID = E.InventoryID)
                  and exists (select 1 
                              from VehicleCatalog.Chrome.StyleGenericEquipment SGE
                                  inner join VehicleCatalog.Chrome.Categories C on SGE.CategoryID = C.CategoryID
                              where SGE.CountryCode = 1
                                  and C.CountryCode = 1
                                  AND C.CategoryTypeFilter = '{0}'
                                  and SGE.ChromeStyleID = OC.ChromeStyleId
                                  and StyleAvailability = 'Standard')",
                ensureThereIsAStandardForEquipmentType));
        }

        private static VehicleKey FindVehicleWithNewEquipmentMappingRecord(string filter)
        {
            return DataFinder.FindVehicle(
                string.Format(@"exists (select 1 from Merchandising.builder.NewEquipmentMapping E 
                              where I.BusinessUnitID = E.BusinessUnitID 
                                  and I.InventoryID = E.InventoryID)
                  and exists (select 1
                              from Merchandising.builder.OptionsConfiguration OC2
                                  inner join Merchandising.builder.NewEquipmentMapping E 
                                      on OC2.BusinessUnitID = E.BusinessUnitID and OC2.InventoryID = E.InventoryID
                                  left join VehicleCatalog.Chrome.StyleGenericEquipment SGE
                                      on SGE.ChromeStyleID = OC2.ChromeStyleID and SGE.CountryCode = 1 and SGE.StyleAvailability = 'Standard'
                                  left join VehicleCatalog.Chrome.Categories C 
                                      on SGE.CategoryID = C.CategoryID and C.CountryCode = 1
                              where
                                  OC2.BusinessUnitID = OC.BusinessUnitID
                                  and OC2.InventoryID = OC.InventoryID
                                  and {0})",
                              filter));
        }
    }
}