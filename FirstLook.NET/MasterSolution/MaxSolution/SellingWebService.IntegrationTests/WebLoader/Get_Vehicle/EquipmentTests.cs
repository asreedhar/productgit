﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using NUnit.Framework;

namespace SellingWebService.IntegrationTests.WebLoader.Get_Vehicle
{
    [TestFixture]
    public class EquipmentTests : BaseFixture
    {
        [Test]
        public void Equipment_should_not_return_items_for_Engine_Drivetrain_Transmission_or_Fuel_categories()
        {
            var forbiddenCategoryIds = 
                new HashSet<int>(
                    DataFinder.ExecQuery(DataFinder.Chrome,
                    @"
                    select CategoryID
                    from Chrome.Categories C
                        inner join Chrome.CategoryHeaders CH
                            on C.CategoryHeaderId = CH.CategoryHeaderId
                    where C.CountryCode = 1 and CH.CountryCode = 1
                        and CH.CategoryHeader in ('Engine', 'Drivetrain', 'Transmission', 'Fuel')
                    ")
                    .Rows
                    .Cast<DataRow>()
                    .Select(r => (int)r["CategoryID"])
                );

            var key = DataFinder.FindVehicle(
                @"
                isnull(OC.ChromeStyleID, -1) <> -1 
                and exists (select 1
                            from Merchandising.Builder.VehicleOptions VO
                                inner join VehicleCatalog.Chrome.Categories C 
                                    on VO.OptionID = C.CategoryID
                                inner join VehicleCatalog.Chrome.CategoryHeaders CH 
                                    on C.CategoryHeaderID = CH.CategoryHeaderID
                            where VO.BusinessUnitID = OC.BusinessUnitID
                                and VO.InventoryID = OC.InventoryID
                                and C.CountryCode = 1
                                and CH.CountryCode = 1
                                and CH.CategoryHeader in ('Engine', 'Drivetrain', 'Transmission', 'Fuel'))
                ");

            var client = new WebLoaderVehicleClient();
            var response = client.GetVehicle(key);

            Assert.That(
                response.Doc.SelectToken("Equipment").Select(r => int.Parse((string)r.SelectToken("Id"))).Where(forbiddenCategoryIds.Contains).ToArray(),
                Is.Empty);
        }

         
    }
}