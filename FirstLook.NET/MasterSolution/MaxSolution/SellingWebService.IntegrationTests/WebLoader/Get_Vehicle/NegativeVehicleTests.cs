﻿using System.Net;
using System.Net.Http;
using NUnit.Framework;

namespace SellingWebService.IntegrationTests.WebLoader.Get_Vehicle
{
    [TestFixture]
    public class NegativeVehicleTests : BaseFixture
    {
        [Test]
        public void Get_with_invalid_Vehicle_should_return_404()
        {
            var url = GetUrl(_validVehicle.DealerCode, Settings.InvalidVehicle);

            var client = new HttpClient();

            var request = new HttpRequestMessage(HttpMethod.Get, url);
            SetValidCredential(request);

            var response = client.SendAsync(request).Result;

            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NotFound), string.Format("Url: {0}", url));
        }

        [TestCase("INVALID_VEHICLE")]
        [TestCase("%20%20100%20%20")]
        [TestCase("    100    ")]
        public void Get_with_unparsable_Vehicle_identifier_should_return_400(string badVehicleId)
        {
            var url = GetUrl(_validVehicle.DealerCode, badVehicleId);

            var client = new HttpClient();

            var request = new HttpRequestMessage(HttpMethod.Get, url);
            SetValidCredential(request);

            var response = client.SendAsync(request).Result;

            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest), string.Format("Url: {0}", url));
        } 
    }
}