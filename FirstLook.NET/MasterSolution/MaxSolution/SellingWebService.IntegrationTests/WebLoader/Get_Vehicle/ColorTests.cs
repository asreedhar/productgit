﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Web;
using NUnit.Framework;
using Newtonsoft.Json.Linq;
using System;

namespace SellingWebService.IntegrationTests.WebLoader.Get_Vehicle
{
    [TestFixture]
    public class ColorTests : BaseFixture
    {
        [Test]
        public void ExtColor_should_return_no_value_when_color_Desc_and_Code_are_empty()
        {
            var key = DataFinder.FindVehicle(
                "OC.ExtColor1 = '' and OC.ExtColor2 = '' and isnull(OC.ExteriorColorCode, '') = '' and isnull(OC.ExteriorColorCode2, '') = ''");

            var vehicle = GetVehicle(key);

            JToken token;
            Assert.That(vehicle.TryGetValue("ExtColor", out token), Is.False);
        }

        [Test]
        public void ExtColor_should_return_no_value_when_OptionsConfiguration_record_is_missing()
        {
            var key = DataFinder.FindVehicle(@"not exists (select 1 
                                                           from Merchandising.builder.OptionsConfiguration OC2 
                                                           where I.BusinessUnitID = OC2.BusinessUnitID and I.InventoryID = OC2.InventoryID)");

            var vehicle = GetVehicle(key);

            JToken token;
            Assert.That(vehicle.TryGetValue("ExtColor", out token), Is.False);
        }

        [Test]
        public void ExtColor_should_return_color_when_ExtColor1_set_and_no_code_and_color_found_in_chrome()
        {
            var key = DataFinder.FindVehicle(
                @"OC.ChromeStyleID <> -1 and
                    OC.ExtColor1 <> '' and OC.ExtColor2 = '' and isnull(OC.ExteriorColorCode, '') = '' and isnull(OC.ExteriorColorCode2, '') = ''
                    and exists (select 1
                                from VehicleCatalog.Chrome.Colors C
                                where C.CountryCode = 1
                                    and C.StyleID = OC.ChromeStyleID
                                    and isnull(C.Ext1Desc, '') = OC.ExtColor1
                                    and isnull(C.Ext2Desc, '') = '')");

            var chromeColor = DataFinder.GetExteriorChromeColor(key);

            var vehicle = GetVehicle(key);

            Assert.That((string) vehicle.SelectToken("ExtColor.Id"), Is.EqualTo(GetExtColorId(chromeColor)));
            Assert.That((string) vehicle.SelectToken("ExtColor.Desc"), Is.EqualTo(GetExtColorDesc(chromeColor)));
            Assert.That((string) vehicle.SelectToken("ExtColor.Simple"), Is.EqualTo(GetExtColorSimple(chromeColor)));
        }

        [Test]
        public void ExtColor_should_return_color_when_ExtColor1_and_ExteriorColorCode_set_and_color_found_in_chrome()
        {
            var key = DataFinder.FindVehicle(
                @"OC.ChromeStyleID <> -1 and
                    OC.ExtColor1 <> '' and OC.ExtColor2 = '' and isnull(OC.ExteriorColorCode, '') <> '' and isnull(OC.ExteriorColorCode2, '') = ''
                    and (exists (select 1
                                from VehicleCatalog.Chrome.Colors C
                                where C.CountryCode = 1
                                    and C.StyleID = OC.ChromeStyleID
                                    and case when isnull(C.Ext1ManCode, '') = '' then c.Ext1Code else c.Ext1ManCode end = OC.ExteriorColorCode
                                    and case when isnull(C.Ext2ManCode, '') = '' then c.Ext2Code else c.Ext2ManCode end = '')
                        or exists (select 1
                                from VehicleCatalog.Chrome.Colors C
                                where C.CountryCode = 1
                                    and C.StyleID = OC.ChromeStyleID
                                    and C.Ext1Desc = OC.ExtColor1
                                    and C.Ext2Desc = ''))");

            var chromeColor = DataFinder.GetExteriorChromeColor(key);

            var vehicle = GetVehicle(key);

            Assert.That((string)vehicle.SelectToken("ExtColor.Id"), Is.EqualTo(GetExtColorId(chromeColor)));
            Assert.That((string)vehicle.SelectToken("ExtColor.Desc"), Is.EqualTo(GetExtColorDesc(chromeColor)));
            Assert.That((string)vehicle.SelectToken("ExtColor.Simple"), Is.EqualTo(GetExtColorSimple(chromeColor)));
        }

        [Test]
        public void ExtColor_should_return_color_when_ExtColor1_and_ExtColor2_set_but_no_codes_and_color_found_in_chrome()
        {
            try
            {
                var key = DataFinder.FindVehicle(
                    @"OC.ChromeStyleID <> -1 and
                    OC.ExtColor1 <> '' and OC.ExtColor2 <> '' and isnull(OC.ExteriorColorCode, '') = '' and isnull(OC.ExteriorColorCode2, '') = ''
                    and exists (select 1
                                from VehicleCatalog.Chrome.Colors C
                                where C.CountryCode = 1
                                    and C.StyleID = OC.ChromeStyleID
                                    and isnull(C.Ext1Desc, '') = OC.ExtColor1
                                    and isnull(C.Ext2Desc, '') = OC.ExtColor2)");

                var chromeColor = DataFinder.GetExteriorChromeColor(key);

                var vehicle = GetVehicle(key);

                Assert.That((string) vehicle.SelectToken("ExtColor.Id"), Is.EqualTo(GetExtColorId(chromeColor)));
                Assert.That((string) vehicle.SelectToken("ExtColor.Desc"), Is.EqualTo(GetExtColorDesc(chromeColor)));
                Assert.That((string) vehicle.SelectToken("ExtColor.Simple"), Is.EqualTo(GetExtColorSimple(chromeColor)));
            }
            catch (InconclusiveException ie)
            {
                // No vehicles found that match the query criteria
            }
        }

        [Test]
        public void ExtColor_should_return_color_when_ExtColor1_ExtColor2_ExteriorColorCode_and_ExteriorColorCode2_set_and_color_found_in_chrome()
        {
            var key = DataFinder.FindVehicle(
                @"OC.ChromeStyleID <> -1 and
                    OC.ExtColor1 <> '' and OC.ExtColor2 <> '' and isnull(OC.ExteriorColorCode, '') <> '' and isnull(OC.ExteriorColorCode2, '') <> ''
                    and (exists (select 1
                                from VehicleCatalog.Chrome.Colors C
                                where C.CountryCode = 1
                                    and C.StyleID = OC.ChromeStyleID
                                    and case when isnull(C.Ext1ManCode, '') = '' then c.Ext1Code else c.Ext1ManCode end = OC.ExteriorColorCode
                                    and case when isnull(C.Ext2ManCode, '') = '' then c.Ext2Code else c.Ext2ManCode end = '')
                        or exists (select 1
                                from VehicleCatalog.Chrome.Colors C
                                where C.CountryCode = 1
                                    and C.StyleID = OC.ChromeStyleID
                                    and C.Ext1Desc = OC.ExtColor1
                                    and C.Ext2Desc = ''))");

            var chromeColor = DataFinder.GetExteriorChromeColor(key);

            var vehicle = GetVehicle(key);

            Assert.That((string)vehicle.SelectToken("ExtColor.Id"), Is.EqualTo(GetExtColorId(chromeColor)));
            Assert.That((string)vehicle.SelectToken("ExtColor.Desc"), Is.EqualTo(GetExtColorDesc(chromeColor)));
            Assert.That((string)vehicle.SelectToken("ExtColor.Simple"), Is.EqualTo(GetExtColorSimple(chromeColor)));
        }

        [Test]
        public void ExtColor_should_return_color_when_ExtColor1_ExtColor2_are_set_and_color_IS_NOT_found_in_chrome()
        {
            var key = DataFinder.FindVehicle(
                @"OC.ChromeStyleID <> -1 and
                    OC.ExtColor1 <> ''
                    and (not exists (select 1
                                from VehicleCatalog.Chrome.Colors C
                                where C.CountryCode = 1
                                    and C.StyleID = OC.ChromeStyleID
                                    and case when isnull(C.Ext1ManCode, '') = '' then c.Ext1Code else c.Ext1ManCode end = OC.ExteriorColorCode
                                    and case when isnull(C.Ext2ManCode, '') = '' then c.Ext2Code else c.Ext2ManCode end = OC.ExteriorColorCode2)
                        and not exists (select 1
                                from VehicleCatalog.Chrome.Colors C
                                where C.CountryCode = 1
                                    and C.StyleID = OC.ChromeStyleID
                                    and C.Ext1Desc = OC.ExtColor1
                                    and C.Ext2Desc = OC.ExtColor2))");

            var fromDb = DataFinder.GetDataForVehicle(key);

            var vehicle = GetVehicle(key);

            Assert.That((string)vehicle.SelectToken("ExtColor.Id"), 
                Is.EqualTo(GetColorId(
                    DataFinder.GetColorCode(fromDb["ExteriorColorCode"]),
                    DataFinder.GetColorDesc(fromDb["ExtColor1"]), 
                    DataFinder.GetColorCode(fromDb["ExteriorColorCode2"]), 
                    DataFinder.GetColorDesc(fromDb["ExtColor2"]))));
            Assert.That((string)vehicle.SelectToken("ExtColor.Desc"), 
                Is.EqualTo(GetColorDesc(fromDb["ExtColor1"], fromDb["ExtColor2"])));
            JToken dummy;
            Assert.That(((JObject)vehicle.SelectToken("ExtColor")).TryGetValue("Simple", out dummy), Is.False);
        }

        [Test]
        public void IntColor_should_return_color_when_IntColor_set_and_color_found_in_chrome()
        {
            var key = DataFinder.FindVehicle(
                @"OC.ChromeStyleID <> -1
                    and OC.IntColor <> '' and isnull(OC.InteriorColorCode, '') = ''
                    and exists (select 1
                                from VehicleCatalog.Chrome.Colors C
                                where C.CountryCode = 1
                                    and C.StyleID = OC.ChromeStyleID
                                    and isnull(C.IntDesc, '') = OC.IntColor)");

            var chromeColor = DataFinder.GetInteriorChromeColor(key);

            var vehicle = GetVehicle(key);

            Assert.That((string) vehicle.SelectToken("IntColor.Id"), Is.EqualTo(GetIntColorId(chromeColor)));
            Assert.That((string) vehicle.SelectToken("IntColor.Desc"), Is.EqualTo(GetIntColorDesc(chromeColor)));
        }

        [Test]
        public void IntColor_should_return_color_when_IntColor_and_InteriorColorCode_set_and_Color_found_in_chrome()
        {
            var key = DataFinder.FindVehicle(
                @"OC.ChromeStyleID <> -1
                    and OC.IntColor <> '' and isnull(OC.InteriorColorCode, '') <> ''
                    and (exists (select 1
                                 from VehicleCatalog.Chrome.Colors C
                                 where C.CountryCode = 1
                                     and C.StyleID = OC.ChromeStyleID
                                     and case when isnull(C.IntManCode, '') = '' then C.IntCode else C.IntManCode end = OC.InteriorColorCode)
                      or exists (select 1
                                 from VehicleCatalog.Chrome.Colors C
                                 where C.CountryCode = 1
                                     and C.StyleID = OC.ChromeStyleID
                                     and C.IntDesc = OC.IntColor))");

            var chromeColor = DataFinder.GetInteriorChromeColor(key);

            var vehicle = GetVehicle(key);

            Assert.That((string)vehicle.SelectToken("IntColor.Id"), Is.EqualTo(GetIntColorId(chromeColor)));
            Assert.That((string)vehicle.SelectToken("IntColor.Desc"), Is.EqualTo(GetIntColorDesc(chromeColor)));
        }

        [Test]
        public void IntColor_should_return_color_when_IntColor_set_but_color_IS_NOT_found_in_chrome()
        {
            var key = DataFinder.FindVehicle(
                @"OC.ChromeStyleID <> -1
                    and OC.IntColor <> '' and isnull(OC.InteriorColorCode, '') = ''
                    and not exists (select 1
                                from VehicleCatalog.Chrome.Colors C
                                where C.CountryCode = 1
                                    and C.StyleID = OC.ChromeStyleID
                                    and isnull(C.IntDesc, '') = OC.IntColor)");

            var fromDb = DataFinder.GetDataForVehicle(key);

            var vehicle = GetVehicle(key);

            Assert.That((string)vehicle.SelectToken("IntColor.Id"), 
                Is.EqualTo(GetColorId(
                    DataFinder.GetColorCode(fromDb["InteriorColorCode"]), 
                    DataFinder.GetColorDesc(fromDb["IntColor"]))));
            Assert.That((string) vehicle.SelectToken("IntColor.Desc"),
                Is.EqualTo(GetColorDesc(
                    DataFinder.GetColorDesc(fromDb["IntColor"]))));
        }

        [Test]
        public void IntColor_should_return_color_when_IntColor_and_InteriorColorCode_set_but_color_IS_NOT_found_in_chrome()
        {
            var key = DataFinder.FindVehicle(
                @"OC.ChromeStyleID <> -1
                    and OC.IntColor <> '' and isnull(OC.InteriorColorCode, '') <> ''
                    and not exists (select 1
                                from VehicleCatalog.Chrome.Colors C
                                where C.CountryCode = 1
                                    and C.StyleID = OC.ChromeStyleID
                                    and isnull(C.IntDesc, '') = OC.IntColor)
                    and not exists (select 1
                                from VehicleCatalog.Chrome.Colors C
                                where C.CountryCode = 1
                                    and C.StyleID = OC.ChromeStyleID
                                    and case when isnull(C.IntManCode, '') = '' then C.IntCode else C.IntManCode end = OC.InteriorColorCode)");

            var fromDb = DataFinder.GetDataForVehicle(key);

            var vehicle = GetVehicle(key);

            Assert.That((string)vehicle.SelectToken("IntColor.Id"),
                Is.EqualTo(GetColorId(
                    DataFinder.GetColorCode(fromDb["InteriorColorCode"]),
                    DataFinder.GetColorDesc(fromDb["IntColor"]))));
            Assert.That((string)vehicle.SelectToken("IntColor.Desc"),
                Is.EqualTo(GetColorDesc(
                    DataFinder.GetColorDesc(fromDb["IntColor"]))));
        }

        [Test]
        public void IntColor_should_return_null_when_IntColor_description_is_empty()
        {
            var key = DataFinder.FindVehicle(
                @"OC.ChromeStyleID <> -1
                    and OC.IntColor = '' and isnull(OC.InteriorColorCode, '') = ''");

            var vehicle = GetVehicle(key);

            Assert.That(vehicle.PropertyExists("IntColor"), Is.False);
        }

        [Test]
        public void IntColor_should_return_null_when_OptionsConfiguration_record_is_missing()
        {
            var key = DataFinder.FindVehicle(
                @"not exists (select 1 
                              from Merchandising.builder.OptionsConfiguration OC2 
                              where I.BusinessUnitID = OC2.BusinessUnitID and I.InventoryID = OC2.InventoryID)");

            var vehicle = GetVehicle(key);

            Assert.That(vehicle.PropertyExists("IntColor"), Is.False);
        }

        private static string GetIntColorId(DataRow color)
        {
            var code = DataFinder.GetColorCode(color["IntManCode"], color["IntCode"]);
            var desc = DataFinder.GetColorDesc(color["IntDesc"]);

            return GetColorId(code, desc);
        }

        private static string GetExtColorId(DataRow color)
        {
            var code1 = DataFinder.GetColorCode(color["Ext1ManCode"], color["Ext1Code"]);
            var desc1 = DataFinder.GetColorDesc(color["Ext1Desc"]);
            var code2 = DataFinder.GetColorCode(color["Ext2ManCode"], color["Ext2Code"]);
            var desc2 = DataFinder.GetColorDesc(color["Ext2Desc"]);

            return GetColorId(code1, desc1, code2, desc2);
        }

        private static string GetColorId(string code1, string desc1, string code2 = "", string desc2 = "")
        {
            var vals = new List<string>();

            if (code1 != "") vals.Add("c=" + code1);
            if (desc1 != "") vals.Add("d=" + HttpUtility.UrlEncode(desc1));
            if (code2 != "") vals.Add("c2=" + code2);
            if (desc2 != "") vals.Add("d2=" + HttpUtility.UrlEncode(desc2));

            return string.Join("&", vals);
        }

        private static string GetExtColorDesc(DataRow color)
        {
            return GetColorDesc(color["Ext1Desc"], color["Ext2Desc"]);
        }

        private static string GetIntColorDesc(DataRow color)
        {
            return GetColorDesc(color["IntDesc"]);
        }

        private static string GetColorDesc(object d1, object d2 = null)
        {
            var desc1 = DataFinder.GetColorDesc(d1);
            var desc2 = DataFinder.GetColorDesc(d2);
            return string.Join(" with ", new[] {desc1, desc2}.Where(t => t != ""));
        }

        private static string GetExtColorSimple(DataRow color)
        {
            var simple1 = DataFinder.GetColorSimple(color["GenericExtColor"]);
            var simple2 = DataFinder.GetColorSimple(color["GenericExt2Color"]);
            return string.Join(" / ", new[] {simple1, simple2}.Where(t => t != ""));
        }

        private HttpResponseMessage GetResponse(VehicleKey key)
        {
            var url = GetUrl(key);
            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            SetValidCredential(request);
            var response = client.SendAsync(request).Result;
            return response;
        }

        private JObject GetVehicle(VehicleKey key)
        {
            var response = GetResponse(key);

            if (!response.IsSuccessStatusCode)
                throw new Exception("HTTP Request Failed: " + response.ToString());
                        
            return JObject.Parse(response.Content.ReadAsStringAsync().Result);
        }
    }
}