﻿using System.Data;
using System.Net.Http;
using NUnit.Framework;
using Newtonsoft.Json.Linq;
using System;

namespace SellingWebService.IntegrationTests.WebLoader.Get_Vehicle
{
    [TestFixture]
    public class NoChromeStyleVehicleTests : BaseFixture
    {
        private HttpResponseMessage _response;
        private string _url;
        private DataRow _fromDb;
        private JObject _vehicle;
        private VehicleKey _vehicleWithNoChromeStyle;

        [TestFixtureSetUp]
        public void SetupFixture()
        {
            _vehicleWithNoChromeStyle = DataFinder.FindVehicle("isnull(OC.ChromeStyleID, -1) = -1");
            _url = GetUrl(_vehicleWithNoChromeStyle);
            _fromDb = DataFinder.GetDataForVehicle(_vehicleWithNoChromeStyle);
            _response = GetResponse();
            _vehicle = JObject.Parse(_response.Content.ReadAsStringAsync().Result);
        }

        private HttpResponseMessage GetResponse()
        {
            var client = new HttpClient();

            var request = new HttpRequestMessage(HttpMethod.Get, _url);
            SetValidCredential(request);

            var response = client.SendAsync(request).Result;
            if (!response.IsSuccessStatusCode)
                throw new Exception("HTTP Request Failed: " + response.ToString());

            return response;
        }

        [Test]
        public void Make_should_pull_from_MMG_table_when_there_is_no_style()
        {
            Assert.That((string) _vehicle.SelectToken("Make"), Is.EqualTo(_fromDb["MMG_Make"]));
        }

        [Test]
        public void Model_should_pull_from_MMG_table_when_there_is_no_style()
        {
            Assert.That((string) _vehicle.SelectToken("Model"), Is.EqualTo(_fromDb["MMG_Model"]));
        }

        [Test]
        public void Trim_should_not_exist_when_there_is_no_style()
        {
            JToken token;
            Assert.That(_vehicle.TryGetValue("Trim", out token), Is.False);
        }
    }
}