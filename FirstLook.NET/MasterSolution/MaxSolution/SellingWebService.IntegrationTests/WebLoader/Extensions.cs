﻿using Newtonsoft.Json.Linq;

namespace SellingWebService.IntegrationTests.WebLoader
{
    public static class Extensions
    {
        public static bool PropertyExists(this JObject obj, string propName)
        {
            JToken token;
            return obj.TryGetValue(propName, out token);
        }

         
    }
}