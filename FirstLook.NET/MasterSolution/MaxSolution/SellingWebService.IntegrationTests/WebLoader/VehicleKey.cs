﻿using System.Data;
using System.Globalization;
using System.Linq;
using NUnit.Framework;

namespace SellingWebService.IntegrationTests.WebLoader
{
    public class VehicleKey
    {
        public string DealerCode { get; set; }
        public string Vehicle { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public static VehicleKey FromTable(DataTable table)
        {
            var row = table.Rows
                .Cast<DataRow>()
                .Select(FromRow)
                .SingleOrDefault();
            if(row == null)
                Assert.Inconclusive("No vehicles matched the given criteria.");
            return row;
        }

        public static VehicleKey FromRow(DataRow r)
        {
            return new VehicleKey
                {
                    DealerCode = (string) r["DealerCode"],
                    Vehicle = ((int) r["InventoryID"]).ToString(CultureInfo.InvariantCulture),
                    UserName = (string)r["UserName"],
                    Password = (string)r["Password"]
                };
        }
    }
}