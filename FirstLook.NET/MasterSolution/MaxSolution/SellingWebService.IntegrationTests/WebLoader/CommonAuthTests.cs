﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using NUnit.Framework;

namespace SellingWebService.IntegrationTests.WebLoader
{
    public static class CommonAuthTests
    {
        public static VehicleKey FindVehicleHavingUserWithNoAccessToDealer(string username)
        {
            var table = DataFinder.ExecQuery(DataFinder.Merchandising,
                string.Format(@"
                select top 1 BusinessUnitCode as DealerCode, InventoryID, '{0}' as UserName, '{1}' as Password
                from IMT.dbo.BusinessUnit BU
                    inner join FLDW.dbo.InventoryActive I on I.BusinessUnitID = BU.BusinessUnitID
                    inner join IMT.dbo.DealerPreference DP on BU.BusinessUnitID = DP.BusinessUnitID
                where I.InventoryActive = 1
                    and BU.Active = 1
                    and BU.BusinessUnitTypeID = 4
                    and DP.GoLiveDate is not null
                    and exists (select 1 
                                from IMT.dbo.DealerUpgrade DU
                                where DealerUpgradeCD = 24 
                                    and EffectiveDateActive = 1
                                    and DU.BusinessUnitID = BU.BusinessUnitID)
                    and exists (select 1
                                from Merchandising.settings.Merchandising S
                                where WebLoaderEnabled = 1
                                    and S.BusinessUnitID = BU.BusinessUnitID)
                    and not exists 
                               (select 1
                                from IMT.dbo.Member M
                                    inner join IMT.dbo.MemberAccess MA on M.MemberID = MA.MemberID
                                where MA.BusinessUnitID = BU.BusinessUnitID
                                    and M.Login = '{0}')
                                
                ", username, Settings.StandardPassword));
            return VehicleKey.FromTable(table);
        }

        private static VehicleKey FindDealerWhichDoesntHaveMerchandisingUpgrade()
        {
            var table = DataFinder.ExecQuery(DataFinder.Merchandising,
                string.Format(@"
                select top 1 BusinessUnitCode as DealerCode, InventoryID, '{0}' as UserName, '{1}' as Password
                from IMT.dbo.BusinessUnit BU
                    inner join FLDW.dbo.InventoryActive I on I.BusinessUnitID = BU.BusinessUnitID
                    inner join IMT.dbo.DealerPreference DP on BU.BusinessUnitID = DP.BusinessUnitID
                where I.InventoryActive = 1
                    and BU.Active = 1
                    and BU.BusinessUnitTypeID = 4
                    and DP.GoLiveDate is not null
                    and not exists (select 1
                                from IMT.dbo.DealerUpgrade DU
                                where DealerUpgradeCD = 24 
                                    and EffectiveDateActive = 1
                                    and DU.BusinessUnitID = BU.BusinessUnitID)
                    and exists (select 1
                                from Merchandising.settings.Merchandising S
                                where WebLoaderEnabled = 1
                                    and S.BusinessUnitID = BU.BusinessUnitID)
                ", Settings.AdminUser, Settings.StandardPassword));
            return VehicleKey.FromTable(table);
        }

        private static VehicleKey FindDealerWhichDoesntHaveWebLoaderUpgrade()
        {
            var table = DataFinder.ExecQuery(DataFinder.Merchandising,
                string.Format(@"
                select top 1 BusinessUnitCode as DealerCode, InventoryID, '{0}' as UserName, '{1}' as Password
                from IMT.dbo.BusinessUnit BU
                    inner join FLDW.dbo.InventoryActive I on I.BusinessUnitID = BU.BusinessUnitID
                    inner join IMT.dbo.DealerPreference DP on BU.BusinessUnitID = DP.BusinessUnitID
                where I.InventoryActive = 1
                    and BU.Active = 1
                    and BU.BusinessUnitTypeID = 4
                    and DP.GoLiveDate is not null
                    and exists (select 1
                                from IMT.dbo.DealerUpgrade DU
                                where DealerUpgradeCD = 24 
                                    and EffectiveDateActive = 1
                                    and DU.BusinessUnitID = BU.BusinessUnitID)
                    and not exists (select 1
                                from Merchandising.settings.Merchandising S
                                where WebLoaderEnabled = 1
                                    and S.BusinessUnitID = BU.BusinessUnitID)
                ", Settings.AdminUser, Settings.StandardPassword));
            return VehicleKey.FromTable(table);
        }

        public static void Get_with_no_Authorization_header_should_return_401(string url)
        {
            var client = new HttpClient();

            var request = new HttpRequestMessage(HttpMethod.Get, url);
            
            var response = client.SendAsync(request).Result;
            
            Assert401(response);
        }

        public static void Get_with_valid_user_but_bad_password_should_return_401(string url)
        {
            var client = new HttpClient();

            var request = new HttpRequestMessage(HttpMethod.Get, url);
            request.Headers.Authorization = new AuthenticationHeaderValue("Basic", EncodeCredential(Settings.AdminUser, Settings.InvalidPassword));

            var response = client.SendAsync(request).Result;

            Assert401(response);
        }

        public static void Get_with_bad_user_but_valid_password_should_return_401(string url)
        {
            var client = new HttpClient();

            var request = new HttpRequestMessage(HttpMethod.Get, url);
            request.Headers.Authorization = new AuthenticationHeaderValue("Basic", EncodeCredential(Settings.InvalidUsername, Settings.StandardPassword));

            var response = client.SendAsync(request).Result;

            Assert401(response);
        }

        public static void Get_with_good_credentials_but_invalid_dealer_should_return_404(string urlWithBadDealer)
        {
            var client = new HttpClient();

            var request = new HttpRequestMessage(HttpMethod.Get, urlWithBadDealer);
            request.Headers.Authorization = new AuthenticationHeaderValue("Basic", EncodeCredential(Settings.AdminUser, Settings.StandardPassword));

            var response = client.SendAsync(request).Result;

            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NotFound), string.Format("Url: {0}", urlWithBadDealer));
        }

        public static void Get_with_good_credentials_and_dealer_but_user_not_allowed_at_dealer_should_return_403(
            Func<VehicleKey,string> getUrl)
        {
            var vehicleKey = FindVehicleHavingUserWithNoAccessToDealer(Settings.LimitedUser);
            var url = getUrl(vehicleKey);

            var client = new HttpClient();

            var request = new HttpRequestMessage(HttpMethod.Get, url);
            request.Headers.Authorization = new AuthenticationHeaderValue("Basic", EncodeCredential(vehicleKey));

            var response = client.SendAsync(request).Result;

            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Forbidden), string.Format("Url: {0}", url));
        }

        public static void Get_with_good_credentials_and_dealer_but_dealer_doesnt_have_Merchandising_upgrade_should_return_403(
            Func<VehicleKey, string> getUrl)
        {
            var vehicleKey = FindDealerWhichDoesntHaveMerchandisingUpgrade();
            var url = getUrl(vehicleKey);

            var client = new HttpClient();

            var request = new HttpRequestMessage(HttpMethod.Get, url);
            request.Headers.Authorization = new AuthenticationHeaderValue("Basic", EncodeCredential(vehicleKey));

            var response = client.SendAsync(request).Result;

            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Forbidden), string.Format("Url: {0}", url));
        }

        public static void Get_with_good_credential_and_dealer_but_dealer_doesnt_have_WebLoader_upgrade_should_return_403(
            Func<VehicleKey, string> getUrl)
        {
            var vehicleKey = FindDealerWhichDoesntHaveWebLoaderUpgrade();
            var url = getUrl(vehicleKey);

            var client = new HttpClient();

            var request = new HttpRequestMessage(HttpMethod.Get, url);
            request.Headers.Authorization = new AuthenticationHeaderValue("Basic", EncodeCredential(vehicleKey));

            var response = client.SendAsync(request).Result;

            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Forbidden), string.Format("Url: {0}", url));
        }

        public static string EncodeCredential(VehicleKey vehicleKey)
        {
            return EncodeCredential(vehicleKey.UserName, vehicleKey.Password);
        }

        public static string EncodeCredential(string username, string password)
        {
            return Convert.ToBase64String(
                Encoding.UTF8.GetBytes(
                string.Format("{0}:{1}", username, password)));
        }

        private static void Assert401(HttpResponseMessage response)
        {
            var message = string.Format("Url: {0}", response.RequestMessage.RequestUri);
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Unauthorized), message);
            Assert.That(response.Headers.WwwAuthenticate.Count, Is.GreaterThan(0), message);
            Assert.That(response.Headers.WwwAuthenticate.Select(h => h.Scheme), Contains.Item("Basic"), message);
        }
    }
}