﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SellingWebService.IntegrationTests.WebLoader
{
    public static class MiniPhotoClient
    {
        private static readonly XNamespace _ns = "http://schemas.incisent.com/internal/photoWebServices/2011/01";
        private static readonly XNamespace _nsA = "http://schemas.microsoft.com/2003/10/Serialization/Arrays";

        public static Task<int> GetPhotoCountForVehicle(string dealerCode, string vin)
        {
            var client = new HttpClient();
            var url =
                string.Format(
                    "{0}/photoservices/v1.svc/rest/GetPhotoUrlsByVin?businessUnitCode={1}&vin={2}&timeoutMilliseconds=30000",
                    Settings.MaxServicesBaseUrl, dealerCode, vin);

            return client
                .GetStringAsync(url)
                .ContinueWith(t => XDocument.Parse(t.Result)
                                       .Descendants(_ns + "PhotoUrls")
                                       .Elements(_nsA + "string")
                                       .Count());
        }

        public static Task<Dictionary<string, int>> GetPhotoCountsForDealership(string dealerCode)
        {
            var client = new HttpClient();
            var url =
                string.Format(
                    "{0}/photoservices/v1.svc/rest/GetPhotoUrlsByBusinessUnit?businessUnitCode={1}&timeoutMilliseconds=30000",
                    Settings.MaxServicesBaseUrl, dealerCode);

            return client
                .GetStringAsync(url)
                .ContinueWith(t => XDocument.Parse(t.Result)
                                       .Descendants(_ns + "VehiclePhotos")
                                       .ToDictionary(
                                           vp => (string) vp.Element(_ns + "Vin"),
                                           vp => vp.Descendants(_nsA + "string").Count(),
                                           StringComparer.InvariantCultureIgnoreCase
                                       ));
        }
    }
}