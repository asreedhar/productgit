﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Max.Selling.WebService.WebLoader.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace SellingWebService.IntegrationTests.WebLoader
{
    [TestFixture]
    public class EtagTests
    {
        [Test]
        public void Test_Etag_ChangeExteriorColor()
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            var vehicleKey = DataFinder.FindVehicle(
               @"OC.ChromeStyleID <> -1 and
                    OC.ExtColor1 <> '' and OC.ExtColor2 <> '' and isnull(OC.ExteriorColorCode, '') = '' and isnull(OC.ExteriorColorCode2, '') = ''
                    and exists (select 1
                                from VehicleCatalog.Chrome.Colors C
                                where C.CountryCode = 1
                                    and C.StyleID = OC.ChromeStyleID
                                    and isnull(C.Ext1Desc, '') = OC.ExtColor1
                                    and isnull(C.Ext2Desc, '') = OC.ExtColor2)");

            WebLoaderVehicleClient client = new WebLoaderVehicleClient();
            WebLoaderTrimClient trimClient = new WebLoaderTrimClient();

            var response = client.GetVehicle(vehicleKey);
            var getEtag = response.Response.Headers.ETag;
            var trimData = trimClient.GetTrim(response.Vehicle.Trim.Id, vehicleKey.UserName, vehicleKey.Password);

            var setExtColor = trimData.AvailableExtColors.Where(x => x.Id != response.Vehicle.ExtColor.Id).First();

            //assert colors are different
            Assert.IsTrue(response.Vehicle.ExtColor.Id != setExtColor.Id);

            var inputModel = JsonConvert.SerializeObject(new ExteriorColor{ExtColor = setExtColor});
            response = client.PutVehicle(vehicleKey, inputModel, response);
            var putEtag = response.Response.Headers.ETag;

            //assert etags are different
            Assert.IsTrue(getEtag.Tag != putEtag.Tag);
            Assert.IsTrue(response.Vehicle.ExtColor.Id == setExtColor.Id);

            response = client.GetVehicle(vehicleKey, putEtag.Tag, /*expect bad result*/ true);

            //assert not modified
            Assert.IsTrue(response.Response.StatusCode == HttpStatusCode.NotModified);
            //assert reponse eTag is same as put tag.
            Assert.IsTrue(response.Response.Headers.ETag.Tag == putEtag.Tag);
        }

        [Test]
        public void Test_Etag_ChangeInteriorColor_AndPackages()
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            var vehicleKey = DataFinder.FindVehicle(
              @"OC.IntColor = ''  and isnull(OC.InteriorColorCode, '') = '' and (exists (select 1
                                 from VehicleCatalog.Chrome.Colors C
                                 where C.CountryCode = 1
                                     and C.StyleID = OC.ChromeStyleID
                                     and c.IntManCode is not null and c.IntManCode <> '' )
                      )");

            WebLoaderVehicleClient client = new WebLoaderVehicleClient();
            WebLoaderTrimClient trimClient = new WebLoaderTrimClient();

            var response = client.GetVehicle(vehicleKey);
            var getEtag = response.Response.Headers.ETag;
            var trimData = trimClient.GetTrim(response.Vehicle.Trim.Id, vehicleKey.UserName, vehicleKey.Password);

            var setInteriorColor = trimData.AvailableIntColors.First();
            if (response.Vehicle.IntColor != null)
            {
                setInteriorColor = trimData.AvailableIntColors.Where(x => x.Id != response.Vehicle.IntColor.Id).First();
                //assert colors are different
                Assert.IsTrue(response.Vehicle.IntColor.Id != setInteriorColor.Id);
            }

            var inputModel = JsonConvert.SerializeObject(new InteriorColor() { IntColor = new IntColor(){Id = setInteriorColor.Id}});
            response = client.PutVehicle(vehicleKey, inputModel, response);

            var putEtag = response.Response.Headers.ETag;

            //assert etags are different
            Assert.IsTrue(HttpUtility.UrlDecode(response.Vehicle.IntColor.Id) == HttpUtility.UrlDecode(setInteriorColor.Id));
            Assert.IsTrue(getEtag.Tag != putEtag.Tag);
        }

        private class InteriorColor
        {
            public IntColor IntColor { get; set; }
        }

        private class IntColor
        {
            public string Id { get; set; }
        }

        private class ExteriorColor
        {
            public ExtColor ExtColor { get; set; }
        }
    }
}
