﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using Max.Selling.WebService.WebLoader.ViewModels;
using Newtonsoft.Json.Linq;

namespace SellingWebService.IntegrationTests.WebLoader
{
    public class WebLoaderVehicleClient
    {
        public class VehicleResponse
        {
            private readonly Lazy<Vehicle> _vehicle;
            private readonly JObject _doc;
            private readonly HttpResponseMessage _response;

            public Vehicle Vehicle
            {
                get { return _vehicle.Value; }
            }

            public JObject Doc
            {
                get { return _doc; }
            }

            public HttpResponseMessage Response
            {
                get { return _response; }
            }

            public VehicleResponse(HttpResponseMessage response)
            {
                Console.WriteLine("Response Status: {0} {1}", (int)response.StatusCode, response.ReasonPhrase);
                if(response.IsSuccessStatusCode)
                {
                    _doc = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                    _vehicle = new Lazy<Vehicle>(() => _doc.ToObject<Vehicle>());
                }
                else
                {
                    _doc = null;
                    _vehicle = new Lazy<Vehicle>(() => null);
                }
                _response = response;
            }
        }

        public VehicleResponse GetVehicle(VehicleKey key)
        {
            return GetVehicle(key, null);

        }

        public VehicleResponse GetVehicle(VehicleKey key, string eTag, bool expectBadResult=false)
        {
            var requestUri = GetUrl(key);
            Console.WriteLine("GET {0}", requestUri);

            var request = new HttpRequestMessage(HttpMethod.Get, requestUri);
                        
            if(eTag != null)
                request.Headers.IfNoneMatch.Add(new EntityTagHeaderValue(eTag));
            
            SetValidCredential(request, key.UserName, key.Password);

            // Allow the test client to work with a server that has an invalid SSL Cert. i.e. Int-B
            ServicePointManager.ServerCertificateValidationCallback += (o, c, chain, policy) => true;
            var client = new HttpClient();
            var response = client.SendAsync(request).Result;
            if (!expectBadResult && !response.IsSuccessStatusCode)
                throw new Exception("HTTP Request Failed: " + response.ToString());

            return new VehicleResponse(response);
        }

        public VehicleResponse PutVehicle (VehicleKey key, string payLoad, VehicleResponse originalGetResponse)
        {
            var uri = GetUrl(key);
            Console.WriteLine("PUT {0}", uri);

            var request = new HttpRequestMessage(HttpMethod.Put, uri);
            request.Content = new StringContent(payLoad);
            SetIfMatchHeader(originalGetResponse, request);
            SetValidCredential(request, key.UserName, key.Password);

            // Allow the test client to work with a server that has an invalid SSL Cert. i.e. Int-B
            ServicePointManager.ServerCertificateValidationCallback += (o, c, chain, policy) => true;
            var client = new HttpClient();
            var response = client.SendAsync(request).Result;

            return new VehicleResponse(response);
        }

        public VehicleResponse PutVehicle (VehicleKey key, Vehicle vehicle, VehicleResponse originalGetResponse)
        {
            var uri = GetUrl(key);
            Console.WriteLine("PUT {0}", uri);

            var request = new HttpRequestMessage(HttpMethod.Put, uri);
            SetRequestContent(vehicle, request);
            SetIfMatchHeader(originalGetResponse, request);
            SetValidCredential(request, key.UserName, key.Password);

            // Allow the test client to work with a server that has an invalid SSL Cert. i.e. Int-B
            ServicePointManager.ServerCertificateValidationCallback += (o, c, chain, policy) => true;
            var client = new HttpClient();
            var response = client.SendAsync(request).Result;

            return new VehicleResponse(response);
        }

        private static HttpContent SetRequestContent(Vehicle vehicle, HttpRequestMessage request)
        {
            return request.Content = new ObjectContent(typeof (Vehicle), vehicle, new JsonMediaTypeFormatter());
        }

        private static void SetIfMatchHeader(VehicleResponse originalGetResponse, HttpRequestMessage request)
        {
            if (originalGetResponse != null && originalGetResponse.Response.Headers.Contains("ETag"))
            {
                request.Headers.IfMatch.Add(originalGetResponse.Response.Headers.ETag);
            }
        }

        private static string GetUrl(VehicleKey key)
        {
            return string.Format("{0}{1}/dealers/{2}/vehicles/{3}", Settings.MaxWebsiteBaseUrl, Settings.WebLoaderBaseUrl, key.DealerCode, key.Vehicle);
        }

        private static void SetValidCredential(HttpRequestMessage request, string username, string password)
        {
            request.Headers.Authorization = new AuthenticationHeaderValue("Basic", CommonAuthTests.EncodeCredential(username, password));
        }
    }
}
