﻿using Max.Selling.WebService.WebLoader.ViewModels;
using NUnit.Framework;
using SellingWebService.IntegrationTests.WebLoader.Get_Vehicle;

namespace SellingWebService.IntegrationTests.WebLoader.Put_Vehicle
{
    [TestFixture]
    public class VehicleUpdaterTests : BaseFixture
    {
        [TestCase( true )]
        [TestCase( false )]
        public void Put_Equipment_Completed_should_update_the_model(bool valueToChangeTo)
        {
            //Get current vehicle info
            var key = DataFinder.FindVehicle(
                ( valueToChangeTo ? "not " : ""  ) +
                @"exists (select 1 
                          from Merchandising.builder.VehicleStatus VS 
                          where VS.BusinessUnitID = I.BusinessUnitID 
                            and VS.InventoryID = I.InventoryID
                            and VS.StatusLevel >= 2
                            and VS.StatusTypeId in (1, 2))
                            AND not exists (select 1 from IMT.dbo.MakeModelGrouping mmg
                            where v.MakeModelGroupingID = mmg.MakeModelGroupingID and mmg.Make = 'HONDA')");;
            
            var client = new WebLoaderVehicleClient();
            var getResponse = client.GetVehicle(key);

            //Create the update requests to set the trim to the one noe selected
            var vehicle = new Vehicle { EquipmentCompleted = valueToChangeTo };
            var putResponse = client.PutVehicle(key, vehicle, getResponse);

            var getAgainResponse = client.GetVehicle(key);

            Assert.That(getResponse.Vehicle.EquipmentCompleted, Is.Not.EqualTo(valueToChangeTo));
            Assert.That(putResponse.Vehicle.EquipmentCompleted, Is.EqualTo(valueToChangeTo));
            Assert.That(getAgainResponse.Vehicle.EquipmentCompleted, Is.EqualTo(valueToChangeTo));
        }

        [TestCase( true )]
        [TestCase( false )]
        public void Put_Photos_Completed_should_update_the_model (bool valueToChangeTo)
        {
            //Get current vehicle info
            var key = DataFinder.FindVehicle(
                ( valueToChangeTo ? "not " : ""  ) +
                @"exists (select 1 
                          from Merchandising.builder.VehicleStatus VS 
                          where VS.BusinessUnitID = I.BusinessUnitID 
                            and VS.InventoryID = I.InventoryID
                            and VS.StatusLevel >= 2
                            and VS.StatusTypeId = 3)" );

            var client = new WebLoaderVehicleClient();
            var getResponse = client.GetVehicle(key);

            //Create the update requests to set the trim to the one noe selected
            var vehicle = new Vehicle { PhotosCompleted = valueToChangeTo };
            var putResponse = client.PutVehicle(key, vehicle, getResponse);

            var getAgainResponse = client.GetVehicle(key);

            Assert.That(getResponse.Vehicle.PhotosCompleted, Is.Not.EqualTo(valueToChangeTo));
            Assert.That(putResponse.Vehicle.PhotosCompleted, Is.EqualTo(valueToChangeTo));
            Assert.That(getAgainResponse.Vehicle.PhotosCompleted, Is.EqualTo(valueToChangeTo));
        }
    }
}
