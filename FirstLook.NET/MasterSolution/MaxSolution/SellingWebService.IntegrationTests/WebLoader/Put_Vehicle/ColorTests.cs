﻿using System.Net;
using Max.Selling.WebService.WebLoader.Model;
using Max.Selling.WebService.WebLoader.ViewModels;
using NUnit.Framework;

namespace SellingWebService.IntegrationTests.WebLoader.Put_Vehicle
{
    public class ColorTests
    {
        private readonly WebLoaderVehicleClient _vehicleClient = new WebLoaderVehicleClient();
        private readonly WebLoaderTrimClient _trimClient = new WebLoaderTrimClient();

        private VehicleKey _key;
        private Trim _trim;

        [Test]
        public void Ext1_put_should_update_extcolor_in_database ()
        {
            _key = DataFinder.FindVehicle(
                @"OC.ExtColor1 = '' and OC.ExtColor2 = '' and isnull(OC.ExteriorColorCode, '') = '' and isnull(OC.ExteriorColorCode2, '') = ''
                and (exists (select 1
                                from VehicleCatalog.Chrome.Colors C
                                where C.CountryCode = 1
                                    and C.StyleID = OC.ChromeStyleID
                                    and c.Ext1Code is not null and c.Ext1Code <> '' ))" );

            var getResponse = _vehicleClient.GetVehicle( _key );
            var vehicle = getResponse.Vehicle;
            _trim = _trimClient.GetTrim(getResponse.Vehicle.Trim.Id, _key.UserName, _key.Password );

            vehicle.ExtColor = new ExtColorRef { Id = _trim.AvailableExtColors[0].Id, Desc = _trim.AvailableExtColors[0].Desc };

            var putResponse = _vehicleClient.PutVehicle( _key, vehicle, getResponse );
            var colorSet = Color.ParseColor1( vehicle.ExtColor.Id );
            var resultColor = Color.ParseColor1( putResponse.Vehicle.ExtColor.Id );
            Assert.AreEqual( colorSet.Code, resultColor.Code );
        }
        
        [Test]
        public void Ext1_and_ext2_put_should_update_extcolor_int_database ()
        {
            _key = DataFinder.FindVehicle(
                @"OC.ExtColor1 = '' and OC.ExtColor2 = '' and isnull(OC.ExteriorColorCode, '') = '' and isnull(OC.ExteriorColorCode2, '') = ''
                and (exists (select 1
                                from VehicleCatalog.Chrome.Colors C
                                where C.CountryCode = 1
                                    and C.StyleID = OC.ChromeStyleID
                                    and c.Ext1Code is not null and c.Ext1Code <> '' 
                                    and c.Ext2Code is not null and c.Ext2Code <> '' ))" );

            var getResponse = _vehicleClient.GetVehicle( _key );
            var vehicle = getResponse.Vehicle;
            _trim = _trimClient.GetTrim( vehicle.Trim.Id, _key.UserName, _key.Password );
            var extColorToSet = _trim.AvailableExtColors.Find(x => x.Id.IndexOf("c2") > 0);

            vehicle.ExtColor = new ExtColorRef { Id = extColorToSet.Id, Desc =extColorToSet.Desc };

            var putResponse = _vehicleClient.PutVehicle( _key, vehicle, getResponse );
            var colorSet1 = Color.ParseColor1( vehicle.ExtColor.Id );
            var resultColor1 = Color.ParseColor1( putResponse.Vehicle.ExtColor.Id );

            var colorSet2 = Color.ParseColor2( vehicle.ExtColor.Id );
            var resultColor2 = Color.ParseColor2( putResponse.Vehicle.ExtColor.Id );
            Assert.AreEqual( colorSet1.Code, resultColor1.Code );
            Assert.AreEqual( colorSet2.Code, resultColor2.Code );
        }

        [Test]
        public void Int_color_put_should_update_the_database ()
        {
            _key = DataFinder.FindVehicle(
                @"OC.IntColor = ''  and isnull(OC.InteriorColorCode, '') = '' and (exists (select 1
                                 from VehicleCatalog.Chrome.Colors C
                                 where C.CountryCode = 1
                                     and C.StyleID = OC.ChromeStyleID
                                     and c.IntManCode is not null and c.IntManCode <> '' )
                      )");
            var getResponse = _vehicleClient.GetVehicle(_key);
            _trim = _trimClient.GetTrim(getResponse.Vehicle.Trim.Id, _key.UserName, _key.Password);

            getResponse.Vehicle.IntColor = new Ref
                                               {
                                                   Id = _trim.AvailableIntColors[0].Id,
                                                   Desc = _trim.AvailableIntColors[0].Desc
                                               };

            var putResponse = _vehicleClient.PutVehicle(_key, getResponse.Vehicle, getResponse);
            var colorSet = Color.ParseColor1(getResponse.Vehicle.IntColor.Id);
            var resultColor = Color.ParseColor1(putResponse.Vehicle.IntColor.Id);

            Assert.AreEqual(colorSet.Code, resultColor.Code);
        }

        [Test]
        public void IntColor_put_should_throw_exception_if_trim_is_not_set()
        {
            _key = DataFinder.FindVehicle("isnull(OC.ChromeStyleID, -1) = -1");
            var getResponse = _vehicleClient.GetVehicle(_key);

            getResponse.Vehicle.IntColor = new Ref {Id = "abc", Desc = "def"};

            var putResponse = _vehicleClient.PutVehicle(_key, getResponse.Vehicle, getResponse);

            Assert.That(putResponse.Response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
        }

        [Test]
        public void ExtColor_put_should_throw_exception_if_trim_is_not_set()
        {
            _key = DataFinder.FindVehicle("isnull(OC.ChromeStyleID, -1) = -1");
            var getResponse = _vehicleClient.GetVehicle(_key);

            getResponse.Vehicle.ExtColor = new ExtColorRef() {Id = "abc", Desc = "def"};

            var putResponse = _vehicleClient.PutVehicle(_key, getResponse.Vehicle, getResponse);

            Assert.That(putResponse.Response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
        }
    }
}
