﻿using System.Collections.Generic;
using System.Net;
using Max.Selling.WebService.WebLoader.ViewModels;
using NUnit.Framework;

namespace SellingWebService.IntegrationTests.WebLoader.Put_Vehicle
{
    public class OptionsTests
    {
        private readonly WebLoaderVehicleClient _vehicleClient = new WebLoaderVehicleClient();
        private readonly WebLoaderTrimClient _trimClient = new WebLoaderTrimClient();

        [Test]
        public void Options_posted_should_set_options_available_for_trim_in_database()
        {
            var key = DataFinder.FindVehicle("OC.ChromeStyleID > 0 ");

            var getResponse = _vehicleClient.GetVehicle(key);
            var trim = _trimClient.GetTrim(getResponse.Vehicle.Trim.Id, key.UserName, key.Password);

            var vehicle = getResponse.Vehicle;
            vehicle.Options = new List<OptionRef>();

            if (trim.AvailableOptions.Count > 0)
                vehicle.Options.Add(new OptionRef {Id = trim.AvailableOptions[0].Id, Desc = trim.AvailableOptions[0].Desc});
            if (trim.AvailableOptions.Count > 1)
                vehicle.Options.Add(new OptionRef {Id = trim.AvailableOptions[2].Id, Desc = trim.AvailableOptions[2].Desc});

            var putResponse = _vehicleClient.PutVehicle(key, vehicle, getResponse);

            Assert.AreEqual(putResponse.Vehicle.Options.Count, vehicle.Options.Count);
        }

        [Test]
        public void Options_posted_should_not_set_options_not_available_for_trim_in_database()
        {
            var key = DataFinder.FindVehicle("OC.ChromeStyleID > 0 ");
            var getResponse = _vehicleClient.GetVehicle(key);
            var vehicle = getResponse.Vehicle;
            vehicle.Options = new List<OptionRef>
                                  {
                                      new OptionRef {Id = "@123nonexistentoption1", Desc = "test1"},
                                      new OptionRef {Id = "@123nonexistentoption2", Desc = "test2"}
                                  };

            var putResponse = _vehicleClient.PutVehicle(key, vehicle, getResponse);

            Assert.AreEqual(putResponse.Vehicle.Options.Count, 0);
        }

        [Test]
        public void Options_posted_should_throw_exception_if_trim_is_not_set()
        {
            var key = DataFinder.FindVehicle("isnull(OC.ChromeStyleID, -1) = -1");
            var getResponse = _vehicleClient.GetVehicle(key);
            var vehicle = getResponse.Vehicle;
            vehicle.Options = new List<OptionRef>
                                  {
                                      new OptionRef {Id = "@123nonexistentoption1", Desc = "test1"},
                                      new OptionRef {Id = "@123nonexistentoption2", Desc = "test2"}
                                  };

            var putResponse = _vehicleClient.PutVehicle(key, vehicle, getResponse);

            Assert.That(putResponse.Response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
        }
    }
}