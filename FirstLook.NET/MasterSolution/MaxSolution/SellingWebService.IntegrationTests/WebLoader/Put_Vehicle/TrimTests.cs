﻿using Max.Selling.WebService.WebLoader.ViewModels;
using NUnit.Framework;

namespace SellingWebService.IntegrationTests.WebLoader.Put_Vehicle
{
    [TestFixture]
    public class TrimTests
	{
        private readonly WebLoaderVehicleClient _vehicleClient = new WebLoaderVehicleClient();

        [Test]
        public void Should_be_able_to_set_trim_for_vehicle_with_multiple_available_trims()
        {
            var vehicleKey = DataFinder.FindVehicle(
                having: string.Format( "count(distinct VPSM.ChromeStyleID) > 1 and BusinessUnitCode = '{0}'",
                                  Settings.DefaultDealerCode ) );
           TestPut( vehicleKey );
        }
        [Test]
        public void Should_be_able_to_set_trim_for_vehicle_with_trim_not_set()
        {
            var vehicleKey = DataFinder.FindVehicle("isnull(OC.ChromeStyleID, -1) = -1");
            TestPut(vehicleKey);
        }

        [Test]
        public void Should_be_able_to_change_trim ()
        {
            var vehicleKey = DataFinder.FindVehicle( "OC.ChromeStyleID > 0", having: string.Format( "count(distinct VPSM.ChromeStyleID) > 1 and BusinessUnitCode = '{0}'",
                                  Settings.DefaultDealerCode ) );
            TestPut( vehicleKey );
        }
       
        private void TestPut(VehicleKey vehicleKey)
        {
            var getResponse = _vehicleClient.GetVehicle( vehicleKey );

            var trimIds = getResponse.Vehicle.AvailableTrimIds;
            var currentTrimId = string.Empty;
            if (getResponse.Vehicle.Trim != null && !string.IsNullOrWhiteSpace(getResponse.Vehicle.Trim.Id))
            {
               currentTrimId = getResponse.Vehicle.Trim.Id;
            }
            var trimToSet = trimIds.IndexOf(currentTrimId) > 0 ? trimIds[0] : trimIds[1];

            var vehicle = new Vehicle {Trim = new Ref {Desc = "test", Id = trimToSet}};

            var putResponse =_vehicleClient.PutVehicle( vehicleKey, vehicle, getResponse );
            currentTrimId = putResponse.Vehicle.Trim.Id;

            //verify the trim is set
            Assert.AreEqual(currentTrimId, trimToSet);
           
        }
    }
}
