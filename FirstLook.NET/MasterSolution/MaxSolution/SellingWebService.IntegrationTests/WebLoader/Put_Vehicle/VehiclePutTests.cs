﻿using System.Linq;
using Max.Selling.WebService.WebLoader.ViewModels;
using NUnit.Framework;

namespace SellingWebService.IntegrationTests.WebLoader.Put_Vehicle
{
    [TestFixture]
    public class VehiclePutTests
    {
        private readonly WebLoaderVehicleClient _vehicleClient = new WebLoaderVehicleClient();
        private readonly WebLoaderTrimClient _trimClient = new WebLoaderTrimClient();

        private VehicleKey _key;
        private Trim _trim;
        private WebLoaderVehicleClient.VehicleResponse _getResponse;
        private WebLoaderVehicleClient.VehicleResponse _putResponse;

        [TestFixtureSetUp]
        public void SetUp()
        {
            _key = DataFinder.FindVehicle("OC.ChromeStyleID > 0", having: string.Format("BusinessUnitCode = '{0}'", Settings.DefaultDealerCode));
            _getResponse = _vehicleClient.GetVehicle(_key);
            _trim = _trimClient.GetTrim(_getResponse.Vehicle.Trim.Id, _key.UserName, _key.Password);
            
            _getResponse.Vehicle.Engine = Ref(_trim.AvailableEngines.First(available => available.Id != _getResponse.Vehicle.Engine.Id));
            _getResponse.Vehicle.Drivetrain = Ref(_trim.AvailableDrivetrains.First(available => available.Id != _getResponse.Vehicle.Drivetrain.Id));
            _getResponse.Vehicle.Transmission = Ref(_trim.AvailableTransmissions.First(available => available.Id != _getResponse.Vehicle.Transmission.Id));

            _putResponse = _vehicleClient.PutVehicle(_key, _getResponse.Vehicle, _getResponse);
        }

        [Test]
        public void Should_be_able_to_set_engine()
        {
            Assert.AreEqual(_getResponse.Vehicle.Engine.Id, _putResponse.Vehicle.Engine.Id);
            Assert.AreEqual(_getResponse.Vehicle.Engine.Desc, _putResponse.Vehicle.Engine.Desc);
        }

        [Test]
        public void Should_be_able_to_set_drivetrain()
        {
            Assert.AreEqual(_getResponse.Vehicle.Drivetrain.Id, _putResponse.Vehicle.Drivetrain.Id);
            Assert.AreEqual(_getResponse.Vehicle.Drivetrain.Desc, _putResponse.Vehicle.Drivetrain.Desc);
        }

        [Test]
        public void Should_be_able_to_set_transmission()
        {
            Assert.AreEqual(_getResponse.Vehicle.Transmission.Id, _putResponse.Vehicle.Transmission.Id);
            Assert.AreEqual(_getResponse.Vehicle.Transmission.Desc, _putResponse.Vehicle.Transmission.Desc);
        }

        private static Ref Ref(SpecificEquipment equipment)
        {
            return new Ref { Id = equipment.Id, Desc = equipment.Desc };
        }
    }
}
