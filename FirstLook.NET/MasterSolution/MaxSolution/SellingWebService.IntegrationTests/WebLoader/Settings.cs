﻿using System.Configuration;

namespace SellingWebService.IntegrationTests.WebLoader
{
    public static class Settings
    {
        public static string MaxWebsiteBaseUrl { get { return AppSetting("MaxWebsiteBaseUrl"); } }

        public static string MaxServicesBaseUrl { get { return AppSetting("MaxServicesBaseUrl"); } }

        public static string WebLoaderBaseUrl { get { return AppSetting("WebLoaderBaseUrl"); } }

        // Auth Settings
        public static string StandardPassword { get { return AppSetting("StandardPassword"); } }

        public static string AdminUser { get { return AppSetting("AdminUser"); } }

        public static string LimitedUser { get { return AppSetting("LimitedUser"); } }

        public static string InvalidUsername { get { return AppSetting("InvalidUsername"); } }

        public static string InvalidPassword { get { return AppSetting("InvalidPassword"); } }

        
        // Tweak Settings
        public static string InvalidDealerCode { get { return AppSetting("InvalidDealerCode"); } }

        public static string InvalidVehicle { get { return AppSetting("InvalidVehicle"); } }

        public static string DefaultDealerCode { get { return AppSetting("DefaultDealerCode"); } }


        private static string AppSetting(string name, string defaultVal = "")
        {
            return ConfigurationManager.AppSettings[name] ?? defaultVal;
        }
    }
}