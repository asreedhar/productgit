﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using Max.Selling.Domain;
using NUnit.Framework;

namespace Max.Selling.Tests
{
    [TestFixture]
    public class SchemaTests
    {
        private void Validate(string xmlResource, Version version)
        {
            string xmlDocPath = String.Format("Max.Selling.Tests.TestXml.{0}", xmlResource);
            Validate(typeof (SchemaTests).Assembly.GetManifestResourceStream(xmlDocPath), version);
        }

        private void Validate(Stream xmlStream, Version version)
        {
            using (XmlReader schemaReader = XmlReader.Create(typeof(Resources).Assembly.GetManifestResourceStream(Resources.GetEmbeddedSchemaResource(version))))
            {
                XmlSchemaSet set = new XmlSchemaSet();
                set.Add(null, schemaReader);

                XmlReaderSettings settings = new XmlReaderSettings();
                settings.Schemas = set;
                settings.ValidationType = ValidationType.Schema;

                using (XmlReader xmlReader = XmlReader.Create(xmlStream, settings))
                {

                    XmlDocument doc = new XmlDocument();
                    doc.Load(xmlReader);

                    doc.Validate((sender, args) =>
                    {
                        Assert.AreNotEqual(args.Severity, XmlSeverityType.Error);
                    });

                }
            }
            
        }
    
        
        [Test]
        [ExpectedException(typeof(XmlSchemaValidationException))]
        public void BadDocumentTest()
        {
            using (MemoryStream stream = new MemoryStream())
            using (TextWriter writer = new StreamWriter(stream))
            {
                writer.WriteLine("<salesaccelerator></salesaccelerator>");
                writer.Flush();
                stream.Seek(0, SeekOrigin.Begin);
                Validate(stream, new Version(1, 0));
            }
        }

        [Test]
        public void ValidDocument1Test()
        {
            Validate("ValidDocument1.xml", new Version(1,0));
        }

        [Test]
        public void ValidDocument2Test()
        {
            Validate("ValidDocument2.xml", new Version(1, 0));
        }

        [Test]
        public void ValidDocument3Test()
        {
            Validate("ValidDocument3.xml", new Version(1, 0));
        }

        [Test]
        public void ValidDocument4Test()
        {
            Validate("ValidDocument4.xml", new Version(1, 0));
        }

        [Test]
        public void ValidDocument5Test()
        {
            Validate("ValidDocument5.xml", new Version(1, 0));
        }

        [Test]
        public void ValidDocument6Test()
        {
            Validate("ValidDocument6.xml", new Version(1, 0));
        }


        [Test]
        public void ValidDocument7Test()
        {
            Validate("ValidDocument7.xml", new Version(1, 0));
        }

        [Test]
        public void ValidDocument8Test()
        {
            Validate("ValidDocument8.xml", new Version(1, 0));
        }

        [Test]
        public void ValidDocument9Test()
        {
            Validate("ValidDocument9.xml", new Version(1, 1));
        }

        [Test]
        public void ValidDocument10Test()
        {
            Validate("ValidDocument10.xml", new Version(1, 2));
        }

        [Test]
        public void ValidDocument11Test()
        {
            Validate("ValidDocument11.xml", new Version(1, 1));
        }

    }

   
}
