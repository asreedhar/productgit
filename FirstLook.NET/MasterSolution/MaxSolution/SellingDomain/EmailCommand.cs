﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Data;
using FirstLook.DomainModel.Oltp;
using Max.Selling.Common;
using Max.Selling.Domain.Internal;
using System.Data;

namespace Max.Selling.Domain
{
    public class EmailCommand : XmlCommand
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const string PublishedDocsUrlKey = "PublishedDocsUrl";

        private XmlDocument _entireDocument;

        public ErrorInformation ErrorInfo { get; private set; }

        private int _businessUnitID;
        private string _vin;

        public int Year { get; private set;}
        public string Make { get; private set; }
        public string Model { get; private set; }
        public string Trim { get; private set; }
        private string _stockNumber;

        private string _user;
        public string UserEmail { get; private set; }
        public string UserOfficePhoneNumber { get; private set;}
        public string UserOfficePhoneExtension { get; private set; }
        public string UserMobilePhoneNumber { get; private set; }

        public string DealerName { get; private set; }
        public string DealerPhoneNumber { get; private set; }
        
        public byte[] Pdf { get; private set;}

        public EmailCommand(string user, int businessUnitID, string vin, ErrorInformation errorInformation)
        {
            ErrorInfo = errorInformation;

            _user = user;

            _businessUnitID = businessUnitID;
            _vin = vin;

            Pdf = new byte[0];
        }

        public string Xml
        {
            get
            {
                return _entireDocument.InnerXml;
            }
        }

        private void FetchVehicleInfo()
        {
            VehicleCommand inventoryCommand = new VehicleCommand(_businessUnitID, _vin);
            DoRun(inventoryCommand);
            ErrorInfo = ErrorInfo.ProcessNewError(inventoryCommand.ErrorInfo);

            if (!ErrorInfo.HasError)
            {
                try
                {
                    Owner owner = Owner.GetOwner(_businessUnitID);
                    string vehicleHandle = string.Format("1{0}", inventoryCommand.InventoryID);

                    using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection("Market"))
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                        {
                            command.CommandText = "Pricing.VehicleInformation#Inventory#Fetch";
                            command.CommandType = CommandType.StoredProcedure;

                            command.AddParameterWithValue("OwnerHandle", DbType.String, false, owner.Handle);
                            command.AddParameterWithValue("VehicleHandle", DbType.String, false, vehicleHandle);

                            using (IDataReader reader = command.ExecuteReader())
                            {
                                if (reader.Read())
                                {
                                    _stockNumber = reader.GetString(reader.GetOrdinal("StockNumber"));
                                    Year = reader.GetInt32(reader.GetOrdinal("ModelYear"));
                                    Make = reader.GetString(reader.GetOrdinal("Make"));
                                    Model = reader.GetString(reader.GetOrdinal("Line"));
                                    Trim = reader.GetString(reader.GetOrdinal("Series"));
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Log.Error(string.Format("Failed to get vehicle info for BusinessUnitID = {0}, VIN = {1}", _businessUnitID, _vin), e);
                    ErrorInfo = ErrorInfo.ProcessNewError(new ErrorInformation(e));
                }
            }
        }

        private void FetchContactInfo()
        {
            try
            {
                using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection("IMT"))
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.CommandText = "dbo.MemberContactInfo#Fetch";
                        command.CommandType = CommandType.StoredProcedure;

                        command.AddParameterWithValue("userName", DbType.String, false, _user);
                        command.AddParameterWithValue("businessUnitID", DbType.String, false, _businessUnitID);

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                UserEmail = reader.GetString(reader.GetOrdinal("EmailAddress"));
                                if(UserEmail == string.Empty)
                                    ErrorInfo = ErrorInfo.ProcessNewError(new ErrorInformation(string.Format("No email address found for user {0}", _user)));
                                
                                UserOfficePhoneNumber = reader.GetString(reader.GetOrdinal("OfficePhoneNumber"));
                                UserOfficePhoneExtension = reader.GetString(reader.GetOrdinal("OfficePhoneExtension"));
                                UserMobilePhoneNumber = reader.GetString(reader.GetOrdinal("MobilePhoneNumber"));
                            }

                            if(reader.NextResult() && reader.Read())
                            {
                                DealerName = reader.GetString(reader.GetOrdinal("BusinessUnit"));
                                DealerPhoneNumber = reader.GetString(reader.GetOrdinal("OfficePhone"));   
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Failed to get contact info for BusinessUnitID = {0}, User = {1}", _businessUnitID, _user), e);
                ErrorInfo = ErrorInfo.ProcessNewError(new ErrorInformation(e));
            }
        }

        private void FetchPdf()
        {
            //(3:08 PM) Zac Brown: http://pub.firstlook.biz/go?buc=abc&vin=123&snum=123&doctype=1
            //(3:15 PM) Zac Brown: http://betapub.firstlook.biz/go?buc=abc&vin=123&snum=123&doctype=1
            //http://pub.firstlook.biz/go?buc=WINDYCIT01&vin=1G1ZT538X6F996158&snum=P003442&doctype=
            //(3:16 PM) Zac Brown: \\file01\

            BusinessUnit dealer = BusinessUnitFinder.Instance().Find(_businessUnitID);
            string publishUrl = ConfigurationManager.AppSettings[PublishedDocsUrlKey];
            publishUrl = string.Format("{0}?buc={1}&vin={2}&snum={3}&doctype=1", publishUrl, dealer.BusinessUnitCode, _vin, _stockNumber);
            
            Log.DebugFormat("Requesting published pdf at {0}", publishUrl);
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(publishUrl);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if(response.ContentType == "application/pdf")
                {
                    using(Stream stream = response.GetResponseStream())
                    using(MemoryStream memoryStream = new MemoryStream())
                    {
                        int count = 0;
                        do
                        {
                            var buf = new byte[1024];
                            count = stream.Read(buf, 0, 1024);
                            memoryStream.Write(buf, 0, count);
                        } while (stream.CanRead && count > 0);
                        Pdf = memoryStream.ToArray();
                    }
                }
                else
                {
                    string message = string.Format("The pdf at url {0} was not found", publishUrl);
                    Log.Error(message);
                    throw new InvalidOperationException(message);
                }
            }
            catch (Exception e)
            {
                Log.Error("Exception during request for pdf", e);
                ErrorInfo = ErrorInfo.ProcessNewError(new ErrorInformation(e));
            }
        }

        protected override void Run()
        {
            if(!ErrorInfo.HasError)
                FetchVehicleInfo();
            if (!ErrorInfo.HasError)
                FetchContactInfo();
            if (!ErrorInfo.HasError)
                FetchPdf();
        }

        protected override void DoPostRun()
        {
            _entireDocument = new XmlDocument();
            _entireDocument.AppendChild(CreateErrorNode(_entireDocument, ErrorInfo));
        }
    }
}
