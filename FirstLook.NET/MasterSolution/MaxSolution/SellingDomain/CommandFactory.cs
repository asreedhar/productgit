﻿using System.Security.Principal;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.Common.PhotoServices;

namespace Max.Selling.Domain
{
    public class CommandFactory : ICommandFactory
    {
        public TokenCreationCommand CreateTokenCommand(string userName, string password)
        {
            return userName.IsDemo()
                ? new DemoTokenCreationCommand(userName, password)
                : new TokenCreationCommand(userName, password);
        }

        public FetchDealersCommand CreateDealersCommand(IPrincipal user)
        {
            return user.IsDemo()
                ? new DemoDealersCommand(user.Identity.Name)
                : new FetchDealersCommand(user.Identity.Name);
        }


        public InventoryCommand CreateInventoryCommand(int businessUnitId)
        {
            return new InventoryCommand(businessUnitId);
        }

        public PhotosCommand CreatePhotosCommand(int businessUnitId, string vin)
        {
            return new PhotosCommand(businessUnitId, vin, Registry.Resolve<IPhotoServices>());
        }

        public DealerPhotosCommand CreateDealerPhotosCommand(int businessUnitId)
        {
            return new DealerPhotosCommand(businessUnitId, Registry.Resolve<IPhotoServices>());
        }

    }
}
