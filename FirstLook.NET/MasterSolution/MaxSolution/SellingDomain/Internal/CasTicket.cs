﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Max.Selling.Domain.Internal
{
    internal class CasTicket : ITicket
    {
        public CasTicket()
            : this(false, string.Empty, string.Empty)
        {
        }

        public CasTicket(bool isAuthenticated, string tsUserName, string casTgc)
        {
            IsAuthenticated = isAuthenticated;
            UserName = tsUserName;
            CasTgc = casTgc;
        }

        public bool IsAuthenticated { get; private set; }
        public string UserName { get; private set; }
        public string CasTgc { get; private set; }
    }
}
