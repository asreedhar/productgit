﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Max.Selling.Domain.Internal
{
    internal class LoginEntry
    {
        public LoginEntry(string userName, DateTime expiration)
        {
            User = userName;
            Expiration = expiration.ToUniversalTime();
        }

        public string User { get; private set; }
        public DateTime Expiration { get; private set; }

        public bool Expired
        {
            get
            {
                return Expiration < DateTime.Now.ToUniversalTime();
            }
        }

        public static LoginEntry FromToken(string token)
        {
            byte[] bytes = HttpServerUtility.UrlTokenDecode(token);
            token = Encoding.ASCII.GetString(bytes);
            string[] tokens = token.Split(':');
            return new LoginEntry(tokens[0], DateTime.FromFileTimeUtc(long.Parse(tokens[1])));
        }

        public string ToToken()
        {
            string token = string.Format("{0}:{1}", User, Expiration.ToFileTimeUtc());
            byte[] bytes = Encoding.ASCII.GetBytes(token);
            token = HttpServerUtility.UrlTokenEncode(bytes);
            return token;
        }
    }
}
