﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;
using HtmlAgilityPack;
using FirstLook.Common.Security.CentralAuthenticationService.Client.Configuration;
using System.Configuration;

namespace Max.Selling.Domain.Internal
{
    internal class CasLogin : ICasLogin
    {
        private static readonly Regex JSessionParser = new Regex(@"JSESSIONID=([^;]*)");
        private static readonly Regex TsUsernameParser = new Regex(@"ts_username=([^;]*)");
        private static readonly Regex CasTgcParser = new Regex(@"CASTGC=([^;]*)");

        private CentralAuthenticationServiceConfigurationSection _casConfigSection;

        public CasLogin()
        {
            _casConfigSection = (CentralAuthenticationServiceConfigurationSection)
                ConfigurationManager.GetSection("centralAuthenticationService");            
        }

        private RequestData GetRequestData(string userName, string password)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(_casConfigSection.LoginUrl);
            ServicePointManager.ServerCertificateValidationCallback = delegate { return _casConfigSection.IgnoreSslErrors; };
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            Match match = JSessionParser.Match(response.Headers["Set-Cookie"]);
            string jSessionID = match.Groups[1].Value;

            HtmlDocument doc = new HtmlDocument();
            doc.Load(response.GetResponseStream());
            var node = doc.DocumentNode.SelectSingleNode("//input[@name = 'lt']");
            string ltValue = node.Attributes["value"].Value;

            string postedPairs = string.Format(
                    "username={0}&password={1}&lt={2}&_currentStateId=viewLoginForm&_eventId=submit",
                    userName, password, ltValue);

            return new RequestData(jSessionID, postedPairs);
        }


        private CookieCollection GetCookies(RequestData data)
        {
            var uri = new Uri(_casConfigSection.LoginUrl, UriKind.Absolute); 
            CookieCollection collection = new CookieCollection();
            Cookie cookie = new Cookie("JSESSIONID", data.JSessionID, "/cas", uri.Host);
            collection.Add(cookie);
            return collection;
        }


        public ITicket FetchTicket(string userName, string password)
        {
            var requestData = GetRequestData(userName, password);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(_casConfigSection.LoginUrl);
            request.CookieContainer = new CookieContainer();
            request.CookieContainer.Add(GetCookies(requestData));
            request.AllowAutoRedirect = false;
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            ServicePointManager.ServerCertificateValidationCallback = delegate { return _casConfigSection.IgnoreSslErrors; };

            byte[] requestPostBuffer = Encoding.ASCII.GetBytes(requestData.PostedNamePairs);
            request.ContentLength = requestPostBuffer.Length;

            Stream requestStream = request.GetRequestStream();
            requestStream.Write(requestPostBuffer, 0, requestPostBuffer.Length);
            requestStream.Close();

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            CasTicket ticket;
            if (response.Headers.AllKeys.Contains("Set-Cookie")) //login succeeded
            {

                ticket = new CasTicket(true,
                    TsUsernameParser.Match(response.Headers["Set-Cookie"]).Groups[1].Value,
                    CasTgcParser.Match(response.Headers["Set-Cookie"]).Groups[1].Value);
            }
            else
            {
                ticket = new CasTicket();
            }

            return ticket;
        }

        private class RequestData
        {
            public RequestData(string jSessionId, string postedNamePairs)
            {
                JSessionID = jSessionId;
                PostedNamePairs = postedNamePairs;
            }

            public string JSessionID { get; private set; }
            public string PostedNamePairs { get; private set; }
        }

    }
}
