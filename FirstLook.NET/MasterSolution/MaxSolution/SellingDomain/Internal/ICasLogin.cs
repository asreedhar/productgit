﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Max.Selling.Domain.Internal
{
    internal interface ICasLogin
    {
        ITicket FetchTicket(string userName, string password);
    }
}
