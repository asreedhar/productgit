﻿using System.Configuration;
using CASWrapper;

namespace Max.Selling.Domain.Internal
{
    internal class CasServiceLogin : ICasLogin
    {
        private static readonly string CasServiceUrlKey = "CasWebServiceUrl";

        public ITicket FetchTicket(string userName, string password)
        {
            string url = ConfigurationManager.AppSettings[CasServiceUrlKey];
            CasServiceWrapper wrapper = new CasServiceWrapper(url);
            bool authenticated;
            string ticket = wrapper.CreateTicket(userName, password, out authenticated);
            ITicket casTicket;
            if(authenticated)
                casTicket = new CasTicket(true, userName, ticket);
            else
                casTicket = new CasTicket();

            return casTicket;
        }
    }
}
