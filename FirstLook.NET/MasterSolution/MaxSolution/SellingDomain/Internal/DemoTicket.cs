﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Max.Selling.Domain.Internal
{
    internal class DemoTicket : ITicket
    {
        public DemoTicket()
            : this(false, string.Empty)
        {
        }

        public DemoTicket(bool isAuthenticated, string tsUserName)
        {
            IsAuthenticated = isAuthenticated;
            UserName = tsUserName;
        }

        public bool IsAuthenticated { get; private set; }
        public string UserName { get; private set; }

    }
}
