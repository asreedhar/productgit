﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FirstLook.Common.Data;
using FirstLook.DomainModel.Oltp;

namespace Max.Selling.Domain.Internal
{
    internal class InventoryDataAccess : IInventoryDataAccess
    {
        private const string InventoryDatabase = "IMT";

        public IList<string> FetchDocs(int businessUnitId)
        {
            IList<string> xmlDocs = new List<string>();
            Owner owner = Owner.GetOwner(businessUnitId);

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand((connection.CreateCommand())))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Marketing.InventoryDocuments#FetchByOwnerHandle";
                    command.AddParameterWithValue("ownerHandle", DbType.String, false, owner.Handle);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            xmlDocs.Add(reader.GetString(reader.GetOrdinal("DocumentXml")));
                        }
                    }
                }
            }

            return xmlDocs;
        }

    }
}
