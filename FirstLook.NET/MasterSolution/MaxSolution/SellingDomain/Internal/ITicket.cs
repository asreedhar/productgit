﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Max.Selling.Domain.Internal
{
    internal interface ITicket
    {
        bool IsAuthenticated { get;}
        string UserName { get;}
    }
}
