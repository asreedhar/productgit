﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Max.Selling.Domain.Configuration
{
    public class DealerConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("dealerid", IsRequired = true)]
        public int DealerId
        {
            get { return (int)this["dealerid"]; }
            set { this["dealerid"] = value; }
        }
    }
}
