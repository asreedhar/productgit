﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Max.Selling.Domain.Configuration
{
    public class DealerConfigurationElementCollection : ConfigurationElementCollection
    {
        public DealerConfigurationElement this[int index]
        {
            get
            {
                return BaseGet(index) as DealerConfigurationElement;
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new DealerConfigurationElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((DealerConfigurationElement)element).DealerId;
        }

        protected override string ElementName
        {
            get { return "dealer"; }
        }
    }
}
