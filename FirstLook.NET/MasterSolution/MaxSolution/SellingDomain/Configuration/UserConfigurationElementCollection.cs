﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Max.Selling.Domain.Configuration
{
    public class UserConfigurationElementCollection : ConfigurationElementCollection
    {
        public UserConfigurationElement this[int index]
        {
            get
            {
                return BaseGet(index) as UserConfigurationElement;
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new UserConfigurationElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((UserConfigurationElement)element).Name;
        }

        protected override string ElementName
        {
            get { return "user"; }
        }
    }
}
