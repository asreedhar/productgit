﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.DomainModel.Oltp;

namespace Max.Selling.Domain.Configuration
{
    public interface IDemoDealerConfigurationSection
    {
        UserConfigurationElement GetUserConfigurationElement(string userName);
        bool IsDemoUser(string userName);
        IList<BusinessUnit> GetBusinessUnitFromUser(string userName);
    }
}
