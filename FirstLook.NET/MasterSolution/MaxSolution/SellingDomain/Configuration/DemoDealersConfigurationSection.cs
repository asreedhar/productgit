﻿using System.Collections.Generic;
using System.Configuration;
using FirstLook.DomainModel.Oltp;

namespace Max.Selling.Domain.Configuration
{
    public class DemoDealersConfigurationSection : ConfigurationSection, IDemoDealerConfigurationSection
    {
        private const string ConfigSectionName = "demodealers";

        private DemoDealersConfigurationSection()
        {
        }

        [ConfigurationProperty("entry", IsDefaultCollection = true)]
        public EntryConfigurationElement Entries
        {
            get { return (EntryConfigurationElement)this["entry"]; }
        }

        public static IDemoDealerConfigurationSection Create()
        {
            return ConfigurationManager.GetSection(ConfigSectionName) as DemoDealersConfigurationSection;
        }


        public UserConfigurationElement GetUserConfigurationElement(string userName)
        {
            UserConfigurationElement userConfiguration = null;
            for (int x = 0; x < Entries.Users.Count; x++)
            {
                if (userName == Entries.Users[x].Name)
                {
                    userConfiguration = Entries.Users[x];
                    break;
                }
            }

            return userConfiguration;
        }
      
        public bool IsDemoUser(string userName)
        {
            for (int x = 0; x < Entries.Users.Count; x++)
            {
                if (userName == Entries.Users[x].Name)
                    return true;
            }

            return false;
        }

        public IList<BusinessUnit> GetBusinessUnitFromUser(string userName)
        {
            var list = new List<BusinessUnit>();
            for (int x = 0; x < Entries.Dealers.Count; x++)
            {
                int businessUnit = Entries.Dealers[x].DealerId;
                list.Add(BusinessUnitFinder.Instance().Find(businessUnit));
            }

            return list;
        }

    }
}
