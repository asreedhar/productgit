﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Max.Selling.Domain.Configuration
{
    public class EntryConfigurationElement : ConfigurationElement
    {

        [ConfigurationProperty("user", IsDefaultCollection = true)]
        public UserConfigurationElementCollection Users
        {
            get
            {
                return (UserConfigurationElementCollection)this["user"];
            }
        }

        [ConfigurationProperty("dealer")]
        public DealerConfigurationElementCollection Dealers
        {
            get
            {
                return (DealerConfigurationElementCollection)this["dealer"];
            }
        }
    }
}
