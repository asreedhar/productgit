﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Principal;
using System.Text;
using FirstLook.Common.Security.CentralAuthenticationService.Client.Configuration;
using Max.Selling.Domain.Configuration;

namespace Max.Selling.Domain
{
    public static class Roles
    {
        public const string Demo = "Demo";

        public static bool IsDemo(this string userName)
        {
            IDemoDealerConfigurationSection demoSection = DemoDealersConfigurationSection.Create();
            return demoSection.IsDemoUser(userName);
        }

        public static bool IsDemo(this IPrincipal principal)
        {
            return principal.IsInRole(Demo);
        }
    }
}
