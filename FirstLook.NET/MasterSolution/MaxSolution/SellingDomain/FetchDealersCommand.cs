﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using FirstLook.DomainModel.Oltp;
using Max.Selling.Common;
using Max.Selling.Domain.Internal;

namespace Max.Selling.Domain
{
    public class FetchDealersCommand : XmlCommand
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Member _member;
        private XmlDocument _entireDocument;

        public FetchDealersCommand(string user)
        {
            ErrorInfo = ErrorInformation.NoError;
            User = user;
        }

        public ErrorInformation ErrorInfo { get; private set; }
        protected string User { get; private set;}

        public string Xml
        {
            get
            {
                return _entireDocument.InnerXml;
            }
        }

        protected override void Run()
        {
            try
            {
                _member = MemberFacade.FindByUserName(User);
            }
            catch (Exception e)
            {
                Log.Error("Failed to get member information", e);
                ErrorInfo = new ErrorInformation(e);
            }
        }

        protected XmlNode CreateItem(XmlDocument doc, BusinessUnit businessUnit)
        {
            var item = doc.CreateElement("dealer");
            var name = doc.CreateElement("name");
            name.InnerText = businessUnit.Name;
            item.AppendChild(name);

            var idElement = doc.CreateElement("businessunitid");
            idElement.InnerText = businessUnit.Id.ToString();
            item.AppendChild(idElement);

            return item;
        }

        protected virtual void CreateDealerItems(XmlDocument entireDocument, XmlNode rootNode)
        {
            if (_member != null)
            {
                Log.DebugFormat("{0} associated with {1} dealers", _member.Dealers.Count, User);

                var dealers = _member.Dealers;
                foreach (var businessUnit in dealers)
                    rootNode.AppendChild(CreateItem(_entireDocument, businessUnit));
            }
        }

        protected override void DoPostRun()
        {
            _entireDocument = new XmlDocument();
            XmlNode rootNode = _entireDocument.CreateElement("dealers");

            var userAttribute = _entireDocument.CreateAttribute("username");
            userAttribute.Value = User;
            rootNode.Attributes.Append(userAttribute);

            _entireDocument.AppendChild(rootNode);
            rootNode.AppendChild(CreateErrorNode(_entireDocument, ErrorInfo));

            CreateDealerItems(_entireDocument, rootNode);
        }

    }
}
