﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;

namespace Max.Selling.Domain
{
    public interface ICommandFactory
    {
        TokenCreationCommand CreateTokenCommand(string userName, string password);
        FetchDealersCommand CreateDealersCommand(IPrincipal user);
        InventoryCommand CreateInventoryCommand(int businessUnitId);
        PhotosCommand CreatePhotosCommand(int businessUnitId, string vin);
        DealerPhotosCommand CreateDealerPhotosCommand(int businessUnitId);
    }
}
