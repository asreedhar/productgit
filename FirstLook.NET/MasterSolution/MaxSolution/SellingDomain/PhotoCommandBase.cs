﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace Max.Selling.Domain
{
    public abstract class PhotoCommandBase : XmlCommand
    {
        private const string MerchandisingPhotosDomainKey = "MerchandisingPhotosDomain";
        private static readonly Regex MerchandisingPhotosMatch = new Regex(@"merchandising_photos", RegexOptions.IgnoreCase);

        private static string GetUrl(IPhotoItem photoItem)
        {
            if (MerchandisingPhotosMatch.IsMatch(photoItem.ImageUrl) && !String.IsNullOrEmpty(ConfigurationManager.AppSettings[MerchandisingPhotosDomainKey]))
                return string.Format("{0}{1}", ConfigurationManager.AppSettings[MerchandisingPhotosDomainKey], photoItem.ImageUrl);

            return photoItem.ImageUrl;
        }

        protected XmlNode CreateItem(XmlDocument doc, IPhotoItem photoItem)
        {
            var item = doc.CreateElement("photo");
            var attribute = doc.CreateAttribute("rank");
            attribute.Value = photoItem.ImageSequenceNumber.ToString();
            item.InnerText = GetUrl(photoItem);
            item.Attributes.Append(attribute);

            return item;
        }
    }
}
