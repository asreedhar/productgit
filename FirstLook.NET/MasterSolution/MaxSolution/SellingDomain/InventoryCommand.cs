﻿using System;
using System.Collections.Generic;
using System.Xml;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Data;
using System.Data;
using FirstLook.DomainModel.Oltp;
using Max.Selling.Common;
using Max.Selling.Domain.Internal;

namespace Max.Selling.Domain
{
    public class InventoryCommand : XmlCommand
    {
        private IInventoryDataAccess _dataAcess;

        private IList<string> _xmlDocs;
        private XmlDocument _entireDocument;
        private ErrorInformation _errorInfo;
        private int _businessUnitID;

        public InventoryCommand(int businessUnitID)
            :this(businessUnitID, new InventoryDataAccess())
        {
        }

        public InventoryCommand(int businessUnitID, IInventoryDataAccess dataAccess)
        {
            _dataAcess = dataAccess;
            _xmlDocs = new List<string>();
            _businessUnitID = businessUnitID;
        }

        public string InventoryXml
        {
            get
            {
                return _entireDocument.InnerXml;
            }
        }

        protected override void Run()
        {
            _errorInfo = ErrorInformation.NoError;

            try
            {
                _xmlDocs = _dataAcess.FetchDocs(_businessUnitID);
            }
            catch (Exception e)
            {
                _errorInfo = new ErrorInformation(e);
            }
        }

        private XmlNode CreateItem(XmlDocument doc, string xml)
        {
            var item = doc.CreateElement("item");

            XmlDocument xmlFragment = new XmlDocument();
            xmlFragment.LoadXml(xml);
            item.AppendChild(doc.ImportNode(xmlFragment.FirstChild, true));

            return item;
        }

        protected override void DoPostRun()
        {
            _entireDocument = new XmlDocument();
            XmlNode rootNode = _entireDocument.CreateElement("inventory");
            _entireDocument.AppendChild(rootNode);
            rootNode.AppendChild(CreateErrorNode(_entireDocument, _errorInfo));

            foreach (string xml in _xmlDocs)
                rootNode.AppendChild(CreateItem(_entireDocument, xml));
        }

    }
}
