﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Max.Selling.Domain
{
    public static class Resources
    {
        public static string GetEmbeddedSchemaResource(Version version)
        {
            return string.Format("Max.Selling.Domain.DocumentSchemas._{0}._{1}.InventoryDocument.xsd",
                                  version.Major, version.Minor);
        }
    }
}
