﻿using System;
using System.Text;
using System.Web.Security;
using System.Web;
using Max.Selling.Domain.Internal;

namespace Max.Selling.Domain
{
    public class SellingTicket
    {
        private static readonly TimeSpan ExpireTime = TimeSpan.FromHours(12);
        
        private LoginEntry _entry;

        public static string CreateTicket(string userName)
        {
            LoginEntry entry = new LoginEntry(userName, DateTime.Now + ExpireTime);
            return entry.ToToken();
        }

        public static SellingTicket FromString(string ticket)
        {
            SellingTicket sellingTicket = new SellingTicket();
            sellingTicket._entry = LoginEntry.FromToken(ticket);

            return sellingTicket;
        }

        public string UserName
        {
            get
            {
                return _entry.User;
            }
        }

        public bool IsExpired
        {
            get
            {
                return _entry.Expired;
            }
        }
       
    }
}
