﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Xml;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.Merchandising.DomainModel.Vehicles;
using Max.Selling.Common;
using Max.Selling.Domain.Internal;
using VehicleDataAccess;
using System.Text.RegularExpressions;

namespace Max.Selling.Domain
{
    public class PhotosCommand : PhotoCommandBase
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private ErrorInformation _errorInfo;

        private XmlDocument _entireDocument;
        private IList<IPhotoItem> _photos;
        private int _businessUnitID;
        private string _vin;
        private IPhotoServices _photoService;

        public PhotosCommand(int businessUnitID, string vin, IPhotoServices service)
        {
            _businessUnitID = businessUnitID;
            _vin = vin;
            _photos = new List<IPhotoItem>();
            _photoService = service;
        }

        protected override void Run()
        {
            _errorInfo = ErrorInformation.NoError;

            if (!_errorInfo.HasError)
            {
                try
                {
                    IVehiclePhotosResult result = _photoService.GetPhotoUrlsByVin(_businessUnitID, _vin, TimeSpan.FromSeconds(15));
                    int sequenceNumber = 1;
                    foreach (string url in result.PhotoUrls)
                    {
                        var photo = new Photo() { ImageSequenceNumber = sequenceNumber, ImageUrl = url, Vin = _vin };
                        _photos.Add(photo);
                        sequenceNumber++;
                    }
                }
                catch (Exception e)
                {
                    Log.Error(string.Format("Failed to get photos for BusinessUnitID = {0}, VIN = {1}", _businessUnitID, _vin), e);
                    _errorInfo = new ErrorInformation(e);
                }
            }
        }

        protected override void DoPostRun()
        {
            _entireDocument = new XmlDocument();
            XmlNode rootNode = _entireDocument.CreateElement("photos");

            var businessUnitIdAttribute = _entireDocument.CreateAttribute("businessunitid");
            var vinAttribute = _entireDocument.CreateAttribute("vin");
            businessUnitIdAttribute.Value = _businessUnitID.ToString();
            vinAttribute.Value = _vin;

            rootNode.Attributes.Append(businessUnitIdAttribute);
            rootNode.Attributes.Append(vinAttribute);

            _entireDocument.AppendChild(rootNode);
            rootNode.AppendChild(CreateErrorNode(_entireDocument, _errorInfo));

            Log.DebugFormat("Returning {0} photos", _photos.Count);
            foreach (IPhotoItem pic in _photos)
                rootNode.AppendChild(CreateItem(_entireDocument, pic));
        }


        public string Xml
        {
            get
            {
                return _entireDocument.InnerXml;
            }
        }

        private class Photo : IPhotoItem
        {
            public int ImageSequenceNumber { get; set; }
            public string ImageUrl { get; set; }
            public string Vin { get; set; }
        }
    }
}
