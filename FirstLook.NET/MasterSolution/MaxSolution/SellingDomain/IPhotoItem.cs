﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Max.Selling.Domain
{
    public interface IPhotoItem
    {
        int ImageSequenceNumber { get; set;}
        string ImageUrl { get; set;}
        string Vin { get; set;}
    }
}
