﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Max.Selling.Domain
{
    public interface IInventoryDataAccess
    {
        IList<string> FetchDocs(int businessUnitId);
    }
}
