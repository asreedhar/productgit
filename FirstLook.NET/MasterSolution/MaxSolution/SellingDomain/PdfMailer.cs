﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Net.Mail;
using Max.Selling.Common;

namespace Max.Selling.Domain
{
    public class PdfMailer
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const string FromAddress = "no-reply@firstlook.biz";
        
        public static ErrorInformation SendMail(string to, string replyTo, string subject, string body, string friendlyFrom, byte[] pdf)
        {
            ErrorInformation errorInfo = ErrorInformation.NoError;

            MailMessage message = new MailMessage();

            message.From = new MailAddress(FromAddress, friendlyFrom);
            message.ReplyTo = new MailAddress(replyTo);
            message.CC.Add(replyTo);

            message.To.Add(new MailAddress(to));

            MemoryStream pdfStream = new MemoryStream(pdf);
            var attachment = new Attachment(pdfStream, MediaTypeNames.Application.Pdf);
            attachment.Name = "Vehicle.pdf";
            message.Attachments.Add(attachment);

            message.Subject = subject;
            message.Body = body;

            try
            {
                SmtpClient client = new SmtpClient();
                client.Send(message);
            }
            catch (Exception e)
            {
                Log.Error("Error sending email", e);
                errorInfo = errorInfo.ProcessNewError(new ErrorInformation(e));
            }

            return errorInfo;
        }
    }
}
