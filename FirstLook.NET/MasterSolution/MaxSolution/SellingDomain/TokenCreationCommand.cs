﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Max.Selling.Common;
using Max.Selling.Domain.Internal;

namespace Max.Selling.Domain
{
    public class TokenCreationCommand : XmlCommand
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string _userName;
        private string _password;

        private XmlDocument _entireDocument;

        private ITicket _ticket;
        
        public TokenCreationCommand(string userName, string password)
        {
            _userName = userName;
            _password = password;
        }

        public ErrorInformation ErrorInfo { get; private set; }

        public string Xml
        {
            get
            {
                return _entireDocument.InnerXml;
            }
        }

        protected override void Run()
        {
            ErrorInfo = ErrorInformation.NoError;

            try
            {
                _ticket = GetTicket(_userName, _password);
            }
            catch (Exception e)
            {
                Log.Error("Failed to create CAS Token");
                ErrorInfo = new ErrorInformation(e);
            }
        }

        internal virtual ITicket GetTicket(string userName, string password)
        {
            ICasLogin login = new CasServiceLogin();
            return login.FetchTicket(userName, password);
        }
       
        protected override void DoPostRun()
        {
            _entireDocument = new XmlDocument();
            XmlNode rootNode = _entireDocument.CreateElement("token");

            var tokenValueAttribute = _entireDocument.CreateAttribute("value");
            
            if(_ticket != null &&_ticket.IsAuthenticated)
            {
                tokenValueAttribute.Value = SellingTicket.CreateTicket(_ticket.UserName);
            }
            else
            {
                tokenValueAttribute.Value = string.Empty;
                ErrorInfo = ErrorInfo.ProcessNewError(new ErrorInformation(ErrorCode.GeneralError, "Invalid Credentials"));
            }
            Log.DebugFormat("Got Selling token: {0}", tokenValueAttribute.Value);

            rootNode.Attributes.Append(tokenValueAttribute);
            _entireDocument.AppendChild(rootNode);
            rootNode.AppendChild(CreateErrorNode(_entireDocument, ErrorInfo));
        }


    }
}
