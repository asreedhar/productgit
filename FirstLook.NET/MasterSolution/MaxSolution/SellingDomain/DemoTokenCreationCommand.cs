﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using Max.Selling.Domain.Configuration;
using Max.Selling.Domain.Internal;

namespace Max.Selling.Domain
{
    public class DemoTokenCreationCommand : TokenCreationCommand
    {
        private IDemoDealerConfigurationSection _dealerSection;

        public DemoTokenCreationCommand(string userName, string password)
            : this(userName, password, DemoDealersConfigurationSection.Create())
        {
        }

        public DemoTokenCreationCommand(string userName, string password, IDemoDealerConfigurationSection dealerSection)
            : base(userName, password)
        {
            _dealerSection = dealerSection;
        }

        internal override ITicket GetTicket(string userName, string password)
        {
            UserConfigurationElement userConfiguration = _dealerSection.GetUserConfigurationElement(userName);
           
            DemoTicket ticket = new DemoTicket();
            if(userConfiguration != null && password == userConfiguration.Password)
                ticket = new DemoTicket(true, userName);

            return ticket;
        }
    }
}
