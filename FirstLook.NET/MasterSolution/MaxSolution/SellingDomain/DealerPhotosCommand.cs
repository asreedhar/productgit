﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Vehicles;
using Max.Selling.Common;
using Max.Selling.Domain;


namespace Max.Selling.Domain
{
    public class DealerPhotosCommand : PhotoCommandBase
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private ErrorInformation _errorInfo;

        private int _businessUnitID;
        private IList<IPhotoItem> _photos;
        private XmlDocument _entireDocument;
        private IPhotoServices _photoService;

        public DealerPhotosCommand(int businessUnitID, IPhotoServices service)
        {
            _businessUnitID = businessUnitID;
            _photos = new List<IPhotoItem>();
            _photoService = service;
        }

        public string Xml
        {
            get
            {
                return _entireDocument.InnerXml;
            }
        }

        protected override void Run()
        {
            _errorInfo = ErrorInformation.NoError;

            try
            {
                InventoryCommand command = new InventoryCommand(_businessUnitID);
                DoRun(command); 

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(command.InventoryXml);
                var vinNodes = new List<XmlNode>();
                var nodeList = doc.SelectNodes("//item/salesaccelerator/vehicleyearmodel/vehiclesummary/vin");
                if (nodeList != null)
                    vinNodes = nodeList.Cast<XmlNode>().ToList(); //doing this to get link to work. Must implement generic interfaces
                
                IVehiclesPhotosResult result = _photoService.GetPhotoUrlsByBusinessUnit(_businessUnitID, TimeSpan.FromSeconds(15));
                foreach(string key in result.PhotosByVin.Keys)
                {
                    if (!vinNodes.Exists(x => string.Compare(x.InnerText, key, true) == 0))
                        continue;

                    int sequenceNumber = 1;
                    foreach(string url in result.PhotosByVin[key].PhotoUrls)
                    {
                        var photo = new Photo(){ ImageSequenceNumber = sequenceNumber, ImageUrl = url, Vin = result.PhotosByVin[key].Vin };
                        _photos.Add(photo);
                        sequenceNumber++;
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Failed to get Dealer photos for BusinessUnitID = {0}", _businessUnitID), e);
                _errorInfo = new ErrorInformation(e);
            }
            
        }

        private XmlNode CreatePhotoElements(ref int index, string vin)
        {
            XmlNode rootNode = _entireDocument.CreateElement("photos");

            var businessUnitIdAttribute = _entireDocument.CreateAttribute("businessunitid");
            var vinAttribute = _entireDocument.CreateAttribute("vin");
            businessUnitIdAttribute.Value = _businessUnitID.ToString();
            vinAttribute.Value = vin;

            rootNode.Attributes.Append(businessUnitIdAttribute);
            rootNode.Attributes.Append(vinAttribute);

            for (; index < _photos.Count; index++)
            {
                if (_photos[index].Vin != vin)
                    break;

                rootNode.AppendChild(CreateItem(_entireDocument, _photos[index]));
            }

            return rootNode;
        }


        protected override void DoPostRun()
        {
            _entireDocument = new XmlDocument();

            XmlNode rootNode = _entireDocument.CreateElement("dealerphotos");
            _entireDocument.AppendChild(rootNode);
            rootNode.AppendChild(CreateErrorNode(_entireDocument, _errorInfo));

            for (int x = 0; x < _photos.Count;)
            {
                rootNode.AppendChild(CreatePhotoElements(ref x, _photos[x].Vin));
            }
        }

        private class Photo : IPhotoItem
        {
            public int ImageSequenceNumber { get; set;}
            public string ImageUrl { get; set;}
            public string Vin { get; set;}
        }
    }
}
