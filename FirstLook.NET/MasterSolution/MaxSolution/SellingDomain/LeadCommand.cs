﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Data;
using FirstLook.DomainModel.Oltp;
using Max.Selling.Common;
using Max.Selling.Domain.Internal;

namespace Max.Selling.Domain
{
    public class LeadCommand : XmlCommand
    {
        
        private XmlDocument _entireDocument;
        private int _businessUnitID;
        private string _vin;

        public LeadCommand(int businessUnitID, string vin)
        {
            _businessUnitID = businessUnitID;
            _vin = vin;

            FirstName = string.Empty;
            LastName = string.Empty;
            Email = string.Empty;
            PhoneNumber = string.Empty;
            Address = string.Empty;
            City = string.Empty;
            State = string.Empty;
            ZipCode = string.Empty;
        }

        public ErrorInformation ErrorInfo { get; private set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }

        public string ReturnXml
        {
            get
            {
                return _entireDocument.InnerXml;
            }
        }

        protected override void Run()
        {
            VehicleCommand inventoryCommand = new VehicleCommand(_businessUnitID, _vin);
            DoRun(inventoryCommand);
            ErrorInfo = inventoryCommand.ErrorInfo;

            if (!ErrorInfo.HasError)
            {
                try
                {
                    Owner owner = Owner.GetOwner(_businessUnitID);
                    string vehicleHandle = string.Format("1{0}", inventoryCommand.InventoryID);

                    using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection("IMT"))
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        using (IDataCommand command = new DataCommand((connection.CreateCommand())))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.CommandText = "Marketing.InventoryLeads#Insert";
                            command.AddParameterWithValue("OwnerHandle", DbType.String, false, owner.Handle);
                            command.AddParameterWithValue("vehicleHandle", DbType.String, false, vehicleHandle);
                            
                            command.AddParameterWithValue("FirstName", DbType.String, false, FirstName);
                            command.AddParameterWithValue("LastName", DbType.String, false, LastName);
                            command.AddParameterWithValue("Email", DbType.String, false, Email);
                            command.AddParameterWithValue("PhoneNumber", DbType.String, false, PhoneNumber);
                            command.AddParameterWithValue("Address", DbType.String, false, Address);
                            command.AddParameterWithValue("City", DbType.String, false, City);
                            command.AddParameterWithValue("State", DbType.String, false, State);
                            command.AddParameterWithValue("ZipCode", DbType.String, false, ZipCode);

                            command.ExecuteNonQuery();
                        }
                    }
                }
                catch (Exception e)
                {
                    ErrorInfo = new ErrorInformation(e);
                }
            }
        }


        protected override void DoPostRun()
        {
            _entireDocument = new XmlDocument();
            _entireDocument.AppendChild(CreateErrorNode(_entireDocument, ErrorInfo));
        }
    }
}
