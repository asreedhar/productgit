﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using FirstLook.DomainModel.Oltp;
using Max.Selling.Domain.Configuration;

namespace Max.Selling.Domain
{
    public class DemoDealersCommand : FetchDealersCommand
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private IDemoDealerConfigurationSection _dealerSection;

        public DemoDealersCommand(string user)
            : this(user, DemoDealersConfigurationSection.Create())
        {
        }

        public DemoDealersCommand(string user, IDemoDealerConfigurationSection dealerSection) : base(user)
        {
            _dealerSection = dealerSection;
        }

        protected override void Run()
        {
        }

        protected override void CreateDealerItems(XmlDocument entireDocument, XmlNode rootNode)
        {
            var units = _dealerSection.GetBusinessUnitFromUser(User);
            Log.DebugFormat("{0} associated with {1} dealers", units.Count, User);

            foreach (var businessUnit in units)
                rootNode.AppendChild(CreateItem(entireDocument, businessUnit));
        }

    }
}
