﻿using System.Xml;
using FirstLook.Common.Core.Command;
using Max.Selling.Common;

namespace Max.Selling.Domain
{
    public abstract class XmlCommand : AbstractCommand
    {
        protected XmlCommand()
        {
        }

        internal XmlNode CreateErrorNode(XmlDocument doc, ErrorInformation errorInfo)
        {
            return errorInfo.CreateErrorNode(doc);
        }
    }
}
