<%@ Page Language="C#" MasterPageFile="~/Application/Transform/Dealer/TransformMasterPage.master" AutoEventWireup="true" CodeFile="MappingTransformationsConfiguration.aspx.cs" Inherits="Application_Transform_Dealer_MappingTransformationsConfiguration" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContentPlaceHolder" Runat="Server">
        <title>Configure Custom Transformations</title>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="BodyContentPlaceHolder" Runat="Server">
    <fieldset>
        <legend>Dealer Mappings/Custom Transformations Configuration</legend>
        <p>
            Viewing: <asp:DropDownList AutoPostBack="true" OnSelectedIndexChanged="EnvironmentDropDownList_SelectedIndexChanged" runat="server" ID="EnvironmentDropDownList">
                <asp:ListItem Text="Pre-Production" Value="2" />
                <asp:ListItem Text="Production" Value="1" />
            </asp:DropDownList>
        </p>
        <asp:Panel runat="server" ID="MigrateSettingsPanel">
            <p><asp:LinkButton runat="server" ID="MigrateCopyToProdLinkButton" OnClick="MigrateCopyToProdLinkButton_Click" Text="Copy configuration to production" /></p>
            <p><asp:LinkButton runat="server" ID="MigrateRestoreFromProdLinkButton" OnClick="MigrateRestoreFromProdLinkButton_Click" Text="Restore configuration from production" /></p>
            <br />
        </asp:Panel>
        <asp:BulletedList ID="ErrorList" runat="server" Visible="false" CssClass="ErrorList" />
        <h4><asp:Label runat="server" ID="SavedLabel" Visible="false" Text="Mappings/Transformations succesfully saved." /></h4>
        <h4><asp:Label runat="server" ID="PromoteSavedLabel" Visible="false" Text="Configuration Promotion successful." /></h4>
        <cwc:TabContainer ID="CustomTransformationsTabContainer" runat="server" ActiveTabIndex="0" HasEmptyState="false">
        <cwc:TabPanel ID="TradePurchaseTabPanel" runat="server" Text="Trade/Purchase Logic">
            <p>Select the type of logic used by the dealership to identify Trades and Purchases.</p>
                <csla:CslaDataSource ID="TradePurchaseRadioButtonListDataSource" runat="server"
                    OnSelectObject="TradePurchaseRadioButtonListDataSource_SelectObject"
                    TypeSupportsPaging="false"
                    TypeSupportsSorting="false">
                </csla:CslaDataSource>
                <asp:RadioButtonList AutoPostBack="true" OnSelectedIndexChanged="TradePurchaseRadioButtonList_SelectedIndexChanged" OnDataBound="TradePurchaseRadioButtonList_DataBound" runat="server" DataSourceID="TradePurchaseRadioButtonListDataSource" ID="TradePurchaseRadioButtonList" />
                <asp:Panel runat="server" ID="StockNumberLogicPanel" Visible="false">
                    <csla:CslaDataSource ID="StockNumberLogicRepeaterDataSource" runat="server"
                        OnSelectObject="StockNumberLogicRepeaterDataSource_SelectObject"
                        TypeSupportsPaging="false"
                        TypeSupportsSorting="false">
                    </csla:CslaDataSource>
                    <p>Enter Stock Number details in the form below:</p>
                    
                    <div style="float:left; width:50%">
                        <asp:Repeater runat="server" ID="StockNumberLogicRepeater" DataSourceID="StockNumberLogicRepeaterDataSource" OnItemDataBound="StockNumberLogicRepeater_ItemDataBound" >
                            <HeaderTemplate>
                                <table width="35%">
                                    <tr>
                                        <td width="50%">Trade/Purchase</td>
                                        <td width="30%">Stock Pattern</td>
                                        <td width="20%"><asp:Button runat="server" ID="StockNumberLogicTableAddRowButton" OnClick="StockNumberLogicTableAddRowButton_Click" Text="Add Row" /></td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td width="50%">
                                        <asp:DropDownList runat="server" ID="StockPatternTypeDropDownList">
                                            <asp:ListItem Text="Trade" Value="T" />
                                            <asp:ListItem Text="Purchase" Value="P" />
                                        </asp:DropDownList>
                                    </td>
                                    <td width="30%">
                                        <asp:TextBox runat="server" ID="StockPatternText" />
                                    </td>
                                    <td width="20%"><asp:Button runat="server" ID="StockNumberLogicTableRemoveRowButton" OnClick="StockNumberLogicTableRemoveRowButton_Click" Text="Delete" /></td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                    <div style="float:left; width:50%; margin-top:-50px">
                        <p><b><span style="font-size: 11pt"><span>How to create a Stock Pattern</span></span></b></p>
                        <p>
                            <table border="1" cellpadding="0" cellspacing="0"  style="width: 100%;">
                                <tr style="">
                                    <td><p align="center" ><b><span >Wildcard character</span></b></p></td>
                                    <td><p align="center" ><b><span >Description</span></b></p></td>
                                    <td><p align="center" ><b><span >Example</span></b></p></td>
                                </tr>
                                <tr>
                                    <td><p><span>%</span></p></td>
                                    <td><p><span>Any string of zero or more characters.</span></p></td>
                                    <td><p><span>'%A%' finds any stock number containing an 'A'.</span></p></td>
                                </tr>
                                <tr>
                                    <td><p><span>_ (underscore)</span></p></td>
                                    <td><p><span>Any single character.</span></p></td>
                                    <td><p><span>'_A%' finds all stock numbers with an 'A' as the second character.</span></p></td>
                                </tr>
                                <tr>
                                    <td><p><span>[ ]</span></p></td>
                                    <td ><p><span>Any single character within the specified range ([a-f]) or set ([abcdef]).</span></p></td>
                                    <td><p><span>'[ABC]%' finds all stock numbers starting with 'A' or 'B' or 'C'.</span></p></td>
                                </tr>
                                <tr>
                                    <td><p><span>[^]</span></p></td>
                                    <td><p><span>Any single character not within the specified range ([^a-f]) or set ([^abcdef]).</span></p></td>
                                    <td><p><span>'[^ABC]%' finds all stock numbers not starting with 'A' or 'B' or 'C'.</span></p></td>
                                </tr>
                            </table>
                        </p>
                        <p></p>
                        <p>The pattern description cannot include single quotes.</p>
                        <p>Rules are evaluated in the order listed.  Once a stock number has met a rule, it is no longer evaluated against any other rule.</p>
                    </div>
                </asp:Panel>
        </cwc:TabPanel>
        <cwc:TabPanel ID="UnitCostTabPanel" runat="server" Text="Unit Cost/Pack Configuration">
            <p>Select the way in which the dealership defines their Pack.</p>
            <asp:RadioButtonList OnSelectedIndexChanged="PackDefinitionRadioButtonList_SelectedIndexChanged" AutoPostBack="true" runat="server" ID="PackDefinitionRadioButtonList">
                <asp:ListItem Text="DMS field - PACK_AMT" Value="DMS" />
                <asp:ListItem Text="Dealer Defined" Value="Dealer" />
            </asp:RadioButtonList>
            <asp:Panel runat="server" ID="DealerDefinedAmountPanel" Visible="false">
                <p>Enter dealer defined Pack amount: <asp:TextBox runat="server" ID="DealerDefinedPackAmount" OnTextChanged="DealerDefinedPackAmount_TextChanged" /></p>
            </asp:Panel>
            <asp:Panel runat="server" ID="DealerDefinedPanel">
                <p>
                    <asp:CheckBox runat="server" ID="DealerDefinedDeals_All" Text="All Vehicles" /><br />
                    <asp:CheckBox runat="server" ID="DealerDefinedDeals_Retail" Text="Retail Deals" /><br />
                    <asp:CheckBox runat="server" ID="DealerDefinedDeals_Wholesale" Text="Wholesale Deals" /><br />
                    <asp:CheckBox runat="server" ID="DealerDefinedDeals_TradeIns" Text="Trade-Ins" /><br />
                    <asp:CheckBox runat="server" ID="DealerDefinedDeals_Purchases" Text="Purchases" /><br />
                    <asp:CheckBox runat="server" ID="DealerDefinedDeals_UnitCost" Text="Unit Cost" />
                    <asp:DropDownList runat="server" ID="DealerDefinedDeals_UnitCostOperator">
                        <asp:ListItem Text="&gt;=" Value="GTE" />
                        <asp:ListItem Text="&lt;=" Value="LTE" />
                    </asp:DropDownList>
                    <asp:TextBox runat="server" ID="DealerDefinedDeals_UnitCostThreshold" OnTextChanged="DealerDefinedDeals_UnitCostThreshold_TextChanged" />
                    <br />
                    <asp:CheckBox runat="server" ID="DealerDefinedDeals_AgeInDays" Text="Age in Days" />
                    <asp:DropDownList runat="server" ID="DealerDefinedDeals_AgeInDaysOperator">
                        <asp:ListItem Text="&gt;=" Value="GTE" />
                        <asp:ListItem Text="&lt;=" Value="LTE" />
                    </asp:DropDownList>
                    <asp:TextBox runat="server" ID="DealerDefinedDeals_AgeInDaysThreshold" OnTextChanged="DealerDefinedDeals_AgeInDaysThreshold_TextChanged" />
                    <br />
                </p>
            </asp:Panel>
        </cwc:TabPanel>
        <cwc:TabPanel ID="StandardMappingTabPanel" runat="server" Text="Standard Mappings">
                <p>Select the standard mapping fields specified by the dealership in their DMS Setup Packet (or setup phone call).</p>
                <p>
                    <asp:Label ID="Label1" runat="server" AssociatedControlID="MileageMappingDropDownList" Text="Mileage: " />
                    <csla:CslaDataSource ID="MileageMappingDropDownListDataSource" runat="server"
                        OnSelectObject="MileageMappingDropDownListDataSource_SelectObject"
                        TypeSupportsPaging="false"
                        TypeSupportsSorting="false">
                    </csla:CslaDataSource>
                    <asp:DropDownList OnDataBound="MileageMappingDropDownList_DataBound" ID="MileageMappingDropDownList" DataSourceID="MileageMappingDropDownListDataSource" DataTextField="Name" DataValueField="Id" runat="server" />
                </p>
                <p>
                    <asp:Label ID="Label2" runat="server" AssociatedControlID="ListPriceMappingDropDownList" Text="List Price: " />
                    <csla:CslaDataSource ID="ListPriceMappingDropDownListDataSource" runat="server"
                        OnSelectObject="ListPriceMappingDropDownListDataSource_SelectObject"
                        TypeSupportsPaging="false"
                        TypeSupportsSorting="false">
                    </csla:CslaDataSource>
                    <asp:DropDownList OnDataBound="ListPriceMappingDropDownList_DataBound" ID="ListPriceMappingDropDownList" DataSourceID="ListPriceMappingDropDownListDataSource" DataTextField="Name" DataValueField="Id" runat="server" />
                </p>
                <p>
                    <asp:Label ID="Label3" runat="server" AssociatedControlID="LotPriceMappingDropDownList" Text="Lot Price: " />
                    <csla:CslaDataSource ID="LotPriceMappingDropDownListDataSource" runat="server"
                        OnSelectObject="LotPriceMappingDropDownListDataSource_SelectObject"
                        TypeSupportsPaging="false"
                        TypeSupportsSorting="false">
                    </csla:CslaDataSource>
                    <asp:DropDownList OnDataBound="LotPriceMappingDropDownList_DataBound" ID="LotPriceMappingDropDownList" DataSourceID="LotPriceMappingDropDownListDataSource" DataTextField="Name" DataValueField="Id" runat="server" />
                </p>
                <p>
                    <asp:Label ID="Label4" runat="server" AssociatedControlID="ReceivedDateMappingDropDownList" Text="Received Date: " />
                    <csla:CslaDataSource ID="ReceivedDateMappingDropDownListDataSource" runat="server"
                        OnSelectObject="ReceivedDateMappingDropDownListDataSource_SelectObject"
                        TypeSupportsPaging="false"
                        TypeSupportsSorting="false">
                    </csla:CslaDataSource>
                    <asp:DropDownList OnDataBound="ReceivedDateMappingDropDownList_DataBound" ID="ReceivedDateMappingDropDownList" DataSourceID="ReceivedDateMappingDropDownListDataSource" DataTextField="Name" DataValueField="Id" runat="server" />
                </p>
                
                <br />
                <br />
        </cwc:TabPanel>
    </cwc:TabContainer>
    <p class="controls">
        <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
        <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" />
    </p>
    </fieldset>
</asp:Content>
