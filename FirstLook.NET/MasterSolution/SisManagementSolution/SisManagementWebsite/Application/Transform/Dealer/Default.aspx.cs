using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Csla.Web;
using FirstLook.Sis.Management.DomainModel.Transform.Dealer;

public partial class Application_Transform_Dealer_Default : System.Web.UI.Page
{
    private string SearchTerm
    {
        get { return ViewState["SearchTerm"] == null ? "" : ViewState["SearchTerm"].ToString(); }
        set { ViewState["SearchTerm"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void DealerNameTextBox_TextChanged(object sender, EventArgs e)
    {
        DealerSearch();
    }

    protected void DealerSearchButton_Click(object sender, EventArgs e)
    {
        DealerSearch();
    }

    private void DealerSearch()
    {
        SearchTerm = DealerNameTextBox.Text;
        DealerGridView.PageIndex = 0;
        DealerGridView.DataBind();
    }

    protected void ClearSearchButton_Click(object sender, EventArgs e)
    {
        SearchTerm = "";
        DealerGridView.DataBind();
    }

    protected void DealerGridView_DataBound(object sender, EventArgs e)
    {
        DealerNameTextBox.Text = SearchTerm;
        NoDealerText.Visible = DealerGridView.Rows.Count < 1;
    }

    protected void DealerGridViewDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        e.BusinessObject = DealerMappingTransformationInfoCollection.GetDealers(
            SearchTerm,
            e.SortExpression,
            e.MaximumRows,
            e.StartRowIndex);
    }

    protected void DealerGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ((Label)e.Row.FindControl("TradePurchaseLabel")).Text = ((DealerMappingTransformationInfo)e.Row.DataItem).TradePurchaseDefined ? "OK" : "N/A";
            ((Label)e.Row.FindControl("PackLabel")).Text = ((DealerMappingTransformationInfo)e.Row.DataItem).UnitCostDefined ? "OK" : "N/A";
            ((Label)e.Row.FindControl("StandardMappingsLabel")).Text = ((DealerMappingTransformationInfo)e.Row.DataItem).MappingComplete ? "OK" : "N/A";
        }
    }

    protected void DealerGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Transform"))
        {
            int selectedIndex = Convert.ToInt32(e.CommandArgument);

            GridView grid = sender as GridView;

            if (grid != null)
            {
                int dealerId = (int)grid.DataKeys[selectedIndex].Values[0];
                Response.Redirect(string.Format(CultureInfo.InvariantCulture, "~/Application/Transform/Dealer/MappingTransformationsConfiguration.aspx?DealerId={0}", dealerId), true);
            }
        }
    }
}
