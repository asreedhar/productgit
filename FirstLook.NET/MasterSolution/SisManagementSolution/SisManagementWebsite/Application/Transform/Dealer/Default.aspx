<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Application_Transform_Dealer_Default" MasterPageFile="~/Application/BasicMasterPage.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="TitleContentPlaceHolder" runat="server">
    <title>Mappings/Custom Transformations Dealer Selection</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContentPlaceHolder" Runat="Server">
    <fieldset>
        <legend>Dealer Mappings/Custom Transformations</legend>
        <p>
            Select a dealership to manage their Mappings/Custom Transformations.
        </p>
        <p>
            <asp:Panel ID="DealerSearchPanel" DefaultButton="DealerSearchButton" runat="server">
            <asp:Label ID="DealerNameLabel" runat="server" Text="Dealer Name:" AssociatedControlID="DealerNameTextBox"></asp:Label>
            <asp:TextBox ID="DealerNameTextBox" runat="server" OnTextChanged="DealerNameTextBox_TextChanged"></asp:TextBox>
            <asp:Button ID="DealerSearchButton" runat="server" Text="Search" OnClick="DealerSearchButton_Click" />
            <asp:Button ID="ClearSearchButton" runat="server" Text="Clear" OnClick="ClearSearchButton_Click" />
            </asp:Panel>
        </p>
        <csla:CslaDataSource ID="DealerGridViewDataSource" runat="server"
            OnSelectObject="DealerGridViewDataSource_SelectObject"
            TypeSupportsPaging="true"
            TypeSupportsSorting="true">
        </csla:CslaDataSource>
        <asp:GridView ID="DealerGridView" runat="server"
            AllowPaging="true"
            AllowSorting="true"
            AutoGenerateColumns="false"
            DataKeyNames="Id"
            DataSourceID="DealerGridViewDataSource"
            OnRowCommand="DealerGridView_RowCommand"
            OnRowDataBound="DealerGridView_RowDataBound"
            OnDataBound="DealerGridView_DataBound">
            <Columns>
                <asp:BoundField SortExpression="Name" AccessibleHeaderText="Name" HeaderText="Name" DataField="Name" ReadOnly="true" />
                <asp:BoundField SortExpression="DealerGroupName" AccessibleHeaderText="Dealer Group Name" HeaderText="Dealer Group Name" DataField="DealerGroupName" ReadOnly="true" />
                <asp:BoundField SortExpression="Address" AccessibleHeaderText="Address" HeaderText="Address" DataField="Address" ReadOnly="true" />
                <asp:TemplateField AccessibleHeaderText="Transformations Defined" HeaderText="Transformations Defined">
                    <ItemTemplate>
                        Trade/Purchase: <asp:Label runat="server" ID="TradePurchaseLabel" /><br />
                        Unit Cost: <asp:Label runat="server" ID="PackLabel" /><br />
                        Standard Mappings: <asp:Label runat="server" ID="StandardMappingsLabel" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:ButtonField ButtonType="Button" Text="Transform" CommandName="Transform" ShowHeader="false" />
            </Columns>
        </asp:GridView>
        <p>
            <asp:Label ID="NoDealerText" runat="server" Visible="false">
                No dealerships matched your search criteria.
                Please try changing or removing your search criteria.
            </asp:Label>
        </p>
    </fieldset>
</asp:Content>

