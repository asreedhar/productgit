using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Csla.Validation;
using Csla.Web;
using FirstLook.Sis.Management.DomainModel.Configuration.Adp;
using FirstLook.Sis.Management.DomainModel.Transform.Dealer;
using Environment = FirstLook.Sis.Management.DomainModel.Configuration.Environment;


public partial class Application_Transform_Dealer_MappingTransformationsConfiguration : System.Web.UI.Page
{
    private Dealer _dealer;
    private DealerMappingTransformationSettings mtSettings;

    private Dealer Dealer
    {
        get
        {
            if (_dealer == null)
            {
                string id = Request.QueryString["DealerId"];

                if (string.IsNullOrEmpty(id))
                {
                    throw new ApplicationException("Missing DealerId");
                }
                else
                {
                    _dealer = Dealer.GetDealer(int.Parse(id), Env);
                }
            }

            return _dealer;
        }
    }
    private DealerMappingTransformationSettings MTSettings
    {
        get
        {
            if (mtSettings == null)
            {
                mtSettings = DealerMappingTransformationSettings.GetDealerMappingTransformationSettings(Dealer);
            }
            return mtSettings;
        }
    }
    private List<TradePurchaseTransformationStockNumberLogic> SNLs
    {
        get
        {
            return ViewState["SNL"] as List<TradePurchaseTransformationStockNumberLogic>;
        }
        set
        {
            ViewState["SNL"] = value;
        }
    }
    private Environment Env
    {
        get
        {
            return (Environment)Enum.ToObject(typeof(Environment), int.Parse(EnvironmentDropDownList.SelectedValue));
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        SavedLabel.Visible = false;
        PromoteSavedLabel.Visible = false;

        SaveButton.Visible = Env != Environment.Production;
        CancelButton.Visible = Env != Environment.Production;

        if (!IsPostBack)
        {
            BindDealerUnitCostTransformation();
            SNLs = new List<TradePurchaseTransformationStockNumberLogic>();
            if (MTSettings.DealerTradePurchaseTransformationSettings != null)
            {
                if (MTSettings.DealerTradePurchaseTransformationSettings.TradePurchaseTransformationStockNumberLogicCollection != null)
                {
                    SNLs.AddRange(MTSettings.DealerTradePurchaseTransformationSettings.TradePurchaseTransformationStockNumberLogicCollection);
                }
            }
        }

        Dealer prodDlr = FirstLook.Sis.Management.DomainModel.Configuration.Adp.Dealer.GetDealer(int.Parse(Request.QueryString["DealerId"]), Environment.Production);
        if (DealerTradePurchaseTransformationSettings.GetDealerTradePurchaseTransformationSettings(prodDlr).TradePurchaseTransformationType == 0)
        {
            if (EnvironmentDropDownList.Items.Count > 1)
            {
                EnvironmentDropDownList.Items.RemoveAt(1);
            }
            MigrateRestoreFromProdLinkButton.Visible = false;
        }
        else
        {
            if (EnvironmentDropDownList.Items.Count == 1)
            {
                EnvironmentDropDownList.Items.Add(new ListItem("Production", "1"));
            }
            MigrateRestoreFromProdLinkButton.Visible = true;
        }

    }


    #region Standard Mapping Settings


    protected void MileageMappingDropDownList_DataBound(object sender, EventArgs e)
    {
        if (MTSettings.DealerStandardMappingSettings.MileageMapping != null)
        {
            MileageMappingDropDownList.SelectedValue = MTSettings.DealerStandardMappingSettings.MileageMapping.Id.ToString();
        }
        MileageMappingDropDownList.Items.Insert(0, new ListItem("Select one...", ""));
    }

    protected void ListPriceMappingDropDownList_DataBound(object sender, EventArgs e)
    {
        if (MTSettings.DealerStandardMappingSettings.ListPriceMapping != null)
        {
            ListPriceMappingDropDownList.SelectedValue = MTSettings.DealerStandardMappingSettings.ListPriceMapping.Id.ToString();
        }
        ListPriceMappingDropDownList.Items.Insert(0, new ListItem("Select one...", ""));
    }

    protected void LotPriceMappingDropDownList_DataBound(object sender, EventArgs e)
    {
        if (MTSettings.DealerStandardMappingSettings.LotPriceMapping != null)
        {
            LotPriceMappingDropDownList.SelectedValue = MTSettings.DealerStandardMappingSettings.LotPriceMapping.Id.ToString();
        }
        LotPriceMappingDropDownList.Items.Insert(0, new ListItem("Select one...", ""));
    }

    protected void ReceivedDateMappingDropDownList_DataBound(object sender, EventArgs e)
    {
        if (MTSettings.DealerStandardMappingSettings.ReceivedDateMapping != null)
        {
            ReceivedDateMappingDropDownList.SelectedValue = MTSettings.DealerStandardMappingSettings.ReceivedDateMapping.Id.ToString();
        }
        ReceivedDateMappingDropDownList.Items.Insert(0, new ListItem("Select one...", ""));
    }

    protected void MileageMappingDropDownListDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        e.BusinessObject = MileageMappingCollection.GetMappings();
    }

    protected void ListPriceMappingDropDownListDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        e.BusinessObject = ListPriceMappingCollection.GetMappings();
    }

    protected void LotPriceMappingDropDownListDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        e.BusinessObject = LotPriceMappingCollection.GetMappings();
    }

    protected void ReceivedDateMappingDropDownListDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        e.BusinessObject = ReceivedDateMappingCollection.GetMappings();
    }


    #endregion Standard Mapping Settings

    #region Trade/Purchase Transformation Settings


    protected void TradePurchaseRadioButtonListDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        e.BusinessObject = Enum.GetValues(typeof(TradePurchaseTransformationType));
    }

    protected void TradePurchaseRadioButtonList_DataBound(object sender, EventArgs e)
    {
        if (MTSettings.DealerTradePurchaseTransformationSettings.TradePurchaseTransformationType == 0)
        {
            TradePurchaseRadioButtonList.SelectedValue = TradePurchaseTransformationType.JournalLogic.ToString();
        }
        else
        {
            TradePurchaseRadioButtonList.SelectedValue = MTSettings.DealerTradePurchaseTransformationSettings.TradePurchaseTransformationType.ToString();
        }
        StockNumberLogicPanel.Visible = TradePurchaseRadioButtonList.SelectedValue == TradePurchaseTransformationType.StockNumberLogic.ToString();
    }

    protected void TradePurchaseRadioButtonList_SelectedIndexChanged(object sender, EventArgs e)
    {
        StockNumberLogicPanel.Visible = TradePurchaseRadioButtonList.SelectedValue == TradePurchaseTransformationType.StockNumberLogic.ToString();
    }

    protected void StockNumberLogicRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        TradePurchaseTransformationStockNumberLogic logic = e.Item.DataItem as TradePurchaseTransformationStockNumberLogic;
        if (logic != null)
        {
            ((DropDownList)e.Item.FindControl("StockPatternTypeDropDownList")).SelectedValue = logic.StockPatternType;
            ((TextBox)e.Item.FindControl("StockPatternText")).Text = logic.StockPatternText;
            ((Button)e.Item.FindControl("StockNumberLogicTableRemoveRowButton")).CommandArgument = logic.Id.ToString();
        }
    }

    protected void StockNumberLogicRepeaterDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        SNLs.Sort(
            delegate(TradePurchaseTransformationStockNumberLogic x, TradePurchaseTransformationStockNumberLogic y)
            {
                return x.EvaluationOrder.CompareTo(y.EvaluationOrder);
            });

        e.BusinessObject = SNLs;
    }

    protected void StockNumberLogicTableAddRowButton_Click(object sender, EventArgs e)
    {
        BindSNLs();

        TradePurchaseTransformationStockNumberLogic newone = TradePurchaseTransformationStockNumberLogic.NewTradePurchaseTransformationStockNumberLogic();
        newone.EvaluationOrder = SNLs.Count + 1;
        SNLs.Add(newone);
        StockNumberLogicRepeater.DataBind();
    }

    private void BindSNLs()
    {
        foreach (RepeaterItem item in StockNumberLogicRepeater.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                SNLs[item.ItemIndex].StockPatternType = ((DropDownList)item.FindControl("StockPatternTypeDropDownList")).SelectedValue;
                SNLs[item.ItemIndex].StockPatternText = ((TextBox)item.FindControl("StockPatternText")).Text;
                SNLs[item.ItemIndex].EvaluationOrder = item.ItemIndex + 1;
            }
        }
    }

    private void SetDealerTradePurchaseTransformationStockNumberLogics()
    {
        //bind in memory collection from repeater
        BindSNLs();

        //delete extras if in memory collection is smaller than current settings
        for (int i = MTSettings.DealerTradePurchaseTransformationSettings.TradePurchaseTransformationStockNumberLogicCollection.Count; i > SNLs.Count; i--)
        {
            MTSettings.DealerTradePurchaseTransformationSettings.TradePurchaseTransformationStockNumberLogicCollection.Remove(MTSettings.DealerTradePurchaseTransformationSettings.TradePurchaseTransformationStockNumberLogicCollection[i - 1].Id);
        }

        //set counts of dealer collection and in memory collection equal
        while (MTSettings.DealerTradePurchaseTransformationSettings.TradePurchaseTransformationStockNumberLogicCollection.Count < SNLs.Count)
        {
            MTSettings.DealerTradePurchaseTransformationSettings.TradePurchaseTransformationStockNumberLogicCollection.Assign(TradePurchaseTransformationStockNumberLogic.NewTradePurchaseTransformationStockNumberLogic());
        }

        //sync collections
        for (int i = 0; i < SNLs.Count; i++)
        {
            MTSettings.DealerTradePurchaseTransformationSettings.TradePurchaseTransformationStockNumberLogicCollection[i].StockPatternText = SNLs[i].StockPatternText;
            MTSettings.DealerTradePurchaseTransformationSettings.TradePurchaseTransformationStockNumberLogicCollection[i].StockPatternType = SNLs[i].StockPatternType;
            MTSettings.DealerTradePurchaseTransformationSettings.TradePurchaseTransformationStockNumberLogicCollection[i].EvaluationOrder = i + 1;
        }
    }

    protected void StockNumberLogicTableRemoveRowButton_Click(object sender, EventArgs e)
    {
        BindSNLs();

        SNLs.Remove(SNLs.Find(delegate(TradePurchaseTransformationStockNumberLogic l) { return l.Id == new Guid(((Button)sender).CommandArgument); }));
        StockNumberLogicRepeater.DataBind();
    }


    #endregion Trade/Purchase Transformation Settings

    #region Unit Cost Transformation Settings


    private void BindDealerUnitCostTransformation()
    {
        DealerDefinedDeals_All.Checked = false;
        DealerDefinedDeals_Purchases.Checked = false;
        DealerDefinedDeals_Retail.Checked = false;
        DealerDefinedDeals_TradeIns.Checked = false;
        DealerDefinedDeals_Wholesale.Checked = false;
        DealerDefinedDeals_UnitCost.Checked = false;
        DealerDefinedDeals_UnitCostOperator.SelectedIndex = 0;
        DealerDefinedDeals_UnitCostThreshold.Text = "";
        DealerDefinedDeals_AgeInDays.Checked = false;
        DealerDefinedDeals_AgeInDaysOperator.SelectedIndex = 0;
        DealerDefinedDeals_AgeInDaysThreshold.Text = "";



        if (MTSettings.DealerUnitCostTransformationSettings.IsSet)
        {
            if (MTSettings.DealerUnitCostTransformationSettings.UseDMSPACK)
            {
                PackDefinitionRadioButtonList.SelectedIndex = 0;
            }
            else if (MTSettings.DealerUnitCostTransformationSettings.UseStaticPack)
            {
                PackDefinitionRadioButtonList.SelectedIndex = 1;
                DealerDefinedPackAmount.Text = MTSettings.DealerUnitCostTransformationSettings.StaticPackAmount.ToString();
            }

            if (MTSettings.DealerUnitCostTransformationSettings.UsesAllDeals)
            {
                DealerDefinedDeals_All.Checked = true;
            }
            else
            {
                DealerDefinedDeals_Purchases.Checked = MTSettings.DealerUnitCostTransformationSettings.ApplyToPurchase;
                DealerDefinedDeals_Retail.Checked = MTSettings.DealerUnitCostTransformationSettings.ApplyToRetail;
                DealerDefinedDeals_TradeIns.Checked = MTSettings.DealerUnitCostTransformationSettings.ApplyToTrade;
                DealerDefinedDeals_Wholesale.Checked = MTSettings.DealerUnitCostTransformationSettings.ApplyToWholesale;

                if (MTSettings.DealerUnitCostTransformationSettings.UnitCostThreshold != null)
                {
                    DealerDefinedDeals_UnitCost.Checked = true;
                    DealerDefinedDeals_UnitCostOperator.SelectedValue = MTSettings.DealerUnitCostTransformationSettings.UnitCostThreshold.Operator.ToString();
                    DealerDefinedDeals_UnitCostThreshold.Text = MTSettings.DealerUnitCostTransformationSettings.UnitCostThreshold.Amount.ToString();
                }

                if (MTSettings.DealerUnitCostTransformationSettings.AgeInDaysThreshold != null)
                {
                    DealerDefinedDeals_AgeInDays.Checked = true;
                    DealerDefinedDeals_AgeInDaysOperator.SelectedValue = MTSettings.DealerUnitCostTransformationSettings.AgeInDaysThreshold.Operator.ToString();
                    DealerDefinedDeals_AgeInDaysThreshold.Text = MTSettings.DealerUnitCostTransformationSettings.AgeInDaysThreshold.Amount.ToString();
                }
            }
        }
        else
        {
            PackDefinitionRadioButtonList.SelectedIndex = 0;
            DealerDefinedDeals_All.Checked = true;
        }
        PackDefinitionRadioButtonList_SelectedIndexChanged(null, null);
    }

    protected void DealerDefinedDeals_UnitCostThreshold_TextChanged(object sender, EventArgs e)
    {
        //BindDealerUnitCostTransformation();
    }

    protected void DealerDefinedDeals_AgeInDaysThreshold_TextChanged(object sender, EventArgs e)
    {
        //BindDealerUnitCostTransformation();
    }

    protected void DealerDefinedPackAmount_TextChanged(object sender, EventArgs e)
    {
        //BindDealerUnitCostTransformation();
    }

    protected void PackDefinitionRadioButtonList_SelectedIndexChanged(object sender, EventArgs e)
    {
        DealerDefinedAmountPanel.Visible = PackDefinitionRadioButtonList.SelectedIndex == 1;
    }


    #endregion Unit Cost Transformation Settings

    #region Global Save/Cancel


    protected void SaveButton_Click(object sender, EventArgs e)
    {
        MTSettings.DealerStandardMappingSettings.ListPriceMapping = 
            ListPriceMappingDropDownList.SelectedValue == "" ? 
            null : 
            ListPriceMapping.GetListPriceMapping(int.Parse(ListPriceMappingDropDownList.SelectedValue));

        MTSettings.DealerStandardMappingSettings.LotPriceMapping = 
            LotPriceMappingDropDownList.SelectedValue == ""? 
            null : 
            LotPriceMapping.GetLotPriceMapping(int.Parse(LotPriceMappingDropDownList.SelectedValue));

        MTSettings.DealerStandardMappingSettings.MileageMapping = 
            MileageMappingDropDownList.SelectedValue == "" ?
            null : 
            MileageMapping.GetMileageMapping(int.Parse(MileageMappingDropDownList.SelectedValue));

        MTSettings.DealerStandardMappingSettings.ReceivedDateMapping =
            ReceivedDateMappingDropDownList.SelectedValue == "" ? 
            null : 
            ReceivedDateMapping.GetReceivedDateMapping(int.Parse(ReceivedDateMappingDropDownList.SelectedValue));
        


        if (TradePurchaseRadioButtonList.SelectedIndex != -1)
        {
            TradePurchaseTransformationType t = (TradePurchaseTransformationType)Enum.Parse(typeof(TradePurchaseTransformationType), TradePurchaseRadioButtonList.SelectedValue);
            MTSettings.DealerTradePurchaseTransformationSettings.TradePurchaseTransformationType = t;

            if (t == TradePurchaseTransformationType.StockNumberLogic)
            {
                SetDealerTradePurchaseTransformationStockNumberLogics();
            }
        }




        if (PackDefinitionRadioButtonList.SelectedIndex == 0)
        {
            MTSettings.DealerUnitCostTransformationSettings.UseDMSPACK = true;
            MTSettings.DealerUnitCostTransformationSettings.UseStaticPack = false;
        }
        else if (PackDefinitionRadioButtonList.SelectedIndex == 1)
        {
            MTSettings.DealerUnitCostTransformationSettings.UseDMSPACK = false;
            MTSettings.DealerUnitCostTransformationSettings.UseStaticPack = true;
            MTSettings.DealerUnitCostTransformationSettings.StaticPackAmount = DealerDefinedPackAmount.Text.Trim() == "" ? 0 : decimal.Parse(DealerDefinedPackAmount.Text);
        }

        if (DealerDefinedDeals_All.Checked)
        {
            MTSettings.DealerUnitCostTransformationSettings.ApplyToPurchase = true;
            MTSettings.DealerUnitCostTransformationSettings.ApplyToRetail = true;
            MTSettings.DealerUnitCostTransformationSettings.ApplyToTrade = true;
            MTSettings.DealerUnitCostTransformationSettings.ApplyToWholesale = true;

            MTSettings.DealerUnitCostTransformationSettings.UnitCostThreshold = UnitCostTransformationThreshold.NewUnitCostTransformationThreshold();
            MTSettings.DealerUnitCostTransformationSettings.AgeInDaysThreshold = UnitCostTransformationThreshold.NewUnitCostTransformationThreshold();

            MTSettings.DealerUnitCostTransformationSettings.UnitCostThreshold.Operator = UnitCostTransformationThresholdOperator.GTE;
            MTSettings.DealerUnitCostTransformationSettings.UnitCostThreshold.Amount = 0;
            MTSettings.DealerUnitCostTransformationSettings.AgeInDaysThreshold.Operator = UnitCostTransformationThresholdOperator.GTE;
            MTSettings.DealerUnitCostTransformationSettings.AgeInDaysThreshold.Amount = 0;
        }
        else
        {
            MTSettings.DealerUnitCostTransformationSettings.ApplyToPurchase = DealerDefinedDeals_Purchases.Checked;
            MTSettings.DealerUnitCostTransformationSettings.ApplyToRetail = DealerDefinedDeals_Retail.Checked;
            MTSettings.DealerUnitCostTransformationSettings.ApplyToTrade = DealerDefinedDeals_TradeIns.Checked;
            MTSettings.DealerUnitCostTransformationSettings.ApplyToWholesale = DealerDefinedDeals_Wholesale.Checked;

            if (DealerDefinedDeals_UnitCost.Checked)
            {
                MTSettings.DealerUnitCostTransformationSettings.UnitCostThreshold = UnitCostTransformationThreshold.NewUnitCostTransformationThreshold();
                MTSettings.DealerUnitCostTransformationSettings.UnitCostThreshold.Operator = (UnitCostTransformationThresholdOperator)Enum.Parse(typeof(UnitCostTransformationThresholdOperator), DealerDefinedDeals_UnitCostOperator.SelectedValue);
                MTSettings.DealerUnitCostTransformationSettings.UnitCostThreshold.Amount = decimal.Parse(DealerDefinedDeals_UnitCostThreshold.Text);
            }
            else
            {
                MTSettings.DealerUnitCostTransformationSettings.UnitCostThreshold = null;
            }

            if (DealerDefinedDeals_AgeInDays.Checked)
            {
                MTSettings.DealerUnitCostTransformationSettings.AgeInDaysThreshold = UnitCostTransformationThreshold.NewUnitCostTransformationThreshold();
                MTSettings.DealerUnitCostTransformationSettings.AgeInDaysThreshold.Operator = (UnitCostTransformationThresholdOperator)Enum.Parse(typeof(UnitCostTransformationThresholdOperator), DealerDefinedDeals_AgeInDaysOperator.SelectedValue);
                MTSettings.DealerUnitCostTransformationSettings.AgeInDaysThreshold.Amount = decimal.Parse(DealerDefinedDeals_AgeInDaysThreshold.Text);
            }
            else
            {
                MTSettings.DealerUnitCostTransformationSettings.AgeInDaysThreshold = null;
            }
        }

        if (!MTSettings.DealerTradePurchaseTransformationSettings.IsValid)
        {
            ErrorList.Visible = true;
            ErrorList.Items.Add("Error adding Stock Number logic.  Pattern type and text cannot be empty.");
        }
        else if (!MTSettings.DealerUnitCostTransformationSettings.IsValid)
        {
            ErrorList.Visible = true;
            ErrorList.Items.Add("Error adding Unit Cost Transformation.  If using static pack amount, amount must be greater than 0.");
            foreach (BrokenRule rule in MTSettings.DealerUnitCostTransformationSettings.BrokenRulesCollection)
            {
                ErrorList.Items.Add(rule.Description);
            }
        }
        else
        {
            ErrorList.Items.Clear();
            ErrorList.Visible = false;
            MTSettings.Save();
            SavedLabel.Visible = true;
        }
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        MileageMappingDropDownList.DataBind();
        ListPriceMappingDropDownList.DataBind();
        LotPriceMappingDropDownList.DataBind();
        ReceivedDateMappingDropDownList.DataBind();


        BindDealerUnitCostTransformation();
    }


    #endregion Global Save/Cancel

    #region Environment Selection/Migration


    protected void EnvironmentDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        MileageMappingDropDownList.DataBind();
        ListPriceMappingDropDownList.DataBind();
        LotPriceMappingDropDownList.DataBind();
        ReceivedDateMappingDropDownList.DataBind();
        TradePurchaseRadioButtonList.DataBind();
        BindDealerUnitCostTransformation();
    }

    protected void MigrateCopyToProdLinkButton_Click(object sender, EventArgs e)
    {
        MigrateTransformationCommand.Execute(Dealer.Id, Environment.PreProduction, Environment.Production);
        PromoteSavedLabel.Visible = true;
    }

    protected void MigrateRestoreFromProdLinkButton_Click(object sender, EventArgs e)
    {
        MigrateTransformationCommand.Execute(Dealer.Id, Environment.Production, Environment.PreProduction);
        PromoteSavedLabel.Visible = true;
    }


    #endregion Environment Selection/Migration
}
