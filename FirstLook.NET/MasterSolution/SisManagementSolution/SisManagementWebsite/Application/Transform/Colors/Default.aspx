<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Application_Transform_Colors_Default" MasterPageFile="~/Application/BasicMasterPage.master" %>

<asp:Content ContentPlaceHolderID="TitleContentPlaceHolder" runat="server">
    <title>Color Menu</title>
</asp:Content>

<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
    <fieldset>
        <legend>Color Menu</legend>
        <ol>
            <li><asp:HyperLink ID="AddColorMappingLink" runat="server" NavigateUrl="~/Application/Transform/Colors/UnmappedColors.aspx">Manage Unmapped Colors</asp:HyperLink></li>
            <li><asp:HyperLink ID="EditColorMappingLink" runat="server" NavigateUrl="~/Application/Transform/Colors/ColorMapping.aspx">Edit Color Mapping</asp:HyperLink></li>
        </ol>
    </fieldset>
</asp:Content>
