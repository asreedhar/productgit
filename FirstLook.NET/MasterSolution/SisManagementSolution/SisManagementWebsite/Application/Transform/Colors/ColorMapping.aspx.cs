using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Csla.Web;
using FirstLook.Sis.Management.DomainModel.Transform.Colors;

public partial class Application_Transform_Colors_ColorMapping : Page
{
    protected void ColorDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        int? standardColorId = int.Parse(StandardColorDropDownList.SelectedValue);

        e.BusinessObject = ColorCollection.GetColors(
            standardColorId == 0 ? null : standardColorId,
            ColorNameTextBox.Text,
            e.SortProperty,
            e.MaximumRows,
            e.StartRowIndex);
    }

    protected void ColorGridView_DataBound(object sender, EventArgs e)
    {
        NoColorsLabel.Visible = (ColorGridView.Rows.Count < 1);
    }

    protected void ColorGridView_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridView view = sender as GridView;

        if (view != null)
        {
            DropDownList list = (DropDownList) view.Rows[e.RowIndex].FindControl("StandardColorDropDownList");

            if (list != null)
            {
                int newStandardColorId = int.Parse(list.SelectedValue);

                int oldStandardColorId = ((StandardColor) e.Keys["StandardColor"]).Id;

                if (newStandardColorId != oldStandardColorId)
                {
                    e.OldValues["StandardColorId"] = oldStandardColorId;
                    e.NewValues["StandardColorId"] = newStandardColorId;
                    e.Cancel = false;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
    }

    protected void StandardColorDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        e.BusinessObject = StandardColorCollection.GetStandardColors();
    }

    protected void ColorDataSource_UpdateObject(object sender, UpdateObjectArgs e)
    {
        if (StandardColorMappingCollection.Transfer(
            (int) e.OldValues["StandardColorId"],
            (int) e.Values["StandardColorId"],
            (int) e.Keys["Id"]))
        {
            ColorGridView.DataBind();
        }
    }

    protected void ColorNameTextBox_TextChanged(object sender, EventArgs e)
    {
        ColorSearch();
    }

    protected void SearchButton_Click(object sender, EventArgs e)
    {
        ColorSearch();
    }

    private void ColorSearch()
    {
        ColorGridView.DataBind();
    }
}
