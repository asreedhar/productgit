using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Csla.Web;
using FirstLook.Sis.Management.DomainModel.Transform.Colors;

public partial class Application_Transform_Colors_UnmappedColors : Page
{
    protected void StandardColorDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        e.BusinessObject = StandardColorCollection.GetStandardColors();
    }

    protected void UnmappedColorDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        e.BusinessObject = ColorSummaryCollection.GetUnknownColors(
            ColorNameTextBox.Text,
            e.SortProperty,
            e.MaximumRows,
            e.StartRowIndex);
    }

    protected void UnmappedColorDataSource_UpdateObject(object sender, UpdateObjectArgs e)
    {
        int standardColorId = (int)e.Values["StandardColorId"];

        int colorId = (int)e.Keys["Id"];

        StandardColorMappingCollection mappings = StandardColorMappingCollection.GetStandardColorMappings(standardColorId);

        mappings.Assign(colorId);

        mappings.Save();

        UnmappedColorGridView.DataBind();
    }

    protected void ColorNameTextBox_TextChanged(object sender, EventArgs e)
    {
        ColorSearch();
    }

    protected void SearchButton_Click(object sender, EventArgs e)
    {
        ColorSearch();
    }

    private void ColorSearch()
    {
        UnmappedColorGridView.DataBind();
    }


    protected void UnmappedColorGridView_DataBound(object sender, EventArgs e)
    {
        NoColorsLabel.Visible = UnmappedColorGridView.Rows.Count == 0;
    }

    protected void UnmappedColorGridView_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridView view = sender as GridView;

        if (view != null)
        {
            DropDownList list = (DropDownList)view.Rows[e.RowIndex].FindControl("StandardColorDropDownList");

            if (list != null)
            {
                int newStandardColorId = int.Parse(list.SelectedValue);
                e.NewValues["StandardColorId"] = newStandardColorId;
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }
    }

}
