<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ColorMapping.aspx.cs" Inherits="Application_Transform_Colors_ColorMapping" MasterPageFile="~/Application/BasicMasterPage.master" %>

<asp:Content ContentPlaceHolderID="TitleContentPlaceHolder" runat="server">
    <title>Color Mapping</title>
</asp:Content>

<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
    <csla:CslaDataSource ID="StandardColorDataSource" runat="server"
            OnSelectObject="StandardColorDataSource_SelectObject" />
    <csla:CslaDataSource runat="server" ID="ColorDataSource"
            OnSelectObject="ColorDataSource_SelectObject"
            OnUpdateObject="ColorDataSource_UpdateObject"
            TypeSupportsPaging="true"
            TypeSupportsSorting="true" />
    <fieldset>
        <legend>Color Mapping</legend>
        <asp:BulletedList ID="ErrorList" runat="server" Visible="false" CssClass="ErrorList">
        </asp:BulletedList>
        <p>Please enter the search criteria for the colors whose mappings you would like to edit.</p>
        <p>
            <asp:Label ID="StandardColorLabel" runat="server" Text="StandardColor: " AssociatedControlID="StandardColorDropDownList"></asp:Label>
            <asp:DropDownList ID="StandardColorDropDownList" runat="server"
                    AppendDataBoundItems="true"
                    DataTextField="Name"
                    DataValueField="Id"
                    DataSourceID="StandardColorDataSource">
                <asp:ListItem Text="Any" Value="0" />
            </asp:DropDownList>
        </p>
        <p>
            <asp:Label ID="ColorNameLabel" runat="server" Text="Color Name: " AssociatedControlID="ColorNameTextBox"></asp:Label>
            <asp:TextBox ID="ColorNameTextBox" runat="server" MaxLength="255" OnTextChanged="ColorNameTextBox_TextChanged"></asp:TextBox>
            <asp:Button ID="SearchButton" runat="server" Text="Search" OnClick="SearchButton_Click" />
        </p>
        <p>Results</p>
        <asp:GridView runat="server" ID="ColorGridView"
                AutoGenerateColumns="false"
                AllowSorting="True"
                AllowPaging="True"
                DataKeyNames="Id,StandardColor"
                DataSourceID="ColorDataSource"
                OnDataBound="ColorGridView_DataBound"
                OnRowUpdating="ColorGridView_RowUpdating">
            <Columns>
                <asp:BoundField AccessibleHeaderText="Color" HeaderText="Color" DataField="Name" ReadOnly="true" />
                <asp:TemplateField AccessibleHeaderText="Stanndard Color" HeaderText="Standard Color">
                    <ItemTemplate>
                        <%# Eval("StandardColor.Name") %>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:DropDownList ID="StandardColorDropDownList" runat="server"
                                DataSourceID="StandardColorDataSource"
                                DataTextField="Name"
                                DataValueField="Id"
                                SelectedValue='<%# Eval("StandardColor.Id") %>'>
                        </asp:DropDownList>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ButtonType="Button" ShowCancelButton="true" ShowEditButton="true" />
            </Columns>
        </asp:GridView>
        <asp:Label runat="server" ID="NoColorsLabel" Text="No colors matched the search criteria.  Please try again." />
    </fieldset>
</asp:Content>
