<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UnmappedColors.aspx.cs" Inherits="Application_Transform_Colors_UnmappedColors" MasterPageFile="~/Application/BasicMasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContentPlaceHolder" runat="server">
    <title>Unmapped Colors</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
    <csla:CslaDataSource ID="StandardColorDataSource" runat="server"
            OnSelectObject="StandardColorDataSource_SelectObject" />
    <csla:CslaDataSource runat="server" ID="UnmappedColorDataSource"
            OnSelectObject="UnmappedColorDataSource_SelectObject"
            OnUpdateObject="UnmappedColorDataSource_UpdateObject"
            TypeSupportsPaging="true"
            TypeSupportsSorting="true" />
    <fieldset>
        <legend>Unmapped Colors</legend>
        <p>Here are the colors found on inventory in the system that are not mapped to a First Look Standard Color.</p>
        <p>Please restrict your results by specify search criteria.</p>
        <p>
            <asp:Label ID="ColorNameLabel" runat="server" Text="Color Name: " AssociatedControlID="ColorNameTextBox"></asp:Label>
            <asp:TextBox ID="ColorNameTextBox" runat="server" MaxLength="255" OnTextChanged="ColorNameTextBox_TextChanged"></asp:TextBox>
            <asp:Button ID="SearchButton" runat="server" Text="Search" OnClick="SearchButton_Click" />
        </p>
        <p>Results:</p>
        <asp:GridView runat="server" ID="UnmappedColorGridView"
                AutoGenerateColumns="false"
                AllowPaging="true"
                AllowSorting="true"
                DataKeyNames="Id"
                DataSourceID="UnmappedColorDataSource"
                OnDataBound="UnmappedColorGridView_DataBound"
                OnRowUpdating="UnmappedColorGridView_RowUpdating">
            <Columns>
                <asp:BoundField AccessibleHeaderText="Color" HeaderText="Color" DataField="Name" ReadOnly="true" />
                <asp:BoundField AccessibleHeaderText="Inventory Count" HeaderText="Inventory Count" DataField="NumberOfVehicles" ReadOnly="true" />
                <asp:TemplateField AccessibleHeaderText="Standard Color" HeaderText="Standard Color">
                    <ItemTemplate>
                        N/A
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:DropDownList ID="StandardColorDropDownList" runat="server"
                                DataSourceID="StandardColorDataSource"
                                DataTextField="Name"
                                DataValueField="Id">
                        </asp:DropDownList>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ButtonType="Button" ShowCancelButton="true" ShowEditButton="true" />
            </Columns>
        </asp:GridView>
        <asp:Label runat="server" ID="NoColorsLabel" Text="No colors are currently unmapped." />
    </fieldset>
</asp:Content>
