<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Application_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Main Menu</title>
</head>
<body class="ApplicationDefault">
    <div id="white"><div id="white_r"></div></div>
    <form id="form1" runat="server">
        <div class="Wrapper">
            <div class="Navigation"><div id="header_r"><div id="header_l">
                <asp:SiteMapPath ID="ApplicationSiteMapPath" runat="server" PathDirection="RootToCurrent" CssClass="Breadcrumb" />
                <p class="Shortcut">Home</p>                
            </div></div></div>
            <div class="Content">
                <h2>Generic Data Transformations</h2>
                <ol>
                    <li><asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Application/Transform/Colors/Default.aspx">Color Configuration</asp:HyperLink></li>
                    <li><asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/Application/Transform/Dealer/Default.aspx">Dealer Configuration</asp:HyperLink></li>
                </ol>
                <h2>SIS Configurations</h2>
                <ol>
                    <li><asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Application/Configuration/ADP/Default.aspx">ADP Configuration</asp:HyperLink></li>
                    <li><asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/Application/Configuration/Preferences/Default.aspx">Dealer Preferences</asp:HyperLink></li>
                </ol>
            </div>
        </div>
    </form>
</body>
</html>
