<%@ Page Language="C#" MasterPageFile="~/Application/Configuration/Preferences/PreferenceMasterPage.master" AutoEventWireup="true" CodeFile="DealerPreferenceConfiguration.aspx.cs" Inherits="Application_Configuration_ADP_Preferences_DealerPreferenceConfiguration" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContentPlaceHolder" Runat="Server">
        <title>Configure Dealer Preferences</title>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="BodyContentPlaceHolder" Runat="Server">
    <fieldset>
        <legend>Dealer Preference Configuration</legend>
        <asp:BulletedList ID="ErrorList" runat="server" Visible="false" CssClass="ErrorList" />
        <h4><asp:Label runat="server" ID="SavedLabel" Visible="false" Text="Dealer Environment Preferences succesfully saved." /></h4>

        <csla:CslaDataSource ID="DealerEnvironmentPreferenceRepeaterDataSource" runat="server"
            OnSelectObject="DealerEnvironmentPreferenceRepeaterDataSource_SelectObject"
            TypeSupportsPaging="false"
            TypeSupportsSorting="false">
        </csla:CslaDataSource>
        <p>Enter dealer preference details in the form below:</p>
                    
        <asp:Repeater runat="server" ID="DealerEnvironmentPreferenceRepeater" DataSourceID="DealerEnvironmentPreferenceRepeaterDataSource" OnItemDataBound="DealerEnvironmentPreferenceRepeater_ItemDataBound" >
            <HeaderTemplate>
                <table width="75%">
                    <tr>
                        <td width="30%">Name</td>
                        <td width="30%">Description</td>
                        <td width="20%">Data Type</td>
                        <td width="20%">Value</td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td width="30%">
                        <asp:Label runat="server" ID="NameLabel" />
                    </td>
                    <td width="30%">
                        <asp:Label runat="server" ID="DescriptionLabel" />
                    </td>
                    <td width="20%">
                        <asp:Label runat="server" ID="DataTypeLabel" />
                    </td>
                    <td width="20%">
                        <asp:TextBox runat="server" ID="ValueTextBox" />
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <p class="controls">
            <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
            <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" />
        </p>

    </fieldset>
</asp:Content>
