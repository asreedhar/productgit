<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Application_Configuration_ADP_Preferences_Default" MasterPageFile="~/Application/Configuration/Preferences/PreferenceMasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContentPlaceHolder" runat="server">
    <title>Dealer Preference Dealer Selection</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContentPlaceHolder" Runat="Server">
    <fieldset>
        <legend>Dealer Preferences</legend>
        <p>
            Select a dealership to manage their Dealer Preferences.
        </p>
        <p>
            <asp:Panel ID="DealerSearchPanel" DefaultButton="DealerSearchButton" runat="server">
            <asp:Label ID="DealerNameLabel" runat="server" Text="Dealer Name:" AssociatedControlID="DealerNameTextBox"></asp:Label>
            <asp:TextBox ID="DealerNameTextBox" runat="server" OnTextChanged="DealerNameTextBox_TextChanged"></asp:TextBox>
            <asp:Button ID="DealerSearchButton" runat="server" Text="Search" OnClick="DealerSearchButton_Click" />
            <asp:Button ID="ClearSearchButton" runat="server" Text="Clear" OnClick="ClearSearchButton_Click" />
            </asp:Panel>
        </p>
        <csla:CslaDataSource ID="DealerGridViewDataSource" runat="server"
            OnSelectObject="DealerGridViewDataSource_SelectObject"
            TypeSupportsPaging="true"
            TypeSupportsSorting="true">
        </csla:CslaDataSource>
        <asp:GridView ID="DealerGridView" runat="server"
            AllowPaging="true"
            AllowSorting="true"
            AutoGenerateColumns="false"
            DataKeyNames="Id,ConfigurationWorkStep"
            DataSourceID="DealerGridViewDataSource"
            OnRowCommand="DealerGridView_RowCommand"
            OnDataBound="DealerGridView_DataBound">
            <Columns>
                <asp:BoundField SortExpression="Name" AccessibleHeaderText="Name" HeaderText="Name" DataField="Name" ReadOnly="true" />
                <asp:BoundField SortExpression="Address" AccessibleHeaderText="Address" HeaderText="Address" DataField="Address" ReadOnly="true" />
                <asp:BoundField SortExpression="Active" AccessibleHeaderText="Active" HeaderText="Active" DataField="Active" ReadOnly="true" />
                <asp:BoundField SortExpression="DealerGroupName" AccessibleHeaderText="Dealer Group Name" HeaderText="Dealer Group Name" DataField="DealerGroupName" ReadOnly="true" />
                <asp:ButtonField ButtonType="Button" Text="Configure" CommandName="Configure" ShowHeader="false" />
            </Columns>
        </asp:GridView>
        <p>
            <asp:Label ID="NoDealerText" runat="server" Visible="false">
                No dealerships matched your search criteria.
                Please try changing or removing your search criteria.
            </asp:Label>
        </p>
    </fieldset>
</asp:Content>
