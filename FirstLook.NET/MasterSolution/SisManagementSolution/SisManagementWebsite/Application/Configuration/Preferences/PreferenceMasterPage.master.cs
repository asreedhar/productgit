using System;
using System.Web.UI;
using FirstLook.Sis.Management.DomainModel.Configuration.Adp;

public partial class Application_Configuration_ADP_Preferences_MasterPage : MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string value = Request.QueryString["DealerId"];

        if (!string.IsNullOrEmpty(value))
        {
            int id;

            if (int.TryParse(value, out id))
            {
                DealerConfigurationInfo dealer = DealerConfigurationInfo.GetDealer(id);

                DealerLabel.Text = string.Format("{0} / {1}", dealer.Name, dealer.DealerGroupName);
            }
        }

        Page.Header.DataBind(); // resolve JS links
    }
}
