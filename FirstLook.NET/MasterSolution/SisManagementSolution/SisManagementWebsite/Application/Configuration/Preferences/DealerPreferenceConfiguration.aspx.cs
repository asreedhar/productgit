using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Csla.Validation;
using Csla.Web;
using FirstLook.Sis.Management.DomainModel.Configuration;
using FirstLook.Sis.Management.DomainModel.Configuration.Adp;
using FirstLook.Sis.Management.DomainModel.Transform.Dealer;
using Environment = FirstLook.Sis.Management.DomainModel.Configuration.Environment;


public partial class Application_Configuration_ADP_Preferences_DealerPreferenceConfiguration : System.Web.UI.Page
{
    private Dealer _dealer;
    private DealerEnvironmentPreferenceCollection dealerPrefs;
    private PreferenceCollection prefs;

    private Dealer Dealer
    {
        get
        {
            if (_dealer == null)
            {
                string id = Request.QueryString["DealerId"];

                if (string.IsNullOrEmpty(id))
                {
                    throw new ApplicationException("Missing DealerId");
                }
                else
                {
                    _dealer = Dealer.GetDealer(int.Parse(id), Environment.PreProduction);
                }
            }

            return _dealer;
        }
    }
    private PreferenceCollection Prefs
    {
        get
        {
            if (prefs == null)
            {
                prefs = PreferenceCollection.GetPreferenceCollection();
            }
            return prefs;
        }
    }
    private DealerEnvironmentPreferenceCollection DealerPrefs
    {
        get
        {
            if (dealerPrefs == null)
            {
                dealerPrefs = DealerEnvironmentPreferenceCollection.GetDealerEnvironmentPreferenceCollection(Dealer);
            }
            return dealerPrefs;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        SavedLabel.Visible = false;

    }

    protected void DealerEnvironmentPreferenceRepeaterDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        e.BusinessObject = Prefs;
    }

    protected void DealerEnvironmentPreferenceRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ((Label)e.Item.FindControl("NameLabel")).Text = ((Preference)e.Item.DataItem).ShortDescription;
            ((Label)e.Item.FindControl("DescriptionLabel")).Text = ((Preference)e.Item.DataItem).LongDescription;
            ((Label)e.Item.FindControl("DataTypeLabel")).Text = ((Preference)e.Item.DataItem).DataType.Name;
            if (DealerPrefs.Contains(((Preference)e.Item.DataItem)))
            {
                ((TextBox)e.Item.FindControl("ValueTextBox")).Text = dealerPrefs.Get(((Preference)e.Item.DataItem)).Value.ToString();
            }
        }
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        foreach (RepeaterItem item in DealerEnvironmentPreferenceRepeater.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                //existing pref, need to delete or edit
                if(DealerPrefs.Contains(Prefs[item.ItemIndex]))
                {
                    TextBox tb = ((TextBox)item.FindControl("ValueTextBox"));
                    if(tb.Text == "")
                    {
                        DealerPrefs.Remove(Prefs[item.ItemIndex]);
                    }
                    else 
                    {
                        DealerPrefs.Get(Prefs[item.ItemIndex]).Value = tb.Text;
                    }
                }
                //new pref, need to add
                else
                {
                    TextBox tb = ((TextBox)item.FindControl("ValueTextBox"));
                    if (tb.Text != "")
                    {
                        DealerEnvironmentPreference p = DealerEnvironmentPreference.NewDealerEnvironmentPreference(Dealer, Prefs[item.ItemIndex], tb.Text);
                        DealerPrefs.Add(p);
                    }
                }
            }
        }


        if (!DealerPrefs.IsValid)
        {
            ErrorList.Visible = true;
            ErrorList.Items.Add("Error adding Dealer Preferences.  Value text cannot be empty and must be of the type specified.");
        }
        else
        {
            ErrorList.Items.Clear();
            ErrorList.Visible = false;
            DealerPrefs.Save();
            SavedLabel.Visible = true;
        }
        
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        DealerEnvironmentPreferenceRepeater.DataBind();
    }
}
