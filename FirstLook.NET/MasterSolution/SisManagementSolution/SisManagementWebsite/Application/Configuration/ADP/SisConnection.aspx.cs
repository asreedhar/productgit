using System;
using System.Globalization;
using System.Transactions;
using System.Web.UI;
using System.Web.UI.WebControls;
using Csla;
using Csla.Core;
using Csla.Validation;
using Csla.Web;
using FirstLook.Sis.Management.DomainModel.Configuration.Adp;
using Environment=FirstLook.Sis.Management.DomainModel.Configuration.Environment;

public partial class Application_Configuration_ADP_SisConnection : Page
{
    private DmsHost _dmsHost;
    private Dealer _dealer;
    private SisConnection _connection;

    private Dealer Dealer
    {
        get
        {
            if (_dealer == null)
            {
                string id = Request.QueryString["DealerId"];

                if (string.IsNullOrEmpty(id))
                {
                    throw new ApplicationException("Missing DealerId");
                }
                else
                {
                    _dealer = Dealer.GetDealer(int.Parse(id), Environment.PreProduction);
                }
            }

            return _dealer;
        }
    }

    private DmsHost DmsHost
    {
        get
        {
            if (_dmsHost == null)
            {
                string id = Request.QueryString["DmsHostId"];

                if (string.IsNullOrEmpty(id))
                {
                    throw new ApplicationException("Missing DmsHostId");
                }
                else
                {
                    _dmsHost = DmsHost.GetDmsHost(new Guid(id));
                }
            }

            return _dmsHost;
        }
    }

    private SisConnection Connection
    {
        get
        {
            if (_connection == null)
            {
                string id = Request.QueryString["SisConnectionId"];

                if (string.IsNullOrEmpty(id))
                {
                    _connection = SisConnection.NewSisConnection();
                }
                else
                {
                    _connection = DmsHost.SisConnections.GetItem(new Guid(id));
                }
            }

            return _connection;
        }
    }

    private bool IsUpdate
    {
        get
        {
            return (string.Equals(Request.QueryString["Mode"], "Update"));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PopulateForm(Connection);

            if (IsUpdate)
            {
                AccountingLogonRadioButton.Visible = false;
                AccountingLogonDropDownList.Enabled = false;

                NewAccountingLogonLabel.Visible = false;
                NewAccountingLogonRadioButton.Visible = false;
                NewAccountingLogonTextBox.Visible = false;

                FinanceLogonRadioButton.Visible = false;
                FinanceLogonDropDownList.Enabled = false;

                NewFinanceLogonLabel.Visible = false;
                NewFinanceLogonRadioButton.Visible = false;
                NewFinanceLogonTextBox.Visible = false;
            }
            else
            {
                CancelButton.Visible = false;
            }
        }
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        if (IsUpdate)
        {
            LoadComplete += ResetForm;
            Response.Redirect(string.Format(CultureInfo.InvariantCulture,
                "~/Application/Configuration/ADP/ExistingDealerConfiguration.aspx?DealerId={0}",
            Dealer.Id), true);
        }
        else
        {
            Redirect();
        }
    }

    private void ResetForm(object sender, EventArgs e)
    {
        LoadComplete -= ResetForm;

        _connection = null;

        PopulateForm(Connection);
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        LoadComplete += MarshallForm;
    }

    private void Redirect()
    {
        Response.Redirect(string.Format(CultureInfo.InvariantCulture,
            "~/Application/Configuration/ADP/DownloadDmsReferenceData.aspx?DealerId={0}&SisConnectionId={1}&DmsHostId={2}",
            Dealer.Id,
            Connection.Id,
            DmsHost.Id), true);
    }

    private void MarshallForm(object sender, EventArgs e)
    {
        LoadComplete -= MarshallForm;

        ErrorList.Items.Clear();

        MarshallForm(DmsHost, Connection);

        if (DmsHost.IsValid && Dealer.IsValid)
        {
            ErrorList.Visible = false;

            bool redirect = true;
            try
            {
                SisConnectionCreateAssignCommand.Execute(Dealer, DmsHost, Connection);
            }
            catch (Exception ex)
            {
                ErrorList.Visible = true;
                ErrorList.Items.Add(ex.Message);
                redirect = false;
            }

            if (redirect)
            {
                Redirect();
            }
        }
        else
        {
            ErrorList.Visible = true;

            AddErrorMessages(DmsHost);

            AddErrorMessages(Connection);

            AddErrorMessages(Connection.Credential);

            AddErrorMessages(Connection.DmsHostCredential);

            AddErrorMessages(Connection.AccountingLogon);

            AddErrorMessages(Connection.FinanceLogon);
        }
    }

    private void AddErrorMessages(BusinessBase obj)
    {
        if (obj != null)
        {
            foreach (BrokenRule rule in obj.BrokenRulesCollection)
            {
                ErrorList.Items.Add(rule.Description);
            }
        }
    }

    protected void AccountingLogonRadioButton_CheckedChanged(object sender, EventArgs e)
    {
        AccountingLogonDropDownList.Enabled = AccountingLogonRadioButton.Checked;
        NewAccountingLogonTextBox.Enabled = NewAccountingLogonRadioButton.Checked;
        if (AccountingLogonRadioButton.Checked)
            NewAccountingLogonTextBox.Text = string.Empty;
    }

    protected void FinanceLogonRadioButton_CheckedChanged(object sender, EventArgs e)
    {
        FinanceLogonDropDownList.Enabled = FinanceLogonRadioButton.Checked;
        NewFinanceLogonTextBox.Enabled = NewFinanceLogonRadioButton.Checked;
        if (FinanceLogonRadioButton.Checked)
            NewFinanceLogonTextBox.Text = string.Empty;
    }

    protected void AccountingLogonDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        FilteredBindingList<Logon> filtered = new FilteredBindingList<Logon>(DmsHost.Logons);
        filtered.ApplyFilter("LogonType", LogonType.Accounting);
        e.BusinessObject = filtered;
    }

    protected void FinanceLogonDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        FilteredBindingList<Logon> filtered = new FilteredBindingList<Logon>(DmsHost.Logons);
        filtered.ApplyFilter("LogonType", LogonType.Finance);
        e.BusinessObject = filtered;
    }

    protected void AccountingLogonDropDownList_DataBound(object sender, EventArgs e)
    {
        if (AccountingLogonDropDownList.Items.Count > 1)
        {
            if (Connection.AccountingLogon != null)
                foreach (ListItem item in AccountingLogonDropDownList.Items)
                    item.Selected = string.Equals(item.Value, ToString(Connection.AccountingLogon.Id));
        }
        else
        {
            AccountingLogonDropDownList.Visible = false;
            AccountingLogonRadioButton.Visible = false;
            AccountingLogonLabel.Visible = false;

            NewAccountingLogonRadioButton.Checked = true;
            NewAccountingLogonRadioButton.Visible = false;
            NewAccountingLogonLabel.Text = "-A Logon#:(new)";
            NewAccountingLogonTextBox.Enabled = true;
        }
    }

    protected void FinanceLogonDropDownList_DataBound(object sender, EventArgs e)
    {
        if (FinanceLogonDropDownList.Items.Count > 1)
        {
            if (Connection.FinanceLogon != null)
                foreach (ListItem item in FinanceLogonDropDownList.Items)
                    item.Selected = string.Equals(item.Value, ToString(Connection.FinanceLogon.Id));
        }
        else
        {
            FinanceLogonDropDownList.Visible = false;
            FinanceLogonRadioButton.Visible = false;
            FinanceLogonLabel.Visible = false;

            NewFinanceLogonRadioButton.Checked = true;
            NewFinanceLogonRadioButton.Visible = false;
            NewFinanceLogonLabel.Text = "-FI Logon#:(new):";
            NewFinanceLogonTextBox.Enabled = true;
        }
    }

    private void PopulateForm(SisConnection connection)
    {
        if (connection.Credential != null)
        {
            SisUsernameTextBox.Text = connection.Credential.UserName;
            SisPasswordTextBox.Text = connection.Credential.Password;
        }

        if (connection.DmsHostCredential != null)
        {
            DmsUsernameTextBox.Text = connection.DmsHostCredential.UserName;
            DmsPasswordTextBox.Text = connection.DmsHostCredential.Password;
        }
        else if(DmsHost.Credential != null)
        {
            DmsUsernameTextBox.Text = DmsHost.Credential.UserName;
            DmsPasswordTextBox.Text = DmsHost.Credential.Password;
        }
    }

    private void MarshallForm(DmsHost dmsHost, SisConnection connection)
    {
        AssignLogon(
            AccountingLogonRadioButton.Checked,
            AccountingLogonDropDownList.SelectedValue,
            NewAccountingLogonTextBox.Text,
            LogonType.Accounting,
            dmsHost,
            connection);

        AssignLogon(
            FinanceLogonRadioButton.Checked,
            FinanceLogonDropDownList.SelectedValue,
            NewFinanceLogonTextBox.Text,
            LogonType.Finance,
            dmsHost,
            connection);

        if (connection.IsNew)
        {
            dmsHost.SisConnections.Add(connection);
        }

        if (connection.IsNew)
        {
            if (!AllEmpty(SisUsernameTextBox, SisPasswordTextBox))
            {
                Credential credential = Credential.NewCredential();
                credential.UserName = SisUsernameTextBox.Text;
                credential.Password = SisPasswordTextBox.Text;
                connection.Credential = credential;
            }
            else
            {
                if (string.IsNullOrEmpty(SisUsernameTextBox.Text))
                    ErrorList.Items.Add(new ListItem("Missing SIS User Name"));
                if (string.IsNullOrEmpty(SisPasswordTextBox.Text))
                    ErrorList.Items.Add(new ListItem("Missing SIS Password"));
            }
        }
        else
        {
            Credential credential = connection.Credential;
            credential.UserName = SisUsernameTextBox.Text;
            credential.Password = SisPasswordTextBox.Text;
        }

        if (connection.IsNew)
        {
            if (!AllEmpty(DmsUsernameTextBox, DmsPasswordTextBox))
            {
                Credential credential = Credential.NewCredential();
                credential.UserName = DmsUsernameTextBox.Text;
                credential.Password = DmsPasswordTextBox.Text;
                connection.DmsHostCredential = credential;
            }
            else
            {
                if (string.IsNullOrEmpty(DmsUsernameTextBox.Text))
                    ErrorList.Items.Add(new ListItem("Missing DMS User Name"));
                if (string.IsNullOrEmpty(DmsPasswordTextBox.Text))
                    ErrorList.Items.Add(new ListItem("Missing DMS Password"));
            }
        }
        else
        {
            Credential credential = connection.DmsHostCredential;
            credential.UserName = DmsUsernameTextBox.Text;
            credential.Password = DmsPasswordTextBox.Text;
        }
    }

    private static void AssignLogon(bool existingLogon, string logonId, string logonName, LogonType logonType, DmsHost dmsHost, SisConnection connection)
    {
        Logon logon = null;

        if (existingLogon)
        {
            if (!string.Equals(logonId, "0"))
            {
                Logon lookupLogon = dmsHost.Logons.GetItem(new Guid(logonId));

                if (lookupLogon != null)
                {
                    if (lookupLogon.LogonType == logonType)
                    {
                        logon = lookupLogon;
                    }
                }

            }
        }
        else
        {
            logon = Logon.NewLogon();
            logon.LogonType = logonType;
            logon.Name = logonName;

            dmsHost.Logons.Assign(logon);
        }

        if (logonType == LogonType.Accounting)
        {
            connection.AccountingLogon = logon;
        }
        else
        {
            connection.FinanceLogon = logon;
        }
    }

    private static bool AllEmpty(params ITextControl[] textControls)
    {
        bool allEmpty = true;

        foreach (ITextControl textControl in textControls)
        {
            allEmpty &= string.IsNullOrEmpty(textControl.Text);
        }

        return allEmpty;
    }

    private static string ToString(Guid guid)
    {
        return guid.ToString(string.Empty, CultureInfo.InvariantCulture);
    }
}
