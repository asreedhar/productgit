<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ExistingDealerSelection.aspx.cs" Inherits="Application_Configuration_ADP_ExistingDealerSelection" MasterPageFile="~/Application/BasicMasterPage.master" %>

<asp:Content ContentPlaceHolderID="TitleContentPlaceHolder" runat="server">
    <title>Existing Dealer Selection</title>
</asp:Content>

<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
    <fieldset>
        <legend>Existing Dealer Selection</legend>
        <p>Please enter search criteria to limit your selection of configured dealers.</p>
        <p>
            <asp:Label ID="DealerNameLabel" runat="server" Text="Dealer Name:" AssociatedControlID="DealerNameTextBox"></asp:Label>
            <asp:TextBox ID="DealerNameTextBox" runat="server" OnTextChanged="DealerNameTextBox_TextChange"></asp:TextBox>
            <asp:Button ID="DealerSearchButton" runat="server" Text="Search" OnClick="DealerSearchButton_Click" />
            <asp:Button ID="ClearSearchButton" runat="server" Text="Clear" OnClick="ClearSearchButton_Click" />
        </p>
        <csla:CslaDataSource ID="DealerGridViewDataSource" runat="server"
            OnSelectObject="DealerGridViewDataSource_SelectObject"
            TypeSupportsPaging="true"
            TypeSupportsSorting="true">
        </csla:CslaDataSource>
        <asp:GridView ID="DealerGridView" runat="server"
            AllowPaging="true"
            AllowSorting="true"
            AutoGenerateColumns="false"
            DataKeyNames="Id"
            DataSourceID="DealerGridViewDataSource"
            OnRowCommand="DealerGridView_RowCommand"
            OnDataBound="DealerGridView_DataBound">
            <Columns>
                <asp:BoundField SortExpression="Name" AccessibleHeaderText="Name" HeaderText="Name" DataField="Name" ReadOnly="true" />
                <asp:BoundField SortExpression="Address" AccessibleHeaderText="Address" HeaderText="Address" DataField="Address" ReadOnly="true" />
                <asp:BoundField SortExpression="Active" AccessibleHeaderText="Active" HeaderText="Active" DataField="Active" ReadOnly="true" />
                <asp:BoundField SortExpression="DealerGroupName" AccessibleHeaderText="Dealer Group Name" HeaderText="Dealer Group Name" DataField="DealerGroupName" ReadOnly="true" />
                <asp:ButtonField ButtonType="Button" Text="Edit" CommandName="Configure" ShowHeader="false" />
            </Columns>
        </asp:GridView>
        <p>
            <asp:Label ID="NoDealerText" runat="server" Visible="false">
                No dealerships matched your search criteria.
                Please try changing or removing your search criteria.
            </asp:Label>
        </p>
    </fieldset>

</asp:Content>
