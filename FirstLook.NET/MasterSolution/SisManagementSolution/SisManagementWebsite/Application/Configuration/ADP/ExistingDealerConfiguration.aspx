<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ExistingDealerConfiguration.aspx.cs" Inherits="Application_Configuration_ADP_ExistingDealerConfiguration" MasterPageFile="~/Application/Configuration/ADP/DealerMasterPage.master" %>
<%@ Register Assembly="FirstLook.Common.WebControls" Namespace="FirstLook.Common.WebControls.UI" TagPrefix="cwc" %>

<asp:Content ContentPlaceHolderID="TitleContentPlaceHolder" runat="server">
    <title>Existing Dealer Configuration</title>
</asp:Content>

<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
    <fieldset>
        <legend>Existing Dealer Configuration</legend>
            <p>
                Viewing: <asp:DropDownList AutoPostBack="true" OnSelectedIndexChanged="EnvironmentDropDownList_SelectedIndexChanged" runat="server" ID="EnvironmentDropDownList">
                    <asp:ListItem Text="Pre-Production" Value="2" />
                    <asp:ListItem Text="Production" Value="1" />
                </asp:DropDownList>
            </p>
            <asp:Panel runat="server" ID="MigrateSettingsPanel">
                <p>
                    <asp:LinkButton runat="server" ID="MigrateCopyToProdLinkButton" OnClick="MigrateCopyToProdLinkButton_Click" Text="Copy Profile To Production" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:LinkButton runat="server" ID="MigrateRestoreFromProdLinkButton" OnClick="MigrateRestoreFromProdLinkButton_Click" Text="Restore Profile From Production" />
                </p><br />
            </asp:Panel>
            <asp:BulletedList ID="ErrorList" runat="server" Visible="false" CssClass="ErrorList" />
            <cwc:TabContainer ActiveTabIndex="0" runat="server" ID="MainTabs">
                <cwc:TabPanel Text="Connections" runat="server" ID="ConnectionsTab">
                    <p>Host: <asp:HyperLink runat="server" ID="HostHyperLink" /></p>

                    <p><b>Lot Splitting</b></p>
                    <asp:Panel runat="server" ID="LotSplittingPanel">
                        <asp:Repeater runat="server" ID="LotSplittingRepeater" OnItemDataBound="LotSplittingRepeater_Bound">
                            <ItemTemplate>
                                Logon : <asp:Literal runat="server" Id="LogonLiteral" /><br />
                                Uses logon data splitting: <asp:Literal runat="server" Id="UsesLiteral" /><br />
                                <asp:Panel Visible="false" runat="server" ID="LogonLotSplittingPanel">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Company: <asp:Literal runat="server" Id="CompanyLiteral" /><br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lot: <asp:Literal runat="server" Id="LotLiteral" /><br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Stock (Pattern Type/Text): <asp:Literal runat="server" Id="StockLiteral" /><br />
                                </asp:Panel>
                                <br />
                            </ItemTemplate>
                        </asp:Repeater>
                        <br />
                    </asp:Panel>
                    
                    <p><b>Connections</b></p>
                    <asp:Panel runat="server" ID="AddConnectionPanel">
                        <p><asp:HyperLink runat="server" ID="NewConnectionHyperlink" NavigateUrl="~/Application/Configuration/ADP/SisConnection.aspx" Text="Click here to create a new connection." /></p>
                    <p>
                        <asp:Label ID="DmsHostSisConnectionsLabel" 
                            AssociatedControlID="DmsHostSisConnectionsDropDownList"
                            Text="Add an existing connection:" 
                            runat="server"/>
                        <asp:DropDownList ID="DmsHostSisConnectionsDropDownList"
                            OnDataBinding="DmsHostSisConnectionsDropDownList_DataBinding"
                            OnDataBound="DmsHostSisConnectionsDropDownList_DataBound"
                            runat="server" />
                        <asp:Button runat="server" Text="Add" ID="AddConnectionButton" OnClick="AddConnectionButtonClick" />
                    </p>
                    </asp:Panel>
                    <csla:CslaDataSource ID="DealerSisConnectionsGridViewDataSource" 
                        OnSelectObject="DealerSisConnectionsGridViewDataSource_SelectObject" 
                        TypeSupportsPaging="false" 
                        TypeSupportsSorting="false"
                        runat="server"  />
                    <asp:GridView ID="DealerSisConnectionsGridView"
                        DataSourceID="DealerSisConnectionsGridViewDataSource"
                        DataKeyNames="SisConnectionId"
                        OnRowCommand="DealerSisConnectionsGridView_RowCommand"
                        OnDataBound="DealerSisConnectionsGridView_DataBound"
                        OnRowDataBound="DealerSisConnectionsGridView_RowDataBound"
                        AutoGenerateColumns="false" 
                        AllowPaging="true" 
                        AllowSorting="true" 
                        runat="server" >
                        <Columns>
                            <asp:BoundField AccessibleHeaderText="UserName" HeaderText="UserName" DataField="SisCredentialUserName" ReadOnly="true" />
                            <asp:BoundField AccessibleHeaderText="AccountingLogon" HeaderText="AccountingLogon" DataField="AccountingLogon" ReadOnly="true" />
                            <asp:BoundField AccessibleHeaderText="FinanceLogon" HeaderText="FinanceLogon" DataField="FinanceLogon" ReadOnly="true" />
                            <asp:BoundField AccessibleHeaderText="Create Date" HeaderText="Created" DataField="Created" ReadOnly="true" />
                            <asp:BoundField AccessibleHeaderText="Modified Date" HeaderText="Modified" DataField="Modified" ReadOnly="true" />
                            <asp:ButtonField ButtonType="Button" Text="Edit" CommandName="Edit" ShowHeader="false" />
                            <asp:ButtonField ButtonType="Button" Text="Remove" CommandName="Remove" ShowHeader="false" />
                        </Columns>
                    </asp:GridView>
                    <asp:Label runat="server" ID="NoDealerSisConnectionsLabel" Text="No current sis connections." />
                    <br />
                    <br />
                    <p><b>Accounting Logons</b></p>
                    <csla:CslaDataSource ID="AccountingLogonGridViewDataSource" 
                        OnSelectObject="AccountingLogonGridViewDataSource_SelectObject" 
                        TypeSupportsPaging="false" 
                        TypeSupportsSorting="false"
                        runat="server"  />
                    <asp:GridView ID="AccountingLogonGridView" 
                        DataSourceID="AccountingLogonGridViewDataSource"
                        DataKeyNames="Id"
                        OnRowDataBound="AccountingLogonGridView_RowDataBound"
                        OnRowCommand="AccountingLogonGridView_RowCommand"
                        AutoGenerateColumns="false"
                        AllowPaging="false"
                        AllowSorting="true"
                        runat="server">
                        <Columns>
                            <asp:BoundField AccessibleHeaderText="Name" HeaderText="Name" DataField="Name" SortExpression="Name" ReadOnly="true" />
                            <asp:ButtonField ButtonType="Link" ShowHeader="True" HeaderText="Logon Data Splitting" />
                            <asp:ButtonField ButtonType="Button" Text="Edit Logon Data Splitting" CommandName="LogonDataSplitting" ShowHeader="false" />
                            <asp:ButtonField ButtonType="Link" Text="Accounts" CommandName="Accounts" ShowHeader="false" />
                            <asp:ButtonField ButtonType="Link" Text="Journals" CommandName="Journals" ShowHeader="false" />
                            <asp:ButtonField ButtonType="Link" Text="Schedules" CommandName="Schedules" ShowHeader="false" />
                        </Columns>
                    </asp:GridView>
                </cwc:TabPanel>
                <cwc:TabPanel Text="Data" runat="server" ID="DataTab">
                    <table width="100%" cellpadding="5" cellspacing="0">
                        <tr>
                            <td valign="top">
                                <asp:Label runat="server" Visible="false" ID="DataRequirementSavedLabel" Text="<h4>The data requirement has been saved.</h4><br /><br />" />
                                <p><asp:Label runat="server" ID="DataRequirementLabel" /></p>
                                <p><asp:CheckBox runat="server" ID="ExecuteDailyLoadCheckbox" Text="Include in daily load process" /></p>
                                <p><asp:CheckBox runat="server" ID="UseDealSalePriceCheckbox" Text="Use Deal Sale Price" /></p>
                                <p><asp:CheckBox runat="server" ID="UseDealUnitCostCheckbox" Text="Use Deal Unit Cost" /></p>
                                <p><asp:CheckBox runat="server" ID="UseDealFEGCheckbox" Text="Use Deal FEG" /></p>
                                <p><asp:CheckBox runat="server" ID="UseDealBEGCheckbox" Text="Use Deal BEG" /></p>
                                <p><asp:CheckBox runat="server" ID="CalcSalePriceFromDealCheckbox" Text="Calculate Sale Price From Deal" /></p>
                                <p><asp:CheckBox runat="server" ID="UseJournalVINMappingCheckBox" Text="Use Journal VIN Mapping" /></p>
                                <p></p>
                                <p>
                                    <table>
                                        <tr>
                                            <td>Daily Inventory Download Start Time:</td>
                                            <td>
                                                <asp:DropDownList runat="server" ID="DailyInventoryLoadHour" /> : 
                                                <asp:DropDownList runat="server" ID="DailyInventoryLoadMinute" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Daily Sales Download Start Time:</td>
                                            <td>
                                                <asp:DropDownList runat="server" ID="DailySalesLoadHour" /> : 
                                                <asp:DropDownList runat="server" ID="DailySalesLoadMinute" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Inventory Load Priority:</td>
                                            <td>
                                                <asp:DropDownList runat="server" ID="InventoryLoadPriorityDropDownList" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Sales Load Priority:</td>
                                            <td>
                                                <asp:DropDownList runat="server" ID="SalesLoadPriorityDropDownList" />
                                            </td>
                                        </tr>
                                    </table>
                                </p>
                                <p></p>
                                <p><asp:Button runat="server" ID="SaveDataButton" Text="Save" OnClick="SaveDataButton_Click" /></p>
                                <p></p>
                                <hr />
                                <p></p>
                                <asp:Label runat="server" Visible="false" ID="StartDownloadLabel" Text="<h4>The download has been started.</h4><br /><br />" />
                                <p>
                                    <asp:Button runat="server" ID="DownloadStandardDataButton" Text="Download Standard Data" OnClick="DownloadStandardDataButton_Click" />
                                </p>
                                <p>
                                    <asp:Button runat="server" ID="DownloadHistoricalDataButton" Text="Download Historical Data" OnClick="DownloadHistoricalDataButton_Click" />
                                    &nbsp;&nbsp;&nbsp;
                                    starting from <asp:TextBox Columns="10" runat="server" ID="HistoricalDataRequirementTextBox" />
                                    <asp:Label runat="server" Visible="false" ID="HistoricalDataRequirementLabel" Text="<h4>Please enter a valid date no more than 3 years back.</h4>" />
                                </p>
                            </td>
                            <td valign="top">
                                <p>Summary of last run dealer load:</p>
                                Inventory:
                                <asp:Panel runat="server" ID="InventoryLoadPanel" Visible="false">
                                    <div style="padding-left:30px;">Last load date: <asp:Label runat="server" ID="InventoryLoadDateLabel" /></div>
                                    <div style="padding-left:30px;">Processed: <asp:Label runat="server" ID="InventoryLoadStatusLabel" /></div>
                                    <div style="padding-left:30px;">Number of queries: <asp:Label runat="server" ID="InventoryLoadQueryCountLabel" /></div>
                                    <div style="padding-left:30px;">Number of errored queries: <asp:Label runat="server" ID="InventoryLoadErroredQueriesCountLabel" /></div>
                                </asp:Panel>
                                <asp:Panel runat="server" ID="InventoryNoLoadPanel" Visible="false">
                                    <div style="padding-left:30px;">No past inventory load.</div>
                                </asp:Panel>
                                Sales:
                                <asp:Panel runat="server" ID="SalesLoadPanel" Visible="false">
                                    <div style="padding-left:30px;">Last load date: <asp:Label runat="server" ID="SalesLoadDateLabel" /></div>
                                    <div style="padding-left:30px;">Processed: <asp:Label runat="server" ID="SalesLoadStatusLabel" /></div>
                                    <div style="padding-left:30px;">Number of queries: <asp:Label runat="server" ID="SalesLoadQueryCountLabel" /></div>
                                    <div style="padding-left:30px;">Number of errored queries: <asp:Label runat="server" ID="SalesLoadErroredQueriesCountLabel" /></div>
                                </asp:Panel>
                                <asp:Panel runat="server" ID="SalesNoLoadPanel" Visible="false">
                                    <div style="padding-left:30px;">No past sales load.</div>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </cwc:TabPanel>
                <cwc:TabPanel Text="Reports" runat="server" ID="ReportsTab">
                    <p>Please choose the report you wish to run</p>
                    <p>1. Monthly Financials</p>
                    <p>2. DataLoad Raw History</p>
                    <p>3. Combined Tech QA</p>
                    <p>4. Inventory</p>
                    <p>5. Sales</p>
                    <p>6. Data Download Report</p>
                </cwc:TabPanel>
            </cwc:TabContainer>
    </fieldset>
</asp:Content>