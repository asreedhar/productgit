<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ExistingDmsHostSelection.aspx.cs" Inherits="Application_Configuration_ADP_ExistingDmsHostSelection" MasterPageFile="~/Application/Configuration/ADP/DealerMasterPage.master" %>

<asp:Content ContentPlaceHolderID="TitleContentPlaceHolder" runat="server">
    <title>Existing DMS Host Selection</title>
</asp:Content>

<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
    <fieldset>
        <legend>Existing DMS Host Selection</legend>
        <p>Please enter search criteria to limit your DMS host selection.</p>
        <p>
            <asp:Label ID="IPAddressLabel" runat="server" Text="IP Address:" AssociatedControlID="IPAddressByte1TextBox"></asp:Label>
             <asp:TextBox ID="IPAddressByte1TextBox" runat="server" MaxLength="3" CssClass="IPAddressByte"></asp:TextBox>
            . <asp:TextBox ID="IPAddressByte2TextBox" runat="server" MaxLength="3" CssClass="IPAddressByte"></asp:TextBox>
            . <asp:TextBox ID="IPAddressByte3TextBox" runat="server" MaxLength="3" CssClass="IPAddressByte"></asp:TextBox>
            . <asp:TextBox ID="IPAddressByte4TextBox" runat="server" MaxLength="3" CssClass="IPAddressByte"></asp:TextBox>
            <asp:Button ID="DmsHostSearchButton" runat="server" Text="Search" OnClick="DmsHostSearchButton_Click" />
            <asp:Button ID="ClearSearchButton" runat="server" Text="Clear" OnClick="ClearSearchButton_Click" />
        </p>
        <p>
            <asp:Label ID="TelephoneNumberLabel" runat="server" Text="Telephone Number:" AssociatedControlID="AreaCodeTextBox"></asp:Label>
             <asp:TextBox ID="AreaCodeTextBox" runat="server" MaxLength="3" CssClass="AreaCode"></asp:TextBox>
            - <asp:TextBox ID="ExchangeCodeTextBox" runat="server" MaxLength="3" CssClass="ExchangeCode"></asp:TextBox>
            - <asp:TextBox ID="StationCodeTextBox" runat="server" MaxLength="4" CssClass="StationCode"></asp:TextBox>
        </p>
        <p>
            <asp:Label ID="DmsHostNameLabel" runat="server" Text="DMS Host Name:" AssociatedControlID="DmsHostNameTextBox"></asp:Label>
            <asp:TextBox ID="DmsHostNameTextBox" runat="server"></asp:TextBox>
        </p>
        <csla:CslaDataSource ID="TimeZoneDataSource" runat="server"
            OnSelectObject="TimeZoneDataSource_SelectObject">
        </csla:CslaDataSource>
        <p>
            <asp:Label ID="TimeZoneLabel" runat="server" Text="Time Zone:" AssociatedControlID="TimeZoneDropDownList"></asp:Label>
            <asp:DropDownList ID="TimeZoneDropDownList" runat="server" AppendDataBoundItems="true" DataSourceID="TimeZoneDataSource" DataTextField="Name" DataValueField="Id">
                <asp:ListItem Text="Any" Value="0" />
            </asp:DropDownList>
        </p>
        <p>
            Please choose an existing DMS host from the table below.  
            <asp:HyperLink ID="NewDmsHostLink" runat="server" OnInit="NewDmsHostLink_Init">Alternatively, click here to create a new DMS Host record</asp:HyperLink>.
        </p>
        <csla:CslaDataSource ID="DmsHostGridViewDataSource" runat="server"
            OnSelectObject="DmsHostGridViewDataSource_SelectObject"
            TypeSupportsPaging="true"
            TypeSupportsSorting="true">
        </csla:CslaDataSource>
        <asp:GridView ID="DmsHostGridView" runat="server"
            AllowPaging="true"
            AllowSorting="true"
            AutoGenerateColumns="false"
            DataKeyNames="Id"
            DataSourceID="DmsHostGridViewDataSource"
            OnRowDataBound="DmsHostGridView_RowDataBound"
            OnDataBound="DmsHostGridView_DataBound"
            OnRowCommand="DmsHostGridView_RowCommand">
            <Columns>
                <asp:BoundField SortExpression="IPAddress" AccessibleHeaderText="IP Address" HeaderText="IP Address" DataField="IPAddress" ReadOnly="true" />
                <asp:BoundField SortExpression="Name" AccessibleHeaderText="Name" HeaderText="Name" DataField="Name" ReadOnly="true" />
                <asp:TemplateField AccessibleHeaderText="TimeZone" HeaderText="TimeZone">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="TimeZoneLabel" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:ButtonField ButtonType="Button" Text="Select" CommandName="Select" ShowHeader="false" />
                <asp:ButtonField ButtonType="Button" Text="Edit" CommandName="Edit" ShowHeader="false" />
            </Columns>
        </asp:GridView>
        <p>
            <asp:Label ID="NoDmsHostText" runat="server" Visible="false">
                No DMS Hosts matched your search criteria.
                Please try changing or removing your search criteria.
            </asp:Label>
        </p>
    </fieldset>
</asp:Content>
