<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DmsHost.aspx.cs" Inherits="Application_Configuration_ADP_DmsHost" MasterPageFile="~/Application/Configuration/ADP/DealerMasterPage.master" %>

<asp:Content ContentPlaceHolderID="TitleContentPlaceHolder" runat="server">
    <title>DMS Host</title>
</asp:Content>

<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
    <csla:CslaDataSource ID="HourDataSource" runat="server" OnSelectObject="HourDataSource_SelectObject" />
    <csla:CslaDataSource ID="MinuteDataSource" runat="server" OnSelectObject="MinuteDataSource_SelectObject" />
    <csla:CslaDataSource ID="TimeZoneDataSource" runat="server" OnSelectObject="TimeZoneDataSource_SelectObject" />
    <fieldset>
        <legend>DMS Host</legend>
        <asp:BulletedList ID="ErrorList" runat="server" Visible="false" CssClass="ErrorList">
        </asp:BulletedList>        
        <p>Please enter the DMS Host information in the form below.</p>
        <div class="ColumnOne">
        <p>
            <asp:Label ID="DmsHostNameLabel" runat="server" Text="Name: " AssociatedControlID="DmsHostNameTextBox"></asp:Label>
            <asp:TextBox ID="DmsHostNameTextBox" runat="server" MaxLength="50" ></asp:TextBox>
        </p>
        <p>
            <asp:Label ID="IPAddressLabel" runat="server" Text="IP Address: " AssociatedControlID="IPAddressByte1TextBox"></asp:Label>
              <asp:TextBox ID="IPAddressByte1TextBox" runat="server" CssClass="IPAddressByte" MaxLength="3" ></asp:TextBox>
            . <asp:TextBox ID="IPAddressByte2TextBox" runat="server" CssClass="IPAddressByte" MaxLength="3" ></asp:TextBox>
            . <asp:TextBox ID="IPAddressByte3TextBox" runat="server" CssClass="IPAddressByte" MaxLength="3" ></asp:TextBox>
            . <asp:TextBox ID="IPAddressByte4TextBox" runat="server" CssClass="IPAddressByte" MaxLength="3" ></asp:TextBox>
        </p>
        <p>
            <asp:Label ID="Telephone1Label" runat="server" Text="Dial-Up Phone #1: " AssociatedControlID="Telephone1AreaCodeTextBox"></asp:Label>
              <asp:TextBox ID="Telephone1AreaCodeTextBox" runat="server" CssClass="AreaCode" MaxLength="3" ></asp:TextBox>
            - <asp:TextBox ID="Telephone1ExchangeCodeTextBox" runat="server" CssClass="ExchangeCode" MaxLength="3" ></asp:TextBox>
            - <asp:TextBox ID="Telephone1StationCodeTextBox" runat="server" CssClass="StationCode" MaxLength="4" ></asp:TextBox>
        </p>
        <p>
            <asp:Label ID="Telephone2Label" runat="server" Text="Dial-Up Phone #2: " AssociatedControlID="Telephone2AreaCodeTextBox"></asp:Label>
              <asp:TextBox ID="Telephone2AreaCodeTextBox" runat="server" CssClass="AreaCode" MaxLength="3" ></asp:TextBox>
            - <asp:TextBox ID="Telephone2ExchangeCodeTextBox" runat="server" CssClass="ExchangeCode" MaxLength="3" ></asp:TextBox>
            - <asp:TextBox ID="Telephone2StationCodeTextBox" runat="server" CssClass="StationCode" MaxLength="4" ></asp:TextBox>
        </p>
        <p>
            <asp:Label ID="CredentialUserNameLabel" runat="server" Text="DMS User Name: " AssociatedControlID="CredentialUserNameTextBox"></asp:Label>
            <asp:TextBox ID="CredentialUserNameTextBox" runat="server" MaxLength="255" ></asp:TextBox>
        </p>
        <p>
            <asp:Label ID="CredentialPasswordLabel" runat="server" Text="DMS Password: " AssociatedControlID="CredentialPasswordTextBox"></asp:Label>
            <asp:TextBox ID="CredentialPasswordTextBox" runat="server" MaxLength="255" ></asp:TextBox>
        </p>
        <p>
            <asp:Label ID="BackupStartTimeLabel" runat="server" Text="Backup Start Time: " AssociatedControlID="BackupStartHourDropDownList"></asp:Label>
            <asp:DropDownList ID="BackupStartHourDropDownList" runat="server" DataSourceID="HourDataSource" OnDataBound="BackupStartHourDropDownList_DataBound" DataTextField="Text" DataValueField="Value" ></asp:DropDownList>
            <asp:DropDownList ID="BackupStartMinuteDropDownList" runat="server" DataSourceID="MinuteDataSource" OnDataBound="BackupStartMinuteDropDownList_DataBound" DataTextField="Text" DataValueField="Value" ></asp:DropDownList>
        </p>
        <p>
            <asp:Label ID="BackupEndTimeLabel" runat="server" Text="Backup End Time: " AssociatedControlID="BackupEndHourDropDownList"></asp:Label>
            <asp:DropDownList ID="BackupEndHourDropDownList" runat="server" DataSourceID="HourDataSource" OnDataBound="BackupEndHourDropDownList_DataBound" DataTextField="Text" DataValueField="Value" ></asp:DropDownList>
            <asp:DropDownList ID="BackupEndMinuteDropDownList" runat="server" DataSourceID="MinuteDataSource" OnDataBound="BackupEndMinuteDropDownList_DataBound" DataTextField="Text" DataValueField="Value" ></asp:DropDownList>
        </p>
        <p>
            <asp:Label ID="TimeZoneLabel" runat="server" Text="Time Zone:" AssociatedControlID="TimeZoneDropDownList"></asp:Label>
            <asp:DropDownList ID="TimeZoneDropDownList" runat="server"
                    AppendDataBoundItems="true"
                    DataSourceID="TimeZoneDataSource"
                    DataTextField="Name"
                    DataValueField="Id"
                    OnDataBound="TimeZoneDropDownList_DataBound" >
                <asp:ListItem Text="Any" Value="0" />
            </asp:DropDownList>
        </p>
        <p>
            <asp:Label ID="BackupFollowsDaylightSavingsTimeLabel" runat="server" Text="Follows Daylight Savings Time: " AssociatedControlID="BackupFollowsDaylightSavingsTimeRadioButtonList"></asp:Label>
            <asp:RadioButtonList ID="BackupFollowsDaylightSavingsTimeRadioButtonList" runat="server" CssClass="BooleanRadioButtonList" RepeatColumns="2" RepeatDirection="Horizontal" >
                <asp:ListItem Text="Yes" Value="True" />
                <asp:ListItem Text="No" Value="False" />
            </asp:RadioButtonList>
        </p>
        <p>
            <asp:Label ID="ConnectionLimitBusinessHoursLabel" runat="server" AssociatedControlID="ConnectionLimitBusinessHoursTextBox">Connection Limit <br /><small>(Business Hours)</small>: </asp:Label>
            <asp:TextBox ID="ConnectionLimitBusinessHoursTextBox" runat="server" MaxLength="2" CssClass="ConnectionLimit" ></asp:TextBox>
        </p>
        <p>
            <asp:Label ID="ConnectionLimitClosedHoursLabel" runat="server" AssociatedControlID="ConnectionLimitClosedHoursTextBox">Connection Limit <br /><small>(Off Hours)</small>: </asp:Label>
            <asp:TextBox ID="ConnectionLimitClosedHoursTextBox" runat="server" MaxLength="2" CssClass="ConnectionLimit" ></asp:TextBox>
        </p>       
        </div>
        <div class="ColumnTwo">
            <fieldset>
                <legend>Contact Information</legend>
                <p>
                    <asp:Label ID="ContactNameLabel" runat="server" Text="Name: " AssociatedControlID="ContactNameTextBox"></asp:Label>
                    <asp:TextBox ID="ContactNameTextBox" runat="server" MaxLength="255" ></asp:TextBox>
                </p>
                <p>
                    <asp:Label ID="ContactTelephoneLandLabel" runat="server" AssociatedControlID="ContactTelephoneLandAreaCodeTextBox">Office Phone # <small>(Land)</small> :</asp:Label>
                      <asp:TextBox ID="ContactTelephoneLandAreaCodeTextBox" runat="server" CssClass="AreaCode" MaxLength="3" ></asp:TextBox>
                    - <asp:TextBox ID="ContactTelephoneLandExchangeCodeTextBox" runat="server" CssClass="ExchangeCode" MaxLength="3" ></asp:TextBox>
                    - <asp:TextBox ID="ContactTelephoneLandStationCodeTextBox" runat="server" CssClass="StationCode" MaxLength="4" ></asp:TextBox>
                </p>
                <p>
                    <asp:Label ID="ContactTelephoneCellLabel" runat="server" AssociatedControlID="ContactTelephoneCellAreaCodeTextBox">Office Phone # <small>(Cell)</small>:</asp:Label>
                      <asp:TextBox ID="ContactTelephoneCellAreaCodeTextBox" runat="server" CssClass="AreaCode" MaxLength="3" ></asp:TextBox>
                    - <asp:TextBox ID="ContactTelephoneCellExchangeCodeTextBox" runat="server" CssClass="ExchangeCode" MaxLength="3" ></asp:TextBox>
                    - <asp:TextBox ID="ContactTelephoneCellStationCodeTextBox" runat="server" CssClass="StationCode" MaxLength="4" ></asp:TextBox>
                </p>
                <p>
                    <asp:Label ID="ContactEmailAddressLabel" runat="server" AssociatedControlID="ContactEmailAddressTextBox">Email Address:</asp:Label>
                    <asp:TextBox ID="ContactEmailAddressTextBox" runat="server" MaxLength="255" ></asp:TextBox>
                </p>
            </fieldset>
        </div>
         <p class="controls">
            <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click"  />
            <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" />
        </p>
    </fieldset>
    
</asp:Content>
