<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DownloadDmsReferenceData.aspx.cs" Inherits="Application_Configuration_ADP_DownloadDmsReferenceData" MasterPageFile="~/Application/Configuration/ADP/DealerMasterPage.master" %>

<asp:Content ContentPlaceHolderID="TitleContentPlaceHolder" runat="server">
    <title>Download DMS Reference Data</title>
</asp:Content>

<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
    <fieldset>
        <legend>Download DMS Reference Data</legend>
        <asp:Panel runat="server" Visible="false" ID="ProcessingPanel">
            <p>Please wait a few moments while we download the reference data sets for the selected connection.</p>
            <p>This page will refresh itself automatically.</p>
            <p>Total elapsed time : <asp:Label runat="server" ID="ElapsedTimeLabel" />.</p>
        </asp:Panel>
        <asp:Panel runat="server" Visible="true" ID="StatusPanel">
            <dl>
                <dt>Accounts</dt>
                <dd><asp:Label runat="server" ID="AccountLabel" /></dd>
                <dt>Journals</dt>
                <dd><asp:Label runat="server" ID="JournalsLabel" /></dd>
                <dt>Schedules</dt>
                <dd><asp:Label runat="server" ID="SchedulesLabel" /></dd>
            </dl>
        </asp:Panel>
        <asp:Panel runat="server" Visible="false" ID="ErrorPanel" CssClass="ErrorPanel">
            The following errors have occurred:<br /><br />
                <asp:Panel runat="server" Visible="false" ID="QueryErrorPanel">
                    <b>Account Errors</b><br />
                    <asp:Label runat="server" ID="AccountErrorsLabel" />
                    <br />
                    <b>Journal Errors</b><br />
                    <asp:Label runat="server" ID="JournalErrorsLabel" />
                    <br />
                    <b>Schedule Errors</b><br />
                    <asp:Label runat="server" ID="ScheduleErrorsLabel" />
                </asp:Panel>
                <asp:Panel runat="server" Visible="false" ID="LoginErrorPanel">
                    <h4>Your SisConnection contains an invalid username/password.</h4>
                    <h4><asp:HyperLink Font-Underline="true" runat="server" ID="LoginErrorHyperlink" OnInit="LoginErrorHyperlink_Init" Text="Please click here to go back and change it." /> </h4>
                </asp:Panel>
        </asp:Panel>
        <asp:Panel runat="server" Visible="false" ID="CompletePanel">
            <br />
            <p>All accounts, journals, and schedules were successfully obtained.</p>
            <p><asp:Button ID="ContinueButton" runat="server" OnClick="ContinueButton_Click" Text="Continue" /></p>
        </asp:Panel>
        <asp:Panel runat="server" Visible="false" ID="ReRunPanel">
            <br />
            <p>You may rerun this download anytime by clicking the button below.</p>
            <p><asp:Button runat="server" ID="RerunQueriesButton" Text="Re-Run" OnClick="RerunQueriesButton_Click" /></p>
        </asp:Panel>
    </fieldset>
</asp:Content>
