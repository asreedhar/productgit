<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LogonDataSplittingConfiguration.aspx.cs" Inherits="Application_Configuration_ADP_LogonDataSplittingConfiguration" MasterPageFile="~/Application/Configuration/ADP/DealerMasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContentPlaceHolder" runat="server">
    <title>Logon Data Splitting Configuration</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
    <fieldset>
        <legend>Logon Data Splitting Configuration</legend>
        Does Logon <asp:Literal runat="server" ID="LogonLiteral" /> contain the data for more than one dealer?
        <asp:RadioButtonList AutoPostBack="true" runat="server" ID="UsesLogonDataSplittingRadioButtonList">
            <asp:ListItem Text="Yes" Value="True" />
            <asp:ListItem Text="No" Value="False" /> 
        </asp:RadioButtonList>
        <br />
        <br />
        <p class="controls"><asp:Button ID="UsesLogonDataSplittingSaveButton" runat="server" Text="Save" OnClick="UsesLogonDataSplittingSaveButton_Click" /></p>
        <cwc:TabContainer ID="LoginDataSplittingTabContainer" runat="server" ActiveTabIndex="0" Visible="false" HasEmptyState="false">
            <cwc:TabPanel ID="CompanyTabPanel" runat="server" Text="Company #">
                <p>Enter DMS Company Ids in the form below:</p>
                <p>DMS Company Id: <asp:TextBox runat="server" ID="DMSCompanyIdTextBox" /> <asp:Button ID="DMSCompanyIdAddButton" runat="server" Text="Add" OnClick="DMSCompanyIdAddButton_Click" /></p>
                <p>
                    DMS Company Ids:<br />
                    <csla:CslaDataSource ID="CompanyLoginDataSplittingGridViewDataSource" runat="server"
                        OnSelectObject="CompanyLoginDataSplittingGridView_SelectObject"
                        TypeSupportsPaging="false"
                        TypeSupportsSorting="false">
                    </csla:CslaDataSource>
                    <asp:GridView ID="CompanyLoginDataSplittingGridView" runat="server"
                        AllowPaging="false" 
                        AllowSorting="true"
                        AutoGenerateColumns="false"
                        DataKeyNames="Id"
                        OnDataBound="CompanyLoginDataSplittingGridView_DataBound"
                        OnRowDataBound="CompanyLoginDataSplittingGridView_RowDataBound"
                        OnRowCommand="CompanyLoginDataSplittingGridView_RowCommand"
                        DataSourceID="CompanyLoginDataSplittingGridViewDataSource">
                        <Columns>
                            <asp:TemplateField AccessibleHeaderText="DMS Company Id" HeaderText="DMS Company Id">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="DMSCompanyId" />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox runat="server" ID="DMSCompanyIdTextBox" />
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:CommandField ButtonType="Button" ShowDeleteButton="true" ShowEditButton="true" ShowCancelButton="true" />
                        </Columns>
                    </asp:GridView>
                    <p><asp:Label runat="server" ID="NoCompanyLoginDataSplitLabel" Text="No Company Login Data Splits" /></p>
                </p>
            </cwc:TabPanel>
            <cwc:TabPanel ID="LotTabPanel" runat="server" Text="Lot #">
                <p>Enter Lot Numbers in the form below:</p>
                <p>Lot Number: <asp:TextBox runat="server" ID="LotNumberTextBox" /> <asp:Button ID="LotNumberAddButton" runat="server" Text="Add" OnClick="LotNumberAddButton_Click" /></p>
                <p>
                    Lot Numbers:<br />
                    <csla:CslaDataSource ID="LotLoginDataSplittingGridViewDataSource" runat="server"
                        OnSelectObject="LotLoginDataSplittingGridView_SelectObject"
                        TypeSupportsPaging="false"
                        TypeSupportsSorting="false">
                    </csla:CslaDataSource>
                    <asp:GridView ID="LotLoginDataSplittingGridView" runat="server"
                        AllowPaging="false" 
                        AllowSorting="true"
                        AutoGenerateColumns="false"
                        DataKeyNames="Id"
                        OnDataBound="LotLoginDataSplittingGridView_DataBound"
                        OnRowDataBound="LotLoginDataSplittingGridView_RowDataBound"
                        OnRowCommand="LotLoginDataSplittingGridView_RowCommand"
                        DataSourceID="LotLoginDataSplittingGridViewDataSource">
                        <Columns>
                            <asp:TemplateField AccessibleHeaderText="Lot Number" HeaderText="Lot Number">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="LotNumber" />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox runat="server" ID="LotNumberTextBox" />
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:CommandField ButtonType="Button" ShowDeleteButton="true" ShowEditButton="true" ShowCancelButton="true" />
                        </Columns>
                    </asp:GridView>
                    <p><asp:Label runat="server" ID="NoLotLoginDataSplitLabel" Text="No Lot Login Data Splits" /></p>
                </p>
            </cwc:TabPanel>
            <cwc:TabPanel ID="StockTabPanel" runat="server" Text="Stock #">
                    <p>Enter Stock details in the form below:</p>
                    <p>
                        Stock Pattern Type: 
                        <asp:DropDownList runat="server" ID="StockPatternTypeDropDownList">
                            <asp:ListItem Text="Like" Value="LIKE" />
                            <asp:ListItem Text="Not Like" Value="NOT LIKE" />
                        </asp:DropDownList>
                    </p>
		            <p>Stock Pattern Text: <asp:TextBox runat="server" ID="StockPatternTextTextBox" /> <asp:Button ID="StockAddButton" runat="server" Text="Add" OnClick="StockAddButton_Click" /></p>
                    <div style="float:left; width:50%">
                        <p>
                            Stock details:<br />
                            <csla:CslaDataSource ID="StockLoginDataSplittingGridViewDataSource" runat="server"
                                OnSelectObject="StockLoginDataSplittingGridView_SelectObject"
                                TypeSupportsPaging="false"
                                TypeSupportsSorting="false">
                            </csla:CslaDataSource>
                            <asp:GridView ID="StockLoginDataSplittingGridView" runat="server"
                                AllowPaging="false" 
                                AllowSorting="true"
                                AutoGenerateColumns="false"
                                DataKeyNames="Id"
                                OnDataBound="StockLoginDataSplittingGridView_DataBound"
                                OnRowDataBound="StockLoginDataSplittingGridView_RowDataBound"
                                OnRowCommand="StockLoginDataSplittingGridView_RowCommand"
                                DataSourceID="StockLoginDataSplittingGridViewDataSource">
                                <Columns>
                                    <asp:TemplateField AccessibleHeaderText="StockPatternType" HeaderText="StockPatternType">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="StockPatternType" />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                        <asp:DropDownList runat="server" ID="StockPatternTypeDropDownList">
                                            <asp:ListItem Text="After" Value="After" />
                                            <asp:ListItem Text="Before" Value="Before" />
                                        </asp:DropDownList>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField AccessibleHeaderText="StockPatternText" HeaderText="StockPatternText">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="StockPatternText" />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox runat="server" ID="StockPatternTextTextBox" />
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ButtonType="Button" ShowDeleteButton="true" ShowEditButton="true" ShowCancelButton="true" />
                                </Columns>
                            </asp:GridView>
                            <p><asp:Label runat="server" ID="NoStockLoginDataSplitLabel" Text="No Stock Login Data Splits" /></p>
                        </div>
                        <div style="float:left; width:50%; margin-top:-50px">
                            <p><b><span style="font-size: 11pt"><span>Help for Logon Data splitting on Stock Number:</span></span></b></p>
                            <p>
                                <table border="1" cellpadding="0" cellspacing="0"  style="width: 100%;">
                                    <tr style="">
                                        <td><p align="center" ><b><span >Wildcard character</span></b></p></td>
                                        <td><p align="center" ><b><span >Description</span></b></p></td>
                                        <td><p align="center" ><b><span >Example</span></b></p></td>
                                    </tr>
                                    <tr>
                                        <td><p><span>%</span></p></td>
                                        <td><p><span>Any string of zero or more characters.</span></p></td>
                                        <td><p><span>'%A%' finds any stock number containing an 'A'.</span></p></td>
                                    </tr>
                                    <tr>
                                        <td><p><span>_ (underscore)</span></p></td>
                                        <td><p><span>Any single character.</span></p></td>
                                        <td><p><span>'_A%' finds all stock numbers with an 'A' as the second character.</span></p></td>
                                    </tr>
                                    <tr>
                                        <td><p><span>[ ]</span></p></td>
                                        <td ><p><span>Any single character within the specified range ([a-f]) or set ([abcdef]).</span></p></td>
                                        <td><p><span>'[ABC]%' finds all stock numbers starting with 'A' or 'B' or 'C'.</span></p></td>
                                    </tr>
                                    <tr>
                                        <td><p><span>[^]</span></p></td>
                                        <td><p><span>Any single character not within the specified range ([^a-f]) or set ([^abcdef]).</span></p></td>
                                        <td><p><span>'[^ABC]%' finds all stock numbers not starting with 'A' or 'B' or 'C'.</span></p></td>
                                    </tr>
                                </table>
                            </p>
                            <p></p>
                            <p>The pattern description cannot include single quotes.</p>
                            <p>If there are multiple 'Like' rules, a stock number must meet at least one of the rules.</p>
                            <p>If there are one or more 'Not Like' rules, a stock number cannot meet any of the 'Not Like' rules.</p>
                            <p>If there are 'Like' and 'Not Like' rules, a stock number must meet at least one 'Like' rule and cannot meet any 'Not Like' rule.</p>
                        </div>
                    </p>
            </cwc:TabPanel>
        </cwc:TabContainer>
    </fieldset>
</asp:Content>
