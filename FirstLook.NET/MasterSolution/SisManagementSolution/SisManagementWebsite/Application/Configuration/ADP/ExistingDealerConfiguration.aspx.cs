using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Web.UI.WebControls;
using Csla.Validation;
using Csla.Web;
using FirstLook.Sis.Management.DomainModel.Configuration;
using FirstLook.Sis.Management.DomainModel.Configuration.Adp;
using Environment = FirstLook.Sis.Management.DomainModel.Configuration.Environment;

public partial class Application_Configuration_ADP_ExistingDealerConfiguration : System.Web.UI.Page
{
    private DmsHost _dmsHost;
    private Dealer _dealer;

    private Dealer Dealer
    {
        get
        {
            if (_dealer == null)
            {
                string id = Request.QueryString["DealerId"];

                if (string.IsNullOrEmpty(id))
                {
                    throw new ApplicationException("Missing DealerId");
                }
                else
                {
                    _dealer = Dealer.GetDealer(int.Parse(id), Env);
                }
            }

            return _dealer;
        }
    }
    private DmsHost DmsHost
    {
        get
        {
            if (_dmsHost == null)
            {
                _dmsHost = DmsHost.GetDmsHost(Dealer.DmsHostId);
            }

            return _dmsHost;
        }
    }
    private Environment Env
    {
        get
        {
            return (Environment)Enum.ToObject(typeof(Environment), int.Parse(EnvironmentDropDownList.SelectedValue));
        }
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        BindAvailableEnvironments();

        DmsHostSisConnectionsDropDownList.DataBind();

        if (!IsPostBack)
        {
            ExecuteDailyLoadCheckbox.Checked = Dealer.ExecuteDailyLoad;
            UseDealSalePriceCheckbox.Checked = Dealer.UseDealSalePrice;
            UseDealUnitCostCheckbox.Checked = Dealer.UseDealUnitCost;
            UseDealFEGCheckbox.Checked = Dealer.UseDealFEG;
            UseDealBEGCheckbox.Checked = Dealer.UseDealBEG;
            CalcSalePriceFromDealCheckbox.Checked = Dealer.CalcSalePriceFromDeal;
            UseJournalVINMappingCheckBox.Checked = Dealer.UseJournalVINMapping;
        }

        //global setting for some environment-specific functionality
        MigrateSettingsPanel.Visible = Env == Environment.PreProduction;

        HostHyperLink.Enabled = Env == Environment.PreProduction;

        if (DmsHost.IPAddress != null)
        {
            HostHyperLink.Text = string.Format(CultureInfo.InvariantCulture, "{0} / {1}", DmsHost.Name, DmsHost.IPAddress.ToString());
        }
        else
        {
            HostHyperLink.Text = string.Format(CultureInfo.InvariantCulture, "{0} / {1}", DmsHost.Name, DmsHost.DialUpTelephoneNumber1);
        }
        HostHyperLink.NavigateUrl = string.Format(CultureInfo.InvariantCulture, "~/Application/Configuration/ADP/DmsHost.aspx?Mode=Update&DealerId={0}&DmsHostId={1}", Dealer.Id, DmsHost.Id);


        AddConnectionPanel.Visible = Env == Environment.PreProduction;
        NewConnectionHyperlink.NavigateUrl = string.Format(CultureInfo.InvariantCulture, "~/Application/Configuration/ADP/SisConnection.aspx?Mode=Insert&DealerId={0}&DmsHostId={1}", Dealer.Id, DmsHost.Id);

        AddConnectionPanel.Visible = Env == Environment.PreProduction;
        DataRequirementLabel.Text = Env == Environment.PreProduction ? "Please select the amount of historical data needed." : "Historical Data Requirement.";
        SaveDataButton.Visible = Env == Environment.PreProduction;
        ExecuteDailyLoadCheckbox.Enabled = Env == Environment.PreProduction;
        UseDealBEGCheckbox.Enabled = Env == Environment.PreProduction;
        UseDealFEGCheckbox.Enabled = Env == Environment.PreProduction;
        UseDealSalePriceCheckbox.Enabled = Env == Environment.PreProduction;
        UseDealUnitCostCheckbox.Enabled = Env == Environment.PreProduction;
        CalcSalePriceFromDealCheckbox.Enabled = Env == Environment.PreProduction;
        UseJournalVINMappingCheckBox.Enabled = Env == Environment.PreProduction;
        DailyInventoryLoadHour.Enabled = Env == Environment.PreProduction;
        DailyInventoryLoadMinute.Enabled = Env == Environment.PreProduction;
        DailySalesLoadHour.Enabled = Env == Environment.PreProduction;
        DailySalesLoadMinute.Enabled = Env == Environment.PreProduction;
        InventoryLoadPriorityDropDownList.Enabled = Env == Environment.PreProduction;
        SalesLoadPriorityDropDownList.Enabled = Env == Environment.PreProduction;


        if (!IsPostBack)
        {
            for (int i = 0; i < 24; i++)
            {
                DailyInventoryLoadHour.Items.Add(new ListItem(i.ToString(), i.ToString()));
                DailySalesLoadHour.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            for (int i = 0; i < 60; i++)
            {
                DailyInventoryLoadMinute.Items.Add(new ListItem(i.ToString(), i.ToString()));
                DailySalesLoadMinute.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            for (int i = 1; i < 256; i++)
            {
                InventoryLoadPriorityDropDownList.Items.Add(new ListItem(i.ToString(), i.ToString()));
                SalesLoadPriorityDropDownList.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }

            DealerLoadConfiguration conf = DealerLoadConfiguration.GetDealerLoadConfiguration(this.Dealer);
            if(conf != null)
            {
                DailyInventoryLoadHour.SelectedValue = conf.DailyInventoryLoadHour.ToString();
                DailyInventoryLoadMinute.SelectedValue = conf.DailyInventoryLoadMinute.ToString();
                DailySalesLoadHour.SelectedValue = conf.DailySalesLoadHour.ToString();
                DailySalesLoadMinute.SelectedValue = conf.DailySalesLoadMinute.ToString();
                InventoryLoadPriorityDropDownList.SelectedValue = conf.InventoryLoadPriority.ToString();
                SalesLoadPriorityDropDownList.SelectedValue = conf.SalesLoadPriority.ToString();
            }
            else
            {
                DailyInventoryLoadHour.SelectedValue = Dealer.BusinessHoursEnd.Hour.ToString();
                DailyInventoryLoadMinute.SelectedValue = Dealer.BusinessHoursEnd.Minute.ToString();
                DailySalesLoadHour.SelectedValue = Dealer.BusinessHoursEnd.Hour.ToString();
                DailySalesLoadMinute.SelectedValue = Dealer.BusinessHoursEnd.Minute.ToString();
                InventoryLoadPriorityDropDownList.SelectedValue = "50";
                SalesLoadPriorityDropDownList.SelectedValue = "100";
            }
        }

        

        QuerySet inventoryQuerySet = QuerySet.GetLastRunDealerLoad(Dealer, "Inventory");
        InventoryNoLoadPanel.Visible = inventoryQuerySet.Id == Guid.Empty;
        InventoryLoadPanel.Visible = inventoryQuerySet.Id != Guid.Empty;
        if (inventoryQuerySet.Id != Guid.Empty)
        {
            InventoryLoadDateLabel.Text = inventoryQuerySet.LoadDate.ToShortDateString();
            InventoryLoadStatusLabel.Text = inventoryQuerySet.IsProcessed.ToString();
            InventoryLoadQueryCountLabel.Text = inventoryQuerySet.Queries.Count.ToString();
            InventoryLoadErroredQueriesCountLabel.Text = new List<Query>(inventoryQuerySet.Queries).FindAll(delegate(Query q) { return q.Status == QueryStatus.Error; }).Count.ToString();
        }

        QuerySet salesQuerySet = QuerySet.GetLastRunDealerLoad(Dealer, "Sales");
        SalesNoLoadPanel.Visible = salesQuerySet.Id == Guid.Empty;
        SalesLoadPanel.Visible = salesQuerySet.Id != Guid.Empty;
        if (salesQuerySet.Id != Guid.Empty)
        {
            SalesLoadDateLabel.Text = salesQuerySet.LoadDate.ToShortDateString();
            SalesLoadStatusLabel.Text = salesQuerySet.IsProcessed.ToString();
            SalesLoadQueryCountLabel.Text = salesQuerySet.Queries.Count.ToString();
            SalesLoadErroredQueriesCountLabel.Text = new List<Query>(salesQuerySet.Queries).FindAll(delegate(Query q) { return q.Status == QueryStatus.Error; }).Count.ToString();
        }
        DataRequirementSavedLabel.Visible = false;
        StartDownloadLabel.Visible = false;
        DataRequirementSavedLabel.Visible = false;
    }

    private void BindAvailableEnvironments()
    {
        //Check if dealer has production settings
        //if not, hide production as a choice in the environment drop down
        if (FirstLook.Sis.Management.DomainModel.Configuration.Adp.Dealer.GetDealer(int.Parse(Request.QueryString["DealerId"]), Environment.Production).DmsHostId == Guid.Empty)
        {
            if (EnvironmentDropDownList.Items.Count > 1)
            {
                EnvironmentDropDownList.Items.RemoveAt(1);
            }
            MigrateRestoreFromProdLinkButton.Visible = false;
        }
        else
        {
            if (EnvironmentDropDownList.Items.Count == 1)
            {
                EnvironmentDropDownList.Items.Add(new ListItem("Production", "1"));
            }
            MigrateRestoreFromProdLinkButton.Visible = true;
        }
    }

    protected void EnvironmentDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        //rebind the dealer sis connection gridview and the 
        //accounting logon gridview when changing environment
        //in order to show/hide the edit functionality
        DealerSisConnectionsGridView.DataBind();
        AccountingLogonGridView.DataBind();

        ExecuteDailyLoadCheckbox.Checked = Dealer.ExecuteDailyLoad;
        UseDealSalePriceCheckbox.Checked = Dealer.UseDealSalePrice;
        UseDealUnitCostCheckbox.Checked = Dealer.UseDealUnitCost;
        UseDealFEGCheckbox.Checked = Dealer.UseDealFEG;
        UseDealBEGCheckbox.Checked = Dealer.UseDealBEG;
        CalcSalePriceFromDealCheckbox.Checked = Dealer.CalcSalePriceFromDeal;
        UseJournalVINMappingCheckBox.Checked = Dealer.UseJournalVINMapping;

    }

    protected void MigrateCopyToProdLinkButton_Click(object sender, EventArgs e)
    {
        MigrateConfigurationCommand.Execute(Dealer.Id, Environment.PreProduction, Environment.Production);
        BindAvailableEnvironments();
    }

    protected void MigrateRestoreFromProdLinkButton_Click(object sender, EventArgs e)
    {
        MigrateConfigurationCommand.Execute(Dealer.Id, Environment.Production, Environment.PreProduction);
        BindAvailableEnvironments();
    }

    protected void DmsHostSisConnectionsDropDownList_DataBinding(object sender, EventArgs e)
    {
        DropDownList list = sender as DropDownList;
        list.Items.Clear();
        DmsHostSisConnectionCollection connections = DmsHost.SisConnections;
        DealerSisConnectionCollection dealerConnections = Dealer.Connections;
        foreach (SisConnection connection in connections)
        {
            if (!dealerConnections.Contains(connection.Id))
            {
                list.Items.Add(new ListItem(connection.ToString(), connection.Id.ToString()));
            }
        }
    }

    protected void DmsHostSisConnectionsDropDownList_DataBound(object sender, EventArgs e)
    {
        DropDownList list = sender as DropDownList;
        if (list != null)
        {
            if (list.Items.Count < 1)
            {
                DmsHostSisConnectionsLabel.Visible = false;
                list.Visible = false;
                AddConnectionButton.Visible = false;
            }
            else
            {
                DmsHostSisConnectionsLabel.Visible = true;
                list.Visible = true;
                AddConnectionButton.Visible = true;
            }
        }
    }

    protected void AddConnectionButtonClick(object sender, EventArgs e)
    {
        Guid sisConnectionId = new Guid(DmsHostSisConnectionsDropDownList.SelectedValue.ToString());
        Dealer.Connections.Assign(DmsHost.SisConnections.GetItem(sisConnectionId));
        DealerSave();
        DmsHostSisConnectionsDropDownList.DataBind();
        DealerSisConnectionsGridView.DataBind();
        AccountingLogonGridView.DataBind();
    }

    protected void DealerSisConnectionsGridViewDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        e.BusinessObject = Dealer.Connections;
    }

    protected void DealerSisConnectionsGridView_DataBound(object sender, EventArgs e)
    {
        NoDealerSisConnectionsLabel.Visible = (DealerSisConnectionsGridView.Rows.Count == 0);
    }

    protected void DealerSisConnectionsGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //hide edit and remove buttons for production mode
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[5].Visible = Env == Environment.PreProduction;
            e.Row.Cells[6].Visible = Env == Environment.PreProduction;
        }
    }

    protected void DealerSisConnectionsGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int selectedIndex = Convert.ToInt32(e.CommandArgument);
        GridView grid = sender as GridView;
        if (grid != null)
        {
            Guid sisconnid = new Guid(grid.DataKeys[selectedIndex].Value.ToString());
            if (e.CommandName.Equals("Edit"))
            {
                Response.Redirect(string.Format(CultureInfo.InvariantCulture, 
                    "~/Application/Configuration/ADP/SisConnection.aspx?Mode=Update&DealerId={0}&DmsHostId={1}&SisConnectionId={2}", 
                    Dealer.Id, DmsHost.Id, sisconnid), true);
            }
            else if (e.CommandName.Equals("Remove"))
            {
                Dealer.Connections.Remove(sisconnid);
                DealerSave();
                DmsHostSisConnectionsDropDownList.DataBind();
                DealerSisConnectionsGridView.DataBind();
                AccountingLogonGridView.DataBind();
            }

        }
    }

    protected void AccountingLogonGridViewDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        List<Logon> list = new List<Logon>();
        DealerSisConnectionCollection dealerConnections = Dealer.Connections;
        foreach (DealerSisConnection connection in dealerConnections)
        {
            if (!list.Contains(connection.AccountingLogon))
            {
                list.Add(connection.AccountingLogon);
            }
        }

        list.Sort(
            delegate(Logon x, Logon y)
            {
                if (e.SortDirection.Equals(ListSortDirection.Descending))
                {
                    return y.Name.CompareTo(x.Name);
                }
                else
                {
                    return x.Name.CompareTo(y.Name);
                }
            });

        e.BusinessObject = list;

        LotSplittingRepeater.DataSource = list;
        LotSplittingRepeater.DataBind();
    }

    protected void AccountingLogonGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string tab = e.CommandName;

        if (tab.Equals("Accounts") || tab.Equals("Journals") || tab.Equals("Schedules"))
        {
            GridView gridView = sender as GridView;
            if (gridView != null)
            {
                Guid accoutingLogonId = new Guid(gridView.DataKeys[Convert.ToInt32(e.CommandArgument)].Value.ToString());

                Response.Redirect(string.Format(CultureInfo.InvariantCulture,
                    "~/Application/Configuration/ADP/LogonDataExtractionConfiguration.aspx?Mode=Update&DealerId={0}&DmsHostId={1}&LogonId={2}&TabName={3}",
                    Dealer.Id, DmsHost.Id, accoutingLogonId, tab), true);
            }
        }
        else if (tab.Equals("LogonDataSplitting"))
        {
            GridView gridView = sender as GridView;
            if (gridView != null)
            {
                Logon logon = DmsHost.Logons.GetItem(new Guid(gridView.DataKeys[Convert.ToInt32(e.CommandArgument)].Value.ToString()));
                Guid cid = Guid.Empty;
                foreach (DealerSisConnection connection in Dealer.Connections)
                {
                    if (connection.AccountingLogon.Id == logon.Id)
                    {
                        cid = connection.SisConnectionId;
                    }
                }
                Response.Redirect(string.Format(CultureInfo.InvariantCulture,
                    "~/Application/Configuration/ADP/LogonDataSplittingConfiguration.aspx?Mode=Update&DealerId={0}&DmsHostId={1}&SisConnectionId={2}",
                    Dealer.Id, DmsHost.Id, cid), true);
            }
        }
    }

    protected void AccountingLogonGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[2].Visible = Env == Environment.PreProduction;
            e.Row.Cells[3].Visible = Env == Environment.PreProduction;
            e.Row.Cells[4].Visible = Env == Environment.PreProduction;
            e.Row.Cells[5].Visible = Env == Environment.PreProduction;

            Logon logon = DmsHost.Logons.GetItem(new Guid((sender as GridView).DataKeys[e.Row.RowIndex].Value.ToString()));
            // 04/01/11 DMW - Added handling of case where logon object is null
            if (logon != null && logon.UsesLogonDataSplitting.HasValue)
            {
                e.Row.Cells[1].Text += logon.UsesLogonDataSplitting.Value;
            }
            else
            {
                e.Row.Cells[1].Text += "Unknown";
                e.Row.Cells[2].Controls.Clear();

                // 04/01/11 DMW - Added error message
                if (logon == null)
                {
                    ErrorList.Visible = true;
                    ErrorList.Items.Add("This dealer has one or more connections to host(s) that are not associated with this dealer. Recommended resolution: delete the connections and reconfigure the dealer.");
                }
            }
        }
    }

    protected void SaveDataButton_Click(object sender, EventArgs e)
    {
        Dealer.ExecuteDailyLoad = ExecuteDailyLoadCheckbox.Checked;
        Dealer.UseDealSalePrice = UseDealSalePriceCheckbox.Checked;
        Dealer.UseDealUnitCost = UseDealUnitCostCheckbox.Checked;
        Dealer.UseDealFEG = UseDealFEGCheckbox.Checked;
        Dealer.UseDealBEG = UseDealBEGCheckbox.Checked;
        Dealer.CalcSalePriceFromDeal = CalcSalePriceFromDealCheckbox.Checked;
        Dealer.UseJournalVINMapping = UseJournalVINMappingCheckBox.Checked;

        Dealer.Save();


        DealerLoadConfiguration conf = DealerLoadConfiguration.GetDealerLoadConfiguration(this.Dealer);
        conf.DailyInventoryLoadHour = int.Parse(DailyInventoryLoadHour.SelectedValue);
        conf.DailyInventoryLoadMinute = int.Parse(DailyInventoryLoadMinute.SelectedValue);
        conf.DailySalesLoadHour = int.Parse(DailySalesLoadHour.SelectedValue);
        conf.DailySalesLoadMinute = int.Parse(DailySalesLoadMinute.SelectedValue);
        conf.InventoryLoadPriority = int.Parse(InventoryLoadPriorityDropDownList.SelectedValue);
        conf.SalesLoadPriority = int.Parse(SalesLoadPriorityDropDownList.SelectedValue);
        conf.Save();

        DataRequirementSavedLabel.Visible = true;
    }

    protected void DownloadStandardDataButton_Click(object sender, EventArgs e)
    {
        DownloadTestDataCommand.Execute(Dealer);
        StartDownloadLabel.Visible = true;
    }

    protected void DownloadHistoricalDataButton_Click(object sender, EventArgs e)
    {
        DateTime req;

        if (!DateTime.TryParse(HistoricalDataRequirementTextBox.Text, out req)
            || DateTime.Now.AddYears(-3) > req  
            || req > DateTime.Now)
        {
            HistoricalDataRequirementLabel.Visible = true;
        }
        else
        {
            DownloadHistoricalDataCommand.Execute(Dealer, req);
            StartDownloadLabel.Visible = true;
            HistoricalDataRequirementLabel.Visible = false;
        }
    }

    protected void DealerSave()
    {
        ErrorList.Items.Clear();
        ErrorList.Visible = false;
        
        if (Dealer.IsValid)
        {
            try
            {
                Dealer.Save();
            }
            catch (Exception e)
            {
                ErrorList.Visible = true;
                ErrorList.Items.Add(e.Message);
                _dealer = null; //reset the dealer
            }
        }
        else
        {
            ErrorList.Visible = true;
            foreach (BrokenRule rule in Dealer.BrokenRulesCollection)
            {
                ErrorList.Items.Add(rule.Description);
            }
        }
    }

    protected void LotSplittingRepeater_Bound(object sender, RepeaterItemEventArgs e)
    {
        Logon l = (e.Item.DataItem as Logon);
        (e.Item.FindControl("LogonLiteral") as Literal).Text = l.Name;
        (e.Item.FindControl("UsesLiteral") as Literal).Text = l.UsesLogonDataSplitting.HasValue ? l.UsesLogonDataSplitting.Value.ToString() : "Not Set";
        if (l.UsesLogonDataSplitting.HasValue && l.UsesLogonDataSplitting.Value)
        {
            (e.Item.FindControl("LogonLotSplittingPanel") as Panel).Visible = true;
            DealerLogonDataSplitConfiguration c = DealerLogonDataSplitConfiguration.GetDealerLogonDataSplitConfiguration(this.Dealer, l);
            foreach (DealerCompanyLogonDataSplit dc in c.DealerCompanyLogonDataSplitCollection)
            {
                (e.Item.FindControl("CompanyLiteral") as Literal).Text += dc.CompanyLogonDataSplit.DmsCompanyId + ", ";
            }
            foreach (DealerLotLogonDataSplit dc in c.DealerLotLogonDataSplitCollection)
            {
                (e.Item.FindControl("LotLiteral") as Literal).Text += dc.LotLogonDataSplit.LotNumber + ", ";
            }
            foreach (DealerStockLogonDataSplit dc in c.DealerStockLogonDataSplitCollection)
            {
                (e.Item.FindControl("StockLiteral") as Literal).Text += dc.StockLogonDataSplit.StockPatternType + " - " + dc.StockLogonDataSplit.StockPatternText + ", ";
            }
        }
    }
}