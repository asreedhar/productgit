using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI.WebControls;
using Csla.Web;
using Csla.Validation;
using FirstLook.Sis.Management.DomainModel.Configuration.Adp;
using Environment = FirstLook.Sis.Management.DomainModel.Configuration.Environment;

public partial class Application_Configuration_ADP_LogonDataExtractionConfiguration : System.Web.UI.Page
{
    private int _dealerId;
    private Logon _accountingLogon;

    private bool IsUpdate
    {
        get
        {
            return (string.Equals(Request.QueryString["Mode"], "Update"));
        }
    }
    private int DealerId
    {
        get
        {
            string id = Request.QueryString["DealerId"];

            if (string.IsNullOrEmpty(id))
            {
                throw new ApplicationException("Missing DealerId");
            }
            else
            {
                _dealerId = int.Parse(id);
            }

            return _dealerId;
        }
    }
    private static Environment Environment
    {
        get
        {
            return Environment.PreProduction;
        }
    }
    private Logon AccountingLogon
    {
        get
        {
            if (_accountingLogon == null)
            {
                string dmsHostId = Request.QueryString["DmsHostId"];
                string sisConnectionId = Request.QueryString["SisConnectionId"];
                string accountingLogonId = Request.QueryString["LogonId"];

                if (string.IsNullOrEmpty(dmsHostId))
                {
                    throw new ApplicationException("Missing DmsHostId");
                }
                else
                {
                    DmsHost _dmsHost = DmsHost.GetDmsHost(new Guid(dmsHostId));
                    if (!string.IsNullOrEmpty(sisConnectionId))
                    {
                        Guid sisConnectionGuid = new Guid(sisConnectionId);
                        if (_dmsHost.SisConnections.Contains(sisConnectionGuid))
                        {
                            SisConnection sisConn = _dmsHost.SisConnections.GetItem(sisConnectionGuid);
                            _accountingLogon = sisConn.AccountingLogon;
                        }
                        else
                        {
                            throw new ApplicationException("SisConnection is not valid for the DMS host!");
                        }
                    }
                    else if (!string.IsNullOrEmpty(accountingLogonId))
                    {
                        Guid accountingLogonGuid = new Guid(accountingLogonId);
                        if (_dmsHost.Logons.Contains(accountingLogonGuid))
                        {
                            _accountingLogon = _dmsHost.Logons.GetItem(accountingLogonGuid);
                        }
                        else
                        {
                            throw new ApplicationException("Logon does not exist for the DMS host!");
                        }
                    }
                    else
                    {
                        throw new ApplicationException("Missing either SisConnectionId or LogonId");
                    }
                }
            }

            return _accountingLogon;
        }
    }    
    private DealerAccountChart DealerAccountChart
    {
        get
        {
            return (DealerAccountChart)ViewState["DealerAccountChart"];
        }
        set
        {
            ViewState["DealerAccountChart"] = value;
        }
    }

    private string InventoryAccountFilter
    {
        get { return ViewState["InventoryAccountFilter"] == null ? "" : ViewState["InventoryAccountFilter"].ToString(); }
        set { ViewState["InventoryAccountFilter"] = value; }
    }
    private string CostAccountFilter
    {
        get { return ViewState["CostAccountFilter"] == null ? "" : ViewState["CostAccountFilter"].ToString(); }
        set { ViewState["CostAccountFilter"] = value; }
    }
    private string SalesAccountFilter
    {
        get { return ViewState["SalesAccountFilter"] == null ? "" : ViewState["SalesAccountFilter"].ToString(); }
        set { ViewState["SalesAccountFilter"] = value; }
    }

    private bool reset = false;

    protected void Page_Load(object sender, EventArgs e)
    {

        // 04/01/11 DMW - Added clear error
        ErrorList.Items.Clear();
        ErrorList.Visible = false;

        if(!IsPostBack)
        {
            //In case we get more asset classes or reference types, invalidate the cache
            AssetClassCollection.InvalidateCache();
            ReferenceTypeCollection.InvalidateCache();
            DealerAccountChart = DealerAccountChart.GetDealerAccountChart(DealerId, Environment, AccountingLogon.Id);

            //from existing dealer configuration
            string activeTabName = Request.QueryString["TabName"];
            if (activeTabName != null)
            {
                switch (activeTabName)
                {
                    case "Accounts":
                        LogonDataExtraction.ActiveTabIndex = 0;
                        break;
                    case "Journals":
                        LogonDataExtraction.ActiveTabIndex = 1;
                        break;
                    case "Schedules":
                        LogonDataExtraction.ActiveTabIndex = 2;
                        break;
                    default:
                        break;
                }
            }

            if (IsUpdate)
            {
                DoneButton.Visible = true;
            }
        }
    }

    protected static void SortDropDownList(DropDownList ddl)
    {
        List<string> items = new List<string>();
        foreach (ListItem o in ddl.Items)
        {
            if(o.Value != "")
            items.Add(o.Value);
        }
        items.Sort();
        ddl.Items.Clear();
        foreach (string s in items)
        {
            ddl.Items.Add(new ListItem(s, s));
        }
    }

    #region Account Tab

    #region Inventory Tab

    protected void InventoryAccountFilterButton_Click(object sender, EventArgs e)
    {
        InventoryAccountFilter = InventoryAccountDepartmentDropDownList.SelectedValue;
        InventoryAccountGridView.DataBind();
    }

    protected void InventoryAccountFilterClearButton_Click(object sender, EventArgs e)
    {
        InventoryAccountFilter = "";
        InventoryAccountGridView.DataBind();
    }

    protected void InventoryAccountGridViewDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        AccountCollection ac = AccountCollection.GetAccounts(AccountingLogon.Id, AccountType.Asset, InventoryAccountFilter, e.SortExpression);
        foreach (Account account in ac)
        {
            if (InventoryAccountDepartmentDropDownList.Items.FindByValue(account.Department) == null && account.Department.Trim() != "")
            {
                InventoryAccountDepartmentDropDownList.Items.Add(new ListItem(account.Department, account.Department));
            }
        }
        e.BusinessObject = ac;
    }

    protected void InventoryAccountGridView_DataBound(object sender, EventArgs e)
    {
        SortDropDownList(InventoryAccountDepartmentDropDownList);
        if (InventoryAccountFilter != "")
        {
            InventoryAccountDepartmentDropDownList.SelectedValue = InventoryAccountFilter;
        }
    }

    protected void InventoryAccountGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Account account = (Account)e.Row.DataItem;
            e.Row.Visible = !(DealerAccountChart.InventoryAccounts.Contains(account.Id));
        }
    }

    // 04/01/11 DMW - Added ability to check all accounts
    protected void CheckAllInventoryAccountsButton_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow row in InventoryAccountGridView.Rows)
        {
            CheckBox include = (CheckBox)row.FindControl("IncludeCheckbox");
            if (include != null)
                include.Checked = true;
        }
    }

    protected void IncludeInventoryAccountsButton_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow row in InventoryAccountGridView.Rows)
        {
            if(((CheckBox)row.FindControl("IncludeCheckbox")).Checked)
            {
                // 04/01/11 DMW - Added try-catch error handling
                try
                {
                    DealerAccountChart.InventoryAccounts.Assign((Guid)InventoryAccountGridView.DataKeys[row.RowIndex]["Id"]);
                }
                catch (InvalidOperationException ex)
                {
                    ErrorList.Visible = true;
                    ErrorList.Items.Add(row.Cells[1].Text + " (" + row.Cells[6].Text + ") " + ex.Message);
                }
                InventoryAccountGridView.DataBind();
                DealerInventoryAccountGridView.DataBind();
            }
        }
    }

    protected void DealerInventoryAccountGridViewDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        e.BusinessObject = DealerAccountChart.InventoryAccounts;
    }

    protected void DealerInventoryAccountGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Exclude"))
        {
            GridView gv = (GridView)sender;
            int index = Convert.ToInt32(e.CommandArgument);
            Guid accountId = (Guid)gv.DataKeys[index].Value;
            DealerAccountChart.InventoryAccounts.Remove(accountId);
            InventoryAccountGridView.DataBind();
            gv.DataBind();
        }
    }

    #endregion

    #region Sales Tab

    protected void SalesAccountFilterButton_Click(object sender, EventArgs e)
    {
        SalesAccountFilter = SalesAccountDepartmentDropDownList.SelectedValue;
        SalesAccountGridView.DataBind();
    }

    protected void SalesAccountFilterClearButton_Click(object sender, EventArgs e)
    {
        SalesAccountFilter = "";
        SalesAccountGridView.DataBind();
    }

    protected void SalesAccountGridViewDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        AccountCollection ac = AccountCollection.GetAccounts(AccountingLogon.Id, AccountType.Sales, SalesAccountFilter, e.SortExpression);
        foreach (Account account in ac)
        {
            if (SalesAccountDepartmentDropDownList.Items.FindByValue(account.Department) == null && account.Department.Trim() != "")
            {
                SalesAccountDepartmentDropDownList.Items.Add(new ListItem(account.Department, account.Department));
            }
        }
        e.BusinessObject = ac;
    }

    protected void SalesAccountGridView_DataBound(object sender, EventArgs e)
    {
        SortDropDownList(SalesAccountDepartmentDropDownList);
        if (SalesAccountFilter != "")
        {
            SalesAccountDepartmentDropDownList.SelectedValue = SalesAccountFilter;
        }
    }

    protected void SalesAccountGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Account account = (Account)e.Row.DataItem;
            e.Row.Visible = !(DealerAccountChart.SalesAccounts.Contains(account.Id));
        }
    }

    // 04/01/11 DMW - Added ability to check all accounts
    protected void CheckAllSalesAccountsButton_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow row in SalesAccountGridView.Rows)
        {
            CheckBox include = (CheckBox)row.FindControl("IncludeCheckbox");
            if (include != null)
                include.Checked = true;
        }
    }

    protected void IncludeSalesAccountsButton_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow row in SalesAccountGridView.Rows)
        {
            if (((CheckBox)row.FindControl("IncludeCheckbox")).Checked)
            {
                // 04/01/11 DMW - Added try-catch error handling
                try
                {
                    DealerAccountChart.SalesAccounts.Assign((Guid)SalesAccountGridView.DataKeys[row.RowIndex]["Id"]);
                }
                catch (InvalidOperationException ex)
                {
                    ErrorList.Visible = true;
                    ErrorList.Items.Add(row.Cells[1].Text + " (" + row.Cells[6].Text + ") " + ex.Message);
                }
                SalesAccountGridView.DataBind();
                DealerSalesAccountGridView.DataBind();
            }
        }
    }

    protected void DealerSalesAccountGridViewDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        if (!reset)
            UpdateDealerSalesAccountGridView();
        e.BusinessObject = DealerAccountChart.SalesAccounts;
    }

    protected void UpdateDealerSalesAccountGridView()
    {
        foreach (GridViewRow row in DealerSalesAccountGridView.Rows)
        {
            DealerAccount da = DealerAccountChart.SalesAccounts.GetItem((Guid)DealerSalesAccountGridView.DataKeys[row.RowIndex]["AccountId"]);
            if (da != null)
            {
                da.IsFrontEnd = ((CheckBox)row.FindControl("IsFrontEndCheckbox")).Checked;
                da.IsRetail = ((CheckBox)row.FindControl("IsRetailCheckbox")).Checked;
                da.IsWholesale = ((CheckBox)row.FindControl("IsWholesaleCheckbox")).Checked;
            }
        }
    }

    protected void DealerSalesAccountGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Exclude"))
        {
            GridView gv = (GridView)sender;
            int index = Convert.ToInt32(e.CommandArgument);
            Guid accountId = (Guid)gv.DataKeys[index].Value;
            DealerAccountChart.SalesAccounts.Remove(accountId);
            SalesAccountGridView.DataBind();
            gv.DataBind();
        }
    }

    #endregion

    #region Cost Tab

    protected void CostAccountFilterButton_Click(object sender, EventArgs e)
    {
        CostAccountFilter = CostAccountDepartmentDropDownList.SelectedValue;
        CostAccountGridView.DataBind();
    }

    protected void CostAccountFilterClearButton_Click(object sender, EventArgs e)
    {
        CostAccountFilter = "";
        CostAccountGridView.DataBind();
    }

    protected void CostAccountGridViewDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        List<Account> costAccounts = new List<Account>();

        //add accounts with type 'Cost'
        costAccounts.AddRange(AccountCollection.GetAccounts(AccountingLogon.Id, AccountType.Cost, CostAccountFilter, e.SortExpression));

        //add accounts with type 'Income'
        costAccounts.AddRange(AccountCollection.GetAccounts(AccountingLogon.Id, AccountType.Income, CostAccountFilter, e.SortExpression));


        costAccounts.Sort(
            delegate(Account x, Account y)
            {
                if (e.SortExpression.ToLower() == "number desc")
                    return y.Number.CompareTo(x.Number);
                if (e.SortExpression.ToLower() == "type desc")
                    return y.Type.CompareTo(x.Type);
                if (e.SortExpression.ToLower() == "subtype desc")
                    return y.Subtype.CompareTo(x.Subtype);
                if (e.SortExpression.ToLower() == "controltype desc")
                    return y.ControlType.CompareTo(x.ControlType);
                if (e.SortExpression.ToLower() == "department desc")
                    return y.Department.CompareTo(x.Department);
                if (e.SortExpression.ToLower() == "description desc")
                    return y.Description.CompareTo(x.Description);

                if (e.SortExpression.ToLower() == "number")
                    return x.Number.CompareTo(y.Number);
                if (e.SortExpression.ToLower() == "type")
                    return x.Type.CompareTo(y.Type);
                if (e.SortExpression.ToLower() == "subtype")
                    return x.Subtype.CompareTo(y.Subtype);
                if (e.SortExpression.ToLower() == "controltype")
                    return x.ControlType.CompareTo(y.ControlType);
                if (e.SortExpression.ToLower() == "department")
                    return x.Department.CompareTo(y.Department);
                if (e.SortExpression.ToLower() == "description")
                    return x.Description.CompareTo(y.Description);

                return y.Number.CompareTo(x.Number);
            });


        foreach (Account account in costAccounts)
        {
            if (CostAccountDepartmentDropDownList.Items.FindByValue(account.Department) == null && account.Department.Trim() != "")
            {
                CostAccountDepartmentDropDownList.Items.Add(new ListItem(account.Department, account.Department));
            }
        }

        e.BusinessObject = costAccounts;
    }

    protected void CostAccountGridView_DataBound(object sender, EventArgs e)
    {
        SortDropDownList(CostAccountDepartmentDropDownList);
        if (CostAccountFilter != "")
        {
            CostAccountDepartmentDropDownList.SelectedValue = CostAccountFilter;
        }
    }

    protected void CostAccountGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Account account = (Account)e.Row.DataItem;
            e.Row.Visible = !(DealerAccountChart.CostAccounts.Contains(account.Id));
        }
    }

    // 04/01/11 DMW - Added ability to check all accounts
    protected void CheckAllCostAccountsButton_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow row in CostAccountGridView.Rows)
        {
            CheckBox include = (CheckBox)row.FindControl("IncludeCheckbox");
            if (include != null)
                include.Checked = true;
        }
    }

    protected void IncludeCostAccountsButton_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow row in CostAccountGridView.Rows)
        {
            if (((CheckBox)row.FindControl("IncludeCheckbox")).Checked)
            {
                // 04/01/11 DMW - Added try-catch error handling
                try
                {
                    DealerAccountChart.CostAccounts.Assign((Guid)CostAccountGridView.DataKeys[row.RowIndex]["Id"]);
                }
                catch (InvalidOperationException ex)
                {
                    ErrorList.Visible = true;
                    ErrorList.Items.Add(row.Cells[1].Text + " (" + row.Cells[6].Text + ") " + ex.Message);
                }
                CostAccountGridView.DataBind();
                DealerCostAccountGridView.DataBind();
            }
        }
    }

    protected void DealerCostAccountGridViewDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        if(!reset)
            UpdateDealerCostAccountGridView();
        e.BusinessObject = DealerAccountChart.CostAccounts;
    }

    protected void UpdateDealerCostAccountGridView()
    {
        foreach (GridViewRow row in DealerCostAccountGridView.Rows)
        {
            DealerAccount da = DealerAccountChart.CostAccounts.GetItem((Guid)DealerCostAccountGridView.DataKeys[row.RowIndex]["AccountId"]);
            if (da != null)
            {
                da.IsFrontEnd = ((CheckBox)row.FindControl("IsFrontEndCheckbox")).Checked;
            }
        }
    }

    protected void DealerCostAccountGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Exclude"))
        {
            GridView gv = (GridView)sender;
            int index = Convert.ToInt32(e.CommandArgument);
            Guid accountId = (Guid)gv.DataKeys[index].Value;
            DealerAccountChart.CostAccounts.Remove(accountId);
            CostAccountGridView.DataBind();
            gv.DataBind();
        }
    }

    #endregion

    #endregion

    #region Journal Tab

    protected void AssetClassDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        e.BusinessObject = AssetClassCollection.GetAssetClasses();
    }

    protected void ReferenceTypeDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        e.BusinessObject = ReferenceTypeCollection.GetReferenceTypes();
    }

    protected void JournalTypeTextBox_TextChanged(object sender, EventArgs e)
    {
        FilterJournalType();
    }

    protected void JournalTypeFilterButton_Click(object sender, EventArgs e)
    {
        FilterJournalType();
    }

    private void FilterJournalType()
    {
        JournalGridView.DataBind();
    }

    protected void JournalGridViewDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        e.BusinessObject = JournalCollection.GetJournals(AccountingLogon.Id, JournalTypeTextBox.Text.ToUpper(), e.SortExpression);
    }

    protected void JournalGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Journal journal = (Journal)e.Row.DataItem;
            e.Row.Visible = !(DealerAccountChart.Journals.Contains(journal.Id));
        }
    }

    protected void IncludeJournalsButton_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow row in JournalGridView.Rows)
        {
            if (((CheckBox)row.FindControl("IncludeCheckbox")).Checked)
            {
                DealerAccountChart.Journals.Assign((Guid)JournalGridView.DataKeys[row.RowIndex]["Id"]);
                JournalGridView.DataBind();
                DealerJournalGridView.DataBind();
            }
        }
    }

    protected void DealerJournalGridViewDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        if (!reset)
            UpdateDealerJournalGridView();

        List<DealerJournal> dj = new List<DealerJournal>(DealerAccountChart.Journals);
        e.BusinessObject = dj;
    }

    protected void DealerJournalGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Exclude"))
        {
            GridView gv = (GridView)sender;
            int index = Convert.ToInt32(e.CommandArgument);
            Guid journalId = (Guid)gv.DataKeys[index].Value;
            DealerAccountChart.Journals.Remove(journalId);
            JournalGridView.DataBind();
            gv.DataBind();
        }
    }

    protected void UpdateDealerJournalGridView()
    {
        foreach (GridViewRow row in DealerJournalGridView.Rows)
        {
            DealerJournal dj = DealerAccountChart.Journals.GetItem((Guid)DealerJournalGridView.DataKeys[row.RowIndex]["JournalId"]);
            if (dj != null)
            {
                dj.IsPurchase = ((CheckBox)row.FindControl("IsPurchaseCheckBox")).Checked;
                dj.IsTrade = ((CheckBox)row.FindControl("IsTradeCheckBox")).Checked;
                dj.ReferenceTypeId = int.Parse(((DropDownList)row.FindControl("ReferenceTypeDropDownList")).SelectedValue);
                dj.AssetClassId = int.Parse(((DropDownList)row.FindControl("AssetClassDropDownList")).SelectedValue);
            }
        }
    }

    #endregion

    #region Schedule Tab

    protected void ScheduleGridViewDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        e.BusinessObject = ScheduleCollection.GetSchedules(AccountingLogon.Id);
    }

    #endregion

    #region Global Save/Cancel

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        ResetErrorList(ErrorList);

        UpdateDealerSalesAccountGridView();
        UpdateDealerCostAccountGridView();
        UpdateDealerJournalGridView();

        if (DealerAccountChart.IsValid)
        {
            try
            {
                DealerAccountChart.Save();
                DoneButton.Visible = true;
                DoneButton.Text = "Done";
            }
            catch (Exception ex)
            {
                ErrorList.Items.Add(ex.Message);
                ErrorList.Visible = true;
            }
        }
        else
        {
            ErrorList.Visible = true;
            AddBrokenRules(DealerAccountChart.BrokenRulesCollection);
            foreach (DealerAccount account in DealerAccountChart.SalesAccounts)
            {
                AddBrokenRules(account.BrokenRulesCollection);
            }
            foreach (DealerAccount account in DealerAccountChart.CostAccounts)
            {
                AddBrokenRules(account.BrokenRulesCollection);
            }
            foreach (DealerJournal journal in DealerAccountChart.Journals)
            {
                AddBrokenRules(journal.BrokenRulesCollection);
            }
        }
    }

    private void AddBrokenRules(BrokenRulesCollection borkedRules)
    {
        foreach (BrokenRule rule in borkedRules)
        {
            ListItem li = ErrorList.Items.FindByText(rule.Description);
            if (li == null)
            {
                ErrorList.Items.Add(rule.Description);
            }
        }
    }

    protected void ResetButton_Click(object sender, EventArgs e)
    {
        reset = true;
        DealerAccountChart = DealerAccountChart.GetDealerAccountChart(DealerId, Environment, AccountingLogon.Id);
        DataBindChildren();
        ResetErrorList(ErrorList);
    }

    protected void DoneButton_Click(object sender, EventArgs e)
    {
        Response.Redirect(string.Format(CultureInfo.InvariantCulture,
            "~/Application/Configuration/ADP/ExistingDealerConfiguration.aspx?DealerId={0}",
        DealerId), true);
    }

    private void ResetErrorList(BulletedList errorList)
    {
        errorList.Visible = false;
        errorList.Items.Clear();
    }

    #endregion

}