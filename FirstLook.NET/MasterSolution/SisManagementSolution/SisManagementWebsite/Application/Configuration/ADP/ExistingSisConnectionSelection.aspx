<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ExistingSisConnectionSelection.aspx.cs" Inherits="Application_Configuration_ADP_ExistingSisConnectionSelection" MasterPageFile="~/Application/Configuration/ADP/DealerMasterPage.master" %>

<asp:Content ContentPlaceHolderID="TitleContentPlaceHolder" runat="server">
    <title>Existing SIS Connection Selection</title>
</asp:Content>

<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
    <csla:CslaDataSource ID="AccountingLogonDataSource" runat="server" OnSelectObject="AccountingLogonDataSource_SelectObject" />
    <csla:CslaDataSource ID="FinanceLogonDataSource" runat="server" OnSelectObject="FinanceLogonDataSource_SelectObject" />
    <fieldset>
        <legend>Existing SIS Connection Selection</legend>
        <p>Please enter search criteria to limit the DMS host connections.</p>
        <div>
            <p class="ColumnOne">
                <asp:Label ID="AccountingLogonLabel" runat="server" Text="-A Logon: " AssociatedControlID="AccountingLogonDropDownList"></asp:Label>
                <asp:DropDownList ID="AccountingLogonDropDownList" runat="server" AppendDataBoundItems="true" DataSourceID="AccountingLogonDataSource" DataTextField="Name" DataValueField="Id">
                    <asp:ListItem Value="0" Text="Please Select" />
                </asp:DropDownList>
            </p>
            <p class="ColumnTwo">
                <asp:Label ID="FinanceLogonLabel" runat="server" Text="-FI Logon: " AssociatedControlID="FinanceLogonDropDownList"></asp:Label>
                <asp:DropDownList ID="FinanceLogonDropDownList" runat="server" AppendDataBoundItems="true" DataSourceID="FinanceLogonDataSource" DataTextField="Name" DataValueField="Id">
                    <asp:ListItem Value="0" Text="Please Select" />
                </asp:DropDownList>
            </p>
            <p style="position: relative; left: 10px;">
                <asp:Button ID="SearchButton" runat="server" Text="Search" OnClick="SearchButton_Click" />
                <asp:Button ID="ClearButton" runat="server" Text="Clear" OnClick="ClearButton_Click" />
            </p>
        </div>
        <p>
            Please select an existing SIS connection from the table below. 
            <asp:HyperLink ID="SisConnectionHyperLink" runat="server" OnInit="SisConnectionHyperLink_Init">Alternatively, click here to create a new SIS connection.</asp:HyperLink>
        </p>
        <csla:CslaDataSource ID="SisConnectionGridViewDataSource" runat="server"
            OnSelectObject="SisConnectionGridViewDataSource_SelectObject"
            TypeSupportsPaging="true"
            TypeSupportsSorting="true">
        </csla:CslaDataSource>
        <asp:GridView ID="SisConnectionGridView" runat="server"
            AllowPaging="true"
            AllowSorting="true"
            AutoGenerateColumns="false"
            DataKeyNames="Id"
            DataSourceID="SisConnectionGridViewDataSource"
            OnDataBound="SisConnectionGridView_DataBound"
            OnRowCommand="SisConnectionGridView_RowCommand">
            <Columns>
                <asp:BoundField SortExpression="AccountingLogonName" AccessibleHeaderText="Accounting Logon" HeaderText="Accounting Logon" DataField="AccountingLogonName" ReadOnly="true" />
                <asp:BoundField SortExpression="FinanceLogonName" AccessibleHeaderText="Finance Logon" HeaderText="Finance Logon" DataField="FinanceLogonName" ReadOnly="true" />
                <asp:BoundField SortExpression="NumberOfAssociatedDealers" AccessibleHeaderText="Number of Associated Dealers" HeaderText="Number of Associated Dealers" DataField="NumberOfAssociatedDealers" ReadOnly="true" />
                <asp:ButtonField ButtonType="Button" Text="Select" CommandName="Select" ShowHeader="false" />
            </Columns>
        </asp:GridView>
        <p>
            <asp:Label ID="NoSisConnectionText" runat="server" Visible="false">
                No SIS Connections matched your search criteria.
                Please try changing or removing your search criteria.
            </asp:Label>
        </p>
    </fieldset>
</asp:Content>
