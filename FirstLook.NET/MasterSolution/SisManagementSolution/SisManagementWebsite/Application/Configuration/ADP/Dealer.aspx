<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Dealer.aspx.cs" Inherits="Application_Configuration_ADP_Dealer" MasterPageFile="~/Application/Configuration/ADP/DealerMasterPage.master" %>

<asp:Content ContentPlaceHolderID="TitleContentPlaceHolder" runat="server">
    <title>Dealer</title>
</asp:Content>

<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
    <csla:CslaDataSource ID="HourDataSource" runat="server" OnSelectObject="HourDataSource_SelectObject" />
    <csla:CslaDataSource ID="MinuteDataSource" runat="server" OnSelectObject="MinuteDataSource_SelectObject" />
    <csla:CslaDataSource ID="TimeZoneDataSource" runat="server" OnSelectObject="TimeZoneDataSource_SelectObject" />
    <fieldset>
        <legend>Dealer</legend>
        <asp:BulletedList ID="ErrorList" runat="server" Visible="false" CssClass="ErrorList">
        </asp:BulletedList>
        <p>Please enter the dealer business hours.</p>
        <p>
            <asp:Label ID="DealerOpenTimeLabel" runat="server" Text="Dealer Open: " AssociatedControlID="DealerOpenHourDropDownList"></asp:Label>
            <asp:DropDownList ID="DealerOpenHourDropDownList" runat="server" DataSourceID="HourDataSource" OnDataBound="DealerOpenHourDropDownList_DataBound" DataTextField="Text" DataValueField="Value"></asp:DropDownList>
            <asp:DropDownList ID="DealerOpenMinuteDropDownList" runat="server" DataSourceID="MinuteDataSource" OnDataBound="DealerOpenMinuteDropDownList_DataBound" DataTextField="Text" DataValueField="Value"></asp:DropDownList>
        </p>
        <p>
            <asp:Label ID="DealerCloseTimeLabel" runat="server" Text="Dealer Close: " AssociatedControlID="DealerCloseHourDropDownList"></asp:Label>
            <asp:DropDownList ID="DealerCloseHourDropDownList" runat="server" DataSourceID="HourDataSource" OnDataBound="DealerCloseHourDropDownList_DataBound" DataTextField="Text" DataValueField="Value"></asp:DropDownList>
            <asp:DropDownList ID="DealerCloseMinuteDropDownList" runat="server" DataSourceID="MinuteDataSource" OnDataBound="DealerCloseMinuteDropDownList_DataBound" DataTextField="Text" DataValueField="Value"></asp:DropDownList>
        </p>
        <p>
            <asp:Label ID="DealerTimeZoneLabel" runat="server" Text="Time Zone:" AssociatedControlID="DealerTimeZoneDropDownList"></asp:Label>
            <asp:DropDownList ID="DealerTimeZoneDropDownList" runat="server"
                    AppendDataBoundItems="true"
                    DataSourceID="TimeZoneDataSource"
                    DataTextField="Name"
                    DataValueField="Id"
                    OnDataBound="DealerTimeZoneDropDownList_DataBound">
                <asp:ListItem Text="Select a Time Zone" Value="0" />
            </asp:DropDownList>
        </p>
        <p>
            <asp:Label ID="DealerFollowsDaylightSavingsTimeLabel" runat="server" Text="Follows Daylight Savings Time: " AssociatedControlID="DealerFollowsDaylightSavingsTimeRadioButtonList"></asp:Label>
            <asp:RadioButtonList ID="DealerFollowsDaylightSavingsTimeRadioButtonList" runat="server" CssClass="BooleanRadioButtonList" RepeatColumns="2" RepeatDirection="Horizontal">
                <asp:ListItem Text="Yes" Value="True" />
                <asp:ListItem Text="No" Value="False" />
            </asp:RadioButtonList>
        </p>
        <p class="controls">
            <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
            <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" />
        </p>
    </fieldset>
</asp:Content>
