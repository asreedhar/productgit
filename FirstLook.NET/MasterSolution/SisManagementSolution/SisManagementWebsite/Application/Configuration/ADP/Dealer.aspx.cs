using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using Csla.Validation;
using Csla.Web;
using FirstLook.Sis.Management.DomainModel.Configuration.Adp;
using Environment=FirstLook.Sis.Management.DomainModel.Configuration.Environment;
using TimeZone=FirstLook.Sis.Management.DomainModel.Configuration.Adp.TimeZone;

public partial class Application_Configuration_ADP_Dealer : Page
{
    private Dealer _dealer;

    private Dealer Dealer
    {
        get
        {
            if (_dealer == null)
            {
                string id = Request.QueryString["DealerId"];

                if (string.IsNullOrEmpty(id))
                {
                    throw new ApplicationException("Missing DealerId");
                }
                else
                {
                    _dealer = Dealer.GetDealer(int.Parse(id), Environment.PreProduction);
                }
            }

            return _dealer;
        }
    }

    private bool IsUpdate
    {
        get
        {
            return (string.Equals(Request.QueryString["Mode"], "Update"));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PopulateForm(Dealer);
        }

        if (!IsUpdate)
        {
            CancelButton.Visible = false;
        }
    }

    private void PopulateForm(Dealer dealer)
    {
        if (dealer.FollowsDaylightSavingsTime)
        {
            DealerFollowsDaylightSavingsTimeRadioButtonList.Items[0].Selected = true;
            DealerFollowsDaylightSavingsTimeRadioButtonList.Items[1].Selected = false;
        }
        else
        {
            DealerFollowsDaylightSavingsTimeRadioButtonList.Items[0].Selected = false;
            DealerFollowsDaylightSavingsTimeRadioButtonList.Items[1].Selected = true;
        }
    }

    protected void TimeZoneDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        e.BusinessObject = TimeZoneCollection.GetTimeZones();
    }

    protected void HourDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        e.BusinessObject = Hour.GetHours();
    }

    protected void MinuteDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        e.BusinessObject = Minute.GetMinutes();
    }

    protected void DealerTimeZoneDropDownList_DataBound(object sender, EventArgs e)
    {
        if (Dealer.TimeZone != null)
            foreach (ListItem item in DealerTimeZoneDropDownList.Items)
                item.Selected = string.Equals(item.Value, ToString(Dealer.TimeZone.Id));
    }

    protected void DealerOpenHourDropDownList_DataBound(object sender, EventArgs e)
    {
        if (Dealer.BusinessHoursBegin != null)
            foreach (ListItem item in DealerOpenHourDropDownList.Items)
                item.Selected = string.Equals(item.Value, ToString(Dealer.BusinessHoursBegin.Hour));
    }

    protected void DealerOpenMinuteDropDownList_DataBound(object sender, EventArgs e)
    {
        if (Dealer.BusinessHoursBegin != null)
            foreach (ListItem item in DealerOpenMinuteDropDownList.Items)
                item.Selected = string.Equals(item.Value, ToString(Dealer.BusinessHoursBegin.Minute));
    }

    protected void DealerCloseHourDropDownList_DataBound(object sender, EventArgs e)
    {
        if (Dealer.BusinessHoursEnd != null)
            foreach (ListItem item in DealerCloseHourDropDownList.Items)
                item.Selected = string.Equals(item.Value, ToString(Dealer.BusinessHoursEnd.Hour));
    }

    protected void DealerCloseMinuteDropDownList_DataBound(object sender, EventArgs e)
    {
        if (Dealer.BusinessHoursEnd != null)
            foreach (ListItem item in DealerCloseMinuteDropDownList.Items)
                item.Selected = string.Equals(item.Value, ToString(Dealer.BusinessHoursEnd.Minute));
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        LoadComplete += MarshallForm;
    }

    private void MarshallForm(object sender, EventArgs e)
    {
        LoadComplete -= MarshallForm;

        ErrorList.Items.Clear();

        MarshallForm(Dealer);

        if (Dealer.IsValid)
        {
            ErrorList.Visible = false;

            Dealer.Save();

            Response.Redirect(string.Format(CultureInfo.InvariantCulture,
                "~/Application/Configuration/ADP/ExistingDmsHostSelection.aspx?DealerId={0}",
                Dealer.Id), true);
        }
        else
        {
            ErrorList.Visible = true;

            foreach (BrokenRule rule in Dealer.BrokenRulesCollection)
            {
                ErrorList.Items.Add(rule.Description);
            }
        }
    }

    private void MarshallForm(Dealer dealer)
    {
        dealer.BusinessHoursBegin = MarshallTime(DealerOpenHourDropDownList, DealerOpenMinuteDropDownList);
        dealer.BusinessHoursEnd = MarshallTime(DealerCloseHourDropDownList, DealerCloseMinuteDropDownList);

        int timeZoneId = int.Parse(DealerTimeZoneDropDownList.SelectedItem.Value);

        if (timeZoneId > 0)
        {
            dealer.TimeZone = new TimeZone(timeZoneId, DealerTimeZoneDropDownList.SelectedItem.Text);
        }
        else
        {
            dealer.TimeZone = null;
        }

        dealer.FollowsDaylightSavingsTime = (string.Equals(DealerFollowsDaylightSavingsTimeRadioButtonList.SelectedValue, "True"));
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        if (IsUpdate)
        {
            LoadComplete += ResetForm;
        }
    }

    protected void ResetForm(object sender, EventArgs e)
    {
        LoadComplete -= ResetForm;

        _dealer = null;

        PopulateForm(Dealer);
    }

    private static Time MarshallTime(ListControl hours, ListControl minutes)
    {
        return new Time(
            int.Parse(hours.SelectedValue),
            int.Parse(minutes.SelectedValue));
    }

    private static string ToString(int value)
    {
        return value.ToString(CultureInfo.InvariantCulture);
    }
}
