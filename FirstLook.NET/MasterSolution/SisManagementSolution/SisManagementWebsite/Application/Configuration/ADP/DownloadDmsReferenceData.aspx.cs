using System;
using System.Collections.Specialized;
using System.Globalization;
using System.Web.UI;
using FirstLook.Sis.Management.DomainModel.Configuration;
using FirstLook.Sis.Management.DomainModel.Configuration.Adp;

public partial class Application_Configuration_ADP_DownloadDmsReferenceData : Page
{
    private DmsHost dmsHost;
    private SisConnection sisConn;

    private int DealerId
    {
        get
        {
            string id = Request.QueryString["DealerId"];

            if (string.IsNullOrEmpty(id))
            {
                throw new ApplicationException("Missing DealerId");
            }
            return int.Parse(id);
        }
    }
    private DmsHost DmsHost
    {
        get
        {
            if (dmsHost == null)
            {
                string id = Request.QueryString["DmsHostId"];

                if (string.IsNullOrEmpty(id))
                {
                    throw new ApplicationException("Missing DmsHostId");
                }
                dmsHost = DmsHost.GetDmsHost(new Guid(id));
            }

            return dmsHost;
        }
    }
    private SisConnection SisConn
    {
        get
        {
            if (sisConn == null)
            {
                string id = Request.QueryString["SisConnectionId"];

                if (string.IsNullOrEmpty(id))
                {
                    throw new ApplicationException("Missing SisConnectionId");
                }
                sisConn = DmsHost.SisConnections.GetItem(new Guid(id));
            }

            return sisConn;
        }
    }

    private Query AccountQuery;
    private Query JournalQuery;
    private Query ScheduleQuery;

    protected void Page_Load(object sender, EventArgs e)
    {
        AccountQuery = Query.GetQuery(SisConn, TransactionType.GetAccounts);
        JournalQuery = Query.GetQuery(SisConn, TransactionType.GetJournals);
        ScheduleQuery = Query.GetQuery(SisConn, TransactionType.GetSchedules);

        //Null queries
        if (AccountQuery.Id == Guid.Empty || JournalQuery.Id == Guid.Empty || ScheduleQuery.Id == Guid.Empty)
        {
            if (AccountQuery.Id == Guid.Empty)
            {
                AccountQuery = Query.NewQuery(SisConn, TransactionType.GetAccounts);
            }
            if (JournalQuery.Id == Guid.Empty)
            {
                JournalQuery = Query.NewQuery(SisConn, TransactionType.GetJournals);
            }
            if (ScheduleQuery.Id == Guid.Empty)
            {
                ScheduleQuery = Query.NewQuery(SisConn, TransactionType.GetSchedules);
            }
        }

        //errors
        if (AccountQuery.Status == QueryStatus.Error || JournalQuery.Status == QueryStatus.Error || ScheduleQuery.Status == QueryStatus.Error)
        {
            ErrorPanel.Visible = true;
            ReRunPanel.Visible = false;
            StatusPanel.Visible = false;
            bool invalidLogin = false;

            if (AccountQuery.Status == QueryStatus.Error)
            {
                QueryErrorCollection accountErrors = QueryErrorCollection.GetQueryErrors(AccountQuery.Id);
                invalidLogin = invalidLogin || ContainsInvalidLoginError(accountErrors);
                AccountErrorsLabel.Text = GetErrorSummary(accountErrors);
            }

            if (JournalQuery.Status == QueryStatus.Error)
            {
                QueryErrorCollection journalErrors = QueryErrorCollection.GetQueryErrors(JournalQuery.Id);
                invalidLogin = invalidLogin || ContainsInvalidLoginError(journalErrors);
                JournalErrorsLabel.Text = GetErrorSummary(journalErrors);
            }

            if (ScheduleQuery.Status == QueryStatus.Error)
            {
                QueryErrorCollection scheduleErrors = QueryErrorCollection.GetQueryErrors(ScheduleQuery.Id);
                invalidLogin = invalidLogin || ContainsInvalidLoginError(scheduleErrors);
                ScheduleErrorsLabel.Text = GetErrorSummary(scheduleErrors);
            }

            LoginErrorPanel.Visible = invalidLogin;
            QueryErrorPanel.Visible = !invalidLogin;
            ReRunPanel.Visible = true;
        }
        //complete
        else if (AccountQuery.Status == QueryStatus.Completed && JournalQuery.Status == QueryStatus.Completed && ScheduleQuery.Status == QueryStatus.Completed)
        {
            AccountLabel.Text = GetStatusMessage(AccountQuery.Status);
            JournalsLabel.Text = GetStatusMessage(JournalQuery.Status);
            SchedulesLabel.Text = GetStatusMessage(ScheduleQuery.Status);
            CompletePanel.Visible = true;
            ReRunPanel.Visible = true;
        }
        //processing
        else
        {
            Response.AddHeader("Refresh", "15");
            ProcessingPanel.Visible = true;
            TimeSpan elapsed = DateTime.Now - AccountQuery.Date;
            ElapsedTimeLabel.Text = String.Format("{0:D2}:{1:D2}", elapsed.Minutes, elapsed.Seconds);

            AccountLabel.Text = GetStatusMessage(AccountQuery.Status);
            JournalsLabel.Text = GetStatusMessage(JournalQuery.Status);
            SchedulesLabel.Text = GetStatusMessage(ScheduleQuery.Status);
        }
    }

    private string GetStatusMessage(QueryStatus s)
    {
        switch(s)
        {
            case QueryStatus.NotSubmitted:
                return "Query awaiting submission to Sis.";
            case QueryStatus.Submitted:
                return "Query submitted to Sis.";
            case QueryStatus.Accepted:
                return "Query accepted by Sis and awaiting processing.";
            case QueryStatus.Completed:
                return "Query processing complete.";
            case QueryStatus.Error:
                return "Query Error.";
            case QueryStatus.Staged:
                return "Query Results staged.";
        }
        return "Unknown status.";
    }


    private static string GetErrorSummary(QueryErrorCollection qec)
    {
        if (qec.Count == 0)
        {
            return "No errors.";
        }

        string summary = "";
        StringDictionary errors = new StringDictionary();
        foreach (QueryError qe in qec)
        {
            if (!errors.ContainsKey(qe.SuggestedResolution))
            {
                errors.Add(qe.SuggestedResolution, qe.SuggestedResolution);
                summary += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + qe.Type + "<br/>";
                summary += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Resolution:" + qe.SuggestedResolution;
                summary += "<br /><br />";
            }
        }
        return summary;
    }

    private static bool ContainsInvalidLoginError(QueryErrorCollection qec)
    {
        foreach (QueryError qe in qec)
        {
            if (qe.Type.Contains("SisInvalidCredentialsException"))
            {
                return true;
            }
        }
        return false;
    }

    protected void ContinueButton_Click(object sender, EventArgs e)
    {
        Response.Redirect(string.Format(CultureInfo.InvariantCulture,
            "~/Application/Configuration/ADP/LogonDataSplittingConfiguration.aspx?DealerId={0}&DmsHostId={1}&SisConnectionId={2}",
            DealerId,
            DmsHost.Id,
            SisConn.Id), true);
    }

    protected void RerunQueriesButton_Click(object sender, EventArgs e)
    {
        Response.AddHeader("Refresh", "15");
        ProcessingPanel.Visible = true;
        ElapsedTimeLabel.Text = "00:00";

        AccountQuery = Query.NewQuery(SisConn, TransactionType.GetAccounts);
        JournalQuery = Query.NewQuery(SisConn, TransactionType.GetJournals);
        ScheduleQuery = Query.NewQuery(SisConn, TransactionType.GetSchedules);

        AccountLabel.Text = GetStatusMessage(AccountQuery.Status);
        JournalsLabel.Text = GetStatusMessage(JournalQuery.Status);
        SchedulesLabel.Text = GetStatusMessage(ScheduleQuery.Status);

        CompletePanel.Visible = false;
        ReRunPanel.Visible = false;
        ErrorPanel.Visible = false;
        StatusPanel.Visible = true;
    }

    protected void LoginErrorHyperlink_Init(object sender, EventArgs e)
    {
        LoginErrorHyperlink.NavigateUrl = string.Format(CultureInfo.InvariantCulture,
                                                        "~/Application/Configuration/ADP/SisConnection.aspx?Mode=Update&DealerId={0}&DmsHostId={1}&SisConnectionId={2}",
                                                        DealerId,
                                                        DmsHost.Id,
                                                        SisConn.Id);
    }
}