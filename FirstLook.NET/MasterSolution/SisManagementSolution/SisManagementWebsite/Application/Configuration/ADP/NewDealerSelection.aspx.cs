using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Csla.Web;
using FirstLook.Sis.Management.DomainModel.Configuration.Adp;

public partial class Application_Configuration_ADP_NewDealerSelection : Page
{
    private string SearchTerm
    {
        get { return ViewState["SearchTerm"] == null ? "" : ViewState["SearchTerm"].ToString(); }
        set { ViewState["SearchTerm"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void DealerNameTextBox_TextChanged(object sender, EventArgs e)
    {
        DealerSearch();
    }

    protected void DealerSearchButton_Click(object sender, EventArgs e)
    {
        DealerSearch();
    }

    private void DealerSearch()
    {
        SearchTerm = DealerNameTextBox.Text;
        DealerGridView.PageIndex = 0;
        DealerGridView.DataBind();
    }

    protected void ClearSearchButton_Click(object sender, EventArgs e)
    {
        SearchTerm = "";
        DealerGridView.DataBind();
    }

    protected void DealerGridView_DataBound(object sender, EventArgs e)
    {
        DealerNameTextBox.Text = SearchTerm;
        NoDealerText.Visible = DealerGridView.Rows.Count < 1;
    }

    protected void DealerGridViewDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        e.BusinessObject = DealerConfigurationInfoCollection.GetNewDealers(
            SearchTerm,
            e.SortExpression,
            e.MaximumRows,
            e.StartRowIndex);
    }

    protected void DealerGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Configure"))
        {
            int selectedIndex = Convert.ToInt32(e.CommandArgument);

            GridView grid = sender as GridView;

            if (grid != null)
            {
                int dealerId = (int) grid.DataKeys[selectedIndex].Values[0];

                ConfigurationWorkStep workStep = (ConfigurationWorkStep)grid.DataKeys[selectedIndex].Values[1];
                
                switch (workStep)
                {
                    case ConfigurationWorkStep.DealerConfiguration:
                        Response.Redirect(string.Format(CultureInfo.InvariantCulture, "~/Application/Configuration/ADP/Dealer.aspx?DealerId={0}", dealerId), true);
                        break;
                    case ConfigurationWorkStep.DmsHostConfiguration:
                        Response.Redirect(string.Format(CultureInfo.InvariantCulture, "~/Application/Configuration/ADP/ExistingDmsHostSelection.aspx?DealerId={0}", dealerId), true);
                        break;
                    case ConfigurationWorkStep.SisConnectionConfiguration:
                        Response.Redirect(string.Format(CultureInfo.InvariantCulture, "~/Application/Configuration/ADP/ExistingSisConnectionSelection.aspx?DealerId={0}&DmsHostId={1}", dealerId, Dealer.GetDealer(dealerId, FirstLook.Sis.Management.DomainModel.Configuration.Environment.PreProduction).DmsHostId), true);
                        break;
                    case ConfigurationWorkStep.SisConnectionTest:
                        Response.Redirect(string.Format(CultureInfo.InvariantCulture,
                            "~/Application/Configuration/ADP/DownloadDmsReferenceData.aspx?DealerId={0}&DmsHostId={1}&SisConnectionId={2}",
                            dealerId, 
                            Dealer.GetDealer(dealerId, FirstLook.Sis.Management.DomainModel.Configuration.Environment.PreProduction).DmsHostId, 
                            Dealer.GetDealer(dealerId, FirstLook.Sis.Management.DomainModel.Configuration.Environment.PreProduction).Connections[0]), true);
                        break;
                    case ConfigurationWorkStep.LogonDataSharingConfiguration:
                        //need to choose a connection from the collection, Params: DealerId, DmsHostId, SisConnectionId
                        Response.Redirect(string.Format(CultureInfo.InvariantCulture,
                            "~/Application/Configuration/ADP/LogonDataSplittingConfiguration.aspx?DealerId={0}&DmsHostId={1}&SisConnectionId={2}",
                            dealerId,
                            Dealer.GetDealer(dealerId, FirstLook.Sis.Management.DomainModel.Configuration.Environment.PreProduction).DmsHostId,
                            Dealer.GetDealer(dealerId, FirstLook.Sis.Management.DomainModel.Configuration.Environment.PreProduction).Connections[0]), true);
                        break;
                    case ConfigurationWorkStep.LogonDataExtractionConfiguration:
                        //need to choose a connection from the collection, Params: DealerId, DmsHostId, SisConnectionId
                        Response.Redirect(string.Format(CultureInfo.InvariantCulture,
                            "~/Application/Configuration/ADP/LogonDataExtractionConfiguration.aspx?DealerId={0}&DmsHostId={1}&SisConnectionId={2}",
                            dealerId,
                            Dealer.GetDealer(dealerId, FirstLook.Sis.Management.DomainModel.Configuration.Environment.PreProduction).DmsHostId,
                            Dealer.GetDealer(dealerId, FirstLook.Sis.Management.DomainModel.Configuration.Environment.PreProduction).Connections[0]), true);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
    }    
}
