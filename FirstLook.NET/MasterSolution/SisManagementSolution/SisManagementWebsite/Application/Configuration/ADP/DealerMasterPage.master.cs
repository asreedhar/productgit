using System;
using System.Web.UI;
using FirstLook.Sis.Management.DomainModel.Configuration.Adp;

public partial class Application_Configuration_ADP_DealerMasterPage : MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string value = Request.QueryString["DealerId"];

        if (!string.IsNullOrEmpty(value))
        {
            int id;

            if (int.TryParse(value, out id))
            {
                DealerConfigurationInfo dealer = DealerConfigurationInfo.GetDealer(id);

                DealerLabel.Text = string.Format("{0} / {1} / {2}", dealer.Name, dealer.DealerGroupName, dealer.Active ? "Active" : "Inactive");
            }
        }

        Page.Header.DataBind(); // resolve JS links
        string mode = Request.QueryString["Mode"];
        if (!string.IsNullOrEmpty(mode))
        {
            if (mode.Equals("Update"))
            {
                ApplicationSiteMapPath.Visible = false;
            }
            else
            {
                ApplicationSiteMapPath.Visible = true;
            }
        }
    }
}
