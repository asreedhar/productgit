<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SisConnection.aspx.cs" Inherits="Application_Configuration_ADP_SisConnection" MasterPageFile="~/Application/Configuration/ADP/DealerMasterPage.master" %>

<asp:Content ContentPlaceHolderID="TitleContentPlaceHolder" runat="server">
    <title>SIS Connection</title>
</asp:Content>

<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
    <csla:CslaDataSource ID="AccountingLogonDataSource" runat="server"
        OnSelectObject="AccountingLogonDataSource_SelectObject">
    </csla:CslaDataSource>
    <csla:CslaDataSource ID="FinanceLogonDataSource" runat="server"
        OnSelectObject="FinanceLogonDataSource_SelectObject">
    </csla:CslaDataSource>
    <fieldset>
        <legend>SIS Connection</legend>
        <p>Please enter the SIS Host Connection information in the form below.</p>
        <asp:BulletedList ID="ErrorList" runat="server" Visible="false" CssClass="ErrorList">
        </asp:BulletedList>
        	
            <p class="ColumnOne">
                <asp:Label ID="SisUserNameLabel" runat="server" Text="SIS User Name: " AssociatedControlID="SisUsernameTextBox"></asp:Label>
                <asp:TextBox runat="server" ID="SisUsernameTextBox" MaxLength="255" />
            </p>
            <p class="ColumnTwo">
                <asp:Label ID="SisPasswordLabel" runat="server" Text="SIS Password:" AssociatedControlID="SisPasswordTextBox"></asp:Label>
                <asp:TextBox runat="server" ID="SisPasswordTextBox" MaxLength="255"  />
            </p>            
             <p class="ColumnOne">
                <asp:Label ID="DmsUserNameLabel" runat="server" Text="DMS User Name: " AssociatedControlID="DmsUsernameTextBox"></asp:Label>
                <asp:TextBox runat="server" ID="DmsUsernameTextBox" MaxLength="255" />
            </p>
            <p class="ColumnTwo">
                <asp:Label ID="DmsPasswordLabel" runat="server" Text="DMS Password:" AssociatedControlID="DmsPasswordTextBox"></asp:Label>
                <asp:TextBox runat="server" ID="DmsPasswordTextBox" MaxLength="255"  />
            </p>
        <div class="ColumnOne">
            <p>
                <asp:Label ID="AccountingLogonLabel" runat="server" AssociatedControlID="AccountingLogonRadioButton">-A Logon#: <small>(Existing)</small></asp:Label>
                <asp:RadioButton runat="server" ID="AccountingLogonRadioButton" 
                        AutoPostBack="true"
                        Checked="true"
                        GroupName="AccountingLogonRadioButtonGroup"
                        OnCheckedChanged="AccountingLogonRadioButton_CheckedChanged" />
                <asp:DropDownList runat="server" ID="AccountingLogonDropDownList"
                        AppendDataBoundItems="true"
                        DataTextField="Name"
                        DataValueField="Id"
                        DataSourceID="AccountingLogonDataSource"
                        OnDataBound="AccountingLogonDropDownList_DataBound">
                    <asp:ListItem Text="Please Select" Value="0" />
                </asp:DropDownList>
            </p>
            <p>
                <asp:Label ID="NewAccountingLogonLabel" runat="server" AssociatedControlID="NewAccountingLogonRadioButton"><small>(New)</small></asp:Label>
                <asp:RadioButton runat="server" ID="NewAccountingLogonRadioButton"
                        AutoPostBack="true"
                        GroupName="AccountingLogonRadioButtonGroup" 
                        OnCheckedChanged="AccountingLogonRadioButton_CheckedChanged"
                         />
                <asp:TextBox runat="server" ID="NewAccountingLogonTextBox"
                        Enabled="False"
                        MaxLength="50"
                         />
            </p>
        </div>
        <div class="ColumnTwo">            
            <p>
                <asp:Label ID="FinanceLogonLabel" runat="server" AssociatedControlID="FinanceLogonRadioButton">-FI Logon#: <small>(Existing)</small></asp:Label>
                <asp:RadioButton runat="server" ID="FinanceLogonRadioButton" 
                        AutoPostBack="true"
                        Checked="true"
                        GroupName="FinanceLogonRadioButtonGroup"
                        OnCheckedChanged="FinanceLogonRadioButton_CheckedChanged"
                         />
                <asp:DropDownList runat="server" ID="FinanceLogonDropDownList"
                        AppendDataBoundItems="true"
                        DataTextField="Name"
                        DataValueField="Id"
                        DataSourceID="FinanceLogonDataSource"
                        OnDataBound="FinanceLogonDropDownList_DataBound"
                        >
                    <asp:ListItem Text="Please Select" Value="0" />
                </asp:DropDownList>
            </p>
            <p>
                <asp:Label ID="NewFinanceLogonLabel" runat="server" AssociatedControlID="NewFinanceLogonRadioButton"><small>(New)</small></asp:Label>
                <asp:RadioButton runat="server" ID="NewFinanceLogonRadioButton"
                        AutoPostBack="true"
                        GroupName="FinanceLogonRadioButtonGroup" 
                        OnCheckedChanged="FinanceLogonRadioButton_CheckedChanged"
                         />
                <asp:TextBox runat="server" ID="NewFinanceLogonTextBox" Enabled="False" MaxLength="50"  />
            </p>
            <p>
                <asp:Button runat="server" ID="CancelButton" Text="Cancel" OnClick="CancelButton_Click"  />
                <asp:Button runat="server" ID="SaveButton" Text="Save" OnClick="SaveButton_Click"  />
            </p>
        </div>
    </fieldset>
</asp:Content>
