using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Csla;
using Csla.Web;
using FirstLook.Sis.Management.DomainModel.Configuration.Adp;

public partial class Application_Configuration_ADP_ExistingSisConnectionSelection : Page
{
    private Guid DmsHostId
    {
        get
        {
            string id = Request.QueryString["DmsHostId"];

            if (string.IsNullOrEmpty(id))
            {
                throw new InvalidOperationException("Page requires a DmsHostId");
            }
            else
            {
                return new Guid(id);
            }
        }
    }

    private DmsHost _dmsHost;

    private DmsHost DmsHost
    {
        get
        {
            if (_dmsHost == null)
            {
                _dmsHost = DmsHost.GetDmsHost(DmsHostId);
            }

            return _dmsHost;
        }
    }

    private Dealer _dealer;

    private Dealer Dealer
    {
        get
        {
            string id = Request.QueryString["DealerId"];

            if (string.IsNullOrEmpty(id))
            {
                throw new ApplicationException("Missing DealerId");
            }
            else if(_dealer == null)
            {
                _dealer = Dealer.GetDealer(int.Parse(id), FirstLook.Sis.Management.DomainModel.Configuration.Environment.PreProduction);
            }

            return _dealer;
        }
    }

    protected void SisConnectionHyperLink_Init(object sender, EventArgs e)
    {
        SisConnectionHyperLink.NavigateUrl = string.Format(
            CultureInfo.InvariantCulture,
            "~/Application/Configuration/ADP/SisConnection.aspx?DealerId={0}&DmsHostId={1}",
            Dealer.Id, DmsHostId);
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void AccountingLogonDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        FilteredBindingList<Logon> list = new FilteredBindingList<Logon>(DmsHost.Logons);
        list.ApplyFilter("LogonType", LogonType.Accounting);
        e.BusinessObject = list;
    }

    protected void FinanceLogonDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        FilteredBindingList<Logon> list = new FilteredBindingList<Logon>(DmsHost.Logons);
        list.ApplyFilter("LogonType", LogonType.Finance);
        e.BusinessObject = list;
    }

    protected void SisConnectionGridViewDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        Guid? accountingLogonId = null, financeLogonId = null;

        if (!string.Equals(AccountingLogonDropDownList.SelectedValue, "0"))
            accountingLogonId = new Guid(AccountingLogonDropDownList.SelectedValue);

        if (!string.Equals(FinanceLogonDropDownList.SelectedValue, "0"))
            financeLogonId = new Guid(FinanceLogonDropDownList.SelectedValue);

        e.BusinessObject = SisConnectionInfoCollection.GetSisConnections(
            DmsHostId,
            accountingLogonId,
            financeLogonId,
            e.SortExpression,
            e.MaximumRows,
            e.StartRowIndex);
    }

    protected void SearchButton_Click(object sender, EventArgs e)
    {
        SisConnectionGridView.DataBind();
    }

    protected void ClearButton_Click(object sender, EventArgs e)
    {
        AccountingLogonDropDownList.SelectedIndex = -1;
        FinanceLogonDropDownList.SelectedIndex = -1;

        SisConnectionGridView.DataBind();
    }

    protected void SisConnectionGridView_DataBound(object sender, EventArgs e)
    {
        NoSisConnectionText.Visible = SisConnectionGridView.Rows.Count < 1;
    }

    protected void SisConnectionGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Select"))
        {
            int selectedIndex = Convert.ToInt32(e.CommandArgument);

            GridView grid = sender as GridView;

            if (grid != null)
            {
                Guid sisConnectionId = (Guid) grid.DataKeys[selectedIndex].Value;

                if (!Dealer.Connections.Contains(sisConnectionId))
                {
                    Dealer.Connections.Assign(DmsHost.SisConnections.GetItem(sisConnectionId));
                    Dealer.Save();
                }

                Response.Redirect(string.Format(CultureInfo.InvariantCulture,
                    "~/Application/Configuration/ADP/DownloadDmsReferenceData.aspx?DealerId={0}&DmsHostId={1}&SisConnectionId={2}",
                    Dealer.Id,
                    DmsHostId,
                    sisConnectionId), true);
            }
        }
    }
}
