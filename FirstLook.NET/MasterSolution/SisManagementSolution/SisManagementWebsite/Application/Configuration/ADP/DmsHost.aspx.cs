using System;
using System.Globalization;
using System.Transactions;
using System.Web.UI;
using System.Web.UI.WebControls;
using App_Code;
using Csla.Validation;
using Csla.Web;
using FirstLook.Sis.Management.DomainModel.Configuration.Adp;
using TimeZone=FirstLook.Sis.Management.DomainModel.Configuration.Adp.TimeZone;
using Environment=FirstLook.Sis.Management.DomainModel.Configuration.Environment;

public partial class Application_Configuration_ADP_DmsHost : Page
{
    private DmsHost _dmsHost;
    private Dealer _dealer;

    private Dealer Dealer
    {
        get
        {
            if (_dealer == null)
            {
                string id = Request.QueryString["DealerId"];

                if (string.IsNullOrEmpty(id))
                {
                    throw new ApplicationException("Missing DealerId");
                }
                else
                {
                    _dealer = Dealer.GetDealer(int.Parse(id), Environment.PreProduction);
                }
            }

            return _dealer;
        }
    }

    private DmsHost DmsHost
    {
        get
        {
            if (_dmsHost == null)
            {
                string id = Request.QueryString["DmsHostId"];

                if (string.IsNullOrEmpty(id))
                {
                    _dmsHost = DmsHost.NewDmsHost();
                }
                else
                {
                    _dmsHost = DmsHost.GetDmsHost(new Guid(id));
                }
            }
            
            return _dmsHost;
        }
    }
    private bool IsUpdate
    {
        get
        {
            return (string.Equals(Request.QueryString["Mode"], "Update"));
        }
    }
    private bool IsEdit
    {
        get
        {
            return (string.Equals(Request.QueryString["Mode"], "Edit"));
        }
    }
    private string EditRedirect
    {
        get
        {
            return (System.Web.HttpUtility.UrlDecode(Request.QueryString["EditRedirect"]));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PopulateForm(DmsHost);
        }

        if (!IsUpdate)
        {
            CancelButton.Visible = false;
        }
    }

    protected void TimeZoneDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        e.BusinessObject = TimeZoneCollection.GetTimeZones();
    }

    protected void HourDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        e.BusinessObject = Hour.GetHours();
    }

    protected void MinuteDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        e.BusinessObject = Minute.GetMinutes();
    }

    protected void TimeZoneDropDownList_DataBound(object sender, EventArgs e)
    {
        if (!DmsHost.IsNew)
            foreach (ListItem item in TimeZoneDropDownList.Items)
                item.Selected = string.Equals(item.Value, ToString(DmsHost.TimeZone.Id));
    }

    protected void BackupStartHourDropDownList_DataBound(object sender, EventArgs e)
    {
        if (!DmsHost.IsNew)
            foreach (ListItem item in BackupStartHourDropDownList.Items)
                item.Selected = string.Equals(item.Value, ToString(DmsHost.BackupBegin.Hour));
    }

    protected void BackupStartMinuteDropDownList_DataBound(object sender, EventArgs e)
    {
        if (!DmsHost.IsNew)
            foreach (ListItem item in BackupStartMinuteDropDownList.Items)
                item.Selected = string.Equals(item.Value, ToString(DmsHost.BackupBegin.Minute));
    }

    protected void BackupEndHourDropDownList_DataBound(object sender, EventArgs e)
    {
        if (!DmsHost.IsNew)
            foreach (ListItem item in BackupEndHourDropDownList.Items)
                item.Selected = string.Equals(item.Value, ToString(DmsHost.BackupEnd.Hour));
    }

    protected void BackupEndMinuteDropDownList_DataBound(object sender, EventArgs e)
    {
        if (!DmsHost.IsNew)
            foreach (ListItem item in BackupEndMinuteDropDownList.Items)
                item.Selected = string.Equals(item.Value, ToString(DmsHost.BackupEnd.Minute));
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        LoadComplete += MarshallForm;
    }

    private void MarshallForm(object sender, EventArgs e)
    {
        LoadComplete -= MarshallForm;

        ErrorList.Items.Clear();

        MarshallForm(DmsHost);

        if (DmsHost.IsValid && Dealer.IsValid)
        {
            ErrorList.Visible = false;

            bool isSaveError = false;
            try
            {
                DmsHostCreateAssignCommand.Execute(Dealer, DmsHost);
            }
            catch (Exception ex)
            {
                ErrorList.Visible = true;
                ErrorList.Items.Add(ex.Message);
                isSaveError = true;
            }

            if (!isSaveError)
            {
                Redirect();
            }
        }
        else
        {
            ErrorList.Visible = true;

            foreach (BrokenRule rule in Dealer.BrokenRulesCollection)
            {
                ErrorList.Items.Add(rule.Description);
            }

            foreach (BrokenRule rule in DmsHost.BrokenRulesCollection)
            {
                ErrorList.Items.Add(rule.Description);
            }

            foreach (BrokenRule rule in DmsHost.ContactInformation.BrokenRulesCollection)
            {
                ErrorList.Items.Add(rule.Description);
            }

            if (DmsHost.Credential != null)
            {
                foreach (BrokenRule rule in DmsHost.Credential.BrokenRulesCollection)
                {
                    ErrorList.Items.Add(rule.Description);
                }
            }
        }
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        if (IsUpdate)
        {
            LoadComplete += ResetForm;
            Redirect();
        }
    }

    protected void ResetForm(object sender, EventArgs e)
    {
        LoadComplete -= ResetForm;

        _dmsHost = null;

        PopulateForm(DmsHost);
    }

    private void Redirect()
    {
        if (IsUpdate)
        {
            Response.Redirect(string.Format(CultureInfo.InvariantCulture,
            "~/Application/Configuration/ADP/ExistingDealerConfiguration.aspx?DealerId={0}",
            Dealer.Id), true);
        }
        else if (IsEdit)
        {
            if (EditRedirect.ToLower().Contains("existingsisconnection.aspx")
                || EditRedirect.ToLower().Contains("downloaddmsreferencedata.aspx")
                || EditRedirect.ToLower().Contains("logondatasplittingconfiguration.aspx")
                || EditRedirect.ToLower().Contains("logondataextractionconfiguration.aspx"))
            {
                Response.Redirect(EditRedirect);
            }
        }

        Response.Redirect(string.Format(CultureInfo.InvariantCulture,
            "~/Application/Configuration/ADP/SisConnection.aspx?Mode=Insert&DealerId={0}&DmsHostId={1}",
            Dealer.Id,
            DmsHost.Id), true);

    }

    private void PopulateForm(DmsHost dmsHost)
    {
        DmsHostNameTextBox.Text = dmsHost.Name;

        if (dmsHost.IPAddress != null)
        {
            IPAddressByte1TextBox.Text = ToString(dmsHost.IPAddress.Byte1);
            IPAddressByte2TextBox.Text = ToString(dmsHost.IPAddress.Byte2);
            IPAddressByte3TextBox.Text = ToString(dmsHost.IPAddress.Byte3);
            IPAddressByte4TextBox.Text = ToString(dmsHost.IPAddress.Byte4);
        }
        
        PopulateTelephoneNumber(
            Telephone1AreaCodeTextBox,
            Telephone1ExchangeCodeTextBox,
            Telephone1StationCodeTextBox,
            dmsHost.DialUpTelephoneNumber1);

        PopulateTelephoneNumber(
            Telephone2AreaCodeTextBox,
            Telephone2ExchangeCodeTextBox,
            Telephone2StationCodeTextBox,
            dmsHost.DialUpTelephoneNumber2);

        PopulateCredential(
            CredentialUserNameTextBox,
            CredentialPasswordTextBox,
            dmsHost.Credential);

        if (dmsHost.FollowsDaylightSavingsTime)
        {
            BackupFollowsDaylightSavingsTimeRadioButtonList.Items[0].Selected = true;
            BackupFollowsDaylightSavingsTimeRadioButtonList.Items[1].Selected = false;
        }
        else
        {
            BackupFollowsDaylightSavingsTimeRadioButtonList.Items[0].Selected = false;
            BackupFollowsDaylightSavingsTimeRadioButtonList.Items[1].Selected = true;
        }

        ConnectionLimitBusinessHoursTextBox.Text = ToString(dmsHost.BusinessHoursConnectionLimit);
        ConnectionLimitClosedHoursTextBox.Text = ToString(dmsHost.OffHoursConnectionLimit);

        PopulateContactInformation(dmsHost.ContactInformation);
    }

    private void PopulateContactInformation(ContactInformation contactInformation)
    {
        ContactNameTextBox.Text = contactInformation.Name;
        ContactEmailAddressTextBox.Text = contactInformation.EmailAddress;

        PopulateTelephoneNumber(
            ContactTelephoneLandAreaCodeTextBox,
            ContactTelephoneLandExchangeCodeTextBox,
            ContactTelephoneLandStationCodeTextBox,
            contactInformation.LandTelephoneNumber);

        PopulateTelephoneNumber(
            ContactTelephoneCellAreaCodeTextBox,
            ContactTelephoneCellExchangeCodeTextBox,
            ContactTelephoneCellStationCodeTextBox,
            contactInformation.CellTelephoneNumber);
    }

    private static void PopulateTelephoneNumber(ITextControl area, ITextControl exchange, ITextControl station, TelephoneNumber telephoneNumber)
    {
        if (telephoneNumber != null)
        {
            area.Text = ToString(telephoneNumber.AreaCode);
            exchange.Text = ToString(telephoneNumber.ExchangeCode);
            station.Text = ToString(telephoneNumber.StationCode);
        }
    }

    private static void PopulateCredential(ITextControl userName, ITextControl password, Credential credential)
    {
        if (credential != null)
        {
            userName.Text = credential.UserName;
            password.Text = credential.Password;
        }
    }

    private void MarshallForm(DmsHost dmsHost)
    {
        dmsHost.Name = DmsHostNameTextBox.Text;

        dmsHost.IPAddress = MarshallIPAddress(
            IPAddressByte1TextBox,
            IPAddressByte2TextBox,
            IPAddressByte3TextBox,
            IPAddressByte4TextBox);

        dmsHost.DialUpTelephoneNumber1 = MarshallTelephoneNumber(
            Telephone1AreaCodeTextBox,
            Telephone1ExchangeCodeTextBox,
            Telephone1StationCodeTextBox);

        dmsHost.DialUpTelephoneNumber2 = MarshallTelephoneNumber(
            Telephone2AreaCodeTextBox,
            Telephone2ExchangeCodeTextBox,
            Telephone2StationCodeTextBox);

        dmsHost.Credential = MarshallCredential(
            CredentialUserNameTextBox,
            CredentialPasswordTextBox);

        dmsHost.BackupBegin = MarshallTime(BackupStartHourDropDownList, BackupStartMinuteDropDownList);
        dmsHost.BackupEnd = MarshallTime(BackupEndHourDropDownList, BackupEndMinuteDropDownList);

        int timeZoneId = int.Parse(TimeZoneDropDownList.SelectedItem.Value);

        if (timeZoneId > 0)
        {
            dmsHost.TimeZone = new TimeZone(timeZoneId, TimeZoneDropDownList.SelectedItem.Text);
        }
        else
        {
            dmsHost.TimeZone = null;
        }

        dmsHost.FollowsDaylightSavingsTime = (string.Equals(BackupFollowsDaylightSavingsTimeRadioButtonList.SelectedValue, "True"));
        
        int i;

        if (int.TryParse(ConnectionLimitBusinessHoursTextBox.Text, out i))
        {
            dmsHost.BusinessHoursConnectionLimit = i;
        }

        if (int.TryParse(ConnectionLimitClosedHoursTextBox.Text, out i))
        {
            dmsHost.OffHoursConnectionLimit = i;
        }

        MarshallContactInformation(dmsHost.ContactInformation);
    }

    private void MarshallContactInformation(ContactInformation information)
    {
        information.Name = ContactNameTextBox.Text;

        information.EmailAddress = ContactEmailAddressTextBox.Text;

        information.LandTelephoneNumber = MarshallTelephoneNumber(
            ContactTelephoneLandAreaCodeTextBox,
            ContactTelephoneLandExchangeCodeTextBox,
            ContactTelephoneLandStationCodeTextBox);

        information.CellTelephoneNumber = MarshallTelephoneNumber(
            ContactTelephoneCellAreaCodeTextBox,
            ContactTelephoneCellExchangeCodeTextBox,
            ContactTelephoneCellStationCodeTextBox);
    }

    private IPAddress MarshallIPAddress(ITextControl byte1, ITextControl byte2, ITextControl byte3, ITextControl byte4)
    {
        IPAddress address = null;

        if (!AllEmpty(byte1, byte2, byte3, byte4))
        {
            if (!IPAddress.TryParse(byte1.Text, byte2.Text, byte3.Text, byte4.Text, ref address))
            {
                ErrorList.Items.Add("IP Address is not valid");
            }
        }

        return address;
    }

    private TelephoneNumber MarshallTelephoneNumber(ITextControl area, ITextControl exchange, ITextControl station)
    {
        TelephoneNumber telephone = null;

        if (!AllEmpty(area, exchange, station))
        {
            if (!TelephoneNumber.TryParse(area.Text, exchange.Text, station.Text, ref telephone))
            {
                ErrorList.Items.Add("Telephone number is not valid");
            }
        }
        
        return telephone;
    }

    private static Time MarshallTime(ListControl hours, ListControl minutes)
    {
        return new Time(
            int.Parse(hours.SelectedValue),
            int.Parse(minutes.SelectedValue));
    }

    private static Credential MarshallCredential(ITextControl userName, ITextControl password)
    {
        if (AllEmpty(userName, password))
        {
            return null;
        }
        Credential credential = Credential.NewCredential();
        credential.UserName = userName.Text;
        credential.Password = password.Text;
        return credential;
    }

    private static bool AllEmpty(params ITextControl[] textControls)
    {
        bool allEmpty = true;

        foreach (ITextControl textControl in textControls)
        {
            allEmpty &= string.IsNullOrEmpty(textControl.Text);
        }

        return allEmpty;
    }

    private static string ToString(int value)
    {
        return value.ToString(CultureInfo.InvariantCulture);
    }
}
