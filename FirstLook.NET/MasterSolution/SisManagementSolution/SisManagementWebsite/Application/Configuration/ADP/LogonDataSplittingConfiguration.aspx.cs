using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI.WebControls;
using Csla.Web;
using FirstLook.Sis.Management.DomainModel.Configuration.Adp;
using Environment = FirstLook.Sis.Management.DomainModel.Configuration.Environment;

public partial class Application_Configuration_ADP_LogonDataSplittingConfiguration : System.Web.UI.Page
{
    private Dealer _dealer;
    private DmsHost _DmsHost;
    private SisConnection _SisConnection;
    private Logon _Logon;
    private DealerLogonDataSplitConfiguration _Configuration;

    private Dealer Dealer
    {
        get
        {
            if (_dealer == null)
            {
                string id = Request.QueryString["DealerId"];

                if (string.IsNullOrEmpty(id))
                {
                    throw new ApplicationException("Missing DealerId");
                }
                _dealer = Dealer.GetDealer(int.Parse(id), Environment.PreProduction);
            }
            return _dealer;
        }
    }
    private DmsHost DmsHost
    {
        get
        {
            if (_DmsHost == null)
            {
                string id = Request.QueryString["DmsHostId"];

                if (string.IsNullOrEmpty(id))
                {
                    throw new ApplicationException("Missing DmsHostId");
                }
                _DmsHost = DmsHost.GetDmsHost(new Guid(id));
            }
            return _DmsHost;
        }
    }
    private SisConnection SisConn
    {
        get
        {
            if (_SisConnection == null)
            {
                string id = Request.QueryString["SisConnectionId"];

                if (string.IsNullOrEmpty(id))
                {
                    throw new ApplicationException("Missing SisConnectionId");
                }
                _SisConnection = DmsHost.SisConnections.GetItem(new Guid(id));
            }
            return _SisConnection;
        }
    }
    private Logon Logon
    {
        get
        {
            if (_Logon == null)
            {
                _Logon = DmsHost.Logons.GetItem(SisConn.AccountingLogon.Id);
            }
            return _Logon;
        }
    }
    private DealerLogonDataSplitConfiguration Configuration
    {
        get
        {
            if(_Configuration == null)
            {
                _Configuration = DealerLogonDataSplitConfiguration.GetDealerLogonDataSplitConfiguration(Dealer, Logon);
            }
            return _Configuration;
        }
    }

    //in memory lot splitting collections used to persist changes in viewstate before saving
    private List<DealerCompanyLogonDataSplit> CompanySplits
    {
        get
        {
            return ViewState["CompanySplits"] as List<DealerCompanyLogonDataSplit>;
        }
        set
        {
            ViewState["CompanySplits"] = value;
        }
    }
    private List<DealerLotLogonDataSplit> LotSplits
    {
        get
        {
            return ViewState["LotSplits"] as List<DealerLotLogonDataSplit>;
        }
        set
        {
            ViewState["LotSplits"] = value;
        }
    }
    private List<DealerStockLogonDataSplit> StockSplits
    {
        get
        {
            return ViewState["StockSplits"] as List<DealerStockLogonDataSplit>;
        }
        set
        {
            ViewState["StockSplits"] = value;
        }
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        LogonLiteral.Text = Logon.Name;

        if (!IsPostBack)
        {
            CompanySplits = new List<DealerCompanyLogonDataSplit>(Configuration.DealerCompanyLogonDataSplitCollection);
            LotSplits = new List<DealerLotLogonDataSplit>(Configuration.DealerLotLogonDataSplitCollection);
            StockSplits = new List<DealerStockLogonDataSplit>(Configuration.DealerStockLogonDataSplitCollection);

            if (Logon.UsesLogonDataSplitting.HasValue)
            {
                UsesLogonDataSplittingRadioButtonList.SelectedValue = Logon.UsesLogonDataSplitting.Value.ToString();
            }
        }
        LoginDataSplittingTabContainer.Visible = UsesLogonDataSplittingRadioButtonList.SelectedValue == "True";
    }

    protected void UsesLogonDataSplittingSaveButton_Click(object sender, EventArgs e)
    {
        Logon.UsesLogonDataSplitting = bool.Parse(UsesLogonDataSplittingRadioButtonList.SelectedValue);
        DmsHost.Save();

        if (UsesLogonDataSplittingRadioButtonList.SelectedValue == "True")
        {
            foreach (DealerCompanyLogonDataSplit o in Configuration.DealerCompanyLogonDataSplitCollection)
            {
                if (!CompanySplits.Exists(delegate(DealerCompanyLogonDataSplit s) { return (s.CompanyLogonDataSplit.DmsCompanyId == o.CompanyLogonDataSplit.DmsCompanyId); }))
                {
                    o.Delete();
                }
            }
            foreach (DealerCompanyLogonDataSplit split in CompanySplits)
            {
                if (!Configuration.DealerCompanyLogonDataSplitCollection.Contains(split.CompanyLogonDataSplit.DmsCompanyId))
                {
                    Configuration.DealerCompanyLogonDataSplitCollection.Assign(split);
                }
            }


            foreach (DealerLotLogonDataSplit o in Configuration.DealerLotLogonDataSplitCollection)
            {
                if (!LotSplits.Exists(delegate(DealerLotLogonDataSplit s) { return (s.LotLogonDataSplit.LotNumber == o.LotLogonDataSplit.LotNumber); }))
                {
                    o.Delete();
                }
            }
            foreach (DealerLotLogonDataSplit split in LotSplits)
            {
                if (!Configuration.DealerLotLogonDataSplitCollection.Contains(split.LotLogonDataSplit.LotNumber))
                {
                    Configuration.DealerLotLogonDataSplitCollection.Assign(split);
                }
            }


            foreach (DealerStockLogonDataSplit o in Configuration.DealerStockLogonDataSplitCollection)
            {
                if (!StockSplits.Exists(delegate(DealerStockLogonDataSplit s) { return (s.StockLogonDataSplit.StockPatternText == o.StockLogonDataSplit.StockPatternText && s.StockLogonDataSplit.StockPatternType == o.StockLogonDataSplit.StockPatternType); }))
                {
                    o.Delete();
                }
            }
            foreach (DealerStockLogonDataSplit split in StockSplits)
            {
                if (!Configuration.DealerStockLogonDataSplitCollection.Contains(split.StockLogonDataSplit.StockPatternText, split.StockLogonDataSplit.StockPatternType))
                {
                    Configuration.DealerStockLogonDataSplitCollection.Assign(split);
                }
            }
        }
        else
        {
            foreach (DealerCompanyLogonDataSplit o in Configuration.DealerCompanyLogonDataSplitCollection)
            {
                o.Delete();
            }
            CompanySplits.Clear();

            foreach (DealerLotLogonDataSplit o in Configuration.DealerLotLogonDataSplitCollection)
            {
                o.Delete();
            }
            LotSplits.Clear();

            foreach (DealerStockLogonDataSplit o in Configuration.DealerStockLogonDataSplitCollection)
            {
                o.Delete();
            }
            StockSplits.Clear();

        }
        Configuration.Save();

        if (Request["Mode"] != null && Request["Mode"].ToLower() == "update")
        {
            Response.Redirect(string.Format(CultureInfo.InvariantCulture,
                "~/Application/Configuration/ADP/ExistingDealerConfiguration.aspx?DealerId={0}",
                Dealer.Id), true);
        }
        else
        {
            Response.Redirect(string.Format(CultureInfo.InvariantCulture,
                "~/Application/Configuration/ADP/LogonDataExtractionConfiguration.aspx?DealerId={0}&DmsHostId={1}&SisConnectionId={2}",
                Dealer.Id,
                DmsHost.Id,
                SisConn.Id), true);
        }
    }

    #region Company Tab

    protected void DMSCompanyIdAddButton_Click(object sender, EventArgs e)
    {
        if (!CompanySplits.Exists(delegate(DealerCompanyLogonDataSplit s) { return (s.CompanyLogonDataSplit.DmsCompanyId == DMSCompanyIdTextBox.Text); }))
        {
            DealerCompanyLogonDataSplit dsplit = DealerCompanyLogonDataSplit.NewDealerCompanyLogonDataSplit();
            CompanyLogonDataSplit split = CompanyLogonDataSplit.NewCompanyLogonDataSplit();
            split.DmsCompanyId = DMSCompanyIdTextBox.Text;
            dsplit.CompanyLogonDataSplit = split;
            dsplit.DealerId = Dealer.Id;
            dsplit.Environment = Dealer.Environment;
            dsplit.LogonId = Logon.Id;
            CompanySplits.Add(dsplit);
            CompanyLoginDataSplittingGridView.DataBind();
        }
    }

    protected void CompanyLoginDataSplittingGridView_SelectObject(object sender, SelectObjectArgs e)
    {
        e.BusinessObject = CompanySplits;
    }

    protected void CompanyLoginDataSplittingGridView_DataBound(object sender, EventArgs e)
    {
        NoCompanyLoginDataSplitLabel.Visible = (CompanyLoginDataSplittingGridView.Rows.Count < 1);
    }

    protected void CompanyLoginDataSplittingGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if ((e.Row.FindControl("DMSCompanyId") as Label) != null)
            {
                ((Label)e.Row.FindControl("DMSCompanyId")).Text = ((DealerCompanyLogonDataSplit) e.Row.DataItem).CompanyLogonDataSplit.DmsCompanyId.ToString();
            }
            if ((e.Row.FindControl("DMSCompanyIdTextBox") as TextBox) != null)
            {
                ((TextBox)e.Row.FindControl("DMSCompanyIdTextBox")).Text = ((DealerCompanyLogonDataSplit) e.Row.DataItem).CompanyLogonDataSplit.DmsCompanyId.ToString();
            }
        }
    }

    protected void CompanyLoginDataSplittingGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int selectedIndex = Convert.ToInt32(e.CommandArgument);
        if (e.CommandName.Equals("Update"))
        {
            CompanySplits[selectedIndex].CompanyLogonDataSplit.DmsCompanyId = ((TextBox)((GridView) sender).Rows[selectedIndex].FindControl("DMSCompanyIdTextBox")).Text;
        }
        else if (e.CommandName.Equals("Delete"))
        {
            CompanySplits.RemoveAt(selectedIndex);
        }
    }

    #endregion Company Tab

    #region Lot Tab

    protected void LotNumberAddButton_Click(object sender, EventArgs e)
    {
        if (!LotSplits.Exists(delegate(DealerLotLogonDataSplit s) { return (s.LotLogonDataSplit.LotNumber == LotNumberTextBox.Text); }))
        {
            DealerLotLogonDataSplit dsplit = DealerLotLogonDataSplit.NewDealerLotLogonDataSplit();
            LotLogonDataSplit split = LotLogonDataSplit.NewLotLogonDataSplit();
            split.LotNumber = LotNumberTextBox.Text;
            dsplit.LotLogonDataSplit = split;
            dsplit.DealerId = Dealer.Id;
            dsplit.Environment = Dealer.Environment;
            dsplit.LogonId = Logon.Id;
            LotSplits.Add(dsplit);
            LotLoginDataSplittingGridView.DataBind();
        }
    }

    protected void LotLoginDataSplittingGridView_SelectObject(object sender, SelectObjectArgs e)
    {
        e.BusinessObject = LotSplits;
    }

    protected void LotLoginDataSplittingGridView_DataBound(object sender, EventArgs e)
    {
        NoLotLoginDataSplitLabel.Visible = (LotLoginDataSplittingGridView.Rows.Count < 1);
    }

    protected void LotLoginDataSplittingGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if ((e.Row.FindControl("LotNumber") as Label) != null)
            {
                ((Label)e.Row.FindControl("LotNumber")).Text = ((DealerLotLogonDataSplit)e.Row.DataItem).LotLogonDataSplit.LotNumber.ToString();
            }
            if ((e.Row.FindControl("LotNumberTextBox") as TextBox) != null)
            {
                ((TextBox)e.Row.FindControl("LotNumberTextBox")).Text = ((DealerLotLogonDataSplit)e.Row.DataItem).LotLogonDataSplit.LotNumber.ToString();
            }
        }
    }

    protected void LotLoginDataSplittingGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int selectedIndex = Convert.ToInt32(e.CommandArgument);
        if (e.CommandName.Equals("Update"))
        {
            LotSplits[selectedIndex].LotLogonDataSplit.LotNumber = ((TextBox)((GridView)sender).Rows[selectedIndex].FindControl("LotNumberTextBox")).Text;
        }
        else if (e.CommandName.Equals("Delete"))
        {
            LotSplits.RemoveAt(selectedIndex);
        }
    }

    #endregion Lot Tab

    #region Stock Tab

    protected void StockAddButton_Click(object sender, EventArgs e)
    {
        if (!StockSplits.Exists(delegate(DealerStockLogonDataSplit s) { return (s.StockLogonDataSplit.StockPatternText == StockPatternTextTextBox.Text && s.StockLogonDataSplit.StockPatternType == StockPatternTypeDropDownList.SelectedValue); }))
        {
            DealerStockLogonDataSplit dsplit = DealerStockLogonDataSplit.NewDealerStockLogonDataSplit();
            StockLogonDataSplit split = StockLogonDataSplit.NewStockLogonDataSplit();
            split.StockPatternText = StockPatternTextTextBox.Text;
            split.StockPatternType = StockPatternTypeDropDownList.SelectedValue;
            dsplit.StockLogonDataSplit = split;
            dsplit.DealerId = Dealer.Id;
            dsplit.Environment = Dealer.Environment;
            dsplit.LogonId = Logon.Id;
            StockSplits.Add(dsplit);
            StockLoginDataSplittingGridView.DataBind();
        }
    }

    protected void StockLoginDataSplittingGridView_SelectObject(object sender, SelectObjectArgs e)
    {
        e.BusinessObject = StockSplits;
    }

    protected void StockLoginDataSplittingGridView_DataBound(object sender, EventArgs e)
    {
        NoStockLoginDataSplitLabel.Visible = (StockLoginDataSplittingGridView.Rows.Count < 1);
    }

    protected void StockLoginDataSplittingGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if ((e.Row.FindControl("StockPatternText") as Label) != null)
            {
                ((Label)e.Row.FindControl("StockPatternText")).Text = ((DealerStockLogonDataSplit)e.Row.DataItem).StockLogonDataSplit.StockPatternText;
            }
            if ((e.Row.FindControl("StockPatternType") as Label) != null)
            {
                ((Label)e.Row.FindControl("StockPatternType")).Text = ((DealerStockLogonDataSplit)e.Row.DataItem).StockLogonDataSplit.StockPatternType;
            }

            if ((e.Row.FindControl("StockPatternTextTextBox") as TextBox) != null)
            {
                ((TextBox)e.Row.FindControl("StockPatternTextTextBox")).Text = ((DealerStockLogonDataSplit)e.Row.DataItem).StockLogonDataSplit.StockPatternText;
            }
            if ((e.Row.FindControl("StockPatternTypeDropDownList") as DropDownList) != null)
            {
                ((DropDownList)e.Row.FindControl("StockPatternTypeDropDownList")).SelectedValue = ((DealerStockLogonDataSplit)e.Row.DataItem).StockLogonDataSplit.StockPatternType;
            }
        }
    }

    protected void StockLoginDataSplittingGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int selectedIndex = Convert.ToInt32(e.CommandArgument);
        if (e.CommandName.Equals("Update"))
        {
            StockSplits[selectedIndex].StockLogonDataSplit.StockPatternText = ((TextBox)((GridView)sender).Rows[selectedIndex].FindControl("StockPatternTextTextBox")).Text;
            StockSplits[selectedIndex].StockLogonDataSplit.StockPatternType = ((DropDownList)((GridView)sender).Rows[selectedIndex].FindControl("StockPatternTypeDropDownList")).SelectedValue;
        }
        else if (e.CommandName.Equals("Delete"))
        {
            StockSplits.RemoveAt(selectedIndex);
        }
    }

    #endregion Stock Tab
}
