<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Application_Configuration_ADP_Default" MasterPageFile="~/Application/BasicMasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContentPlaceHolder" runat="server">
    <title>ADP / SIS Configuration</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
    <fieldset>
        <legend>ADP / SIS Configuration</legend>
        <ol>
            <li><asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Application/Configuration/ADP/ExistingDealerSelection.aspx">Existing Dealer Configuration</asp:HyperLink></li>
            <li><asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Application/Configuration/ADP/NewDealerSelection.aspx">New Dealer Configuration</asp:HyperLink></li>
        </ol>
    </fieldset>
</asp:Content>
