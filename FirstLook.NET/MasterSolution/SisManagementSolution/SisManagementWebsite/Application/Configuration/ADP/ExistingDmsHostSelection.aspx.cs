using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Csla.Web;
using FirstLook.Sis.Management.DomainModel.Configuration.Adp;
using Environment = FirstLook.Sis.Management.DomainModel.Configuration.Environment;

public partial class Application_Configuration_ADP_ExistingDmsHostSelection : Page
{
    private Dealer _dealer;

    private Dealer Dealer
    {
        get
        {
            if (_dealer == null)
            {
                string id = Request.QueryString["DealerId"];

                if (string.IsNullOrEmpty(id))
                {
                    throw new ApplicationException("Missing DealerId");
                }
                else
                {
                    _dealer = Dealer.GetDealer(int.Parse(id), Environment.PreProduction);
                }
            }

            return _dealer;
        }
    }
    private Uri EditReferringUrl
    {
        get
        {
            if (ViewState["EditReferringUrl"] == null)
                return null;
            return (Uri)ViewState["EditReferringUrl"];
        }
        set
        {
            ViewState["EditReferringUrl"] = value;
        }
    }

    protected void NewDmsHostLink_Init(object sender, EventArgs e)
    {
        NewDmsHostLink.NavigateUrl = string.Format(CultureInfo.InvariantCulture, "~/Application/Configuration/ADP/DmsHost.aspx?Mode=Insert&DealerId={0}", Request.QueryString["DealerId"]);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(EditReferringUrl == null)
        {
            EditReferringUrl = Request.UrlReferrer;
        }
    }

    protected void DmsHostSearchButton_Click(object sender, EventArgs e)
    {
        DmsHostGridView.DataBind();
    }

    protected void ClearSearchButton_Click(object sender, EventArgs e)
    {
        IPAddressByte1TextBox.Text = string.Empty;
        IPAddressByte2TextBox.Text = string.Empty;
        IPAddressByte3TextBox.Text = string.Empty;
        IPAddressByte4TextBox.Text = string.Empty;
        DmsHostNameTextBox.Text = string.Empty;
        TimeZoneDropDownList.SelectedIndex = 0;
        DmsHostGridView.DataBind();
    }

    protected void DmsHostGridViewDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        IPAddress address = null;

        IPAddress.TryParse(
             IPAddressByte1TextBox.Text,
             IPAddressByte2TextBox.Text,
             IPAddressByte3TextBox.Text,
             IPAddressByte4TextBox.Text,
             ref address);

        TelephoneNumber number = null;

        TelephoneNumber.TryParse(
             AreaCodeTextBox.Text,
             ExchangeCodeTextBox.Text,
             StationCodeTextBox.Text,
             ref number);

        e.BusinessObject = DmsHostInfoCollection.GetDmsHosts(
            Dealer.Id,
            DmsHostNameTextBox.Text,
            address,
            number,
            int.Parse(TimeZoneDropDownList.SelectedValue),
            e.SortExpression,
            e.MaximumRows,
            e.StartRowIndex);
    }

    protected void DmsHostGridView_DataBound(object sender, EventArgs e)
    {
        NoDmsHostText.Visible = DmsHostGridView.Rows.Count < 1;
    }

    protected void DmsHostGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if(e.Row.RowType == DataControlRowType.DataRow)
        {
            ((Label) e.Row.FindControl("TimeZoneLabel")).Text = ((DmsHostInfo) e.Row.DataItem).TimeZone.Name;
        }
    }

    protected void DmsHostGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Edit"))
        {
            int selectedIndex = Convert.ToInt32(e.CommandArgument);

            GridView grid = sender as GridView;

            if (grid != null)
            {
                Guid dmsHostId = (Guid)grid.DataKeys[selectedIndex].Value;

                Dealer.DmsHostId = dmsHostId;
                Dealer.Save();
                
                Response.Redirect(string.Format(CultureInfo.InvariantCulture,
                    "~/Application/Configuration/ADP/DmsHost.aspx?DealerId={0}&DmsHostId={1}&Mode=Edit&EditRedirect={2}",
                    Dealer.Id,
                    dmsHostId,
                    System.Web.HttpUtility.UrlEncode(EditReferringUrl.ToString())), true);
            }
        }
        else if (e.CommandName.Equals("Select"))
        {
            int selectedIndex = Convert.ToInt32(e.CommandArgument);

            GridView grid = sender as GridView;

            if (grid != null)
            {
                Guid dmsHostId = (Guid)grid.DataKeys[selectedIndex].Value;

                Dealer.DmsHostId = dmsHostId;
                Dealer.Connections.RemoveAll();
                Dealer.Save();

                Response.Redirect(string.Format(CultureInfo.InvariantCulture,
                    "~/Application/Configuration/ADP/ExistingSisConnectionSelection.aspx?DealerId={0}&DmsHostId={1}",
                    Dealer.Id,
                    dmsHostId), true);
            }
        }
    }

    protected void TimeZoneDataSource_SelectObject(object sender, SelectObjectArgs e)
    {
        e.BusinessObject = TimeZoneCollection.GetTimeZones();
    }

}
