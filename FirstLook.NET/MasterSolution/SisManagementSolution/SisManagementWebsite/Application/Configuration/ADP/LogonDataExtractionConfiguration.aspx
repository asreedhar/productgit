<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LogonDataExtractionConfiguration.aspx.cs" Inherits="Application_Configuration_ADP_LogonDataExtractionConfiguration" MasterPageFile="~/Application/Configuration/ADP/DealerMasterPage.master" %>

<asp:Content ContentPlaceHolderID="TitleContentPlaceHolder" runat="server">
    <title>Logon Data Extraction Configuration</title>
</asp:Content>


<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
    <fieldset>
        <legend>Logon Data Extraction Configuration</legend>
        <p>Please identify and configure the data extraction criteria.</p>
        <cwc:TabContainer ID="LogonDataExtraction" runat="server" ActiveTabIndex="0" HasEmptyState="false">
            <cwc:TabPanel ID="AccountsTabPanel" runat="server" Text="Accounts">
                <p>Please identify and configure the relevant accounts in these sections.</p>
                <cwc:TabContainer ID="AccountsTabContainer" runat="server" ActiveTabIndex="0" HasEmptyState="false">
                    <cwc:TabPanel ID="InventoryAccountsTabPanel" runat="server" Text="Inventory Accounts">
                        <p>Please identify the dealers inventory accounts.You can filter the list of accounts using the form below.</p>
                        <p>
                            Department:
                            <asp:DropDownList ID="InventoryAccountDepartmentDropDownList" runat="server" />
                            <asp:Button ID="InventoryAccountFilterButton" runat="server" Text="Filter" OnClick="InventoryAccountFilterButton_Click" />
                            <asp:Button ID="InventoryAccountFilterClearButton" runat="server" Text="Clear" OnClick="InventoryAccountFilterClearButton_Click" />
                        </p>
                            <csla:CslaDataSource ID="InventoryAccountGridViewDataSource" runat="server"
                                OnSelectObject="InventoryAccountGridViewDataSource_SelectObject"
                                TypeSupportsPaging="false"
                                TypeSupportsSorting="true">
                            </csla:CslaDataSource>
                            <csla:CslaDataSource ID="DealerInventoryAccountGridViewDataSource" runat="server"
                                OnSelectObject="DealerInventoryAccountGridViewDataSource_SelectObject"
                                TypeSupportsPaging="false"
                                TypeSupportsSorting="true">
                            </csla:CslaDataSource>
                        <p>
                            <asp:Panel runat="server" SkinID="ScrollableTablePanel">
                            <asp:GridView ID="InventoryAccountGridView" runat="server"
                                AllowPaging="false"
                                AllowSorting="true"
                                AutoGenerateColumns="false"
                                DataKeyNames="Id"
                                OnDataBound="InventoryAccountGridView_DataBound"
                                OnRowDataBound="InventoryAccountGridView_RowDataBound"
                                DataSourceID="InventoryAccountGridViewDataSource">
                                <Columns>
                                    <asp:TemplateField HeaderText="Include?" AccessibleHeaderText="Include?">
                                        <ItemTemplate><asp:CheckBox ID="IncludeCheckBox" runat="server"/></ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField AccessibleHeaderText="Number" SortExpression="Number" HeaderText="#" DataField="Number" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="Type" SortExpression="Type" HeaderText="Type" DataField="Type" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="SubType" SortExpression="SubType" HeaderText="SubType" DataField="SubType" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="ControlType" SortExpression="ControlType" HeaderText="ControlType" DataField="ControlType" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="Department" SortExpression="Department" HeaderText="Department" DataField="Department" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="Description" SortExpression="Description" HeaderText="Description" DataField="Description" ReadOnly="true" />
                                </Columns>
                            </asp:GridView>
                            </asp:Panel>
                            <br />
                            <div style="margin-left:100px;">
                                <asp:Button runat="server" ID="CheckAllInventoryAccountsButton" OnClick="CheckAllInventoryAccountsButton_Click" Text="Check All Accounts" />
                                <asp:Button runat="server" ID="IncludeInventoryAccountsButton" OnClick="IncludeInventoryAccountsButton_Click" Text="Include Checked Accounts" />
                            </div>
                            <br />
                            <br />
                            <br />
                            
                            <asp:Panel runat="server" SkinID="ScrollableTablePanel">
                            <asp:GridView ID="DealerInventoryAccountGridView" runat="server"
                                AutoGenerateColumns="false"
                                DataKeyNames="AccountId"
                                OnRowCommand="DealerInventoryAccountGridView_RowCommand"
                                DataSourceID="DealerInventoryAccountGridViewDataSource">
                                <Columns>
                                    <asp:ButtonField ButtonType="Button" CommandName="Exclude" Text="Exclude"/>
                                    <asp:BoundField AccessibleHeaderText="Number" HeaderText="#" DataField="Number" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="Type" HeaderText="Type" DataField="Type" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="SubType" HeaderText="SubType" DataField="SubType" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="ControlType" HeaderText="ControlType" DataField="ControlType" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="Department" HeaderText="Department" DataField="Department" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="Description" HeaderText="Description" DataField="Description" ReadOnly="true" />
                                </Columns>
                            </asp:GridView>
                            </asp:Panel>
                        </p>
                    </cwc:TabPanel>
                    <cwc:TabPanel ID="SalesAccountsTabPanel" runat="server" Text="Sales Accounts">
                        <p>Please identify the dealers sales accounts.You can filter the list of accounts using the form below.</p>
                        <p>
                            Department:
                            <asp:DropDownList ID="SalesAccountDepartmentDropDownList" runat="server" />
                            <asp:Button ID="SalesAccountFilterButton" runat="server" Text="Filter" OnClick="SalesAccountFilterButton_Click" />
                            <asp:Button ID="SalesAccountFilterClearButton" runat="server" Text="Clear" OnClick="SalesAccountFilterClearButton_Click" />
                        </p>
                            <csla:CslaDataSource ID="SalesAccountGridViewDataSource" runat="server"
                                OnSelectObject="SalesAccountGridViewDataSource_SelectObject"
                                TypeSupportsPaging="false"
                                TypeSupportsSorting="false">
                            </csla:CslaDataSource>
                            <csla:CslaDataSource ID="DealerSalesAccountGridViewDataSource" runat="server"
                                OnSelectObject="DealerSalesAccountGridViewDataSource_SelectObject"
                                TypeSupportsPaging="false"
                                TypeSupportsSorting="false">
                            </csla:CslaDataSource>
                        <p>
                            <asp:Panel runat="server" SkinID="ScrollableTablePanel">
                            <asp:GridView ID="SalesAccountGridView" runat="server"
                                AllowPaging="false"
                                AllowSorting="true"
                                OnDataBound="SalesAccountGridView_DataBound"
                                AutoGenerateColumns="false"
                                DataKeyNames="Id"
                                OnRowDataBound="SalesAccountGridView_RowDataBound"
                                DataSourceID="SalesAccountGridViewDataSource">
                                <Columns>
                                    <asp:TemplateField HeaderText="Include?" AccessibleHeaderText="Include?">
                                        <ItemTemplate><asp:CheckBox ID="IncludeCheckBox" runat="server"/></ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField AccessibleHeaderText="Number" SortExpression="Number" HeaderText="#" DataField="Number" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="Type" SortExpression="Type" HeaderText="Type" DataField="Type" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="SubType" SortExpression="SubType" HeaderText="SubType" DataField="SubType" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="ControlType" SortExpression="ControlType" HeaderText="ControlType" DataField="ControlType" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="Department" SortExpression="Department" HeaderText="Department" DataField="Department" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="Description" SortExpression="Description" HeaderText="Description" DataField="Description" ReadOnly="true" />
                                </Columns>
                            </asp:GridView>
                            </asp:Panel>
                            <br />
                            <div style="margin-left:100px;">
                                <asp:Button runat="server" ID="CheckAllSalesAccountsButton" OnClick="CheckAllSalesAccountsButton_Click" Text="Check All Accounts" />
                                <asp:Button runat="server" ID="IncludeSalesAccountsButton" OnClick="IncludeSalesAccountsButton_Click" Text="Include Checked Accounts" />
                            </div>
                            <br />
                            <br />
                            <br />

                            <asp:Panel runat="server" SkinID="ScrollableTablePanel">
                            <asp:GridView ID="DealerSalesAccountGridView" runat="server"
                                AutoGenerateColumns="false"
                                DataKeyNames="AccountId"
                                OnRowCommand="DealerSalesAccountGridView_RowCommand"
                                DataSourceID="DealerSalesAccountGridViewDataSource">
                                <Columns>
                                    <asp:ButtonField ButtonType="Button" CommandName="Exclude" Text="Exclude"/>
                                    <asp:BoundField AccessibleHeaderText="Number" HeaderText="#" DataField="Number" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="Type" HeaderText="Type" DataField="Type" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="SubType" HeaderText="SubType" DataField="SubType" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="ControlType" HeaderText="ControlType" DataField="ControlType" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="Department" HeaderText="Department" DataField="Department" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="Description" HeaderText="Description" DataField="Description" ReadOnly="true" />
                                    <asp:TemplateField HeaderText="IsFrontEnd?" AccessibleHeaderText="IsFrontEnd?">
                                        <ItemTemplate><asp:CheckBox ID="IsFrontEndCheckBox" Checked='<%# Bind("IsFrontEnd") %>' runat="server"/></ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="IsRetail?" AccessibleHeaderText="IsRetail?">
                                        <ItemTemplate><asp:CheckBox ID="IsRetailCheckBox" Checked='<%# Bind("IsRetail") %>' runat="server"/></ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="IsWholesale?" AccessibleHeaderText="IsWholesale?">
                                        <ItemTemplate><asp:CheckBox ID="IsWholesaleCheckBox" Checked='<%# Bind("IsWholesale") %>' runat="server"/></ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            </asp:Panel>
                        </p>
                    </cwc:TabPanel>
                    <cwc:TabPanel ID="CostAccountsTabPanel" runat="server" Text="Cost Accounts">
                        <p>Please identify the dealers cost accounts.You can filter the list of accounts using the form below.</p>
                        <p>
                            Department:
                            <asp:DropDownList ID="CostAccountDepartmentDropDownList" runat="server" />
                            <asp:Button ID="CostAccountsFilterButton" runat="server" Text="Filter" OnClick="CostAccountFilterButton_Click" />
                            <asp:Button ID="CostAccountsFilterClearButton" runat="server" Text="Clear" OnClick="CostAccountFilterClearButton_Click" />
                        </p>
                            <csla:CslaDataSource ID="CostAccountGridViewDataSource" runat="server"
                                OnSelectObject="CostAccountGridViewDataSource_SelectObject"
                                TypeSupportsPaging="false"
                                TypeSupportsSorting="false">
                            </csla:CslaDataSource>
                            <csla:CslaDataSource ID="DealerCostAccountGridViewDataSource" runat="server"
	                            OnSelectObject="DealerCostAccountGridViewDataSource_SelectObject"
	                            TypeSupportsPaging="false"
	                            TypeSupportsSorting="false">
                            </csla:CslaDataSource>
                        <p>
                            <asp:Panel runat="server" SkinID="ScrollableTablePanel">
                            <asp:GridView ID="CostAccountGridView" runat="server"
                                AllowPaging="false"
                                AllowSorting="true"
                                AutoGenerateColumns="false"
                                DataKeyNames="Id"
                                OnDataBound="CostAccountGridView_DataBound"
                                OnRowDataBound="CostAccountGridView_RowDataBound"
                                DataSourceID="CostAccountGridViewDataSource">
                                <Columns>
                                    <asp:TemplateField HeaderText="Include?" AccessibleHeaderText="Include?">
                                        <ItemTemplate><asp:CheckBox ID="IncludeCheckBox" runat="server"/></ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField AccessibleHeaderText="Number" SortExpression="Number" HeaderText="#" DataField="Number" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="Type" SortExpression="Type" HeaderText="Type" DataField="Type" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="SubType" SortExpression="SubType" HeaderText="SubType" DataField="SubType" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="ControlType" SortExpression="ControlType" HeaderText="ControlType" DataField="ControlType" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="Department" SortExpression="Department" HeaderText="Department" DataField="Department" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="Description" SortExpression="Description" HeaderText="Description" DataField="Description" ReadOnly="true" />
                                </Columns>
                            </asp:GridView>
                            </asp:Panel>
                            <br />
                            <div style="margin-left:100px;">
                                <asp:Button runat="server" ID="CheckAllCostAccountsButton" OnClick="CheckAllCostAccountsButton_Click" Text="Check All Accounts" />
                                <asp:Button runat="server" ID="IncludeCostAccountsButton" OnClick="IncludeCostAccountsButton_Click" Text="Include Checked Accounts" />
                            </div>
                            <br />
                            <br />
                            <br />

                            <asp:Panel runat="server" SkinID="ScrollableTablePanel">
                            <asp:GridView ID="DealerCostAccountGridView" runat="server"
                                AutoGenerateColumns="false"
                                DataKeyNames="AccountId"
                                OnRowCommand="DealerCostAccountGridView_RowCommand"
                                DataSourceID="DealerCostAccountGridViewDataSource">
                                <Columns>
                                    <asp:ButtonField ButtonType="Button" CommandName="Exclude" Text="Exclude"/>
                                    <asp:BoundField AccessibleHeaderText="Number" HeaderText="#" DataField="Number" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="Type" HeaderText="Type" DataField="Type" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="SubType" HeaderText="SubType" DataField="SubType" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="ControlType" HeaderText="ControlType" DataField="ControlType" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="Department" HeaderText="Department" DataField="Department" ReadOnly="true" />
                                    <asp:BoundField AccessibleHeaderText="Description" HeaderText="Description" DataField="Description" ReadOnly="true" />
                                    <asp:TemplateField HeaderText="IsFrontEnd?" AccessibleHeaderText="IsFrontEnd?">
                                        <ItemTemplate><asp:CheckBox ID="IsFrontEndCheckBox" Checked='<%# Bind("IsFrontEnd") %>' runat="server"/></ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            </asp:Panel>
                        </p>
                    </cwc:TabPanel>
                </cwc:TabContainer>
            </cwc:TabPanel>
            <cwc:TabPanel ID="JournalsTabPanel" runat="server" Text="Journals">
                <p>Please identify and configure the relevant journals in these sections.
                   You can filter the list of journals using the form below.</p>
                <p>
                    <asp:Panel ID="JournalTypeFilterPanel" DefaultButton="JournalTypeFilterButton" runat="server">
                        <asp:Label ID="JournalTypeLabel" runat="server" Text="Journal Type:" AssociatedControlID="JournalTypeFilterButton"></asp:Label>
                        <asp:TextBox ID="JournalTypeTextBox" MaxLength="5" runat="server" OnTextChanged="JournalTypeTextBox_TextChanged"></asp:TextBox>
                        <asp:Button ID="JournalTypeFilterButton" runat="server" Text="Filter" OnClick="JournalTypeFilterButton_Click" />
                    </asp:Panel>
                </p>
                <csla:CslaDataSource ID="JournalGridViewDataSource" runat="server"
                    OnSelectObject="JournalGridViewDataSource_SelectObject"
                    TypeSupportsPaging="false"
                    TypeSupportsSorting="true">
                </csla:CslaDataSource>
                <csla:CslaDataSource ID="DealerJournalGridViewDataSource" runat="server"
                    OnSelectObject="DealerJournalGridViewDataSource_SelectObject"
                    TypeSupportsPaging="false"
                    TypeSupportsSorting="true">
                </csla:CslaDataSource>
                <csla:CslaDataSource ID="AssetClassDataSource" runat="server"
                    OnSelectObject="AssetClassDataSource_SelectObject"
                    TypeSupportsPaging="false"
                    TypeSupportsSorting="false">
                </csla:CslaDataSource>
                <csla:CslaDataSource ID="ReferenceTypeDataSource" runat="server"
                    OnSelectObject="ReferenceTypeDataSource_SelectObject"
                    TypeSupportsPaging="false"
                    TypeSupportsSorting="false">
                </csla:CslaDataSource>
                <p>
                    <asp:Panel ID="JournalGridViewPanel" SkinID="ScrollableTablePanel" runat="server">
                    <asp:GridView ID="JournalGridView" runat="server"
                        AllowPaging="false"
                        AllowSorting="true"
                        AutoGenerateColumns="false"
                        DataKeyNames="Id"
                        OnRowDataBound="JournalGridView_RowDataBound"
                        DataSourceID="JournalGridViewDataSource">
                        <Columns>
                            <asp:TemplateField HeaderText="Include?" AccessibleHeaderText="Include?">
                                <ItemTemplate><asp:CheckBox ID="IncludeCheckBox" runat="server"/></ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField AccessibleHeaderText="Number" SortExpression="Number" HeaderText="#" DataField="Number" ReadOnly="true" />
                            <asp:BoundField AccessibleHeaderText="Type" SortExpression="Type" HeaderText="Type" DataField="Type" ReadOnly="true" />
                            <asp:BoundField AccessibleHeaderText="Description" SortExpression="Description" HeaderText="Description" DataField="Description" ReadOnly="true" />
                        </Columns>
                    </asp:GridView>
                    </asp:Panel>
                    <br />
                    <div style="margin-left:100px;">
                        <asp:Button runat="server" ID="IncludeJournalsButton" OnClick="IncludeJournalsButton_Click" Text="Include Checked Accounts" />
                    </div>
                    <br />
                    <br />
                    <br />

                    <asp:Panel ID="DealerJournalGridViewPanel" SkinID="ScrollableTablePanel" runat="server">
                    <asp:GridView ID="DealerJournalGridView" runat="server"
                        AutoGenerateColumns="false"
                        DataKeyNames="JournalId"
                        OnRowCommand="DealerJournalGridView_RowCommand"
                        DataSourceID="DealerJournalGridViewDataSource">
                        <Columns>
                            <asp:ButtonField ButtonType="Button" CommandName="Exclude" Text="Exclude"/>
                            <asp:BoundField AccessibleHeaderText="Number" HeaderText="#" DataField="Number" ReadOnly="true" />
                            <asp:BoundField AccessibleHeaderText="Type" HeaderText="Type" DataField="Type" ReadOnly="true" />
                            <asp:BoundField AccessibleHeaderText="Description" HeaderText="Description" DataField="Description" ReadOnly="true" />
                            
                            <asp:TemplateField AccessibleHeaderText="IsPurchase?" HeaderText="IsPurchase?" >
                                <ItemTemplate><asp:CheckBox ID="IsPurchaseCheckBox" Checked='<%# Bind("IsPurchase") %>' runat="server"/></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField AccessibleHeaderText="IsTrade?" HeaderText="IsTrade?">
                                <HeaderTemplate>IsTrade?</HeaderTemplate>
                                <ItemTemplate><asp:CheckBox ID="IsTradeCheckBox" Checked='<%# Bind("IsTrade") %>' runat="server"/></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField AccessibleHeaderText="N/U" HeaderText="N/U">
                                <ItemTemplate>
                                    <asp:DropDownList ID="AssetClassDropDownList" 
                                        DataSourceID="AssetClassDataSource" 
                                        DataTextField="Value" 
                                        DataValueField="Key" 
                                        SelectedValue='<%# Bind("AssetClassId") %>'
                                        runat="server"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField AccessibleHeaderText="ReferenceType" HeaderText="ReferenceType">
                                <ItemTemplate>
                                    <asp:DropDownList ID="ReferenceTypeDropDownList" 
                                        DataSourceID="ReferenceTypeDataSource" 
                                        DataTextField="Value" 
                                        DataValueField="Key" 
                                        SelectedValue='<%# Bind("ReferenceTypeId") %>'
                                        runat="server"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    </asp:Panel>
                </p>
            </cwc:TabPanel>
            <cwc:TabPanel ID="SchedulesTabPanel" runat="server" Text="Schedules">
                <p>The schedules pulled from the dms are listed here.</p>
                <csla:CslaDataSource ID="ScheduleGridViewDataSource" runat="server"
                    OnSelectObject="ScheduleGridViewDataSource_SelectObject"
                    TypeSupportsPaging="false"
                    TypeSupportsSorting="false">
                </csla:CslaDataSource>
                <p>
                <asp:Panel runat="server" SkinID="ScrollableTablePanel">
                <asp:GridView ID="ScheduleGridView" runat="server"
                    AllowPaging="true"
                    AllowSorting="true"
                    AutoGenerateColumns="false"
                    DataKeyNames="Id"
                    DataSourceID="ScheduleGridViewDataSource">
                    <Columns>
                        <asp:BoundField AccessibleHeaderText="Number" HeaderText="#" DataField="Number" ReadOnly="true" />
                        <asp:BoundField AccessibleHeaderText="Type" HeaderText="Type" DataField="Type" ReadOnly="true" />
                        <asp:BoundField AccessibleHeaderText="ControlType" HeaderText="ControlType" DataField="ControlType" ReadOnly="true" />
                        <asp:BoundField AccessibleHeaderText="Control2Type" HeaderText="Control2Type" DataField="Control2Type" ReadOnly="true" />
                        <asp:BoundField AccessibleHeaderText="Description" HeaderText="Description" DataField="Description" ReadOnly="true" />
                    </Columns>
                </asp:GridView>
                </asp:Panel>
                </p>
            </cwc:TabPanel>
        </cwc:TabContainer>
        <p class="controls">
            <asp:BulletedList ID="ErrorList" runat="server" Visible="false" CssClass="ErrorList"></asp:BulletedList>
            
            <%-- This brings the user back to existing dealer page.  Only available on UpdateMode --%>
            <asp:Button ID="DoneButton" runat="server" Text="Back To Dealer" OnClick="DoneButton_Click" Visible="false" />
            
            <%-- These buttons keep the user on the page --%>
            <asp:Button ID="ResetButton" runat="server" Text="Reset" OnClick="ResetButton_Click" />
            <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
        </p>
    </fieldset>
</asp:Content>
