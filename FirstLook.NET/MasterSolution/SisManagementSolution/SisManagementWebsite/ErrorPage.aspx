<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ErrorPage.aspx.cs" Inherits="ErrorPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div>
                <h2 runat="server" id="ErrorTitle"></h2>
                <p runat="server" id="ErrorDescription"></p>
                <p runat="server" id="ErrorClose" visible="false">If the problem persists, please contact your account manager or call Customer Service at 1-877-378-5665. Click <a href="javascript:window.close()">here</a> to close the window.</p>
                <p runat="server" id="ErrorTarget" visible="false">You can <asp:HyperLink runat="server" ID="PageLink">enter the application</asp:HyperLink> if you like. If the problem persists, please contact your account manager or call Customer Service at 1-877-378-5665.</p>
                <p runat="server" id="ErrorHome" visible="false">You can <asp:HyperLink runat="server" ID="HomeLink">return home</asp:HyperLink> if you like. If the problem persists, please contact your account manager or call Customer Service at 1-877-378-5665.</p>
            </div>
        </div>
    </form>
</body>
</html>
