var hijacked__doPostBack;
var hijacked__doPostBack_event = function() {};
function hijack__doPostBack() {
	if (__doPostBack != undefined) {
		hijacked__doPostBack = __doPostBack;
		__doPostBack = function() { 
			hijacked__doPostBack_event = function() { hijacked__doPostBack(arguments); };
		 };
	};
}

(function($) {	
		
    // ================
    // = On Page Load =
    // ================
    $(document).ready(function() {	
        new SISManagementPage();
    });
    
    $(window).bind('load', function() {
        if (hijacked__doPostBack != undefined) {
    		__doPostBack = function() {
    			hijacked__doPostBack(arguments[0], arguments[1]);
    		};
    		hijacked__doPostBack_event();
    	};
    });

    // ==========================
    // = Objects and Extensions =
    // ==========================
    function EventDelegate(fn, scope) {
        return function(e) {
            fn.call(scope, e);
        };
    }
    
    function SISManagementPage() {
        this.setBodyClass();
        this.loadPageScroll();
        $(window).bind('resize', {
            callback: this.isWide
        },
        this.onResize);
        $(window).bind('scroll', this.onScroll);
        this.tabControl = new TabControl();
        this.scrollableGridViews = new ScrollableGridViews();
        this.pageSpecificInit();
    }
    SISManagementPage.prototype = {
        setBodyClass: function() {
            $('body').addClass(this.getBodyClass()).addClass('loaded');
            this.isWide();
        },
        getBodyClass: function() {
            if (this.bodyClass) return this.bodyClass;
            var regex = /\/(\w*)\..*/,
            match = window.location.pathname.toString().match(regex);
            this.bodyClass = match[1] || "";
            return this.bodyClass;
        },
        isWide: function() {
            var body = $('body');
            if (body.width() > 1800) {
                body.addClass('wide');
            } else {
                body.removeClass('wide');
            }
        },
        pageSpecificInit: function() {
            switch (this.getBodyClass()) {
            case 'SisConnection':
                new LogonInput(document.getElementById('ctl00_BodyContentPlaceHolder_NewAccountingLogonTextBox'), 'a');
                new LogonInput(document.getElementById('ctl00_BodyContentPlaceHolder_NewFinanceLogonTextBox'), 'fi');
                this.tabControl.selectFirst();
                break;
            case 'LogonDataExtractionConfiguration':
                var table_row_edit = $('#ctl00_BodyContentPlaceHolder_DealerSalesAccountGridView tbody tr');
                for (var i=0,l=table_row_edit.length; i < l; i++) {
                	if (table_row_edit.get(i)) {
                	    new SalesAccountValidator(table_row_edit.get(i));
                	};
                };
                break;
            case 'CustomTransformationsConfiguration':
            	new DealerDefinedValidator(document.getElementById('ctl00_BodyContentPlaceHolder_DealerDefinedPanel'));
            	this.tabControl.selectFirst();
            	break;
            default:
            	new IPAddressByteValidator();
            	this.tabControl.selectFirst();
                return;
            }
        },
        loadPageScroll: function() {
            var pattern = /scrollToWindow=(\d*)/;
            var result = document.cookie.match(pattern);
            if (result && result[1]) {
                if (document.documentElement.scrollTop != undefined) {
                    document.documentElement.scrollTop = result[1];
                } else if (document.body.scrollTop != undefined) {
                    document.body.scrollTop = result[1];
                }
            }
        },
        // =============================
        // = Events (this == element)) =
        // =============================
        onResize: function(e) {
            $('body').removeClass('loaded');
            setTimeout(function() {
                $('body').addClass('loaded');
            },
            100);
            try {
                e.data.callback();
            } catch(er) {}
        },
        onScroll: function(e) {
            var pos = document.documentElement.scrollTop || document.body.scrollTop;
            document.cookie = "scrollToWindow=" + pos + ";";
        }

    };

    function ScrollableGridViews() {
        this.gridViews = $(this.selector);
        this.initGridViewButtons();
        this.getCookieScrollPosition();
        this.registerScrollBack();
        this.fixHeaderClicks();
    }
    ScrollableGridViews.prototype = {
        selector: "div.scrollable_table_container table.grid_view",
        initGridViewButtons: function() {
            for (var i = 0, l = this.gridViews.length; i < l; i++) {
                this.gridViews[i].buttons = new GridViewButtons(this.gridViews[i]);
            };
        },
        registerScrollBack: function() {
            for (var i = 0, l =this.gridViews.length; i < l; i++) {
                $(this.gridViews[i]).parent().bind('scroll', this.remeberScrollPosition);
            };
        },
        fixHeaderClicks: function () {
    		$('thead',this.gridViews).bind('mousedown',this.onMouseDown);
        },
        getCookieScrollPosition: function() {
            var pattern = /scrollTo([^=]*)=(\d*)/g;
            var result;
            while ((result = pattern.exec(document.cookie)) != null) {
                if (result && result[1] && result[2]) {
                    var el = document.getElementById(result[1]);
                    if (el) {
                        var parent = el.parentElement || el.parent;
                        if (parent) {
                            parent.scrollTop = result[2];
                        };
                    };
                };
            }
        },
        // ============================
        // = Events (this == element) =
        // ============================
        remeberScrollPosition: function(e) {
            var pos = this.scrollTop || 0,
            id = $('.grid_view', this)[0].id || '';
            document.cookie = "scrollTo" + id + "=" + pos + ";";            
        },
        onMouseDown: function (e) {
        	var a;
    		if ($(e.target).is('a')) {
    			a = e.target;
    		} else {
    			a = $('a input',e.target).get(0);
    		}
    		try	{ 
    			a.click();
    			if (a.href.indexOf('Sort') != -1) {
    				var id = $(a).parents('table.grid_view').get(0).id;
    				document.cookie = "scrollTo" + id + "=0;";
    			};
    		} catch (er) {}
        }
        
    };

    function GridViewButtons(scope) {
        this.buttons = $(this.selector, scope || document);
        this.id = scope.id || undefined;
        this.getCookieFocus();
        this.populateButtonCache();
    }
    GridViewButtons.prototype = {
        selector: "input[onclick]",
        setFocus: function(i) {
            if (this.buttons[i]) {
                this.buttons[i].focus();
            }
        },
        getCookieFocus: function(onCookieFocus) {
            if (this.buttons.length <= 0) return;
            var grid_view_focus = document.cookie.match(/focusOn\=([^;]*)/);
            if (grid_view_focus && grid_view_focus[1]) {
                grid_view_focus = grid_view_focus[1];
            } else {
                return;
            }
            var pattern = /focusOn([^=]*)=(\d*)/g;
            var result;
            while ((result = pattern.exec(document.cookie)) != null) {
                if (result &&
                result[1] && result[1] == grid_view_focus &&
                result[2] && result[2] < this.buttons.length) {
                    this.setFocus(result[2]);
                };
            }
        },
        populateButtonCache: function() {
            for (var i = 0, l = this.buttons.length; i < l; i++) {
                $(this.buttons[i]).bind('click', {
                    index: i
                },
                this.addFocusCookie);
            }
        },
        addFocusCookie: function(e) {
            e.preventDefault();
            var grid_view = $(this).parents('.grid_view').get(0),
            id = grid_view.id || undefined;
            if (id && e.data && e.data.index) {
                var focus_cookie = "focusOn" + id + "=" + e.data.index + ";";
                document.cookie = focus_cookie;
                var focus_gridview = "focusOn=" + id + ";";
                document.cookie = focus_gridview;
            }
        }
    };

    function TabControl() {
        this.inputs = this.getInputs();
        this.links = this.getLinks();
        this.bindEvents();
    }
    TabControl.prototype = {
        getInputs: function() {
            return $(':input:enabled:visible');
        },
        getLinks: function() {
            return $('a:visible');
        },
        bindEvents: function() {
            for (var i = 0, l = (this.inputs.length - 1); i < l; i++) {
                if (this.inputs[i].maxLength != undefined) {
                    $(this.inputs[i]).bind('keyup', {
                        nextInput: this.inputs[i + 1]
                    },
                    this.onKeyup);
                }
                if ($(this.inputs[i]).is(':text')) {
                    $(this.inputs[i]).bind('focus', this.selectOnFocus);
                }
            }
            for (var i = 0, l = this.links.length; i < l; i++) {
                $(this.links[i]).bind('focus', this.onLinkFocus).bind('blur', this.onLinkBlur);
            };
        },
        bindAccessKeys: function() {
            var text = '';
            for (var i = 0; i < this.inputs.length; i++) {
                if (this.inputs[i].type == 'submit' || this.inputs[i].type == 'button') {
                    text = this.inputs[i].value;
                    if (text != '' && text != undefined && text != 'Include' && text != 'Exclude')
                    {
                        this.inputs[i].accessKey = text.substring(0, 1);
                    }
                }
            };
        },
        selectFirst: function () {
			try {
	            this.inputs[0].focus();
	            if (this.inputs[0].type == 'text') {
	            	this.inputs[0].select();
	            };            
	        } catch(er) {
	            var found = false;
	            for (var i = 0, l = this.links.length; i < l; i++) {
	                if (found) continue;                
	                if (this.links[i].id && this.links[i].id.match(/HomeLink/)) {
	                    this.links[i].focus();
	                    $(this).addClass('focus');
	                    found = true;
	                }
	            };
	        }
        },
        // ============================
        // = Events (this == element) =
        // ============================
        onKeyup: function(e) {
            if (e.keyCode === 9 || e.keyCode === 16) return; // tab or shift
            if (this.maxLength !== undefined && this.maxLength !== -1 && this.value &&
            this.value.length >= this.maxLength && e.data.nextInput) {
                e.data.nextInput.focus();
            }
        },
        selectOnFocus: function(e) {
            try {
                this.select();
            } catch(e) {
                void(e);
            }
        },
        onLinkFocus: function(e) {
            $(this).addClass('focus');
        },
        onLinkBlur: function(e) {
            $(this).removeClass('focus');
        }
    };
    
    function IPAddressByteValidator(sel) {
    	this.inputs = $(sel || this.selector);
    	if (this.inputs.length != 0) {
    		this.addEvents();
    	}    	
    }
    IPAddressByteValidator.prototype = {
    	selector: "input.IPAddressByte",
    	addEvents: function () {
    		this.inputs.bind('keyup',this.onChange);
    	},
    	onChange: function (e) {
    		if (!(e.keyCode > 47 && e.keyCode < 58) &&
    			!(e.keyCode > 95 && e.keyCode < 106) && 
    			e.keyCode != 16 && e.keyCode != 9) { // numeric only
    			e.target.value = e.target.value.substring(0,e.target.value.length-1);
			}
			if ((e.keyCode == 110 || e.keyCode == 190) && e.target.value.length != 0) {
				try {
					$(e.target).next('input').focus();
				} catch (e) {}    				
    		}
    	}    	    	
    };

    function LogonInput(el, type) {
    	if (! !!el) return;
        this.input = el;
        this.type = type;
        this.suffix = this.getSuffix();
        this.addEvents();
    }
    LogonInput.prototype = {
        getSuffix: function() {
            switch (this.type) {
            case 'a':
                return "-A";
            case 'fi':
                return "-FI";
            }
        },
        addEvents: function() {
            if (this.suffix) {
                $(this.input).bind('keyup', {
                    suffix: this.suffix
                },
                this.onChange);
            }
        },
        onChange: function(e) {
            var regEx = new RegExp("(\w*)(" + e.data.suffix + ")"),
            thisVal = '',
            range;
            if (this.value == e.data.suffix) return;
            thisVal = this.value.replace(regEx, "$1");
            this.value = thisVal + e.data.suffix;
            if ((e.keyCode > 47 && e.keyCode < 90) || e.keyCode == 16) {
                // alphaNumeric or Shift
                if (this.createTextRange) {
                    range = this.createTextRange();
                    range.move("character", thisVal.length);
                    range.select();
                } else if (this.selectionStart) {
                    this.setSelectionRange(thisVal.length, thisVal.length);
                }
            } else {
                this.select();
            }
        }
    };

    function SalesAccountValidator(row) {
        this.row = row;
        this.container = $(this.row).parents('.scrollable_table_container');
        this.th = $('thead tr th',this.container);
        this.checkBoxes = this.getCheckBoxes();
        this.bindEvents();
        this.setInitState();
    }
    SalesAccountValidator.prototype = {
        getCheckBoxes: function() {
            var checkBoxes = $('input', this.row);
            var isFrontEnd,
            isRetailCheckBox,
            isWholesaleCheckBox;
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].id == undefined) continue;
                if (checkBoxes[i].id.indexOf("IsFrontEndCheckBox") != -1) {
                    isFrontEnd = checkBoxes[i];
                } else if (checkBoxes[i].id.indexOf("IsRetailCheckBox") != -1) {
                    isRetailCheckBox = checkBoxes[i];
                } else if (checkBoxes[i].id.indexOf("IsWholesaleCheckBox") != -1) {
                    isWholesaleCheckBox = checkBoxes[i];
                }
            };
            return {
                "isFrontEnd": isFrontEnd,
                "isRetail": isRetailCheckBox,
                "isWholesale": isWholesaleCheckBox
            };
        },
        setInitState: function () {
        	this.onChange({ target: this.checkBoxes.isWholesale});
        },
        bindEvents: function() {
            var changeDelegate = new EventDelegate(this.onChange, this);
            $(this.checkBoxes.isRetail).bind('click', changeDelegate);
            $(this.checkBoxes.isWholesale).bind('click', changeDelegate);
            $(this.checkBoxes.isFrontEnd).bind('click', changeDelegate);
            $(this.checkBoxes.isRetail).bind('change', changeDelegate);
            $(this.checkBoxes.isWholesale).bind('change', changeDelegate);
            $(this.checkBoxes.isFrontEnd).bind('change', changeDelegate);
        },
        onChange: function(e) {
        	var dim =0.55, 
        		needsRedraw = false,
        		scrollTop = this.container.attr('scrollTop') + 2;
            if (e.target.id.indexOf("IsWholesaleCheckBox") != -1) {
                if (e.target.checked) {
                	this.checkBoxes.isRetail.checked = false;
                    this.checkBoxes.isFrontEnd.checked = true;
                    $(this.checkBoxes.isFrontEnd).fadeTo('fast',dim);
                    needsRedraw = true;
                } else {
                	this.checkBoxes.isRetail.checked = true;
                	$(this.checkBoxes.isFrontEnd).fadeTo('fast',1);
                	needsRedraw = true;
                }
            } else if (e.target.id.indexOf("IsRetailCheckBox") != -1) {
            	if (e.target.checked) {
            		this.checkBoxes.isWholesale.checked = false;
            		$(this.checkBoxes.isFrontEnd).fadeTo('fast',1);
            		needsRedraw = true;
            	} else {
            		this.checkBoxes.isWholesale.checked = true;
            		this.checkBoxes.isFrontEnd.checked = true;
            		$(this.checkBoxes.isFrontEnd).fadeTo('fast',dim);
            		needsRedraw = true;
            	}
            } else if (e.target.id.indexOf("IsFrontEndCheckBox") != -1) {
            	if (!this.checkBoxes.isRetail.checked) {
            		e.target.checked = true;
            		$(this.checkBoxes.isFrontEnd).fadeTo('fast',1);
            		e.preventDefault();            		
            	}
            }
            if (needsRedraw) {
            	// ========================================================================================
	            // = This is an ugly hack to prevent the header from moving in the scrolling table, Sorry =
	            // ========================================================================================
            	this.th.css('top',scrollTop);
            };            
        }

    };

	function DealerDefinedValidator(el) {
		if (! !!el) return;
		this.parent = el;
		this.checkboxes = this.getCheckBoxes();
		this.packAmount = this.getPackAmountInput();
		this.bindEvents();
		if (this.checkboxes['All'].checked) 
			this.setAllDealsState();
		if (this.packAmount.value == "") 
			this.packAmount.value = "0";
	}
	DealerDefinedValidator.prototype = {
		getCheckBoxes: function () {
			function  getIdentifyer(el) {
				var id = el.id.split('_');
				return id[id.length-1];
			}
			var inputs = this.parent.getElementsByTagName('input'), c = 0, checkboxes = {};
			for (var i = 0, l = inputs.length; i < l;i++ ) {
				if (inputs[i].type.toLowerCase()== "checkbox") {
					checkboxes[c] = inputs[i];
					checkboxes[getIdentifyer(inputs[i])] = inputs[i];
					checkboxes.length = ++c;
				};
			}
			return checkboxes;
		},
		getPackAmountInput: function () {
			var input = this.parent.getElementsByTagName('input')[0];
			return input;
		},
		bindEvents:function () {
			var changeDelegate = new EventDelegate(this.onChange, this);
			$('input:checkbox', this.parent).bind('click', changeDelegate);
			$(this.packAmount).bind('keyup', this.onPackAmoutKeyup);
		},
		saveState: function () {
			this.prevState = {};
			this.prevState.checkboxes = [];
			this.prevState.textbox = [];
			this.prevState.select = [];
			for (var i=0,l=this.checkboxes.length; i < l; i++) {				
				if (this.checkboxes[i] == this.checkboxes['All']){
					this.prevState.checkboxes.push(false);
				} else {
					this.prevState.checkboxes.push(this.checkboxes[i].checked);
				};				
			};
			var text = $('input:checkbox ~ input:text',this.parent);
			for (var i=0,l=text.length; i < l; i++) {
				this.prevState.textbox.push(text[i].value);
			};
			var sel = $('select',this.parent);
			for (var i=0,l=sel.length; i < l; i++) {
				this.prevState.select.push(sel[i].selectedIndex);
			};
		},
		loadState: function () {
			if (this.prevState && this.prevState.checkboxes.length == this.checkboxes.length) {
				for (var i=0,l=this.checkboxes.length; i < l; i++) {
					this.checkboxes[i].checked = this.prevState.checkboxes[i];
				}
				
				var text = $('input:checkbox ~ input:text',this.parent);
				for (var i=0,l=text.length; i < l; i++) {
					text[i].value = this.prevState.textbox[i];
				};
				
				var sel = $('select', this.parent);
				for (var i=0,l=sel.length; i < l; i++) {
					sel[i].selectedIndex = this.prevState.select[i];
				};
			}
		},
		setAllDealsState: function () {
			this.saveState();
			var dim = 0.55;
			for (var i=0,l=this.checkboxes.length; i < l; i++) {
				if (this.checkboxes[i] == this.checkboxes['All']) continue;
				this.checkboxes[i].checked = true;
				$(this.checkboxes[i]).fadeTo('fast',dim);
			}
			$('input:checkbox ~ input:text', this.parent).not(this.checkboxes['All']).val('0').attr('readonly',true).fadeTo('fast', dim);
			$('select', this.parent).attr('disabled', true).attr('selectedIndex',0);
		},
		setSpecificDealsState: function () {
			this.loadState();
			this.prevState = undefined;
			$('input', this.parent).not(this.checkboxes['All']).attr('readOnly', false).fadeTo('fast', 1);
			$('select', this.parent).not(this.checkboxes['All']).attr('disabled', false);			
		},
		onChange: function (e) {
			if (e.target == this.checkboxes['All']) {
				if (e.target.checked) {
					this.setAllDealsState();
				} else {
					this.setSpecificDealsState();
				}
			} else {
				if (this.prevState) {
					e.preventDefault();
				};
			} 
		},
		onPackAmoutKeyup:function (e) {
			var regEx = /[^\d|\.]/g,
            thisVal = '',
            range;
            this.value = thisVal = this.value.replace(regEx, "");
            if ((e.keyCode > 47 && e.keyCode < 58) || 
            	(e.keyCode > 95 && e.keyCode < 106) || 
            	e.keyCode == 16 || e.keyCode == 110 || e.keyCode == 190) {
                // numeric or Shift or period
                if (this.createTextRange) {
                    range = this.createTextRange();
                    range.move("character", thisVal.length);
                    range.select();
                } else if (this.selectionStart) {
                    this.setSelectionRange(thisVal.length, thisVal.length);
                }
            } else {
                this.select();
            }
        }
	};
})(jQuery);