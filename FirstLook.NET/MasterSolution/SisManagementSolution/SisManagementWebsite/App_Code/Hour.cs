using System.Collections.Generic;
using System.Globalization;

namespace App_Code
{
    public class Hour
    {
        private static readonly Hour[] Hours;

        static Hour()
        {
            Hours = new Hour[24];
            
            for (int i = 0; i < 24; i++)
            {
                Hours[i] = new Hour(i, string.Format(CultureInfo.InvariantCulture, "{0:00}", i));
            }
        }

        private readonly int value;
        private readonly string text;

        private Hour(int value, string text)
        {
            this.value = value;
            this.text = text;
        }

        public int Value
        {
            get { return value; }
        }

        public string Text
        {
            get { return text; }
        }

        public static IEnumerable<Hour> GetHours()
        {
            return Hours;
        }
    }    
}


