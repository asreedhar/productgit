using System.Collections.Generic;
using System.Globalization;

namespace App_Code
{
    public class Minute
    {
        private static readonly Minute[] Minutes;

        static Minute()
        {
            Minutes = new Minute[4];

            for (int i = 0; i < 4; i++)
            {
                Minutes[i] = new Minute(i*15, string.Format(CultureInfo.InvariantCulture, "{0:00}", i*15));
            }
        }

        private readonly int value;
        private readonly string text;

        private Minute(int value, string text)
        {
            this.value = value;
            this.text = text;
        }

        public int Value
        {
            get { return value; }
        }

        public string Text
        {
            get { return text; }
        }

        public static IEnumerable<Minute> GetMinutes()
        {
            return Minutes;
        }
    }
}


