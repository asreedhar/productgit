using System;
using System.Collections.Specialized;
using System.Web;

namespace FirstLook.Sis.Management.WebControls
{
    public class QueryStringSiteMapProvider : XmlSiteMapProvider
    {
        public override void Initialize(string name, NameValueCollection attributes)
        {
            base.Initialize(name, attributes);
            SiteMapResolve += new SiteMapResolveEventHandler(QueryStringSiteMapProvider_SiteMapResolve);
        }

        SiteMapNode QueryStringSiteMapProvider_SiteMapResolve(object sender, SiteMapResolveEventArgs e)
        {
            if (SiteMap.CurrentNode == null)
                return null;

            SiteMapNode temp;
            temp = SiteMap.CurrentNode.Clone(true);

            SiteMapNode tempNode = temp;
            while (tempNode != null)
            {
                string qs = GetReliance(tempNode, e.Context);
                if (qs != null)
                    tempNode.Url += qs;

                tempNode = tempNode.ParentNode;
            }

            return temp;
        }

        private static string GetReliance(SiteMapNode node, HttpContext context)
        {
            //Check to see if the node supports reliance
            if (node["reliantOn"] == null)
                return null;

            NameValueCollection values = new NameValueCollection();
            string[] vars = node["reliantOn"].Split(",".ToCharArray());

            foreach (string s in vars)
            {
                string var = s.Trim();
                //Make sure the var exists in the querystring
                if (context.Request.QueryString[var] == null)
                    continue;

                values.Add(var, context.Request.QueryString[var]);
            }

            if (values.Count == 0)
                return null;

            return NameValueCollectionToString(values);
        }

        private static string NameValueCollectionToString(NameValueCollection col)
        {
            string[] parts = new string[col.Count];
            string[] keys = col.AllKeys;

            for (int i = 0; i < keys.Length; i++)
                parts[i] = keys[i] + "=" + col[keys[i]];

            string url = "?" + String.Join("&", parts);
            return url;
        }
    }
}
