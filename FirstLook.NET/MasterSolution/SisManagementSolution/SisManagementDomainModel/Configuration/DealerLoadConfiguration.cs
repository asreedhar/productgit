using System;
using System.Collections.Generic;
using System.Data;
using System.Security;
using System.Text;
using Csla;
using FirstLook.Common.Data;
using FirstLook.Sis.Management.DomainModel.Configuration.Adp;

namespace FirstLook.Sis.Management.DomainModel.Configuration
{
    public class DealerLoadConfiguration : BusinessBase<DealerLoadConfiguration>
    {
        #region Business Methods

        private Dealer dealer;
        private int dailyInventoryLoadHour;
        private int dailyInventoryLoadMinute;
        private int dailySalesLoadHour;
        private int dailySalesLoadMinute;
        private int inventoryLoadPriority;
        private int salesLoadPriority;

        public Dealer Dealer
        {
            get
            {
                CanReadProperty("Dealer", true);
                return dealer;
            }
            set
            {
                CanWriteProperty("Dealer", true);
                if (dealer != value)
                {
                    dealer = value;
                    PropertyHasChanged("Dealer");
                }
            }
        }
        public int DailyInventoryLoadHour
        {
            get
            {
                CanReadProperty("DailyInventoryLoadHour", true);
                return dailyInventoryLoadHour;
            }
            set
            {
                CanWriteProperty("DailyInventoryLoadHour", true);
                if (dailyInventoryLoadHour != value)
                {
                    dailyInventoryLoadHour = value;
                    PropertyHasChanged("DailyInventoryLoadHour");
                }
            }
        }
        public int DailyInventoryLoadMinute
        {
            get
            {
                CanReadProperty("DailyInventoryLoadMinute", true);
                return dailyInventoryLoadMinute;
            }
            set
            {
                CanWriteProperty("DailyInventoryLoadMinute", true);
                if (dailyInventoryLoadMinute != value)
                {
                    dailyInventoryLoadMinute = value;
                    PropertyHasChanged("DailyInventoryLoadMinute");
                }
            }
        }
        public int DailySalesLoadHour
        {
            get
            {
                CanReadProperty("DailySalesLoadHour", true);
                return dailySalesLoadHour;
            }
            set
            {
                CanWriteProperty("DailySalesLoadHour", true);
                if (dailySalesLoadHour != value)
                {
                    dailySalesLoadHour = value;
                    PropertyHasChanged("DailySalesLoadHour");
                }
            }
        }
        public int DailySalesLoadMinute
        {
            get
            {
                CanReadProperty("DailySalesLoadMinute", true);
                return dailySalesLoadMinute;
            }
            set
            {
                CanWriteProperty("DailySalesLoadMinute", true);
                if (dailySalesLoadMinute != value)
                {
                    dailySalesLoadMinute = value;
                    PropertyHasChanged("DailySalesLoadMinute");
                }
            }
        }
        public int InventoryLoadPriority
        {
            get
            {
                CanReadProperty("InventoryLoadPriority", true);
                return inventoryLoadPriority;
            }
            set
            {
                CanWriteProperty("InventoryLoadPriority", true);
                if (inventoryLoadPriority != value)
                {
                    inventoryLoadPriority = value;
                    PropertyHasChanged("InventoryLoadPriority");
                }
            }
        }
        public int SalesLoadPriority
        {
            get
            {
                CanReadProperty("SalesLoadPriority", true);
                return salesLoadPriority;
            }
            set
            {
                CanWriteProperty("SalesLoadPriority", true);
                if (salesLoadPriority != value)
                {
                    salesLoadPriority = value;
                    PropertyHasChanged("SalesLoadPriority");
                }
            }
        }

        protected override object GetIdValue()
        {
            return dealer.Id;
        }

        #endregion

        #region Authorization Rules

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        public static bool CanAddObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        #endregion

        #region Factory Methods

        public static DealerLoadConfiguration GetDealerLoadConfiguration(Dealer dealer)
        {
            if (!CanGetObject())
                throw new SecurityException("Insufficient priviledges to load DealerLoadConfiguration");
            return DataPortal.Fetch<DealerLoadConfiguration>(new Criteria(dealer));
        }

        internal static DealerLoadConfiguration GetDealerLoadConfiguration(IDataReader reader)
        {
            return new DealerLoadConfiguration(reader);
        }

        private DealerLoadConfiguration()
        {
            /* Force use of factory methods */
        }

        private DealerLoadConfiguration(IDataRecord record)
        {
            Fetch(record);
        }

        #endregion

        #region Data Access

        [Serializable]
        class Criteria
        {
            private readonly Dealer dealer;

            public Criteria(Dealer dealer)
            {
                this.dealer = dealer;
            }

            public Dealer Dealer
            {
                get { return dealer; }
            }
        }


        protected override void DataPortal_Insert()
        {
            if (!IsDirty) return;

            DoInsertUpdate("Runtime.DealerLoadConfiguration#Insert");
        }

        protected override void DataPortal_Update()
        {
            if (!IsDirty) return;

            DoInsertUpdate("Runtime.DealerLoadConfiguration#Update");
        }

        private void DoInsertUpdate(string commandText)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = commandText;

                    command.AddParameterWithValue("DealerId", DbType.Int32, false, dealer.Id);
                    command.AddParameterWithValue("EnvironmentId", DbType.Int32, false, (int)dealer.Environment);
                    command.AddParameterWithValue("DailyInventoryLoadHour", DbType.Int32, false, dailyInventoryLoadHour);
                    command.AddParameterWithValue("DailyInventoryLoadMinute", DbType.Int32, false, dailyInventoryLoadMinute);
                    command.AddParameterWithValue("DailySalesLoadHour", DbType.Int32, false, dailySalesLoadHour);
                    command.AddParameterWithValue("DailySalesLoadMinute", DbType.Int32, false, dailySalesLoadMinute);
                    command.AddParameterWithValue("InventoryLoadPriority", DbType.Int32, false, inventoryLoadPriority);
                    command.AddParameterWithValue("SalesLoadPriority", DbType.Int32, false, salesLoadPriority);

                    command.ExecuteNonQuery();
                }
            }
        }

        private void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Runtime.DealerLoadConfiguration#Fetch";
                    command.AddParameterWithValue("DealerId", DbType.Int32, false, criteria.Dealer.Id);
                    command.AddParameterWithValue("EnvironmentId", DbType.Int32, false, (int)criteria.Dealer.Environment);
                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            this.dealer = criteria.Dealer;
                            Fetch(reader);
                        }
                    }
                }
            }
        }

        private void Fetch(IDataRecord record)
        {
            dailyInventoryLoadHour = record.GetInt32(record.GetOrdinal("DailyInventoryLoadHour"));
            dailyInventoryLoadMinute = record.GetInt32(record.GetOrdinal("DailyInventoryLoadMinute"));
            dailySalesLoadHour = record.GetInt32(record.GetOrdinal("DailySalesLoadHour"));
            dailySalesLoadMinute = record.GetInt32(record.GetOrdinal("DailySalesLoadMinute"));            
            inventoryLoadPriority = record.GetInt32(record.GetOrdinal("InventoryLoadPriority"));
            salesLoadPriority = record.GetInt32(record.GetOrdinal("SalesLoadPriority"));
        }

        #endregion
    }
}
