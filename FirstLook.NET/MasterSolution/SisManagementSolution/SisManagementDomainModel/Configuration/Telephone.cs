namespace FirstLook.Sis.Management.DomainModel.Configuration
{
    /// <summary>
    /// USA telephone numbers are of the format <code>AreaCode ExchangeCode StationCode</code>.
    /// </summary>
    public class Telephone
    {
        private int areaCode;
        private int exchangeCode;
        private int stationCode;

        public int AreaCode
        {
            get { return areaCode; }
            set { areaCode = value; }
        }

        public int ExchangeCode
        {
            get { return exchangeCode; }
            set { exchangeCode = value; }
        }

        public int StationCode
        {
            get { return stationCode; }
            set { stationCode = value; }
        }
    }
}