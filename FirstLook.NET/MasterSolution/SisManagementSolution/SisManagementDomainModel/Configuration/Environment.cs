using System;

namespace FirstLook.Sis.Management.DomainModel.Configuration
{
    [Serializable]
    public enum Environment
    {
        Undefined = 0,
        Production = 1,
        PreProduction = 2
    }
}
