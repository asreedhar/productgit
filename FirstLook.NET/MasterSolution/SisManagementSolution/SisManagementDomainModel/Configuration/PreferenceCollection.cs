using System.Data;
using System.Security;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration
{
    public class PreferenceCollection : ReadOnlyListBase<PreferenceCollection, Preference>
    {
        #region Authorization Methods

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Factory Methods

        public static PreferenceCollection GetPreferenceCollection()
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view Preferences");

            return DataPortal.Fetch<PreferenceCollection>();
        }

        #endregion

        #region Data Access

        private void DataPortal_Fetch()
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Configuration.PreferenceCollection#Fetch";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            IsReadOnly = false;
                            Add(Preference.GetPreference(reader));
                            IsReadOnly = true;
                        }
                    }
                }
            }

            RaiseListChangedEvents = true;
        }


        #endregion
    }
}