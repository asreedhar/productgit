using System;
using System.Data;
using System.Security;
using Csla;
using FirstLook.Common.Data;

using FirstLook.Sis.Management.DomainModel.Configuration.Adp;

namespace FirstLook.Sis.Management.DomainModel.Configuration
{
    [Serializable]
    public class QueryCollection : ReadOnlyListBase<QueryCollection, Query>
    {
        #region Authorization Methods

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Factory Methods
        
        internal static QueryCollection GetQueryCollection(IDataReader reader)
        {
            return new QueryCollection(reader);
        }

        private QueryCollection()
        {
            /* Force use orf factory methods */
        }

        private QueryCollection(IDataReader reader)
        {
            Fetch(reader);
        }


        #endregion

        #region Data Access

        private void Fetch(IDataReader reader)
        {
            RaiseListChangedEvents = false;
            IsReadOnly = false;
            while(reader.Read())
            {
                Add(Query.GetQuery(reader));
            }
            IsReadOnly = true;
            RaiseListChangedEvents = true;
        }

        #endregion
    }
}
