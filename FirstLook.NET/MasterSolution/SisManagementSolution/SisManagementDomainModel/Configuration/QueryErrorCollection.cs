using System;
using System.Data;
using System.Security;
using Csla;
using FirstLook.Common.Data;

using FirstLook.Sis.Management.DomainModel.Configuration.Adp;

namespace FirstLook.Sis.Management.DomainModel.Configuration
{
    [Serializable]
    public class QueryErrorCollection : ReadOnlyListBase<QueryErrorCollection, QueryError>
    {
        #region Factory Methods

        public static QueryErrorCollection GetQueryErrors(Guid queryId)
        {
            return DataPortal.Fetch<QueryErrorCollection>(new FetchCriteria(queryId));
        }

        #endregion

        #region Data Access

        [Serializable]
        class FetchCriteria
        {
            private readonly Guid queryId;

            public FetchCriteria(Guid queryId)
            {
                this.queryId = queryId;
            }

            public Guid QueryId
            {
                get { return queryId; }
            }

        }

        private void DataPortal_Fetch(FetchCriteria criteria)
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Runtime.QueryErrorCollection#Fetch";

                    command.AddParameterWithValue("QueryId", DbType.Guid, false, criteria.QueryId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        IsReadOnly = false;

                        while (reader.Read())
                        {
                            Add(QueryError.GetQueryError(reader));
                        }

                        IsReadOnly = true;
                    }
                }
            }

            RaiseListChangedEvents = true;
        }

        #endregion
    }
}
