using System;
using System.Data;
using System.Security;
using Csla;
using Csla.Validation;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    /// <summary>
    /// A mapping that indicates Journals to pull for a Connection
    /// </summary>
    [Serializable]
    public class DealerJournal : BusinessBase<DealerJournal>
    {
        #region Business Methods

        private Guid _journalId = Guid.Empty;
        private string _number;
        private string _description;
        private string _type;
        private bool _isPurchase = false;
        private bool _isTrade = false;
        private int _referenceTypeId = ReferenceTypeCollection.DefaultReferenceType();
        private int _assetClassId = AssetClassCollection.DefaultAssetClass();

        public Guid JournalId
        {
            get
            {
                CanReadProperty("JournalId", true);
                return _journalId;
            }
        }

        public string Number
        {
            get
            {
                CanReadProperty("Number", true);
                return _number;
            }
        }

        public string Description
        {
            get
            {
                CanReadProperty("Description", true);
                return _description;
            }
        }

        public string Type
        {
            get
            {
                CanReadProperty("Type", true);
                return _type;
            }
        }


        public bool IsPurchase
        {
            get
            {
                CanReadProperty("IsPurchase", true);
                return _isPurchase;
            }
            set
            {
                CanWriteProperty("IsPurchase", true);

                if (IsPurchase != value)
                {
                    _isPurchase = value;

                    ValidationRules.CheckRules("IsTrade");

                    PropertyHasChanged("IsPurchase");
                }
            }
        }

        public bool IsTrade
        {
            get
            {
                CanReadProperty("IsTrade", true);
                return _isTrade;
            }
            set
            {
                CanWriteProperty("IsTrade", true);

                if (IsTrade != value)
                {
                    _isTrade = value;

                    ValidationRules.CheckRules("IsPurchase");

                    PropertyHasChanged("IsTrade");
                }
            }
        }

        public int ReferenceTypeId
        {
            get
            {
                CanReadProperty("ReferenceTypeId", true);
                return _referenceTypeId;
            }
            set
            {
                CanWriteProperty("ReferenceTypeId", true);

                if (ReferenceTypeId != value)
                {
                    _referenceTypeId = value;

                    PropertyHasChanged("ReferenceTypeId");
                }
            }
        }

        public int AssetClassId
        {
            get
            {
                CanReadProperty("AssetClassId", true);
                return _assetClassId;
            }
            set
            {
                CanWriteProperty("AssetClassId", true);

                if (AssetClassId != value)
                {
                    _assetClassId = value;

                    PropertyHasChanged("AssetClassId");
                }
            }
        }

        protected override object GetIdValue()
        {
            return _journalId;
        }

        #endregion

        #region Authorization Rules

        private static readonly string[] PropertyNames = new string[]
            {
                "IsPurchase", "IsTrade", 
                "ReferenceTypeId", "AssetClassId",
            };

        protected override void AddAuthorizationRules()
        {
            foreach (string propertyName in PropertyNames)
            {
                AuthorizationRules.AllowWrite(propertyName, "DataManagementWriter");
            }
        }

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        public static bool CanAddObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        public static bool CanEditObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        public static bool CanDeleteObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        #endregion

        #region Validation Rules

        protected override void AddBusinessRules()
        {
            ValidationRules.AddRule(FinancialTransactionTypeRequired, "IsPurchase");
            ValidationRules.AddRule(FinancialTransactionTypeRequired, "IsTrade");
            ValidationRules.AddRule(AssetClassCollection.ValidAssetClassIdRequired, "AssetClassId");
            ValidationRules.AddRule(ReferenceTypeCollection.ValidReferenceTypeIdRequired, "ReferenceTypeId");
        }

        protected static bool FinancialTransactionTypeRequired(object target, RuleArgs e)
        {
            bool isValid = false;
            DealerJournal journal = target as DealerJournal;
            if (journal != null)
            {
                
                isValid = journal.IsPurchase || journal.IsTrade;
                if (!isValid)
                {
                    e.Description = string.Format("Journal #{0} must have at least one of IsPurchase or IsTrade selected.", journal.Number);
                }
            }
            return isValid;
        }

        #endregion

        #region Factory Methods

        internal static DealerJournal NewDealerJournal(Guid journalId)
        {
        	if (!CanGetObject())
                throw new SecurityException("User not authorized to view DealerJournal");
                
            return new DealerJournal(Journal.GetJournal(journalId));
        }

        internal static DealerJournal GetDealerJournal(IDataRecord record)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view DealerJournal");
                
            return new DealerJournal(record);
        }

        private DealerJournal()
        {
            MarkAsChild();
        }

        /// <summary>
        /// Called when a new object is created.
        /// </summary>
        /// <param name="journal"></param>
        private DealerJournal(Journal journal)
        {
            MarkAsChild();
            _journalId = journal.Id;
            _number = journal.Number;
            _description = journal.Description;
            _type = journal.Type;

            ValidationRules.CheckRules();
        }

        /// <summary>
        /// Called when loading data from database.
        /// </summary>
        /// <param name="record"></param>
        private DealerJournal(IDataRecord record)
        {
            MarkAsChild();
            Fetch(record);
            MarkOld();
            ValidationRules.CheckRules();
        }

        public override DealerJournal Save()
        {
            if (IsNew && !CanAddObject())
                throw new SecurityException("User not authorized to add DealerJournal");

            if (IsDeleted && !CanDeleteObject())
                throw new SecurityException("User not authorized to delete DealerJournal");

            if (!CanEditObject())
                throw new SecurityException("User not authorized to edit DealerJournal");

            return base.Save();
        }

        #endregion

        #region Data Access

        private void Fetch(IDataRecord record)
        {
            _journalId = record.GetGuid(record.GetOrdinal("JournalId"));
            _number = record.GetString(record.GetOrdinal("Number"));
            _description = record.GetString(record.GetOrdinal("Description"));
            _type = record.GetString(record.GetOrdinal("Type"));
            _isPurchase = record.GetBoolean(record.GetOrdinal("IsPurchase"));
            _isTrade = record.GetBoolean(record.GetOrdinal("IsTrade"));
            if (!record.IsDBNull(record.GetOrdinal("ReferenceTypeId")))
            {
                _referenceTypeId = record.GetInt32(record.GetOrdinal("ReferenceTypeId"));
            }
            if (!record.IsDBNull(record.GetOrdinal("AssetClassId")))
            {
                _assetClassId = record.GetInt32(record.GetOrdinal("AssetClassId"));
            }
        }

        internal void Insert(IDataConnection connection, IDbTransaction transaction, int dealerId, Environment environment)
        {
            if (!IsDirty) return;

            DoInsertUpdate(connection, transaction, "Configuration.DealerJournal#AddAssignment", dealerId, environment);

            MarkOld();
        }

        internal void Update(IDataConnection connection, IDbTransaction transaction, int dealerId, Environment environment)
        {
            if (!IsDirty) return;

            if (IsNew) return;

            DoInsertUpdate(connection, transaction, "Configuration.DealerJournal#Update", dealerId, environment);

            MarkOld();
        }

        internal void DeleteSelf(IDataConnection connection, IDbTransaction transaction, int dealerId, Environment environment)
        {
            if (!IsDirty) return;

            if (IsNew) return;

            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = "Configuration.DealerJournal#DeleteAssignment";
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;

                command.AddParameterWithValue("JournalId", DbType.Guid, false, _journalId);
                command.AddParameterWithValue("DealerId", DbType.Int32, false, dealerId);
                command.AddParameterWithValue("EnvironmentId", DbType.Int32, false, (int)environment);

                command.ExecuteNonQuery();
            }


            MarkNew();
        }

        private void DoInsertUpdate(IDataConnection connection, IDbTransaction transaction, string commandText, int dealerId, Environment environment)
        {
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = commandText;
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;

                command.AddParameterWithValue("JournalId", DbType.Guid, false, _journalId);
                command.AddParameterWithValue("DealerId", DbType.Int32, false, dealerId);
                command.AddParameterWithValue("EnvironmentId", DbType.Int32, false, (int)environment);
                command.AddParameterWithValue("IsPurchase", DbType.Boolean, false, _isPurchase);
                command.AddParameterWithValue("IsTrade", DbType.Boolean, false, _isTrade);
                command.AddParameterWithValue("ReferenceTypeId", DbType.Int32, false, _referenceTypeId);
                command.AddParameterWithValue("AssetClassId", DbType.Int32, false, _assetClassId);

                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}
