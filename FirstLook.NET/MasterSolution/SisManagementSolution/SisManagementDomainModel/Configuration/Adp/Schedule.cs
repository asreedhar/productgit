using System;
using System.Collections.Generic;
using System.Data;
using System.Security;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class Schedule : ReadOnlyBase<Schedule>
    {
        #region Business Methods

        private Guid id;
        private string number;
        private string description;
        private string type;
        private string controlType;
        private string control2Type;


        public Guid Id
        {
            get
            {
                CanReadProperty("ScheduleId", true);
                return id;
            }
        }
        public string Number
        {
            get
            {
                CanReadProperty("Number", true);
                return number;
            }
        }
        public string Description
        {
            get
            {
                CanReadProperty("Description", true);
                return description;
            }
        }
        public string Type
        {
            get
            {
                CanReadProperty("Type", true);
                return type;
            }
        }
        public string ControlType
        {
            get
            {
                CanReadProperty("ControlType", true);
                return controlType;
            }
        }
        public string Control2Type
        {
            get
            {
                CanReadProperty("Control2Type", true);
                return control2Type;
            }
        }

        protected override object GetIdValue()
        {
            return id;
        }

        #endregion

        #region Authorization Rules

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Factory Methods

        public static Schedule GetSchedule(Guid id)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view schedule");

            return DataPortal.Fetch<Schedule>(new Criteria(id));
        }
        
        internal static Schedule GetSchedule(IDataRecord record)
        {
            return new Schedule(record);
        }

        private Schedule()
        {
            /* force use of factory methods */
        }

        private Schedule(IDataRecord record)
        {
            Fetch(record);
        }

        #endregion

        #region Data Access

        [Serializable]
        class Criteria
        {
            private readonly Guid id;

            public Criteria(Guid id)
            {
                this.id = id;
            }

            public Guid Id
            {
                get { return id; }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "criteria")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Configuration.Schedule#Fetch";

                    command.AddParameterWithValue("Id", DbType.Guid, false, criteria.Id);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Fetch(reader);
                        }
                        else
                        {
                            throw new DataException("Schedule not found");
                        }
                    }
                }
            }
        }


        private void Fetch(IDataRecord record)
        {
            id = record.GetGuid(record.GetOrdinal("Id"));
            number = record.GetString(record.GetOrdinal("Number"));
            description = record.GetString(record.GetOrdinal("Description"));
            type = record.GetString(record.GetOrdinal("Type"));
            controlType = record.GetString(record.GetOrdinal("ControlType"));
            control2Type = record.GetString(record.GetOrdinal("Control2Type"));
        }

        #endregion
    }
}
