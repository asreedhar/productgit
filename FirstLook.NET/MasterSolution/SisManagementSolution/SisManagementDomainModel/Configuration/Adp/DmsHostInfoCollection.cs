using System;
using System.Data;
using Csla;
using Csla.Core;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class DmsHostInfoCollection : ReadOnlyListBase<DmsHostInfoCollection, DmsHostInfo>, IReportTotalRowCount
    {
        #region IReportTotalRowCount Members

        private int totalRowCount;

        public int TotalRowCount
        {
            get
            {
                return totalRowCount;
            }
        }

        #endregion

        public static DmsHostInfoCollection GetDmsHosts(int dealerId, string name, IPAddress ipAddress, TelephoneNumber telephoneNumber, int timeZoneId, string sortOrder, int maximumRows, int startRowIndex)
        {
            if (maximumRows < 1)
                throw new ArgumentOutOfRangeException("maximumRows", maximumRows, "Must be greater than zero");

            if (startRowIndex < 0)
                throw new ArgumentOutOfRangeException("startRowIndex", startRowIndex, "Must be greater than zero");

            if (string.IsNullOrEmpty(name))
                name = string.Empty;

            if (string.IsNullOrEmpty(sortOrder))
                sortOrder = "Name";

            return DataPortal.Fetch<DmsHostInfoCollection>(new FetchCriteria(dealerId, ipAddress, name, telephoneNumber, timeZoneId, new DataSourceSelectCriteria(sortOrder, maximumRows, startRowIndex)));
        }


        private DmsHostInfoCollection()
        {
        }

        [Serializable]
        class FetchCriteria
        {
            private readonly int dealerId;
            private readonly IPAddress ipAddress;
            private readonly string name;
            private readonly TelephoneNumber telephoneNumber;
            private readonly int timeZoneId;
            private readonly DataSourceSelectCriteria criteria;

            public FetchCriteria(int dealerId, IPAddress ipAddress, string name, TelephoneNumber telephoneNumber, int timeZoneId, DataSourceSelectCriteria criteria)
            {
                this.dealerId = dealerId;
                this.ipAddress = ipAddress;
                this.name = name;
                this.telephoneNumber = telephoneNumber;
                this.timeZoneId = timeZoneId;
                this.criteria = criteria;
            }

            public int DealerId
            {
                get { return dealerId; }
            }

            public IPAddress IPAddress
            {
                get { return ipAddress; }
            }

            public string Name
            {
                get { return name; }
            }

            public TelephoneNumber TelephoneNumber
            {
                get { return telephoneNumber; }
            }

            public int TimeZoneId
            {
                get { return timeZoneId; }
            }

            public DataSourceSelectCriteria Criteria
            {
                get { return criteria; }
            }
        }

        private void DataPortal_Fetch(FetchCriteria criteria)
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Configuration.DmsHostInfoCollection#Fetch";

                    command.AddParameterWithValue("DealerId", DbType.Int32, true, criteria.DealerId);
                    command.AddParameterWithValue("Name", DbType.String, true, criteria.Name);
                    command.AddParameterWithValue("TimeZoneId", DbType.Int32, true, criteria.TimeZoneId);

                    IPAddress.AddToCommand(criteria.IPAddress, command, "IPAddress");
                    TelephoneNumber.AddToCommand(criteria.TelephoneNumber, command, "TelephoneNumber");

                    criteria.Criteria.AddToCommand(command);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            totalRowCount = reader.GetInt32(reader.GetOrdinal("TotalRowCount"));

                            if (reader.NextResult())
                            {
                                IsReadOnly = false;

                                while (reader.Read())
                                {
                                    Add(new DmsHostInfo(reader));
                                }

                                IsReadOnly = true;
                            }
                            else
                            {
                                throw new DataException("DataReader missing second of two result sets");
                            }
                        }
                        else
                        {
                            throw new DataException("DataReader missing first of two result sets");
                        }
                    }
                }
            }

            RaiseListChangedEvents = true;
        }
    }
}
