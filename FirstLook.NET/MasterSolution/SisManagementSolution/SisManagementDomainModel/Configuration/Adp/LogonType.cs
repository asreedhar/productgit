using System;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public enum LogonType
    {
        Undefined,
        Accounting,
        Finance
    }
}
