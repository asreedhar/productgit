using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class LotLogonDataSplit : BusinessBase<LotLogonDataSplit>
    {
        #region Business Methods

        private string lotNumber;

        public string LotNumber
        {
            get
            {
                CanReadProperty("LotNumber", true);
                return lotNumber;
            }
            set
            {
                CanWriteProperty("LotNumber", true);
                lotNumber = value;
                PropertyHasChanged("LotNumber");
            }
        }

        protected override object GetIdValue()
        {
            return null;
        }

        #endregion

        #region Factory Methods

        internal static LotLogonDataSplit GetLotLogonDataSplit(IDataRecord record)
        {
            return new LotLogonDataSplit(record);
        }

        public static LotLogonDataSplit NewLotLogonDataSplit()
        {
            return DataPortal.Create<LotLogonDataSplit>();
        }

        private LotLogonDataSplit()
        {
            MarkAsChild();
        }

        private LotLogonDataSplit(IDataRecord record) : this()
        {
            Fetch(record);
            MarkOld();
        }


        #endregion

        #region Authorization

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Data Access

        protected override void DataPortal_Create()
        {
            
        }

        protected void Fetch(IDataRecord record)
        {
            lotNumber = record.GetString(record.GetOrdinal("LotNumber"));
        }

        #endregion
    }
}