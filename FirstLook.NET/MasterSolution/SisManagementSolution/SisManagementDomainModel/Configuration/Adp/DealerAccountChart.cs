using System;
using System.ComponentModel;
using System.Data;
using System.Security;
using Csla;
using Csla.Validation;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    /// <summary>
    /// Represents the Chart of Accounts for a Dealer.
    /// 
    /// The Chart of Accounts is a list of all Accounts, Journals, and Schedules.
    /// </summary>
    [Serializable]
    public class DealerAccountChart : BusinessBase<DealerAccountChart>
    {
        #region Business Methods

        private int _dealerId;
        private Environment _environment;
        private Guid _accountingLogonId;

        private DealerAccountCollection _inventoryAccounts;
        private DealerAccountCollection _salesAccounts;
        private DealerAccountCollection _costAccounts;
        private DealerJournalCollection _journals;
        //No schedules needed.

        public DealerAccountCollection InventoryAccounts
        {
            get
            {
                CanReadProperty("InventoryAccounts", true);

                return _inventoryAccounts;
            }
        }

        public DealerAccountCollection SalesAccounts
        {
            get
            {
                CanReadProperty("SalesAccounts", true);

                return _salesAccounts;
            }
        }
        
        public DealerAccountCollection CostAccounts
        {
            get
            {
                CanReadProperty("CostAccounts", true);

                return _costAccounts;
            }
        }

        public DealerJournalCollection Journals
        {
            get
            {
                CanReadProperty("Journals", true);

                return _journals;
            }
        }

        public override bool IsDirty
        {
            get
            {
                bool dirty =
                    (base.IsDirty
                        || _inventoryAccounts.IsDirty
                        || _salesAccounts.IsDirty
                        || _costAccounts.IsDirty
                        || _journals.IsDirty);

                return dirty;
            }
        }

        public override bool IsValid
        {
            get
            {
                ValidationRules.CheckRules();

                return (base.IsValid 
                        && _inventoryAccounts.IsValid
                        && _salesAccounts.IsValid
                        && _costAccounts.IsValid
                        && _journals.IsValid);
            }
        }

        public override DealerAccountChart Save()
        {
            if(!CanEditObject())
                throw new SecurityException("User not authorized to edit DealerAccountChart");

            return base.Save();
        }

        /// <summary>
        /// This object doesn't have an ID!
        /// </summary>
        /// <returns></returns>
        protected override object GetIdValue()
        {
            return null;
        }

        #endregion

        #region Authorization Methods

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        public static bool CanEditObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        #endregion

        #region Validation Rules

        protected override void AddBusinessRules()
        {
            ValidationRules.AddRule(DealerAccountChartAtLeastOneEntry, "Count");
        }

        protected static bool DealerAccountChartAtLeastOneEntry(object target, RuleArgs e)
        {
            bool isValid = false;
            DealerAccountChart chartOfAccounts = target as DealerAccountChart;
            if (chartOfAccounts != null)
            {
                isValid = chartOfAccounts.InventoryAccounts.Count > 0
                        && chartOfAccounts.SalesAccounts.Count > 0
                        && chartOfAccounts.CostAccounts.Count > 0
                        && chartOfAccounts.Journals.Count > 0;

                if (!isValid)
                {
                    e.Description = "Dealer must have at least 1 Inventory Account, 1 Sales Account, 1 Cost Account and 1 Journal included.";
                }
            }
            return isValid;
        }

        #endregion

        #region Factory Methods

        public static DealerAccountChart GetDealerAccountChart(int dealerId, Environment environment, Guid accountingLogonId)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to retrieve DealerAccountChart");

            return DataPortal.Fetch<DealerAccountChart>(new FetchCriteria(dealerId, environment, accountingLogonId));
        }

        private DealerAccountChart()
        { 
            //This object is never saved to the database as it is a transaction object.
            MarkOld();
        }

        #endregion

        #region Data Access

        [Serializable]
        private class FetchCriteria
        {
            private readonly int dealerId;
            private readonly Environment environment;
            private readonly Guid accountingLogonId;

            public FetchCriteria(int dealerId, Environment environment, Guid accountingLogonId)
            {
                this.dealerId = dealerId;
                this.environment = environment;
                this.accountingLogonId = accountingLogonId;
            }

            internal Guid AccountingLogonId
            {
                get { return accountingLogonId; }
            }

            internal int DealerId
            {
                get { return dealerId; }
            }

            internal Environment Environment
            {
                get { return environment; }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called by CSLA using reflection")]
        private void DataPortal_Fetch(FetchCriteria criteria)
        {
            _dealerId = criteria.DealerId;
            _environment = criteria.Environment;
            _accountingLogonId = criteria.AccountingLogonId;

            _inventoryAccounts = DealerAccountCollection.GetDealerAccounts(_dealerId, _environment, _accountingLogonId, AccountType.Asset);
            _salesAccounts = DealerAccountCollection.GetDealerAccounts(_dealerId, _environment, _accountingLogonId, AccountType.Sales);
            
            _costAccounts = DealerAccountCollection.GetDealerAccounts(_dealerId, _environment, _accountingLogonId, AccountType.Cost);
            //adding Income accounts to cost account
            foreach(DealerAccount a in DealerAccountCollection.GetDealerAccounts(_dealerId, _environment, _accountingLogonId, AccountType.Income))
            {
                _costAccounts.Add(a);
            }


            _journals = DealerJournalCollection.GetDealerJournals(_dealerId, _environment, _accountingLogonId);

            ValidationRules.CheckRules();
        }

        [Transactional(TransactionalTypes.Manual)]
        protected override void DataPortal_Update()
        {
            if (!IsDirty) return;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        if (_inventoryAccounts.IsDirty)
                        {
                            _inventoryAccounts.Update(connection, transaction, _dealerId, _environment);
                        }

                        if (_salesAccounts.IsDirty)
                        {
                            _salesAccounts.Update(connection, transaction, _dealerId, _environment);
                        }

                        if (_costAccounts.IsDirty)
                        {
                            _costAccounts.Update(connection, transaction, _dealerId, _environment);
                        }

                        if (_journals.IsDirty)
                        {
                            _journals.Update(connection, transaction, _dealerId, _environment);
                        }
                        transaction.Commit();
                    }
                    catch
                    {
                        try
                        {
                            transaction.Rollback();
                        }
                        catch
                        {
                            throw;
                        }

                        throw;
                    }
                }
            }
        }

        #endregion

    }
}
