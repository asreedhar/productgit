namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    /// <summary>
    /// Enumerates the next work-step in the SIS / ADP configuration workflow.
    /// </summary>
    public enum ConfigurationWorkStep
    {
        /// <summary>
        /// The next work-step is to configure the dealer hours (etc).
        /// </summary>
        DealerConfiguration,

        /// <summary>
        /// The next work-step is to configure the DMS host.
        /// </summary>
        DmsHostConfiguration,

        /// <summary>
        /// The next work-step is to configure the SIS connection to the DMS host.
        /// </summary>
        SisConnectionConfiguration,

        /// <summary>
        /// The next work-step is to configure the SIS connection to the DMS host.
        /// </summary>
        SisConnectionTest,

        /////// <summary>
        ///// The next work-step is to say whether the data in the logon is shared with
        ///// another dealer (i.e. data split).
        ///// </summary>
        //LogonDataSharingPrompt,

        /// <summary>
        /// The next work-step is to configure logon data-sharing.
        /// </summary>
        LogonDataSharingConfiguration,

        /// <summary>
        /// The next work-step is to configure inventory/sales/cost accounts, journals and schedules.
        /// </summary>
        LogonDataExtractionConfiguration,

        /// <summary>
        /// All work-steps have been completed.
        /// </summary>
        Complete
    }
}
