using System;
using System.Data;
using Csla;
using Csla.Validation;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class ContactInformation : BusinessBase<ContactInformation>
    {
        #region Business Methods

        private byte[] version = new byte[8];

        private TelephoneNumber cellTelephoneNumber;
        private string emailAddress;
        private Guid id;
        private TelephoneNumber landTelephoneNumber;
        private string name;

        public Guid Id
        {
            get
            {
                CanReadProperty("Id", true);

                return id;
            }
        }

        public string Name
        {
            get
            {
                CanReadProperty("Name", true);

                return name;
            }
            set
            {
                CanWriteProperty("Name", true);

                if (!Equals(Name, value))
                {
                    name = value;

                    PropertyHasChanged("Name");
                }
            }
        }

        public TelephoneNumber LandTelephoneNumber
        {
            get
            {
                CanReadProperty("LandTelephoneNumber", true);

                return landTelephoneNumber;
            }
            set
            {
                CanWriteProperty("LandTelephoneNumber", true);

                if (!Equals(LandTelephoneNumber, value))
                {
                    landTelephoneNumber = value;

                    PropertyHasChanged("LandTelephoneNumber");
                }
            }
        }

        public TelephoneNumber CellTelephoneNumber
        {
            get
            {
                CanReadProperty("CellTelephoneNumber", true);

                return cellTelephoneNumber;
            }
            set
            {
                CanWriteProperty("CellTelephoneNumber", true);

                if (!Equals(CellTelephoneNumber, value))
                {
                    cellTelephoneNumber = value;

                    PropertyHasChanged("CellTelephoneNumber");
                }
            }
        }

        public string EmailAddress
        {
            get
            {
                CanReadProperty("EmailAddress", true);

                return emailAddress;
            }
            set
            {
                CanWriteProperty("EmailAddress", true);

                if (!Equals(EmailAddress, value))
                {
                    emailAddress = value;

                    PropertyHasChanged("EmailAddress");
                }
            }
        }

        protected override object GetIdValue()
        {
            return id;
        }

        #endregion

        #region Authorization Rules

        private static readonly string[] PropertyNames = new string[]
            {
                "Name", "EmailAddress", "LandTelephoneNumber", "CellTelephoneNumber"
            };

        protected override void AddAuthorizationRules()
        {
            foreach (string propertyName in PropertyNames)
            {
                AuthorizationRules.AllowWrite(propertyName, "DataManagementWriter");
            }
        }

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        public static bool CanAddObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        public static bool CanEditObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        #endregion

        #region Validation Rules

        protected override void AddBusinessRules()
        {
            ValidationRules.AddRule(CommonRules.StringRequired, "Name");

            ValidationRules.AddRule(CommonRules.StringMaxLength,
                new CommonRules.MaxLengthRuleArgs("Name", 255));

            ValidationRules.AddRule(TelephoneNumber.TelephoneNumberIsValid,
                "LandTelephoneNumber");

            ValidationRules.AddRule(TelephoneNumber.TelephoneNumberIsValid,
                "CellTelephoneNumber");

            ValidationRules.AddRule(CommonRules.StringRequired, "EmailAddress");

            ValidationRules.AddRule(CommonRules.StringMaxLength,
                new CommonRules.MaxLengthRuleArgs("EmailAddress", 255));

            ValidationRules.AddRule(CommonRules.RegExMatch,
                new CommonRules.RegExRuleArgs("EmailAddress", CommonRules.RegExPatterns.Email));
        }

        #endregion

        #region Factory Methods

        public static ContactInformation NewContactInformation()
        {
            return DataPortal.Create<ContactInformation>();
        }

        private ContactInformation()
        {
            MarkAsChild();
        }

        internal ContactInformation(IDataRecord record) : this()
        {
            Fetch(record);
            ValidationRules.CheckRules(); //make sure we're in a good state on db load.
        }

        #endregion

        #region Data Access

        protected override void DataPortal_Create()
        {
            id = Guid.NewGuid();
        }

        private void Fetch(IDataRecord record)
        {
            id = record.GetGuid(record.GetOrdinal("Id"));
            name = record.GetString(record.GetOrdinal("Name"));
            emailAddress = record.GetString(record.GetOrdinal("EmailAddress"));
            cellTelephoneNumber = TelephoneNumber.GetTelephoneNumber(record, "CellTelephoneNumber");
            landTelephoneNumber = TelephoneNumber.GetTelephoneNumber(record, "LandTelephoneNumber");
            
            if (!record.IsDBNull(record.GetOrdinal("Version")))
            {
                record.GetBytes(record.GetOrdinal("Version"), 0, version, 0, 8);
            }

            MarkOld();
        }

        internal void Insert(IDataConnection connection, IDbTransaction transaction, DmsHost dmsHost)
        {
            if (!IsDirty) return;

            DoInsertUpdate(connection, transaction, dmsHost, "Configuration.ContactInformation#Insert", false);
        }

        internal void Update(IDataConnection connection, IDbTransaction transaction, DmsHost dmsHost)
        {
            if (!IsDirty) return;

            DoInsertUpdate(connection, transaction, dmsHost, "Configuration.ContactInformation#Update", true);
        }

        private void DoInsertUpdate(IDataConnection connection, IDbTransaction transaction, DmsHost dmsHost, string commandText, bool isUpdate)
        {
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                IDataParameter newVersion = Database.AddArrayOutParameter(command, "NewVersion", DbType.Binary, false, 8);

                command.CommandText = commandText;
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;

                command.AddParameterWithValue("DmsHostId", DbType.Guid, false, dmsHost.Id);
                command.AddParameterWithValue("Id", DbType.Guid, false, id);
                command.AddParameterWithValue("Name", DbType.String, true, name);
                command.AddParameterWithValue("EmailAddress", DbType.String, true, emailAddress);

                if (isUpdate)
                {
                    Database.AddArrayParameter(command, "Version", DbType.Binary, false, version);
                }
                
                TelephoneNumber.AddToCommand(landTelephoneNumber, command, "LandTelephoneNumber");
                TelephoneNumber.AddToCommand(cellTelephoneNumber, command, "CellTelephoneNumber");

                command.ExecuteNonQuery();

                version = (byte[]) newVersion.Value;
            }

            MarkOld();
        }

        #endregion
    }
}