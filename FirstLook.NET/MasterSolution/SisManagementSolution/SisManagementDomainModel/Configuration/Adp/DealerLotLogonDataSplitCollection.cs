using System;
using System.Data;
using System.Security;
using Csla;
using Csla.Validation;
using FirstLook.Common.Data;
using FirstLook.Sis.Management.DomainModel.Configuration.Adp;
using TimeZone=System.TimeZone;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class DealerLotLogonDataSplitCollection : BusinessListBase<DealerLotLogonDataSplitCollection, DealerLotLogonDataSplit>
    {
        #region Business Methods

        public void Assign(DealerLotLogonDataSplit dealerLotLogonDataSplit)
        {
            if (!Contains(dealerLotLogonDataSplit.LotLogonDataSplit.LotNumber))
            {
                Add(dealerLotLogonDataSplit);
            }
        }

        public void Remove(string LotNumber)
        {
            foreach (DealerLotLogonDataSplit item in this)
            {
                if (item.LotLogonDataSplit.LotNumber == LotNumber)
                {
                    Remove(item);
                    break;
                }
            }
        }

        public bool Contains(string LotNumber)
        {
            foreach (DealerLotLogonDataSplit item in this)
            {
                if (item.LotLogonDataSplit.LotNumber == LotNumber)
                {
                    return true;
                }
            }
            return false;
        }

        #endregion

        #region Factory Methods

        internal static DealerLotLogonDataSplitCollection NewDealerLotLogonDataSplitCollection()
        {
            return new DealerLotLogonDataSplitCollection();
        }

        internal static DealerLotLogonDataSplitCollection GetDealerLotLogonDataSplitCollection(IDataReader dr)
        {
            return new DealerLotLogonDataSplitCollection(dr);
        }

        private DealerLotLogonDataSplitCollection()
        {

        }

        private DealerLotLogonDataSplitCollection(IDataReader dr)
        {
            Fetch(dr);
        }

        #endregion

        #region Data Access

        // called to load data from the database
        private void Fetch(IDataReader reader)
        {
            RaiseListChangedEvents = false;
            while (reader.Read())// && !reader.IsDBNull(reader.GetOrdinal("SisConnectionId")))
            {
                Add(DealerLotLogonDataSplit.GetDealerLotLogonDataSplit(reader));
            }
            RaiseListChangedEvents = true;
        }

        internal void Update(IDataConnection connection, IDbTransaction transaction)
        {
            RaiseListChangedEvents = false;

            // add/update any current child objects
            foreach (DealerLotLogonDataSplit obj in this)
            {
                if (obj.IsDeleted)
                {
                    obj.DeleteSelf(connection, transaction);
                }
                else if (obj.IsNew)
                {
                    obj.Insert(connection, transaction);
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion
    }
}