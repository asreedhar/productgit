using System;
using System.Collections.Generic;
using System.Data;
using System.Security;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    public class ScheduleInfo : ReadOnlyBase<ScheduleInfo>
    {
        #region Business Methods

        private Guid id;
        private int number;
        private string description;
        private string type;
        private string controlType;
        private string control2Type;
        private bool isDealerSchedule;


        public Guid Id
        {
            get
            {
                CanReadProperty("Id", true);
                return id;
            }
        }
        public int Number
        {
            get
            {
                CanReadProperty("Number", true);
                return number;
            }
        }
        public string Description
        {
            get
            {
                CanReadProperty("Description", true);
                return description;
            }
        }
        public string Type
        {
            get
            {
                CanReadProperty("Type", true);
                return type;
            }
        }
        public string ControlType
        {
            get
            {
                CanReadProperty("ControlType", true);
                return controlType;
            }
        }
        public string Control2Type
        {
            get
            {
                CanReadProperty("Control2Type", true);
                return control2Type;
            }
        }
        public bool IsDealerSchedule
        {
            get
            {
                CanReadProperty("IsDealerSchedule", true);
                return isDealerSchedule;
            }
        }
        protected override object GetIdValue()
        {
            return id;
        }

        #endregion

        #region Authorization Rules

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Factory Methods

        public static ScheduleInfo GetScheduleInfo(Guid id)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view DMS host");

            return DataPortal.Fetch<ScheduleInfo>(new Criteria(id));
        }

        internal static ScheduleInfo GetScheduleInfo(IDataRecord record)
        {
            return new ScheduleInfo(record);
        }

        private ScheduleInfo()
        {
            /* force use of factory methods */
        }

        private ScheduleInfo(IDataRecord record)
        {
            Fetch(record);
        }

        #endregion

        #region Data Access

        [Serializable]
        class Criteria
        {
            private readonly Guid id;

            public Criteria(Guid id)
            {
                this.id = id;
            }

            public Guid Id
            {
                get { return id; }
            }
        }

        private void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Configuration.ScheduleInfo#Fetch";

                    command.AddParameterWithValue("Id", DbType.Guid, false, criteria.Id);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        Fetch(reader);
                    }
                }
            }
        }


        private void Fetch(IDataRecord record)
        {
            id = record.GetGuid(record.GetOrdinal("Id"));
            number = record.GetInt32(record.GetOrdinal("Number"));
            description = record.GetString(record.GetOrdinal("Description"));
            type = record.GetString(record.GetOrdinal("Type"));
            controlType = record.GetString(record.GetOrdinal("ControlType"));
            control2Type = record.GetString(record.GetOrdinal("Control2Type"));
            isDealerSchedule = record.GetBoolean(record.GetOrdinal("IsDealerSchedule"));
        }

        #endregion
    }
}
