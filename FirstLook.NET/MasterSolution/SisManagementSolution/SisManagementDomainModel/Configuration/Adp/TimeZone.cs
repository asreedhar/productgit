using System;
using System.Data;
using Csla;
using Csla.Validation;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class TimeZone : ReadOnlyBase<TimeZone>
    {
        private readonly int id;
        private readonly string name;

        public int Id
        {
            get { return id; }
        }

        public string Name
        {
            get { return name; }
        }

        protected override object GetIdValue()
        {
            return id;
        }

        private TimeZone()
        {
            /* force factory methods */
        }

        public TimeZone(int id, string name)
        {
            this.id = id;
            this.name = name;
        }

        internal TimeZone(IDataRecord record)
        {
            id = record.GetInt32(record.GetOrdinal("Id"));
            name = record.GetString(record.GetOrdinal("Name"));
        }

        public static bool TimeZoneRequired(object target, RuleArgs e)
        {
            TimeZone value = (TimeZone)Utilities.CallByName(target, e.PropertyName, CallType.Get);

            if (value == null)
            {
                e.Description = string.Format("{0} required", e.PropertyName);

                return false;
            }

            return true;
        }

        internal static TimeZone GetTimeZone(IDataRecord record, string prefix)
        {
            int ordinal = record.GetOrdinal(prefix + "Id");

            if (record.IsDBNull(ordinal))
                return null;

            int id = record.GetInt32(ordinal);

            ordinal = record.GetOrdinal(prefix + "Name");

            if (record.IsDBNull(ordinal))
                return null;

            string name = record.GetString(ordinal);

            return new TimeZone(id, name);
        }

        internal static void AddToCommand(TimeZone timeZone, IDataCommand command, string prefix)
        {
            if (timeZone != null)
            {
                command.AddParameterWithValue(prefix + "Id", DbType.Int32, false, timeZone.id);
            }
            else
            {
                command.AddParameterWithValue(prefix + "Id", DbType.Int32, true, DBNull.Value);
            }
        }
    }
}
