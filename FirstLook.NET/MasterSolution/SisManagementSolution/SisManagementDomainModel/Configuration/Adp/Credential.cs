using System;
using System.Data;
using System.Security;
using Csla;
using Csla.Validation;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class Credential : BusinessBase<Credential>
    {
        #region Business Methods

        private byte[] version = new byte[8];

        private Guid id;

        public Guid Id
        {
            get
            {
                CanReadProperty("Id", true);

                return id;
            }
            set
            {
                CanWriteProperty("Id", true);

                if (!Equals(Id, value))
                {
                    id = value;

                    PropertyHasChanged("Id");
                }
            }
        }

        private string userName;

        public string UserName
        {
            get
            {
                CanReadProperty("UserName", true);

                return userName;
            }
            set
            {
                CanWriteProperty("UserName", true);

                if (!Equals(UserName, value))
                {
                    userName = value;

                    PropertyHasChanged("UserName");
                }
            }
        }

        private string password;

        public string Password
        {
            get
            {
                CanReadProperty("Password", true);

                return password;
            }
            set
            {
                CanWriteProperty("Password", true);

                if (!Equals(Password, value))
                {
                    password = value;

                    PropertyHasChanged("Password");
                }
            }
        }

        protected override object GetIdValue()
        {
            return id;
        }

        #endregion

        #region Authorization Rules

        protected override void AddAuthorizationRules()
        {
            AuthorizationRules.AllowRead("Password", "DataManagementWriter");

            AuthorizationRules.AllowWrite("UserName", "DataManagementWriter");
            AuthorizationRules.AllowWrite("Password", "DataManagementWriter");
        }

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        public static bool CanAddObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        public static bool CanEditObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        #endregion

        #region Validation Rules

        protected override void AddBusinessRules()
        {
            ValidationRules.AddRule(CommonRules.StringRequired, "UserName");

            ValidationRules.AddRule(CommonRules.StringMaxLength,
                new CommonRules.MaxLengthRuleArgs("UserName", 255));

            ValidationRules.AddRule(CommonRules.StringRequired, "Password");

            ValidationRules.AddRule(CommonRules.StringMaxLength,
                new CommonRules.MaxLengthRuleArgs("Password", 255));
        }

        public static bool CredentialRequired(object target, RuleArgs e)
        {
            if (target == null)
            {
                e.Description = string.Format("{0} required", e.PropertyName);

                return false;
            }

            return true;
        }

        #endregion

        #region Factory Methods

        public static Credential NewCredential()
        {
            if (!CanAddObject())
                throw new SecurityException("User not authorized to add a credential");

            return DataPortal.Create<Credential>();
        }

        internal static Credential GetCredential(IDataRecord record, string prefix)
        {
            return new Credential(record, prefix);
        }

        private Credential()
        {
            MarkAsChild();
        }

        private Credential(IDataRecord record, string prefix) : this()
        {
            Fetch(record, prefix);
        }

        #endregion

        #region Data Access

        [RunLocal]
        protected override void DataPortal_Create()
        {
            id = Guid.NewGuid();

            ValidationRules.CheckRules();
        }

        private void Fetch(IDataRecord record, string prefix)
        {
            id = record.GetGuid(record.GetOrdinal(prefix + "Id"));
            userName = record.GetString(record.GetOrdinal(prefix + "UserName"));
            password = record.GetString(record.GetOrdinal(prefix + "Password"));

            if (!record.IsDBNull(record.GetOrdinal(prefix + "Version")))
            {
                record.GetBytes(record.GetOrdinal(prefix + "Version"), 0, version, 0, 8);
            }

            MarkOld();
        }

        internal void Insert(IDataConnection connection, IDbTransaction transaction)
        {
            if (!IsDirty) return;

            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = "Configuration.Credential#Insert";
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;
                DoInsertUpdate(command, false);
            }

            MarkOld();
        }

        internal void Update(IDataConnection connection, IDbTransaction transaction)
        {
            if (!IsDirty) return;

            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = "Configuration.Credential#Update";
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;
                DoInsertUpdate(command, true);
            }

            MarkOld();
        }

        internal void DeleteSelf(IDataConnection connection, IDbTransaction transaction)
        {
            if (!IsDirty) return;

            if (IsNew) return;

            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = "Configuration.Credential#Delete";
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;
                command.AddParameterWithValue("Id", DbType.Guid, false, id);
                Database.AddArrayParameter(command, "Version", DbType.Binary, false, version);
                command.ExecuteNonQuery();
            }

            MarkNew();
        }

        private void DoInsertUpdate(IDataCommand command, bool isUpdate)
        {
            IDataParameter newVersion = Database.AddArrayOutParameter(command, "NewVersion", DbType.Binary, false, 8);
            command.AddParameterWithValue("Id", DbType.Guid, false, id);
            command.AddParameterWithValue("UserName", DbType.String, false, userName);
            command.AddParameterWithValue("Password", DbType.String, false, password);
            if (isUpdate)
            {
                Database.AddArrayParameter(command, "Version", DbType.Binary, false, version);
            }
            command.ExecuteNonQuery();
            version = (byte[])newVersion.Value;
        }

        #endregion
    }
}
