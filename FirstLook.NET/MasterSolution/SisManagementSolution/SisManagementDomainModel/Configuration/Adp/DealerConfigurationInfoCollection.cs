using System;
using System.Data;
using Csla;
using Csla.Core;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class DealerConfigurationInfoCollection : ReadOnlyListBase<DealerConfigurationInfoCollection, DealerConfigurationInfo>, IReportTotalRowCount
    {
        #region IReportTotalRowCount Members

        private int totalRowCount = 0;

        public int TotalRowCount
        {
            get
            {
                return totalRowCount;
            }
        }

        #endregion

        public static DealerConfigurationInfoCollection GetNewDealers(string filter, string sortOrder, int maximumRows, int startRowIndex)
        {
            if (maximumRows < 1)
                throw new ArgumentOutOfRangeException("maximumRows", maximumRows, "Must be greater than zero");

            if (startRowIndex < 0)
                throw new ArgumentOutOfRangeException("startRowIndex", startRowIndex, "Must be greater than zero");

            if (string.IsNullOrEmpty(filter))
                filter = string.Empty;

            if (string.IsNullOrEmpty(sortOrder))
                sortOrder = "Name";

            return DataPortal.Fetch<DealerConfigurationInfoCollection>(new FetchCriteria(true, false, filter, new DataSourceSelectCriteria(sortOrder, maximumRows, startRowIndex)));
        }

        public static DealerConfigurationInfoCollection GetExistingDealers(string filter, string sortOrder, int maximumRows, int startRowIndex)
        {
            if (maximumRows < 1)
                throw new ArgumentOutOfRangeException("maximumRows", maximumRows, "Must be greater than zero");

            if (startRowIndex < 0)
                throw new ArgumentOutOfRangeException("startRowIndex", startRowIndex, "Must be greater than zero");

            if (string.IsNullOrEmpty(filter))
                filter = string.Empty;

            if (string.IsNullOrEmpty(sortOrder))
                sortOrder = "Name";

            return DataPortal.Fetch<DealerConfigurationInfoCollection>(new FetchCriteria(false, true, filter, new DataSourceSelectCriteria(sortOrder, maximumRows, startRowIndex)));
        }

        private DealerConfigurationInfoCollection()
        {

        }

        [Serializable]
        class FetchCriteria
        {
            private readonly bool includeUnconfigured;
            private readonly bool includeConfigured;
            private readonly string filter;
            private readonly DataSourceSelectCriteria criteria;

            public FetchCriteria(bool includeUnconfigured, bool includeConfigured, string filter, DataSourceSelectCriteria criteria)
            {
                this.includeUnconfigured = includeUnconfigured;
                this.includeConfigured = includeConfigured;
                this.filter = filter;
                this.criteria = criteria;
            }

            public bool IncludeUnconfigured
            {
                get { return includeUnconfigured; }
            }

            public bool IncludeConfigured
            {
                get { return includeConfigured; }
            }

            public string Filter
            {
                get { return filter; }
            }

            public DataSourceSelectCriteria Criteria
            {
                get { return criteria; }
            }
        }

        private void DataPortal_Fetch(FetchCriteria criteria)
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Configuration.DealerConfigurationInfoCollection#Fetch";

                    command.AddParameterWithValue("IncludeUnconfigured", DbType.Boolean, true, criteria.IncludeUnconfigured);
                    command.AddParameterWithValue("IncludeConfigured", DbType.Boolean, true, criteria.IncludeConfigured);
                    command.AddParameterWithValue("Filter", DbType.String, true, criteria.Filter);

                    criteria.Criteria.AddToCommand(command);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            totalRowCount = reader.GetInt32(reader.GetOrdinal("TotalRowCount"));

                            if (reader.NextResult())
                            {
                                IsReadOnly = false;

                                while (reader.Read())
                                {
                                    Add(new DealerConfigurationInfo(reader));
                                }

                                IsReadOnly = true;
                            }
                            else
                            {
                                throw new DataException("DataReader missing second of two result sets");
                            }
                        }
                        else
                        {
                            throw new DataException("DataReader missing first of two result sets");
                        }
                    }
                }
            }

            RaiseListChangedEvents = true;
        }
    }
}
