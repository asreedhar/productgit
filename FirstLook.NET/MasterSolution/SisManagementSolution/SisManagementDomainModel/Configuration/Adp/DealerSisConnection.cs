using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    /// <summary>
    /// A mapping entry that links a Dealer to a SIS connection.
    /// </summary>
    [Serializable]
    public class DealerSisConnection : BusinessBase<DealerSisConnection>
    {
        #region Business Methods

        private Guid sisConnectionId;
        private string sisCredentialUserName;
        private Logon accountingLogon;
        private Logon financeLogon;
        private DateTime created;
        private DateTime modified;

        public Guid SisConnectionId
        {
            get
            {
                CanReadProperty("SisConnectionId", true);

                return sisConnectionId;
            }
        }

        public string SisCredentialUserName
        {
            get
            {
                CanReadProperty("SisCredentialUserName", true);

                return sisCredentialUserName;
            }
        }

        public Logon AccountingLogon
        {
            get
            {
                CanReadProperty("AccountingLogon", true);

                return accountingLogon;
            }
        }

        public Logon FinanceLogon
        {
            get
            {
                CanReadProperty("FinanceLogon", true);

                return financeLogon;
            }
        }

        public DateTime Created
        {
            get
            {
                CanReadProperty("Created", true);

                return created;
            }
        }

        public DateTime Modified
        {
            get
            {
                CanReadProperty("Modified", true);

                return modified;
            }
        }


        protected override object GetIdValue()
        {
            return sisConnectionId;
        }

        #endregion

        #region Factory Methods

        internal static DealerSisConnection NewDealerSisConnection(SisConnection sisConnection)
        {
            return new DealerSisConnection(sisConnection);
        }

        internal static DealerSisConnection GetDealerSisConnection(IDataRecord record)
        {
            return new DealerSisConnection(record);
        }

        private DealerSisConnection()
        { MarkAsChild(); }

        private DealerSisConnection(SisConnection sisConnection)
            : this()
        {
            this.sisConnectionId = sisConnection.Id;
            this.sisCredentialUserName = sisConnection.Credential.UserName;
            this.accountingLogon = sisConnection.AccountingLogon;
            this.financeLogon = sisConnection.FinanceLogon;
            this.created = sisConnection.Created;
            this.modified = sisConnection.Modified;
        }

        private DealerSisConnection(IDataRecord record)
            : this()
        {
            Fetch(record);
            MarkOld();
        }

        #endregion

        #region Data Access

        private void Fetch(IDataRecord record)
        {
            sisConnectionId = record.GetGuid(record.GetOrdinal("SisConnectionId"));
            sisCredentialUserName = record.GetString(record.GetOrdinal("SisCredentialUserName"));
            accountingLogon = Logon.GetLogon(record, "AccountingLogon");
            financeLogon = Logon.GetLogon(record, "FinanceLogon");
            created = record.GetDateTime(record.GetOrdinal("InsertDate"));
            modified = record.GetDateTime(record.GetOrdinal("UpdateDate"));
        }

        internal void Insert(IDataConnection connection, IDbTransaction transaction, Dealer dealer)
        {
            if (!IsDirty) return;

            DoAssignmentCommand(connection, "Configuration.DealerSisConnection#AddAssignment", transaction, dealer);

            MarkOld();
        }

        internal void DeleteSelf(IDataConnection connection, IDbTransaction transaction, Dealer dealer)
        {
            // if we're not dirty then don't update the database
            if (!IsDirty) return;

            // if we're new then don't update the database
            if (IsNew) return;

            DoAssignmentCommand(connection, "Configuration.DealerSisConnection#DeleteAssignment", transaction, dealer);

            MarkNew();
        }

        private void DoAssignmentCommand(IDataConnection connection, string commandText, IDbTransaction transaction, Dealer dealer)
        {
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = commandText;
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;
                command.AddParameterWithValue("EnvironmentId", DbType.Int32, false, (int)dealer.Environment);
                command.AddParameterWithValue("DealerId", DbType.Int32, false, dealer.Id);
                command.AddParameterWithValue("SisConnectionId", DbType.Guid, false, sisConnectionId);                
                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}
