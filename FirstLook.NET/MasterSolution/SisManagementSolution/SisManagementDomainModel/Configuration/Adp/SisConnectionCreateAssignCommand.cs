using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class SisConnectionCreateAssignCommand : CommandBase
    {
        private Dealer _dealer;
        private DmsHost _dmsHost;
        private SisConnection _sisconnection;

        public static void Execute(Dealer dealer, DmsHost dmsHost, SisConnection sisconnection)
        {
            DataPortal.Execute<SisConnectionCreateAssignCommand>(new SisConnectionCreateAssignCommand(dealer, dmsHost, sisconnection));
        }

        private SisConnectionCreateAssignCommand()
        { /* force factory methods */}

        private SisConnectionCreateAssignCommand(Dealer dealer, DmsHost dmsHost, SisConnection sisconnection)
        {
            this._dealer = dealer;
            this._dmsHost = dmsHost;
            this._sisconnection = sisconnection;
        }

        [Transactional(TransactionalTypes.Manual)]
        protected override void DataPortal_Execute()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        if (!_dmsHost.SisConnections.Contains(_sisconnection))
                        {
                            _dmsHost.SisConnections.Add(_sisconnection);
                        }
                        
                        _dmsHost.DoUpdate(connection, transaction);

                        if (!_dealer.Connections.Contains(_sisconnection.Id))
                        {
                            _dealer.Connections.Assign(_sisconnection);
                        }
                        _dealer.DoUpdate(connection, transaction);
                        transaction.Commit(); // also disposes of the transaction and its resources
                    }
                    catch
                    {
                        try
                        {
                            transaction.Rollback();
                        }
                        catch
                        {
                            throw;
                        }
                        throw;
                    }
                }
            }
        }

    }
}