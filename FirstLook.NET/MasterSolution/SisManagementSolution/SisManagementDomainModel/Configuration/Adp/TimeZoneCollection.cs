using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class TimeZoneCollection : ReadOnlyListBase<TimeZoneCollection, TimeZone>
    {
        public static TimeZoneCollection GetTimeZones()
        {
            return DataPortal.Fetch<TimeZoneCollection>(new Criteria());
        }

        [Serializable]
        class Criteria
        {
            
        }

        private void DataPortal_Fetch(Criteria criteria)
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Configuration.TimeZoneCollection#Fetch";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        IsReadOnly = false;

                        while (reader.Read())
                        {
                            Add(new TimeZone(reader));
                        }

                        IsReadOnly = true;
                    }
                }
            }

            RaiseListChangedEvents = true;
        }
    }
}
