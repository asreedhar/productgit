using System;
using System.Data;
using System.Globalization;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    /// <summary>
    /// An abstraction of an actual connection made to a DmsHost.
    /// </summary>
    [Serializable]
    public class SisConnection : BusinessBase<SisConnection>
    {
        #region Business Methods

        private byte[] version = new byte[8];

        private Guid id;
        private DateTime created;
        private DateTime modified;

        public Guid Id
        {
            get
            {
                CanReadProperty("Id", true);

                return id;
            }
        }

        private Credential dmsHostCredential;

        public Credential DmsHostCredential
        {
            get
            {
                CanReadProperty("DmsHostCredential", true);

                return dmsHostCredential;
            }
            set
            {
                CanWriteProperty("DmsHostCredential", true);

                if (!Equals(DmsHostCredential, value))
                {
                    dmsHostCredential = value;

                    PropertyHasChanged("DmsHostCredential");
                }
            }
        }

        private Logon accountingLogon;

        public Logon AccountingLogon
        {
            get
            {
                CanReadProperty("AccountingLogon", true);

                return accountingLogon;
            }
            set
            {
                CanWriteProperty("AccountingLogon", true);

                if (!Equals(AccountingLogon, value))
                {
                    accountingLogon = value;

                    PropertyHasChanged("AccountingLogon");
                }
            }
        }

        private Logon financeLogon;

        public Logon FinanceLogon
        {
            get
            {
                CanReadProperty("FinanceLogon", true);

                return financeLogon;
            }
            set
            {
                CanWriteProperty("FinanceLogon", true);

                if (!Equals(FinanceLogon, value))
                {
                    financeLogon = value;

                    PropertyHasChanged("FinanceLogon");
                }
            }
        }

        private Credential credential;

        public Credential Credential
        {
            get
            {
                CanReadProperty("Credential", true);

                return credential;
            }
            set
            {
                CanWriteProperty("Credential", true);

                if (!Equals(Credential, value))
                {
                    credential = value;

                    PropertyHasChanged("Credential");
                }
            }
        }

        public DateTime Created
        {
            get
            {
                CanReadProperty("Created", true);

                return created;
            }
        }

        public DateTime Modified
        {
            get
            {
                CanReadProperty("Modified", true);

                return modified;
            }
        }

        protected override object GetIdValue()
        {
            return id;
        }

        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture, "{0}/{1}", AccountingLogon, FinanceLogon);
        }

        #endregion

        #region Authorization Rules

        private static readonly string[] PropertyNames = new string[]
            {
                "DmsHostCredential", "Credential",
                "FinanceLogon", "AccountingLogon"
            };

        protected override void AddAuthorizationRules()
        {
            foreach (string propertyName in PropertyNames)
            {
                AuthorizationRules.AllowWrite(propertyName, "DataManagementWriter");
            }
        }

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        public static bool CanAddObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        public static bool CanEditObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        #endregion

        #region Validation Rules

        public override bool IsValid
        {
            get
            {
                return (
                    base.IsValid &&
                    (credential != null && credential.IsValid) &&
                    (dmsHostCredential != null && dmsHostCredential.IsValid) &&
                    (accountingLogon != null && accountingLogon.IsValid) &&
                    (financeLogon != null && financeLogon.IsValid));
            }
        }

        public override bool IsDirty
        {
            get
            {
                return (
                    base.IsDirty ||
                    (credential != null && credential.IsDirty) ||
                    (dmsHostCredential != null && dmsHostCredential.IsDirty) ||
                    (accountingLogon != null && accountingLogon.IsDirty) ||
                    (financeLogon != null && financeLogon.IsDirty));
            }
        }

        protected override void AddBusinessRules()
        {
            ValidationRules.AddRule(Credential.CredentialRequired,
                "Credential");

            ValidationRules.AddRule(Credential.CredentialRequired,
                "DmsHostCredential");

            ValidationRules.AddRule(Logon.LogonRequired,
                "AccountingLogon");

            ValidationRules.AddRule(Logon.LogonRequired,
                "FinanceLogon");
        }

        #endregion

        #region Factory Methods

        internal static SisConnection GetSisConnection(IDataRecord record)
        {
            return new SisConnection(record);
        }

        public static SisConnection NewSisConnection()
        {
            return DataPortal.Create<SisConnection>();
        }

        private SisConnection()
        { 
            MarkAsChild();
        }

        private SisConnection(IDataRecord record) : this()
        {
            Fetch(record);
            MarkOld();
        }

        #endregion

        #region Data Access

        [Serializable]
        class Criteria
        {
            private readonly Guid id;

            public Criteria(Guid id)
            {
                this.id = id;
            }

            public Guid Id
            {
                get { return id; }
            }
        }

        [RunLocal]
        protected override void DataPortal_Create()
        {
            id = Guid.NewGuid();
        }

        private void Fetch(IDataRecord record)
        {
            id = record.GetGuid(record.GetOrdinal("Id"));
            credential = Credential.GetCredential(record, "SisCredential");
            dmsHostCredential = Credential.GetCredential(record, "DmsHostCredential");
            accountingLogon = Logon.GetLogon(record, "AccountingLogon");
            financeLogon = Logon.GetLogon(record, "FinanceLogon");

            created = record.GetDateTime(record.GetOrdinal("InsertDate"));
            modified = record.GetDateTime(record.GetOrdinal("UpdateDate"));

            if (!record.IsDBNull(record.GetOrdinal("Version")))
            {
                record.GetBytes(record.GetOrdinal("Version"), 0, version, 0, 8);
            }
        }

        internal void Insert(IDataConnection connection, IDbTransaction transaction, DmsHost dmsHost)
        {
            if (!IsDirty) return;

            credential.Insert(connection, transaction);

            dmsHostCredential.Insert(connection, transaction);

            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = "Configuration.SisConnection#Insert";
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;
                DoInsertUpdate(command, dmsHost, false);
            }

            MarkOld();
        }

        internal void Update(IDataConnection connection, IDbTransaction transaction, DmsHost dmsHost)
        {
            if (!IsDirty) return;

            credential.Update(connection, transaction);

            dmsHostCredential.Update(connection, transaction);

            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = "Configuration.SisConnection#Update";
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;
                DoInsertUpdate(command, dmsHost, true);
            }

            MarkOld();
        }

        internal void DeleteSelf(IDataConnection connection, IDbTransaction transaction, DmsHost dmsHost)
        {
            if (!IsDirty) return;

            if (IsNew) return;

            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = "Configuration.SisConnection#Delete";
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;
                command.AddParameterWithValue("Id", DbType.Guid, false, id);
                command.AddParameterWithValue("Version", DbType.Binary, false, version);
                command.ExecuteNonQuery();
            }

            credential.DeleteSelf(connection, transaction);

            dmsHostCredential.DeleteSelf(connection, transaction);

            MarkNew();
        }

        private void DoInsertUpdate(IDataCommand command, DmsHost dmsHost, bool isUpdate)
        {
            IDataParameter newVersion = Database.AddArrayOutParameter(command, "NewVersion", DbType.Binary, false, 8);
            command.AddParameterWithValue("DmsHostId", DbType.Guid, false, dmsHost.Id);
            command.AddParameterWithValue("Id", DbType.Guid, false, id);
            command.AddParameterWithValue("SisCredentialId", DbType.Guid, false, credential.Id);
            command.AddParameterWithValue("DmsHostCredentialId", DbType.Guid, false, dmsHostCredential.Id);
            command.AddParameterWithValue("AccountingLogonId", DbType.Guid, false, accountingLogon.Id);
            command.AddParameterWithValue("FinanceLogonId", DbType.Guid, false, financeLogon.Id);
            if (isUpdate)
            {
                Database.AddArrayParameter(command, "Version", DbType.Binary, false, version);
            }
            command.ExecuteNonQuery();
            version = (byte[])newVersion.Value;
        }

        #endregion
    }
}
