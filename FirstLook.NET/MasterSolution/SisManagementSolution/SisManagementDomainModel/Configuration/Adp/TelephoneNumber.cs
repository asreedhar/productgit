using System;
using System.Data;
using System.Globalization;
using System.Runtime.CompilerServices;
using Csla;
using Csla.Validation;
using FirstLook.Common.Data;

[assembly: InternalsVisibleTo("FirstLook.Sis.Management.DomainModel.Test")]
namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class TelephoneNumber
    {
        private int areaCode;
        private int exchangeCode;
        private int stationCode;

        public int AreaCode
        {
            get
            {
                return areaCode;
            }
            set
            {
                if (AreaCode != value)
                {
                    if (IsValidAreaCode(value))
                    {
                        areaCode = value;
                    }
                    else
                    {
                        throw new ArgumentOutOfRangeException("value", value, "Area Code must be [2-9][0-8][0-9]");
                    }
                }
            }
        }

        /// <summary>
        /// Validates according to the rule: NPA (Numbering Plan Area code) = [2-9][0-8][0-9]
        /// 
        /// http://en.wikipedia.org/wiki/North_American_Numbering_Plan
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        internal static bool IsValidAreaCode(int value)
        {
            if (value >= 200 && value <= 989)
            {
                //purposely get ride of decimal points
                int hundredDigit = value / 100;
                value = value - (hundredDigit * 100);
                return (value >= 0 && value <= 89);
            }
            else
            {
                return false;
            }
        }

        public int ExchangeCode
        {
            get
            {
                return exchangeCode;
            }
            set
            {
                if (ExchangeCode != value)
                {
                    if (IsValidExchangeCode(value))
                    {
                        exchangeCode = value;
                    }
                    else
                    {
                        throw new ArgumentOutOfRangeException("value", value, "Exchange Code must be [2-9][0-9][0-9]");
                    }
                }
            }
        }

        internal static bool IsValidExchangeCode(int value)
        {
            return value >= 200 && value <= 999;
        }

        public int StationCode
        {
            get
            {
                return stationCode;
            }
            set
            {
                if (StationCode != value)
                {
                    if (IsValidStationCode(value))
                    {
                        stationCode = value;
                    }
                    else
                    {
                        throw new ArgumentOutOfRangeException("value", value, "Station Code must be [0-9][0-9][0-9][0-9]");
                    }
                }
            }
        }

        internal static bool IsValidStationCode(int value)
        {
            return value >= 0 && value <= 9999;
        }

        public bool IsValid
        {
            get
            {
                return (
                    IsValidAreaCode(areaCode) && 
                    IsValidExchangeCode(exchangeCode) &&
                    IsValidStationCode(stationCode));
            }
        }

        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture, "{0}-{1}-{2}", areaCode, exchangeCode, stationCode);
        }

        internal TelephoneNumber(int areaCode, int exchangeCode, int stationCode)
        {
            this.AreaCode = areaCode;
            this.ExchangeCode = exchangeCode;
            this.StationCode = stationCode;
        }

        public static bool TryParse(string areaCode, string exchangeCode, string stationCode, ref TelephoneNumber number)
        {
            bool success = true;

            int a, e, s;

            success &= int.TryParse(areaCode, out a);
            success &= int.TryParse(exchangeCode, out e);
            success &= int.TryParse(stationCode, out s);

            if (success)
            {
                if (IsValidAreaCode(a) && IsValidExchangeCode(e) && IsValidStationCode(s))
                {
                    number = new TelephoneNumber(a, e, s);
                }
                else
                {
                    success = false;
                }
            }

            return success;
        }

        public static bool TryParse(string value, ref TelephoneNumber number)
        {
            bool success = false;

            if (!string.IsNullOrEmpty(value))
            {
                string[] values = value.Split(new char[] { '-', '.', ' ' });

                if (values.Length == 3)
                {
                    success = TryParse(values[0], values[1], values[2], ref number);
                }
            }

            return success;
        }

        public static bool TelephoneNumberIsValid(object target, RuleArgs e)
        {
            TelephoneNumber value = (TelephoneNumber)Utilities.CallByName(target, e.PropertyName, CallType.Get);

            if (value != null && !value.IsValid)
            {
                e.Description = string.Format("{0} is not a valid telephone number", e.PropertyName);

                return false;
            }

            return true;
        }

        internal static TelephoneNumber GetTelephoneNumber(IDataRecord record, string parameterName)
        {
            TelephoneNumber number = null;

            if (!record.IsDBNull(record.GetOrdinal(parameterName)))
            {
                string value = record.GetString(record.GetOrdinal(parameterName));

                TryParse(value, ref number);
            }

            return number;
        }

        internal static void AddToCommand(TelephoneNumber number, IDataCommand command, string parameterName)
        {
            if (number != null)
            {
                command.AddParameterWithValue(parameterName, DbType.String, false, number.ToString());
            }
            else
            {
                command.AddParameterWithValue(parameterName, DbType.String, true, DBNull.Value);
            }
        }
    }
}
