using System;
using System.Data;
using System.Security;
using Csla;
using FirstLook.Common.Data;
using FirstLook.Sis.Management.DomainModel.Configuration.Adp;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class DealerLogonDataSplitConfiguration : BusinessBase<DealerLogonDataSplitConfiguration>
    {
        #region Business Methods

        private DealerCompanyLogonDataSplitCollection dealerCompanyLogonDataSplitCollection = Adp.DealerCompanyLogonDataSplitCollection.NewDealerCompanyLogonDataSplitCollection();
        private DealerStockLogonDataSplitCollection dealerStockLogonDataSplitCollection = Adp.DealerStockLogonDataSplitCollection.NewDealerStockLogonDataSplitCollection();
        private DealerLotLogonDataSplitCollection dealerLotLogonDataSplitCollection = DealerLotLogonDataSplitCollection.NewDealerLotLogonDataSplitCollection();

        public DealerCompanyLogonDataSplitCollection DealerCompanyLogonDataSplitCollection
        {
            get
            {
                CanReadProperty("DealerCompanyLogonDataSplitCollection", true);
                return dealerCompanyLogonDataSplitCollection;
            }
            set
            {
                CanWriteProperty("DealerCompanyLogonDataSplitCollection", true);
                dealerCompanyLogonDataSplitCollection = value;
                PropertyHasChanged("DealerCompanyLogonDataSplitCollection");
            }
        }
        public DealerStockLogonDataSplitCollection DealerStockLogonDataSplitCollection
        {
            get
            {
                CanReadProperty("DealerStockLogonDataSplitCollection", true);
                return dealerStockLogonDataSplitCollection;
            }
            set
            {
                CanWriteProperty("DealerStockLogonDataSplitCollection", true);
                dealerStockLogonDataSplitCollection = value;
                PropertyHasChanged("DealerStockLogonDataSplitCollection");
            }
        }
        public DealerLotLogonDataSplitCollection DealerLotLogonDataSplitCollection
        {
            get
            {
                CanReadProperty("DealerLotLogonDataSplitCollection", true);
                return dealerLotLogonDataSplitCollection;
            }
            set
            {
                CanWriteProperty("DealerLotLogonDataSplitCollection", true);
                dealerLotLogonDataSplitCollection = value;
                PropertyHasChanged("DealerLotLogonDataSplitCollection");
            }
        }

        protected override object GetIdValue()
        {
            return 0;
        }

        #endregion

        #region Authorization Rules

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        public static bool CanAddObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        public static bool CanEditObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        #endregion

        #region Factory Methods

        public static DealerLogonDataSplitConfiguration GetDealerLogonDataSplitConfiguration(Dealer dealer, Logon logon)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view DealerLogonDataSplitConfiguration");

            return DataPortal.Fetch<DealerLogonDataSplitConfiguration>(new Criteria(dealer, logon));
        }

        public override DealerLogonDataSplitConfiguration Save()
        {
            if (IsNew && !CanAddObject())
                throw new SecurityException("User not authorized to add DealerLogonDataSplitConfiguration");

            if (!CanEditObject())
                throw new SecurityException("User not authorized to edit DealerLogonDataSplitConfiguration");

            MarkDirty();
            return base.Save();
        }

        #endregion

        #region Data Access

        [Serializable]
        class Criteria
        {
            private readonly Dealer dealer;
            private readonly Logon logon;

            public Criteria(Dealer dealer, Logon logon)
            {
                this.dealer = dealer;
                this.logon = logon;
            }

            public Dealer Dealer
            {
                get { return dealer; }
            }

            public Logon Logon
            {
                get { return logon; }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called by CSLA using reflection")]
        [Transactional(TransactionalTypes.Manual)]
        private void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Configuration.DealerLogonDataSplitConfiguration#Fetch";
                    command.AddParameterWithValue("DealerId", DbType.Int32, true, criteria.Dealer.Id);
                    command.AddParameterWithValue("EnvironmentId", DbType.Int32, true, (int)criteria.Dealer.Environment);
                    command.AddParameterWithValue("LogonId", DbType.Guid, true, criteria.Logon.Id);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        //dealer = criteria.Dealer;
                        
                        dealerCompanyLogonDataSplitCollection = DealerCompanyLogonDataSplitCollection.GetDealerCompanyLogonDataSplitCollection(reader);

                        if (reader.NextResult())
                        {
                            dealerLotLogonDataSplitCollection = DealerLotLogonDataSplitCollection.GetDealerLotLogonDataSplitCollection(reader);
                        }
                        else
                        {
                            throw new Exception("Missing DealerLotLogonDataSplitCollection ResultSet.");
                        }

                        if (reader.NextResult())
                        {
                            dealerStockLogonDataSplitCollection = DealerStockLogonDataSplitCollection.GetDealerStockLogonDataSplitCollection(reader);
                        }
                        else
                        {
                            throw new Exception("Missing DealerStockLogonDataSplitCollection ResultSet.");
                        }
                    }
                }
            }
            ValidationRules.CheckRules();
        }

        [Transactional(TransactionalTypes.Manual)]
        protected override void DataPortal_Update()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        dealerCompanyLogonDataSplitCollection.Update(connection, transaction);
                        dealerLotLogonDataSplitCollection.Update(connection, transaction);
                        dealerStockLogonDataSplitCollection.Update(connection, transaction);
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }


        #endregion
    }
}