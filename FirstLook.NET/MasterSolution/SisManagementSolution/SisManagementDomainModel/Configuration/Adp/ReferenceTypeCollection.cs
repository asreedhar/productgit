using System;
using System.Data;
using System.Security;
using Csla;
using Csla.Validation;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    /// <summary>
    /// This class lists the Reference Type for a Journal.
    /// </summary>
    [Serializable]
    public class ReferenceTypeCollection : NameValueListBase<int, string>
    {
        /// <summary>
        /// Returns the default ReferenceTypeId
        /// </summary>
        /// <returns></returns>
        public static int DefaultReferenceType()
        {
            ReferenceTypeCollection collection = GetReferenceTypes();
            if (collection.Count > 0)
            {
                if (collection.ContainsKey(1))
                    return 1;
                else
                    return collection.Items[0].Key;
            }
            else
                throw new NullReferenceException(
                    "No ReferenceType available; default ReferenceType cannot be returned");
        }

        #region Validation Rules

        /// <summary>
        /// Checks the collection of ReferenceTypes for a valid value.
        /// </summary>
        /// <param name="target">calling object</param>
        /// <param name="e"></param>
        /// <returns></returns>
        public static bool ValidReferenceTypeIdRequired(object target, RuleArgs e)
        {
            int? referenceTypeId = (int?)Utilities.CallByName(target, e.PropertyName, CallType.Get);

            bool isValid = false;
            if (referenceTypeId.HasValue)
            {
                isValid = ReferenceTypeCollection.GetReferenceTypes().ContainsKey(referenceTypeId.Value);
                if (!isValid)
                {
                    e.Description = "ReferenceTypeId is not a valid value.";
                }
            }
            return isValid;
        }

        #endregion

        #region Factory Methods

        private static ReferenceTypeCollection _list;

        public static ReferenceTypeCollection GetReferenceTypes()
        {
            if (_list == null)
            {
                _list = DataPortal.Fetch<ReferenceTypeCollection>(new Criteria(typeof(ReferenceTypeCollection)));
            }
            return _list;
        }

        public static void InvalidateCache()
        {
            _list = null;
        }

        private ReferenceTypeCollection()
        { /* require use of factory methods */}

        #endregion

        #region Data Access

        private void DataPortal_Fetch(Criteria criteria)
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Configuration.ReferenceTypeCollection#Fetch";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        IsReadOnly = false;

                        while (reader.Read())
                        {
                            Add(new NameValuePair(reader.GetInt32(reader.GetOrdinal("Id")), reader.GetString(reader.GetOrdinal("Name"))));
                        }

                        IsReadOnly = true;
                    }
                }
            }

            RaiseListChangedEvents = true;
        }

        #endregion
    }
}
