using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class DmsHostLogonCollection : BusinessListBase<DmsHostLogonCollection, Logon>
    {
        #region Business Methods

        public Logon GetItem(Guid logonId)
        {
            foreach (Logon logon in this)
                if (logon.Id == logonId)
                    return logon;
            return null;
        }

        public void Assign(Logon logon)
        {
            if (!Contains(logon.Id))
            {
                //foreach (Logon l in this)
                //    if (l.LogonType == logon.LogonType && l.Name == logon.Name)
                //        throw new InvalidOperationException(logon.LogonType + " Logon with the same name already assigned to DMS Host");

                Add(logon);
            }
            else
            {
                throw new InvalidOperationException("Logon already assigned to DMS Host");
            }
        }

        public void Remove(Guid logonId)
        {
            foreach (Logon logon in this)
            {
                if (logon.Id == logonId)
                {
                    Remove(logon);
                    break;
                }
            }
        }

        public bool Contains(Guid logonId)
        {
            foreach (Logon logon in this)
                if (logon.Id == logonId)
                    return true;
            return false;
        }

        public bool ContainsDeleted(Guid logonId)
        {
            foreach (Logon logon in DeletedList)
                if (logon.Id == logonId)
                    return true;
            return false;
        }

        #endregion

        #region Factory Methods

        internal static DmsHostLogonCollection NewDmsHostLogonCollection()
        {
            return new DmsHostLogonCollection();
        }

        public static DmsHostLogonCollection GetDmsHostLogonCollection(IDataReader reader)
        {
            return new DmsHostLogonCollection(reader);
        }

        private DmsHostLogonCollection()
        {
            MarkAsChild();
        }

        private DmsHostLogonCollection(IDataReader reader)
            : this()
        {
            Fetch(reader);
        }

        #endregion

        #region Data Access

        private void Fetch(IDataReader reader)
        {
            RaiseListChangedEvents = false;
            while (reader.Read())
            {
                Add(Logon.GetLogon(reader, string.Empty));
            }
            RaiseListChangedEvents = true;
        }

        internal void Update(IDataConnection connection, IDbTransaction transaction, DmsHost dmsHost)
        {
            RaiseListChangedEvents = false;
            // update (thus deleting) any deleted child objects
            foreach (Logon obj in DeletedList)
                obj.DeleteSelf(connection, transaction, dmsHost);
            // now that they are deleted, remove them from memory too
            DeletedList.Clear();
            // add/update any current child objects
            foreach (Logon obj in this)
            {
                if (obj.IsNew)
                    obj.Insert(connection, transaction, dmsHost);
                else
                    obj.Update(connection, transaction, dmsHost);
            }
            RaiseListChangedEvents = true;
        }

        #endregion
    }
}
