using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class DealerConfigurationInfo : ReadOnlyBase<DealerConfigurationInfo>
    {
        #region Business Methods

        private bool active;
        private string address;
        private ConfigurationWorkStep configurationWorkStep;
        private string dealerGroupName;
        private int id;
        private string name;

        public int Id
        {
            get
            {
                CanReadProperty("Id", true);

                return id;
            }
        }

        public string Name
        {
            get
            {
                CanReadProperty("Name", true);

                return name;
            }
        }

        public string Address
        {
            get
            {
                CanReadProperty("Address", true);

                return address;
            }
        }

        public string DealerGroupName
        {
            get
            {
                CanReadProperty("DealerGroupName", true);

                return dealerGroupName;
            }
        }

        public bool Active
        {
            get
            {
                CanReadProperty("Active", true);

                return active;
            }
        }

        public ConfigurationWorkStep ConfigurationWorkStep
        {
            get
            {
                CanReadProperty("ConfigurationWorkStep", true);

                return configurationWorkStep;
            }
        }

        protected override object GetIdValue()
        {
            return id;
        }

        #endregion

        #region Constructors

        public static DealerConfigurationInfo GetDealer(int id)
        {
            return DataPortal.Fetch<DealerConfigurationInfo>(new Criteria(id));
        }

        private DealerConfigurationInfo()
        {
            /* force use of factory methods */
        }

        internal DealerConfigurationInfo(IDataRecord record)
        {
            Fetch(record);
        }

        #endregion

        [Serializable]
        class Criteria
        {
            private readonly int id;

            public Criteria(int id)
            {
                this.id = id;
            }

            public int Id
            {
                get { return id; }
            }
        }

        private void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Configuration.DealerConfigurationInfo#Fetch";
                    command.AddParameterWithValue("Id", DbType.Int32, true, criteria.Id);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Fetch(reader);
                        }
                        else
                        {
                            throw new DataException("Dealer not found");
                        }
                    }
                }
            }
        }

        private void Fetch(IDataRecord record)
        {
            active = record.GetBoolean(record.GetOrdinal("Active"));
            address = record.GetString(record.GetOrdinal("Address"));
            configurationWorkStep = (ConfigurationWorkStep)Enum.ToObject(typeof(ConfigurationWorkStep), record.GetInt32(record.GetOrdinal("ConfigurationWorkStep")));
            dealerGroupName = record.GetString(record.GetOrdinal("DealerGroupName"));
            id = record.GetInt32(record.GetOrdinal("Id"));
            name = record.GetString(record.GetOrdinal("Name"));
        }
    }
}