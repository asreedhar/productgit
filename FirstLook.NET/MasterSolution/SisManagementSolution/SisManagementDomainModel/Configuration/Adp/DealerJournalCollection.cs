using System;
using System.Collections.Generic;
using System.Data;
using System.Security;
using Csla;
using Csla.Core;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    /// <summary>
    /// The collection mapping of Journals belonging to a Dealer.  
    /// This is a root object instead of a child object to Dealer to avoid loading the entire database.
    /// </summary>
    [Serializable]
    public class DealerJournalCollection : BusinessListBase<DealerJournalCollection, DealerJournal>
    {
        #region Business Methods

        private int _dealerId;
        private Environment _environment;

        public DealerJournal GetItem(Guid journalId)
        {
            foreach (DealerJournal journal in this)
                if (journal.JournalId.Equals(journalId))
                    return journal;
            return null;
        }

        public void Assign(Guid journalId)
        {
            if (!Contains(journalId))
            {
                if (!ContainsDeleted(journalId))
                {
                    Add(DealerJournal.NewDealerJournal(journalId));
                }
                else
                {
                    DealerJournal deleted = GetDeletedItem(journalId);
                    Add(deleted);
                    DeletedList.Remove(deleted);
                }
            }
            else
            {
                throw new InvalidOperationException("Journal already assigned to Dealer");
            }
        }

        public void Remove(Guid journalId)
        {
            foreach (DealerJournal journal in this)
            {
                if (journal.JournalId.Equals(journalId))
                {
                    Remove(journal);
                    break;
                }
            }
        }

        public bool Contains(Guid journalId)
        {
            foreach (DealerJournal journal in this)
                if (journal.JournalId.Equals(journalId))
                    return true;
            return false;
        }

        public bool ContainsDeleted(Guid journalId)
        {
            foreach (DealerJournal journal in DeletedList)
                if (journal.JournalId.Equals(journalId))
                    return true;
            return false;
        }

        private DealerJournal GetDeletedItem(Guid journalId)
        {
            foreach (DealerJournal journal in DeletedList)
                if (journal.JournalId.Equals(journalId))
                    return journal;
            return null;
        }

        #endregion

        #region Authorization Methods

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Factory Methods

        public static DealerJournalCollection GetDealerJournals(int dealerId, Environment environment, Guid accountingLogonId)
        {
        	if (!CanGetObject())
                throw new SecurityException("User not authorized to retrieve DealerJournals");
                
            return DataPortal.Fetch<DealerJournalCollection>(new FetchCriteria(dealerId, environment, accountingLogonId, "Number"));
        }

        private DealerJournalCollection()
        {
            MarkAsChild();
        }

        #endregion

        #region Data Access 

        [Serializable]
        private class FetchCriteria
        {
            private readonly int dealerId;
            private readonly Environment environment;
            private readonly Guid accountingLogonId;
            private readonly JournalType journalType;
            private readonly string sortOrder;

            public FetchCriteria(int dealerId, Environment environment, Guid accountingLogonId, string sortOrder)
            {
                this.dealerId = dealerId;
                this.sortOrder = sortOrder;
                this.environment = environment;
                this.accountingLogonId = accountingLogonId;
            }

            public FetchCriteria(int dealerId, Environment environment, Guid accountingLogonId, JournalType journalType, string sortOrder)
            {
                this.dealerId = dealerId;
                this.sortOrder = sortOrder;
                this.environment = environment;
                this.accountingLogonId = accountingLogonId;
                this.journalType = journalType;
            }

            public Guid AccountingLogonId
            {
                get { return accountingLogonId; }
            }

            public int DealerId
            {
                get { return dealerId; }
            }

            public Environment Environment
            {
                get { return environment; }
            }

            public JournalType JournalType
            {
                get { return journalType; }
            }

            public string SortOrder
            {
                get { return sortOrder; }
            }
        }

        private void DataPortal_Fetch(FetchCriteria criteria)
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Configuration.DealerJournalCollection#Fetch";

                    command.AddParameterWithValue("DealerId", DbType.Int32, false, criteria.DealerId);
                    command.AddParameterWithValue("EnvironmentId", DbType.Int32, false, (int)criteria.Environment);
                    command.AddParameterWithValue("AccountingLogonId", DbType.Guid, false, criteria.AccountingLogonId);
                    command.AddParameterWithValue("JournalTypeId", DbType.String, true, (int?)criteria.JournalType);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Add(DealerJournal.GetDealerJournal(reader));
                        }

                        _dealerId = criteria.DealerId;
                        _environment = criteria.Environment;
                    }
                }
            }

            RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Let the DealerAccountChart handle the transaction
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="dealerId"></param>
        /// <param name="environment"></param>
        internal void Update(IDataConnection connection, IDbTransaction transaction, int dealerId, Environment environment)
        {
            this.RaiseListChangedEvents = false;

            foreach (DealerJournal item in DeletedList)
            {
                item.DeleteSelf(connection, transaction, _dealerId, _environment);
            }
            DeletedList.Clear();

            // add/update any current child objects
            foreach (DealerJournal item in this)
            {
                if (item.IsNew)
                {
                    item.Insert(connection, transaction, _dealerId, _environment);
                }
                else
                {
                    item.Update(connection, transaction, _dealerId, _environment);
                }
            }

            this.RaiseListChangedEvents = true;
        }

        #endregion
    }
}
