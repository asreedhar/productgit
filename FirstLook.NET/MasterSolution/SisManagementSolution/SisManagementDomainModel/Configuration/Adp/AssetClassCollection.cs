using System;
using System.Data;
using System.Security;
using Csla;
using Csla.Validation;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    /// <summary>
    /// Maintains the reference lookup of AssetClass.  Code transcribed from 
    /// CSLA.Net ProjectTracker.Library sample code.  It doesn't seem to worry about 
    /// threading issues with _list.
    /// </summary>
    [Serializable]
    public class AssetClassCollection : NameValueListBase<int, string>
    {
        #region Business Methods

        /// <summary>
        /// Returns the default AssetClassId
        /// </summary>
        /// <returns></returns>
        public static int DefaultAssetClass()
        {
            AssetClassCollection collection = GetAssetClasses();
            if (collection.Count > 0)
            {
                if (collection.ContainsKey(1))
                    return 1;
                else
                    return collection.Items[0].Key;
            }
            else
                throw new NullReferenceException(
                    "No Asset Classes available; default asset class cannot be returned");
        }

        #endregion


        #region Validation Rules

        /// <summary>
        /// Checks the collection of AssetClass for a valid value.
        /// </summary>
        /// <param name="target">calling object</param>
        /// <param name="e"></param>
        /// <returns></returns>
        public static bool ValidAssetClassIdRequired(object target, RuleArgs e)
        {
            int? assetClassId = (int?)Utilities.CallByName(target, e.PropertyName, CallType.Get);

            bool isValid = false;
            if (assetClassId.HasValue)
            {
                isValid = AssetClassCollection.GetAssetClasses().ContainsKey(assetClassId.Value);
                if (!isValid)
                {
                    e.Description = "AssetClassId is not a valid value.";
                }
            }
            return isValid;
        }

        #endregion

        #region Factory Methods

        private static AssetClassCollection _list;

        public static AssetClassCollection GetAssetClasses()
        {
            if (_list == null)
            {
                _list = DataPortal.Fetch<AssetClassCollection>(new Criteria(typeof(AssetClassCollection)));
            }
            return _list;
        }

        public static void InvalidateCache()
        {
            _list = null;
        }

        private AssetClassCollection()
        { /* require use of factory methods */}

        #endregion

        #region Data Access

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "criteria")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(Criteria criteria)
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Configuration.AssetClassCollection#Fetch";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        IsReadOnly = false;

                        while (reader.Read())
                        {
                            Add(new NameValuePair(reader.GetInt32(reader.GetOrdinal("Id")), reader.GetString(reader.GetOrdinal("Name"))));
                        }

                        IsReadOnly = true;
                    }
                }
            }

            RaiseListChangedEvents = true;
        }

        #endregion
    }
}
