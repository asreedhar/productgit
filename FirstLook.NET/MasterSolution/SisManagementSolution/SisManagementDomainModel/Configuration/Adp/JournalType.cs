namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    public enum JournalType
    {
        Undefined = 0,

        A = 65,
        B = 66,
        C = 67,
        F = 70,

        /// <summary>
        /// "G"
        /// </summary>
        General = 71,

        /// <summary>
        /// "H"
        /// </summary>
        History = 72,

        /// <summary>
        /// "R"
        /// </summary>
        Receipts = 82,

        /// <summary>
        /// "S"
        /// </summary>
        Sales = 83,

        /// <summary>
        /// "Y"
        /// </summary>
        YearToDateAdjustments = 89

    }
}
