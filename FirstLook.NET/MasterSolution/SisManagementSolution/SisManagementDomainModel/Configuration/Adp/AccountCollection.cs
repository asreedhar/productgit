using System;
using System.Data;
using System.Security;
using Csla;
using Csla.Core;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class AccountCollection : ReadOnlyListBase<AccountCollection, Account>
    {
        #region Authorization Methods

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Factory Methods

        public static AccountCollection GetAccounts(Guid accountingLogonId, AccountType accountType, string department, string sortOrder)
        {
            if(!CanGetObject())
                throw new SecurityException("User not authorized to view accounts");

            if (string.IsNullOrEmpty(sortOrder))
                sortOrder = "Number";

            return DataPortal.Fetch<AccountCollection>(new FetchCriteria(accountingLogonId, accountType, department, sortOrder));
        }

        #endregion

        #region Data Access

        [Serializable]
        private class FetchCriteria
        {
            private readonly Guid accountingLogonId;
            private readonly AccountType accountType;
            private readonly string department;
            private readonly string sortOrder;

            public FetchCriteria(Guid accountingLogonId, AccountType accountType, string department, string sortOrder)
            {
                this.accountingLogonId = accountingLogonId;
                this.accountType = accountType;
                this.department = department;
                this.sortOrder = sortOrder;
            }

            public Guid AccountingLogonId
            {
                get { return accountingLogonId; }
            }

            public AccountType AccountType
            {
                get { return accountType; }
            }

            public string Department
            {
                get { return department; }
            }

            public string SortOrder
            {
                get { return sortOrder; }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "criteria")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchCriteria criteria)
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Configuration.AccountCollection#Fetch";

                    command.AddParameterWithValue("AccountingLogonId", DbType.Guid, false, criteria.AccountingLogonId);
                    command.AddParameterWithValue("AccountTypeId", DbType.Int32, false, (int)criteria.AccountType);
                    command.AddParameterWithValue("DepartmentFilter", DbType.String, true, criteria.Department);
                    command.AddParameterWithValue("SortOrder", DbType.String, true, criteria.SortOrder);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        IsReadOnly = false;

                        while (reader.Read())
                        {
                            Add(Account.GetAccount(reader));
                        }

                        IsReadOnly = true;
                    }
                }
            }

            RaiseListChangedEvents = true;
        }

        #endregion
    }
}
