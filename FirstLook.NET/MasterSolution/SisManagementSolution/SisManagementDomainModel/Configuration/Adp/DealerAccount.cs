using System;
using System.Data;
using System.Security;
using Csla;
using Csla.Validation;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class DealerAccount : BusinessBase<DealerAccount>
    {
        #region Business Methods

        private Guid _accountId = Guid.Empty;
        private string _number;
        private string _description;
        private string _type;
        private string _subType;
        private int _controlType;
        private string _department;
        private bool _isRetail;
        private bool _isWholesale;
        private bool _isFrontEnd;

        public Guid AccountId
        {
            get
            {
                CanReadProperty("AccountId", true);
                return _accountId;
            }
        }

        public string Number
        {
            get
            {
                CanReadProperty("Number", true);
                return _number;
            }
        }

        public string Description
        {
            get
            {
                CanReadProperty("Description", true);
                return _description;
            }
        }

        public string Type
        {
            get
            {
                CanReadProperty("Type", true);
                return _type;
            }
        }

        protected AccountType AccountType
        {
            get
            {
                return Account.GetAccountType(Type);
            }
        }

        public string SubType
        {
            get
            {
                CanReadProperty("Subtype", true);
                return _subType;
            }
        }

        public int ControlType
        {
            get
            {
                CanReadProperty("ControlType", true);
                return _controlType;
            }
        }

        public string Department
        {
            get
            {
                CanReadProperty("Department", true);
                return _department;
            }
        }

        public bool IsRetail
        {
            get
            {
                CanReadProperty("IsRetail", true);
                return _isRetail;
            }
            set
            {
                CanWriteProperty("IsRetail", true);

                if (IsRetail != value)
                {
                    _isRetail = value;

                    ValidationRules.CheckRules("IsWholesale");

                    PropertyHasChanged("IsRetail");
                }
            }
        }

        public bool IsWholesale
        {
            get
            {
                CanReadProperty("IsWholesale", true);
                return _isWholesale;
            }
            set
            {
                CanWriteProperty("IsWholesale", true);

                if (IsWholesale != value)
                {
                    _isWholesale = value;

                    ValidationRules.CheckRules("IsRetail");

                    PropertyHasChanged("IsWholesale");
                }
            }
        }

        public bool IsFrontEnd
        {
            get
            {
                CanReadProperty("IsFrontEnd", true);
                return _isFrontEnd;
            }
            set
            {
                CanWriteProperty("IsFrontEnd", true);

                if (IsFrontEnd != value)
                {
                    _isFrontEnd = value;

                    ValidationRules.CheckRules("IsFrontEnd");

                    PropertyHasChanged("IsFrontEnd");
                }
            }
        }

        protected override object GetIdValue()
        {
            return _accountId;
        }

        #endregion

        #region Authorization Rules

        private static readonly string[] PropertyNames = new string[]
            {
                "IsRetail", "IsWholesale", "IsFrontEnd", "AccountProfitTypeId",
            };

        protected override void AddAuthorizationRules()
        {
            foreach (string propertyName in PropertyNames)
            {
                AuthorizationRules.AllowWrite(propertyName, "DataManagementWriter");
            }
        }

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        public static bool CanAddObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        public static bool CanEditObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        public static bool CanDeleteObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        #endregion

        #region Validation Rules

        protected override void AddBusinessRules()
        {
            ValidationRules.AddRule(FinancialTransactionTypeRequired, "IsRetail");
            ValidationRules.AddRule(FinancialTransactionTypeRequired, "IsWholesale");
        }

        protected static bool FinancialTransactionTypeRequired(object target, RuleArgs ruleArgs)
        {
            bool isValid = false;
            DealerAccount account = target as DealerAccount;
            if (account != null)
            {
                switch (account.AccountType)
                {
                    case AccountType.Sales:
                        isValid = (account.IsRetail || account.IsWholesale)
                                  && !(account.IsRetail && account.IsWholesale)
                                  && !(account.IsWholesale && !account.IsFrontEnd);
                        if (!isValid)
                        {
                            ruleArgs.Description = string.Format("Account #{0} must have only one of IsRetail or IsWholesale selected, and may not have IsWholesale selected withough IsFrontEnd selected.", account.Number);
                        }
                        break;
                    default:
                        isValid = true;
                        break;
                }
            }
            return isValid;
        }

        #endregion

        #region Factory Methods

        internal static DealerAccount NewDealerAccount(Guid accountId)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view DealerAccount");

            return new DealerAccount(Account.GetAccount(accountId));
        }

        public static DealerAccount GetDealerAccount(IDataRecord record)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view DealerAccount");

            return new DealerAccount(record);
        }

        private DealerAccount()
        {
            MarkAsChild();
        }

        private DealerAccount(Account account)
        {
            MarkAsChild();
            _accountId = account.Id;
            _number = account.Number;
            _description = account.Description;
            _type = account.Type;
            _subType = account.Subtype;
            _controlType = account.ControlType;
            _department = account.Department;

            ValidationRules.CheckRules();
        }

        private DealerAccount(IDataRecord record)
        {
            MarkAsChild();
            Fetch(record);
            MarkOld();
            ValidationRules.CheckRules();
        }

        public override DealerAccount Save()
        {
            if (IsNew && !CanAddObject())
                throw new SecurityException("User not authorized to add DealerAccount");

            if (IsDeleted && !CanDeleteObject())
                throw new SecurityException("User not authorized to delete DealerAccount");

            if (!CanEditObject())
                throw new SecurityException("User not authorized to edit DealerAccount");

            return base.Save();
        }

        #endregion

        #region Data Access

        private void Fetch(IDataRecord record)
        {
            _accountId = record.GetGuid(record.GetOrdinal("AccountId"));
            _number = record.GetString(record.GetOrdinal("Number"));
            _description = record.GetString(record.GetOrdinal("Description"));
            _type = record.GetString(record.GetOrdinal("Type"));
            if (!record.IsDBNull(record.GetOrdinal("SubType")))
            {
                _subType = record.GetString(record.GetOrdinal("SubType"));
            }
            if (!record.IsDBNull(record.GetOrdinal("ControlType")))
            {
                _controlType = record.GetInt32(record.GetOrdinal("ControlType"));
            }
            if (!record.IsDBNull(record.GetOrdinal("Department")))
            {
                _department = record.GetString(record.GetOrdinal("Department"));
            }
            _isRetail = record.GetBoolean(record.GetOrdinal("IsRetail"));
            _isWholesale = record.GetBoolean(record.GetOrdinal("IsWholesale"));
            _isFrontEnd = record.GetBoolean(record.GetOrdinal("IsFrontEnd"));
        }

        internal void Insert(IDataConnection connection, IDbTransaction transaction, int dealerId, Environment environment)
        {
            if (!IsDirty) return;

            DoInsertUpdate(connection, transaction, "Configuration.DealerAccount#AddAssignment", dealerId, environment);

            MarkOld();
        }

        internal void Update(IDataConnection connection, IDbTransaction transaction, int dealerId, Environment environment)
        {
            if (!IsDirty) return;

            if (IsNew) return;

            DoInsertUpdate(connection, transaction, "Configuration.DealerAccount#Update", dealerId, environment);

            MarkOld();
        }

        internal void DeleteSelf(IDataConnection connection, IDbTransaction transaction, int dealerId, Environment environment)
        {
            if (!IsDirty) return;

            if (IsNew) return;

            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = "Configuration.DealerAccount#DeleteAssignment";
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;
                command.AddParameterWithValue("AccountId", DbType.Guid, false, _accountId);
                command.AddParameterWithValue("DealerId", DbType.Int32, false, dealerId);
                command.AddParameterWithValue("EnvironmentId", DbType.Int32, false, (int)environment);
                command.ExecuteNonQuery();
            }

            MarkNew();
        }

        private void DoInsertUpdate(IDataConnection connection, IDbTransaction transaction, string commandText, int dealerId, Environment environment)
        {
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = commandText;
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;

                command.AddParameterWithValue("AccountId", DbType.Guid, false, _accountId);
                command.AddParameterWithValue("DealerId", DbType.Int32, false, dealerId);
                command.AddParameterWithValue("EnvironmentId", DbType.Int32, false, (int)environment);
                command.AddParameterWithValue("IsRetail", DbType.Boolean, false, _isRetail);
                command.AddParameterWithValue("IsWholesale", DbType.Boolean, false, _isWholesale);
                command.AddParameterWithValue("IsFrontEnd", DbType.Boolean, false, _isFrontEnd);

                command.ExecuteNonQuery();
            }
        }
        
        #endregion
    }
}
