namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    public enum AccountType
    {
        Undefined  = 0,
        Asset      = 65,
        Cost       = 67,
        Expense    = 69,
        Income     = 73,
        Liability  = 76,
        Memo       = 77,
        N          = 78,
        Q          = 81,
        Sales      = 83,
        X          = 88
    }
}
