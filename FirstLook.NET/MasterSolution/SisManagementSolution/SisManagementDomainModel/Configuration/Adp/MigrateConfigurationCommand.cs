using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class MigrateConfigurationCommand : CommandBase
    {
        private int _dealerId;
        private Environment _source;
        private Environment _destination;

        public static void Execute(int dealerId, Environment source, Environment destination)
        {
            DataPortal.Execute<MigrateConfigurationCommand>(new MigrateConfigurationCommand(dealerId, source, destination));
        }

        private MigrateConfigurationCommand()
        { /* force factory methods */}

        private MigrateConfigurationCommand(int dealerId, Environment source, Environment destination)
        {
            this._dealerId = dealerId;
            this._source = source;
            this._destination = destination;
        }

        [Transactional(TransactionalTypes.Manual)]
        protected override void DataPortal_Execute()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.CommandText = "Configuration.DealerConfiguration#Copy";
                            command.Transaction = transaction;
                            command.AddParameterWithValue("DealerId", DbType.Int32, false, _dealerId);
                            command.AddParameterWithValue("SourceEnvironmentId", DbType.Int32, false, (int)_source);
                            command.AddParameterWithValue("DestinationEnvironmentId", DbType.Int32, false, (int)_destination);
                            command.ExecuteNonQuery();
                        }
                        transaction.Commit();
                    }
                    catch
                    {
                        try
                        {
                            transaction.Rollback();
                        }
                        catch
                        {
                            throw;
                        }
                        throw;
                    }
                }
            }
        }
    }
}
