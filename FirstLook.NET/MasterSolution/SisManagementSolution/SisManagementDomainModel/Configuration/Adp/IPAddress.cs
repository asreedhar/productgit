using System;
using System.Data;
using System.Globalization;
using Csla;
using Csla.Validation;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class IPAddress
    {
        private readonly byte[] bytes = new byte[4];

        public byte Byte1
        {
            get
            {
                return bytes[0];
            }
            set
            {
                if (Byte1 != value)
                {
                    bytes[0] = value;
                }
            }
        }

        public byte Byte2
        {
            get
            {
                return bytes[1];
            }
            set
            {
                if (Byte1 != value)
                {
                    bytes[1] = value;
                }
            }
        }

        public byte Byte3
        {
            get
            {
                return bytes[2];
            }
            set
            {
                if (Byte3 != value)
                {
                    bytes[2] = value;
                }
            }
        }

        public byte Byte4
        {
            get
            {
                return bytes[3];
            }
            set
            {
                if (Byte4 != value)
                {
                    bytes[3] = value;
                }
            }
        }

        public bool IsPrivateAddress
        {
            get
            {
                if ((Byte1 == 127) ||
                    (Byte1 == 192 && Byte2 == 168) ||
                    (Byte1 == 10) ||
                    (Byte1 == 172 && (Byte2 >= 16 && Byte2 < 32)))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public override string ToString()
        {
            return string.Format("{0:##0}.{1:##0}.{2:##0}.{3:##0}", bytes[0], bytes[1], bytes[2], bytes[3]);
        }

        private IPAddress(byte byte1, byte byte2, byte byte3, byte byte4)
        {
            bytes[0] = byte1;
            bytes[1] = byte2;
            bytes[2] = byte3;
            bytes[3] = byte4;
        }

        public static bool TryParse(string value, ref IPAddress address)
        {
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }

            string[] values = value.Split(new char[] { '.' });

            if (values.Length != 4)
            {
                return false;
            }

            return TryParse(values[0], values[1], values[2], values[3], ref address);
        }

        public static bool TryParse(string byte1, string byte2, string byte3, string byte4, ref IPAddress address)
        {
            bool success = true;

            byte b1, b2, b3, b4;

            success &= byte.TryParse(byte1, NumberStyles.Integer, CultureInfo.InvariantCulture, out b1);
            success &= byte.TryParse(byte2, NumberStyles.Integer, CultureInfo.InvariantCulture, out b2);
            success &= byte.TryParse(byte3, NumberStyles.Integer, CultureInfo.InvariantCulture, out b3);
            success &= byte.TryParse(byte4, NumberStyles.Integer, CultureInfo.InvariantCulture, out b4);

            if (success)
            {
                address = new IPAddress(b1, b2, b3, b4);
            }
            else
            {
                address = null;
            }

            return success;
        }

        public static bool IPAddressIsValid(object target, RuleArgs e)
        {
            IPAddress value = (IPAddress)Utilities.CallByName(target, e.PropertyName, CallType.Get);

            bool isValid = IPAddressIsValid(value);
            if (!isValid)
            {
                e.Description = string.Format("{0} is not a valid IP address", e.PropertyName);
            }
            return isValid;
        }

        public static bool IPAddressIsValid(IPAddress ipaddress)
        {
            if (ipaddress == null)
            {
                return true;
            }

            bool notZero = true;

            for (int i = 0; i < 4; i++)
            {
                notZero &= (ipaddress.bytes[i] == 0);
            }

            notZero = !notZero;

            if (notZero)
            {
                return true;
            }

            return false;
        }

        public static bool IPAddressIsPublic(object target, RuleArgs e)
        {
            IPAddress value = (IPAddress)Utilities.CallByName(target, e.PropertyName, CallType.Get);

            if (value == null)
            {
                return true;
            }

            if (value.IsPrivateAddress)
            {
                e.Description = string.Format("{0} is not a public IP address", e.PropertyName);

                return false;
            }

            return true;
        }

        internal static IPAddress GetIPAddress(IDataRecord record, string parameterName)
        {
            IPAddress address = null;

            if (!record.IsDBNull(record.GetOrdinal(parameterName)))
            {
                TryParse(record.GetString(record.GetOrdinal(parameterName)), ref address);
            }

            return address;
        }

        internal static void AddToCommand(IPAddress address, IDataCommand command, string parameterName)
        {
            if (address != null)
            {
                command.AddParameterWithValue(parameterName, DbType.String, false, address.ToString());
            }
            else
            {
                command.AddParameterWithValue(parameterName, DbType.String, true, DBNull.Value);
            }
        }
    }
}
