using System;
using System.Data;
using System.Globalization;
using System.Security;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class Account : ReadOnlyBase<Account>
    {
        #region Business Methods

        private Guid _id;
        private string _number;
        private string _description;
        private string _type;
        private string _subType;
        private int _controlType;
        private string _department;

        public Guid Id
        {
            get
            {
                CanReadProperty("Id", true);
                return _id;
            }
        }

        public string Number
        {
            get
            {
                CanReadProperty("Number", true);
                return _number;
            }
        }

        public string Description
        {
            get
            {
                CanReadProperty("Description", true);
                return _description;
            }
        }

        public string Type
        {
            get
            {
                CanReadProperty("Type", true);
                return _type;
            }
        }

        public string Subtype
        {
            get
            {
                CanReadProperty("Subtype", true);
                return _subType;
            }
        }

        public int ControlType
        {
            get
            {
                CanReadProperty("ControlType", true);
                return _controlType;
            }
        }

        public string Department
        {
            get
            {
                CanReadProperty("Department", true);
                return _department;
            }
        }

        protected override object GetIdValue()
        {
            return _id;
        }

        #endregion

        #region Authorization Rules

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Factory Methods

        public static Account GetAccount(Guid id)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view Account");

            return DataPortal.Fetch<Account>(new Criteria(id));
        }

        internal static Account GetAccount(IDataReader reader)
        {
            return new Account(reader);
        }

        private Account()
        {
            /* Force use orf factory methods */
        }

        private Account(IDataRecord record)
        {
            Fetch(record);
        }

        #endregion

        #region Data Access

        [Serializable]
        private class Criteria
        {
            private readonly Guid id;

            public Criteria(Guid id)
            {
                this.id = id;
            }

            public Guid Id
            {
                get { return id; }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "criteria")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Configuration.Account#Fetch";

                    command.AddParameterWithValue("Id", DbType.Guid, false, criteria.Id);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Fetch(reader);
                        }
                        else
                        {
                            throw new DataException("Schedule not found");
                        }
                    }
                }
            }
        }

        private void Fetch(IDataRecord record)
        {
            _id = record.GetGuid(record.GetOrdinal("Id"));
            _number = record.GetString(record.GetOrdinal("Number"));
            _description = record.GetString(record.GetOrdinal("Description"));
            _type = record.GetString(record.GetOrdinal("Type"));
            if (!record.IsDBNull(record.GetOrdinal("SubType")))
			{
            	_subType = record.GetString(record.GetOrdinal("SubType"));
			}
            if (!record.IsDBNull(record.GetOrdinal("ControlType")))
			{
            	_controlType = record.GetInt32(record.GetOrdinal("ControlType"));
			}
            if (!record.IsDBNull(record.GetOrdinal("Department")))
			{
	            _department = record.GetString(record.GetOrdinal("Department"));
			}
        }

        public static AccountType GetAccountType(string type)
        {
            if (type == null)
            {
                return AccountType.Undefined;
            }

            char chartype = Convert.ToChar(type.ToUpper(CultureInfo.InvariantCulture), CultureInfo.InvariantCulture);
            switch (chartype)
            {
                case 'A':
                    return AccountType.Asset;
                case 'C':
                    return AccountType.Cost;
                case 'E':
                    return AccountType.Expense;
                case 'I':
                    return AccountType.Income;
                case 'L':
                    return AccountType.Liability;
                case 'M':
                    return AccountType.Memo;
                case 'N':
                    return AccountType.N;
                case 'Q':
                    return AccountType.Q;
                case 'S':
                    return AccountType.Sales;
                case 'X':
                    return AccountType.X;
                default:
                    return AccountType.Undefined;
            }
        }

        #endregion
    }
}