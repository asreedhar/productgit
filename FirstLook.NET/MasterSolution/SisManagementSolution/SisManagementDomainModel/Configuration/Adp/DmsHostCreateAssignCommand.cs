using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class DmsHostCreateAssignCommand : CommandBase
    {
        private Dealer _dealer;
        private DmsHost _dmsHost;

        public static void Execute(Dealer dealer, DmsHost dmsHost)
        {
            DataPortal.Execute<DmsHostCreateAssignCommand>(new DmsHostCreateAssignCommand(dealer, dmsHost));
        }

        private DmsHostCreateAssignCommand()
        { /* force factory methods */}

        private DmsHostCreateAssignCommand(Dealer dealer, DmsHost dmsHost)
        {
            this._dealer = dealer;
            this._dmsHost = dmsHost;
        }

        [Transactional(TransactionalTypes.Manual)]
        protected override void DataPortal_Execute()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        if(_dmsHost.IsNew)
                            _dmsHost.DoInsert(connection, transaction);
                        else
                            _dmsHost.DoUpdate(connection, transaction);

                        _dealer.DmsHostId = _dmsHost.Id;
                        _dealer.DoUpdate(connection, transaction);
                        transaction.Commit(); // also disposes of the transaction and its resources
                    }
                    catch
                    {
                        try
                        {
                            transaction.Rollback();
                        }
                        catch
                        {
                            throw;
                        }
                        throw;
                    }
                }
            }
        }

    }
}
