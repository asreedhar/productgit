using System;
using System.Data;
using Csla;
using Csla.Core;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    public class ScheduleInfoCollection : ReadOnlyListBase<ScheduleInfoCollection, ScheduleInfo>, IReportTotalRowCount
    {
        #region IReportTotalRowCount Members

        private int totalRowCount = 0;

        public int TotalRowCount
        {
            get
            {
                return totalRowCount;
            }
        }

        #endregion

        #region Factory Methods

        public static ScheduleInfoCollection GetScheduleInfos(Guid dmsHostId, Guid sisConnectionId, string sortOrder, int maximumRows, int startRowIndex)
        {
            return DataPortal.Fetch<ScheduleInfoCollection>(new FetchCriteria(dmsHostId, sisConnectionId, new DataSourceSelectCriteria(sortOrder, maximumRows, startRowIndex)));
        }

        #endregion

        #region Data Access

        [Serializable]
        class FetchCriteria
        {
            private readonly Guid dmsHostId;
            private readonly Guid sisConnectionId;
            private readonly DataSourceSelectCriteria criteria;

            public FetchCriteria(Guid dmsHostId, Guid sisConnectionId, DataSourceSelectCriteria criteria)
            {
                this.dmsHostId = dmsHostId;
                this.sisConnectionId = sisConnectionId;
                this.criteria = criteria;
            }

            public Guid DmsHostId
            {
                get { return dmsHostId; }
            }

            public Guid SisConnectionId
            {
                get { return sisConnectionId; }
            }

            public DataSourceSelectCriteria Criteria
            {
                get { return criteria; }
            }
        }

        private void DataPortal_Fetch(FetchCriteria criteria)
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Configuration.ScheduleInfoCollection#Fetch";

                    command.AddParameterWithValue("DmsHostId", DbType.Guid, false, criteria.DmsHostId);
                    command.AddParameterWithValue("SisConnectionId", DbType.Guid, false, criteria.SisConnectionId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            totalRowCount = reader.GetInt32(reader.GetOrdinal("TotalRowCount"));

                            if (reader.NextResult())
                            {
                                IsReadOnly = false;

                                while (reader.Read())
                                {
                                    Add(ScheduleInfo.GetScheduleInfo(reader));
                                }

                                IsReadOnly = true;
                            }
                            else
                            {
                                throw new DataException("DataReader missing second of two result sets");
                            }
                        }
                        else
                        {
                            throw new DataException("DataReader missing first of two result sets");
                        }
                    }
                }
            }

            RaiseListChangedEvents = true;
        }

        #endregion
    }
}
