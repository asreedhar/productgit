using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class DealerLotLogonDataSplit : BusinessBase<DealerLotLogonDataSplit>
    {
        #region Business Methods

        private int id;
        private int dealerId;
        private Environment environment;
        private Guid logonId;
        private LotLogonDataSplit lotLogonDataSplit;

        public int Id
        {
            get
            {
                CanReadProperty("Id", true);
                return id;
            }
            set
            {
                CanWriteProperty("Id", true);
                id = value;
                PropertyHasChanged("Id");
            }
        }
        public int DealerId
        {
            get
            {
                CanReadProperty("DealerId", true);
                return dealerId;
            }
            set
            {
                CanWriteProperty("DealerId", true);
                dealerId = value;
                PropertyHasChanged("DealerId");
            }
        }
        public Environment Environment
        {
            get
            {
                CanReadProperty("Environment", true);
                return environment;
            }
            set
            {
                CanWriteProperty("Environment", true);
                environment = value;
                PropertyHasChanged("Environment");
            }
        }
        public Guid LogonId
        {
            get
            {
                CanReadProperty("LogonId", true);
                return logonId;
            }
            set
            {
                CanWriteProperty("LogonId", true);
                logonId = value;
                PropertyHasChanged("LogonId");
            }
        }
        public LotLogonDataSplit LotLogonDataSplit
        {
            get
            {
                CanReadProperty("LotLogonDataSplit", true);
                return lotLogonDataSplit;
            }
            set
            {
                CanWriteProperty("LotLogonDataSplit", true);
                lotLogonDataSplit = value;
                PropertyHasChanged("LotLogonDataSplit");
            }
        }

        protected override object GetIdValue()
        {
            return id;
        }

        #endregion

        #region Factory Methods

        internal static DealerLotLogonDataSplit GetDealerLotLogonDataSplit(IDataRecord record)
        {
            return new DealerLotLogonDataSplit(record);
        }

        public static DealerLotLogonDataSplit NewDealerLotLogonDataSplit()
        {
            return DataPortal.Create<DealerLotLogonDataSplit>();
        }

        private DealerLotLogonDataSplit()
        {

        }

        private DealerLotLogonDataSplit(IDataRecord record)
        {
            Fetch(record);
            MarkOld();
        }

        #endregion

        #region Authorization

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Data Access

        protected override void DataPortal_Create()
        {
            
        }

        protected void Fetch(IDataRecord record)
        {
            id = record.GetInt32(record.GetOrdinal("Id"));
            dealerId = record.GetInt32(record.GetOrdinal("DealerId"));
            environment = (Environment)record.GetInt32(record.GetOrdinal("EnvironmentId"));
            logonId = record.GetGuid(record.GetOrdinal("LogonId"));
            lotLogonDataSplit = Adp.LotLogonDataSplit.GetLotLogonDataSplit(record);
        }

        internal void Insert(IDataConnection connection, IDbTransaction transaction)
        {
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = "Configuration.DealerLotLogonDataSplit#Insert";
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;

                command.AddParameterWithValue("DealerId", DbType.Int32, false, dealerId);
                command.AddParameterWithValue("EnvironmentId", DbType.Int32, false, (int)environment);
                command.AddParameterWithValue("LogonId", DbType.Guid, false, logonId);
                command.AddParameterWithValue("LotNumber", DbType.String, false, lotLogonDataSplit.LotNumber);

                command.ExecuteNonQuery();
            }
            MarkOld();
        }

        internal void DeleteSelf(IDataConnection connection, IDbTransaction transaction)
        {
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = "Configuration.DealerLotLogonDataSplit#Delete";
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;
                command.AddParameterWithValue("Id", DbType.Int32, false, id);
                command.ExecuteNonQuery();
            }

            MarkNew();
        }

        #endregion
    }
}