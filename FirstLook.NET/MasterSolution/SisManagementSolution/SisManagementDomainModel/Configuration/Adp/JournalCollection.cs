using System;
using System.Data;
using System.Security;
using Csla;
using Csla.Core;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    public class JournalCollection : ReadOnlyListBase<JournalCollection, Journal>
    {
        #region Authorization Methods

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Factory Methods

        public static JournalCollection GetJournals(Guid accountingLogonId, string sortOrder)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view journals");

            if (string.IsNullOrEmpty(sortOrder))
                sortOrder = "Number";

            return DataPortal.Fetch<JournalCollection>(new FetchCriteria(accountingLogonId, sortOrder));
        }

        public static JournalCollection GetJournals(Guid accountingLogonId, string journalType, string sortOrder)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view journals");

            if (string.IsNullOrEmpty(sortOrder))
                sortOrder = "Number";

            return DataPortal.Fetch<JournalCollection>(new FetchCriteria(accountingLogonId, journalType, sortOrder));
        }

        #endregion

        #region Data Access

        [Serializable]
        class FetchCriteria
        {
            private readonly Guid accountingLogonId;
            private readonly string journalType;
            private readonly string sortOrder;

            public FetchCriteria(Guid accountingLogonId, string sortOrder)
            {
                this.accountingLogonId = accountingLogonId;
                this.sortOrder = sortOrder;
            }

            public FetchCriteria(Guid accountingLogonId, string journalType, string sortOrder)
            {
                this.accountingLogonId = accountingLogonId;
                this.sortOrder = sortOrder;
                this.journalType = journalType;
            }

            public Guid AccountingLogonId
            {
                get { return accountingLogonId; }
            }

            public string JournalType
            {
                get { return journalType; }
            }

            public string SortOrder
            {
                get { return sortOrder; }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "criteria")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchCriteria criteria)
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Configuration.JournalCollection#Fetch";

                    command.AddParameterWithValue("AccountingLogonId", DbType.Guid, false, criteria.AccountingLogonId);
                    command.AddParameterWithValue("JournalType", DbType.String, true, criteria.JournalType);
                    command.AddParameterWithValue("SortOrder", DbType.String, true, criteria.SortOrder);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        IsReadOnly = false;

                        while (reader.Read())
                        {
                            Add(Journal.GetJournal(reader));
                        }

                        IsReadOnly = true;
                    }
                }
            }

            RaiseListChangedEvents = true;
        }

        #endregion
    }
}
