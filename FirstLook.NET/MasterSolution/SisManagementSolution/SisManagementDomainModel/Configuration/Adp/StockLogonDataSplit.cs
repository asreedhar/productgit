using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class StockLogonDataSplit : BusinessBase<StockLogonDataSplit>
    {
        #region Business Methods

        private string stockPatternType;
        private string stockPatternText;

        public string StockPatternType
        {
            get
            {
                CanReadProperty("StockPatternType", true);
                return stockPatternType;
            }
            set
            {
                CanWriteProperty("StockPatternType", true);
                stockPatternType = value;
                PropertyHasChanged("StockPatternType");
            }
        }

        public string StockPatternText
        {
            get
            {
                CanReadProperty("StockPatternText", true);
                return stockPatternText;
            }
            set
            {
                CanWriteProperty("StockPatternText", true);
                stockPatternText = value;
                PropertyHasChanged("StockPatternText");
            }
        }

        protected override object GetIdValue()
        {
            return null;
        }

        #endregion

        #region Factory Methods

        internal static StockLogonDataSplit GetStockLogonDataSplit(IDataRecord record)
        {
            return new StockLogonDataSplit(record);
        }

        public static StockLogonDataSplit NewStockLogonDataSplit()
        {
            return DataPortal.Create<StockLogonDataSplit>();
        }

        private StockLogonDataSplit()
        {
            MarkAsChild();
        }

        private StockLogonDataSplit(IDataRecord record) : this()
        {
            Fetch(record);
            MarkOld();
        }

        #endregion

        #region Authorization

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Data Access

        protected override void DataPortal_Create()
        {
            
        }

        protected void Fetch(IDataRecord record)
        {
            stockPatternType = record.GetString(record.GetOrdinal("StockPatternType"));
            stockPatternText = record.GetString(record.GetOrdinal("StockPatternText"));
        }

        #endregion
    }
}