using System;
using System.Data;
using Csla;
using Csla.Core;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class SisConnectionInfoCollection : ReadOnlyListBase<SisConnectionInfoCollection, SisConnectionInfo>, IReportTotalRowCount
    {
        private int totalRowCount;

        #region IReportTotalRowCount Members

        public int TotalRowCount
        {
            get
            {
                return totalRowCount;
            }
        }

        #endregion

        public static SisConnectionInfoCollection GetSisConnections(Guid dmsHostId, Guid? accountingLogonId, Guid? financeLogonId, string sortOrder, int maximumRows, int startRowIndex)
        {
            if (maximumRows < 1)
                throw new ArgumentOutOfRangeException("maximumRows", maximumRows, "Must be greater than zero");

            if (startRowIndex < 0)
                throw new ArgumentOutOfRangeException("startRowIndex", startRowIndex, "Must be greater than zero");

            if (string.IsNullOrEmpty(sortOrder))
                sortOrder = "AccountingLogonName";

            return DataPortal.Fetch<SisConnectionInfoCollection>(new FetchCriteria(dmsHostId, accountingLogonId, financeLogonId, new DataSourceSelectCriteria(sortOrder, maximumRows, startRowIndex)));
        }

        private SisConnectionInfoCollection()
        {
        }

        [Serializable]
        class FetchCriteria
        {
            private readonly Guid dmsHostId;
            private readonly Guid? accountingLogonId;
            private readonly Guid? financeLogonId;
            private readonly DataSourceSelectCriteria criteria;

            public FetchCriteria(Guid dmsHostId, Guid? accountingLogonId, Guid? financeLogonId, DataSourceSelectCriteria criteria)
            {
                this.dmsHostId = dmsHostId;
                this.accountingLogonId = accountingLogonId;
                this.financeLogonId = financeLogonId;
                this.criteria = criteria;
            }

            public Guid DmsHostId
            {
                get { return dmsHostId; }
            }

            public Guid? AccountingLogonId
            {
                get { return accountingLogonId; }
            }

            public Guid? FinanceLogonId
            {
                get { return financeLogonId; }
            }

            public DataSourceSelectCriteria Criteria
            {
                get { return criteria; }
            }
        }

        private void DataPortal_Fetch(FetchCriteria criteria)
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Configuration.SisConnectionInfoCollection#Fetch";
                    command.AddParameterWithValue("DmsHostId", DbType.Guid, false, criteria.DmsHostId);
                    command.AddParameterWithValue("AccountingLogonId", DbType.Guid, true, criteria.AccountingLogonId);
                    command.AddParameterWithValue("FinanceLogonId", DbType.Guid, true, criteria.FinanceLogonId);

                    criteria.Criteria.AddToCommand(command);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            totalRowCount = reader.GetInt32(reader.GetOrdinal("TotalRowCount"));

                            if (reader.NextResult())
                            {
                                IsReadOnly = false;

                                while (reader.Read())
                                {
                                    Add(new SisConnectionInfo(reader));
                                }

                                IsReadOnly = true;
                            }
                            else
                            {
                                throw new DataException("DataReader missing second of two result sets");
                            }
                        }
                        else
                        {
                            throw new DataException("DataReader missing first of two result sets");
                        }
                    }
                }
            }

            RaiseListChangedEvents = true;
        }
    }
}
