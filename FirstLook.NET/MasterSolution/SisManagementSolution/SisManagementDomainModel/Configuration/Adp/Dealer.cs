using System;
using System.Data;
using System.Globalization;
using System.Security;
using Csla;
using Csla.Validation;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class Dealer : BusinessBase<Dealer>
    {
        #region Business Methods

        private byte[] version = new byte[8];

        private Environment environment;
        private int id;
        private string name;
        private Time businessHoursBegin;
        private Time businessHoursEnd;
        private TimeZone timeZone;
        private bool followsDaylightSavingsTime = true;
        private DealerSisConnectionCollection connections = DealerSisConnectionCollection.NewDealerSisConnectionCollection();
        private Guid dmsHostId = Guid.Empty;

        private bool executeDailyLoad;
        private bool useDealSalePrice;
        private bool useDealUnitCost;
        private bool useDealFEG;
        private bool useDealBEG;
        private bool calcSalePriceFromDeal;
        private bool useJournalVINMapping;

        public Environment Environment
        {
            get
            {
                CanReadProperty("Environment", true);

                return environment;
            }
        }
        public int Id
        {
            get
            {
                CanReadProperty("Id", true);

                return id;
            }
        }
        public string Name
        {
            get
            {
                CanReadProperty("Name", true);

                return name;
            }
        }
        public Time BusinessHoursBegin
        {
            get
            {
                CanReadProperty("BusinessHoursBegin", true);

                return businessHoursBegin;
            }
            set
            {
                CanWriteProperty("BusinessHoursBegin", true);

                if (BusinessHoursBegin != value)
                {
                    businessHoursBegin = value;

                    PropertyHasChanged("BusinessHoursBegin");
                }
            }
        }
        public Time BusinessHoursEnd
        {
            get
            {
                CanReadProperty("BusinessHoursEnd", true);

                return businessHoursEnd;
            }
            set
            {
                CanWriteProperty("BusinessHoursEnd", true);

                if (BusinessHoursEnd != value)
                {
                    businessHoursEnd = value;

                    PropertyHasChanged("BusinessHoursEnd");
                }
            }
        }
        public TimeZone TimeZone
        {
            get
            {
                CanReadProperty("TimeZone", true);

                return timeZone;
            }
            set
            {
                CanWriteProperty("TimeZone", true);

                if (!Equals(TimeZone, value))
                {
                    timeZone = value;

                    PropertyHasChanged("TimeZone");
                }
            }
        }
        public bool FollowsDaylightSavingsTime
        {
            get
            {
                CanReadProperty("FollowsDayLightSavingsTime", true);

                return followsDaylightSavingsTime;
            }
            set
            {
                CanWriteProperty("FollowsDayLightSavingsTime", true);

                if (FollowsDaylightSavingsTime != value)
                {
                    followsDaylightSavingsTime = value;

                    PropertyHasChanged("FollowsDayLightSavingsTime");
                }
            }
        }
        public DealerSisConnectionCollection Connections
        {
            get { return connections; }
        }
        public Guid DmsHostId
        {
            get
            {
                CanReadProperty("DmsHostId", true);

                return dmsHostId;
            }
            set
            {
                CanWriteProperty("DmsHostId", true);

                if (DmsHostId != value)
                {
                    dmsHostId = value;

                    PropertyHasChanged("DmsHostId");
                }
            }
        }

        public bool ExecuteDailyLoad
        {
            get
            {
                CanReadProperty("ExecuteDailyLoad", true);

                return executeDailyLoad;
            }
            set
            {
                CanWriteProperty("ExecuteDailyLoad", true);

                if (executeDailyLoad != value)
                {
                    executeDailyLoad = value;

                    PropertyHasChanged("ExecuteDailyLoad");
                }
            }
        }
        public bool UseDealSalePrice
        {
            get
            {
                CanReadProperty("UseDealSalePrice", true);

                return useDealSalePrice;
            }
            set
            {
                CanWriteProperty("UseDealSalePrice", true);

                if (useDealSalePrice != value)
                {
                    useDealSalePrice = value;
                    PropertyHasChanged("UseDealSalePrice");
                }
            }
        }
        public bool UseDealUnitCost
        {
            get
            {
                CanReadProperty("UseDealUnitCost", true);

                return useDealUnitCost;
            }
            set
            {
                CanWriteProperty("UseDealUnitCost", true);

                if (useDealUnitCost != value)
                {
                    useDealUnitCost = value;
                    PropertyHasChanged("UseDealUnitCost");
                }
            }
        }
        public bool UseDealFEG
        {
            get
            {
                CanReadProperty("UseDealFEG", true);
                return useDealFEG;
            }
            set
            {
                CanWriteProperty("UseDealFEG", true);

                if (useDealFEG != value)
                {
                    useDealFEG = value;
                    PropertyHasChanged("UseDealFEG");
                }
            }
        }
        public bool UseDealBEG
        {
            get
            {
                CanReadProperty("UseDealBEG", true);
                return useDealBEG;
            }
            set
            {
                CanWriteProperty("UseDealBEG", true);
                if (useDealBEG != value)
                {
                    useDealBEG = value;
                    PropertyHasChanged("UseDealBEG");
                }
            }
        }
        public bool CalcSalePriceFromDeal
        {
            get
            {
                CanReadProperty("CalcSalePriceFromDeal", true);
                return calcSalePriceFromDeal;
            }
            set
            {
                CanWriteProperty("CalcSalePriceFromDeal", true);
                if (calcSalePriceFromDeal != value)
                {
                    calcSalePriceFromDeal = value;
                    PropertyHasChanged("CalcSalePriceFromDeal");
                }
            }
        }
        public bool UseJournalVINMapping
        {
            get
            {
                CanReadProperty("UseJournalVINMapping", true);
                return useJournalVINMapping;
            }
            set
            {
                CanWriteProperty("UseJournalVINMapping", true);
                if (useJournalVINMapping != value)
                {
                    useJournalVINMapping = value;
                    PropertyHasChanged("UseJournalVINMapping");
                }
            }
        }
        

        protected override object GetIdValue()
        {
            return id;
        }

        #endregion

        #region Authorization Rules

        private static readonly string[] PropertyNames = new string[]
            {
                "BusinessHoursBegin", "BusinessHoursEnd", "TimeZone"
            };

        protected override void AddAuthorizationRules()
        {
            foreach (string propertyName in PropertyNames)
            {
                AuthorizationRules.AllowWrite(propertyName, "DataManagementWriter");
            }
        }

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        public static bool CanAddObject()
        {
            return false; // no new dealers from this application (yet)
        }

        public static bool CanEditObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        #endregion

        #region Validation Rules

        public override bool IsValid
        {
            get
            {
                return (base.IsValid && connections.IsValid);
            }
        }

        public override bool IsDirty
        {
            get
            {
                return (base.IsDirty || connections.IsDirty);
            }
        }

        protected override void AddBusinessRules()
        {
            ValidationRules.AddRule(Adp.TimeZone.TimeZoneRequired,
                "TimeZone");

            ValidationRules.AddRule(Time.TimeRequired,
                "BusinessHoursBegin");

            ValidationRules.AddRule(Time.TimeIsValid,
                "BusinessHoursBegin");

            ValidationRules.AddRule(Time.TimeRequired,
                "BusinessHoursEnd");

            ValidationRules.AddRule(Time.TimeIsValid,
                "BusinessHoursEnd");

            ValidationRules.AddRule<Dealer>(StartDateGTEndDate,
                "BusinessHoursBegin");

            ValidationRules.AddRule<Dealer>(StartDateGTEndDate,
                "BusinessHoursEnd");

            ValidationRules.AddRule<Dealer>(ValidBusinessHours,
                "BusinessHoursBegin");

            ValidationRules.AddRule<Dealer>(ValidBusinessHours,
                "BusinessHoursEnd");

            ValidationRules.AddDependantProperty(
                "BusinessHoursBegin",
                "BusinessHoursEnd",
                false);
        }

        private static bool StartDateGTEndDate<T>(T target, RuleArgs e) where T : Dealer
        {
            if (target.businessHoursBegin == null || target.businessHoursEnd == null)
            {
                return true;
            }

            if (target.businessHoursBegin.CompareTo(target.businessHoursEnd) > 0)
            {
                e.Description = "Start time can't be after end time";

                return false;
            }
            else
            {
                return true;
            }
        }

        private static bool ValidBusinessHours<T>(T target, RuleArgs ruleArgs) where T : Dealer
        {
            if (target.businessHoursBegin == null || target.businessHoursEnd == null)
            {
                return true;
            }

            if (target.businessHoursBegin.TimeUntil(target.businessHoursEnd).Hour < 8)
            {
                ruleArgs.Description = "Dealer must be open for 8 hours a day.";

                return false;
            }
            else
            {
                return true;
            }
        }

        public static bool DealerRequired(object target, RuleArgs ruleArgs)
        {
            if (target == null)
            {
                ruleArgs.Description = string.Format(CultureInfo.InvariantCulture, "{0} required", ruleArgs.PropertyName);

                return false;
            }

            return true;
        }

        #endregion

        #region Factory Methods

        public static Dealer GetDealer(int id, Environment environment)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view dealer");

            return DataPortal.Fetch<Dealer>(new Criteria(id, environment));
        }

        public override Dealer Save()
        {
            if (IsNew && !CanAddObject())
                throw new SecurityException("User not authorized to add Dealer");

            if (!CanEditObject())
                throw new SecurityException("User not authorized to edit Dealer");

            return base.Save();
        }

        #endregion

        #region Data Access

        [Serializable]
        class Criteria
        {
            private readonly Environment environment;
            private readonly int id;

            public Criteria(int id, Environment environment)
            {
                this.id = id;
                this.environment = environment;
            }

            internal Environment Environment
            {
                get { return environment; }
            }

            internal int Id
            {
                get { return id; }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called by CSLA using reflection")]
        [Transactional(TransactionalTypes.Manual)]
        private void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Configuration.Dealer#Fetch";
                    command.AddParameterWithValue("Id", DbType.Int32, true, criteria.Id);
                    command.AddParameterWithValue("EnvironmentId", DbType.Int32, true, (int) criteria.Environment);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            environment = criteria.Environment;
                            id = reader.GetInt32(reader.GetOrdinal("Id"));
                            name = reader.GetString(reader.GetOrdinal("Name"));
                            businessHoursBegin = Time.GetTime(reader, "BusinessHoursBegin");
                            businessHoursEnd = Time.GetTime(reader, "BusinessHoursEnd");
                            timeZone = TimeZone.GetTimeZone(reader, "TimeZone");


                            if (!reader.IsDBNull(reader.GetOrdinal("ExecuteDailyLoad")))
                            {
                                executeDailyLoad = reader.GetBoolean(reader.GetOrdinal("ExecuteDailyLoad"));
                            }

                            if (!reader.IsDBNull(reader.GetOrdinal("UseDealSalePrice")))
                            {
                                useDealSalePrice = reader.GetBoolean(reader.GetOrdinal("UseDealSalePrice"));
                            }
                            if (!reader.IsDBNull(reader.GetOrdinal("UseDealUnitCost")))
                            {
                                useDealUnitCost = reader.GetBoolean(reader.GetOrdinal("UseDealUnitCost"));
                            }
                            if (!reader.IsDBNull(reader.GetOrdinal("UseDealFEG")))
                            {
                                useDealFEG = reader.GetBoolean(reader.GetOrdinal("UseDealFEG"));
                            }
                            if (!reader.IsDBNull(reader.GetOrdinal("UseDealBEG")))
                            {
                                useDealBEG = reader.GetBoolean(reader.GetOrdinal("UseDealBEG"));
                            }
                            if (!reader.IsDBNull(reader.GetOrdinal("CalcSalePriceFromDeal")))
                            {
                                calcSalePriceFromDeal = reader.GetBoolean(reader.GetOrdinal("CalcSalePriceFromDeal"));
                            }
                            if (!reader.IsDBNull(reader.GetOrdinal("UseJournalVINMapping")))
                            {
                                useJournalVINMapping = reader.GetBoolean(reader.GetOrdinal("UseJournalVINMapping"));
                            }


                            if (!reader.IsDBNull(reader.GetOrdinal("FollowsDaylightSavingsTime")))
                            {
                                followsDaylightSavingsTime = reader.GetBoolean(reader.GetOrdinal("FollowsDaylightSavingsTime"));
                            }
                            
                            if (!reader.IsDBNull(reader.GetOrdinal("Version")))
                            {
                                reader.GetBytes(reader.GetOrdinal("Version"), 0, version, 0, 8);
                            }
                            
                            if (reader.NextResult())
                            {
                                if (reader.Read())
                                {
                                    if (!reader.IsDBNull(reader.GetOrdinal("HostId")))
                                    {
                                        dmsHostId = reader.GetGuid(reader.GetOrdinal("HostId"));
                                    }
                                }

                                if (reader.NextResult())
                                {
                                    connections = DealerSisConnectionCollection.GetDealerSisConnectionCollection(reader);
                                }
                                else
                                {
                                    throw new DataException("Missing DealerSisConnectionCollection data-set");
                                }
                            }
                            else
                            {
                                throw new DataException("Missing HostId data-set");
                            }
                        }
                        else
                        {
                            throw new DataException("Dealer not found");
                        }
                    }
                }
            }

            ValidationRules.CheckRules();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called by CSLA using reflection")]
        [Transactional(TransactionalTypes.Manual)]
        protected override void DataPortal_Update()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();
                
                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        DoUpdate(connection, transaction);
                        transaction.Commit();
                    }
                    catch
                    {
                        try
                        {
                            transaction.Rollback();
                        }
                        catch
                        {
                            throw;
                        }
                        throw;
                    }
                }
            }
        }

        internal void DoUpdate(IDataConnection connection, IDbTransaction transaction)
        {
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "Configuration.Dealer#Update";
                command.Transaction = transaction;

                command.AddParameterWithValue("Id", DbType.Int32, false, id);
                command.AddParameterWithValue("EnvironmentId", DbType.Int32, true, (int)environment);

                if (dmsHostId == Guid.Empty)
                    command.AddParameterWithValue("HostId", DbType.Guid, true, null);
                else
                    command.AddParameterWithValue("HostId", DbType.Guid, true, dmsHostId);

                command.AddParameterWithValue("ExecuteDailyLoad", DbType.Boolean, true, executeDailyLoad);

                command.AddParameterWithValue("UseDealSalePrice", DbType.Boolean, true, useDealSalePrice);
                command.AddParameterWithValue("UseDealUnitCost", DbType.Boolean, true, useDealUnitCost);
                command.AddParameterWithValue("UseDealFEG", DbType.Boolean, true, useDealFEG);
                command.AddParameterWithValue("UseDealBEG", DbType.Boolean, true, useDealBEG);
                command.AddParameterWithValue("CalcSalePriceFromDeal", DbType.Boolean, true, calcSalePriceFromDeal);
                command.AddParameterWithValue("UseJournalVINMapping", DbType.Boolean, true, useJournalVINMapping);

                
                businessHoursBegin.AddToCommand(command, "BusinessHoursBegin");
                businessHoursEnd.AddToCommand(command, "BusinessHoursEnd");
                TimeZone.AddToCommand(timeZone, command, "TimeZone");
                command.AddParameterWithValue("FollowsDaylightSavingsTime", DbType.Boolean, false, followsDaylightSavingsTime);

                Database.AddArrayParameter(command, "Version", DbType.Binary, false, version);

                IDataParameter newVersion = Database.AddArrayOutParameter(command, "NewVersion", DbType.Binary, false, 8);

                command.ExecuteNonQuery();

                version = (byte[])newVersion.Value;

                connections.Update(connection, transaction, this);
            }   
        }

        #endregion
    }
}