using System;
using System.Data;
using System.Security;
using Csla;
using Csla.Validation;
using FirstLook.Common.Data;
using FirstLook.Sis.Management.DomainModel.Configuration.Adp;
using TimeZone=System.TimeZone;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class DealerStockLogonDataSplitCollection : BusinessListBase<DealerStockLogonDataSplitCollection, DealerStockLogonDataSplit>
    {
        #region Business Methods

        public void Assign(DealerStockLogonDataSplit dealerStockLogonDataSplit)
        {
            if (!Contains(dealerStockLogonDataSplit.StockLogonDataSplit.StockPatternText, dealerStockLogonDataSplit.StockLogonDataSplit.StockPatternType))
            {
                Add(dealerStockLogonDataSplit);
            }
        }

        public void Remove(string StockPatternText, string StockPatternType)
        {
            foreach (DealerStockLogonDataSplit item in this)
            {
                if (item.StockLogonDataSplit.StockPatternText == StockPatternText && item.StockLogonDataSplit.StockPatternType == StockPatternType)
                {
                    Remove(item);
                    break;
                }
            }
        }

        public bool Contains(string StockPatternText, string StockPatternType)
        {
            foreach (DealerStockLogonDataSplit item in this)
            {
                if (item.StockLogonDataSplit.StockPatternText == StockPatternText && item.StockLogonDataSplit.StockPatternType == StockPatternType)
                {
                    return true;
                }
            }
            return false;
        }

        #endregion

        #region Factory Methods

        internal static DealerStockLogonDataSplitCollection NewDealerStockLogonDataSplitCollection()
        {
            return new DealerStockLogonDataSplitCollection();
        }

        internal static DealerStockLogonDataSplitCollection GetDealerStockLogonDataSplitCollection(IDataReader dr)
        {
            return new DealerStockLogonDataSplitCollection(dr);
        }

        private DealerStockLogonDataSplitCollection()
        {

        }

        private DealerStockLogonDataSplitCollection(IDataReader dr)
        {
            Fetch(dr);
        }

        #endregion

        #region Data Access

        // called to load data from the database
        private void Fetch(IDataReader reader)
        {
            RaiseListChangedEvents = false;
            while (reader.Read())// && !reader.IsDBNull(reader.GetOrdinal("SisConnectionId")))
            {
                Add(DealerStockLogonDataSplit.GetDealerStockLogonDataSplit(reader));
            }
            RaiseListChangedEvents = true;
        }

        internal void Update(IDataConnection connection, IDbTransaction transaction)
        {
            RaiseListChangedEvents = false;

            // add/update any current child objects
            foreach (DealerStockLogonDataSplit obj in this)
            {
                if (obj.IsDeleted)
                {
                    obj.DeleteSelf(connection, transaction);
                }
                else if (obj.IsNew)
                {
                    obj.Insert(connection, transaction);
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion
    }
}