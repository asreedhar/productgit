using System;
using System.Data;
using Csla;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class DmsHostInfo : ReadOnlyBase<DmsHostInfo>
    {
        #region Business Methods

        private readonly Guid id;
        private readonly IPAddress ipAddress;
        private readonly string name;
        private readonly TelephoneNumber telephoneNumber1;
        private readonly TelephoneNumber telephoneNumber2;
        private readonly TimeZone timeZone;

        public Guid Id
        {
            get
            {
                CanReadProperty("Id", true);

                return id;
            }
        }

        public IPAddress IPAddress
        {
            get
            {
                CanReadProperty("IPAddress", true);

                return ipAddress;
            }
        }

        public string Name
        {
            get
            {
                CanReadProperty("Name", true);

                return name;
            }
        }

        public TelephoneNumber TelephoneNumber1
        {
            get
            {
                CanReadProperty("TelephoneNumber1", true);

                return telephoneNumber1;
            }
        }

        public TelephoneNumber TelephoneNumber2
        {
            get
            {
                CanReadProperty("TelephoneNumber2", true);

                return telephoneNumber2;
            }
        }

        public TimeZone TimeZone
        {
            get
            {
                CanReadProperty("TimeZone", true);

                return timeZone;
            }
        }

        protected override object GetIdValue()
        {
            return id;
        }

        #endregion

        internal DmsHostInfo(IDataRecord record)
        {
            id = record.GetGuid(record.GetOrdinal("Id"));
            ipAddress = IPAddress.GetIPAddress(record, "IPAddress");
            name = record.GetString(record.GetOrdinal("Name"));
            telephoneNumber1 = TelephoneNumber.GetTelephoneNumber(record, "TelephoneNumber1");
            telephoneNumber2 = TelephoneNumber.GetTelephoneNumber(record, "TelephoneNumber2");
            timeZone = TimeZone.GetTimeZone(record, "TimeZone");
        }
    }
}