using System;
using System.Collections.Generic;
using System.Data;
using System.Security;
using Csla;
using Csla.Core;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class DealerAccountCollection : BusinessListBase<DealerAccountCollection, DealerAccount>
    {
        #region Business Methods

        private int _dealerId;
        private Environment _environment;

        public DealerAccount GetItem(Guid accountId)
        {
            foreach (DealerAccount account in this)
                if (account.AccountId.Equals(accountId))
                    return account;
            return null;
        }

        public void Assign(Guid accountId)
        {
            if (!Contains(accountId))
            {
                if (!ContainsDeleted(accountId))
                {
                    Add(DealerAccount.NewDealerAccount(accountId));
                }
                else
                {
                    DealerAccount deleted = GetDeletedItem(accountId);
                    Add(deleted);
                    DeletedList.Remove(deleted);
                }
            }
            else
            {
                throw new InvalidOperationException("Account already assigned to Dealer");
            }
        }

        public void Remove(Guid accountId)
        {
            foreach (DealerAccount account in this)
            {
                if (account.AccountId.Equals(accountId))
                {
                    Remove(account);
                    break;
                }
            }
        }

        public bool Contains(Guid accountId)
        {
            foreach (DealerAccount account in this)
                if (account.AccountId.Equals(accountId))
                    return true;
            return false;
        }

        public bool ContainsDeleted(Guid accountId)
        {
            foreach (DealerAccount account in DeletedList)
                if (account.AccountId.Equals(accountId))
                    return true;
            return false;
        }

        private DealerAccount GetDeletedItem(Guid accountId)
        {
            foreach (DealerAccount account in DeletedList)
                if (account.AccountId.Equals(accountId))
                    return account;
            return null;
        }

        #endregion

        #region Authorization Methods

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Factory Methods

        internal static DealerAccountCollection GetDealerAccounts(int dealerId, Environment environment, Guid accountingLogonId, AccountType accountType)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to retrieve DealerAccounts");

            return DataPortal.Fetch<DealerAccountCollection>(new FetchCriteria(dealerId, environment, accountingLogonId, accountType));
        }

        private DealerAccountCollection()
        {
            MarkAsChild();
        }

        #endregion

        #region Data Access

        [Serializable]
        private class FetchCriteria
        {
            private readonly int dealerId;
            private readonly Environment environment;
            private readonly Guid accountingLogonId;
            private readonly AccountType accountType;

            public FetchCriteria(int dealerId, Environment environment, Guid accountingLogonId, AccountType accountType)
            {
                this.dealerId = dealerId;
                this.environment = environment;
                this.accountingLogonId = accountingLogonId;
                this.accountType = accountType;
            }

            public Guid AccountingLogonId
            {
                get { return accountingLogonId; }
            }

            public int DealerId
            {
                get { return dealerId; }
            }

            public Environment Environment
            {
                get { return environment; }
            }

            public AccountType AccountType
            {
                get { return accountType; }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called by CSLA using reflection")]
        private void DataPortal_Fetch(FetchCriteria criteria)
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Configuration.DealerAccountCollection#Fetch";

                    command.AddParameterWithValue("DealerId", DbType.Int32, false, criteria.DealerId);
                    command.AddParameterWithValue("EnvironmentId", DbType.Int32, false, (int)criteria.Environment);
                    command.AddParameterWithValue("AccountingLogonId", DbType.Guid, false, criteria.AccountingLogonId);
                    command.AddParameterWithValue("AccountTypeId", DbType.String, true, (int?)criteria.AccountType);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Add(DealerAccount.GetDealerAccount(reader));
                        }

                        _dealerId = criteria.DealerId;
                        _environment = criteria.Environment;
                    }
                }
            }
            RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Let the DealerAccountChart handle the transaction
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="dealerId"></param>
        /// <param name="environment"></param>
        internal void Update(IDataConnection connection, IDbTransaction transaction, int dealerId, Environment environment)
        {
            this.RaiseListChangedEvents = false;

            foreach (DealerAccount item in DeletedList)
            {
                item.DeleteSelf(connection, transaction, _dealerId, _environment);
            }
            DeletedList.Clear();

            // add/update any current child objects
            foreach (DealerAccount item in this)
            {
                if (item.IsNew)
                {
                    item.Insert(connection, transaction, _dealerId, _environment);
                }
                else
                {
                    item.Update(connection, transaction, _dealerId, _environment);
                }
            }

            this.RaiseListChangedEvents = true;
        }

        #endregion
    }
}
