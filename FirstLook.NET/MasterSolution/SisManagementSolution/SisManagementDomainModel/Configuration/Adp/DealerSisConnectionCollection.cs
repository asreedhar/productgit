using System;
using System.Collections.Generic;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    /// <summary>
    /// The collection of the mappings between Dealers and SisConnection.
    /// </summary>
    [Serializable]
    public class DealerSisConnectionCollection : BusinessListBase<DealerSisConnectionCollection, DealerSisConnection>
    {
        #region Business Methods

        public DealerSisConnection GetItem(Guid sisConnectionId)
        {
            foreach (DealerSisConnection connection in this)
                if (connection.SisConnectionId == sisConnectionId)
                    return connection;
            return null;
        }

        public void Assign(SisConnection sisConnection)
        {
            if (!Contains(sisConnection.Id))
            {
                Add(DealerSisConnection.NewDealerSisConnection(sisConnection));
            }
            else
            {
                throw new InvalidOperationException("Connection already assigned to Dealer");
            }
        }


        public void RemoveAll()
        {
            List<DealerSisConnection> coll = new List<DealerSisConnection>(this);
            foreach (DealerSisConnection connection in coll)
            {
                Remove(connection);
            }
        }

        public void Remove(Guid sisConnectionId)
        {
            foreach (DealerSisConnection connection in this)
            {
                if (connection.SisConnectionId == sisConnectionId)
                {
                    Remove(connection);
                    break;
                }
            }
        }

        public bool Contains(Guid sisConnectionId)
        {
            foreach (DealerSisConnection connection in this)
                if (connection.SisConnectionId == sisConnectionId)
                    return true;
            return false;
        }

        public bool ContainsDeleted(Guid sisConnectionId)
        {
            foreach (DealerSisConnection connection in DeletedList)
                if (connection.SisConnectionId == sisConnectionId)
                    return true;
            return false;
        }

        #endregion

        #region Factory Methods

        internal static DealerSisConnectionCollection NewDealerSisConnectionCollection()
        {
            return new DealerSisConnectionCollection();
        }

        internal static DealerSisConnectionCollection GetDealerSisConnectionCollection(IDataReader dr)
        {
            return new DealerSisConnectionCollection(dr);
        }

        private DealerSisConnectionCollection()
        {
            MarkAsChild();
        }

        private DealerSisConnectionCollection(IDataReader dr) : this()
        {
            Fetch(dr);
        }

        #endregion

        #region Data Access

        // called to load data from the database
        private void Fetch(IDataReader reader)
        {
            RaiseListChangedEvents = false;
            while (reader.Read() && !reader.IsDBNull(reader.GetOrdinal("SisConnectionId")))
            {
                Add(DealerSisConnection.GetDealerSisConnection(reader));
            }
            RaiseListChangedEvents = true;
        }

        internal void Update(IDataConnection connection, IDbTransaction transaction, Dealer dealer)
        {
            RaiseListChangedEvents = false;
            // update (thus deleting) any deleted child objects
            foreach (DealerSisConnection obj in DeletedList)
                obj.DeleteSelf(connection, transaction, dealer);
            // now that they are deleted, remove them from memory too
            DeletedList.Clear();
            // add/update any current child objects
            foreach (DealerSisConnection obj in this)
            {
                if (obj.IsNew)
                {
                    obj.Insert(connection, transaction, dealer);
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion
    }
}
