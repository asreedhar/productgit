using System;
using System.Data;
using System.Security;
using Csla;
using Csla.Core;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class ScheduleCollection : ReadOnlyListBase<ScheduleCollection, Schedule>
    {
        #region Authorization Methods

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Factory Methods

        public static ScheduleCollection GetSchedules(Guid accountingLogonId)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view schedules");

            return DataPortal.Fetch<ScheduleCollection>(new FetchCriteria(accountingLogonId));
        }

        #endregion

        #region Data Access

        [Serializable]
        class FetchCriteria
        {
            private readonly Guid accountingLogonId;

            public FetchCriteria(Guid accountingLogonId)
            {
                this.accountingLogonId = accountingLogonId;
            }

            public Guid AccountingLogonId
            {
                get { return accountingLogonId; }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "criteria")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchCriteria criteria)
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Configuration.ScheduleCollection#Fetch";

                    command.AddParameterWithValue("AccountingLogonId", DbType.Guid, false, criteria.AccountingLogonId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        IsReadOnly = false;

                        while (reader.Read())
                        {
                            Add(Schedule.GetSchedule(reader));
                        }

                        IsReadOnly = true;
                    }
                }
            }

            RaiseListChangedEvents = true;
        }

        #endregion
    }
}
