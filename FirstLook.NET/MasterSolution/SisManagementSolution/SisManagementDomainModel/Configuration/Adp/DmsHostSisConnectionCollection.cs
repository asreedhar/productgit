using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class DmsHostSisConnectionCollection : BusinessListBase<DmsHostSisConnectionCollection,SisConnection>
    {
        #region Business Methods

        public SisConnection GetItem(Guid sisConnectionId)
        {
            foreach (SisConnection res in this)
                if (res.Id == sisConnectionId)
                    return res;
            return null;
        }

        public void Remove(Guid sisConnectionId)
        {
            foreach (SisConnection res in this)
            {
                if (res.Id == sisConnectionId)
                {
                    Remove(res);
                    break;
                }
            }
        }

        public bool Contains(Guid sisConnectionId)
        {
            foreach (SisConnection res in this)
                if (res.Id == sisConnectionId)
                    return true;
            return false;
        }

        public bool ContainsDeleted(Guid sisConnectionId)
        {
            foreach (SisConnection res in DeletedList)
                if (res.Id == sisConnectionId)
                    return true;
            return false;
        }

        #endregion

        #region Factory Methods

        internal static DmsHostSisConnectionCollection NewDmsHostSisConnections()
        {
            return new DmsHostSisConnectionCollection();
        }

        internal static DmsHostSisConnectionCollection GetDmsHostSisConnections(IDataReader reader)
        {
            return new DmsHostSisConnectionCollection(reader);
        }

        private DmsHostSisConnectionCollection()
        {
            MarkAsChild();
        }

        private DmsHostSisConnectionCollection(IDataReader record)
            : this()
        {
            Fetch(record);
        }

        #endregion

        #region Data Access

        private void Fetch(IDataReader reader)
        {
            RaiseListChangedEvents = false;
            while (reader.Read())
            {
                Add(SisConnection.GetSisConnection(reader));
            }
            RaiseListChangedEvents = true;
        }

        internal void Update(IDataConnection connection, IDbTransaction transaction, DmsHost dmsHost)
        {
            RaiseListChangedEvents = false;
            // update (thus deleting) any deleted child objects
            foreach (SisConnection obj in DeletedList)
                obj.DeleteSelf(connection, transaction, dmsHost);
            // now that they are deleted, remove them from memory too
            DeletedList.Clear();
            // add/update any current child objects
            foreach (SisConnection obj in this)
            {
                if (obj.IsNew)
                    obj.Insert(connection, transaction, dmsHost);
                else
                    obj.Update(connection, transaction, dmsHost);
            }
            RaiseListChangedEvents = true;
        }

        #endregion
    }
}
