using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class CompanyLogonDataSplit : BusinessBase<CompanyLogonDataSplit>
    {
        #region Business Methods

        private string dmsCompanyId;

        public string DmsCompanyId
        {
            get
            {
                CanReadProperty("DmsCompanyId", true);
                return dmsCompanyId;
            }
            set
            {
                CanWriteProperty("DmsCompanyId", true);
                dmsCompanyId = value;
                PropertyHasChanged("DmsCompanyId");
            }
        }

        protected override object GetIdValue()
        {
            return null;
        }

        #endregion

        #region Factory Methods

        internal static CompanyLogonDataSplit GetCompanyLogonDataSplit(IDataRecord record)
        {
            return new CompanyLogonDataSplit(record);
        }

        public static CompanyLogonDataSplit NewCompanyLogonDataSplit()
        {
            return DataPortal.Create<CompanyLogonDataSplit>();
        }

        private CompanyLogonDataSplit()
        {
            MarkAsChild();
        }

        private CompanyLogonDataSplit(IDataRecord record) : this()
        {
            Fetch(record);
            MarkOld();
        }


        #endregion

        #region Authorization

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Data Access

        protected override void DataPortal_Create()
        {
            
        }

        protected void Fetch(IDataRecord record)
        {
            dmsCompanyId = record.GetString(record.GetOrdinal("DmsCompanyId"));
        }

        #endregion
    }
}