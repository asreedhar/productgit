using System;
using System.Data;
using System.Globalization;
using System.Security;
using System.Text.RegularExpressions;
using Csla;
using Csla.Validation;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class Logon : BusinessBase<Logon>
    {
        #region Business Methods

        private byte[] version = new byte[8];

        private Guid id;
        private LogonType logonType;
        private string name;
        private bool? usesLogonDataSplitting;

        public Guid Id
        {
            get
            {
                CanReadProperty("Id", true);

                return id;
            }
        }
        public LogonType LogonType
        {
            get
            {
                CanReadProperty("LogonType", true);

                return logonType;
            }
            set
            {
                CanWriteProperty("LogonType", true);

                if (!Equals(LogonType, value))
                {
                    logonType = value;

                    PropertyHasChanged("LogonType");
                }
            }
        }
        public string Name
        {
            get
            {
                CanReadProperty("Name", true);

                return name;
            }
            set
            {
                CanWriteProperty("Name", true);

                if (!Equals(Name, value))
                {
                    name = value;

                    PropertyHasChanged("Name");
                }
            }
        }
        public bool? UsesLogonDataSplitting
        {
            get
            {
                CanReadProperty("UsesLogonDataSplitting", true);
                return usesLogonDataSplitting;
            }
            set
            {
                CanWriteProperty("UsesLogonDataSplitting", true);
                usesLogonDataSplitting = value;
                PropertyHasChanged("UsesLogonDataSplitting");
                MarkDirty();
            }
        }


        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture, Name);
        }

        protected override object GetIdValue()
        {
            return id;
        }

        #endregion

        #region Authorization Rules

        private static readonly string[] PropertyNames = new string[]
            {
                "LogonType", "Name", "UsesLogonDataSplitting"
            };

        protected override void AddAuthorizationRules()
        {
            foreach (string propertyName in PropertyNames)
            {
                AuthorizationRules.AllowWrite(propertyName, "DataManagementWriter");
            }
        }

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        public static bool CanAddObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        public static bool CanEditObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        #endregion

        #region Validation Rules

        protected override void AddBusinessRules()
        {
            ValidationRules.AddRule(CommonRules.StringRequired,
                "Name");

            ValidationRules.AddRule(CommonRules.StringMaxLength,
                new CommonRules.MaxLengthRuleArgs("Name", 50));

            ValidationRules.AddRule(Logon.IsValidLogonName, "Name");

            ValidationRules.AddRule<Logon>(ValidLogonType,
                "LogonType");

            
        }

        //this method could probably be global somewhere.
        public static bool IsValidLogonName(object target, RuleArgs e)
        {
            bool isValid;
            if (target == null)
            {
                e.Description = string.Format(CultureInfo.InvariantCulture, "{0} is required.", e.PropertyName);
                isValid = false;
            }
            else
            {
                Regex regex = new Regex("^\\w*\\-(A|FI)");

                isValid = regex.IsMatch((string)Utilities.CallByName(target, e.PropertyName, CallType.Get));
                if (!isValid)
                {
                    e.Description = string.Format(CultureInfo.InvariantCulture, "{0} contains non alphanumeric characters.", e.PropertyName);
                }
            }
            return isValid;
        }

        public static bool LogonRequired(object target, RuleArgs e)
        {
            if (target == null)
            {
                e.Description = "Logon Required";

                return false;
            }

            return true;
        }

        private static bool ValidLogonType<T>(T target, RuleArgs e) where T : Logon
        {
            if (target.logonType == LogonType.Undefined)
            {
                e.Description = "Logon Type is undefined";

                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion

        #region Factory Methods

        public static Logon NewLogon()
        {
            return DataPortal.Create<Logon>();
        }

        internal static Logon GetLogon(IDataRecord record, string prefix)
        {
            return new Logon(record, prefix);
        }

        private Logon()
        {
            MarkAsChild();
        }

        private Logon(IDataRecord record, string prefix)
            : this()
        {
            Fetch(record, prefix);
            MarkOld();
        }

        public override Logon Save()
        {
            if (IsNew && !CanAddObject())
                throw new SecurityException("User not authorized to add Dealer");

            if (!CanEditObject())
                throw new SecurityException("User not authorized to edit Dealer");

            return base.Save();
        }

        #endregion

        #region Data Access

        [Serializable]
        class Criteria
        {
            private readonly Guid id;

            public Criteria(Guid id)
            {
                this.id = id;
            }

            public Guid Id
            {
                get { return id; }
            }
        }

        [RunLocal]
        protected override void DataPortal_Create()
        {
            id = Guid.NewGuid();
        }

        private void Fetch(IDataRecord record, string prefix)
        {
            id = record.GetGuid(record.GetOrdinal(prefix + "Id"));
            logonType = (LogonType)Enum.ToObject(typeof(LogonType), record.GetInt32(record.GetOrdinal(prefix + "LogonType")));
            name = record.GetString(record.GetOrdinal(prefix + "Name"));
            if (!record.IsDBNull(record.GetOrdinal(prefix + "UsesLogonDataSplitting")))
            {
                usesLogonDataSplitting = record.GetBoolean(record.GetOrdinal(prefix + "UsesLogonDataSplitting"));
            }

            if (!record.IsDBNull(record.GetOrdinal(prefix + "Version")))
            {
                record.GetBytes(record.GetOrdinal(prefix + "Version"), 0, version, 0, 8);
            }

            MarkOld();
        }

        internal void Insert(IDataConnection connection, IDbTransaction transaction, DmsHost dmsHost)
        {
            if (!IsDirty) return;

            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = "Configuration.Logon#Insert";
                command.Transaction = transaction;
                DoInsertUpdate(command, dmsHost, false);
            }
        }

        internal void Update(IDataConnection connection, IDbTransaction transaction, DmsHost dmsHost)
        {
            if (!IsDirty) return;

            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = "Configuration.Logon#Update";
                command.Transaction = transaction;
                DoInsertUpdate(command, dmsHost, true);
            }
        }

        internal void DeleteSelf(IDataConnection connection, IDbTransaction transaction, DmsHost dmsHost)
        {
            if (!IsDirty) return;

            if (IsNew) return;

            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = "Configuration.Logon#Delete";
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;
                command.AddParameterWithValue("Id", DbType.Guid, false, id);
                Database.AddArrayParameter(command, "Version", DbType.Binary, false, version);
                command.ExecuteNonQuery();
            }

            MarkNew();
        }

        private void DoInsertUpdate(IDataCommand command, DmsHost dmsHost, bool isUpdate)
        {
            IDataParameter newVersion = Database.AddArrayOutParameter(command, "NewVersion", DbType.Binary, false, 8);
            command.CommandType = CommandType.StoredProcedure;
            command.AddParameterWithValue("Id", DbType.Guid, false, id);
            command.AddParameterWithValue("DmsHostId", DbType.Guid, false, dmsHost.Id);
            command.AddParameterWithValue("LogonType", DbType.Int32, false, (int) logonType);
            command.AddParameterWithValue("Name", DbType.String, false, name);
            command.AddParameterWithValue("UsesLogonDataSplitting", DbType.Boolean, true, usesLogonDataSplitting);
            if (isUpdate)
            {
                Database.AddArrayParameter(command, "Version", DbType.Binary, false, version);
            }
            command.ExecuteNonQuery();
            version = (byte[])newVersion.Value;
        }

        #endregion
    }
}
