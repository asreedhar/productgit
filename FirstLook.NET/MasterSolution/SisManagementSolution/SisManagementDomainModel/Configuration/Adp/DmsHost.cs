using System;
using System.Data;
using System.Security;
using Csla;
using Csla.Validation;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class DmsHost : BusinessBase<DmsHost>
    {
        #region Business Methods

        private byte[] version = new byte[8];

        private Guid id;
        private string name;
        private Credential credential;
        
        private Time backupBegin;
        private Time backupEnd;
        private TimeZone timeZone;
        private bool followsDaylightSavingsTime = true;

        private int businessHoursConnectionLimit;
        private int offHoursConnectionLimit;

        private IPAddress ipAddress;
        private TelephoneNumber dialUpTelephoneNumber1;
        private TelephoneNumber dialUpTelephoneNumber2;

        private ContactInformation contactInformation = ContactInformation.NewContactInformation();
        private DmsHostLogonCollection logons = DmsHostLogonCollection.NewDmsHostLogonCollection();
        private DmsHostSisConnectionCollection sisConnections = DmsHostSisConnectionCollection.NewDmsHostSisConnections();

        public Guid Id
        {
            get
            {
                CanReadProperty("Id", true);

                return id;
            }
        }

        public Time BackupBegin
        {
            get
            {
                CanReadProperty("BackupBegin", true);

                return backupBegin;
            }
            set
            {
                CanWriteProperty("BackupBegin", true);

                if (BackupBegin != value)
                {
                    backupBegin = value;

                    PropertyHasChanged("BackupBegin");
                }
            }
        }

        public Time BackupEnd
        {
            get
            {
                CanReadProperty("BackupEnd", true);

                return backupEnd;
            }
            set
            {
                CanWriteProperty("BackupEnd", true);

                if (BackupEnd != value)
                {
                    backupEnd = value;

                    PropertyHasChanged("BackupEnd");
                }
            }
        }

        public string Name
        {
            get
            {
                CanReadProperty("Name", true);

                return name;
            }
            set
            {
                CanWriteProperty("Name", true);

                if (!Equals(Name, value))
                {
                    name = value;

                    PropertyHasChanged("Name");
                }
            }
        }

        public IPAddress IPAddress
        {
            get
            {
                CanReadProperty("IPAddress", true);

                return ipAddress;
            }
            set
            {
                CanWriteProperty("IPAddress", true);

                if (!Equals(IPAddress, value))
                {
                    ipAddress = value;

                    PropertyHasChanged("IPAddress");
                }
            }
        }

        public TelephoneNumber DialUpTelephoneNumber1
        {
            get
            {
                CanReadProperty("DialUpTelephoneNumber1", true);

                return dialUpTelephoneNumber1;
            }
            set
            {
                CanWriteProperty("DialUpTelephoneNumber1", true);

                if (DialUpTelephoneNumber1 != value)
                {
                    dialUpTelephoneNumber1 = value;

                    PropertyHasChanged("DialUpTelephoneNumber1");
                }
            }
        }

        public TelephoneNumber DialUpTelephoneNumber2
        {
            get
            {
                CanReadProperty("DialUpTelephoneNumber2", true);

                return dialUpTelephoneNumber2;
            }
            set
            {
                CanWriteProperty("DialUpTelephoneNumber2", true);

                if (DialUpTelephoneNumber2 != value)
                {
                    dialUpTelephoneNumber2 = value;

                    PropertyHasChanged("DialUpTelephoneNumber2");
                }
            }
        }

        public int BusinessHoursConnectionLimit
        {
            get
            {
                CanReadProperty("BusinessHoursConnectionLimit", true);

                return businessHoursConnectionLimit;
            }
            set
            {
                CanWriteProperty("BusinessHoursConnectionLimit", true);

                if (BusinessHoursConnectionLimit != value)
                {
                    businessHoursConnectionLimit = value;

                    PropertyHasChanged("BusinessHoursConnectionLimit");
                }
            }
        }
        
        public int OffHoursConnectionLimit
        {
            get
            {
                CanReadProperty("OffHoursConnectionLimit", true);

                return offHoursConnectionLimit;
            }
            set
            {
                CanWriteProperty("OffHoursConnectionLimit", true);

                if (OffHoursConnectionLimit != value)
                {
                    offHoursConnectionLimit = value;

                    PropertyHasChanged("OffHoursConnectionLimit");
                }
            }
        }

        public Credential Credential
        {
            get
            {
                CanReadProperty("Credential", true);

                return credential;
            }
            set
            {
                CanWriteProperty("Credential", true);

                if (!Equals(Credential, value))
                {
                    credential = value;

                    PropertyHasChanged("Credential");
                }
            }
        }
        public ContactInformation ContactInformation
        {
            get
            {
                CanReadProperty("ContactInformation", true);

                return contactInformation;
            }
        }

        public DmsHostLogonCollection Logons
        {
            get { return logons; }
        }

        public TimeZone TimeZone
        {
            get
            {
                CanReadProperty("TimeZone", true);

                return timeZone;
            }
            set
            {
                CanWriteProperty("TimeZone", true);

                if (!Equals(TimeZone, value))
                {
                    timeZone = value;

                    PropertyHasChanged("TimeZone");
                }
            }
        }

        public bool FollowsDaylightSavingsTime
        {
            get
            {
                CanReadProperty("FollowsDaylightSavingsTime", true);

                return followsDaylightSavingsTime;
            }
            set
            {
                CanWriteProperty("FollowsDaylightSavingsTime", true);

                if (FollowsDaylightSavingsTime != value)
                {
                    followsDaylightSavingsTime = value;

                    PropertyHasChanged("FollowsDaylightSavingsTime");
                }
            }
        }

        public DmsHostSisConnectionCollection SisConnections
        {
            get { return sisConnections; }
        }

        protected override object GetIdValue()
        {
            return id;
        }

        #endregion

        #region Validation Rules

        public override bool IsValid
        {
            get
            {
                ValidationRules.CheckRules();
                return (
                    base.IsValid &&
                    (credential == null || credential.IsValid) &&
                    contactInformation.IsValid &&
                    logons.IsValid &&
                    sisConnections.IsValid);
            }
        }

        public override bool IsDirty
        {
            get
            {
                return (
                    base.IsDirty ||
                    (credential != null && credential.IsDirty) ||
                    contactInformation.IsDirty ||
                    logons.IsDirty ||
                    sisConnections.IsDirty);
            }
        }

        protected override void AddBusinessRules()
        {
            ValidationRules.AddRule(CommonRules.StringRequired, "Name");

            ValidationRules.AddRule(CommonRules.StringMaxLength,
                new CommonRules.MaxLengthRuleArgs("Name", 255));

            ValidationRules.AddRule(CommonRules.IntegerMinValue,
                new CommonRules.IntegerMinValueRuleArgs("BusinessHoursConnectionLimit", 1));

            ValidationRules.AddRule(CommonRules.IntegerMaxValue,
                new CommonRules.IntegerMaxValueRuleArgs("BusinessHoursConnectionLimit", 10));

            ValidationRules.AddRule(CommonRules.IntegerMinValue,
                new CommonRules.IntegerMinValueRuleArgs("OffHoursConnectionLimit", 1));

            ValidationRules.AddRule(CommonRules.IntegerMaxValue,
                new CommonRules.IntegerMaxValueRuleArgs("OffHoursConnectionLimit", 10));

            ValidationRules.AddRule(IPAddress.IPAddressIsValid,
                "IPAddress");

            //ValidationRules.AddRule(IPAddress.IPAddressIsPublic,
            //    "IPAddress");

            ValidationRules.AddRule<DmsHost>(IPAddressOrDialUpPhoneNumberRequired,
                "IPAddress");

            ValidationRules.AddRule(TelephoneNumber.TelephoneNumberIsValid,
                "DialUpTelephoneNumber1");

            ValidationRules.AddRule<DmsHost>(IPAddressOrDialUpPhoneNumberRequired,
                "DialUpTelephoneNumber1");

            ValidationRules.AddRule(TelephoneNumber.TelephoneNumberIsValid,
                "DialUpTelephoneNumber2");

            ValidationRules.AddRule(Time.TimeRequired,
                "BackupBegin");

            ValidationRules.AddRule(Time.TimeIsValid,
                "BackupBegin");

            ValidationRules.AddRule(Time.TimeRequired,
                "BackupEnd");

            ValidationRules.AddRule(Time.TimeIsValid,
                "BackupEnd");

            ValidationRules.AddRule<DmsHost>(ValidBackupHours,
                "BackupBegin");

            ValidationRules.AddRule<DmsHost>(ValidBackupHours,
                "BackupEnd");

            ValidationRules.AddDependantProperty(
                "BackupBegin",
                "BackupEnd",
                false);

            ValidationRules.AddRule(TimeZone.TimeZoneRequired,
                "TimeZone");

            ValidationRules.AddRule<DmsHost>(ValidSisConnections, 
                "SisConnections");

        }

        private static bool ValidBackupHours<T>(T target, RuleArgs e) where T : DmsHost
        {
            if (target.backupBegin == null || target.backupEnd == null)
            {
                return true;
            }

            Time diff = target.backupBegin.TimeUntil(target.backupEnd);

            int minutes = diff.Hour*60 + diff.Minute;

            if (minutes < 15)
            {
                e.Description = "Backup window must be at least 15 minutes";

                return false;
            }
            else
            {
                return true;
            }
        }

        private static bool ValidSisConnections<T>(T target, RuleArgs e) where T : DmsHost
        {
            bool isValid = target.SisConnections.IsValid;

            DmsHostSisConnectionCollection connections = target.SisConnections;

            int maxSize = connections.Count;
            for (int i = 0; i < maxSize; i++)
            {
                Logon aLogon1 = connections[i].AccountingLogon;
                Logon fiLogon1 = connections[i].FinanceLogon;
                for (int j = i + 1; j < maxSize; j++)
                {
                    Logon aLogon2 = connections[j].AccountingLogon;
                    Logon fiLogon2 = connections[j].FinanceLogon;

                    if (aLogon1.Name.Equals(aLogon2.Name)
                        && fiLogon1.Name.Equals(fiLogon2.Name))
                    {
                        //found a duplicate, not valid!
                        e.Description = "Found multiple connections with the same -A Logon and -FI Logon pair.  Please choose another -A Logon and -FI Logon name pair.";
                        return false;
                    }
                }
            }

            if (!isValid)
            {
                e.Description = "DmsHost does not accept this SisConnection.";
            }
            return isValid;
        }

        private static bool IPAddressOrDialUpPhoneNumberRequired<T>(T target, RuleArgs e) where T : DmsHost
        {
            bool isValid = target.IPAddress != null || target.DialUpTelephoneNumber1 != null;
            if (!isValid)
            {
                e.Description = "Either IPAddress or Dial-up Phone # 1 must be present";
            }
            return isValid;
        }

        #endregion

        #region Authorization Rules

        private static readonly string[] PropertyNames = new string[]
            {
                "Name", "IPAddress", "Credential",
                "DialUpTelephoneNumber1", "DialUpTelephoneNumber2",
                "TimeZone", "BackupBegin", "BackupEnd",
                "BusinessHoursConnectionLimit", "OffHoursConnectionLimit",
            };

        protected override void AddAuthorizationRules()
        {
            foreach (string propertyName in PropertyNames)
            {
                AuthorizationRules.AllowWrite(propertyName, "DataManagementWriter");
            }
        }

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        public static bool CanAddObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        public static bool CanEditObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        #endregion

        #region Factory Methods

        public static DmsHost GetDmsHost(Guid id)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view DMS host"); 

            return DataPortal.Fetch<DmsHost>(new Criteria(id));
        }

        public static DmsHost NewDmsHost()
        {
            if (!CanAddObject())
                throw new SecurityException("User not authorized to add DMS host");

            return DataPortal.Create<DmsHost>();
        }

        public override DmsHost Save()
        {
            if (IsNew && !CanAddObject())
                throw new SecurityException("User not authorized to add DMS host");

            if (!CanEditObject())
                throw new SecurityException("User not authorized to edit DMS host");

            return base.Save();
        }

        private DmsHost()
        {
            /* force use of factory methods */
        }

        #endregion

        #region Data Access

        [Serializable]
        class Criteria
        {
            private readonly Guid id;

            public Criteria(Guid id)
            {
                this.id = id;
            }

            public Guid Id
            {
                get { return id; }
            }
        }

        [RunLocal]
        protected override void DataPortal_Create()
        {
            id = Guid.NewGuid();
            businessHoursConnectionLimit = 1;
            offHoursConnectionLimit = 1;
            ValidationRules.CheckRules();
        }

        private void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Configuration.DmsHost#Fetch";
                    command.AddParameterWithValue("Id", DbType.Guid, true, criteria.Id);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            #region Read Properties

                            backupBegin = Time.GetTime(reader, "BackupBegin");
                            backupEnd = Time.GetTime(reader, "BackupEnd");
                            businessHoursConnectionLimit = reader.GetInt32(reader.GetOrdinal("BusinessHoursConnectionLimit"));
                            dialUpTelephoneNumber1 = TelephoneNumber.GetTelephoneNumber(reader, "DialUpTelephoneNumber1");
                            dialUpTelephoneNumber2 = TelephoneNumber.GetTelephoneNumber(reader, "DialUpTelephoneNumber2");
                            id = reader.GetGuid(reader.GetOrdinal("Id"));
                            ipAddress = IPAddress.GetIPAddress(reader, "IPAddress");
                            name = reader.GetString(reader.GetOrdinal("Name"));
                            offHoursConnectionLimit = reader.GetInt32(reader.GetOrdinal("OffHoursConnectionLimit"));
                            timeZone = TimeZone.GetTimeZone(reader, "TimeZone");

                            if (!reader.IsDBNull(reader.GetOrdinal("FollowsDaylightSavingsTime")))
                            {
                                followsDaylightSavingsTime = reader.GetBoolean(reader.GetOrdinal("FollowsDaylightSavingsTime"));
                            }

                            if (!reader.IsDBNull(reader.GetOrdinal("Version")))
                            {
                                reader.GetBytes(reader.GetOrdinal("Version"), 0, version, 0, 8);
                            }

                            #endregion

                            #region Read Credential

                            if (reader.NextResult())
                            {
                                if (reader.Read())
                                {
                                    credential = Credential.GetCredential(reader, "Credential");
                                }
                            }
                            else
                            {
                                throw new DataException("Missing Credential result set");
                            }

                            #endregion

                            #region Read Contact Information

                            if (reader.NextResult())
                            {
                                if (reader.Read())
                                {
                                    contactInformation = new ContactInformation(reader);
                                }
                                else
                                {
                                    throw new DataException("Empty ContactInformation result set");
                                }
                            }
                            else
                            {
                                throw new DataException("Missing ContactInformation result set");
                            }

                            #endregion

                            #region Read Logons

                            if (reader.NextResult())
                            {
                                logons = DmsHostLogonCollection.GetDmsHostLogonCollection(reader);
                            }
                            else
                            {
                                throw new DataException("Missing Logons result set");
                            }

                            #endregion

                            #region Read SisConnections

                            if (reader.NextResult())
                            {
                                sisConnections = DmsHostSisConnectionCollection.GetDmsHostSisConnections(reader);
                            }
                            else
                            {
                                throw new DataException("Missing SisConnections result set");
                            }

                            #endregion
                        }
                        else
                        {
                            throw new DataException("Empty DmsHost result set");
                        }
                    }
                }
            }
            ValidationRules.CheckRules();
        }

        [Transactional(TransactionalTypes.Manual)]
        protected override void DataPortal_Insert()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        DoInsert(connection, transaction);
                        transaction.Commit(); // also disposes of the transaction and its resources
                    }
                    catch
                    {
                        try
                        {
                            transaction.Rollback();
                        }
                        catch
                        {
                            throw;
                        }
                        throw;
                    }
                }
            }

        }

        [Transactional(TransactionalTypes.Manual)]
        protected override void DataPortal_Update()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        DoUpdate(connection, transaction);
                        transaction.Commit(); // also disposes of the transaction and its resources
                    }
                    catch
                    {
                        try
                        {
                            transaction.Rollback();
                        }
                        catch
                        {
                            throw;
                        }
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Performs an insert update given a connection and transaction.
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="commandText"></param>
        /// <param name="transaction"></param>
        /// <param name="isUpdate"></param>
        internal void DoInsert(IDataConnection connection, IDbTransaction transaction)
        {
            DoInsertUpdate(connection, "Configuration.DmsHost#Insert", transaction, false);
        }

        internal void DoUpdate(IDataConnection connection, IDbTransaction transaction)
        {
            DoInsertUpdate(connection, "Configuration.DmsHost#Update", transaction, true);
        }

        private void DoInsertUpdate(IDataConnection connection, string commandText, IDbTransaction transaction, bool isUpdate)
        {
            if (credential != null)
            {
                if (credential.IsNew)
                {
                    credential.Insert(connection, transaction);
                }
                else
                {
                    credential.Update(connection, transaction);
                }
            }

            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = commandText;
                command.Transaction = transaction;

                IDataParameter newVersion = Database.AddArrayOutParameter(command, "NewVersion", DbType.Binary, false, 8);

                command.AddParameterWithValue("Id", DbType.Guid, false, id);
                command.AddParameterWithValue("Name", DbType.String, false, name);
                command.AddParameterWithValue("BusinessHoursConnectionLimit", DbType.Int32, false, businessHoursConnectionLimit);
                command.AddParameterWithValue("OffHoursConnectionLimit", DbType.Int32, false, offHoursConnectionLimit);

                if (credential != null)
                {
                    command.AddParameterWithValue("CredentialId", DbType.Guid, true, credential.Id);
                }
                else
                {
                    command.AddParameterWithValue("CredentialId", DbType.Guid, true, DBNull.Value);
                }

                IPAddress.AddToCommand(ipAddress, command, "IPAddress");
                TelephoneNumber.AddToCommand(dialUpTelephoneNumber1, command, "DialUpTelephoneNumber1");
                TelephoneNumber.AddToCommand(dialUpTelephoneNumber2, command, "DialUpTelephoneNumber2");

                backupBegin.AddToCommand(command, "BackupBegin");
                backupEnd.AddToCommand(command, "BackupEnd");
                TimeZone.AddToCommand(timeZone, command, "TimeZone");

                command.AddParameterWithValue("FollowsDaylightSavingsTime", DbType.Boolean, false, followsDaylightSavingsTime);

                if (isUpdate)
                {
                    Database.AddArrayParameter(command, "Version", DbType.Binary, false, version);
                }

                command.ExecuteNonQuery();

                version = (byte[])newVersion.Value;
            }

            if (contactInformation.IsNew)
            {
                contactInformation.Insert(connection, transaction, this);
            }
            else
            {
                contactInformation.Update(connection, transaction, this);
            }

            logons.Update(connection, transaction, this);

            sisConnections.Update(connection, transaction, this);
        }

        #endregion
    }
}