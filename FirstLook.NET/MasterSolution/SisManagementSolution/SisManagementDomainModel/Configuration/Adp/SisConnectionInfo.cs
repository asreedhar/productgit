using System;
using System.Data;
using Csla;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class SisConnectionInfo : ReadOnlyBase<SisConnectionInfo>
    {
        #region Business Methods

        private string accountingLogonName;
        private string financeLogonName;
        private Guid id;
        private int numberOfAssociatedDealers;

        public Guid Id
        {
            get
            {
                CanReadProperty("Id", true);

                return id;
            }
        }

        public string AccountingLogonName
        {
            get
            {
                CanReadProperty("AccountingLogonName", true);

                return accountingLogonName;
            }
        }

        public string FinanceLogonName
        {
            get
            {
                CanReadProperty("FinanceLogonName", true);

                return financeLogonName;
            }
        }

        public int NumberOfAssociatedDealers
        {
            get
            {
                CanReadProperty("NumberOfAssociatedDealers", true);

                return numberOfAssociatedDealers;
            }
        }

        protected override object GetIdValue()
        {
            return id;
        }

        #endregion

        internal SisConnectionInfo(IDataRecord record)
        {
            Fetch(record);
        }

        private void Fetch(IDataRecord record)
        {
            id = record.GetGuid(record.GetOrdinal("Id"));
            accountingLogonName = record.GetString(record.GetOrdinal("AccountingLogonName"));
            financeLogonName = record.GetString(record.GetOrdinal("FinanceLogonName"));
            numberOfAssociatedDealers = record.GetInt32(record.GetOrdinal("NumberOfAssociatedDealers"));
        }
    }
}