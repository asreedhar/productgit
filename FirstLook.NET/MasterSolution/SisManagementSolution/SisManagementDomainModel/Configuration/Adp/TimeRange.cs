using System;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class TimeRange
    {
        private readonly Time begin;
        private readonly Time end;

        public Time Begin
        {
            get { return begin; }
        }

        public Time End
        {
            get { return end; }
        }

        public TimeRange(Time begin, Time end)
        {
            this.begin = begin;
            this.end = end;
        }
    }
}
