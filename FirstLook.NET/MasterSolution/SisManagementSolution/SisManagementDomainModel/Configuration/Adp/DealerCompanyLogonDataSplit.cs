using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class DealerCompanyLogonDataSplit : BusinessBase<DealerCompanyLogonDataSplit>
    {
        #region Business Methods

        private int id;
        private int dealerId;
        private Environment environment;
        private Guid logonId;
        private CompanyLogonDataSplit companyLogonDataSplit;

        public int Id
        {
            get
            {
                CanReadProperty("Id", true);
                return id;
            }
            set
            {
                CanWriteProperty("Id", true);
                id = value;
                PropertyHasChanged("Id");
            }
        }
        public int DealerId
        {
            get
            {
                CanReadProperty("DealerId", true);
                return dealerId;
            }
            set
            {
                CanWriteProperty("DealerId", true);
                dealerId = value;
                PropertyHasChanged("DealerId");
            }
        }
        public Environment Environment
        {
            get
            {
                CanReadProperty("Environment", true);
                return environment;
            }
            set
            {
                CanWriteProperty("Environment", true);
                environment = value;
                PropertyHasChanged("Environment");
            }
        }
        public Guid LogonId
        {
            get
            {
                CanReadProperty("LogonId", true);
                return logonId;
            }
            set
            {
                CanWriteProperty("LogonId", true);
                logonId = value;
                PropertyHasChanged("LogonId");
            }
        }
        public CompanyLogonDataSplit CompanyLogonDataSplit
        {
            get
            {
                CanReadProperty("CompanyLogonDataSplit", true);
                return companyLogonDataSplit;
            }
            set
            {
                CanWriteProperty("CompanyLogonDataSplit", true);
                companyLogonDataSplit = value;
                PropertyHasChanged("CompanyLogonDataSplit");
            }
        }

        protected override object GetIdValue()
        {
            return id;
        }

        #endregion

        #region Factory Methods

        internal static DealerCompanyLogonDataSplit GetDealerCompanyLogonDataSplit(IDataRecord record)
        {
            return new DealerCompanyLogonDataSplit(record);
        }

        public static DealerCompanyLogonDataSplit NewDealerCompanyLogonDataSplit()
        {
            return DataPortal.Create<DealerCompanyLogonDataSplit>();
        }

        private DealerCompanyLogonDataSplit()
        {

        }

        private DealerCompanyLogonDataSplit(IDataRecord record)
        {
            Fetch(record);
            MarkOld();
        }

        #endregion

        #region Authorization

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Data Access

        protected override void DataPortal_Create()
        {
            
        }

        protected void Fetch(IDataRecord record)
        {
            id = record.GetInt32(record.GetOrdinal("Id"));
            dealerId = record.GetInt32(record.GetOrdinal("DealerId"));
            environment = (Environment)record.GetInt32(record.GetOrdinal("EnvironmentId"));
            logonId = record.GetGuid(record.GetOrdinal("LogonId"));
            companyLogonDataSplit = Adp.CompanyLogonDataSplit.GetCompanyLogonDataSplit(record);
        }


        internal void Insert(IDataConnection connection, IDbTransaction transaction)
        {
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = "Configuration.DealerCompanyLogonDataSplit#Insert";
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;

                command.AddParameterWithValue("DealerId", DbType.Int32, false, dealerId);
                command.AddParameterWithValue("EnvironmentId", DbType.Int32, false, (int)environment);
                command.AddParameterWithValue("LogonId", DbType.Guid, false, logonId);
                command.AddParameterWithValue("DMSCompanyId", DbType.String, false, companyLogonDataSplit.DmsCompanyId);

                command.ExecuteNonQuery();
            }
            MarkOld();
        }

        internal void DeleteSelf(IDataConnection connection, IDbTransaction transaction)
        {
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = "Configuration.DealerCompanyLogonDataSplit#Delete";
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;
                command.AddParameterWithValue("Id", DbType.Int32, false, id);
                command.ExecuteNonQuery();
            }

            MarkNew();
        }

        #endregion
    }
}