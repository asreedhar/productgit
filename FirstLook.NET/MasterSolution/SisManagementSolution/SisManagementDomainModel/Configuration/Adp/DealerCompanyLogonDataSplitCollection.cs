using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;
using FirstLook.Sis.Management.DomainModel.Configuration.Adp;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class DealerCompanyLogonDataSplitCollection : BusinessListBase<DealerCompanyLogonDataSplitCollection, DealerCompanyLogonDataSplit>
    {
        #region Business Methods

        public void Assign(DealerCompanyLogonDataSplit dealerCompanyLogonDataSplit)
        {
            if (!Contains(dealerCompanyLogonDataSplit.CompanyLogonDataSplit.DmsCompanyId))
            {
                Add(dealerCompanyLogonDataSplit);
            }
        }

        public void Remove(string DmsCompanyId)
        {
            foreach (DealerCompanyLogonDataSplit item in this)
            {
                if (item.CompanyLogonDataSplit.DmsCompanyId == DmsCompanyId)
                {
                    Remove(item);
                    break;
                }
            }
        }

        public bool Contains(string DmsCompanyId)
        {
            foreach (DealerCompanyLogonDataSplit item in this)
            {
                if (item.CompanyLogonDataSplit.DmsCompanyId == DmsCompanyId)
                {
                    return true;
                }
            }
            return false;
        }

        #endregion

        #region Factory Methods

        public static DealerCompanyLogonDataSplitCollection NewDealerCompanyLogonDataSplitCollection()
        {
            return new DealerCompanyLogonDataSplitCollection();
        }

        internal static DealerCompanyLogonDataSplitCollection GetDealerCompanyLogonDataSplitCollection(IDataReader dr)
        {
            return new DealerCompanyLogonDataSplitCollection(dr);
        }

        private DealerCompanyLogonDataSplitCollection()
        {

        }

        private DealerCompanyLogonDataSplitCollection(IDataReader dr)
        {
            Fetch(dr);
        }

        #endregion

        #region Data Access

        // called to load data from the database
        private void Fetch(IDataReader reader)
        {
            RaiseListChangedEvents = false;
            while (reader.Read())// && !reader.IsDBNull(reader.GetOrdinal("SisConnectionId")))
            {
                Add(DealerCompanyLogonDataSplit.GetDealerCompanyLogonDataSplit(reader));
            }
            RaiseListChangedEvents = true;
        }

        internal void Update(IDataConnection connection, IDbTransaction transaction)
        {
            RaiseListChangedEvents = false;

            // add/update any current child objects
            foreach (DealerCompanyLogonDataSplit obj in this)
            {
                if (obj.IsDeleted)
                {
                    obj.DeleteSelf(connection, transaction);
                }
                else if (obj.IsNew)
                {
                    obj.Insert(connection, transaction);
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion
    }
}