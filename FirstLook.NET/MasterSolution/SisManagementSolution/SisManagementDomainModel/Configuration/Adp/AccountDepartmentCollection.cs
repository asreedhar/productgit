using System;
using System.Data;
using System.Security;
using Csla;
using Csla.Validation;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class AccountDepartmentCollection : NameValueListBase<string, string>
    {
        #region Factory Methods

        public static AccountDepartmentCollection GetAccountDepartments(AccountType accountType)
        {
            return DataPortal.Fetch<AccountDepartmentCollection>(new FetchCriteria(accountType));
        }

        private AccountDepartmentCollection()
        { /* require use of factory methods */}

        #endregion

        #region Data Access

        [Serializable]
        class FetchCriteria
        {
            private readonly AccountType accountType;

            public FetchCriteria(AccountType accountType)
            {
                this.accountType = accountType;
            }

            public AccountType AccountType
            {
                get { return accountType; }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "criteria")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchCriteria criteria)
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Configuration.AccountDepartmentCollection#Fetch";
                    
                    command.AddParameterWithValue("AccountTypeId", DbType.Int32, false, (int)criteria.AccountType);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        IsReadOnly = false;

                        Add(new NameValuePair("", "All"));
                        while (reader.Read())
                        {
                            string department = reader.GetString(reader.GetOrdinal("Department"));
                            Add(new NameValuePair(department, department));
                        }

                        IsReadOnly = true;
                    }
                }
            }

            RaiseListChangedEvents = true;
        }

        #endregion
    }
}
