using System;
using System.Data;
using Csla;
using Csla.Validation;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp
{
    [Serializable]
    public class Time: IEquatable<Time>, IComparable<Time>
    {
        private int hour;

        public int Hour
        {
            get
            {
                return hour;
            }
            set
            {
                if (Hour != value)
                {
                    if (IsValidHour(value))
                    {
                        hour = value;
                    }
                    else
                    {
                        throw new ArgumentOutOfRangeException("value", value, "Hour must be between 0 and 24");
                    }
                }
            }
        }

        private static bool IsValidHour(int value)
        {
            return value >= 0 && value <= 23;
        }

        private int minute;

        public int Minute
        {
            get
            {
                return minute;
            }
            set
            {
                if (Minute != value)
                {
                    if (IsValidMinute(value))
                    {
                        minute = value;
                    }
                    else
                    {
                        throw new ArgumentOutOfRangeException("value", value, "Hour must be between 0 and 24");
                    }
                }
            }
        }

        private static bool IsValidMinute(int value)
        {
            return value >= 0 && value <= 59;
        }

        public bool IsValid
        {
            get
            {
                return IsValidMinute(minute) && IsValidHour(hour);
            }
        }

        public Time(int hour, int minute)
        {
            if (!IsValidHour(hour))
                throw new ArgumentOutOfRangeException("hour", hour, "between 0 and 23 incl please");

            if (!IsValidMinute(minute))
                throw new ArgumentOutOfRangeException("hour", hour, "between 0 and 59 incl please");

            this.hour = hour;
            this.minute = minute;
        }

        public static bool TimeRequired(object target, RuleArgs e)
        {
            Time value = (Time)Utilities.CallByName(target, e.PropertyName, CallType.Get);

            if (value == null)
            {
                e.Description = string.Format("{0} required", e.PropertyName);

                return false;
            }

            return true;
        }

        public static bool TimeIsValid(object target, RuleArgs e)
        {
            Time value = (Time)Utilities.CallByName(target, e.PropertyName, CallType.Get);

            if (value != null && !value.IsValid)
            {
                e.Description = string.Format("{0} is not a valid time", e.PropertyName);

                return false;
            }

            return true;
        }

        internal static Time GetTime(IDataRecord record, string prefix)
        {
            int ordinal = record.GetOrdinal(prefix + "Hour");

            if (record.IsDBNull(ordinal))
                return null;

            int hour = record.GetInt32(ordinal);

            ordinal = record.GetOrdinal(prefix + "Minute");

            if (record.IsDBNull(ordinal))
                return null;

            int minute = record.GetInt32(ordinal);

            return new Time(hour, minute);
        }

        internal void AddToCommand(IDataCommand command, string prefix)
        {
            command.AddParameterWithValue(prefix + "Hour", DbType.Int32, false, hour);
            command.AddParameterWithValue(prefix + "Minute", DbType.Int32, false, minute);
        }

        /// <summary>
        /// Return the number of hours and minutes between this
        /// (the start) ending at the "other" time.  If the end
        /// is before the start value we assume that it wraps
        /// around midnight.
        /// </summary>
        /// <param name="endTime">End Time</param>
        /// <returns>Duration in hours and minutes</returns>
        public Time TimeUntil(Time endTime)
        {
            Time beginTime = this;
            
            int beginMinutes = beginTime.hour * 60 + beginTime.minute; // minutes since midnight of begin-time
            int endMinutes = endTime.hour * 60 + endTime.minute;  // minutes since midnight of end-time
            int durationMinutes;

            if (beginTime.hour > endTime.hour)
            {
                // 0100,0000 = 24 - (1-0) = 23
                // 0900,0200 = 24 - (9-2) = 17
                durationMinutes = (24 * 60) - (beginMinutes - endMinutes);
            }
            else
            {
                // 0000,0100 = 1-0 = 1
                durationMinutes = (endMinutes - beginMinutes);
            }

            return new Time(durationMinutes / 60, durationMinutes % 60);
        }

        public bool Equals(Time time)
        {
            if (time == null) return false;
            return hour == time.hour && minute == time.minute;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj)) return true;
            return Equals(obj as Time);
        }

        public override int GetHashCode()
        {
            return hour + 29*minute;
        }

        #region IComparable<Time> Members

        public int CompareTo(Time other)
        {
            int c = hour.CompareTo(other.hour);

            if (c == 0)
            {
                c = minute.CompareTo(other.minute);
            }

            return c;
        }

        #endregion
    }
}
