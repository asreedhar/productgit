using System;
using System.Data;

using Csla;

using FirstLook.Common.Data;
using FirstLook.Sis.Management.DomainModel.Configuration.Adp;

namespace FirstLook.Sis.Management.DomainModel.Configuration
{
    [Serializable]
    public class DownloadTestDataCommand : CommandBase
    {
        private readonly Dealer _dealer;

        public static void Execute(Dealer dealer)
        {
            DataPortal.Execute<DownloadTestDataCommand>(new DownloadTestDataCommand(dealer));
        }

        private DownloadTestDataCommand()
        {
            /* force factory methods */
        }

        private DownloadTestDataCommand(Dealer dealer)
        {
            _dealer = dealer;
        }

        [Transactional(TransactionalTypes.Manual)]
        protected override void DataPortal_Execute()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();
                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                        {
                            command.CommandType = CommandType.StoredProcedure;

                            command.CommandText = "Runtime.Query#DealerLoad";
                            command.AddParameterWithValue("DealerId", DbType.Int32, false, _dealer.Id);
                            command.AddParameterWithValue("EnvironmentId", DbType.Int32, false, (int)_dealer.Environment);
                            
                            command.Transaction = transaction;
                            command.ExecuteNonQuery();
                        }
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }
    }
}