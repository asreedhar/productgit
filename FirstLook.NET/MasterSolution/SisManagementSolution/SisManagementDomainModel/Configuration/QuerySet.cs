using System;
using System.Data;
using System.Security;
using Csla;
using FirstLook.Common.Data;

using FirstLook.Sis.Management.DomainModel.Configuration.Adp;

namespace FirstLook.Sis.Management.DomainModel.Configuration
{
    [Serializable]
    public class QuerySet : ReadOnlyBase<QuerySet>
    {
        #region Business Methods

        private Guid id;
        private DateTime loadDate;
	    private bool isProcessed;
        private QueryCollection queries;

        public Guid Id
        {
            get { return id; }
        }

        public DateTime LoadDate
        {
            get { return loadDate; }
        }

        public bool IsProcessed
        {
            get { return isProcessed; }
        }

        public QueryCollection Queries
        {
            get { return queries; }
        }

        protected override object GetIdValue()
        {
            return id;
        }

        #endregion

        #region Authorization Methods

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Factory Methods

        public static QuerySet GetLastRunDealerLoad(Dealer dealer, string loadType)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view queries");

            return DataPortal.Fetch<QuerySet>(new FetchCriteria(dealer, loadType));
        }

        private QuerySet() { }

        #endregion

        #region Data Access

        [Serializable]
        private class FetchCriteria
        {
            private readonly Dealer dealer;
            private readonly string loadType;

            public FetchCriteria(Dealer dealer, string loadType)
            {
                this.dealer = dealer;
                this.loadType = loadType;
            }

            public Dealer Dealer
            {
                get { return dealer; }
            }

            public string LoadType
            {
                get { return loadType; }
            }
        }

        private void DataPortal_Fetch(FetchCriteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Configuration.Dealer#LoadHistory#Fetch";

                    command.AddParameterWithValue("DealerId", DbType.Int32, false, criteria.Dealer.Id);
                    command.AddParameterWithValue("EnvironmentId", DbType.Int32, false, (int)criteria.Dealer.Environment);
                    command.AddParameterWithValue("LoadType", DbType.String, false, criteria.LoadType);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            id = reader.GetGuid(reader.GetOrdinal("Id"));
                            loadDate = reader.GetDateTime(reader.GetOrdinal("LoadDate"));
                            isProcessed = reader.GetBoolean(reader.GetOrdinal("IsProcessed"));

                            if (reader.NextResult())
                            {
                                queries = QueryCollection.GetQueryCollection(reader);
                            }
                            else
                            {
                                throw new DataException("Missing Query data-set");
                            }
                        }
                        //else
                        //{
                        //    throw new DataException("QuerySet not found");
                        //}
                    }
                }
            }
        }

        #endregion

    }
}
