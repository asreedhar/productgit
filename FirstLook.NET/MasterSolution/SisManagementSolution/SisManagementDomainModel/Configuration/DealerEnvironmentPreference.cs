using System;
using System.Data;
using System.Security;
using Csla;
using FirstLook.Common.Data;
using FirstLook.Sis.Management.DomainModel.Configuration.Adp;


namespace FirstLook.Sis.Management.DomainModel.Configuration
{
    public class DealerEnvironmentPreference : BusinessBase<DealerEnvironmentPreference>
    {
        #region Business Methods

        private Dealer dealer;
        private Preference pref;
        private object value;

        public Dealer Dealer
        {
            get
            {
                CanReadProperty("Dealer", true);
                return dealer;
            }
            set
            {
                CanWriteProperty("Dealer", true);
                if (dealer != value)
                {
                    dealer = value;
                    PropertyHasChanged("Dealer");
                }
            }
        }

        public Preference Pref
        {
            get
            {
                CanReadProperty("Pref", true);
                return pref;
            }
            set
            {
                CanWriteProperty("Pref", true);
                if (pref != value)
                {
                    pref = value;
                    PropertyHasChanged("Pref");
                }
            }
        }

        public object Value
        {
            get
            {
                CanReadProperty("Value", true);
                return value;
            }
            set
            {
                CanWriteProperty("Value", true);
                if (this.value != value)
                {
                    try
                    {
                        this.value = Convert.ChangeType(value, pref.DataType);
                    }
                    catch
                    {
                        this.value = value;
                    }
                    PropertyHasChanged("Value");
                }
            }
        }

        protected override object GetIdValue()
        {
            return pref.Id;
        }
        #endregion

        #region Authorization

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        public static bool CanAddObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        public static bool CanEditObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        public static bool CanDeleteObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }
        #endregion

        #region Validation Rules

        public override bool IsValid
        {
            get
            {
                return (
                    base.IsValid &&
                    (dealer != null && dealer.IsValid) &&
                    (pref != null) &&
                    (value != null) &&
                    value.GetType().Equals(pref.DataType));
            }
        }

        #endregion

        #region Factory Methods

        public static DealerEnvironmentPreference NewDealerEnvironmentPreference(Dealer dealer, Preference pref, object value)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view DealerEnvironmentPreference");

            return new DealerEnvironmentPreference(dealer, pref, value);
        }

        public static DealerEnvironmentPreference GetDealerEnvironmentPreference(IDataRecord record)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view DealerEnvironmentPreference");

            return new DealerEnvironmentPreference(record);
        }

        private DealerEnvironmentPreference()
        {
            MarkAsChild();
        }

        private DealerEnvironmentPreference(Dealer dealer, Preference pref, object value)
        {
            MarkAsChild();
            this.dealer = dealer;
            this.pref = pref;
            this.Value = value;
            ValidationRules.CheckRules();
        }

        private DealerEnvironmentPreference(IDataRecord record)
        {
            MarkAsChild();
            Fetch(record);
            MarkOld();
            ValidationRules.CheckRules();
        }

        public override DealerEnvironmentPreference Save()
        {
            if (IsNew && !CanAddObject())
                throw new SecurityException("User not authorized to add DealerEnvironmentPreference");

            if (IsDeleted && !CanDeleteObject())
                throw new SecurityException("User not authorized to delete DealerEnvironmentPreference");

            if (!CanEditObject())
                throw new SecurityException("User not authorized to edit DealerEnvironmentPreference");

            return base.Save();
        }

        #endregion

        #region Data Access

        protected void Fetch(IDataRecord reader)
        {
            pref = Preference.GetPreference(reader);
            value = reader.GetValue(reader.GetOrdinal("Value"));
        }

        internal void Insert(IDataConnection connection, IDbTransaction transaction)
        {
            if (!IsDirty) return;

            DoInsertUpdate(connection, transaction, "Configuration.DealerEnvironmentPreference#Insert");

            MarkOld();
        }

        internal void Update(IDataConnection connection, IDbTransaction transaction)
        {
            if (!IsDirty) return;

            if (IsNew) return;

            DoInsertUpdate(connection, transaction, "Configuration.DealerEnvironmentPreference#Update");

            MarkOld();
        }

        internal void DeleteSelf(IDataConnection connection, IDbTransaction transaction)
        {
            if (!IsDirty) return;

            if (IsNew) return;

            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = "Configuration.DealerEnvironmentPreference#Delete";
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;
                command.AddParameterWithValue("DealerId", DbType.Int32, false, dealer.Id);
                command.AddParameterWithValue("EnvironmentId", DbType.Int32, false, (int)dealer.Environment);
                command.AddParameterWithValue("PreferenceId", DbType.Int32, false, this.pref.Id);
                command.ExecuteNonQuery();
            }

            MarkNew();
        }

        private void DoInsertUpdate(IDataConnection connection, IDbTransaction transaction, string commandText)
        {
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = commandText;
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;

                command.AddParameterWithValue("DealerId", DbType.Int32, false, dealer.Id);
                command.AddParameterWithValue("EnvironmentId", DbType.Int32, false, (int)dealer.Environment);
                command.AddParameterWithValue("PreferenceId", DbType.Int32, false, this.pref.Id);
                command.AddParameterWithValue("PreferenceValue", DbType.Object, false, Convert.ChangeType(this.value, pref.DataType));

                command.ExecuteNonQuery();
            }
        }
        
        #endregion
    }
}