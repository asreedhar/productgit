using System;
using System.Data;

using Csla;

using FirstLook.Common.Data;
using FirstLook.Sis.Management.DomainModel.Configuration.Adp;

namespace FirstLook.Sis.Management.DomainModel.Configuration
{
    [Serializable]
    public class DownloadHistoricalDataCommand : CommandBase
    {
        private readonly Dealer _dealer;
        private readonly DateTime _salesDateMin;

        public static void Execute(Dealer dealer, DateTime salesDateMin)
        {
            DataPortal.Execute<DownloadHistoricalDataCommand>(new DownloadHistoricalDataCommand(dealer, salesDateMin));
        }

        private DownloadHistoricalDataCommand()
        {
            /* force factory methods */
        }

        private DownloadHistoricalDataCommand(Dealer dealer, DateTime salesDateMin)
        {
            _dealer = dealer;
            _salesDateMin = salesDateMin;
        }

        [Transactional(TransactionalTypes.Manual)]
        protected override void DataPortal_Execute()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();
                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                        {
                            command.CommandType = CommandType.StoredProcedure;

                            command.CommandText = "Runtime.Query#DealerLoad";
                            command.AddParameterWithValue("DealerId", DbType.Int32, false, _dealer.Id);
                            command.AddParameterWithValue("EnvironmentId", DbType.Int32, false, (int)_dealer.Environment);
                            command.AddParameterWithValue("IsHistorical", DbType.Boolean, false, true);
                            command.AddParameterWithValue("SalesDateMin", DbType.DateTime, false, _salesDateMin);

                            command.Transaction = transaction;
                            command.ExecuteNonQuery();
                        }
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }
    }
}