namespace FirstLook.Sis.Management.DomainModel.Configuration
{
    public enum TransactionType
    {
        Undefined    = 0,
        GetAccounts  = 1,
        GetJournals  = 2,
        GetSchedules = 3
    }
}