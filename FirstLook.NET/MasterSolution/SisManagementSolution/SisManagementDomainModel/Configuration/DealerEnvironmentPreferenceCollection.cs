using System;
using System.Data;
using System.Security;
using Csla;
using FirstLook.Common.Data;
using FirstLook.Sis.Management.DomainModel.Configuration.Adp;

namespace FirstLook.Sis.Management.DomainModel.Configuration
{
    public class DealerEnvironmentPreferenceCollection : BusinessListBase<DealerEnvironmentPreferenceCollection, DealerEnvironmentPreference>
    {
        #region Business Methods

        private Dealer dealer;

        public Dealer Dealer 
        {
            get { return dealer; }
            set { dealer = value; }
        }


        public void Remove(Preference p)
        {
            foreach (DealerEnvironmentPreference pref in this)
            {
                if (pref.Pref.Id == p.Id)
                {
                    Remove(pref);
                    break;
                }
            }
        }


        public DealerEnvironmentPreference Get(Preference p)
        {
            foreach (DealerEnvironmentPreference pref in this)
            {
                if (pref.Pref.Id == p.Id)
                {
                    return pref;
                }
            }
            return null;
        }

        public new bool Contains(DealerEnvironmentPreference dep)
        {
            foreach (DealerEnvironmentPreference pref in this)
            {
                if (pref == dep)
                {
                    return true;
                }
            }
            return false;
        }

        public bool Contains(Preference p)
        {
            foreach (DealerEnvironmentPreference pref in this)
            {
                if (pref.Pref.Id == p.Id)
                {
                    return true;
                }
            }
            return false;
        }

        public new bool ContainsDeleted(DealerEnvironmentPreference dep)
        {
            foreach (DealerEnvironmentPreference pref in DeletedList)
            {
                if (pref == dep)
                {
                    return true;
                }
            }
            return false;
        }


        #endregion

        #region Authorization Methods

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Factory Methods

        public static DealerEnvironmentPreferenceCollection GetDealerEnvironmentPreferenceCollection(Dealer dealer)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view Mappings");

            return DataPortal.Fetch<DealerEnvironmentPreferenceCollection>(new FetchCriteria(dealer));
        }

        #endregion

        #region Data Access

        [Serializable]
        private class FetchCriteria
        {
            private readonly Dealer dealer;

            public FetchCriteria(Dealer dealer)
            {
                this.dealer = dealer;
            }

            public Dealer Dealer
            {
                get { return dealer; }
            }
        }

        private void DataPortal_Fetch(FetchCriteria criteria)
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Configuration.DealerEnvironmentPreferenceCollection#Fetch";

                    command.AddParameterWithValue("DealerId", DbType.Int32, false, criteria.Dealer.Id);
                    command.AddParameterWithValue("EnvironmentId", DbType.Int32, false, (int)criteria.Dealer.Environment);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DealerEnvironmentPreference p = DealerEnvironmentPreference.GetDealerEnvironmentPreference(reader);
                            p.Dealer = criteria.Dealer;
                            Add(p);
                        }
                    }
                }
            }

            RaiseListChangedEvents = true;
        }


        [Transactional(TransactionalTypes.Manual)]
        protected override void DataPortal_Update()
        {
            if (!IsDirty) return;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        RaiseListChangedEvents = false;

                        foreach (DealerEnvironmentPreference item in DeletedList)
                        {
                            item.DeleteSelf(connection, transaction);
                        }
                        DeletedList.Clear();

                        // add/update any current child objects
                        foreach (DealerEnvironmentPreference item in this)
                        {
                            if (item.IsNew)
                            {
                                item.Insert(connection, transaction);
                            }
                            else
                            {
                                item.Update(connection, transaction);
                            }
                        }

                        RaiseListChangedEvents = true;

                        transaction.Commit();
                    }
                    catch
                    {
                        try
                        {
                            transaction.Rollback();
                        }
                        catch
                        {
                            throw;
                        }

                        throw;
                    }
                }
            }
        }

        #endregion
    }
}