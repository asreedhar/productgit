using System;
using System.Data;
using System.Security;
using Csla;
using FirstLook.Common.Data;

using FirstLook.Sis.Management.DomainModel.Configuration.Adp;

namespace FirstLook.Sis.Management.DomainModel.Configuration
{
    [Serializable]
    public class Query : BusinessBase<Query>
    {
        #region Business Methods

        private Guid id;
        private DateTime date;
        private QueryStatus status;

        public Guid Id
        {
            get
            {
                CanReadProperty("Id", true);
                return id;
            }
        }
        public DateTime Date
        {
            get
            {
                CanReadProperty("Date", true);
                return date;
            }
        }
        public QueryStatus Status
        {
            get
            {
                CanReadProperty("Status", true);
                return status;
            }
        }

        protected override object GetIdValue()
        {
            return id;
        }

        #endregion

        #region Authorization Rules

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        public static bool CanAddObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        #endregion

        #region Factory Methods

        public static Query NewQuery(SisConnection sisConnection, TransactionType transactionType)
        {
            if (!CanAddObject())
                throw new SecurityException("Insufficient priviledges to create new Query");
            return DataPortal.Create<Query>(new Criteria(sisConnection, transactionType));
        }

        public static Query GetQuery(SisConnection sisConnection, TransactionType transactionType)
        {
            if (!CanGetObject())
                throw new SecurityException("Insufficient priviledges to load Query");
            return DataPortal.Fetch<Query>(new Criteria(sisConnection, transactionType));
        }

        
        internal static Query GetQuery(IDataReader reader)
        {
            return new Query(reader);
        }

        private Query()
        {
            /* Force use orf factory methods */
        }

        private Query(IDataRecord record)
        {
            Fetch(record);
        }

        #endregion

        #region Data Access

        [Serializable]
        class Criteria
        {
            private readonly SisConnection sisConnection;
            private readonly TransactionType transactionType;

            public Criteria(SisConnection sisConnection, TransactionType transactionType)
            {
                this.sisConnection = sisConnection;
                this.transactionType = transactionType;
            }

            public SisConnection SisConnection
            {
                get { return sisConnection; }
            }

            public TransactionType TransactionType
            {
                get { return transactionType; }
            }
        }

        private void DataPortal_Create(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Runtime.Query#Create";
                    command.AddParameterWithValue("SISCredentialId", DbType.Guid, false, criteria.SisConnection.Credential.Id);
                    command.AddParameterWithValue("TransactionType", DbType.String, false, criteria.TransactionType.ToString());
                    IDataParameter outId = command.AddOutParameter("QueryId", DbType.Guid);
                    command.ExecuteNonQuery();
                    id = (Guid)outId.Value;
                    status = QueryStatus.NotSubmitted;
                }
            }
        }

        private void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Configuration.SISConnection#ReferenceHistory#Fetch";
                    command.AddParameterWithValue("ConnectionId", DbType.Guid, false, criteria.SisConnection.Id);
                    command.AddParameterWithValue("TransactionType", DbType.String, false, criteria.TransactionType.ToString());

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Fetch(reader);
                        }
                    }
                }
            }
        }

        private void Fetch(IDataRecord record)
        {
            id = record.GetGuid(record.GetOrdinal("QueryId"));
            date = record.GetDateTime(record.GetOrdinal("QueryDate"));
            status = (QueryStatus)Enum.Parse(typeof(QueryStatus), record.GetString(record.GetOrdinal("ProcessBuffer")));
        }

        #endregion
    }
}
