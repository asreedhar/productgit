using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Configuration
{
    public class Preference : ReadOnlyBase<Preference>
    {
        #region Business Methods

        private int id;
        private string shortDescription;
        private string longDescription;
        private Type dataType;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string ShortDescription
        {
            get { return shortDescription; }
            set { shortDescription = value; }
        }

        public string LongDescription
        {
            get { return longDescription; }
            set { longDescription = value; }
        }

        public Type DataType
        {
            get { return dataType; }
            set { dataType = value; }
        }

        protected override object GetIdValue()
        {
            return id;
        }
        #endregion

        #region Authorization

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Factory Methods

        internal static Preference GetPreference(IDataRecord reader)
        {
            return new Preference(reader);
        }

        private Preference()
        {
            /* Force use orf factory methods */
        }

        private Preference(IDataRecord record)
        {
            Fetch(record);
        }

        #endregion

        #region Data Access

        protected void Fetch(IDataRecord reader)
        {
            id = reader.GetInt32(reader.GetOrdinal("Id"));
            shortDescription = reader.GetString(reader.GetOrdinal("ShortDescription"));
            longDescription = reader.GetString(reader.GetOrdinal("LongDescription"));
            string s = reader.GetString(reader.GetOrdinal("DataType"));
            dataType = Type.GetType(reader.GetString(reader.GetOrdinal("DataType")));
        }

        #endregion
    }
}