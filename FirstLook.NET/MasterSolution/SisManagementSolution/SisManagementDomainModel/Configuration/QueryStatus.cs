namespace FirstLook.Sis.Management.DomainModel.Configuration
{
    public enum QueryStatus
    {
        Undefined    = 0,
        NotSubmitted = 1,
        Submitted    = 2,
        Accepted     = 3,
        Completed    = 4,
        Error        = 5,
        Staged       = 6
    }
}