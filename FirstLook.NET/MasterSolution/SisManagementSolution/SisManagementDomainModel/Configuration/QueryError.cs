using System;
using System.Data;
using System.Security;
using Csla;
using FirstLook.Common.Data;

using FirstLook.Sis.Management.DomainModel.Configuration.Adp;

namespace FirstLook.Sis.Management.DomainModel.Configuration
{
    [Serializable]
    public class QueryError : ReadOnlyBase<QueryError>
    {
        #region Business Methods
        
        private string _type;
        private string _stackTrace;
        private string _suggestedResolution;

        public string Type
        {
            get
            {
                CanReadProperty("Type", true);
                return _type;
            }
        }

        public string SuggestedResolution
        {
            get
            {
                CanReadProperty("SuggestedResolution", true);
                return _suggestedResolution;
            }
        }

        public string StackTrace
        {
            get
            {
                CanReadProperty("StackTrace", true);
                return _stackTrace;
            }
        }


        protected override object GetIdValue()
        {
            return null;
        }

        #endregion

        #region Authorization Rules

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Factory Methods

        internal static QueryError GetQueryError(IDataReader reader)
        {
            return new QueryError(reader);
        }

        private QueryError()
        {
            /* Force use orf factory methods */
        }

        private QueryError(IDataRecord record)
        {
            Fetch(record);
        }

        #endregion

        #region Data Access

        private void Fetch(IDataRecord record)
        {
            _type = record.IsDBNull(record.GetOrdinal("Type")) ? "" : record.GetString(record.GetOrdinal("Type"));
            _suggestedResolution = record.IsDBNull(record.GetOrdinal("SuggestedResolution")) ? "" : record.GetString(record.GetOrdinal("SuggestedResolution"));
            _stackTrace = record.IsDBNull(record.GetOrdinal("StackTrace")) ? "" : record.GetString(record.GetOrdinal("StackTrace"));
        }

        #endregion
    }
}