
using System;
using System.Data;
using System.Data.SqlClient;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel
{
    static class Database
    {
        private const string sisDatabase = "SIS";

        public static string SisDatabase
        {
            get { return sisDatabase; }
        }

        public static void AddArrayParameter(IDataCommand command, string name, DbType type, bool nullable, Array value)
        {
            IDbDataParameter paramVersion = command.CreateParameter();
            paramVersion.DbType = type;
            paramVersion.ParameterName = name;
            paramVersion.Size = value.Length;
            paramVersion.Value = value;

            SqlParameter paramVersionSql = paramVersion as SqlParameter;

            if (paramVersionSql != null)
            {
                paramVersionSql.IsNullable = nullable;
            }

            command.Parameters.Add(paramVersion);
        }

        public static IDataParameter AddArrayOutParameter(IDataCommand command, string name, DbType type, bool nullable, int size)
        {
            IDbDataParameter parameter = command.CreateParameter();
            parameter.DbType = type;
            parameter.Direction = ParameterDirection.Output;
            parameter.ParameterName = name;
            parameter.Size = size;
            
            SqlParameter sqlParameter = parameter as SqlParameter;

            if (sqlParameter != null)
            {
                // sqlParameter.SqlDbType = SqlDbType.Timestamp;
                sqlParameter.IsNullable = nullable;
            }

            command.Parameters.Add(parameter);

            return parameter;
        }
    }
}