using System;
using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel
{
    [Serializable]
    public sealed class DataSourceSelectCriteria
    {
        private readonly string sortOrder;
        private readonly int maximumRows;
        private readonly int startRowIndex;

        public DataSourceSelectCriteria(string sortOrder, int maximumRows, int startRowIndex)
        {
            this.sortOrder = sortOrder;
            this.maximumRows = maximumRows;
            this.startRowIndex = startRowIndex;
        }

        public string SortOrder
        {
            get { return sortOrder; }
        }

        public int MaximumRows
        {
            get { return maximumRows; }
        }

        public int StartRowIndex
        {
            get { return startRowIndex; }
        }

        public void AddToCommand(IDataCommand command)
        {
            command.AddParameterWithValue("SortColumns", DbType.String, false, SortOrder);
            command.AddParameterWithValue("MaximumRows", DbType.String, false, MaximumRows);
            command.AddParameterWithValue("StartRowIndex", DbType.String, false, StartRowIndex);
        }
    }
}