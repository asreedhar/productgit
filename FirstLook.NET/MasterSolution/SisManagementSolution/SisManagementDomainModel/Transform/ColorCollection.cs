using System;
using System.Data;
using System.Security;
using Csla;
using Csla.Core;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Transform
{
    [Serializable]
    public class ColorCollection : ReadOnlyListBase<ColorCollection, Color>, IReportTotalRowCount
    {
        #region IReportTotalRowCount Members

        private int totalRowCount = 0;

        public int TotalRowCount
        {
            get
            {
                return totalRowCount;
            }
        }

        #endregion

        #region Authorization Rules

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Factory Methods

        public static ColorCollection GetColors(int? standardColorId, string name, string sortOrder, int maximumRows, int startRowIndex)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view ColorCollection");

            if (string.IsNullOrEmpty(name))
                name = string.Empty;

            if (maximumRows < 1)
                throw new ArgumentOutOfRangeException("maximumRows", maximumRows, "Must be greater than zero");

            if (startRowIndex < 0)
                throw new ArgumentOutOfRangeException("startRowIndex", startRowIndex, "Must be greater than zero");

            if (string.IsNullOrEmpty(sortOrder))
                sortOrder = "Name";

            return DataPortal.Fetch<ColorCollection>(new Criteria(standardColorId, name, new DataSourceSelectCriteria(sortOrder, maximumRows, startRowIndex)));
        }

        private ColorCollection()
        {
            /* force use of factory methods */
        }

        #endregion

        #region Data Access

        [Serializable]
        private class Criteria
        {
            private readonly int? standardColorId;
            private readonly string colorName;
            private readonly DataSourceSelectCriteria dsc;

            public Criteria(int? standardColorId, string colorName, DataSourceSelectCriteria dsc)
            {
                this.standardColorId = standardColorId;
                this.colorName = colorName;
                this.dsc = dsc;
            }

            public int? StandardColorId
            {
                get { return standardColorId; }
            }

            public string ColorName
            {
                get { return colorName; }
            }

            public DataSourceSelectCriteria DataSourceSelectCriteria
            {
                get { return dsc; }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called by CSLA using reflection")]
        private void DataPortal_Fetch(Criteria criteria)
        {
            RaiseListChangedEvents = false;

            using (IDataConnection conn = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                conn.Open();

                using (IDataCommand command = new DataCommand(conn.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Transform.ColorCollection#Fetch";
                    if (criteria.StandardColorId.HasValue)
                    {
                        command.AddParameterWithValue("StandardColorId", DbType.Int32, true, criteria.StandardColorId.Value);
                    }
                    else
                    {
                        command.AddParameterWithValue("StandardColorId", DbType.Int32, true, DBNull.Value);
                    }
                    command.AddParameterWithValue("ColorName", DbType.String, true, criteria.ColorName);
                    criteria.DataSourceSelectCriteria.AddToCommand(command);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            totalRowCount = reader.GetInt32(reader.GetOrdinal("TotalRowCount"));
                        }
                        else
                        {
                            throw new DataException("Missing TotalRowCount");
                        }

                        if (reader.NextResult())
                        {
                            IsReadOnly = false;

                            while (reader.Read())
                            {
                                Add(new Color(reader));
                            }

                            IsReadOnly = true;
                        }
                        else
                        {
                            throw new DataException("Missing Colors result set");
                        }
                    }
                }
            }

            RaiseListChangedEvents = true;
        }

        #endregion
    }
}