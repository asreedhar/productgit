using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Transform
{
    [Serializable]
    public class StandardColorMapping : BusinessBase<StandardColorMapping>
    {
        #region Business Methods

        private int colorId;

        public int ColorId
        {
            get
            {
                CanReadProperty("ColorId", true);

                return colorId;
            }
            set
            {
                CanWriteProperty("ColorId", true);

                if (ColorId != value)
                {
                    colorId = value;

                    PropertyHasChanged("ColorId");
                }
            }
        }

        public Color Color
        {
            get
            {
                return Color.GetColor(colorId);
            }
        }

        protected override object GetIdValue()
        {
            return colorId;
        }

        #endregion

        #region Factory Methods

        public static StandardColorMapping NewStandardColorMapping(int colorId)
        {
            return new StandardColorMapping(Color.GetColor(colorId));
        }

        internal static StandardColorMapping GetStandardColorMapping(IDataRecord record)
        {
            return new StandardColorMapping(record);
        }

        private StandardColorMapping()
        {
            MarkAsChild();
        }

        private StandardColorMapping(IDataRecord record) : this()
        {
            Fetch(record);
        }

        private StandardColorMapping(Color color) : this()
        {
            colorId = color.Id;
        }

        #endregion

        #region Data Access

        private void Fetch(IDataRecord record)
        {
            colorId = record.GetInt32(record.GetOrdinal("ColorId"));
        }

        internal void Insert(IDataConnection connection, IDbTransaction transaction, StandardColor standardColor)
        {
            if (!IsDirty) return;

            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                DoInsertUpdate(command, "Transform.StandardColorMapping#AddAssignment", transaction, standardColor);
            }

            MarkOld();
        }

        internal void Update(IDataConnection connection, IDbTransaction transaction, StandardColor standardColor)
        {
            if (!IsDirty) return;

            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                DoInsertUpdate(command, "Transform.StandardColorMapping#UpdateAssignment", transaction, standardColor);
            }

            MarkOld();
        }

        internal void DeleteSelf(IDataConnection connection, IDbTransaction transaction, StandardColor standardColor)
        {
            // if we're not dirty then don't update the database
            if (!IsDirty) return;

            // if we're new then don't update the database
            if (IsNew) return;

            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                DoInsertUpdate(command, "Transform.StandardColorMapping#DeleteAssignment", transaction, standardColor);
            }

            MarkNew();
        }

        private void DoInsertUpdate(IDataCommand command, string commandText, IDbTransaction transaction, StandardColor standardColor)
        {
            command.CommandText = commandText;
            command.CommandType = CommandType.StoredProcedure;
            command.Transaction = transaction;
            command.AddParameterWithValue("StandardColorId", DbType.Int32, false, standardColor.Id);
            command.AddParameterWithValue("ColorId", DbType.Int32, false, colorId);
            command.ExecuteNonQuery();
        }

        #endregion
    }
}
