using System;
using System.Data;
using Csla;

namespace FirstLook.Sis.Management.DomainModel.Transform
{
    [Serializable]
    public class ColorSummary : ReadOnlyBase<ColorSummary>
    {
        #region Business Methods

        private int id;

        public int Id
        {
            get
            {
                CanReadProperty("Id", true);

                return id;
            }
        }

        private string name;

        public string Name
        {
            get
            {
                CanReadProperty("Name", true);

                return name;
            }
        }

        private int numberOfVehicles;

        public int NumberOfVehicles
        {
            get
            {
                CanReadProperty("NumberOfVehicles", true);

                return numberOfVehicles;
            }
        }

        protected override object GetIdValue()
        {
            return id;
        }

        #endregion

        #region Factory Methods

        internal static ColorSummary GetColorSummary(IDataRecord record)
        {
            return new ColorSummary(record);
        }

        internal ColorSummary(IDataRecord record)
        {
            Fetch(record);
        }

        private void Fetch(IDataRecord record)
        {
            id = record.GetInt32(record.GetOrdinal("Id"));
            name = record.GetString(record.GetOrdinal("Name"));
            numberOfVehicles = record.GetInt32(record.GetOrdinal("NumberOfVehicles"));
        }

        #endregion
    }
}
