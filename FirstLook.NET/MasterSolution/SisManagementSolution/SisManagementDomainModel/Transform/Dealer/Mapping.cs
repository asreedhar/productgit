using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    public abstract class Mapping : ReadOnlyBase<Mapping>
    {
        #region Business Methods

        private int id;
        private string name;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        protected override object GetIdValue()
        {
            return id;
        }
        #endregion

        #region Authorization

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Data Access

        [Serializable]
        protected class Criteria
        {
            private readonly int id;

            public Criteria(int id)
            {
                this.id = id;
            }

            internal int Id
            {
                get { return id; }
            }
        }

        protected void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Transform.Mapping#Fetch";

                    command.AddParameterWithValue("Id", DbType.String, false, criteria.Id);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Fetch(reader);
                        }
                        else
                        {
                            throw new DataException("Mapping not found");
                        }

                    }
                }
            }
        }

        protected void Fetch(IDataRecord reader)
        {
            id = reader.GetInt32(reader.GetOrdinal("Id"));
            name = reader.GetString(reader.GetOrdinal("Name"));
        }

        #endregion
    }
}