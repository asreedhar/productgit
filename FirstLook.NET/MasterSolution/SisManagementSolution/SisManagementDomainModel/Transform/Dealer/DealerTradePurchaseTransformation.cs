using System;
using System.Data;
using System.Security;
using Csla;
using Csla.Validation;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    public class DealerTradePurchaseTransformation : BusinessBase<DealerTradePurchaseTransformation>
    {
        #region Business Methods

        private Configuration.Adp.Dealer dealer;
        private TradePurchaseTransformationType tradePurchaseTransformationType;
        private TradePurchaseTransformationStockNumberLogicCollection tradePurchaseTransformationStockNumberLogicCollection;


        public Configuration.Adp.Dealer Dealer
        {
            get
            {
                CanReadProperty("Dealer", true);
                return dealer;
            }
        }
        public TradePurchaseTransformationType TradePurchaseTransformationType
        {
            get
            {
                CanReadProperty("TradePurchaseTransformationType", true);
                return tradePurchaseTransformationType;
            }
            set
            {
                CanWriteProperty("TradePurchaseTransformationType", true);
                tradePurchaseTransformationType = value;
                PropertyHasChanged("TradePurchaseTransformationType");
            }
        }
        public TradePurchaseTransformationStockNumberLogicCollection TradePurchaseTransformationStockNumberLogicCollection
        {
            get
            {
                CanReadProperty("TradePurchaseTransformationStockNumberLogicCollection", true);
                return tradePurchaseTransformationStockNumberLogicCollection;
            }
            set
            {
                CanWriteProperty("TradePurchaseTransformationStockNumberLogicCollection", true);
                tradePurchaseTransformationStockNumberLogicCollection = value;
                PropertyHasChanged("TradePurchaseTransformationStockNumberLogicCollection");
            }
        }

        protected override object GetIdValue()
        {
            return dealer.Id;
        }

        #endregion

        #region Authorization Rules

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        public static bool CanAddObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        public static bool CanEditObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        #endregion

        #region Validation Rules

        public override bool IsValid
        {
            get
            {
                return tradePurchaseTransformationType == Transform.Dealer.TradePurchaseTransformationType.StockNumberLogic
                    ? tradePurchaseTransformationStockNumberLogicCollection != null
                    && tradePurchaseTransformationStockNumberLogicCollection.Count > 0
                    && tradePurchaseTransformationStockNumberLogicCollection.IsValid
                    : true;
            }
        }

        protected override void AddBusinessRules()
        {
            if (tradePurchaseTransformationType == Transform.Dealer.TradePurchaseTransformationType.StockNumberLogic)
            {
                ValidationRules.AddRule(TradePurchaseTransformationStockNumberLogicCollectionValidation, "tradePurchaseTransformationStockNumberLogicCollection");
            }
        }

        protected static bool TradePurchaseTransformationStockNumberLogicCollectionValidation(object target, RuleArgs ruleArgs)
        {
            bool isValid = false;

            TradePurchaseTransformationStockNumberLogicCollection uct = ((DealerTradePurchaseTransformation)target).TradePurchaseTransformationStockNumberLogicCollection;
            if (uct == null)
            {
                ruleArgs.Description = "TradePurchaseTransformationStockNumberLogicCollection cannot be null.";
            }
            else
            {
                isValid = (uct.IsValid);
                if (!isValid)
                {
                    ruleArgs.Description = "TradePurchase Transformation StockNumber Logic Collection must not have an empty stock number pattern or type.";
                }
            }
            return isValid;
        }

        #endregion

        #region Factory Methods

        public static DealerTradePurchaseTransformation GetDealerTradePurchaseTransformation(Configuration.Adp.Dealer dealer)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view DealerTradePurchaseTransformation");

            return DataPortal.Fetch<DealerTradePurchaseTransformation>(new Criteria(dealer));
        }

        public override DealerTradePurchaseTransformation Save()
        {
            if (IsNew && !CanAddObject())
                throw new SecurityException("User not authorized to add DealerTradePurchaseTransformation");

            if (!CanEditObject())
                throw new SecurityException("User not authorized to edit DealerTradePurchaseTransformation");

            return base.Save();
        }

        #endregion

        #region Data Access

        [Serializable]
        class Criteria
        {
            private readonly Configuration.Adp.Dealer dealer;

            public Criteria(Configuration.Adp.Dealer dealer)
            {
                this.dealer = dealer;
            }

            public Configuration.Adp.Dealer Dealer
            {
                get { return dealer; }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called by CSLA using reflection")]
        [Transactional(TransactionalTypes.Manual)]
        private void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Transform.DealerTradePurchaseTransformation#Fetch";
                    command.AddParameterWithValue("DealerId", DbType.Int32, true, criteria.Dealer.Id);
                    command.AddParameterWithValue("EnvironmentId", DbType.Int32, true, (int)criteria.Dealer.Environment);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        dealer = criteria.Dealer;
                        if (reader.Read() && !reader.IsDBNull(reader.GetOrdinal("TradePurchaseTransformationId")))
                        {
                            tradePurchaseTransformationType = (TradePurchaseTransformationType)reader.GetInt32(reader.GetOrdinal("TradePurchaseTransformationId"));
                        }
                        if (reader.NextResult())
                        {
                            tradePurchaseTransformationStockNumberLogicCollection = Transform.Dealer.TradePurchaseTransformationStockNumberLogicCollection.GetTradePurchaseTransformationStockNumberLogicCollection(reader);
                        }
                    }
                }
            }
            ValidationRules.CheckRules();
        }



        [Transactional(TransactionalTypes.Manual)]
        protected override void DataPortal_Update()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.CommandText = "Transform.DealerTradePurchaseTransformation#Update";
                            command.Transaction = transaction;

                            command.AddParameterWithValue("DealerId", DbType.Int32, false, dealer.Id);
                            command.AddParameterWithValue("EnvironmentId", DbType.Int32, true, (int)dealer.Environment);
                            command.AddParameterWithValue("TradePurchaseTransformationId", DbType.Int32, false, (int)tradePurchaseTransformationType);
                            command.ExecuteNonQuery();
                        }
                        
                        tradePurchaseTransformationStockNumberLogicCollection.Update(dealer, connection, transaction);
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }

        #endregion
    }
}