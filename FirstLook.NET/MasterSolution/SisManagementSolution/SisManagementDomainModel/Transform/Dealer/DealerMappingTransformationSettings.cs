using System;
using System.Data;
using System.Security;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    public class DealerMappingTransformationSettings : BusinessBase<DealerMappingTransformationSettings>
    {
        #region Business Methods

        private Configuration.Adp.Dealer dealer;
        private DealerStandardMappingSettings dealerStandardMappingSettings;
        private DealerTradePurchaseTransformationSettings dealerTradePurchaseTransformation;
        private DealerUnitCostTransformationSettings dealerUnitCostTransformationSettings;

        public Configuration.Adp.Dealer Dealer
        {
            get
            {
                CanReadProperty("Dealer", true);
                return dealer;
            }
        }
        public DealerStandardMappingSettings DealerStandardMappingSettings
        {
            get
            {
                CanReadProperty("DealerStandardMappingSettings", true);
                return dealerStandardMappingSettings;
            }
            set
            {
                CanWriteProperty("DealerStandardMappingSettings", true);
                dealerStandardMappingSettings = value;
                PropertyHasChanged("DealerStandardMappingSettings");
            }
        }
        public DealerTradePurchaseTransformationSettings DealerTradePurchaseTransformationSettings
        {
            get
            {
                CanReadProperty("DealerTradePurchaseTransformationSettings", true);
                return dealerTradePurchaseTransformation;
            }
            set
            {
                CanWriteProperty("DealerTradePurchaseTransformationSettings", true);
                dealerTradePurchaseTransformation = value;
                PropertyHasChanged("DealerTradePurchaseTransformationSettings");
            }
        }
        public DealerUnitCostTransformationSettings DealerUnitCostTransformationSettings
        {
            get
            {
                CanReadProperty("DealerUnitCostTransformationSettings", true);
                return dealerUnitCostTransformationSettings;
            }
            set
            {
                CanWriteProperty("DealerUnitCostTransformationSettings", true);
                dealerUnitCostTransformationSettings = value;
                PropertyHasChanged("DealerUnitCostTransformationSettings");
            }
        }


        protected override object GetIdValue()
        {
            return dealer.Id;
        }

        #endregion

        #region Authorization Rules

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        public static bool CanAddObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        public static bool CanEditObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        #endregion

        #region Validation Rules

        public override bool IsValid
        {
            get
            {
                return dealerStandardMappingSettings.IsValid &&
                        dealerTradePurchaseTransformation.IsValid &&
                        dealerUnitCostTransformationSettings.IsValid;
            }
        }

        #endregion

        #region Factory Methods

        public static DealerMappingTransformationSettings GetDealerMappingTransformationSettings(Configuration.Adp.Dealer dealer)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view DealerMappingTransformationSettings");

            return DataPortal.Fetch<DealerMappingTransformationSettings>(new Criteria(dealer));
        }

        public override DealerMappingTransformationSettings Save()
        {
            if (IsNew && !CanAddObject())
                throw new SecurityException("User not authorized to add DealerMappingTransformationSettings");

            if (!CanEditObject())
                throw new SecurityException("User not authorized to edit DealerMappingTransformationSettings");
            
            MarkDirty();
            return base.Save();
        }

        #endregion

        #region Data Access

        [Serializable]
        class Criteria
        {
            private readonly Configuration.Adp.Dealer dealer;

            public Criteria(Configuration.Adp.Dealer dealer)
            {
                this.dealer = dealer;
            }

            public Configuration.Adp.Dealer Dealer
            {
                get { return dealer; }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called by CSLA using reflection")]
        [Transactional(TransactionalTypes.Manual)]
        private void DataPortal_Fetch(Criteria criteria)
        {
            dealer = criteria.Dealer;
            dealerStandardMappingSettings = Transform.Dealer.DealerStandardMappingSettings.GetDealerStandardMappingSettings(criteria.Dealer);
            dealerTradePurchaseTransformation = DealerTradePurchaseTransformationSettings.GetDealerTradePurchaseTransformationSettings(criteria.Dealer);
            dealerUnitCostTransformationSettings = DealerUnitCostTransformationSettings.GetDealerUnitCostTransformationSettings(criteria.Dealer);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called by CSLA using reflection")]
        [Transactional(TransactionalTypes.Manual)]
        protected override void DataPortal_Update()
        {
            dealerStandardMappingSettings.Save();
            dealerTradePurchaseTransformation.Save();
            dealerUnitCostTransformationSettings.Save();

            //if (!IsDirty) return;

            //using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            //{
            //    connection.Open();

            //    using (IDbTransaction transaction = connection.BeginTransaction())
            //    {
            //        try
            //        {
            //            dealerStandardMappingSettings.Save();
            //            dealerTradePurchaseTransformation.Save();
            //            dealerUnitCostTransformationSettings.Save();
            //            transaction.Commit();
            //        }
            //        catch
            //        {
            //            transaction.Rollback();
            //            throw;
            //        }
            //    }
            //}
        }

    }

    #endregion
}