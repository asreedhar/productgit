using System.Data;
using System.Security;
using Csla;
using FirstLook.Common.Data;
using Environment = FirstLook.Sis.Management.DomainModel.Configuration.Environment;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    public class DealerStandardMapping : BusinessBase<DealerStandardMapping>
    {
        #region Business Methods

        private int _standardMappingId;
        private int _standardMappingValueId;

        public int StandardMappingId
        {
            get
            {
                CanReadProperty("StandardMappingId", true);
                return _standardMappingId;
            }
        }

        public int StandardMappingValueId
        {
            get
            {
                CanReadProperty("StandardMappingValueId", true);
                return _standardMappingValueId;
            }
        }

        protected override object GetIdValue()
        {
            return _standardMappingId;
        }

        #endregion

        #region Authorization Rules

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        public static bool CanAddObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        public static bool CanEditObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        public static bool CanDeleteObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        #endregion

        #region Factory Methods

        internal static DealerStandardMapping NewDealerStandardMapping(int standardMappingId, int standardMappingValueId)
        {
        	if (!CanGetObject())
                throw new SecurityException("User not authorized to view DealerStandardMapping");
                
            return new DealerStandardMapping(standardMappingId, standardMappingValueId);
        }

        internal static DealerStandardMapping GetDealerStandardMapping(IDataRecord record)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view DealerStandardMapping");
                
            return new DealerStandardMapping(record);
        }

        private DealerStandardMapping()
        {
            MarkAsChild();
        }

        private DealerStandardMapping(int standardMappingId, int standardMappingValueId)
        {
            MarkAsChild();
            _standardMappingId = standardMappingId;
            _standardMappingValueId = standardMappingValueId;
        }

        private DealerStandardMapping(IDataRecord record)
        {
            MarkAsChild();
            Fetch(record);
            MarkOld();
        }

        public override DealerStandardMapping Save()
        {
            if (IsNew && !CanAddObject())
                throw new SecurityException("User not authorized to add DealerStandardMapping");

            if (IsDeleted && !CanDeleteObject())
                throw new SecurityException("User not authorized to delete DealerStandardMapping");

            if (!CanEditObject())
                throw new SecurityException("User not authorized to edit DealerStandardMapping");

            return base.Save();
        }

        #endregion

        #region Data Access

        private void Fetch(IDataRecord record)
        {
            _standardMappingId = record.GetInt32(record.GetOrdinal("StandardMappingId"));
            _standardMappingValueId = record.GetInt32(record.GetOrdinal("StandardMappingValueId"));
        }

        internal void Insert(IDataConnection connection, IDbTransaction transaction, int dealerId, Environment environment)
        {
            if (!IsDirty) return;

            DoInsertUpdate(connection, transaction, "Transform.DealerStandardMapping#AddAssignment", dealerId, environment);

            MarkOld();
        }

        internal void Update(IDataConnection connection, IDbTransaction transaction, int dealerId, Environment environment)
        {
            if (!IsDirty) return;

            if (IsNew) return;

            DoInsertUpdate(connection, transaction, "Transform.DealerStandardMapping#Update", dealerId, environment);

            MarkOld();
        }

        internal void DeleteSelf(IDataConnection connection, IDbTransaction transaction, int dealerId, Environment environment)
        {
            if (!IsDirty) return;

            if (IsNew) return;

            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = "Transform.DealerStandardMapping#DeleteAssignment";
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;

                command.AddParameterWithValue("StandardMappingId", DbType.Guid, false, _standardMappingId);
                command.AddParameterWithValue("StandardMappingValueId", DbType.Guid, false, _standardMappingValueId);
                command.AddParameterWithValue("DealerId", DbType.Int32, false, dealerId);
                command.AddParameterWithValue("EnvironmentId", DbType.Int32, false, (int)environment);

                command.ExecuteNonQuery();
            }


            MarkNew();
        }

        private void DoInsertUpdate(IDataConnection connection, IDbTransaction transaction, string commandText, int dealerId, Environment environment)
        {
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = commandText;
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;

                command.AddParameterWithValue("StandardMappingId", DbType.Guid, false, _standardMappingId);
                command.AddParameterWithValue("DealerId", DbType.Int32, false, dealerId);
                command.AddParameterWithValue("EnvironmentId", DbType.Int32, false, (int)environment);

                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}