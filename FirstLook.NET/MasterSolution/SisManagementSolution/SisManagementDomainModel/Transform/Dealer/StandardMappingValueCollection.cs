using System.Data;
using System.Security;
using Csla;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    public class StandardMappingValueCollection : ReadOnlyListBase<StandardMappingValueCollection, StandardMappingValue>
    {
        #region Authorization Methods

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Factory Methods

        public static StandardMappingValueCollection GetStandardMappingValues(IDataReader reader)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view Standard Mapping Values");

            return new StandardMappingValueCollection(reader);
        }

        private StandardMappingValueCollection(IDataReader reader)
        {
            Fetch(reader);
        }

        #endregion

        #region Data Access

        private void Fetch(IDataReader record)
        {
            IsReadOnly = false;

            Add(StandardMappingValue.GetStandardMappingValue(record));

            IsReadOnly = true;
        }

        #endregion
    }
}
