using System;
using System.Data;
using System.Security;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    public class StandardMapping : ReadOnlyBase<StandardMapping>
    {
        #region Business Methods

        private int _id;
        private string _name;
        private StandardMappingValueCollection _values;

        public int Id
        {
            get
            {
                CanReadProperty("Id", true);
                return _id;
            }
        }

        public string Number
        {
            get
            {
                CanReadProperty("Name", true);
                return _name;
            }
        }

        public StandardMappingValueCollection Values
        {
            get
            {
                CanReadProperty("Values", true);
                return _values;
            }
        }

        protected override object GetIdValue()
        {
            return _id;
        }

        #endregion

        #region Authorization Rules

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Factory Methods

        public static StandardMapping GetStandardMapping(int id)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view Standard Mapping");

            return DataPortal.Fetch<StandardMapping>(new Criteria(id));
        }

        internal static StandardMapping GetStandardMapping(IDataReader reader)
        {
            return new StandardMapping(reader);
        }

        private StandardMapping()
        {
            /* Force use orf factory methods */
        }

        private StandardMapping(IDataReader record)
        {
            Fetch(record);
        }

        #endregion

        #region Data Access

        [Serializable]
        private class Criteria
        {
            private readonly int id;

            public Criteria(int id)
            {
                this.id = id;
            }

            public int Id
            {
                get { return id; }
            }
        }

        private void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Transfrom.StandardMapping#Fetch";

                    command.AddParameterWithValue("Id", DbType.Int32, false, criteria.Id);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        Fetch(reader);
                    }
                }
            }
        }

        private void Fetch(IDataReader reader)
        {
            if (reader.Read())
            {
                _id = reader.GetInt32(reader.GetOrdinal("Id"));
                _name = reader.GetString(reader.GetOrdinal("Name"));

                if (reader.NextResult() && reader.Read())
                {
                    _values = StandardMappingValueCollection.GetStandardMappingValues(reader);
                }
                else
                {
                    throw new Exception("Missing Standard Mapping Values dataset.");
                }
            }
            else
            {
                throw new DataException("Missing Standard Mapping dataset");
            }
        }

        #endregion
    }
}
