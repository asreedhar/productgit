using System;
using System.Data;

using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    [Serializable]
    public class TradePurchaseTransformationStockNumberLogic : BusinessBase<TradePurchaseTransformationStockNumberLogic>
    {
        #region Business Methods

        private Guid id;
        private string stockPatternType;
        private string stockPatternText;
        private int evaluationOrder;

        public Guid Id
        {
            get
            {
                CanReadProperty("Id", true);
                return id;
            }
            set
            {
                CanWriteProperty("Id", true);
                id = value;
                PropertyHasChanged("Id");
            }
        }
        public string StockPatternType
        {
            get
            {
                CanReadProperty("StockPatternType", true);
                return stockPatternType;
            }
            set
            {
                CanWriteProperty("StockPatternType", true);
                stockPatternType = value;
                PropertyHasChanged("StockPatternType");
            }
        }
        public string StockPatternText
        {
            get
            {
                CanReadProperty("StockPatternText", true);
                return stockPatternText;
            }
            set
            {
                CanWriteProperty("StockPatternText", true);
                stockPatternText = value;
                PropertyHasChanged("StockPatternText");
            }
        }
        public int EvaluationOrder
        {
            get
            {
                CanReadProperty("EvaluationOrder", true);
                return evaluationOrder;
            }
            set
            {
                CanWriteProperty("EvaluationOrder", true);
                evaluationOrder = value;
                PropertyHasChanged("EvaluationOrder");
            }
        }

        protected override object GetIdValue()
        {
            return id;
        }

        #endregion

        #region Factory Methods

        internal static TradePurchaseTransformationStockNumberLogic GetTradePurchaseTransformationStockNumberLogic(IDataRecord record)
        {
            return new TradePurchaseTransformationStockNumberLogic(record);
        }

        public static TradePurchaseTransformationStockNumberLogic NewTradePurchaseTransformationStockNumberLogic()
        {
            return DataPortal.Create<TradePurchaseTransformationStockNumberLogic>();
        }

        private TradePurchaseTransformationStockNumberLogic()
        {
            MarkNew();
            MarkAsChild();
        }

        private TradePurchaseTransformationStockNumberLogic(IDataRecord record)
        {
            Fetch(record);
            MarkOld();
            MarkAsChild();
        }

        #endregion

        #region Validation Rules

        public override bool IsValid
        {
            get
            {
                return (stockPatternText != "" && stockPatternType != "");
            }
        }

        #endregion Validation Rules

        #region Authorization

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Data Access

        protected override void DataPortal_Create()
        {
            id = Guid.NewGuid();
        }

        protected void Fetch(IDataRecord record)
        {
            id = record.GetGuid(record.GetOrdinal("Id"));
            stockPatternType = record.GetString(record.GetOrdinal("StockPatternType"));
            stockPatternText = record.GetString(record.GetOrdinal("StockPattern"));
            evaluationOrder = record.GetInt32(record.GetOrdinal("EvaluationOrder"));
        }

        internal void Insert(IDataConnection connection, IDbTransaction transaction, Configuration.Adp.Dealer dealer)
        {
            if (!IsDirty) return;

            DoInsertUpdate(connection, transaction, "Transform.TradePurchaseTransformationStockNumberLogic#Insert", dealer, true);

            MarkOld();
        }

        internal void Update(IDataConnection connection, IDbTransaction transaction, Configuration.Adp.Dealer dealer)
        {
            if (!IsDirty) return;

            if (IsNew) return;

            DoInsertUpdate(connection, transaction, "Transform.TradePurchaseTransformationStockNumberLogic#Update", dealer, false);

            MarkOld();
        }

        private void DoInsertUpdate(IDataConnection connection, IDbTransaction transaction, string commandText, Configuration.Adp.Dealer dealer, bool insert)
        {

            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = commandText;
                command.CommandType = CommandType.StoredProcedure;

                if (insert)
                {
                    command.AddParameterWithValue("DealerId", DbType.Int32, false, dealer.Id);
                    command.AddParameterWithValue("EnvironmentId", DbType.Int32, false, (int)dealer.Environment);
                }
                else
                {
                    command.AddParameterWithValue("Id", DbType.Guid, false, id);
                }
                    
                command.AddParameterWithValue("StockPatternType", DbType.String, false, stockPatternType);
                command.AddParameterWithValue("EvaluationOrder", DbType.Int32, false, evaluationOrder);
                command.AddParameterWithValue("StockPattern", DbType.String, false, stockPatternText);

                command.Transaction = transaction;
                command.ExecuteNonQuery();
            }
        }

        internal void DeleteSelf(IDataConnection connection, IDbTransaction transaction, Configuration.Adp.Dealer dealer)
        {
            if (!IsDirty) return;

            if (IsNew) return;

            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = "Transform.TradePurchaseTransformationStockNumberLogic#Delete";
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;
                command.AddParameterWithValue("Id", DbType.Guid, false, id);
                command.ExecuteNonQuery();
            }

            MarkNew();
        }

        #endregion
    }
}