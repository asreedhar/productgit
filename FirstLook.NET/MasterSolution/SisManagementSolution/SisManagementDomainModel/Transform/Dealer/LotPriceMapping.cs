using System.Data;
using Csla;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    public class LotPriceMapping : Mapping
    {
        #region Factory Methods

        internal static LotPriceMapping GetLotPriceMapping(IDataReader reader)
        {
            return new LotPriceMapping(reader);
        }

        public static LotPriceMapping GetLotPriceMapping(int id)
        {
            return DataPortal.Fetch<LotPriceMapping>(new Criteria(id));
        }

        private LotPriceMapping()
        {
            /* Force use of factory methods */
        }

        private LotPriceMapping(IDataRecord record)
        {
            Fetch(record);
        }

        #endregion
    }
}
