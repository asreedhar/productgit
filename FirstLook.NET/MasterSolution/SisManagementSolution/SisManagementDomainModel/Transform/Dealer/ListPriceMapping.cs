using System.Data;
using Csla;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    public class ListPriceMapping : Mapping
    {
        #region Factory Methods

        internal static ListPriceMapping GetListPriceMapping(IDataRecord record)
        {
            return new ListPriceMapping(record);
        }

        public static ListPriceMapping GetListPriceMapping(int id)
        {
            return DataPortal.Fetch<ListPriceMapping>(new Criteria(id));
        }

        private ListPriceMapping()
        {
            /* Force use of factory methods */
        }

        private ListPriceMapping(IDataRecord record)
        {
            Fetch(record);
        }

        #endregion
    }
}
