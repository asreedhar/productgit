using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    public enum UnitCostTransformationThresholdOperator
    {
        GTE,
        LTE
    }
}