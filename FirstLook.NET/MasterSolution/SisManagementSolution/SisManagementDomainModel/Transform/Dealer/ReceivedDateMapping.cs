using System.Data;
using Csla;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    public class ReceivedDateMapping : Mapping
    {
        #region Factory Methods

        internal static ReceivedDateMapping GetReceivedDateMapping(IDataReader reader)
        {
            return new ReceivedDateMapping(reader);
        }

        public static ReceivedDateMapping GetReceivedDateMapping(int id)
        {
            return DataPortal.Fetch<ReceivedDateMapping>(new Criteria(id));
        }

        private ReceivedDateMapping()
        {
            /* Force use of factory methods */
        }

        private ReceivedDateMapping(IDataRecord record)
        {
            Fetch(record);
        }

        #endregion
    }
}
