using System;
using System.Data;
using System.Security;
using Csla;
using Csla.Validation;
using FirstLook.Common.Data;
using FirstLook.Sis.Management.DomainModel.Configuration.Adp;
using TimeZone=System.TimeZone;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    public class DealerUnitCostTransformation : BusinessBase<DealerUnitCostTransformation>
    {
        #region Business Methods

        private Configuration.Adp.Dealer dealer;
        private UnitCostTransformation unitCostTransformation;
        
        public Configuration.Adp.Dealer Dealer
        {
            get
            {
                CanReadProperty("Dealer", true);
                return dealer;
            }
        }

        public UnitCostTransformation UnitCostTransformation
        {
            get
            {
                CanReadProperty("UnitCostTransformation", true);
                return unitCostTransformation;
            }
            set
            {
                CanWriteProperty("UnitCostTransformation", true);
                unitCostTransformation = value;
                PropertyHasChanged("UnitCostTransformation");
            }
        }

        protected override object GetIdValue()
        {
            return dealer.Id;
        }

        #endregion

        #region Authorization Rules

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        public static bool CanAddObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        public static bool CanEditObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        #endregion

        #region Validation Rules

        public override bool IsValid
        {
            get
            {
                return unitCostTransformation != null && unitCostTransformation.IsValid;
            }
        }

        protected override void AddBusinessRules()
        {
            ValidationRules.AddRule(UnitCostTransformationValidation, "UnitCostTransformation");
        }

        protected static bool UnitCostTransformationValidation(object target, RuleArgs ruleArgs)
        {
            bool isValid = false;
            UnitCostTransformation uct = ((DealerUnitCostTransformation)target).UnitCostTransformation;
            if (uct == null)
            {
                ruleArgs.Description = "UnitCostTransformation cannot be null.";
            }
            else
            {
                isValid = (uct.IsValid);
                if (!isValid)
                {
                    ruleArgs.Description = "UnitCostTransformation must choose either 'Use DMS Pack' or 'Use Dealer Defined Pack Amount', and must specify at least one type of deal when choosing Dealer Defined.";
                }
            }
            return isValid;
        }

        #endregion

        #region Factory Methods

        public static DealerUnitCostTransformation GetDealerUnitCostTransformation(Configuration.Adp.Dealer dealer)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view DealerUnitCostTransformation");

            return DataPortal.Fetch<DealerUnitCostTransformation>(new Criteria(dealer));
        }

        public override DealerUnitCostTransformation Save()
        {
            if (IsNew && !CanAddObject())
                throw new SecurityException("User not authorized to add DealerUnitCostTransformation");

            if (!CanEditObject())
                throw new SecurityException("User not authorized to edit DealerUnitCostTransformation");

            MarkDirty();
            return base.Save();
        }

        #endregion

        #region Data Access

        [Serializable]
        class Criteria
        {
            private readonly Configuration.Adp.Dealer dealer;

            public Criteria(Configuration.Adp.Dealer dealer)
            {
                this.dealer = dealer;
            }

            public Configuration.Adp.Dealer Dealer
            {
                get { return dealer; }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called by CSLA using reflection")]
        [Transactional(TransactionalTypes.Manual)]
        private void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Transform.DealerUnitCostTransformation#Fetch";
                    command.AddParameterWithValue("DealerId", DbType.Int32, true, criteria.Dealer.Id);
                    command.AddParameterWithValue("EnvironmentId", DbType.Int32, true, (int)criteria.Dealer.Environment);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        dealer = criteria.Dealer;
                        if (reader.Read())
                        {
                           unitCostTransformation = Transform.Dealer.UnitCostTransformation.GetUnitCostTransformation(reader);
                        }
                    }
                }
            }
            ValidationRules.CheckRules();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called by CSLA using reflection")]
        protected override void DataPortal_Insert()
        {
            InsertUpdate();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called by CSLA using reflection")]
        protected override void DataPortal_Update()
        {
            InsertUpdate();
        }


        protected void InsertUpdate()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Transform.DealerUnitCostTransformation#Update";

                    command.AddParameterWithValue("DealerId", DbType.Int32, false, dealer.Id);
                    command.AddParameterWithValue("EnvironmentId", DbType.Int32, true, (int)dealer.Environment);

                    if (unitCostTransformation.UseDMSPACK)
                    {
                        command.AddParameterWithValue("UseDMSPack", DbType.Boolean, false, unitCostTransformation.UseDMSPACK);
                        command.AddParameterWithValue("UseStaticPack", DbType.Boolean, false, false);
                        command.AddParameterWithValue("StaticPackAmount", DbType.Decimal, false, null);
                        //command.AddParameterWithValue("ApplyToRetail", DbType.Boolean, false, false);
                        //command.AddParameterWithValue("ApplyToWholesale", DbType.Boolean, false, false);
                        //command.AddParameterWithValue("ApplyToTrade", DbType.Boolean, false, false);
                        //command.AddParameterWithValue("ApplyToPurchase", DbType.Boolean, false, false);
                        //command.AddParameterWithValue("UnitCostGTEThreshold", DbType.Decimal, true, null);
                        //command.AddParameterWithValue("UnitCostLTEThreshold", DbType.Decimal, true, null);
                        //command.AddParameterWithValue("AgeInDaysGTEThreshold", DbType.Decimal, true, null);
                        //command.AddParameterWithValue("AgeInDaysLTEThreshold", DbType.Decimal, true, null);
                    }
                    else
                    {
                        command.AddParameterWithValue("UseDMSPack", DbType.Boolean, false, unitCostTransformation.UseDMSPACK);
                        command.AddParameterWithValue("UseStaticPack", DbType.Boolean, false, unitCostTransformation.UseStaticPack);
                        command.AddParameterWithValue("StaticPackAmount", DbType.Decimal, false, unitCostTransformation.StaticPackAmount);
                    }
                    command.AddParameterWithValue("ApplyToRetail", DbType.Boolean, false, unitCostTransformation.ApplyToRetail);
                    command.AddParameterWithValue("ApplyToWholesale", DbType.Boolean, false, unitCostTransformation.ApplyToWholesale);
                    command.AddParameterWithValue("ApplyToTrade", DbType.Boolean, false, unitCostTransformation.ApplyToTrade);
                    command.AddParameterWithValue("ApplyToPurchase", DbType.Boolean, false, unitCostTransformation.ApplyToPurchase);

                    if (unitCostTransformation.UnitCostThreshold == null)
                    {
                        command.AddParameterWithValue("UnitCostGTEThreshold", DbType.Decimal, true, null);
                        command.AddParameterWithValue("UnitCostLTEThreshold", DbType.Decimal, true, null);
                    }
                    else
                    {
                        if (unitCostTransformation.UnitCostThreshold.Operator == UnitCostTransformationThresholdOperator.GTE)
                        {
                            command.AddParameterWithValue("UnitCostGTEThreshold", DbType.Decimal, true, Convert.ToDecimal(unitCostTransformation.UnitCostThreshold.Amount));
                            command.AddParameterWithValue("UnitCostLTEThreshold", DbType.Decimal, true, null);
                        }
                        else
                        {
                            command.AddParameterWithValue("UnitCostGTEThreshold", DbType.Decimal, true, null);
                            command.AddParameterWithValue("UnitCostLTEThreshold", DbType.Decimal, true, Convert.ToDecimal(unitCostTransformation.UnitCostThreshold.Amount));
                        }
                    }

                    if (unitCostTransformation.AgeInDaysThreshold == null)
                    {
                        command.AddParameterWithValue("AgeInDaysGTEThreshold", DbType.Int32, true, null);
                        command.AddParameterWithValue("AgeInDaysLTEThreshold", DbType.Int32, true, null);
                    }
                    else
                    {
                        if (unitCostTransformation.AgeInDaysThreshold.Operator == UnitCostTransformationThresholdOperator.GTE)
                        {
                            command.AddParameterWithValue("AgeInDaysGTEThreshold", DbType.Int32, true, Convert.ToInt32(unitCostTransformation.AgeInDaysThreshold.Amount));
                            command.AddParameterWithValue("AgeInDaysLTEThreshold", DbType.Int32, true, null);
                        }
                        else
                        {
                            command.AddParameterWithValue("AgeInDaysGTEThreshold", DbType.Int32, true, null);
                            command.AddParameterWithValue("AgeInDaysLTEThreshold", DbType.Int32, true, Convert.ToInt32(unitCostTransformation.AgeInDaysThreshold.Amount));
                        }
                    }
                    //}

                    command.ExecuteNonQuery();
                }
            }
        }

        #endregion
    }
}