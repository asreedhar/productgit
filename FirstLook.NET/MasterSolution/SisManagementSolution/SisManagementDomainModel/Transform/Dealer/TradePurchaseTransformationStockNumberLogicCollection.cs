using System;
using System.Data;
using System.Security;

using Csla;
using Csla.Validation;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    public class TradePurchaseTransformationStockNumberLogicCollection : BusinessListBase<TradePurchaseTransformationStockNumberLogicCollection, TradePurchaseTransformationStockNumberLogic>
    {
        #region Business Methods

        public TradePurchaseTransformationStockNumberLogic GetItem(Guid Id)
        {
            foreach (TradePurchaseTransformationStockNumberLogic item in this)
                if (item.Id.Equals(Id))
                    return item;
            return null;
        }

        public void Assign(TradePurchaseTransformationStockNumberLogic item)
        {
            if (!Contains(item.Id))
            {
                Add(item);
            }
        }

        public void Remove(Guid id)
        {
            foreach (TradePurchaseTransformationStockNumberLogic item in this)
            {
                if (item.Id == id)
                {
                    Remove(item);
                    break;
                }
            }
        }

        public bool Contains(Guid id)
        {
            foreach (TradePurchaseTransformationStockNumberLogic item in this)
            {
                if (item.Id == id)
                {
                    return true;
                }
            }
            return false;
        }
        
        #endregion

        #region Authorization Methods

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Validation Rules

        public override bool IsValid
        {
            get
            {
                foreach (TradePurchaseTransformationStockNumberLogic logic in this)
                {
                    if(!logic.IsValid)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        #endregion

        #region Factory Methods

        private TradePurchaseTransformationStockNumberLogicCollection(IDataReader reader)
        {
            MarkAsChild();
            Fetch(reader);
        }
        
        public static TradePurchaseTransformationStockNumberLogicCollection NewTradePurchaseTransformationStockNumberLogicCollection()
        {
            return DataPortal.Create<TradePurchaseTransformationStockNumberLogicCollection>();
        }

        public static TradePurchaseTransformationStockNumberLogicCollection GetTradePurchaseTransformationStockNumberLogicCollection(IDataReader reader)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view Mappings");

            return new TradePurchaseTransformationStockNumberLogicCollection(reader);
        }

        #endregion

        #region Data Access

        protected override void DataPortal_Create()
        {

        }

        private void Fetch(IDataReader reader)
        {
            while (reader.Read())
            {
                Add(TradePurchaseTransformationStockNumberLogic.GetTradePurchaseTransformationStockNumberLogic(reader));
            }
        }

        internal void Update(Configuration.Adp.Dealer dealer, IDataConnection connection, IDbTransaction transaction)
        {
            this.RaiseListChangedEvents = false;

            foreach (TradePurchaseTransformationStockNumberLogic item in DeletedList)
            {
                item.DeleteSelf(connection, transaction, dealer);
            }
            DeletedList.Clear();

            // add/update any current child objects
            foreach (TradePurchaseTransformationStockNumberLogic item in this)
            {
                if (item.IsNew)
                {
                    item.Insert(connection, transaction, dealer);
                }
                else if(item.IsDirty)
                {
                    item.Update(connection, transaction, dealer);
                }
            }

            this.RaiseListChangedEvents = true;
        }

        #endregion
    }
}
