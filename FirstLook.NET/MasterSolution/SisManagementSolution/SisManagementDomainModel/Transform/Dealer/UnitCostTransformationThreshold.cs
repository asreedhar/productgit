namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    public class UnitCostTransformationThreshold
    {
        private UnitCostTransformationThresholdOperator _Operator;
        private object _Amount;

        public UnitCostTransformationThresholdOperator Operator
        {
            get
            {
                return _Operator;
            }
            set
            {
                _Operator = value;
            }
        }
        public object Amount
        {
            get
            {
                return _Amount;
            }
            set
            {
                _Amount = value;
            }
        }


        internal UnitCostTransformationThreshold()
        {

        }


        public static UnitCostTransformationThreshold NewUnitCostTransformationThreshold()
        {
            return new UnitCostTransformationThreshold();
        }

        internal UnitCostTransformationThreshold(UnitCostTransformationThresholdOperator op, object amount)
        {
            _Operator = op;
            _Amount = amount;
        }
    }
}