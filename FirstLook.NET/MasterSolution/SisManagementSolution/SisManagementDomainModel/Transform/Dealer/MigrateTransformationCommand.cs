using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

using Environment=FirstLook.Sis.Management.DomainModel.Configuration.Environment;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    [Serializable]
    public class MigrateTransformationCommand : CommandBase
    {
        private int _dealerId;
        private Environment _source;
        private Environment _destination;

        public static void Execute(int dealerId, Environment source, Environment destination)
        {
            DataPortal.Execute<MigrateTransformationCommand>(new MigrateTransformationCommand(dealerId, source, destination));
        }

        private MigrateTransformationCommand()
        { /* force factory methods */}

        private MigrateTransformationCommand(int dealerId, Environment source, Environment destination)
        {
            this._dealerId = dealerId;
            this._source = source;
            this._destination = destination;
        }

        [Transactional(TransactionalTypes.Manual)]
        protected override void DataPortal_Execute()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.CommandText = "Transform.DealerConfiguration#Copy";
                            command.Transaction = transaction;
                            command.AddParameterWithValue("DealerId", DbType.Int32, false, _dealerId);
                            command.AddParameterWithValue("SourceEnvironmentId", DbType.Int32, false, (int)_source);
                            command.AddParameterWithValue("DestinationEnvironmentId", DbType.Int32, false, (int)_destination);
                            command.ExecuteNonQuery();
                        }
                        transaction.Commit();
                    }
                    catch
                    {
                        try
                        {
                            transaction.Rollback();
                        }
                        catch
                        {
                            throw;
                        }
                        throw;
                    }
                }
            }
        }
    }
}