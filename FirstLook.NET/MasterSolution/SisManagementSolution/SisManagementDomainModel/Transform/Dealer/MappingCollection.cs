using System;
using System.Collections.Generic;
using System.Data;
using System.Security;
using System.Text;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    public abstract class MappingCollection<T,C> : ReadOnlyListBase<T,C> where T : ReadOnlyListBase<T,C> where C : Mapping
    {
        #region Business Methods

        internal abstract string Name { get; }

        #endregion

        #region Authorization Methods

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Factory Methods

        public static T GetMappings()
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view Mappings");

            return DataPortal.Fetch<T>();
        }

        #endregion

        #region Data Access

        internal abstract void AddItems(IDataReader reader);

        private void DataPortal_Fetch()
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Transform.MappingCollection#Fetch";

                    command.AddParameterWithValue("Name", DbType.String, false, Name);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        IsReadOnly = false;
                        AddItems(reader);
                        IsReadOnly = true;
                    }
                }
            }

            RaiseListChangedEvents = true;
        }
        

        #endregion
    }
}
