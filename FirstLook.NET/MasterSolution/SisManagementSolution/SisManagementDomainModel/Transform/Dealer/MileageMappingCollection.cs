using System.Data;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    public class MileageMappingCollection : MappingCollection<MileageMappingCollection, MileageMapping>
    {
        #region Business Methods

        internal override string Name
        {
            get { return "Mileage"; }
        }

        #endregion

        #region Data Access

        internal override void AddItems(IDataReader reader)
        {
            while (reader.Read())
            {
                Add(MileageMapping.GetMileageMapping(reader));
            }
        }

        #endregion
    }
}
