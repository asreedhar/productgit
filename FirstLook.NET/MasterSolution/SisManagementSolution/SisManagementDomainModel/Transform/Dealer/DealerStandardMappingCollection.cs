using System;
using System.Data;
using System.Security;
using Csla;
using FirstLook.Common.Data;
using Environment = FirstLook.Sis.Management.DomainModel.Configuration.Environment;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    public class DealerStandardMappingCollection : BusinessListBase<DealerStandardMappingCollection, DealerStandardMapping>
    {
        #region Business Methods

        private int _dealerId;
        private Environment _environment;

        public DealerStandardMapping GetItem(int Id)
        {
            foreach (DealerStandardMapping dealerStandardMapping in this)
                if (dealerStandardMapping.StandardMappingId.Equals(Id))
                    return dealerStandardMapping;
            return null;
        }

        public void Assign(int standardMappingId, int standardMappingValueId)
        {
            if (!Contains(standardMappingId, standardMappingValueId))
            {
                if (!ContainsDeleted(standardMappingId, standardMappingValueId))
                {
                    Add(DealerStandardMapping.NewDealerStandardMapping(standardMappingId, standardMappingValueId));
                }
                else
                {
                    DealerStandardMapping deleted = GetDeletedItem(standardMappingId, standardMappingValueId);
                    Add(deleted);
                    DeletedList.Remove(deleted);
                }
            }
            else
            {
                throw new InvalidOperationException("dealerStandardMapping already assigned to Dealer");
            }
        }

        public void Remove(int Id)
        {
            foreach (DealerStandardMapping dealerStandardMapping in this)
            {
                if (dealerStandardMapping.StandardMappingId.Equals(Id))
                {
                    Remove(dealerStandardMapping);
                    break;
                }
            }
        }

        public bool Contains(int standardMappingId, int standardMappingValueId)
        {
            foreach (DealerStandardMapping dealerStandardMapping in this)
                if (dealerStandardMapping.StandardMappingId.Equals(standardMappingId) 
                    && dealerStandardMapping.StandardMappingValueId.Equals(standardMappingValueId))
                    return true;
            return false;
        }

        public bool Contains(int Id)
        {
            foreach (DealerStandardMapping dealerStandardMapping in this)
                if (dealerStandardMapping.StandardMappingId.Equals(Id))
                    return true;
            return false;
        }

        public bool ContainsDeleted(int Id)
        {
            foreach (DealerStandardMapping dealerStandardMapping in DeletedList)
                if (dealerStandardMapping.StandardMappingId.Equals(Id))
                    return true;
            return false;
        }

        public bool ContainsDeleted(int standardMappingId, int standardMappingValueId)
        {
            foreach (DealerStandardMapping dealerStandardMapping in DeletedList)
                if (dealerStandardMapping.StandardMappingId.Equals(standardMappingId)
                    && dealerStandardMapping.StandardMappingValueId.Equals(standardMappingValueId))
                    return true;
            return false;
        }

        private DealerStandardMapping GetDeletedItem(int Id)
        {
            foreach (DealerStandardMapping dealerStandardMapping in DeletedList)
                if (dealerStandardMapping.StandardMappingId.Equals(Id))
                    return dealerStandardMapping;
            return null;
        }

        private DealerStandardMapping GetDeletedItem(int standardMappingId, int standardMappingValueId)
        {
            foreach (DealerStandardMapping dealerStandardMapping in DeletedList)
                if (dealerStandardMapping.StandardMappingId.Equals(standardMappingId)
                    && dealerStandardMapping.StandardMappingValueId.Equals(standardMappingValueId))
                    return dealerStandardMapping;
            return null;
        }

        #endregion

        #region Authorization Methods

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Factory Methods

        internal static DealerStandardMappingCollection GetDealerStandardMappings(int dealerId, Environment environment, int dealerStandardMappingingLogonId)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to retrieve DealerStandardMappings");

            return DataPortal.Fetch<DealerStandardMappingCollection>(new FetchCriteria(dealerId, environment));
        }

        private DealerStandardMappingCollection()
        {
            MarkAsChild();
        }

        #endregion

        #region Data Access

        [Serializable]
        private class FetchCriteria
        {
            private readonly int dealerId;
            private readonly Environment environment;

            public FetchCriteria(int dealerId, Environment environment)
            {
                this.dealerId = dealerId;
                this.environment = environment;
            }

            public int DealerId
            {
                get { return dealerId; }
            }

            public Environment Environment
            {
                get { return environment; }
            }
        }

        private void DataPortal_Fetch(FetchCriteria criteria)
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Transform.DealerStandardMappingCollection#Fetch";

                    command.AddParameterWithValue("DealerId", DbType.Int32, false, criteria.DealerId);
                    command.AddParameterWithValue("EnvironmentId", DbType.Int32, false, (int)criteria.Environment);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Add(DealerStandardMapping.GetDealerStandardMapping(reader));
                        }

                        _dealerId = criteria.DealerId;
                        _environment = criteria.Environment;
                    }
                }
            }
            RaiseListChangedEvents = true;
        }

        internal void Update(IDataConnection connection, IDbTransaction transaction, int dealerId, Environment environment)
        {
            RaiseListChangedEvents = false;

            foreach (DealerStandardMapping item in DeletedList)
            {
                item.DeleteSelf(connection, transaction, _dealerId, _environment);
            }
            DeletedList.Clear();

            // add/update any current child objects
            foreach (DealerStandardMapping item in this)
            {
                if (item.IsNew)
                {
                    item.Insert(connection, transaction, _dealerId, _environment);
                }
                else
                {
                    item.Update(connection, transaction, _dealerId, _environment);
                }
            }

            RaiseListChangedEvents = true;
        }

        #endregion
    }
}
