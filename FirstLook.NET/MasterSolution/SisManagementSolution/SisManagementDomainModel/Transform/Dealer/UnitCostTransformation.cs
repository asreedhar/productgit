using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    public class UnitCostTransformation : BusinessBase<UnitCostTransformation>
    {
        #region Business Methods

        private bool _UseDMSPACK;
        private bool _UseStaticPack;
        private decimal _StaticPackAmount;
        private bool _ApplyToRetail;
        private bool _ApplyToWholesale;
        private bool _ApplyToTrade;
        private bool _ApplyToPurchase;
        private UnitCostTransformationThreshold _UnitCostThreshold;
        private UnitCostTransformationThreshold _AgeInDaysThreshold;


        public bool UseDMSPACK
        {
            get
            {
                CanReadProperty("UseDMSPACK", true);
                return _UseDMSPACK;
            }
            set
            {
                CanWriteProperty("UseDMSPACK", true);
                _UseDMSPACK = value;
                PropertyHasChanged("UseDMSPACK");
            }
        }
        public bool UseStaticPack
        {
            get
            {
                CanReadProperty("UseStaticPack", true);
                return _UseStaticPack;
            }
            set
            {
                CanWriteProperty("UseStaticPack", true);
                _UseStaticPack = value;
                PropertyHasChanged("UseStaticPack");
            }
        }
        public decimal StaticPackAmount
        {
            get
            {
                CanReadProperty("StaticPackAmount", true);
                return _StaticPackAmount;
            }
            set
            {
                CanWriteProperty("StaticPackAmount", true);
                _StaticPackAmount = value;
                PropertyHasChanged("StaticPackAmount");
            }
        }
        public bool ApplyToRetail
        {
            get
            {
                CanReadProperty("ApplyToRetail", true);
                return _ApplyToRetail;
            }
            set
            {
                CanWriteProperty("ApplyToRetail", true);
                _ApplyToRetail = value;
                PropertyHasChanged("ApplyToRetail");
            }
        }
        public bool ApplyToWholesale
        {
            get
            {
                CanReadProperty("ApplyToWholesale", true);
                return _ApplyToWholesale;
            }
            set
            {
                CanWriteProperty("ApplyToWholesale", true);
                _ApplyToWholesale = value;
                PropertyHasChanged("ApplyToWholesale");
            }
        }
        public bool ApplyToTrade
        {
            get
            {
                CanReadProperty("ApplyToTrade", true);
                return _ApplyToTrade;
            }
            set
            {
                CanWriteProperty("ApplyToTrade", true);
                _ApplyToTrade = value;
                PropertyHasChanged("ApplyToTrade");
            }
        }
        public bool ApplyToPurchase
        {
            get
            {
                CanReadProperty("ApplyToPurchase", true);
                return _ApplyToPurchase;
            }
            set
            {
                CanWriteProperty("ApplyToPurchase", true);
                _ApplyToPurchase = value;
                PropertyHasChanged("ApplyToPurchase");
            }
        }
        public UnitCostTransformationThreshold UnitCostThreshold
        {
            get
            {
                CanReadProperty("UnitCostThreshold", true);
                return _UnitCostThreshold;
            }
            set
            {
                CanWriteProperty("UnitCostThreshold", true);
                _UnitCostThreshold = value;
                PropertyHasChanged("UnitCostThreshold");
            }
        }
        public UnitCostTransformationThreshold AgeInDaysThreshold
        {
            get
            {
                CanReadProperty("AgeInDaysThreshold", true);
                return _AgeInDaysThreshold;
            }
            set
            {
                CanWriteProperty("AgeInDaysThreshold", true);
                _AgeInDaysThreshold = value;
                PropertyHasChanged("AgeInDaysThreshold");
            }
        }

        public bool UsesAllDeals
        {
            get
            {
                return
                    ApplyToPurchase
                    && ApplyToRetail
                    && ApplyToTrade
                    && ApplyToWholesale
                    && UnitCostThreshold != null
                    && UnitCostThreshold.Operator == UnitCostTransformationThresholdOperator.GTE
                    && (decimal)UnitCostThreshold.Amount == 0
                    && AgeInDaysThreshold != null
                    && AgeInDaysThreshold.Operator == UnitCostTransformationThresholdOperator.GTE
                    && (int)AgeInDaysThreshold.Amount == 0;
            }
        }

        protected override object GetIdValue()
        {
            return null;
        }

        #endregion

        #region Validation Rules

        public override bool IsValid
        {
            get
            {
                return !(!ApplyToPurchase && !ApplyToRetail && !ApplyToTrade && !ApplyToWholesale && AgeInDaysThreshold == null && UnitCostThreshold == null);
            }
        }

        #endregion

        #region Factory Methods
        
        internal static UnitCostTransformation GetUnitCostTransformation(IDataReader reader)
        {
            return new UnitCostTransformation(reader);
        }

        private UnitCostTransformation()
        {

        }

        private UnitCostTransformation(IDataRecord record)
        {
            Fetch(record);
        }

        public static UnitCostTransformation NewUnitCostTransformation()
        {
            return DataPortal.Create<UnitCostTransformation>();
        }

        #endregion

        #region Authorization

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Data Access

        protected override void DataPortal_Create()
        {
            
        }

        protected void Fetch(IDataRecord reader)
        {
            _UseDMSPACK = reader.GetBoolean(reader.GetOrdinal("UseDMSPACK"));

            if (!reader.IsDBNull(reader.GetOrdinal("UseStaticPack")))
            {
                _UseStaticPack = reader.GetBoolean(reader.GetOrdinal("UseStaticPack"));
            }
            if (!reader.IsDBNull(reader.GetOrdinal("StaticPackAmount")))
            {
                _StaticPackAmount = reader.GetDecimal(reader.GetOrdinal("StaticPackAmount"));
            }

            if (!reader.IsDBNull(reader.GetOrdinal("ApplyToRetail")))
            {
                _ApplyToRetail = reader.GetBoolean(reader.GetOrdinal("ApplyToRetail"));
            }
            if (!reader.IsDBNull(reader.GetOrdinal("ApplyToWholesale")))
            {
                _ApplyToWholesale = reader.GetBoolean(reader.GetOrdinal("ApplyToWholesale"));
            }
            if (!reader.IsDBNull(reader.GetOrdinal("ApplyToTrade")))
            {
                _ApplyToTrade = reader.GetBoolean(reader.GetOrdinal("ApplyToTrade"));
            }
            if (!reader.IsDBNull(reader.GetOrdinal("ApplyToPurchase")))
            {
                _ApplyToPurchase = reader.GetBoolean(reader.GetOrdinal("ApplyToPurchase"));
            }
            

            if(!reader.IsDBNull(reader.GetOrdinal("UnitCostLTEThreshold")))
            {
                _UnitCostThreshold = new UnitCostTransformationThreshold(UnitCostTransformationThresholdOperator.LTE, reader.GetDecimal(reader.GetOrdinal("UnitCostLTEThreshold")));
            }
            else if (!reader.IsDBNull(reader.GetOrdinal("UnitCostGTEThreshold")))
            {
                _UnitCostThreshold = new UnitCostTransformationThreshold(UnitCostTransformationThresholdOperator.GTE, reader.GetDecimal(reader.GetOrdinal("UnitCostGTEThreshold")));
            }

            if (!reader.IsDBNull(reader.GetOrdinal("AgeInDaysLTEThreshold")))
            {
                _AgeInDaysThreshold = new UnitCostTransformationThreshold(UnitCostTransformationThresholdOperator.LTE, reader.GetInt32(reader.GetOrdinal("AgeInDaysLTEThreshold")));
            }
            else if (!reader.IsDBNull(reader.GetOrdinal("AgeInDaysGTEThreshold")))
            {
                _AgeInDaysThreshold = new UnitCostTransformationThreshold(UnitCostTransformationThresholdOperator.GTE, reader.GetInt32(reader.GetOrdinal("AgeInDaysGTEThreshold")));
            }
        }

        #endregion
    }
}