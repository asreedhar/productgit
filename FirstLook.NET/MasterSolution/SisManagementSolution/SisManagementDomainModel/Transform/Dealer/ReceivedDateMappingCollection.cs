using System.Data;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    public class ReceivedDateMappingCollection : MappingCollection<ReceivedDateMappingCollection, ReceivedDateMapping>
    {
        #region Business Methods

        internal override string Name
        {
            get { return "Received Date"; }
        }

        #endregion

        #region Data Access

        internal override void AddItems(IDataReader reader)
        {
            while (reader.Read())
            {
                Add(ReceivedDateMapping.GetReceivedDateMapping(reader));
            }
        }

        #endregion
    }
}
