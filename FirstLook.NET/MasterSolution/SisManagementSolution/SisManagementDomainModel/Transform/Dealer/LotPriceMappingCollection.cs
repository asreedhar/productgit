using System.Data;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    public class LotPriceMappingCollection : MappingCollection<LotPriceMappingCollection, LotPriceMapping>
    {
        #region Business Methods

        internal override string Name
        {
            get { return "Lot Price"; }
        }

        #endregion

        #region Data Access

        internal override void AddItems(IDataReader reader)
        {
            while (reader.Read())
            {
                Add(LotPriceMapping.GetLotPriceMapping(reader));
            }
        }

        #endregion
    }
}
