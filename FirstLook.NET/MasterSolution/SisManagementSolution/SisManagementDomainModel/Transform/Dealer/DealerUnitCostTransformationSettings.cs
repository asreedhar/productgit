using System;
using System.Data;
using System.Security;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    public class DealerUnitCostTransformationSettings : BusinessBase<DealerUnitCostTransformationSettings>
    {
        #region Business Methods

        private Configuration.Adp.Dealer dealer;
        private bool _UseDMSPACK;
        private bool _UseStaticPack;
        private decimal _StaticPackAmount;
        private bool _ApplyToRetail;
        private bool _ApplyToWholesale;
        private bool _ApplyToTrade;
        private bool _ApplyToPurchase;
        private UnitCostTransformationThreshold _UnitCostThreshold;
        private UnitCostTransformationThreshold _AgeInDaysThreshold;
        private bool isSet;

        public Configuration.Adp.Dealer Dealer
        {
            get
            {
                CanReadProperty("Dealer", true);
                return dealer;
            }
            set
            {
                CanWriteProperty("Dealer", true);
                dealer = value;
                PropertyHasChanged("Dealer");
            }
        }
        public bool UseDMSPACK
        {
            get
            {
                CanReadProperty("UseDMSPACK", true);
                return _UseDMSPACK;
            }
            set
            {
                CanWriteProperty("UseDMSPACK", true);
                _UseDMSPACK = value;
                PropertyHasChanged("UseDMSPACK");
            }
        }
        public bool UseStaticPack
        {
            get
            {
                CanReadProperty("UseStaticPack", true);
                return _UseStaticPack;
            }
            set
            {
                CanWriteProperty("UseStaticPack", true);
                _UseStaticPack = value;
                PropertyHasChanged("UseStaticPack");
            }
        }
        public decimal StaticPackAmount
        {
            get
            {
                CanReadProperty("StaticPackAmount", true);
                return _StaticPackAmount;
            }
            set
            {
                CanWriteProperty("StaticPackAmount", true);
                _StaticPackAmount = value;
                PropertyHasChanged("StaticPackAmount");
            }
        }
        public bool ApplyToRetail
        {
            get
            {
                CanReadProperty("ApplyToRetail", true);
                return _ApplyToRetail;
            }
            set
            {
                CanWriteProperty("ApplyToRetail", true);
                _ApplyToRetail = value;
                PropertyHasChanged("ApplyToRetail");
            }
        }
        public bool ApplyToWholesale
        {
            get
            {
                CanReadProperty("ApplyToWholesale", true);
                return _ApplyToWholesale;
            }
            set
            {
                CanWriteProperty("ApplyToWholesale", true);
                _ApplyToWholesale = value;
                PropertyHasChanged("ApplyToWholesale");
            }
        }
        public bool ApplyToTrade
        {
            get
            {
                CanReadProperty("ApplyToTrade", true);
                return _ApplyToTrade;
            }
            set
            {
                CanWriteProperty("ApplyToTrade", true);
                _ApplyToTrade = value;
                PropertyHasChanged("ApplyToTrade");
            }
        }
        public bool ApplyToPurchase
        {
            get
            {
                CanReadProperty("ApplyToPurchase", true);
                return _ApplyToPurchase;
            }
            set
            {
                CanWriteProperty("ApplyToPurchase", true);
                _ApplyToPurchase = value;
                PropertyHasChanged("ApplyToPurchase");
            }
        }
        public UnitCostTransformationThreshold UnitCostThreshold
        {
            get
            {
                CanReadProperty("UnitCostThreshold", true);
                return _UnitCostThreshold;
            }
            set
            {
                CanWriteProperty("UnitCostThreshold", true);
                _UnitCostThreshold = value;
                PropertyHasChanged("UnitCostThreshold");
            }
        }
        public UnitCostTransformationThreshold AgeInDaysThreshold
        {
            get
            {
                CanReadProperty("AgeInDaysThreshold", true);
                return _AgeInDaysThreshold;
            }
            set
            {
                CanWriteProperty("AgeInDaysThreshold", true);
                _AgeInDaysThreshold = value;
                PropertyHasChanged("AgeInDaysThreshold");
            }
        }

        public bool IsSet
        {
            get
            {
                return isSet;
            }
        }

        public bool UsesAllDeals
        {
            get
            {
                return
                    ApplyToPurchase
                    && ApplyToRetail
                    && ApplyToTrade
                    && ApplyToWholesale
                    && UnitCostThreshold != null
                    && UnitCostThreshold.Operator == UnitCostTransformationThresholdOperator.GTE
                    && (decimal)UnitCostThreshold.Amount == 0
                    && AgeInDaysThreshold != null
                    && AgeInDaysThreshold.Operator == UnitCostTransformationThresholdOperator.GTE
                    && (int)AgeInDaysThreshold.Amount == 0;
            }
        }

        protected override object GetIdValue()
        {
            return dealer.Id;
        }

        #endregion

        #region Authorization Rules

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        public static bool CanAddObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        public static bool CanEditObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        #endregion

        #region Validation Rules

        public override bool IsValid
        {
            get
            {
                return !(!ApplyToPurchase && !ApplyToRetail && !ApplyToTrade && !ApplyToWholesale && AgeInDaysThreshold == null && UnitCostThreshold == null)
                    && !(UseStaticPack && StaticPackAmount <= 0);
            }
        }

        #endregion

        #region Factory Methods

        public static DealerUnitCostTransformationSettings GetDealerUnitCostTransformationSettings(Configuration.Adp.Dealer dealer)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view DealerUnitCostTransformation");

            return DataPortal.Fetch<DealerUnitCostTransformationSettings>(new Criteria(dealer));
        }

        public static DealerUnitCostTransformationSettings NewDealerUnitCostTransformationSettings()
        {
            return DataPortal.Create<DealerUnitCostTransformationSettings>();
        }

        public override DealerUnitCostTransformationSettings Save()
        {
            if (IsNew && !CanAddObject())
                throw new SecurityException("User not authorized to add DealerUnitCostTransformation");

            if (!CanEditObject())
                throw new SecurityException("User not authorized to edit DealerUnitCostTransformation");

            MarkDirty();
            return base.Save();
        }

        #endregion

        #region Data Access

        [Serializable]
        class Criteria
        {
            private readonly Configuration.Adp.Dealer dealer;

            public Criteria(Configuration.Adp.Dealer dealer)
            {
                this.dealer = dealer;
            }

            public Configuration.Adp.Dealer Dealer
            {
                get { return dealer; }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called by CSLA using reflection")]
        [Transactional(TransactionalTypes.Manual)]
        private void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Transform.DealerUnitCostTransformation#Fetch";
                    command.AddParameterWithValue("DealerId", DbType.Int32, true, criteria.Dealer.Id);
                    command.AddParameterWithValue("EnvironmentId", DbType.Int32, true, (int)criteria.Dealer.Environment);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        dealer = criteria.Dealer;
                        if (reader.Read())
                        {
                            isSet = true;
                            _UseDMSPACK = reader.GetBoolean(reader.GetOrdinal("UseDMSPACK"));

                            if (!reader.IsDBNull(reader.GetOrdinal("UseStaticPack")))
                            {
                                _UseStaticPack = reader.GetBoolean(reader.GetOrdinal("UseStaticPack"));
                            }
                            if (!reader.IsDBNull(reader.GetOrdinal("StaticPackAmount")))
                            {
                                _StaticPackAmount = reader.GetDecimal(reader.GetOrdinal("StaticPackAmount"));
                            }

                            if (!reader.IsDBNull(reader.GetOrdinal("ApplyToRetail")))
                            {
                                _ApplyToRetail = reader.GetBoolean(reader.GetOrdinal("ApplyToRetail"));
                            }
                            if (!reader.IsDBNull(reader.GetOrdinal("ApplyToWholesale")))
                            {
                                _ApplyToWholesale = reader.GetBoolean(reader.GetOrdinal("ApplyToWholesale"));
                            }
                            if (!reader.IsDBNull(reader.GetOrdinal("ApplyToTrade")))
                            {
                                _ApplyToTrade = reader.GetBoolean(reader.GetOrdinal("ApplyToTrade"));
                            }
                            if (!reader.IsDBNull(reader.GetOrdinal("ApplyToPurchase")))
                            {
                                _ApplyToPurchase = reader.GetBoolean(reader.GetOrdinal("ApplyToPurchase"));
                            }


                            if (!reader.IsDBNull(reader.GetOrdinal("UnitCostLTEThreshold")))
                            {
                                _UnitCostThreshold = new UnitCostTransformationThreshold(UnitCostTransformationThresholdOperator.LTE, reader.GetDecimal(reader.GetOrdinal("UnitCostLTEThreshold")));
                            }
                            else if (!reader.IsDBNull(reader.GetOrdinal("UnitCostGTEThreshold")))
                            {
                                _UnitCostThreshold = new UnitCostTransformationThreshold(UnitCostTransformationThresholdOperator.GTE, reader.GetDecimal(reader.GetOrdinal("UnitCostGTEThreshold")));
                            }

                            if (!reader.IsDBNull(reader.GetOrdinal("AgeInDaysLTEThreshold")))
                            {
                                _AgeInDaysThreshold = new UnitCostTransformationThreshold(UnitCostTransformationThresholdOperator.LTE, reader.GetInt32(reader.GetOrdinal("AgeInDaysLTEThreshold")));
                            }
                            else if (!reader.IsDBNull(reader.GetOrdinal("AgeInDaysGTEThreshold")))
                            {
                                _AgeInDaysThreshold = new UnitCostTransformationThreshold(UnitCostTransformationThresholdOperator.GTE, reader.GetInt32(reader.GetOrdinal("AgeInDaysGTEThreshold")));
                            }
                        }
                    }
                }
            }
            ValidationRules.CheckRules();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called by CSLA using reflection")]
        protected override void DataPortal_Insert()
        {
            InsertUpdate();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called by CSLA using reflection")]
        protected override void DataPortal_Update()
        {
            InsertUpdate();
        }

        protected void InsertUpdate()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Transform.DealerUnitCostTransformation#Update";

                    command.AddParameterWithValue("DealerId", DbType.Int32, false, dealer.Id);
                    command.AddParameterWithValue("EnvironmentId", DbType.Int32, true, (int)dealer.Environment);

                    command.AddParameterWithValue("UseDMSPack", DbType.Boolean, false, UseDMSPACK);

                    if (UseDMSPACK)
                    {
                        command.AddParameterWithValue("UseStaticPack", DbType.Boolean, false, false);
                        command.AddParameterWithValue("StaticPackAmount", DbType.Decimal, false, null);
                    }
                    else
                    {
                        command.AddParameterWithValue("UseStaticPack", DbType.Boolean, false, UseStaticPack);
                        command.AddParameterWithValue("StaticPackAmount", DbType.Decimal, false, StaticPackAmount);
                    }
                    command.AddParameterWithValue("ApplyToRetail", DbType.Boolean, false, ApplyToRetail);
                    command.AddParameterWithValue("ApplyToWholesale", DbType.Boolean, false, ApplyToWholesale);
                    command.AddParameterWithValue("ApplyToTrade", DbType.Boolean, false, ApplyToTrade);
                    command.AddParameterWithValue("ApplyToPurchase", DbType.Boolean, false, ApplyToPurchase);

                    if (UnitCostThreshold == null)
                    {
                        command.AddParameterWithValue("UnitCostGTEThreshold", DbType.Decimal, true, null);
                        command.AddParameterWithValue("UnitCostLTEThreshold", DbType.Decimal, true, null);
                    }
                    else
                    {
                        if (UnitCostThreshold.Operator == UnitCostTransformationThresholdOperator.GTE)
                        {
                            command.AddParameterWithValue("UnitCostGTEThreshold", DbType.Decimal, true, Convert.ToDecimal(UnitCostThreshold.Amount));
                            command.AddParameterWithValue("UnitCostLTEThreshold", DbType.Decimal, true, null);
                        }
                        else
                        {
                            command.AddParameterWithValue("UnitCostGTEThreshold", DbType.Decimal, true, null);
                            command.AddParameterWithValue("UnitCostLTEThreshold", DbType.Decimal, true, Convert.ToDecimal(UnitCostThreshold.Amount));
                        }
                    }

                    if (AgeInDaysThreshold == null)
                    {
                        command.AddParameterWithValue("AgeInDaysGTEThreshold", DbType.Int32, true, null);
                        command.AddParameterWithValue("AgeInDaysLTEThreshold", DbType.Int32, true, null);
                    }
                    else
                    {
                        if (AgeInDaysThreshold.Operator == UnitCostTransformationThresholdOperator.GTE)
                        {
                            command.AddParameterWithValue("AgeInDaysGTEThreshold", DbType.Int32, true, Convert.ToInt32(AgeInDaysThreshold.Amount));
                            command.AddParameterWithValue("AgeInDaysLTEThreshold", DbType.Int32, true, null);
                        }
                        else
                        {
                            command.AddParameterWithValue("AgeInDaysGTEThreshold", DbType.Int32, true, null);
                            command.AddParameterWithValue("AgeInDaysLTEThreshold", DbType.Int32, true, Convert.ToInt32(AgeInDaysThreshold.Amount));
                        }
                    }

                    command.ExecuteNonQuery();
                }
            }
        }


        #endregion
    }
}