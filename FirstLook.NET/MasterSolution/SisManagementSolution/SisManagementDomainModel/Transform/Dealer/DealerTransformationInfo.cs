using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    [Serializable]
    public class DealerTransformationInfo : ReadOnlyBase<DealerTransformationInfo>
    {
        #region Business Methods

        private int id;
        private string name;
        private string address;
        private string dealerGroupName;
        private bool tradePurchaseDefined;
        private bool unitCostDefined;

        public int Id
        {
            get
            {
                CanReadProperty("Id", true);

                return id;
            }
        }

        public string Name
        {
            get
            {
                CanReadProperty("Name", true);

                return name;
            }
        }

        public string Address
        {
            get
            {
                CanReadProperty("Address", true);

                return address;
            }
        }

        public string DealerGroupName
        {
            get
            {
                CanReadProperty("DealerGroupName", true);

                return dealerGroupName;
            }
        }

        public bool TradePurchaseDefined
        {
            get
            {
                CanReadProperty("TradePurchaseDefined", true);

                return tradePurchaseDefined;
            }
        }
        
        public bool UnitCostDefined
        {
            get
            {
                CanReadProperty("UnitCostDefined", true);

                return unitCostDefined;
            }
        }
        
        protected override object GetIdValue()
        {
            return id;
        }

        #endregion

        #region Factory Methods

        private DealerTransformationInfo()
        {
            /* force use of factory methods */
        }

        internal DealerTransformationInfo(IDataRecord record)
        {
            Fetch(record);
        }

        #endregion

        #region Data Access

        private void Fetch(IDataRecord record)
        {
            id = record.GetInt32(record.GetOrdinal("Id"));
            name = record.GetString(record.GetOrdinal("Name"));
            address = record.GetString(record.GetOrdinal("Address"));
            dealerGroupName = record.GetString(record.GetOrdinal("DealerGroupName"));
            tradePurchaseDefined = record.GetBoolean(record.GetOrdinal("TradePurchaseDefined"));
            unitCostDefined = record.GetBoolean(record.GetOrdinal("UnitCostDefined"));
        }

        #endregion
    }
}