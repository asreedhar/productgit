using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    [Serializable]
    public class DealerMappingInfo : ReadOnlyBase<DealerMappingInfo>
    {
        #region Business Methods

        private int id;
        private string name;
        private string address;
        private string dealerGroupName;
        private bool mappingComplete;

        public int Id
        {
            get
            {
                CanReadProperty("Id", true);

                return id;
            }
        }

        public string Name
        {
            get
            {
                CanReadProperty("Name", true);

                return name;
            }
        }

        public string Address
        {
            get
            {
                CanReadProperty("Address", true);

                return address;
            }
        }

        public string DealerGroupName
        {
            get
            {
                CanReadProperty("DealerGroupName", true);

                return dealerGroupName;
            }
        }

        public bool MappingComplete
        {
            get
            {
                CanReadProperty("TradePurchaseDefined", true);

                return mappingComplete;
            }
        }
        
        protected override object GetIdValue()
        {
            return id;
        }

        #endregion

        #region Factory Methods

        private DealerMappingInfo()
        {
            /* force use of factory methods */
        }

        internal DealerMappingInfo(IDataRecord record)
        {
            Fetch(record);
        }

        #endregion

        #region Data Access

        private void Fetch(IDataRecord record)
        {
            id = record.GetInt32(record.GetOrdinal("Id"));
            name = record.GetString(record.GetOrdinal("Name"));
            address = record.GetString(record.GetOrdinal("Address"));
            dealerGroupName = record.GetString(record.GetOrdinal("DealerGroupName"));
            mappingComplete = record.GetBoolean(record.GetOrdinal("MappingComplete"));
        }

        #endregion
    }
}