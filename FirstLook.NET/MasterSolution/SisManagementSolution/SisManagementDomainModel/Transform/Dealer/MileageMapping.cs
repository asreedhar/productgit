using System.Data;
using Csla;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    public class MileageMapping : Mapping
    {
        #region Factory Methods

        internal static MileageMapping GetMileageMapping(IDataReader reader)
        {
            return new MileageMapping(reader);
        }

        public static MileageMapping GetMileageMapping(int id)
        {
            return DataPortal.Fetch<MileageMapping>(new Criteria(id));
        }

        private MileageMapping()
        {
            /* Force use of factory methods */
        }

        private MileageMapping(IDataRecord record)
        {
            Fetch(record);
        }

        #endregion
    }
}
