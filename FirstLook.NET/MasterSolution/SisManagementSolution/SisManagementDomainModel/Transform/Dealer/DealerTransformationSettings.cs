using System;
using System.Data;
using System.Security;
using Csla;
using FirstLook.Common.Data;
using FirstLook.Sis.Management.DomainModel.Configuration.Adp;
using TimeZone=System.TimeZone;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    public class DealerTransformationSettings : BusinessBase<DealerTransformationSettings>
    {
        #region Business Methods

        private Configuration.Adp.Dealer dealer;
        private DealerTradePurchaseTransformation dealerTradePurchaseTransformation;


        public Configuration.Adp.Dealer Dealer
        {
            get
            {
                CanReadProperty("Dealer", true);

                return dealer;
            }
        }

        public DealerTradePurchaseTransformation DealerTradePurchaseTransformation
        {
            get
            {
                CanReadProperty("DealerTradePurchaseTransformation", true);

                return dealerTradePurchaseTransformation;
            }
            set
            {
                CanWriteProperty("DealerTradePurchaseTransformation", true);
                dealerTradePurchaseTransformation = value;
                PropertyHasChanged("DealerTradePurchaseTransformation");
            }
        }


        protected override object GetIdValue()
        {
            return dealer.Id;
        }

        #endregion

        #region Authorization Rules

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        public static bool CanAddObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        public static bool CanEditObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        #endregion

        #region Validation Rules

        public override bool IsValid
        {
            get
            {
                return dealerTradePurchaseTransformation != null;
            }
        }

        protected override void AddBusinessRules()
        {

        }

        #endregion

        #region Factory Methods

        public static DealerTransformationSettings GetDealerTransformationSettings(Configuration.Adp.Dealer dealer)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view DealerTransformationSettings");

            return DataPortal.Fetch<DealerTransformationSettings>(new Criteria(dealer));
        }

        public override DealerTransformationSettings Save()
        {
            if (IsNew && !CanAddObject())
                throw new SecurityException("User not authorized to add DealerTransformationSettings");

            if (!CanEditObject())
                throw new SecurityException("User not authorized to edit DealerTransformationSettings");

            return base.Save();
        }

        #endregion

        #region Data Access

        [Serializable]
        class Criteria
        {
            private readonly Configuration.Adp.Dealer dealer;

            public Criteria(Configuration.Adp.Dealer dealer)
            {
                this.dealer = dealer;
            }

            public Configuration.Adp.Dealer Dealer
            {
                get { return dealer; }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called by CSLA using reflection")]
        [Transactional(TransactionalTypes.Manual)]
        private void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Transform.DealerTransformationSettings#Fetch";
                    command.AddParameterWithValue("DealerId", DbType.Int32, true, criteria.Dealer.Id);
                    command.AddParameterWithValue("EnvironmentId", DbType.Int32, true, (int)criteria.Dealer.Environment);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            dealer = criteria.Dealer;

                            if (!reader.IsDBNull(reader.GetOrdinal("TradePurchaseTransformation")))
                            {
                                //dealerTradePurchaseTransformation = Transform.Dealer.DealerTradePurchaseTransformation.GetDealerTradePurchaseTransformation(reader.GetInt32(reader.GetOrdinal("TradePurchaseTransformation")));
                            }
                        }
                        else
                        {
                            throw new DataException("DealerTransformationSettings not found");
                        }
                    }
                }
            }
            ValidationRules.CheckRules();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called by CSLA using reflection")]
        protected override void DataPortal_Update()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Transform.TradePurchaseTransformation#Update";

                    command.AddParameterWithValue("DealerId", DbType.Int32, false, dealer.Id);
                    command.AddParameterWithValue("EnvironmentId", DbType.Int32, true, (int)dealer.Environment);
                    //command.AddParameterWithValue("TradePurchaseTransformation", DbType.Int32, false, dealerTradePurchaseTransformation.Id);

                    command.ExecuteNonQuery();
                }
            }
        }

        #endregion
    }
}