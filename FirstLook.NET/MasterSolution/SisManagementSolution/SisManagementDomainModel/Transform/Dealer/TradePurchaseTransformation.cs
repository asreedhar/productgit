using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    public class TradePurchaseTransformation : ReadOnlyBase<TradePurchaseTransformation>
    {
        #region Business Methods

        private int id;
        private string name;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        protected override object GetIdValue()
        {
            return id;
        }
        #endregion

        #region Factory Methods

        internal static TradePurchaseTransformation GetTradePurchaseTransformation(IDataReader reader)
        {
            return new TradePurchaseTransformation(reader);
        }

        public static TradePurchaseTransformation GetTradePurchaseTransformation(int id)
        {
            return DataPortal.Fetch<TradePurchaseTransformation>(new Criteria(id));
        }

        private TradePurchaseTransformation()
        {
            /* Force use orf factory methods */
        }

        private TradePurchaseTransformation(IDataRecord record)
        {
            Fetch(record);
        }

        #endregion

        #region Authorization

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Data Access

        [Serializable]
        protected class Criteria
        {
            private readonly int id;

            public Criteria(int id)
            {
                this.id = id;
            }

            internal int Id
            {
                get { return id; }
            }
        }

        protected void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Transform.TradePurchaseTransformation#Fetch";

                    command.AddParameterWithValue("Id", DbType.String, false, criteria.Id);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Fetch(reader);
                        }
                        else
                        {
                            throw new DataException("Mapping not found");
                        }

                    }
                }
            }
        }

        protected void Fetch(IDataRecord reader)
        {
            id = reader.GetInt32(reader.GetOrdinal("Id"));
            name = reader.GetString(reader.GetOrdinal("Name"));
        }

        #endregion
    }
}