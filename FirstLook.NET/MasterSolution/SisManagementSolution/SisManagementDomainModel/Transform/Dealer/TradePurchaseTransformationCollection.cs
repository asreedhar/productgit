using System;
using System.Collections.Generic;
using System.Data;
using System.Security;
using System.Text;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    public class TradePurchaseTransformationCollection : ReadOnlyListBase<TradePurchaseTransformationCollection, TradePurchaseTransformation>
    {
        #region Authorization Methods

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Factory Methods

        public static TradePurchaseTransformationCollection GetTradePurchaseTransformations()
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view Mappings");

            return DataPortal.Fetch<TradePurchaseTransformationCollection>();
        }

        #endregion

        #region Data Access

        private void DataPortal_Fetch()
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Transform.TradePurchaseTransformationCollection#Fetch";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        IsReadOnly = false;
                        while (reader.Read())
                        {
                            Add(TradePurchaseTransformation.GetTradePurchaseTransformation(reader));
                        }
                        IsReadOnly = true;
                    }
                }
            }

            RaiseListChangedEvents = true;
        }
        

        #endregion
    }
}
