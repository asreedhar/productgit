using System;
using System.Data;
using System.Security;
using Csla;
using FirstLook.Common.Data;
using FirstLook.Sis.Management.DomainModel.Configuration.Adp;
using TimeZone=System.TimeZone;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    public class DealerMappingSettings : BusinessBase<DealerMappingSettings>
    {
        #region Business Methods

        private Configuration.Adp.Dealer dealer;
        private ListPriceMapping listPriceMapping;
        private LotPriceMapping lotPriceMapping;
        private MileageMapping mileageMapping;
        private ReceivedDateMapping receivedDateMapping;


        public Configuration.Adp.Dealer Dealer
        {
            get
            {
                CanReadProperty("Dealer", true);

                return dealer;
            }
        }

        public ListPriceMapping ListPriceMapping
        {
            get
            {
                CanReadProperty("ListPriceMapping", true);

                return listPriceMapping;
            }
            set
            {
                CanWriteProperty("ListPriceMapping", true);
                listPriceMapping = value;
                PropertyHasChanged("ListPriceMapping");
            }
        }

        public LotPriceMapping LotPriceMapping
        {
            get
            {
                CanReadProperty("LotPriceMapping", true);

                return lotPriceMapping;
            }
            set
            {
                CanWriteProperty("LotPriceMapping", true);
                lotPriceMapping = value;
                PropertyHasChanged("LotPriceMapping");
            }
        }

        public MileageMapping MileageMapping
        {
            get
            {
                CanReadProperty("MileageMapping", true);

                return mileageMapping;
            }
            set
            {
                CanWriteProperty("MileageMapping", true);
                mileageMapping = value;
                PropertyHasChanged("MileageMapping");
            }
        }

        public ReceivedDateMapping ReceivedDateMapping
        {
            get
            {
                CanReadProperty("ReceivedDateMapping", true);

                return receivedDateMapping;
            }
            set
            {
                CanWriteProperty("ReceivedDateMapping", true);
                receivedDateMapping = value;
                PropertyHasChanged("ReceivedDateMapping");
            }
        }
        
        protected override object GetIdValue()
        {
            return dealer.Id;
        }

        #endregion

        #region Authorization Rules

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        public static bool CanAddObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        public static bool CanEditObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementWriter");
        }

        #endregion

        #region Validation Rules

        public override bool IsValid
        {
            get
            {
                return lotPriceMapping != null && listPriceMapping != null  && mileageMapping != null && receivedDateMapping != null;
            }
        }

        protected override void AddBusinessRules()
        {
            
        }

        #endregion

        #region Factory Methods

        public static DealerMappingSettings GetDealerMappingSettings(Configuration.Adp.Dealer dealer)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view DealerMappingSettings");

            return DataPortal.Fetch<DealerMappingSettings>(new Criteria(dealer));
        }

        public override DealerMappingSettings Save()
        {
            if (IsNew && !CanAddObject())
                throw new SecurityException("User not authorized to add DealerMappingSettings");

            if (!CanEditObject())
                throw new SecurityException("User not authorized to edit DealerMappingSettings");

            return base.Save();
        }

        #endregion

        #region Data Access

        [Serializable]
        class Criteria
        {
            private readonly Configuration.Adp.Dealer dealer;

            public Criteria(Configuration.Adp.Dealer dealer)
            {
                this.dealer = dealer;
            }

            public Configuration.Adp.Dealer Dealer
            {
                get { return dealer; }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called by CSLA using reflection")]
        [Transactional(TransactionalTypes.Manual)]
        private void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Transform.DealerMappingSettings#Fetch";
                    command.AddParameterWithValue("DealerId", DbType.Int32, true, criteria.Dealer.Id);
                    command.AddParameterWithValue("EnvironmentId", DbType.Int32, true, (int)criteria.Dealer.Environment);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            dealer = criteria.Dealer;

                            if (!reader.IsDBNull(reader.GetOrdinal("ListPriceMappingId")))
                            {
                                listPriceMapping = Transform.Dealer.ListPriceMapping.GetListPriceMapping(reader.GetInt32(reader.GetOrdinal("ListPriceMappingId")));
                            }
                            if (!reader.IsDBNull(reader.GetOrdinal("LotPriceMappingId")))
                            {
                                lotPriceMapping = Transform.Dealer.LotPriceMapping.GetLotPriceMapping(reader.GetInt32(reader.GetOrdinal("LotPriceMappingId")));
                            }
                            if (!reader.IsDBNull(reader.GetOrdinal("MileageMappingId")))
                            {
                                mileageMapping = Transform.Dealer.MileageMapping.GetMileageMapping(reader.GetInt32(reader.GetOrdinal("MileageMappingId")));
                            }
                            if (!reader.IsDBNull(reader.GetOrdinal("ReceivedDateMappingId")))
                            {
                                receivedDateMapping = Transform.Dealer.ReceivedDateMapping.GetReceivedDateMapping(reader.GetInt32(reader.GetOrdinal("ReceivedDateMappingId")));
                            }
                        }
                        else
                        {
                            throw new DataException("DealerMappingSettings not found");
                        }
                    }
                }
            }
            ValidationRules.CheckRules();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called by CSLA using reflection")]
        protected override void DataPortal_Update()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Transform.DealerMappingSettings#Update";

                    command.AddParameterWithValue("DealerId", DbType.Int32, false, dealer.Id);
                    command.AddParameterWithValue("EnvironmentId", DbType.Int32, true, (int)dealer.Environment);
                    command.AddParameterWithValue("LotPriceMappingId", DbType.Int32, false, lotPriceMapping.Id);
                    command.AddParameterWithValue("ListPriceMappingId", DbType.Int32, false, listPriceMapping.Id);
                    command.AddParameterWithValue("MileageMappingId", DbType.Int32, false, mileageMapping.Id);
                    command.AddParameterWithValue("ReceivedDateMappingId", DbType.Int32, false, receivedDateMapping.Id);

                    command.ExecuteNonQuery();
                }
            }
        }

        #endregion
    }
}