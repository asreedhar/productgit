using System.Data;
using Csla;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    public class StandardMappingValue : ReadOnlyBase<StandardMappingValue>
    {
        #region Business Methods

        private string _value;

        public string Value
        {
            get
            {
                CanReadProperty("Value", true);
                return _value;
            }
        }

        protected override object GetIdValue()
        {
            return _value;
        }

        #endregion

        #region Authorization Rules

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Factory Methods

        internal static StandardMappingValue GetStandardMappingValue(IDataReader reader)
        {
            return new StandardMappingValue(reader);
        }

        private StandardMappingValue()
        {
            /* Force use orf factory methods */
        }

        private StandardMappingValue(IDataRecord record)
        {
            Fetch(record);
        }

        #endregion

        #region Data Access

        private void Fetch(IDataRecord record)
        {
            _value = record.GetString(record.GetOrdinal("Value"));
        }

        #endregion
    }
}
