using System;
using System.Data;
using Csla;
using Csla.Core;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    [Serializable]
    public class DealerMappingInfoCollection : ReadOnlyListBase<DealerMappingInfoCollection, DealerMappingInfo>, IReportTotalRowCount
    {
        #region IReportTotalRowCount Members

        private int totalRowCount = 0;

        public int TotalRowCount
        {
            get
            {
                return totalRowCount;
            }
        }

        #endregion

        public static DealerMappingInfoCollection GetDealers(string filter, string sortOrder, int maximumRows, int startRowIndex)
        {
            if (maximumRows < 1)
                throw new ArgumentOutOfRangeException("maximumRows", maximumRows, "Must be greater than zero");

            if (startRowIndex < 0)
                throw new ArgumentOutOfRangeException("startRowIndex", startRowIndex, "Must be greater than zero");

            if (string.IsNullOrEmpty(filter))
                filter = string.Empty;

            if (string.IsNullOrEmpty(sortOrder))
                sortOrder = "Name";

            return DataPortal.Fetch<DealerMappingInfoCollection>(new FetchCriteria(filter, new DataSourceSelectCriteria(sortOrder, maximumRows, startRowIndex)));
        }


        private DealerMappingInfoCollection()
        {

        }

        [Serializable]
        class FetchCriteria
        {
            private readonly string filter;
            private readonly DataSourceSelectCriteria criteria;

            public FetchCriteria(string filter, DataSourceSelectCriteria criteria)
            {
                this.filter = filter;
                this.criteria = criteria;
            }

            public string Filter
            {
                get { return filter; }
            }

            public DataSourceSelectCriteria Criteria
            {
                get { return criteria; }
            }
        }

        private void DataPortal_Fetch(FetchCriteria criteria)
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Transform.DealerMappingInfoCollection#Fetch";

                    command.AddParameterWithValue("Filter", DbType.String, true, criteria.Filter);

                    criteria.Criteria.AddToCommand(command);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            totalRowCount = reader.GetInt32(reader.GetOrdinal("TotalRowCount"));

                            if (reader.NextResult())
                            {
                                IsReadOnly = false;

                                while (reader.Read())
                                {
                                    Add(new DealerMappingInfo(reader));
                                }

                                IsReadOnly = true;
                            }
                            else
                            {
                                throw new DataException("DataReader missing second of two result sets");
                            }
                        }
                        else
                        {
                            throw new DataException("DataReader missing first of two result sets");
                        }
                    }
                }
            }

            RaiseListChangedEvents = true;
        }
    }
}