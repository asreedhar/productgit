using System.Data;
using System.Security;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Transform.Dealer
{
    public class ListPriceMappingCollection : MappingCollection<ListPriceMappingCollection, ListPriceMapping>
    {
        #region Business Methods

        internal override string Name
        {
            get { return "Sticker Price"; }
        }

        #endregion

        #region Data Access

        internal override void AddItems(IDataReader reader)
        {
            while (reader.Read())
            {
                Add(ListPriceMapping.GetListPriceMapping(reader));
            }
        }

        #endregion
    }
}
