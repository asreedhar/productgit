using System;
using System.Data;
using System.Security;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Transform
{
    [Serializable]
    public class StandardColorMappingCollection : BusinessListBase<StandardColorMappingCollection,StandardColorMapping>
    {
        #region Business Methods

        private StandardColor standardColor;

        public StandardColor StandardColor
        {
            get
            {
                return standardColor;
            }
        }

        public StandardColorMapping GetItem(int colorId)
        {
            foreach (StandardColorMapping mapping in this)
                if (mapping.ColorId == colorId)
                    return mapping;
            return null;
        }

        public void Assign(int colorId)
        {
            if (!Contains(colorId))
            {
                Add(StandardColorMapping.NewStandardColorMapping(colorId));
            }
            else
            {
                throw new InvalidOperationException("Color already assigned to mapping");
            }
        }

        public void Remove(int colorId)
        {
            foreach (StandardColorMapping mapping in this)
            {
                if (mapping.ColorId == colorId)
                {
                    Remove(mapping);
                    break;
                }
            }
        }

        public bool Contains(int colorId)
        {
            foreach (StandardColorMapping mapping in this)
                if (mapping.ColorId == colorId)
                    return true;
            return false;
        }

        public bool ContainsDeleted(int colorId)
        {
            foreach (StandardColorMapping mapping in DeletedList)
                if (mapping.ColorId == colorId)
                    return true;
            return false;
        }

        #endregion

        #region Authorization Rules

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Factory Methods

        public static StandardColorMappingCollection GetStandardColorMappings(int standardColorId)
        {
            if (!CanGetObject())
                throw new SecurityException("User not authorized to view StandardColorMappingCollection");

            return DataPortal.Fetch<StandardColorMappingCollection>(new Criteria(standardColorId));
        }

        #endregion

        #region Data Access
        
        [Serializable]
        class Criteria
        {
            private readonly int standardColorId;

            public Criteria(int standardColorId)
            {
                this.standardColorId = standardColorId;
            }

            public int StandardColorId
            {
                get { return standardColorId; }
            }
        }

        private void DataPortal_Fetch(Criteria criteria)
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Transform.StandardColorMappingCollection#Fetch";
                    command.AddParameterWithValue("StandardColorId", DbType.Int32, true, criteria.StandardColorId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            while (reader.Read())
                            {
                                Add(StandardColorMapping.GetStandardColorMapping(reader));
                            }
                        }

                        if (reader.NextResult())
                        {
                            if (reader.Read())
                            {
                                standardColor = StandardColor.GetStandardColor(reader, string.Empty);
                            }
                            else
                            {
                                throw new DataException("Missing StandardColor record");
                            }
                        }
                        else
                        {
                            throw new DataException("Missing StandardColor result set");
                        }
                    }
                }
            }

            RaiseListChangedEvents = true;
        }

        [Transactional(TransactionalTypes.Manual)]
        protected override void DataPortal_Update()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    RaiseListChangedEvents = false;

                    // update (thus deleting) any deleted child objects
                    foreach (StandardColorMapping obj in DeletedList)
                    {
                        obj.DeleteSelf(connection, transaction, standardColor);
                    }

                    // now that they are deleted, remove them from memory too
                    DeletedList.Clear();

                    // add/update any current child objects
                    foreach (StandardColorMapping obj in this)
                    {
                        if (obj.IsNew)
                        {
                            obj.Insert(connection, transaction, standardColor);
                        }
                        else
                        {
                            obj.Update(connection, transaction, standardColor);
                        }
                    }

                    transaction.Commit();

                    RaiseListChangedEvents = true;
                }
            }
        }

        #endregion

        #region Transfer

        public static bool Transfer(int srcStandardColorId, int dstStandardColorId, int colorId)
        {
            TransferCommand result = DataPortal.Execute(new TransferCommand(srcStandardColorId, dstStandardColorId, colorId));

            return result.Success;
        }

        [Serializable]
        class TransferCommand : CommandBase
        {
            private readonly int srcStandardColorId;
            private readonly int dstStandardColorId;
            private readonly int colorId;
            private bool success;

            public TransferCommand(int srcStandardColorId, int dstStandardColorId, int colorId)
            {
                this.srcStandardColorId = srcStandardColorId;
                this.dstStandardColorId = dstStandardColorId;
                this.colorId = colorId;
            }

            public bool Success
            {
                get { return success; }
            }

            [Transactional(TransactionalTypes.Manual)]
            protected override void DataPortal_Execute()
            {
                using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
                {
                    connection.Open();

                    using (IDbTransaction transaction = connection.BeginTransaction())
                    {
                        using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                        {
                            command.CommandText = "Transform.StandardColorMappingCollection#Transfer";
                            command.CommandType = CommandType.StoredProcedure;
                            command.Transaction = transaction;
                            command.AddParameterWithValue("SrcStandardColorId", DbType.Int32, false, srcStandardColorId);
                            command.AddParameterWithValue("DstStandardColorId", DbType.Int32, false, dstStandardColorId);
                            command.AddParameterWithValue("ColorId", DbType.Int32, false, colorId);
                            IDataParameter result = command.AddOutParameter("Success", DbType.Boolean);
                            command.ExecuteNonQuery();
                            success = (bool)result.Value;
                        }

                        transaction.Commit();
                    }
                }
            }
        }

        #endregion
    }
}
