using System;
using System.Data;
using System.Security;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Transform
{
    [Serializable]
    public class Color : ReadOnlyBase<Color>
    {
        #region Business Methods

        private int id;
        private string name;
        private StandardColor standardColor;

        public int Id
        {
            get
            {
                CanReadProperty("Name", true);

                return id;
            }
        }

        public string Name
        {
            get
            {
                CanReadProperty("Name", true);

                return name;
            }
        }

        public StandardColor StandardColor
        {
            get
            {
                CanReadProperty("StandardColor", true);

                return standardColor;
            }
        }

        protected override object GetIdValue()
        {
            return id;
        }

        #endregion

        #region Authorization Rules

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Factory Methods

        public static Color GetColor(int id)
        {
            if (!CanGetObject())
                throw new SecurityException("Insufficient priviledges to get Color");

            return DataPortal.Fetch<Color>(new Criteria(id));
        }

        internal static Color GetColor(IDataRecord record)
        {
            return new Color(record);
        }

        private Color()
        {
            /* force use of factory methods */
        }

        internal Color(IDataRecord record)
        {
            Fetch(record);
        }

        #endregion

        #region Data Access

        [Serializable]
        private class Criteria
        {
            private readonly int id;

            public int Id
            {
                get { return id; }
            }

            public Criteria(int id)
            {
                this.id = id;
            }
        }

        private void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Transform.Color#Fetch";
                    command.AddParameterWithValue("Id", DbType.Int32, false, criteria.Id);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Fetch(reader);
                        }
                        else
                        {
                            throw new DataException("Missing Color record");
                        }
                    }
                }
            }
        }

        private void Fetch(IDataRecord record)
        {
            id = record.GetInt32(record.GetOrdinal("Id"));
            name = record.GetString(record.GetOrdinal("Name"));
            standardColor = StandardColor.GetStandardColor(record, "StandardColor");
        }

        #endregion
    }
}