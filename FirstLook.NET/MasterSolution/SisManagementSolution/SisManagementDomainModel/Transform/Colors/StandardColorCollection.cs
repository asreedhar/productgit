using System.Data;
using Csla;
using Csla.Core;
using FirstLook.Common.Data;
using System;

namespace FirstLook.Sis.Management.DomainModel.Transform.Colors
{
    [Serializable]
    public class StandardColorCollection : ReadOnlyListBase<StandardColorCollection, StandardColor>, IReportTotalRowCount
    {
        #region IReportTotalRowCount Members

        private int totalRowCount;

        public int TotalRowCount
        {
            get { return totalRowCount; }
        }

        #endregion

        #region Factory Methods

        public static StandardColorCollection GetStandardColors()
        {
            return GetStandardColors("Name", Int32.MaxValue-1, 0);
        }

        public static StandardColorCollection GetStandardColors(string sortOrder, int maximumRows, int startRowIndex)
        {
            if (maximumRows < 1)
                throw new ArgumentOutOfRangeException("maximumRows", maximumRows, "Must be greater than zero");

            if (startRowIndex < 0)
                throw new ArgumentOutOfRangeException("startRowIndex", startRowIndex, "Must be greater nor equal to zero");

            if (string.IsNullOrEmpty(sortOrder))
                sortOrder = "Name";

            return DataPortal.Fetch<StandardColorCollection>(new DataSourceSelectCriteria(sortOrder, maximumRows, startRowIndex));
        }

        private StandardColorCollection()
        {
            // force use of factory methods
        }

        #endregion

        #region Data Access

        private void DataPortal_Fetch(DataSourceSelectCriteria criteria)
        {
            RaiseListChangedEvents = false;

            using(IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Transform.StandardColorCollection#Fetch";

                    criteria.AddToCommand(command);

                    using(IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            totalRowCount = reader.GetInt32(reader.GetOrdinal("TotalRowCount"));
                        }
                        else
                        {
                            throw new DataException("Missing TotalRowCount");
                        }

                        if (reader.NextResult())
                        {
                            IsReadOnly = false;

                            while (reader.Read())
                            {
                                Add(StandardColor.GetStandardColor(reader, string.Empty));
                            }

                            IsReadOnly = true;
                        }
                        else
                        {
                            throw new DataException("Missing StandardColor result set");
                        }
                    }
                }
            }

            RaiseListChangedEvents = true;
        }

        #endregion
    }
}