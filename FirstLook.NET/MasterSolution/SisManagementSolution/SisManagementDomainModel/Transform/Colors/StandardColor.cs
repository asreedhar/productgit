using System;
using System.Data;
using System.Security;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Sis.Management.DomainModel.Transform.Colors
{
    [Serializable]
    public class StandardColor : ReadOnlyBase<StandardColor>
    {
        #region Business Methods

        private int id;

        public int Id
        {
            get
            {
                CanReadProperty("Id", true);

                return id;
            }
        }

        private string name;

        public string Name
        {
            get
            {
                CanReadProperty("Name", true);

                return name;
            }
        }

        protected override object GetIdValue()
        {
            return id;
        }

        #endregion

        #region Authorization Rules

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Factory Methods

        public static StandardColor GetStandardColor(int id)
        {
            if (!CanGetObject())
                throw new SecurityException("Insufficient priviledges to get StandardColor");

            return DataPortal.Fetch<StandardColor>(new Criteria(id));
        }

        internal static StandardColor GetStandardColor(IDataRecord record, string prefix)
        {
            if (record.IsDBNull(record.GetOrdinal(prefix + "Id")) ||
                record.IsDBNull(record.GetOrdinal(prefix + "Name")))
                return null;
            return new StandardColor(record, prefix);
        }

        private StandardColor()
        {
            /* force use of factory methods */
        }

        private StandardColor(IDataRecord record, string prefix)
        {
            Fetch(record, prefix);
        }

        #endregion

        #region Data Access

        [Serializable]
        private class Criteria
        {
            private readonly int id;

            public int Id
            {
                get { return id; }
            }

            public Criteria(int id)
            {
                this.id = id;
            }
        }

        private void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandText = "Transform.StandardColor#Fetch";
                    command.CommandType = CommandType.StoredProcedure;
                    command.AddParameterWithValue("Id", DbType.Int32, false, criteria.Id);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Fetch(reader, string.Empty);
                        }
                    }
                }
            }
        }

        private void Fetch(IDataRecord record, string prefix)
        {
            id = record.GetInt32(record.GetOrdinal(prefix + "Id"));
            name = record.GetString(record.GetOrdinal(prefix + "Name"));
        }

        #endregion
    }
}