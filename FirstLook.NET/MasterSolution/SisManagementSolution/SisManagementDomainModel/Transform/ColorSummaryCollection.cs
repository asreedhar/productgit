using System;
using System.Data;
using System.Security;
using Csla;
using Csla.Core;
using FirstLook.Common.Data;
using FirstLook.Sis.Management.DomainModel;

namespace FirstLook.Sis.Management.DomainModel.Transform
{
    [Serializable]
    public class ColorSummaryCollection : ReadOnlyListBase<ColorSummaryCollection, ColorSummary>, IReportTotalRowCount
    {
        #region IReportTotalRowCount Members

        private int totalRowCount;

        public int TotalRowCount
        {
            get
            {
                return totalRowCount;
            }
        }

        #endregion

        #region Authorization Rules

        public static bool CanGetObject()
        {
            return ApplicationContext.User.IsInRole("DataManagementReader");
        }

        #endregion

        #region Factory Methods

        public static ColorSummaryCollection GetUnknownColors(string name, string sortOrder, int maximumRows, int startRowIndex)
        {
            if (!CanGetObject())
                throw new SecurityException("Insufficient priviledges to get ColorSummaryCollection");

            return DataPortal.Fetch<ColorSummaryCollection>(new Criteria(name, new DataSourceSelectCriteria(sortOrder, maximumRows, startRowIndex)));
        }

        #endregion

        #region Data Access

        [Serializable]
        private class Criteria
        {
            private readonly string colorName;
            private readonly DataSourceSelectCriteria dsc;

            public Criteria(string colorName, DataSourceSelectCriteria dsc)
            {
                this.colorName = colorName;
                this.dsc = dsc;
            }

            public string ColorName
            {
                get { return colorName; }
            }

            public DataSourceSelectCriteria DataSourceSelectCriteria
            {
                get { return dsc; }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Called by CSLA using reflection")]
        private void DataPortal_Fetch(Criteria criteria)
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.SisDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Transform.ColorSummaryCollection#Fetch";
                    command.AddParameterWithValue("ColorName", DbType.String, true, criteria.ColorName);
                    criteria.DataSourceSelectCriteria.AddToCommand(command);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            totalRowCount = reader.GetInt32(reader.GetOrdinal("TotalRowCount"));
                        }
                        else
                        {
                            throw new DataException("Missing TotalRowCount");
                        }

                        if (reader.NextResult())
                        {
                            IsReadOnly = false;

                            while (reader.Read())
                            {
                                Add(new ColorSummary(reader));
                            }

                            IsReadOnly = true;
                        }
                        else
                        {
                            throw new DataException("Missing ColorSummary result set");
                        }
                    }
                }
            }

            RaiseListChangedEvents = true;
        }

        #endregion
    }
}
