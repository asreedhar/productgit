using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp.Test
{
    /// <summary>
    /// Tests the IPAddress class and its validation methods.
    /// </summary>
    [TestFixture]
    public class IPAddressTest
    {
        private List<string> _validPublicIPAddresses;
        private List<string> _validPrivateIPAddresses;
        private List<string> _invalidIPAddresses;

        [SetUp]
        public void Init()
        {
            _validPublicIPAddresses = new List<string>();
            _validPublicIPAddresses.Add("206.180.0.162");
            _validPublicIPAddresses.Add("65.118.241.20");

            _validPrivateIPAddresses = new List<string>();
            _validPrivateIPAddresses.Add("127.0.0.1");
            _validPrivateIPAddresses.Add("10.0.0.1");
            _validPrivateIPAddresses.Add("10.0.123.0");
            _validPrivateIPAddresses.Add("10.255.255.255");
            _validPrivateIPAddresses.Add("172.16.0.0");
            _validPrivateIPAddresses.Add("172.17.0.0");
            _validPrivateIPAddresses.Add("172.25.3.0");
            _validPrivateIPAddresses.Add("172.31.255.255");
            _validPrivateIPAddresses.Add("192.168.0.0");
            _validPrivateIPAddresses.Add("192.168.255.255");

            _invalidIPAddresses = new List<string>();
            _invalidIPAddresses.Add("0.0.0.0");
            _invalidIPAddresses.Add("0.0.0.256");
            _invalidIPAddresses.Add("256.0.0.0");

        }

        [TearDown]
        public void Dispose()
        {
            _validPublicIPAddresses = null;
            _validPrivateIPAddresses = null;
        }

        [NUnit.Framework.Test]
        public void TestValidIPAddress()
        {
            IPAddress address = null;
            foreach (string ipaddress in _validPublicIPAddresses)
            {
                IPAddress.TryParse(ipaddress, ref address);

                Assert.IsTrue(IPAddress.IPAddressIsValid(address));
            }

            foreach (string ipaddress in _validPrivateIPAddresses)
            {   
                IPAddress.TryParse(ipaddress, ref address);

                Assert.IsTrue(IPAddress.IPAddressIsValid(address));
            }

            foreach (string ipaddress in _invalidIPAddresses)
            {
                bool parsed = IPAddress.TryParse(ipaddress, ref address);

                if (parsed)
                {
                    Assert.IsFalse(IPAddress.IPAddressIsValid(address));
                }
                else
                {
                    Assert.IsNull(address);
                }
            }
        }

        [NUnit.Framework.Test]
        public void TestPrivateIPAddress()
        {
            IPAddress address = null;
            foreach (string ipaddress in _validPublicIPAddresses)
            {
                IPAddress.TryParse(ipaddress, ref address);

                Assert.IsFalse(address.IsPrivateAddress);
            }

            foreach (string ipaddress in _validPrivateIPAddresses)
            {
                IPAddress.TryParse(ipaddress, ref address);

                Assert.IsTrue(address.IsPrivateAddress);
            }
        }
    }
}
