using System;
using NUnit.Framework;

namespace FirstLook.Sis.Management.DomainModel.Configuration.Adp.Test
{
    /// <summary>
    /// Tests the telephone number class and its validation methods.
    /// <para>
    /// The NANP is administered by the North American Numbering Plan Administration (NANPA).
    /// Current NANP number format can be summed up via the following:
    /// </para>
    /// <para>NPA Nxx Station, where</para>
    /// <list type="bullet">
    /// <item>NPA (Numbering Plan Area code) = [2-9][0-8][0-9]</item>
    /// <item>NXX (Central Office or Exchange code) = [2-9][0-9][0-9]</item>
    /// <item>Station code = [0-9][0-9][0-9][0-9]</item>
    /// </list>
    /// </summary>
    [TestFixture]
    public class TelephoneNumberTest
    {
        [NUnit.Framework.Test]
        public void TestValidTelephoneNumbers()
        {
            TelephoneNumber number = null;

            TelephoneNumber.TryParse("312-279-1234", ref number);
            number.AreaCode = 200;
            number.AreaCode = 289;
            number.AreaCode = 300;
            number.AreaCode = 389;
            number.AreaCode = 900;
            number.AreaCode = 989;
            number.ExchangeCode = 200;
            number.ExchangeCode = 209;
            number.ExchangeCode = 290;
            number.ExchangeCode = 299;
            number.ExchangeCode = 900;
            number.ExchangeCode = 909;
            number.ExchangeCode = 990;
            number.ExchangeCode = 999;
            number.StationCode = 0;     // "0000"
            number.StationCode = 9;     // "0009"
            number.StationCode = 99;    // "0099"
            number.StationCode = 999;   // "0999"
            number.StationCode = 9999;  // "9999"

            Assert.IsNotNull(number);
        }

        [NUnit.Framework.Test]
        public void TestInvalidTelephoneNumber()
        {
            // area code is the tricky one

            TelephoneNumber number = null;
            try
            {
                TelephoneNumber.TryParse("199-279-1234", ref number);
            }
            catch
            {
                Assert.IsNull(number);
            }

            if (number != null)
            {
                Assert.Fail("The phone number 199-279-1234 has an invalid area code.");
            }

            try
            {
                TelephoneNumber.TryParse("290-279-1234", ref number);
            }
            catch
            {
                Assert.IsNull(number);
            }

            if (number != null)
            {
                Assert.Fail("The phone number 290-279-1234 has an invalid area code.");
            }

            try
            {
                TelephoneNumber.TryParse("990-279-1234", ref number);
            }
            catch
            {
                Assert.IsNull(number);
            }

            if (number != null)
            {
                Assert.Fail("The phone number 990-279-1234 has an invalid area code.");
            }
        }

        [NUnit.Framework.Test]
        public void IsValidAreaCode()
        {
            Assert.IsFalse(TelephoneNumber.IsValidAreaCode(199));

            Assert.IsTrue(TelephoneNumber.IsValidAreaCode(200));
            Assert.IsTrue(TelephoneNumber.IsValidAreaCode(289));
            Assert.IsFalse(TelephoneNumber.IsValidAreaCode(290));
            Assert.IsFalse(TelephoneNumber.IsValidAreaCode(299));

            Assert.IsTrue(TelephoneNumber.IsValidAreaCode(300));
            Assert.IsTrue(TelephoneNumber.IsValidAreaCode(389));
            Assert.IsFalse(TelephoneNumber.IsValidAreaCode(390));
            Assert.IsFalse(TelephoneNumber.IsValidAreaCode(399));

            Assert.IsTrue(TelephoneNumber.IsValidAreaCode(400));

            Assert.IsTrue(TelephoneNumber.IsValidAreaCode(900));
            Assert.IsTrue(TelephoneNumber.IsValidAreaCode(989));
            Assert.IsFalse(TelephoneNumber.IsValidAreaCode(990));
        }

        [NUnit.Framework.Test]
        public void IsValidExchangeCode()
        {
            Assert.IsFalse(TelephoneNumber.IsValidExchangeCode(199));

            Assert.IsTrue(TelephoneNumber.IsValidExchangeCode(200));
            Assert.IsTrue(TelephoneNumber.IsValidExchangeCode(289));
            Assert.IsTrue(TelephoneNumber.IsValidExchangeCode(290));
            Assert.IsTrue(TelephoneNumber.IsValidExchangeCode(299));

            Assert.IsTrue(TelephoneNumber.IsValidExchangeCode(300));
            Assert.IsTrue(TelephoneNumber.IsValidExchangeCode(389));
            Assert.IsTrue(TelephoneNumber.IsValidExchangeCode(390));
            Assert.IsTrue(TelephoneNumber.IsValidExchangeCode(399));

            Assert.IsTrue(TelephoneNumber.IsValidExchangeCode(400));

            Assert.IsTrue(TelephoneNumber.IsValidExchangeCode(900));
            Assert.IsTrue(TelephoneNumber.IsValidExchangeCode(989));
            Assert.IsTrue(TelephoneNumber.IsValidExchangeCode(990));
            Assert.IsTrue(TelephoneNumber.IsValidExchangeCode(999));

            Assert.IsFalse(TelephoneNumber.IsValidExchangeCode(1000));
        }

        [NUnit.Framework.Test]
        public void IsValidStationCode()
        {
            Assert.IsFalse(TelephoneNumber.IsValidStationCode(-1));
            Assert.IsTrue(TelephoneNumber.IsValidStationCode(0));
            Assert.IsTrue(TelephoneNumber.IsValidStationCode(9999));
            Assert.IsFalse(TelephoneNumber.IsValidStationCode(10000));
        }
    }
}
