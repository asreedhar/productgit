﻿using System;
using FirstLook.Common.Core;

namespace PricingPresentersTests
{
    class ConsoleLogger : ILogger
    {
        public void Log(Exception e)
        {
            Console.WriteLine(e.ToString());
        }

        public void Log(string message, object source)
        {
            Console.WriteLine(source);
            Console.WriteLine(message);
        }

        public void Log(string message)
        {
            Console.WriteLine(message);
        }
    }
}
