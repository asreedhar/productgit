﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.PhotoServices;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel.Marketing.Deal;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;
using FirstLook.Merchandising.ModelBuilder;
using FirstLook.Pricing.DomainModel;
using Merchandising.Messages;
using Moq;
using NUnit.Framework;
using PricingPresentersTests;

namespace PricingDomainModel_Test.Presentation
{
    [TestFixture]
    public class DealTests
    {
        private ILogger _logger;
        private IMarketListingSearchAdapter _searchAdapter;
        private string _ownerHandle;
        private string _vehicleHandle;
        private string _searchHandle;

        private Deal _deal;

        [TestFixtureSetUp]
        public void Setup()
        {
            SetupAutofac();

            _logger = new ConsoleLogger();
            _searchAdapter = new MarketListingSearchAdapter();
            _ownerHandle = string.Empty;
            _vehicleHandle = string.Empty;
            _searchHandle = string.Empty;

            _deal = new Deal(_logger, _searchAdapter, _ownerHandle, _vehicleHandle, _searchHandle);
        }

        [Test]
        public void CanConstruct()
        {
            Assert.IsNotNull(_deal);

            Assert.AreEqual(_ownerHandle, _deal.OwnerHandle);
            Assert.AreEqual(_vehicleHandle, _deal.VehicleHandle);
            Assert.AreEqual(_searchHandle, _deal.SearchHandle );
            Assert.AreEqual(_logger, _deal.Logger);

            Assert.IsNotNull(_deal.CustomerPacketOptions);
            Assert.AreEqual( SalesPackOptions.SellingSheet, _deal.CustomerPacketOptions.SalesPacketOptions );

            Assert.IsFalse(_deal.CustomerPacketOptions.AutoCheckReport);
            Assert.IsFalse(_deal.CustomerPacketOptions.CarFaxReport);
            Assert.IsNotNull( _deal.CustomerPacketOptions.Customer );
            Assert.IsEmpty( _deal.CustomerPacketOptions.Customer.Email );
            Assert.IsEmpty( _deal.CustomerPacketOptions.Customer.FirstName);
            Assert.IsEmpty( _deal.CustomerPacketOptions.Customer.LastName);
            Assert.IsEmpty( _deal.CustomerPacketOptions.Customer.PhoneNumber);

            Assert.IsEmpty(_deal.DealerDisclaimer);
            
            //Assert.IsNotNull(_deal.MarketAnalysis);
            //Assert.IsNull(_deal.MarketAnalysis.DealerPreference);
            //Assert.IsNull(_deal.MarketAnalysis.VehiclePreference);
            //Assert.IsNull(_deal.MarketAnalysis.Pricing);

            
            Assert.AreEqual(0, _deal.OfferPrice); // no DAL registered, bogus handles.
            // _deal.PacketType

            
            Assert.AreEqual(DateTime.Now.ToShortDateString(), _deal.ValidUntilDate); // no dal, bogus handles
            Assert.IsNotNull(_deal.ValueAnalyzerPacketOptions);
        }

        [Test]
        public void Foo()
        {
        }

        private static void SetupAutofac()
        {
            var builder = new ContainerBuilder();

            #region marketing XML Autofac registration
            /** The below are all required for MarketingXML generation **/
            //builder.RegisterModule(new CoreModule());
            //builder.RegisterType<DealerGeneralPreferenceDataAccess>().As<IDealerGeneralPreferenceDataAccess>();

            //// Photos
            //builder.RegisterType<PhotoLocatorDataAccess>().As<IPhotoLocatorDataAccess>();
            //builder.RegisterType<PhotoVehiclePreferenceDataAccess>().As<IPhotoVehiclePreferenceDataAccess>();
            //builder.RegisterModule(new PhotoServicesModule());
            ////AdPreview
            //builder.RegisterType<AdPreviewVehiclePreferenceDataAccess>().As<IAdPreviewVehiclePreferenceDataAccess>();
            //builder.RegisterType<EquipmentDataAccess>().As<IEquipmentDataAccess>();
            //// Equipment
            //builder.RegisterType<EquipmentProviderAssignmentDataAccess>().As<IEquipmentProviderAssignmentDataAccess>();
            //builder.RegisterType<EquipmentDataAccess>().As<IEquipmentDataAccess>();
            //builder.RegisterType<EquipmentProviderDataAccess>().As<IEquipmentProviderDataAccess>();
            //// Vehicle summary
            //builder.RegisterType<VehicleSummaryDataAccess>().As<IVehicleSummaryDataAccess>();
            //// Consumer Highlights
            //builder.RegisterType<ConsumerHighlightsDealerPreferenceDataAccess>()
            //    .As<IConsumerHighlightsDealerPreferenceDataAccess>();
            //builder.RegisterType<ConsumerHighlightsVehiclePreferenceDataAccess>()
            //    .As<IConsumerHighlightsVehiclePreferenceDataAccess>();

            //// Certified
            //builder.RegisterType<CertifiedVehiclePreferenceDataAccess>().As<ICertifiedVehiclePreferenceDataAccess>();

            //// Market Analysis
            //builder.RegisterType<MarketAnalysisDealerPreferenceDataAccess>()
            //    .As<IMarketAnalysisDealerPreferenceDataAccess>();
            //builder.RegisterType<MarketAnalysisPricingDataAccess>().As<IMarketAnalysisPricingDataAccess>();
            //builder.RegisterType<MarketAnalysisVehiclePreferenceDataAccess>()
            //    .As<IMarketAnalysisVehiclePreferenceDataAccess>();

            //// MarketListing
            //builder.RegisterType<MarketListingVehiclePreferenceDataAccess>()
            //    .As<IMarketListingVehiclePreferenceDataAccess>();
            //builder.Register(c => new SearchAdapter(c.Resolve<IMarketListingVehiclePreferenceDataAccess>()))
            //    .As<ISearchAdapter>();

            //// Publishing
            //builder.RegisterType<ValueAnalyzerVehiclePreferenceDataAccess>()
            //    .As<IValueAnalyzerVehiclePreferenceDataAccess>();
            #endregion marketing XML

            builder.RegisterModule(new CoreModule());
            builder.RegisterModule(new OltpModule());
            builder.RegisterModule(new PhotoServicesModule());
            builder.RegisterModule(new MerchandisingMessagesModule());

            new PricingDomainRegister().Register(builder);
            new MerchandisingDomainRegistry().Register(builder);
            new MerchandisingModelBuilderRegistry().Register(builder);

            var mockLogger = new Mock<ILogger>().Object;
            builder.RegisterInstance(mockLogger).AsImplementedInterfaces();

            var container = builder.Build();
            Registry.RegisterContainer(container);
        }
    }
}
