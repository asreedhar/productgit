﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;
using FirstLook.Merchandising.ModelBuilder;
using Moq;
using NUnit.Framework;
using PricingPresenters;
using Autofac;

namespace PricingPresentersTests
{
    [TestFixture]
    public class MarketListingsPresenterTests
    {
        private string _ownerHandle, _vehicleHandle;
        private ILogger _throwLogger;
        public IMarketListingSearchAdapter SearchAdapter { get; set; }

        [TestFixtureSetUp]
        public void Setup()
        {
            Registry.Reset();

            // Register the normal dependencies.  We will override these in a bit.
            var builder = new ContainerBuilder();
            
            // Setup test handles used.
            _ownerHandle = Guid.Empty.ToString();
            _vehicleHandle = String.Empty;

            // A test preference transfer object.
            var dto = BuildTestPreference();

            // Build a mock data access object.
            var mockDao = new Mock<IMarketListingVehiclePreferenceDataAccess>();
            mockDao.Setup(d => d.Exists(_ownerHandle, _vehicleHandle)).Returns(true);
            mockDao.Setup(d => d.Fetch(_ownerHandle, _vehicleHandle, 10)).Returns(dto);

            var dao = mockDao.Object;
            builder.RegisterInstance(dao).As<IMarketListingVehiclePreferenceDataAccess>();

            // Build a throwing logger
            var throwLogger = new Mock<ILogger>();
            throwLogger.Setup(l => l.Log(It.IsAny<Exception>())).Throws(new Exception()); // how to capture original exception?
            _throwLogger = throwLogger.Object;

            // Build a search adapter.
            //builder.Register(c => new SearchAdapter(c.Resolve<IMarketListingVehiclePreferenceDataAccess>())).As
            //    <IMarketListingSearchAdapter>();
            builder.Register(c => new MarketListingSearchAdapter())
                .As<IMarketListingSearchAdapter>();

            // Old Ping access
            builder.Register(c => new MarketListingsV1DataAccess()).As<IMarketListingsV1DataAccess>();

            // New Ping (ProfitMAX)
            builder.Register(c => new MarketListingsV2DataAccess()).As<IMarketListingsV2DataAccess>();

            new MerchandisingModelBuilderRegistry().Register(builder);

            var mockLogger = new Mock<ILogger>().Object;
            builder.RegisterInstance(mockLogger).AsImplementedInterfaces();

            // Build the container.
            IContainer container = builder.Build();
            Registry.RegisterContainer(container);

            // Set any of our dependencies (SearchAdapter).
            Registry.BuildUp(this);

            Assert.IsNotNull(SearchAdapter);
        }

        private MarketListingVehiclePreferenceTO BuildTestPreference()
        {
            var dto = new MarketListingVehiclePreferenceTO();

            dto.DisplayListingsIDs = new List<int>();
            dto.ID = 1;
            dto.IsDisplayed = true;
            dto.MileageHigh = 10;
            dto.OwnerHandle = _ownerHandle;
            dto.PriceHigh = 10;
            dto.PriceLow = 1;
            dto.SortAscending = true;
            dto.Sortcode = SortCode.ListPrice;
            dto.VehicleHandle = _vehicleHandle;            

            return dto;
        }

        private Mock<IMarketListingsView> MockView(IMarketListingFilterSpecification search, bool editMode, LayoutKeys layoutKey)
        {
            var mockView = new Mock<IMarketListingsView>();

            mockView.Setup(v => v.OwnerHandle).Returns(Guid.Empty.ToString());
            mockView.Setup(v => v.VehicleHandle).Returns(String.Empty);
            mockView.Setup(v => v.OfferPrice).Returns(0);

            mockView.SetupGet(v => v.EditMode).Returns(editMode);
            mockView.SetupGet(v => v.LayoutKey).Returns(layoutKey);

            mockView.SetupProperty(v => v.XmlDoc);

            mockView.SetupProperty(v => v.DisplaySellingSheetPanel);
            mockView.SetupProperty(v => v.StyleSellingSheetEmpty);

            mockView.SetupProperty(v => v.DisplayMarketComparisonPanel);
            mockView.SetupProperty(v => v.StyleMarketComparisonEmpty);

            return mockView;
        }

        private IMarketListingFilterSpecification MockSearchSpecification(bool isDisplayed)
        {
            // A mock search specification - assume caller wants to display market listings.
            var mockSearch = new Mock<IMarketListingFilterSpecification>();
            mockSearch.SetupGet(s => s.MileageHigh).Returns(2);
            mockSearch.SetupGet(s => s.MileageLow).Returns(1);
            mockSearch.SetupGet(s => s.PriceHigh).Returns(10);
            mockSearch.SetupGet(s => s.PriceLow).Returns(9);

            return mockSearch.Object;
        }

        [Test]
        public void CanCreate()
        {
            var presenter = new MarketListingsPresenter(_throwLogger, SearchAdapter);
            Assert.IsNotNull( presenter );
        }

        [Test]
        public void ViewReferenceValid()
        {
            var mockView = new Mock<IMarketListingsView>();
            var view = mockView.Object;
            var presenter = new MarketListingsPresenter(_throwLogger, SearchAdapter) { View = view };

            Assert.AreEqual( view, presenter.View );
        }


        // If marked for display and data exists, it should be displayed.

        // If in SellingSheet, only one repeater should be shown

        // If in market comparison, only the second repeater should be shown.

        // If in edit mode, the display checkbox column should be shown

        // If in edit mode, do not use the "empty module" styling - always show normal styling.

        // If in market comparison, do not use the "empty module" styling - just hide the panel if no data.

    }

}
