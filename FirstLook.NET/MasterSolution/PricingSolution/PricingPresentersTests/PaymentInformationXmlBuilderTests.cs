﻿using System;
using FirstLook.Merchandising.DomainModel.Marketing.Deal;
using NUnit.Framework;
using PricingPresenters;

namespace PricingPresentersTests
{
    [TestFixture]
    public class PaymentInformationXmlBuilderTests
    {
        private const string EXPECTED =
            "<paymentInformation><lease isDisplayed=\"true\"><paymentAmount>a</paymentAmount><paymentTerms>b</paymentTerms><downPaymentAmount>c</downPaymentAmount><interestRate>d</interestRate><showInterestRate>true</showInterestRate></lease>" +
	        "<own isDisplayed=\"true\"><paymentAmount>1</paymentAmount><paymentTerms>2</paymentTerms><downPaymentAmount>3</downPaymentAmount><interestRate>4</interestRate><showInterestRate>true</showInterestRate></own></paymentInformation>";

        [Test]
        public void EmptyValuesWhenNull()
        {
            var xml = PaymentInformationXmlBuilder.AsXml(null, null);
            Assert.IsNotNull( xml );
            Assert.IsNotEmpty( xml );
            Console.WriteLine(xml);
        }

        [Test]
        public void CorrectValues()
        {
            const string L_AMOUNT = "a";
            const string L_TERM = "b";
            const string L_DOWN = "c";
            const string L_INTEREST = "d";
            const bool L_SHOW = true;
            const bool L_DISPLAYED = true;

            const string O_AMOUNT = "1";
            const string O_TERM = "2";
            const string O_DOWN = "3";
            const string O_INTEREST = "4";
            const bool O_SHOW = true;
            const bool O_DISPLAYED = true;


            var lease = new PaymentInformation
            {
                DownPaymentAmount = L_DOWN,
                InterestRate = L_INTEREST,
                IsDisplayed = L_DISPLAYED,
                PaymentAmount = L_AMOUNT,
                PaymentTerms = L_TERM,
                ShowInterestRate = L_SHOW
            };

            var own = new PaymentInformation
            {
                DownPaymentAmount = O_DOWN,
                InterestRate = O_INTEREST,
                IsDisplayed = O_DISPLAYED,
                PaymentAmount = O_AMOUNT,
                PaymentTerms = O_TERM,
                ShowInterestRate = O_SHOW
            };

            string xml = PaymentInformationXmlBuilder.AsXml(lease, own);
            Console.WriteLine(xml);
           
            Assert.AreEqual( EXPECTED, xml );


        }
    }
}
