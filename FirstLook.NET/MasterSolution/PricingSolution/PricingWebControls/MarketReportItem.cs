using System.Collections.Generic;
using System.Web.UI;
using FirstLook.Pricing.DomainModel.Market;

namespace FirstLook.Pricing.WebControls
{
    internal class MarketReportItem : IReportItem, IStateManager
    {
        public int Id
        {
            get { return _id; }
        }

        public bool Editing
        {
            get { return _editing; }
            set { _editing = value; }
        }

        public bool Expanded
        {
            get { return _expanded; }
            set { _expanded = value; }
        }

        IList<IReportItem> IReportItem.ReportItems
        {
            get { return MarketReportItems; }
        }

        public MarketReportItemCollection MarketReportItems
        {
            get
            {
                if (_marketReportItems == null)
                {
                    _marketReportItems = new MarketReportItemCollection();
                    if (IsTrackingViewState)
                    {
                        ((IStateManager) _marketReportItems).TrackViewState();
                    }
                }

                return _marketReportItems;
            }
        }

        public string Make
        {
            get { return _make; }
        }

        public int MakeId
        {
            get { return _makeId; }
        }

        public int LineId
        {
            get { return _lineId; }
        }

        public int ModelYear
        {
            get { return _modelYear; }
        }

        public string ModelFamily
        {
            get { return _modelFamily; }
        }

        public int ModelFamilyId
        {
            get { return _modelFamilyId; }
        }

        public string Segment
        {
            get { return _segment; }
        }

        public int SegmentId
        {
            get { return _segmentId; }
        }

        public int? ModelConfigurationId
        {
            get { return _modelConfigurationId; }
        }

        public string Series
        {
            get { return _series; }
        }

        public string BodyType
        {
            get { return _bodyType; }
        }

        public string Transmission
        {
            get { return _transmission; }
        }

        public string Engine
        {
            get { return _engine; }
        }

        public string FuelType
        {
            get { return _fuelType; }
        }

        public string PassengerDoors
        {
            get { return _passengerDoors; }
        }

        public string DriveTrain
        {
            get { return _driveTrain; }
        }

        public int? MarketDaysSupply
        {
            get { return _marketDaysSupply; }
        }

        public int? SmallestMarketDaysSupply
        {
            get { return _smallestMarketDaysSupply; }
        }

        public int? UnitsSold
        {
            get { return _unitsSold; }
        }

        public int? NumberOfListings
        {
            get { return _numberOfListings; }
        }

        public int? SumInternetPrice
        {
            get { return _sumInternetPrice; }
        }

        public int? AverageInternetPrice
        {
            get { return _averageInternetPrice; }
        }

        public int? SumInternetMileage
        {
            get { return _sumInternetMileage; }
        }

        public int? AverageInternetMileage
        {
            get { return _averageInternetMileage; }
        }

        public int? UnitsInStock
        {
            get { return _unitsInStock; }
        }

        public static int MaxQuantity = 99;

        public int? Quantity
        {
            get { return _quantity; }
            set
            {
                // to be  consistent with PurchaseListItem.Quantity, PM
                if (value < 0)
                {
                    _quantity = 0;
                }
                else if (value > MaxQuantity)
                {
                    _quantity = MaxQuantity;
                }
                else
                {
                    _quantity = value;   
                }                
            }
        }

        public string Notes
        {
            get { return _notes; }
            set { _notes = value; }
        }

        private int _id;
        private bool _editing;
        private bool _expanded;
        private MarketReportItemCollection _marketReportItems;

        #region Vehicle

        private string _make;
        private int _makeId;
        private int _lineId;
        private int _modelYear;
        private string _modelFamily;
        private int _modelFamilyId;
        private string _segment;
        private int _segmentId;

        #endregion

        #region VehicleConfiguration

        private int? _modelConfigurationId;
        private string _series;
        private string _bodyType;
        private string _transmission;
        private string _engine;
        private string _fuelType;
        private string _passengerDoors;
        private string _driveTrain;

        #endregion

        #region MarketInformation

        private int? _marketDaysSupply;
        private int? _smallestMarketDaysSupply;
        private int? _unitsSold;
        private int? _numberOfListings;
        private int? _sumInternetPrice;
        private int? _averageInternetPrice;
        private int? _sumInternetMileage;
        private int? _averageInternetMileage;
        private int? _unitsInStock;

        #endregion

        #region ListInformation

        private int? _quantity;
        private string _notes;

        #endregion

        public MarketReportItem()
        {
        }

        public MarketReportItem(IReportItem reportItem)
        {
            _id = reportItem.Id;

            foreach (IReportItem list in reportItem.ReportItems)
            {
                MarketReportItems.Add(new MarketReportItem(list));
            }

            _make = reportItem.Make;
            _makeId = reportItem.MakeId;
            _lineId = reportItem.LineId;
            _modelYear = reportItem.ModelYear;
            _modelFamily = reportItem.ModelFamily;
            _modelFamilyId = reportItem.ModelFamilyId;
            _segment = reportItem.Segment;
            _segmentId = reportItem.SegmentId;
            _modelConfigurationId = reportItem.ModelConfigurationId;
            _series = reportItem.Series;
            _bodyType = reportItem.BodyType;
            _transmission = reportItem.Transmission;
            _engine = reportItem.Engine;
            _fuelType = reportItem.FuelType;
            _passengerDoors = reportItem.PassengerDoors;
            _driveTrain = reportItem.DriveTrain;
            _marketDaysSupply = reportItem.MarketDaysSupply;
            _smallestMarketDaysSupply = reportItem.SmallestMarketDaysSupply;
            _unitsSold = reportItem.UnitsSold;
            _numberOfListings = reportItem.NumberOfListings;
            _sumInternetPrice = reportItem.SumInternetPrice;
            _averageInternetPrice = reportItem.AverageInternetPrice;
            _sumInternetMileage = reportItem.SumInternetPrice;
            _averageInternetMileage = reportItem.AverageInternetMileage;
            _unitsInStock = reportItem.UnitsInStock;
            _quantity = reportItem.Quantity;
            _notes = reportItem.Notes;
        }

        #region IStateManager Members

        private bool _isTrackingViewState;

        public bool IsTrackingViewState
        {
            get { return _isTrackingViewState; }
        }

        public void LoadViewState(object state)
        {
            object[] reportItemState = state as object[];

            if (reportItemState != null)
            {
                int idx = 0;

                _id = (int) reportItemState[idx++];
                ((IStateManager) MarketReportItems).LoadViewState(reportItemState[idx++]);
                _make = (string) reportItemState[idx++];
                _makeId = (int)reportItemState[idx++];
                _lineId = (int)reportItemState[idx++];
                _modelYear = (int) reportItemState[idx++];
                _modelFamily = (string) reportItemState[idx++];
                _modelFamilyId = (int) reportItemState[idx++];
                _segment = (string) reportItemState[idx++];
                _segmentId = (int) reportItemState[idx++];
                _modelConfigurationId = (int?) reportItemState[idx++];
                _series = (string) reportItemState[idx++];
                _bodyType = (string) reportItemState[idx++];
                _transmission = (string) reportItemState[idx++];
                _engine = (string) reportItemState[idx++];
                _fuelType = (string) reportItemState[idx++];
                _passengerDoors = (string) reportItemState[idx++];
                _driveTrain = (string) reportItemState[idx++];
                _marketDaysSupply = (int?) reportItemState[idx++];
                _smallestMarketDaysSupply = (int?)reportItemState[idx++];
                _unitsSold = (int?) reportItemState[idx++];
                _numberOfListings = (int?) reportItemState[idx++];
                _sumInternetPrice = (int?) reportItemState[idx++];
                _averageInternetPrice = (int?) reportItemState[idx++];
                _sumInternetMileage = (int?) reportItemState[idx++];
                _averageInternetMileage = (int?) reportItemState[idx++];
                _unitsInStock = (int?) reportItemState[idx++];
                _quantity = (int?) reportItemState[idx++];
                _notes = (string) reportItemState[idx++];
                _editing = (bool) reportItemState[idx++];
                _expanded = (bool)reportItemState[idx];
            }
        }

        public object SaveViewState()
        {
            object[] reportItemState = new object[31];

            int idx = 0;

            reportItemState[idx++] = _id;
            reportItemState[idx++] = ((IStateManager) MarketReportItems).SaveViewState();
            reportItemState[idx++] = _make;
            reportItemState[idx++] = _makeId;
            reportItemState[idx++] = _lineId;
            reportItemState[idx++] = _modelYear;
            reportItemState[idx++] = _modelFamily;
            reportItemState[idx++] = _modelFamilyId;
            reportItemState[idx++] = _segment;
            reportItemState[idx++] = _segmentId;
            reportItemState[idx++] = _modelConfigurationId;
            reportItemState[idx++] = _series;
            reportItemState[idx++] = _bodyType;
            reportItemState[idx++] = _transmission;
            reportItemState[idx++] = _engine;
            reportItemState[idx++] = _fuelType;
            reportItemState[idx++] = _passengerDoors;
            reportItemState[idx++] = _driveTrain;
            reportItemState[idx++] = _marketDaysSupply;
            reportItemState[idx++] = _smallestMarketDaysSupply;
            reportItemState[idx++] = _unitsSold;
            reportItemState[idx++] = _numberOfListings;
            reportItemState[idx++] = _sumInternetPrice;
            reportItemState[idx++] = _averageInternetPrice;
            reportItemState[idx++] = _sumInternetMileage;
            reportItemState[idx++] = _averageInternetMileage;
            reportItemState[idx++] = _unitsInStock;
            reportItemState[idx++] = _quantity;
            reportItemState[idx++] = _notes;
            reportItemState[idx++] = _editing;
            reportItemState[idx] = _expanded;

            return reportItemState;
        }

        public void TrackViewState()
        {
            _isTrackingViewState = true;

            ((IStateManager) MarketReportItems).TrackViewState();
        }

        #endregion
    }
}