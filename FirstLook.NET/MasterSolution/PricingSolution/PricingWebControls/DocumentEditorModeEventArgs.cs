using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace FirstLook.Pricing.WebControls
{
	public class DocumentEditorModeEventArgs : CancelEventArgs
	{
		private DocumentEditorMode newMode;

		public DocumentEditorModeEventArgs(DocumentEditorMode newMode)
        {
            this.newMode = newMode;
        }

		public DocumentEditorMode NewMode
        {
            get { return newMode; }
            set { newMode = value; }
        }
	}
}
