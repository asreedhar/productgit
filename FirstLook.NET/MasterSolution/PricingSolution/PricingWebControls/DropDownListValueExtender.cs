using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: WebResource("FirstLook.Pricing.WebControls.DropDownListValueBehavior.js", "text/javascript")]
[assembly: WebResource("FirstLook.Pricing.WebControls.DropDownListValueBehavior.debug.js", "text/javascript")]

namespace FirstLook.Pricing.WebControls
{
    [TargetControlType(typeof(DropDownList))]
    public class DropDownListValueExtender : ExtenderControl
    {
        [DefaultValue(""),
         Description("Button to which clicks are delegated"),
         Category("Behavior"),
         IDReferenceProperty(typeof(DropDownList))]
        public string DropDownListID
        {
            get
            {
                string value = (string)ViewState["DropDownListID"];
                if (string.IsNullOrEmpty(value))
                    return string.Empty;
                return value;
            }
            set
            {
                if (string.Compare(DropDownListID, value) != 0)
                {
                    ViewState["DropDownListID"] = value;
                }
            }
        }

        protected override IEnumerable<ScriptDescriptor> GetScriptDescriptors(Control targetControl)
        {
            ScriptBehaviorDescriptor descriptor = new ScriptBehaviorDescriptor("FirstLook.Pricing.WebControls.DropDownListValueBehavior", targetControl.ClientID);
            descriptor.AddElementProperty("DropDownList", FindControl(DropDownListID).ClientID);
            return new ScriptDescriptor[] { descriptor };
        }

        protected override IEnumerable<ScriptReference> GetScriptReferences()
        {
            ScriptReference reference = new ScriptReference("FirstLook.Pricing.WebControls.DropDownListValueBehavior.js", "FirstLook.Pricing.WebControls");
            return new ScriptReference[] { reference };
        }
    }
}
