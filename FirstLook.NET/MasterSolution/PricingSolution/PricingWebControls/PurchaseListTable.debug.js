Type.registerNamespace('FirstLook.Pricing.WebControls');

FirstLook.Pricing.WebControls.PurchaseListTable = function(element) {
    FirstLook.Pricing.WebControls.PurchaseListTable.initializeBase(this, [element]);
    this.delegates = {};
    var props = [
    "confirm_dirty_unload",
    "is_dirty",
    "internal_is_dirty"
    ];
    for (var i = 0, l = props.length; i < l; i++) {
        this.create_getter_setter(props[i]);
    };
};

FirstLook.Pricing.WebControls.PurchaseListTable.prototype = {
    initialize: function() {
        FirstLook.Pricing.WebControls.PurchaseListTable.callBaseMethod(this, "initialize");
    },
    updated: function() {
        this.delegates['onPropertyChanged'] = Function.createDelegate(this, this.onPropertyChanged);
        this.delegates['onBeforeUnload'] = Function.createDelegate(this, this.onBeforeUnload);
        this.delegates['onInputKeyUp'] = Function.createDelegate(this, this.onInputKeyUp);

        this.add_propertyChanged(this.delegates['onPropertyChanged']);

        window.onbeforeunload = this.delegates['onBeforeUnload'];

        var inputs = this.get_inputs();

        for (var i = inputs.length - 1; i >= 0; i--) {
            this.add_handler(inputs[i], 'keydown', this.delegates['onInputKeyUp']);
        };

        FirstLook.Pricing.WebControls.PurchaseListTable.callBaseMethod(this, "updated");
    },
    dispose: function() {
        this.remove_propertyChanged(this.delegates['onPropertyChanged']);

        this.remove_handlers();

        window.onbeforeunload = null;

        this.delegates = undefined;

        FirstLook.Pricing.WebControls.PurchaseListTable.callBaseMethod(this, "dispose");
    },
    // ===========
    // = Methods =
    // ===========
    get_inputs: function() {
        if (!!!this._inputs) {
            var el = this.get_element(),
            inputs = el.getElementsByTagName('input'),
            input_array = [],
            i;
            for (var i = inputs.length - 1; i >= 0; i--) {
                if (inputs[i].type == "text") {
                    input_array.push(inputs[i]);
                }
            };

            this._inputs = input_array;
        };

        return this._inputs;
    },
    get_textareas: function() {
        if (!!!this._textareas) {
            var el = this.get_element(),
            textareas = el.getElementsByTagName('textarea'),
            textarea_array = [],
            i;
            for (var i = textareas.length - 1; i >= 0; i--) {
                textarea_array.push(textareas[i]);
            };

            this._textareas = textarea_array;
        };

        return this._textareas;
    },
    update_is_dirty: function() {
        if (this.get_is_dirty()) {
            return true;
        }
        var el = this.get_element(),
        inputs = this.get_inputs(),
        textareas = this.get_textareas(),
        i;
        for (i = inputs.length - 1; i >= 0; i--) {
            if (inputs[i].value != inputs[i].defaultValue) {
                this.set_internal_is_dirty(true);
                return;
            }
        };
        for (i = textareas.length - 1; i >= 0; i--) {
            if (textareas[i].value != textareas[i].defaultValue) {
                this.set_internal_is_dirty(true);
                return;
            }
        };
        this.set_internal_is_dirty(false);
    },
    bind_window_before_unload: function() {
        window.onbeforeunload = this.delegates["onBeforeUnload"];
    },
    // ==========
    // = Events =
    // ==========
    onPropertyChanged: function(sender, args) {
        var property = args.get_propertyName(),
        source_class = sender.get_name();

        Sys.Debug.assert( !! !FirstLook.Pricing.WebControls.PurchaseListTable["debug_" + property], property);

        if ( !! FirstLook.Pricing.WebControls.PurchaseListTable.Trace || !!FirstLook.Pricing.WebControls.PurchaseListTable["trace_" + property]) {
            Sys.Debug.traceDump(sender["get_" + property](), sender.get_id() + ":" + property);
        };
    },
    onBeforeUnload: function(ev) {
        this.update_is_dirty();
        if (this.get_is_dirty() || this.get_internal_is_dirty()) {
            return "You have unsaved changes to your Purchase List. Would you like to navigate away?";
        }
    },
    onInputKeyUp: function(ev) {
        if (ev.keyCode == Sys.UI.Key.enter) {
            ev.preventDefault();
            ev.stopPropagation();
        }
    },
    // ==================
    // = Helper Methods =
    // ==================
    create_getter_setter: function(property) {
        // =================================================================
        // = Generic Function to create AJAX.net style getters and setters =
        // =================================================================
        if (this["get_" + property] === undefined) {
            this["get_" + property] = (function(thisProp) {
                return function get_property() {
                    return this[thisProp];
                };
            })("_" + property);
        };
        if (this["set_" + property] === undefined) {
            this["set_" + property] = (function(thisProp, raiseType) {
                return function set_property(value) {
                    if (value !== this[thisProp]) {
                        this[thisProp] = value;
                        this.raisePropertyChanged(raiseType);
                    };
                };
            })("_" + property, property);
        };
    },
    add_handler: function(el, type, fn) {
        if (this._eventCache === undefined) {
            this._eventCache = [];
        }
        this._eventCache.push([el, type, fn]);
        $addHandler(el, type, fn);
    },
    remove_handlers: function() {
        if (this._eventCache !== undefined) {
            for (var i = 0, l = this._eventCache.length; i < l; i++) {
                $removeHandler(this._eventCache[i][0], this._eventCache[i][1], this._eventCache[i][2]);
                this._eventCache[i][0] = undefined;
                this._eventCache[i][1] = undefined;
                this._eventCache[i][2] = undefined;
            };
            delete this._eventCache;
        };
    }
};

if (FirstLook.Pricing.WebControls.PurchaseListTable.registerClass != undefined)
 FirstLook.Pricing.WebControls.PurchaseListTable.registerClass('FirstLook.Pricing.WebControls.PurchaseListTable', Sys.UI.Behavior);

Sys.Application.notifyScriptLoaded();