using System;

namespace FirstLook.Pricing.WebControls
{
    public class OrderedTableViewUpdatedEventArgs : EventArgs
    {
        private readonly int _affectedRows;
        private readonly Exception _exception;
        private bool _exceptionHandled;
        private bool _keepInEditMode;

        public OrderedTableViewUpdatedEventArgs(int affectedRows, Exception exception)
        {
            _affectedRows = affectedRows;
            _exception = exception;
        }

        public int AffectedRows
        {
            get { return _affectedRows; }
        }

        public bool KeepInEditMode
        {
            get { return _keepInEditMode; }
            set { _keepInEditMode = value; }
        }

        public Exception Exception
        {
            get { return _exception; }
        }

        public bool ExceptionHandled
        {
            get { return _exceptionHandled; }
            set { _exceptionHandled = value; }
        }
    }
}