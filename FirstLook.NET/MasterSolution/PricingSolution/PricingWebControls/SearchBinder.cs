using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Utilities;
using FirstLook.Common.WebControls.Extenders;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search;

namespace FirstLook.Pricing.WebControls
{
    public class SearchBinder : IDisposable
    {
        private Search _search;

        public SearchBinder(Search search)
        {
            _search = search;
        }

        #region Web Controls

        private TextBox _searchDistanceTextBox;

        public TextBox SearchDistanceTextBox
        {
            get { return _searchDistanceTextBox; }
            set { _searchDistanceTextBox = value; }
        }

        private DropDownList _searchMileageStart;

        public DropDownList SearchMileageStart
        {
            get { return _searchMileageStart; }
            set { _searchMileageStart = value; }
        }

        private DropDownList _searchMileageEnd;

        public DropDownList SearchMileageEnd
        {
            get { return _searchMileageEnd; }
            set { _searchMileageEnd = value; }
        }

        private SliderExtender _searchDistanceSliderExtender;

        public SliderExtender SearchDistanceSliderExtender
        {
            get { return _searchDistanceSliderExtender; }
            set { _searchDistanceSliderExtender = value; }
        }

        private SliderLabelExtender _searchDistanceSliderLabelExtender;

        public SliderLabelExtender SearchDistanceSliderLabelExtender
        {
            get { return _searchDistanceSliderLabelExtender; }
            set { _searchDistanceSliderLabelExtender = value; }
        }

		private CheckBox _searchCertified;

		public CheckBox SearchCertified
		{
			get { return _searchCertified; }
			set { _searchCertified = value; }
		}

		private CheckBox _searchColor;

		public CheckBox SearchColor
		{
			get { return _searchColor; }
			set { _searchColor = value; }
		}

        #endregion

        #region Business Methods

        public virtual void RegisterEventListeners()
        {
            SearchDistanceTextBox.TextChanged += SearchDistance_Changed;

            SearchMileageStart.DataBound += SearchMileageStart_DataBound;
            SearchMileageStart.SelectedIndexChanged += SearchMileageStart_Changed;

            SearchMileageEnd.DataBound += SearchMileageEnd_DataBound;
            SearchMileageEnd.SelectedIndexChanged += SearchMileageEnd_Changed;

			SearchCertified.CheckedChanged += SearchCertified_Changed;

			SearchColor.CheckedChanged += SearchColor_Changed;
        }

    	public virtual void PopulateSearchCriteria()
        {
            SearchDistanceTextBox.Text = _search.Distance.Id.ToString(CultureInfo.InvariantCulture);

            Bind(SearchMileageStart, _search.Mileages);

            Bind(SearchMileageEnd, _search.Mileages);

            IList<Distance> distances = _search.Distances;

            if (distances.Count > 0)
            {
                SearchDistanceSliderExtender.Minimum = distances[0].Id;

                SearchDistanceSliderExtender.Maximum = distances[distances.Count - 1].Id;

                SearchDistanceSliderExtender.Steps = distances.Count;

                SearchDistanceSliderLabelExtender.ValueMappings.Clear();

                foreach (Distance distance in distances)
                {
                    SearchDistanceSliderLabelExtender.ValueMappings.Add(
                        distance.Id.ToString(CultureInfo.InvariantCulture),
                        distance.Value.ToString(CultureInfo.InvariantCulture));
                }
            }

    		SearchCertified.Checked = _search.MatchCertified;

			SearchColor.Checked = _search.MatchColor;
        }

        public virtual bool UpdateSearch(string updateUser)
        {
            MarshallSearchCriteria();

            if (_search.IsDirty)
            {
				_search.UpdateUser = updateUser;

				_search = _search.Save();

                return true;
            }

            return false;
        }

        public virtual void MarshallSearchCriteria()
        {
            SearchDistance_Changed(this, EventArgs.Empty);

            SearchMileageStart_Changed(this, EventArgs.Empty);

            SearchMileageEnd_Changed(this, EventArgs.Empty);

			SearchCertified_Changed(this, EventArgs.Empty);

			SearchColor_Changed(this, EventArgs.Empty);
        }

        internal void ChangeSearch(Search search)
        {
            _search = search;
        }

        #endregion

        #region Changed Events

        protected void SearchDistance_Changed(object sender, EventArgs e)
        {
            foreach (Distance d in _search.Distances)
                if (d.Id == Int32Helper.ToInt32(SearchDistanceTextBox.Text))
                    _search.Distance = d;
        }

        protected void SearchMileageStart_Changed(object sender, EventArgs e)
        {
            foreach (string value in GetSelectedItems(SearchMileageStart))
                foreach (Mileage mileage in _search.Mileages)
                    if (string.Equals(mileage.Value.ToString(CultureInfo.InvariantCulture), value))
                        _search.LowMileage = mileage;
        }

        protected void SearchMileageEnd_Changed(object sender, EventArgs e)
        {
            foreach (string value in GetSelectedItems(SearchMileageEnd))
            {
                if ("max".Equals(value))
                {
                    _search.HighMileage = null;
                }
                else
                {
                    foreach (Mileage mileage in _search.Mileages)
                        if (string.Equals(mileage.Value.ToString(CultureInfo.InvariantCulture), value))
                            _search.HighMileage = Mileage.NewMileage(mileage.Value - 1);
                }
            }
        }

		private void SearchCertified_Changed(object sender, EventArgs e)
		{
			_search.MatchCertified = _searchCertified.Checked;
		}

		private void SearchColor_Changed(object sender, EventArgs e)
		{
			_search.MatchColor = _searchColor.Checked;
		}

        #endregion

        #region Data Bound Events

        protected void SearchMileageStart_DataBound(object sender, EventArgs e)
        {
            SearchMileageStart.SelectedValue = _search.LowMileage.ToString();
        }

        protected void SearchMileageEnd_DataBound(object sender, EventArgs e)
        {
            if (SearchMileageEnd.Items.Count == 0) return;  // Nothing to do here

            ListItem unlimitedListItem = new ListItem("Unlimited", "max");
            if (!SearchMileageEnd.Items[SearchMileageEnd.Items.Count - 1].Equals(unlimitedListItem)) // Only Run Once
            {
                SearchMileageEnd.Items.RemoveAt(0); // stops 0 to 0 mileage from occuring 
                SearchMileageEnd.Items.Add(unlimitedListItem); 
            }

            Mileage mileage = _search.HighMileage;
            if (mileage != null)
            {
                SearchMileageEnd.SelectedValue = (mileage.Value + 1).ToString();
            }
            else
            {
                SearchMileageEnd.SelectedIndex = SearchMileageEnd.Items.Count - 1;
            }
                
            if (SearchMileageEnd.SelectedIndex < SearchMileageStart.SelectedIndex)
            {
                SearchMileageEnd.SelectedIndex = SearchMileageStart.SelectedIndex;
            }
        }

        #endregion

        #region Helper Methods

        protected static List<string> GetSelectedItems(ListControl list)
        {
            List<string> values = new List<string>();

            foreach (ListItem item in list.Items)
                if (item.Selected)
                    values.Add(item.Value);

            return values;
        }

        protected static void Bind(BaseDataBoundControl control, IEnumerable data)
        {
            control.DataSource = data;
            control.DataBind();
        }

        #endregion

        #region IDisposable Members

        private bool _disposed;

        public void Dispose()
        {
            if (!_disposed)
            {
                try
                {
                    Dispose(true);
                }
                finally
                {
                    _disposed = true;
                }
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                SearchDistanceTextBox.TextChanged -= SearchDistance_Changed;

                SearchMileageStart.DataBound -= SearchMileageStart_DataBound;
                SearchMileageStart.SelectedIndexChanged -= SearchMileageStart_Changed;

                SearchMileageEnd.DataBound -= SearchMileageEnd_DataBound;
                SearchMileageEnd.SelectedIndexChanged -= SearchMileageEnd_Changed;

            	SearchCertified.CheckedChanged -= SearchCertified_Changed;

				SearchColor.CheckedChanged -= SearchColor_Changed;
            }

            _searchDistanceSliderExtender = null;
            _searchDistanceSliderLabelExtender = null;
            _searchDistanceTextBox = null;
            _searchMileageEnd = null;
            _searchMileageStart = null;
			_searchCertified = null;
        	_searchColor = null;
        }

        #endregion
    }
}
