
using System.Diagnostics.CodeAnalysis;

// common abbreviations

[assembly: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope = "member", Target = "FirstLook.Pricing.WebControls.MarketPricing.#PctAvgMarketPrice", MessageId = "Avg")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope = "member", Target = "FirstLook.Pricing.WebControls.MarketPricing.#NumComparableListings", MessageId = "Num")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope = "member", Target = "FirstLook.Pricing.WebControls.MarketPricing.#MaxPctMarketAvg", MessageId = "Avg")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope = "member", Target = "FirstLook.Pricing.WebControls.MarketPricing.#AvgVehicleMileage", MessageId = "Avg")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope = "member", Target = "FirstLook.Pricing.WebControls.MarketPricing.#AvgMarketPrice", MessageId = "Avg")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope = "member", Target = "FirstLook.Pricing.WebControls.MarketPricing.#NumListings", MessageId = "Num")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope = "member", Target = "FirstLook.Pricing.WebControls.MarketPricing.#MinPctMarketAvg", MessageId = "Avg")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope = "member", Target = "FirstLook.Pricing.WebControls.MarketPricingSliderBridge.#MaxMktPct", MessageId = "Mkt")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope = "member", Target = "FirstLook.Pricing.WebControls.MarketPricingSliderBridge.#MinMktPct", MessageId = "Mkt")]
[assembly: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope = "member", Target = "FirstLook.Pricing.WebControls.MarketPricingSliderBridge.#AvgPrice", MessageId = "Avg")]

