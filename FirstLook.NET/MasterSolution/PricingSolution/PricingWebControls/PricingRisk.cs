namespace FirstLook.Pricing.WebControls
{
	public enum PricingRisk
	{
		NotAnalyzed,
		Underpriced,
		PricedAtMarket,
		Overpriced,
		NoPrice
	}
}
