using System;
using System.Text.RegularExpressions;
using System.Web;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search;

namespace FirstLook.Pricing.WebControls
{
    public static class SearchHelper
    {
        private const string SearchKeyContextKey = "SearchKey";
        private const string SearchContextKey = "Search";
        private const string SearchSummaryCollectionContextKey = "SearchSummaryCollection";
        private const string SearchSummaryInfoContextKey = "SearchSummaryInfo";

        public static SearchKey GetSearchKey(HttpContext context)
        {
            SearchKey key = (SearchKey)context.Items[SearchKeyContextKey];

            if (key == null)
            {
                key = new SearchKey(
                    context.Request.QueryString["oh"],
                    context.Request.QueryString["vh"],
                    context.Request.QueryString["sh"]);

                context.Items[SearchKeyContextKey] = key;
            }

            return key;
        }

        public static SearchSummaryCollection GetSearchSummaryCollection(HttpContext context)
        {
            SearchSummaryCollection values = (SearchSummaryCollection) context.Items[SearchSummaryCollectionContextKey];

            if (values == null)
            {
                values = SearchSummaryCollection.GetSearchSummaryCollection(GetSearchKey(context));

                context.Items[SearchSummaryCollectionContextKey] = values;
            }

            return values;
        }

        public static SearchSummaryInfo GetSearchSummaryInfo(HttpContext context)
        {
            SearchSummaryInfo value = (SearchSummaryInfo) context.Items[SearchSummaryInfoContextKey];

            if (value == null)
            {
                value = SearchSummaryInfo.GetSearchSummaryInfo(context.Request.QueryString["oh"],
                                                                context.Request.QueryString["vh"], 
                                                                context.Request["sh"],
                                                                GetSearch(context));
                context.Items[SearchSummaryInfoContextKey] = value;
            }

            return value;
        }

        public static void ClearSearch(HttpContext context)
        {
            context.Items.Remove(SearchSummaryCollectionContextKey);
            context.Items.Remove(SearchSummaryInfoContextKey);
            context.Items.Remove(SearchContextKey);
        }

        public static Search GetSearch(HttpContext context)
        {
            Search value = (Search)context.Items[SearchContextKey];

            if (value == null)
            {
                value = Search.GetSearch(GetSearchKey(context));

                context.Items[SearchContextKey] = value;
            }

            return value;
        }


        public static string GetModifySearchUrl(HttpRequest request)
        {
            return string.Format("~/Pages/Internet/ModifySearch.aspx?oh={0}&vh={1}&sh={2}",
                                 request.QueryString["oh"],
                                 request.QueryString["vh"],
                                 request.QueryString["sh"]);
        }
    }
}