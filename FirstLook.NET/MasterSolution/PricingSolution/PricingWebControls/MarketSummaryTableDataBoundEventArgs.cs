using System;

namespace FirstLook.Pricing.WebControls
{
    public class MarketSummaryTableDataBoundEventArgs : EventArgs
    {
        private int _you;
        private string _title;
        private string _className;

        public int You
        {
            get { return _you; }
            set { _you = value; }
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public string ClassName
        {
            get { return _className; }
            set { _className = value; }
        }

        public MarketSummaryTableDataBoundEventArgs(int you, string title, string className)
        {
            _you = you;
            _title = title;
            _className = className;
        }

        public MarketSummaryTableDataBoundEventArgs()
        {
        }
    }
}
