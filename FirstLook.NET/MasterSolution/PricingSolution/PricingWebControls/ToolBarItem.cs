using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstLook.Pricing.WebControls
{
	[DefaultProperty("Name")]
	public class ToolBarItem : IStateManager
	{
		private StateBag _viewState;

		private Style _controlStyle;

		[Category("Appearance"), DefaultValue(""), Description("WebControl_CSSClassName")]
		public string CssClass
		{
			get
			{
				if (!ControlStyleCreated)
				{
					return string.Empty;
				}
				return ControlStyle.CssClass;
			}
			set
			{
				ControlStyle.CssClass = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Advanced),
		 Browsable(false),
		 DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
		 Description("WebControl_ControlStyleCreated")]
		public bool ControlStyleCreated
		{
			get
			{
				return (_controlStyle != null);
			}
		}

		[Description("WebControl_ControlStyle"),
		 Browsable(false),
		 DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public Style ControlStyle
		{
			get
			{
				if (_controlStyle == null)
				{
					_controlStyle = CreateControlStyle();
					if (IsTrackingViewState)
					{
						((IStateManager)_controlStyle).TrackViewState();
					}
				}
				return _controlStyle;
			}
		}

		[DefaultValue(""), Localizable(false), Bindable(false), Category("Data"), Description("Data Field")]
		public string DataField
		{
			get
			{
				string str = (string)ViewState["DataField"];
				if (string.IsNullOrEmpty(str))
					return string.Empty;
				return str;
			}
			set
			{
				if (string.Compare(DataField, value, StringComparison.InvariantCulture) != 0)
				{
					if (string.IsNullOrEmpty(value))
					{
						ViewState.Remove("DataField");
					}
					else
					{
						ViewState["DataField"] = value;
					}
				}
			}
		}

		[DefaultValue(false), Localizable(false), Bindable(false), Category("Behavior"), Description("Enabled")]
		public bool Enabled
		{
			get
			{
				object value = ViewState["Enabled"];
				if (value == null)
					return false;
				return (bool) value;
			}
			set
			{
				if (Enabled != value)
				{
					if (!value)
					{
						ViewState.Remove("Enabled");
					}
					else
					{
						ViewState["Enabled"] = value;
					}
				}
			}
		}

        [DefaultValue(true), Localizable(false), Bindable(false), Category("Behavior"), Description("Visible")]
        public bool Visible
        {
            get
            {
                object value = ViewState["Visible"];
                if (value == null)
                    return true;
                return (bool)value;
            }
            set
            {
                if (Visible != value)
                {
                    if (value)
                    {
                        ViewState.Remove("Visible");
                    }
                    else
                    {
                        ViewState["Visible"] = value;
                    }
                }
            }
        }
        
        [DefaultValue(""), Localizable(true), Bindable(true), Category("Appearance"), Description("Fragment Name")]
		public string Name
		{
			get
			{
				string str = (string)ViewState["Name"];
				if (string.IsNullOrEmpty(str))
					return string.Empty;
				return str;
			}
			set
			{
				if (string.Compare(Name, value, StringComparison.InvariantCulture) != 0)
				{
					if (string.IsNullOrEmpty(value))
					{
						ViewState.Remove("Name");
					}
					else
					{
						ViewState["Name"] = value;
					}
				}
			}
		}

        [DefaultValue(""), Localizable(true), Bindable(true), Category("Appearance"), Description("")]
        public string Text
        {
            get
            {
                string str = (string)ViewState["Text"];
                if (string.IsNullOrEmpty(str))
                    return string.Empty;
                return str;
            }
            set
            {
                if (string.Compare(Text, value, StringComparison.InvariantCulture) != 0)
                {
                    if (string.IsNullOrEmpty(value))
                    {
                        ViewState.Remove("Text");
                    }
                    else
                    {
                        ViewState["Text"] = value;
                    }
                }
            }
        }

		internal void SetDirty()
		{
			ViewState.SetDirty(true);
		}

		protected StateBag ViewState
		{
			get
			{
				if (_viewState == null)
				{
					_viewState = new StateBag(false);

					if (IsTrackingViewState)
					{
						((IStateManager)_viewState).TrackViewState();
					}
				}

				return _viewState;
			}
		}

		protected virtual Style CreateControlStyle()
		{
			return new Style(ViewState);
		}

		#region IStateManager Members

		private bool trackViewState;

		protected virtual bool IsTrackingViewState
		{
			get
			{
				return trackViewState;
			}
		}

		bool IStateManager.IsTrackingViewState
		{
			get { return IsTrackingViewState; }
		}

		protected virtual void LoadViewState(object state)
		{
			object[] values = (object[]) state;

			if (values.Length == 1)
			{
				if (values[0] != null)
				{
					((IStateManager)ViewState).LoadViewState(values[0]);
				}
			}
		}

		void IStateManager.LoadViewState(object state)
		{
			LoadViewState(state);
		}

		protected virtual object SaveViewState()
		{
			object[] values = new object[1];

			if (_viewState != null)
			{
				values[0] = ((IStateManager)_viewState).SaveViewState();
			}

			return values;
		}

		object IStateManager.SaveViewState()
		{
			return SaveViewState();
		}

		protected virtual void TrackViewState()
		{
			trackViewState = true;

			if (_viewState != null)
			{
				((IStateManager)_viewState).TrackViewState();
			}
		}

		void IStateManager.TrackViewState()
		{
			TrackViewState();
		}

		#endregion
	}
}
