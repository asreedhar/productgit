using System;
using System.Collections;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;

namespace FirstLook.Pricing.WebControls
{
    public class MarketPricing : DataBoundControl
    {
        protected override void Render(HtmlTextWriter writer)
        {
            // do not render any output what so ever
        }

        #region Properties

        private T GetViewState<T>(string key)
        {
            object obj = ViewState[key];
            if (obj == null)
                return default(T);
            return (T) obj;
        }

        private void SetViewState<T>(string key, T value) where T : struct
        {
            T old = GetViewState<T>(key);
            if (!value.Equals(old))
            {
                ViewState[key] = value;   
            }
        }

        private void SetViewState<T>(string key, T? value) where T : struct
        {
            if (value.HasValue)
            {
                T old = GetViewState<T>(key);
                if (!value.Equals(old))
                {
                    ViewState[key] = value;
                }   
            }
            else
            {
                ViewState.Remove(key);
            }
        }

        public int VehicleEntityTypeID
        {
            get { return GetViewState<int>("VehicleEntityTypeID"); }
            set { SetViewState("VehicleEntityTypeID", value); }
        }

        public int VehicleEntityID
        {
            get { return GetViewState<int>("VehicleEntityID"); }
            set { SetViewState("VehicleEntityID", value); }
        }

        public int? VehicleMileage
        {
            get { return GetViewState<int?>("VehicleMileage"); }
            set { SetViewState("VehicleMileage", value); }
        }

        public int? MarketDaySupply
        {
            get { return GetViewState<int?>("MarketDaySupply"); }
            set { SetViewState("MarketDaySupply", value); }
        }

        public int? MinMarketPrice
        {
            get { return GetViewState<int?>("MinMarketPrice"); }
            set { SetViewState("MinMarketPrice", value); }
        }

        public int? AvgMarketPrice
        {
            get { return GetViewState<int?>("AvgMarketPrice"); }
            set { SetViewState("AvgMarketPrice", value); }
        }

        public int? MaxMarketPrice
        {
            get { return GetViewState<int?>("MaxMarketPrice"); }
            set { SetViewState("MaxMarketPrice", value); }
        }

        public int? MinVehicleMileage
        {
            get { return GetViewState<int?>("MinVehicleMileage"); }
            set { SetViewState("MinVehicleMileage", value); }
        }

        public int? AvgVehicleMileage
        {
            get { return GetViewState<int?>("AvgVehicleMileage"); }
            set { SetViewState("AvgVehicleMileage", value); }
        }

        public int? MaxVehicleMileage
        {
            get { return GetViewState<int?>("MaxVehicleMileage"); }
            set { SetViewState("MaxVehicleMileage", value); }
        }

        public int? InventoryUnitCost
        {
            get { return GetViewState<int?>("InventoryUnitCost"); }
            set { SetViewState("InventoryUnitCost", value); }
        }

        public int? InventoryListPrice
        {
            get { return GetViewState<int?>("InventoryListPrice"); }
            set { SetViewState("InventoryListPrice", value); }
        }

        public int? MinPctMarketAvg
        {
            get { return GetViewState<int?>("MinPctMarketAvg"); }
            set { SetViewState("MinPctMarketAvg", value); }
        }

        public int? MaxPctMarketAvg
        {
            get { return GetViewState<int?>("MaxPctMarketAvg"); }
            set { SetViewState("MaxPctMarketAvg", value); }
        }

        public int? MinGrossProfit
        {
            get { return GetViewState<int?>("MinGrossProfit"); }
            set { SetViewState("MinGrossProfit", value); }
        }

        public int? MinPctMarketValue
        {
            get { return GetViewState<int?>("MinPctMarketValue"); }
            set { SetViewState("MinPctMarketValue", value); }
        }

        public int? MaxPctMarketValue
        {
            get { return GetViewState<int?>("MaxPctMarketValue"); }
            set { SetViewState("MaxPctMarketValue", value); }
        }

        public int? PctAvgMarketPrice
        {
            get { return GetViewState<int?>("PctAvgMarketPrice"); }
            set { SetViewState("PctAvgMarketPrice", value); }
        }

        public int? MinComparableMarketPrice
        {
            get { return GetViewState<int?>("MinComparableMarketPrice"); }
            set { SetViewState("MinComparableMarketPrice", value); }
        }

        public int? MaxComparableMarketPrice
        {
            get { return GetViewState<int?>("MaxComparableMarketPrice"); }
            set { SetViewState("MaxComparableMarketPrice", value); }
        }

        public int NumListings
        {
            get { return GetViewState<int>("NumListings"); }
            set { SetViewState("NumListings", value); }
        }

        public int NumComparableListings
        {
            get { return GetViewState<int>("NumComparableListings"); }
            set { SetViewState("NumComparableListings", value); }
        }

        public int? AppraisalValue
        {
            get { return GetViewState<int?>("AppraisalValue"); }
            set { SetViewState("AppraisalValue", value); }
        }

        public int? EstimatedReconditioningCost
        {
            get { return GetViewState<int?>("EstimatedReconditioningCost"); }
            set { SetViewState("EstimatedReconditioningCost", value); }
        }

        public int? TargetGrossProfit
        {
            get { return GetViewState<int?>("TargetGrossProfit"); }
            set { SetViewState("TargetGrossProfit", value); }
        }

        public int? SearchRadius
        {
            get { return GetViewState<int?>("SearchRadius"); }
            set { SetViewState("SearchRadius", value); }
        }

        public string Vin
        {
            get
            {
                object value = ViewState["Vin"];
                if (value == null)
                    return string.Empty;
                return (string) value;
            }
            set
            {
                if (string.Compare(Vin, value, false) != 0)
                {
                    ViewState["Vin"] = value;
                }
            }
        }

        public int? JDPowerAverageSalePrice
        {
            get { return GetViewState<int?>("JDPowerAverageSalePrice"); }
            set { SetViewState("JDPowerAverageSalePrice", value); }
        }

        public int? NaaaAverageSalePrice
        {
            get { return GetViewState<int?>("NaaaAverageSalePrice"); }
            set { SetViewState("NaaaAverageSalePrice", value); }
        }

        #endregion

        #region Business Methods

        public bool HasMarket()
        {
            return NumComparableListings >= 5;
        }

        public bool NotAnalyzed()
        {
            return NotAnalyzed(InventoryListPrice);
        }

        public bool NotAnalyzed(int? price)
        {
            bool notAnalyzed = NumComparableListings < 5;

            if (VehicleEntityTypeID == 1)
            {
                notAnalyzed |= price < MinComparableMarketPrice;
                notAnalyzed |= price > MaxComparableMarketPrice;
            }

            return notAnalyzed;
        }

        public int MaxPrice()
        {
            return (int) ((Convert.ToSingle(AvgMarketPrice) * Convert.ToSingle(MaxPctMarketAvg)) / 100.0f);
        }

        public int ProfitAtMaxPrice()
        {
            return (int) (MaxPrice() - Convert.ToSingle(InventoryUnitCost));
        }

        public bool HasInsufficientProfit()
        {
            return (ProfitAtMaxPrice() <= MinGrossProfit);
        }

        private int CalculatePctAvgMarketPrice(int price, int currentPctAvgMarketPrice)
        {
            double onePct = AvgMarketPrice.Value / 100.0d;
            double min = currentPctAvgMarketPrice * onePct;
            double max = (currentPctAvgMarketPrice + 1) * onePct;

			if (price >= Math.Floor(min) && price < Math.Ceiling(max))
				return currentPctAvgMarketPrice;
            return (int)Math.Floor(price / onePct);
        }

        public int? CalculatePctAvgMarketPrice(int? price, int? currentPctAvgMarketPrice)
        {
            if (price.HasValue && AvgMarketPrice.HasValue)
                return CalculatePctAvgMarketPrice(price.Value, currentPctAvgMarketPrice.GetValueOrDefault());
            return null;
        }

        private int CalculatePrice(int pctAvgMarketPrice, int currentPrice)
        {
            double onePct = AvgMarketPrice.Value / 100.0d;
            double min = pctAvgMarketPrice * onePct;
            double max = (pctAvgMarketPrice + 1) * onePct;

            if (currentPrice >= Math.Floor(min) && currentPrice < Math.Ceiling(max))
                return currentPrice;
            return (int) Math.Floor(pctAvgMarketPrice*onePct);
        }

        public int? CalculatePrice(int? pctAvgMarketPrice, int? currentPrice)
        {
			if (pctAvgMarketPrice.HasValue && AvgMarketPrice.HasValue)
                return CalculatePrice(pctAvgMarketPrice.Value, currentPrice.GetValueOrDefault());
            return null;
        }

        private int? GetPctAvgMarketPrice(int? inventoryListPrice)
        {
            if (!inventoryListPrice.HasValue || !AvgMarketPrice.HasValue)
                return null;
            double listPrice = inventoryListPrice.Value;
            double marketPrice = AvgMarketPrice.Value;
            return (int) Math.Round(listPrice/marketPrice*100.0);
        }

		public PricingRisk GetPricingRisk(int? inventoryListPrice)
		{
            if (NotAnalyzed(inventoryListPrice))
                return PricingRisk.NotAnalyzed;
            if (!inventoryListPrice.HasValue || inventoryListPrice.Value == 0)
                return PricingRisk.NoPrice;
            if (GetPctAvgMarketPrice(inventoryListPrice) < MinPctMarketAvg)
                return PricingRisk.Underpriced;
            if (GetPctAvgMarketPrice(inventoryListPrice) > MaxPctMarketAvg)
                return PricingRisk.Overpriced;
		    return PricingRisk.PricedAtMarket;
		}

        public string QueryString(int? newPctAvgMarketPrice, int? newPrice)
        {
            StringBuilder sb = new StringBuilder();

            AppendInt(sb, "VehicleEntityTypeID", VehicleEntityTypeID);
            AppendInt(sb, "VehicleEntityID", VehicleEntityID);
            AppendInt(sb, "VehicleMileage", VehicleMileage);
            AppendInt(sb, "MarketDaySupply", MarketDaySupply);
            AppendInt(sb, "InventoryListPrice", InventoryListPrice);
            AppendInt(sb, "MinMarketPrice", MinMarketPrice);
            AppendInt(sb, "AvgMarketPrice", AvgMarketPrice);
            AppendInt(sb, "MaxMarketPrice", MaxMarketPrice);
            AppendInt(sb, "MinVehicleMileage", MinVehicleMileage);
            AppendInt(sb, "AvgVehicleMileage", AvgVehicleMileage);
            AppendInt(sb, "MaxVehicleMileage", MaxVehicleMileage);
            AppendInt(sb, "MinPctMarketAvg", MinPctMarketAvg);
            AppendInt(sb, "MaxPctMarketAvg", MaxPctMarketAvg);
            AppendInt(sb, "MinPctMarketValue", MinPctMarketValue);
            AppendInt(sb, "MaxPctMarketValue", MaxPctMarketValue);
            AppendInt(sb, "PctAvgMarketPrice", PctAvgMarketPrice);
            AppendInt(sb, "MinComparableMarketPrice", MinComparableMarketPrice);
            AppendInt(sb, "MaxComparableMarketPrice", MaxComparableMarketPrice);
            AppendInt(sb, "NumListings", NumListings);
            AppendInt(sb, "NumComparableListings", NumComparableListings);
            AppendInt(sb, "SearchRadius", SearchRadius);
            AppendInt(sb, "NewPctAvgMarketPrice", newPctAvgMarketPrice);
            AppendInt(sb, "NewPrice", newPrice);

            return sb.ToString();
        }

        public NameValueCollection ToNameValueCollection(int? newPctAvgMarketPrice, int? newPrice)
        {
            NameValueCollection nameValueCollection = new NameValueCollection();

            nameValueCollection.Add("VehicleEntityTypeID", VehicleEntityTypeID.ToString());
            nameValueCollection.Add("VehicleEntityID", VehicleEntityID.ToString());
            nameValueCollection.Add("VehicleMileage", VehicleMileage.ToString());
            nameValueCollection.Add("MarketDaySupply", MarketDaySupply.ToString());
            nameValueCollection.Add("InventoryListPrice", InventoryListPrice.ToString());
            nameValueCollection.Add("MinMarketPrice", MinMarketPrice.ToString());
            nameValueCollection.Add("AvgMarketPrice", AvgMarketPrice.ToString());
            nameValueCollection.Add("MaxMarketPrice", MaxMarketPrice.ToString());
            nameValueCollection.Add("MinVehicleMileage", MinVehicleMileage.ToString());
            nameValueCollection.Add("AvgVehicleMileage", AvgVehicleMileage.ToString());
            nameValueCollection.Add("MaxVehicleMileage", MaxVehicleMileage.ToString());
            nameValueCollection.Add("MinPctMarketAvg", MinPctMarketAvg.ToString());
            nameValueCollection.Add("MaxPctMarketAvg", MaxPctMarketAvg.ToString());
            nameValueCollection.Add("MinPctMarketValue", MinPctMarketValue.ToString());
            nameValueCollection.Add("MaxPctMarketValue", MaxPctMarketValue.ToString());
            nameValueCollection.Add("PctAvgMarketPrice", PctAvgMarketPrice.ToString());
            nameValueCollection.Add("MinComparableMarketPrice", MinComparableMarketPrice.ToString());
            nameValueCollection.Add("MaxComparableMarketPrice", MaxComparableMarketPrice.ToString());
            nameValueCollection.Add("NumListings", NumListings.ToString());
            nameValueCollection.Add("NumComparableListings", NumComparableListings.ToString());
            nameValueCollection.Add("SearchRadius", SearchRadius.ToString());
            nameValueCollection.Add("NewPctAvgMarketPrice", newPctAvgMarketPrice.ToString());
            nameValueCollection.Add("NewPrice", newPrice.ToString());

            return nameValueCollection;
        }

        private static void AppendInt(StringBuilder sb, string key, int? value)
        {
            if (value.HasValue)
            {
                if (sb.Length > 0)
                    sb.Append("&");
                sb.Append(key).Append("=").Append(value.Value);
            }
        }

        #endregion

        #region Data Binding
        
        protected override void PerformDataBinding(IEnumerable data)
        {
            if (data == null)
                return;

            IEnumerator en = data.GetEnumerator();
            if (en.MoveNext())
            {
                object item = en.Current;

                VehicleEntityTypeID = (byte) DataBinder.Eval(item, "VehicleEntityTypeID");
                VehicleEntityID = (int)DataBinder.Eval(item, "VehicleEntityID");
                VehicleMileage = GetInt(item, "VehicleMileage");
                MarketDaySupply = GetInt(item, "MarketDaySupply");
                MinMarketPrice = GetInt(item, "MinMarketPrice");
                AvgMarketPrice = GetInt(item, "AvgMarketPrice");
                MaxMarketPrice = GetInt(item, "MaxMarketPrice");
                MinVehicleMileage = GetInt(item, "MinVehicleMileage");
                AvgVehicleMileage = GetInt(item, "AvgVehicleMileage");
                MaxVehicleMileage = GetInt(item, "MaxVehicleMileage");
                InventoryUnitCost = GetInt(item, "InventoryUnitCost");
                InventoryListPrice = GetInt(item, "InventoryListPrice");
                MinPctMarketAvg = GetInt(item, "MinPctMarketAvg");
                MaxPctMarketAvg = GetInt(item, "MaxPctMarketAvg");
                MinGrossProfit = GetInt(item, "MinGrossProfit");
                MinPctMarketValue = GetInt(item, "MinPctMarketValue");
                MaxPctMarketValue = GetInt(item, "MaxPctMarketValue");
                PctAvgMarketPrice = GetInt(item, "PctAvgMarketPrice");
                MinComparableMarketPrice = GetInt(item, "MinComparableMarketPrice");
                MaxComparableMarketPrice = GetInt(item, "MaxComparableMarketPrice");
                NumListings = (int)DataBinder.Eval(item, "NumListings");
                NumComparableListings = (int)DataBinder.Eval(item, "NumComparableListings");
                AppraisalValue = GetInt(item, "AppraisalValue");
                EstimatedReconditioningCost = GetInt(item, "EstimatedReconditioningCost");
                TargetGrossProfit = GetInt(item, "TargetGrossProfit");
                SearchRadius = GetInt(item, "SearchRadius");
                JDPowerAverageSalePrice = GetInt(item, "JDPowerAverageSalePrice");
                NaaaAverageSalePrice = GetInt(item, "NaaaAverageSalePrice");

                object value = DataBinder.Eval(item, "Vin");
                if (value != null && !DBNull.Value.Equals(value))
                    Vin = (string) value;
            }
        }

        private static int? GetInt(object item, string property)
        {
            object o = DataBinder.Eval(item, property);
            if (o == null || o.Equals(DBNull.Value))
                return null;
            return Convert.ToInt32(o);
        }

        #endregion
    }
}