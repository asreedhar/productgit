using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: WebResource("FirstLook.Pricing.WebControls.Toolbar.js", "text/javascript")]
[assembly: WebResource("FirstLook.Pricing.WebControls.Toolbar.debug.js", "text/javascript")]

namespace FirstLook.Pricing.WebControls
{
	[ParseChildren(true, "Items"), PersistChildren(false)]
    public class ToolBar : DataBoundControl, INamingContainer, IScriptControl, IPostBackDataHandler
	{
		#region Events

		private static readonly object EventCommand = new object();

		public event EventHandler<CommandEventArgs> Command
		{
			add
			{
				Events.AddHandler(EventCommand, value);
			}
			remove
			{
				Events.RemoveHandler(EventCommand, value);
			}
		}

		protected virtual void OnCommand(CommandEventArgs e)
		{
			EventHandler<CommandEventArgs> handler = (EventHandler<CommandEventArgs>)Events[EventCommand];

			if (handler != null)
			{
				handler(this, e);
			}
		}

		#endregion

		#region Fields

		private ScriptManager _manager;

		#endregion

		#region Properties

	    private ToolBarItemCollection _items;
		private ArrayList _itemClickQueue;
	    private bool _autoGenerateAds = true;
	    private bool _hasRoundedCorners = false;

		[DefaultValue(true), Localizable(false), Bindable(false), Category("Behavior"), Description("Whether the items are static (false) or dynamic (true)")]
		public bool AutoGenerateItems
		{
			get { return _autoGenerateAds; }
			set { _autoGenerateAds = value; }
		}

		[Category("Default"), Description("Toolbar Items"),
         MergableProperty(false), PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public ToolBarItemCollection Items
		{
			get
			{
                if (_items == null)
                {
                    _items = new ToolBarItemCollection();
                    if (IsTrackingViewState)
                    {
                        ((IStateManager)_items).TrackViewState();
                    }
                }
			    return _items;
			}
		}
        
	    [Description("Add Rounded Corner HTML Hooks"),
         DefaultValue(false),
         Category("Appearance")]
        public bool HasRoundedCorners
	    {
            get { return _hasRoundedCorners; }
            set { _hasRoundedCorners = value; }
	    }


		[Browsable(false)]
		protected ArrayList ItemClickQueue
		{
			get
			{
				if (_itemClickQueue == null)
				{
					_itemClickQueue = _items == null ? new ArrayList() : new ArrayList(_items.Count);
				}
				return _itemClickQueue;
			}
		}

        protected override string TagName
		{
			get
			{
				return "div";
			}
		}

		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Div;
			}
		}

	    #endregion

		#region Data Binding

		protected override void PerformDataBinding(IEnumerable data)
		{
			if (data != null)
			{
				IHierarchicalEnumerable tree = data as IHierarchicalEnumerable;

				if (tree != null)
				{
					PerformDataBinding(tree, Items, AutoGenerateItems);
				}
				else
				{
					PerformDataBinding(data, Items, AutoGenerateItems);
				}
			}
		}

		private static void PerformDataBinding(IEnumerable data, ToolBarItemCollection collection, bool autoGenerateItems)
		{
			IEnumerator en = data.GetEnumerator();

			if (autoGenerateItems)
			{
				collection.Clear();

				while (en.MoveNext())
				{
					ToolBarButton button = new ToolBarButton();

					button.Enabled = bool.Parse(DataBinder.Eval(en.Current, "Enabled", null));
					button.Name = DataBinder.Eval(en.Current, "Name", null);
					button.Text = DataBinder.Eval(en.Current, "Text", null);
					button.BodyText = DataBinder.Eval(en.Current, "BodyText", null);
					button.FooterText = DataBinder.Eval(en.Current, "FooterText", null);

					collection.Add(button);
				}
			}
			else
			{
				while (en.MoveNext())
				{
					foreach (ToolBarItem item in collection)
					{
						string name = DataBinder.Eval(en.Current, "Name", null);

						if (string.Equals(item.Name, name))
						{
							ToolBarButton button = item as ToolBarButton;

							if (button != null)
							{
								button.BodyText = DataBinder.Eval(en.Current, "BodyText", null);

								button.FooterText = DataBinder.Eval(en.Current, "FooterText", null);

								break;
							}
						}
					}
				}
			}
		}

		private static void PerformDataBinding(IHierarchicalEnumerable tree, ToolBarItemCollection collection, bool autoGenerateItems)
		{
			IEnumerator en = tree.GetEnumerator();

			if (autoGenerateItems)
			{
				collection.Clear();

				while (en.MoveNext())
				{
					IHierarchyData data = tree.GetHierarchyData(en.Current);

					if (data.HasChildren)
					{
						ToolBarDropDown drop = new ToolBarDropDown();

						drop.Enabled = bool.Parse(DataBinder.Eval(data, "Enabled", null));
						drop.Name = DataBinder.Eval(data, "Name", null);
						drop.Text = DataBinder.Eval(data, "Text", null);

						collection.Add(drop);

						PerformDataBinding(data.GetChildren(), drop.DropDownItems, true);
					}
					else
					{
						ToolBarButton button = new ToolBarButton();

						button.Enabled = bool.Parse(DataBinder.Eval(data, "Enabled", null));
						button.Name = DataBinder.Eval(data, "Name", null);
						button.Text = DataBinder.Eval(data, "Text", null);
						button.BodyText = DataBinder.Eval(data, "BodyText", null);
						button.FooterText = DataBinder.Eval(data, "FooterText", null);

						collection.Add(button);
					}
				}
			}
			else
			{
				while (en.MoveNext())
				{
					IHierarchyData data = tree.GetHierarchyData(en.Current);

					string name = DataBinder.Eval(data, "Name", null);

					foreach (ToolBarItem item in collection)
					{
						if (string.Equals(name, item.Name, StringComparison.Ordinal))
						{
							item.Enabled = bool.Parse(DataBinder.Eval(data, "Enabled", null));

							ToolBarButton button = item as ToolBarButton;

							if (button != null)
							{
								button.BodyText = DataBinder.Eval(data, "BodyText", null);
								button.FooterText = DataBinder.Eval(data, "FooterText", null);
							}
							else
							{
								ToolBarDropDown drop = item as ToolBarDropDown;

								if (drop != null)
								{
									PerformDataBinding(data.GetChildren(), drop.DropDownItems, drop.AutoGenerateItems);
								}
							}
						}
					}
				}
			}
		}

		#endregion

		#region Methods

		#region WebControls Override

		protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            if (string.IsNullOrEmpty(CssClass))
            {
                CssClass = SR.ToolbarWrapperCssClass;
            }
            else
            {
                CssClass += " " + SR.ToolbarWrapperCssClass;
            }

            base.AddAttributesToRender(writer);
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ToolbarItemsWrapperListCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            RenderToolBarItems(writer, Items);

            writer.RenderEndTag(); // div

            RenderClickStreamInput(writer);
        }

        #endregion

        protected void RenderToolBarItems(HtmlTextWriter writer, ToolBarItemCollection items)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ToolbarItemsListCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Ul); 

            foreach (ToolBarItem item in items)
            {
                if (!item.Visible)
                {
                    continue;
                }

                string cssClass = item.CssClass;
                if (!item.Enabled)
                {
                    cssClass += " disabled";
                }
                writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClass);

                if (!item.ControlStyle.IsEmpty)
                {
                    item.ControlStyle.AddAttributesToRender(writer);
                }

                writer.RenderBeginTag(HtmlTextWriterTag.Li);

                ToolBarButton toolBarButton = item as ToolBarButton;

                ToolBarDropDown dropDown = item as ToolBarDropDown;

                if (toolBarButton != null)
                {
                    RenderToolbarButton(writer, toolBarButton);

                    if (!string.IsNullOrEmpty(toolBarButton.BodyText))
                    {
                        RenderFullBodyText(writer, toolBarButton); 
                    }

                    if (!string.IsNullOrEmpty(toolBarButton.FooterText))
                    {
                        RenderFullFooterText(writer, toolBarButton); 
                    }
                }
                else if (dropDown != null)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ToolbarItemCssClass);

                    RenderToolbarItem(writer, dropDown);

                    if (dropDown.DropDownItems.Count > 0)
                    {
                        if (HasRoundedCorners)
                        {
                            RenderRoundedCornerTopWrapper(writer);
                        }

                        RenderToolBarItems(writer, dropDown.DropDownItems);

                        if (HasRoundedCorners)
                        {
                            RenderRoundedCornerBottomWrapper(writer);
                        }
                    }
                }

                writer.RenderEndTag(); // li
            }

            writer.RenderEndTag();
        }

	    private static void RenderFullBodyText(HtmlTextWriter writer, ToolBarButton button)
	    {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.FullBodyTextCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.P);

            writer.Write(button.BodyText);

            writer.RenderEndTag();
	    }

        private static void RenderFullFooterText(HtmlTextWriter writer, ToolBarButton button)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.FullFooterTextCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.P);

            writer.Write(button.FooterText);

            writer.RenderEndTag();
        }

	    private static void RenderRoundedCornerTopWrapper(HtmlTextWriter writer)
	    {
	        writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.OuterBoxCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Span);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.InnerBoxOneCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Span);

	        writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.InnerBoxTwoCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Span);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.InnerBoxThreeCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Span);
	    }

        private static void RenderRoundedCornerBottomWrapper(HtmlTextWriter writer)
        {
            writer.RenderEndTag(); // span.OuterBoxCssClass
            writer.RenderEndTag(); // span.InnerBoxOneCssClass
            writer.RenderEndTag(); // span.InnerBoxTwoCssClass
            writer.RenderEndTag(); // span.InnerBoxThreeCssClass
        }

	    protected void RenderToolbarButton(HtmlTextWriter writer, ToolBarButton item)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ToolbarButtonCssClass + " " + item.CssClass);

            if (!string.IsNullOrEmpty(item.BodyText))
            {
                int maxLength = 150;
                string title = item.BodyText;
                if (title.Length > maxLength)
                {
                    title = title.Substring(0, maxLength - 3) + "...";
                }

                writer.AddAttribute(HtmlTextWriterAttribute.Title, title);
            }

            RenderToolbarItem(writer, item);
        }

        protected void RenderToolbarItem(HtmlTextWriter writer, ToolBarItem item)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "submit");

            writer.AddAttribute(HtmlTextWriterAttribute.Name, CreateId(item.Name));

            writer.AddAttribute(HtmlTextWriterAttribute.Name, CreateId(item.Name));

            writer.AddAttribute(HtmlTextWriterAttribute.Value, item.Text);

            if (!item.Enabled)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Disabled, "disabled");
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ToolbarWrapperCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Input);

            writer.RenderEndTag();
        }

        protected void RenderClickStreamInput(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Id, CreateId(SR.ClickStreamId));

            writer.AddAttribute(HtmlTextWriterAttribute.Name, CreateId(SR.ClickStreamId));

            writer.AddAttribute(HtmlTextWriterAttribute.Type, "hidden");

            writer.RenderBeginTag(HtmlTextWriterTag.Input);

            writer.RenderEndTag();
        }

        protected string CreateId(string id)
        {
            return UniqueID + IdSeparator + id;
        }

        #endregion

		#region Life Cycle

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			if (!DesignMode)
			{
				_manager = ScriptManager.GetCurrent(Page);

				if (_manager != null)
				{
					_manager.RegisterScriptControl(this);
				}
				else
				{
					throw new InvalidOperationException("You must have a ScriptManager!");
				}
			}
		}

        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);

            if (_manager != null)
            {
                _manager.RegisterScriptDescriptors(this);
            }
        }

		#endregion

		#region IScriptControl Members

		public IEnumerable<ScriptDescriptor> GetScriptDescriptors()
		{
            ScriptBehaviorDescriptor scriptBehaviorDescriptor = new ScriptBehaviorDescriptor("FirstLook.Pricing.WebControls.Toolbar", ClientID);
		    scriptBehaviorDescriptor.ID = ClientID;

            scriptBehaviorDescriptor.AddElementProperty("click_stream_input", CreateId(SR.ClickStreamId));
            scriptBehaviorDescriptor.AddProperty("full_body_text_class", SR.FullBodyTextCssClass);
            scriptBehaviorDescriptor.AddProperty("full_footer_text_class", SR.FullFooterTextCssClass);

		    return new ScriptDescriptor[] { scriptBehaviorDescriptor };
		}

		public IEnumerable<ScriptReference> GetScriptReferences()
		{
           return new ScriptReference[]
                {
                    new ScriptReference("FirstLook.Pricing.WebControls.Toolbar.js", "FirstLook.Pricing.WebControls")
                };		    
		}

		#endregion

	    #region IPostBackDataHandler Members

        protected override Control FindControl(string id, int pathOffset)
        {
            return this;
        }

		private void HandleClickStream(string stream)
		{
			if (!string.IsNullOrEmpty(stream))
			{
				string[] names = stream.Split(new char[] {','});

				foreach (string name in names)
				{
					foreach (Pair pair in ItemClickQueue)
					{
						if (name.Equals(pair.First))
						{
							pair.Second = ((int) pair.Second) + 1;

							goto RaiseEvent;
						}
					}
					
					ItemClickQueue.Add(new Pair(name, 1));

				RaiseEvent:
					
					HandleEvent(name);
				}
			}
		}

		protected void HandleEvent(string commandName)
        {
			ToolBarButton button = Items.FindItem(commandName) as ToolBarButton;

            if (button != null)
            {
            	OnCommand(new CommandEventArgs(
            	          	button.CommandName ?? SR.DefaultButtonCommandName,
            	          	button.Name));
            }
        }

        private string _commandName;
	    private string _clickStream;

	    public bool LoadPostData(string postDataKey, NameValueCollection postCollection)
	    {
	        if (IsEnabled)
            {
				int idOffset = UniqueID.Length + 1;

				if (postDataKey.Length > idOffset)
				{
					string controlKey = postDataKey.Substring(idOffset);

					if (!string.IsNullOrEmpty(controlKey))
					{
						if (controlKey.Equals(SR.ClickStreamId))
						{
							_clickStream = postCollection[controlKey];

							return true;
						}
						else // FIXME: should be a Items.Contains type search
						{
							_commandName = controlKey;

							return true;
						}
					}
				}
            }

	        return false;
	    }

	    public void RaisePostDataChangedEvent()
	    {
	    	HandleClickStream(_clickStream);

	        HandleEvent(_commandName);
	    }

		bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
            return LoadPostData(postDataKey, postCollection);
        }

        void IPostBackDataHandler.RaisePostDataChangedEvent()
        {
            RaisePostDataChangedEvent();
        }

	    #endregion

        #region IStateManager Members

	    protected override void LoadViewState(object state)
        {
			Pair pair = (Pair) state;

			base.LoadViewState(pair.First);

			if (pair.Second != null)
			{
				object[] values = (object[]) pair.Second;
				
				_autoGenerateAds = (bool)values[0];
				_hasRoundedCorners = (bool)values[1];
			    _itemClickQueue = (ArrayList) values[2];

                ((IStateManager)Items).LoadViewState(values[3]);
			}		
        }

        protected override object SaveViewState()
        {
            object[] values = new object[4];

            values[0] = _autoGenerateAds;
            values[1] = _hasRoundedCorners;
            values[2] = _itemClickQueue;

            values[3] = ((IStateManager)Items).SaveViewState();

			return new Pair(base.SaveViewState(), values);
        }

        protected override void TrackViewState()
        {
            base.TrackViewState();

            if (_items != null)
            {
                ((IStateManager)_items).TrackViewState();
            }
        }

        #endregion

        #region innerClasses

        private static class SR
        {
            public static string ToolbarWrapperCssClass
            {
                get { return toolbarWrapperCssClass; }
            }

            public static string ToolbarItemsListCssClass
            {
                get { return toolbarItemsListCssClass; }
            }

            public static string ToolbarItemsWrapperListCssClass
            {
                get { return toolbarItemsWrapperListCssClass; }
            }

            public static string ToolbarItemCssClass
            {
                get { return toolbarItemCssClass; }
            }

            public static string ToolbarButtonCssClass
            {
                get { return toolbarButtonCssClass; }
            }

            public static string OuterBoxCssClass
            {
                get { return outerBoxCssClass; }
            }

            public static string InnerBoxOneCssClass
            {
                get { return innerBoxOneCssClass; }
            }

            public static string InnerBoxTwoCssClass
            {
                get { return innerBoxTwoCssClass; }
            }

            public static string InnerBoxThreeCssClass
            {
                get { return innerBoxThreeCssClass; }
            }

            public static string ClickStreamId
            {
                get { return toolbarClickStreamId; }
            }

            public static string DefaultButtonCommandName
            {
                get { return defaultButtonCommandName; }
            }

            public static string FullBodyTextCssClass
            {
                get { return fullBodyTextCssClass; }
            }

            public static string FullFooterTextCssClass
            {
                get { return fullFooterTextCssClass; }
            }

            private const string toolbarWrapperCssClass = "ui-toolbar";
            private const string toolbarItemsWrapperListCssClass = "ui-toolbar-items-wrapper";
            private const string toolbarItemsListCssClass = "ui-toolbar-items";
            private const string toolbarItemCssClass = "ui-toolbar-item";
            private const string toolbarButtonCssClass = "ui-toolbar-button";

            private const string toolbarClickStreamId = "clickStream";

            private const string outerBoxCssClass = "ui-toolbar-box-outer";
            private const string innerBoxOneCssClass = "ui-toolbar-box-inner-1";
            private const string innerBoxTwoCssClass = "ui-toolbar-box-inner-2";
            private const string innerBoxThreeCssClass = "ui-toolbar-box-inner-3";

            private const string defaultButtonCommandName = "Click";

            private const string fullBodyTextCssClass = "ui-full-body-text";
            private const string fullFooterTextCssClass = "ui-full-footer-text";
        }

        #endregion
	}
}
