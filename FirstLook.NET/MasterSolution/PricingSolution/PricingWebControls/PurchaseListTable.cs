using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Pricing.DomainModel.Market;

[assembly: WebResource("FirstLook.Pricing.WebControls.PurchaseListTable.js", "text/javascript")]
[assembly: WebResource("FirstLook.Pricing.WebControls.PurchaseListTable.debug.js", "text/javascript")]

namespace FirstLook.Pricing.WebControls
{
    public class PurchaseListTable : DataBoundReportTable, IPostBackDataHandler, IScriptControl
    {
        #region Properties

        public bool OpenFlashLocateAsPopup { get; set; }

        private bool _confirmDirtyUnload;
        public bool ConfirmDirtyUnload
        {
            get { return _confirmDirtyUnload; }
            set
            {
                if (value != _confirmDirtyUnload)
                {
                    _confirmDirtyUnload = value;
                }
            }
        }

        private bool ListInfoIsDirty
        {
            get
            {
                return (DeletedRows.Count > 0 || QuantityDelta.Count > 0 || NotesDelta.Count > 0);
            }
        }

        private ScriptManager _manager;

        private MarketReportItemCollection _purchaseList;

        private MarketReportItemCollection PurchaseList
        {
            get
            {
                if (_purchaseList == null)
                {
                    _purchaseList = new MarketReportItemCollection();
                    if (IsTrackingViewState)
                    {
                        ((IStateManager)_purchaseList).TrackViewState();
                    }
                }
                return _purchaseList;
            }
        }      

        private Collection<int> _deletedRows;

        private Collection<int> DeletedRows
        {
            get
            {
                if (_deletedRows == null)
                {
                    _deletedRows = new Collection<int>();
                }

                return _deletedRows;
            }
        }

        private Dictionary<int, int> _quantityDelta;
        private Dictionary<int, int> QuantityDelta
        {
            get
            {
                if (_quantityDelta == null)
                {
                    _quantityDelta = new Dictionary<int, int>();
                }

                return _quantityDelta;
            }
        }

        private Dictionary<int, string> _notesDelta;
        private Dictionary<int, string> NotesDelta
        {
            get
            {
                if (_notesDelta == null)
                {
                    _notesDelta = new Dictionary<int, string>();
                }

                return _notesDelta;
            }
        }
        
        private bool _managedDelta;

        private static readonly Column[] Columns;

        static PurchaseListTable()
        {
            Columns = new[]
                      {
                          new Column(ThisSR.TableColumnSelectCssClass,
                                     ThisSR.TableColumnSelectTitle,
                                     string.Empty),

                          new Column(SR.TableColumnMarketDaysSupplyCssClass,
                                     SR.TableColumnMarketDaysSupplyTitle,
                                     "MarketDaysSupply"),

                          new Column(SR.TableColumnVehicleInformationCssClass,
                                     SR.TableColumnVehicleInformationTitle,
                                     "ModelYear"), // , Make, ModelFamily"

                          new Column(SR.TableColumnUnitsSoldCssClass,
                                     SR.TableColumnUnitsSoldTitle,
                                     "UnitsSold"),

                          new Column(SR.TableColumnNumberOfListingsCssClass,
                                     SR.TableColumnNumberOfListingsTitle,
                                     "NumberOfListings"),

                          new Column(SR.TableColumnAverageMarketPriceCssClass,
                                     SR.TableColumnAverageMarketPriceTitle,
                                     "AverageInternetPrice"),

                          new Column(SR.TableColumnAverageMileageCssClass,
                                     SR.TableColumnAverageMileageTitle,
                                     "AverageInternetMileage"),

                          new Column(SR.TableColumnUnitsInStockCssClass,
                                     SR.TableColumnUnitsInStockTitle,
                                     "UnitsInStock"),

                          new Column(ThisSR.TableColumnQuantityCssClass,
                                     ThisSR.TableColumnQuantityTitle,
                                     "Quantity"),

                          new Column(ThisSR.TableColumnFlashLocateCssClass,
                                     ThisSR.TableColumnFlashLocateTitle,
                                     string.Empty)
                      };

        }

        private bool _showUnitsSold = true;

        public bool ShowUnitsSold
        {
            get { return _showUnitsSold; }
            set { _showUnitsSold = value; }
        }

        private bool _showFlashLocate = true;

        public bool ShowFlashLocate
        {
            get { return _showFlashLocate; }
            set { _showFlashLocate = value; }
        }

        #endregion

        #region Events

        private static readonly object EventRowQuantityChanged = new object();
        private static readonly object EventRowNotesChanged = new object();

        public event EventHandler<MarketReportTableRowQuantityChangedEventArgs> RowQuantityChanged
        {
            add { Events.AddHandler(EventRowQuantityChanged, value); }
            remove { Events.RemoveHandler(EventRowQuantityChanged, value); }
        }

        public event EventHandler<MarketReportTableRowQuantityChangedEventArgs> RowNotesChanged
        {
            add { Events.AddHandler(EventRowNotesChanged, value); }
            remove { Events.RemoveHandler(EventRowNotesChanged, value); }
        }

        protected virtual void OnRowQuantityChanged(MarketReportTableRowQuantityChangedEventArgs e)
        {
            EventHandler<MarketReportTableRowQuantityChangedEventArgs> handler =
                (EventHandler<MarketReportTableRowQuantityChangedEventArgs>)Events[EventRowQuantityChanged];
            if (handler != null)
            {
                handler(this, e);
            }
        }

        protected virtual void OnRowNotesChanged(MarketReportTableRowQuantityChangedEventArgs e)
        {
            EventHandler<MarketReportTableRowQuantityChangedEventArgs> handler =
                (EventHandler<MarketReportTableRowQuantityChangedEventArgs>)Events[EventRowQuantityChanged];
            if (handler != null)
            {
                handler(this, e);
            }
        }

        #endregion

        #region Overrides

        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Div;
            }
        }

        public override Control FindControl(string id)
        {
            return this;
        }

        protected override Control FindControl(string id, int pathOffset)
        {
            return this;
        }

        protected override string TagName
        {
            get
            {
                return "div";
            }
        }

        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            if (string.IsNullOrEmpty(CssClass))
            {
                CssClass = ThisSR.CssClass;
            }
            else
            {
                CssClass += " " + ThisSR.CssClass;
            }

            base.AddAttributesToRender(writer);
        }

        protected override void OnInit(EventArgs e)
        {
            Page.RegisterRequiresControlState(this);

            base.OnInit(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (!DesignMode)
            {
                _manager = ScriptManager.GetCurrent(Page);

                if (_manager != null)
                {
                    _manager.RegisterScriptControl(this);
                }
                else
                {
                    throw new InvalidOperationException("You must have a ScriptManager!");
                }
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);

            if (!DesignMode)
            {
                _manager.RegisterScriptDescriptors(this);
            }
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            if (PurchaseList != null)
            {
                RenderMarketReportTable(writer);
            }
        }


        #endregion

        #region Methods

        private void HandleEvents()
        {
            if (!_managedDelta)
            {                                
                for (int i = 0, l = PurchaseList.Count; i < l; i++)
                {
                    if (QuantityDelta.ContainsKey(PurchaseList[i].Id))
                    {
                        PurchaseList[i].Quantity = QuantityDelta[PurchaseList[i].Id];
                    }

                    if (NotesDelta.ContainsKey(PurchaseList[i].Id))
                    {
                        PurchaseList[i].Notes = NotesDelta[PurchaseList[i].Id];
                    }                    
                }                

                if (_handleDeletes)
                {
                    for (int i = 0; i < DeletedRows.Count; i++)
                    {
                        for (int j = 0; j < PurchaseList.Count; j++)
                        {
                            if (DeletedRows[i] == PurchaseList[j].Id)
                            {
                                PurchaseList.Remove(PurchaseList[j]);
                                break;
                            }
                        }
                    }
                }                                

                _managedDelta = true;
            }
        }

        public void Update()
        {
            IOrderedDictionary keys = new OrderedDictionary();
            IOrderedDictionary values = new OrderedDictionary();

            values["deleted"] = DeletedRows;
            values["notes"] = NotesDelta;
            values["quantities"] = QuantityDelta;

            DataSourceObject.GetView(DataMember ?? string.Empty).Update(keys, values, null, HandleUpdateCallback);
        }

        public void ClearAllItems()
        {
            foreach (IReportItem item in PurchaseList)
            {
                if (!DeletedRows.Contains(item.Id))
                {
                    DeletedRows.Add(item.Id);
                }
            }

            PurchaseList.Clear();
        }

        private bool HandleUpdateCallback(int affectedrecords, Exception ex)
        {
            if (ex != null)
            {
                // allow client to handle error
            }
            else
            {
                RequiresDataBinding = true;
            }

            return false; // Return true if client handled error
        }

        private void RenderMarketReportTable(HtmlTextWriter writer)
        {
            RenderTableBeginTag(writer);

            RenderTableHeader(writer);

            RenderTableBody(writer);

            RenderTableEndTag(writer);
        }

        private static void RenderTableBeginTag(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, ThisSR.TableCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Table);
        }

        private static void RenderTableEndTag(HtmlTextWriter writer)
        {
            writer.RenderEndTag(); // Table
        }

        private void RenderTableHeader(HtmlTextWriter writer)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.Thead);

            RenderTableHeaderHeadingsRow(writer);

            writer.RenderEndTag(); // Thead
        }

        private void RenderTableHeaderHeadingsRow(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.TableRowHeadingsCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Tr);


            for (int j = 0, k = Columns.Length; j < k; j++)
            {
                Column column = Columns[j];

                if (!ShowUnitsSold && column.Title == SR.TableColumnUnitsSoldTitle)
                    continue;

                writer.AddAttribute(HtmlTextWriterAttribute.Class, column.Css);

                writer.RenderBeginTag(HtmlTextWriterTag.Th);

                writer.Write(column.Title);

                if (AllowSorting && !string.IsNullOrEmpty(column.Sort))
                {
                    RenderSortButton(writer, column.Sort);
                }

                if (AllowSorting && column.Title.Equals(SR.TableColumnVehicleInformationTitle))
                {
                    RenderVehicleInformationExpandedSort(writer);
                }                

                writer.RenderEndTag(); // Th

                writer.WriteLine();
            }          

            writer.RenderEndTag(); // Tr
        }        

        private void RenderVehicleInformationExpandedSort(HtmlTextWriter writer)
        {
            writer.WriteBreak();

            writer.Write("(");

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "sort_list");

            writer.RenderBeginTag(HtmlTextWriterTag.Ul);

            writer.RenderBeginTag(HtmlTextWriterTag.Li);

            writer.Write("Year");

            RenderSortButton(writer, "ModelYear");

            writer.Write(",");

            writer.RenderEndTag(); // Li

            writer.RenderBeginTag(HtmlTextWriterTag.Li);

            writer.Write("Make");

            RenderSortButton(writer, "Make");

            writer.Write(",");

            writer.RenderEndTag(); // Li

            writer.RenderBeginTag(HtmlTextWriterTag.Li);

            writer.Write("Model");

            RenderSortButton(writer, "ModelFamily");

            writer.RenderEndTag(); // Li

            writer.RenderEndTag(); // Ul

            writer.Write(")");
        }

        private void RenderTableBody(HtmlTextWriter writer)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.Tbody);

            int i = 0;

            foreach (IReportItem purchaseListItem in PurchaseList)
            {
                RenderTableBodyPurchaseListItem(writer, purchaseListItem, i);

                RenderTableBodyPurchaseListItemNotes(writer, purchaseListItem, i++);
            }

            writer.RenderEndTag(); // Tbody
        }

        private void RenderTableBodyPurchaseListItem(HtmlTextWriter writer, IReportItem purchaseListItem, int rowIndex)
        {
            bool isOdd = rowIndex % 2 != 0;

            if (isOdd)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.TableRowOddNumberRowCssClass);
            }

            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            RenderTableBodyCellSelect(writer, purchaseListItem.Id);

            RenderTableBodyCellMarketDaysSupply(writer, purchaseListItem);

            RenderTableBodyCellVehicleInformation(writer, purchaseListItem, true);

            if (ShowUnitsSold)
            {
                RenderTableBodyCellUnitsSold(writer, purchaseListItem); 
            }

            RenderTableBodyCellNumberOfListings(writer, purchaseListItem);

            RenderTableBodyCellAverageMarketPrice(writer, purchaseListItem);

            RenderTableBodyCellAverageMileage(writer, purchaseListItem);

            RenderTableBodyCellUnitsInStock(writer, purchaseListItem);

            RenderTableBodyCellQuantity(writer, purchaseListItem);

            RenderTableBodyCellFlashLocatLink(writer, purchaseListItem);             

            writer.RenderEndTag(); // Tr
        }

        private void RenderTableBodyPurchaseListItemNotes(HtmlTextWriter writer, IReportItem purchaseListItem, int rowIndex)
        {
            bool isOdd = rowIndex % 2 != 0;

            string cssClass = ThisSR.TableRowNotesCssClass;

            if (isOdd)
            {
                cssClass += " " + SR.TableRowOddNumberRowCssClass;
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            int colspan = Columns.Length;

            if (!ShowUnitsSold)
            {
                colspan -= 1;
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Colspan, colspan.ToString());

            writer.RenderBeginTag(HtmlTextWriterTag.Td);

            string id = CreateId(new[] { ThisSR.NotesIdPrefix, purchaseListItem.Id.ToString() });

            writer.AddAttribute(HtmlTextWriterAttribute.For, id);

            writer.RenderBeginTag(HtmlTextWriterTag.Label);

            writer.Write(ThisSR.NotesLabelText);

            writer.RenderEndTag(); // Label

            writer.AddAttribute(HtmlTextWriterAttribute.Rows, ThisSR.NotesTextAreaRows);

            writer.AddAttribute(HtmlTextWriterAttribute.Cols, ThisSR.NotesTextAreaCols);

            writer.AddAttribute(HtmlTextWriterAttribute.Name, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Id, id);

            writer.RenderBeginTag(HtmlTextWriterTag.Textarea);

            writer.Write(purchaseListItem.Notes);

            writer.RenderEndTag(); // Textarea

            writer.RenderEndTag(); // Td

            writer.RenderEndTag(); // Tr
        }

        private void RenderTableBodyCellSelect(HtmlTextWriter writer, int index)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, ThisSR.TableColumnSelectCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Td);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "button");

            writer.RenderBeginTag(HtmlTextWriterTag.Span);

            writer.AddAttribute(HtmlTextWriterAttribute.Type, "submit");

            string id = CreateId(new[] { ThisSR.SelectedCheckboxIdPrefix, index.ToString() });

            writer.AddAttribute(HtmlTextWriterAttribute.Id, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Name, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Value, "&times;", false);

            writer.RenderBeginTag(HtmlTextWriterTag.Input);

            writer.RenderEndTag(); // Input

            writer.RenderEndTag(); // Span

            writer.RenderEndTag(); // Td
        }

        private void RenderTableBodyCellQuantity(HtmlTextWriter writer, IReportItem reportItem)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, ThisSR.TableColumnQuantityCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Td);

            writer.AddAttribute(HtmlTextWriterAttribute.Maxlength, MarketReportItem.MaxQuantity.ToString().Length.ToString());

            writer.AddAttribute(HtmlTextWriterAttribute.Type, "text");

            string id = CreateId(new[]
                                     {
                                         ThisSR.QuantityIdPrefix, reportItem.Id.ToString()
                                     });

            writer.AddAttribute(HtmlTextWriterAttribute.Name, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Id, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Value, string.Format("{0}", reportItem.Quantity));

            writer.RenderBeginTag(HtmlTextWriterTag.Input);

            writer.RenderEndTag(); // Input

            writer.RenderEndTag(); // Td
        }

        private void RenderTableBodyCellFlashLocatLink(HtmlTextWriter writer, IReportItem reportItem)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, ThisSR.TableColumnFlashLocateCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Td);

            if (ShowFlashLocate)
            {
                string uri =
                        string.Format(
                            "{0}?makeText={1}&modelText={2}&yearBegin={3}&yearEnd={3}&ignoreMarketSuppression={4}&flashLocateNotAjax={4}&flashLocateRunSearch={4}&selectedMarket={5}",
                            "/NextGen/FlashLocateSummary.go",
                            reportItem.Make,
                            reportItem.ModelFamily,
                            reportItem.ModelYear,
                            "true",
                            "ALL");

                writer.AddAttribute(HtmlTextWriterAttribute.Href, uri);

                if (OpenFlashLocateAsPopup)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Target, "FlashLocate");

                    writer.AddAttribute(HtmlTextWriterAttribute.Onclick, "window.open(this.href, this.target, 'status=1,scrollbars=1,toolbar=0,resizable=1,width=995,height=720',''); return false;");
                }

            }
            else
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Href, "#");

                writer.AddAttribute(HtmlTextWriterAttribute.Disabled, "true");
            }

            writer.RenderBeginTag(HtmlTextWriterTag.A);

            writer.Write("flash locate");

            writer.RenderEndTag(); // A

            writer.RenderEndTag(); // Td
        }

        #endregion

        #region Implementation of IPostBackDataHandler

        private bool _newSort;

        private string _newSortExpression;

        private SortDirection _newSortDirection;

        private bool _handleDeletes;

        public bool LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
            int idOffset = UniqueID.Length + 1;

            if (postDataKey.StartsWith(UniqueID, StringComparison.InvariantCulture) && !postDataKey.Equals(UniqueID))
            {
                string controlKey = postDataKey.Substring(idOffset);

                string[] rowCommand = controlKey.Split(IdSeparator);

                int row;

                if (ThisSR.NotesIdPrefix.Equals(rowCommand[0]) && int.TryParse(rowCommand[1], out row))
                {
                    if (!NotesDelta.ContainsKey(row))
                    {
                        NotesDelta.Add(row, postCollection[postDataKey]); 
                    }
                    else
                    {
                        NotesDelta[row] = postCollection[postDataKey];
                    }

                    return true;
                }

                if (ThisSR.QuantityIdPrefix.Equals(rowCommand[0]) && int.TryParse(rowCommand[1], out row))
                {
                    int quantity;

                    if (int.TryParse(postCollection[postDataKey], out quantity))
                    {
                        if (!QuantityDelta.ContainsKey(row))
                        {
                            QuantityDelta.Add(row, quantity); 
                        }
                        else
                        {
                            QuantityDelta[row] = quantity;
                        }                        

                        return true;
                    }
                }

                if (ThisSR.SelectedCheckboxIdPrefix.Equals(rowCommand[0]) && int.TryParse(rowCommand[1], out row))
                {
                    if (!DeletedRows.Contains(row))
                    {
                        DeletedRows.Add(row);
                    }
                    else
                    {
                        DeletedRows.Remove(row);
                    }

                    _handleDeletes = true;
                }

                if (AllowSorting && string.Equals(rowCommand[0], "sort", StringComparison.InvariantCulture))
                {
                    _newSort = true;

                    if (string.Equals(SortExpressionInternal, rowCommand[1], StringComparison.InvariantCultureIgnoreCase))
                    {
                        _newSortExpression = SortExpressionInternal;
                        _newSortDirection = SortDirectionInternal == SortDirection.Ascending
                                                    ? SortDirection.Descending
                                                    : SortDirection.Ascending;
                    }
                    else
                    {
                        _newSortExpression = rowCommand[1];
                        _newSortDirection = SortDirection.Ascending;
                    }                    
                }
            }

            return false;
        }

        public void RaisePostDataChangedEvent()
        {
            if (!EnableViewState)
            {
                EnsureDataBound();
            }

            HandleEvents();

            if (_newSort)
            {
                SortDirectionInternal = _newSortDirection;

                SortExpressionInternal = _newSortExpression;
            }
        }

        bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
            return LoadPostData(postDataKey, postCollection);
        }

        void IPostBackDataHandler.RaisePostDataChangedEvent()
        {
            RaisePostDataChangedEvent();
        }

        #endregion

        #region Implementation of IScriptControl

        public IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {
            ScriptBehaviorDescriptor scriptBehaviorDescriptor =
                new ScriptBehaviorDescriptor("FirstLook.Pricing.WebControls.PurchaseListTable", ClientID);

            scriptBehaviorDescriptor.AddProperty("confirm_dirty_unload", ConfirmDirtyUnload);
            scriptBehaviorDescriptor.AddProperty("is_dirty", ListInfoIsDirty);

            return new ScriptDescriptor[] { scriptBehaviorDescriptor };
        }

        public IEnumerable<ScriptReference> GetScriptReferences()
        {
            return new[]
                       {
                           new ScriptReference("FirstLook.Pricing.WebControls.PurchaseListTable.js",
                                               "FirstLook.Pricing.WebControls")
                       };
        }

        #endregion

        #region Databinding

        protected override void PerformDataBinding(IEnumerable data)
        {
            DeletedRows.Clear();
            NotesDelta.Clear();
            QuantityDelta.Clear();
            PurchaseList.Clear();

            if (data != null)
            {
                IReportItemCollection collection = (IReportItemCollection)data;

                if (AllowSorting)
                {
                    string sortExpression = SortExpressionInternal;

                    if (string.IsNullOrEmpty(sortExpression))
                    {
                        sortExpression = "MarketDaysSupply";
                    }

                    if (Enum.IsDefined(typeof(SortExpression), sortExpression))
                    {
                        SortExpression sort =
                            (SortExpression)
                            Enum.Parse(typeof(SortExpression), sortExpression);

                        collection.Sort(sort, SortDirection == SortDirection.Ascending);
                    }
                }

                foreach (IReportItem reportItem in collection)
                {
                    PurchaseList.Add(new MarketReportItem(reportItem));
                }
            }
        }

        #endregion

        #region ViewState

        protected override void LoadViewState(object savedState)
        {
            if (savedState != null)
            {
                Pair state = savedState as Pair;
                if (state != null)
                {
                    base.LoadViewState(state.First);

                    ((IStateManager)PurchaseList).LoadViewState(state.Second);
                }
                else
                {
                    base.LoadViewState(savedState);    
                }
            }
        }

        protected override object SaveViewState()
        {
            return new Pair(base.SaveViewState(), ((IStateManager)PurchaseList).SaveViewState());
        }

        protected override void LoadControlState(object savedState)
        {
            if (savedState != null)
            {
                Pair state = savedState as Pair;

                if (state != null)
                {
                    base.LoadControlState(state.First);

                    object[] propertyState = state.Second as object[];

                    if (propertyState != null)
                    {
                        int idx = 0;
                        _notesDelta = (Dictionary<int, string>)propertyState[idx++];
                        _quantityDelta = (Dictionary<int, int>)propertyState[idx++];
                        _sortExpression = (string)propertyState[idx++];
                        _sortDirection = (SortDirection)propertyState[idx++];
                        _deletedRows = (Collection<int>)propertyState[idx];
                    }
                }
                else
                {
                    base.LoadControlState(savedState);
                }
            }
        }

        protected override object SaveControlState()
        {
            object[] propertyState = new object[5];

            int i = 0;

            propertyState[i++] = _notesDelta;
            propertyState[i++] = _quantityDelta;
            propertyState[i++] = _sortExpression;
            propertyState[i++] = _sortDirection;
            propertyState[i] = _deletedRows;

            return new Pair(base.SaveControlState(), propertyState);
        }

        #endregion

        #region InnerClasses

        private static class ThisSR
        {
            public const string CssClass = "shopping_list_table wrapper";

            public const string TableCssClass = "grid";

            #region TableColumns

            public const string TableColumnSelectCssClass = "select";

            public const string TableColumnQuantityCssClass = "quantity";

            public const string TableColumnFlashLocateCssClass = "flash_locate";

            public const string TableColumnSelectTitle = "&times;";

            public const string TableColumnQuantityTitle = "Quantity";

            public const string TableColumnFlashLocateTitle = "Flash";

            #endregion

            #region TableRows

            public const string TableRowNotesCssClass = "notes";

            #endregion

            #region Controls

            public const string NotesIdPrefix = "no";

            public const string NotesLabelText = "Notes:";

            public const string NotesTextAreaRows = "8";

            public const string NotesTextAreaCols = "40";

            public const string SelectedCheckboxIdPrefix = "se";

            public const string QuantityIdPrefix = "qu";

            #endregion
        }

        #endregion

        #region Overrides of DataBoundReportTable

        public override int PageCount
        {
            get
            {
                /// Integer Division in C# is always rounded towards zero, PM
                return PurchaseList.Count / PageSize;
            }
        }

        public override int PageIndex
        {
            get { return 1; }
            set { }
        }

        public override int RowCount
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
    }
}
