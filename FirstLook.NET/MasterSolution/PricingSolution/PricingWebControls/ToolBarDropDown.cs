using System.ComponentModel;
using System.Web.UI;

namespace FirstLook.Pricing.WebControls
{
	[ParseChildren(true, "DropDownItems"), PersistChildren(false)]
	public class ToolBarDropDown : ToolBarItem
	{
		[DefaultValue(true), Localizable(false), Bindable(false), Category("Behavior"), Description("Enabled")]
		public bool AutoGenerateItems
		{
			get
			{
				object value = ViewState["AutoGenerateItems"];
				if (value == null)
					return true;
				return (bool)value;
			}
			set
			{
				if (AutoGenerateItems != value)
				{
					if (value)
					{
						ViewState.Remove("AutoGenerateItems");
					}
					else
					{
						ViewState["AutoGenerateItems"] = value;
					}
				}
			}
		}

	    private ToolBarItemCollection _dropDownItems;

		[Description("Drop-Down Items"),
		 MergableProperty(false),
		 PersistenceMode(PersistenceMode.InnerDefaultProperty),
		 Category("Default")]
		public ToolBarItemCollection DropDownItems
		{
			get
			{
                if (_dropDownItems == null)
                {
                    _dropDownItems = new ToolBarItemCollection();
                    if (IsTrackingViewState)
                    {
                        ((IStateManager)_dropDownItems).TrackViewState();
                    }
                }
                return _dropDownItems;
			}			
		}

        protected override void TrackViewState()
        {
            base.TrackViewState();
            if (_dropDownItems != null)
            {
                ((IStateManager)_dropDownItems).TrackViewState();
            }
        }

		protected override void LoadViewState(object state)
		{
			Pair pair = (Pair) state;

			base.LoadViewState(pair.First);

			if (pair.Second != null)
			{
				((IStateManager)DropDownItems).LoadViewState(pair.Second);
			}
		}

		protected override object SaveViewState()
		{
			Pair pair = new Pair(base.SaveViewState(), null);

			if (_dropDownItems != null)
			{
				pair.Second = ((IStateManager) DropDownItems).SaveViewState();
			}

			return pair;
		}
	}
}
