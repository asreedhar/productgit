using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing.Design;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Utilities;

[assembly: WebResource("FirstLook.Pricing.WebControls.DocumentEditor.js", "text/javascript")]
[assembly: WebResource("FirstLook.Pricing.WebControls.DocumentEditor.debug.js", "text/javascript")]

namespace FirstLook.Pricing.WebControls
{
	[ParseChildren(true, "ToolBars"), PersistChildren(false)]
	public class DocumentEditor : DataBoundControl, INamingContainer, IPostBackDataHandler, IScriptControl
	{
		#region Events

		private static readonly object EventModeChanging = new object();
		private static readonly object EventUpdated = new object();
		private static readonly object EventUpdating = new object();
		private static readonly object EventDeleting = new object();
		private static readonly object EventDeleted = new object();

		public event EventHandler<DocumentEditorModeEventArgs> ModeChanging
		{
			add
			{
				Events.AddHandler(EventModeChanging, value);
			}
			remove
			{
				Events.RemoveHandler(EventModeChanging, value);
			}
		}

		protected virtual void OnModeChanging(DocumentEditorModeEventArgs e)
		{
			EventHandler<DocumentEditorModeEventArgs> handler = (EventHandler<DocumentEditorModeEventArgs>)Events[EventModeChanging];

			if (handler != null)
			{
				handler(this, e);
			}
		}

		public event EventHandler<DocumentEditorUpdatedEventArgs> Updated
		{
			add
			{
				Events.AddHandler(EventUpdated, value);
			}
			remove
			{
				Events.RemoveHandler(EventUpdated, value);
			}
		}

		protected virtual void OnUpdated(DocumentEditorUpdatedEventArgs e)
		{
			EventHandler<DocumentEditorUpdatedEventArgs> handler = (EventHandler<DocumentEditorUpdatedEventArgs>)Events[EventUpdated];

			if (handler != null)
			{
				handler(this, e);
			}
		}

		public event EventHandler<CancelEventArgs> Updating
		{
			add
			{
				Events.AddHandler(EventUpdating, value);
			}
			remove
			{
				Events.RemoveHandler(EventUpdating, value);
			}
		}

		protected virtual void OnUpdating(CancelEventArgs e)
		{
			EventHandler<CancelEventArgs> handler = (EventHandler<CancelEventArgs>)Events[EventUpdating];

			if (handler != null)
			{
				handler(this, e);
			}
		}

		public event EventHandler<CancelEventArgs> Deleting
		{
			add
			{
				Events.AddHandler(EventDeleting, value);
			}
			remove
			{
				Events.RemoveHandler(EventDeleting, value);
			}
		}

		protected virtual void OnDeleting(CancelEventArgs e)
		{
			EventHandler<CancelEventArgs> handler = (EventHandler<CancelEventArgs>)Events[EventDeleting];

			if (handler != null)
			{
				handler(this, e);
			}
		}

		public event EventHandler<DocumentEditorDeletedEventArgs> Deleted
		{
			add
			{
				Events.AddHandler(EventDeleted, value);
			}
			remove
			{
				Events.RemoveHandler(EventDeleted, value);
			}
		}

		protected virtual void OnDeleted(DocumentEditorDeletedEventArgs e)
		{
			EventHandler<DocumentEditorDeletedEventArgs> handler = (EventHandler<DocumentEditorDeletedEventArgs>)Events[EventDeleted];

			if (handler != null)
			{
				handler(this, e);
			}
		}

		#endregion

		#region Fields

		private ScriptManager _manager;

		#endregion

		#region Properties

		private string _documentBody;
		private string _documentFooter;
		private string _documentAuthor;
		private DateTime _documentCreated = DateTime.Now;
		private ArrayList _documentWarnings;

		private DateTime _startEditTime;
		private bool _hasEditedBody;
		private bool _hasEditedFooter;
		private DocumentEditorToolBarCollection _toolBars;

		private string _cancelText;
		private string _editText;
		private string _updateText;
		private string _deleteText;

		private bool _showConfirmationDialog;
		private bool _allowNewLines;
		private bool _showGeneratedByText;
		private bool _hasScrollBars;
		private bool _hasIESpellHook;
		private bool _javascriptEditsAtCursor;
		private bool _onlyShowToolBarOnFocus;
		private bool _hasFooterText = true;
		private bool _hasDocumentProperties;
		private ArrayList _toolBarCommandQueue;

		[DefaultValue(""),
		 Category("Data"),
		 Description("")]
		public string BodyText
		{
			get { return DocumentBody; }
			set { DocumentBody = value; }
		}

		[DefaultValue(""),
		 Category("Data"),
		 Description("")]
		public string FooterText
		{
			get { return DocumentFooter; }
			set { DocumentFooter = value; }
		}

		[DefaultValue("Cancel"),
		 Category("Appearance"),
		 Description("")]
		public string CancelText
		{
			get { return _cancelText ?? SR.CancelButtonText; }
			set { _cancelText = CleanStringValue(value, SR.CancelButtonText); }
		}

		[DefaultValue("Edit"),
		 Category("Appearance"),
		 Description("")]
		public string EditText
		{
			get { return _editText ?? SR.EditButtonText; }
			set { _editText = CleanStringValue(value, SR.EditButtonText); }
		}

		[DefaultValue("Delete"),
		 Category("Appearance"),
		 Description("")]
		public string DeleteText
		{
			get { return _deleteText ?? SR.DeleteButtonText; }
			set { _deleteText = CleanStringValue(value, SR.DeleteButtonText); }
		}

		[DefaultValue("Update"),
		 Category("Appearance"),
		 Description("")]
		public string UpdateText
		{
			get { return _updateText ?? SR.UpdateButtonText; }
			set { _updateText = CleanStringValue(value, SR.UpdateButtonText); }
		}

		[DefaultValue(true),
		 Category("Behavior"),
		 Description("")]
		public bool ShowConfirmationDialog
		{
			get { return _showConfirmationDialog; }
			set { _showConfirmationDialog = value; }
		}

		[DefaultValue(true),
		 Category("Behavior"),
		 Description("")]
		public bool AllowNewLines
		{
			get { return _allowNewLines; }
			set { _allowNewLines = value; }
		}

		[DefaultValue(true),
		 Category("Behavior"),
		 Description("")]
		public bool ShowGeneratedByText
		{
			get { return _showGeneratedByText; }
			set { _showGeneratedByText = value; }
		}
		
		[DefaultValue(false),
		 Category("Behavior"),
		 Description("")]
		public bool HasScrollBars
		{
			get { return _hasScrollBars; }
			set { _hasScrollBars = value; }
		}
		
        [DefaultValue(false),
         Category("Behavior"),
         Description("")]
        public bool HasIESpellHook
        {
        	get { return _hasIESpellHook; }
        	set { _hasIESpellHook = value; }
        }
		
        [DefaultValue(false),
         Category("Behavior"),
         Description("")]
        public bool JavaScriptEditsAtCursor
        {
        	get { return _javascriptEditsAtCursor; }
        	set { _javascriptEditsAtCursor = value; }
        }
		
        [DefaultValue(false),
         Category("Behavior"),
         Description("")]
        public bool OnlyShowToolBarOnFocus
        {
        	get { return _onlyShowToolBarOnFocus; }
        	set { _onlyShowToolBarOnFocus = value; }
        }
		
        [DefaultValue(true),
		 Category("Behavior"),
		 Description("")]
        public bool HasFooterText
        {
        	get { return _hasFooterText; }
			set { _hasFooterText = value; }
        }

		[DefaultValue(true),
		 Category("Behavior"),
		 Description("")]
		public bool HasDocumentProperties
		{
			get { return _hasDocumentProperties; }
			set { _hasDocumentProperties = value; }
		}

		[Description("Document Editor Tool Bars"),
		 MergableProperty(false),
		 PersistenceMode(PersistenceMode.InnerDefaultProperty),
		 Category("Default")]
		public DocumentEditorToolBarCollection ToolBars
		{
			get
			{
				if (_toolBars == null)
				{
					_toolBars = new DocumentEditorToolBarCollection();

					if (IsTrackingViewState)
					{
						((IStateManager)_toolBars).TrackViewState();
					}
				}
				return _toolBars;
			}
		}

		[DefaultValue(DocumentEditorMode.ReadOnly),
		 Category("Behavior"),
		 Description("The current mode of the text editor")]
		public DocumentEditorMode CurrentMode
		{
			get
			{
				object value = ViewState["CurrentMode"];
				if (value == null)
					return DefaultMode;
				return (DocumentEditorMode)value;
			}
		}

		[DefaultValue(true),
		 Category("Behavior"),
		 Description("The default mode of the text editor")]
		public DocumentEditorMode DefaultMode
		{
			get
			{
				object value = ViewState["DefaultMode"];
				if (value == null)
					return DocumentEditorMode.ReadOnly;
				return (DocumentEditorMode)value;
			}
			set
			{
				if (DefaultMode != value)
				{
					ViewState["DefaultMode"] = value;
				}
			}
		}

		protected override string TagName
		{
			get
			{
				return "div";
			}
		}

		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Div;
			}
		}

		[Browsable(false)]
		protected string DocumentBody
		{
			get { return _documentBody ?? string.Empty; }
			set { _documentBody = CleanStringValue(value, string.Empty); }
		}

		[Browsable(false)]
		protected string DocumentFooter
		{
			get { return _documentFooter ?? string.Empty; }
			set { _documentFooter = CleanStringValue(value, string.Empty); }
		}

		[Browsable(false)]
		protected string DocumentAuthor
		{
			get { return _documentAuthor ?? string.Empty; }
			set { _documentAuthor = CleanStringValue(value, string.Empty); }
		}

		[Browsable(false)]
		protected DateTime DocumentCreated
		{
			get { return _documentCreated; }
			set { _documentCreated = value; }
		}

		[Browsable(false)]
		protected ArrayList DocumentWarnings
		{
			get
			{
				if (_documentWarnings == null)
				{
					_documentWarnings = new ArrayList();
				}
				return _documentWarnings;
			}
			set
			{
				if (DocumentWarnings != value)
				{
					_documentWarnings = value;
				}
			}
		}

		[Browsable(false)]
		protected ArrayList ToolBarCommandQueue
		{
			get
			{
				if (_toolBarCommandQueue == null)
				{
					_toolBarCommandQueue = new ArrayList();
				}
				return _toolBarCommandQueue;
			}
			set
			{
				if (DocumentWarnings != value)
				{
					_documentWarnings = value;
				}
			}
		}

        #endregion

		#region Methods

        public void ChangeMode(DocumentEditorMode mode)
        {
            ViewState["CurrentMode"] = mode;
        }

        protected string CreateId(string id)
        {
            return UniqueID + IdSeparator + id;
        }

		private static string CleanStringValue(string value, string defaultValue)
		{
			if (value == null) return null;

			if (value.Equals(defaultValue)) return null;

			return value;
		}

        #region WebControls Override

        protected override Control FindControl(string id, int pathOffset)
        {
            return this;
        }
        
        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            if (string.IsNullOrEmpty(CssClass))
            {
                CssClass = SR.TextEditorWrapperCssClass;
            }

            if (CurrentMode == DocumentEditorMode.ReadOnly)
            {
                CssClass += " " + SR.ReadonlyModeCssClass;
            }
            else if (CurrentMode == DocumentEditorMode.Edit)
            {
                CssClass += " " + SR.EditModeCssClass;
            }
            else if (CurrentMode == DocumentEditorMode.Confirmation)
            {
                CssClass += " " + SR.ConfirmationModeCssClass;
            }

            base.AddAttributesToRender(writer);
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            if (CurrentMode == DocumentEditorMode.Insert)
            {
                RenderInsertMode(writer);
            }
            else if (CurrentMode == DocumentEditorMode.ReadOnly)
            {
                RenderReadOnlyMode(writer);
            }
            else if (CurrentMode == DocumentEditorMode.Edit)
            {
                RenderEditMode(writer);
            }
            else if (CurrentMode == DocumentEditorMode.Confirmation)
            {
                RenderConfirmationMode(writer);
            }
        }

        #endregion

        #region Render Methods

        private void RenderInsertMode(HtmlTextWriter writer)
        {
			RenderClickSteamInput(writer);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.TextareaWrapperCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            RenderReadOnlyBodyTextBox(writer);

            writer.AddStyleAttribute(HtmlTextWriterStyle.Display, "none");
            RenderReadOnlyFooterTextBox(writer);

            writer.RenderEndTag(); // div SR.TextareaWrapperCssClass

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ControlButtonsWrapperCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            RenderEditControlButton(writer);

            writer.RenderEndTag(); // div SR.ControlButtonsWrapperCssClass
        }

        private void RenderReadOnlyMode(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.TextareaWrapperCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            RenderReadOnlyBodyTextBox(writer);

            writer.AddStyleAttribute(HtmlTextWriterStyle.Display, "none");
            RenderReadOnlyFooterTextBox(writer);

            writer.RenderEndTag(); // div SR.TextareaWrapperCssClass

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ControlButtonsWrapperCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            RenderEditControlButton(writer);

            writer.RenderEndTag(); // div SR.ControlButtonsWrapperCssClass
        }

        private void RenderEditMode(HtmlTextWriter writer)
        {
			RenderClickSteamInput(writer);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.TextareaWrapperCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            RenderBodyTextBox(writer);

            writer.AddStyleAttribute(HtmlTextWriterStyle.Display, "none");
            RenderFooterTextBox(writer);

            writer.RenderEndTag(); // div SR.TextareaWrapperCssClass

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ControlButtonsWrapperCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            RenderUpdateControlButton(writer);
            RenderCancelControlButton(writer);
            RenderDeleteControlButton(writer);

            writer.RenderEndTag(); // div SR.ControlButtonsWrapperCssClass
        }

        private void RenderConfirmationMode(HtmlTextWriter writer)
        {
            RenderClickSteamInput(writer);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.TextareaWrapperCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            RenderReadOnlyBodyTextBox(writer);

            writer.AddStyleAttribute(HtmlTextWriterStyle.Display, "none");
            RenderReadOnlyFooterTextBox(writer);

            writer.RenderEndTag(); // div SR.TextareaWrapperCssClass

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ConfirmationWrapperCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ConfirmationDialogContainerCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ConfirmationDialogTitlebarCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ConfirmationDialogTitleCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Span);
            writer.Write(SR.ConfirmationTitle);
            writer.RenderEndTag(); // span SR.ConfirmationDialogTitleCssClass
            writer.RenderEndTag(); // div SR.ConfirmationDialogTitlebarCssClass

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ConfirmationDialogContentCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ConfirmationDialogContentWrapperCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            // text

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ConfirmationDialogContentConfirmationTextCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.P);
            writer.Write(SR.ConfirmationText);
            writer.RenderEndTag(); // P

            writer.AddAttribute(HtmlTextWriterAttribute.Id, CreateId(SR.ConfirmationBodyTextId));
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ConfirmationDialogContentAdContentCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.P);
            writer.Write(DocumentBody);
            writer.RenderEndTag(); // P

            if (HasFooterText)
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Hr);
                writer.RenderEndTag();

                RenderConfirmationFooterTextBox(writer);
            }

            writer.RenderEndTag(); // Div.ConfirmationDialogContentCssClass

            writer.RenderEndTag(); // Div.ConfirmationDialogContentWrapperCssClass

            // okay and cancel buttons
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ConfirmationDialogButtonWrapperCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.RenderBeginTag(HtmlTextWriterTag.Ul);

            writer.RenderBeginTag(HtmlTextWriterTag.Li);
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ConfirmationButtonCssClass);
            RenderConfirmControlButton(writer);
            writer.RenderEndTag();

            writer.RenderBeginTag(HtmlTextWriterTag.Li);
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ConfirmationButtonCssClass);
            RenderCancleConfirmControlButton(writer);
            writer.RenderEndTag();

            writer.RenderEndTag(); // UL

            writer.RenderEndTag(); // Div.ConfirmationDialogButtonWrapperCssClass

            writer.RenderEndTag(); // div SR.ConfirmationDialogContainerCssClass

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ConfirmationDialogNBorderCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ConfirmationDialogSBorderCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ConfirmationDialogEBorderCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ConfirmationDialogWBorderCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ConfirmationDialogNEBorderCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ConfirmationDialogSEBorderCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ConfirmationDialogSWBorderCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ConfirmationDialogNWBorderCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.RenderEndTag();

            writer.RenderEndTag(); // div SR.ConfirmationWrapperCssClass
        }

		private void RenderClickSteamInput(HtmlTextWriter writer)
		{
			writer.AddAttribute(HtmlTextWriterAttribute.Name, CreateId(SR.ClickStreamId));
			writer.AddAttribute(HtmlTextWriterAttribute.Id, CreateId(SR.ClickStreamId));
			writer.AddAttribute(HtmlTextWriterAttribute.Type, "hidden");
			writer.RenderBeginTag(HtmlTextWriterTag.Input);
			writer.RenderEndTag();
		}

        #region Control Button Helpers
        private void RenderEditControlButton(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.EditButtonCssClass);
            RenderControlButton(writer, SR.EditButtonId, EditText);
        }

        private void RenderUpdateControlButton(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.EditButtonCssClass);
            RenderControlButton(writer, SR.UpdateButtonId, UpdateText);
        }

        private void RenderCancelControlButton(HtmlTextWriter writer)
        {
            RenderControlButton(writer, SR.CancelButtonId, CancelText);
        }

        private void RenderDeleteControlButton(HtmlTextWriter writer)
        {
            RenderControlButton(writer, SR.DeleteButtonId, DeleteText);
        }

        private void RenderConfirmControlButton(HtmlTextWriter writer)
        {
            RenderControlButton(writer, SR.UpdateButtonId, UpdateText);
        }

        private void RenderCancleConfirmControlButton(HtmlTextWriter writer)
        {
            RenderControlButton(writer, SR.CancelButtonId, CancelText);
        }

        private void RenderControlButton(HtmlTextWriter writer, string id, string text)
        {
            string uniqueID = CreateId(id);

            writer.AddAttribute(HtmlTextWriterAttribute.Id, uniqueID);
            writer.AddAttribute(HtmlTextWriterAttribute.Type, SR.InputButtonType);
            writer.AddAttribute(HtmlTextWriterAttribute.Name, uniqueID);
            writer.AddAttribute(HtmlTextWriterAttribute.Value, text);
            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();
        }
        #endregion

        #region TextBox Helpers
        private void RenderBodyTextBox(HtmlTextWriter writer)
        {
            RenderTextBox(writer, SR.BodyTextArea, false, DocumentBody);
        }

        private void RenderReadOnlyBodyTextBox(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ReadonlyTextAreaCssClass);
            RenderTextBox(writer, SR.ReadonlyBodyTextArea, true, DocumentBody);
        }

        private void RenderFooterTextBox(HtmlTextWriter writer)
        {
            RenderTextBox(writer, SR.FooterTextArea, false, DocumentFooter);
        }

        private void RenderReadOnlyFooterTextBox(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ReadonlyTextAreaCssClass);
            RenderTextBox(writer, SR.ReadonlyFooterTextArea, true, DocumentFooter);
        }

        private void RenderConfirmationFooterTextBox(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.For, SR.ConfirmationFooterTextAreaId);

            writer.RenderBeginTag(HtmlTextWriterTag.Label);

            writer.Write(SR.ConfirmationFooterTextLabel);

            writer.RenderEndTag();

            RenderTextBox(writer, SR.ConfirmationFooterTextAreaId, false, DocumentFooter);
        }
        
        private void RenderTextBox(HtmlTextWriter writer, string id, bool isReadOnly, string text)
        {
            string uniqueID = CreateId(id);
            writer.AddAttribute(HtmlTextWriterAttribute.Rows, "10");
            writer.AddAttribute(HtmlTextWriterAttribute.Cols, "48");
            writer.AddAttribute(HtmlTextWriterAttribute.Id, uniqueID);
            writer.AddAttribute(HtmlTextWriterAttribute.Name, uniqueID);

            if (isReadOnly)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.ReadOnly, "ReadOnly");
            }

            writer.RenderBeginTag(HtmlTextWriterTag.Textarea);
            writer.Write(text);
            writer.RenderEndTag();
        } 
        #endregion

        #endregion

        #endregion

		#region Data Binding

		private OrderedDictionary _boundFieldValues;
		private DataKey _dataKey;
		private string[] _dataKeyNames;
		private OrderedDictionary _keyTable;

		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Description("DetailsView_DataKey")]
		public virtual DataKey DataKey
		{
			get
			{
				if (_dataKey == null)
				{
					_dataKey = new DataKey(KeyTable);
				}
				return _dataKey;
			}
		}

		[Description("DataControls_DataKeyNames"), DefaultValue((string)null), Category("Data")]
		[Editor("System.Web.UI.Design.WebControls.DataFieldEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor)), TypeConverter(typeof(StringArrayConverter))]
		public virtual string[] DataKeyNames
		{
			get
			{
				if (_dataKeyNames != null)
				{
					return (string[])(_dataKeyNames).Clone();
				}
				return new string[0];
			}
			set
			{
				if (!StringHelper.CompareStringArrays(value, DataKeyNamesInternal))
				{
					if (value != null)
					{
						_dataKeyNames = (string[])value.Clone();
					}
					else
					{
						_dataKeyNames = null;
					}
					_keyTable = null;
					if (Initialized)
					{
						RequiresDataBinding = true;
					}
				}
			}
		}

		private string[] DataKeyNamesInternal
		{
			get
			{
				if (_dataKeyNames != null)
				{
					return _dataKeyNames;
				}
				return new string[0];
			}
		}

		private IOrderedDictionary BoundFieldValues
		{
			get
			{
				if (_boundFieldValues == null)
				{
					_boundFieldValues = new OrderedDictionary();
				}
				return _boundFieldValues;
			}
		}

		private OrderedDictionary KeyTable
		{
			get
			{
				if (_keyTable == null)
				{
					_keyTable = new OrderedDictionary(DataKeyNamesInternal.Length);
				}
				return _keyTable;
			}
		}

		protected override void PerformDataBinding(IEnumerable data)
		{
			BoundFieldValues.Clear();

            KeyTable.Clear();

			if (data != null)
			{
				IEnumerator en = data.GetEnumerator();

				if (en.MoveNext())
				{
					object document = en.Current;

					DocumentBody = DataBinder.Eval(document, "Body", null);

					if (HasFooterText)
					{
						DocumentFooter = DataBinder.Eval(document, "Footer", null);
					}
					else
					{
						DocumentFooter = string.Empty;
					}

					if (HasDocumentProperties)
					{
						DocumentAuthor = DataBinder.Eval(document, "Properties.Author", null);

						DocumentCreated = (DateTime) DataBinder.Eval(document, "Properties.Created");

						IEnumerable warnings = (IEnumerable) DataBinder.Eval(document, "Properties.Warnings");

						foreach (string warning in warnings)
						{
							DocumentWarnings.Add(warning);
						}
					}
					else
					{
						DocumentAuthor = string.Empty;
					}

					ExtractKeyTable(document);

					ExtractBoundFieldValues(BoundFieldValues);
				}
			}
		}

		private void ExtractKeyTable(object dataItem)
		{
			string[] dataKeyNamesInternal = DataKeyNamesInternal;

			if (dataKeyNamesInternal.Length != 0)
			{
				OrderedDictionary keyTable = KeyTable;

				foreach (string str in dataKeyNamesInternal)
				{
					object propertyValue = DataBinder.GetPropertyValue(dataItem, str);

					keyTable.Add(str, propertyValue);
				}

				_dataKey = new DataKey(keyTable);
			}
		}

		private void ExtractBoundFieldValues(IDictionary values)
		{
			values.Add("Body", DocumentBody);
			values.Add("Footer", DocumentFooter);
		}

		#endregion

		#region Life Cycle

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			if (_toolBars != null)
			{
				foreach (DocumentEditorToolBar toolBarReference in _toolBars)
				{
					ToolBar toolBar = toolBarReference.GetControl(this);

					if (toolBar != null)
					{
						toolBar.Command += HandleToolBarEvent;
					}
				}
			}
		}

		protected override void OnUnload(EventArgs e)
		{
			base.OnUnload(e);

			if (_toolBars != null)
			{
				foreach (DocumentEditorToolBar toolBarReference in _toolBars)
				{
					ToolBar toolBar = toolBarReference.GetControl(this);

					if (toolBar != null)
					{
						toolBar.Command -= HandleToolBarEvent;
					}
				}
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			if (!DesignMode)
			{
				_manager = ScriptManager.GetCurrent(Page);

				if (_manager != null)
				{
					_manager.RegisterScriptControl(this);
				}
				else
				{
					throw new InvalidOperationException("You must have a ScriptManager!");
				}
			}
		}

        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);

            if (!DesignMode)
            {
                _manager.RegisterScriptDescriptors(this);
            }
        }

		#endregion

		#region Event Handling Code

		private void HandleToolBarEvent(object sender, CommandEventArgs e)
		{
			if (string.Equals(e.CommandName, "TextFragment", StringComparison.OrdinalIgnoreCase))
			{
				// make sure that the command is not for an event processed on the client side

				bool append = true;

				foreach (Triplet item in ToolBarCommandQueue)
				{
					string buttonName = (string) item.First;

					bool asyncOnly = (bool) item.Second;

					if (Equals(buttonName, e.CommandArgument))
					{
						if (asyncOnly)
						{
							// yup - it was added on the client side already so mark
							// the triplet as *not being* async-only

							item.Second = false;

							append = false;
						}
						
						break;
					}
				}

				if (append)
				{
					// append non-async items to the queue

					ToolBarButton button = ((ToolBar) sender).Items.FindItem((string) e.CommandArgument) as ToolBarButton;

					if (button != null)
					{
						int position = string.IsNullOrEmpty(DocumentBody) ? 0 : DocumentBody.Length - 1;
						ToolBarCommandQueue.Add(new Triplet(e.CommandArgument, false, position));
						DocumentBody += button.BodyText;
						DocumentFooter += button.FooterText;
					}
				}
			}
		}
		
		private void HandleToolBarClickStream(string events)
		{
			if (!string.IsNullOrEmpty(events))
			{
				// we expect to deserialize an array of pairs (i.e. array[2])
				// * array[0] is the command name
				// * array[1] is the character position

				string[] commands = events.Split(new char[] {';'});

				foreach (string command in commands)
				{
					if (!string.IsNullOrEmpty(command))
					{
						string[] parts = command.Split(new char[] { ',' });

						if (parts.Length == 2)
						{
							string name = parts[0];

							int position = int.Parse(parts[1]);

							ToolBarCommandQueue.Add(new Triplet(name, true, position));
						}
					}
				}
			}
		}

		private void HandleUserEdits()
		{
			StringBuilder bodyBuilder = new StringBuilder();

			StringBuilder footerBuilder = new StringBuilder();

			bool hasEditedBody = false, hasEditedFooter = false;

			foreach (Triplet toolBarCommand in ToolBarCommandQueue)
			{
				string name = (string) toolBarCommand.First;

				int position = (int) toolBarCommand.Third;

				foreach (DocumentEditorToolBar toolBarReference in ToolBars)
				{
					ToolBar toolBar = toolBarReference.GetControl(this);

					if (toolBar != null)
					{
						ToolBarButton item = toolBar.Items.FindItem(name) as ToolBarButton;

						if (item != null)
						{
							if (!hasEditedBody)
							{
								if (!string.IsNullOrEmpty(item.BodyText))
								{
									if (position < bodyBuilder.Length)
									{
										bodyBuilder.Append(item.BodyText, position, item.BodyText.Length);
									}
									else
									{
										hasEditedBody = true;
									}
								}
							}
							
							if (!hasEditedFooter)
							{
								if (!string.IsNullOrEmpty(item.FooterText))
								{
									if (position < item.FooterText.Length)
									{
										footerBuilder.Append(item.FooterText);
									}
									else
									{
										hasEditedFooter = true;
									}
								}
							}
						}
					}

					if (hasEditedBody && hasEditedFooter) break;
				}
			}

			if (!hasEditedBody)
			{
				hasEditedBody = !DocumentBody.Equals(bodyBuilder.ToString(), StringComparison.OrdinalIgnoreCase);
			}

			if (!hasEditedFooter)
			{
				hasEditedFooter = !DocumentBody.Equals(footerBuilder.ToString(), StringComparison.OrdinalIgnoreCase);
			}

			_hasEditedBody = hasEditedBody;

			_hasEditedFooter = hasEditedFooter;
		}

		private void HandleEvent(string commandName)
		{
			// TextEditorMode Commands

			if (string.Equals(commandName, SR.EditButtonId))
			{
				HandleEdit();
			}
			else if (string.Equals(commandName, SR.UpdateButtonId))
			{
				if (CurrentMode == DocumentEditorMode.Edit)
				{
					if (ShowConfirmationDialog)
					{
						HandleConfirmation();
					}
					else
					{
						HandleUpdate();
					}
				}
				else
				{
					HandleUpdate();
				}
			}
			else if (string.Equals(commandName, SR.CancelButtonId))
			{
				if (CurrentMode == DocumentEditorMode.Confirmation)
				{
					HandleEdit();
				}
				else
				{
					HandleCancel();
				}
			}
			else if (string.Equals(commandName, SR.DeleteButtonId))
			{
				HandleDelete();
			}
		}

		private void HandleCancel()
		{
			DocumentEditorModeEventArgs e = new DocumentEditorModeEventArgs(DocumentEditorMode.ReadOnly);

			OnModeChanging(e);

			if (!e.Cancel)
			{
				RequiresDataBinding = true;

				ChangeMode(e.NewMode);
			}
		}

		private void HandleEdit()
		{
			DocumentEditorModeEventArgs e = new DocumentEditorModeEventArgs(DocumentEditorMode.Edit);

			OnModeChanging(e);

			if (!e.Cancel)
			{
				_startEditTime = DateTime.Now;

				ChangeMode(e.NewMode);
			}
		}

		private void HandleConfirmation()
		{
			DocumentEditorModeEventArgs e = new DocumentEditorModeEventArgs(DocumentEditorMode.Confirmation);

			OnModeChanging(e);

			if (!e.Cancel)
			{
				ChangeMode(e.NewMode);
			}
		}

		private void HandleDelete()
		{
			if (IsBoundUsingDataSourceID)
			{
				GetDataSource().GetView(DataMember ?? string.Empty).Delete(
						DataKey.Values, BoundFieldValues, HandleDeleteCallback);
			}
		}

		bool HandleDeleteCallback(int affectedRows, Exception ex)
		{
			DocumentEditorDeletedEventArgs e = new DocumentEditorDeletedEventArgs(affectedRows, ex);

			OnDeleted(e);

			if ((ex != null) && !e.ExceptionHandled)
			{
				return false;
			}

			if (!e.KeepInEditMode)
			{
				DocumentEditorModeEventArgs m = new DocumentEditorModeEventArgs(DefaultMode);

				OnModeChanging(m);

				if (!m.Cancel)
				{
					ChangeMode(DefaultMode);

					RequiresDataBinding = true;
				}
			}

			return true;
		}

		private void HandleUpdate()
		{
			if (CurrentMode == DocumentEditorMode.Edit || CurrentMode == DocumentEditorMode.Confirmation)
			{
				if (IsBoundUsingDataSourceID)
				{
					CancelEventArgs ce = new CancelEventArgs();

					OnUpdating(ce);

					if (ce.Cancel)
						return;

					OrderedDictionary values = new OrderedDictionary();

					ExtractBoundFieldValues(values);

					IDictionary properties = new Hashtable();
					properties["Author"] = Context.User.Identity.Name;
					properties["EditDuration"] = DateTime.Now - _startEditTime;
					properties["HasEditedBody"] = _hasEditedBody;
					properties["HasEditedFooter"] = _hasEditedFooter;

					values.Add("Properties", properties);

					IDictionary toolBarUsage = new Hashtable();

					foreach (DocumentEditorToolBar toolBarReference in ToolBars)
					{
						ToolBar toolBar = toolBarReference.GetControl(this);

						if (toolBar != null)
						{
                            GetAvailableToolBarItems(toolBar.Items, toolBarUsage);
						}
					}

                   
					if (_toolBarCommandQueue != null)
					{
						foreach (Triplet triplet in _toolBarCommandQueue)
						{
							string name = (string) triplet.First;

							if (toolBarUsage.Contains(name))
							{
								if (toolBarUsage[name].Equals("Available"))
								{
									toolBarUsage[name] = "Included";
								}
							}
						}
					}

					values.Add("ToolBarUsage", toolBarUsage);

					GetDataSource().GetView(DataMember ?? string.Empty).Update(
						DataKey.Values, values, BoundFieldValues, HandleUpdateCallback);
				}
			}
		}

	    static void GetAvailableToolBarItems(ToolBarItemCollection items, IDictionary usage)
        {
            foreach (ToolBarItem item in items)
            {
                usage[item.Name] = item.Enabled ? "Available" : "NotAvailable";

                ToolBarDropDown dropDown = item as ToolBarDropDown;

                if (dropDown != null)
                {
                    GetAvailableToolBarItems(dropDown.DropDownItems, usage);
                }
            }
        }

		bool HandleUpdateCallback(int affectedRows, Exception ex)
		{
			DocumentEditorUpdatedEventArgs e = new DocumentEditorUpdatedEventArgs(affectedRows, ex);

			OnUpdated(e);

			if ((ex != null) && !e.ExceptionHandled)
			{
				return false;
			}

			if (!e.KeepInEditMode)
			{
				DocumentEditorModeEventArgs m = new DocumentEditorModeEventArgs(DefaultMode);

				OnModeChanging(m);

				if (!m.Cancel)
				{
					ChangeMode(m.NewMode);

					RequiresDataBinding = true;
				}
			}

			return true;
		}

		#endregion

		#region IStateManager

		protected override void TrackViewState()
		{
			base.TrackViewState();

			if (_toolBars != null)
			{
				((IStateManager)_toolBars).TrackViewState();
			}
		}

		protected override void LoadViewState(object savedState)
		{
			Pair pair = savedState as Pair;

			if (pair != null)
			{
                base.LoadViewState(pair.First);

				object[] state = (object[]) pair.Second;

				int idx = 0;

				DocumentBody = (string) state[idx++];
				DocumentFooter = (string) state[idx++];
				DocumentAuthor = (string) state[idx++];
				DocumentCreated = (DateTime) state[idx++];
				DocumentWarnings = (ArrayList) state[idx++];

				_startEditTime = (DateTime) state[idx++];
				_hasEditedBody = (bool) state[idx++];
				_hasEditedFooter = (bool) state[idx++];
				if (state[idx] != null) ((IStateManager)ToolBars).LoadViewState(state[idx]); idx++;
				if (state[idx] != null) OrderedDictionaryStateHelper.LoadViewState(BoundFieldValues, (ArrayList) state[idx]); idx++;
				if (state[idx] != null) _toolBarCommandQueue = (ArrayList) state[idx]; idx++;

				CancelText = (string) state[idx++];
				EditText = (string) state[idx++];
				UpdateText = (string) state[idx++];
				DeleteText = (string) state[idx++];

				ShowConfirmationDialog = (bool) state[idx++];
				AllowNewLines = (bool) state[idx++];
				ShowGeneratedByText = (bool) state[idx++];
				HasScrollBars = (bool) state[idx++];
				HasIESpellHook = (bool) state[idx++];
				JavaScriptEditsAtCursor = (bool)state[idx++];
				OnlyShowToolBarOnFocus = (bool)state[idx++];
				HasFooterText = (bool) state[idx++];
				HasDocumentProperties = (bool)state[idx++];
			}
			else
			{
				base.LoadViewState(savedState);
			}
		}

		protected override object SaveViewState()
		{
			object[] state = new object[24];

			int idx = 0;

			state[idx++] = _documentBody;
			state[idx++] = _documentFooter;
			state[idx++] = _documentAuthor;
			state[idx++] = _documentCreated = DateTime.Now;
			state[idx++] = _documentWarnings;

			state[idx++] = _startEditTime;
			state[idx++] = _hasEditedBody;
			state[idx++] = _hasEditedFooter;
			state[idx++] = (_toolBars != null) ? ((IStateManager)_toolBars).SaveViewState() : null;
			state[idx++] = (_boundFieldValues != null) ? OrderedDictionaryStateHelper.SaveViewState(_boundFieldValues) : null;
			state[idx++] = _toolBarCommandQueue;

			state[idx++] = _cancelText;
			state[idx++] = _editText;
			state[idx++] = _updateText;
			state[idx++] = _deleteText;

			state[idx++] = _showConfirmationDialog;
			state[idx++] = _allowNewLines;
			state[idx++] = _showGeneratedByText;
			state[idx++] = _hasScrollBars;
			state[idx++] = _hasIESpellHook;
			state[idx++] = _javascriptEditsAtCursor;
			state[idx++] = _onlyShowToolBarOnFocus;
			state[idx++] = _hasFooterText;
			state[idx++] = _hasDocumentProperties;

			return new Pair(base.SaveViewState(), state);
		}

		#endregion

		#region IPostBackDataHandler Members

		private string _commandName;
		private string _clickStream;

		protected bool LoadPostData(string postDataKey, NameValueCollection postCollection)
		{
			if (IsEnabled)
			{
				int idOffset = UniqueID.Length + 1;

				if (postDataKey.StartsWith(UniqueID, StringComparison.InvariantCulture))
				{
					string controlKey = postDataKey.Substring(idOffset);

					string controlValue = postCollection[postDataKey];

					// Text Areas

					if (string.Equals(controlKey, SR.BodyTextArea))
                    {
                        DocumentBody = controlValue;
                    }
                    else if (string.Equals(controlKey, SR.FooterTextArea))
                    {
                        DocumentFooter = controlValue;
                    }
                    else if (string.Equals(controlKey, SR.ConfirmationFooterTextAreaId))
                    {
                        DocumentFooter = controlValue;
                    }

					// Click Stream

					else if (string.Equals(controlKey, SR.ClickStreamId))
					{
						_clickStream = controlValue;
						return true;
					}
					
					// Commands

                    else if (string.Equals(controlKey, SR.EditButtonId))
					{
					    _commandName = controlKey;
						return true;
					}
                    else if (string.Equals(controlKey, SR.UpdateButtonId))
                    {
                        _commandName = controlKey;
						return true;
					}
                    else if (string.Equals(controlKey, SR.CancelButtonId))
                    {
                        _commandName = controlKey;
						return true;
					}
                    else if (string.Equals(controlKey, SR.DeleteButtonId))
                    {
                        _commandName = controlKey;
						return true;
					}
				}
			}

			return false;
		}

		protected void RaisePostDataChangedEvent()
		{
			HandleToolBarClickStream(_clickStream);

		    _clickStream = null;

			HandleUserEdits();

			HandleEvent(_commandName);

		    _commandName = null;
		}

		bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
		{
			return LoadPostData(postDataKey, postCollection);
		}

		void IPostBackDataHandler.RaisePostDataChangedEvent()
		{
			RaisePostDataChangedEvent();
		}

		#endregion

		#region IScriptControl Members

        IEnumerable<ScriptDescriptor> IScriptControl.GetScriptDescriptors()
		{
            ScriptBehaviorDescriptor documentEditor = new ScriptBehaviorDescriptor("FirstLook.Pricing.WebControls.DocumentEditor", ClientID);
            documentEditor.ID = ClientID;

            documentEditor.AddProperty("current_mode", CurrentMode);
            documentEditor.AddProperty("allow_new_lines", AllowNewLines);
            documentEditor.AddProperty("has_scroll_bars", HasScrollBars);
            documentEditor.AddProperty("has_ieSpell_hook", HasIESpellHook);
            documentEditor.AddProperty("edits_at_cursor", JavaScriptEditsAtCursor);
            documentEditor.AddProperty("only_show_toolbar_on_focus", OnlyShowToolBarOnFocus);

			ArrayList visibleToolBars = new ArrayList();

			if (CurrentMode == DocumentEditorMode.Insert || CurrentMode == DocumentEditorMode.Edit)
			{
				foreach (DocumentEditorToolBar toolBarReference in ToolBars)
				{
					ToolBar toolBar = toolBarReference.GetControl(this);

					if (toolBar != null && toolBar.Visible)
					{
						Hashtable hashtable = new Hashtable();
						hashtable.Add("ControlID", toolBar.ClientID);
						visibleToolBars.Add(hashtable);
					}
				}
			}

        	documentEditor.AddProperty("toolbars", visibleToolBars);

            if (CurrentMode == DocumentEditorMode.ReadOnly || CurrentMode == DocumentEditorMode.Insert)
            {
                if (CurrentMode == DocumentEditorMode.Insert)
                {
                    documentEditor.AddElementProperty("click_stream_input", CreateId(SR.ClickStreamId)); 
                }
                documentEditor.AddElementProperty("body_textarea", CreateId(SR.ReadonlyBodyTextArea));
                documentEditor.AddElementProperty("footer_textarea", CreateId((SR.ReadonlyFooterTextArea)));
            }
            else if (CurrentMode == DocumentEditorMode.Edit)
            {
                documentEditor.AddElementProperty("click_stream_input", CreateId(SR.ClickStreamId));
                documentEditor.AddElementProperty("body_textarea", CreateId(SR.BodyTextArea));
                documentEditor.AddElementProperty("footer_textarea", CreateId((SR.FooterTextArea)));
            }
            else if (CurrentMode == DocumentEditorMode.Confirmation)
            {
                string highlightDetails =
                "[{Label:'No Highlight',PreviewLength:0,Description:''},{Label:'Basic Ad',PreviewLength:147,Description:'Autotrader Basic Ad Limits the \\n Ad to 150 Characters on the Results Page'},{Label:'Spotlight Ad',PreviewLength:247,Description:'Autotrader Spotlight Ad Limits the \\n Ad to 250 Characters on the Results Page'}]";

                documentEditor.AddProperty("highlight_details", highlightDetails);
                documentEditor.AddProperty("max_characters", 2000); // TODO: Needs to be dynmaic
                documentEditor.AddElementProperty("body_text_element", CreateId(SR.ConfirmationBodyTextId));
                documentEditor.AddElementProperty("confirmation_footer_textarea", CreateId((SR.ConfirmationFooterTextAreaId)));
            }

            return new ScriptDescriptor[] { documentEditor };
		}

		IEnumerable<ScriptReference> IScriptControl.GetScriptReferences()
		{
            if (!HasScrollBars)
            {
                return new ScriptReference[]
                {
                    new ScriptReference("FirstLook.Common.WebControls.Timer.js", "FirstLook.Common.WebControls"),
                    new ScriptReference("FirstLook.Common.WebControls.Animations.Animation.js", "FirstLook.Common.WebControls"),
                    new ScriptReference("FirstLook.Common.WebControls.Animations.PropertyAnimation.js", "FirstLook.Common.WebControls"),
                    new ScriptReference("FirstLook.Common.WebControls.Animations.InterpolatedAnimation.js", "FirstLook.Common.WebControls"),
                    new ScriptReference("FirstLook.Common.WebControls.Animations.LengthAnimation.js", "FirstLook.Common.WebControls"),
                    new ScriptReference("FirstLook.Common.WebControls.Extenders.ExpandingTextarea.js", "FirstLook.Common.WebControls"),
                    new ScriptReference("FirstLook.Pricing.WebControls.DocumentEditor.js","FirstLook.Pricing.WebControls")
                };
            }
            else
            {
                return new ScriptReference[]
                {
                    new ScriptReference("FirstLook.Pricing.WebControls.DocumentEditor.js", "FirstLook.Pricing.WebControls")
                };
            }
		}

		#endregion

		#region Inner Classes

		static class OrderedDictionaryStateHelper
		{
			public static void LoadViewState(IDictionary dictionary, IList state)
			{
				if (dictionary == null)
				{
					throw new ArgumentNullException("dictionary");
				}
				if (state == null)
				{
					throw new ArgumentNullException("state");
				}
				for (int i = 0; i < state.Count; i++)
				{
					Pair pair = (Pair)state[i];
					dictionary.Add(pair.First, pair.Second);
				}
			}

			public static ArrayList SaveViewState(IOrderedDictionary dictionary)
			{
				if (dictionary == null)
				{
					throw new ArgumentNullException("dictionary");
				}
				ArrayList list = new ArrayList(dictionary.Count);
				foreach (DictionaryEntry entry in dictionary)
				{
					list.Add(new Pair(entry.Key, entry.Value));
				}
				return list;
			}
		}

		private static class SR
		{
			public static string TextEditorWrapperCssClass
			{
				get { return textEditorWrapperCssClass; }
			}

			public static string TextareaWrapperCssClass
			{
				get { return textareaWrapperCssClass; }
			}

			public static string ControlButtonsWrapperCssClass
			{
				get { return controlButtonsWrapperCssClass; }
			}

			public static string InputButtonType
			{
				get { return inputButtonType; }
			}

			public static string UpdateButtonId
			{
				get { return updateButtonId; }
			}

			public static string UpdateButtonText
			{
				get { return updateButtonText; }
			}

			public static string CancelButtonId
			{
				get { return cancelButtonId; }
			}

			public static string CancelButtonText
			{
				get { return cancelButtonText; }
			}

			public static string DeleteButtonId
			{
				get { return deleteButtonId; }
			}

			public static string DeleteButtonText
			{
				get { return deleteButtonText; }
			}

			public static string EditButtonId
			{
				get { return editButtonId; }
			}

			public static string EditButtonText
			{
				get { return editButtonText; }
			}

			public static string ConfirmationText
			{
				get { return confirmationText; }
			}

			public static string ConfirmationWrapperCssClass
			{
				get { return confirmationWrapperCssClass; }
			}

			public static string ConfirmationDialogTitlebarCssClass
			{
				get { return confirmationDialogTitlebarCssClass; }
			}

			public static string ConfirmationDialogTitleCssClass
			{
				get { return confirmationDialogTitleCssClass; }
			}

			public static string ConfirmationDialogContentCssClass
			{
				get { return confirmationDialogContentCssClass; }
			}

			public static string ConfirmationDialogContentWrapperCssClass
			{
				get { return confirmationDialogContentWrapperCssClass; }
			}

			public static string ConfirmationDialogContentAdContentCssClass
			{
				get { return confirmationDialogContentAdContentCssClass; }
			}

			public static string ConfirmationDialogButtonWrapperCssClass
			{
				get { return confirmationDialogButtonWrapperCssClass; }
			}

			public static string ConfirmationDialogNBorderCssClass
			{
				get { return confirmationDialogNBorderCssClass; }
			}

			public static string ConfirmationDialogSBorderCssClass
			{
				get { return confirmationDialogSBorderCssClass; }
			}

			public static string ConfirmationDialogEBorderCssClass
			{
				get { return confirmationDialogEBorderCssClass; }
			}

			public static string ConfirmationDialogWBorderCssClass
			{
				get { return confirmationDialogWBorderCssClass; }
			}

			public static string ConfirmationDialogNEBorderCssClass
			{
				get { return confirmationDialogNEBorderCssClass; }
			}

			public static string ConfirmationDialogSEBorderCssClass
			{
				get { return confirmationDialogSEBorderCssClass; }
			}

			public static string ConfirmationDialogSWBorderCssClass
			{
				get { return confirmationDialogSWBorderCssClass; }
			}

			public static string ConfirmationDialogNWBorderCssClass
			{
				get { return confirmationDialogNWBorderCssClass; }
			}

			public static string ConfirmationDialogContainerCssClass
			{
				get { return confirmationDialogContainerCssClass; }
			}

			public static string ReadonlyBodyTextArea
			{
				get { return readonlyBodyTextArea; }
			}

			public static string ReadonlyFooterTextArea
			{
				get { return readonlyFooterTextArea; }
			}

			public static string BodyTextArea
			{
				get { return bodyTextArea; }
			}

			public static string FooterTextArea
			{
				get { return footerTextId; }
			}

			public static string ClickStreamId
			{
				get { return clickStreamId; }
			}

			public static string ConfirmationFooterTextAreaId
			{
				get { return confirmationFooterTextAreaId; }
			}

			public static string ConfirmationTitle
			{
				get { return confirmationTitle; }
			}

			public static string ConfirmationBodyTextId
			{
				get { return confirmationBodyTextId; }
			}

		    public static string ReadonlyTextAreaCssClass
		    {
                get { return readonlyTextAreaCssClass; }
		    }

		    public static string EditButtonCssClass
		    {
		        get { return editButtonCssClass; }
		    }

			// CssClasses
			private const string textEditorWrapperCssClass = "ui-text-editor";
			private const string textareaWrapperCssClass = "ui-textarea";
			private const string controlButtonsWrapperCssClass = "ui-control-buttons";
		    private const string editButtonCssClass = "edit-button";

		    private const string readonlyTextAreaCssClass = "readonly";

			// ID Suffix
			private const string readonlyBodyTextArea = "readonly-textbody";
			private const string readonlyFooterTextArea = "readonly-textfooter";
			private const string bodyTextArea = "body";
			private const string footerTextId = "footer";

			private const string clickStreamId = "clickStream";

			// Buttons 
			private const string inputButtonType = "submit";
			private const string updateButtonId = "Update";
			private const string updateButtonText = "Update";
			private const string cancelButtonId = "Cancel";
			private const string cancelButtonText = "Cancel";
			private const string deleteButtonId = "Delete";
			private const string deleteButtonText = "Delete";
			private const string editButtonId = "Edit";
			private const string editButtonText = "Edit";

			// Confirmation
			private const string confirmationFooterTextAreaId = "Disclaimer";
			private const string confirmationWrapperCssClass = "ui-text-editor-confirmation ui-dialog";
			private const string confirmationDialogContainerCssClass = "ui-dialog-container";
			private const string confirmationDialogTitlebarCssClass = "ui-dialog-titlebar";
			private const string confirmationDialogTitleCssClass = "ui-dialog-title";
			private const string confirmationDialogContentCssClass = "ui-dialog-content";
			private const string confirmationDialogContentWrapperCssClass = "ui-dialog-content-wrapper";

			public static string ConfirmationDialogContentConfirmationTextCssClass
			{
				get { return confirmationDialogContentConfirmationTextCssClass; }
			}

			private const string confirmationDialogContentConfirmationTextCssClass = "ui-i-confirm-statement";
			private const string confirmationDialogContentAdContentCssClass = "ui-confirm-ad-text";
			private const string confirmationDialogButtonWrapperCssClass = "ui-dialog-buttonpane";
			private const string confirmationDialogNBorderCssClass = "ui-resizable-n ui-resizable-handle";
			private const string confirmationDialogSBorderCssClass = "ui-resizable-s ui-resizable-handle";
			private const string confirmationDialogEBorderCssClass = "ui-resizable-e ui-resizable-handle";
			private const string confirmationDialogWBorderCssClass = "ui-resizable-w ui-resizable-handle";
			private const string confirmationDialogNEBorderCssClass = "ui-resizable-ne ui-resizable-handle";
			private const string confirmationDialogSEBorderCssClass = "ui-resizable-se ui-resizable-handle";
			private const string confirmationDialogSWBorderCssClass = "ui-resizable-sw ui-resizable-handle";
			private const string confirmationDialogNWBorderCssClass = "ui-resizable-nw ui-resizable-handle";

			private const string confirmationTitle = "Internet Ad Saving Confirmation";
			private const string confirmationText =
				"To the best of my knowledge the information in this advertisement is correct.";

			public static string ConfirmationFooterTextLabel
			{
				get { return confirmationFooterTextLabel; }
			}

			private const string confirmationFooterTextLabel = "Disclaimer";

			private const string confirmationBodyTextId = "DocumentBody";

			public static string ReadonlyModeCssClass
			{
				get { return readonlyModeCssClass; }
			}

			public static string EditModeCssClass
			{
				get { return editModeCssClass; }
			}

			public static string ConfirmationModeCssClass
			{
				get { return confirmationModeCssClass; }
			}

			private const string readonlyModeCssClass = "readonly_mode";

			private const string editModeCssClass = "edit_mode";

			private const string confirmationModeCssClass = "confirmation_mode";

			public static string ConfirmationButtonCssClass
			{
				get { return confirmationButtonCssClass; }
			}

			private const string confirmationButtonCssClass = "button";
		}

		#endregion
	}
}
