using System;
using System.Collections;
using System.Web.UI;

namespace FirstLook.Pricing.WebControls
{
	public class ToolBarItemCollection : StateManagedCollection, IParserAccessor
	{
		private static readonly Type[] knownTypes = new Type[]
			{
				typeof (ToolBarButton),
				typeof (ToolBarDropDown)
			};

		protected override object CreateKnownType(int index)
		{
			switch (index)
			{
				case 0:
					return new ToolBarButton();
				case 1:
					return new ToolBarDropDown();
			}
			throw new ArgumentOutOfRangeException("index");
		}

		protected override Type[] GetKnownTypes()
		{
			return knownTypes;
		}

		protected override void SetDirtyObject(object o)
		{
			((ToolBarItem) o).SetDirty();
		}

        public void Add(ToolBarItem field)
        {
            ((IList)this).Add(field);
        }

        public bool Contains(ToolBarItem field)
        {
            return ((IList) this).Contains(field);
        }

        public int IndexOf(ToolBarItem field)
        {
            return ((IList) this).IndexOf(field);
        }

        public void Insert(int index, ToolBarItem field)
        {
            ((IList)this).Insert(index, field);
        }

        public void Remove(ToolBarItem field)
        {
            ((IList)this).Remove(field);
        }

        public void RemoveAt(int index)
        {
            ((IList)this).RemoveAt(index);
        }

		protected override void OnValidate(object o)
		{
			base.OnValidate(o);

			bool isToolBarButton = o is ToolBarButton;
			bool isToolBarDropDown = o is ToolBarDropDown;

			if (!(isToolBarButton || isToolBarDropDown))
			{
				throw new ArgumentException("DataControlFieldCollection_InvalidType");
			}
		}

		public ToolBarItem this[int index]
		{
			get
			{
				return this[index];
			}
		}

		public ToolBarItem FindItem(string name)
		{
			if (string.IsNullOrEmpty(name) || Count == 0)
			{
				return null;
			}
			
			foreach (ToolBarItem item in this)
			{
				if (string.Equals(item.Name, name, StringComparison.OrdinalIgnoreCase))
				{
					return item;
				}
				else
				{
					ToolBarDropDown drop = item as ToolBarDropDown;

					if (drop != null)
					{
						ToolBarItem dropItem = drop.DropDownItems.FindItem(name);

						if (dropItem != null)
						{
							return dropItem;
						}
					}
				}
			}

			return null;
		}

		#region IParserAccessor Members

		protected void AddParsedSubObject(object obj)
		{
			ToolBarButton button = obj as ToolBarButton;

			if (button != null)
			{
				Add(button);
			}
			else
			{
				ToolBarDropDown drop = obj as ToolBarDropDown;

				if (drop != null)
				{
					Add(drop);
				}
			}
		}

		void IParserAccessor.AddParsedSubObject(object obj)
		{
			AddParsedSubObject(obj);
		}

		#endregion
	}
}
