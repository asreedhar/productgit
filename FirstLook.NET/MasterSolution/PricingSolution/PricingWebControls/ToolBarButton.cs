using System;
using System.ComponentModel;
using System.Web.UI;

namespace FirstLook.Pricing.WebControls
{
	public class ToolBarButton : ToolBarItem
	{
		[DefaultValue(""), Localizable(true), Bindable(true), Category("Appearance"), Description("Fragment Name")]
		public string BodyText
		{
			get
			{
				string str = (string)ViewState["Body"];
				if (string.IsNullOrEmpty(str))
					return string.Empty;
				return str;
			}
			set
			{
				if (string.Compare(BodyText, value, StringComparison.InvariantCulture) != 0)
				{
					if (string.IsNullOrEmpty(value))
					{
						ViewState.Remove("Body");
					}
					else
					{
						ViewState["Body"] = value;
					}
				}
			}
		}

		[DefaultValue(""), Localizable(true), Bindable(true), Category("Behavior"), Description("Command Name")]
		public string CommandName
		{
			get
			{
				string str = (string)ViewState["CommandName"];
				if (string.IsNullOrEmpty(str))
					return string.Empty;
				return str;
			}
			set
			{
				if (string.Compare(CommandName, value, StringComparison.InvariantCulture) != 0)
				{
					if (string.IsNullOrEmpty(value))
					{
						ViewState.Remove("CommandName");
					}
					else
					{
						ViewState["CommandName"] = value;
					}
				}
			}
		}

		[DefaultValue(""), Localizable(true), Bindable(true), Category("Appearance"), Description("Fragment Name")]
		public string FooterText
		{
			get
			{
				string str = (string)ViewState["Footer"];
				if (string.IsNullOrEmpty(str))
					return string.Empty;
				return str;
			}
			set
			{
				if (string.Compare(FooterText, value, StringComparison.InvariantCulture) != 0)
				{
					if (string.IsNullOrEmpty(value))
					{
						ViewState.Remove("Footer");
					}
					else
					{
						ViewState["Footer"] = value;
					}
				}
			}
		}

		[DefaultValue(""), Themeable(false), Category("Behavior"), Description("Button_OnClientClick")]
		public virtual string OnClientClick
		{
			get
			{
				string str = (string)ViewState["OnClientClick"];
				if (str == null)
				{
					return string.Empty;
				}
				return str;
			}
			set
			{
				ViewState["OnClientClick"] = value;
			}
		}
	}
}
