Type.registerNamespace('FirstLook.Pricing.WebControls');

FirstLook.Pricing.WebControls.MarketReportTable = function(element) {
    FirstLook.Pricing.WebControls.MarketReportTable.initializeBase(this, [element]);
    this.delegates = {};
    var props = [
    "shopping_list_edit_class_name",
    "expando_class_name",
    "parent_row_class_name",
    "other_row_class_name",
    "vehicle_info_class_name",
    "other_row_parent_class_name",
    "expanded_value",
    "collapsed_value"
    ];
    for (var i = 0, l = props.length; i < l; i++) {
        this.create_getter_setter(props[i]);
    };
};

FirstLook.Pricing.WebControls.MarketReportTable.prototype = {
    initialize: function() {
        FirstLook.Pricing.WebControls.MarketReportTable.callBaseMethod(this, "initialize");
    },
    updated: function() {
        FirstLook.Pricing.WebControls.MarketReportTable.callBaseMethod(this, "updated");

        this.delegates["onShoppingListKeyDown"] = Function.createDelegate(this, this.onShoppingListKeyDown);
        this.delegates["onParentRowClick"] = Function.createDelegate(this, this.onParentRowClick);
        this.delegates["onOtherCellClick"] = Function.createDelegate(this, this.onOtherCellClick);

        var shopping_list_inputs = this.get_shopping_list_inputs();
        for (var i = 0, l = shopping_list_inputs.length; i < l; i++) {
            this.add_handler(shopping_list_inputs[i], "keyup", this.delegates["onShoppingListKeyDown"]);
        }

        var parent_rows = this.get_parent_rows();
        for (var i = 0, l = parent_rows.length; i < l; i++) {
            this.add_handler(parent_rows[i], "click", this.delegates["onParentRowClick"]);
        };
        
        var other_row_parents = this.get_other_row_parents();
        for (var i = 0, l = other_row_parents.length; i < l; i++) {
            this.add_handler(other_row_parents[i], "click", this.delegates["onOtherCellClick"]);
            this.apply_default_row_visibility(other_row_parents[i]);
        };

        if (shopping_list_inputs.length == 1) {
            shopping_list_inputs[0].focus();
            shopping_list_inputs[0].select();
        };        
    },
    dispose: function() {
        this.remove_handlers();

        this.delegates = undefined;

        FirstLook.Pricing.WebControls.MarketReportTable.callBaseMethod(this, "dispose");
    },
    get_shopping_list_inputs: function() {
        if ( !! !this._shopping_list_inputs) {            
            this._shopping_list_inputs = this.get_elements_with_class("input", this.get_shopping_list_edit_class_name());
        };
        return this._shopping_list_inputs;
    },
    onShoppingListKeyDown: function(ev) {
        if (ev.keyCode == Sys.UI.Key.enter) {
            var el = ev.target,
            td = this.find_parent_node(el, "td"),
            inputs = td.getElementsByTagName('input');

            for (var i = 0, l = inputs.length; i < l; i++) {
                if (inputs[i].type == "submit") {
                    inputs[i].click();
                }
            }
        }
    },
    get_parent_rows: function() {
    	if (! !!this._parent_rows) {
    		this._parent_rows = this.get_elements_with_class("tr", this.get_parent_row_class_name());
    	};

    	return this._parent_rows;
    },
    get_parent_cells: function() {
        if ( !! !this._parent_cells) {
            var el = this.get_element(),
            cells = el.getElementsByTagName('td'),
            className = this.get_parent_row_class_name(),
            cellCollection = [];
            for (var i = 0, l = cells.length; i < l; i++) {
                if (Sys.UI.DomElement.containsCssClass(cells[i].parentNode, className)) {
                    cellCollection.push(cells[i]);
                }
            };
            this._parent_cells = cellCollection;
        };
        return this._parent_cells;
    },
    get_other_row_parents: function() {
        if (!!!this._other_row_parents) {            
            this._other_row_parents = this.get_elements_with_class("tr", this.get_other_row_parent_class_name());
        };
        return this._other_row_parents;
    },
    get_elements_with_class: function(element_type, className) {
    	var el = this.get_element(),
    	els = el.getElementsByTagName(element_type),
        elCollection = [];
        for (var i = els.length - 1; i >= 0; i--) {
            if (els[i].className.indexOf(className) != -1) {
                elCollection.push(els[i]);
            };
        };
        return elCollection;
    },
    get_expando_for_row: function(row) {
        var tds = row.getElementsByTagName('td'),
        className = this.get_expando_class_name(),
        input,
        inputCollection = [];
        for (var i = tds.length - 1; i >= 0; i--) {
            if (Sys.UI.DomElement.containsCssClass(tds[i], className)) {
                inputCollection = tds[i].getElementsByTagName('input');
                if ( !! !inputCollection[0]) {
                    Error.create("input not found", inputCollection);
                } else {
                    input = inputCollection[0];
                    break;
                }
            }
        };
        return input;
    },
    apply_default_row_visibility: function(row) {
    	var expanded = this.get_expanded_value(),
    	expando = this.get_expando_for_row(row);
    	
    	this.hide_show_other_rows(row, this.get_other_row_class_name(), expando.value != expanded);
    },
    onParentRowClick: function(ev) {
        if ( !! ev.target && ev.target.nodeName != "INPUT" && ev.target.nodeName != "LABEL" && ev.target.nodeName != "A") {
            var tr = ev.target.nodeName == "TR" ? ev.target : this.find_parent_node(ev.target, "TR");
            if (Sys.UI.DomElement.containsCssClass(tr, this.get_parent_row_class_name())) {
                var input = this.get_expando_for_row(tr);
                input.click();
                ev.stopPropagation();
            }
        };
    },
    onOtherCellClick: function(ev) {
        if ( !! ev.target) {
            var tr = this.find_parent_node(ev.target, "TR"),
            td = ev.target.nodeName == "TD" ? ev.target: this.find_parent_node(ev.target, "TD"),
            other_class_name = this.get_other_row_class_name();
            
            if (tr.className.indexOf(this.get_other_row_parent_class_name()) != -1 &&
            	td.className.indexOf(this.get_vehicle_info_class_name()) != -1) {
                	this.toggle_other_equipment_rows(tr, other_class_name);
            };
        };
    },
    toggle_other_equipment_rows: function(row, className) {
        var should_hide = this.get_computed_style(row.nextSibling, "display") != "none";
        this.hide_show_other_rows(row, className, should_hide);
    },
    hide_show_other_rows: function(row, className, should_hide) {
    	var expando = this.get_expando_for_row(row);
    	row = row.nextSibling;
    	
    	if (!!expando){expando.value = should_hide ? this.get_collapsed_value() : this.get_expanded_value();}
    	
    	while ( !! row && row.className.indexOf(className) != -1) {
            // =============================================================================================
            // = We don't call toggle, as it is would need to recalculate for each row wether to hide/show =
            // =============================================================================================
            if (should_hide) {
                this.hide_row(row);
            } else {
                this.show_row(row);
            }
            row = row.nextSibling;
        }
    },
    hide_row: function(row) {
        row.style.display = "none";
    },
    show_row: function(row) {
        row.style.display = "";
    },
    // ==================
    // = Helper Methods =
    // ==================
    create_getter_setter: function(property) {
        // =================================================================
        // = Generic Function to create AJAX.net style getters and setters =
        // =================================================================
        if (this["get_" + property] === undefined) {
            this["get_" + property] = (function(thisProp) {
                return function get_property() {
                    return this[thisProp];
                };
            })("_" + property);
        };
        if (this["set_" + property] === undefined) {
            this["set_" + property] = (function(thisProp, raiseType) {
                return function set_property(value) {
                    if (value !== this[thisProp]) {
                        this[thisProp] = value;
                        this.raisePropertyChanged(raiseType);
                    };
                };
            })("_" + property, property);
        };
    },
    add_handler: function(el, type, fn) {
        if (this._eventCache === undefined) {
            this._eventCache = [];
        }
        this._eventCache.push([el, type, fn]);
        $addHandler(el, type, fn);
    },
    remove_handlers: function() {
        if (this._eventCache !== undefined) {
            for (var i = 0, l = this._eventCache.length; i < l; i++) {
                $removeHandler(this._eventCache[i][0], this._eventCache[i][1], this._eventCache[i][2]);
                this._eventCache[i][0] = undefined;
                this._eventCache[i][1] = undefined;
                this._eventCache[i][2] = undefined;
            };
            delete this._eventCache;
        };
    },
    get_computed_style: function(el, style) {
        if ( !! el.currentStyle) {
            return el.currentStyle[style];
        } else if ( !! document.defaultView && !!document.defaultView.getComputedStyle) {
            return document.defaultView.getComputedStyle(el, "")[style];
        } else {
            return el.style[style];
        }
    },
    find_parent_node: function(el, nodeName) {
        if ( !! !el.parentElement) {
            return null;
        } else if (el.parentElement.nodeName === nodeName.toUpperCase()) {
            return el.parentElement;
        } else {
            return this.find_parent_node(el.parentElement, nodeName);
        }
    },
    fire_event: function(el, ev) {
        if (document.createEvent) {
            var new_event = document.createEvent('HTMLEvents');
            new_event.initEvent(ev, true, false);

            el.dispatchEvent(new_event);
        }
        else if (document.createEventObject) {
            el.fireEvent('on' + ev);
        }
    },
    get_text_value: function(el) {
        return el.value || el.textContent || el.innerText;
    },
    set_text_value: function(el, text) {
        if (el.value) {
            el.value = text;
        } else if (el.textContent) {
            el.textContent = text;
        } else if (el.innerText) {
            el.innerText = text;
        };
    }
};

if (FirstLook.Pricing.WebControls.MarketReportTable.registerClass != undefined)
 FirstLook.Pricing.WebControls.MarketReportTable.registerClass('FirstLook.Pricing.WebControls.MarketReportTable', Sys.UI.Behavior);