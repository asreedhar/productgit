using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Drawing.Design;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Common.WebControls.Extenders;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search;

namespace FirstLook.Pricing.WebControls
{
    [DefaultEvent("SearchChanged"), DefaultProperty("SliderHandleImageUrl")]
    public class SearchDataView : DataBoundControl, INamingContainer
    {
        #region Properties

        private string _sliderHandleImageUrl = string.Empty;

        [DefaultValue(""),
         Editor("System.Web.UI.Design.ImageUrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor)),
         UrlProperty,
         SuppressMessage("Microsoft.Design", "CA1056:UriPropertiesShouldNotBeStrings", Justification = "Following ASP.NET pattern")]
        public string SliderHandleImageUrl
        {
            get { return _sliderHandleImageUrl; }
            set { _sliderHandleImageUrl = value; }
        }

        public string Distance
        {
            get
            {
                EnsureChildControls();

                return _searchDistanceTextBox.Text ?? string.Empty;
            }
        }

        public string MileageStart
        {
            get
            {
                EnsureChildControls();

                return _searchMileageStart.SelectedValue ?? string.Empty;
            }
        }

        public string MileageEnd
        {
            get
            {
                EnsureChildControls();

                return _searchMileageEnd.SelectedValue ?? string.Empty;
            }
        }

        public bool MatchCertified
        {
            get
            {
                EnsureChildControls();

                return _searchCertified.Checked;
            }
        }

        public bool MatchColor
        {
            get
            {
                EnsureChildControls();

                return _searchColor.Checked;
            }
        }

        //public bool PreviousYearFlag                              //31079
        //{
        //    get
        //    {
        //        EnsureChildControls();

        //        return _searchPreviousYear.Checked;
        //    }
        //}

        //public bool NextYearFlag                                      //31079
        //{
        //    get
        //    {
        //        EnsureChildControls();

        //        return _searchNextYear.Checked;
        //    }
        //}

        //public bool YearFlag                                          //31079
        //{
        //    get
        //    {
        //        EnsureChildControls();

        //        return _searchYear.Checked;
        //    }
        //}


        public override ControlCollection Controls
        {
            get
            {
                EnsureChildControls();

                return base.Controls;
            }
        }

        #endregion

        #region Events

        private static readonly object EventSearchChanged = new object();

        public event EventHandler SearchChanged
        {
            add { Events.AddHandler(EventSearchChanged, value); }
            remove { Events.RemoveHandler(EventSearchChanged, value); }
        }

        protected virtual void OnSearchChanged(EventArgs e)
        {
            EventHandler handler = (EventHandler)Events[EventSearchChanged];
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void ChildControlChanged(object sender, EventArgs e)
        {
            OnSearchChanged(e);
        }
        
        #endregion

        #region Methods

        protected override void CreateChildControls()
        {
            Controls.Clear();

            _searchDistanceTextBox = new TextBox();
            _searchDistanceTextBox.ID = SR.SearchDistanceTextBoxID;
            _searchDistanceTextBox.CssClass = SR.SearchDistanceTextBoxCssClass;
            _searchDistanceTextBox.TextChanged += ChildControlChanged;

            _searchDistanceSliderExtender = new SliderExtender();
            _searchDistanceSliderExtender.ID = SR.SearchDistanceSliderExtenderID;
            _searchDistanceSliderExtender.TargetControlID = SR.SearchDistanceTextBoxID;
            _searchDistanceSliderExtender.EnableHandleAnimation = true;
            _searchDistanceSliderExtender.RaiseChangeOnlyOnMouseUp = false;
            _searchDistanceSliderExtender.HandleImageUrl = SliderHandleImageUrl;
            _searchDistanceSliderExtender.HandleCssClass = SR.SearchDistanceSliderExtenderHandleCssClass;
            _searchDistanceSliderExtender.RailCssClass = SR.SearchDistanceSliderExtenderRailCssClass;
            _searchDistanceSliderExtender.Length = 125;

            _searchDistanceSliderLabelExtender = new SliderLabelExtender();
            _searchDistanceSliderLabelExtender.ID = SR.SearchDistanceSliderLabelExtenderID;
            _searchDistanceSliderLabelExtender.TargetControlID = SR.SearchDistanceTextBoxID;
            _searchDistanceSliderLabelExtender.LabelID = SR.SearchDistanceSliderLabelID;
            _searchDistanceSliderLabelExtender.LabelFormat = SR.SearchDistanceSliderLabelExtenderLabelFormat;

            _searchDistanceLabel = new Label();
            _searchDistanceLabel.ID = SR.SearchDistanceSliderLabelID;
            _searchDistanceLabel.Text = SR.SearchDistanceSliderLabelText;

            _searchMileageStart = new DropDownList();
            _searchMileageStart.ID = SR.MileageSelectsDropDownStartID;
            _searchMileageStart.DataTextField = SR.MileageSelectsDropDownDataTextField;
            _searchMileageStart.DataValueField = SR.MileageSelectsDropDownDataValueField;
            _searchMileageStart.DataTextFormatString = SR.MileageSelectsDropDownFormatString;
            _searchMileageStart.SelectedIndexChanged += ChildControlChanged;

            _searchMileageEnd = new DropDownList();
            _searchMileageEnd.ID = SR.MileageSelectsDropDownEndID;
            _searchMileageEnd.DataTextField = SR.MileageSelectsDropDownDataTextField;
            _searchMileageEnd.DataValueField = SR.MileageSelectsDropDownDataValueField;
            _searchMileageEnd.DataTextFormatString = SR.MileageSelectsDropDownFormatString;
            _searchMileageEnd.SelectedIndexChanged += ChildControlChanged;

            _searchMileageExtender = new DropDownListValueExtender();
            _searchMileageExtender.ID = SR.MileageSelectsDropDownValueExtenderID;
            _searchMileageExtender.TargetControlID = SR.MileageSelectsDropDownStartID;
            _searchMileageExtender.DropDownListID = SR.MileageSelectsDropDownEndID;

            _searchCertified = new CheckBox();
            _searchCertified.ID = SR.OtherMatchesCertifiedCheckboxID;
            _searchCertified.Text = SR.OtherMatchesCertifiedCheckboxText;
            _searchCertified.CheckedChanged += ChildControlChanged;

            _searchColor = new CheckBox();
            _searchColor.ID = SR.OtherMatchesColorCheckboxID;
            _searchColor.Text = SR.OtherMatchesColorCheckboxText;
            _searchColor.CheckedChanged += ChildControlChanged;

            //_searchPreviousYear = new CheckBox();                                 //31079
            //_searchPreviousYear.ID = SR.OtherPreviousYearCheckboxID;
            //_searchPreviousYear.CheckedChanged += ChildControlChanged;

            //_searchNextYear = new CheckBox();
            //_searchNextYear.ID = SR.OtherNextYearCheckboxID;
            //_searchNextYear.CheckedChanged += ChildControlChanged;

            //_searchYear = new CheckBox();
            //_searchYear.ID = SR.OtherYearCheckboxID;
            //_searchYear.CheckedChanged += ChildControlChanged;


            Controls.Add(_searchDistanceTextBox);
            Controls.Add(_searchDistanceSliderExtender);
            Controls.Add(_searchDistanceSliderLabelExtender);
            Controls.Add(_searchDistanceLabel);
            Controls.Add(_searchMileageStart);
            Controls.Add(_searchMileageEnd);
            Controls.Add(_searchMileageExtender);
            Controls.Add(_searchCertified);
            Controls.Add(_searchColor);
            //Controls.Add(_searchPreviousYear);                            //31079
            //Controls.Add(_searchNextYear);
            //Controls.Add(_searchYear);                                                
            
        }

        protected override void PerformDataBinding(IEnumerable data)
        {
            EnsureChildControls();

            IEnumerator en = data.GetEnumerator();

            if (en.MoveNext())
            {
                Search search = (Search) en.Current;

                _searchDistanceTextBox.Text = search.Distance.Id.ToString(CultureInfo.InvariantCulture);

                _searchDistanceSliderExtender.Minimum = search.Distances[0].Id;
                _searchDistanceSliderExtender.Maximum = search.Distances[search.Distances.Count - 1].Id;
                _searchDistanceSliderExtender.Steps = search.Distances.Count;

                foreach (Distance distance in search.Distances)
                {
                    _searchDistanceSliderLabelExtender.ValueMappings.Add(
                        distance.Id.ToString(CultureInfo.InvariantCulture),
                        distance.Value.ToString(CultureInfo.InvariantCulture));
                }

                _searchMileageStart.DataSource = search.Mileages;
                _searchMileageStart.DataBind();
                _searchMileageStart.SelectedIndex = search.Mileages.IndexOf(search.LowMileage);

                _searchMileageEnd.DataBound += SearchMileageEnd_DataBound;
                _searchMileageEnd.DataSource = search.Mileages;
                _searchMileageEnd.DataBind();

                Mileage mileage = search.HighMileage;

                if (mileage != null)
                {
                    _searchMileageEnd.SelectedValue = (mileage.Value + 1).ToString();
                }
                else
                {
                    _searchMileageEnd.SelectedIndex = _searchMileageEnd.Items.Count - 1;
                }

                if (_searchMileageEnd.SelectedIndex < _searchMileageStart.SelectedIndex)
                {
                    _searchMileageEnd.SelectedIndex = _searchMileageStart.SelectedIndex;
                }

                _searchCertified.Checked = search.MatchCertified;

                _searchColor.Checked = search.MatchColor;

                //_searchPreviousYear.Checked = Convert.ToBoolean(search.PreviousYear);                 //31079

                //_searchPreviousYear.Text = (search.ModelYear - 1).ToString();

                //_searchNextYear.Checked = Convert.ToBoolean(search.NextYear);

                //_searchNextYear.Text = (search.ModelYear + 1).ToString();

                //_searchYear.Checked = true;

                //_searchYear.Enabled = false;

                //_searchYear.Text = search.ModelYear.ToString();   
            }
        }

        private void SearchMileageEnd_DataBound(object sender, EventArgs e)
        {
            ListItem unlimitedListItem = new ListItem("Unlimited", "max");

            if (!_searchMileageEnd.Items[_searchMileageEnd.Items.Count - 1].Equals(unlimitedListItem))
            {
                _searchMileageEnd.Items.RemoveAt(0);

                _searchMileageEnd.Items.Add(unlimitedListItem);
            }
        }

        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            CssClass = string.IsNullOrEmpty(CssClass) ? SR.ControlCssClass : CssClass + " " + SR.ControlCssClass;

            base.AddAttributesToRender(writer);
        }

        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Div;
            }
        }

        protected override string TagName
        {
            get
            {
                return "div";
            }
        }

        protected override void RenderChildren(HtmlTextWriter writer)
        {
            RenderRangeSlider(writer);

            RenderMileageSelects(writer);

            RenderOtherMatches(writer);

            //RenderYearsMatches(writer);                                   //31079      
        }

        private void RenderRangeSlider(HtmlTextWriter writer)
        {
            // TODO: Review why this is an id

            writer.AddAttribute(HtmlTextWriterAttribute.Id, SR.SearchDistanceWrapperID);

            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.RenderBeginTag(HtmlTextWriterTag.Span);

            writer.Write(SR.SearchDistancePreText);

            writer.RenderEndTag(); // Span

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.SearchDistanceInnerWrapperCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            _searchDistanceTextBox.RenderControl(writer);

            _searchDistanceSliderExtender.RenderControl(writer);

            _searchDistanceSliderLabelExtender.RenderControl(writer);
            
            writer.RenderEndTag(); // Div

            _searchDistanceLabel.RenderControl(writer);

            writer.RenderEndTag(); // Div
        }

        private void RenderMileageSelects(HtmlTextWriter writer)
        {
            // TODO: Review why this is an id

            writer.AddAttribute(HtmlTextWriterAttribute.Id, SR.MileageSelectsWrapperID);

            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.RenderBeginTag(HtmlTextWriterTag.Label);

            writer.Write(SR.MileageSelectsPreText);
            
            _searchMileageStart.RenderControl(writer);
            
            writer.RenderEndTag(); // Label

            writer.RenderBeginTag(HtmlTextWriterTag.Label);

            writer.Write(SR.MileageSelectsMidText);

            _searchMileageEnd.RenderControl(writer);

            _searchMileageExtender.RenderControl(writer);

            writer.RenderEndTag(); // Label

            writer.RenderEndTag(); // Div
        }

        private void RenderOtherMatches(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.OtherMatchesWrapperCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Span);

            _searchCertified.RenderControl(writer);

            _searchColor.RenderControl(writer);

            writer.RenderEndTag(); // Span
        }

        //private void RenderYearsMatches(HtmlTextWriter writer)
        //{
        //    writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.OtherYearsMatchesWrapperCssClass);

        //    writer.RenderBeginTag(HtmlTextWriterTag.Div);

        //    _searchPreviousYear.RenderControl(writer);

        //    writer.Write("&nbsp;&nbsp;&nbsp;&nbsp;");

        //    _searchYear.RenderControl(writer);

        //    writer.Write("&nbsp;&nbsp;");

        //    _searchNextYear.RenderControl(writer);

        //    writer.RenderEndTag(); // Div
        //}



        #endregion

        #region Controls

        private TextBox _searchDistanceTextBox;
        private SliderExtender _searchDistanceSliderExtender;
        private SliderLabelExtender _searchDistanceSliderLabelExtender;
        private Label _searchDistanceLabel;
        private DropDownList _searchMileageStart;
        private DropDownList _searchMileageEnd;
        private DropDownListValueExtender _searchMileageExtender;
        private CheckBox _searchCertified;
        private CheckBox _searchColor;
        //private CheckBox _searchPreviousYear;                                 //31079
        //private CheckBox _searchNextYear;
        //private CheckBox _searchYear;                                   


        #endregion

        #region IStateManager

        protected override void LoadViewState(object savedState)
        {
            if (savedState != null)
            {
                Pair state = savedState as Pair;

                if (state != null)
                {
                    base.LoadViewState(state.First);

                    _sliderHandleImageUrl = state.Second as string ?? string.Empty;
                }
                else
                {
                    base.LoadViewState(savedState);
                }
            }
        }

        protected override object SaveViewState()
        {
            return new Pair(base.SaveViewState(), _sliderHandleImageUrl);
        }

        #endregion

        #region InnerClasses

        private static class SR
        {
            #region General

            public static string ControlCssClass
            {
                get { return _controlCssClass; }
            }

            private const string _controlCssClass = "search_data_view";
            #endregion

            #region Search Distance

            public static string SearchDistanceWrapperID
            {
                get { return _searchDistanceWrapperID; }
            }

            public static string SearchDistancePreText
            {
                get { return _searchDistancePreText; }
            }

            public static string SearchDistanceInnerWrapperCssClass
            {
                get { return _searchDistanceInnerWrapperCssClass; }
            }

            public static string SearchDistanceTextBoxID
            {
                get { return _searchDistanceTextBoxID; }
            }

            public static string SearchDistanceTextBoxCssClass
            {
                get { return _searchDistanceTextBoxCssClass; }
            }

            public static string SearchDistanceSliderExtenderID
            {
                get { return _searchDistanceSliderExtenderID; }
            }

            public static string SearchDistanceSliderExtenderHandleCssClass
            {
                get { return _searchDistanceSliderExtenderHandleCssClass; }
            }

            public static string SearchDistanceSliderExtenderRailCssClass
            {
                get { return _searchDistanceSliderExtenderRailCssClass; }
            }

            public static string SearchDistanceSliderLabelExtenderID
            {
                get { return _searchDistanceSliderLabelExtenderID; }
            }

            public static string SearchDistanceSliderLabelExtenderLabelFormat
            {
                get { return _searchDistanceSliderLabelExtenderLabelFormat; }
            }

            public static string SearchDistanceSliderLabelID
            {
                get { return _searchDistanceSliderLabelID; }
            }

            public static string SearchDistanceSliderLabelText
            {
                get { return _searchDistanceSliderLabelText; }
            }

            private const string _searchDistanceWrapperID = "search-ctrls-distance-slider";
            private const string _searchDistancePreText = "Within:";
            private const string _searchDistanceInnerWrapperCssClass = "SearchDistanceSlider";

            private const string _searchDistanceTextBoxID = "SearchDistanceTextBox";
            private const string _searchDistanceTextBoxCssClass = "hidden";

            private const string _searchDistanceSliderExtenderID = "SearchDistanceSliderExtender";
            private const string _searchDistanceSliderExtenderHandleCssClass = "ui-slider-handle";
            private const string _searchDistanceSliderExtenderRailCssClass = "ui-slider-1";

            private const string _searchDistanceSliderLabelExtenderID = "SearchDistanceSliderLabelExtender";
            private const string _searchDistanceSliderLabelExtenderLabelFormat = "{0} miles";

            private const string _searchDistanceSliderLabelID = "SearchDistanceSliderLabel";
            private const string _searchDistanceSliderLabelText = "miles"; 
            #endregion

            #region Mileage Distance

            public static string MileageSelectsWrapperID
            {
                get { return _mileageSelectsWrapperID; }
            }

            public static string MileageSelectsPreText
            {
                get { return _mileageSelectsPreText; }
            }

            public static string MileageSelectsMidText
            {
                get { return _mileageSelectsMidText; }
            }

            public static string MileageSelectsDropDownStartID
            {
                get { return _mileageSelectsDropDownStartID; }
            }

            public static string MileageSelectsDropDownEndID
            {
                get { return _mileageSelectsDropDownEndID; }
            }

            public static string MileageSelectsDropDownDataTextField
            {
                get { return _mileageSelectsDropDownDataTextField; }
            }

            public static string MileageSelectsDropDownDataValueField
            {
                get { return _mileageSelectsDropDownDataValueField; }
            }

            public static string MileageSelectsDropDownFormatString
            {
                get { return _mileageSelectsDropDownFormatString; }
            }

            public static string MileageSelectsDropDownValueExtenderID
            {
                get { return _mileageSelectsDropDownValueExtenderID; }
            }

            private const string _mileageSelectsWrapperID = "search-modify-mileage";
            private const string _mileageSelectsPreText = "Mileage:";
            private const string _mileageSelectsMidText = "to:";

            private const string _mileageSelectsDropDownStartID = "SearchMileageStart";
            private const string _mileageSelectsDropDownEndID = "SearchMileageEnd";
            private const string _mileageSelectsDropDownDataTextField = "Value";
            private const string _mileageSelectsDropDownDataValueField = "Value";
            private const string _mileageSelectsDropDownFormatString = "{0:###,##0}";

            private const string _mileageSelectsDropDownValueExtenderID = "SearchMileageExtender";

            #endregion

            #region Other Matches

            public static string OtherMatchesWrapperCssClass
            {
                get { return _otherMatchesWrapperCssClass; }
            }

            //public static string OtherYearsMatchesWrapperCssClass                 //31079
            //{
            //    get { return _otheryearsMatchesWrapperCssClass; }
            //} 

            public static string OtherMatchesCertifiedCheckboxID
            {
                get { return _otherMatchesCertifiedCheckboxID; }
            }

            public static string OtherMatchesCertifiedCheckboxText
            {
                get { return _otherMatchesCertifiedCheckboxText; }
            }

            public static string OtherMatchesColorCheckboxID
            {
                get { return _otherMatchesColorCheckboxID; }
            }

            public static string OtherMatchesColorCheckboxText
            {
                get { return _otherMatchesColorCheckboxText; }
            }

            //public static string OtherPreviousYearCheckboxID                          //31079
            //{
            //    get { return _otherPreviousYearCheckboxID; }
            //}

            //public static string OtherNextYearCheckboxID
            //{
            //    get { return _otherNextYearCheckboxID; }
            //}

            //public static string OtherYearCheckboxID
            //{
            //    get { return _otherYearCheckboxID; }
            //} 

            private const string _otherMatchesWrapperCssClass = "other_matches";
            //private const string _otheryearsMatchesWrapperCssClass = "other_years_matches";       //31079
            private const string _otherMatchesCertifiedCheckboxID = "MatchCertifiedCheckbox";
            private const string _otherMatchesCertifiedCheckboxText = "match certified";
            private const string _otherMatchesColorCheckboxID = "MatchColorCheckbox";
            private const string _otherMatchesColorCheckboxText = "match color";
            //private const string _otherPreviousYearCheckboxID = "PreviousYearCheckbox";           //31079
            //private const string _otherYearCheckboxID = "YearCheckbox";
            //private const string _otherNextYearCheckboxID = "NextYearCheckbox";                     


            #endregion
        }

        #endregion        
    }

}
