using System;

namespace FirstLook.Pricing.WebControls
{
    public class OrderedTableViewSelectedChangedEventArgs : EventArgs
    {
        private readonly int _id;

        public OrderedTableViewSelectedChangedEventArgs(int id)
        {
            _id = id;
        }

        public int Id
        {
            get { return _id; }
        }
    }
}