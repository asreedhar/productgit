using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing.Design;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Utilities;
using FirstLook.Pricing.DomainModel.Presentation;

[assembly: WebResource("FirstLook.Pricing.WebControls.OrderedTableView.debug.js", "text/javascript")]
[assembly: WebResource("FirstLook.Pricing.WebControls.OrderedTableView.js", "text/javascript")]

namespace FirstLook.Pricing.WebControls
{
    public class OrderedTableView : DataBoundControl, IPostBackDataHandler, IScriptControl, ITableView
    {
        public OrderedTableView()
        {
            _presenter = new TableViewPresenter(this);
        }

        #region Events

        private static readonly object EventSelectedChanged = new object();
        private static readonly object EventUpdated = new object();

        public event EventHandler<OrderedTableViewSelectedChangedEventArgs> SelectedChanged
        {
            add
            {
                Events.AddHandler(EventSelectedChanged, value);
            }
            remove
            {
                Events.RemoveHandler(EventSelectedChanged, value);
            }
        }

        protected virtual void OnSelectedChanged(ITableRow row)
        {
            EventHandler<OrderedTableViewSelectedChangedEventArgs> handler = (EventHandler<OrderedTableViewSelectedChangedEventArgs>)Events[EventSelectedChanged];

            if (handler != null)
            {
                handler(this, new OrderedTableViewSelectedChangedEventArgs(row.Id));
            }
        }

        public event EventHandler<OrderedTableViewUpdatedEventArgs> Updated
        {
            add
            {
                Events.AddHandler(EventUpdated, value);
            }
            remove
            {
                Events.RemoveHandler(EventUpdated, value);
            }
        }

        protected virtual void OnUpdated(OrderedTableViewUpdatedEventArgs e)
        {
            EventHandler<OrderedTableViewUpdatedEventArgs> handler = (EventHandler<OrderedTableViewUpdatedEventArgs>) Events[EventUpdated];

            if (handler != null)
            {
                handler(this, e);
            }
        }

        #endregion

        #region Properties

        private readonly TableViewPresenter _presenter;

        public TableViewPresenter Presenter
        {
            get { return _presenter; }
        }

        private OrderedTableRowCollection _items;

        public ITableRowCollection Items
        {
            get
            {
                if (_items == null)
                {
                    _items = new OrderedTableRowCollection();

                    if (IsTrackingViewState)
                    {
                        ((IStateManager) _items).TrackViewState();
                    }
                }

                return _items;
            }
        }

        #endregion

        #region Data Binding Ugliness

        private DataKey _dataKey;
        private string[] _dataKeyNames;
        private OrderedDictionary _keyTable;

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Description("DetailsView_DataKey")]
        public virtual DataKey DataKey
        {
            get
            {
                if (_dataKey == null)
                {
                    _dataKey = new DataKey(KeyTable);
                }
                return _dataKey;
            }
        }

        [Description("DataControls_DataKeyNames"), DefaultValue((string)null), Category("Data")]
        [Editor("System.Web.UI.Design.WebControls.DataFieldEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor)), TypeConverter(typeof(StringArrayConverter))]
        public virtual string[] DataKeyNames
        {
            get
            {
                if (_dataKeyNames != null)
                {
                    return (string[])(_dataKeyNames).Clone();
                }
                return new string[0];
            }
            set
            {
                if (!StringHelper.CompareStringArrays(value, DataKeyNamesInternal))
                {
                    if (value != null)
                    {
                        _dataKeyNames = (string[])value.Clone();
                    }
                    else
                    {
                        _dataKeyNames = null;
                    }
                    _keyTable = null;
                    if (Initialized)
                    {
                        RequiresDataBinding = true;
                    }
                }
            }
        }

        private string[] DataKeyNamesInternal
        {
            get
            {
                if (_dataKeyNames != null)
                {
                    return _dataKeyNames;
                }
                return new string[0];
            }
        }

        private OrderedDictionary KeyTable
        {
            get
            {
                if (_keyTable == null)
                {
                    _keyTable = new OrderedDictionary(DataKeyNamesInternal.Length);
                }
                return _keyTable;
            }
        }

        #endregion

        #region Data Bound Control Overrides

        protected override void PerformDataBinding(IEnumerable data)
        {
            Items.Clear();

            KeyTable.Clear();

            if (data != null)
            {
                ExtractKeyTable(data); // keys of the from the collection

                IEnumerator en = data.GetEnumerator();

                while (en.MoveNext())
                {
                    object value = en.Current;

                    OrderedTableRow item = new OrderedTableRow();

                    item.Id = (int) DataBinder.GetPropertyValue(value, "Id");
                    item.Name = (string) DataBinder.GetPropertyValue(value, "Name");
                    item.Selected = (bool) DataBinder.GetPropertyValue(value, "Selected");
                    item.Enabled = true; // (bool) DataBinder.GetPropertyValue(value, "Enabled");

                    Items.Add(item);
                }
            }
        }

        protected override void LoadViewState(object savedState)
        {
            Pair pair = savedState as Pair;

            if (pair == null)
            {
                base.LoadViewState(savedState);
            }
            else
            {
                base.LoadViewState(pair.First);

                ((IStateManager) Items).LoadViewState(pair.Second);
            }
        }

        protected override object SaveViewState()
        {
            if (_items == null)
            {
                return new Pair(base.SaveViewState(), null);
            }

            return new Pair(base.SaveViewState(), ((IStateManager) Items).SaveViewState());
        }

        private void ExtractKeyTable(object dataItem)
        {
            string[] dataKeyNamesInternal = DataKeyNamesInternal;

            if (dataKeyNamesInternal.Length != 0)
            {
                OrderedDictionary keyTable = KeyTable;

                foreach (string str in dataKeyNamesInternal)
                {
                    object propertyValue = DataBinder.GetPropertyValue(dataItem, str);

                    keyTable.Add(str, propertyValue);
                }

                _dataKey = new DataKey(keyTable);
            }
        }

        #endregion

        #region Render

        protected override HtmlTextWriterTag TagKey
        {
            get { return HtmlTextWriterTag.Div; }
        }

        protected override string TagName
        {
            get { return "div"; }
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            // editor buttons

            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            NameValueCollection saveButtonAdditionalAttr = new NameValueCollection();
            saveButtonAdditionalAttr.Add("style", "display:none;");
            saveButtonAdditionalAttr.Add("class", "featuresSaveBtn");
            Html.RenderButton(writer, Html.ExternalId(UniqueID, IdSeparator, "Save"), "Save", true, saveButtonAdditionalAttr);

            writer.RenderEndTag();

            // table

            writer.RenderBeginTag(HtmlTextWriterTag.Table);

            // thead, th, td x 6

            string[] columnNames = new string[] {"Selected", "Name", "Up", "Down"};

            writer.RenderBeginTag(HtmlTextWriterTag.Thead);
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            foreach (string columnName in columnNames)
            {
                Html.RenderHeaderTextCell(writer, columnName, columnName.Trim().Replace(" ", ""));
            }

            writer.RenderEndTag();
            writer.RenderEndTag();

            // tbody, tr, td x 6

            writer.RenderBeginTag(HtmlTextWriterTag.Tbody);

            for (int i = 0, l = Items.Count; i < l; i++)
            {
                ITableRow item = Items[i];

                writer.AddAttribute(HtmlTextWriterAttribute.Id, Html.ExternalId(UniqueID, IdSeparator, item.Id));

                writer.RenderBeginTag(HtmlTextWriterTag.Tr);

                writer.RenderBeginTag(HtmlTextWriterTag.Td); // Selected

                Html.RenderCheckBox(writer, Html.ExternalId(UniqueID, IdSeparator, item.Id, "Selected"), item.Selected);


                Html.RenderHiddenField(writer, Html.ExternalId(UniqueID, IdSeparator, item.Id, "Rank"),
                                       i.ToString(CultureInfo.InvariantCulture));

                writer.RenderEndTag();

                Html.RenderTextCell(writer, item.Name, "name"); // Name

                writer.WriteLine();

                Html.RenderCell(writer, Html.ExternalId(UniqueID, IdSeparator, item.Id, "Up"), "Up", i > 0,
                                Html.RenderButton, "Cell_Up"); // Up

                Html.RenderCell(writer, Html.ExternalId(UniqueID, IdSeparator, item.Id, "Down"), "Down", i + 1 != l,
                                Html.RenderButton, "Cell_Down"); // Down

                writer.RenderEndTag();
            }

            writer.RenderEndTag(); // tbody
            writer.RenderEndTag(); // table
        }

        #endregion

        #region IPostBackDataHandler Members

        public override Control FindControl(string id)
        {
            return this;
        }

        protected override Control FindControl(string id, int pathOffset)
        {
            return this;
        }

        private bool _changed;

        private bool _raisedChanged;

        private bool _save;

        private bool _inspectCheckBoxes = true;

        protected bool LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
            if (!IsEnabled)
            {
                return false;
            }

            if (!postDataKey.StartsWith(UniqueID, StringComparison.InvariantCulture))
            {
                return false;
            }

            int idOffset = UniqueID.Length + 1;

            string controlId = postDataKey.Substring(idOffset);

            string controlValue = postCollection[postDataKey];

            if ("Save".Equals(controlId, StringComparison.InvariantCulture))
            {
                _save = _changed = true;
            }
            else
            {
                int commandOffset = controlId.IndexOf(IdSeparator);

                if (commandOffset > 0)
                {
                    string command = controlId.Substring(commandOffset + 1);

                    int id;

                    if (int.TryParse(controlId.Substring(0, commandOffset), out id))
                    {
                        switch (command)
                        {
                            case "Up":
                                _changed |= Presenter.MoveUp(id);
                                break;
                            case "Down":
                                _changed |= Presenter.MoveDown(id);
                                break;
                            case "Rank":
                                _changed |= Presenter.SetRank(id, int.Parse(controlValue));
                                break;
                        }
                    }
                }
            }

            if (_inspectCheckBoxes)
            {
                foreach (ITableRow item in Items)
                {
                    string id = Html.ExternalId(UniqueID, IdSeparator, item.Id, "Selected");

                    string value = postCollection[id];

                    _changed |= Presenter.SetSelected(item.Id, value != null);
                }

                _inspectCheckBoxes = false;
            }

            if (_raisedChanged)
            {
                return false;
            }

            if (_changed)
            {
                return (_raisedChanged = _changed);
            }

            return false;
        }

        protected void RaisePostDataChangedEvent()
        {
            foreach (OrderedTableRow item in Items)
            {
                if (item.HasSelectedChanged)
                {
                    OnSelectedChanged(item);
                }
            }

            if (_save)
            {
                HandleUpdate();
            }
        }

        bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
            return LoadPostData(postDataKey, postCollection);
        }

        void IPostBackDataHandler.RaisePostDataChangedEvent()
        {
            RaisePostDataChangedEvent();
        }

        #endregion

        #region Handle Events

        private void HandleUpdate()
        {
            if (IsBoundUsingDataSourceID)
            {
                OrderedDictionary values = new OrderedDictionary();

                IList<ITableRow> items = new List<ITableRow>();

                foreach (OrderedTableRow item in Items)
                {
                    items.Add(item);
                }

                values["values"] = items;

                DataSourceView view = GetDataSource().GetView(DataMember ?? string.Empty);

                view.Update(DataKey.Values, values, null, HandleUpdateCallback);
            }
        }

        private bool HandleUpdateCallback(int affectedRows, Exception ex)
        {
            OrderedTableViewUpdatedEventArgs e = new OrderedTableViewUpdatedEventArgs(affectedRows, ex);

            OnUpdated(e);

            if ((ex != null) && !e.ExceptionHandled)
            {
                return false;
            }

            RequiresDataBinding = true;
            
            return true;
        }

        #endregion

        #region IScriptControl Members

        private ScriptManager _manager;

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (!DesignMode)
            {
                _manager = ScriptManager.GetCurrent(Page);

                if (_manager != null)
                {
                    _manager.RegisterScriptControl(this);
                }
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);

            if (!DesignMode)
            {
                if (_manager != null)
                {
                    _manager.RegisterScriptDescriptors(this);
                }
            }
        }

        IEnumerable<ScriptDescriptor> IScriptControl.GetScriptDescriptors()
        {
            if (_manager == null)
            {
                return new ScriptDescriptor[0];
            }

            List<ScriptDescriptor> descriptors = new List<ScriptDescriptor>();

            ScriptBehaviorDescriptor table =
                new ScriptBehaviorDescriptor("FirstLook.Pricing.WebControls.OrderedTableView", ClientID);

            table.ID = ClientID;

            descriptors.Add(table);

            foreach (ITableRow item in Items)
            {
                string rowId = Html.ExternalId(UniqueID, IdSeparator, item.Id);

                string inputPrefix = rowId + IdSeparator;

                ScriptBehaviorDescriptor row = new ScriptBehaviorDescriptor("FirstLook.Pricing.WebControls.OrderedTableRow", rowId);
                row.AddComponentProperty("Editor", ClientID);
                row.AddElementProperty("UpButton", inputPrefix + "Up");
                row.AddElementProperty("DownButton", inputPrefix + "Down");
                row.AddElementProperty("RankField", inputPrefix + "Rank");

                descriptors.Add(row);
            }

            return descriptors;
        }

        IEnumerable<ScriptReference> IScriptControl.GetScriptReferences()
        {
            if (_manager == null)
            {
                return new ScriptReference[0];
            }

            return new ScriptReference[]
                       {
                           new ScriptReference("FirstLook.Pricing.WebControls.OrderedTableView.js",
                                               "FirstLook.Pricing.WebControls")
                       };
        }

        #endregion
    }
}