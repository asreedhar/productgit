using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI;
using FirstLook.Pricing.DomainModel.Presentation;

namespace FirstLook.Pricing.WebControls
{
    public class OrderedTableRowCollection : StateManagedCollection, ITableRowCollection
    {
        #region ITableRowCollection Members

        public ITableRow NewItem()
        {
            return new OrderedTableRow();
        }

        public int IndexOf(int id)
        {
            for (int i = 0; i < Count; i++)
            {
                if (((IList<ITableRow>) this)[i].Id == id)
                {
                    return i;
                }
            }

            return -1;
        }

        public ITableRow Find(int id)
        {
            int index = IndexOf(id);

            return (index == -1) ? null : ((IList<ITableRow>) this)[index];
        }

        #endregion

        private static readonly Type[] KnownTypes = new Type[] {typeof (OrderedTableRow)};

        protected override object CreateKnownType(int index)
        {
            switch (index)
            {
                case 0:
                    return new OrderedTableRow();
            }
            throw new ArgumentOutOfRangeException("index");
        }

        protected override Type[] GetKnownTypes()
        {
            return KnownTypes;
        }

        protected override void OnValidate(object o)
        {
            base.OnValidate(o);

            if (!(o is OrderedTableRow))
            {
                throw new ArgumentException("DataControlFieldCollection_InvalidType");
            }
        }

        protected override void SetDirtyObject(object o)
        {
            // do nothing
        }

        #region IList<ITableRow> Members

        int IList<ITableRow>.IndexOf(ITableRow item)
        {
            return ((IList) this).IndexOf(item);
        }

        void IList<ITableRow>.Insert(int index, ITableRow item)
        {
            ((IList) this).Insert(index, item);
        }

        void IList<ITableRow>.RemoveAt(int index)
        {
            ((IList) this).RemoveAt(index);
        }

        ITableRow IList<ITableRow>.this[int index]
        {
            get { return (ITableRow) (((IList) this)[index]); }
            set { (((IList) this)[index]) = value; }
        }

        #endregion

        #region ICollection<ITableRow> Members

        void ICollection<ITableRow>.Add(ITableRow item)
        {
            ((IList) this).Add(item);
        }

        void ICollection<ITableRow>.Clear()
        {
            ((IList) this).Clear();
        }

        bool ICollection<ITableRow>.Contains(ITableRow item)
        {
            return ((IList) this).Contains(item);
        }

        void ICollection<ITableRow>.CopyTo(ITableRow[] array, int arrayIndex)
        {
            ((IList) this).CopyTo(array, arrayIndex);
        }

        int ICollection<ITableRow>.Count
        {
            get { return ((IList) this).Count; }
        }

        bool ICollection<ITableRow>.IsReadOnly
        {
            get { return false; }
        }

        bool ICollection<ITableRow>.Remove(ITableRow item)
        {
            bool contains = ((IList) this).Contains(item);
            ((IList) this).Remove(item);
            return contains;
        }

        #endregion

        #region IEnumerable<ITableRow> Members

        IEnumerator<ITableRow> IEnumerable<ITableRow>.GetEnumerator()
        {
            return new List<ITableRow>(this).GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}