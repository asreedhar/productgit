using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web.UI;
using FirstLook.Pricing.DomainModel.Market;

namespace FirstLook.Pricing.WebControls
{
    internal class MarketReportItemCollection : StateManagedCollection, IList<IReportItem>
    {
        private static readonly Type[] KnownTypes = new Type[]
			{
				typeof (MarketReportItem)
			};

        protected override object CreateKnownType(int index)
        {
            switch (index)
            {
                case 0:
                    return new MarketReportItem();
            }
            throw new ArgumentOutOfRangeException("index");
        }

        protected override Type[] GetKnownTypes()
        {
            return KnownTypes;
        }

        protected override void SetDirtyObject(object o)
        {

        }

        public void Add(MarketReportItem field)
        {
            ((IList)this).Add(field);
        }

        public bool Contains(MarketReportItem field)
        {
            return ((IList)this).Contains(field);
        }

        public int IndexOf(MarketReportItem field)
        {
            return ((IList)this).IndexOf(field);
        }

        public void Insert(int index, MarketReportItem field)
        {
            ((IList)this).Insert(index, field);
        }

        public void Remove(MarketReportItem field)
        {
            ((IList)this).Remove(field);
        }

        public void RemoveAt(int index)
        {
            ((IList)this).RemoveAt(index);
        }

        protected override void OnValidate(object o)
        {
            base.OnValidate(o);

            bool isToolBar = o is MarketReportItem;

            if (!isToolBar)
            {
                throw new ArgumentException("DataControlFieldCollection_InvalidType");
            }
        }

        public MarketReportItem this[int index]
        {
            get
            {
                return (MarketReportItem)((IList)this)[index];
            }
        }

        #region Implementation of IList<IReportItem>

        int IList<IReportItem>.IndexOf(IReportItem item)
        {
            for (int i = 0; i < Count; i++)
            {
                if (((IReportItem)this[i]).Id == item.Id)
                    return i;
            }

            return -1;
        }

        void IList<IReportItem>.Insert(int index, IReportItem item)
        {
            throw new NotSupportedException();
        }

        IReportItem IList<IReportItem>.this[int index]
        {
            get { return this[index]; }
            set { throw new NotSupportedException(); }
        }

        #endregion

        #region IList<IReportItem> Members


        void IList<IReportItem>.RemoveAt(int index)
        {
            throw new NotSupportedException();
        }

        #endregion

        #region ICollection<IReportItem> Members

        void ICollection<IReportItem>.Add(IReportItem item)
        {
            throw new NotSupportedException();
        }

        void ICollection<IReportItem>.Clear()
        {
            ((IList<IReportItem>)this).Clear();
        }

        bool ICollection<IReportItem>.Contains(IReportItem item)
        {
            return ((IList<IReportItem>)this).IndexOf(item) != -1;
        }

        void ICollection<IReportItem>.CopyTo(IReportItem[] array, int arrayIndex)
        {
            throw new NotSupportedException();
        }

        int ICollection<IReportItem>.Count
        {
            get { return Count; }
        }

        bool ICollection<IReportItem>.IsReadOnly
        {
            get { return true; }
        }

        bool ICollection<IReportItem>.Remove(IReportItem item)
        {
            throw new NotSupportedException();
        }

        #endregion

        #region IEnumerable<IReportItem> Members

        IEnumerator<IReportItem> IEnumerable<IReportItem>.GetEnumerator()
        {
            Collection<IReportItem> list = new Collection<IReportItem>();

            foreach (MarketReportItem item in this)
            {
                list.Add(item);
            }

            return list.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        public IReportItem Find(Predicate<IReportItem> match)
        {
            if (match == null)
            {
                throw new NullReferenceException("match");
            }

            for (int i = 0; i < Count; i++)
            {
                if (match(this[i]))
                {
                    return this[i];
                }
            }

            return default(IReportItem);
        }

        public List<IReportItem> FindAll(Predicate<IReportItem> match)
        {
            if (match == null)
            {
                throw new NullReferenceException("match");
            }
            List<IReportItem> list = new List<IReportItem>();
            for (int i = 0; i < Count; i++)
            {
                if (match(this[i]))
                {
                    list.Add(this[i]);
                }
            }
            return list;
        }

        public IReportItem FindLast(Predicate<IReportItem> match)
        {
            if (match == null)
            {
                throw new NullReferenceException("match");
            }
            for (int i = Count - 1; i >= 0; i--)
            {
                if (match(this[i]))
                {
                    return this[i];
                }
            }
            return default(IReportItem);
        }

        public List<int> CollectYears()
        {
            List<int> list = new List<int>();

            for (int i = 0; i < Count; i++)
            {
                if (!list.Contains(this[i].ModelYear))
                {
                    list.Add(this[i].ModelYear);
                }
            }

            list.Sort();

            return list;
        }

        public List<string> CollectMakes()
        {
            List<string> list = new List<string>();

            for (int i = 0; i < Count; i++)
            {
                if (!list.Contains(this[i].Make))
                {
                    list.Add(this[i].Make);
                }
            }

            return list;
        }

        public Dictionary<int, string> CollectModels()
        {
            Dictionary<int, string> dictionary = new Dictionary<int, string>();

            for (int i = 0; i < Count; i++)
            {
                if (!dictionary.ContainsKey(this[i].ModelFamilyId))
                {
                    dictionary.Add(this[i].ModelFamilyId, this[i].ModelFamily);
                }
            }

            return dictionary;
        }

        public Dictionary<int, string> CollectSegments()
        {
            Dictionary<int, string> dictionary = new Dictionary<int, string>();

            for (int i = 0; i < Count; i++)
            {
                if (!dictionary.ContainsKey(this[i].SegmentId))
                {
                    dictionary.Add(this[i].SegmentId, this[i].Segment);
                }
            }

            return dictionary;
        }

        public List<int> CollectPrices()
        {
            List<int> collection = new List<int>();

            for (int i = 0; i < Count; i++)
            {
                if (this[i].AverageInternetPrice.HasValue && !collection.Contains(this[i].AverageInternetPrice.Value))
                {
                    collection.Add(this[i].AverageInternetPrice.Value);
                }
            }

            return collection;
        }

        public List<int> CollectMileages()
        {
            List<int> collection = new List<int>();

            for (int i = 0; i < Count; i++)
            {
                if (this[i].AverageInternetMileage.HasValue && !collection.Contains(this[i].AverageInternetMileage.Value))
                {
                    collection.Add(this[i].AverageInternetMileage.Value);
                }
            }

            return collection;
        }
    }
}
