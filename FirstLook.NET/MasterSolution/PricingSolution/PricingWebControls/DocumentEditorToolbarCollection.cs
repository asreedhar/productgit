using System;
using System.Collections;
using System.Web.UI;

namespace FirstLook.Pricing.WebControls
{
	public class DocumentEditorToolBarCollection : StateManagedCollection, IParserAccessor
	{
		private static readonly Type[] knownTypes = new Type[]
			{
				typeof (DocumentEditorToolBar)
			};

		protected override object CreateKnownType(int index)
		{
			switch (index)
			{
				case 0:
					return new DocumentEditorToolBar();
			}
			throw new ArgumentOutOfRangeException("index");
		}

		protected override Type[] GetKnownTypes()
		{
			return knownTypes;
		}

		protected override void SetDirtyObject(object o)
		{
			
		}

        public void Add(DocumentEditorToolBar field)
        {
            ((IList)this).Add(field);
        }

        public bool Contains(DocumentEditorToolBar field)
        {
            return ((IList)this).Contains(field);
        }

        public int IndexOf(DocumentEditorToolBar field)
        {
            return ((IList)this).IndexOf(field);
        }

        public void Insert(int index, DocumentEditorToolBar field)
        {
            ((IList)this).Insert(index, field);
        }

        public void Remove(DocumentEditorToolBar field)
        {
            ((IList)this).Remove(field);
        }

        public void RemoveAt(int index)
        {
            ((IList)this).RemoveAt(index);
        }

		protected override void OnValidate(object o)
		{
			base.OnValidate(o);

			bool isToolBar = o is DocumentEditorToolBar;

			if (!isToolBar)
			{
				throw new ArgumentException("DataControlFieldCollection_InvalidType");
			}
		}

		public DocumentEditorToolBar this[int index]
		{
			get
			{
				return this[index];
			}
		}

		#region IParserAccessor Members

		protected void AddParsedSubObject(object obj)
		{
			DocumentEditorToolBar toolBar = obj as DocumentEditorToolBar;

			if (toolBar != null)
			{
				Add(toolBar);
			}
		}

		void IParserAccessor.AddParsedSubObject(object obj)
		{
			AddParsedSubObject(obj);
		}

		#endregion
	}
}
