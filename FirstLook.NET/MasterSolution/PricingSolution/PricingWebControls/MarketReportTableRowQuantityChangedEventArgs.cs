using System;
using FirstLook.Pricing.DomainModel.Market;

namespace FirstLook.Pricing.WebControls
{
    public class MarketReportTableRowQuantityChangedEventArgs: EventArgs
    {
        #region Properties

        private readonly IReportItem _reportItem;
        private int _quantity;
        private string _notes;

        public IReportItem ReportItem
        {
            get
            {
                return _reportItem;
            }
        }

        public bool HasVehicleConfiguration
        {
            get
            {
                return _reportItem.Series == null &&
                 _reportItem.DriveTrain == null &&
                 _reportItem.Engine == null &&
                 _reportItem.FuelType == null &&
                 _reportItem.Transmission == null;
            }
        }

        public int Quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }

        public string Notes
        {
            get { return _notes; }
            set { _notes = value; }
        }

        #endregion

        public MarketReportTableRowQuantityChangedEventArgs(IReportItem reportItem, int quantity, string notes)
        {
            _reportItem = reportItem;
            _quantity = quantity;
            _notes = notes;
        }

        public MarketReportTableRowQuantityChangedEventArgs(IReportItem reportItem, int quantity)
        {
            _reportItem = reportItem;
            _quantity = quantity;
        }
    }
}
