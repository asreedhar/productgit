using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: WebResource("FirstLook.Pricing.WebControls.CheckBoxListToggleBehavior.js", "text/javascript")]
[assembly: WebResource("FirstLook.Pricing.WebControls.CheckBoxListToggleBehavior.debug.js", "text/javascript")]

namespace FirstLook.Pricing.WebControls
{
    [TargetControlType(typeof(CheckBoxList))]
    public class CheckBoxListToggleExtender : ExtenderControl
    {
        [DefaultValue(""),
         Description("Button to which clicks are delegated"),
         Category("Behavior"),
         IDReferenceProperty(typeof(CheckBox))]
        public string CheckBoxID
        {
            get
            {
                string value = (string)ViewState["CheckBoxID"];
                if (string.IsNullOrEmpty(value))
                    return string.Empty;
                return value;
            }
            set
            {
                if (string.Compare(CheckBoxID, value) != 0)
                {
                    ViewState["CheckBoxID"] = value;
                }
            }
        }

        protected override IEnumerable<ScriptDescriptor> GetScriptDescriptors(Control targetControl)
        {
            ScriptBehaviorDescriptor descriptor = new ScriptBehaviorDescriptor("FirstLook.Pricing.WebControls.CheckBoxListToggleBehavior", targetControl.ClientID);
            descriptor.AddElementProperty("CheckBox", FindControl(CheckBoxID).ClientID);
            return new ScriptDescriptor[] { descriptor };
        }

        protected override IEnumerable<ScriptReference> GetScriptReferences()
        {
            ScriptReference reference =
                new ScriptReference("FirstLook.Pricing.WebControls.CheckBoxListToggleBehavior.js",
                                    "FirstLook.Pricing.WebControls");
            return new ScriptReference[] { reference };
        }
    }
}
