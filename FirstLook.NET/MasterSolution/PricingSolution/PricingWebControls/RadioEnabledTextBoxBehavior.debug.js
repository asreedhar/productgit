

Type.registerNamespace('FirstLook.Pricing.WebControls');

/** -------------------------------------------------------------------
 ** RadioEnabledTextBox Behavior Class Declaration
 ** ---------------------------------------------------------------- */

FirstLook.Pricing.WebControls.RadioEnabledTextBoxBehavior = function (element)
{
    FirstLook.Pricing.WebControls.RadioEnabledTextBoxBehavior.initializeBase(this,[element]);
    this._RadioButtonGroupName = null;
}

function FirstLook$Pricing$Website$RadioEnabledTextBoxBehavior$initialize () 
{
    FirstLook.Pricing.WebControls.RadioEnabledTextBoxBehavior.callBaseMethod(this, 'initialize');
    this._OnChangeDelegate = Function.createDelegate(this, this._OnChange);
}

function FirstLook$Pricing$Website$RadioEnabledTextBoxBehavior$dispose () 
{    
    // dispose of event handlers    
    var rButtons = this.get_RadioButtons();
    
    for (var i = rButtons.length - 1; i >= 0; i--){
    	$removeHandler(rButtons[i], 'click', this._OnChangeDelegate);	
    };  
        
    delete this._OnChangeDelegate;    
    // finally
    FirstLook.Pricing.WebControls.RadioEnabledTextBoxBehavior.callBaseMethod(this, 'dispose');
}

function FirstLook$Pricing$Website$RadioEnabledTextBoxBehavior$updated () {

    var rButtons = this.get_RadioButtons();
    
    for (var i = rButtons.length - 1; i >= 0; i--){
    	$addHandler(rButtons[i], 'click', this._OnChangeDelegate);	
    };  
}

function FirstLook$Pricing$Website$RadioEnabledTextBoxBehavior$get_RadioButton () {
    return this._RadioButton;
}

function FirstLook$Pricing$Website$RadioEnabledTextBoxBehavior$set_RadioButton (value) {
    this._RadioButton = value;
}

function FirstLook$Pricing$Website$RadioEnabledTextBoxBehavior$get_RadioButtons (value) {
	if (! !!this._radioButtons) {
		var groupName = this.get_RadioButton().name;
		var radios = document.getElementsByTagName('input');
		this._radioButtons = [];
		for (var i = radios.length - 1; i >= 0; i--){
			if (radios[i].type != "radio" && radios[i].name != groupName) continue;
			this._radioButtons.push(radios[i]);
		};
	};
	
	return this._radioButtons;
}

function FirstLook$Pricing$Website$RadioEnabledTextBoxBehavior$_OnChange (e) 
{       
    var rButton = this.get_RadioButton();
    var txtBox = this.get_element();
    txtBox.disabled = !rButton.checked;
}

FirstLook.Pricing.WebControls.RadioEnabledTextBoxBehavior.prototype =
{
    initialize:       FirstLook$Pricing$Website$RadioEnabledTextBoxBehavior$initialize,
    dispose:          FirstLook$Pricing$Website$RadioEnabledTextBoxBehavior$dispose,
    updated:          FirstLook$Pricing$Website$RadioEnabledTextBoxBehavior$updated,
    get_RadioButtons: FirstLook$Pricing$Website$RadioEnabledTextBoxBehavior$get_RadioButtons,
    get_RadioButton:  FirstLook$Pricing$Website$RadioEnabledTextBoxBehavior$get_RadioButton,
    set_RadioButton:  FirstLook$Pricing$Website$RadioEnabledTextBoxBehavior$set_RadioButton,
    _OnChange:        FirstLook$Pricing$Website$RadioEnabledTextBoxBehavior$_OnChange
};

if (FirstLook.Pricing.WebControls.RadioEnabledTextBoxBehavior.registerClass != undefined)
    FirstLook.Pricing.WebControls.RadioEnabledTextBoxBehavior.registerClass('FirstLook.Pricing.WebControls.RadioEnabledTextBoxBehavior', Sys.UI.Behavior);

