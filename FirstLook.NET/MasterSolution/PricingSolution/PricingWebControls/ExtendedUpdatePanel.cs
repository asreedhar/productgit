﻿using System.Web.UI;

namespace FirstLook.Pricing.WebControls
{
    /// <summary>
    /// A specialized UpdatePanel which exposes a way to determine whether it will re-render.
    /// </summary>
    public class ExtendedUpdatePanel : UpdatePanel 
    {
        /// <summary>
        /// Will the panel re-render>?
        /// </summary>
        /// <param name="postBackControlId">The uniqueId of the control which caused post-back. Also known as the event target.</param>
        /// <returns></returns>
        public bool WillReRender(string postBackControlId)
        {
            // The panel will render if:
            // 1 - The UpdatePanel.RequiresUpdate is true (there are several reasons for this) OR
            // 2 - A child control of the panel is the event target AND the panel is configured to use children as triggers.

            bool childTriggeredPostBack = false;

            // Look for a child control with the specified eventTargetID.  
            Control target = DepthFirstSearchFindControl(this, postBackControlId);

            if( target != null && ChildrenAsTriggers )
            {
                childTriggeredPostBack = true;
            }

            return RequiresUpdate || childTriggeredPostBack;
        }

        /// <summary>
        /// Perform a depth-first-search for a control with specified uniqueID starting at root control.
        /// </summary>
        /// <param name="root"></param>
        /// <param name="uniqueID"></param>
        /// <returns></returns>
        private static Control DepthFirstSearchFindControl(Control root, string uniqueID)
        {
            Control found = null;

            foreach( Control c in root.Controls )
            {
                if (found != null) continue;

                if (c.UniqueID == uniqueID)
                {
                    found = c;
                    break;
                }

                found = DepthFirstSearchFindControl(c, uniqueID);
            }

            return found;            
        }
    }
}


