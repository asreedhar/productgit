using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: WebResource("FirstLook.Pricing.WebControls.RadioEnabledTextBoxBehavior.js", "text/javascript")]
[assembly: WebResource("FirstLook.Pricing.WebControls.RadioEnabledTextBoxBehavior.debug.js", "text/javascript")]


namespace FirstLook.Pricing.WebControls
{
    [TargetControlType(typeof(TextBox))]
    public class RadioEnabledTextBoxExtender : ExtenderControl
    {

        [
            DefaultValue(""),
            Description("The Radio Button used to enable or disable the text box"),
            Category("Behavior"),
            IDReferenceProperty(typeof(RadioButton))
        ]
        public String RadioButtonId
        {
            get
            {
                String value = (String) ViewState["RadioButtonId"];
                return (String.IsNullOrEmpty(value)) ? String.Empty : value;
            }
            set
            {
                if (string.Compare(RadioButtonId, value) != 0)
                {
                    ViewState["RadioButtonId"] = value;
                }
            }
        }



        #region Overrides of ExtenderControl

        protected override IEnumerable<ScriptDescriptor> GetScriptDescriptors(Control targetControl)
        {

            ScriptBehaviorDescriptor descriptor = 
                new ScriptBehaviorDescriptor("FirstLook.Pricing.WebControls.RadioEnabledTextBoxBehavior", targetControl.ClientID);
            descriptor.AddElementProperty("RadioButton", FindControl(RadioButtonId).ClientID);
            return new ScriptDescriptor[] { descriptor };

        }

        protected override IEnumerable<ScriptReference> GetScriptReferences()
        {
            ScriptReference reference = new ScriptReference("FirstLook.Pricing.WebControls.RadioEnabledTextBoxBehavior.js", "FirstLook.Pricing.WebControls");
            return new ScriptReference[] { reference };

        }


        #endregion
    }
}
