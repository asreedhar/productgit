using System.Web;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel;

namespace FirstLook.Pricing.WebControls
{
    public class DecisionForm
    {
        public const string VehiclePricingDecisionInputContextKey = "VehiclePricingDecisionInput";

        public static VehiclePricingDecisionInput GetVehiclePricingDecisionInput(HttpContext context, HttpRequest request)
        {
            return GetVehiclePricingDecisionInput(context, request.QueryString["oh"], request.QueryString["vh"]);
        }

        public static VehiclePricingDecisionInput GetVehiclePricingDecisionInput(HttpContext context, string ownerHandle, string vehicleHandle)
        {
            VehiclePricingDecisionInput input = (VehiclePricingDecisionInput)context.Items[VehiclePricingDecisionInputContextKey];

            if (input == null)
            {
                input = VehiclePricingDecisionInput.GetVehiclePricingDecisionInput(
                    ownerHandle,
                    vehicleHandle);

                context.Items[VehiclePricingDecisionInputContextKey] = input;
            }

            return input;
        }

        public static void SaveVehiclePricingDecisionInput(HttpContext context)
        {
            VehiclePricingDecisionInput input = (VehiclePricingDecisionInput)context.Items[VehiclePricingDecisionInputContextKey];
            if (input != null)
                input.Save();
        }
    }
}