using System.Web.UI;
using FirstLook.Pricing.DomainModel.Presentation;

namespace FirstLook.Pricing.WebControls
{
    public class OrderedTableRow : IStateManager, ITableRow
    {
        private int _id;
        private string _name;
        private bool _enabled;
        private bool _selected;
        private bool _selectedChanged;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        public bool Selected
        {
            get { return _selected; }
            set
            {
                if (value != _selected)
                {
                    _selected = value;
                    _selectedChanged = true;
                }
            }
        }

        public bool HasSelectedChanged
        {
            get { return _selectedChanged; }
        }

        #region IStateManager Members

        private bool _trackingViewState;

        public bool IsTrackingViewState
        {
            get { return _trackingViewState; }
        }

        public void LoadViewState(object state)
        {
            object[] values = state as object[];

            if (values != null)
            {
                _id = (int) values[0];
                _name = (string) values[1];
                _enabled = (bool) values[2];
                _selected = (bool) values[3];
            }
        }

        public object SaveViewState()
        {
            return new object[] {_id, _name, _enabled, _selected};
        }

        public void TrackViewState()
        {
            _trackingViewState = true;
        }

        #endregion
    }
}