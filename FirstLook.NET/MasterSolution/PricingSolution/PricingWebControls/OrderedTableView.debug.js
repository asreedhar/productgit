
Type.registerNamespace('FirstLook.Pricing.WebControls');

/* Editor */

FirstLook.Pricing.WebControls.OrderedTableView = function (element)
{
    FirstLook.Pricing.WebControls.OrderedTableView.initializeBase(this,[element]);
    this._rows = [];
}

FirstLook.Pricing.WebControls.OrderedTableView.prototype =
{
    initialize: function() {
        FirstLook.Pricing.WebControls.OrderedTableView.callBaseMethod(this, 'initialize');
    },
    dispose : function() {
        FirstLook.Pricing.WebControls.OrderedTableView.callBaseMethod(this, 'dispose');
    },
    updated: function() {
        FirstLook.Pricing.WebControls.OrderedTableView.callBaseMethod(this, 'updated');
    },
    Register: function(row) {
        this._rows[this._rows.length] = row;
    },
    Deregister: function(row) {
        for (var i = 0, l = this._rows.length; i < l; i++) {
            if (row == this._rows[i]) {
                this._rows.splice(i, 1);
                break;
            }
        }
    },
    MoveUp: function(row) {
        var cmdRank = row.get_RankField().value;
        var rowCount = this._rows.length;
        if (cmdRank == 0) {
            return;
        }
        this.SwapRows(row, cmdRank-1);
    },
    MoveDown: function(row) {
        var cmdRank = parseInt(row.get_RankField().value, 10);
        var rowCount = this._rows.length;
        if (cmdRank == rowCount-1) {
            return;
        }
        this.SwapRows(row, cmdRank+1);
    },
    SwapRows: function(a, newIndex) {
        var oldIndex = parseInt(a.get_RankField().value, 10);
        var insIndex = (newIndex > oldIndex) ? newIndex+1 : newIndex;
        var rowCount = this._rows.length;
        var dv = this.get_element(),
            ra = a.get_element(),
            ia = ra.rowIndex;
        var ta = dv.getElementsByTagName("TBODY")[0];
        // verify newIndex at some point
        var rb = ta.insertRow(insIndex);
        rb.id = ra.id;
        // copy cells one by one (sigh)
        for (var i=0, l=ra.cells.length; i<l; i++) {
            var oldCell = ra.cells[i]; 
            var newCell = document.createElement("TD");
            newCell.className = oldCell.className;
            newCell.innerHTML = oldCell.innerHTML; 
            rb.appendChild(newCell); 
            for (var j=0, k=oldCell.childNodes.length; j < k; j++) {
                try {
                    newCell.childNodes[j].value = oldCell.childNodes[j].value;
                }
                catch(e){
                    // ignore horrors
                }
            }
        }
        // copy a's ids
        var xd = a.get_DownButton().id,
            xu = a.get_UpButton().id,
            xr = a.get_RankField().id;
        // dispose of a's behavior
        a.dispose();
        // dispose of a's table row
        try {
            ta.deleteRow(ra.rowIndex-1);
        }
        catch (e) {
            // more ignoration
        }
        // create a new behavior on the new row
        a = $create(
            FirstLook.Pricing.WebControls.OrderedTableRow, {
                "DownButton":$get(xd),
                "RankField":$get(xr),
                "UpButton":$get(xu)
            },
            null,
            { "Editor": this.get_id() },
            $get(rb.id));
        // update rank fields
        for (var i = 0, l = rowCount; i < l; i++) {
            var b = this._rows[i];
            if (b.get_RankField().value == newIndex) {
                // debugger;
                // rank
                a.get_RankField().value = newIndex;
                b.get_RankField().value = oldIndex;
                // button a
                a.get_UpButton().disabled = (newIndex == 0);
                a.get_DownButton().disabled = (newIndex+1 == rowCount);
                // button b
                b.get_UpButton().disabled = (oldIndex == 0);
                b.get_DownButton().disabled = (oldIndex+1 == rowCount);
                // done
                break;
            }
        }
    }
};

FirstLook.Pricing.WebControls.OrderedTableView.registerClass('FirstLook.Pricing.WebControls.OrderedTableView', Sys.UI.Behavior);

/* EditorRow */

FirstLook.Pricing.WebControls.OrderedTableRow = function (element)
{
    FirstLook.Pricing.WebControls.OrderedTableRow.initializeBase(this,[element]);
    this._editor = null;
    this._upButton = null;
    this._downButton = null;
    this._rankField = null;
}

FirstLook.Pricing.WebControls.OrderedTableRow.prototype =
{
    initialize: function() {
        FirstLook.Pricing.WebControls.OrderedTableRow.callBaseMethod(this, 'initialize');
        // delegates
        this._onUpClickDelegate = Function.createDelegate(this, this._onUpClick);
        this._onDownClickDelegate = Function.createDelegate(this, this._onDownClick);
    },
    dispose : function() {
        // handlers
        Sys.UI.DomEvent.removeHandler(this.get_UpButton(), 'click', this._onUpClickDelegate);
        Sys.UI.DomEvent.removeHandler(this.get_DownButton(), 'click', this._onDownClickDelegate);
        // delegates
        delete this._onUpClickDelegate;
        delete this._onDownClickDelegate;
        // editor
        this.get_Editor().Deregister(this);
        // fields
        this._editor = null;
        this._upButton = null;
        this._downButton = null;
        this._rankField = null;
        // parent
        FirstLook.Pricing.WebControls.OrderedTableRow.callBaseMethod(this, 'dispose');
    },
    updated: function() {
        Sys.UI.DomEvent.addHandler(this.get_UpButton(), 'click', this._onUpClickDelegate);
        Sys.UI.DomEvent.addHandler(this.get_DownButton(), 'click', this._onDownClickDelegate);
        this.get_Editor().Register(this);
        FirstLook.Pricing.WebControls.OrderedTableRow.callBaseMethod(this, 'updated');
    },
    get_Editor: function() {
        return this._editor;
    },
    set_Editor: function(value) {
        this._editor = value;
    },
    get_RankField: function() {
        return this._rankField;
    },
    set_RankField: function(value) {
        this._rankField = value;
    },
    get_UpButton: function() {
        return this._upButton;
    },
    set_UpButton: function(value) {
        this._upButton = value;
    },
    get_DownButton: function() {
        return this._downButton;
    },
    set_DownButton: function(value) {
        this._downButton = value;
    },
    _onUpClick: function(e) {
        this.get_Editor().MoveUp(this);
        e.preventDefault();
        return e.returnValue = false;
    },
    _onDownClick: function(e) {
        this.get_Editor().MoveDown(this);
        e.preventDefault();
        return e.returnValue = false;
    }
};

FirstLook.Pricing.WebControls.OrderedTableRow.registerClass('FirstLook.Pricing.WebControls.OrderedTableRow', Sys.UI.Behavior);

Sys.Application.notifyScriptLoaded();
