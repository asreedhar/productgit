using System.ComponentModel;
using System.Web.UI;
using FirstLook.Common.WebControls;

namespace FirstLook.Pricing.WebControls
{
	[TypeConverter(typeof(ExpandableObjectConverter)), DefaultProperty("ControlID")]
	public class DocumentEditorToolBar : IStateManager
	{
        private readonly StateBag _viewState = new StateBag();
		private string controlId;

		[IDReferenceProperty, DefaultValue(""), Description("DocumentEditorToolBar_ControlID"), Category("Behavior")]
		public string ControlID
		{
			get { return controlId; }
			set { controlId = value; }
		}

		public ToolBar GetControl(Control control)
		{
			return DataBoundControlHelper.FindControl(control, ControlID) as ToolBar;
		}

        protected StateBag ViewState
        {
            get { return _viewState; }
        }

		#region IStateManager Members

        private bool trackViewState;

        protected bool IsTrackingViewState
        {
            get
            {
                return trackViewState;
            }
        }

        bool IStateManager.IsTrackingViewState
        {
            get { return IsTrackingViewState; }
        }

        protected void LoadViewState(object state)
        {
            if (state != null)
            {
                ((IStateManager)ViewState).LoadViewState(state);
            }
        }

        void IStateManager.LoadViewState(object state)
        {
            LoadViewState(state);
        }

        protected object SaveViewState()
        {
            return ((IStateManager)ViewState).SaveViewState();
        }

        object IStateManager.SaveViewState()
        {
            return SaveViewState();
        }

        protected void TrackViewState()
        {
            trackViewState = true;

            ((IStateManager)ViewState).TrackViewState();
        }

        void IStateManager.TrackViewState()
        {
            TrackViewState();
        }

		#endregion
	}
}
