Type.registerNamespace('FirstLook.Pricing.WebControls');

FirstLook.Pricing.WebControls.MarketReportTableFilterHelper = function(element) {
    FirstLook.Pricing.WebControls.MarketReportTableFilterHelper.initializeBase(this, [element]);
    this.delegates = {};
    var props = [
    // ============
    // = Elements =
    // ============
    "choose_make_button",
    "choose_make_select",
    "select_all_segments_button",
    "has_column_filter_class",
    "filter_row_class",
    // ==============
    // = Properties =
    // ==============
    "has_column_filter_class"
    ];
    for (var i = 0, l = props.length; i < l; i++) {
        this.create_getter_setter(props[i]);
    };
};

FirstLook.Pricing.WebControls.MarketReportTableFilterHelper.prototype = {
    initialize: function() {
        FirstLook.Pricing.WebControls.MarketReportTableFilterHelper.callBaseMethod(this, "initialize");
    },
    updated: function() {
        FirstLook.Pricing.WebControls.MarketReportTableFilterHelper.callBaseMethod(this, "updated");

        this.delegates['onPropertyChanged'] = Function.createDelegate(this, this.onPropertyChanged);

        this.add_propertyChanged(this.delegates['onPropertyChanged']);

        this.delegates["onColumnFilterClick"] = Function.createDelegate(this, this.onColumnFilterClick);
        this.delegates["onChooseMakeChanged"] = Function.createDelegate(this, this.onChooseMakeChanged);
        this.delegates["onSelectAllSegmentsButton"] = Function.createDelegate(this, this.onSelectAllSegmentsButton);

        var columns_with_filters = this.get_columns_with_filters();
        for (var i = 0, l = columns_with_filters.length; i < l; i++) {
            this.add_handler(columns_with_filters[i], "click", this.delegates["onColumnFilterClick"]);
        };

        this.add_handler(this.get_choose_make_select(), "change", this.delegates["onChooseMakeChanged"]);
        
        this.add_handler(this.get_select_all_segments_button(), "click", this.delegates["onSelectAllSegmentsButton"]);
        
        this.hide_filters();

        this.hide_buttons();
    },
    dispose: function() {
        this.remove_propertyChanged(this.delegates['onPropertyChanged']);

        this.remove_handlers();

        this.delegates = undefined;

        FirstLook.Pricing.WebControls.MarketReportTableFilterHelper.callBaseMethod(this, "dispose");
    },
    // ===========
    // = Methods =
    // ===========
    get_columns_with_filters: function() {
        if ( !! !this._columns_with_filters) {
            var el = this.get_element(),
            ths = el.getElementsByTagName('td'),
            className = this.get_has_column_filter_class(),
            i,
            l;

            this._columns_with_filters = [];
            for (i = 0, l = ths.length; i < l; i++) {
                if (Sys.UI.DomElement.containsCssClass(ths[i], className)) {
                    this._columns_with_filters.push(ths[i]);
                }
            };
        };

        return this._columns_with_filters;
    },
    get_filter_for_column: function(td) {
        var td = (td.nodeName == "TD") ? td: this.find_parent_node(td, "TD"),
        tr = td.parentNode,
        summary_tds = tr.children,
        filter_row = tr.nextSibling,
        filter_tds = filter_row.children;
        for (var i = 0, l = summary_tds.length; i < l; i++) {
            if (summary_tds[i] == td) {
                return filter_tds[i].getElementsByTagName('div')[0];
            }
        };
    },
    set_columns_with_filters: function() {
        Error.invalidOperation("Not Supported");
    },
    hide_buttons: function() {
        this.get_choose_make_button().style.display = "none";
    },
    hide_filter: function(el) {
        el.style.display = "none";
    },
    show_filter: function(el) {
        el.style.display = "block";
    },
    hide_filters: function(el) {
        var columns = this.get_columns_with_filters(),
        filter;
        for (var i = 0; i < columns.length; i++) {
            if ( (FirstLook.Pricing.WebControls.MarketReportTableFilterHelper.OpenIndex == i) ||
            	 (! !!el && el == columns[i]) ) {
                this.show_filter(this.get_filter_for_column(columns[i]));
                FirstLook.Pricing.WebControls.MarketReportTableFilterHelper.OpenIndex = undefined;
            } 
            else {
            	this.hide_filter(this.get_filter_for_column(columns[i]));                
            }
        };
    },
    get_segment_checkboxes: function() {
        if (!!!this._segment_checkboxes) {
            var className = "segment",
            checkbox_array = [],
            inputs = document.getElementsByTagName('input'),
            i;
            for (var i = inputs.length - 1; i >= 0; i--) {
                if (inputs[i].type == "checkbox" && inputs[i].className.indexOf(className) != -1) {
                    checkbox_array.push(inputs[i]);
                }
            };
            this._segment_checkboxes = checkbox_array;
        };

        return this._segment_checkboxes;
    },
    // ==========
    // = Events =
    // ==========
    onPropertyChanged: function(sender, args) {
        var property = args.get_propertyName(),
        source_class = sender.get_name();

        Sys.Debug.assert( !! !FirstLook.Pricing.WebControls.MarketReportTableFilterHelper["debug_" + property], property);

        if ( !! FirstLook.Pricing.WebControls.MarketReportTableFilterHelper.Trace || !!FirstLook.Pricing.WebControls.MarketReportTableFilterHelper["trace_" + property]) {
            Sys.Debug.traceDump(sender["get_" + property](), sender.get_id() + ":" + property);
        };
    },
    onColumnFilterClick: function(ev) {
        var filter = this.get_filter_for_column(ev.target);    
        if (this.get_computed_style(filter, "display") == "none") {
        	this.hide_filters(filter);
            this.show_filter(filter);
        } else {
            this.hide_filter(filter);
        }
    },
    onChooseMakeChanged: function(ev) {
        FirstLook.Pricing.WebControls.MarketReportTableFilterHelper.OpenIndex = 0;
        this.get_choose_make_button().click();
    },
    onSelectAllSegmentsButton: function(ev) {
        ev.preventDefault();
        var checkboxes = this.get_segment_checkboxes();
        
        for (var i = checkboxes.length - 1; i >= 0; i--) {
            checkboxes[i].checked = "checked";
        };
    },
    // ==================
    // = Helper Methods =
    // ==================
    create_getter_setter: function(property) {
        // =================================================================
        // = Generic Function to create AJAX.net style getters and setters =
        // =================================================================
        if (this["get_" + property] === undefined) {
            this["get_" + property] = (function(thisProp) {
                return function get_property() {
                    return this[thisProp];
                };
            })("_" + property);
        };
        if (this["set_" + property] === undefined) {
            this["set_" + property] = (function(thisProp, raiseType) {
                return function set_property(value) {
                    if (value !== this[thisProp]) {
                        this[thisProp] = value;
                        this.raisePropertyChanged(raiseType);
                    };
                };
            })("_" + property, property);
        };
    },
    add_handler: function(el, type, fn) {
        if (this._eventCache === undefined) {
            this._eventCache = [];
        }
        this._eventCache.push([el, type, fn]);
        $addHandler(el, type, fn);
    },
    remove_handlers: function() {
        if (this._eventCache !== undefined) {
            for (var i = 0, l = this._eventCache.length; i < l; i++) {
                $removeHandler(this._eventCache[i][0], this._eventCache[i][1], this._eventCache[i][2]);
                this._eventCache[i][0] = undefined;
                this._eventCache[i][1] = undefined;
                this._eventCache[i][2] = undefined;
            };
            delete this._eventCache;
        };
    },
    get_computed_style: function(el, style) {
        if ( !! el.currentStyle) {
            return el.currentStyle[style];
        } else if ( !! document.defaultView && !!document.defaultView.getComputedStyle) {
            return document.defaultView.getComputedStyle(el, "")[style];
        } else {
            return el.style[style];
        }
    },
    find_parent_node: function(el, nodeName) {
        if (el.parentElement.nodeName === nodeName.toUpperCase()) {
            return el.parentElement;
        } else {
            return this.find_parent_node(el.parentElement, nodeName);
        }
    }
};

if (FirstLook.Pricing.WebControls.MarketReportTableFilterHelper.registerClass != undefined)
 FirstLook.Pricing.WebControls.MarketReportTableFilterHelper.registerClass('FirstLook.Pricing.WebControls.MarketReportTableFilterHelper', Sys.UI.Behavior);

Sys.Application.notifyScriptLoaded();