using System;
using System.ComponentModel;
using System.Text;
using System.Web.UI;

namespace FirstLook.Pricing.WebControls.UserControls
{
    public abstract class PriceControl : ApplicationUserControl, IScriptControl
    {
        private const string CurrencyFormat = "{0:$#,###,##0;($#,###,##0);$0}"; // "+;-;0"
        private const string CommaFormat = "{0:#,###,##0;(#,###,##0);0}"; // "+;-;0"

        #region PriceControl

        private static readonly object EventPriceChanging = new object();
        private static readonly object EventPriceChanged = new object();

        [Category("Behavior")]
        public event EventHandler PriceChanging
        {
            add { Events.AddHandler(EventPriceChanging, value); }
            remove { Events.RemoveHandler(EventPriceChanging, value); }
        }

        [Category("Behavior")]
        public event PriceChangedEventHandler PriceChanged
        {
            add { Events.AddHandler(EventPriceChanged, value); }
            remove { Events.RemoveHandler(EventPriceChanged, value); }

        }

        public abstract int? Price
        {
            get;
            set;
        }

        protected virtual void RaisePriceChangingEvent()
        {
            EventHandler handler = Events[EventPriceChanging] as EventHandler;

            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        protected virtual void RaisePriceChangedEvent(int? oldPrice, int? newPrice)
        {
            PriceChangedEventHandler handler = Events[EventPriceChanged] as PriceChangedEventHandler;

            if (handler != null)
            {
                handler(this, new PriceChangedEventArgs(newPrice, oldPrice));
            }
        }

        protected virtual void RaisePriceChangedEvent(PriceChangedEventArgs e)
        {
            PriceChangedEventHandler handler = Events[EventPriceChanged] as PriceChangedEventHandler;

            if (handler != null)
            {
                handler(this, e);
            }
        }

        #endregion

        #region Page Life Cycle
        
        private ScriptManager manager;

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (!DesignMode)
            {
                manager = ScriptManager.GetCurrent(Page);

                if (manager != null)
                    manager.RegisterScriptControl(this);
                else
                    throw new ApplicationException("You must have a ScriptManager!");
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);

            if (!DesignMode)
            {
                manager.RegisterScriptDescriptors(this);
            }
        }

        #endregion

        #region Helper Methods

        protected static string RemoveCurrencyFormatting(string text)
        {
            if (string.IsNullOrEmpty(text))
                return null;
            else if (string.Compare("NA", text, StringComparison.OrdinalIgnoreCase) == 0)
                return null;
            else if (string.Compare("--", text, StringComparison.OrdinalIgnoreCase) == 0)
                return null;
            else
            {
                StringBuilder sb = new StringBuilder(text);
                sb.Replace("$", "");
                sb.Replace(",", "");
                sb.Replace(".00", "");
                sb.Replace("-", "");
                sb.Replace(")", "");
                sb.Replace("(", "-");
                return sb.ToString();
            }
        }

        protected static string FormatCurrency(int? value, string ifNull)
        {
            if (value.HasValue)
                return string.Format(CurrencyFormat, value);
            return ifNull;
        }

        protected static string FormatCurrency(int? value)
        {
            return FormatCurrency(value, string.Empty);
        }

        protected static string FormatComma(int? value, string isNull)
        {
            if (value.HasValue)
                return string.Format(CommaFormat, value);
            return isNull;
        }


        protected static string FormatComma(int? value)
        {
            return FormatComma(value, string.Empty);
        }

        protected static string FormatPercentage(int? value, string ifNull)
        {
            if (value.HasValue)
                return value.Value.ToString();
            return ifNull;
        }

        protected static string FormatPercentage(int? value)
        {
            return FormatPercentage(value, "--");
        }

        protected static string FormatNumber(int? value)
        {
            if (value.HasValue)
                return value.Value.ToString();
            return "--";
        }

        #endregion

        #region IScriptControl Members

        public virtual System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {
            return new ScriptDescriptor[0];
        }

        public virtual System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
        {
            return new ScriptReference[0];
        }

        #endregion
    }
}