using System;
using System.Collections;
using System.Text;
using System.Web.UI;
using FirstLook.Common.Core.Utilities;
using System.Web.UI.HtmlControls;

namespace FirstLook.Pricing.WebControls.UserControls
{
    public abstract class Calculator : PriceControl
    {
        public virtual void Calculate()
        {
        }

        public virtual void OnPriceRankChanged(object sender, PriceRankChangedEventArgs e)
        {
        }

        protected virtual void Page_DataBound(object sender, EventArgs e)
        {
        }

        public abstract string RepricePostbackScript { get; }
        public abstract string PriceClientId { get; }

        public int NumListings
        {
            get
            {
                object value = ViewState["NumListings"];
                if (value == null)
                    return 0;
                return (int) value;
            }
            set
            {
                if (NumListings != value)
                {
                    ViewState["NumListings"] = value;
                }
            }
        }

        public int SearchRadius
        {
            get
            {
                object value = ViewState["SearchRadius"];
                if (value == null)
                    return 0;
                return (int)value;
            }
            set
            {
                if (SearchRadius != value)
                {
                    ViewState["SearchRadius"] = value;
                }
            }
        }

        public abstract PricingRisk PricingRisk
        {
            get;
        }
        
        protected override void PerformDataBinding(IEnumerable data)
        {
            if (data == null)
                return;

            base.PerformDataBinding(data);
            Page_DataBound(this, EventArgs.Empty);
            NumListings = Int32Helper.ToInt32(DataBinder.Eval(DataItem, "NumListings"));
            SearchRadius = Int32Helper.ToInt32(DataBinder.Eval(DataItem, "SearchRadius"));
            DataBindChildren();
        }

        protected string MarketAverageGaugeUrl(string qs)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(ResolveUrl("~/Pages/Internet/Dundas/Gauge.aspx"));
            sb.Append('?');
            sb.Append(qs);
            return sb.ToString();
        }

        protected void FeedBack_PreRender(object sender, EventArgs e)
        {			
            HtmlGenericControl feedbackElement = sender as HtmlGenericControl;
            if (feedbackElement != null)
            {
                switch (PricingRisk)
                {
                    case PricingRisk.NotAnalyzed:
                        feedbackElement.Attributes.Add("class", "feedback low_risk");
                        feedbackElement.InnerText = "Not Analyzed";
                        break;
                    case PricingRisk.Underpriced:
                        feedbackElement.Attributes.Add("class", "feedback medium_risk");
                        feedbackElement.InnerText = "Underpriced";
                        break;
                    case PricingRisk.PricedAtMarket:
                        feedbackElement.Attributes.Add("class", "feedback low_risk");
                        feedbackElement.InnerText = "Priced At Market";
                        break;
                    case PricingRisk.Overpriced:
                        feedbackElement.Attributes.Add("class", "feedback high_risk");
                        feedbackElement.InnerText = "Potentially Overpriced";
                        break;
                    case PricingRisk.NoPrice:
                        feedbackElement.Attributes.Add("class", "feedback high_risk");
                        feedbackElement.InnerText = "No Price";
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
