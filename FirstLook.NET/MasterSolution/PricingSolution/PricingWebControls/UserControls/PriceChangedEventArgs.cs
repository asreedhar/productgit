using System;

namespace FirstLook.Pricing.WebControls.UserControls
{
    public class PriceChangedEventArgs : EventArgs
    {
        private readonly int? newPrice;
        private readonly int? oldPrice;

        public int? NewPrice
        {
            get { return newPrice; }
        }

        public int? OldPrice
        {
            get { return oldPrice; }
        }

        public PriceChangedEventArgs(int? newPrice, int? oldPrice)
        {
            this.newPrice = newPrice;
            this.oldPrice = oldPrice;
        }
    }
}