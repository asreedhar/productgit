using System;
using System.Collections;
using System.Web.UI;
using FirstLook.Common.WebControls;

namespace FirstLook.Pricing.WebControls.UserControls
{
    public class VehicleEntityTypeSwitchControl : DataBoundUserControl
    {
        #region Properties

        public bool IsInventory
        {
            get { return VehicleEntityTypeID == 1; }
        }

        public bool IsAppraisal
        {
            get { return VehicleEntityTypeID == 2; }
        }

        public bool IsOnlineAuctionVehicle
        {
            get { return VehicleEntityTypeID == 3; }
        }

        public bool IsInGroupVehicle
        {
            get { return VehicleEntityTypeID == 4; }
        }

        public bool IsSalesToolVehicle
        {
            get { return VehicleEntityTypeID == 5; }
        }

        public int VehicleEntityTypeID
        {
            get
            {
                object value = ViewState["VehicleEntityTypeID"];
                if (value == null)
                    return 0;
                return (int) value;
            }
            set
            {
                if (value != VehicleEntityTypeID)
                {
                    ViewState["VehicleEntityTypeID"] = value;
                }
            }
        }

        public int VehicleEntityID
        {
            get
            {
                object value = ViewState["VehicleEntityID"];
                if (value == null)
                    return 0;
                return (int)value;
            }
            set
            {
                if (value != VehicleEntityID)
                {
                    ViewState["VehicleEntityID"] = value;
                }
            }
        }

        #endregion

        #region DataBinding

        protected override void PerformDataBinding(IEnumerable data)
        {
            IEnumerator en = data.GetEnumerator();
            if (en.MoveNext())
            {
                object item = en.Current;
                // vehicle type
                object val1 = DataBinder.Eval(item, "VehicleEntityTypeID");
                if (val1 != null && !val1.Equals(DBNull.Value))
                    VehicleEntityTypeID = Convert.ToInt32(val1);
                // vehicle id
                object val2 = DataBinder.Eval(item, "VehicleEntityID");
                if (val2 != null && !val2.Equals(DBNull.Value))
                    VehicleEntityID = Convert.ToInt32(val2);
            }
        }

        #endregion
    }
}