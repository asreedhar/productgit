using System;

namespace FirstLook.Pricing.WebControls.UserControls
{
    public class PriceRankChangedEventArgs : EventArgs
    {
        private readonly int minimumRank;
        private readonly int maximumRank;
        private readonly int rank;

        public PriceRankChangedEventArgs(int minimumRank, int maximumRank, int rank)
        {
            this.minimumRank = minimumRank;
            this.maximumRank = maximumRank;
            this.rank = rank;
        }

        public int MinimumRank
        {
            get { return minimumRank; }
        }

        public int MaximumRank
        {
            get { return maximumRank; }
        }

        public int Rank
        {
            get { return rank; }
        }
    }
}