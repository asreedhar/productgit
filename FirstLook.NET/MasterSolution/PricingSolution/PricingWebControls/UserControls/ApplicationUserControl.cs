using System.Collections;
using System.ComponentModel;
using FirstLook.Common.WebControls;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;

namespace FirstLook.Pricing.WebControls.UserControls
{
    /// <summary>
    /// A base class for the pricing user controls. Has a number of helper methods that make the code much
    /// neater and better encapsulated.
    /// </summary>
    public abstract class ApplicationUserControl : DataBoundUserControl
    {
        #region Events
        
        public event PropertyChangedEventHandler PropertyChanged;

        private bool raisePropertyChangedEvents = true;

        public bool RaisePropertyChangedEvents
        {
            get { return raisePropertyChangedEvents; }
            set { raisePropertyChangedEvents = value; }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null && RaisePropertyChangedEvents)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region Request Information

        protected string OwnerHandle
        {
            get { return Request.QueryString["oh"]; }
        }

        protected string VehicleHandle
        {
            get { return Request.QueryString["vh"]; }
        }

        protected string SearchHandle
        {
            get { return Request.QueryString["sh"]; }
        }

        protected string Login
        {
            get { return Context.User.Identity.Name; }
        }

        #endregion

        #region Vehicle Helper Methods

        protected bool IsInventory()
        {
            return (VehicleEntityType == VehicleType.Inventory);
        }

        protected bool IsAppraisal()
        {
            return (VehicleEntityType == VehicleType.Appraisal);
        }

        protected bool IsOnlineAuctionVehicle()
        {
            return (VehicleEntityType == VehicleType.OnlineAuctionListing);
        }

        protected bool IsInGroupVehicle()
        {
            return (VehicleEntityType == VehicleType.DealerGroupListing);
        }

        protected bool IsSalesToolVehicle()
        {
            return (VehicleEntityType == VehicleType.InternetListing);
        }

        protected virtual VehicleType VehicleEntityType
        {
            get
            {
                return VehicleTypeCommand.Execute(VehicleHandle);
            }
        }

        #endregion

        #region Data Binding
        
        private object dataItem;

        public object DataItem
        {
            get { return dataItem; }
            protected set { dataItem = value; }
        }

        protected override void PerformDataBinding(IEnumerable data)
        {
            IEnumerator en = data.GetEnumerator();
            if (en.MoveNext())
            {
                DataItem = en.Current;
            }
        }

        #endregion
    }
}