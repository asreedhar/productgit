using System;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel;

namespace FirstLook.Pricing.WebControls
{
    public class MarketPricingSliderBridge
    {
        public MarketPricingSliderBridge(MarketPricingData marketPricingData) 
        {
            data = marketPricingData;
        }

        #region Properties
        private const double SCALE_PADDING_PCT = 0.1;
        private const int DIS_FROM_SCALE = -10;

        private MarketPricingData _data;
        private MarketPricingData data
        {
            get
            {
                return _data;
            }
            set
            {
                if (value != _data)
                {
                    _data = value;
                }
            }

        }

        public double MinPrice
        {
            get
            {
                return Convert.ToDouble(data.MinMarketPrice.Value);
            }
        }

        public double AvgPrice
        {
            get
            {
                return Convert.ToDouble(data.AvgMarketPrice.Value);
            }
        }

        public double MaxPrice
        {
            get
            {
                return Convert.ToDouble(data.MaxMarketPrice.Value);
            }
        }

        public double MinMktPct
        {
            get
            {
                return Convert.ToDouble(data.MinPctMarketAvg.Value);
            }
        }

        public double MaxMktPct
        {
            get
            {
                return Convert.ToDouble(data.MaxPctMarketAvg.Value);
            }
        }

        public double RangeLowBound
        {
            get
            {
                return Math.Round(MinMktPct);
            }
        }

        public double RangeHighBound
        {
            get
            {
                return Math.Round(MaxMktPct);
            }
        }

        public double MinPricePct
        {
            get
            {
                return Math.Round((MinPrice / AvgPrice) * 100);
            }
        }

        public double MaxPricePct
        {
            get
            {
                return Math.Round((MaxPrice / AvgPrice) * 100);
            }
        }

        public double ScaleWidth
        {
            get
            {
                return Math.Round(MaxPricePct - MinPricePct);
            }
        }

        public double Swing
        {
            get
            {
                return Math.Round(ScaleWidth * SCALE_PADDING_PCT);
            }
        }

        public double ScaleLowBoundPct
        {
            get
            {
                double value = Math.Round(MinPricePct - Swing);
                if (RangeLowBound <= value)
                {
                    return RangeLowBound; 
                }
                else
                {
                    return value;
                }
            }
        }

        public double ScaleHighBoundPct
        {
            get
            {
                double value = Math.Round(MaxPricePct + Swing);
                if (RangeHighBound >= value)
                {
                    return RangeHighBound;
                }
                else
                {
                    return value;
                }
            }
        }

        public double ScaleLowBound
        {
            get
            {
                return (ScaleLowBoundPct / 100) * AvgPrice;
            }
        }

        public double ScaleHighBound
        {
            get
            {
                return (ScaleHighBoundPct / 100) * AvgPrice;
            }
        }

        public int DisFromScale
        {
            get
            {
                return DIS_FROM_SCALE;
            }
        } 
        #endregion
    }
}
