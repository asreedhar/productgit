using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Pricing.DomainModel.Market;

[assembly: WebResource("FirstLook.Pricing.WebControls.MarketReportTable.js", "text/javascript")]
[assembly: WebResource("FirstLook.Pricing.WebControls.MarketReportTable.debug.js", "text/javascript")]

namespace FirstLook.Pricing.WebControls
{
    public class MarketReportTable : DataBoundReportTable, IPostBackDataHandler, INamingContainer, IScriptControl
    {
        #region Events

        internal static readonly object EventRowQuantityChanged = new object();

        public event EventHandler<MarketReportTableRowQuantityChangedEventArgs> RowQuantityChanged
        {
            add { Events.AddHandler(EventRowQuantityChanged, value); }
            remove { Events.RemoveHandler(EventRowQuantityChanged, value); }
        }

        protected virtual void OnRowQuantityChanged(MarketReportTableRowQuantityChangedEventArgs e)
        {
            EventHandler<MarketReportTableRowQuantityChangedEventArgs> handler =
                (EventHandler<MarketReportTableRowQuantityChangedEventArgs>) Events[EventRowQuantityChanged];
            if (handler != null)
            {
                handler(this, e);
            }
        }

        #endregion

        #region Column Definitions

        private static readonly Column[] Columns;

        static MarketReportTable()
        {
            Columns = new Column[]
                          {
                              new Column(StringResource.TableColumnExpandCssClass,
                                         SR.TableColumnExpandTitle,
                                         string.Empty),
                              new Column(SR.TableColumnMarketDaysSupplyCssClass,
                                         SR.TableColumnMarketDaysSupplyTitle,
                                         "MarketDaysSupply"),
                              new Column(SR.TableColumnSmallestMarketDaysSupplyCssClass,
                                         SR.TableColumnSmallestMarketDaysSupplyTitle,
                                         "SmallestMarketDaysSupply"),
                              new Column(SR.TableColumnVehicleInformationCssClass,
                                         SR.TableColumnVehicleInformationTitle,
                                         "ModelYear"), // , Make, ModelFamily"
                              new Column(SR.TableColumnUnitsSoldCssClass,
                                         SR.TableColumnUnitsSoldTitle,
                                         "UnitsSold"),
                              new Column(SR.TableColumnNumberOfListingsCssClass,
                                         SR.TableColumnNumberOfListingsTitle,
                                         "NumberOfListings"),
                              new Column(SR.TableColumnAverageMarketPriceCssClass,
                                         SR.TableColumnAverageMarketPriceTitle,
                                         "AverageInternetPrice"),
                              new Column(SR.TableColumnAverageMileageCssClass,
                                         SR.TableColumnAverageMileageTitle,
                                         "AverageInternetMileage"),
                              new Column(SR.TableColumnUnitsInStockCssClass,
                                         SR.TableColumnUnitsInStockTitle,
                                         "UnitsInStock"),
                              new Column(SR.TableColumnShoppingListCssClass,
                                         SR.TableColumnShoppingListTitle,
                                         string.Empty),
                          };
        }

        #endregion

        #region Properties

        private MarketReportTableFilterHelper _marketReportTableFilterHelper;

        public ReportFilterArguments Filters
        {
            get
            {
                if (_marketReportTableFilterHelper == null)
                {
                    return new ReportFilterArguments();
                }

                return _marketReportTableFilterHelper.Filters.ToReportFilterArguments();
            }
        }

        public List<int> ExpandedReportItems
        {
            get
            {
                return GetExpandedReportItems(new List<int>(), _marketReportItems);
            }
        }

        private MarketReportItemCollection _marketReportItems;

        private MarketReportItemCollection MarketReportItems
        {
            get
            {
                if (_marketReportItems == null)
                {
                    _marketReportItems = new MarketReportItemCollection();
                    if (IsTrackingViewState)
                    {
                        ((IStateManager) _marketReportItems).TrackViewState();
                    }
                }
                return _marketReportItems;
            }
        }

        public bool AreAllRowsExpanded()
        {
            foreach (MarketReportItem item in MarketReportItems)
            {
                if (!item.Expanded)
                {
                    return false;
                }
            }

            return true;
        }

        public virtual bool AllowFiltering
        {
            get
            {
                object obj2 = ViewState["AllowFiltering"];
                return ((obj2 != null) && ((bool) obj2));
            }
            set
            {
                if (value != AllowFiltering)
                {
                    ViewState["AllowFiltering"] = value;
                    if (Initialized)
                    {
                        RequiresDataBinding = true;
                    }
                }
            }
        }

        private int _pageCount;

        public override int PageCount
        {
            get { return _pageCount; }
        }

        private int _pageIndex;

        public override int PageIndex
        {
            get
            {
                return _pageIndex;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException("value");
                }
                if (PageIndex != value)
                {
                    _pageIndex = value;
                    if (Initialized)
                    {
                        RequiresDataBinding = true;
                    }
                }
            }
        }

        private int _rowCount;

        public override int RowCount
        {
            get { return _rowCount; }
        }

        private bool _showUnitsSold = true;

        public bool ShowUnitsSold
        {
            get { return _showUnitsSold; }
            set
            {
                _showUnitsSold = value;
                if (AllowFiltering && _marketReportTableFilterHelper != null)
                {
                    _marketReportTableFilterHelper.ShowUnitsSold = _showUnitsSold;
                }
            }
        }

        #endregion

        #region Overrides

        private static List<int> GetExpandedReportItems(List<int> expandedReportItems, MarketReportItemCollection reportItemCollection)
        {
            foreach (MarketReportItem reportItem in reportItemCollection)
            {
                if (reportItem.Expanded)
                {
                    expandedReportItems.Add(reportItem.Id);

                    if (reportItem.MarketReportItems.Count > 0)
                    {
                        expandedReportItems = GetExpandedReportItems(expandedReportItems, reportItem.MarketReportItems);
                    }
                }
            }

            return expandedReportItems;
        }
        
        protected override Control FindControl(string id, int pathOffset)
        {
            return base.FindControl(id, pathOffset) ?? this;
        }

        protected override void OnInit(EventArgs e)
        {
            Page.RegisterRequiresControlState(this);

            base.OnInit(e);
        }

        public override ControlCollection Controls
        {
            get
            {
                EnsureChildControls(); // makes sure CreateChildControls() was called

                return base.Controls;
            }
        }

        protected override void CreateChildControls()
        {
            base.Controls.Clear(); // do not add two filters

            _marketReportTableFilterHelper = new MarketReportTableFilterHelper();

            _marketReportTableFilterHelper.ID = "Filter";

            _marketReportTableFilterHelper.ShowUnitsSold = _showUnitsSold;

            _marketReportTableFilterHelper.FilterChanged += FilterChanged;

            Controls.Add(_marketReportTableFilterHelper);
        }

        public override void DataBind()
        {
            EnsureChildControls();

            _marketReportTableFilterHelper.DataBind();

            base.DataBind();
        }

        private void FilterChanged(object sender, EventArgs e)
        {
            PageIndex = 0;

            RequiresDataBinding = true;
        }

        #endregion

        #region Render

        protected override HtmlTextWriterTag TagKey
        {
            get { return HtmlTextWriterTag.Div; }
        }

        protected override string TagName
        {
            get { return "div"; }
        }

        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            if (string.IsNullOrEmpty(CssClass))
            {
                CssClass = StringResource.CssClass;
            }
            else
            {
                CssClass += " " + StringResource.CssClass;
            }

            base.AddAttributesToRender(writer);
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            RenderMarketReportTable(writer);

            RenderRowControls(writer);
        }

        private void RenderMarketReportTable(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, StringResource.TableCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Table);

            RenderTableHeader(writer);

            if (MarketReportItems.Count > 0)
            {
                RenderTableBody(writer);
            }

            writer.RenderEndTag();
        }

        private void RenderTableHeader(HtmlTextWriter writer)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.Thead);

            RenderTableHeaderHeadingsRow(writer);

            if (AllowFiltering)
            {
                _marketReportTableFilterHelper.RenderControl(writer);
            }

            writer.RenderEndTag(); // Thead
        }

        private void RenderTableHeaderHeadingsRow(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.TableRowHeadingsCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            for (int j = 0, k = Columns.Length; j < k; j++)
            {
                Column column = Columns[j];

                if (!ShowUnitsSold && column.Title == SR.TableColumnUnitsSoldTitle)
                    continue;

                writer.AddAttribute(HtmlTextWriterAttribute.Class, column.Css);

                writer.RenderBeginTag(HtmlTextWriterTag.Th);
               
                if (AllowSorting && !string.IsNullOrEmpty(column.Sort))
                {
                    RenderSortButton(writer, column.Title, column.Sort);
                }
                else
                {
                    writer.Write(column.Title);    
                }

                if (AllowSorting && column.Title.Equals(SR.TableColumnVehicleInformationTitle))
                {
                    RenderVehicleInformationExpandedSort(writer);
                }

                writer.RenderEndTag(); // Th

                writer.WriteLine();
            }

            writer.RenderEndTag(); // Tr
        }

        private void RenderSortButton(HtmlTextWriter writer, string title, string sortKey)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Title, "Sort By " + title);

            writer.AddAttribute(HtmlTextWriterAttribute.For, CreateId(new string[] { "sort", sortKey }));

            writer.RenderBeginTag(HtmlTextWriterTag.Label);

            writer.Write(title);

            RenderSortButton(writer, sortKey);

            writer.RenderEndTag();
        }

        private void RenderVehicleInformationExpandedSort(HtmlTextWriter writer)
        {
            Column[] columns = new Column[]
                                   {
                                       new Column("", "Year", "ModelYear"),
                                       new Column("", "Make", "Make"),
                                       new Column("", "Model", "ModelFamily"),
                                       new Column("", "Segment", "Segment")
                                   };

            writer.WriteBreak();

            writer.Write("(");

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "sort_list");

            writer.RenderBeginTag(HtmlTextWriterTag.Ul);

            foreach (Column column in columns)
            {
                RenderSortButton(writer, column.Title, column.Sort);

                if (!column.Equals(columns[columns.Length-1]))
                {
                    writer.Write(",");
                }
            }

            writer.RenderEndTag(); // Ul

            writer.Write(")");            
        }

        private void RenderTableBody(HtmlTextWriter writer)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.Tbody);

            for (int i = 0, l = MarketReportItems.Count; i < l; i++)
            {
                RenderTableBodyParentRow(writer, MarketReportItems[i], i%2 != 0);
            }

            writer.RenderEndTag(); // Tbody
        }

        private void RenderTableBodyParentRow(HtmlTextWriter writer, MarketReportItem reportItem, bool isOdd)
        {
            string cssClass = StringResource.TableRowParentCssClass;

            if (isOdd)
            {
                cssClass += " " + SR.TableRowOddNumberRowCssClass;
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            RenderTableBodyParentCellExpand(writer, reportItem);

            writer.WriteLine();

            RenderTableBodyCellMarketDaysSupply(writer, reportItem);

            writer.WriteLine();

            RenderTableBodyCellSmallestMarketDaysSupply(writer, reportItem);

            writer.WriteLine();

            RenderTableBodyCellVehicleInformation(writer, reportItem, false);

            writer.WriteLine();

            if (ShowUnitsSold)
            {
                RenderTableBodyCellUnitsSold(writer, reportItem);

                writer.WriteLine();
            }

            RenderTableBodyCellNumberOfListings(writer, reportItem);

            writer.WriteLine();

            RenderTableBodyCellAverageMarketPrice(writer, reportItem);

            writer.WriteLine();

            RenderTableBodyCellAverageMileage(writer, reportItem);

            writer.WriteLine();

            RenderTableBodyCellUnitsInStock(writer, reportItem);

            writer.WriteLine();

            RenderTableBodyCellShoppingList(writer, reportItem);

            writer.WriteLine();

            writer.RenderEndTag(); // Tr

            if (reportItem.MarketReportItems.Count > 0)
            {
                if (reportItem.Expanded)
                {
                    RenderTableBodyChildRows(writer, reportItem.MarketReportItems, isOdd);
                }
            }
        }

        private void RenderTableBodyParentCellExpand(HtmlTextWriter writer, MarketReportItem reportItem)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, StringResource.TableColumnExpandCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Td);

            writer.AddAttribute(HtmlTextWriterAttribute.Type, "submit");

            if (reportItem.Expanded)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Value, StringResource.TableRowExpandedValue, false);
            }
            else
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Value, StringResource.TableRowCollapsedValue, false);
            }

            string id = CreateId(StringResource.TableRowExpandButtonIdPrefix, reportItem.Id.ToString());

            writer.AddAttribute(HtmlTextWriterAttribute.Id, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Name, id);

            writer.RenderBeginTag(HtmlTextWriterTag.Input);

            writer.RenderEndTag(); // Input

            writer.RenderEndTag(); // Td
        }

        private void RenderTableBodyChildCellExpand(HtmlTextWriter writer, MarketReportItem reportItem)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, StringResource.TableColumnExpandCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Td);

            if (reportItem.MarketReportItems.Count > 0)
            {
                RenderHiddenStateInput(writer, reportItem);
            }            

            writer.RenderEndTag(); // Td
        }

        private void RenderHiddenStateInput(HtmlTextWriter writer, MarketReportItem reportItem)
        {
            string id = CreateId(StringResource.TableRowHiddenExpandButtonIdPrefix, reportItem.Id.ToString());

            writer.AddAttribute(HtmlTextWriterAttribute.Type, "hidden");

            writer.AddAttribute(HtmlTextWriterAttribute.Name, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Id, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Value, reportItem.Expanded ? StringResource.TableRowExpandedValue : StringResource.TableRowCollapsedValue);

            writer.RenderBeginTag(HtmlTextWriterTag.Input);

            writer.RenderEndTag(); // Input
        }

        private static void RenderTableBodyCellSmallestMarketDaysSupply(HtmlTextWriter writer, IReportItem reportItem)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.TableColumnMarketDaysSupplyCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Td);

            int? mds = reportItem == null ? null : reportItem.SmallestMarketDaysSupply;

            writer.Write(GetMarketDaysSupplyText(mds));

            writer.RenderEndTag(); // Td
        }

        private void RenderTableBodyCellShoppingList(HtmlTextWriter writer, MarketReportItem reportItem)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.TableColumnShoppingListCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Td);

            if (reportItem != null)
            {
                if (reportItem.Editing)
                {
                    RenderShoppingListEdit(writer, reportItem);
                }
                else
                {
                    RenderShoppingListReadOnly(writer, reportItem);
                } 
            }

            writer.RenderEndTag(); // Td
        }

        private void RenderShoppingListReadOnly(HtmlTextWriter writer, IReportItem reportItem)
        {
            bool inList = reportItem.Quantity != null && reportItem.Quantity != 0;

            bool hasChildrenInList = false;

            if (reportItem.ReportItems != null)
            {
                foreach (IReportItem item in reportItem.ReportItems)
                {
                    if (item.Quantity.HasValue)
                    {
                        hasChildrenInList = true;
                        break;
                    }
                }
            }

            string cssClass = StringResource.TableColumnShoppingListReadonlyCssClass;

            if (inList)
            {
                cssClass += " " + StringResource.TableColumnShoppingListInListCssClass;
            }

            if (hasChildrenInList)
            {
                cssClass += " " + StringResource.TableColumnShoppingChildListInListCssClass;
            }

            string id = CreateId(StringResource.TableRowShoppingListButtonEditIdPrefix, reportItem.Id.ToString());

            writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClass);

            writer.AddAttribute(HtmlTextWriterAttribute.Title, "Add to your Shopping List");

            writer.AddAttribute(HtmlTextWriterAttribute.For, id);

            writer.RenderBeginTag(HtmlTextWriterTag.Label);

            writer.AddAttribute(HtmlTextWriterAttribute.Type, "submit");

            writer.AddAttribute(HtmlTextWriterAttribute.Value, StringResource.TableRowShoppingListValue, false);            

            writer.AddAttribute(HtmlTextWriterAttribute.Id, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Name, id);

            writer.RenderBeginTag(HtmlTextWriterTag.Input);

            writer.RenderEndTag(); // Input

            if (inList)
            {
                writer.Write(" &times; " + reportItem.Quantity);
            }

            writer.RenderEndTag(); // Label
        }

        private void RenderShoppingListEdit(HtmlTextWriter writer, IReportItem reportItem)
        {
            string reportItemId = reportItem.Id.ToString();

            writer.AddAttribute(HtmlTextWriterAttribute.Class, StringResource.TableColumnShoppingListEditCssClass);

            writer.AddAttribute(HtmlTextWriterAttribute.Type, "text");

            int quantity = reportItem.Quantity == null ? 1 : (int)reportItem.Quantity;            
            writer.AddAttribute(HtmlTextWriterAttribute.Value, string.Format("{0:0}", quantity));

            string id = CreateId(StringResource.TableRowShoppingListEditQuantityIdPrefix, reportItemId);

            writer.AddAttribute(HtmlTextWriterAttribute.Id, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Maxlength, MarketReportItem.MaxQuantity.ToString().Length.ToString());

            writer.AddAttribute(HtmlTextWriterAttribute.Name, id);

            writer.RenderBeginTag(HtmlTextWriterTag.Input);

            writer.RenderEndTag(); // Input

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "button");

            writer.RenderBeginTag(HtmlTextWriterTag.Span);

            id = CreateId(StringResource.TableRowShoppingListUpdateButtonIdPrefix, reportItemId);

            writer.AddAttribute(HtmlTextWriterAttribute.Type, "submit");

            writer.AddAttribute(HtmlTextWriterAttribute.Value, StringResource.TableRowShoppingListEditQuantityValue,
                                false);

            writer.AddAttribute(HtmlTextWriterAttribute.Id, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Name, id);

            writer.RenderBeginTag(HtmlTextWriterTag.Input);

            writer.RenderEndTag(); // Input

            writer.RenderEndTag(); // span
        }

        private void RenderTableBodyChildRows(HtmlTextWriter writer, MarketReportItemCollection reportItemCollection,
                                              bool isOdd)
        {
            string baseCssClass = StringResource.TableRowChildCssClass;

            if (isOdd)
            {
                baseCssClass += " " + SR.TableRowOddNumberRowCssClass;
            }

            foreach (MarketReportItem reportItem in reportItemCollection)
            {
                string cssClass = baseCssClass;

                bool hasChildren = reportItem.MarketReportItems.Count > 0;

                if (!reportItem.MarketDaysSupply.HasValue)
                {
                    cssClass += " " + StringResource.TableRowOtherChildCssClass; 
                }

                if (hasChildren)
                {
                    cssClass += " " + StringResource.TableRowOtherParentCssClass;
                }

                writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClass); 

                writer.RenderBeginTag(HtmlTextWriterTag.Tr);

                RenderTableBodyChildCellExpand(writer, reportItem);

                RenderTableBodyCellMarketDaysSupply(writer, hasChildren ? null : reportItem);

                RenderTableBodyCellSmallestMarketDaysSupply(writer, null);

                if (hasChildren)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.TableColumnVehicleInformationCssClass);
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write("Other Equipment - See Details");
                    writer.RenderEndTag(); // Td
                }
                else
                {
                    RenderTableBodyCellVehicleInformation(writer, reportItem, false); 
                }

                if (ShowUnitsSold)
                {
                    RenderTableBodyCellUnitsSold(writer, reportItem); 
                }

                RenderTableBodyCellNumberOfListings(writer, reportItem);

                RenderTableBodyCellAverageMarketPrice(writer, hasChildren ? null : reportItem);

                RenderTableBodyCellAverageMileage(writer, hasChildren ? null : reportItem);

                RenderTableBodyCellUnitsInStock(writer, reportItem);

                RenderTableBodyCellShoppingList(writer, hasChildren ? null : reportItem);

                writer.RenderEndTag(); // Tr

                RenderTableBodyChildRows(writer, reportItem.MarketReportItems, isOdd);
            }
        }

        private void RenderExpandAllButton(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, StringResource.ExpandAllCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Span);

            string id = CreateId(StringResource.ExpandAllId);

            writer.AddAttribute(HtmlTextWriterAttribute.Id, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Name, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Type, "submit");

            if (AreAllRowsExpanded())
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Value, StringResource.CollapseAllValue);
            }
            else
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Value, StringResource.ExpandAllValue);
            }

            writer.RenderBeginTag(HtmlTextWriterTag.Input);

            writer.RenderEndTag(); // Input

            writer.RenderEndTag(); // Span
        }

        private void RenderRowControls(HtmlTextWriter writer)
        {
            string cssClass = StringResource.RowControlsCssClass;

            if (AllowPaging)
            {
                cssClass += " " + StringResource.PagingControlsCssClass;
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            RenderExpandAllButton(writer);

            if (AllowPaging)
            {
                RenderPaging(writer);
            }

            writer.RenderEndTag(); // Div
        }

        #endregion        

        #region Databinding

        protected override void PerformDataBinding(IEnumerable data)
        {
            MarketReportItems.Clear();

            if (data != null)
            {
                IReportItemCollection collection = (IReportItemCollection) data;

                if (AllowFiltering)
                {
                    collection = collection.Filter(_marketReportTableFilterHelper.Filters.ToReportFilterArguments());
                }

                SortExpression sort = DomainModel.Market.SortExpression.SmallestMarketDaysSupply;

                if (AllowSorting)
                {
                    string sortExpression = SortExpressionInternal;

                    if (string.IsNullOrEmpty(sortExpression))
                    {
                        sortExpression = _sortExpression = "SmallestMarketDaysSupply";
                    }

                    if (Enum.IsDefined(typeof(SortExpression), sortExpression))
                    {
                        sort =
                            (SortExpression)
                            Enum.Parse(typeof (SortExpression), sortExpression);

                        ((ReportItemCollection)collection).Sort(sort, SortDirection == SortDirection.Ascending, SortStructure.Self);
                    }
                }

                int startRowIndex, stopRowIndex;

                if (AllowPaging)
                {
                    _pageCount = 1 + (collection.Count/PageSize);

                    startRowIndex = PageIndex*PageSize;

                    stopRowIndex = startRowIndex + PageSize;

                    if (stopRowIndex > collection.Count)
                    {
                        stopRowIndex = collection.Count;
                    }

                    collection = collection.Page(PageIndex, PageSize);
                }
                else
                {
                    _pageCount = 1;

                    startRowIndex = 0;

                    stopRowIndex = collection.Count;
                }

                _rowCount = stopRowIndex - startRowIndex;

                if (AllowSorting)
                {
                    ((ReportItemCollection)collection).Sort(sort, SortDirection == SortDirection.Ascending, SortStructure.Children);
                }

                foreach (IReportItem item in collection)
                {
                    MarketReportItem reportItem = new MarketReportItem(item);
                    MarketReportItems.Add(reportItem);
                    if (_allExpanded)
                    {
                        reportItem.Expanded = true;
                    }
                }
            }
        }

        #endregion

        #region IPostBackDataHandler

        private readonly List<MarketReportItem> _updates = new List<MarketReportItem>();

        private bool _changed;

        private bool _raisedChanged;

        private int? _newPageIndex;

        private bool _newSort;

        private string _newSortExpression;

        private bool _allExpanded;

        private SortDirection _newSortDirection;

        private static bool IsCommand(string text, string command)
        {
            return (string.Equals(command, text, StringComparison.InvariantCulture));
        }

        private MarketReportItem FindReportItem(int id)
        {
            return FindReportItem(id, MarketReportItems);
        }

        private static MarketReportItem FindReportItem(int id, MarketReportItemCollection items)
        {
            foreach (MarketReportItem item in items)
            {
                MarketReportItem value = id == item.Id ? item : FindReportItem(id, item.MarketReportItems);

                if (value != null)
                {
                    return value;
                }
            }

            return null;
        }

        protected bool LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
            if (!postDataKey.StartsWith(UniqueID, StringComparison.InvariantCulture))
            {
                return false;
            }

            int idOffset = UniqueID.Length + 1;

            string commandName = postDataKey.Substring(idOffset);

            string commandValue = postCollection[postDataKey];

            if (IsCommand(commandName, StringResource.ExpandAllId))
            {
                bool expanded = AreAllRowsExpanded();

                foreach (MarketReportItem item in MarketReportItems)
                {
                    item.Expanded = !expanded;
                }
                _allExpanded = !_allExpanded;
                _changed = true;
            }
            else
            {
                int offset = commandName.IndexOf(IdSeparator);

                string rowCommandName = commandName.Substring(0, offset);

                string rowCommandValue = commandName.Substring(offset + 1);

                MarketReportItem item = null;

                int rowId;

                if (int.TryParse(rowCommandValue, out rowId))
                {
                    item = FindReportItem(rowId);
                }

                if (IsCommand(rowCommandName, StringResource.TableRowExpandButtonIdPrefix))
                {
                    if (item != null)
                    {
                        /// Button only post back when it changes
                        item.Expanded = commandValue == StringResource.TableRowCollapsedValue;
                        if(!item.Expanded)
                        {
                            _allExpanded = false;
                        }
                        _changed = true;
                    }
                }
                else if (IsCommand(rowCommandName, StringResource.TableRowHiddenExpandButtonIdPrefix))
                {
                    if (item != null)
                    {
                        /// Hidden inputs always post back
                        item.Expanded = commandValue != StringResource.TableRowCollapsedValue;
                       
                        _changed = true;
                    }
                }
                else if (IsCommand(rowCommandName, StringResource.TableRowShoppingListButtonEditIdPrefix))
                {
                    if (item != null)
                    {
                        item.Editing = !item.Editing;

                        _changed = true;
                    }
                }
                else if (IsCommand(rowCommandName, StringResource.TableRowShoppingListUpdateButtonIdPrefix))
                {
                    if (item != null)
                    {
                        _updates.Add(item);

                        _changed = true;
                    }
                }
                else if (IsCommand(rowCommandName, StringResource.TableRowShoppingListEditQuantityIdPrefix))
                {
                    if (item != null)
                    {
                        int newRowQuantity;

                        if (int.TryParse(commandValue, out newRowQuantity))
                        {
                            item.Quantity = newRowQuantity;

                            _changed = true;
                        }
                    }
                }
                else if (AllowPaging && IsCommand(rowCommandName, SR.PageInputIdPrefix))
                {
                    _newPageIndex = rowId;

                    _changed = true;
                }
                else if (AllowSorting && IsCommand(rowCommandName, "sort"))
                {
                    _newSort = true;

                    if (string.Equals(SortExpressionInternal, rowCommandValue, StringComparison.InvariantCultureIgnoreCase))
                    {
                        _newSortExpression = SortExpressionInternal;
                        _newSortDirection = SortDirectionInternal == SortDirection.Ascending
                                                    ? SortDirection.Descending
                                                    : SortDirection.Ascending;
                    }
                    else
                    {
                        _newSortExpression = rowCommandValue;
                        _newSortDirection = SortDirection.Ascending;                        
                    }

                    _newPageIndex = 0;
                    _changed = true;
                }
            }

            if (_raisedChanged)
            {
                return false;
            }

            if (_changed)
            {
                return (_raisedChanged = _changed);
            }

            return false;
        }

        protected void RaisePostDataChangedEvent()
        {
            foreach (MarketReportItem item in _updates)
            {
                MarketReportTableRowQuantityChangedEventArgs e = new MarketReportTableRowQuantityChangedEventArgs(
                    item,
                    item.Quantity.GetValueOrDefault());

                OnRowQuantityChanged(e);

                // If item's are not in view state we need to redatabind to get updated quantities, PM
                if (!EnableViewState)
                {
                    RequiresDataBinding = true;
                }

                item.Editing = false;
            }

            if (_allExpanded)
            {
                foreach (MarketReportItem item in MarketReportItems)
                {
                    item.Expanded = true;
                }
            }

            _updates.Clear();

            if (_newPageIndex.HasValue)
            {
                PageIndex = _newPageIndex.Value;
            }            

            if (_newSort)
            {
                SortDirectionInternal = _newSortDirection;

                SortExpressionInternal = _newSortExpression;
            }
        }

        bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
            return LoadPostData(postDataKey, postCollection);
        }

        void IPostBackDataHandler.RaisePostDataChangedEvent()
        {
            RaisePostDataChangedEvent();
        }

        #endregion

        #region IStateManager

        protected override void LoadViewState(object savedState)
        {
            if (savedState != null)
            {
                Pair state = savedState as Pair;

                if (state != null)
                {
                    base.LoadViewState(state.First);

                    ((IStateManager) MarketReportItems).LoadViewState(state.Second);
                }
                else
                {
                    base.LoadViewState(savedState);
                }
            }
        }

        protected override object SaveViewState()
        {
            return new Pair(
                base.SaveViewState(),
                ((IStateManager) MarketReportItems).SaveViewState());
        }

        protected override void LoadControlState(object savedState)
        {
            if (savedState != null)
            {
                Pair state = savedState as Pair;

                if (state != null)
                {
                    base.LoadControlState(state.First);

                    object[] propertyState = state.Second as object[];

                    if (propertyState != null)
                    {
                        int idx = 0;

                        _pageIndex = (int) propertyState[idx++];
                        _pageCount = (int) propertyState[idx++];
                        _sortExpression = (string) propertyState[idx++];
                        _sortDirection = (SortDirection) propertyState[idx++];
                        _allExpanded = (bool)propertyState[idx];
                    }
                }
                else
                {
                    base.LoadControlState(savedState);
                }
            }
        }

        protected override object SaveControlState()
        {
            object[] propertyState = new object[5];

            int idx = 0;

            propertyState[idx++] = _pageIndex;
            propertyState[idx++] = _pageCount;
            propertyState[idx++] = _sortExpression;
            propertyState[idx++] = _sortDirection;
            propertyState[idx] = _allExpanded;

            return new Pair(base.SaveControlState(), propertyState);
        }

        #endregion

        #region String Resources

        internal static class StringResource
        {
            public const string CssClass = "market_report_table wrapper";

            public const string TableCssClass = "grid";

            #region TableRows

            public const string TableRowParentCssClass = "parent";

            public const string TableRowChildCssClass = "child";

            public const string TableRowOtherChildCssClass = "other_child";

            public const string TableRowOtherParentCssClass = "other_parent";

            #endregion

            #region Table Columns

            public const string TableColumnExpandCssClass = "expando";

            public const string TableColumnShoppingListReadonlyCssClass = "readonly";

            public const string TableColumnShoppingListInListCssClass = "in_list";

            public const string TableColumnShoppingChildListInListCssClass = "child_in_list";

            public const string TableColumnShoppingListEditCssClass = "edit";

            #endregion

            #region SubmitButtons

            public const string TableRowCollapsedValue = "+";

            public const string TableRowExpandedValue = "&minus;";

            public const string TableRowExpandButtonIdPrefix = "ex";

            public const string TableRowHiddenExpandButtonIdPrefix = "hex";

            public const string TableRowShoppingListValue = "&Xi;";

            public const string TableRowShoppingListButtonEditIdPrefix = "ed";

            public const string TableRowShoppingListEditQuantityIdPrefix = "qu";

            public const string TableRowShoppingListUpdateButtonIdPrefix = "eb";

            public const string TableRowShoppingListEditQuantityValue = "&raquo;";

            public const string ExpandAllId = "expand_all";

            public const string ExpandAllCssClass = "button";

            public const string ExpandAllValue = "Expand all rows";

            public const string CollapseAllValue = "Collapse all rows";

            #endregion

            #region RowControls

            public const string RowControlsCssClass = "row_controls";

            public const string PagingControlsCssClass = "paging_controls";

            #endregion
        }

        #endregion

        #region Implementation of IScriptControl

        private ScriptManager _manager;

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (!DesignMode)
            {
                _manager = ScriptManager.GetCurrent(Page);

                if (_manager != null)
                {
                    _manager.RegisterScriptControl(this);
                }
                else
                {
                    throw new InvalidOperationException("You must have a ScriptManager!");
                }
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);

            if (!DesignMode)
            {
                _manager.RegisterScriptDescriptors(this);
            }
        }

        public IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {
            ScriptBehaviorDescriptor scriptBehaviorDescriptor =
                new ScriptBehaviorDescriptor("FirstLook.Pricing.WebControls.MarketReportTable", ClientID);

            scriptBehaviorDescriptor.AddProperty("shopping_list_edit_class_name", StringResource.TableColumnShoppingListEditCssClass);
            scriptBehaviorDescriptor.AddProperty("parent_row_class_name", StringResource.TableRowParentCssClass);
            scriptBehaviorDescriptor.AddProperty("other_row_parent_class_name", StringResource.TableRowOtherParentCssClass);
            scriptBehaviorDescriptor.AddProperty("other_row_class_name", StringResource.TableRowOtherChildCssClass);
            scriptBehaviorDescriptor.AddProperty("vehicle_info_class_name", SR.TableColumnVehicleInformationCssClass);
            scriptBehaviorDescriptor.AddProperty("expando_class_name", StringResource.TableColumnExpandCssClass);
            scriptBehaviorDescriptor.AddProperty("expanded_value", StringResource.TableRowExpandedValue);
            scriptBehaviorDescriptor.AddProperty("collapsed_value", StringResource.TableRowCollapsedValue);

            return new ScriptDescriptor[] { scriptBehaviorDescriptor };
        }

        public IEnumerable<ScriptReference> GetScriptReferences()
        {
            return new ScriptReference[]
                       {
                           new ScriptReference("FirstLook.Pricing.WebControls.MarketReportTable.js",
                                               "FirstLook.Pricing.WebControls")
                       };
        }

        #endregion
    }
}