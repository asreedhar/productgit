Type.registerNamespace('FirstLook.Pricing.WebControls');

FirstLook.Pricing.WebControls.DocumentEditor = function (element) {
    FirstLook.Pricing.WebControls.DocumentEditor.initializeBase(this, [element]);
    this.ns = FirstLook.Pricing.WebControls.DocumentEditor;
    this.delegates = {};
    var props = [
    "current_mode",
    "allow_new_lines",
    "has_scroll_bars",
    "has_ieSpell_hook",
    "edits_at_cursor",
    "has_footer_text",
    "only_show_toolbar_on_focus",
    "toolbars",
    "click_stream_input",
    "body_textarea",
    "footer_textarea",
    "confirmation_footer_textarea",
    "body_text_element",
    "highlight_details",
    "max_characters",
    // ============
    // = Internal =
    // ============
    "click_stream",
    "last_cursor_input_position",
    "expanding_textarea",
    "footer_edit_stream",
    "has_mouse_over",
    "body_textarea_control",
    "highlighted"
    ];
    for (var i = 0, l = props.length; i < l; i++) {
        this.create_getter_setter(props[i]);
    };
    this.set_click_stream([]);
};
FirstLook.Pricing.WebControls.DocumentEditor.prototype = {
    initialize: function () {
        FirstLook.Pricing.WebControls.DocumentEditor.callBaseMethod(this, "initialize");
        this.set_has_mouse_over(false);
    },
    updated: function () {
        var current_mode = this.get_current_mode(),
        el = this.get_element();

        this.delegates['element_property_change'] = Function.createDelegate(this, this.onPropertyChanged);

        this.add_propertyChanged(this.delegates['element_property_change']);

        if (current_mode === FirstLook.Pricing.WebControls.DocumentEditorMode.Edit || current_mode === FirstLook.Pricing.WebControls.DocumentEditorMode.Insert) {
            this.delegates["this_mouseenter"] = Function.createDelegate(this, this.onMouseOver);
            this.delegates["this_mouseleave"] = Function.createDelegate(this, this.onMouseOut);

            if ('mouseenter' in el) {
                this.add_handler(el, "mouseenter", this.delegates["this_mouseenter"]);
                this.add_handler(el, "mouseleave", this.delegates["this_mouseleave"]);
            } else {
                this.add_handler(el, "mouseover", this.delegates["this_mouseenter"]);
                this.add_handler(el, "mouseout", this.delegates["this_mouseleave"]);
            }

            this.update_body_textarea_control();
            this.update_expanding_textarea();

            setTimeout(Function.createDelegate(this, this.update_toolbar_controls), 0);
            // Psuedothread, ensures Toolbars have a chance to init
        } else
            if (current_mode === FirstLook.Pricing.WebControls.DocumentEditorMode.ReadOnly) {
                this.update_expanding_textarea();
            } else
                if (current_mode === FirstLook.Pricing.WebControls.DocumentEditorMode.Confirmation) {
                    var confirmation_footer_textarea = this.get_confirmation_footer_textarea();

                    this.delegates["footer_textarea_keyup"] = Function.createDelegate(this, this.onFooterKeyUp);
                    this.delegates["footer_textarea_blur"] = Function.createDelegate(this, this.onFooterBlur);

                    this.create_character_count_summary();
                    this.create_summary_radio_group();

                    this.add_handler(confirmation_footer_textarea, "keyup", this.delegates["footer_textarea_keyup"]);
                    this.add_handler(confirmation_footer_textarea, "blur", this.delegates["footer_textarea_blur"]);

                    this.highlight_summary(0);

                }

FirstLook.Pricing.WebControls.DocumentEditor.callBaseMethod(this, "updated");
},
dispose: function () {
    var current_mode = this.get_current_mode();
    this.remove_propertyChanged(this.delegates['element_property_change']);

    if (current_mode === FirstLook.Pricing.WebControls.DocumentEditorMode.Edit ||
        current_mode === FirstLook.Pricing.WebControls.DocumentEditorMode.Insert
        ) {
        var toolbars = this.get_toolbars();

        for (var i = 0, l = toolbars.length; i < l; i++) {
            toolbars[i].set_use_javascript_insert(false);
            toolbars[i].remove_propertyChanged(this.delegates['element_property_change']);
        };
    };

    this.remove_handlers();

    this.delegates = undefined;

    FirstLook.Pricing.WebControls.DocumentEditor.callBaseMethod(this, "dispose");
},
update_toolbar_controls: function () {
    var toolbar_controls = [],
        toolbars = this.get_toolbars(),
        hide_toobar_on_load = this.get_only_show_toolbar_on_focus();

    for (var i = 0, l = toolbars.length; i < l; i++) {
        toolbar_controls[i] = $find(toolbars[i].ControlID);
        Sys.Debug.assert(!!toolbar_controls[i], 'Toolbar Control Must Exist');
        if (toolbar_controls[i].set_use_javascript_insert) {
            toolbar_controls[i].set_use_javascript_insert(true);
        };

        if (hide_toobar_on_load) {
            toolbar_controls[i].hide();
        };
        toolbar_controls[i].add_propertyChanged(this.delegates['element_property_change']);
    };
    this.set_toolbars(toolbar_controls);
},
update_body_textarea_control: function () {
    var body_textarea_control = $create(FirstLook.Pricing.WebControls.DocumentEditor.Textarea, null, null, null, this.get_body_textarea());
    body_textarea_control.set_edits_at_cursor(this.get_edits_at_cursor());
    this.set_body_textarea_control(body_textarea_control);
    body_textarea_control.add_propertyChanged(this.delegates['element_property_change']);
    this.raisePropertyChanged("body_textarea_control");
},
update_expanding_textarea: function () {
    if (!this.get_has_scroll_bars()) {
        var expanding_textarea = $create(FirstLook.Common.WebControls.Extenders.ExpandingTextarea, null, null, null, this.get_body_textarea());
        this.set_expanding_textarea(expanding_textarea);
        expanding_textarea.add_propertyChanged(this.delegates['element_property_change']);
        this.raisePropertyChanged("expanding_textarea");
    };
},
// ==================
// = Methods: start =
// ==================
handle_body_text_update: function (sender, edit_text) {
    var body_textarea = this.get_body_textarea_control();
    if (body_textarea.get_edits_at_cursor()) {
        var click_stream = this.get_click_stream();
        click_stream.push([edit_text, body_textarea.get_cursor_position()]);
        this.raisePropertyChanged("click_stream");

        var name_for_last_click = sender.get_name_for_last_click(),
            click_stream_input = this.get_click_stream_input();

        click_stream_input.value += this.format_click_stream(name_for_last_click, body_textarea.get_cursor_position());

        body_textarea.edit_at_cursor(edit_text);
    } else {
        body_textarea.append_text(edit_text);
        this.raisePropertyChanged("click_stream");
    }
},
handle_footer_text_update: function (edit_text) {
    this.get_footer_textarea().value += " " + edit_text;
},
hide_toolbars_without_focus: function () {
    var has_focus = this.get_body_textarea_control().get_has_focus(),
        has_mouse_over = this.get_has_mouse_over(),
        toolbars = this.get_toolbars();

    if (!has_mouse_over) {
        for (var i = 0, l = toolbars.length; i < l; i++) {
            if (toolbars[i].get_has_mouse_over()) {
                has_mouse_over = true;
            }
        };
    };

    if (!has_focus && !has_mouse_over) {
        this.hide_toolbars();
    }
},
show_toolbars: function () {
    var toolbars = this.get_toolbars();
    for (var i = 0; i < toolbars.length; i++) {
        if (!toolbars[i].get_is_visible()) {
            toolbars[i].show();
        };
    };
},
hide_toolbars: function () {
    var toolbars = this.get_toolbars();
    for (var i = 0; i < toolbars.length; i++) {
        if (toolbars[i].get_is_visible()) {
            toolbars[i].hide();
        };
    };
},

focus: function () {
    this.get_body_textarea_control().focus();
    this.moveCaretToEnd(this.get_body_textarea_control().get_element());
    // Work around Chrome's little problem
    //window.setTimeout(function() {
    //this.moveCaretToEnd(this.get_body_textarea_control().get_element());
    //}, 1);

},
moveCaretToEnd: function (el) {
    if (typeof el.selectionStart == "number") {
        el.selectionStart = el.selectionEnd = el.value.length;
    } else if (typeof el.createTextRange != "undefined") {
        el.focus();
        var range = el.createTextRange();
        range.collapse(false);
        range.select();
    }
},
get_has_focus: function () {
    return (this.get_body_textarea_control()) ? this.get_body_textarea_control().get_has_focus() : false;
},
// = Methods: end =
// =================
// = Events: start =
// =================
onMouseOver: function (e) {
    this.set_has_mouse_over(true);
},
onMouseOut: function (e) {
    this.set_has_mouse_over(false);
},
onFooterBlur: function (e) {
    this.highlight_summary(this.get_highlighted() || 0);
},
onFooterKeyUp: function (e) {
    var character_count_element = this.get_character_count_element(),
        count;
    if (!!character_count_element) {
        count = this.get_character_count();

        this.set_text_value(character_count_element, count);

        this.highlight_character_count_summary();
    };
},
onHightlightRadioButtonClick: function (e) {
    var el = e.target;
    if (e.target.value !== undefined) {
        this.highlight_summary(parseInt(e.target.value, 10) || 0);
    };
},
onPropertyChanged: function (sender, args) {
    var property = args.get_propertyName(),
        source_class = sender.get_name();


    Sys.Debug.assert(!!!FirstLook.Pricing.WebControls.DocumentEditor["debug_" + property], property);

    if (property == "body_text_for_last_click") {
        if (sender.get_use_javascript_insert()) {
            this.handle_body_text_update(sender, sender.get_body_text_for_last_click());
        };
    } else
        if (property == "footer_text_for_last_click") {
            if (sender.get_use_javascript_insert()) {
                this.handle_footer_text_update(sender.get_footer_text_for_last_click());
            };
        } else
            if (property == "edits_at_cursor") {
                this.get_body_textarea_control().set_edits_at_cursor(this.get_edits_at_cursor());
            } else
                if (property == "targetHeight" && sender != this) {
                    this.raisePropertyChanged("targetHeight");
                } else
                    if (property == "currentHeight" && sender != this) {
                        this.raisePropertyChanged("currentHeight");
                    } else
                        if (property == "has_focus" && sender != this) {
                            this.raisePropertyChanged("has_focus");
                        };

if (this.get_only_show_toolbar_on_focus()) {
    this.delegates["hide_toolbars_without_focus"] = Function.createDelegate(this, this.hide_toolbars_without_focus);
    if (property == "has_focus" && sender.get_has_focus()) {
        this.show_toolbars();
    } else if ((property == "has_mouse_over" && !sender.get_has_mouse_over()) ||
            (property == "has_focus" && !sender.get_has_focus())) {
        setTimeout(this.delegates["hide_toolbars_without_focus"], 250);
    }
}

if (!!FirstLook.Pricing.WebControls.DocumentEditor.Trace || !!FirstLook.Pricing.WebControls.DocumentEditor["trace_" + property]) {
    Sys.Debug.traceDump(sender["get_" + property](), sender.get_id() + ":" + property);
};
},
// = Events: end =
// =============================
// = Expanding Textarea: start =
// =============================
set_targetHeight: function (value) {
    var expanding_textarea = this.get_expanding_textarea();

    if (!!expanding_textarea) {
        expanding_textarea.set_targetHeight(value);
    }
},
get_targetHeight: function () {
    var height = this.get_currentHeight(),
        expanding_textarea = this.get_expanding_textarea();

    if (!!expanding_textarea) {
        return expanding_textarea.get_targetHeight();
    } else {
        return height;
    }
},
get_currentHeight: function () {
    return this.get_element().offsetHeight;
},
// = Expanding Textarea: end =
// ============================
// = Confirmation Mode: start =
// ============================
set_highlight_details: function (value) {
    if (typeof value === "string") {
        try {
            this._highlight_details = eval(value);
        } catch (err) {
            throw new Error([value + "is invalid", err]);
        }
    } else {
        this._highlight_details = value;
        this.raisePropertyChanged("highlight_details");
    }
},
get_character_count: function () {
    // ===========================================================
    // = Retreive number of characters in both Ad and Disclaimer =
    // ===========================================================
    var disclaimer_textarea = this.get_confirmation_footer_textarea(),
        text_el = this.get_body_text_element(),
        count = 0;

    if (disclaimer_textarea !== null)
        count += this.get_text_value(disclaimer_textarea).length;

    count += this.get_text_value(text_el).length;

    this.create_getter_setter("character_count");
    this.set_character_count(count);

    return count;
},
highlight_summary: function (length) {
    // ====================================================
    // = Set the CssClass for the highlight in the body_text =
    // ===================================================
    var confirm_element = this.get_body_text_element(),
        max_length = this.get_max_characters(),
        body_text = this.get_text_value(confirm_element),
        character_count = this.get_character_count(),
        clone,
        hightlight_text,
        over_text,
        middle_text,
        hightlight_text_node,
        middle_text_node,
        over_text_node,
        highlight_el,
        over_text_el,
        parent,
        scroll_top,
        dom_fragment,
        has_over_text = max_length < character_count;

    this.set_highlighted(length);

    hightlight_text = body_text.substring(0, length);

    if (has_over_text) {
        var overMark = max_length - (character_count - body_text.length);
        middle_text = body_text.substring(length, overMark);
        over_text = body_text.substring(overMark, body_text.length);
    } else {
        middle_text = body_text.substring(length, body_text.length);
    }

    dom_fragment = document.createDocumentFragment();

    hightlight_text_node = document.createTextNode(hightlight_text);
    middle_text_node = document.createTextNode(middle_text);

    clone = confirm_element.cloneNode(false);
    scroll_top = confirm_element.scrollTop;

    highlight_el = document.createElement("span");

    highlight_el.className = "highlight_text";

    highlight_el.appendChild(hightlight_text_node);

    clone.appendChild(highlight_el);
    clone.appendChild(middle_text_node);

    if (has_over_text) {
        over_text_node = document.createTextNode(over_text);
        over_text_el = document.createElement("span");
        over_text_el.className = "over_limit_text";
        over_text_el.appendChild(over_text_node);
        clone.appendChild(over_text_el);
    }

    dom_fragment.appendChild(clone);

    parent = confirm_element.parent || confirm_element.parentElement;

    parent.replaceChild(dom_fragment, confirm_element);

    clone.scrollTop = scroll_top;

    this.set_body_text_element($get(clone.id));

},
create_summary_radio_group: function () {
    // ============================================
    // = append the radio group for the ad length =
    // ============================================
    var summary_radio_group,
        body_text_element,
        parent,
        highlight_details = this.get_highlight_details(),
        base_element_id = this.get_element().id,
        options_wrapper;

    // ==================================================
    // = Not all text areas will define highlight areas =
    // ==================================================
    if (highlight_details == undefined)
        return;

    var create_id = function (i) {
        return base_element_id + "$highlights_" + i;
    };
    // ===========================================================
    // = I Hate You IE, you don't like form elements inserted in
    // = DOM standard way, so I must use your flaky innerHTML,
    // = Douh! Paul
    // ===========================================================
    body_text_element = this.get_body_text_element();
    parent = body_text_element.parent || body_text_element.parentElement;
    summary_radio_group = document.createDocumentFragment();
    options_wrapper = document.createElement("p");
    options_wrapper.className = "preview_options";

    for (var i = 0, l = highlight_details.length; i < l; i++) {
        var label_textNode = document.createTextNode(highlight_details[i].Label),
            label_element = document.createElement("label"),
            input_element = [];

        input_element.push("<input type='radio' name='highlight'");
        input_element.push("value='" + highlight_details[i].PreviewLength + "'");
        input_element.push("title='" + highlight_details[i].Description + "'");
        input_element.push("id='" + create_id(i) + "'");
        input_element.push("/>");

        label_element.appendChild(label_textNode);
        label_element.innerHTML += input_element.join(" ");

        options_wrapper.appendChild(label_element);
    };

    summary_radio_group.appendChild(options_wrapper);

    parent.insertBefore(summary_radio_group, body_text_element.nextSibling);

    this.delegates["Summary_highlight"] = Function.createDelegate(this, this.onHightlightRadioButtonClick);
    for (var i = 0, l = highlight_details.length; i < l; i++) {
        this.add_handler($get(create_id(i)), 'click', this.delegates["Summary_highlight"]);
    };
},
create_character_count_summary: function () {
    // ========================================
    // = append the character count interface =
    // ========================================
    var character_count_fragment = document.createDocumentFragment(),
        description_textNode = document.createTextNode("character count: "),
        wrapper_el = document.createElement("p"),
        character_count_el = document.createElement("span"),
        character_count_textNode = document.createTextNode(this.get_character_count()),
        disclaimer_textarea = this.get_confirmation_footer_textarea(),
        parent = disclaimer_textarea.parent || disclaimer_textarea.parentElement;

    wrapper_el.className = "character_count";
    character_count_el.className = "character_count_value";

    wrapper_el.appendChild(description_textNode);
    character_count_el.appendChild(character_count_textNode);
    wrapper_el.appendChild(character_count_el);
    character_count_fragment.appendChild(wrapper_el);

    this.create_getter_setter("character_count_element");
    this.set_character_count_element(character_count_el);

    parent.appendChild(character_count_fragment);

    this.highlight_character_count_summary();
},
highlight_character_count_summary: function () {
    // ===========================================================
    // = Updates character count summary to hightligh character
    // = counts over max
    // ===========================================================
    var character_count_element = this.get_character_count_element(),
        count = parseInt(this.get_text_value(character_count_element), 10),
        max_length = this.get_max_characters(),
        cssClass = "character_count_value_over";

    if (!isNaN(count) && count > max_length) {
        Sys.UI.DomElement.addCssClass(character_count_element, cssClass);
    } else {
        Sys.UI.DomElement.removeCssClass(character_count_element, cssClass);
    }
},
// = Confirmation Mode: end =
// =========================
// = Helper Methods: start =
// =========================
create_getter_setter: function (property) {
    // =================================================================
    // = Generic Function to create AJAX.net style getters and setters =
    // =================================================================
    if (this["get_" + property] === undefined) {
        this["get_" + property] = (function (thisProp) {
            return function get_property() {
                return this[thisProp];
            };
        })("_" + property);
    };
    if (this["set_" + property] === undefined) {
        this["set_" + property] = (function (thisProp, raiseType) {
            return function set_property(value) {
                if (value !== this[thisProp]) {
                    this[thisProp] = value;
                    this.raisePropertyChanged(raiseType);
                };
            };
        })("_" + property, property);
    };
},
add_handler: function (el, type, fn) {
    if (this._eventCache === undefined) {
        this._eventCache = [];
    }
    this._eventCache.push([el, type, fn]);
    $addHandler(el, type, fn);
},
remove_handlers: function () {
    if (this._eventCache !== undefined) {
        for (var i = 0, l = this._eventCache.length; i < l; i++) {
            $removeHandler(this._eventCache[i][0], this._eventCache[i][1], this._eventCache[i][2]);
            this._eventCache[i][0] = undefined;
            this._eventCache[i][1] = undefined;
            this._eventCache[i][2] = undefined;
        };
        delete this._eventCache;
    };
},
get_computed_style: function (el, style) {
    if (!!el.currentStyle) {
        return el.currentStyle[style];
    } else if (!!document.defaultView && !!document.defaultView.getComputedStyle) {
        return document.defaultView.getComputedStyle(el, "")[style];
    } else {
        return el.style[style];
    }
},
fire_event: function (el, ev) {
    if (document.createEvent) {
        var new_event = document.createEvent('HTMLEvents');
        new_event.initEvent(ev, true, false);

        el.dispatchEvent(new_event);
    }
    else if (document.createEventObject) {
        el.fireEvent('on' + ev);
    }
},
get_text_value: function (el) {
    return el.value || el.textContent || el.innerText;
},
set_text_value: function (el, text) {
    if (el.value) {
        el.value = text;
    } else if (el.textContent) {
        el.textContent = text;
    } else if (el.innerText) {
        el.innerText = text;
    };
},
format_click_stream: function (text, pos) {
    return text + "," + pos + ";";
}
// = Helper Methods: end =
};

if (FirstLook.Pricing.WebControls.DocumentEditor.registerClass != undefined)
    FirstLook.Pricing.WebControls.DocumentEditor.registerClass('FirstLook.Pricing.WebControls.DocumentEditor', Sys.UI.Behavior);


FirstLook.Pricing.WebControls.DocumentEditor.Textarea = function (element) {
    FirstLook.Pricing.WebControls.DocumentEditor.Textarea.initializeBase(this, [element]);
    this.delegates = {};
    var props = [
    "text",
    "has_focus",
    "edits_at_cursor",
    "allow_new_lines"
    ];
    for (var i = 0, l = props.length; i < l; i++) {
        this.create_getter_setter(props[i]);
    };

};
FirstLook.Pricing.WebControls.DocumentEditor.Textarea.prototype = {
    initialize: function () {
        FirstLook.Pricing.WebControls.DocumentEditor.Textarea.callBaseMethod(this, "initialize");
    },
    updated: function () {
        var el = this.get_element();

        this.delegates["element_focus"] = Function.createDelegate(this, this.onFocus);
        this.delegates["element_blur"] = Function.createDelegate(this, this.onBlur);
        this.delegates["element_keyDown"] = Function.createDelegate(this, this.onKeyDown);
        this.delegates["element_propertChange"] = Function.createDelegate(this, this.onPropertyChanged);

        this.add_handler(el, 'focus', this.delegates["element_focus"]);
        this.add_handler(el, 'blur', this.delegates["element_blur"]);
        this.add_handler(el, 'keyDown', this.delegates["element_keyDown"]);

        this.add_propertyChanged(this.delegates["element_propertChange"]);

        this.set_text(el.textContent || el.innerText || "");

        FirstLook.Pricing.WebControls.DocumentEditor.Textarea.callBaseMethod(this, "updated");
    },
    dispose: function () {
        FirstLook.Pricing.WebControls.DocumentEditor.Textarea.callBaseMethod(this, "dispose");
    },

    // =======================
    // = Text Methods: start =
    // =======================
    append_text: function (value) {
        var current_text = this.get_text(),
        formated_value = this.format_append_padding(value, current_text);
        this.set_text(current_text + formated_value);
    },
    format_append_padding: function (src, dest) {
        // =============================================================
        // = Logic to ensure Buttons always have a space, and will not
        // = insert double spaces before str
        // =============================================================
        if (dest.length > 1) {
            var srcFirstChar = src.substr(0, 1),
            destValueLastChar = dest.substr(dest.length - 1, 1);
            if (srcFirstChar !== " " && destValueLastChar !== " ") {
                src = " " + src;
            }

            if (srcFirstChar === " " && destValueLastChar === " ") {
                src = substr(0, src.length - 1);
            }
        }

        return src;
    },
    edit_at_cursor: function (new_text) {
        var el = this.get_element(),
        range,
        selection;

        el.focus();

        range = this.get_text_range();
        selection = this.get_selection_range();

        if (selection && selection.text !== undefined && selection.text !== "") {
            // =============================
            // = Replace Current Selection =
            // =============================
            selection.text = new_text;
            selection.collapse(false);
            selection.select();
            //this.set_text(el.value);
            this.set_text(new_text);
        } else {
            var pos = this.get_cursor_position();

            var string_builder = [el.value.substr(0, pos), new_text, el.value.substr(pos, el.value.length)];

            for (var i = 0, l = string_builder.length; i < l; i++) {
                string_builder[i] = string_builder[i].replace(/^\ \ */, '').replace(/\ \ *$/, '');
            }

            var cursor_end_position = string_builder[0].length + string_builder[1].length + 1;

            for (i = 0, l = string_builder.length; i < l; i++) {
                if (string_builder[i] === "") {
                    string_builder.splice(i, 1);
                }
            }

            el.value = string_builder.join(" ");
            this.set_text(el.value);
            if (range && range.move) {
                range.move("character", cursor_end_position);
                range.select();
            }
        }

        this.focus();
    },
    // = Text Methods: end =
    // =========================
    // = Cursor Control: start =
    // =========================
    get_text_range: function () {
        var el = this.get_element();
        if (!!el.createTextRange) {
            return this.get_element().createTextRange();
        }
        else {
            var rangeObj = document.createRange();
            rangeObj.selectNodeContents(el);
            return rangeObj;

        };
    },
    get_selection_range: function () {
        if (!!document.selection) {
            return document.selection.createRange();
        }
        else {
            return window.getSelection();
        };
    },
    get_cursor_position: function () {
        var el = this.get_element();
        el.focus();
        var range = this.get_text_range(),
        selection = this.get_selection_range()
        //div = document.createElement('div'),
        //tempDoc = document.createDocumentFragment(),
        // tempTextarea;

        try {
            selection.moveStart('character', -el.value.length);

            if (selection.text == el.value) {
                return selection.text.length;
            };
        } catch (err) {

        }
        if (el && el.selectionStart) {

            return el.selectionStart;
        }
        else {
            return 0;
        }
        //        try {
        //            div.innerHTML = selection.htmlText;
        //        } catch(err) {
        //            Sys.Debug.trace(err);
        //		if(selection)
        //		{
        //            		Sys.Debug.trace('Malformed selectionRange.htmlText:\n' + selectionRange.htmlText + '--');
        //		}
        //        }
        //try{
        //        tempDoc.appendChild(div);

        //        tempTextarea = tempDoc.getElementById(el.id);
        //}catch(err)
        //{Sys.Debug.trace(err);}
        //        if (tempTextarea) {
        //            return tempTextarea.value.length;
        //       } else {
        //            return 0;
        //        }
    },
    // = Cursor Control: end =
    // =================
    // = Events: start =
    // =================
    onFocus: function (e) {
        this.set_has_focus(true);
    },
    onBlur: function (e) {
        this.set_has_focus(false);
    },
    onKeyDown: function (e) {
        // ===================================
        // = Don't allow New line in the ads
        // = 13 === [ Enter ]
        // ===================================
        if (e.keyCode === 13) {
            e.preventDefault();
        }
    },
    onPropertyChanged: function (sender, args) {
        var type = args.get_propertyName();

        if (type == "text" && !this.get_edits_at_cursor()) {
            this.get_element().value = this.get_text();
        };
    },
    focus: function () {
        this.fire_event(this.get_element(), 'focus');
    },
    blur: function () {
        this.fire_event(this.get_element(), 'blur');
    },
    // = Events: end =
    // =========================
    // = Helper Methods: start =
    // =========================
    create_getter_setter: FirstLook.Pricing.WebControls.DocumentEditor.prototype.create_getter_setter,
    add_handler: FirstLook.Pricing.WebControls.DocumentEditor.prototype.add_handler,
    removeHandler: FirstLook.Pricing.WebControls.DocumentEditor.prototype.remove_handlers,
    fire_event: FirstLook.Pricing.WebControls.DocumentEditor.prototype.fire_event
    // = Helper Methods: start =
};

if (FirstLook.Pricing.WebControls.DocumentEditor.Textarea.registerClass != undefined)
    FirstLook.Pricing.WebControls.DocumentEditor.Textarea.registerClass('FirstLook.Pricing.WebControls.DocumentEditor.Textarea', Sys.UI.Behavior);

FirstLook.Pricing.WebControls.DocumentEditorMode = function () { };
FirstLook.Pricing.WebControls.DocumentEditorMode.prototype = {
    ReadOnly: 0,
    Insert: 1,
    Edit: 2,
    Confirmation: 3
};

if (FirstLook.Pricing.WebControls.DocumentEditorMode.registerEnum != undefined)
    FirstLook.Pricing.WebControls.DocumentEditorMode.registerEnum('FirstLook.Pricing.WebControls.DocumentEditorMode');

Sys.Application.notifyScriptLoaded();