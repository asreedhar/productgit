Type.registerNamespace('FirstLook.Pricing.WebControls');

FirstLook.Pricing.WebControls.Toolbar = function(element) {
    FirstLook.Pricing.WebControls.Toolbar.initializeBase(this, [element]);
    this.delegates = {};
    var props = [
    "click_stream_input",
    "full_body_text_class",
    "full_footer_text_class",
    // ============
    // = Internal =
    // ============
    "hide_dropdown_after_click",
    "is_visible",
    "click_stream",
    "has_mouse_over",
    "use_javascript_insert",
    "name_for_last_click",
    "body_text_for_last_click",
    "footer_text_for_last_click"
    ];
    for (var i = 0, l = props.length; i < l; i++) {
        this.create_getter_setter(props[i]);
    };
    this.set_hide_dropdown_after_click(true);
    this.set_click_stream([]);
};
FirstLook.Pricing.WebControls.Toolbar.prototype = {
    initialize: function() {
        FirstLook.Pricing.WebControls.Toolbar.callBaseMethod(this, "initialize");
    },
    updated: function() {
        FirstLook.Pricing.WebControls.Toolbar.callBaseMethod(this, "updated");
        var el = this.get_element();

        this.delegates['element_mouseover'] = Function.createDelegate(this, this.onMouseOver);
        this.delegates['element_mouseout'] = Function.createDelegate(this, this.onMouseOut);
        this.delegates['element_mouseenter'] = Function.createDelegate(this, this.onMouseEnter);
        this.delegates['element_mouseleave'] = Function.createDelegate(this, this.onMouseLeave);
        this.delegates['element_click'] = Function.createDelegate(this, this.onClick);

        if ('onmouseenter' in el && 'onmouseleave' in el) {
            this.add_handler(el, 'mouseenter', this.delegates['element_mouseenter']);
            this.add_handler(el, 'mouseleave', this.delegates['element_mouseleave']);
        } else {
            this.add_handler(el, 'mouseover', this.delegates['element_mouseenter']);
            this.add_handler(el, 'mouseout', this.delegates['element_mouseleave']);
        }

        this.add_handler(el, 'mouseover', this.delegates['element_mouseover']);
        this.add_handler(el, 'mouseout', this.delegates['element_mouseout']);
        this.add_handler(el, 'click', this.delegates['element_click']);

        this.delegates['property_changed'] = Function.createDelegate(this, this.onPropertyChanged);
        this.add_propertyChanged(this.delegates['property_changed']);

        this.update_disabled_buttons();
    },
    dispose: function() {
        this.remove_propertyChanged(this.delegates['property_changed']);

        this.remove_handlers();

        this.delegates = undefined;

        FirstLook.Pricing.WebControls.Toolbar.callBaseMethod(this, "dispose");
    },

    // ==============================
    // = ClickStream Methods: start =
    // ==============================
    push_click_stream: function(value) {
        this.get_click_stream().push(value);
        this.raisePropertyChanged("click_stream");
    },
    set_name_for_last_click: function(value) {
        // ===========================================
        // = We need to always raise this property   =
        // = change even if it is the same value, PM =
        // ===========================================
        this._name_for_last_click = value;
        this.raisePropertyChanged("name_for_last_click");
    },
    set_body_text_for_last_click: function(value) {
        // ===========================================
        // = We need to always raise this property   =
        // = change even if it is the same value, PM =
        // ===========================================
        this._body_text_for_last_click = value;
        this.raisePropertyChanged("body_text_for_last_click");
    },
    set_footer_text_for_last_click: function(value) {
        // ===========================================
        // = We need to always raise this property   =
        // = change even if it is the same value, PM =
        // ===========================================
        this._footer_text_for_last_click = value;
        this.raisePropertyChanged("footer_text_for_last_click");
    },
    // = ClickStream Methods: End =
    // ============================
    // = Hide Show Methods: start =
    // ============================
    hide: function() {
        this.get_element().style.display = 'none';
        this.set_is_visible(false);
    },
    show: function() {
        this.get_element().style.display = 'block';
        this.set_is_visible(true);
    },
    // = Hide Show Methods: start =
    // =============================
    // = Get Mouse Tracking: start =
    // =============================
    resetHovers: function() {
    	if (! !!this.get_element()) return;
        var lis = this.get_element().getElementsByTagName('li');
        for (var i = 0, l = lis.length; i < l; i++) {
            Sys.UI.DomElement.removeCssClass(lis[i], 'hover');
        };
    },
    resetHoversExcept: function(el) {
        var sibling = el.parentNode.childNodes;
        for (var i = 0, l = sibling.length; i < l; i++) {
            if (sibling[i].nodeType == 1 && sibling[i].tagName == 'LI' && sibling[i] != el) {
                Sys.UI.DomElement.removeCssClass(sibling[i], 'hover');
            }
        };
    },
    onHoverIntent: function(el, e) {
        this._hover_intent_el = el;
        this._hover_intent_x = e.screenX;
        this._hover_intent_y = e.screenY;
        clearTimeout(this._hover_intent_timout);
        this._hover_intent_timout = setTimeout((function(scope, el, x, y) {
            return function() {
                if (scope._hover_intent_el === el && scope.get_has_mouse_over()) {
                    var delta_x = Math.abs(x - scope._hover_intent_x),
                    delta_y = Math.abs(y - scope._hover_intent_y);
                    if ((delta_y < 4 || delta_x < 4) && el.className.indexOf("disabled") == -1) {
                        Sys.UI.DomElement.addCssClass(el, 'hover');
                    };
                }
            };
        })(this, el, e.screenX, e.screenY), 350);
    },
    onMouseOver: function(e) {
        if (!!!e.target) {
            return;
        };
        var el = e.target;
        if (el.tagName.toUpperCase() == "INPUT") {
            var parent = el.parent || el.parentElement;
            if (parent.tagName.toUpperCase() == "LI") {
                el = parent;
            };
        };
        if (el.tagName.toUpperCase() == 'LI') {
            this._current_hover_element = el;
            this.onHoverIntent(el, e);
            this.resetHoversExcept(el);
        }
    },
    onMouseOut: function(e) {
        if (!!!e.target) {
            return;
        };
        if (e.target.tagName.toUpperCase() == 'LI') {
            setTimeout((function(scope, ev) {
                return function() {
                    if (scope._current_hover_element == ev.target) {
                        Sys.UI.DomElement.removeCssClass(ev.target, 'hover');
                        scope._current_hover_element = undefined;
                    };
                };
            })(this, e), 250);
        };
    },
    onMouseEnter: function(e) {
        this.set_has_mouse_over(true);
    },
    onMouseLeave: function(e) {
        this.set_has_mouse_over(false);
    },
    onClick: function(e) {
        if (e.target.tagName.toUpperCase() == 'INPUT') {

            // =================================
            // = Short Circuit Disabled Buttons =
            // =================================
            if (e.target.parentNode.className.match("disabled")) {
                e.preventDefault();
                e.stopPropagation();
                return;
            };

            var siblings = e.target.parentNode.childNodes,
            body_class = this.get_full_body_text_class(),
            footer_class = this.get_full_footer_text_class(),
            suppress_postback = this.get_use_javascript_insert();

            for (var i = 0, l = siblings.length; i < l; i++) {
                if (siblings[i].className.indexOf(body_class) !== -1) {
                    this.set_name_for_last_click(this.buttonIdToCommandName(e.target.name));
                    this.set_body_text_for_last_click(siblings[i].textContent || siblings[i].innerText);
                    suppress_postback = this.get_use_javascript_insert();
                };
                if (siblings[i].className.indexOf(footer_class) !== -1) {
                    this.set_name_for_last_click(this.buttonIdToCommandName(e.target.name));
                    this.set_footer_text_for_last_click(siblings[i].textContent || siblings[i].innerText);
                    suppress_postback = this.get_use_javascript_insert();
                };
            };
            if (suppress_postback) {
                this.push_click_stream(e.target.name);
                e.preventDefault();
                e.stopPropagation();
            };
            if (this.get_hide_dropdown_after_click()) {
                this.resetHovers();
            };
        };
    },
    onPropertyChanged: function(sender, args) {
        var property = args.get_propertyName();

        Sys.Debug.assert(!!!FirstLook.Pricing.WebControls.Toolbar["debug_" + property], property);

        if (property == 'has_mouse_over') {
            if (!this.get_has_mouse_over()) {
                setTimeout(Function.createDelegate(this,
                function() {
                    if (!this.get_has_mouse_over()) {
                        this.resetHovers();
                    };
                }), 250);
            };
        };
        if (property == 'click_stream') {
            this.get_click_stream_input().value = this.get_click_stream();
        };
        if ( !! FirstLook.Pricing.WebControls.Toolbar.Trace || !!FirstLook.Pricing.WebControls.Toolbar["trace_" + property]) {
            Sys.Debug.traceDump(sender["get_" + property](), sender.get_id() + ":" + property);
        };
    },
    // = Get Mouse Tracking: end =
    // =========================
    // = Helper Methods: start =
    // =========================
    create_getter_setter: function(property) {
        // =================================================================
        // = Generic Function to create AJAX.net style getters and setters =
        // =================================================================
        if (this["get_" + property] === undefined) {
            this["get_" + property] = (function(thisProp) {
                return function get_property() {
                    return this[thisProp];
                };
            })("_" + property);
        };
        if (this["set_" + property] === undefined) {
            this["set_" + property] = (function(thisProp, raiseType) {
                return function set_property(value) {
                    if (value !== this[thisProp]) {
                        this[thisProp] = value;
                        this.raisePropertyChanged(raiseType);
                    };
                };
            })("_" + property, property);
        };
    },
    add_handler: function(el, type, fn) {
        if (this._eventCache === undefined) {
            this._eventCache = [];
        }
        this._eventCache.push([el, type, fn]);
        $addHandler(el, type, fn);
    },
    remove_handlers: function() {
        if (this._eventCache !== undefined) {
            for (var i = 0, l = this._eventCache.length; i < l; i++) {
                $removeHandler(this._eventCache[i][0], this._eventCache[i][1], this._eventCache[i][2]);
                this._eventCache[i][0] = undefined;
                this._eventCache[i][1] = undefined;
                this._eventCache[i][2] = undefined;
            };
            delete this._eventCache;
        };
    },
    buttonIdToCommandName: function(id) {
        var elementID = this.get_element().id;
        return id.substring(elementID.length + 1);
    },
    update_disabled_buttons: function() {
        var inputs = this.get_element().getElementsByTagName('input');
        for (var i = inputs.length - 1; i >= 0; i--) {
            if (inputs[i].disabled) {
                Sys.UI.DomElement.addCssClass(inputs[i], 'disabled');
                inputs[i].disabled = false;
            };
        };
    }
    // = Helper Methods: start =
};

if (FirstLook.Pricing.WebControls.Toolbar.registerClass != undefined)
 FirstLook.Pricing.WebControls.Toolbar.registerClass("FirstLook.Pricing.WebControls.Toolbar", Sys.UI.Behavior);

Sys.Application.notifyScriptLoaded();
