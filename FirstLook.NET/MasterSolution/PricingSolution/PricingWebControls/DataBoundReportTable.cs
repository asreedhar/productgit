using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Pricing.DomainModel.Market;

namespace FirstLook.Pricing.WebControls
{
    public abstract class DataBoundReportTable : DataBoundControl
    {
        #region Render Table Cell Methods

        private bool _openSearchAsPopUp;

        public bool OpenSearchAsPopUp
        {
            get { return _openSearchAsPopUp; }
            set { _openSearchAsPopUp = value; }
        }

        private int _distanceBucketID;

        public int DistanceBucketID
        {
            get
            {
                return _distanceBucketID;
            }
            set
            {
                if (value != _distanceBucketID)
                {
                    _distanceBucketID = value;
                }
            }
        }

        public static void RenderTableBodyCellMarketDaysSupply(HtmlTextWriter writer, IReportItem reportItem)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.TableColumnMarketDaysSupplyCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Td);

            int? mds = reportItem == null ? null : reportItem.MarketDaysSupply;

            writer.Write(GetMarketDaysSupplyText(mds));

            writer.RenderEndTag(); // Td
        }

        public static string GetMarketDaysSupplyText(int? marketDaysSupply)
        {
            if (marketDaysSupply.HasValue)
            {
                return marketDaysSupply.GetValueOrDefault() < 180
                               ? string.Format("{0:#,0;#,0;--}", marketDaysSupply)
                               : "180+"; 
            }
            return "--";
        }

        public virtual void RenderTableBodyCellVehicleInformation(HtmlTextWriter writer, IReportItem reportItem, bool alwaysRenderVehicle)
        {
            bool hasConfiguration = (!string.IsNullOrEmpty(reportItem.Series) ||
                                     !string.IsNullOrEmpty(reportItem.DriveTrain) ||
                                     !string.IsNullOrEmpty(reportItem.Engine) ||
                                     !string.IsNullOrEmpty(reportItem.FuelType) ||
                                     !string.IsNullOrEmpty(reportItem.Transmission));

            string cssClass = SR.TableColumnVehicleInformationCssClass;

            if (hasConfiguration)
            {
                cssClass += " has_configuration";
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Td);

            writer.WriteLine();

            if (alwaysRenderVehicle || !hasConfiguration)
            {
                RenderVehicle(writer, reportItem);
            }

            if (hasConfiguration)
            {
                RenderVehicleConfiguration(writer, reportItem); 
            }

            writer.RenderEndTag(); // Td
        }

        public static void RenderVehicle(HtmlTextWriter writer, IReportItem reportItem)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ModelYearCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Span);

            writer.Write(reportItem.ModelYear);

            writer.RenderEndTag(); // Span.SR.ModelYearCssClass

            writer.WriteLine();

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.MakeCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Span);

            writer.Write(reportItem.Make);

            writer.WriteLine();

            writer.RenderEndTag(); // Span.SR.MakeCssClass

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.ModelFamilyCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Span);

            writer.Write(reportItem.ModelFamily);

            writer.RenderEndTag(); // Span.SR.ModelFamilyCssClass

            writer.WriteLine();

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.SegmentCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Span);

            writer.Write(reportItem.Segment);

            writer.RenderEndTag(); // Span.SR.SegementCssClass

            writer.WriteLine();
        }

        public static void RenderVehicleConfiguration(HtmlTextWriter writer, IReportItem reportItem)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.Ul);

            if (!string.IsNullOrEmpty(reportItem.Series))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.SeriesCssClass);

                writer.RenderBeginTag(HtmlTextWriterTag.Li);

                writer.Write(reportItem.Series);

                writer.RenderEndTag(); // Li

            }
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.DriveTrainCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Li);

            writer.Write(reportItem.DriveTrain);

            writer.RenderEndTag(); // Li

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.EngineCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Li);

            writer.Write(reportItem.Engine);

            writer.RenderEndTag(); // Li

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.FuelTypeCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Li);

            writer.Write(reportItem.FuelType);

            writer.RenderEndTag(); // Li

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.TransmissionCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Li);

            writer.Write(reportItem.Transmission);

            writer.RenderEndTag(); // Li

            writer.RenderEndTag(); // Ul
        }

        public static void RenderTableBodyCellUnitsSold(HtmlTextWriter writer, IReportItem reportItem)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.TableColumnUnitsSoldCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Td);

            writer.Write(string.Format("{0:#,0;#,0;--}",reportItem.UnitsSold));

            writer.RenderEndTag(); // Td
        }

        public void RenderTableBodyCellNumberOfListings(HtmlTextWriter writer, IReportItem reportItem)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.TableColumnNumberOfListingsCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Td);

            if (reportItem.ReportItems.Count <= 0)
            {
                string href = string.Format("/pricing/Pages/Market/Search.aspx?oh={0}&my={1}&ma={2}&li={3}&mf={4}&se={5}&mc={6}&db={7}",
                        Page.Request["oh"],
                        reportItem.ModelYear,
                        reportItem.MakeId,
                        reportItem.LineId,
                        reportItem.ModelFamilyId,
                        reportItem.SegmentId,
                        reportItem.ModelConfigurationId.GetValueOrDefault(),
                        DistanceBucketID);

                writer.AddAttribute(HtmlTextWriterAttribute.Href, href);

                if (OpenSearchAsPopUp)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Target, "MarketSearch");

                    writer.AddAttribute(HtmlTextWriterAttribute.Onclick, "window.open(this.href, this.target, 'status=1,scrollbars=1,toolbar=0,resizable=1,width=1010,height=730',''); return false;");
                }

                writer.RenderBeginTag(HtmlTextWriterTag.A);

                writer.Write(reportItem.NumberOfListings);

                writer.RenderEndTag(); // A 
            }
            else
            {
                writer.Write(string.Format("{0:#,0;#,0;--}",reportItem.NumberOfListings));
            }

            writer.RenderEndTag(); // Td
        }

        public static void RenderTableBodyCellAverageMarketPrice(HtmlTextWriter writer, IReportItem reportItem)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.TableColumnAverageMarketPriceCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Td);

            int? avgPrice = reportItem == null ? 0 : reportItem.AverageInternetPrice;

            writer.Write(string.Format("{0:$#,0;$#,0;--}", avgPrice));

            writer.RenderEndTag(); // Td
        }

        public static void RenderTableBodyCellAverageMileage(HtmlTextWriter writer, IReportItem reportItem)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.TableColumnAverageMarketPriceCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Td);

            int? avgMileage = reportItem == null ? 0 : reportItem.AverageInternetMileage;

            writer.Write(string.Format("{0:#,0;#,0;--}", avgMileage));

            writer.RenderEndTag(); // Td
        }

        public static void RenderTableBodyCellUnitsInStock(HtmlTextWriter writer, IReportItem reportItem)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.TableColumnUnitsInStockCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Td);

            writer.Write(string.Format("{0:#,0}",reportItem.UnitsInStock));

            writer.RenderEndTag(); // Td
        }

        #endregion

        #region Paging Properties

        public virtual bool AllowPaging
        {
            get
            {
                object obj2 = ViewState["AllowPaging"];
                return ((obj2 != null) && ((bool) obj2));
            }
            set
            {
                if (value != AllowPaging)
                {
                    ViewState["AllowPaging"] = value;
                    if (Initialized)
                    {
                        RequiresDataBinding = true;
                    }
                }
            }
        }

        public virtual int PageSize
        {
            get
            {
                object obj2 = ViewState["PageSize"];
                if (obj2 != null)
                {
                    return (int) obj2;
                }
                return 25;
            }
            set
            {
                if (value < 1)
                {
                    throw new ArgumentOutOfRangeException("value");
                }
                if (PageSize != value)
                {
                    ViewState["PageSize"] = value;
                    if (Initialized)
                    {
                        RequiresDataBinding = true;
                    }
                }
            }
        }

        public abstract int PageCount { get; }

        public abstract int PageIndex { get; set; }

        public abstract int RowCount { get; }

        #endregion

        #region Render Paging Methods

        protected string CreateId(string s)
        {
            return CreateId(new string[] {s});
        }

        protected string CreateId(params string[] strings)
        {
            StringBuilder id = new StringBuilder(UniqueID);

            foreach (string s in strings)
            {
                id.Append(IdSeparator);
                id.Append(s);
            }

            return id.ToString();
        }        

        public void RenderPaging(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.PagingListCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Ul);

            const int pagesToDisplay = 10;

            // Calculate the current range of pages.
            int startIndex = PageIndex - pagesToDisplay / 2;
            if (startIndex < 0)
            {
                startIndex = 0;
            }
            int endIndex = PageIndex + pagesToDisplay - (PageIndex - startIndex);
            if (endIndex > PageCount)
            {
                startIndex -= endIndex - PageCount;
                endIndex = PageCount;
                if (startIndex < 0)
                {
                    startIndex = 0;
                }
            }
            
            // Display paging.
            if (startIndex > 0)
            {
                RenderPagingUnit(writer, "First", 0);
                int lastIndex = PageIndex - pagesToDisplay > 0 ? PageIndex - pagesToDisplay : 0;
                RenderPagingUnit(writer, "Prev " + pagesToDisplay, lastIndex);
            }            
            for (int i = startIndex; i < endIndex; i++)
            {
                RenderPagingUnit(writer, (i + 1).ToString(), i);
            }                        
            if (PageCount > endIndex)
            {
                int nextIndex = PageIndex + pagesToDisplay < PageCount ? PageIndex + pagesToDisplay : PageCount - 1;
                RenderPagingUnit(writer, "Next " + pagesToDisplay, nextIndex);
                RenderPagingUnit(writer, "Last", PageCount - 1);                
            }

            writer.RenderEndTag(); // Ul
        }

        public void RenderPagingUnit(HtmlTextWriter writer, string pageDisplay, int pageValue)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.Li);

            string cssClass = SR.PagingInputCssClass;

            if (pageValue == PageIndex)
            {
                cssClass += " " + SR.PagingCurrentPageInputCssClass;
            }

            string id = CreateId(new string[] { SR.PageInputIdPrefix, pageValue.ToString() });

            writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClass);

            writer.AddAttribute(HtmlTextWriterAttribute.Type, "submit");

            writer.AddAttribute(HtmlTextWriterAttribute.Name, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Id, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Value, pageDisplay);

            writer.RenderBeginTag(HtmlTextWriterTag.Input);

            writer.RenderEndTag(); // Input

            writer.RenderEndTag(); // Li            
        }

        #endregion

        #region Column Definitions

        protected class Column
        {
            private readonly string _title;
            private readonly string _css;
            private readonly string _sort;

            public Column(string css, string title, string sort)
            {
                _title = title;
                _css = css;
                _sort = sort;
            }

            public string Title
            {
                get { return _title; }
            }

            public string Css
            {
                get { return _css; }
            }

            public string Sort
            {
                get { return _sort; }
            }
        }        

        #endregion

        #region Sorting Properties

        #pragma warning disable 3008
        protected string _sortExpression = string.Empty;
        protected SortDirection _sortDirection;
        #pragma warning restore 3008

        public virtual bool AllowSorting
        {
            get
            {
                object obj2 = ViewState["AllowSorting"];
                return ((obj2 != null) && ((bool)obj2));
            }
            set
            {
                if (value != AllowSorting)
                {
                    ViewState["AllowSorting"] = value;
                    if (Initialized)
                    {
                        RequiresDataBinding = true;
                    }
                }
            }
        }

        public virtual SortDirection SortDirection
        {
            get { return SortDirectionInternal; }
        }

        protected SortDirection SortDirectionInternal
        {
            get { return _sortDirection; }
            set
            {
                if ((value < SortDirection.Ascending) || (value > SortDirection.Descending))
                {
                    throw new ArgumentOutOfRangeException("value");
                }
                if (_sortDirection != value)
                {
                    _sortDirection = value;
                    if (Initialized)
                    {
                        RequiresDataBinding = true;
                    }
                }
            }
        }

        public virtual string SortExpression
        {
            get { return SortExpressionInternal; }
        }

        protected string SortExpressionInternal
        {
            get { return _sortExpression; }
            set
            {
                if (_sortExpression != value)
                {
                    _sortExpression = value;
                    if (Initialized)
                    {
                        RequiresDataBinding = true;
                    }
                }
            }
        }

        #endregion

        #region Sorting Methods

        private static SortDirection FlipSortDirection(SortDirection sortDirection)
        {
            return sortDirection.Equals(SortDirection.Descending) ? SortDirection.Ascending : SortDirection.Ascending;
        }

        internal void RenderSortButton(HtmlTextWriter writer, string column)
        {
            bool isSorted = SortExpressionInternal.Equals(column);

            string id = CreateId(new string[] { "sort", column });

            writer.AddAttribute(HtmlTextWriterAttribute.Id, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Name, id);

            if (isSorted)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "sorted");

                string value = SortDirectionInternal.Equals(FlipSortDirection(SortDirection.Descending))
                                   ? "&uarr;"
                                   : "&darr;";

                writer.AddAttribute(HtmlTextWriterAttribute.Value, value, false);
            }
            else
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "unsorted");

                writer.AddAttribute(HtmlTextWriterAttribute.Value, "_");
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Type, "submit");

            writer.RenderBeginTag(HtmlTextWriterTag.Input);

            writer.RenderEndTag();
        }

        #endregion

        #region InnerClasses

        internal static class SR
        {
            #region TableRows

            public const string TableRowHeadingsCssClass = "headings";
            public const string TableRowOddNumberRowCssClass = "odd";

            #endregion

            #region Table Columns

            public const string TableColumnMarketDaysSupplyCssClass = "mds";
            public const string TableColumnSmallestMarketDaysSupplyCssClass = "smallest_mds";
            public const string TableColumnVehicleInformationCssClass = "vehicle_info";
            public const string TableColumnUnitsSoldCssClass = "units_sold";
            public const string TableColumnNumberOfListingsCssClass = "number_of_listings";
            public const string TableColumnAverageMarketPriceCssClass = "avg_market_price";
            public const string TableColumnAverageMileageCssClass = "avg_mileage";
            public const string TableColumnUnitsInStockCssClass = "units_in_stock";            
            public const string TableColumnShoppingListCssClass = "shopping_list";
            public const string TableColumnExpandTitle = "";
            public const string TableColumnMarketDaysSupplyTitle = "Market Days Supply";
            public const string TableColumnSmallestMarketDaysSupplyTitle = "Best Equip MDS";
            public const string TableColumnVehicleInformationTitle = "Vehicle Information";
            public const string TableColumnUnitsSoldTitle = "Units Sold";
            public const string TableColumnNumberOfListingsTitle = "Number of Listings";
            public const string TableColumnAverageMarketPriceTitle = "Avg Market Internet Price";
            public const string TableColumnAverageMileageTitle = "Avg Mileage";
            public const string TableColumnUnitsInStockTitle = "Units in Stock";
            public const string TableColumnShoppingListTitle = "";

            #endregion

            #region Vehicle Info

            public const string ModelYearCssClass = "year";
            public const string MakeCssClass = "make";
            public const string ModelFamilyCssClass = "model";
            public const string SegmentCssClass = "segment";
            public const string FamilyCssClass = "family";
            public const string SeriesCssClass = "series";
            public const string DriveTrainCssClass = "drive_train";
            public const string EngineCssClass = "engine";
            public const string FuelTypeCssClass = "fuel_type";
            public const string TransmissionCssClass = "transmission";
            public const string HasConfigurationCssClass = "has_configuration";

            #endregion

            #region Paging

            public const string PagingListCssClass = "paging_list";
            public const string PagingInputCssClass = "paging";
            public const string PagingCurrentPageInputCssClass = "paging_current";
            public const string PageInputIdPrefix = "page";

            #endregion
        }

        #endregion
    }
}