using System;
using System.Security.AccessControl;
using System.Web;
using FirstLook.DomainModel.Oltp;

namespace FirstLook.Pricing.WebControls.AccessControl
{
    public interface IAccessController
    {
        void AssertPermissions(Owner owner, string identity, string path, out bool confirmLicense);
    }

    internal class IdentityManager
    {
        private readonly string _identity;
        private Owner _owner;
        private Member _member;
        private SoftwareSystemComponentState _state;

        public IdentityManager(Owner owner, string identity)
        {
            _owner = owner;
            _identity = identity;
        }

        public string Identity
        {
            get { return _identity; }
        }

        public Member Member
        {
            get
            {
                if (_member == null)
                {
                    _member = MemberFacade.FindByUserName(_identity);
                }

                return _member;
            }
        }

        public SoftwareSystemComponentState State
        {
            get
            {
                if (_state == null)
                {
                    if (_owner != null && _owner.OwnerEntityType == OwnerEntityType.Dealer)
                    {
                        _state = SoftwareSystemComponentStateFacade.FindOrCreate(
                            _identity,
                            SoftwareSystemComponentStateFacade.DealerComponentToken,
                            _owner.OwnerEntityId);
                    }
                    else
                    {
                        _state = SoftwareSystemComponentStateFacade.FindOrCreate(
                            _identity,
                            SoftwareSystemComponentStateFacade.DealerComponentToken);
                    }
                }

                return _state;
            }
        }

        public Owner Owner
        {
            get
            {
                if (_owner == null)
                {
                    SoftwareSystemComponentState state = State;

                    if (state != null)
                    {
                        BusinessUnit dealer = state.Dealer.GetValue();

                        if (dealer != null)
                        {
                            _owner = Owner.GetOwner(dealer.Id);
                        }
                    }
                }
                return _owner;
            }
        }
    }

    public class RoutingAccessController : IAccessController
    {
        public void AssertPermissions(Owner owner, string identity, string path, out bool confirmLicense)
        {
            IdentityManager manager = new IdentityManager(owner, identity);

            confirmLicense = false; // sensible default value

            HttpContext.Current.Items.Add(MemberFacade.HttpContextKey, manager.Member);

            SoftwareSystemComponentState state = manager.State;

            if (state == null)
            {
                return;
            }

            HttpContext.Current.Items.Add(SoftwareSystemComponentStateFacade.HttpContextKey, state);

            BusinessUnit dealer = state.Dealer.GetValue();

            if (dealer == null)
            {
                return;
            }

            HttpContext.Current.Items.Add(BusinessUnit.HttpContextKey, dealer);            
        }
    }

    public class InternetAccessController : IAccessController
    {
        public void AssertPermissions(Owner owner, string identity, string path, out bool confirmLicense)
        {
            IdentityManager manager = new IdentityManager(owner, identity);

            confirmLicense = false; // sensible default value

            if (path.StartsWith("Pages/Internet/") || path.StartsWith("Pages/Preferences/"))
            {
                if (path.EndsWith("ChooseDealer.aspx") || path.EndsWith("ChooseSeller.aspx") || path.EndsWith("ProcessSeller.aspx"))
                {
                    if (manager.Member.MemberType != MemberType.Administrator)
                    {
                        throw new PrivilegeNotHeldException("Administrator");
                    }
                }
                else
                {
                    // users and administrators accessing the product need to be validated

                    if ((manager.Member.MemberType == MemberType.User) ||
                        (manager.Member.MemberType == MemberType.Administrator && manager.Owner.OwnerEntityType == OwnerEntityType.Dealer))
                    {
                        SoftwareSystemComponentState state = manager.State;

                        if (state == null)
                        {
                            throw new PrivilegeNotHeldException("Pricing");
                        }

                        BusinessUnit dealer = state.Dealer.GetValue();

                        if (dealer == null || !dealer.HasDealerUpgrade(Upgrade.PingTwo))
                        {
                            throw new PrivilegeNotHeldException("Pricing");
                        }

                        confirmLicense = dealer.HasDealerUpgrade(Upgrade.JDPowerUsedCarMarketData) &&
                                         !LicenseFacade.HadAcceptedLicense(License.JDPowerLicenseName, identity);


                        HttpContext.Current.Items.Add(SoftwareSystemComponentStateFacade.HttpContextKey, state);

                        HttpContext.Current.Items.Add(BusinessUnit.HttpContextKey, dealer);

                        HttpContext.Current.Items.Add(MemberFacade.HttpContextKey, manager.Member);
                    }
                    else if (manager.Member.MemberType == MemberType.Administrator)
                    {
                        HttpContext.Current.Items.Add(MemberFacade.HttpContextKey, manager.Member); // sales tool
                    }
                    else
                    {
                        throw new PrivilegeNotHeldException("MemberType");
                    }
                }
            }
            else
            {
                throw new PrivilegeNotHeldException("Pricing");
            }
        }
    }

    public class EdmundsAccessController : IAccessController
    {
        public void AssertPermissions(Owner owner, string identity, string path, out bool confirmLicense)
        {
            IdentityManager manager = new IdentityManager(owner, identity);

            confirmLicense = false; // sensible default value

            if (path.StartsWith("Pages/Edmunds/"))
            {
                if ((manager.Member.MemberType == MemberType.User) ||
                    (manager.Member.MemberType == MemberType.Administrator && manager.Owner.OwnerEntityType == OwnerEntityType.Dealer))
                {
                    SoftwareSystemComponentState state = manager.State;

                    if (state == null)
                    {
                        throw new PrivilegeNotHeldException("EdmundsTMV");
                    }

                    BusinessUnit dealer = state.Dealer.GetValue();

                    if (dealer == null || !dealer.HasDealerUpgrade(Upgrade.EdmundsTrueMarketValue))
                    {
                        throw new PrivilegeNotHeldException("EdmundsTMV");
                    }

                    HttpContext.Current.Items.Add(SoftwareSystemComponentStateFacade.HttpContextKey, state);

                    HttpContext.Current.Items.Add(BusinessUnit.HttpContextKey, dealer);

                    HttpContext.Current.Items.Add(MemberFacade.HttpContextKey, manager.Member);
                }
                else if (manager.Member.MemberType == MemberType.Administrator)
                {
                    HttpContext.Current.Items.Add(MemberFacade.HttpContextKey, manager.Member); // sales tool
                }
                else
                {
                    throw new PrivilegeNotHeldException("MemberType");
                }
            }
            else
            {
                throw new PrivilegeNotHeldException("EdmundsTMV");
            }
        }
    }

    public class JDPowerAccessController : IAccessController
    {
        public void AssertPermissions(Owner owner, string identity, string path, out bool confirmLicense)
        {
            IdentityManager manager = new IdentityManager(owner, identity);

            confirmLicense = true; // sensible default value

            // there is no owner-handle in the URL which means there is no sales tool at the moment
            // and we must take the information from the software system component state record ...

            if (path.StartsWith("Pages/JDPower/"))
            {
                SoftwareSystemComponentState state = manager.State;

                if (state == null)
                {
                    throw new PrivilegeNotHeldException("JDPower");
                }

                BusinessUnit dealer = state.Dealer.GetValue();

                if (dealer == null || !dealer.HasDealerUpgrade(Upgrade.JDPowerUsedCarMarketData))
                {
                    throw new PrivilegeNotHeldException("JDPower");
                }

                bool isLicensePage = !string.IsNullOrEmpty(path) && path.Contains("Pages/JDPower/JDPowerLicense.aspx");

                confirmLicense = !LicenseFacade.HadAcceptedLicense(License.JDPowerLicenseName, identity) &&
                                 !isLicensePage;

                HttpContext.Current.Items.Add(SoftwareSystemComponentStateFacade.HttpContextKey, state);

                HttpContext.Current.Items.Add(BusinessUnit.HttpContextKey, dealer);

                HttpContext.Current.Items.Add(MemberFacade.HttpContextKey, manager.Member);
            }
            else
            {
                throw new PrivilegeNotHeldException("JDPower");
            }
        }
    }

    public class MarketAccessController : IAccessController
    {
        public void AssertPermissions(Owner owner, string identity, string path, out bool confirmLicense)
        {
            IdentityManager manager = new IdentityManager(owner, identity);

            confirmLicense = false; // sensible default value

            if (path.StartsWith("Pages/Market/"))
            {
                if ((manager.Member.MemberType == MemberType.User) ||
                    (manager.Member.MemberType == MemberType.Administrator && manager.Owner.OwnerEntityType == OwnerEntityType.Dealer))
                {
                    SoftwareSystemComponentState state = manager.State;

                    if (state == null)
                    {
                        throw new PrivilegeNotHeldException("Market");
                    }

                    BusinessUnit dealer = state.Dealer.GetValue();

                    if (dealer == null || !dealer.HasDealerUpgrade(Upgrade.MarketStocking))
                    {
                        throw new PrivilegeNotHeldException("Market");
                    }

                    HttpContext.Current.Items.Add(SoftwareSystemComponentStateFacade.HttpContextKey, state);

                    HttpContext.Current.Items.Add(BusinessUnit.HttpContextKey, dealer);

                    HttpContext.Current.Items.Add(MemberFacade.HttpContextKey, manager.Member);
                }
                else if (manager.Member.MemberType == MemberType.Administrator)
                {
                    HttpContext.Current.Items.Add(MemberFacade.HttpContextKey, manager.Member); // sales tool
                }
                else
                {
                    throw new PrivilegeNotHeldException("MemberType");
                }
            }
            else
            {
                throw new PrivilegeNotHeldException("Market");
            }
        }
    }

    public class MarketingAccessController : IAccessController
    {
        public void AssertPermissions(Owner owner, string identity, string path, out bool confirmLicense)
        {
            IdentityManager manager = new IdentityManager(owner, identity);

            confirmLicense = true; // sensible default value

            if (path.StartsWith("Pages/SalesAccelerator/") || path.Contains("Pages/MaxMarketing/"))
            {
                if ((manager.Member.MemberType == MemberType.User) ||
                    (manager.Member.MemberType == MemberType.Administrator && manager.Owner.OwnerEntityType == OwnerEntityType.Dealer))
                {
                    SoftwareSystemComponentState state = manager.State;

                    if (state == null)
                    {
                        throw new PrivilegeNotHeldException("Market");
                    }

                    BusinessUnit dealer = state.Dealer.GetValue();

                    if (dealer == null ||
                        !dealer.HasDealerUpgrade(Upgrade.PingTwo) ||
                        !dealer.HasDealerUpgrade(Upgrade.Marketing))
                    {
                        throw new PrivilegeNotHeldException("Market");
                    }

                    confirmLicense = dealer.HasDealerUpgrade(Upgrade.JDPowerUsedCarMarketData) &&
                                     !LicenseFacade.HadAcceptedLicense(License.JDPowerLicenseName, identity);

                    HttpContext.Current.Items.Add(SoftwareSystemComponentStateFacade.HttpContextKey, state);

                    HttpContext.Current.Items.Add(BusinessUnit.HttpContextKey, dealer);

                    HttpContext.Current.Items.Add(MemberFacade.HttpContextKey, manager.Member);
                }
                else if (manager.Member.MemberType == MemberType.Administrator)
                {
                    HttpContext.Current.Items.Add(MemberFacade.HttpContextKey, manager.Member); // sales tool
                }
                else
                {
                    throw new PrivilegeNotHeldException("MemberType");
                }
            }
            else
            {
                throw new PrivilegeNotHeldException("Market");
            }
        }
    }
}