using System;
using System.Text;
using System.Web;
using FirstLook.DomainModel.Oltp;

namespace FirstLook.Pricing.WebControls.AccessControl
{
    public class AccessControlHttpModule : IHttpModule
    {
        #region IHttpModule Members

        public void Dispose()
        {
            // no state to destroy
        }

        public void Init(HttpApplication application)
        {
            application.PostAcquireRequestState += OnPostAcquireRequestState;
        }

        #endregion

        public void OnPostAcquireRequestState(object sender, EventArgs args)
        {
            HttpRequest request = HttpContext.Current.Request;

            HttpResponse response = HttpContext.Current.Response;

            int i = request.Path.IndexOf('/', 1) + 1;

            string path = request.Path.Substring(i, request.Path.Length - i);

            IAccessController controller = null;

            if (string.IsNullOrEmpty(path) ||
                path.Equals("Default.aspx", StringComparison.InvariantCultureIgnoreCase))
            {
                controller = new RoutingAccessController();
            }
            else if (path.Contains("Pages/Edmunds/"))
            {
                controller = new EdmundsAccessController();
            }
            else if (path.Contains("Pages/Internet/") || path.Contains("Pages/Preferences/"))
            {
                controller = new InternetAccessController();
            }
            else if (path.Contains("Pages/JDPower/"))
            {
                controller = new JDPowerAccessController();
            }
            else if (path.Contains("Pages/Market/"))
            {
                controller = new MarketAccessController();
            }
            else if (path.Contains("Pages/SalesAccelerator/") || path.Contains("Pages/MaxMarketing/"))
            {
                controller = new MarketingAccessController();
            }

            if (controller != null)
            {
                string identity = HttpContext.Current.User.Identity.Name;

                string ownerHandle = request.QueryString["oh"];

                Owner owner = null;

                if (!string.IsNullOrEmpty(ownerHandle))
                {
                    owner = Owner.GetOwner(ownerHandle);
                }

                bool confirmLicense;

                controller.AssertPermissions(owner, identity, path, out confirmLicense);

                if (confirmLicense)
                {
                    StringBuilder callbackUrl = new StringBuilder(request.Path);

                    if (request.QueryString.Count > 0)
                    {
                        callbackUrl.Append("?").Append(request.QueryString.ToString());
                    }

                    StringBuilder jdpowerUrl =
                        new StringBuilder("/pricing/Pages/JDPower/JDPowerLicense.aspx?callback=");

                    jdpowerUrl.Append(HttpUtility.UrlEncode(callbackUrl.ToString()));

                    response.Redirect(jdpowerUrl.ToString(), true);
                }
            }
        }
    }
}