using System;
using System.Collections.Generic;
using System.Text;

namespace FirstLook.Pricing.WebControls
{
	public class DocumentEditorDeletedEventArgs : EventArgs
	{
		private readonly int _affectedRows;
		private readonly Exception _exception;
		private bool _exceptionHandled = false;
		private bool _keepInEditMode = false;

		public DocumentEditorDeletedEventArgs(int affectedRows, Exception exception)
		{
			_affectedRows = affectedRows;
			_exception = exception;
		}

		public int AffectedRows
		{
			get { return _affectedRows; }
		}

		public bool KeepInEditMode
		{
			get { return _keepInEditMode; }
			set { _keepInEditMode = value; }
		}

		public Exception Exception
		{
			get { return _exception; }
		}

		public bool ExceptionHandled
		{
			get { return _exceptionHandled; }
			set { _exceptionHandled = value; }
		}
	}
}
