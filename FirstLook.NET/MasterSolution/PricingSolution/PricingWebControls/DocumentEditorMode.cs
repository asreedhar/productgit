using System;
using System.Collections.Generic;
using System.Text;

namespace FirstLook.Pricing.WebControls
{
	public enum DocumentEditorMode
	{
		ReadOnly,
		Insert,
		Edit,
		Confirmation
	}
}
