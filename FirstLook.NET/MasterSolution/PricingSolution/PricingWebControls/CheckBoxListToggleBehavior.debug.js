
Type.registerNamespace('FirstLook.Pricing.WebControls');

/** -------------------------------------------------------------------
 ** CheckBoxList Check-All Behaviour Class Declaration
 ** ---------------------------------------------------------------- */
 
FirstLook.Pricing.WebControls.CheckBoxListToggleBehavior = function (element)
{
    FirstLook.Pricing.WebControls.CheckBoxListToggleBehavior.initializeBase(this,[element]);
    this._CheckBox = null;
}

function FirstLook$Pricing$Website$CheckBoxListToggleBehavior$initialize () {
    FirstLook.Pricing.WebControls.CheckBoxListToggleBehavior.callBaseMethod(this, 'initialize');
    this._OnCboxClickDelegate = Function.createDelegate(this, this._OnCboxClick);
    this._OnListClickDelegate = Function.createDelegate(this, this._OnListClick);
}

function FirstLook$Pricing$Website$CheckBoxListToggleBehavior$dispose () {
    // dispose of list item click event handlers
    var list = this.get_element();
    if (list) {
        var items = list.getElementsByTagName("INPUT");
        for (var i = 0, l = items.length; i < l; i++) {
            $removeHandler(items[i], 'click', this._OnListClickDelegate);
        }
    }
    delete this._onListClickDelegate;
    // dispose of cbox click event handler
    var cbox = this.get_CheckBox();
    if (cbox) {
        $removeHandler(cbox, 'click', this._OnCboxClickDelegate);
    }
    delete this._OnCboxClickDelegate;
    // finally
    FirstLook.Pricing.WebControls.CheckBoxListToggleBehavior.callBaseMethod(this, 'dispose');
}

function FirstLook$Pricing$Website$CheckBoxListToggleBehavior$updated () {
    var cbox = this.get_CheckBox();
    var list = this.get_element();
    if (cbox && list) {
        var items = list.getElementsByTagName("INPUT");
        if (cbox.checked || items.length == 1) {
            cbox.checked = true;
            this._CheckAll(items, true);
        }        
        for (var i = 0, l = items.length; i < l; i++) {
            $addHandler(items[i], 'click', this._OnListClickDelegate);
        }
        $addHandler(cbox, 'click', this._OnCboxClickDelegate);
    }
}

function FirstLook$Pricing$Website$CheckBoxListToggleBehavior$get_CheckBox () {
    return this._CheckBox;
}

function FirstLook$Pricing$Website$CheckBoxListToggleBehavior$set_CheckBox (value) {
    this._CheckBox = value;
}

function FirstLook$Pricing$Website$CheckBoxListToggleBehavior$_OnListClick (e) {
    this.get_CheckBox().checked = this._TestAllChecked(this.get_element().getElementsByTagName("INPUT"));
}

function FirstLook$Pricing$Website$CheckBoxListToggleBehavior$_OnCboxClick (e) {
    this._CheckAll(this.get_element().getElementsByTagName("INPUT"), e.target.checked);
}

function FirstLook$Pricing$Website$CheckBoxListToggleBehavior$_CheckAll (items, value) {
    for (var i = 0, l = items.length; i < l; i++) {
        items[i].checked = value;
    }
}

function FirstLook$Pricing$Website$CheckBoxListToggleBehavior$_TestAllChecked (items) {
    for (var i = 0, l = items.length; i < l; i++) {
        if (!items[i].checked) {
            return false;
        }
    }
    return true;
}

function FirstLook$Pricing$Website$CheckBoxListToggleBehavior$_TestAllUnchecked (items) {
    for (var i = 0, l = items.length; i < l; i++) {
        if (items[i].checked == true) {
            return true;
        }
    }
    return true;
}

FirstLook.Pricing.WebControls.CheckBoxListToggleBehavior.prototype =
{
     initialize:        FirstLook$Pricing$Website$CheckBoxListToggleBehavior$initialize
    ,dispose:           FirstLook$Pricing$Website$CheckBoxListToggleBehavior$dispose
    ,updated:           FirstLook$Pricing$Website$CheckBoxListToggleBehavior$updated
    ,get_CheckBox:      FirstLook$Pricing$Website$CheckBoxListToggleBehavior$get_CheckBox
    ,set_CheckBox:      FirstLook$Pricing$Website$CheckBoxListToggleBehavior$set_CheckBox
    ,_OnListClick:      FirstLook$Pricing$Website$CheckBoxListToggleBehavior$_OnListClick
    ,_OnCboxClick:      FirstLook$Pricing$Website$CheckBoxListToggleBehavior$_OnCboxClick
    ,_CheckAll:         FirstLook$Pricing$Website$CheckBoxListToggleBehavior$_CheckAll
    ,_TestAllChecked:   FirstLook$Pricing$Website$CheckBoxListToggleBehavior$_TestAllChecked
    ,_TestAllUnchecked: FirstLook$Pricing$Website$CheckBoxListToggleBehavior$_TestAllUnchecked
}

if (FirstLook.Pricing.WebControls.CheckBoxListToggleBehavior.registerClass != undefined)
    FirstLook.Pricing.WebControls.CheckBoxListToggleBehavior.registerClass('FirstLook.Pricing.WebControls.CheckBoxListToggleBehavior', Sys.UI.Behavior);

