/**
 * Adds Min/Max behavior to two select boxes,
 * Ensuring the max select box value is always above the value of min select box
 * Paul Monson, Simon Swenmouth
 * @version 2
 * @requires FirstlLook.Utility
 */

if ( !! Sys) {
    Type.registerNamespace('FirstLook.Pricing.WebControls');
    /**
     * @constructor
     */    
    FirstLook.Pricing.WebControls.DropDownListValueBehavior = function(element) {
        FirstLook.Pricing.WebControls.DropDownListValueBehavior.initializeBase(this, [element]);
    };
    FirstLook.Pricing.WebControls.DropDownListValueBehavior.prototype = {
        /**
         * Binds Change Events to Elements
         * @public
         */        
        updated: function() {
            var lower = this.get_element(),
            upper = this.get_DropDownList();

            if ( !! lower && !!upper) {
                this.addHandler(lower, 'change', this.getDelegate('onChange'));
                this.addHandler(upper, 'change', this.getDelegate('onChange'));
            };

            FirstLook.Pricing.WebControls.DropDownListValueBehavior.callBaseMethod(this, 'updated');
        },
        /**
         * Cleans up Event Handlers and Delegates.
    	 * @public
    	 */
        dispose: function() {
            this.removeHandlers();
            this.clearDelegates();
            FirstLook.Pricing.WebControls.DropDownListValueBehavior.callBaseMethod(this, 'dispose');
        },
        // ==========
        // = Events =
        // ==========
        /**
         * Ensures the upper select box value is above the lower select box value.
         * @public
         * @param {Event} e Event Object
         */
        onChange: function(e) {
            var lower = this.get_element(),
            upper = this.get_DropDownList();

            if (e.target.id == lower.id) {
                if (lower.selectedIndex > upper.selectedIndex) {
                    upper.selectedIndex = lower.selectedIndex;
                }
            }
            else {
                if (upper.selectedIndex < lower.selectedIndex) {
                    lower.selectedIndex = upper.selectedIndex;
                }
            }
        }
    };
    FirstLook.Utility.createGetterSetters(FirstLook.Pricing.WebControls.DropDownListValueBehavior, ["DropDownList"]);

    FirstLook.Pricing.WebControls.DropDownListValueBehavior.registerClass('FirstLook.Pricing.WebControls.DropDownListValueBehavior', Sys.UI.Behavior);
};

Sys.Application.notifyScriptLoaded();