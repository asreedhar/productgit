using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Web.UI;
using FirstLook.DomainModel.Lexicon.Categorization;
using FirstLook.Pricing.DomainModel.Market;

[assembly: WebResource("FirstLook.Pricing.WebControls.MarketReportTableFilterHelper.js", "text/javascript")]
[assembly: WebResource("FirstLook.Pricing.WebControls.MarketReportTableFilterHelper.debug.js", "text/javascript")]

namespace FirstLook.Pricing.WebControls
{
    internal class MarketReportTableFilterHelper : Control, IPostBackDataHandler, IScriptControl
    {
        #region Events

        internal static readonly object EventFilterChanged = new object();

        public event EventHandler<EventArgs> FilterChanged
        {
            add { Events.AddHandler(EventFilterChanged, value); }
            remove { Events.RemoveHandler(EventFilterChanged, value); }
        }

        protected virtual void OnFilterChanged()
        {
            EventHandler<EventArgs> handler = (EventHandler<EventArgs>)Events[EventFilterChanged];

            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        #endregion

        #region Constants

        private static readonly int[] PriceFilterValues = new int[]
                                                              {
                                                                  0,
                                                                  5000,
                                                                  15000,
                                                                  25000,
                                                                  35000,
                                                                  45000,
                                                                  55000,
                                                                  65000,
                                                                  75000,
                                                                  int.MaxValue
                                                              };

        private static readonly int[] MileageFilterValues = new int[]
                                                                {
                                                                    0,
                                                                    1000,
                                                                    2000,
                                                                    3000,
                                                                    4000,
                                                                    5000,
                                                                    6000,
                                                                    7000,
                                                                    8000,
                                                                    9000,
                                                                    10000,
                                                                    20000,
                                                                    30000,
                                                                    40000,
                                                                    50000,
                                                                    60000,
                                                                    70000,
                                                                    80000,
                                                                    90000,
                                                                    100000,
                                                                    110000,
                                                                    120000,
                                                                    130000,
                                                                    140000,
                                                                    int.MaxValue
                                                                };

        #endregion

        #region Properties

        #region Filter Values Class

        internal class FilterValues : IStateManager
        {
            private bool _propertyHasChanged;
            private int _minModelYear;
            private int _maxModelYear;
            private int _makeId;
            private int _modelFamilyId;
            private int _minInternetPrice;
            private int _maxInternetPrice;
            private int _minInternetMileage;
            private int _maxInternetMileage;
            private int _minUnitsInStock;
            private int _maxUnitsInStock = int.MaxValue;
            private ArrayList _segmentSelections = new ArrayList();

            public bool PropertyHasChanged
            {
                get { return _propertyHasChanged; }
                set { _propertyHasChanged = value; }
            }

            public int MinModelYear
            {
                get { return _minModelYear; }
                set
                {
                    if (value != MinModelYear)
                    {
                        _minModelYear = value;

                        _propertyHasChanged = true;
                    }
                }
            }

            public int MaxModelYear
            {
                get { return _maxModelYear; }
                set
                {
                    if (value != MaxModelYear)
                    {
                        _maxModelYear = value;

                        _propertyHasChanged = true;
                    }
                }
            }

            public int MakeId
            {
                get { return _makeId; }
                set
                {
                    if (value != MakeId)
                    {
                        _makeId = value;

                        _modelFamilyId = default(int);

                        _propertyHasChanged = true;
                    }
                }
            }

            public int ModelFamilyId
            {
                get { return _modelFamilyId; }
                set
                {
                    if (value != ModelFamilyId)
                    {
                        _modelFamilyId = value;

                        _propertyHasChanged = true;
                    }
                }
            }

            public int MinInternetPrice
            {
                get { return _minInternetPrice; }
                set
                {
                    if (value != MinInternetPrice)
                    {
                        _minInternetPrice = value;

                        _propertyHasChanged = true;
                    }
                }
            }

            public int MaxInternetPrice
            {
                get { return _maxInternetPrice; }
                set
                {
                    if (value != MaxInternetPrice)
                    {
                        _maxInternetPrice = value;

                        _propertyHasChanged = true;
                    }
                }
            }

            public int MinInternetMileage
            {
                get { return _minInternetMileage; }
                set
                {
                    if (value != MinInternetMileage)
                    {
                        _minInternetMileage = value;

                        _propertyHasChanged = true;
                    }
                }
            }

            public int MaxInternetMileage
            {
                get { return _maxInternetMileage; }
                set
                {
                    if (value != MaxInternetMileage)
                    {
                        _maxInternetMileage = value;

                        _propertyHasChanged = true;
                    }
                }
            }

            public int MinUnitsInStock
            {
                get { return _minUnitsInStock; }
                set
                {
                    if (value != MinUnitsInStock)
                    {
                        _minUnitsInStock = value;

                        _propertyHasChanged = true;
                    }
                }
            }

            public int MaxUnitsInStock
            {
                get { return _maxUnitsInStock; }
                set
                {
                    if (value != MaxUnitsInStock)
                    {
                        _maxUnitsInStock = value;

                        _propertyHasChanged = true;
                    }
                }
            }

            public ArrayList SegmentSelections
            {
                get { return _segmentSelections; }
            }

            public bool AreAllSegmentsSelected()
            {
                foreach (Pair pair in SegmentSelections)
                {
                    if (!((bool)pair.Second))
                    {
                        return false;
                    }
                }

                return true;
            }

            public bool AreNoSegmentsSelected()
            {
                bool selected = false;

                foreach (Pair pair in SegmentSelections)
                {
                    selected |= (bool)pair.Second;
                }

                return !selected;
            }

            public bool IsSegmentSelected(int id)
            {
                foreach (Pair pair in SegmentSelections)
                {
                    if (((int)pair.First) == id)
                    {
                        return (bool)pair.Second;
                    }
                }

                return false;
            }

            public bool Equals(FilterValues other)
            {
                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;
                return other._minModelYear == _minModelYear &&
                       other._maxModelYear == _maxModelYear &&
                       other._makeId == _makeId &&
                       other._modelFamilyId == _modelFamilyId &&
                       other._minInternetPrice == _minInternetPrice &&
                       other._maxInternetPrice == _maxInternetPrice &&
                       other._minInternetMileage == _minInternetMileage &&
                       other._maxInternetMileage == _maxInternetMileage &&
                       other._minUnitsInStock == _minUnitsInStock &&
                       other._maxUnitsInStock == _maxUnitsInStock &&
                       Equals(other._segmentSelections, _segmentSelections);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != typeof(FilterValues)) return false;
                return Equals((FilterValues)obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    int result = _minModelYear;
                    result = (result * 397) ^ _maxModelYear;
                    result = (result * 397) ^ _makeId;
                    result = (result * 397) ^ _modelFamilyId;
                    result = (result * 397) ^ _minInternetPrice;
                    result = (result * 397) ^ _maxInternetPrice;
                    result = (result * 397) ^ _minInternetMileage;
                    result = (result * 397) ^ _maxInternetMileage;
                    result = (result * 397) ^ _minUnitsInStock;
                    result = (result * 397) ^ _maxUnitsInStock;
                    result = (result * 397) ^ _segmentSelections.GetHashCode();
                    return result;
                }
            }

            #region IStateManager Members

            private bool _trackingViewState;

            bool IStateManager.IsTrackingViewState
            {
                get { return _trackingViewState; }
            }

            void IStateManager.LoadViewState(object state)
            {
                object[] propertyState = state as object[];

                if (propertyState != null)
                {
                    int i = 0;

                    _minModelYear = (int)propertyState[i++];
                    _maxModelYear = (int)propertyState[i++];
                    _makeId = (int)propertyState[i++];
                    _modelFamilyId = (int)propertyState[i++];
                    _minInternetPrice = (int)propertyState[i++];
                    _maxInternetPrice = (int)propertyState[i++];
                    _minInternetMileage = (int)propertyState[i++];
                    _maxInternetMileage = (int)propertyState[i++];
                    _minUnitsInStock = (int)propertyState[i++];
                    _maxUnitsInStock = (int)propertyState[i++];
                    _segmentSelections = (ArrayList)propertyState[i];
                }
            }

            object IStateManager.SaveViewState()
            {
                object[] propertyState = new object[11];

                int i = 0;

                propertyState[i++] = _minModelYear;
                propertyState[i++] = _maxModelYear;
                propertyState[i++] = _makeId;
                propertyState[i++] = _modelFamilyId;
                propertyState[i++] = _minInternetPrice;
                propertyState[i++] = _maxInternetPrice;
                propertyState[i++] = _minInternetMileage;
                propertyState[i++] = _maxInternetMileage;
                propertyState[i++] = _minUnitsInStock;
                propertyState[i++] = _maxUnitsInStock;
                propertyState[i] = _segmentSelections;

                return propertyState;
            }

            void IStateManager.TrackViewState()
            {
                _trackingViewState = true;
            }

            #endregion

            public void Update(FilterValues filters)
            {

                UpdateVehcileInfo(filters);
                UpdatePrice(filters);
                UpdateMileage(filters);
                UpdateUnitsInStock(filters);
            }

            public void UpdateVehcileInfo(FilterValues filters)
            {
                MinModelYear = filters._minModelYear;
                MaxModelYear = filters._maxModelYear;
                MakeId = filters._makeId;
                ModelFamilyId = filters._modelFamilyId;

                SegmentSelections.Clear();

                foreach (Pair pair in filters.SegmentSelections)
                {
                    SegmentSelections.Add(new Pair(pair.First, pair.Second));
                }
                _propertyHasChanged = true;
            }

            public void UpdatePrice(FilterValues filters)
            {
                MinInternetPrice = filters._minInternetPrice;
                MaxInternetPrice = filters._maxInternetPrice;
            }

            public void UpdateMileage(FilterValues filters)
            {
                MinInternetMileage = filters._minInternetMileage;
                MaxInternetMileage = filters._maxInternetMileage;
            }

            public void UpdateUnitsInStock(FilterValues filters)
            {
                MinUnitsInStock = filters._minUnitsInStock;
                MaxUnitsInStock = filters._maxUnitsInStock;
            }

            public ReportFilterArguments ToReportFilterArguments()
            {
                ReportFilterArguments args = new ReportFilterArguments();

                foreach (Pair pair in SegmentSelections)
                {
                    if ((bool)pair.Second)
                    {
                        args.Segments.Add((int)pair.First);
                    }
                }

                int maxModelYear = MaxModelYear == 0 ? 2019 : MaxModelYear;

                int minModelYear = MinModelYear == 0 ? 1980 : MinModelYear;

                args.ModelYears = new Range(
                    minModelYear,
                    maxModelYear);

                args.Mileages = new Range(
                    MinInternetMileage,
                    MaxInternetMileage == 0 ? Int32.MaxValue : MaxInternetMileage);

                args.Prices = new Range(
                    MinInternetPrice,
                    MaxInternetPrice == 0 ? Int32.MaxValue : MaxInternetPrice);

                args.UnitsInStock = new Range(
                    MinUnitsInStock,
                    MaxUnitsInStock);

                if (MakeId != 0)
                {
                    args.MakeId = MakeId;
                }

                if (ModelFamilyId != 0)
                {
                    args.ModelFamilyId = ModelFamilyId;
                }

                // unit cost

                return args;
            }
        }

        #endregion

        private FilterValues _filters;

        private FilterValues _newFilters;

        internal FilterValues Filters
        {
            get
            {
                if (_filters == null)
                {
                    _filters = new FilterValues();

                    if (IsTrackingViewState)
                    {
                        ((IStateManager)_filters).TrackViewState();
                    }
                }

                return _filters;
            }
        }

        private FilterValues NewFilters
        {
            get
            {
                if (_newFilters == null)
                {
                    _newFilters = new FilterValues();

                    if (IsTrackingViewState)
                    {
                        ((IStateManager)_newFilters).TrackViewState();
                    }
                }

                return _newFilters;
            }
        }

        private bool _showUnitsSold = true;

        public bool ShowUnitsSold
        {
            get { return _showUnitsSold; }
            set { _showUnitsSold = value; }
        }

        #endregion

        #region Reference Data

        private bool _requiresDataBinding = true;
        private ModelYearList _modelYears;
        private MakeList _makes;
        private ModelFamilyList _modelFamilies;
        private ModelFamilyList _currentModelFamilies;
        private SegmentList _segments;

        public ModelYearList ModelYears
        {
            get { EnsureDataBound(); return _modelYears; }
        }

        public MakeList Makes
        {
            get { EnsureDataBound(); return _makes; }
        }

        public ModelFamilyList ModelFamilies
        {
            get { EnsureDataBound(); return _modelFamilies; }
        }

        public ModelFamilyList CurrentModelFamilies
        {
            get { EnsureDataBound(); return _currentModelFamilies; }
        }

        public SegmentList Segments
        {
            get { EnsureDataBound(); return _segments; }
        }

        #endregion

        #region Data Binding

        protected override void OnPreRender(EventArgs e)
        {
            EnsureDataBound();

            base.OnPreRender(e);

            if (!DesignMode)
            {
                _manager = ScriptManager.GetCurrent(Page);

                if (_manager != null)
                {
                    _manager.RegisterScriptControl(this);
                }
                else
                {
                    throw new InvalidOperationException("You must have a ScriptManager!");
                }
            }
        }

        protected void EnsureDataBound()
        {
            if (_requiresDataBinding)
            {
                DataBind();
            }
        }

        public override void DataBind()
        {
            /* DO NOT USE THE PROPERTIES IN THIS METHOD AS YOU WILL CAUSE INIFINTE RECURSION */

            bool initializeFilters = (Filters.MinModelYear == 0);

            _modelYears = ModelYearList.GetModelYears();

            int maxModelYear = NewFilters.MaxModelYear == 0 ? _modelYears[0].Id : NewFilters.MaxModelYear;

            int minModelYear = NewFilters.MinModelYear == 0 ? _modelYears[_modelYears.Count - 1].Id : NewFilters.MinModelYear;

            if (initializeFilters)
            {
                minModelYear = maxModelYear - 7;
            }

            if (Filters.MakeId != 0)
            {
                _currentModelFamilies = ModelFamilyList.GetModelFamilies(minModelYear, maxModelYear, Filters.MakeId); 
            }
            else
            {
                _currentModelFamilies = ModelFamilyList.EmptyList();
            }

            _makes = MakeList.GetMakes(minModelYear, maxModelYear);

            if (NewFilters.MakeId != 0)
            {
                _modelFamilies = ModelFamilyList.GetModelFamilies(minModelYear, maxModelYear, NewFilters.MakeId);
            }
            else
            {
                _modelFamilies = ModelFamilyList.EmptyList();
            }

            _segments = SegmentList.GetSegments();

            if (initializeFilters)
            {
                Filters.MinModelYear = minModelYear;

                Filters.MaxModelYear = maxModelYear;

                Filters.MaxInternetPrice = 75000;

                Filters.MaxInternetMileage = 70000;

                foreach (Segment segment in _segments)
                {
                    Filters.SegmentSelections.Add(new Pair(segment.Id, true));
                }

                NewFilters.Update(Filters);
            }

            _requiresDataBinding = false;
        }

        #endregion

        #region Render

        protected override void Render(HtmlTextWriter writer)
        {
            RenderTableFilterSummaryRow(writer);

            RenderTableFilterRow(writer);

            if (!DesignMode)
            {
                _manager.RegisterScriptDescriptors(this);
            }
        }

        private void RenderTableFilterSummaryRow(HtmlTextWriter writer)
        {
            const string filterCssClass = SR.HasColumnFilter + " " + SR.HasColumnFilterOpen;

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.TableRowFilterSummaryCssClass);

            writer.AddAttribute(HtmlTextWriterAttribute.Id, ClientID);

            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, MarketReportTable.StringResource.TableColumnExpandCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.RenderEndTag(); // Td

            writer.AddAttribute(HtmlTextWriterAttribute.Class, DataBoundReportTable.SR.TableColumnMarketDaysSupplyCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.RenderEndTag(); // Td

            writer.AddAttribute(HtmlTextWriterAttribute.Class, DataBoundReportTable.SR.TableColumnSmallestMarketDaysSupplyCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.RenderEndTag(); // Td

            writer.AddAttribute(HtmlTextWriterAttribute.Class, DataBoundReportTable.SR.TableColumnVehicleInformationCssClass + " " + filterCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            RenderSummaryVehicleInfo(writer);
            writer.RenderEndTag(); // Td

            if (ShowUnitsSold)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, DataBoundReportTable.SR.TableColumnUnitsSoldCssClass);
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.RenderEndTag(); // Td 
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Class, DataBoundReportTable.SR.TableColumnNumberOfListingsCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.RenderEndTag(); // Td

            writer.AddAttribute(HtmlTextWriterAttribute.Class, DataBoundReportTable.SR.TableColumnAverageMarketPriceCssClass + " " + filterCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            RenderSummaryPrice(writer);
            writer.RenderEndTag(); // Td

            writer.AddAttribute(HtmlTextWriterAttribute.Class, DataBoundReportTable.SR.TableColumnAverageMileageCssClass + " " + filterCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            RenderSummaryMileage(writer);
            writer.RenderEndTag(); // Td

            writer.AddAttribute(HtmlTextWriterAttribute.Class, DataBoundReportTable.SR.TableColumnUnitsInStockCssClass + " " + filterCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            RenderSummaryUnitsInStock(writer);
            writer.RenderEndTag(); // Td

            writer.AddAttribute(HtmlTextWriterAttribute.Class, MarketReportTable.StringResource.TableColumnShoppingListEditCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);

            writer.RenderEndTag(); // Td

            writer.RenderEndTag(); // Tr
        }

        private void RenderTableFilterRow(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.TableRowFilterCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, MarketReportTable.StringResource.TableColumnExpandCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.RenderEndTag(); // Td

            writer.AddAttribute(HtmlTextWriterAttribute.Class, DataBoundReportTable.SR.TableColumnMarketDaysSupplyCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.RenderEndTag(); // Td

            writer.AddAttribute(HtmlTextWriterAttribute.Class, DataBoundReportTable.SR.TableColumnSmallestMarketDaysSupplyCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.RenderEndTag(); // Td

            writer.AddAttribute(HtmlTextWriterAttribute.Class, DataBoundReportTable.SR.TableColumnVehicleInformationCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            RenderTableFilterVehicleColumn(writer);
            writer.RenderEndTag(); // td

            if (ShowUnitsSold)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, DataBoundReportTable.SR.TableColumnUnitsSoldCssClass);
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.RenderEndTag(); // Td 
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Class, DataBoundReportTable.SR.TableColumnNumberOfListingsCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.RenderEndTag(); // Td

            writer.AddAttribute(HtmlTextWriterAttribute.Class, DataBoundReportTable.SR.TableColumnAverageMarketPriceCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            RenderPriceFilter(writer);
            writer.RenderEndTag(); // Td

            writer.AddAttribute(HtmlTextWriterAttribute.Class, DataBoundReportTable.SR.TableColumnAverageMileageCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            RenderMileageFilter(writer);
            writer.RenderEndTag(); // Td

            writer.AddAttribute(HtmlTextWriterAttribute.Class, DataBoundReportTable.SR.TableColumnUnitsInStockCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            RenderUnitsInStockFilter(writer);
            writer.RenderEndTag(); // Td

            writer.AddAttribute(HtmlTextWriterAttribute.Class, MarketReportTable.StringResource.TableColumnShoppingListEditCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.RenderEndTag(); // Td

            writer.RenderEndTag(); // Tr
        }

        #region Summary Render Methods

        internal void RenderSummaryVehicleInfo(HtmlTextWriter writer)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.Dl);

            writer.RenderBeginTag(HtmlTextWriterTag.Dt);

            writer.Write(SR.MinYearLabel);

            writer.RenderEndTag(); // Dt

            writer.RenderBeginTag(HtmlTextWriterTag.Dd);

            int minModelYear = Filters.MinModelYear == 0 ? ModelYears[ModelYears.Count - 1].Id : Filters.MinModelYear;

            int maxModelYear = Filters.MaxModelYear == 0 ? ModelYears[0].Id : Filters.MaxModelYear;

            writer.Write("{0} to {1}", minModelYear, maxModelYear);

            writer.RenderEndTag(); // Dd

            writer.RenderEndTag(); // Dl

            writer.RenderBeginTag(HtmlTextWriterTag.Dl);

            writer.RenderBeginTag(HtmlTextWriterTag.Dt);

            writer.Write(SR.MakeLabel);

            writer.RenderEndTag(); // Dt

            writer.RenderBeginTag(HtmlTextWriterTag.Dd);

            if (Filters.MakeId == 0)
            {
                writer.Write("Any");
            }
            else
            {
                writer.Write(Makes.GetItem(Filters.MakeId).Name);
            }

            writer.RenderEndTag(); // Dd

            writer.RenderEndTag(); // Dl

            writer.RenderBeginTag(HtmlTextWriterTag.Dl);

            writer.RenderBeginTag(HtmlTextWriterTag.Dt);

            writer.Write(SR.ModelLabel);

            writer.RenderEndTag(); // Dt

            writer.RenderBeginTag(HtmlTextWriterTag.Dd);

            if (Filters.ModelFamilyId == 0)
            {
                writer.Write("Any");
            }
            else
            {
                writer.Write(CurrentModelFamilies.GetItem(Filters.ModelFamilyId).Name);
            }

            writer.RenderEndTag(); // Dd

            writer.RenderEndTag(); // Dl
        }

        internal void RenderSummaryPrice(HtmlTextWriter writer)
        {
            if (Filters.MinInternetPrice == 0)
            {
                writer.Write("0");
            }
            else
            {
                writer.Write("${0:#,#}", Filters.MinInternetPrice);
            }

            writer.Write(" to ");

            if (Filters.MaxInternetPrice > 0 && Filters.MaxInternetPrice != int.MaxValue)
            {
                writer.Write("${0:#,#}", Filters.MaxInternetPrice);
            }
            else
            {
                writer.Write("Any");
            }
        }

        internal void RenderSummaryMileage(HtmlTextWriter writer)
        {
            if (Filters.MinInternetMileage == 0)
            {
                writer.Write("0");
            }
            else
            {
                writer.Write("{0:#,#}", Filters.MinInternetMileage);
            }

            writer.Write(" to ");

            if (Filters.MaxInternetMileage > 0 && Filters.MaxInternetMileage != int.MaxValue)
            {
                writer.Write("{0:#,#}", Filters.MaxInternetMileage);
            }
            else
            {
                writer.Write("Any");
            }
        }

        internal void RenderSummaryUnitsInStock(HtmlTextWriter writer)
        {
            if (Filters.MaxUnitsInStock == 0)
            {
                writer.Write(SR.UnitsInStockNoneLabel);
            }
            else
            {
                if (Filters.MinUnitsInStock == 0)
                {
                    writer.Write(SR.UnitsInStockAllLabel);
                }
                else
                {
                    writer.Write(SR.UnitsInStockOnlyLabel);
                }
            }
        }
       
        #endregion

        #region Filter Render Methods

        internal void RenderTableFilterVehicleColumn(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.FilterCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "hform");

            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

            writer.RenderBeginTag(HtmlTextWriterTag.P);

            RenderYearFilter(writer);

            writer.RenderEndTag(); // P

            writer.RenderBeginTag(HtmlTextWriterTag.P);

            RenderMakeFilter(writer);

            writer.RenderEndTag(); // P

            writer.RenderBeginTag(HtmlTextWriterTag.P);

            RenderModelFilter(writer);

            writer.RenderEndTag(); // P

            RenderSegmentFilter(writer);

            RenderFilterButton(writer, CreateId(SR.VehicleInfoUpdateId));

            writer.RenderEndTag(); // Fieldset

            writer.RenderEndTag(); // Div
        }

        internal void RenderYearFilter(HtmlTextWriter writer)
        {
            int minModelYear = NewFilters.MinModelYear == 0 ? ModelYears[ModelYears.Count - 1].Id : NewFilters.MinModelYear;

            int maxModelYear = NewFilters.MaxModelYear == 0 ? ModelYears[0].Id : NewFilters.MaxModelYear;

            string minYearId = CreateId(SR.MinYearIdPrefix);

            writer.AddAttribute(HtmlTextWriterAttribute.For, minYearId);

            writer.RenderBeginTag(HtmlTextWriterTag.Label);

            writer.Write(SR.MinYearLabel);

            writer.RenderEndTag(); //Label

            writer.AddAttribute(HtmlTextWriterAttribute.Id, minYearId);

            writer.AddAttribute(HtmlTextWriterAttribute.Name, minYearId);

            writer.RenderBeginTag(HtmlTextWriterTag.Select);

            RenderModelYearOptions(writer, minModelYear);

            writer.RenderEndTag(); // Select

            string maxYearId = CreateId(SR.MaxYearIdPrefix);

            writer.AddAttribute(HtmlTextWriterAttribute.For, maxYearId);

            writer.RenderBeginTag(HtmlTextWriterTag.Label);

            writer.Write(SR.MaxYearLabel);

            writer.RenderEndTag(); //Label

            writer.AddAttribute(HtmlTextWriterAttribute.Id, maxYearId);

            writer.AddAttribute(HtmlTextWriterAttribute.Name, maxYearId);

            writer.RenderBeginTag(HtmlTextWriterTag.Select);

            RenderModelYearOptions(writer, maxModelYear);

            writer.RenderEndTag(); // Select
        }

        private void RenderModelYearOptions(HtmlTextWriter writer, int selectedModelYear)
        {
            foreach (ModelYear modelYear in ModelYears)
            {
                if (modelYear.Id == selectedModelYear)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Selected, "selected");
                }

                writer.AddAttribute(HtmlTextWriterAttribute.Value, modelYear.Id.ToString());

                writer.RenderBeginTag(HtmlTextWriterTag.Option);

                writer.Write(modelYear.Name);

                writer.RenderEndTag();
            }
        }

        internal void RenderMakeFilter(HtmlTextWriter writer)
        {
            string id = CreateId(SR.MakeIdPrefix);

            writer.AddAttribute(HtmlTextWriterAttribute.For, id);

            writer.RenderBeginTag(HtmlTextWriterTag.Label);

            writer.Write(SR.MakeLabel);

            writer.RenderEndTag(); // Label

            writer.AddAttribute(HtmlTextWriterAttribute.Id, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Name, id);

            writer.RenderBeginTag(HtmlTextWriterTag.Select);

            writer.RenderBeginTag(HtmlTextWriterTag.Option);

            writer.RenderEndTag();

            foreach (Make make in Makes)
            {
                if (NewFilters.MakeId == make.Id)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Selected, "selected");
                }

                writer.AddAttribute(HtmlTextWriterAttribute.Value, make.Id.ToString());

                writer.RenderBeginTag(HtmlTextWriterTag.Option);

                writer.Write(make.Name);

                writer.RenderEndTag(); // Option
            }

            writer.RenderEndTag(); // select

            RenderSubmitButton(writer, CreateId(SR.ChooseMakeId), "+", false);
        }

        internal void RenderModelFilter(HtmlTextWriter writer)
        {
            string id = CreateId(SR.ModelIdPrefix);

            writer.AddAttribute(HtmlTextWriterAttribute.For, id);

            writer.RenderBeginTag(HtmlTextWriterTag.Label);

            writer.Write(SR.ModelLabel);

            writer.RenderEndTag(); // Label

            writer.AddAttribute(HtmlTextWriterAttribute.Id, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Name, id);

            writer.RenderBeginTag(HtmlTextWriterTag.Select);

            writer.RenderBeginTag(HtmlTextWriterTag.Option);

            writer.RenderEndTag();

            foreach (ModelFamily modelFamily in ModelFamilies)
            {
                if (NewFilters.ModelFamilyId == modelFamily.Id)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Selected, "selected");
                }

                writer.AddAttribute(HtmlTextWriterAttribute.Value, modelFamily.Id.ToString());

                writer.RenderBeginTag(HtmlTextWriterTag.Option);

                writer.Write(modelFamily.Name);

                writer.RenderEndTag(); // Option
            }

            writer.RenderEndTag(); // Select
        }

        internal void RenderSegmentFilter(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "checkbox");

            writer.RenderBeginTag(HtmlTextWriterTag.P);

            bool allOrNoneSelected = NewFilters.AreAllSegmentsSelected() || NewFilters.AreNoSegmentsSelected();

            RenderSubmitButton(writer, CreateId(SR.SegmentAllIdPrefix), SR.SegmentAllLabel);

            writer.RenderEndTag(); // P 

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.CheckBoxListCssClass + " wrapper");

            writer.RenderBeginTag(HtmlTextWriterTag.Ul);

            foreach (Segment segment in Segments)
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Li);

                RenderCheckBox(
                    writer,
                    SR.SegmentIdPrefix + IdSeparator + segment.Id,
                    segment.Name,
                    allOrNoneSelected || NewFilters.IsSegmentSelected(segment.Id));

                writer.RenderEndTag(); // Li
            }

            writer.RenderEndTag(); // Ul
        }

        internal void RenderPriceFilter(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.FilterCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

            writer.RenderBeginTag(HtmlTextWriterTag.P);

            string minId = CreateId(SR.MinPriceIdPrefix);

            writer.AddAttribute(HtmlTextWriterAttribute.For, minId);

            writer.RenderBeginTag(HtmlTextWriterTag.Label);

            writer.Write(SR.MinPriceLabel);

            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.Id, minId);

            writer.AddAttribute(HtmlTextWriterAttribute.Name, minId);

            writer.RenderBeginTag(HtmlTextWriterTag.Select);

            RenderPriceOptions(writer, NewFilters.MinInternetPrice);

            writer.RenderEndTag(); // Select

            string maxId = CreateId(SR.MaxPriceIdPrefix);

            writer.AddAttribute(HtmlTextWriterAttribute.For, maxId);

            writer.RenderBeginTag(HtmlTextWriterTag.Label);

            writer.Write(SR.MaxPriceLabel);

            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.Id, maxId);

            writer.AddAttribute(HtmlTextWriterAttribute.Name, maxId);

            writer.RenderBeginTag(HtmlTextWriterTag.Select);

            RenderPriceOptions(writer, NewFilters.MaxInternetPrice);

            writer.RenderEndTag(); // Select

            writer.RenderEndTag(); // p

            RenderFilterButton(writer, CreateId(SR.PriceUpdateId));

            writer.RenderEndTag(); // Fieldset

            writer.RenderEndTag(); // div
        }

        private static void RenderPriceOptions(HtmlTextWriter writer, int selectedPrice)
        {
            foreach (int price in PriceFilterValues)
            {
                if (price == selectedPrice)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Selected, "selected");
                }

                writer.AddAttribute(HtmlTextWriterAttribute.Value, price.ToString());

                writer.RenderBeginTag(HtmlTextWriterTag.Option);

                if (price != int.MaxValue)
                {
                    writer.Write("${0:#,#0}", price); 
                }
                else
                {
                    writer.Write("$75K +");
                }

                writer.RenderEndTag(); // Option
            }
        }

        internal void RenderMileageFilter(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.FilterCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

            writer.RenderBeginTag(HtmlTextWriterTag.P);

            string minId = CreateId(SR.MinMileageIdPrefix);

            writer.AddAttribute(HtmlTextWriterAttribute.For, minId);

            writer.RenderBeginTag(HtmlTextWriterTag.Label);

            writer.Write(SR.MinMileageLabel);

            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.Id, minId);

            writer.AddAttribute(HtmlTextWriterAttribute.Name, minId);

            writer.RenderBeginTag(HtmlTextWriterTag.Select);

            RenderMileageOptions(writer, NewFilters.MinInternetMileage);

            writer.RenderEndTag(); // Select

            string maxId = CreateId(SR.MaxMileageIdPrefix);

            writer.AddAttribute(HtmlTextWriterAttribute.For, maxId);

            writer.RenderBeginTag(HtmlTextWriterTag.Label);

            writer.Write(SR.MaxMileageLabel);

            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.Id, maxId);

            writer.AddAttribute(HtmlTextWriterAttribute.Name, maxId);

            writer.RenderBeginTag(HtmlTextWriterTag.Select);

            RenderMileageOptions(writer, NewFilters.MaxInternetMileage);

            writer.RenderEndTag(); // Select

            writer.RenderEndTag(); // P

            RenderFilterButton(writer, CreateId(SR.MileageUpdateId));

            writer.RenderEndTag(); // Fieldset

            writer.RenderEndTag(); // Div
        }

        private static void RenderMileageOptions(HtmlTextWriter writer, int selectedMileage)
        {
            foreach (int mileage in MileageFilterValues)
            {
                if (selectedMileage == mileage)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Selected, "selected");
                }

                writer.AddAttribute(HtmlTextWriterAttribute.Value, mileage.ToString());

                writer.RenderBeginTag(HtmlTextWriterTag.Option);

                if (mileage != int.MaxValue)
                {
                    writer.Write("{0:#,#0}", mileage); 
                }
                else
                {
                    writer.Write("140k +");
                }

                writer.RenderEndTag(); // Option
            }
        }

        internal void RenderUnitsInStockFilter(HtmlTextWriter writer)
        {
            bool all, stocked, notStocked;

            if (Filters.MaxUnitsInStock == 0)
            {
                all = stocked = false;

                notStocked = true;
            }
            else
            {
                if (Filters.MinUnitsInStock == 0)
                {
                    stocked = notStocked = false;

                    all = true;
                }
                else
                {
                    all = notStocked = false;

                    stocked = true;
                }
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.FilterCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, SR.RadioListCssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Ul);

            writer.RenderBeginTag(HtmlTextWriterTag.Li);

            RenderRadioButton(writer, SR.UnitsInStockAllIdPrefix, SR.UnitsInStockInputName, SR.UnitsInStockAllLabel, all);

            writer.RenderEndTag(); // Li

            writer.RenderBeginTag(HtmlTextWriterTag.Li);

            RenderRadioButton(writer, SR.UnitsInStockOnlyIdPrefix, SR.UnitsInStockInputName, SR.UnitsInStockOnlyLabel, stocked);

            writer.RenderEndTag(); // Li

            writer.RenderBeginTag(HtmlTextWriterTag.Li);

            RenderRadioButton(writer, SR.UnitsInStockNoneIdPrerfix, SR.UnitsInStockInputName, SR.UnitsInStockNoneLabel, notStocked);

            writer.RenderEndTag(); // Li

            writer.RenderEndTag(); // Ul

            RenderFilterButton(writer, CreateId(SR.UnitsInStockUpdateId));

            writer.RenderEndTag(); // Fieldset

            writer.RenderEndTag(); // Div
        }

        private void RenderCheckBox(HtmlTextWriter writer, string id, string label, bool selected)
        {
            string uniqueId = CreateId(id);

            writer.AddAttribute(HtmlTextWriterAttribute.For, uniqueId);

            writer.RenderBeginTag(HtmlTextWriterTag.Label);

            writer.Write(label);

            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.Type, "checkbox");

            if (selected)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Checked, bool.TrueString);
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Id, uniqueId);

            writer.AddAttribute(HtmlTextWriterAttribute.Name, uniqueId);

            writer.AddAttribute(HtmlTextWriterAttribute.Value, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "segment checkbox");

            writer.RenderBeginTag(HtmlTextWriterTag.Input);

            writer.RenderEndTag(); // Input
        }

        private void RenderRadioButton(HtmlTextWriter writer, string id, string name, string label, bool selected)
        {
            string uniqueId = CreateId(id);

            writer.AddAttribute(HtmlTextWriterAttribute.Type, "radio");

            writer.AddAttribute(HtmlTextWriterAttribute.Id, uniqueId);

            writer.AddAttribute(HtmlTextWriterAttribute.Name, CreateName(name));

            writer.AddAttribute(HtmlTextWriterAttribute.Value, id);

            if (selected)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Checked, "checked");
            }

            writer.RenderBeginTag(HtmlTextWriterTag.Input);

            writer.RenderEndTag(); // Input

            writer.AddAttribute(HtmlTextWriterAttribute.For, uniqueId);

            writer.RenderBeginTag(HtmlTextWriterTag.Label);

            writer.Write(label);

            writer.RenderEndTag(); // Label
        }

        private static void RenderSubmitButton(HtmlTextWriter writer, string id, string label, bool wrap)
        {
            if (wrap)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "button");

                writer.RenderBeginTag(HtmlTextWriterTag.Span);
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Type, "submit");

            writer.AddAttribute(HtmlTextWriterAttribute.Value, label);

            writer.AddAttribute(HtmlTextWriterAttribute.Id, id);

            writer.AddAttribute(HtmlTextWriterAttribute.Name, id);

            writer.RenderBeginTag(HtmlTextWriterTag.Input);

            writer.RenderEndTag(); // Input

            if (wrap)
            {
                writer.RenderEndTag(); // Span 
            }
        }

        private static void RenderSubmitButton(HtmlTextWriter writer, string id, string label)
        {
            RenderSubmitButton(writer, id, label, true);
        }

        private static void RenderFilterButton(HtmlTextWriter writer, string id)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "filter_control");

            writer.RenderBeginTag(HtmlTextWriterTag.P);

            RenderSubmitButton(writer, id, SR.FilterButtonText);

            writer.RenderEndTag();
        }

        #endregion

        protected string CreateId(params string[] strings)
        {
            StringBuilder id = new StringBuilder(UniqueID);

            foreach (string s in strings)
            {
                id.Append(IdSeparator);
                id.Append(s);
            }

            return id.ToString();
        }

        protected string CreateName(string s)
        {
            StringBuilder id = new StringBuilder(UniqueID);

            id.Append(IdSeparator);
            id.Append(s);

            return id.ToString();
        }

        #endregion

        #region IStateManager Members

        protected override void LoadViewState(object state)
        {
            object[] propertyState = state as object[];

            if (propertyState != null)
            {
                if (propertyState[0] != null)
                {
                    ((IStateManager)Filters).LoadViewState(propertyState[0]);
                }

                if (propertyState[1] != null)
                {
                    ((IStateManager)NewFilters).LoadViewState(propertyState[1]);
                }

            }
        }

        protected override object SaveViewState()
        {
            object[] propertyState = new object[2];

            propertyState[0] = _filters == null ? null : ((IStateManager)_filters).SaveViewState();

            propertyState[1] = _newFilters == null ? null : ((IStateManager)_newFilters).SaveViewState();

            return propertyState;
        }

        #endregion

        #region IPostBackDataHandler Members

        protected override Control FindControl(string id, int pathOffset)
        {
            return this;
        }

        private bool _selectAllSegments;

        private bool _vehicalInfoFilterChanged;

        private bool _priceFilterChanged;

        private bool _mileageFilterChanged;

        private bool _unitsInStockFilterChanged;

        private bool _raisedChanged;

        private bool _testCheckBoxes = true;

        private static bool IsCommand(string text, string command)
        {
            return (string.Equals(command, text, StringComparison.InvariantCulture));
        }

        private static bool IsValue(string text, string value)
        {
            return IsCommand(text, value);
        }

        private static bool IsCommand(string text, int length, string command)
        {
            int copyLength = length > text.Length ? text.Length : length;
            return (string.Equals(text.Substring(0, copyLength), command, StringComparison.InvariantCulture));
        }

        protected bool LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
            if (!postDataKey.StartsWith(UniqueID, StringComparison.InvariantCulture))
            {
                return false;
            }

            int idOffset = UniqueID.Length + 1;

            string commandName = postDataKey.Substring(idOffset);

            string commandValue = postCollection[postDataKey];

            bool haveIntValue = false;

            int value;

            if (int.TryParse(commandValue, out value))
            {
                haveIntValue = true;
            }

            bool changed = false;

            if (IsCommand(commandName, SR.MinYearIdPrefix))
            {
                if (haveIntValue)
                {
                    NewFilters.MinModelYear = value;
                }
            }
            else if (IsCommand(commandName, SR.MaxYearIdPrefix))
            {
                if (haveIntValue)
                {
                    NewFilters.MaxModelYear = value;
                }
            }
            else if (IsCommand(commandName, SR.MakeIdPrefix))
            {
                if (haveIntValue)
                {
                    NewFilters.MakeId = value;
                }
                else
                {
                    NewFilters.MakeId = 0;
                }
            }
            else if (IsCommand(commandName, SR.ModelIdPrefix))
            {
                if (haveIntValue)
                {
                    NewFilters.ModelFamilyId = value;
                }
                else
                {
                    NewFilters.ModelFamilyId = 0;
                }
            }
            else if (IsCommand(commandName, SR.MinPriceIdPrefix))
            {
                if (haveIntValue)
                {
                    NewFilters.MinInternetPrice = value;
                }
            }
            else if (IsCommand(commandName, SR.MaxPriceIdPrefix))
            {
                if (haveIntValue)
                {
                    NewFilters.MaxInternetPrice = value;
                }
            }
            else if (IsCommand(commandName, SR.MinMileageIdPrefix))
            {
                if (haveIntValue)
                {
                    NewFilters.MinInternetMileage = value;
                }
            }
            else if (IsCommand(commandName, SR.MaxMileageIdPrefix))
            {
                if (haveIntValue)
                {
                    NewFilters.MaxInternetMileage = value;
                }
            }
            else if (IsCommand(commandName, SR.UnitsInStockInputName))
            {
                if (IsValue(commandValue, SR.UnitsInStockAllIdPrefix))
                {
                    NewFilters.MinUnitsInStock = 0;
                    NewFilters.MaxUnitsInStock = int.MaxValue; 
                }
                else if (IsValue(commandValue, SR.UnitsInStockNoneIdPrerfix))
                {
                    NewFilters.MinUnitsInStock = 0;
                    NewFilters.MaxUnitsInStock = 0;    
                }
                else if (IsValue(commandValue, SR.UnitsInStockOnlyIdPrefix))
                {
                    NewFilters.MinUnitsInStock = 1;
                    NewFilters.MaxUnitsInStock = int.MaxValue;    
                }
            }            
            else if (IsCommand(commandName, SR.SegmentAllIdPrefix))
            {
                _selectAllSegments = true;

                changed = true;
            }
            else if (IsCommand(commandName, SR.ChooseMakeId))
            {
                if (haveIntValue)
                {
                    NewFilters.MakeId = value;
                }
            }
            else if (IsCommand(commandName, SR.VehicleInfoUpdateId))
            {
                changed = _vehicalInfoFilterChanged = true;
            }
            else if (IsCommand(commandName, SR.PriceUpdateId))
            {
                changed = _priceFilterChanged = true;
            }
            else if (IsCommand(commandName, SR.MileageUpdateId))
            {
                changed = _mileageFilterChanged = true;
            }
            else if (IsCommand(commandName, SR.UnitsInStockUpdateId))
            {
                changed = _unitsInStockFilterChanged = true;
            }

            if (IsCommand(commandName, SR.SegmentIdPrefix.Length, SR.SegmentIdPrefix) && _testCheckBoxes)
            {
                foreach (Pair pair in NewFilters.SegmentSelections)
                {
                    string id = CreateId(SR.SegmentIdPrefix, ((int)pair.First).ToString());

                    pair.Second = (postCollection[id] != null);
                }
                NewFilters.PropertyHasChanged = true;

                _testCheckBoxes = false;
            }

            if (_raisedChanged)
            {
                return false;
            }

            if (changed)
            {
                return (_raisedChanged = true);
            }

            return false;
        }

        protected void RaisePostDataChangedEvent()
        {
            bool filterHasChanged = false;

            if (_selectAllSegments)
            {
                foreach (Pair pair in NewFilters.SegmentSelections)
                {
                    pair.Second = true;
                }
            }
            else if (_vehicalInfoFilterChanged)
            {
                Filters.UpdateVehcileInfo(NewFilters);

                filterHasChanged = true;
            }
            else if (_priceFilterChanged)
            {
                Filters.UpdatePrice(NewFilters);

                filterHasChanged = true;
            }
            else if (_mileageFilterChanged)
            {
                Filters.UpdateMileage(NewFilters);

                filterHasChanged = true;
            }
            else if (_unitsInStockFilterChanged)
            {
                Filters.UpdateUnitsInStock(NewFilters);

                filterHasChanged = true;
            }

            if (filterHasChanged && Filters.PropertyHasChanged)
            {
                OnFilterChanged();
            }
        }

        bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
            return LoadPostData(postDataKey, postCollection);
        }

        void IPostBackDataHandler.RaisePostDataChangedEvent()
        {
            RaisePostDataChangedEvent();
        }

        #endregion

        #region IScriptControl

        private ScriptManager _manager;

        private ScriptBehaviorDescriptor DropDownListDescriptor(string minElement, string maxElement)
        {
            ScriptBehaviorDescriptor downListDescriptor =
                new ScriptBehaviorDescriptor("FirstLook.Pricing.WebControls.DropDownListValueBehavior",
                                             CreateId(minElement));
            downListDescriptor.AddElementProperty("DropDownList", CreateId(maxElement));

            return downListDescriptor;
        }

        public IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {
            ScriptBehaviorDescriptor scriptDescriptor =
                new ScriptBehaviorDescriptor("FirstLook.Pricing.WebControls.MarketReportTableFilterHelper", ClientID);


            scriptDescriptor.AddProperty("has_column_filter_class", SR.HasColumnFilter);
            scriptDescriptor.AddProperty("filter_row_class", SR.TableRowFilterCssClass);

            scriptDescriptor.AddElementProperty("choose_make_button", CreateId(SR.ChooseMakeId));
            scriptDescriptor.AddElementProperty("choose_make_select", CreateId(SR.MakeIdPrefix));
            scriptDescriptor.AddElementProperty("select_all_segments_button", CreateId(SR.SegmentAllIdPrefix));

            ScriptBehaviorDescriptor priceDropDownListDescriptor =
                new ScriptBehaviorDescriptor("FirstLook.Pricing.WebControls.DropDownListValueBehavior",
                                             CreateId(SR.MinPriceIdPrefix));
            priceDropDownListDescriptor.AddElementProperty("DropDownList", CreateId(SR.MaxPriceIdPrefix));

            ScriptBehaviorDescriptor mileageDropDownListDescriptor =
                new ScriptBehaviorDescriptor("FirstLook.Pricing.WebControls.DropDownListValueBehavior",
                                             CreateId(SR.MinMileageIdPrefix));
            mileageDropDownListDescriptor.AddElementProperty("DropDownList", CreateId(SR.MaxMileageIdPrefix));

            return new ScriptDescriptor[]
                       {
                           scriptDescriptor, 
                           DropDownListDescriptor(SR.MinPriceIdPrefix, SR.MaxPriceIdPrefix),
                           DropDownListDescriptor(SR.MinMileageIdPrefix, SR.MaxMileageIdPrefix),
                           DropDownListDescriptor(SR.MaxYearIdPrefix, SR.MinYearIdPrefix)
                       };
        }

        public IEnumerable<ScriptReference> GetScriptReferences()
        {
            return new ScriptReference[]
                       {
                           new ScriptReference("FirstLook.Pricing.WebControls.MarketReportTableFilterHelper.js",
                                               "FirstLook.Pricing.WebControls"),
                           new ScriptReference("FirstLook.Pricing.WebControls.DropDownListValueBehavior.js",
                                               "FirstLook.Pricing.WebControls")
                       };
        }

        #endregion

        #region String Resources

        private static class SR
        {
            #region CssClasses

            public const string TableRowFilterSummaryCssClass = "filter_summary";
            public const string TableRowFilterCssClass = "filters";

            public const string FilterCssClass = "filter";
            public const string CheckBoxListCssClass = "checkbox_list";
            public const string RadioListCssClass = "radio_list";
            public const string HasColumnFilter = "has_filter";
            public const string HasColumnFilterOpen = "has_filter_open";

            #endregion

            #region Labels

            public const string MinYearLabel = "Year: ";
            public const string MaxYearLabel = "to";
            public const string MakeLabel = "Make: ";
            public const string ModelLabel = "Model: ";
            public const string SegmentAllLabel = "Check All Segments";
            public const string MinPriceLabel = "Price";
            public const string MaxPriceLabel = "to";
            public const string MinMileageLabel = "Mileage";
            public const string MaxMileageLabel = "to";
            public const string UnitsInStockInputName = "UnitsInStockRadio";
            public const string UnitsInStockAllLabel = "All";
            public const string UnitsInStockOnlyLabel = "Yes";
            public const string UnitsInStockNoneLabel = "No";

            #endregion

            #region IdPrefixes

            public const string MinYearIdPrefix = "minYear";
            public const string MaxYearIdPrefix = "maxYear";
            public const string MakeIdPrefix = "make";
            public const string ModelIdPrefix = "model";
            public const string SegmentAllIdPrefix = "check_all";
            public const string SegmentIdPrefix = "segment";
            public const string MinPriceIdPrefix = "minPrice";
            public const string MaxPriceIdPrefix = "maxPrice";
            public const string MinMileageIdPrefix = "minMileage";
            public const string MaxMileageIdPrefix = "maxMileage";
            public const string UnitsInStockAllIdPrefix = "allUnitsInStock";
            public const string UnitsInStockOnlyIdPrefix = "onlyUnitsInStock";
            public const string UnitsInStockNoneIdPrerfix = "noneUnitsInStock";

            #endregion

            public const string ChooseMakeId = "ChooseMake";
            public const string VehicleInfoUpdateId = "VehicleInfoUpdate";
            public const string PriceUpdateId = "PriceUpdate";
            public const string MileageUpdateId = "MileageUpdate";
            public const string UnitsInStockUpdateId = "UnitsInStockUpdate";
            public const string FilterButtonText = "Filter";
        }

        #endregion
    }
}