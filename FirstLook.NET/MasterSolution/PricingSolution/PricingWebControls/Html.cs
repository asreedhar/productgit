using System;
using System.Collections.Specialized;
using System.Text;
using System.Web.UI;

namespace FirstLook.Pricing.WebControls
{
    internal static class Html
    {
        public delegate void RenderControl(HtmlTextWriter writer, string id, string value);

        public delegate void RenderConditionalControl(HtmlTextWriter writer, string id, string value, bool enabled, NameValueCollection additionalAttrs);

        public static void RenderCell(HtmlTextWriter writer, string id, string value, RenderControl control)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            control(writer, id, value);
            writer.RenderEndTag();
            writer.WriteLine();
        }

        public static void RenderCell(HtmlTextWriter writer, string id, string value, bool enabled, RenderConditionalControl control, string className)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, className);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            control(writer, id, value, enabled, null);
            writer.RenderEndTag();
            writer.WriteLine();
        }

        public static void RenderTextCell(HtmlTextWriter writer, string text, string className)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, className);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.Write(text);
            writer.RenderEndTag();
            writer.WriteLine();
        }

        public static void RenderHeaderTextCell(HtmlTextWriter writer, string text, string className)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, className);
            writer.RenderBeginTag(HtmlTextWriterTag.Th);
            writer.Write(text);
            writer.RenderEndTag();
            writer.WriteLine();
        }

        public static void RenderHiddenField(HtmlTextWriter writer, string id, string value)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Id, id);
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "hidden");
            writer.AddAttribute(HtmlTextWriterAttribute.Name, id);
            writer.AddAttribute(HtmlTextWriterAttribute.Value, value);
            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();
        }

        public static void RenderTextBox(HtmlTextWriter writer, string id, string text)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Id, id);
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "text");
            writer.AddAttribute(HtmlTextWriterAttribute.Name, id);
            writer.AddAttribute(HtmlTextWriterAttribute.Value, text);
            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();
        }

        public static void RenderCheckBox(HtmlTextWriter writer, string id, bool isChecked)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Id, id);
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "checkbox");
            writer.AddAttribute(HtmlTextWriterAttribute.Name, id);
            if (isChecked)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Checked, bool.TrueString);
            }
            writer.AddAttribute(HtmlTextWriterAttribute.Value, bool.TrueString);
            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();
        }

        public static void RenderButton(HtmlTextWriter writer, string id, string text, bool enabled, NameValueCollection additionalAttrs)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Id, id);
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "submit");
            writer.AddAttribute(HtmlTextWriterAttribute.Name, id);
            writer.AddAttribute(HtmlTextWriterAttribute.Value, text);

            if (additionalAttrs != null)
            {
                foreach (string key in additionalAttrs)
                {
                    writer.AddAttribute(key, additionalAttrs.Get(key));
                }
            }

            if (!enabled)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Disabled, Boolean.TrueString);
            }
            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();
        }

        public static string ExternalId(string id, char seperator, params string[] names)
        {
            StringBuilder sb = new StringBuilder(id);

            foreach (string name in names)
            {
                sb.Append(seperator).Append(name);
            }

            return sb.ToString();
        }

        public static string ExternalId(string id, char seperator, int rowId, params string[] names)
        {
            StringBuilder sb = new StringBuilder(id);

            sb.Append(seperator).Append(rowId.ToString("G"));

            foreach (string name in names)
            {
                sb.Append(seperator).Append(name);
            }

            return sb.ToString();
        }
    }
}