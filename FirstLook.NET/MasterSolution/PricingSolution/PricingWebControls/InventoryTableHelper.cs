using System.Collections.Generic;

namespace FirstLook.Pricing.WebControls
{
    public static class InventoryTableHelper
    {
        public static string Age = "Age";
        public static string Risk = "Risk";
        public static string VehicleDescription = "VehicleDescription";
        public static string VehicleColor = "VehicleColor";
        public static string VehicleMileage = "VehicleMileage";
        public static string UnitCost = "UnitCost";
        public static string ListPrice = "ListPrice";
        public static string AvgPinSalePrice = "AvgPinSalePrice";
        public static string PctAvgMarketPrice = "PctAvgMarketPrice";
        public static string ChangePctAvgMarketPrice = "ChangePctAvgMarketPrice";
        public static string MarketDaysSupply = "MarketDaysSupply";
        public static string PrecisionTrimSearch = "PrecisionTrimSearch";
        public static string StockNumber = "StockNumber";

        private static readonly Dictionary<string, int> InventoryTableColumns = new Dictionary<string, int>();

        static InventoryTableHelper()
        {
            InventoryTableColumns.Add(Age, 0);
            InventoryTableColumns.Add(Risk, 1);
            InventoryTableColumns.Add(VehicleDescription, 2);
            InventoryTableColumns.Add(VehicleColor, 3);
            InventoryTableColumns.Add(VehicleMileage, 4);
            InventoryTableColumns.Add(UnitCost, 5);
            InventoryTableColumns.Add(ListPrice, 6);
            InventoryTableColumns.Add(AvgPinSalePrice, 7);
            InventoryTableColumns.Add(PctAvgMarketPrice, 8);
            InventoryTableColumns.Add(ChangePctAvgMarketPrice, 9);
            InventoryTableColumns.Add(MarketDaysSupply, 10);
            InventoryTableColumns.Add(PrecisionTrimSearch, 11);
            InventoryTableColumns.Add(StockNumber, 12);
        }

        public static int ColumnIndex(string columnName)
        {
            return InventoryTableColumns[columnName];
        }
    }
}