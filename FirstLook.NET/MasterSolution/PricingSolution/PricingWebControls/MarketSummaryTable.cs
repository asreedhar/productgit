using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Utilities;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel;

namespace FirstLook.Pricing.WebControls
{
    /// <summary>
    /// Displays a summary table of market listing data that is keyed on either list price or mileage.
    /// </summary>
    public class MarketSummaryTable : DataBoundControl
    {
        #region Properties

        private string    _columnName;     // Heading for this table - Pricing or Mileage?
        private string    _valueFormat;    // How to format values? Normal numbers, or currency?        
        private int       _high;           // Highest value in market listings.
        private int       _low;            // Lowest value in market listings.
        private int       _avg;            // Average value in market listings.
        private int       _you;            // Your value in market listings.        
        private string    _youValueTitle;
        private string    _youValueClassName;
        private int       _comparableListings;
        private int       _totalListings;
        private bool      _showReference;
        private bool      _useOwnerPreference;
        private OwnerPreferences _ownerPreferences;

        public int NumberOfListings
        {
            get
            {
                return _totalListings;
            }
        }

        private bool ShowNotAnalyzed
        {
            get
            {
                return _comparableListings < 2;
            }
        }

        public string ColumnName
        {
            get { return _columnName; }
            set
            {
                if (_columnName != value)
                {
                    _columnName = value;
                }
            }
        }

        public string ValueFormat
        {
            get
            {
                if (string.IsNullOrEmpty(_valueFormat))
                {
                    return "{0}";
                }
                return _valueFormat;
            }
            set
            {
                if (_valueFormat != value)
                {
                    _valueFormat = value;
                }
            }
        }        

        public int ComparableListings
        {
            get
            {
                return _comparableListings;
            }
        }

        public bool ShowReference
        {
            get { return _showReference; }
            set { _showReference = value; }
        }

        public bool UseOwnerPreference
        {
            get { return _useOwnerPreference; }
            set { _useOwnerPreference = value; }
        }

        public OwnerPreferences OwnerPreferences
        {
            get { return _ownerPreferences; }
            set { _ownerPreferences = value; }
        }

        #endregion

        #region Events

        private static readonly object EventMarketSummaryTableDataBound = new object();

        public event EventHandler<MarketSummaryTableDataBoundEventArgs> MarketSummaryTableDataBound
        {
            add { Events.AddHandler(EventMarketSummaryTableDataBound, value); }
            remove { Events.RemoveHandler(EventMarketSummaryTableDataBound, value); }
        }

        protected void OnDataBound(MarketSummaryTableDataBoundEventArgs e)
        {
            EventHandler<MarketSummaryTableDataBoundEventArgs> handler = (EventHandler<MarketSummaryTableDataBoundEventArgs>)Events[EventMarketSummaryTableDataBound];
            if (handler != null)
            {
                handler(this, e);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Add HTML styles and attributes to the table.
        /// </summary>
        /// <param name="writer">Writer for html content.</param>
        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            CssClass = (string.IsNullOrEmpty(CssClass)) ? StringResource.CssClass : 
                                                          CssClass + " " + StringResource.CssClass;
            writer.AddAttribute(HtmlTextWriterAttribute.Border, "0");
            writer.AddAttribute(HtmlTextWriterAttribute.Cellpadding, "0");
            writer.AddAttribute(HtmlTextWriterAttribute.Cellspacing, "0");
            base.AddAttributesToRender(writer);
        }

        /// <summary>
        /// Gets the value that corresponds to this Web server control.
        /// </summary>
        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Table;
            }
        }

        /// <summary>
        /// What kind of html element is this?
        /// </summary>
        protected override string TagName
        {
            get
            {
                return "table";
            }
        }       

        /// <summary>
        /// Render the market listing summary table.
        /// </summary>
        /// <param name="writer">Writer for html content.</param>
        protected override void RenderContents(HtmlTextWriter writer)
        {
            base.RenderContents(writer);
            RenderTableHeader(writer);
            RenderTableBody(writer);
        }

        /// <summary>
        /// Render the head of the market listing summary table.
        /// </summary>
        /// <param name="writer">Writer for html content.</param>
        private void RenderTableHeader(HtmlTextWriter writer)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.Thead);            

            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            // Header text.            
            writer.AddAttribute(HtmlTextWriterAttribute.Class, StringResource.TableTitleCellCssClass);
            writer.AddAttribute(HtmlTextWriterAttribute.Colspan, "2");
            writer.RenderBeginTag(HtmlTextWriterTag.Th);
            writer.Write(_columnName);            
            writer.RenderEndTag();

            // '% of Avg' text.            
            writer.AddAttribute(HtmlTextWriterAttribute.Class, StringResource.PercentCellCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Th);
            writer.Write(StringResource.PercentOfAvgHeading);
            writer.RenderEndTag();

            writer.RenderEndTag(); // Tr
            
            writer.RenderEndTag(); // Thead
        }

        /// <summary>
        /// Render the body of the market listing summary table.
        /// </summary>
        /// <param name="writer">Writer for html content.</param>
        private void RenderTableBody(HtmlTextWriter writer)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.Tbody);

            List<KeyValuePair<int, string>> values = new List<KeyValuePair<int, string>>();
            values.Add(new KeyValuePair<int, string>(_high, "high"));
            values.Add(new KeyValuePair<int, string>(_avg, "avg"));
            if (ShowReference)
            {
                values.Add(new KeyValuePair<int, string>(_you, "you")); 
            }
            values.Add(new KeyValuePair<int, string>(_low, "low"));            

            if (!ShowNotAnalyzed)
            {
                values.Sort(delegate(KeyValuePair<int, string> a, KeyValuePair<int, string> b)
                                    {
                                        return a.Key.CompareTo(b.Key);
                                    });

                values.Reverse();
            }

            for (int i = 0, l = values.Count; i < l; i++)
            {
                bool isOdd = i%2 == 0;
                bool isLowest = i == l - 1;
                bool isHighest = i == 0;

                if (values[i].Value == "high")
                {
                    RenderHighTableRow(writer, isOdd, isHighest);
                }
                else if (values[i].Value == "avg")
                {
                    RenderAverageTableRow(writer, isOdd);
                }
                else if (values[i].Value == "low")
                {
                    RenderLowTableRow(writer, isOdd, isLowest);
                }
                else if (values[i].Value == "you")
                {
                    RenderYouTableRow(writer, isOdd, isHighest, isLowest);
                }
            }
                                               
            writer.RenderEndTag(); // Tbody
        }

        /// <summary>
        /// Render the table row for the high value from the market listings.
        /// </summary>
        /// <param name="writer">Writer for html content.</param>
        /// <param name="oddRow">Should this row get odd row styling.</param>
        /// <param name="isHighest">Is the first row of the summary</param>
        private void RenderHighTableRow(HtmlTextWriter writer, bool oddRow, bool isHighest)
        {                                   
            // Set the row style.
            if (oddRow && isHighest)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, StringResource.HighRowCssClass + " " + 
                                                                   StringResource.OddRowCssClass);
            }
            else if (isHighest)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, StringResource.HighRowCssClass);    
            }            
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                                    
            // High row - heading.
            writer.RenderBeginTag(HtmlTextWriterTag.Td);            
            writer.Write(isHighest ? StringResource.HighRowHeading : StringResource.NextHighestRowHeading);
            writer.RenderEndTag();
            
            // High row - value.            
            writer.AddAttribute(HtmlTextWriterAttribute.Class, StringResource.ValueCellCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            if (!ShowNotAnalyzed)
            {
                writer.Write(string.Format(ValueFormat, _high)); 
            }
            else
            {
                writer.Write(StringResource.NotAnalyzedText);
            }
            writer.RenderEndTag();
         
            // High row - percent of average.
            writer.AddAttribute(HtmlTextWriterAttribute.Class, StringResource.PercentCellCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            if (!ShowNotAnalyzed)
            {
                writer.Write((100 * _high / _avg) + "%"); 
            }
            else
            {
                writer.Write(StringResource.NotAnalyzedText + "%");
            }
            writer.RenderEndTag();

            writer.RenderEndTag(); // Tr
        }

        /// <summary>
        /// Render the table row for the low value from the market listings.
        /// </summary>
        /// <param name="writer">Writer for html content.</param>
        /// <param name="oddRow">Should this row get odd row styling.</param>
        /// <param name="isLowest">Is the last row of the summary</param>
        private void RenderLowTableRow(HtmlTextWriter writer, bool oddRow, bool isLowest)
        {
            // Set the row style.
            if (oddRow && isLowest)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, StringResource.LowRowCssClass + " " +
                                                                   StringResource.OddRowCssClass);
            }
            else if (isLowest)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, StringResource.LowRowCssClass);
            }
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            // Low row - heading.
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.Write(isLowest ? StringResource.LowRowHeading : StringResource.NextLowestRowHeading);
            writer.RenderEndTag();

            // Low row - value.            
            writer.AddAttribute(HtmlTextWriterAttribute.Class, StringResource.ValueCellCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            if (!ShowNotAnalyzed)
            {
                writer.Write(string.Format(ValueFormat, _low)); 
            }
            else
            {
                writer.Write(StringResource.NotAnalyzedText);
            }
            writer.RenderEndTag();

            // Low row - percent of average.            
            writer.AddAttribute(HtmlTextWriterAttribute.Class, StringResource.PercentCellCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            if (!ShowNotAnalyzed)
            {
                writer.Write((100 * _low / _avg) + "%"); 
            }
            else
            {
                writer.Write(StringResource.NotAnalyzedText + "%");                
            }
            writer.RenderEndTag();

            writer.RenderEndTag(); // Tr
        }

        /// <summary>
        /// Render the table row for the average value from the market listings.
        /// </summary>
        /// <param name="writer">Writer for html content.</param>
        /// <param name="oddRow">Should this row get odd row styling.</param>
        private void RenderAverageTableRow(HtmlTextWriter writer, bool oddRow)
        {
            // Set the row style.
            if (oddRow)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, StringResource.AverageRowCssClass + " " +
                                                                   StringResource.OddRowCssClass);
            }
            else
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, StringResource.AverageRowCssClass);
            }            
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            // Avg row - heading.
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.Write(StringResource.AverageRowHeading);
            writer.RenderEndTag();

            // Avg row - value.            
            writer.AddAttribute(HtmlTextWriterAttribute.Class, StringResource.ValueCellCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            if (!ShowNotAnalyzed)
            {
                writer.Write(string.Format(ValueFormat, _avg)); 
            }
            else
            {
                writer.Write(StringResource.NotAnalyzedText);
            }
            writer.RenderEndTag();

            // Avg row - percent of average.            
            writer.AddAttribute(HtmlTextWriterAttribute.Class, StringResource.PercentCellCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            if (!ShowNotAnalyzed)
            {
                writer.Write("100%"); 
            }
            else
            {
                writer.Write(StringResource.NotAnalyzedText + "%");
            }
            writer.RenderEndTag();

            writer.RenderEndTag(); // Tr
        }

        /// <summary>
        /// Render the table row for the 'you' value from the market listings.
        /// </summary>
        /// <param name="writer">Writer for html content.</param>
        /// <param name="oddRow">Should this row get odd row styling.</param>
        /// <param name="isHighest">Is the first row of the summary</param>
        /// <param name="isLowest">Is the last row of the summary</param>
        private void RenderYouTableRow(HtmlTextWriter writer, bool oddRow, bool isHighest, bool isLowest)
        {
            // Set the row style.
            string cssClass = StringResource.YouRowCssClass;
            if (oddRow)
            {
                cssClass += " " + StringResource.OddRowCssClass;
            }

            if (isLowest)
            {
                cssClass += " " + StringResource.LowRowCssClass;
            }
            else if (isHighest)
            {
                cssClass += " " + StringResource.HighRowCssClass;
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            // You row - heading.
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.Write(StringResource.YouRowHeading);
            writer.RenderEndTag();

            // You row - value.            
            string className = StringResource.ValueCellCssClass;
            if (!string.IsNullOrEmpty(_youValueClassName))
            {
                className += " " + _youValueClassName;
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Class, className);

            if (!string.IsNullOrEmpty(_youValueTitle))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Title, _youValueTitle);
            }

            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            if (!ShowNotAnalyzed)
            {
                writer.Write(string.Format(ValueFormat, _you)); 
            }
            else
            {
                writer.Write(StringResource.NotAnalyzedText);
            }
            writer.RenderEndTag();            

            // You row - percent of average.            
            writer.AddAttribute(HtmlTextWriterAttribute.Class, StringResource.PercentCellCssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            if (_you == 0 || _avg == 0 || ShowNotAnalyzed)
            {
                writer.Write(StringResource.NotAnalyzedText + "%");
            }
            else
            {
                writer.Write((100 * _you / _avg) + "%");
            }
            writer.RenderEndTag();

            writer.RenderEndTag(); // Tr
        }

        #endregion

        #region DataBinding

        /// <summary>
        /// Bind data from the associated data source. Parses a DataTable of market listings.
        /// </summary>
        /// <param name="data">Data that is to be bound to.</param>
        protected override void PerformDataBinding(IEnumerable data)
        {            
            if (data != null)
            {
                DataView dataView = data as DataView;

                if (dataView != null)
                {
                    _high = int.MinValue;
                    _low = int.MaxValue;
                    _you = 0;
                    _comparableListings = 0;
                    _totalListings = dataView.Count;
                    List<int> rows = new List<int>();
                    int total = 0;
                    int avg = 0;
                    int lowerBound = int.MinValue;
                    int upperBound = int.MaxValue;

                    foreach (DataRowView row in dataView)
                    {
                        int current;

                        if (_columnName == "Market Pricing")
                        {
                            Int32.TryParse(row["ListPrice"].ToString(), out current);
                        }
                        else
                        {
                            current = (int)row["VehicleMileage"];
                        }                        

                        if (ShowReference)
                        {
                            bool isYou = (bool)row["IsAnalyzedVehicle"];

                            if (isYou)
                            {
                                _you = current;
                                /// Don't count you in the total listings, PM
                                _totalListings -= 1;
                                continue;
                            }
                        }

                        if ((!_useOwnerPreference && current == 0) ||
                            (_useOwnerPreference && _ownerPreferences.ExcludeNoPriceFromCalc && current == 0))
                        {
                            continue;
                        }

                        rows.Add(current);
                        total += current;
                    }

                    if (rows.Count > 0)
                    {
                        avg = total/rows.Count;                        
                    }

                    if (_useOwnerPreference)
                    {
                        lowerBound =
                            Int32Helper.ToInt32(Math.Round(avg*_ownerPreferences.ExcludeLowPriceOutliersMultiplier,
                                                           MidpointRounding.AwayFromZero));
                        upperBound =
                            Int32Helper.ToInt32(Math.Round(avg * _ownerPreferences.ExcludeHighPriceOutliersMultiplier,
                                                           MidpointRounding.AwayFromZero));
                    }

                    total = 0;

                    foreach (int current in rows)
                    {
                        if (current < lowerBound || current > upperBound)
                        {
                            continue;
                        }

                        // High price?
                        if (current > _high)
                        {
                            _high = current;
                        }

                        // Low price?
                        if (current < _low)
                        {
                            _low = current;
                        }

                        // Totals.
                        total += current;

                        _comparableListings++;
                    }                    

                    if (ShowReference)
                    {
                        MarketSummaryTableDataBoundEventArgs e = new MarketSummaryTableDataBoundEventArgs(_you, string.Empty, string.Empty);
                        OnDataBound(e);
                        _you = e.You;
                        _youValueTitle = e.Title;
                        _youValueClassName = e.ClassName;
                    }

                    // Determine the average.
                    if (_comparableListings != 0)
                    {
                        _avg = total / _comparableListings; 
                    }
                    else
                    {
                        _avg = 1;
                    }
                }
            }
        }        

        #endregion

        #region ViewState

        protected override void LoadViewState(object savedState)
        {
            if (savedState != null)
            {
                Pair state = savedState as Pair;
                if (state != null)
                {
                    object[] propertyState = (object[]) state.Second;

                    int idx = 0;

                    _columnName         = (string) propertyState[idx++];
                    _valueFormat        = (string) propertyState[idx++];
                    _high               = (int) propertyState[idx++];
                    _low                = (int) propertyState[idx++];
                    _avg                = (int) propertyState[idx++];
                    _you                = (int) propertyState[idx++];
                    _comparableListings = (int) propertyState[idx++];
                    _totalListings      = (int) propertyState[idx++];
                    _showReference      = (bool)propertyState[idx];
                }
                else
                {
                    base.LoadViewState(savedState);   
                }                
            }
        }

        protected override object SaveViewState()
        {           
            object[] propertyState = new object[9];

            int idx = 0;

            propertyState[idx++] = _columnName;
            propertyState[idx++] = _valueFormat;
            propertyState[idx++] = _high;
            propertyState[idx++] = _low;
            propertyState[idx++] = _avg;
            propertyState[idx++] = _you;
            propertyState[idx++] = _comparableListings;
            propertyState[idx++] = _totalListings;
            propertyState[idx] = _showReference;

            return new Pair(base.SaveViewState(), propertyState);
        }

        #endregion

        #region InnerClasses

        /// <summary>
        /// Helper for putting hardcoded values into a single place.
        /// </summary>
        private static class StringResource
        {
            #region Css Classes
		    
            public const string CssClass               = "market_summary_table";
            public const string TableTitleCellCssClass = "title";
            public const string OddRowCssClass         = "odd";
            public const string HighRowCssClass        = "high";
            public const string LowRowCssClass         = "low";
            public const string AverageRowCssClass     = "avg";
            public const string YouRowCssClass         = "you";
            public const string ValueCellCssClass      = "value";
            public const string PercentCellCssClass    = "percent"; 

	        #endregion

            #region Row Headings

            public const string HighRowHeading        = "High";
            public const string NextHighestRowHeading = "Next Highest";
            public const string LowRowHeading         = "Low";
            public const string NextLowestRowHeading  = "Next Lowest";
            public const string AverageRowHeading     = "Average";
            public const string YouRowHeading         = "You";            

            #endregion

            #region Column Headings

            public const string PercentOfAvgHeading = "% of Avg";

            #endregion

            #region Static Text

            public const string NotAnalyzedText = "--";

            #endregion
        }

        #endregion
    }
}
