using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis;
using NUnit.Framework;

namespace PricingDomainModel_Test.MarketAnalysis
{
    [TestFixture]
    public class MathUtilitiesUnitTests
    {
        private MathUtilities _math;
        private double _3OutlierThreshold;
        private double _4OutlierThreshold;

        [TestFixtureSetUp]
        public void Setup()
        {
            _math = new MathUtilities();
            _3OutlierThreshold = 1d/3d;
            _4OutlierThreshold = 0.5;
        }

        [Test]
        public void IsLargestNumberAnOutlierOneNumber()
        {            
            IList<double> numbers = new List<double>();
            numbers.Add(1);
           
            Assert.IsFalse( _math.IsLargestNumberAnOutlier(numbers, 1) );
        }

        [Test]
        public void IsLargestNumberAnOutlierTwoNumbers()
        {            
            IList<double> numbers = new List<double>();
            numbers.Add(1);
            numbers.Add(2);
            
            Assert.IsFalse( _math.IsLargestNumberAnOutlier(numbers, 0.5) );
        }

       
        #region 3 Point Outliers

        [Test]
        public void IsMaxOutlier_3_1_3()
        {
            IList<double> numbers = new List<double>();
            numbers.Add(1);
            numbers.Add(2);
            numbers.Add(3);

            Assert.IsFalse( _math.IsLargestNumberAnOutlier(numbers, _3OutlierThreshold) );
        }


        [Test]
        public void IsMaxOutlier_3_1_2_4()
        {
            IList<double> numbers = new List<double>();
            numbers.Add(1);
            numbers.Add(2);
            numbers.Add(4);

            Assert.IsFalse(_math.IsLargestNumberAnOutlier(numbers, _3OutlierThreshold));
        }
        
        [Test]
        public void IsMaxOutlier_3_1_2_6()
        {
            IList<double> numbers = new List<double>();
            numbers.Add(1);
            numbers.Add(2);
            numbers.Add(6);

            Assert.IsFalse(_math.IsLargestNumberAnOutlier(numbers, _3OutlierThreshold));
        }

        [Test]
        public void IsMaxOutlier_3_1_2_7()
        {
            IList<double> numbers = new List<double>();
            numbers.Add(1);
            numbers.Add(2);
            numbers.Add(7);

            Assert.IsTrue(_math.IsLargestNumberAnOutlier(numbers, _3OutlierThreshold));
        }

        #endregion

        #region 4 Point tests

        [Test]
        public void IsMaxOutlier_4_1_4()
        {
            IList<double> numbers = new List<double>();
            numbers.Add(1);
            numbers.Add(2);
            numbers.Add(3);
            numbers.Add(4);

            Assert.IsFalse(_math.IsLargestNumberAnOutlier(numbers, _4OutlierThreshold));
        }

        [Test]
        public void IsMaxOutlier_4_1_6()
        {
            IList<double> numbers = new List<double>();
            numbers.Add(1);
            numbers.Add(2);
            numbers.Add(3);
            numbers.Add(6);

            Assert.IsFalse(_math.IsLargestNumberAnOutlier(numbers, _4OutlierThreshold));
        }

        [Test]
        public void IsMaxOutlier_4_1_7()
        {
            IList<double> numbers = new List<double>();
            numbers.Add(1);
            numbers.Add(2);
            numbers.Add(3);
            numbers.Add(7);

            Assert.IsTrue(_math.IsLargestNumberAnOutlier(numbers, _4OutlierThreshold));
        }

        #endregion
    }
}