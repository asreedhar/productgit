using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis;
using NUnit.Framework;

namespace PricingDomainModel_Test.MarketAnalysis
{
    [TestFixture]
    public class ScaleCalculatorUnitTests
    {
        [Test]
        [ExpectedException(typeof(ApplicationException), ExpectedMessage = "Pixels cannot be zero.")]
        public void ThrowsWhenPixelsAreZero()
        {
            new ScaleCalculator(0);
        }      

        [Test]
        public void CalculatePixelPostionsZeroPoints()
        {
            ScaleCalculator c = new ScaleCalculator(10);
            IList<int> positions = c.CalculatePixelPostions(new List<int>());
            Assert.IsTrue( positions.Count == 0 );            
        }

        [Test]
        [ExpectedException(typeof(ApplicationException), ExpectedMessage = "At least two prices are needed to perform gauge scale calculations.")]
        public void CalculatePixelThrowsWithOnePrice()
        {
            ScaleCalculator c = new ScaleCalculator(10);
            IList<int> prices = new List<int>();
            prices.Add(1);
            c.CalculatePixelPostions(prices);            
        }
       

        [Test]
        [ExpectedException(typeof(ApplicationException), ExpectedMessage = "The numbers must be in non-descending order.")]
        public void CalculatePixelPositionThrowsWhenPricesAreDescending()
        {
            ScaleCalculator c = new ScaleCalculator(10);
            IList<int> prices = new List<int>();
            prices.Add(5);
            prices.Add(4);

            c.CalculatePixelPostions(prices);
        }

        [Test]
        public void CalculatePixelPostionsTwoPoints()
        {
            ScaleCalculator c = new ScaleCalculator(10);
            IList<int> prices = new List<int>();
            prices.Add(10);
            prices.Add(20);
            IList<int> positions = c.CalculatePixelPostions(prices);

            Assert.IsTrue(positions.Count == 2);
            Assert.AreEqual(0, positions[0]);
            Assert.AreEqual(10, positions[1]);
        }

        [Test]
        [ExpectedException(typeof(ApplicationException), ExpectedMessage = "It is impossible to plot prices on the gauge when they are all equal.")]
        public void CalculatePixelPostionsThrowsWhenAllNumbersAreSame()
        {
            ScaleCalculator c = new ScaleCalculator(10);
            IList<int> prices = new List<int>();
            prices.Add(10);
            prices.Add(10);
            c.CalculatePixelPostions(prices);
        }

        [Test]
        public void CalculatePixelPositionThreeClosePoints()
        {
            ScaleCalculator c = new ScaleCalculator(10);
            IList<int> prices = new List<int>();
            prices.Add(1);
            prices.Add(2);
            prices.Add(3);
            
            IList<int> positions = c.CalculatePixelPostions(prices);
            Assert.AreEqual(0, positions[0]);
            Assert.AreEqual(5, positions[1]);
            Assert.AreEqual(10, positions[2]);

            Assert.IsFalse(c.BreakPositionInPixels.HasValue);
        }

        [Test]
        public void CalculatePixelPositionThreePointsWithOutlier()
        {
            ScaleCalculator c = new ScaleCalculator(100);
            IList<int> prices = new List<int>();
            prices.Add(1);
            prices.Add(2);
            prices.Add(10);

            IList<int> positions = c.CalculatePixelPostions(prices);
            Assert.AreEqual(0, positions[0]);
            Assert.AreEqual(33, positions[1]);
            Assert.AreEqual(100, positions[2]);

            Assert.IsTrue( c.BreakPositionInPixels.HasValue );
            Assert.AreEqual( 66, c.BreakPositionInPixels );
        }

        [Test]
        public void CalculatePixelPositionFourClosePoints_1_4()
        {
            ScaleCalculator c = new ScaleCalculator(100);
            IList<int> prices = new List<int>();
            prices.Add(1);
            prices.Add(2);
            prices.Add(3);
            prices.Add(4);

            IList<int> positions = c.CalculatePixelPostions(prices);
            Assert.AreEqual(0, positions[0]);
            Assert.AreEqual(33, positions[1]);  
            Assert.AreEqual(66, positions[2]);
            Assert.AreEqual(100, positions[3]);

            Assert.IsFalse(c.BreakPositionInPixels.HasValue);
        }

        [Test]
        public void CalculatePixelPositionFourClosePoints_1_6()
        {
            ScaleCalculator c = new ScaleCalculator(100);
            IList<int> prices = new List<int>();
            prices.Add(1);
            prices.Add(2);
            prices.Add(3);
            prices.Add(6);

            IList<int> positions = c.CalculatePixelPostions(prices);
            Assert.AreEqual(0, positions[0]);
            Assert.AreEqual(20, positions[1]); 
            Assert.AreEqual(40, positions[2]);
            Assert.AreEqual(100, positions[3]);

            Assert.IsFalse(c.BreakPositionInPixels.HasValue);            
        }

        [Test]
        public void CalculatePixelPositionFourPoints_1_7_WithOutlier()
        {
            ScaleCalculator c = new ScaleCalculator(100);
            IList<int> prices = new List<int>();
            prices.Add(1);
            prices.Add(2);
            prices.Add(3);
            prices.Add(7);

            IList<int> positions = c.CalculatePixelPostions(prices);
            Assert.AreEqual(0, positions[0]);
            Assert.AreEqual(25, positions[1]); 
            Assert.AreEqual(50, positions[2]);
            Assert.AreEqual(100, positions[3]);

            foreach(int position in positions)
            {
                Console.WriteLine(position);
            }

            Assert.IsTrue(c.BreakPositionInPixels.HasValue);
            Assert.AreEqual(75, c.BreakPositionInPixels);
        }

        [Test]
        public void CalculatePixelPositionUsing4DistinctPricesInIncreasingOrder()
        {
            ScaleCalculator c = new ScaleCalculator(612);
            IList<int> prices = new List<int>();
            prices.Add(20000);
            prices.Add(32634);
            prices.Add(33800);
            prices.Add(36986);

            IList<int> positions = c.CalculatePixelPostions(prices);

            for (int i = 0;i<positions.Count;i++)
            {
                if (i==0)
                {
                    Assert.AreEqual(0, positions[i]);
                }
                else
                {
                    Assert.Greater(positions[i], positions[i-1]);
                }
            }

        }

        [Test]
        public void CalculateScaledPixelPoisitions()
        {
            ScaleCalculator c = new ScaleCalculator(100);
            IList<double> prices = new List<double>();
            prices.Add(1);
            prices.Add(2);
            prices.Add(3);
            prices.Add(4);

            IList<double> positions = c.CalculateScaledPixelPostions(prices, 100, false);

            Assert.AreEqual(0, positions[0]);
            Assert.AreEqual(33.333333333333329d, positions[1]);
            Assert.AreEqual(66.666666666666657d, positions[2]);
            Assert.AreEqual(100, positions[3]);
        }

        [Test]
        [ExpectedException(typeof(ApplicationException), ExpectedMessage = "All prices must be non-negative.")]
        public void CalculatePixelPostionsThrowsWhenPassedNegativeNumbers()
        {
            ScaleCalculator c = new ScaleCalculator(100);
            IList<int> prices = new List<int>();
            prices.Add(-1);
            prices.Add(1);

            c.CalculatePixelPostions(prices);
        }

        [Test]
        public void CalculatePixelPositionsDuplicateInnerNumbersNoOutlier()
        {
            ScaleCalculator c = new ScaleCalculator(100);
            IList<int> prices = new List<int>();
            prices.Add(1);
            prices.Add(2);
            prices.Add(2);
            prices.Add(3);

            IList<int> positions = c.CalculatePixelPostions(prices);
            Assert.AreEqual(0, positions[0]);
            Assert.AreEqual(50, positions[1]);
            Assert.AreEqual(50, positions[2]);
            Assert.AreEqual(100, positions[3]);            
        }

        [Test]
        public void CalculatePixelPositionsDuplicateLatterNumbers()
        {
            ScaleCalculator c = new ScaleCalculator(100);
            IList<int> prices = new List<int>();
            prices.Add(1);
            prices.Add(2);
            prices.Add(2);
            prices.Add(2);

            IList<int> positions = c.CalculatePixelPostions(prices);
            Assert.AreEqual(0, positions[0]);
            Assert.AreEqual(100, positions[1]);
            Assert.AreEqual(100, positions[2]);
            Assert.AreEqual(100, positions[3]);
        }
    }
}