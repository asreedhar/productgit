using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Types;
using NUnit.Framework;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Implementation;

namespace PricingDomainModel_Test.MarketAnalysis
{
    [TestFixture]
    public class MarketAnalysisUnitTests
    {
        private static void CheckSortOrderAccordingToPreference(IList<GaugePrice> prices, int offerPrice,
                                                                IMarketAnalysisDealerPreference preference)
        {
            if (preference.UseCustomComparisonPricePriority)
            {
                CheckSortOrderByRank(prices, offerPrice);
            }
            else
            {
                CheckSortOrderByOfferPriceThenDescendingPrices(prices, offerPrice);
            }
        }

        private static void CheckSortOrderByOfferPriceThenDescendingPrices(IList<GaugePrice> prices, int offerPrice)
        {
            for (int i = 0; i < prices.Count; i++)
            {
                GaugePrice gaugePrice = prices[i];

                if (i == 0)
                {
                    // the first element should always be the offer price
                    Assert.IsTrue(gaugePrice.PriceProviderId == MarketAnalysisPriceProviders.OfferPrice);
                    Assert.AreEqual(offerPrice, prices[i].Dollars);
                }
                else
                {
                    // only the first element should have this set
                    Assert.IsFalse(prices[i].PriceProviderId == MarketAnalysisPriceProviders.OfferPrice);

                    // Dollars should be descending after offer price.
                    if (i > 1) // we start this with the 3rd item (checking item 3 against 2)
                    {
                        // either the current price is less than the previous price (with the exception of item at index 1)
                        // or the dollars are equal.  If the dollars are equal, then the ranks must be ascending in order.    
                        Assert.IsTrue(prices[i].Dollars < prices[i - 1].Dollars ||
                                      ((prices[i].Dollars == prices[i - 1].Dollars) &&
                                       (prices[i].Rank > prices[i - 1].Rank)));
                    }
                }
            }
        }

        private static void CheckSortOrderByRank(IList<GaugePrice> prices, int offerPrice)
        {
            for (int i = 0; i < prices.Count; i++)
            {
                GaugePrice gaugePrice = prices[i];

                if (i == 0)
                {
                    // the first element should always be the offer price
                    Assert.IsTrue(gaugePrice.PriceProviderId == MarketAnalysisPriceProviders.OfferPrice);
                    Assert.AreEqual(offerPrice, prices[i].Dollars);
                }
                else
                {
                    // only the first element should have this set
                    Assert.IsFalse(prices[i].PriceProviderId == MarketAnalysisPriceProviders.OfferPrice);
                    // ranks should be greater than their predecessor
                    Assert.IsTrue(prices[i].Rank > prices[i - 1].Rank);
                }
            }
        }


        private static void CheckGaugePriceListIntegrity(IList<GaugePrice> prices,
                                                         MarketAnalysisDealerPreferenceTO preference, int offerPrice)
        {
            CheckGaugePriceListIntegrity(prices, preference, offerPrice, false);
        }

        private static void CheckGaugePriceListIntegrity(IList<GaugePrice> prices,
                                                         MarketAnalysisDealerPreferenceTO preference, int offerPrice,
                                                         bool bypassSortCheck)
        {
            // Check for gaugePrice list integrity
            //   * no prices overlap
            //   * no price less than the last price (can be equal or greater)
            if (!bypassSortCheck)
                CheckSortOrderAccordingToPreference(prices, offerPrice, preference);

            // check each price against every other price (except itself) and check for overlapping prices.
            foreach (GaugePrice price1 in prices)
            {
                foreach (GaugePrice price2 in prices)
                {
                    if (!price1.Equals(price2))
                    {
                        if (price1.Dollars > price2.Dollars)
                        {
                            Console.WriteLine("price1.Dollars > price2.Dollars...");
                            Console.WriteLine("price1 (" + price1.Text + ", " + price1.Dollars +
                                              "): LeftEdge (" + price1.LeftEdge + ") should be > price2 (" + price2.Text +
                                              ", " +
                                              price2.Dollars + ") Right Edge + padding (" +
                                              (price2.RightEdge + PriceProcessor.GaugeItemPadding) + ")");
                            Assert.GreaterOrEqual(price1.LeftEdge, price2.RightEdge + PriceProcessor.GaugeItemPadding);
                        }
                        else if (price1.Dollars < price2.Dollars)
                        {
                            Console.WriteLine("price1.Dollars < price2.Dollars...");
                            Assert.GreaterOrEqual(price2.LeftEdge, price1.RightEdge + PriceProcessor.GaugeItemPadding);
                        }
                        else if (price1.Dollars == price2.Dollars)
                        {
                            // dollars match, lowest valued rank should be closest to the left.
                            if (price1.Rank < price2.Rank)
                            {
                                Console.WriteLine("price1.Rank < price2.Rank...");
                                Assert.GreaterOrEqual(price2.LeftEdge,
                                                      price1.RightEdge + PriceProcessor.GaugeItemPadding);
                            }
                            else
                            {
                                Console.WriteLine("price1.Rank >= price2.Rank...");
                                Assert.GreaterOrEqual(price1.LeftEdge,
                                                      price2.RightEdge + PriceProcessor.GaugeItemPadding);
                            }
                        }
                    }
                }
            }
        }

        private static MarketAnalysisDealerPreferenceTO CreateDefaultPreference()
        {
            var pref = new MarketAnalysisDealerPreferenceTO
                           {
                               PriceProviders = GetFullDefaultProviderList(),
                               UseCustomComparisonPricePriority = false,
                               UseCustomCurrentInternetPrice = false,
                               UseCustomOfferPrice = false,
                               Id = 0,
                               NumberOfPricesRequiredForPricingGauge = 3
                           };

            return pref;
        }

        private static List<PriceProvider> GetFullDefaultProviderList()
        {
            string dealerLogo = String.Empty;

            var offerProvider =
                new PriceProvider(0, MarketAnalysisPriceProviders.OfferPrice,
                                                                 "Offer Price", 1, dealerLogo);
            var currentInternetPriceProvider =
                new PriceProvider(0, MarketAnalysisPriceProviders.CurrentInternetPrice,
                                                                 "Current Internet Price", 2, dealerLogo);
            var originalListPriceProvider =
                new PriceProvider(0, MarketAnalysisPriceProviders.OriginalMSRP,
                                                                 "Original MSRP", 3, dealerLogo);
            var pingProvider =
                new PriceProvider(0,
                                                                 MarketAnalysisPriceProviders.
                                                                     PINGMarketAverageInternetPrice,
                                                                 "PING Market Average Internet Price", 4, dealerLogo);
            var jdPowerProvider =
                new PriceProvider(0,
                                                                 MarketAnalysisPriceProviders.
                                                                     JDPowerPINAverageInternetPrice,
                                                                 "JD Power PIN Average Internet Price", 5, dealerLogo);
            var edmundsProvider =
                new PriceProvider(0, MarketAnalysisPriceProviders.EdmundsTrueMarketValue,
                                                                 "Edmunds Price", 6, dealerLogo);
            var nadaProvider =
                new PriceProvider(0, MarketAnalysisPriceProviders.NADARetailValue,
                                                                 "NADA Retail Value", 7, dealerLogo);
            var kbbProvider =
                new PriceProvider(0,
                                                                 MarketAnalysisPriceProviders.KelleyBlueBookRetailValue,
                                                                 "Kelley Blue Book Retail Value", 8, dealerLogo);
            var bbProvider =
                new PriceProvider(0,
                                                                 MarketAnalysisPriceProviders.BlackBook,
                                                                 "Kelley Blue Book Retail Value", 9, dealerLogo);
            var galves =
                new PriceProvider(0,
                                                                 MarketAnalysisPriceProviders.Galves,
                                                                 "Galves Market Ready Value", 10, dealerLogo);

            var providers = new List<PriceProvider>
                                {
                                    offerProvider,
                                    currentInternetPriceProvider,
                                    originalListPriceProvider,
                                    pingProvider,
                                    jdPowerProvider,
                                    edmundsProvider,
                                    nadaProvider,
                                    kbbProvider,
                                    bbProvider,
                                    galves
                                };

            return providers;
        }

        private static void ChangePriceProviderRanks(IMarketAnalysisDealerPreference preferenceTO)
        {
            foreach (var provider in preferenceTO.PriceProviders)
            {
                // change every rank so it is all out of whack - rank should be ignored in this test.
                if (provider.PriceProviderId == MarketAnalysisPriceProviders.OfferPrice) provider.Rank = 8;
                if (provider.PriceProviderId == MarketAnalysisPriceProviders.EdmundsTrueMarketValue) provider.Rank = 7;
                if (provider.PriceProviderId == MarketAnalysisPriceProviders.KelleyBlueBookRetailValue)
                    provider.Rank = 6;
                if (provider.PriceProviderId == MarketAnalysisPriceProviders.OriginalMSRP) provider.Rank = 5;
                if (provider.PriceProviderId == MarketAnalysisPriceProviders.NADARetailValue) provider.Rank = 4;
                if (provider.PriceProviderId == MarketAnalysisPriceProviders.CurrentInternetPrice) provider.Rank = 3;
                if (provider.PriceProviderId == MarketAnalysisPriceProviders.JDPowerPINAverageInternetPrice)
                    provider.Rank = 2;
                if (provider.PriceProviderId == MarketAnalysisPriceProviders.PINGMarketAverageInternetPrice)
                    provider.Rank = 1;
            }
        }

        private static MarketAnalysisDealerPreferenceTO CreatePreference()
        {
            var dealerLogo = String.Empty;


            var offerProvider =
                new PriceProvider(0, MarketAnalysisPriceProviders.OfferPrice,
                                                                 "Offer Price", 0, dealerLogo);

            var currentInternetPriceProvider =
                new PriceProvider(0, MarketAnalysisPriceProviders.CurrentInternetPrice,
                                                                 "Current Internet Price", 1, dealerLogo);

            var edmundsProvider =
                new PriceProvider(0, MarketAnalysisPriceProviders.EdmundsTrueMarketValue,
                                                                 "Edmunds Price", 2, dealerLogo);

            var providers = new List<PriceProvider> {offerProvider, currentInternetPriceProvider, edmundsProvider};

            var pref = new MarketAnalysisDealerPreferenceTO
                           {
                               PriceProviders = providers,
                               UseCustomComparisonPricePriority = false,
                               UseCustomCurrentInternetPrice = false,
                               UseCustomOfferPrice = false,
                               Id = 0,
                               NumberOfPricesRequiredForPricingGauge = 1
                           };

            return pref;
        }

        private static MarketAnalysisVehiclePreferenceTO CreateVehiclePreference()
        {
            var vprefTO = new MarketAnalysisVehiclePreferenceTO(0, "oh", "vh", true, true);
            return vprefTO;
        }

        private static MarketAnalysisPricingInformationTO CreatePricingInformation()
        {
            var to =
                new MarketAnalysisPricingInformationTO("oh", "vh", "sh", 10, true, 10, true, 10, true, 10, true, true, 10, true, true, 10,
                                                       true, 10, true, 10, true, true, 10, true, 123, true, true, "A111", 88, 1973);

            return to;
        }


        private static MarketAnalysisPricingInformationTO CreatePricingInformation(int listPrice, int originalListPrice,
                                                                            int marketPrice, int kbbPrice, int nadaPrice,
                                                                            int edmundsPrice, int jdPowerPrice, int blackBookPrice, int galvesPrice)
        {
            /*
        public MarketAnalysisPricingInformationTO(int? listPrice, bool? hasOriginalListPrice, int? originalMSRP, bool? hasMarketPrice, int? marketPrice, bool? hasKbbPrice,
                                                  int? kbbPrice, bool? kbbPriceIsAccurate, bool? hasNadaPrice,
                                                  int? nadaPrice, bool? nadaPriceIsAccurate,
                                                  bool? hasEdmundsPrice, int? edmundsPrice, bool? hasJdPowerPrice,
                                                  int? jdPowerPrice)
             */

            var hasOriginalListPrice = originalListPrice > 0;
            var hasMarketPrice = marketPrice > 0;
            var hasKbbPrice = kbbPrice > 0;
            var hasNadaPrice = nadaPrice > 0;
            var hasEdmundsPrice = edmundsPrice > 0;
            var hasJdPowerPrice = jdPowerPrice > 0;
            var hasBlackBookPrice = blackBookPrice > 0;
            var hasGalvesPrice = galvesPrice > 0;

            var to =
                new MarketAnalysisPricingInformationTO("oh", "vh", "sh", listPrice, hasOriginalListPrice, originalListPrice,
                                                       hasMarketPrice, marketPrice, hasKbbPrice, kbbPrice, true,
                                                       hasNadaPrice, nadaPrice, true, hasEdmundsPrice, edmundsPrice,
                                                       hasJdPowerPrice, jdPowerPrice, hasBlackBookPrice, blackBookPrice, true, hasGalvesPrice, galvesPrice, true, 123, true, true, "A111", 88, 1973);

            return to;
        }



        private static List<GaugePrice> CreateGaugePrices(int offerPrice, int currentInternetPrice, int originalMSRP,
                                                   int pingPrice, int jdPowerPrice, int edmundsPrice, int nadaPrice,
                                                   int kbbPrice)
        {
            var prices = new List<GaugePrice>
                             {
                                 new GaugePrice(MarketAnalysisPriceProviders.OfferPrice, "Offer Price", 1, offerPrice, 0),
                                 new GaugePrice(MarketAnalysisPriceProviders.CurrentInternetPrice,
                                                "Current Internet Price", 2, currentInternetPrice,
                                                currentInternetPrice - offerPrice),
                                 new GaugePrice(MarketAnalysisPriceProviders.OriginalMSRP, "Original MSRP", 3,
                                                originalMSRP, originalMSRP - offerPrice),
                                 new GaugePrice(MarketAnalysisPriceProviders.PINGMarketAverageInternetPrice,
                                                "PING Market Average Internet Price", 4, pingPrice,
                                                pingPrice - offerPrice),
                                 new GaugePrice(MarketAnalysisPriceProviders.JDPowerPINAverageInternetPrice,
                                                "JD Power PIN Average Internet Price", 5, jdPowerPrice,
                                                jdPowerPrice - offerPrice),
                                 new GaugePrice(MarketAnalysisPriceProviders.EdmundsTrueMarketValue,
                                                "Edmunds True Market Value", 6, edmundsPrice, edmundsPrice - offerPrice),
                                 new GaugePrice(MarketAnalysisPriceProviders.NADARetailValue, "NADA Retail Value", 7,
                                                nadaPrice, nadaPrice - offerPrice),
                                 new GaugePrice(MarketAnalysisPriceProviders.KelleyBlueBookRetailValue,
                                                "Kelley Blue Book Retail Value", 8, kbbPrice, kbbPrice - offerPrice)
                             };

            return prices;
        }


        private static List<GaugePrice> CreateWidestTextGaugePrices()
        {
            var prices = new List<GaugePrice>
                             {
                                 new GaugePrice(MarketAnalysisPriceProviders.OfferPrice, "123456789012345678", 1, 10000,
                                                0),
                                 new GaugePrice(MarketAnalysisPriceProviders.CurrentInternetPrice,
                                                "123456789012345678901234567890", 2, 11000, 1000),
                                 new GaugePrice(MarketAnalysisPriceProviders.PINGMarketAverageInternetPrice,
                                                "PING Market Average Internet Price", 3, 12000, 2000),
                                 new GaugePrice(MarketAnalysisPriceProviders.JDPowerPINAverageInternetPrice,
                                                "JD Power PIN Average Internet Price", 4, 13000, 3000)
                             };
            return prices;
        }


        [Test]
        public void CanCreateGaugeProcessor()
        {
            var processor = new PriceProcessor(null, null, null, 0);
            Assert.IsNotNull(processor);
        }

        [Test]
        public void CanProcessPrices()
        {
            var processor = new PriceProcessor(null, null, null, 0);
            processor.ProcessPrices();
            Assert.IsNotNull(processor.GaugePrices);
            Assert.IsEmpty(processor.GaugePrices);
        }

        [Test]
        public void LoadGaugePricesLoadsASingleOfferPriceInTheFirstPositon()
        {
            var dealerPreferenceTO = CreatePreference();
            var pricingInformationTO = CreatePricingInformation();
            var vehiclePreferenceTO = CreateVehiclePreference();

            const int OFFER_PRICE = 1;
            var processor = new PriceProcessor(dealerPreferenceTO, vehiclePreferenceTO, pricingInformationTO, OFFER_PRICE);
            Assert.AreEqual(0, processor.GaugePrices.Count);
            Assert.AreEqual(3, dealerPreferenceTO.PriceProviders.Count);

            processor.ProcessPrices();


            Assert.AreEqual(dealerPreferenceTO.PriceProviders.Count, processor.GaugePrices.Count);

            for (int i = 0; i < processor.GaugePrices.Count; i++)
            {
                if (i == 0)
                {
                    // the first element should always be the offer price
                    Assert.IsTrue(processor.GaugePrices[i].PriceProviderId == MarketAnalysisPriceProviders.OfferPrice);
                    Assert.AreEqual(OFFER_PRICE, processor.GaugePrices[i].Dollars);
                }
                else
                {
                    // only the first element should have this set
                    Assert.IsFalse(processor.GaugePrices[i].PriceProviderId == MarketAnalysisPriceProviders.OfferPrice);
                }
            }

            // check gauge price integrity
            //if (processor.ShowGauge) CheckGaugePriceListIntegrity(processor.GaugePrices, pref, offerPrice);
        }



        [Test]
        public void LoadGaugePricesLoadsCorrectNumberOfGaugePrices()
        {
            var dealerPreferenceTO = CreatePreference();
            var pricingInformationTO = CreatePricingInformation();
            var vehiclePreferenceTO = CreateVehiclePreference();

            var processor = new PriceProcessor(dealerPreferenceTO, vehiclePreferenceTO, pricingInformationTO, 0);
            Assert.AreEqual(0, processor.GaugePrices.Count);
            Assert.AreEqual(3, dealerPreferenceTO.PriceProviders.Count);

            processor.ProcessPrices();

            Assert.AreEqual(dealerPreferenceTO.PriceProviders.Count, processor.GaugePrices.Count);

            foreach (var price in processor.GaugePrices)
            {
                Console.WriteLine(price);
            }

            // check gauge price integrity
            //if (processor.ShowGauge) CheckGaugePriceListIntegrity(processor.GaugePrices, pref, 0);
        }

        [Test]
        public void LoadGaugePricesSetsGaugePrices()
        {
            var dealerPreferenceTO = CreatePreference();
            var pricingInformationTO = CreatePricingInformation();
            var vehiclePreferenceTO = CreateVehiclePreference();
            var processor = new PriceProcessor(dealerPreferenceTO, vehiclePreferenceTO, pricingInformationTO, 0);
            processor.ProcessPrices();

            Assert.IsNotEmpty(processor.GaugePrices);

            foreach (var price in processor.GaugePrices)
            {
                Console.WriteLine(price.Text + ":" + price.Dollars);
            }

            // check gauge price integrity
            //if (processor.ShowGauge) 
            //  CheckGaugePriceListIntegrity(processor.GaugePrices, pref, 0);
        }

        [Test]
        public void ProcessAllIdenticalComparisonPricesWithLowerOfferPrice()
        {
            const int OFFER_PRICE = 10500;
            const int COMP_PRICE = 11000;
            var dealerPreferenceTO = CreateDefaultPreference();
            var pricingInformationTO = CreatePricingInformation( COMP_PRICE, COMP_PRICE,
                                                                 COMP_PRICE,
                                                                 COMP_PRICE, COMP_PRICE,
                                                                 COMP_PRICE,
                                                                 COMP_PRICE, COMP_PRICE, COMP_PRICE);
            var vehiclePreferenceTO = CreateVehiclePreference();
            var processor = new PriceProcessor(dealerPreferenceTO, vehiclePreferenceTO, pricingInformationTO, OFFER_PRICE);
            processor.ProcessPrices();

            // we should have const PriceProcessor.MaxNumberOfPrices prices in our list
            Assert.AreEqual(PriceProcessor.MaxNumberOfPrices, processor.GaugePrices.Count);
            // since we have the max number of prices, we should be set to show the gauge
            Assert.IsTrue(processor.ShowGauge);

            // check gauge price integrity
            if (processor.ShowGauge) CheckGaugePriceListIntegrity(processor.GaugePrices, dealerPreferenceTO, OFFER_PRICE);
        }

        [Test]
        public void CheckOverallWidthDoesNotThrowWhenPassedPricesWithMaxAllowedWidth()
        {
            /*
             * Currently, the max allowed width, based on (a) the current descriptions for the price providers,
             * (b) the max number of prices to show on a gauge, (c) the padding used between the price elements on the gauge,
             * (d) using a ratio of 10/3 for calculating the width based on the text length, and (d) the max lengths of 
             * the 2 override display names (offer price and current internet price) is 420 px.
             * The max area we have in the gauge is 530.  So we have about 110 px to spare.  */

            List<GaugePrice> prices = CreateWidestTextGaugePrices();
            PriceProcessor.CheckOverallWidth(prices);

            // if you made it here, the test passes.  I don't want it to throw an exception.
        }

        [Test]
        public void ProcessAllIdenticalPricesResultsInOnlyOfferPrice()
        {
            const int PRICE = 10500;
            var dealerPreference = CreateDefaultPreference();
            var pricingInformation = CreatePricingInformation(PRICE, PRICE, PRICE,PRICE, PRICE, PRICE,PRICE, PRICE, PRICE);
            var vehiclePreferenceTO = CreateVehiclePreference();
            var processor = new PriceProcessor(dealerPreference, vehiclePreferenceTO, pricingInformation, PRICE);
            
            processor.ProcessPrices();

            // we should have only one price in our list
            Assert.AreEqual(1, processor.GaugePrices.Count);
            // the price should be the offer price
            Assert.IsTrue(processor.GaugePrices[0].PriceProviderId == MarketAnalysisPriceProviders.OfferPrice);
            // since we have a single price, we should not be set to show the gauge
            Assert.IsFalse(processor.ShowGauge);

            // check gauge price integrity
            if (processor.ShowGauge) CheckGaugePriceListIntegrity(processor.GaugePrices, dealerPreference, PRICE);
        }

        [Test]
        public void ProcessRandomComparisonPricesWithLowerOfferPrice()
        {
            var random = new Random();
            var offerPrice = random.Next(200000);

            var comp1 = random.Next(offerPrice + 1, 300000);
            var comp2 = random.Next(offerPrice + 1, 300000);
            var comp3 = random.Next(offerPrice + 1, 300000);


            var dealerPreference = CreateDefaultPreference();
            dealerPreference.NumberOfPricesRequiredForPricingGauge = 3;
            var pricingInformation = CreatePricingInformation(comp1, comp2, comp3, 0, 0, 0, 0, 0, 0);
            var vehiclePreferenceTO = CreateVehiclePreference();
            var processor = new PriceProcessor(dealerPreference, vehiclePreferenceTO, pricingInformation, offerPrice);
            processor.ProcessPrices();

            Assert.AreEqual(4, processor.GaugePrices.Count); // 3 comps + 1 offer price
            // since we have the max number of prices, we should be set to show the gauge
            Assert.IsTrue(processor.ShowGauge);

            // check gauge price integrity
            if (processor.ShowGauge) CheckGaugePriceListIntegrity(processor.GaugePrices, dealerPreference, offerPrice);
        }

        [Test]
        public void SortByDollarsAscendingWorks()
        {
            var preferenceTO = CreateDefaultPreference();
            var pricingInformationTO = new MarketAnalysisPricingInformationTO("oh", "vh", "sh", 21000, true, 22000, true, 23000, true, 24000, true, true, 25000, true, true, 26000, true, 27000, true, 28000, true, true, 29000, true, 123, true, true, "A111", 88, 1973);
            var vehiclePreferenceTO = CreateVehiclePreference();
            const int OFFER_PRICE = 20000;

            var processor = new PriceProcessor(preferenceTO, vehiclePreferenceTO, pricingInformationTO, OFFER_PRICE);

            processor.ProcessPrices();

            processor.GaugePrices.Sort(GaugePrice.ComparePricesByAscendingDollarValue);

            for (var i = 0; i < processor.GaugePrices.Count; i++)
            {
                var gaugePrice = processor.GaugePrices[i];

                if (i > 0)
                {
                    Assert.Greater(gaugePrice.Dollars, processor.GaugePrices[i - 1].Dollars);
                }
            }

            // check gauge price integrity - ignoring the sort order (we changed it after the processor sorted it)
            //if (processor.ShowGauge) CheckGaugePriceListIntegrity(processor.GaugePrices, preferenceTO, offerPrice, true);
        }

        [Test]
        public void SortByOfferThanDescendingDollarsWorks()
        {
            //MarketAnalysisDealerPreferenceTO preferencefTO = CreateDefaultPreference();
            //MarketAnalysisPricingInformationTO pricingTO = 
            //    new MarketAnalysisPricingInformationTO(21000, true, 22000, true, 23000, true, 24000, true, true, 25000, true, true, 26000, true, 27000);

            const int OFFER_PRICE = 2000;

            //PriceProcessor processor = new PriceProcessor(preferencefTO, pricingTO, offerPrice);

            //processor.ProcessPrices();

            var prices = CreateGaugePrices(OFFER_PRICE, 2100, 2200, 2300, 2400, 2500, 2600, 2700);

            prices.Sort(GaugePrice.ComparePricesByOfferThenDescendingDollarValue);

            for (var i = 0; i < prices.Count; i++)
            {
                var gaugePrice = prices[i];

                if (i == 0)
                {
                    Assert.AreEqual(true, gaugePrice.PriceProviderId == MarketAnalysisPriceProviders.OfferPrice);
                    Assert.AreEqual(OFFER_PRICE, gaugePrice.Dollars);
                }
                else
                {
                    var lastGaugePrice = prices[i - 1];
                    if (i == 1)
                    {
                        // the 2nd price in the list (first comparison price) should be greater than the offer price (as should all comparison prices)
                        Assert.Greater(gaugePrice.Dollars, lastGaugePrice.Dollars);
                    }
                    else
                    {
                        Assert.LessOrEqual(gaugePrice.Dollars, lastGaugePrice.Dollars);
                    }
                }
            }
        }

        [Test]
        public void SortByPriceProviderRankWorks()
        {
            var preferencefTO = CreateDefaultPreference();
            var pricingInformationTO = new MarketAnalysisPricingInformationTO("oh", "vh", "sh", 21000, true, 22000, true, 23000, true, 24000, true, true, 25000, true, true, 26000, true, 27000, true, 28000, true, true, 29000, true, 123, true, true, "A111", 88, 1973);
            var vehiclePreferenceTO = CreateVehiclePreference();

            const int OFFER_PRICE = 20000;

            var processor = new PriceProcessor(preferencefTO, vehiclePreferenceTO, pricingInformationTO, OFFER_PRICE);

            processor.ProcessPrices();

            processor.GaugePrices.Sort(GaugePrice.ComparePricesByRank);

            for (var i = 0; i < processor.GaugePrices.Count; i++)
            {
                var gaugePrice = processor.GaugePrices[i];

                if (i > 0)
                {
                    Assert.Greater(gaugePrice.Rank, processor.GaugePrices[i - 1].Rank);
                }
            }
        }

        [Test]
        public void SortingAccordingToPreferenceWorks_Custom()
        {
            var preferencefTO = CreateDefaultPreference();
            var pricingInformationTO = CreatePricingInformation();
            var vehiclePreferenceTO = CreateVehiclePreference();

            // should sort by offer price, then custom price ranking
            preferencefTO.UseCustomComparisonPricePriority = true;

            ChangePriceProviderRanks(preferencefTO);

            const int OFFER_PRICE = 5;

            var processor = new PriceProcessor(preferencefTO, vehiclePreferenceTO, pricingInformationTO, OFFER_PRICE);
            processor.SortPricesAccordingToPreference();

            for (var i = 0; i < processor.GaugePrices.Count; i++)
            {
                var gaugePrice = processor.GaugePrices[i];

                if (i == 0)
                {
                    // the first element should always be the offer price
                    Assert.IsTrue(gaugePrice.PriceProviderId == MarketAnalysisPriceProviders.OfferPrice);
                    Assert.AreEqual(OFFER_PRICE, gaugePrice.Dollars);
                    Assert.AreEqual(8, gaugePrice.Rank);
                }
                else
                {
                    // only the first element should have this set
                    Assert.IsFalse(gaugePrice.PriceProviderId == MarketAnalysisPriceProviders.OfferPrice);
                }
            }

            // check gauge price integrity
            //if (processor.ShowGauge) CheckGaugePriceListIntegrity(processor.GaugePrices, preferencefTO, offerPrice);
        }

        [Test]
        public void SortingAccordingToPreferenceWorks_Default()
        {
            var preferenceTO = CreateDefaultPreference();
            var pricingInformationTO = CreatePricingInformation();
            var vehiclePreferenceTO = CreateVehiclePreference();

            // should sort by offer price first, then descending
            preferenceTO.UseCustomComparisonPricePriority = false;

            const int OFFER_PRICE = 5;

            var processor = new PriceProcessor(preferenceTO, vehiclePreferenceTO, pricingInformationTO, OFFER_PRICE);
            processor.ProcessPrices();

            // check gauge price integrity
            //if (processor.ShowGauge) CheckGaugePriceListIntegrity(processor.GaugePrices, preferenceTO, offerPrice);

            processor.SortPricesAccordingToPreference();

            // verify the sort order
            CheckSortOrderAccordingToPreference(processor.GaugePrices, OFFER_PRICE, preferenceTO);

            // check gauge price integrity
            //if (processor.ShowGauge) CheckGaugePriceListIntegrity(processor.GaugePrices, preferenceTO, offerPrice);
        }

        [Test]
        public void GaugePriceMinimumWidthIsApplied()
        {
            var gaugePrice = new GaugePrice(MarketAnalysisPriceProviders.OfferPrice, String.Empty, 1, 123, 0);
            Assert.IsTrue(gaugePrice.Width == GaugePrice.MinWidth);
        }
    }
}