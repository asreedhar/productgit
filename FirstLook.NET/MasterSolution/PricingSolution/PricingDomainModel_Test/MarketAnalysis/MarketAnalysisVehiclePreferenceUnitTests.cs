using System;
using System.Collections.Generic;
using Autofac;
using Csla;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Implementation;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Types;
using NUnit.Framework;

namespace PricingDomainModel_Test.MarketAnalysis
{
    [TestFixture]
    public class MarketAnalysisVehiclePreferenceUnitTests
    {
        private const string OWNER_HANDLE = "a";
        private const string VEHICLE_HANDLE = "b";


        private static void ResetTestConditions()
        {
            ResetRegistryWithMockDataAccess();
            ResetMockDataStore();
        }

        private static void ResetRegistryWithMockDataAccess()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<MarketAnalysisVehiclePreferenceMockDataAccess>().As<IMarketAnalysisVehiclePreferenceDataAccess>();
            Registry.RegisterContainer(builder.Build()); 
        }

        private static void ResetMockDataStore()
        {
            MarketAnalysisVehiclePreferenceMockDataAccess.Clear();
        }

        [Test]
        public void CreateSaveFetchVehiclePreferenceWithNonEmptyHandles()
        {
            ResetTestConditions();
            MarketAnalysisVehiclePreference preference =
                MarketAnalysisVehiclePreference.CreateMarketAnalysisVehiclePreference(OWNER_HANDLE, VEHICLE_HANDLE);

            Assert.IsNotNull(preference);

            Assert.AreEqual(OWNER_HANDLE, preference.OwnerHandle);
            Assert.AreEqual(VEHICLE_HANDLE, preference.VehicleHandle);

            preference.Save();

            MarketAnalysisVehiclePreference preference2 =
                MarketAnalysisVehiclePreference.GetMarketAnalysisVehiclePreference(OWNER_HANDLE, VEHICLE_HANDLE);

            Assert.AreEqual(OWNER_HANDLE, preference2.OwnerHandle);
            Assert.AreEqual(VEHICLE_HANDLE, preference2.VehicleHandle);
            Assert.AreEqual(preference.IsDisplayed, preference2.IsDisplayed);
            Assert.AreEqual(preference.IsGaugeDisplayed, preference2.IsGaugeDisplayed);
        }

        [Test]
        public void CreateSaveModifyFetchDeleteVehiclePreferenceWithNonEmptyHandles()
        {
            ResetTestConditions();
            MarketAnalysisVehiclePreference preference =
                MarketAnalysisVehiclePreference.CreateMarketAnalysisVehiclePreference(OWNER_HANDLE, VEHICLE_HANDLE);

            Assert.IsNotNull(preference);

            Assert.AreEqual(OWNER_HANDLE, preference.OwnerHandle);
            Assert.AreEqual(VEHICLE_HANDLE, preference.VehicleHandle);
            preference.IsDisplayed = !preference.IsDisplayed;
            preference.IsGaugeDisplayed = !preference.IsGaugeDisplayed;

            preference.Save();

            MarketAnalysisVehiclePreference preference2 =
                MarketAnalysisVehiclePreference.GetMarketAnalysisVehiclePreference(OWNER_HANDLE, VEHICLE_HANDLE);

            Assert.AreEqual(OWNER_HANDLE, preference2.OwnerHandle);
            Assert.AreEqual(VEHICLE_HANDLE, preference2.VehicleHandle);
            Assert.AreEqual(preference.IsDisplayed, preference2.IsDisplayed);
            Assert.AreEqual(preference.IsGaugeDisplayed, preference2.IsGaugeDisplayed);

            preference2.Delete();
            preference2.Save();

            MarketAnalysisVehiclePreference preference3 =
                MarketAnalysisVehiclePreference.GetOrCreateMarketAnalysisVehiclePreference(OWNER_HANDLE, VEHICLE_HANDLE);

            Assert.IsTrue(preference3.IsNew);
        }

        [Test]
        public void CreateSaveModifyFetchVehiclePreferenceWithNonEmptyHandles()
        {
            ResetTestConditions();
            MarketAnalysisVehiclePreference preference =
                MarketAnalysisVehiclePreference.CreateMarketAnalysisVehiclePreference(OWNER_HANDLE, VEHICLE_HANDLE);

            Assert.IsNotNull(preference);

            Assert.AreEqual(OWNER_HANDLE, preference.OwnerHandle);
            Assert.AreEqual(VEHICLE_HANDLE, preference.VehicleHandle);
            preference.IsDisplayed = !preference.IsDisplayed;
            preference.IsGaugeDisplayed = !preference.IsGaugeDisplayed;

            preference.Save();

            MarketAnalysisVehiclePreference preference2 =
                MarketAnalysisVehiclePreference.GetMarketAnalysisVehiclePreference(OWNER_HANDLE, VEHICLE_HANDLE);

            Assert.AreEqual(OWNER_HANDLE, preference2.OwnerHandle);
            Assert.AreEqual(VEHICLE_HANDLE, preference2.VehicleHandle);
            Assert.AreEqual(preference.IsDisplayed, preference2.IsDisplayed);
            Assert.AreEqual(preference.IsGaugeDisplayed, preference2.IsGaugeDisplayed);
        }

        [Test]
        public void CreateVehiclePreferenceWithEmptyStrings()
        {
            ResetTestConditions();
            MarketAnalysisVehiclePreference preference =
                MarketAnalysisVehiclePreference.CreateMarketAnalysisVehiclePreference(String.Empty, String.Empty);

            Assert.IsNotNull(preference);
        }

        [Test]
        public void CreateVehiclePreferenceWithNonEmptyHandles()
        {
            ResetTestConditions();
            MarketAnalysisVehiclePreference preference =
                MarketAnalysisVehiclePreference.CreateMarketAnalysisVehiclePreference(OWNER_HANDLE, VEHICLE_HANDLE);

            Assert.IsNotNull(preference);

            Assert.AreEqual(OWNER_HANDLE, preference.OwnerHandle);
            Assert.AreEqual(VEHICLE_HANDLE, preference.VehicleHandle);
        }

        [Test]
        [ExpectedException(typeof(DataPortalException))]
        public void GetVehiclePreferenceThrowsWhenNoExistingPreferenceExists()
        {
            ResetTestConditions();

            // Try to get the preference - this should throw an exception.
            MarketAnalysisVehiclePreference.GetMarketAnalysisVehiclePreference(String.Empty, String.Empty);
        }

        [Test]
        public void CreateVehiclePreferenceWithoutSelectedPriceProviders()
        {
            ResetTestConditions();

            // just creating a vehicle preference should result 
            // in HasOverriddenPriceProviders being false and SelectedPriceProviders being null.

            MarketAnalysisVehiclePreference preference =
                MarketAnalysisVehiclePreference.CreateMarketAnalysisVehiclePreference(OWNER_HANDLE, VEHICLE_HANDLE);

            Assert.IsNotNull(preference);
            Assert.IsFalse(preference.HasOverriddenPriceProviders);
            Assert.IsNull(preference.SelectedPriceProviders);
        }

        [Test]
        public void SaveVehiclePreferenceWithEmptySelectedPriceProviders()
        {
            ResetTestConditions();

            MarketAnalysisVehiclePreference preference =
                MarketAnalysisVehiclePreference.CreateMarketAnalysisVehiclePreference(OWNER_HANDLE, VEHICLE_HANDLE);

            preference.HasOverriddenPriceProviders = true;
            preference.SelectedPriceProviders = null;   // null and empty list should be treated the same.

            preference.Save();

            MarketAnalysisVehiclePreference preference2 =
            MarketAnalysisVehiclePreference.GetMarketAnalysisVehiclePreference(OWNER_HANDLE, VEHICLE_HANDLE);

            Assert.IsNotNull(preference2);
            Assert.IsTrue(preference2.HasOverriddenPriceProviders);
            
            var providers = new List<MarketAnalysisPriceProviders>(preference2.SelectedPriceProviders);
            Assert.AreEqual(0, providers.Count);
        }

        [Test]
        public void SaveVehiclePreferenceWithNonEmptySelectedPriceProviders()
        {
            ResetTestConditions();

            MarketAnalysisVehiclePreference preference =
                MarketAnalysisVehiclePreference.CreateMarketAnalysisVehiclePreference(OWNER_HANDLE, VEHICLE_HANDLE);

            preference.HasOverriddenPriceProviders = true;
            List<MarketAnalysisPriceProviders> providers = new List<MarketAnalysisPriceProviders>();

            providers.Add(MarketAnalysisPriceProviders.KelleyBlueBookRetailValue);
            providers.Add(MarketAnalysisPriceProviders.EdmundsTrueMarketValue);

            preference.SelectedPriceProviders = providers;

            preference.Save();

            MarketAnalysisVehiclePreference preference2 =
            MarketAnalysisVehiclePreference.GetMarketAnalysisVehiclePreference(OWNER_HANDLE, VEHICLE_HANDLE);

            Assert.IsNotNull(preference2);
            Assert.IsTrue(preference2.HasOverriddenPriceProviders);
            Assert.IsNotNull(preference2.SelectedPriceProviders);

            var providerList = new List<MarketAnalysisPriceProviders>(preference2.SelectedPriceProviders);
            Assert.AreEqual(2, providerList.Count);
        }

        [Test]
        public void SetPriceProvidersToIdenticalListLeavesPreferenceClean()
        {
            // setting the provider list to an identical list (not same object, but with the same contents)
            // should result in the preference not being dirty (no actual update will take place).

            ResetTestConditions();

            MarketAnalysisVehiclePreference preference =
                MarketAnalysisVehiclePreference.CreateMarketAnalysisVehiclePreference(OWNER_HANDLE, VEHICLE_HANDLE);

            preference.HasOverriddenPriceProviders = true;

            // list we will use to set the property
            List<MarketAnalysisPriceProviders> providers1 = new List<MarketAnalysisPriceProviders>();
            providers1.Add(MarketAnalysisPriceProviders.NADARetailValue);

            // A different list (object), with the same contents as providers1
            List<MarketAnalysisPriceProviders> providers2 = new List<MarketAnalysisPriceProviders>();
            providers2.Add(MarketAnalysisPriceProviders.NADARetailValue);

            preference.SelectedPriceProviders = providers1;

            // save it to make the preference not dirty (clean?)
            preference = preference.Save();

            // make sure the save cleaned the preference
            Assert.IsFalse(preference.IsDirty);

            // set the property to an identical list
            preference.SelectedPriceProviders = providers2;

            // make sure it is still clean
            Assert.IsFalse(preference.IsDirty);
        }

        [Test]
        public void SetPriceProvidersToDifferentListLeavesPreferenceDirty()
        {
            // setting the provider list to a different list should result in the preference being dirty.

            ResetTestConditions();

            MarketAnalysisVehiclePreference preference =
                MarketAnalysisVehiclePreference.CreateMarketAnalysisVehiclePreference(OWNER_HANDLE, VEHICLE_HANDLE);

            preference.HasOverriddenPriceProviders = true;

            // list we will use to set the property
            List<MarketAnalysisPriceProviders> providers1 = new List<MarketAnalysisPriceProviders>();
            providers1.Add(MarketAnalysisPriceProviders.NADARetailValue);

            // A different list
            List<MarketAnalysisPriceProviders> providers2 = new List<MarketAnalysisPriceProviders>();
            providers2.Add(MarketAnalysisPriceProviders.KelleyBlueBookRetailValue);

            preference.SelectedPriceProviders = providers1;

            // save it to make the preference not dirty (clean?)
            preference = preference.Save();

            // make sure the save cleaned the preference
            Assert.IsFalse(preference.IsDirty);

            // set the property to an identical list
            preference.SelectedPriceProviders = providers2;

            // make sure it is now dirty
            Assert.IsTrue(preference.IsDirty);
        }

        [Test]
        public void ListPricesNotNull()
        {
            ResetTestConditions();
            MarketAnalysisVehiclePreference preference =
                MarketAnalysisVehiclePreference.CreateMarketAnalysisVehiclePreference(OWNER_HANDLE, VEHICLE_HANDLE);

            Assert.IsNotNull(preference);
          
        }





    }
}