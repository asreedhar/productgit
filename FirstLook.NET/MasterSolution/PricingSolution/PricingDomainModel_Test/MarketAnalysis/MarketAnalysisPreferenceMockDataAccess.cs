using System;
using System.Collections.Generic;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Types;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Implementation;

namespace PricingDomainModel_Test.MarketAnalysis
{
    class MarketAnalysisPreferenceMockDataAccess : IMarketAnalysisDealerPreferenceDataAccess
    {
        private static readonly Dictionary<string, MarketAnalysisDealerPreferenceTO> Preferences =
            new Dictionary<string, MarketAnalysisDealerPreferenceTO>();

        private static int _maxId = 1;
        private static int _maxProviderId = 1;

        public static void Clear()
        {
            Preferences.Clear();
        }

        #region IMarketAnalysisDealerPreferenceDataAccess implementation
        public ITransaction BeginTransaction()
        {
            return new MockTransaction();
        }

        public bool Exists(string ownerHandle)
        {
            return Preferences.ContainsKey(ownerHandle);
        }

        public MarketAnalysisDealerPreferenceTO Create(string ownerHandle)
        {
            MarketAnalysisDealerPreferenceTO dto = new MarketAnalysisDealerPreferenceTO();
            dto.Id = 0;
            dto.OwnerHandle = ownerHandle;
            dto.NumberOfPricesRequiredForPricingGauge = 1;
            dto.UseCustomOfferPrice = false;
            dto.UseCustomCurrentInternetPrice = false;
            dto.UseCustomComparisonPricePriority = false;
            dto.LogoUrl = String.Empty;
            dto.PriceProviders.Add(new PriceProvider(1, MarketAnalysisPriceProviders.OfferPrice, "Offer Price", 1, dto.LogoUrl));
            dto.PriceProviders.Add(new PriceProvider(2, MarketAnalysisPriceProviders.CurrentInternetPrice, "Current Internet Price", 2, dto.LogoUrl));
            dto.PriceProviders.Add(new PriceProvider(3, MarketAnalysisPriceProviders.PINGMarketAverageInternetPrice, "PING Market Average Internet Price", 3, dto.LogoUrl));
            dto.PriceProviders.Add(new PriceProvider(4, MarketAnalysisPriceProviders.JDPowerPINAverageInternetPrice, "JD Power PIN Average Internet Price", 4, dto.LogoUrl));
            dto.PriceProviders.Add(new PriceProvider(5, MarketAnalysisPriceProviders.EdmundsTrueMarketValue, "Edmunds True Market Value", 5, dto.LogoUrl));
            dto.PriceProviders.Add(new PriceProvider(6, MarketAnalysisPriceProviders.NADARetailValue, "NADA Retail Value", 6, dto.LogoUrl));
            dto.PriceProviders.Add(new PriceProvider(7, MarketAnalysisPriceProviders.KelleyBlueBookRetailValue, "Kelley Blue Book Retail Value", 7, dto.LogoUrl));
            dto.PriceProviders.Add(new PriceProvider(8, MarketAnalysisPriceProviders.BlackBook, "BlackBook Extra Clean Value", 8, dto.LogoUrl));
            dto.PriceProviders.Add(new PriceProvider(9, MarketAnalysisPriceProviders.Galves, "Galves Market Ready Value", 9, dto.LogoUrl));
            return dto;
        }

        public MarketAnalysisDealerPreferenceTO Fetch(string ownerHandle)
        {

            // Return object if it exists, otherwise return null.
            if (Preferences.ContainsKey(ownerHandle))
                return Preferences[ownerHandle];

            // If nothing found, throw.
            throw new ApplicationException("No market analysis preference could be found with the specified handle.");
        }

        public void Insert(ITransaction transaction, string ownerHandle, MarketAnalysisDealerPreferenceTO dealerPreferenceTO)
        {
            dealerPreferenceTO.Id = _maxId++;

            foreach (PriceProvider pp in dealerPreferenceTO.PriceProviders)
            {
                pp.Id = _maxProviderId++;
            }

            // Save preference dto.
            Preferences.Add(ownerHandle, dealerPreferenceTO);
        }

        public void Update(ITransaction transaction, MarketAnalysisDealerPreferenceTO dealerPreference)
        {
            if (!Exists(dealerPreference.OwnerHandle))
            {
                throw new ApplicationException("Preference does not exist");
            }

            Preferences[dealerPreference.OwnerHandle] = dealerPreference;
        }

        public void ChangePriceProviderRank(ITransaction transaction, int ownerPriceProviderId, int newRank, int varianceAmount, int varianceDirection)
        {
            foreach (KeyValuePair<string, MarketAnalysisDealerPreferenceTO> to in Preferences)
            {
                var preferenceTO = to.Value;
                foreach (PriceProvider ownerPriceProvider in preferenceTO.PriceProviders)
                {
                    if (ownerPriceProvider.Id == ownerPriceProviderId)
                    {
                        if (ownerPriceProvider.Rank != newRank)
                        {
                            DoTheRankChange(preferenceTO, ownerPriceProvider.Rank, newRank);
                        }
                    }
                }
            }
        }

        public void InsertPriceProviderRank(ITransaction transaction, string ownerHandle, MarketAnalysisPriceProviders provider, int rank, int varianceAmount, int varianceDirection)
        {
            // this mimics the behavior in the database as close as conveniently possible.

            foreach (var p in Preferences)
            {
                if (p.Value.OwnerHandle == ownerHandle)
                {
                    int maxRank = 0;
    
                    // go through each, making sure that the provider doesn't already exist in the collection;
                    // also get the max value for ranks in the price provider collection.
                    foreach (var pp in p.Value.PriceProviders)
                    {
                        if (pp.PriceProviderId == provider)
                        {
                            throw new ApplicationException("Price Provider Already Exists.");
                        }
                        
                        // track the ranks to set this one to max + 1
                        maxRank = pp.Rank > maxRank ? pp.Rank : maxRank;
                    }

                    // get our new id for the price provider rank 'row'
                    int newId = _maxProviderId++;

                    // add our new price provider 'row'
                    p.Value.PriceProviders.Add(new PriceProvider(newId, provider, "new provider", maxRank + 1, string.Empty));

                    // update the rank
                    DoTheRankChange(p.Value, maxRank + 1, rank);

                    return;
                }

                throw new ApplicationException("Owner Handle not found.");

            }

            throw new ApplicationException("No Preferences were found.");
            
        }

        private void DoTheRankChange(MarketAnalysisDealerPreferenceTO to, int oldRank, int newRank)
        {
            foreach (PriceProvider ownerPriceProvider in to.PriceProviders)
            {
                if (((newRank < oldRank) && (ownerPriceProvider.Rank >= newRank) && (ownerPriceProvider.Rank <= oldRank))
                    ||
                    ((newRank > oldRank) && (ownerPriceProvider.Rank <= newRank) && (ownerPriceProvider.Rank >= oldRank)))
                {
                    if (ownerPriceProvider.Rank == oldRank) ownerPriceProvider.Rank = newRank;
                    else if ((newRank < oldRank) && (ownerPriceProvider.Rank >= newRank) && (ownerPriceProvider.Rank <= oldRank))
                        ownerPriceProvider.Rank++;
                    else if ((newRank > oldRank) && (ownerPriceProvider.Rank <= newRank) && (ownerPriceProvider.Rank >= oldRank))
                        ownerPriceProvider.Rank--;
                }
            }

            CheckForDuplicateRanks(to);

        }

        private void CheckForDuplicateRanks(MarketAnalysisDealerPreferenceTO to)
        {
            List<int> ranks = new List<int>();

            foreach (PriceProvider pp in to.PriceProviders)
            {
                if (ranks.Contains(pp.Rank))
                {
                    throw new ApplicationException("A price provider can not have the same rank as another price provider for a particular owner.");
                }
                ranks.Add(pp.Rank);
            }
        }

        #endregion

        private class MockTransaction : ITransaction
        {
            public void Dispose()
            {

            }

            public void Commit()
            {

            }

            public void Rollback()
            {

            }
        }
    }
}
