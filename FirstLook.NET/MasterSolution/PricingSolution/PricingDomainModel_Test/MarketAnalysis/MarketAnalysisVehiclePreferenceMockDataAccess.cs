using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Implementation;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Types;

namespace PricingDomainModel_Test.MarketAnalysis
{
    class MarketAnalysisVehiclePreferenceMockDataAccess : IMarketAnalysisVehiclePreferenceDataAccess
    {
        private static readonly Dictionary<string, MarketAnalysisVehiclePreferenceTO> Preferences = new Dictionary<string, MarketAnalysisVehiclePreferenceTO>();

        private static int _maxId = 1;

        public static void Clear()
        {
            Preferences.Clear();
        }

        private static string CreateKey(string ownerHandle, string vehicleHandle)
        {
            return ownerHandle + ";" + vehicleHandle;
        }

        #region Implementation of IMarketAnalysisVehiclePreferenceDataAccess

        public bool Exists(string ownerHandle, string vehicleHandle)
        {
            string key = CreateKey(ownerHandle, vehicleHandle);
            return Preferences.ContainsKey(key);
        }

        public MarketAnalysisVehiclePreferenceTO Create(string ownerHandle, string vehicleHandle)
        {
			return new MarketAnalysisVehiclePreferenceTO(null, ownerHandle, vehicleHandle, true, true, false, null);
        }


        public MarketAnalysisVehiclePreferenceTO Fetch(string ownerHandle, string vehicleHandle)
        {
            // Get key
            string key = CreateKey(ownerHandle, vehicleHandle);

            // Return object if it exists, otherwise throw.
            if (Preferences.ContainsKey(key))
                return Preferences[key];

            // If nothing found, throw.
            throw new ApplicationException("No market analysis vehicle preference could be found with the specified handles.");
        }

        public void Insert(MarketAnalysisVehiclePreferenceTO preference, string userName)
        {
            preference.MarketAnalysisVehiclePreferenceId = _maxId;

            // Construct key and increment id.
            _maxId++;
            string key = CreateKey(preference.OwnerHandle, preference.VehicleHandle);

            // Save preference dto.
            Preferences.Add(key, preference);
        }

        public void Update(MarketAnalysisVehiclePreferenceTO preference, string userName)
        {
            if (!Exists(preference.OwnerHandle, preference.VehicleHandle))
            {
                throw new ApplicationException("Does not exist.");
            }

            string key = CreateKey(preference.OwnerHandle, preference.VehicleHandle);

            Preferences[key] = preference;
        }

        public void Delete(MarketAnalysisVehiclePreferenceTO preference, string userName)
        {
            // If the preference exists, delete it.
            if (!Exists(preference.OwnerHandle, preference.VehicleHandle))
            {
                return;
            }

            string key = CreateKey(preference.OwnerHandle, preference.VehicleHandle);
            Preferences.Remove(key);
        }

        #endregion
    }
}
