
using Autofac;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Types;
using NUnit.Framework;


namespace PricingDomainModel_Test.MarketAnalysis
{
    [TestFixture]
    public class MarketAnalysisDealerPreferenceUnitTests
    {

        [Test]
        public void CreateNewDealerPreference()
        {
            ResetTestConditions();
            MarketAnalysisDealerPreference preference =
                MarketAnalysisDealerPreference.GetOrCreateMarketAnalysisDealerPreference(System.Guid.NewGuid().ToString());
            Assert.AreEqual(0, preference.Id);
        }

        [Test]
        public void GetOrCreateDealerPreferenceReturnsZeroIdWhenPrefDoesNotExist()
        {
            ResetTestConditions();
            MarketAnalysisDealerPreference preference =
                MarketAnalysisDealerPreference.GetOrCreateMarketAnalysisDealerPreference(System.Guid.NewGuid().ToString());
            Assert.AreEqual(0, preference.Id);
        }

        [Test]
        public void CreateSaveRetrieveDealerPreference()
        {
            ResetTestConditions();

            string oh = System.Guid.NewGuid().ToString();

            MarketAnalysisDealerPreference preference =
                MarketAnalysisDealerPreference.GetOrCreateMarketAnalysisDealerPreference(oh);

            // new preference should have an id equal to zero
            Assert.AreEqual(0, preference.Id);

            preference.Save();


            MarketAnalysisDealerPreference preference2 =
                MarketAnalysisDealerPreference.GetOrCreateMarketAnalysisDealerPreference(oh);

            // new preference should have a positive id
            Assert.IsTrue(preference2.Id > 0);
        }

        [Test]
        public void CreateSaveRetrieveMultipleDealerPreferences()
        {
            ResetTestConditions();

            string oh1 = System.Guid.NewGuid().ToString();
            string oh2 = System.Guid.NewGuid().ToString();

            MarketAnalysisDealerPreference preference1 =
                MarketAnalysisDealerPreference.GetOrCreateMarketAnalysisDealerPreference(oh1);

            // new preference should have an id equal to zero
            Assert.AreEqual(0, preference1.Id);

            preference1.Save();

            MarketAnalysisDealerPreference found1 =
                MarketAnalysisDealerPreference.GetOrCreateMarketAnalysisDealerPreference(oh1);

            // new preference should have a positive id
            Assert.IsTrue(found1.Id > 0);

            MarketAnalysisDealerPreference preference2 =
                MarketAnalysisDealerPreference.GetOrCreateMarketAnalysisDealerPreference(oh2);


            // new preference should have an id equal to zero
            Assert.AreEqual(0, preference2.Id);

            preference2.Save();

            MarketAnalysisDealerPreference found2 =
                MarketAnalysisDealerPreference.GetOrCreateMarketAnalysisDealerPreference(oh2);


            // new preference should have a positive id
            Assert.IsTrue(found2.Id > 0);
            // new preference should have an id != to the other preference.
            Assert.AreNotEqual( found2.Id, found1.Id );  
        }

        [Test]
        public void CreateTryToRetrieveDealerPreference()
        {
            ResetTestConditions();

            string oh = System.Guid.NewGuid().ToString();

            MarketAnalysisDealerPreference preference =
                MarketAnalysisDealerPreference.GetOrCreateMarketAnalysisDealerPreference(oh);

            // new preference should have an id equal to zero
            Assert.AreEqual(0, preference.Id);
            
            // don't save, try to retrieve...
            //preference.Save();

            MarketAnalysisDealerPreference preference2 =
                MarketAnalysisDealerPreference.GetOrCreateMarketAnalysisDealerPreference(oh);

            // preference should have an id equal to zero (did not exist)
            Assert.AreEqual(0, preference2.Id);
        }


        #region Reset Test Conditions

        private static void ResetTestConditions()
        {
            ResetRegistryWithMockDataAccess();
            ResetMockDataStore();
        }

        private static void ResetRegistryWithMockDataAccess()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<MarketAnalysisPreferenceMockDataAccess>().As<IMarketAnalysisDealerPreferenceDataAccess>();
            Registry.RegisterContainer(builder.Build());
        }

        private static void ResetMockDataStore()
        {
            MarketAnalysisPreferenceMockDataAccess.Clear();
        }

        #endregion
    }
}