﻿using System.Reflection;
using System.Runtime.CompilerServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("PricingDomainModel_Test")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyProduct("PricingDomainModel_Test")]

[assembly: InternalsVisibleTo("PricingDomainModel_IntegrationTests")]