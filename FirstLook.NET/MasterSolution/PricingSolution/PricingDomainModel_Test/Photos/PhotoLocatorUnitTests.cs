using System;
using Autofac;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Core.Membership;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.Merchandising.DomainModel.Marketing.Photos;
using Moq;
using NUnit.Framework;

namespace PricingDomainModel_Test.Photos
{
    [TestFixture]
    public class PhotoLocatorUnitTests
    {
        private readonly string _ownerHandleNotFound = Guid.Empty.ToString();
        private readonly string _ownerHandle = Guid.NewGuid().ToString();

        private readonly string _vehicleHandle = Guid.NewGuid().ToString();
        private readonly string _vehicleHandleNotFound = Guid.Empty.ToString();

        private const string DEFAULT_URL = "default";

        private const string LOCAL_IMAGE_ROOT_URL = "http://local/";
        private const string REMOTE_IMAGE_ROOT_URL = "http://remote/";

        private PhotoLocator _photoLocator;

        private Mock<IPhotoServices> mockPhotoServices;
        private Mock<IPhotoServicesIdMap> mockPhotoServicesIdMap;
        private Mock<ILogger> mockLogger;
            
        [SetUp]
        public void Setup()
        {
            ResetTestConditions();
            _photoLocator = new PhotoLocator(new NullLogger(), new PhotoLocatorMockDataAccess(), mockPhotoServices.Object,
                LOCAL_IMAGE_ROOT_URL, REMOTE_IMAGE_ROOT_URL);
        }

        [TearDown]
        public void Teardown()
        {
            Registry.Reset();
        }

        [Test]
        public void CanCreatePhotoLocator()
        {
            Assert.IsNotNull( _photoLocator );
        }

        [Test]
        public void LocateDealerLogoUrlReturnsUrl()
        {
            string url = _photoLocator.LocateDealerLogoUrl(_ownerHandle, DEFAULT_URL);

            Console.WriteLine( url );

            Assert.IsNotEmpty( url );
            Assert.IsTrue( url.StartsWith(REMOTE_IMAGE_ROOT_URL));
            Assert.AreNotEqual( DEFAULT_URL, url );
        }

        [Test]
        public void LocateDealerLogoUrlReturnsDefault()
        {
            string url = _photoLocator.LocateDealerLogoUrl(_ownerHandleNotFound, DEFAULT_URL);

            Console.WriteLine( url );

            Assert.IsTrue(url.StartsWith(LOCAL_IMAGE_ROOT_URL));
            Assert.AreEqual(LOCAL_IMAGE_ROOT_URL + DEFAULT_URL, url);
        }

        [Test]
        public void LocateVehiclePhotoUrlReturnsUrl()
        {
            mockPhotoServicesIdMap.Setup(
                svc => svc.MapOwnerAndVehicleHandleToBusinessUnitAndVIN(_ownerHandle, _vehicleHandle))
                .Returns(new BusinessUnitAndVIN("WINDY", "SOMEVIN"));
            mockPhotoServices.Setup(
                svc => svc.GetMainPhotoUrl("WINDY", "SOMEVIN")).Returns(REMOTE_IMAGE_ROOT_URL + "WINDYCITY/SOMEVIN/main.jpg");

            string url = _photoLocator.LocateVehiclePhotoUrl(_ownerHandle, _vehicleHandle, DEFAULT_URL);

            Console.WriteLine( url );

            Assert.IsNotEmpty( url );
            Assert.IsTrue(url.StartsWith(REMOTE_IMAGE_ROOT_URL));
            Assert.AreNotEqual(REMOTE_IMAGE_ROOT_URL + DEFAULT_URL, url);
        }

        [Test]
        public void LocateVehiclePhotoUrlReturnsDefault()
        {
            string url = _photoLocator.LocateVehiclePhotoUrl(_ownerHandle, _vehicleHandleNotFound, DEFAULT_URL);

            Console.WriteLine( url );

            Assert.IsTrue(url.StartsWith(LOCAL_IMAGE_ROOT_URL));
            Assert.AreEqual(LOCAL_IMAGE_ROOT_URL + DEFAULT_URL, url);
        }

        [Test]
        public void LocalImageRootUrl()
        {
            string url = _photoLocator.LocalImageRootUrl;

            Assert.AreEqual(LOCAL_IMAGE_ROOT_URL, url);
        }

        [Test]
        public void RemoteImageRootUrl()
        {
            string url = _photoLocator.RemoteImageRootUrl;

            Assert.AreEqual(REMOTE_IMAGE_ROOT_URL, url);
        }

        #region Brand Logos

        [Test]
        public void EdmundsFileName()
        {
            string fileName = _photoLocator.BrandLogoFileName(PhotoLocator.BrandLogo.EdmundsTrueMarketValue);
            Assert.AreEqual("logo_Edmunds.gif", fileName);
        }

        [Test]
        public void JdPowerFileName()
        {
            string fileName = _photoLocator.BrandLogoFileName(PhotoLocator.BrandLogo.JdPowerPinAverageInternetPrice);
            Assert.AreEqual("logo_JDPower.gif", fileName);
        }

        [Test]
        public void KellyBlueBookFileName()
        {
            string fileName = _photoLocator.BrandLogoFileName(PhotoLocator.BrandLogo.KelleyBlueBookRetailValue);
            Assert.AreEqual("logo_KBB2.gif", fileName);
        }


        [Test]
        public void NadaRetailFileName()
        {
            string fileName = _photoLocator.BrandLogoFileName(PhotoLocator.BrandLogo.NadaRetailValue);
            Assert.AreEqual("logo_NADA.gif", fileName);
        }

        [Test]
        public void PingFileName()
        {
            string fileName = _photoLocator.BrandLogoFileName(PhotoLocator.BrandLogo.PingMarketAverageInternetPrice);
            Assert.AreEqual("logo_MktAvgInternetPrice.png", fileName);
        }

        [Test]
        public void BrandLogosHaveLocalUrls()
        {
            Assert.IsTrue(_photoLocator.BrandLogoUrl(PhotoLocator.BrandLogo.EdmundsTrueMarketValue).StartsWith(LOCAL_IMAGE_ROOT_URL));
            Assert.IsTrue(_photoLocator.BrandLogoUrl(PhotoLocator.BrandLogo.JdPowerPinAverageInternetPrice).StartsWith(LOCAL_IMAGE_ROOT_URL));
            Assert.IsTrue(_photoLocator.BrandLogoUrl(PhotoLocator.BrandLogo.KelleyBlueBookRetailValue).StartsWith(LOCAL_IMAGE_ROOT_URL));
            Assert.IsTrue(_photoLocator.BrandLogoUrl(PhotoLocator.BrandLogo.NadaRetailValue).StartsWith(LOCAL_IMAGE_ROOT_URL));
            Assert.IsTrue(_photoLocator.BrandLogoUrl(PhotoLocator.BrandLogo.PingMarketAverageInternetPrice).StartsWith(LOCAL_IMAGE_ROOT_URL));
            Assert.IsTrue(_photoLocator.BrandLogoUrl(PhotoLocator.BrandLogo.Galves).StartsWith(LOCAL_IMAGE_ROOT_URL));
            Assert.IsTrue(_photoLocator.BrandLogoUrl(PhotoLocator.BrandLogo.BlackBook).StartsWith(LOCAL_IMAGE_ROOT_URL));
        }

        #endregion


        [Test]
        public void ExternallyHostedImage()
        {
            string remoteHandle = PhotoLocatorMockDataAccess.ExternalUrlHandle;
            string vehicleHandle = "123456";

            string dealerLogoUrl = _photoLocator.LocateDealerLogoUrl(remoteHandle, string.Empty);
            Assert.AreEqual(PhotoLocatorMockDataAccess.ExternalUrl, dealerLogoUrl);

            mockPhotoServicesIdMap.Setup(
                svc => svc.MapOwnerAndVehicleHandleToBusinessUnitAndVIN(remoteHandle, vehicleHandle))
                .Returns(new BusinessUnitAndVIN("WINDY", "SOMEVIN123"));
            mockPhotoServices.Setup(svc => svc.GetMainPhotoUrl("WINDY", "SOMEVIN123"))
                .Returns(PhotoLocatorMockDataAccess.ExternalUrl);

            string vehicleLogoUrl = _photoLocator.LocateVehiclePhotoUrl(remoteHandle, vehicleHandle, string.Empty);
            Assert.AreEqual(PhotoLocatorMockDataAccess.ExternalUrl, vehicleLogoUrl);
        }

        #region Reset Test Conditions

        private void ResetTestConditions()
        {
            mockPhotoServices = new Mock<IPhotoServices>();
            mockPhotoServicesIdMap = new Mock<IPhotoServicesIdMap>();
            mockLogger = new Mock<ILogger>();

            SetupRegistry();
        }

        private void SetupRegistry()
        {
            var builder = new ContainerBuilder();
            builder.RegisterInstance(mockPhotoServicesIdMap.Object).As<IPhotoServicesIdMap>();
            Registry.RegisterContainer(builder.Build());
        }
        
        #endregion

        public class PhotoLocatorMockDataAccess : IPhotoLocatorDataAccess
        {
            private readonly string _logoPath;

            public const string ExternalUrl = "http://some.url";                
            public static readonly string ExternalUrlHandle = Guid.NewGuid().ToString();

            public PhotoLocatorMockDataAccess()
            {
                _logoPath = "logo";
            }

            #region Implementation of IPhotoLocatorDataAccess

            public string FetchDealerLogoUrl(string ownerHandle)
            {
                // simulate a url reply
                if (ownerHandle == ExternalUrlHandle )
                {
                    return ExternalUrl;
                }

                // simulate a not found when handle is empty guid.
                return ownerHandle == Guid.Empty.ToString() ? string.Empty : _logoPath;
            }

            #endregion
        }

    }
}
