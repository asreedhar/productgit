﻿using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Marketing.Photos;
using FirstLook.Merchandising.DomainModel.Marketing.Photos.Types;

namespace PricingDomainModel_Test.Photos
{
    class PhotoVehiclePreferenceMockDataAccess : IPhotoVehiclePreferenceDataAccess
    {
        private static readonly Dictionary<string, PhotoVehiclePreferenceTO> Preferences = new Dictionary<string, PhotoVehiclePreferenceTO>();

        public static void Clear()
        {
            Preferences.Clear();
        }

        private static string CreateKey(string ownerHandle, string vehicleHandle)
        {
            return ownerHandle + ";" + vehicleHandle;
        }

        #region Implementation of IPhotoVehiclePreferenceDataAccess
        public PhotoVehiclePreferenceTO GetPhotoVehiclePreference(string ownerHandle, string vehicleHandle)
        {
            // Get key
            string key = CreateKey(ownerHandle, vehicleHandle);

            // Return the preference if it exists or a default
            if (Preferences.ContainsKey(key))
                return Preferences[key];

            return new PhotoVehiclePreferenceTO(ownerHandle, vehicleHandle, true);
        }

        public void SetPhotoVehiclePreference(PhotoVehiclePreferenceTO preference, string userName)
        {
            string key = CreateKey(preference.OwnerHandle, preference.VehicleHandle);

            if (Preferences.ContainsKey(key))
            {
                Preferences[key].IsDisplayed = preference.IsDisplayed;
            }
            else
            {
                Preferences.Add(key, preference);
            }
        }
        #endregion
    }

}
