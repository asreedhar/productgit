 
using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Marketing.Photos;
using NUnit.Framework;

namespace PricingDomainModel_Test.Photos
{
    [TestFixture]
    public class ImageScalerUnitTests
    {
        private ImageScaler _scaler;

        private const int MAX_DIM = 400;

        [TestFixtureSetUp]
        public void Setup()
        {
            _scaler = new ImageScaler();
        }

        [Test]
        public void CanCreate()
        {
            Assert.IsNotNull( _scaler );
        }

        [Test]
        public void InputEqualsMax()
        {
            int height, width;
            _scaler.Scale(MAX_DIM, MAX_DIM, MAX_DIM, MAX_DIM, out height, out width);

            Console.WriteLine("height: " + height);
            Console.WriteLine("width: " + width);            

            Assert.AreEqual( MAX_DIM, height );
            Assert.AreEqual( MAX_DIM, width );

            Assert.AreEqual( Scale(MAX_DIM, MAX_DIM), Scale(width, height));
        }

        [Test]
        public void InputLessThanMax()
        {
            int height, width;
            _scaler.Scale(MAX_DIM, MAX_DIM, MAX_DIM - 1, MAX_DIM - 1, out height, out width);

            Console.WriteLine("height: " + height);
            Console.WriteLine("width: " + width);            

            Assert.AreEqual(MAX_DIM - 1, height);
            Assert.AreEqual(MAX_DIM - 1, width);

            Assert.AreEqual(Scale(MAX_DIM - 1, MAX_DIM - 1), Scale(width, height));

        }

        [Test]
        public void InputHeightZero()
        {
            int height, width;
            _scaler.Scale(MAX_DIM, MAX_DIM, 0, MAX_DIM, out height, out width);

            Console.WriteLine("height: " + height);
            Console.WriteLine("width: " + width);            

            Assert.AreEqual(0, height);
            Assert.AreEqual(0, width);                        
        }

        [Test]
        public void InputWidthZero()
        {
            int height, width;
            _scaler.Scale(MAX_DIM, MAX_DIM, MAX_DIM, 0, out height, out width);

            Assert.AreEqual(0, height);
            Assert.AreEqual(0, width);
        }

        [Test]
        public void HeightExceedsMax()
        {
            int height, width;

            // height : width = 4 : 1
            const int NATIVE_HEIGHT = 2 * MAX_DIM;
            const int NATIVE_WIDTH = MAX_DIM / 2;

            _scaler.Scale(MAX_DIM, MAX_DIM, NATIVE_HEIGHT, NATIVE_WIDTH, out height, out width);

            Console.WriteLine("height: " + height);
            Console.WriteLine("width: " + width);            

            Assert.AreEqual(MAX_DIM, height);
            Assert.AreEqual(MAX_DIM/4, width);

            Assert.AreEqual(Scale(NATIVE_WIDTH, NATIVE_HEIGHT), Scale(width, height));

        }

        [Test]
        public void WidthExceedsMax()
        {
            // height : width = 1 : 4
            const int NATIVE_HEIGHT = MAX_DIM / 2;
            const int NATIVE_WIDTH = 2 * MAX_DIM;

            int height, width;
            _scaler.Scale(MAX_DIM, MAX_DIM, NATIVE_HEIGHT, NATIVE_WIDTH, out height, out width);

            Console.WriteLine("height: " + height);
            Console.WriteLine("width: " + width);            

            Assert.AreEqual(MAX_DIM / 4, height);
            Assert.AreEqual(MAX_DIM, width);

            Assert.AreEqual(Scale(NATIVE_WIDTH, NATIVE_HEIGHT), Scale(width, height));
        }

        [Test]
        public void SquareImageConstrainedByHeight()
        {
            const int MAX_WIDTH = 400;
            const int MAX_HEIGHT = 300;

            int height, width;
            _scaler.Scale(MAX_HEIGHT, MAX_WIDTH, MAX_HEIGHT, MAX_HEIGHT, out height, out width);

            Console.WriteLine("height: " + height);
            Console.WriteLine("width: " + width);            

            Assert.IsTrue( height <= MAX_HEIGHT );
            Assert.IsTrue( width <= MAX_WIDTH );

            Assert.AreEqual(Scale(MAX_HEIGHT, MAX_HEIGHT), Scale(width, height));
        }

        [Test]
        public void SquareImageConstrainedByWidth()
        {
            const int MAX_WIDTH = 400;
            const int MAX_HEIGHT = 300;

            int height, width;
            _scaler.Scale(MAX_HEIGHT, MAX_WIDTH, MAX_WIDTH, MAX_WIDTH, out height, out width);

            Console.WriteLine("height: " + height);
            Console.WriteLine("width: " + width);

            Assert.IsTrue(height <= MAX_HEIGHT);
            Assert.IsTrue(width <= MAX_WIDTH);

            Assert.AreEqual(Scale(MAX_WIDTH, MAX_WIDTH), Scale(width, height));
        }

        [Test]
        public void SlightlyWideLargeRectangle()
        {
            const int MAX_WIDTH = 128;
            const int MAX_HEIGHT = 26;

            const int NATIVE_HEIGHT = 600;
            const int NATIVE_WIDTH = 800;

            int height, width;
            _scaler.Scale(MAX_HEIGHT, MAX_WIDTH, NATIVE_HEIGHT, NATIVE_WIDTH, out height, out width);

            Console.WriteLine("height: " + height);
            Console.WriteLine("width: " + width);

            Assert.IsTrue(height <= MAX_HEIGHT);
            Assert.IsTrue(width <= MAX_WIDTH);

            double originalScale = Scale(NATIVE_WIDTH, NATIVE_HEIGHT);
            double outputScale = Scale(width, height);

            Assert.IsTrue(ScalesAreWithinTolerance(originalScale, outputScale));
        }
        [Test]
        public void SlightlyTallLargeRectangle()
        {
            const int MAX_WIDTH = 128;
            const int MAX_HEIGHT = 26;

            const int NATIVE_HEIGHT = 800;
            const int NATIVE_WIDTH = 600;

            int height, width;
            _scaler.Scale(MAX_HEIGHT, MAX_WIDTH, NATIVE_HEIGHT, NATIVE_WIDTH, out height, out width);

            Console.WriteLine("height: " + height);
            Console.WriteLine("width: " + width);

            Assert.IsTrue(height <= MAX_HEIGHT);
            Assert.IsTrue(width <= MAX_WIDTH);

            double originalScale = Scale(NATIVE_WIDTH, NATIVE_HEIGHT);
            double outputScale = Scale(width, height);

            Assert.IsTrue(ScalesAreWithinTolerance(originalScale, outputScale));
        }

        [Test]
        public void TooTall()
        {
            const int MAX_WIDTH = 100;
            const int MAX_HEIGHT = 50;

            const int NATIVE_WIDTH = 40;
            const int NATIVE_HEIGHT = 200;

            int height, width;
            _scaler.Scale(MAX_HEIGHT, MAX_WIDTH, NATIVE_HEIGHT, NATIVE_WIDTH, out height, out width);

            Console.WriteLine("height: " + height);
            Console.WriteLine("width: " + width);

            Assert.IsTrue(height <= MAX_HEIGHT);
            Assert.IsTrue(width <= MAX_WIDTH);

            double originalScale = Scale(NATIVE_WIDTH, NATIVE_HEIGHT);
            double outputScale = Scale(width, height);

            Assert.IsTrue(ScalesAreWithinTolerance(originalScale, outputScale));
        }

        [Test]
        public void TooWide()
        {
            const int MAX_WIDTH = 100;
            const int MAX_HEIGHT = 50;

            const int NATIVE_WIDTH = 200;
            const int NATIVE_HEIGHT = 40;

            int height, width;
            _scaler.Scale(MAX_HEIGHT, MAX_WIDTH, NATIVE_HEIGHT, NATIVE_WIDTH, out height, out width);

            Console.WriteLine("height: " + height);
            Console.WriteLine("width: " + width);

            Assert.IsTrue(height <= MAX_HEIGHT);
            Assert.IsTrue(width <= MAX_WIDTH);

            double originalScale = Scale(NATIVE_WIDTH, NATIVE_HEIGHT);
            double outputScale = Scale(width, height);

            Assert.IsTrue(ScalesAreWithinTolerance(originalScale, outputScale));
        }

        [Test]
        public void OnePixelWide()
        {
            const int MAX_WIDTH = 100;
            const int MAX_HEIGHT = 100;

            const int NATIVE_WIDTH = 150;
            const int NATIVE_HEIGHT = 1;

            int height, width;
            _scaler.Scale(MAX_HEIGHT, MAX_WIDTH, NATIVE_HEIGHT, NATIVE_WIDTH, out height, out width);

            Console.WriteLine("height: " + height);
            Console.WriteLine("width: " + width);

            Assert.IsTrue(height <= MAX_HEIGHT);
            Assert.IsTrue(width <= MAX_WIDTH);

            // We cannot scale this image and maintain the proportions.  Therefore, the calculated dimensions are both 0.
            // This seems to make more sense than displaying a highly stretched image.
            Assert.AreEqual(0, height);
            Assert.AreEqual(0, width);
        }

        [Test]
        public void OnePixelTall()
        {
            const int MAX_WIDTH = 100;
            const int MAX_HEIGHT = 100;

            const int NATIVE_WIDTH = 1;
            const int NATIVE_HEIGHT = 150;

            int height, width;
            _scaler.Scale(MAX_HEIGHT, MAX_WIDTH, NATIVE_HEIGHT, NATIVE_WIDTH, out height, out width);

            Console.WriteLine("height: " + height);
            Console.WriteLine("width: " + width);

            Assert.IsTrue(height <= MAX_HEIGHT);
            Assert.IsTrue(width <= MAX_WIDTH);

            // We cannot scale this image and maintain the proportions.  Therefore, the calculated dimensions are both 0.
            // This seems to make more sense than displaying a highly stretched image.
            Assert.AreEqual(0, height);
            Assert.AreEqual(0, width);
        }

        [Test]
        public void LargeMaximums()
        {
            const int MAX_WIDTH = 132;
            const int MAX_HEIGHT = Int32.MaxValue;

            const int NATIVE_WIDTH = 900;
            const int NATIVE_HEIGHT = 750;

            int height, width;
            _scaler.Scale(MAX_HEIGHT, MAX_WIDTH, NATIVE_HEIGHT, NATIVE_WIDTH, out height, out width);

            Console.WriteLine("height: " + height);
            Console.WriteLine("width: " + width);

            Assert.IsTrue(height <= MAX_HEIGHT);
            Assert.IsTrue(width <= MAX_WIDTH);

            double originalScale = Scale(NATIVE_WIDTH, NATIVE_HEIGHT);
            double outputScale = Scale(width, height);

            Assert.IsTrue(ScalesAreWithinTolerance(originalScale, outputScale));
        }

        /// <summary>
        /// Test a large range of random numbers.
        /// </summary>
        [Test]
        [Ignore]
        public void RandomRectangleRange()
        {
            const int NUM_TESTS = 5000;
            const int MAX_ORIGINAL_DIMENSIONS = 5000;

            const int PERCENT_TOLERANCE = 15;
            const int MAX_AVERAGE_SCALE_DISTORTION_PERCENTAGE = 2;

            const int MAX_WIDTH = 128;
            const int MAX_HEIGHT = 26;

            // Capture all the percent differences so we can display the average distortion across all test runs.
            IList<double> scaleDistortions = new List<double>();

            // Increment the native dimensions for a bit.
            for (int count = 0; count < NUM_TESTS; count++)
            {
                int nativeHeight = 0;
                int nativeWidth = 0;

                // The max scaling that can be done is w:h, 128:1, 1:26.  However, at these extremes, we cannot 
                // guarantee that we can scale the image down without introducing significant distortions.  It is also
                // unlikely we will be asked to scale such images.
                // We will therefore test a random sequence if inputs which do not exceed 10:1, 1:10 scales. We do this by generating 
                // numbers until these contstraints are satisified.
                while(  (nativeHeight == 0 && nativeWidth == 0) ||
                        (nativeWidth > 10 * nativeHeight) || 
                        (nativeHeight > 10 * nativeWidth) )
                {
                    GenerateRandomNumbers(out nativeHeight, out nativeWidth, MAX_ORIGINAL_DIMENSIONS);                    
                }

                double scale = Scale(nativeWidth, nativeHeight);

                Assert.IsTrue( 0.00386 <= scale );
                Assert.IsTrue( scale <= 128 );

                Console.WriteLine("-----------------------------");

                Console.WriteLine("Input height: " + nativeHeight);
                Console.WriteLine("Input width: " + nativeWidth );

                int height;
                int width;

                _scaler.Scale(MAX_HEIGHT, MAX_WIDTH, nativeHeight, nativeWidth, out height, out width);

                Console.WriteLine("height: " + height);
                Console.WriteLine("width: " + width);

                Assert.IsTrue(height <= MAX_HEIGHT);
                Assert.IsTrue(width <= MAX_WIDTH);

                double originalScale = Scale(nativeWidth, nativeHeight);
                double outputScale = Scale(width, height);
                double percentDifference;

                Assert.IsTrue(ScalesAreWithinTolerance(originalScale, outputScale, PERCENT_TOLERANCE, out percentDifference));                

                scaleDistortions.Add(percentDifference);
            }

            double averageDistortion = CalculateAverage(scaleDistortions);
            Console.WriteLine("Average scale distortion: " + averageDistortion);

            Assert.IsTrue(averageDistortion < MAX_AVERAGE_SCALE_DISTORTION_PERCENTAGE);
        }

       

        [Test]
        public void TestScalableLargeNumbers()
        {
            const int MAX_WIDTH = 128;
            const int MAX_HEIGHT = 26;

            const int NATIVE_WIDTH = 268904092;
            const int NATIVE_HEIGHT = 1537626822;

            int height, width;
            _scaler.Scale(MAX_HEIGHT, MAX_WIDTH, NATIVE_HEIGHT, NATIVE_WIDTH, out height, out width);

            Console.WriteLine("height: " + height);
            Console.WriteLine("width: " + width);

            Assert.IsTrue(height <= MAX_HEIGHT);
            Assert.IsTrue(width <= MAX_WIDTH);

            double originalScale = Scale(NATIVE_WIDTH, NATIVE_HEIGHT);
            double outputScale = Scale(width, height);

            Assert.IsTrue(ScalesAreWithinTolerance(originalScale, outputScale));
        }

         

        [Test]
        public void TestRandomNumbers()
        {
            const int MAX_WIDTH = 128;
            const int MAX_HEIGHT = 26;

            const int NATIVE_WIDTH = 8101;
            const int NATIVE_HEIGHT = 249;

            int height, width;
            _scaler.Scale(MAX_HEIGHT, MAX_WIDTH, NATIVE_HEIGHT, NATIVE_WIDTH, out height, out width);

            Console.WriteLine("height: " + height);
            Console.WriteLine("width: " + width);

            Assert.IsTrue(height <= MAX_HEIGHT);
            Assert.IsTrue(width <= MAX_WIDTH);

            double originalScale = Scale(NATIVE_WIDTH, NATIVE_HEIGHT);
            double outputScale = Scale(width, height);

            Assert.IsTrue(ScalesAreWithinTolerance(originalScale, outputScale));

        }

        private static double CalculateAverage(ICollection<double> numbers)
        {
            int count = numbers.Count;
            double sum = 0;

            foreach (double number in numbers)
            {
                sum += number;
            }

            return sum / count;
        }

        private static void GenerateRandomNumbers(out int height, out int width, int max)
        {
            // The height and width will be random.
            int ticks = Math.Abs((int)DateTime.Now.Ticks);
            height = Math.Abs(new Random(ticks).Next(max));
            width = Math.Abs(new Random(height).Next(max));
        }
 
        /// <summary>
        /// Compare two scale values, write a message to the console if they differ by more than 10 percent.
        /// </summary>
        /// <param name="originalScale"></param>
        /// <param name="newScale"></param>
        /// <returns></returns>
        private static bool ScalesAreWithinTolerance(double originalScale, double newScale)
        {
            const int PERCENT_TOLERANCE = 10;
            double percentDifference;
            return ScalesAreWithinTolerance(originalScale, newScale, PERCENT_TOLERANCE, out percentDifference);           
        }

        /// <summary>
        /// Compare two scale values, write a message to the console if they differ by more than a specified percent tolerance.
        /// </summary>
        /// <param name="originalScale"></param>
        /// <param name="newScale"></param>
        /// <param name="percentTolerance"></param>
        /// <param name="percentDifference"></param>
        /// <returns></returns>
        private static bool ScalesAreWithinTolerance(double originalScale, double newScale, int percentTolerance, out double percentDifference)
        {
            bool isNear = IsNear(originalScale, newScale, percentTolerance, out percentDifference);

            if (!isNear)
            {
                Console.WriteLine(
                    String.Format("The new scale {0} was not close enough to the original scale {1}. Percent diff: {2}",
                                  newScale, originalScale, percentDifference));
            }

            return isNear;
        }

        /// <summary>
        /// Are two doubles "near" one another?
        /// </summary>
        /// <param name="originalScale"></param>
        /// <param name="newScale"></param>
        /// <param name="percentTolerance"></param>
        /// <param name="proximity"></param>
        /// <returns></returns>
        private static bool IsNear(double originalScale, double newScale, int percentTolerance, out double proximity)
        {
            // calculate percentage difference
            double scaleDiff = Math.Abs(newScale - originalScale);
            double percentDiff = (scaleDiff/originalScale)*100;

            proximity = percentDiff;

            return proximity <= percentTolerance;
        }

        private static double Scale(int width, int height)
        {
            double h = height;
            double w = width;

            double scale = w/h;

            return scale;
        }

    }
}
