using Autofac;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Core.Logging;
using FirstLook.Merchandising.DomainModel.Marketing.Commands;
using FirstLook.Merchandising.DomainModel.Marketing.Photos;
using FirstLook.Pricing.DomainModel;
using NUnit.Framework;
using ICache = FirstLook.Common.Core.ICache;

namespace PricingDomainModel_Test.Photos
{
    [TestFixture]
    public class PhotoVehiclePreferenceTests
    {
        private string _ownerHandle = "a";
        private string _vehicleHandle = "b";


        [Test]
        public void CreateGetCommand()
        {
            ResetTestConditions();

            GetPhotoVehiclePreference command = new GetPhotoVehiclePreference(_ownerHandle, _vehicleHandle);

            Assert.IsNotNull(command);
        }


        [Test]
        public void RunGetCommand()
        {
            ResetTestConditions();

            GetPhotoVehiclePreference command = new GetPhotoVehiclePreference(_ownerHandle, _vehicleHandle);

            AbstractCommand.DoRun(command);

            Assert.IsNotNull(command.Preference);
        }

        [Test]
        public void RunSetCommand()
        {
            ResetTestConditions();

            bool isDisplayed = false;

            SetPhotoVehiclePreference setCommand = new SetPhotoVehiclePreference(_ownerHandle, _vehicleHandle, isDisplayed, "me");
            AbstractCommand.DoRun(setCommand);

            GetPhotoVehiclePreference getCommand = new GetPhotoVehiclePreference(_ownerHandle, _vehicleHandle);
            AbstractCommand.DoRun(getCommand);

            Assert.IsFalse(getCommand.Preference.IsDisplayed);

        }



        private static void ResetTestConditions()
        {
            ResetRegistryWithMockDataAccess();
            ResetMockDataStore();
        }

        private static void ResetMockDataStore()
        {
            PhotoVehiclePreferenceMockDataAccess.Clear();
        }

        private static void ResetRegistryWithMockDataAccess()
        {
            // Setup registry to use mock data access.
            var builder = new ContainerBuilder();
            builder.RegisterType<PhotoVehiclePreferenceMockDataAccess>().As<IPhotoVehiclePreferenceDataAccess>();
            builder.RegisterType<DotNetCacheWrapper>().As<ICache>();
            builder.RegisterType<HealthMonitoringWrapper>().As<ILogger>();
            Registry.RegisterContainer(builder.Build());
        }
    }
}
