﻿using System;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types.TransferObjects;
using NUnit.Framework;

namespace PricingDomainModel_Test.ConsumerHighlights.UnitTests
{
    /// <summary>
    /// Tests on the different types of consumer highlights.
    /// </summary>
    [TestFixture]
    public class ConsumerHighlightsUnitTests
    {
        /// <summary>
        /// Test that circle rating highlights behave as expected - i.e. that they cannot be deleted, edited, etc.
        /// </summary>
        [Test]
        [ExpectedException(typeof(NotSupportedException))]
        public void CircleRatingHighlightTests()
        {
            const int    highlightId  = 1;
            const int    preferenceId = 2;
            const bool   displayed    = true;
            const int    rank         = 3;
            const double value        = 3.5;
            const string description  = "Circle rating description";
            DateTime     created      = DateTime.Now;
            const int    sourceRowId  = 5;

            CircleRatingHighlightTO highlight = new CircleRatingHighlightTO(highlightId, preferenceId, rank, displayed, 
                                                                            value, description, sourceRowId, created);

            // General consumer highlight fields.
            Assert.AreEqual(highlightId,  highlight.ConsumerHighlightId);
            Assert.AreEqual(preferenceId, highlight.VehiclePreferenceId);
            Assert.AreEqual(displayed,    highlight.IsDisplayed);
            Assert.AreEqual(rank,         highlight.Rank);
            Assert.AreEqual(false,        highlight.CanDelete);
            Assert.AreEqual(false,        highlight.CanEdit);
            Assert.AreEqual(false,        highlight.IsDeleted);
            Assert.IsNotNull(highlight.ProviderSourceRowId);
            Assert.AreEqual(sourceRowId, highlight.ProviderSourceRowId.Value);

            Assert.AreEqual(HighlightType.CircleRating,         highlight.Provider);
            Assert.AreEqual(HighlightSectionType.CircleRatings, highlight.HighlightSection);

            // Circle rating highlight-specific fields.
            Assert.AreEqual(value, highlight.RatingValue);
            Assert.AreEqual(description, highlight.RatingDescription);
            Assert.AreEqual(created, highlight.CreateDate);

            // Should throw an exception.
            highlight.IsDeleted = true;
        }

        /// <summary>
        /// Test that free text highlights behave as expected - i.e. that they can be deleted, edited, etc.
        /// </summary>
        [Test]
        public void FreeTextHighlightTests()
        {
            const int    highlightId  = 1;
            const int    preferenceId = 2;
            const bool   displayed    = true;
            const int    rank         = 3;
            const string text         = "Free text highlight";

            FreeTextHighlightTO highlight = new FreeTextHighlightTO(highlightId, preferenceId, rank, displayed, text);

            // General consumer highlight fields.
            Assert.AreEqual(highlightId,  highlight.ConsumerHighlightId);
            Assert.AreEqual(preferenceId, highlight.VehiclePreferenceId);
            Assert.AreEqual(displayed,    highlight.IsDisplayed);
            Assert.AreEqual(rank,         highlight.Rank);            
            Assert.AreEqual(true,         highlight.CanDelete);
            Assert.AreEqual(true,         highlight.CanEdit);
            Assert.AreEqual(false,        highlight.IsDeleted);            
            highlight.IsDeleted = true;            
            Assert.AreEqual(true,         highlight.IsDeleted);
            Assert.AreEqual(null,         highlight.ProviderSourceRowId);

            Assert.AreEqual(HighlightType.FreeText, highlight.Provider);
            Assert.AreEqual(HighlightSectionType.FreeText, highlight.HighlightSection);

            // Free text highlight-specific fields.
            Assert.AreEqual(text, highlight.Text);
        }

        /// <summary>
        /// Test that snippet highlights behave as expected - i.e. that they cannot be deleted, edited, etc.
        /// </summary>
        [Test]
        [ExpectedException(typeof(NotSupportedException))]
        public void SnippetHighlightTests()
        {
            const int    highlightId  = 1;
            const int    preferenceId = 2;
            const bool   displayed    = true;
            const int    rank         = 3;            
            DateTime     created      = DateTime.Now;
            const int    sourceRowId  = 5;
            const int    sourceId     = 6;
            const string sourceName   = "Snippet source";
            const string snippet      = "Snippet highlight";

            SnippetHighlightTO highlight = new SnippetHighlightTO(highlightId, preferenceId, rank, displayed, 
                                                                  sourceRowId, sourceId, sourceName, snippet, created);

            // General consumer highlight fields.
            Assert.AreEqual(highlightId,  highlight.ConsumerHighlightId);
            Assert.AreEqual(preferenceId, highlight.VehiclePreferenceId);
            Assert.AreEqual(displayed,    highlight.IsDisplayed);
            Assert.AreEqual(rank,         highlight.Rank);
            Assert.AreEqual(false,        highlight.CanDelete);
            Assert.AreEqual(false,        highlight.CanEdit);
            Assert.AreEqual(false,        highlight.IsDeleted);
            Assert.IsNotNull(highlight.ProviderSourceRowId);
            Assert.AreEqual(sourceRowId, highlight.ProviderSourceRowId.Value);

            Assert.AreEqual(HighlightType.Snippet,         highlight.Provider);
            Assert.AreEqual(HighlightSectionType.Snippets, highlight.HighlightSection);

            // Snippet highlight-specific fields.
            Assert.AreEqual(sourceId,     highlight.SnippetSourceId);
            Assert.AreEqual(sourceName,   highlight.SnippetSourceName);
            Assert.AreEqual(snippet,      highlight.Snippet);
            Assert.AreEqual(created,      highlight.CreateDate);

            // Should throw an exception.
            highlight.IsDeleted = true;
        }

        /// <summary>
        /// Test that vehicle history highlights behave as expected - i.e. that they cannot be deleted, edited, etc.
        /// </summary>
        [Test]
        [ExpectedException(typeof(NotSupportedException))]
        public void VehicleHistoryHighlightTests()
        {
            const int    highlightId  = 1;
            const int    preferenceId = 2;
            const bool   displayed    = true;
            const int    rank         = 3;                                                
            DateTime     expiration   = DateTime.Now.AddYears(1);
            const string text         = "Vehicle history text";
            const string key          = "Key";
                 
            VehicleHistoryHighlightTO highlight = new VehicleHistoryHighlightTO(highlightId, preferenceId, rank, 
                                                                                displayed, expiration, 
                                                                                HighlightType.AutoCheck, text, key);

            // General consumer highlight fields.
            Assert.AreEqual(highlightId,  highlight.ConsumerHighlightId);
            Assert.AreEqual(preferenceId, highlight.VehiclePreferenceId);
            Assert.AreEqual(displayed,    highlight.IsDisplayed);
            Assert.AreEqual(rank,         highlight.Rank);
            Assert.AreEqual(false,        highlight.CanDelete);
            Assert.AreEqual(false,        highlight.CanEdit);
            Assert.AreEqual(false,        highlight.IsDeleted);
            Assert.AreEqual(null,         highlight.ProviderSourceRowId);

            Assert.AreEqual(HighlightType.AutoCheck,             highlight.Provider);
            Assert.AreEqual(HighlightSectionType.VehicleHistory, highlight.HighlightSection);            

            // Vehicle history report highlight-specific fields.
            Assert.AreEqual(text,       highlight.Text);
            Assert.AreEqual(expiration, highlight.ExpirationDate);

            // Create another highlight from a different provider and check it.
            highlight = new VehicleHistoryHighlightTO(highlightId, preferenceId, rank, displayed, expiration,
                                                      HighlightType.CarFax, text, key);
            Assert.AreEqual(HighlightType.CarFax, highlight.Provider);

            // Should throw an exception.
            highlight.IsDeleted = true;
        }

        /// <summary>
        /// Test tat an exception is thrown when trying to create a vehicle history report highlight with a provider 
        /// type that is not AutoCheck or CarFax.
        /// </summary>
        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void VehicleHistoryCreationTest()
        {
            new VehicleHistoryHighlightTO(0, 0, 0, true, DateTime.Now, HighlightType.Snippet, "", "");    
        }
    }
}
