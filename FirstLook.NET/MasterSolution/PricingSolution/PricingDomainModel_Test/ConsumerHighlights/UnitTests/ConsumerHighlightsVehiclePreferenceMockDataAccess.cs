using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types.TransferObjects;

namespace PricingDomainModel_Test.ConsumerHighlights.UnitTests
{
    /// <summary>
    /// Mock of consumer highlights vehicle preference data access.
    /// </summary>
    public class ConsumerHighlightsVehiclePreferenceMockDataAccess : IConsumerHighlightsVehiclePreferenceDataAccess
    {
        /// <summary>
        /// OwnerHandle;VehicleHandle -> DTO
        /// </summary>
        private static readonly IDictionary<string, ConsumerHighlightsVehiclePreferenceTO> _preferences
            = new Dictionary<string, ConsumerHighlightsVehiclePreferenceTO>();

        private static int _maxID = 1;

        private static string CreateKey(string ownerHandle, string vehicleHandle)
        {
            return ownerHandle + ";" + vehicleHandle;
        }

        #region Implementation of IConsumerHighlightsVehiclePreferenceDataAccess

        /// <summary>
        /// Check if vehicle preferences exist in our local dictionary for the given owner and vehicle handles.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>
        /// <returns>True if preferences exist, false otherwise.</returns>
        public bool Exists(string ownerHandle, string vehicleHandle)
        {
            string key = CreateKey(ownerHandle, vehicleHandle);
            return _preferences.ContainsKey(key);
        }

        /// <summary>
        /// Get a vehicle preference transfer object for the give owner and vehicle handles if one exists in our 
        /// dictionary. Throws an exception if preferences don't exist.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>
        /// <returns>Vehicle preference transfer object.</returns>
        public ConsumerHighlightsVehiclePreferenceTO Fetch(string ownerHandle, string vehicleHandle)
        {            
            string key = CreateKey(ownerHandle, vehicleHandle);
            
            if (_preferences.ContainsKey(key))
            {
                return _preferences[key];
            }
            
            throw new ApplicationException("No consumer highlights vehicle preference could be found with the specified handles.");
        }

        /// <summary>
        /// Insert the given preference into the local dicitonary.
        /// </summary>
        /// <param name="preference">Preference to hold on to.</param>
        /// <param name="userName">Username. Ignored.</param>
        public void Insert(ConsumerHighlightsVehiclePreferenceTO preference, string userName)
        {
            preference.ConsumerHighlightVehiclePreferenceId = _maxID;

            // Construct key and increment id.
            _maxID++;
            string key = CreateKey(preference.OwnerHandle, preference.VehicleHandle);

            // Save preference dto.
            _preferences.Add(key, preference);
        }

        /// <summary>
        /// Update the given preference in our local dictionary.
        /// </summary>
        /// <param name="preference">Preference to update.</param>
        /// <param name="userName">Username. Ignored.</param>
        public void Update(ConsumerHighlightsVehiclePreferenceTO preference, string userName)
        {
            if (!Exists(preference.OwnerHandle, preference.VehicleHandle))
            {
                throw new ApplicationException("Does not exist.");
            }

            string key = CreateKey(preference.OwnerHandle, preference.VehicleHandle);

            _preferences[key] = preference;
        }

        /// <summary>
        /// Delete the given preference from our local dictionary.
        /// </summary>
        /// <param name="preference">Preference to delete.</param>
        /// <param name="userName">Username. Ignored.</param>
        public void Delete(ConsumerHighlightsVehiclePreferenceTO preference, string userName)
        {            
            if (Exists(preference.OwnerHandle, preference.VehicleHandle))
            {
                string key = CreateKey(preference.OwnerHandle, preference.VehicleHandle);
                _preferences.Remove(key);
            }            
        }

        /// <summary>
        /// Get all vehicle history report highlights for the owner and vehicle with the given handles. Will get the 
        /// highlights from both Carfax and AutoCheck.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>
        /// <returns>Vehicle history report highlights.</returns>
        public List<VehicleHistoryHighlightTO> GetVehicleHistoryHighlights(string ownerHandle, string vehicleHandle)
        {
            List<VehicleHistoryHighlightTO> vehicleHistoryHighlights = new List<VehicleHistoryHighlightTO>();
            
            vehicleHistoryHighlights.AddRange(GetCarfaxHighlights(ownerHandle, vehicleHandle, 0));
            vehicleHistoryHighlights.AddRange(GetAutoCheckHighlights(ownerHandle, vehicleHandle, 0));

            return vehicleHistoryHighlights;
        }

        /// <summary>
        /// Get the Carfax report highlights for the owner and vehicle with the given handles.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>
        /// <param name="startRank">Starting rank that should be applied to retrieved highlights.</param>
        /// <returns>List of Carfax report items as consumer highlights transfer objects.</returns>
        public List<VehicleHistoryHighlightTO> GetCarfaxHighlights(string ownerHandle, string vehicleHandle, int startRank)
        {
            List<VehicleHistoryHighlightTO> carfaxHighlights = new List<VehicleHistoryHighlightTO>();

            carfaxHighlights.Add(new VehicleHistoryHighlightTO(0, 0, 1, true, DateTime.Now.AddYears(1), HighlightType.CarFax, "Carfax Highlight 1", HighlightType.CarFax + ":1"));
            carfaxHighlights.Add(new VehicleHistoryHighlightTO(0, 0, 2, true, DateTime.Now.AddYears(1), HighlightType.CarFax, "Carfax Highlight 2", HighlightType.CarFax + ":2"));
            
            return carfaxHighlights;
        }

        /// <summary>
        /// Get the Autocheck report highlights for the owner and vehicle with the given handles.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>
        /// <param name="startRank">Starting rank that should be applied to retrieved highlights.</param>
        /// <returns>List of AutoCheck report items as consumer highlight transfer objects.</returns>
        public List<VehicleHistoryHighlightTO> GetAutoCheckHighlights(string ownerHandle, string vehicleHandle, int startRank)
        {
            List<VehicleHistoryHighlightTO> carfaxHighlights = new List<VehicleHistoryHighlightTO>();

            carfaxHighlights.Add(new VehicleHistoryHighlightTO(0, 0, 1, true, DateTime.Now.AddYears(1), HighlightType.AutoCheck, "AutoCheck Highlight 1", HighlightType.AutoCheck + ":1"));
            carfaxHighlights.Add(new VehicleHistoryHighlightTO(0, 0, 2, true, DateTime.Now.AddYears(1), HighlightType.AutoCheck, "AutoCheck Highlight 2", HighlightType.AutoCheck + ":2"));

            return carfaxHighlights;
        }

        /// <summary>
        /// Get the marketing text snippets (aka 'Expert Reviews & Awards') for the owner and vehicle with the given
        /// handles.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>        
        /// <returns>List of snippets as consumer highlight transfer objects.</returns>
        public List<SnippetHighlightTO> FetchSnippets(string ownerHandle, string vehicleHandle)
        {
            List<SnippetHighlightTO> snippetHighlights = new List<SnippetHighlightTO>();

            snippetHighlights.Add(new SnippetHighlightTO(0, 0, 1, true, 0, 0, "Snippet Source", "Snippet 1", DateTime.Now));
            snippetHighlights.Add(new SnippetHighlightTO(0, 0, 2, true, 0, 0, "Snippet Source", "Snippet 2", DateTime.Now));

            return snippetHighlights;
        }

        /// <summary>
        /// Get the JD Power circle rating highlights for the owner and vehicle with the given handles.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>        
        /// <returns>List of JD Power circle rating items as consumer highlight transfer objects.</returns>
        public List<CircleRatingHighlightTO> FetchCircleRatings(string ownerHandle, string vehicleHandle)
        {
            List<CircleRatingHighlightTO> circleRatingHighlights = new List<CircleRatingHighlightTO>();

            circleRatingHighlights.Add(new CircleRatingHighlightTO(0, 0, 1, true, 3.5, "Circle Rating 1", 0, DateTime.Now));
            circleRatingHighlights.Add(new CircleRatingHighlightTO(0, 0, 2, true, 3.5, "Circle Rating 2", 0, DateTime.Now));

            return circleRatingHighlights;
        }

        #endregion

        public static void Clear()
        {
            _preferences.Clear();
        }          
    }
}