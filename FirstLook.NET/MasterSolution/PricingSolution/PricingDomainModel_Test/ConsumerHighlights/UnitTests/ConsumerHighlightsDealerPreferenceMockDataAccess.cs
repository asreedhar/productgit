﻿using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types.TransferObjects;

namespace PricingDomainModel_Test.ConsumerHighlights.UnitTests
{
    public class ConsumerHighlightsDealerPreferenceMockDataAccess : IConsumerHighlightsDealerPreferenceDataAccess
    {
        // Status tracking.
        public static bool CreateCalled;
        public static bool ExistsCalled;
        public static bool FetchCalled;        
        public static bool SaveCalled;
        public static bool ShouldExist;

        /// <summary>
        /// Reset the status tracking variables.
        /// </summary>
        public static void Reset()
        {
            CreateCalled = false;
            ExistsCalled = false;
            FetchCalled  = false;
            SaveCalled   = false;
            ShouldExist  = false;
        }

        /// <summary>
        /// Dealer preference to use in calls to this data access object.
        /// </summary>
        internal ConsumerHighlightsDealerPreferenceTO Preference
        {
            get
            {
                ConsumerHighlightsDealerPreferenceTO preference = new ConsumerHighlightsDealerPreferenceTO();
                preference.MinimumJdPowerCircleRating = 3.5;

                preference.HighlightSections =
                    new List<HighlightSection> {
                    new HighlightSection() {Name = "", Rank = 1, Type = HighlightSectionType.CircleRatings},
                    new HighlightSection() {Name = "", Rank = 2, Type = HighlightSectionType.FreeText},
                    new HighlightSection() {Name = "", Rank = 3, Type = HighlightSectionType.Snippets},
                    new HighlightSection() {Name = "", Rank = 4, Type = HighlightSectionType.VehicleHistory}
                };

                preference.DisplayedSnippetSources =
                    new List<SnippetSource> {
                    new SnippetSource {HasCopyright = true, IsDisplayed = true, Name = "", Rank = 1, Type = SnippetSourceType.Automedia},
                    new SnippetSource {HasCopyright = true, IsDisplayed = true, Name = "", Rank = 2, Type = SnippetSourceType.AutomobileMagazine},
                    new SnippetSource {HasCopyright = true, IsDisplayed = true, Name = "", Rank = 3, Type = SnippetSourceType.BmwUsa},
                    new SnippetSource {HasCopyright = true, IsDisplayed = true, Name = "", Rank = 4, Type = SnippetSourceType.CarAndDriver},
                    new SnippetSource {HasCopyright = true, IsDisplayed = true, Name = "", Rank = 5, Type = SnippetSourceType.ConsumerReports},
                    new SnippetSource {HasCopyright = true, IsDisplayed = true, Name = "", Rank = 6, Type = SnippetSourceType.Edmunds},
                    new SnippetSource {HasCopyright = true, IsDisplayed = true, Name = "", Rank = 7, Type = SnippetSourceType.JdPower},
                    new SnippetSource {HasCopyright = true, IsDisplayed = true, Name = "", Rank = 8, Type = SnippetSourceType.KBB},
                    new SnippetSource {HasCopyright = true, IsDisplayed = true, Name = "", Rank = 9, Type = SnippetSourceType.Medley},
                    new SnippetSource {HasCopyright = true, IsDisplayed = true, Name = "", Rank = 10, Type = SnippetSourceType.MotorTrend},
                    new SnippetSource {HasCopyright = true, IsDisplayed = true, Name = "", Rank = 11, Type = SnippetSourceType.NewCarTestDrive},
                    new SnippetSource {HasCopyright = true, IsDisplayed = true, Name = "", Rank = 12, Type = SnippetSourceType.RoadAndTrack},
                    new SnippetSource {HasCopyright = true, IsDisplayed = true, Name = "", Rank = 13, Type = SnippetSourceType.TheCarConnection}
                };

                preference.HiddenSnippetSources = new List<SnippetSource>();

                preference.DisplayedVehicleHistoryReportSources = 
                    new List<VehicleHistoryReportSource> {
                    new VehicleHistoryReportSource {Description = "", IsDisplayed = true, VehicleHistoryProviderId = 1, ReportInspectionId = 1},
                    new VehicleHistoryReportSource {Description = "", IsDisplayed = true, VehicleHistoryProviderId = 1, ReportInspectionId = 2},
                    new VehicleHistoryReportSource {Description = "", IsDisplayed = true, VehicleHistoryProviderId = 1, ReportInspectionId = 3},
                    new VehicleHistoryReportSource {Description = "", IsDisplayed = true, VehicleHistoryProviderId = 1, ReportInspectionId = 4},
                    new VehicleHistoryReportSource {Description = "", IsDisplayed = true, VehicleHistoryProviderId = 2, ReportInspectionId = 1},
                    new VehicleHistoryReportSource {Description = "", IsDisplayed = true, VehicleHistoryProviderId = 2, ReportInspectionId = 2},
                    new VehicleHistoryReportSource {Description = "", IsDisplayed = true, VehicleHistoryProviderId = 2, ReportInspectionId = 3},
                    new VehicleHistoryReportSource {Description = "", IsDisplayed = true, VehicleHistoryProviderId = 2, ReportInspectionId = 4},
                    };
                
                return preference;
            }
        }

        /// <summary>
        /// Create a new set of preferences for the owner with the given handle.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <returns>Dealer preferences object.</returns>
        public ConsumerHighlightsDealerPreferenceTO Create(string ownerHandle)
        {
            CreateCalled = true;

            ConsumerHighlightsDealerPreferenceTO preference = Preference;
            preference.OwnerHandle = ownerHandle;
            return preference;
        }

        /// <summary>
        /// Do preferences exist for the owner with the given handle?
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <returns>True if preferences exist, false otherwise.</returns>
        public bool Exists(string ownerHandle)
        {
            ExistsCalled = true;

            return ShouldExist;
        }

        /// <summary>
        /// Fetch the consumer highglights dealer preferences for the owner with the given handle.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <returns>Dealer preferences object.</returns>
        public ConsumerHighlightsDealerPreferenceTO Fetch(string ownerHandle)
        {
            FetchCalled = true;

            ConsumerHighlightsDealerPreferenceTO preference = Preference;
            preference.OwnerHandle = ownerHandle;
            return preference;
        }

        /// <summary>
        /// Save the dealer preferences.
        /// </summary>        
        /// <param name="preference">Consumer highlights dealer preferences.</param>
        public void Save(ConsumerHighlightsDealerPreferenceTO preference)
        {
            SaveCalled = true;

            return;
        }
    }
}
