using System;
using System.Collections.Generic;
using Autofac;
using Csla;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types.TransferObjects;
using NUnit.Framework;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights;

namespace PricingDomainModel_Test.ConsumerHighlights.UnitTests
{
    /// <summary>
    /// Unit tests on consumer highlight vehicle preferences.
    /// </summary>
    [TestFixture]
    public class ConsumerHighlightsVehiclePreferenceUnitTests
    {
        /// <summary>
        /// Setup the test conditions prior to each test.
        /// </summary>
        [SetUp]
        public void ResetTestConditions()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<ConsumerHighlightsVehiclePreferenceMockDataAccess>().As<IConsumerHighlightsVehiclePreferenceDataAccess>();
            builder.RegisterType<ConsumerHighlightsDealerPreferenceMockDataAccess>().As<IConsumerHighlightsDealerPreferenceDataAccess>();
            Registry.RegisterContainer(builder.Build());

            ConsumerHighlightsVehiclePreferenceMockDataAccess.Clear();
        }

        /// <summary>
        /// Test that trying to retrieve consumer highlight vehicle preferences throws an exception when an empty
        /// owner and vehicle handle are provided.
        /// </summary>
        [Test]
        [ExpectedException(typeof(DataPortalException))]
        public void GetVehiclePreferenceThrowsExceptionWithAnEmptyStringOwnerHandle()
        {            
            ConsumerHighlightsVehiclePreference preference = 
                ConsumerHighlightsVehiclePreference.GetConsumerHighlightsVehiclePreference(String.Empty, String.Empty);
            Assert.IsNotNull(preference);
        }

        /// <summary>
        /// Test that vehicle preferences are properly created.
        /// </summary>
        [Test]
        public void CreateVehiclePreference()
        {            
            string oh = Guid.NewGuid().ToString();
            string vh = Guid.NewGuid().ToString();
            ConsumerHighlightsVehiclePreference preference = ConsumerHighlightsVehiclePreference.CreateConsumerHighlightsVehiclePreference(oh, vh);

            Assert.IsNotNull(preference);
            Assert.IsTrue(preference.OwnerHandle == oh);
            Assert.IsTrue(preference.VehicleHandle == vh);
            Assert.IsTrue(preference.IsNew);
            Assert.IsTrue(preference.IsDirty);
        }

        /// <summary>
        /// Test that an exception is thrown when trying to fetch vehicle preferences that don't exist.
        /// </summary>
        [Test]
        [ExpectedException(typeof(DataPortalException))]
        public void FetchVehiclePreferenceThrowsExceptionWhenOneDoesNotExist()
        {
            string oh = Guid.NewGuid().ToString();
            string vh = Guid.NewGuid().ToString();
            ConsumerHighlightsVehiclePreference.GetConsumerHighlightsVehiclePreference(oh, vh);
        }

        /// <summary>
        /// Test that when calling 'GetOrCreate' with new owner and vehicle handles that a new preference is returned.
        /// </summary>
        [Test]
        public void GetOrCreateReturnsNewPreference()
        {
            string oh = Guid.NewGuid().ToString();
            string vh = Guid.NewGuid().ToString();
            ConsumerHighlightsVehiclePreference preference =
                ConsumerHighlightsVehiclePreference.GetOrCreateConsumerHighlightsPreference(oh, vh);

            Assert.IsNotNull(preference);
            Assert.IsTrue(preference.OwnerHandle == oh);
            Assert.IsTrue(preference.VehicleHandle == vh);
            Assert.IsTrue(preference.IsNew);
            Assert.IsTrue(preference.IsDirty);
        }

        /// <summary>
        /// Test that a newly saved preference gets its consumer highlight identifier set.
        /// </summary>
        [Test]
        public void NewlySavedPreferenceHasID()
        {
            string oh = Guid.NewGuid().ToString();
            string vh = Guid.NewGuid().ToString();

            ConsumerHighlightsVehiclePreference newPreference =
                ConsumerHighlightsVehiclePreference.CreateConsumerHighlightsVehiclePreference(oh, vh);

            // Save the preference.
            newPreference.Save();

            Assert.IsTrue(newPreference.ConsumerHighlightVehiclePreferenceId > 0);
            Console.WriteLine("Preference ID = " + newPreference.ConsumerHighlightVehiclePreferenceId);
        }

        /// <summary>
        /// Test that a newly created preference, when saved, is no longer new and can be fetched.
        /// </summary>
        [Test]
        public void NewlyCreatedPreferenceIsInitializedAndCanBeFound()
        {
            string oh = Guid.NewGuid().ToString();
            string vh = Guid.NewGuid().ToString();

            ConsumerHighlightsVehiclePreference newPreference = ConsumerHighlightsVehiclePreference.CreateConsumerHighlightsVehiclePreference(oh, vh);

            Assert.IsNotNull(newPreference);
            // Verify the properties of the preference were appropriately set.
            Assert.IsTrue(newPreference.OwnerHandle == oh);
            Assert.IsTrue(newPreference.VehicleHandle == vh);
            // Verify the csla properties are correct for the new preference.
            Assert.IsTrue(newPreference.IsNew);
            Assert.IsTrue(newPreference.IsDirty);

            // Save it...
            newPreference.Save();

            // Check the csla properties...
            Assert.IsFalse(newPreference.IsNew);
            Assert.IsFalse(newPreference.IsDirty);

            // Now get it....
            ConsumerHighlightsVehiclePreference foundPreference =
                ConsumerHighlightsVehiclePreference.GetConsumerHighlightsVehiclePreference(newPreference.OwnerHandle, newPreference.VehicleHandle);

            // Verify equality.
            Assert.AreEqual(newPreference.OwnerHandle, foundPreference.OwnerHandle);
            Assert.AreEqual(newPreference.VehicleHandle, foundPreference.VehicleHandle);
            Assert.IsTrue(foundPreference.IsDisplayed == newPreference.IsDisplayed);
            Assert.IsTrue(foundPreference.IncludeCarfaxOneOwnerIcon == newPreference.IncludeCarfaxOneOwnerIcon);
        }

        /// <summary>
        /// Test that vehicle preferences are properly put into a transfer object.
        /// </summary>
        [Test]
        public void ToTransferObject()
        {
            string oh = Guid.NewGuid().ToString();
            string vh = Guid.NewGuid().ToString();

            ConsumerHighlightsVehiclePreference preference = ConsumerHighlightsVehiclePreference.CreateConsumerHighlightsVehiclePreference(oh, vh);
            ConsumerHighlightsVehiclePreferenceTO transferObject = preference.ToTransferObject();

            Assert.AreEqual(preference.ConsumerHighlightVehiclePreferenceId, transferObject.ConsumerHighlightVehiclePreferenceId);
            Assert.AreEqual(preference.IsDisplayed, preference.IsDisplayed);
            Assert.AreEqual(preference.IncludeCarfaxOneOwnerIcon, preference.IncludeCarfaxOneOwnerIcon);
            Assert.AreEqual(preference.OwnerHandle, transferObject.OwnerHandle);
            Assert.AreEqual(preference.VehicleHandle, transferObject.VehicleHandle);
            Assert.AreEqual(preference.CircleRatingHighlights.Count, transferObject.CircleRatingHighlights.Count);
            Assert.AreEqual(preference.FreeTextHighlights.Count, transferObject.FreeTextHighlights.Count);
            Assert.AreEqual(preference.SnippetHighlights.Count, transferObject.SnippetHighlights.Count);
            Assert.AreEqual(preference.VehicleHistoryHighlights.Count, transferObject.VehicleHistoryHighlights.Count);            
        }         
        
        /// <summary>
        /// Test the vehicle preferences are properly set from a transfer object.
        /// </summary>
        [Test]
        public void FromTransferObject()
        {
            // Create a preference.
            string oh = Guid.NewGuid().ToString();
            string vh = Guid.NewGuid().ToString();

            ConsumerHighlightsVehiclePreference preference = 
                ConsumerHighlightsVehiclePreference.CreateConsumerHighlightsVehiclePreference(oh, vh);

            // Create a transfer object (swap the booleans).
            ConsumerHighlightsVehiclePreferenceTO to = 
                new ConsumerHighlightsVehiclePreferenceTO(123, oh, vh, !preference.IsDisplayed, 
                    !preference.IncludeCarfaxOneOwnerIcon, DateTime.Now, new List<HighlightSectionType>(), 
                    new List<CircleRatingHighlightTO>(), new List<FreeTextHighlightTO>(), 
                    new List<SnippetHighlightTO>(), new List<VehicleHistoryHighlightTO>());

            // Call FromTransferObject.
            preference.FromTransferObject(to);

            // Check for equality.
            Assert.AreEqual(to.ConsumerHighlightVehiclePreferenceId, preference.ConsumerHighlightVehiclePreferenceId);
            Assert.AreEqual(to.OwnerHandle, preference.OwnerHandle);
            Assert.AreEqual(to.VehicleHandle, preference.VehicleHandle);
            Assert.AreEqual(to.IsDisplayed, preference.IsDisplayed);
            Assert.AreEqual(to.IncludeCarfaxOneOwnerIcon, preference.IncludeCarfaxOneOwnerIcon);

            // Ensure the highlights match.            
            foreach (IConsumerHighlight highlight in preference.GetHighlights())
            {
                bool matchedHighlight = false;

                foreach (IConsumerHighlight highlightTO in to.GetHighlights())
                {
                    if (highlight.ConsumerHighlightId == highlightTO.ConsumerHighlightId)
                    {
                        matchedHighlight = true;
                        Assert.AreEqual(highlight.CanDelete,           highlightTO.CanDelete);
                        Assert.AreEqual(highlight.CanEdit,             highlightTO.CanEdit);
                        Assert.AreEqual(highlight.ConsumerHighlightId, highlightTO.ConsumerHighlightId);
                        Assert.AreEqual(highlight.VehiclePreferenceId, highlightTO.VehiclePreferenceId);                        
                        Assert.AreEqual(highlight.Provider,            highlightTO.Provider);
                        Assert.AreEqual(highlight.IsDeleted,           highlightTO.IsDeleted);
                        Assert.AreEqual(highlight.IsDisplayed,         highlightTO.IsDisplayed);
                        Assert.AreEqual(highlight.Key,                 highlightTO.Key);
                        Assert.AreEqual(highlight.CanDelete,           highlightTO.CanDelete);
                        Assert.AreEqual(highlight.Rank,                highlightTO.Rank);
                        break;
                    }
                }

                if (!matchedHighlight)
                {
                    Assert.Fail("Not all highlights were accounted for.");
                }
            }           
        }

        /// <summary>
        /// Test that a new preference can be saved.
        /// </summary>
        [Test]
        public void NewPreferenceCanBeSaved()
        {
            // create a preference
            string oh = Guid.NewGuid().ToString();
            string vh = Guid.NewGuid().ToString();

            ConsumerHighlightsVehiclePreference preference = 
                ConsumerHighlightsVehiclePreference.CreateConsumerHighlightsVehiclePreference(oh, vh);

            // Save the preference.
            preference.Save();

            // Can we now find the preference?
            ConsumerHighlightsVehiclePreference foundPreference =
                ConsumerHighlightsVehiclePreference.GetOrCreateConsumerHighlightsPreference(oh, vh);

            Assert.IsNotNull(foundPreference);
            Assert.AreEqual(oh, foundPreference.OwnerHandle);
            Assert.AreEqual(vh, foundPreference.VehicleHandle);
            Assert.IsTrue(foundPreference.ConsumerHighlightVehiclePreferenceId > 0);
        }

        /// <summary>
        /// Test that changing highlights is handled properly (basic).
        /// </summary>
        [Test]
        public void ChangingHighlightDirtiesThePreferenceBasic()
        {
            // Create a preference.
            string oh = Guid.NewGuid().ToString();
            string vh = Guid.NewGuid().ToString();

            ConsumerHighlightsVehiclePreference preference = 
                ConsumerHighlightsVehiclePreference.CreateConsumerHighlightsVehiclePreference(oh, vh);

            // Save the preference.
            preference.Save();

            // Can we now find the preference?
            ConsumerHighlightsVehiclePreference foundPreference =
                ConsumerHighlightsVehiclePreference.GetOrCreateConsumerHighlightsPreference(oh, vh);

            Assert.IsNotNull(foundPreference);
            Assert.AreEqual(oh, foundPreference.OwnerHandle);
            Assert.AreEqual(vh, foundPreference.VehicleHandle);
            Assert.IsTrue(foundPreference.ConsumerHighlightVehiclePreferenceId > 0);

            int hCount = foundPreference.HighlightsCount();

            Assert.IsFalse(foundPreference.IsDirty);

            foundPreference.CreateFreeTextHighlight(true, "asdf", hCount + 1);
            Assert.AreEqual(hCount + 1, foundPreference.HighlightsCount());

            Assert.IsTrue(foundPreference.IsDirty);
        }

        /// <summary>
        /// Test that changing highlights is handled properly (advanced).
        /// </summary>
        [Test]
        public void ChangingHighlightDirtiesThePreferenceAdvanced()
        {
            // Create a preference.
            string oh = Guid.NewGuid().ToString();
            string vh = Guid.NewGuid().ToString();

            ConsumerHighlightsVehiclePreference preference = 
                ConsumerHighlightsVehiclePreference.CreateConsumerHighlightsVehiclePreference(oh, vh);
            Assert.That(preference.IsNew);
            Assert.That(preference.IsDirty);

            // Save the preference.
            preference.Save();

            Assert.IsFalse(preference.IsNew);
            Assert.IsFalse(preference.IsDirty);

            // Get the preference
            ConsumerHighlightsVehiclePreference foundPreference =
                ConsumerHighlightsVehiclePreference.GetOrCreateConsumerHighlightsPreference(oh, vh);

            Assert.IsNotNull(foundPreference);
            Assert.AreEqual(oh, foundPreference.OwnerHandle);
            Assert.AreEqual(vh, foundPreference.VehicleHandle);
            Assert.That(foundPreference.ConsumerHighlightVehiclePreferenceId > 0);
            Assert.IsFalse(foundPreference.IsDirty);
            Assert.IsFalse(foundPreference.IsNew);

            int hCountBeforeDelete = foundPreference.HighlightsCount();

            foundPreference.CreateFreeTextHighlight(true, "asdf", hCountBeforeDelete + 1);
            
            // Creating a highlight should have dirtied the preference.
            Assert.That(foundPreference.IsDirty);
            Assert.IsFalse(foundPreference.IsNew);

            // Save again.
            foundPreference.Save();

            // The save should have made the preference clean again.
            Assert.IsFalse(foundPreference.IsDirty);
            Assert.IsFalse(foundPreference.IsNew);
            
            // The prefernce should have a single highlight now.
            Assert.That(foundPreference.HighlightsCount() == hCountBeforeDelete + 1);

            string key = foundPreference.FreeTextHighlights[0].Key;
            bool isdisplayed = foundPreference.FreeTextHighlights[0].IsDisplayed;
            string text = foundPreference.FreeTextHighlights[0].Text;
            int rank = foundPreference.FreeTextHighlights[0].Rank;
            foundPreference.UpdateHighlight(key, ! isdisplayed, text + "!!", rank + 1, false );

            // Updating the highlight should have dirtied the preference.
            Assert.That(foundPreference.IsDirty);
            Assert.IsFalse(foundPreference.IsNew);

            foundPreference.Save();

            // The save should have made the preference clean again.
            Assert.IsFalse(foundPreference.IsDirty);
            Assert.IsFalse(foundPreference.IsNew);
        }

        /// <summary>
        /// Test that new preferences can be updated.
        /// </summary>
        [Test]
        public void NewPreferenceCanBeUpdated()
        {            
            // get the preference?
            ConsumerHighlightsVehiclePreference preference = GetNewSavedPreference();

            Assert.IsNotNull(preference);
            Assert.IsTrue(preference.ConsumerHighlightVehiclePreferenceId > 0);
            
            // update the preference
            preference.IncludeCarfaxOneOwnerIcon = !preference.IncludeCarfaxOneOwnerIcon;
            preference.IsDisplayed = !preference.IsDisplayed;

            preference.Save();


            // get the preference again
            ConsumerHighlightsVehiclePreference foundPreference2 =
                ConsumerHighlightsVehiclePreference.GetOrCreateConsumerHighlightsPreference(preference.OwnerHandle, preference.VehicleHandle);

            // make sure it was saved as updated.
            Assert.IsNotNull(foundPreference2);
            Assert.AreEqual(preference.ConsumerHighlightVehiclePreferenceId, foundPreference2.ConsumerHighlightVehiclePreferenceId);
            Assert.AreEqual(preference.OwnerHandle, foundPreference2.OwnerHandle);
            Assert.AreEqual(preference.VehicleHandle, foundPreference2.VehicleHandle);
            Assert.AreEqual(preference.IsDisplayed, foundPreference2.IsDisplayed);
            Assert.AreEqual(preference.IncludeCarfaxOneOwnerIcon, foundPreference2.IncludeCarfaxOneOwnerIcon);
        }

        /// <summary>
        /// Test that preferences are deleted properly.
        /// </summary>
        [Test]
        public void PreferenceCanBeDeleted()
        {            
            ConsumerHighlightsVehiclePreference preference = GetNewSavedPreference();

            Assert.IsNotNull(preference);

            preference.MarkForDeletion();
            preference.Save();

            // Get or create the preference again (expect a created one).
            ConsumerHighlightsVehiclePreference foundPreference2 =
                ConsumerHighlightsVehiclePreference.GetOrCreateConsumerHighlightsPreference(preference.OwnerHandle, preference.VehicleHandle);

            // Make sure it is a new preference (not a fetched one).
            Assert.IsTrue(foundPreference2.IsNew);
            Assert.IsTrue(foundPreference2.IsDirty);

        }

        #region Utility Methods

        /// <summary>
        /// Create a new preference, save it, and then fetch it agai.
        /// </summary>
        /// <returns>Consumer highlight vehicle preferences.</returns>
        private static ConsumerHighlightsVehiclePreference GetNewSavedPreference()
        {
            string oh = Guid.NewGuid().ToString();
            string vh = Guid.NewGuid().ToString();

            ConsumerHighlightsVehiclePreference preference = ConsumerHighlightsVehiclePreference.CreateConsumerHighlightsVehiclePreference(oh, vh);

            // Save the preference.
            preference.Save();

            // Get the preference.
            ConsumerHighlightsVehiclePreference foundPreference =
                ConsumerHighlightsVehiclePreference.GetOrCreateConsumerHighlightsPreference(oh, vh);

            return foundPreference;
        }

        #endregion
    }
}
