﻿using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types.TransferObjects;

namespace PricingDomainModel_Test.ConsumerHighlights.UnitTests
{
    /// <summary>
    /// Mock of consumer highlights vehicle preferences.
    /// </summary>
    [Serializable]
    public class ConsumerHighlightsMockVehiclePreference : IConsumerHighlightsVehiclePreference
    {
        /// <summary>
        /// Vehicle preference identifier.
        /// </summary>
        public int ConsumerHighlightVehiclePreferenceId { get; set; }

        /// <summary>
        /// Owner handle.
        /// </summary>
        public string OwnerHandle { get; set; }

        /// <summary>
        /// Vehicle handle.
        /// </summary>
        public string VehicleHandle { get; set; }

        /// <summary>
        /// Are these vehicle preferences displayed?
        /// </summary>
        public bool IsDisplayed { get; set; }

        /// <summary>
        /// Should the "Carfax 1-Owner" icon be included?
        /// </summary>
        public bool IncludeCarfaxOneOwnerIcon { get; set; }

        /// <summary>
        /// Most recent date and time this 
        /// </summary>
        public DateTime ChangedDate { get; set; }

        /// <summary>
        /// Ordering of consumer highlight sections.
        /// </summary>
        public List<HighlightSectionType> SectionOrdering { get; set; }

        /// <summary>
        /// Get all consumer highlights of every type. The order in which each type is added to the enumeration is
        /// defined by the section ordering preferences.
        /// </summary>
        /// <returns>Enumeration of all highlights.</returns>
        public IEnumerable<IConsumerHighlight> GetHighlights()
        {
            foreach (HighlightSectionType sectionType in SectionOrdering)
            {
                switch (sectionType)
                {
                    case HighlightSectionType.CircleRatings:
                        foreach (CircleRatingHighlightTO to in CircleRatingHighlights)
                        {
                            yield return to;
                        }
                        break;

                    case HighlightSectionType.FreeText:
                        foreach (FreeTextHighlightTO to in FreeTextHighlights)
                        {
                            yield return to;
                        }
                        break;

                    case HighlightSectionType.Snippets:
                        foreach (SnippetHighlightTO to in SnippetHighlights)
                        {
                            yield return to;
                        }
                        break;

                    case HighlightSectionType.VehicleHistory:
                        foreach (VehicleHistoryHighlightTO to in VehicleHistoryHighlights)
                        {
                            yield return to;
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// Free text highlights.
        /// </summary>
        public List<FreeTextHighlightTO> FreeTextHighlights { get; set; }

        /// <summary>
        /// Vehicle history highlights.
        /// </summary>
        public List<VehicleHistoryHighlightTO> VehicleHistoryHighlights { get; set; }

        /// <summary>
        /// Snippet highlights.
        /// </summary>
        public List<SnippetHighlightTO> SnippetHighlights { get; set; }

        /// <summary>
        /// JDPower Circle rating highlights.
        /// </summary>
        public List<CircleRatingHighlightTO> CircleRatingHighlights { get; set; }
    }
}
