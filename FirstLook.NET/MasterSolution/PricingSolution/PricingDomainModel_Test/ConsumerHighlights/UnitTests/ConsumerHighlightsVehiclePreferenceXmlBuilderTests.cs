﻿using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights;
using NUnit.Framework;

namespace PricingDomainModel_Test.ConsumerHighlights.UnitTests
{
    /// <summary>
    /// Tests on the xml builder for consumer highlights vehicle preferences.
    /// </summary>
    [TestFixture]
    public class ConsumerHighlightsVehiclePreferenceXmlBuilderTests
    {
        /// <summary>
        /// Test that normal input to xml building returns the expected output.
        /// </summary>
        [Test]
        public void VehiclePreferenceXmlTest()
        {            
            ConsumerHighlightsMockVehiclePreference preference =
                UtilityMethods.ParseVehiclePreferencesFromXml("ConsumerHighlightsVehiclePreferences_AllDisplayed_In.xml");
            string preferenceXml = UtilityMethods.ParseXml("ConsumerHighlightsVehiclePreferences_AllDisplayed_Out.xml");
                        
            string xml = ConsumerHighlightsVehiclePreferenceXmlBuilder.AsXml(preference, false);

            Assert.AreEqual(preferenceXml, xml);
        }

        /// <summary>
        /// Test that the xml builder returns an empty string for bad inputs.
        /// </summary>
        [Test]
        public void BadInputTest()
        {
            // Null preferences should return empty string.
            Assert.AreEqual(string.Empty, ConsumerHighlightsVehiclePreferenceXmlBuilder.AsXml(null, false));

            ConsumerHighlightsMockVehiclePreference preference =
                UtilityMethods.ParseVehiclePreferencesFromXml("ConsumerHighlightsVehiclePreferences_AllDisplayed_In.xml");

            // Empty section ordering should return empty string.
            preference.SectionOrdering.Clear();
            Assert.AreEqual(string.Empty, ConsumerHighlightsVehiclePreferenceXmlBuilder.AsXml(preference, false));

            // Null section ordering should return empty string.
            preference.SectionOrdering = null;
            Assert.AreEqual(string.Empty, ConsumerHighlightsVehiclePreferenceXmlBuilder.AsXml(preference, false));
        }

        /// <summary>
        /// Test that the xml that is built from vehicle preferences does not have the circle rating highlights node
        /// when there are no circle rating highlights.
        /// </summary>
        [Test]
        public void EmptyCircleRatingHighlightTest()
        {
            ConsumerHighlightsMockVehiclePreference preference =
                UtilityMethods.ParseVehiclePreferencesFromXml("ConsumerHighlightsVehiclePreferences_AllDisplayed_In.xml");
            string preferenceXml = UtilityMethods.ParseXml("ConsumerHighlightsVehiclePreferences_AllDisplayed_Out.xml");

            // Remove the circle rating highlights node from it.                        
            int index = preferenceXml.IndexOf("<circleratinghighlights");
            int length = preferenceXml.LastIndexOf("</circleratinghighlights>") - index + "</circleratinghighlights>".Length;
            preferenceXml = preferenceXml.Remove(index, length);

            // Test empty circle rating highlights.
            preference.CircleRatingHighlights.Clear();
            string xml = ConsumerHighlightsVehiclePreferenceXmlBuilder.AsXml(preference, false);
            Assert.AreEqual(preferenceXml, xml);

            // Test null circle rating highlights.
            preference.CircleRatingHighlights = null;
            xml = ConsumerHighlightsVehiclePreferenceXmlBuilder.AsXml(preference, false);
            Assert.AreEqual(preferenceXml, xml);
        }

        /// <summary>
        /// Test that the xml that is built from vehicle preferences does not have the free text highlights node when 
        /// there are no free text highlights.
        /// </summary>
        [Test]
        public void EmptyFreeTextHighlightTest()
        {
            ConsumerHighlightsMockVehiclePreference preference =
                UtilityMethods.ParseVehiclePreferencesFromXml("ConsumerHighlightsVehiclePreferences_AllDisplayed_In.xml");
            string preferenceXml = UtilityMethods.ParseXml("ConsumerHighlightsVehiclePreferences_AllDisplayed_Out.xml");

            // Remove the free text highlights node from it.
            int index = preferenceXml.IndexOf("<freetexthighlights");
            int length = preferenceXml.LastIndexOf("</freetexthighlights>") - index + "</freetexthighlights>".Length;
            preferenceXml = preferenceXml.Remove(index, length);

            // Test empty free text highlights.
            preference.FreeTextHighlights.Clear();
            string xml = ConsumerHighlightsVehiclePreferenceXmlBuilder.AsXml(preference, false);
            Assert.AreEqual(preferenceXml, xml);

            // Test null free text highlights.
            preference.FreeTextHighlights = null;
            xml = ConsumerHighlightsVehiclePreferenceXmlBuilder.AsXml(preference, false);
            Assert.AreEqual(preferenceXml, xml);
        }

        /// <summary>
        /// Test that the xml that is built from vehicle preferences does not have the snippet highlights node when 
        /// there are no snippet highlights.
        /// </summary>
        [Test]
        public void EmptySnippetHighlightTest()
        {
            ConsumerHighlightsMockVehiclePreference preference =
                UtilityMethods.ParseVehiclePreferencesFromXml("ConsumerHighlightsVehiclePreferences_AllDisplayed_In.xml");
            string preferenceXml = UtilityMethods.ParseXml("ConsumerHighlightsVehiclePreferences_AllDisplayed_Out.xml");

            // Remove the snippet highlights node from it.
            int index = preferenceXml.IndexOf("<snippethighlights");
            int length = preferenceXml.LastIndexOf("</snippethighlights>") - index + "</snippethighlights>".Length;
            preferenceXml = preferenceXml.Remove(index, length);

            // Test empty snippet highlights.
            preference.SnippetHighlights.Clear();
            string xml = ConsumerHighlightsVehiclePreferenceXmlBuilder.AsXml(preference, false);
            Assert.AreEqual(preferenceXml, xml);

            // Test null snippet highlights.
            preference.SnippetHighlights = null;
            xml = ConsumerHighlightsVehiclePreferenceXmlBuilder.AsXml(preference, false);
            Assert.AreEqual(preferenceXml, xml);
        }

        /// <summary>
        /// Test that the xml that is built from vehicle preferences does not have the vehicle history highlights node
        /// when there are no vehicle history highlights.        
        /// </summary>
        [Test]
        public void EmptyVehicleHistoryHighlightTest()
        {
            ConsumerHighlightsMockVehiclePreference preference =
                UtilityMethods.ParseVehiclePreferencesFromXml("ConsumerHighlightsVehiclePreferences_AllDisplayed_In.xml");
            string preferenceXml = UtilityMethods.ParseXml("ConsumerHighlightsVehiclePreferences_AllDisplayed_Out.xml");

            // Remove the vehicle history highlights node from it.
            int index = preferenceXml.IndexOf("<vehiclehistoryhighlights");
            int length = preferenceXml.LastIndexOf("</vehiclehistoryhighlights>") - index + "</vehiclehistoryhighlights>".Length;
            preferenceXml = preferenceXml.Remove(index, length);

            // Test empty vehicle history highlights.
            preference.VehicleHistoryHighlights.Clear();
            string xml = ConsumerHighlightsVehiclePreferenceXmlBuilder.AsXml(preference, false);
            Assert.AreEqual(preferenceXml, xml);

            // Test null vehicle history highlights.
            preference.VehicleHistoryHighlights = null;
            xml = ConsumerHighlightsVehiclePreferenceXmlBuilder.AsXml(preference, false);
            Assert.AreEqual(preferenceXml, xml);
        } 

        /// <summary>
        /// Test that highlights that are set to not being displayed do not end up in the xml if the xml builder is 
        /// called with the option to exclude non-displayed highlights.
        /// </summary>
        [Test]
        public void SomeNotDisplayedTest()
        {
            ConsumerHighlightsMockVehiclePreference preference =
                UtilityMethods.ParseVehiclePreferencesFromXml("ConsumerHighlightsVehiclePreferences_SomeDisplayed_In.xml");
            string preferenceXml = UtilityMethods.ParseXml("ConsumerHighlightsVehiclePreferences_SomeDisplayed_Out.xml");

            string xml = ConsumerHighlightsVehiclePreferenceXmlBuilder.AsXml(preference, true);
            Assert.AreEqual(preferenceXml, xml);
        }

        /// <summary>
        /// Check that the xml is correct when preferences as a whole are set to not be displayed.
        /// </summary>
        [Test]
        public void PreferencesNotDisplayedTest()
        {
            ConsumerHighlightsMockVehiclePreference preference =
                   UtilityMethods.ParseVehiclePreferencesFromXml("ConsumerHighlightsVehiclePreferences_AllDisplayed_In.xml");
            string preferenceXml = UtilityMethods.ParseXml("ConsumerHighlightsVehiclePreferences_AllDisplayed_Out.xml");
   
            preference.IsDisplayed = false;
            preferenceXml = preferenceXml.Replace(@"<consumerhighlights display=""true"">", @"<consumerhighlights display=""false"">");

            string xml = ConsumerHighlightsVehiclePreferenceXmlBuilder.AsXml(preference, false);
            Assert.AreEqual(preferenceXml, xml);
        }

        /// <summary>
        /// Check that the xml is correct when the 'show CarFax logo' setting is enabled.
        /// </summary>
        [Test]
        public void ShowCarfaxLogoTest()
        {
            ConsumerHighlightsMockVehiclePreference preference =
                   UtilityMethods.ParseVehiclePreferencesFromXml("ConsumerHighlightsVehiclePreferences_AllDisplayed_In.xml");
            string preferenceXml = UtilityMethods.ParseXml("ConsumerHighlightsVehiclePreferences_AllDisplayed_Out.xml");

            preference.IncludeCarfaxOneOwnerIcon = true;
            preferenceXml = preferenceXml.Replace(@"showCarFaxLogo=""false""", @"showCarFaxLogo=""true""");

            string xml = ConsumerHighlightsVehiclePreferenceXmlBuilder.AsXml(preference, false);
            Assert.AreEqual(preferenceXml, xml);
        }

        /// <summary>
        /// Check that the xml is correct when the CarFax logo url is specified.
        /// </summary>
        [Test]
        public void CarFaxLogoUrl()
        {
            ConsumerHighlightsMockVehiclePreference preference =
                   UtilityMethods.ParseVehiclePreferencesFromXml("ConsumerHighlightsVehiclePreferences_AllDisplayed_In.xml");
            string preferenceXml = UtilityMethods.ParseXml("ConsumerHighlightsVehiclePreferences_AllDisplayed_Out.xml");

            preferenceXml = preferenceXml.Replace(@"carFaxLogoUrl=""""", @"carFaxLogoUrl=""TEST URL""");

            string xml = ConsumerHighlightsVehiclePreferenceXmlBuilder.AsXml(preference, false, "TEST URL");
            Assert.AreEqual(preferenceXml, xml);
        }
    }
}
