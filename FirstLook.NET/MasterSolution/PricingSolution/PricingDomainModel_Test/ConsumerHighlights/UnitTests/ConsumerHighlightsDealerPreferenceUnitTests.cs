﻿using Autofac;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types.TransferObjects;
using NUnit.Framework;

namespace PricingDomainModel_Test.ConsumerHighlights.UnitTests
{
    [TestFixture]
    public class ConsumerHighlightsDealerPreferenceUnitTests
    {
        private ConsumerHighlightsDealerPreferenceTO _preferenceTO;
        private ConsumerHighlightsDealerPreferenceMockDataAccess _dataAccess;

        /// <summary>
        /// Set up this test fixture by registering the mock data access.
        /// </summary>
        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<ConsumerHighlightsDealerPreferenceMockDataAccess>().As<IConsumerHighlightsDealerPreferenceDataAccess>();
            Registry.RegisterContainer(builder.Build());

            _dataAccess = (ConsumerHighlightsDealerPreferenceMockDataAccess)Registry.Resolve<IConsumerHighlightsDealerPreferenceDataAccess>();
        }

        /// <summary>
        /// Set up the conditions prior to every test.
        /// </summary>
        [SetUp]
        public void SetTestConditions()
        {
            ConsumerHighlightsDealerPreferenceMockDataAccess.Reset();
            _preferenceTO = _dataAccess.Preference;
        }

        /// <summary>
        /// Test that the conversion of a dealer preference object to and from a transfer object works as expected.
        /// </summary>
        [Test]
        public void TransferObjectParsingTest()
        {
            ConsumerHighlightsDealerPreference preference = 
                ConsumerHighlightsDealerPreference.GetOrCreateDealerPreference("");
        
            preference.FromTransferObject(_preferenceTO);

            // Minimum JDPower circle rating value.
            Assert.AreEqual(preference.MinimumJdPowerCircleRating, _preferenceTO.MinimumJdPowerCircleRating);

            // Highlight section ordering.
            Assert.AreEqual(preference.HighlightSections.Count, _preferenceTO.HighlightSections.Count);
            for (int i = 0; i < preference.HighlightSections.Count; i++)
            {
                Assert.AreEqual(preference.HighlightSections[i], _preferenceTO.HighlightSections[i]);
            }

            // Displayed snippet source ordering.
            Assert.AreEqual(preference.DisplayedSnippetSources.Count, _preferenceTO.DisplayedSnippetSources.Count);
            for (int i = 0; i < preference.DisplayedSnippetSources.Count; i++)
            {
                Assert.AreEqual(preference.DisplayedSnippetSources[i], _preferenceTO.DisplayedSnippetSources[i]);
            }            

            // Hidden snippet source ordering.
            Assert.AreEqual(preference.HiddenSnippetSources.Count, _preferenceTO.HiddenSnippetSources.Count);

            // Now put the preferences back into a transfer object and compare again.
            ConsumerHighlightsDealerPreferenceTO preferenceTO2 = preference.ToTransferObject();

            // Minimum JDPower circle rating value.
            Assert.AreEqual(preferenceTO2.MinimumJdPowerCircleRating, _preferenceTO.MinimumJdPowerCircleRating);

            // Highlight section ordering.
            Assert.AreEqual(preferenceTO2.HighlightSections.Count, _preferenceTO.HighlightSections.Count);
            for (int i = 0; i < preferenceTO2.HighlightSections.Count; i++)
            {
                Assert.AreEqual(preferenceTO2.HighlightSections[i], _preferenceTO.HighlightSections[i]);
            }

            // Displayed snippet source ordering.
            Assert.AreEqual(preferenceTO2.DisplayedSnippetSources.Count, _preferenceTO.DisplayedSnippetSources.Count);
            for (int i = 0; i < preferenceTO2.DisplayedSnippetSources.Count; i++)
            {
                Assert.AreEqual(preferenceTO2.DisplayedSnippetSources[i], _preferenceTO.DisplayedSnippetSources[i]);
            }

            // Hidden snippet source ordering.
            Assert.AreEqual(preferenceTO2.HiddenSnippetSources.Count, _preferenceTO.HiddenSnippetSources.Count);
        }

        /// <summary>
        /// Test that dealer preferences call the right data access functions when being created.
        /// </summary>
        [Test]
        public void CreateTest()
        {
            ConsumerHighlightsDealerPreferenceMockDataAccess.ShouldExist = false;

            ConsumerHighlightsDealerPreference.GetOrCreateDealerPreference("");

            Assert.AreEqual(true, ConsumerHighlightsDealerPreferenceMockDataAccess.ExistsCalled);
            Assert.AreEqual(true, ConsumerHighlightsDealerPreferenceMockDataAccess.CreateCalled);
            Assert.AreEqual(false, ConsumerHighlightsDealerPreferenceMockDataAccess.FetchCalled);
            Assert.AreEqual(false, ConsumerHighlightsDealerPreferenceMockDataAccess.SaveCalled);
        }

        /// <summary>
        /// Tests that dealer preferences call the right data access function when being fetched.
        /// </summary>
        [Test]
        public void FetchTest()
        {
            ConsumerHighlightsDealerPreferenceMockDataAccess.ShouldExist = true;

            ConsumerHighlightsDealerPreference.GetOrCreateDealerPreference("");

            Assert.AreEqual(true, ConsumerHighlightsDealerPreferenceMockDataAccess.ExistsCalled);
            Assert.AreEqual(false, ConsumerHighlightsDealerPreferenceMockDataAccess.CreateCalled);
            Assert.AreEqual(true, ConsumerHighlightsDealerPreferenceMockDataAccess.FetchCalled);
            Assert.AreEqual(false, ConsumerHighlightsDealerPreferenceMockDataAccess.SaveCalled);
        }

        /// <summary>
        /// Tests that the saving of dealer preferences works as expected.
        /// </summary>
        [Test]
        public void SaveTest()
        {
            ConsumerHighlightsDealerPreference preference = 
                ConsumerHighlightsDealerPreference.GetOrCreateDealerPreference("");

            ConsumerHighlightsDealerPreferenceMockDataAccess.Reset();

            preference.Save();

            Assert.AreEqual(false, ConsumerHighlightsDealerPreferenceMockDataAccess.ExistsCalled);
            Assert.AreEqual(false, ConsumerHighlightsDealerPreferenceMockDataAccess.CreateCalled);
            Assert.AreEqual(false, ConsumerHighlightsDealerPreferenceMockDataAccess.FetchCalled);
            Assert.AreEqual(true, ConsumerHighlightsDealerPreferenceMockDataAccess.SaveCalled);
        }
    }
}
