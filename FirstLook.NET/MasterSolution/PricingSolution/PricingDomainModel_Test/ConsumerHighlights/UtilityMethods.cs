using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types.TransferObjects;
using PricingDomainModel_Test.ConsumerHighlights.UnitTests;

namespace PricingDomainModel_Test.ConsumerHighlights
{
    /// <summary>
    /// Consumer highlights utility methods.
    /// </summary>
    internal class UtilityMethods
    {
        /// <summary>
        /// Converts an enumeration of highlights into a list.
        /// </summary>
        /// <param name="highlights">Enumerated highlights.</param>
        /// <returns>List of highlights.</returns>
        internal static List<ConsumerHighlightTO> GetListOfHighlights(IEnumerable<IConsumerHighlight> highlights)
        {
            List<ConsumerHighlightTO> list = new List<ConsumerHighlightTO>();
            foreach (ConsumerHighlightTO to in highlights)
            {
                list.Add(to);
            }
            return list;
        }

        /// <summary>
        /// Get a stream for the resource with the given filename.
        /// </summary>
        /// <param name="fileName">Name of the file to read.</param>
        /// <returns>Stream.</returns>
        private static Stream GetStreamForFile(string fileName)
        {            
            string resourceName = "PricingDomainModel_Test.Resources." + fileName;

            Assembly assembly = Assembly.GetExecutingAssembly();
            Stream stream = assembly.GetManifestResourceStream(resourceName);

            if (stream == null)
            {
                throw new ArgumentException("Resource for filename not found.");
            }            

            return stream;
        }

        /// <summary>
        /// Get vehicle preferences for the xml file with the given name.
        /// </summary>
        /// <param name="fileName">Name of the xml file to parse.</param>
        /// <returns>Consumer highlights vehicle preferences.</returns>
        internal static ConsumerHighlightsMockVehiclePreference ParseVehiclePreferencesFromXml(string fileName)
        {
            System.Xml.Serialization.XmlSerializer serializer =
                new System.Xml.Serialization.XmlSerializer(typeof(ConsumerHighlightsMockVehiclePreference));

            Stream stream = GetStreamForFile(fileName);

            TextReader reader = new StreamReader(stream);
            return (ConsumerHighlightsMockVehiclePreference)serializer.Deserialize(reader);
        }                

        /// <summary>
        /// Get an xml string from the file with the given name.
        /// </summary>
        /// <param name="fileName">Name of the xml file to parse.</param>
        /// <returns>Xml as a string.</returns>
        internal static string ParseXml(string fileName)
        {
            Stream stream = GetStreamForFile(fileName);    
            XmlDocument document = new XmlDocument();
            document.Load(stream);

            StringWriter sw = new StringWriter();
            XmlTextWriter xw = new XmlTextWriter(sw);
            document.WriteTo(xw);
            return sw.ToString();
        }        
    }
}
