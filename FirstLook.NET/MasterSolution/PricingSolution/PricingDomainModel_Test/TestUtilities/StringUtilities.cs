using System;
using System.Text;
using NUnit.Framework;

namespace PricingDomainModel_Test.TestUtilities
{
    [TestFixture]
    public class StringUtilities
    {

        #region Tests of utilities
        [Test]
        public void GenerateStringCorrectLength()
        {
            string ten = GenerateString(10);
            Assert.AreEqual(10, ten.Length);
        }
        #endregion

        #region Utility Methods

        internal static string GenerateString(int length)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < length; i++)
            {
                sb.Append("a");
            }

            return sb.ToString();
        }

        internal static string GetRandomString()
        {
            return new Random((int)DateTime.Now.Ticks).Next().ToString();
        }

        #endregion
    }
}
