using System;
using System.Collections.Generic;
using FirstLook.Common.Core.Xml;
using NUnit.Framework;

namespace PricingDomainModel_Test
{
    [TestFixture]
    public class TagBuilderUnitTests
    {

        [Test]
        public void WrapEmptyString()
        {
            const string TEXT = "";

            string xml = TagBuilder.Wrap("a", TEXT);         
            Assert.AreEqual("<a></a>", xml);

            Console.WriteLine(xml);
        }

        [Test]
        public void WrapLessThanChar()
        {
            const string TEXT = "<";

            string xml = TagBuilder.Wrap("a", TEXT);
            Assert.AreEqual("<a>&#60;</a>", xml);

            Console.WriteLine(xml);
        }

        [Test]
        public void WrapGreaterThanChar()
        {
            const string TEXT = ">";

            string xml = TagBuilder.Wrap("a", TEXT);
            Assert.AreEqual("<a>&#62;</a>", xml);

            Console.WriteLine(xml);
        }

        [Test]
        public void WrapDoubleQuote()
        {
            const string TEXT = "\"";

            string xml = TagBuilder.Wrap("a", TEXT);
            Assert.AreEqual("<a>&#34;</a>", xml);

            Console.WriteLine(xml);
        }

        [Test]
        public void WrapSingleQuote()
        {
            const string TEXT = "'";
            
            string xml = TagBuilder.Wrap("a", TEXT);
            Assert.AreEqual("<a>&#39;</a>", xml);

            Console.WriteLine(xml);
        }

        [Test]
        public void WrapAmpersand()
        {
            const string TEXT = "&";
            
            string xml = TagBuilder.Wrap("a", TEXT);
            Assert.AreEqual("<a>&#38;</a>", xml);

            Console.WriteLine(xml);
        }

        [Test]
        public void AttributesEscaped()
        {
            IDictionary<string,string> attributes= new Dictionary<string, string>();
            attributes.Add("a", "<");
            attributes.Add("b", ">");
            attributes.Add("c", "'");
            attributes.Add("d", "\"");
            attributes.Add("e", "&");

            string xml = TagBuilder.Wrap("test", attributes);
            Console.WriteLine(xml);

            const string EXPECTED = "<test a=\"&#60;\" b=\"&#62;\" c=\"&#39;\" d=\"&#34;\" e=\"&#38;\"></test>";

            Assert.AreEqual( EXPECTED, xml );
        }

        [Test]
        public void InnerElementsIgnored()
        {
            const string INNER = "<inner>&</inner>";
            string xml = TagBuilder.Wrap("outer", INNER);

            const string EXPECTED = "<outer><inner>&</inner></outer>";

            Console.WriteLine(xml);

            Assert.AreEqual( EXPECTED, xml);
        }

    }
}
