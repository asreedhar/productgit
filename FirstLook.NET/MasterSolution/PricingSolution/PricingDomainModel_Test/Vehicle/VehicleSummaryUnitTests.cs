
using Autofac;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.Marketing.Vehicle;
using NUnit.Framework;

namespace PricingDomainModel_Test.Vehicle
{
    [TestFixture]
    public class VehicleSummaryUnitTests
    {
        [Test]
        public void CanGetVehicleSummary()
        {
            ResetTestConditions();

            string ownerHandle = string.Empty;
            string vehicleHandle = string.Empty;

            VehicleSummary summary = VehicleSummary.GetVehicleSummary(ownerHandle, vehicleHandle);
            Assert.IsNotNull( summary );
        }

        #region Reset Test Conditions

        private static void ResetTestConditions()
        {
            ResetRegistryWithMockDataAccess();
        }

        private static void ResetRegistryWithMockDataAccess()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<VehicleSummaryMockDataAccess>().As<IVehicleSummaryDataAccess>();
            Registry.RegisterContainer(builder.Build());
        }

       

        #endregion
    }
}