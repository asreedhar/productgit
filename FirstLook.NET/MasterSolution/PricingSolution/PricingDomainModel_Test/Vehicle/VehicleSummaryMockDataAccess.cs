using FirstLook.Merchandising.DomainModel.Marketing.Vehicle;

namespace PricingDomainModel_Test.Vehicle
{
    public class VehicleSummaryMockDataAccess : IVehicleSummaryDataAccess 
    {
        public VehicleSummaryTO Fetch(string ownerHandle, string vehicleHandle)
        {
            // Ignore keys
            return new VehicleSummaryTO(ownerHandle, vehicleHandle, 1, "red", "red", "vr6", "5 speed", "manual", "abc", "123", "fake description");
        }
    }
}
