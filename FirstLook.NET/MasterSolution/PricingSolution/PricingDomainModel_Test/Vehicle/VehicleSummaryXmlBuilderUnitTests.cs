using System;
using FirstLook.Merchandising.DomainModel.Marketing.Vehicle;
using NUnit.Framework;

namespace PricingDomainModel_Test.Vehicle
{
    [TestFixture]
    public class VehicleSummaryXmlBuilderUnitTests
    {
        // Verify xml output
        private const string EXPECTED = "<vehiclesummary><color>Silver</color><interiorcolor>Grey</interiorcolor><genericexteriorcolor>Silver</genericexteriorcolor><drivetrain>2WD</drivetrain><engine>V6 3.5 Liter VTEC</engine><mileage>32848</mileage><stocknumber>ACG123</stocknumber><transmission>5-speed Auto</transmission><vin>1FTPW14V77FA04367</vin><year>2008</year><make>Dodge</make><model>Grand Caravan</model><trim>Sport</trim><yearmodel>2008 Honda Odyssey EX-L</yearmodel><advertisement>This is a car advertisement.</advertisement><inventorytype>new</inventorytype></vehiclesummary>";

        [Test]
        public void AsXmlReturnsExpected()
        {
            FakeSummary fake = new FakeSummary();
            string xml = VehicleSummaryXmlBuilder.AsXml(fake);

            Assert.AreEqual( EXPECTED, xml);
        }


        class FakeSummary : IVehicleSummary
        {
            public int Mileage
            {
                get { return 32848; }
            }

            public string GenericExteriorColor
            {
                get { return "Silver"; }
            }

            public string ExteriorColor
            {
                get { return "Silver"; }
            }

            public string Engine
            {
                get { return "V6 3.5 Liter VTEC"; }
            }

            public string Transmission
            {
                get { return "5-speed Auto"; }
            }

            public string DriveTrain
            {
                get { return "2WD"; }
            }

            public string StockNumber
            {
                get { return "ACG123"; }
            }

            public string VIN
            {
                get { return "1FTPW14V77FA04367"; }
            }

            public string VehicleHandle
            {
                get { throw new NotImplementedException(); }
            }

            public string OwnerHandle
            {
                get { throw new NotImplementedException(); }
            }

            public string Description
            {
                get { return "2008 Honda Odyssey EX-L"; }
            }

            public string Advertisement
            {
                get { return "This is a car advertisement."; }
            }

            public int Year
            {
                get { return 2008; }
            }

            public string Make
            {
                get { return "Dodge"; }
            }

            public string Line
            {
                get { return "Grand Caravan"; }
            }

            public string Series
            {
                get { return "Sport"; }
            }

            public string InteriorColor
            {
                get { return "Grey"; }
            }

            public string InventoryType
            {
                get { return "new"; } // just guessing, fixing build!!! tureen 3/4/2015
            }

        }

    }


}
