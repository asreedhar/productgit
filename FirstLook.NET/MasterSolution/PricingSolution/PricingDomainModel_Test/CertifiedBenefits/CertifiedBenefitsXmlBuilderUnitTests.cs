using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits;
using NUnit.Framework;
using FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types.TransferObjects;

namespace PricingDomainModel_Test.CertifiedBenefits
{
    [TestFixture]
    public class CertifiedBenefitsXmlBuilderUnitTests
    {
        private const string BENEFIT_XML_EMPTY = "<certifiedbenefits name=\"a\" logoPath=\"b\" display=\"true\" vehicleIsCertified=\"true\"></certifiedbenefits>";

        private const string BENEFIT_XML_0 =
            "<certifiedbenefits name=\"a\" logoPath=\"b\" display=\"true\" vehicleIsCertified=\"true\"><benefit key=\"1:10\" rank=\"1\">benefit text 0</benefit></certifiedbenefits>";

        private const string BENEFIT_XML_0_1 =
            "<certifiedbenefits name=\"a\" logoPath=\"b\" display=\"true\" vehicleIsCertified=\"true\"><benefit key=\"1:10\" rank=\"1\">benefit text 0</benefit><benefit key=\"1:11\" rank=\"2\">benefit text 1</benefit></certifiedbenefits>";

        private OwnerCertifiedProgramTO _program;
        private IList<CertifiedVehicleBenefitLineItemTO> _benefits;
        private OwnerCertifiedProgramBenefitTO _benefit0;
        private OwnerCertifiedProgramBenefitTO _benefit1;

        [TestFixtureSetUp]
        public void Setup()
        {
			
			
			// internal OwnerCertifiedProgramBenefitTO(int programBenefitId, int ownerBenefitId, string name, string text, bool isHighlighted, int rank)
			_benefit0 = new OwnerCertifiedProgramBenefitTO(10, 110, "benefit name 0", "benefit text 0", true, 1);
			_benefit1 = new OwnerCertifiedProgramBenefitTO(11, 111, "benefit name 1", "benefit text 1", true, 2);

			// internal OwnerCertifiedProgramTO(int programId, int? ownerProgramId, string programName, string logoPath, List<OwnerCertifiedProgramBenefitTO> benefits)
			_program = new OwnerCertifiedProgramTO(1, 10, "a", "b", new List<OwnerCertifiedProgramBenefitTO>{_benefit0, _benefit1});
//internal OwnerCertifiedProgramTO(int programId, int ownerProgramId, string programName, string logoPath, bool isCustomizedByOwner)
//OwnerCertifiedProgramBenefitTO(OwnerCertifiedProgramTO program, int programBenefitId, int ownerBenefitId, string name, string text, bool isHighlighted, int rank)
            _benefits = new List<CertifiedVehicleBenefitLineItemTO>();
        }

        [Test]
        public void CreateNoBenefitsElement()
        {
            CertifiedVehiclePreferenceTO preference = 
                new CertifiedVehiclePreferenceTO(null, String.Empty, String.Empty, true, _benefits, _program, true );

            string xml = CertifiedVehiclePreferenceXmlBuilder.AsXml(preference);
            const string expectedXml = BENEFIT_XML_EMPTY;

            Assert.IsNotEmpty(xml);
            Assert.AreEqual(expectedXml, xml );

            Console.WriteLine(xml);

        }

		private static CertifiedVehicleBenefitLineItemTO CreateBenefitLineItemTOFrom(OwnerCertifiedProgramBenefitTO benefitTO)
	    {
		    return new CertifiedVehicleBenefitLineItemTO(0, 1, benefitTO.ProgramBenefitId, benefitTO.Name, benefitTO.Text,
			    benefitTO.IsHighlighted, benefitTO.Rank);
	    }

	    [Test]
        public void CreateSingleBenefit()
        {
            _benefits.Clear();
			// internal CertifiedVehicleBenefitLineItemTO( int? certifiedVehiclePreferenceId, int certifiedProgramId, int benefitId,string name, string text, bool isHighlighted, int rank)
			_benefits.Add(CreateBenefitLineItemTOFrom(_benefit0));
            
            var preference = new CertifiedVehiclePreferenceTO(null, String.Empty, String.Empty, true, _benefits, _program, true);

            string xml = CertifiedVehiclePreferenceXmlBuilder.AsXml(preference);
            const string expectedXml = BENEFIT_XML_0;

            Assert.IsNotEmpty(xml);
            Assert.AreEqual(expectedXml, xml);     
       
            Console.WriteLine(xml);
        }

        [Test]
        public void CreateTwoBenefits()
        {
            _benefits.Clear();
			_benefits.Add(CreateBenefitLineItemTOFrom(_benefit0));
			_benefits.Add(CreateBenefitLineItemTOFrom(_benefit1));
            
            CertifiedVehiclePreferenceTO preference = 
                new CertifiedVehiclePreferenceTO(null, String.Empty, String.Empty, true, _benefits, _program, true);

            string xml = CertifiedVehiclePreferenceXmlBuilder.AsXml(preference);
            const string expectedXml = BENEFIT_XML_0_1;

            Assert.IsNotEmpty(xml);
            Assert.AreEqual(expectedXml, xml);

            Console.WriteLine(xml);
        }

        [Test]
        public void HideNonHighlightedBenefits()
        {
            _benefits.Clear();
			_benefits.Add(CreateBenefitLineItemTOFrom(_benefit0));
			_benefits.Add(CreateBenefitLineItemTOFrom(_benefit1));

			_benefits[0].IsHighlighted = false;
			_benefits[1].IsHighlighted = false;

            var preference = new CertifiedVehiclePreferenceTO(null, String.Empty, String.Empty,true, _benefits, _program, true);

            string xml = CertifiedVehiclePreferenceXmlBuilder.AsXml(preference);
            const string expectedXml = BENEFIT_XML_EMPTY;

            Assert.IsNotEmpty(xml);
            Assert.AreEqual(expectedXml, xml);

            Console.WriteLine(xml);
        }

    }
}
