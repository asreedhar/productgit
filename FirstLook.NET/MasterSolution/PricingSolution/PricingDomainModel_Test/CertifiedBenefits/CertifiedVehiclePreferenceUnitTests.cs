using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using Csla;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits;
using FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types;
using FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types.TransferObjects;

using NUnit.Framework;

namespace PricingDomainModel_Test.CertifiedBenefits
{
    [TestFixture]
    public class CertifiedVehiclePreferenceUnitTests
    {
        const string OWNER_HANDLE = "a";
        const string VEHICLE_HANDLE = "b";
        const bool IS_DISPLAYED = true;
         

        [Test]
        [ExpectedException(typeof(DataPortalException))]
        public void GetVehiclePreferenceThrowsWhenNoExistingPreferenceExists()
        {
            ResetTestConditions();

            // Try to get the preference - this should throw an exception.
            CertifiedVehiclePreference.GetCertifiedVehiclePreference(String.Empty, String.Empty);            
        }

        [Test]
        [ExpectedException(typeof(DataPortalException))]
        public void NewCertifiedVehiclePreferenceThrowsWhenNoUnderlyingProgram()
        {
            ResetTestConditions();
            CertifiedVehiclePreference.CreateCertifiedVehiclePreference(String.Empty, String.Empty);
        }

        [Test]
        public void GetOrCreateReturnsNewPreference()
        {
            ResetTestConditions();
            CertifiedVehiclePreference preference = 
                CertifiedVehiclePreference.GetOrCreateCertifiedVehiclePreference(OWNER_HANDLE, VEHICLE_HANDLE);

            Assert.IsNotNull( preference );
            Assert.IsTrue( preference.IsNew );
            Assert.IsTrue( preference.IsDirty );
        }

        [Test]
        public void NewlySavedPreferenceHasID()
        {
            ResetTestConditions();
            CertifiedVehiclePreference newPreference = CreatePreference(OWNER_HANDLE, VEHICLE_HANDLE, IS_DISPLAYED);

            // Save the preference.
            newPreference.Save();

            Assert.IsTrue( newPreference.CertifiedVehiclePreferenceID > 0 );
            Console.WriteLine( "Preference ID = " + newPreference.CertifiedVehiclePreferenceID );

        }

        [Test]
        public void NewlyCreatedPreferenceIsInitializedAndCanBeFound()
        {
            ResetTestConditions();
            CertifiedVehiclePreference newPreference = CreatePreference(OWNER_HANDLE, VEHICLE_HANDLE, IS_DISPLAYED);

            // Verify that benefit line items exist.
            Assert.IsNotNull( newPreference.Benefits );
            Assert.AreEqual( CertifiedVehiclePreferenceMockDataAccess.GetOwnerBenefits().Count, 
                             newPreference.Benefits.Count );

            Assert.IsTrue(newPreference.IsNew);
            Assert.IsTrue(newPreference.IsDirty);

            newPreference.Save();

            Assert.IsFalse( newPreference.IsNew );
            Assert.IsFalse( newPreference.IsDirty );

            // Try to get the preference
            CertifiedVehiclePreference foundPreference =
                CertifiedVehiclePreference.GetCertifiedVehiclePreference(newPreference.OwnerHandle, newPreference.VehicleHandle);

            // Verify equality.
            Assert.AreEqual( newPreference.OwnerHandle, foundPreference.OwnerHandle);
            Assert.AreEqual( newPreference.VehicleHandle, foundPreference.VehicleHandle );
            Assert.IsTrue( foundPreference.IsDisplayed );
        }

        [Test]
        public void NewlyCreatedAndSavedBenefitLineItemsMapToOwnerBenefits()
        {
            ResetTestConditions();

            // Create a new preference and save it.
            var newPreference = CreatePreference(OWNER_HANDLE, VEHICLE_HANDLE, IS_DISPLAYED);
            newPreference.Save();

            // Try to get the preference
            var foundPreference = CertifiedVehiclePreference.GetOrCreateCertifiedVehiclePreference(newPreference.OwnerHandle, newPreference.VehicleHandle);

            // Are the owner benefits attached as line items with the correct settings?
            var benefits = foundPreference.Benefits;
            var ownerBenefits = CertifiedVehiclePreferenceMockDataAccess.GetOwnerBenefits();

            // For each benefit line item, verify it maps to an owner certified benefit.
            foreach( var benefit in benefits )
            {
                // find the owner benefit and verify equality of properties
                var found = Find(ownerBenefits, benefit.BenefitId);

                Assert.IsNotNull(found);
                Assert.AreEqual( found.IsHighlighted, benefit.IsHighlighted );
                Assert.AreEqual( found.Name, benefit.Name );
                Assert.AreEqual( found.ProgramBenefitId, benefit.BenefitId);
                Assert.AreEqual( found.Rank, benefit.Rank);
            }

        }

        [Test]
        public void BenefitLineItemRankCanBeAdjusted()
        {
            ResetTestConditions();

            // Create a new preference and save it.
            CertifiedVehiclePreference newPreference = CreatePreference(OWNER_HANDLE, VEHICLE_HANDLE, IS_DISPLAYED);
            newPreference.Save();

            // Get the preference
            CertifiedVehiclePreference foundPreference =
                CertifiedVehiclePreference.GetOrCreateCertifiedVehiclePreference(newPreference.OwnerHandle, newPreference.VehicleHandle);

            // Assign all benefits the same rank.
            var benefits = foundPreference.Benefits;
            foreach(var benefit in benefits)
            {
                benefit.Rank = 100;
            }

            // Save the preference
            foundPreference.Save();

            // Get it again and verify the ranks are as expected.
            var savedPreference = CertifiedVehiclePreference.GetCertifiedVehiclePreference(newPreference.OwnerHandle,
                                                                                           newPreference.VehicleHandle);

            var savedBenefits = savedPreference.Benefits;

            var count = savedBenefits.Where(b => b.Rank != 100).Count();

            Assert.AreEqual(0, count, "Not all benefits have the expected rank.");            
        }


        [Test]
        public void ToTransferObject()
        {
            ResetTestConditions();

            CertifiedVehiclePreference pref = CreatePreference(OWNER_HANDLE, VEHICLE_HANDLE, IS_DISPLAYED);
            CertifiedVehiclePreferenceTO transferObject = pref.ToTransferObject();

            Assert.AreEqual( pref.CertifiedVehiclePreferenceID, transferObject.CertifiedVehiclePreferenceID );
            Assert.AreEqual( pref.IsDisplayed, pref.IsDisplayed );
            Assert.AreEqual( pref.OwnerHandle, transferObject.OwnerHandle );
            Assert.AreEqual( pref.VehicleHandle, transferObject.VehicleHandle );

            // Compare benefit counts
            Assert.AreEqual( pref.Benefits.Count, transferObject.Benefits.Count );

            foreach( CertifiedVehicleBenefitLineItemTO dtoBenefit in transferObject.Benefits )
            {
                // locate and compare benefit transfer object in output. Finding by name as unsaved benefits all have same id.
                CertifiedVehicleBenefitLineItemTO found = FindByName(pref.Benefits, dtoBenefit.Name);
                Assert.IsNotNull(found);

                Assert.AreEqual(found.IsHighlighted, dtoBenefit.IsHighlighted);
                Assert.AreEqual(found.Name, dtoBenefit.Name);
                Assert.AreEqual(found.BenefitId, dtoBenefit.BenefitId);
                Assert.AreEqual(found.Rank, dtoBenefit.Rank);
            }

        }

        [Test]
        public void SetBenefitHighlight()
        {
            ResetTestConditions();
            CertifiedVehiclePreference newPreference = CreatePreference(OWNER_HANDLE, VEHICLE_HANDLE, IS_DISPLAYED);
            Assert.IsTrue(newPreference.Benefits.Count > 0 );

            // Set all to false.
            foreach( CertifiedVehicleBenefitLineItemTO benefit in newPreference.Benefits )
            {
                newPreference.SetBenefitLineItemDisplayProperties( benefit.Key, false, 0);
            }

            Assert.IsTrue( newPreference.IsDirty );

            // Verify all are false.
            foreach (CertifiedVehicleBenefitLineItemTO benefit in newPreference.Benefits)
            {
                Assert.IsFalse( benefit.IsHighlighted );
            }
        }

        [Test]
        public void NewPreferenceCanBeSaved()
        {
            ResetTestConditions();
            CertifiedVehiclePreference newPreference = CreatePreference(OWNER_HANDLE, VEHICLE_HANDLE, IS_DISPLAYED);

            // Assert.IsTrue( newPreference.IsNew );

            // Save the preference.
            newPreference.Save();           

            // Can we now find the preference?
            CertifiedVehiclePreference foundPreference =
                CertifiedVehiclePreference.GetOrCreateCertifiedVehiclePreference(OWNER_HANDLE, VEHICLE_HANDLE);

            Assert.IsNotNull( foundPreference );
            Assert.AreEqual(OWNER_HANDLE, foundPreference.OwnerHandle);
            Assert.AreEqual(VEHICLE_HANDLE, foundPreference.VehicleHandle);
            Assert.IsTrue( foundPreference.CertifiedVehiclePreferenceID > 0 );            
        }

        [Test]
        public void PreferenceCanBeUpdated()
        {
            ResetTestConditions();
            CertifiedVehiclePreference newPreference = CreatePreference(OWNER_HANDLE, VEHICLE_HANDLE, IS_DISPLAYED);

            // Save the preference.
            newPreference.Save();

            Assert.IsFalse( newPreference.IsDirty );

            // Find the preference?
            CertifiedVehiclePreference updatePreference =
                CertifiedVehiclePreference.GetOrCreateCertifiedVehiclePreference(OWNER_HANDLE, VEHICLE_HANDLE);
            
            // Mark all benefits as not highlighted
            foreach( CertifiedVehicleBenefitLineItemTO benefit in updatePreference.GetBenefits() )
            {
                updatePreference.SetBenefitLineItemDisplayProperties( benefit.Key, false, 0);
            }

            Assert.IsTrue( updatePreference.IsDirty );

            // Save
            updatePreference.Save();

            Assert.IsFalse( updatePreference.IsDirty );

            // Verify that save was performed.
            CertifiedVehiclePreference foundPreference =
                CertifiedVehiclePreference.GetOrCreateCertifiedVehiclePreference(OWNER_HANDLE, VEHICLE_HANDLE);

            Assert.IsFalse(foundPreference.IsDirty);

            foreach( CertifiedVehicleBenefitLineItemTO benefit in foundPreference.GetBenefits() )
            {
                Assert.IsFalse( benefit.IsHighlighted );
            }

        }

        [Test]
        public void AsXml()
        {
            ResetTestConditions();
            CertifiedVehiclePreference newPreference = CreatePreference(OWNER_HANDLE, VEHICLE_HANDLE, IS_DISPLAYED);

            string xml = newPreference.AsXml();
            Assert.IsNotEmpty( xml );

            Console.WriteLine( xml );
        }

        [Test]
        [ExpectedException(typeof(DataPortalException))]
        public void MarkForDeletion()
        {
            ResetTestConditions();
            CertifiedVehiclePreference newPreference = CreatePreference(OWNER_HANDLE, VEHICLE_HANDLE, IS_DISPLAYED);
            Assert.IsTrue( newPreference.IsNew );

            // Save the preference.
            newPreference.Save();
            Assert.IsFalse( newPreference.IsNew );

            // Mark the preference for deletion.
            newPreference.MarkForDeletion();
            Assert.IsTrue( newPreference.IsDeleted );

            // Perform the delete.
            newPreference.Save();            

            // We should no longer be able to find it.            
            CertifiedVehiclePreference.GetCertifiedVehiclePreference(OWNER_HANDLE,
                                                                     VEHICLE_HANDLE);
        }

        #region Utility Methods

        private static OwnerCertifiedProgramBenefitTO Find(IEnumerable<OwnerCertifiedProgramBenefitTO> benefitCollection, int id)
        {
	        return benefitCollection.FirstOrDefault(benefitDto => benefitDto.ProgramBenefitId == id);
        }

	    private static CertifiedVehicleBenefitLineItemTO FindByName(IEnumerable<CertifiedVehicleBenefitLineItemTO> benefitCollection, string name )
        {
            foreach( CertifiedVehicleBenefitLineItemTO benefitDto in benefitCollection )
            {
                if (benefitDto.Name == name) return benefitDto;               
            }
            return null;
        }

        private static CertifiedVehiclePreference CreatePreference(string ownerHandle, string vehicleHandle, bool isDisplayed)
        {
            // Create a preference
            CertifiedVehiclePreference preference = CertifiedVehiclePreference.CreateCertifiedVehiclePreference( ownerHandle, vehicleHandle );
            preference.IsDisplayed = isDisplayed;
            return preference;
        }

        #endregion

        #region Reset Test Conditions

        private static void ResetTestConditions()
        {
            ResetRegistryWithMockDataAccess();
            ResetMockDataStore();
        }

        private static void ResetRegistryWithMockDataAccess()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<CertifiedVehiclePreferenceMockDataAccess>().As<ICertifiedVehiclePreferenceDataAccess>();
            Registry.RegisterContainer(builder.Build());            
        }

        private static void ResetMockDataStore()
        {
            CertifiedVehiclePreferenceMockDataAccess.Clear();
        }

        #endregion

    }
}
