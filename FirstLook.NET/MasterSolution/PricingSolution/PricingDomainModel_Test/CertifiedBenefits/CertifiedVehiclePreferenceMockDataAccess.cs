using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types;
using FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types.TransferObjects;

namespace PricingDomainModel_Test.CertifiedBenefits
{
    /// <summary>
    /// Mock data access backed in memory.
    /// </summary>
    class CertifiedVehiclePreferenceMockDataAccess : ICertifiedVehiclePreferenceDataAccess
    {
        /// <summary>
        /// OwnerHandle;VehicleHandle -> DTO
        /// </summary>
        private static readonly IDictionary<string, CertifiedVehiclePreferenceTO> _preferences
            = new Dictionary<string, CertifiedVehiclePreferenceTO>();

        private static int _maxID = 1;

        public static void Clear()
        {
            _preferences.Clear();
        }

        public bool Exists(string ownerHandle, string vehicleHandle)
        {
            string key = CreateKey(ownerHandle, vehicleHandle);
            return _preferences.ContainsKey(key);
        }

        public CertifiedVehiclePreferenceTO Fetch(string ownerHandle, string vehicleHandle)
        {
	        return Fetch(ownerHandle, vehicleHandle, 0);
        }

	    public CertifiedVehiclePreferenceTO Fetch(string ownerHandle, string vehicleHandle, int? certifiedVehicleBenefitPreferenceId)
	    {
			// Get key
			string key = CreateKey(ownerHandle, vehicleHandle);

			// Return object if it exists, otherwise return null.
			if (_preferences.ContainsKey(key))
				return _preferences[key];

			// If nothing found, throw.
			throw new ApplicationException("No certified vehicle preference could be found with the specified handles.");
	    }

	    public void Insert(CertifiedVehiclePreferenceTO preference, string userName)
        {
            preference.CertifiedVehiclePreferenceID = _maxID;

            // Construct key and increment id.
            _maxID++;
            string key = CreateKey(preference.OwnerHandle, preference.VehicleHandle);

            // Save preference dto.
            _preferences.Add(key, preference);
        }

        public void Update(CertifiedVehiclePreferenceTO preference, string userName)
        {
            if (!Exists(preference.OwnerHandle, preference.VehicleHandle))
            {
                throw new ApplicationException("Does not exist.");
            }

            string key = CreateKey(preference.OwnerHandle, preference.VehicleHandle);

            _preferences[key] = preference;
        }

        public IList<OwnerCertifiedProgramBenefitTO> FetchOwnerCertifiedBenefits(string ownerHandle, string vehicleHandle)
        {
            return GetOwnerBenefits();
        }

        public OwnerCertifiedProgramTO FetchProgram(string ownerHandle, string vehicleHandle)
        {
            // Simulate a load failure for emtpy strings.
            if (ownerHandle.Length == 0 && vehicleHandle.Length == 0)
            {
                throw new ApplicationException("No program exists with the specified handles.");
                // return null;
            }

            return GetCertifiedProgram();
        }

        public void Delete( CertifiedVehiclePreferenceTO preference, string userName)
        {
            // If the preference exists, delete it.
            if (!Exists(preference.OwnerHandle, preference.VehicleHandle))
            {
                return;
            }

            string key = CreateKey(preference.OwnerHandle, preference.VehicleHandle);
            _preferences.Remove(key);
        }

        public bool IsCertifiedInventory(string ownerHandle, string vehicleHandle)
        {
            return true;
        }

        public bool OwnerCertifiedProgramExists(string ownerHandle, string vehicleHandle)
        {
            return true;
        }

        private static string CreateKey(string ownerHandle, string vehicleHandle)
        {
            return ownerHandle + ";" + vehicleHandle;
        }

        internal static OwnerCertifiedProgramTO GetCertifiedProgram()
        {
			// internal OwnerCertifiedProgramTO(int programId, int? ownerProgramId, string programName, string logoPath, List<OwnerCertifiedProgramBenefitTO> benefits)
            return new OwnerCertifiedProgramTO(1,10, "Program Name", "/some/logo/path/temp.jpg", GetOwnerBenefits());
        }

        /// <summary>
        /// The list of owner benefits is fixed.
        /// </summary>
        /// <returns></returns>
        internal static List<OwnerCertifiedProgramBenefitTO> GetOwnerBenefits()
        {
            var benefits = new List<OwnerCertifiedProgramBenefitTO>();

            // Create a few owner benefits
			// internal OwnerCertifiedProgramBenefitTO(int programBenefitId, int ownerBenefitId, string name, string text, bool isHighlighted, int rank)
            var benefit1 = new OwnerCertifiedProgramBenefitTO( 1, 10, "benefit 1", "benefit text 1", true, 1);
            var benefit2 = new OwnerCertifiedProgramBenefitTO( 2, 11, "benefit 2", "benefit text 2", false, 2);
            var benefit3 = new OwnerCertifiedProgramBenefitTO( 3, 12, "benefit 3", "benefit text 3", true, 3);

            benefits.Add(benefit1);
            benefits.Add(benefit2);
            benefits.Add(benefit3);

            return benefits;
        }
    }
}
