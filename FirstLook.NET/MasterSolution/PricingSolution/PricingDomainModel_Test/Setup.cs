﻿using Autofac;
using FirstLook.Common.Core.IOC;
using FirstLook.Pricing.DomainModel;
using NUnit.Framework;

// SetUpFixture for assembly
[SetUpFixture]
public class Setup
{
    [SetUp]
    public void RunBeforeAnyTests()
    {
        // Register the normal dependencies.  We will override these in a bit.
        var builder = new ContainerBuilder();
        new PricingDomainRegister().Register(builder);

        IContainer container = builder.Build();
        Registry.RegisterContainer(container);
    }
}
