using System;
using Autofac;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.Marketing.AdditionalSettings;
using FirstLook.Merchandising.DomainModel.Marketing.AdditionalSettings.Types;
using FirstLook.Merchandising.DomainModel.Marketing.AdditionalSettings.Types.TransferObjects;
using NUnit.Framework;
using PricingDomainModel_Test.TestUtilities;

namespace PricingDomainModel_Test.AdditionalSettings
{
    [TestFixture]
    public class DealerInformationUnitTests
    {
        private string _addressLine1;
        private string _addressLine2;
        private string _city, _state, _zipCode;
        private string _url;
        private string _salesEmailAddress;
        private string _salesPhoneNumber;
        private string _extendedTagline;
        private int _daysValidFor;
        private string _disclaimer;
        private string _ownerHandle;

        [TestFixtureSetUp]
        public void Setup()
        {
            _addressLine1 = "line 1";
            _addressLine2 = "line 2";
            _city = "austin";
            _state = "tx";
            _zipCode = "12345";
            _url = "http://pgatour.com";
            _salesEmailAddress = "a@b.c";
            _salesPhoneNumber = "123-456-7890";
            _extendedTagline = "I'm tired";
            _daysValidFor = 1;
            _disclaimer = "evil laugh";
            _ownerHandle = "DealerInformationUnitTests";

            var builder = new ContainerBuilder();
            builder.RegisterType<DealerGeneralPreferenceMockDataAccess>().As<IDealerGeneralPreferenceDataAccess>();
            Registry.RegisterContainer(builder.Build());
        }

        #region Reset Test Conditions

        private static void ResetTestConditions()
        {
            ResetRegistryWithMockDataAccess();
            ResetMockDataStore();
        }

        private static void ResetRegistryWithMockDataAccess()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<DealerGeneralPreferenceMockDataAccess>().As<IDealerGeneralPreferenceDataAccess>();
            Registry.RegisterContainer(builder.Build());
        }

        private static void ResetMockDataStore()
        {
            DealerGeneralPreferenceMockDataAccess.Clear();
        }

        #endregion

        #region Construction Tests

        [Test]
        [Category("Construction")]
        public void CanCreateDealerGeneralPreference()
        {
            Assert.IsNotNull( CreateDealerGeneralPreference() );
        }

        [Test]
        [Category("Construction")]
        public void NewlyCreatedDealerGeneralPreferenceIsNewDirtyAndSavable()
        {
            DealerGeneralPreference pref = CreateDealerGeneralPreference();
            Assert.IsTrue( pref.IsNew );
            Assert.IsTrue( pref.IsDirty );            
            Assert.IsTrue( pref.IsSavable );
        }

        [Test]
        [Category("Construction")]
        public void CreateDealerInformationInitializesPropertiesCorrectly()
        {
            DealerGeneralPreference dealerGeneralPreference = CreateDealerGeneralPreference();

            Assert.AreEqual(_addressLine1, dealerGeneralPreference.AddressLine1);
            Assert.AreEqual(_addressLine2, dealerGeneralPreference.AddressLine2);
            Assert.AreEqual(_city, dealerGeneralPreference.City);
            Assert.AreEqual(_state, dealerGeneralPreference.StateCode);
            Assert.AreEqual(_zipCode, dealerGeneralPreference.ZipCode);
            Assert.AreEqual(_url, dealerGeneralPreference.Url);
            Assert.AreEqual(_salesEmailAddress, dealerGeneralPreference.SalesEmailAddress);
            Assert.AreEqual(_salesPhoneNumber, dealerGeneralPreference.SalesPhoneNumber);
            Assert.AreEqual(_extendedTagline, dealerGeneralPreference.ExtendedTagline);
            Assert.AreEqual(_daysValidFor, dealerGeneralPreference.DaysValidFor);
            Assert.AreEqual(_disclaimer, dealerGeneralPreference.Disclaimer);
        }

        #endregion

        #region Validation tests

        [Test]
        [Category("Validation")]
        public void LongTaglineMakesInvalid()
        {

            string longTagLine = StringUtilities.GenerateString(2001);

            // try to set the tagline to something longer than 250 characters.
            DealerGeneralPreference dealerGeneralPreference = CreateDealerGeneralPreference();
            dealerGeneralPreference.ExtendedTagline = longTagLine;

            Assert.IsFalse( dealerGeneralPreference.IsValid );

        }

        [Test]
        [Category("Validation")]
        public void LongDisclaimerMakesInvalid()
        {
            string longDisclaimer = StringUtilities.GenerateString(501);
            DealerGeneralPreference dealerGeneralPreference = CreateDealerGeneralPreference();
            dealerGeneralPreference.Disclaimer = longDisclaimer;

            Assert.IsFalse( dealerGeneralPreference.IsValid );
        }

        [Test]
        [Category("Validation")]
        public void NegativeDaysInvalid()
        {
            DealerGeneralPreference dealerGeneralPreference = CreateDealerGeneralPreference();
            dealerGeneralPreference.DaysValidFor = -1;

            Assert.IsFalse(dealerGeneralPreference.IsValid);
        }

        [Test]
        [Category("Validation")]
        public void DaysGreaterThan14Invalid()
        {
            DealerGeneralPreference dealerGeneralPreference = CreateDealerGeneralPreference();
            dealerGeneralPreference.DaysValidFor = 15;

            Assert.IsFalse(dealerGeneralPreference.IsValid);
        }

        #endregion

        #region Save Tests

        [Test]
        [Category("Insert")]
        public void CanInsert()
        {
            ResetTestConditions();

            DealerGeneralPreference pref = CreateDealerGeneralPreference();
            DealerGeneralPreference saved = pref.Save();

            Assert.IsFalse(saved.IsNew);
            Assert.IsFalse(saved.IsDirty );

            Assert.AreEqual(pref.OwnerHandle, saved.OwnerHandle);
        }

        [Test]
        [Category("Update")]
        public void CanUpdate()
        {
            const string address = "updated address";

            ResetTestConditions();

            DealerGeneralPreference pref = CreateDealerGeneralPreference();
            DealerGeneralPreference saved = pref.Save();

            Assert.IsFalse(saved.IsNew);
            Assert.IsFalse(saved.IsDirty);

            // Update the preference
            saved.AddressLine1 = address;
            Assert.IsTrue( saved.IsDirty );
            Assert.IsFalse( saved.IsNew );

            DealerGeneralPreference updated = saved.Save();
            Assert.IsFalse( updated.IsDirty );
            
            Assert.AreEqual( address, updated.AddressLine1 );

            // Fetch and confirm.
            DealerGeneralPreferenceTO fetched = DataAccess.Fetch(pref.OwnerHandle);
            Assert.IsNotNull( fetched );
            Assert.AreEqual(pref.OwnerHandle, fetched.OwnerHandle);
            Assert.AreEqual(address, fetched.AddressLine1);
        }

        #endregion

        #region Private Helper Methods

        private static IDealerGeneralPreferenceDataAccess DataAccess
        {
            get { return new DealerGeneralPreferenceMockDataAccess(); }
        }

        private DealerGeneralPreference CreateDealerGeneralPreference()
        {
 
            return DealerGeneralPreference.CreateDealerGeneralPreference(_ownerHandle);

        }

        #endregion

    }
} 