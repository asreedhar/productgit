using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Marketing.AdditionalSettings.Types;
using FirstLook.Merchandising.DomainModel.Marketing.AdditionalSettings.Types.TransferObjects;

namespace PricingDomainModel_Test.AdditionalSettings
{
    /// <summary>
    /// Mock data access backed in memory.
    /// </summary>
    class DealerGeneralPreferenceMockDataAccess : IDealerGeneralPreferenceDataAccess
    {
        /// <summary>
        /// OwnerHandle -> DTO
        /// </summary>
        private static readonly IDictionary<string, DealerGeneralPreferenceTO> Preferences
            = new Dictionary<string, DealerGeneralPreferenceTO>();

        private static readonly IDictionary<string, DealerGeneralPreferenceTO> CreatePreferences
            = new Dictionary<string, DealerGeneralPreferenceTO>();


        static DealerGeneralPreferenceMockDataAccess()
        {
            CreatePreferences.Add("h", new DealerGeneralPreferenceTO("a1", "a2", "c", "s", "z", "http://pgatour.com", "e", "p", "d", 0, "d", "n", "h", false));
            CreatePreferences.Add("00000000-0000-0000-0000-000000000000", new DealerGeneralPreferenceTO(string.Empty, string.Empty, string.Empty, string.Empty,
                                                 string.Empty, string.Empty,
                                                 string.Empty, string.Empty, string.Empty, 0, string.Empty,
                                                 string.Empty, "00000000-0000-0000-0000-000000000000", true));


            CreatePreferences.Add("DealerInformationUnitTests", new DealerGeneralPreferenceTO("line 1", "line 2", "austin", "tx",
                                                 "12345", "http://pgatour.com",
                                                 "a@b.c", "123-456-7890", "I'm tired", 1, "evil laugh",
                                                 "name", "DealerInformationUnitTests", true));
        }


        public static void Clear()
        {
            Preferences.Clear();
        }

        public bool Exists(string ownerHandle)
        {
            string key = CreateKey(ownerHandle);
            return Preferences.ContainsKey(key);
        }

        public DealerGeneralPreferenceTO Create(string ownerHandle)
        {
            string key = CreateKey(ownerHandle);

            // Return object if it exists, otherwise return null.
            if (CreatePreferences.ContainsKey(key))
                return CreatePreferences[key];

            // If nothing found, throw.
            throw new ApplicationException("No preference could be found with the specified handle(s).");
        }

        public DealerGeneralPreferenceTO Fetch(string ownerHandle)
        {
            // Get key
            string key = CreateKey(ownerHandle);

            // Return object if it exists, otherwise return null.
            if (Preferences.ContainsKey(key))
                return Preferences[key];

            // If nothing found, throw.
            throw new ApplicationException("No preference could be found with the specified handle(s).");
        }

        public void Insert(DealerGeneralPreferenceTO preference, string userName)
        {
            // Construct key and increment id.
            string key = CreateKey(preference.OwnerHandle);

            // Save preference dto.
            Preferences.Add(key, preference);
        }

        public void Update(DealerGeneralPreferenceTO preference, string userName)
        {
            if (!Exists(preference.OwnerHandle))
            {
                throw new ApplicationException("Does not exist.");
            }

            string key = CreateKey(preference.OwnerHandle);

            Preferences[key] = preference;
        }

        public void Delete(DealerGeneralPreferenceTO preference, string userName)
        {
            // If the preference exists, delete it.
            if (!Exists(preference.OwnerHandle))
            {
                return;
            }

            string key = CreateKey(preference.OwnerHandle);
            Preferences.Remove(key);
        }

        private static string CreateKey(string ownerHandle)
        {
            return ownerHandle;
        }

        
        
    }
}