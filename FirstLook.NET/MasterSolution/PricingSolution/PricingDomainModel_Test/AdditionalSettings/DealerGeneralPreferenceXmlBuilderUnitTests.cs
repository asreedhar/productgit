using System;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.Marketing.AdditionalSettings;
using FirstLook.Merchandising.DomainModel.Marketing.AdditionalSettings.Types;
using NUnit.Framework;
using Autofac;

namespace PricingDomainModel_Test.AdditionalSettings
{
    [TestFixture]
    public class DealerGeneralPreferenceXmlBuilderUnitTests
    {
        private readonly string _today = DateTime.Now.ToShortDateString();
       
        private string Expected()
        {
             return "<dealer ownerHandle=\"h\"><contact><address>a1 a2</address><city>c</city><email>e</email><phone>p</phone><state>s</state><zip>z</zip></contact>" +
                    "<description>d</description><name>n</name><offerValidUntil>" + _today + @"</offerValidUntil><disclaimer>d</disclaimer><url>http://pgatour.com</url>" +
                    "<displayContactInfoOnWebPdf>false</displayContactInfoOnWebPdf></dealer>";
        }

        private readonly string _empty = "<dealer ownerHandle=\"00000000-0000-0000-0000-000000000000\"><contact><address></address><city></city><email></email><phone></phone><state></state><zip></zip></contact><description></description><name></name><offerValidUntil>" + DateTime.Today.ToShortDateString() + "</offerValidUntil><disclaimer></disclaimer><url></url><displayContactInfoOnWebPdf>true</displayContactInfoOnWebPdf></dealer>";

        [TestFixtureSetUp]
        public void Setup()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<DealerGeneralPreferenceMockDataAccess>().As<IDealerGeneralPreferenceDataAccess>();
            Registry.RegisterContainer(builder.Build());
        }

        [Test]
        public void AsXmlReturnsExpected()
        {
            DealerGeneralPreference dealerGeneralPreference = DealerGeneralPreference.CreateDealerGeneralPreference("h");

            string xml = DealerGeneralPreferenceXmlBuilder.AsXml(dealerGeneralPreference);

            Console.WriteLine(xml);

            Assert.AreEqual( Expected(), xml );            
        }

        [Test]
        public void ReturnsEmptyXml()
        {
            DealerGeneralPreference pref =
                DealerGeneralPreference.GetOrCreateDealerGeneralPreference(Guid.Empty.ToString());

            string xml = DealerGeneralPreferenceXmlBuilder.AsXml(pref);

            Console.WriteLine( xml );

            Assert.AreEqual( _empty, xml );
        }

    }
}
