using System;
using System.Web.Routing;
using System.Web.Compilation;
using System.Web;
using System.Web.UI;
using System.Web.Security;
using System.Security.Principal;
using System.Net;
using Autofac;
using Autofac.Integration.Web;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.DomainModel.Oltp;
using FirstLook.Common.PhotoServices;
using FirstLook.Common.Core.IOC;
using Merchandising.Messages;
using MerchandisingRegistry;
using PricingPresenters;
using FirstLook.Common.Core.Command;
using FirstLook.Fault.DomainModel.Utilities;
using FirstLook.Common.Core.Diagnostics;

namespace FirstLook.Pricing.WebApplication
{
    public class Global : HttpApplication, IContainerProviderAccessor
    {
        private static IContainerProvider _containerProvider;

        protected void Application_Start( object sender, EventArgs e )
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new CoreModule());
            builder.RegisterModule(new OltpModule());
            builder.RegisterModule(new PhotoServicesModule());
            builder.RegisterModule(new MerchandisingDomainModule());
            builder.RegisterModule(new MerchandisingMessagesModule());

            new PricingPresentersRegister().Register(builder);

            IContainer container = builder.Build();
            Registry.RegisterContainer( container );
            _containerProvider = new ContainerProvider( container );

            RegisterRoutes( RouteTable.Routes );

            //Added for fault reporting (health montioring) integration

            IRegistry registry = RegistryFactory.GetRegistry();

            registry.Register<Fault.DomainModel.Module>();

            registry.Register<IDataSessionManager, DataSessionManager>( ImplementationScope.Shared );

            //Register command related classes
            registry.Register<DomainModel.Commands.Module>();
            registry.Register<ICommandExecutor, ListeningCommandExecutor>(ImplementationScope.Isolated);
        }

        //Used for HTTP modules in Autofac.Integration.Web
        IContainerProvider IContainerProviderAccessor.ContainerProvider
        {
            get { return _containerProvider; }
        }

        private static void RegisterRoutes( RouteCollection routes )
        {

            routes.Add( "SalesAccelerator",
                new Route
                (
                    "Pages/MaxMarketing/{page}.aspx",
                    new PageRouteHandler( "~/Pages/SalesAccelerator/{page}.aspx" )
                ) );

            routes.Add( "SalesAcceleratorPreferences",
                new Route
                (
                    "Pages/MaxMarketing/Preferences/Dealer/{page}.aspx",
                    new PageRouteHandler( "~/Pages/SalesAccelerator/Preferences/Dealer/{page}.aspx" )
                ) );

            routes.Add( "PdfCreator",
                new Route
                (
                    "Pages/MaxMarketing/CreatePdf/{page}.aspx",
                    new PageRouteHandler( "~/Pages/SalesAccelerator/Print.aspx" )
                ) );
        }

        protected void Application_End( object sender, EventArgs e )
        {
        }

        /*This class is in .NET 4.0. When we move to it we can use the existing one*/
        private class PageRouteHandler : IRouteHandler
        {
            private string _virtualPath;

            public PageRouteHandler( string virtualPath )
            {
                if ( string.IsNullOrEmpty( virtualPath ) || !virtualPath.StartsWith( "~/", StringComparison.OrdinalIgnoreCase ) )
                    throw new ArgumentException( "Invalid VirtualPath", "virtualPath" );

                _virtualPath = virtualPath;
            }

            private string ProcessVirtualPath( RouteValueDictionary values )
            {
                string virtualPath = _virtualPath;
                foreach ( var key in values.Keys )
                {
                    string formattedKey = String.Format( @"{{{0}}}", key );
                    virtualPath = virtualPath.Replace( formattedKey, ( string )values[ key ] );
                }

                return virtualPath;
            }

            private bool CheckUrlAccess( string virtualPath, RequestContext requestContext )
            {
                IPrincipal user = requestContext.HttpContext.User;
                if ( user == null )
                    user = new GenericPrincipal( new GenericIdentity( string.Empty, string.Empty ), new string[ 0 ] );

                return UrlAuthorizationModule.CheckUrlAccessForPrincipal( virtualPath, user, requestContext.HttpContext.Request.HttpMethod );
            }

            IHttpHandler IRouteHandler.GetHttpHandler( RequestContext requestContext )
            {
                string virtualPath = ProcessVirtualPath( requestContext.RouteData.Values );

                if ( !CheckUrlAccess( virtualPath, requestContext ) )
                    return new UrlAuthFailerHandler();

                return BuildManager.CreateInstanceFromVirtualPath( virtualPath, typeof( Page ) ) as IHttpHandler;
            }

            private class UrlAuthFailerHandler : IHttpHandler
            {
                public bool IsReusable
                {
                    get
                    {
                        return true;
                    }
                }

                public void ProcessRequest( HttpContext context )
                {
                    context.Response.StatusCode = ( int )HttpStatusCode.Unauthorized;
                    context.Response.End();
                }
            }
        }

        #region Nested type: ListeningCommandExecutor

        private class ListeningCommandExecutor : SimpleCommandExecutor
        {
            public ListeningCommandExecutor()
            {
                Add(new FaultWebExceptionObserver());
                Add(new PerformanceMonitoringObserver());
            }
        }

        #endregion

    }
}
