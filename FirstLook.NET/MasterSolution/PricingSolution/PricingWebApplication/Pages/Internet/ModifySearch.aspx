<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ModifySearch.aspx.cs" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.ModifySearch" Theme="Leopard" %>

<%@ OutputCache Location="None" VaryByParam="None" %>

<%@ Register Src="~/Pages/Internet/Controls/VehicleInformation/VehicleInformation.ascx" TagPrefix="Controls" TagName="VehicleInformation" %>
<%@ Register Src="~/Pages/Internet/Controls/MarketListing.ascx" TagPrefix="Controls" TagName="MarketListing" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Modify Search</title>
</head>
<body>
<form id="vpa" class="vpa_modify_search" runat="server">
    <owc:HedgehogTracking ID="Hedgehog" runat="server" />
    <asp:ScriptManager ID="MyScriptManager" EnablePartialRendering="true" runat="server" LoadScriptsBeforeUI="false" ScriptPath="/resources/Scripts">
    	<Scripts>
    	    <asp:ScriptReference Path="~/Public/Scripts/Lib/Combined.js" NotifyScriptLoaded="true" />
    	    <asp:ScriptReference Path="~/Public/Scripts/Lib/DD_Roundies.js" NotifyScriptLoaded="true" />
    	    <asp:ScriptReference Path="~/Public/Scripts/Pages/global.js" NotifyScriptLoaded="true" />
    	    <asp:ScriptReference Path="~/Public/Scripts/Pages/ModifySearch.js" NotifyScriptLoaded="true" />
		</Scripts>
    </asp:ScriptManager>
    
    <div id="wrap" class="clearfix">
        <div id="hd">
            <Controls:VehicleInformation ID="VehicleInformation" runat="server" />
	    </div>
	    
	    <div id="search_wrapper" class="clearfix">
	        <div id="search_controls">
	            <asp:UpdatePanel ID="SearchUpdatePanel" runat="server">
	                <ContentTemplate>	        
    	                <h2>Precision Search</h2>
    	                <dl class="precision_search_results search_results">
    	                    <dt>results <%--  This space is needed, because the Update Panel borks of spaces, PM --%></dt>
    	                    <dd>
    	                        <asp:Literal ID="NumberOfResultsLiteral" runat="server" Text="Unknown" />
    	                    </dd>
    	                </dl>
    	                
    	                <hr />
    	                
    	                <dl class="search_results overall_search_results">
						    <dt>Overall Search Results</dt>
						    <dd><asp:Literal ID="NumberOfOverallResults" runat="server" Text="Unknown" /></dd>
                        </dl>
                        
    	                <asp:ObjectDataSource ID="SearchObjectDataSource" runat="server" 
    	                        TypeName="FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search.Search+DataSource"
    	                        SelectMethod="Select">
    	                    <SelectParameters>
    	                        <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
    	                        <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="vehicleHandle" Type="String" QueryStringField="vh" />
                                <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="searchHandle" Type="String" QueryStringField="sh" />
    	                    </SelectParameters>
    	                </asp:ObjectDataSource>
    	                <pwc:SearchDataView ID="SearchDataView" runat="server" 
    	                        DataSourceID="SearchObjectDataSource"
    	                        SliderHandleImageUrl="../../Public/Images/distance_slider_handle.png" />
	                    <hr />
        	            
    	                <asp:HiddenField ID="ModelYearHf" runat="server" />
                        <asp:HiddenField ID="MakeHf" runat="server" />
                        <asp:HiddenField ID="LineHf" runat="server" />
                        
                       
                        <asp:Label ID="CatalogDVSelectionMessageLabel" runat="server" 
                                CssClass="user_message"
                                Visible="false" />
                        <cwc:FadingUserMessageLabel ID="CatalogDVSelectionMessageLabelFadingExtender" runat="server"
                                TargetControlID="CatalogDVSelectionMessageLabel" />
                  

    	                <owc:CatalogDataSource ID="CatalogDS" runat="server">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="ModelYearHf" Name="ModelYearId" Type="Int32" />
                                <asp:ControlParameter ControlID="MakeHf" Name="MakeId" Type="Int32" />
                                <asp:ControlParameter ControlID="LineHf" Name="LineId" Type="Int32" />
                            </SelectParameters>
                        </owc:CatalogDataSource>
                        
                        <owc:CatalogFacetDataView ID="CatalogFacetDv" CssClass="catalog_data_view" 
                            runat="server" DataSourceID="CatalogDS"
                            AutoGenerateResetButton="true"
                            AutoGenerateRestoreButton="true"
                            AutoGenerateSaveButton="true"
                            OnCatalogDataBound="CatalogDvCatalogDataBound"
                            OnCatalogSelectionRestore="CatalogDVSelectionRestore"
                            OnCatalogSelectionReset="CatalogDVSelectionReset"
                            OnCatalogSelectionUpdated="CatalogDVSelectionUpdated"
                            OnCatalogSelectionMessage="CatalogDVSelectionMessage" />
	                </ContentTemplate>
	            </asp:UpdatePanel>
	        </div>
	    
	        <div id="results_feedback">
	        <asp:UpdatePanel runat="server" ID="CatalogUpdatePanel">
	            <ContentTemplate>
	                <div class="branding">
	                    <h3>ping iii <small>Advanced Market Search</small></h3>
	                    <asp:HyperLink runat="server" ID="ReturnToVpaHyperLink" CssClass="return_to_vpa" Text="return to {vpa branding}" />
	                </div>
    	            
	                <div class="market_overview content_group clearfix">
	                    <div class="header">
	                        <h3>Market Summary</h3>
	                        <p class="text_summary">
	                            <strong><asp:Literal runat="server" ID="SummaryNumberOfResultsLiteral" Text="Unknown" /></strong>
	                            precision listings out of 
	                            <em><asp:Literal runat="server" ID="SummaryNumberOfOverallResultsLiteral" Text="Unknown" /></em>
	                            overall results
	                        </p>
	                        
	                     <asp:ObjectDataSource ID="OwnerPreferencesDataSource" runat="server"
	                                TypeName="FirstLook.Pricing.DomainModel.Internet.ObjectModel.OwnerPreferences+DataSource"
	                                SelectMethod="Select">
	                           <SelectParameters>
	                                <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
	                           </SelectParameters>
	                    </asp:ObjectDataSource>
	                     
                         <pwc:MarketSummaryTable 
	                        ID="MarketSummaryPricingTable"
	                        CssClass="pricing_summary_table"
	                        runat="server"
	                        DataSourceID="MarketListingObjectDataSource"
	                        ColumnName="Market Pricing"
	                        ShowReference="true"
	                        UseOwnerPreference="true"
	                        ValueFormat="{0:$#,0;($#,0);--}"
	                        OnDataBinding="MarketSummaryPricingTable_DataBinding"
	                        OnMarketSummaryTableDataBound="MarketSummaryPricingTable_MarketSummaryTableDataBound" />
	                        
	                     <pwc:MarketSummaryTable 
	                        ID="MarketSummaryMileageTable"
	                        CssClass="mileage_summary_table"
	                        runat="server"
	                        DataSourceID="MarketListingObjectDataSource"
	                        ColumnName="Market Mileage"
	                        ShowReference="true"
	                        ValueFormat="{0:#,0;(#,0);--}"
	                        OnMarketSummaryTableDataBound="MarketSummaryMileageTable_MarketSummaryTableDataBound" />
	                        	                        
	                    </div>
	                </div>           	                	               	                
	                
	                <asp:ObjectDataSource
                            ID="MarketListingObjectDataSource"
                            runat="server"
                            TypeName="FirstLook.Pricing.DomainModel.Internet.DataSource.MarketListingDataSource"
                            SelectMethod="FindByOwnerHandleAndSearchHandle"
                            EnableCaching="true"
                            CacheDuration="60"
                            CacheExpirationPolicy="Sliding"
                            CacheKeyDependency="MarketListingObjectDataSourceCacheKey"
                            SortParameterName="sortColumns"
                            FilterExpression="ModelConfigurationID IN ({0}) AND ListingCertified IN ({1}) AND VehicleColor LIKE '{2}' OR IsAnalyzedVehicle=1"
                            OnFiltering="MarketListingObjectDataSourceFiltering"
                            OnSelected="MarketListingObjectDataSourceSelected">
                         <SelectParameters>
                            <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
                            <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="searchHandle" Type="String" QueryStringField="sh" />
                            <asp:Parameter ConvertEmptyStringToNull="True" Name="searchTypeId" Type="Int32" DefaultValue="1" />
                         </SelectParameters>
                         <FilterParameters>
                            <asp:ControlParameter ControlID="CatalogFacetDv" PropertyName="ModelConfigurationFilter" DefaultValue="-1" Type="string" />
                            <asp:Parameter Name="ListingCertified" DefaultValue="True,False" Type="string" />
                            <asp:Parameter Name="VehicleColor" DefaultValue="%" Type="string" />
                            <%--<asp:Parameter Name="PrevNextConfigIds" DefaultValue="null" Type="string" />--%>
                         </FilterParameters>
                    </asp:ObjectDataSource>
                    
					<h3>Market Listings</h3>
					<asp:Label ID="ModelConfigurationIDLabel" runat="server" Visible="false" EnableViewState="false" />
                    <asp:GridView
					        ID="MarketListingGridView"
					        runat="server"
					        DataSourceID="MarketListingObjectDataSource"
					        Width="720px"
					        PageSize="25"
					        AlternatingRowStyle-CssClass="odd"
					        AutoGenerateColumns="false"
					        AllowPaging="true"
					        AllowSorting="true"
					        CssClass="grid small"
					        EnableViewState="false"
					        OnRowDataBound="MarketListingGridViewRowDataBound"
					        OnRowCreated="MarketListingGridViewRowCreated">
				        <PagerStyle CssClass="gridPager" />
				        <Columns>
					        <asp:BoundField
						        DataField="Age"
						        HeaderText="Age"
						        SortExpression="Age" />
					        <asp:BoundField
						        DataField="VehicleDescription"
						        HeaderText="Vehicle Information"
						        SortExpression="VehicleDescription" />
					        <asp:BoundField
						        DataField="ListingVin"
						        HeaderText="Vin"
						        SortExpression="ListingVin" />
					        <asp:BoundField
						        DataField="ListingCertified"
						        HeaderText="Certified"
						        HtmlEncode="false"
						        SortExpression="ListingCertified" />
					        <asp:BoundField
						        DataField="VehicleColor"
						        HeaderText="Color"
						        SortExpression="VehicleColor" />
					        <asp:BoundField
						        DataField="VehicleMileage"
						        DataFormatString="{0:#,###,###;--;--}"
						        HeaderText="Mileage"
						        SortExpression="VehicleMileage"
						        HtmlEncode="False" />
					        <asp:BoundField
						        DataField="ListPrice"
						        DataFormatString="{0:$#,##0;--;--}"
						        HeaderText="Internet Price"
						        SortExpression="ListPrice"
						        ConvertEmptyStringToNull="true"
						        NullDisplayText="--"
						        HtmlEncode="False" />
					        <asp:BoundField
						        DataField="PctAvgMarketPrice"
						        HeaderText="% Market Avg."
						        DataFormatString="{0:#' %';--;--}"
						        HtmlEncode="false"
						        SortExpression="PctAvgMarketPrice" />
					         <asp:BoundField
						        DataField="DistanceFromDealer"
						        HeaderText="Distance"
						        DataFormatString="{0:#### mi;--;--}"
						        HtmlEncode="false"
						        SortExpression="DistanceFromDealer" />
						     <asp:BoundField DataField="ModelConfigurationID" SortExpression="ModelConfigurationID" />
				        </Columns>
			        </asp:GridView>
	                
	            </ContentTemplate>
	        </asp:UpdatePanel>
	        </div>	    
	    </div>
    </div>
    
    <cwc:PageFooter ID="SiteFooter" runat="server" ShowChromeCopyright="True" />
    
    <div id="progress" style="display: none;">
        <div>
            <img src="../../Public/Images/loading.gif" alt="Saving Search..." /> Please Wait...
        </div>
    </div>
</form>
</body>
</html>
