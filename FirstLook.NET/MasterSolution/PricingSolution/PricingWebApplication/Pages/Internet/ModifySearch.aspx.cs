using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.DomainModel.Lexicon.Categorization;
using FirstLook.DomainModel.Lexicon.Categorization.WebControls;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;
using FirstLook.Pricing.WebControls;
using FirstLook.Pricing.DomainModel;

namespace FirstLook.Pricing.WebApplication.Pages.Internet
{
    public partial class ModifySearch : Page
    {
        private const string ReturnLinkKey = "ModifySearch-Returnlink";
        private const string AppraisalLink = "Return to Appraisal";

        #region Request Properties

        private string OwnerHandle
        {
            get { return Request.QueryString["oh"]; }
        }

        private string VehicleHandle
        {
            get { return Request.QueryString["vh"]; }
        }

        private string SearchHandle
        {
            get { return Request.QueryString["sh"]; }
        }

        private string GenerateCacheKey()
        {
            return OwnerHandle + ":" + VehicleHandle + ":" + SearchHandle + ":OverallSearchCount";
        }

        private int? OverallSearchCount
        {
            get
            {
                return (int?) Cache[GenerateCacheKey()];
            }
            set
            {
                Cache.Insert(
                    GenerateCacheKey(),
                    value,
                    null,
                    Cache.NoAbsoluteExpiration,
                    new TimeSpan(0, 0, MarketListingObjectDataSource.CacheDuration));
            }
        }

        private int SearchCount
        {
            get
            {
                return MarketSummaryPricingTable.NumberOfListings;
            }
        }

        private bool IsDebug
        {
            get
            {
                return Request.QueryString["debug"] == "please";
            }
        }

        #endregion

        private Search _search;

        private Search Search
        {
            get
            {
                if (_search == null)
                {
                    _search = SearchHelper.GetSearch(Context);
                }
                return _search;
            }
        }


        private ReturnLinkState ReturnState
        {
            get
            {
                return ViewState["ReturnLinkURl"] as ReturnLinkState;
            }
            set
            {
                ViewState["ReturnLinkURl"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ReturnState = InitializeReturnLink(Request);

                if (!ReturnState.IsPricing)
                    ReturnToVpaHyperLink.Text = AppraisalLink;

                ModelYearHf.Value = Search.ModelYear.ToString(CultureInfo.InvariantCulture);
                MakeHf.Value = Search.MakeId.ToString(CultureInfo.InvariantCulture);
                LineHf.Value = Search.LineId.ToString(CultureInfo.InvariantCulture);

                CatalogFacetDv.VinPattern = Search.VinPattern;
                CatalogFacetDv.ModelConfigurationId = Search.ModelConfigurationId;

                Page.Cache["MarketListingObjectDataSourceCacheKey"] = DateTime.Today;
            }
            else
            {
                CatalogDVSelectionMessageLabel.Visible = false;
            }

            ReturnToVpaHyperLink.NavigateUrl = ReturnState.RefererLink;
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (IsDebug)
            {
                MarketListingGridView.AllowPaging = false;
                ModelConfigurationIDLabel.Visible = true;
            }

            ApplyPageConfiguration();

            Page.PreRenderComplete += Page_PreRenderComplete;
        }

        private void Page_PreRenderComplete(object sender, EventArgs e)
        {
            UpdatePageCounts();
        }

        private void ApplyPageConfiguration()
        {
            PackageDetails details = PackageDetails.GetDealerPricingPackageDetails(OwnerHandle, SearchHandle,
                                                                                   VehicleHandle);
            Page.Title = details.PageBrand;
            ReturnToVpaHyperLink.Text = "return to " + details.PageBrand;
        }

        protected void CatalogDvCatalogDataBound(object sender, CatalogDataBoundEventArgs e)
        {
            bool selected = false, target = false;

            selected |= (Search.CatalogEntries.GetItem(e.ModelConfigurationId) != null);

            target |= (e.ModelConfigurationId == Search.ModelConfigurationId);

            e.Selected = selected;

            e.Target = target;
        }

        protected void CatalogDVSelectionRestore(object sender, EventArgs e)
        {
            Search.RestorePrecisionSearch(SearchHelper.GetSearchKey(Context));

            Response.Redirect(SearchHelper.GetModifySearchUrl(Request), true);
        }

        protected void CatalogDVSelectionReset(object sender, EventArgs e)
        {
            Response.Redirect(ReturnState.RefererLink, true);
        }

        protected void CatalogDVSelectionUpdated(object sender, EventArgs e)
        {
            LoadComplete += SaveSearch;
        }

        private void SaveSearch(object sender, EventArgs e)
        {
            LoadComplete -= SaveSearch;

            int distanceValue;

            if (int.TryParse(SearchDataView.Distance, out distanceValue))
            {
                foreach (Distance distance in Search.Distances)
                {
                    if (distance.Id == distanceValue)
                    {
                        Search.Distance = distance;
                    }
                }
            }

            int mileageValue;

            if (int.TryParse(SearchDataView.MileageStart, out mileageValue))
            {
                foreach (Mileage mileage in Search.Mileages)
                {
                    if (mileage.Value == mileageValue)
                    {
                        Search.LowMileage = mileage;
                    }
                }
            }

            if (int.TryParse(SearchDataView.MileageEnd, out mileageValue))
            {
                foreach (Mileage mileage in Search.Mileages)
                {
                    if (mileage.Value == mileageValue)
                    {
                        Search.HighMileage = Mileage.NewMileage(mileage.Value - 1);
                    }
                }
            }
            else if (SearchDataView.MileageEnd == "max")
            {
                Search.HighMileage = null;
            }

            Search.MatchCertified = SearchDataView.MatchCertified;

            Search.MatchColor = SearchDataView.MatchColor;

            //Search.PreviousYear = SearchDataView.PreviousYearFlag;                //31079

            //Search.NextYear = SearchDataView.NextYearFlag;                        //31079


            Search.CatalogEntries.Clear();

            foreach (ModelConfiguration modelConfiguration in CatalogFacetDv.ModelConfigurations)
            {
                Search.CatalogEntries.Assign(modelConfiguration.Id);
            }

            Search.UpdateUser = User.Identity.Name;

            Search.Save();

            //While updating , remove all the keys from memcache which were generated while getting the marketListing.----------------------
            PricingAnalyticsClient.ClearMemCache(OwnerHandle);
           
            //Need this for temporary storage because we are redirecting the page and the links in the page need to know where you came from
            //so we can redirect you back to the appraisal page for example
            Session[ReturnLinkKey] = ReturnState;
            Response.Redirect(SearchHelper.GetModifySearchUrl(Request), true);
        }

        protected void ReturnToVpaLinkButtonClick(object sender, EventArgs e)
        {
            Response.Redirect(ReturnState.RefererLink, true);
        }

        protected void MarketListingObjectDataSourceSelected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue != null)
            {
                DataTable table = e.ReturnValue as DataTable;

                if (table != null)
                {
                    OverallSearchCount = table.Rows.Count;
                }
            }
        }

        protected void UpdatePageCounts()
        {
            NumberOfResultsLiteral.Text = string.Format("{0:d}", SearchCount);

            SummaryNumberOfResultsLiteral.Text = string.Format("{0:d}", SearchCount);

            if (OverallSearchCount.HasValue)
            {
                NumberOfOverallResults.Text = string.Format("{0:d}", OverallSearchCount);

                SummaryNumberOfOverallResultsLiteral.Text = string.Format("{0:d}", OverallSearchCount);
            }
        }

        private IVehicle _vehicle;
        private IVehicle GetVehicle()
        {
            if (_vehicle == null)
            {
                _vehicle = VehicleHelper.GetVehicle(OwnerHandle, VehicleHandle);
            }

            return _vehicle;
        }

        protected void MarketListingGridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if ((bool)DataBinder.GetPropertyValue(e.Row.DataItem, "IsAnalyzedVehicle"))
                {
                    e.Row.CssClass += " analyzed";
                    if (!IsPostBack)
                    {
                        OverallSearchCount -= 1; // Don't Count You, PM 
                    }
                }

                e.Row.Cells[3].CssClass = (e.Row.Cells[3].Text == "False") ? "not_certified" : "certified";
            }
        }

        protected void MarketSummaryMileageTable_MarketSummaryTableDataBound(object sender, MarketSummaryTableDataBoundEventArgs e)
        {
            IVehicle vehicle = GetVehicle();

            if (vehicle.Mileage.HasValue)
            {
                if (e.You != vehicle.Mileage.Value)
                {
                    e.ClassName = "market_value_different";
                    e.Title = "Market Listing Mileage Different From Vehicle";
                }

                e.You = vehicle.Mileage.Value;
            }
        }

        protected void MarketSummaryPricingTable_MarketSummaryTableDataBound(object sender, MarketSummaryTableDataBoundEventArgs e)
        {
            IVehicle vehicle = GetVehicle();

            IInventory inventory = vehicle as IInventory;

            if (inventory != null)
            {
                if (inventory.ListPrice.HasValue)
                {
                    if (e.You != inventory.ListPrice)
                    {
                        e.ClassName = "market_value_different";
                        e.Title = "Market Listing Price Different From Vehicle";
                    }

                    e.You = inventory.ListPrice.Value;
                }
            }
        }

        protected void MarketListingObjectDataSourceFiltering(object sender, ObjectDataSourceFilteringEventArgs e)
        {
            IVehicle vehicle = GetVehicle();

            if (SearchDataView.MatchColor)
            {
                e.ParameterValues["VehicleColor"] =  vehicle.ExteriorColor;
            }

            if (SearchDataView.MatchCertified)
            {
                e.ParameterValues["ListingCertified"] = SearchDataView.MatchCertified;
            }

            //string prevNextConfigIds;                                                 //31079
            //if (CatalogFacetDv.ModelConfigurationFilter != null)
            //{
            //    prevNextConfigIds = Search.FindPrevNxtYearsConfigIds(CatalogFacetDv.ModelConfigurationFilter, SearchHandle);
            //    if (prevNextConfigIds != null)
            //    {
            //        e.ParameterValues["CurrentConfigIds"] = prevNextConfigIds;
            //    }
            //} 

            ModelConfigurationIDLabel.Text = CatalogFacetDv.ModelConfigurationFilter;
        }

        protected void MarketSummaryPricingTable_DataBinding(object sender, EventArgs e)
        {
            IEnumerator enumerator = OwnerPreferencesDataSource.Select().GetEnumerator();
            if (enumerator.MoveNext())
            {
                MarketSummaryPricingTable.OwnerPreferences = (OwnerPreferences) enumerator.Current;
            }
        }

        protected void MarketListingGridViewRowCreated(object sender, GridViewRowEventArgs e)
        {
            e.Row.Cells[e.Row.Cells.Count - 1].Visible = IsDebug;
        }

        protected void CatalogDVSelectionMessage(object sender, CatalogSelectionMessageEventArgs e)
        {
            CatalogDVSelectionMessageLabel.Visible = true;
            CatalogDVSelectionMessageLabel.Text = e.Message;
        }

        private ReturnLinkState InitializeReturnLink(HttpRequest request)
        {
            if (Session[ReturnLinkKey] != null)
            {
                var returnState = Session[ReturnLinkKey];
                Session[ReturnLinkKey] = null;
                return returnState as ReturnLinkState;
            }

            //hack to go back to java pages that link into ping.
            string referer = request.Headers["Referer"];

            //13Jan14: hack to set return url to Trademanager BUZID:24772
            if (!String.IsNullOrEmpty(referer) && Regex.IsMatch(referer, "/GuideBookStepThreeSubmitAction.go", RegexOptions.IgnoreCase))
            {
                string link = referer.Replace("GuideBookStepThreeSubmitAction", "TradeManagerEditDisplayAction") + "?pageName=bullpen&appraisalId={0}";
                link = string.Format(link, VehicleHandle.Substring(1,VehicleHandle.Length-1));

                return new ReturnLinkState() { RefererLink = link, IsPricing = false };
            }

            if (String.IsNullOrEmpty(referer) || Regex.IsMatch(referer, "/pricing/", RegexOptions.IgnoreCase))
            {
                string link = string.Format("~/Pages/Internet/VehiclePricingAnalyzer.aspx?oh={0}&vh={1}&sh={2}",
                                     request.QueryString["oh"],
                                     request.QueryString["vh"],
                                     request.QueryString["sh"]);

                return new ReturnLinkState(){RefererLink = link, IsPricing = true};
            }
     

            return new ReturnLinkState() {RefererLink = referer, IsPricing = false};
        }

        [Serializable]
        private class ReturnLinkState
        {
            public string RefererLink { get; set; }
            public bool IsPricing { get; set; }
        }
    }
}