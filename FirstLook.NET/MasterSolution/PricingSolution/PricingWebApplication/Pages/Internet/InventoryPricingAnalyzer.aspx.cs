using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Utilities;
using FirstLook.Common.WebControls.UI.Chart;
using FirstLook.DomainModel.Oltp;
using FirstLook.Pricing.DomainModel.Internet.DataSource;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel;
using FirstLook.Pricing.WebApplication.Pages.Internet.Controls;
using FirstLook.Pricing.WebControls;
using FirstLook.Reports.App.ReportDefinitionLibrary;
using FirstLook.Reports.GoogleAnalytics;
using MsReportViewer = Microsoft.Reporting.WebForms.ReportViewer;
using MsReportParameter = Microsoft.Reporting.WebForms.ReportParameter;
using MsReportDataSource = Microsoft.Reporting.WebForms.ReportDataSource;
using MsWarning = Microsoft.Reporting.WebForms.Warning;



namespace FirstLook.Pricing.WebApplication.Pages.Internet
{
    public partial class InventoryPricingAnalyzer : Page, IGAnalytics
    {
        private string _dealerNameText;
        private bool _isFirstlookDealer;
        private bool _hasJDPowerUpgrade;
        private bool _hasEStockCard;
        private int _inventoryTableRowCount;
        private bool _resetTablePageIndex;
        private OwnerPreferences _ownerPreferenceses;
        private string _maxDigitalShowRoomUrl;

        protected OwnerPreferences OwnerPreferences
        {
            get
            {
                if (_ownerPreferenceses == null)
                {
                    _ownerPreferenceses = OwnerPreferences.GetOwnerPreferences(Request.QueryString["oh"]);
                }

                return _ownerPreferenceses;
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            IsAnalyicsEnabled = true;
            this.PageTitle = "Inventory Pricing Analyzer";
        }

        protected void Page_Init(object s, EventArgs e)
        {
            if (IsAnalyicsEnabled)
            {
                Page.GetGoogleAnalyticsScript();
            }

            _isFirstlookDealer = false;

            DataTable ownerNameTable = new OwnerNameDataSource().FindByOwnerHandle(Request.QueryString["oh"]);

            foreach (DataRow row in ownerNameTable.Rows)
            {
                _dealerNameText = Convert.ToString(row["OwnerName"]);

                _isFirstlookDealer = Int32Helper.ToInt32(row["OwnerType"]) == 1;
            }

            _hasJDPowerUpgrade = Identity.HasJdPowerUpgrade(User.Identity.Name, Request.QueryString["oh"]);

            _hasEStockCard = Identity.HasEStockCard(Request.QueryString["oh"]);

            _inventoryTableRowCount = 0;

            if (!IsPostBack && !string.IsNullOrEmpty(Request.QueryString["q"]))
            {
                stockOrModelTextBox.Text = Request.QueryString["q"];
                StockOrModelSearchButton_Click(s, e);
            }

            if (ShowSellerMenu())
            {
                application_menu_container.Visible = false;
                member_menu_container.Visible = false;
                seller_menu_container.Visible = true;

                MarketStockingHyperlink.Visible = true;
                MarketStockingHyperlink.NavigateUrl = "~/Pages/Market/Default.aspx?oh=" + Request.QueryString["oh"];
            }
        }

        protected void Page_Load(object s, EventArgs e)
        {
            DealerName.Text = _dealerNameText;

            PlanningReportLinks.Visible = _isFirstlookDealer;

            SaleStrategyContainer.Visible = _isFirstlookDealer;

            FilterColumnLabel.Visible = true;

            FilterColumnList.Visible = true;

            InventoryTypeLiteral.Text = _isFirstlookDealer ? "Current Inventory" : "Internet Listings";

            EnablePricingGraph();

            Page.LoadComplete += Page_LoadComplete;
        }

        protected void Page_LoadComplete(object s, EventArgs e)
        {
            if (IsCustomSearch())
            {
                SetCustomSearch();
            }
        }

        protected void EnablePricingGraph()
        {
            if (string.Equals(ModeList.SelectedItem.Value, "A"))
            {
                PricingBarChart.Visible = true;
                RiskBarChart.Visible = false;

                FilterColumnLabel.Visible = true;
                FilterColumnList.Visible = true;
                InventoryFactsRepeater.Visible = true;
            }
            else if (string.Equals(ModeList.SelectedItem.Value, "R"))
            {
                PricingBarChart.Visible = false;
                RiskBarChart.Visible = true;

                FilterColumnLabel.Visible = true;
                FilterColumnList.Visible = true;
                InventoryFactsRepeater.Visible = true;
            }
            else if (string.Equals(ModeList.SelectedItem.Value, "N"))
            {
                PricingBarChart.Visible = false;
                RiskBarChart.Visible = false;

                FilterColumnLabel.Visible = false;
                FilterColumnList.Visible = false;
                InventoryFactsRepeater.Visible = false;
            }

        }

        protected void InventoryTableGridView_Load(object s, EventArgs e)
        {
            InventoryTableGridView.Columns[InventoryTableHelper.ColumnIndex(InventoryTableHelper.Risk)].Visible =
                _isFirstlookDealer;
            InventoryTableGridView.Columns[InventoryTableHelper.ColumnIndex(InventoryTableHelper.UnitCost)].Visible =
                _isFirstlookDealer;
            InventoryTableGridView.Columns[InventoryTableHelper.ColumnIndex(InventoryTableHelper.PrecisionTrimSearch)].Visible =
            !OwnerPreferences.SuppressTrimMatchStatus;

            InventoryTableGridView.Columns[InventoryTableHelper.ColumnIndex(InventoryTableHelper.AvgPinSalePrice)].Visible =
                _hasJDPowerUpgrade;
        }

        protected void InventoryTableGridView_RowDataBound(object s, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell th in e.Row.Cells)
                {
                    if (th.HasControls())
                    {
                        LinkButton lnk = (LinkButton)th.Controls[0];
                        if (lnk != null && InventoryTableGridView.SortExpression == lnk.CommandArgument)
                            th.CssClass += (InventoryTableGridView.SortDirection == SortDirection.Ascending ? "ascending" : "descending");
                    }
                }
            }
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // set up the risk lights

                SetColumnRiskLight(e);

                //Set images in tha last column ------ New case : 31755

                SetPriceComparisionImage(e);

                // setup the navigation url

                HyperLink vehiclePricingAnalyzerLink = e.Row.FindControl("VehiclePricingAnalyzerLink") as HyperLink;

                if (vehiclePricingAnalyzerLink != null)
                {
                    vehiclePricingAnalyzerLink.Attributes.Add("onclick", "OpenPopup(this.href,this.target);return false;");
                }

                // call attention to % of market average if higher than 100%

                FormatMarketPercent(e);

                FormatPrecisionTrimSearch(e);

                SetEstockCardEnabled(e);

                // align columns

                SetColumnAlignment(e);
            }
        }

        private static void FormatPrecisionTrimSearch(GridViewRowEventArgs e)
        {
            int idx = InventoryTableHelper.ColumnIndex(InventoryTableHelper.PrecisionTrimSearch);
            bool isSeriesMatch;
            string text = DataBinder.GetPropertyValue(e.Row.DataItem, "PrecisionTrimSearch", null);
            if (bool.TryParse(text, out isSeriesMatch) && isSeriesMatch)
            {
                e.Row.Cells[idx].CssClass = " series_match is_series_match";
                e.Row.Cells[idx].Text = "Yes";
                e.Row.Cells[idx].ToolTip = "All Vehicles Match Your Trim";
            }
            else
            {
                e.Row.Cells[idx].CssClass = " series_match";
                e.Row.Cells[idx].Text = "No";
            }
        }

        private static void SetColumnAlignment(GridViewRowEventArgs e)
        {
            int[] centerAlignedColumnIndices = {
                                                   InventoryTableHelper.ColumnIndex(InventoryTableHelper.Age),
                                                   InventoryTableHelper.ColumnIndex(InventoryTableHelper.Risk),
                                                   InventoryTableHelper.ColumnIndex(InventoryTableHelper.VehicleColor),
                                               InventoryTableHelper.ColumnIndex(InventoryTableHelper.PctAvgMarketPrice),
                                               InventoryTableHelper.ColumnIndex(InventoryTableHelper.ChangePctAvgMarketPrice)
                                               };

            int[] rightAlignedColumnIndices = {
                                                  InventoryTableHelper.ColumnIndex(InventoryTableHelper.VehicleMileage),
                                                  InventoryTableHelper.ColumnIndex(InventoryTableHelper.UnitCost),
                                                  InventoryTableHelper.ColumnIndex(InventoryTableHelper.ListPrice),
                                              InventoryTableHelper.ColumnIndex(InventoryTableHelper.AvgPinSalePrice),
                                              InventoryTableHelper.ColumnIndex(InventoryTableHelper.MarketDaysSupply),
                                                  InventoryTableHelper.ColumnIndex(InventoryTableHelper.StockNumber)
                                              };


            foreach (int i in centerAlignedColumnIndices)
            {
                e.Row.Cells[i].CssClass += " alignCenter";
            }
            foreach (int i in rightAlignedColumnIndices)
            {
                e.Row.Cells[i].CssClass += " alignRight";
            }

        }

        private void SetEstockCardEnabled(GridViewRowEventArgs e)
        {
            if (_hasEStockCard)
            {
                HyperLink eStockCardHyperlink = e.Row.FindControl("EStockCardHyperlink") as HyperLink;

                if (eStockCardHyperlink != null)
                {
                    eStockCardHyperlink.Attributes.Add("onclick", string.Format("OpenPopup(this.href,this.target,document.getElementById('{0}').click);return false;", refreshButton.ClientID));
                }
            }
            else
            {
                HyperLink eStockCardHyperlink = e.Row.FindControl("EStockCardHyperlink") as HyperLink;

                if (eStockCardHyperlink != null)
                {
                    eStockCardHyperlink.Enabled = false;
                }
            }
        }

        private static void FormatMarketPercent(GridViewRowEventArgs e)
        {
            int idx = InventoryTableHelper.ColumnIndex(InventoryTableHelper.PctAvgMarketPrice);

            string pct = e.Row.Cells[idx].Text;

            if (!string.IsNullOrEmpty(pct) && pct.EndsWith("%"))
                pct = (pct.Length == 1) ? string.Empty : pct.Substring(0, pct.Length - 2);

            if ("&nbsp;".Equals(pct))
                pct = string.Empty;

            if (string.IsNullOrEmpty(pct) || Int32Helper.ToInt32(pct) > 100)
            {
                e.Row.Cells[idx].CssClass += " warning";
            }

            if (string.IsNullOrEmpty(pct))
            {
                e.Row.Cells[idx].Text = "N/A";
            }
        }

        private static void SetColumnRiskLight(GridViewRowEventArgs e)
        {
            DataRowView rowView = (DataRowView)e.Row.DataItem;

            int riskLight = Int32Helper.ToInt32(rowView["RiskLight"]);

            HtmlGenericControl riskLightImage = (HtmlGenericControl)e.Row.FindControl("RiskLightImage");

            if (riskLight == 3)
            {
                riskLightImage.Attributes["class"] += " light green";
                riskLightImage.Attributes["title"] = "Low Risk";
            }
            else if (riskLight == 2)
            {
                riskLightImage.Attributes["class"] += " light yellow";
                riskLightImage.Attributes["title"] = "Medium Risk";
            }
            else if (riskLight == 1)
            {
                riskLightImage.Attributes["class"] += " light red";
                riskLightImage.Attributes["title"] = "High Risk";
            }
            else
            {
                riskLightImage.Visible = false;
            }
        }

        private void SetPriceComparisionImage(GridViewRowEventArgs e)
        {
            _maxDigitalShowRoomUrl = ConfigurationManager.AppSettings["logo_vehicle_pricecomparison_root_url"];

            string[] namesArray = ((DataRowView)e.Row.DataItem)["PriceComparisons"].ToString().Split(',');

            List<string> namesList = new List<string>(namesArray.Length);
            namesList.AddRange(namesArray);

            if (namesList.Count(nl => nl.ToLower().Contains("kbb")) > 1)
            {
                string match = namesList.FirstOrDefault(stringToCheck => stringToCheck.Contains("KBB Consumer"));
                namesList.Remove(match);
            }

            string make = ((DataRowView)e.Row.DataItem)["Make"].ToString();

            HtmlGenericControl litActual2 = (HtmlGenericControl)e.Row.FindControl("ComparisonColor");
            DataRowView rowView = (DataRowView)e.Row.DataItem;
            int comparisonColor = Int32Helper.ToInt32(rowView["ComparisonColor"]);
            if (comparisonColor == 0)
                litActual2.Attributes["class"] += " red-background";
            if (comparisonColor == 1)
                litActual2.Attributes["class"] += " green-background";

            if (comparisonColor == 2)
                litActual2.Attributes["class"] += " yellow-background";

            Literal litActual = (Literal)e.Row.FindControl("LtPriceComparison");
            litActual.Text = null;
            if (namesList.Count != 0 && namesList[0] != "0" && namesList[0] != "")
            {
                for (int i = 0; i < namesList.Count; i++)
                {
                    if (namesList[i].Trim() == "")
                        continue;
                    if ((i != 0) && (i % 2 == 0))
                    {
                        litActual.Text += "<br/>";
                    }

                    //if (namesList[i].ToLower().Contains("kbb consumer"))
                    //{

                    //    litActual.Text += @"<img src=""../../App_Themes/Leopard/Images/NEW_LOGO.gif""></img>" + namesList[i] +
                    //                      " ";
                    //    continue;

                    //}

                    // In both the cases , KBBConsumer and KBB , it will display KBB only
                    if (namesList[i].ToLower().Contains("kbb"))
                    {
                        litActual.Text += @"<img src=""../../App_Themes/Leopard/Images/kbb_icon.png"" class=""image-dimension""></img>" + namesList[i].Replace("Consumer", "") +
                                          " ";
                        continue;
                    }
                    if (namesList[i].ToLower().Contains("nada"))
                    {
                        litActual.Text += @"<img src=""../../App_Themes/Leopard/Images/nada_icon.png"" class=""image-dimension""></img>" +
                                          namesList[i] + " ";
                        continue;
                    }
                    if (namesList[i].ToLower().Contains("msrp"))
                    {

                        string msrpurl = ConfigurationManager.AppSettings["msrp"];
                        string url = ConfigurationManager.AppSettings[make];
                        if (url != null)
                        {
                            litActual.Text += "<img src=" + string.Format("{0}{1}", _maxDigitalShowRoomUrl, url) +
                                              @" class=""msrp-logo image-dimension""></img>" + namesList[i] + " ";
                        }
                        else
                        {
                            litActual.Text += "<img src=" + string.Format("{0}{1}", _maxDigitalShowRoomUrl, msrpurl) +                                               @" class=""msrp-logo image-dimension""></img>" + namesList[i] + " ";
                        }
                        continue;
                    }
                    if (namesList[i].ToLower().Contains("edmunds"))
                    {
                        litActual.Text += @"<img src=""../../App_Themes/Leopard/Images/edmunds.png""                                                                         class=""image-dimension""></img>" + namesList[i] + " ";
                        continue;
                    }
                    if (namesList[i].ToLower().Contains("market"))
                    {
                        litActual.Text += @"<img src=""../../App_Themes/Leopard/Images/avgmarket.png""
                                                           class=""image-dimension""></img>" + namesList[i] + " ";
                    }

                }
            }
            else
            {
                litActual.Text = "N/A";
            }

        }

        protected void InventoryTableGridViewRowCount_PreRender(object sender, EventArgs e)
        {
            StringBuilder showingText = new StringBuilder("Showing ");

            int totalRows = _inventoryTableRowCount;

            if (totalRows == 0)
                showingText.Append("no");
            else if (InventoryTableGridView.Rows.Count == 0)
                showingText.Append("0 to " + InventoryTableGridView.Rows.Count + " of " + totalRows);
            else if (totalRows <= InventoryTableGridView.PageSize)
                showingText.Append("1 to " + InventoryTableGridView.Rows.Count + " of " + totalRows);
            else if (totalRows > InventoryTableGridView.PageSize)
            {
                showingText.Append((InventoryTableGridView.PageIndex * InventoryTableGridView.PageSize) + 1);
                showingText.Append(" to ");
                showingText.Append((InventoryTableGridView.PageIndex * InventoryTableGridView.PageSize) + InventoryTableGridView.Rows.Count);
                showingText.Append(" of " + totalRows);
            }

            showingText.Append(" vehicles.");

            InventoryTableGridViewRowCount.Text = showingText.ToString();
        }

        protected void ModeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.Equals(ModeList.SelectedItem.Value, "A"))
            {
                FilterMode.Value = "R";
                ColumnIndex.Value = "0";
                FilterColumnIndex.Value = "0";
                GraphSubtitle.Text = "Inventory Units";
            }
            else if (string.Equals(ModeList.SelectedItem.Value, "R"))
            {
                FilterMode.Value = "A";
                ColumnIndex.Value = "3";
                FilterColumnIndex.Value = "0";
                GraphSubtitle.Text = "% Inventory";
            }
            else if (string.Equals(ModeList.SelectedItem.Value, "N"))
            {
                FilterMode.Value = "N";
                ColumnIndex.Value = "-1";
                FilterColumnIndex.Value = "0";
                GraphSubtitle.Text = "";
            }

            if (InventoryTableGridView.PageIndex != 0)
            {
                InventoryTableGridView.PageIndex = 0;
            }

            EnablePricingGraph();
            UnsetCustomSearch();
        }

        protected void ModeList_PreRender(object s, EventArgs e)
        {
            ModeLabel.Text = ModeList.SelectedItem.Text;
        }

        protected void SalesStrategyList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (InventoryTableGridView.PageSize != 0)
            {
                InventoryTableGridView.PageIndex = 0;
            }
            UnsetCustomSearch();
        }

        protected void SalesStrategyList_PreRender(object s, EventArgs e)
        {
            SalesStrategyLabel.Text = SaleStrategyList.SelectedItem.Text;
        }

        protected void FilterColumnList_PreRender(object s, EventArgs e)
        {
            FilterColumnLabel.Text = FilterColumnList.SelectedItem.Text;
        }

        protected void InventoryTableTitle_PreRender(object sender, EventArgs e)
        {
            if (IsCustomSearch())
            {
                if (string.IsNullOrEmpty(stockOrModelTextBox.Text))
                {
                    InventoryTableTitle.InnerText = "Results for Search: All Inventory";
                }
                else
                {
                    InventoryTableTitle.InnerText = string.Format("Results for Search: '{0}'", stockOrModelTextBox.Text);
                }
            }
            else if (string.Equals(ModeList.SelectedItem.Value, "N"))
            {
                InventoryTableTitle.InnerText = "All Inventory";
            }
            else
            {
                string column = new PricingGraphDataSource().FindNameByOwnerHandleAndModeAndColumnIndex(
                    Request.QueryString["oh"],
                    ModeList.SelectedItem.Value,
                    Int32Helper.ToInt32(ColumnIndex.Value));

                string filter = string.Equals(ModeList.SelectedItem.Value, "A") ? "Pricing Risk" : "Age";

                InventoryTableTitle.InnerText = string.Format("{0} By {1}", column, filter);
            }
        }

        protected void ReportExportExcel_Click(object sender, EventArgs e)
        {
            string format = "Excel";
            string deviceInfo = "<DeviceInfo><SimplePageHeaders>False</SimplePageHeaders></DeviceInfo>";
            int ExcelMaxRows = 65536; //max rows that excel 97/2000 can display

            MsReportViewer reportViewer = new MsReportViewer();
            // out parameters
            string mimeType, encoding, fileNameExtension;
            string[] streams;
            MsWarning[] warnings;

            //get report
            string reportPath = ConfigurationManager.AppSettings["FirstLook.Reports.App.ReportDefinitionLibrary.Xml.Serialization.ReportMetadata.Path"];
            ReportFactory factory = ReportFactory.NewReportFactory(reportPath);

            InventoryTableDataSource inventoryTableDataSource = new InventoryTableDataSource();

            IReport report = factory.FindReport(InventoryTableDataSource.Id);

            //bind report
            ILocalReportProcessingLocation location = (ILocalReportProcessingLocation)report.ReportProcessingLocation;
            reportViewer.LocalReport.LoadReportDefinition(ReportDefinitionCache.GetReportDefinition(location.File).GetReportStream());
            reportViewer.LocalReport.DataSources.Clear();

            IList<MsReportParameter> parameters = new List<MsReportParameter>();
            parameters.Add(new MsReportParameter("OwnerHandle", Request.QueryString["oh"]));
            parameters.Add(new MsReportParameter("Mode", ModeList.SelectedItem.Value));
            parameters.Add(new MsReportParameter("SaleStrategy", SaleStrategyList.SelectedItem.Value));
            parameters.Add(new MsReportParameter("ColumnIndex", ColumnIndex.Value));
            parameters.Add(new MsReportParameter("FilterMode", FilterMode.Value));
            parameters.Add(new MsReportParameter("FilterColumnIndex", FilterColumnList.SelectedIndex.ToString(CultureInfo.InvariantCulture) == "-1" ? 0.ToString(CultureInfo.InvariantCulture) : FilterColumnList.SelectedIndex.ToString(CultureInfo.InvariantCulture))); // Set parameter value to 0 in case of it is -1
            parameters.Add(new MsReportParameter("InventoryFilter", stockOrModelTextBox.Text));

            string sortColumns = InventoryTableGridView.SortExpression;
            if (string.IsNullOrEmpty(sortColumns))
            {
                sortColumns = FilterMode.Value.Equals("A") ? "Age" : "PctAvgMarketPrice";
            }
            if (InventoryTableGridView.SortDirection == SortDirection.Ascending)
            {
                sortColumns += " ASC";
            }
            else
            {
                sortColumns += " DESC";
            }

            parameters.Add(new MsReportParameter("SortColumns", sortColumns));
            parameters.Add(new MsReportParameter("MaximumRows", ExcelMaxRows.ToString()));
            parameters.Add(new MsReportParameter("StartRowIndex", "0"));
            parameters.Add(new MsReportParameter("InventoryFilter", stockOrModelTextBox.Text));

            reportViewer.LocalReport.SetParameters(parameters);

            DataTable dt = inventoryTableDataSource.FindByOwnerHandleAndModeAndColumnIndexAndFilters(
                Request.QueryString["oh"],
                ModeList.SelectedItem.Value,
                SaleStrategyList.SelectedItem.Value,
                Int32Helper.ToInt32(ColumnIndex.Value),
                FilterMode.Value,
                FilterColumnList.SelectedIndex < 0 ? 0 : FilterColumnList.SelectedIndex, // Added condition not to pass -1 as it gives the user defined exception
                sortColumns,
                ExcelMaxRows, 0,
                stockOrModelTextBox.Text);

            reportViewer.LocalReport.DataSources.Add(new MsReportDataSource(InventoryTableDataSource.TableDataSet, dt));
            reportViewer.LocalReport.DataSources.Add(new MsReportDataSource(InventoryTableDataSource.CountDataSet, dt));

            byte[] content = reportViewer.LocalReport.Render(
                format, deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);

            //IE6 bug: InventoryTableTitle.InnerText has a colon in it and IE6 mangled the file name.
            //Only keep alphanumeric characters. \\W (words) or 0-9 (digits)
            string filename = Regex.Replace(InventoryTableTitle.InnerText, "\\W|[0-9]", "");

            // http://support.microsoft.com/default.aspx?scid=kb;en-us;812935
            // write response
            Response.Clear();
            Response.ClearHeaders();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename + "." + fileNameExtension);
            Response.AddHeader("Content-Length", content.Length.ToString());
            Response.ContentType = mimeType;
            Response.BinaryWrite(content);
            Response.End();
        }

        protected void stockOrModelTextBox_TextChanged(object sender, EventArgs e)
        {
            StockOrModelSearchButton_Click(sender, e);
        }

        protected void StockOrModelSearchButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(stockOrModelTextBox.Text))
            {
                CustomSearchFlag.Value = bool.TrueString;

                _resetTablePageIndex = true;
            }
            else
            {
                UnsetCustomSearch();
            }
        }

        protected void FilterColumnList_SelectedIndexChanged(object sender, EventArgs e)
        {
            FilterColumnIndex.Value = FilterColumnList.SelectedIndex.ToString();
            if (InventoryTableGridView.PageIndex != 0)
            {
                InventoryTableGridView.PageIndex = 0;
            }
            UnsetCustomSearch();
        }

        protected void FilterColumnList_DataBound(object sender, EventArgs e)
        {
            int selectedIndex = Int32Helper.ToInt32(FilterColumnIndex.Value);
            if (selectedIndex != FilterColumnList.SelectedIndex)
            {
                FilterColumnList.SelectedIndex = selectedIndex;
            }
        }

        protected void PostBackButton_Click(object sender, EventArgs e)
        {
            if (InventoryTableGridView.PageIndex != 0)
            {
                InventoryTableGridView.PageIndex = 0;
            }
            UnsetCustomSearch();
        }

        protected void InventoryFactsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                // turn off days supply for seller
                HtmlContainerControl daysSupply = (HtmlContainerControl)e.Item.FindControl("InventoryFactsRepeater_DaySupply");
                daysSupply.Visible = _isFirstlookDealer;
            }
        }

        protected static int GetNullSafeInt(object o)
        {
            return Int32Helper.ToInt32(o);
        }

        protected static string FormatEmptyString(string value)
        {
            if (string.IsNullOrEmpty(value))
                return "N/A";
            return value;
        }

        /*  
         * Hack for the lame custom search context. 
         */

        protected bool IsCustomSearch()
        {
            return CustomSearchFlag.Value.Equals(bool.TrueString);
        }

        protected void SetCustomSearch()
        {
            if (_resetTablePageIndex)
            {
                InventoryTableGridView.SelectedIndex = -1;
                if (InventoryTableGridView.PageIndex != 0)
                {
                    InventoryTableGridView.PageIndex = 0;
                }
            }

            FilterColumnLabel.Visible = false;
            FilterColumnList.Visible = false;

            InventoryTableObjectDataSource.SelectParameters.Clear();
            InventoryTableObjectDataSource.SelectParameters.Add(new QueryStringParameter("ownerHandle", TypeCode.String,
                                                                                         "oh"));
            InventoryTableObjectDataSource.SelectParameters.Add(new Parameter("mode", TypeCode.String, "A"));
            InventoryTableObjectDataSource.SelectParameters.Add(new Parameter("saleStrategy", TypeCode.String, "A"));
            InventoryTableObjectDataSource.SelectParameters.Add(new Parameter("columnIndex", TypeCode.Int32, "-1"));
            InventoryTableObjectDataSource.SelectParameters.Add(new Parameter("filterMode", TypeCode.String, "R"));
            InventoryTableObjectDataSource.SelectParameters.Add(new Parameter("filterColumnIndex", TypeCode.Int32, "0"));
            Parameter inventoryFilterParameter = new ControlParameter("inventoryFilter", TypeCode.String,
                                                                      "stockOrModelTextBox", "Text");
            InventoryTableObjectDataSource.SelectParameters.Add(inventoryFilterParameter);
        }

        protected void UnsetCustomSearch()
        {
            CustomSearchFlag.Value = bool.FalseString;
            stockOrModelTextBox.Text = "";
            if (!string.Equals(ModeList.SelectedItem.Value, "N"))
            {
                FilterColumnLabel.Visible = true;
                FilterColumnList.Visible = true;
            }
        }

        protected void RefreshButton_Click(object sender, EventArgs e)
        {
            DataBindChildren();
        }

        private bool ShowSellerMenu()
        {
            return (Context.Items[BusinessUnit.HttpContextKey] == null);
        }

        protected void PricingBarChart_PreRender(object sender, EventArgs e)
        {
            int idx = 0;
            int activeColumnIndex;
            int.TryParse(ColumnIndex.Value, out activeColumnIndex);
            foreach (Bar bar in PricingBarChart.Bars)
            {
                if (idx++ == activeColumnIndex)
                {
                    bar.CssClass = "active_bar";
                }
                else
                {
                    bar.CssClass = bar.CssClass.Replace("active_bar", string.Empty); // Reset CssClass
                    bar.CssClass = bar.CssClass.Trim(' ');
                }
            }
        }

        protected void RiskBarChart_PreRender(object sender, EventArgs e)
        {
            int idx = 1;
            int activeColumnIndex;
            int.TryParse(ColumnIndex.Value, out activeColumnIndex);
            bool isCustomSearch = IsCustomSearch();
            foreach (Bar bar in RiskBarChart.Bars)
            {
                if (idx++ == activeColumnIndex && !isCustomSearch)
                {
                    bar.CssClass = "active_bar";
                }
                else
                {
                    bar.CssClass = bar.CssClass.Replace("active_bar", string.Empty); // Reset CssClass
                    bar.CssClass = bar.CssClass.Trim(' ');
                }
            }
        }

        protected void InventoryTableGridView_Command(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
                int pageIndex;
                if (int.TryParse((string)e.CommandArgument, out pageIndex))
                    if (InventoryTableGridView.PageIndex != pageIndex - 1)
                        InventoryTableGridView.PageIndex = pageIndex - 1;
            }
        }

        private void PopulateNextPrevLink(VehicleNavigationItem item, object dataItem, int? newPageIndex)
        {
            item.VehicleText = DataBinder.Eval(dataItem, "StockNumber", "{0}");
            item.VehicleTooltip = DataBinder.Eval(dataItem, "VehicleDescription", "{0}");
            item.VehicleUrl = string.Format("~/Pages/Internet/VehiclePricingAnalyzer.aspx?oh={0}&vh={1}&sh={2}",
                                            Request.QueryString["oh"], DataBinder.Eval(dataItem, "VehicleHandle"), DataBinder.Eval(dataItem, "SearchHandle"));

            if (newPageIndex.HasValue)
            {
                item.GridViewId = InventoryTableGridView.ClientID;
                item.NewPageIndex = newPageIndex;
            }
        }

        protected void InventoryTableObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is Int32)
            {
                _inventoryTableRowCount = (Int32)e.ReturnValue;
            }
            else
            {
                DataTable dataTable = e.ReturnValue as DataTable;

                if (dataTable != null)
                {
                    if (_mayHaveNext && dataTable.DefaultView.Count > 1)
                    {
                        _mayHaveNext = false;

                        PopulateNextPrevLink(NextVehicleNavigationItem,
                                             dataTable.DefaultView[dataTable.DefaultView.Count - 1],
                                             InventoryTableGridView.PageIndex + 2);
                    }

                    if (_hasPrevious)
                    {
                        _hasPrevious = false;
                        PopulateNextPrevLink(PrevVehicleNavigationItem, dataTable.DefaultView[0],
                                             InventoryTableGridView.PageIndex);
                        dataTable.DefaultView[0].Delete();
                    }
                }
            }
        }

        private bool _hasPrevious;
        private bool _mayHaveNext;

        protected void InventoryTableObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            int maxRows = 25;

            if (InventoryTableGridView.PageIndex != 0)
            {
                _hasPrevious = true;
                e.Arguments.StartRowIndex -= 1;
                maxRows++;
            }

            // Unfortunatly, at this point in the lifecycle InventoryTableGridView.PageCount will always be 0 or the current page
            if (InventoryTableGridView.PageIndex + 1 != InventoryTableGridView.PageCount)
            {
                _mayHaveNext = true;
                maxRows++;
            }

            e.Arguments.MaximumRows = maxRows;
        }

        #region IGAnalytics Members

        public string PageTitle
        {
            get;
            set;
        }

        public bool IsAnalyicsEnabled
        {
            get;
            set;
        }

        #endregion
    }
}
