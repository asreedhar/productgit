<%@ Page Language="C#" AutoEventWireup="True" MaintainScrollPositionOnPostback="true" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.VehiclePricingAnalyzer" EnableEventValidation="false" Codebehind="VehiclePricingAnalyzer.aspx.cs" Theme="Leopard" %>
    
<%@ OutputCache Location="None" VaryByParam="None" %>

<%@ Register Src="Controls/VehicleInformation/VehicleInformation.ascx" TagPrefix="Controls" TagName="VehicleInformation" %>
<%@ Register Src="Controls/Calculator/Calculator.ascx" TagPrefix="Controls" TagName="Calculator" %>
<%@ Register Src="Controls/SimilarInventory.ascx" TagPrefix="Controls" TagName="SimilarInventory" %>
<%@ Register Src="Controls/MarketListing.ascx" TagPrefix="Controls" TagName="MarketListing" %>
<%@ Register Src="Controls/PricingInformation.ascx" TagPrefix="Controls" TagName="PricingInformation" %>
<%@ Register Src="Controls/SearchSummary.ascx" TagPrefix="Controls" TagName="SearchSummary" %>
<%@ Register Src="Controls/MerchandisingInformation.ascx" TagPrefix="Controls" TagName="MerchandisingInformation" %>
<%@ Register Src="Controls/MysteryShopping.ascx" TagPrefix="Controls" TagName="MysteryShopping" %>
<%@ Register Src="Controls/VehicleHistoryReport.ascx" TagPrefix="Controls" TagName="VehicleHistoryReport" %>
<%@ Register Src="Controls/SearchSummaryTray.ascx" TagPrefix="Controls" TagName="SearchSummaryTray" %>
<%@ Register Src="Controls/VehicleNavigator.ascx" TagPrefix="Controls" TagName="VehicleNavigator" %>

<!DOCTYPE html>
<html class="no-js">
<head runat="server">
        <meta id="XUACompatibleTag" http-equiv="X-UA-Compatible" content="IE=Edge" runat="server"/>
        <link rel="stylesheet" href="/pricing/prototype/Css/global.css">
        <link rel="stylesheet" href="/pricing/prototype/Css/layout.css">
        <link id="Stylesheet_FL30" rel="stylesheet" href="/pricing/prototype/Css/FL_3.0.css" runat="server">
        <script type="text/javascript" src="/pricing/prototype/Views/GaugeComponent.js"></script>
        <script type="text/javascript" src="/pricing/prototype/Globals/SupportCombined.js"></script>
<!--[if IE]>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.1)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.1)">
<![endif]-->
    <title></title>
</head>
<!--[if lt IE 7]>      <body class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <body class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <body class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <body> <!--<![endif]-->
    <form id="vpa" runat="server"> 
		 <script type="text/javascript">

		     $(document).ready(function () {

		         GaugeComponent.mysteryShoppingUrlsChanged(function (values) {
		             if (_.isUndefined(values.autoTraderUrl) == false) {
		                 $("#AutoTraderHyperLink").attr("href", values.autoTraderUrl);
		             }

		             if (_.isUndefined(values.carsDotComUrl) == false) {
		                 $("#CarsDotComHyperLink").attr("href", values.carsDotComUrl);
		             }

		             if (_.isUndefined(values.carSoupUrl) == false) {
		                 $("#CarsoupHyperlink").attr("href", values.carSoupUrl);
		             }
		         });

		     });

        </script>
        <asp:ScriptManager ID="scriptManager" EnablePartialRendering="true" EnablePageMethods="true" runat="server" LoadScriptsBeforeUI="false" ScriptPath="/resources/Scripts">
            <Scripts>
                <asp:ScriptReference Path="~/Public/Scripts/Lib/Combined.js" NotifyScriptLoaded="true" />
                <asp:ScriptReference Path="~/Public/Scripts/Lib/jquery.ui.datepicker.js" NotifyScriptLoaded="true" />
                <asp:ScriptReference Path="~/Public/Scripts/Lib/DD_Roundies.js" NotifyScriptLoaded="true" />
                <asp:ScriptReference Path="~/Public/Scripts/Pages/global.js" NotifyScriptLoaded="true" />
                <asp:ScriptReference Path="~/Public/Scripts/Pages/VPA.debug.js" NotifyScriptLoaded="true" />
            </Scripts>
        </asp:ScriptManager>

        <div id="wrap" class="clearfix">
            <Controls:VehicleNavigator ID="VehicleNavigator" runat="server" />
		    <div id="hd">
    		    <asp:UpdatePanel ID="VehicleInformationUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
    		            <Controls:VehicleInformation ID="VehicleInformation" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
		    </div>
		    
		    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
				<ContentTemplate>
				    <asp:ObjectDataSource ID="NationalAuctionPanelDataSource" runat="server" TypeName="FirstLook.Pricing.DomainModel.Internet.DataSource.NationalAuctionParametersDataSource" SelectMethod="FindByOwnerHandleAndVehicleHandle">
                        <SelectParameters>
                            <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
                            <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="vehicleHandle" Type="String" QueryStringField="vh" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
				    <cwc:TabContainer ID="HeaderTabs" CssClass="vpa_header_info_tabs" runat="server" HasEmptyState="true">
				        <cwc:TabPanel ID="NationalAuctionTab" Text="NAAA Auction" CssClass="national_auction_tab" runat="server">
				            <owc:NationalAuctionPanel ID="NationalAuctionPanel" runat="server" DataSourceID="NationalAuctionPanelDataSource" CssClass="vpa_auction_data" />
				        </cwc:TabPanel>
				        <cwc:TabPanel ID="SelectedEquipmentTab" Text="Selected Equipment" CssClass="selected_equipment_tab" runat="server">
				            <cwc:TabContainer ID="SelectedEquipmentTabContainer" runat="server" CssClass="vpa_selected_equipment" OnInit="SelectedEquipmentTabContainer_Init">
				                <!-- populated in the oninit callback -->
                            </cwc:TabContainer>
				        </cwc:TabPanel>
				        <cwc:TabPanel ID="UnitsInStockTab" Text='<%# UnitsInStockText %>' CssClass="units_in_stock_tab" runat="server">
				            <Controls:SimilarInventory ID="SimilarInventory" runat="server" />
				        </cwc:TabPanel>
				    </cwc:TabContainer>
				</ContentTemplate>					
			</asp:UpdatePanel>
    		
    		<asp:UpdatePanel ID="SearchTypeUpdatePanel" runat="server" UpdateMode="Conditional">
    		    <ContentTemplate>
    		        <asp:HiddenField ID="SearchTypeID" runat="server" Value="1" OnInit="SearchTypeID_Init" />
    		    </ContentTemplate>
    		</asp:UpdatePanel>
    		
    		<asp:ObjectDataSource
                    ID="MarketPricingListObjectDataSource"
                    runat="server"
                    TypeName="FirstLook.Pricing.DomainModel.Internet.DataSource.MarketPricingDataSource"
                    SelectMethod="FindListByOwnerHandleAndSearchHandle">
                <SelectParameters>
                    <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
                    <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="searchHandle" Type="String" QueryStringField="sh" />
                    <asp:ControlParameter ConvertEmptyStringToNull="True" Name="searchTypeId" Type="Int32" ControlID="SearchTypeID" />
                </SelectParameters>
            </asp:ObjectDataSource>
            
            <div id="CVATabs" runat="server">
                <ul class="selected0">
                    <li id="IAATab"><a href="#" onclick="return false;"><span><asp:Literal ID="PageBrandingTabLiteral" runat="server" Mode="Encode"></asp:Literal></span></a></li>
                    <li id="CVATab"><a href="<%= Request.ApplicationPath + "/Pages/MaxMarketing/Preview.aspx?" + Request.QueryString %>"><span>MAX MARKETING</span></a></li>
                </ul>
            </div>
            
            <h1 id="page_branding" runat="server">
            	<asp:Literal runat="server" ID="PageBranding" Mode="Encode" />
        	</h1>

            <div id="gaugeComponent">
            </div>

            <asp:Panel ID="legacyCalculator" runat="server">
    		    <asp:UpdatePanel ID="CalculatorUpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
    		            <div id="center_row" class="clearfix">
    		                <div id="mktPrce">
		                        <div class="inner1">
		                            <div class="inner2">
			                            <asp:ObjectDataSource
                                                ID="MarketPricingObjectDataSource"
                                                runat="server"
                                                TypeName="FirstLook.Pricing.DomainModel.Internet.DataSource.MarketPricingDataSource"
                                                SelectMethod="FindByOwnerHandleAndVehicleHandleAndSearchHandle"
                                                OnObjectCreated="MarketPricingObjectDataSource_ObjectCreated">
                                            <SelectParameters>
                                                <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
                                                <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="vehicleHandle" Type="String" QueryStringField="vh" />
                                                <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="searchHandle" Type="String" QueryStringField="sh" />
                                                <asp:ControlParameter ConvertEmptyStringToNull="True" Name="searchTypeId" Type="Int32" ControlID="SearchTypeID" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                        <Controls:Calculator ID="Calculator" runat="server" DataSourceID="MarketPricingObjectDataSource" OnPriceChanging="Calculator_PriceChanging" OnPriceChanged="Calculator_PriceChanged" />
        		                    </div>
                                </div>
                            </div>
	                        <Controls:SearchSummary ID="SearchSummary" runat="server" OnSearchChanged="SearchSummary_SearchChanged" OnSearchTypeChanged="SearchSummary_SearchTypeChanged" />
			            </div>
			            <Controls:SearchSummaryTray ID="SearchSummaryTray" runat="server" />
				        </ContentTemplate>
                    </asp:UpdatePanel>

                <asp:UpdatePanel ID="MarketListingsUpdatePanel" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>
                        <asp:ObjectDataSource
                                ID="MarketListingObjectDataSource"
                                runat="server"
                                TypeName="FirstLook.Pricing.DomainModel.Internet.DataSource.MarketListingDataSource"
                                SelectMethod="FindByOwnerHandleAndSearchHandle"
                                SelectCountMethod="CountByOwnerHandleAndSearchHandle"
                                EnablePaging="true"
                                MaximumRowsParameterName="maximumRows"
                                StartRowIndexParameterName="startRowIndex"
                                SortParameterName="sortColumns">
                             <SelectParameters>
                                <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
                                <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="searchHandle" Type="String" QueryStringField="sh" />
                                <asp:ControlParameter ConvertEmptyStringToNull="True" Name="searchTypeId" Type="Int32" ControlID="SearchTypeID" />
                             </SelectParameters>
                        </asp:ObjectDataSource>
                        <asp:ObjectDataSource
                                ID="SearchMarketListingObjectDataSource"
                                runat="server"
                                TypeName="FirstLook.Pricing.DomainModel.Internet.DataSource.MarketListingDataSource"
                                SelectMethod="FindVehicleByOwnerHandleAndSearchHandleAndSortColumns"
                                EnablePaging="true"
                                MaximumRowsParameterName="maximumRows"
                                StartRowIndexParameterName="startRowIndex"
                                SortParameterName="sortColumns">
                             <SelectParameters>
                                <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
                                <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="searchHandle" Type="String" QueryStringField="sh" />
                                <asp:ControlParameter ConvertEmptyStringToNull="True" Name="searchTypeId" Type="Int32" ControlID="SearchTypeID" />
                             </SelectParameters>
                        </asp:ObjectDataSource>
                        <Controls:MarketListing ID="MarketListings" runat="server" DataSourceID="MarketListingObjectDataSource" SearchDataSourceID="SearchMarketListingObjectDataSource" />
                    </ContentTemplate>
                </asp:UpdatePanel>
			</asp:Panel>

			<asp:UpdatePanel ID="PricingPanelsUpdatePanel" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <asp:ObjectDataSource
							ID="PricingInformationDataSource"
                            runat="server"
							TypeName="FirstLook.Pricing.DomainModel.Internet.ObjectModel.PricingInformation+DataSource"
							SelectMethod="Select">
                        <SelectParameters>
                            <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
							<asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="vehicleHandle" Type="String" QueryStringField="vh" />
                            <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="searchHandle" Type="String" QueryStringField="sh" />
                            <asp:ControlParameter ConvertEmptyStringToNull="True" Name="searchTypeId" Type="Int32" ControlID="SearchTypeID" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
					<Controls:PricingInformation ID="PricingPanels" runat="server" DataSourceID="PricingInformationDataSource" OnDataBinding="PricingPanels_DataBinding" />
				</ContentTemplate>
			</asp:UpdatePanel>
			
			<Controls:MysteryShopping ID="MysteryShopping" runat="server" Visible="false" />
			<Controls:VehicleHistoryReport ID="VehicleHistoryReport" runat="server" Visible="false" />
			
			<asp:UpdatePanel ID="MerchandisingInformationUpdatePanel" runat="server" UpdateMode="Conditional">   		
    		    <ContentTemplate>
    		        <asp:ObjectDataSource 
    		                ID="AdvertisementDataSource" 
    		                runat="server"
    		                TypeName="FirstLook.Pricing.DomainModel.Internet.ObjectModel.Document.Advertisement+DataSource"
    		                SelectMethod="Select"
    		                UpdateMethod="Update"
    		                DeleteMethod="Delete">
    		            <SelectParameters>
    		                <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
                            <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="vehicleHandle" Type="String" QueryStringField="vh" />
    		            </SelectParameters>
    		            <UpdateParameters>
    		                <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
                            <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="vehicleHandle" Type="String" QueryStringField="vh" />
    		            </UpdateParameters>
    		            <DeleteParameters>
    		                <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
                            <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="vehicleHandle" Type="String" QueryStringField="vh" />
    		            </DeleteParameters>
    		        </asp:ObjectDataSource>
    		        <asp:ObjectDataSource 
    		                ID="NotesDataSource" 
    		                runat="server"
    		                TypeName="FirstLook.Pricing.DomainModel.Internet.ObjectModel.Document.Notes+DataSource"
    		                SelectMethod="Select"
    		                UpdateMethod="Update"
    		                DeleteMethod="Delete">
    		            <SelectParameters>
    		                <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
                            <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="vehicleHandle" Type="String" QueryStringField="vh" />
    		            </SelectParameters>
    		            <UpdateParameters>
    		                <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
                            <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="vehicleHandle" Type="String" QueryStringField="vh" />
    		            </UpdateParameters>
    		            <DeleteParameters>
    		                <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
                            <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="vehicleHandle" Type="String" QueryStringField="vh" />
    		            </DeleteParameters>
    		        </asp:ObjectDataSource>
                    <Controls:MerchandisingInformation ID="MerchandisingInformation" runat="server" AdvertisementDataSourceID="AdvertisementDataSource" NotesDataSourceID="NotesDataSource" />
    		    </ContentTemplate>
    		</asp:UpdatePanel>
            
	    </div>
	    
	    <cwc:PageFooter ID="SiteFooter" runat="server" ShowChromeCopyright="True" />
	    
        <div id="progress">
            <div>
                <img src="../../Public/Images/loading.gif" alt="Loading ... Please Wait" /> Please wait...
            </div>
        </div>
        
        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-1230037-1']);
            _gaq.push(['_setCustomVar', 1, 'BusinessUnitID', '<%= BusinessUnitID %>', 3]);
            _gaq.push(['_setCustomVar', 2, 'HasFirstLook3', '<%= HasFirstLook3 %>', 3]);
            _gaq.push(['_trackPageview']);

            (function () {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
        </script>
    </form>
</body>
</html>
