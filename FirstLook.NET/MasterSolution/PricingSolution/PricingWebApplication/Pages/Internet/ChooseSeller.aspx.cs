using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Utilities;
using FirstLook.Pricing.DomainModel.Internet.DataSource;

namespace FirstLook.Pricing.WebApplication.Pages.Internet
{
    public partial class ChooseSeller : Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!User.IsInRole("Administrator"))
            {
                Response.Redirect("~/Default.aspx", true);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SellerTableGridView.Visible = true;

            AlphaSearchLinks.Visible = true;
        }

        protected void SellerTableGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow row = e.Row;

            if (row.RowType == DataControlRowType.DataRow)
            {
                PopulateStatusTemplate(row, ((DataRowView)row.DataItem).Row, false);
            }
        }

        protected void SellerTableGridView_DataBound(object s, EventArgs e)
        {
            if (SellerTableGridView.Rows.Count < 1)
            {
                SellerTableGridView.Visible = false;

                AlphaSearchLinks.Visible = !"0".Equals(ColumnIndex.Value);
            }
        }

        protected void SellerTableObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is Int32)
            {
                NumberOfSearchResults.Text = Convert.ToString(e.ReturnValue);
            }
        }

        protected void SubmitPricingSearchForm_Click(object sender, EventArgs e)
        {
            SellerTableGridView.PageIndex = 0;
        }

        protected void SellerTableGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Process"))
            {
                int selectedIndex = Int32Helper.ToInt32(e.CommandArgument);

                GridView grid = sender as GridView;

                if (grid != null)
                {
                    LinkButton button = (LinkButton)grid.Rows[selectedIndex].Cells[5].Controls[0];

                    int units = Int32Helper.ToInt32(button.Text);

                    if (units > 0)
                    {
                        HiddenField seller = grid.Rows[selectedIndex].FindControl("SellerID") as HiddenField;

                        HiddenField status = grid.Rows[selectedIndex].FindControl("AggregationStatusID") as HiddenField;

                        HiddenField handle = grid.Rows[selectedIndex].FindControl("OwnerHandle") as HiddenField;

                        if (status != null && seller != null && handle != null)
                        {
                            int statusId = Int32Helper.ToInt32(status.Value);

                            if (statusId == 3)
                            {
                                Response.Redirect(string.Format("~/Pages/Internet/ProcessSeller.aspx?oh={0}", handle.Value), true);
                            }
                            else
                            {
                                SellerInventoryAggregationDataSource dataSource = new SellerInventoryAggregationDataSource();

                                DataTable table =
                                    dataSource.FindBySellerIdAndJDPowerRegionId(seller.Value,
                                                                                JDPowerRegionDropDownList.SelectedValue);

                                PopulateStatusTemplate(grid.Rows[selectedIndex], table.Rows[0], true);
                            }
                        }
                    }
                }
            }
        }

        private void PopulateStatusTemplate(GridViewRow gridRow, DataRow dataRow, bool update)
        {
            if (!update)
            {
                ((HiddenField)gridRow.FindControl("SellerID")).Value = dataRow["SellerID"].ToString();
            }

            ((HiddenField)gridRow.FindControl("OwnerHandle")).Value = dataRow["OwnerHandle"].ToString();

            ((HiddenField)gridRow.FindControl("AggregationStatusID")).Value = dataRow["AggregationStatusID"].ToString();

            int statusId = Int32Helper.ToInt32(dataRow["AggregationStatusID"]);

            string statusText = "";

            if (statusId == 0)
                statusText = "Not Ready";
            else if (statusId == 1)
                statusText = string.Format("Queued: {0} of {1}", dataRow["PositionInQueue"], dataRow["ItemsQueued"]);
            else if (statusId == 2)
                statusText = string.Format("In Progress ({0}s)", dataRow["EstimatedSecondsToCompletion"]);
            else if (statusId == 3)
                statusText = string.Format("Ready");
            else if (statusId == 4)
                statusText = string.Format("Error");

            ((Label)gridRow.FindControl("StatusText")).Text = statusText;
        }
    }
}