﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VehicleNavigationItem.ascx.cs" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.Controls.VehicleNavigationItem" %>

<asp:HyperLink ID="VehiclePricingAnalyzerLink" runat="server"
    CssClass="pingIcon" 
    ImageUrl="~/Public/Images/ping_icon.gif" 
    Target="VPA" />
