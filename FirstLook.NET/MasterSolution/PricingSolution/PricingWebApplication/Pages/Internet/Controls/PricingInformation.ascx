<%@ Control Language="C#" AutoEventWireup="True" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.Controls.Controls_PricingInformation" Codebehind="PricingInformation.ascx.cs" %>

<asp:Panel ID="PricingInformation" runat="server" CssClass="vpa_pricing_tiles box_360 clearfix">
<div class="content">
    <h3>Pricing Comparisons</h3>
    
    <div class="books">
    <div class="widebook">
        <div class="hd" id="hd-consumerguides">            
            <asp:HyperLink ID="StockCardLink" Target="pop" runat="server" Text="Consumer Guide Values" CssClass="tileLink consumerGuidesLink" />            
        </div>
        <div class="bd">
            <div class="guide" id="guide-kbb">
                <asp:HyperLink runat="server" ID="KBBLogoLink" target="kbb"><asp:Image ID="Image1" runat="server" SkinID="KBBLogo" /></asp:HyperLink>
                Retail (w/Adj.): <asp:HyperLink ID="KBB_RetailPrice" runat="server" CssClass="price" Target="kbb" /><br />
                <asp:Label Cssclass="smallNote" ID="KBB_Dataset" runat="server" />
            </div>
            <div class="guide" id="guide-nada">
                <asp:HyperLink runat="server" ID="NADALink" Target="pop">
                    <asp:Image ID="Image2" runat="server" SkinID="NADALogo" />
                </asp:HyperLink>
                Retail: <asp:HyperLink ID="NADA_RetailPrice" runat="server" CssClass="price" Target="pop" /><br />
                <asp:Label Cssclass="smallNote" ID="NADA_Dataset" runat="server" />
            </div>
            <div class="guide" id="guide-tmv">
                <asp:HyperLink ID="TMVLink" Target="TMV" runat="server" CssClass="tileLink tmvLink">
                	<asp:Image ID="Image3" SkinId="TMVLogo" runat="server" />
            	</asp:HyperLink>
                Retail (w/Adj.): <asp:HyperLink ID="TMV_RetailPrice" runat="server" CssClass="price" />
            </div>
        </div>
        <asp:Panel ID="ConsumerGuideFooter_Panel" runat="server" CssClass="ft clearfix" Visible="false">
        <asp:Panel Id="KBBFooter_Panel" runat="server" CssClass="kbb-guide-footer" Visible="false">
            Current Price: 
            <asp:Label ID="KBB_ProfitLabel" CssClass="ft-value" runat="server" />
            <asp:Label ID="KBB_AboveBelowLabel" runat="server" />
            KBB Retail
        </asp:Panel>
        <asp:Panel ID="NADAFooter_Panel" runat="server" CssClass="nada-guide-footer" Visible="false">
            Current Price:
            <asp:Label ID="NADA_ProfitLabel" CssClass="ft-value" runat="server" />
            <asp:Label ID="NADA_AboveBelowLabel" runat="server" />
            NADA Retail
        </asp:Panel>
        <asp:Panel ID="TMVFooter_Panel" runat="server" CssClass="tmv-guide-footer" Visible="false">
            Current Price:
            <asp:Label ID="TMV_ProfitLabel" CssClass="ft-value" runat="server" />
            <asp:Label ID="TMV_AboveBelowLabel" runat="server" />
            TMV Retail
        </asp:Panel>
        </asp:Panel>
    </div>
    
<%--    <div class="book">
        <div class="hd" id="hd-jdpower">
            <asp:HyperLink ID="JDPowerHyperLink" Target="pop" runat="server" Text="Power Market Analyzer" CssClass="tileLink" />
        </div>
        <div class="bd bd-jdpower">
            <asp:HyperLink ID="JDPowerLogoLink" runat="server" Target="pop">
        	    <asp:Image ID="Image4" runat="server" SkinID="JDPowerLogo" />
        	</asp:HyperLink>
            Avg. Selling Price: <asp:Label ID="JDPower_AvgSellingPrice" runat="server" CssClass="price" /><br />
            <span class="smallNote"><asp:Literal ID="JDPower_BeginDate" runat="server" /></span>
        </div>
        <asp:Panel ID="JDPowerFooter_Panel" runat="server" CssClass="ft clearfix" Visible="false">
            Current Price:
            <asp:Label ID="JDPower_ProfitLabel" CssClass="ft-value" runat="server" /><br />
            <asp:Label ID="JDPower_AboveBelowLabel" runat="server" />
            Avg. Selling Price
        </asp:Panel>
    </div>
    --%>
    <div class="book">
        <div class="hd" id="hd-store">
            <asp:HyperLink ID="PerformanceAnalyzerHyperLink" Target="pop" runat="server" CssClass="tileLink" Text="Latest Store Performance" />
        </div>
        <div class="bd">
            Avg. Selling Price: <asp:Label ID="Store_AvgSellingPrice" runat="server" CssClass="price store_avg_selling_price" /><br />
            <span class="smallNote"><asp:Literal ID="Store_BeginDate" runat="server" />
            to 
            <asp:Literal ID="Store_EndDate" runat="server" /></span>
        </div>
        <asp:Panel ID="StorePerformanceFooter_Panel" runat="server" CssClass="ft clearfix" Visible="false">
            Current Price:
            <asp:Label ID="StorePerformance_ProfitLabel" CssClass="ft-value" runat="server" /><br />
            <asp:Label ID="StorePerformance_AboveBelowLabel" runat="server" />
            Avg. Selling Price
        </asp:Panel>
    </div>
    </div>
    
    <asp:Button runat="server" ID="RefreshData" OnClick="RefreshData_Click" Text="Refresh Pricing Data" CssClass="hidden" />
        
    <span class="corner"> </span>
</div>
</asp:Panel>
