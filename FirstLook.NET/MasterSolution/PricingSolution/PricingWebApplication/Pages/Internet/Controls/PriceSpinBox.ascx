<%@ Import namespace="FirstLook.Common.Core.Utilities"%>
<%@ Control Language="C#" AutoEventWireup="true" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.Controls.Controls_PriceSpinBox" Codebehind="PriceSpinBox.ascx.cs" %>
<span class="search-chart-rank">
	<asp:ImageButton ToolTip="Decrease Vehicle Pricing Position" ID="DecreaseVehicleRankButton" runat="server" OnClick="DecreaseVehicleRankButton_Click" ImageUrl="~/Public/Images/arrow_left_small.gif" />
    <asp:Label ID="VehicleRankLabel" runat="server" Text="Rank" AssociatedControlID="VehicleRankLabel" />:
    <b><asp:Label ID="VehicleRankTextBox" runat="server" Text='<%# string.Format("{0} of {1}", HideInsufficientData(Int32Helper.FormatAsOrdinal(Rank)), HideInsufficientData(MaximumRank.ToString())) %>' /></b>
    <asp:ImageButton ToolTip="Increase Vehicle Pricing Position" ID="IncreaseVehicleRankButton" runat="server" OnClick="IncreaseVehicleRankButton_Click" ImageUrl="~/Public/Images/arrow_right_small.gif" />
</span>
