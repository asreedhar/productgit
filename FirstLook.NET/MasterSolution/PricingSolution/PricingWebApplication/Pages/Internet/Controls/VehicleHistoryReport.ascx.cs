using System;
using System.Net;
using System.Threading;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;
using FirstLook.VehicleHistoryReport.WebControls;
using FirstLook.VehicleHistoryReport.WebService.Client.Carfax;
using FirstLook.VehicleHistoryReport.WebService.Client.AutoCheck;
using CarfaxUserIdentity=FirstLook.VehicleHistoryReport.WebService.Client.Carfax.UserIdentity;
using AutoCheckUserIdentity=FirstLook.VehicleHistoryReport.WebService.Client.AutoCheck.UserIdentity;
using CarfaxReportType=FirstLook.VehicleHistoryReport.WebControls.Carfax.CarfaxReportType;
using CarfaxVehicleEntityType=FirstLook.VehicleHistoryReport.WebService.Client.Carfax.VehicleEntityType;

namespace FirstLook.Pricing.WebApplication.Pages.Internet.Controls
{
    public partial class Controls_VehicleHistoryReport : System.Web.UI.UserControl
    {

        #region Events

        protected object ReportChangedEvent = new object();

        public event EventHandler ReportChanged
        {
            add
            {
                Events.AddHandler(ReportChangedEvent, value);
            }
            remove
            {
                Events.RemoveHandler(ReportChangedEvent, value);
            }
        }

        protected void RaiseReportChanged(object sender, EventArgs e)
        {
            EventHandler handler = (EventHandler)Events[ReportChangedEvent];
            if (handler != null)
                handler(this, e);
        } 

        #endregion

        private int DealerId
        {
            get
            {
                return Identity.GetDealerId(Request.QueryString["oh"]);
            }
        }

        private string Vin
        {
            get
            {
                return VehicleHelper.GetVin(Request.QueryString["oh"], Request.QueryString["vh"]);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SetupCarfaxPreferences();

                SetupAutoCheckPreferences();
            }
        }

        private void HideCarfaxReportForm()
        {
            CarfaxOrderForm.Visible = false;
            CarfaxReportView.CssClass = CarfaxReportView.CssClass.Replace(" carfax_form_open", string.Empty);
        }

        private void ShowCarfaxReportForm()
        {
            CarfaxOrderForm.Visible = true;
            CarfaxReportView.CssClass += " carfax_form_open";
            HideAutoCheckReportForm();
        }

        private void HideAutoCheckReportForm()
        {
            AutoCheckOrderForm.Visible = false;
            AutoCheckReportView.CssClass = AutoCheckReportView.CssClass.Replace(" autocheck_form_open", string.Empty);
        }

        private void ShowAutoCheckReportForm()
        {
            AutoCheckReportView.CssClass += " autocheck_form_open";
            AutoCheckOrderForm.Visible = true;
            HideCarfaxReportForm();
        }

        #region Carfax Web Service Properties

        /// <remarks>
        /// This is needed as this control is used in two places on the VPA but
        /// only one is shown.  By caching the information in the Context.Items
        /// dictionary we are reducing the page load time by 1 second. Yay.
        /// </remarks>
        private void LoadCarfaxInformation()
        {
            object latch = Context.Items["LoadCarfaxInformation#Latch"];

            if (latch == null)
            {
                CarfaxVehicleEntityType carfaxVehicleEntityType;

                switch (VehicleTypeCommand.Execute(Request.QueryString["vh"]))
                {
                    case VehicleType.Inventory:
                        carfaxVehicleEntityType = CarfaxVehicleEntityType.Inventory;
                        break;
                    case VehicleType.Appraisal:
                    case VehicleType.OnlineAuctionListing:
                    case VehicleType.DealerGroupListing:
                    case VehicleType.InternetListing:
                        carfaxVehicleEntityType = CarfaxVehicleEntityType.Appraisal;
                        break;
                    default:
                        carfaxVehicleEntityType = CarfaxVehicleEntityType.Undefined;
                        break;
                }

                try
                {
                    CarfaxWebService service = new CarfaxWebService();

                    service.UserIdentityValue = new CarfaxUserIdentity();

                    service.UserIdentityValue.UserName = Thread.CurrentPrincipal.Identity.Name;

                    IsCarfaxReportVisible = true;

                    CanPurchaseCarfaxReport = service.CanPurchaseReport(DealerId);

                    DefaultDisplayInCarfaxHotListings = false;

                    DefaultCarfaxReportType = CarfaxReportType.BTC;

                    if (CanPurchaseCarfaxReport)
                    {
                        if (Identity.IsDealer(Request.QueryString["oh"]))
                        {
                            CarfaxReportPreferenceTO preference = service.GetReportPreference(DealerId, carfaxVehicleEntityType);

                            if (preference != null)
                            {
                                DefaultDisplayInCarfaxHotListings = preference.DisplayInHotListings;

                                DefaultCarfaxReportType = (CarfaxReportType)
                                                          Enum.Parse(
                                                              typeof (CarfaxReportType),
                                                              preference.ReportType.ToString());
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    IsCarfaxReportVisible = false;

                    //if (exception.Status == WebExceptionStatus.ConnectFailure)
                    //{
                    //    /* ================================================= */
                    //    /* = Service is not available, so Hide the Control = */
                    //    /* ================================================= */
                    //    IsCarfaxReportVisible = false;
                    //}
                    //else
                    //{
                    //    throw;
                    //}
                }

                Context.Items["LoadCarfaxInformation#Latch"] = true;
            }
        }

        private bool IsCarfaxReportVisible
        {
            get { return (bool)Context.Items["IsCarfaxReportVisible"]; }
            set { Context.Items["IsCarfaxReportVisible"] = value; }
        }

        private bool CanPurchaseCarfaxReport
        {
            get { return (bool)Context.Items["CanPurchaseCarfaxReport"]; }
            set { Context.Items["CanPurchaseCarfaxReport"] = value; }
        }

        private CarfaxReportType DefaultCarfaxReportType
        {
            get { return (CarfaxReportType)Context.Items["DefaultCarfaxReportType"]; }
            set { Context.Items["DefaultCarfaxReportType"] = value; }
        }

        private bool DefaultDisplayInCarfaxHotListings
        {
            get { return (bool)Context.Items["DefaultDisplayInCarfaxHotListings"]; }
            set { Context.Items["DefaultDisplayInCarfaxHotListings"] = value; }
        }

        #endregion

        protected void SetupCarfaxPreferences()
        {
            LoadCarfaxInformation();

            if (IsCarfaxReportVisible)
            {
                CarfaxReportView.Visible = true;

                CarfaxOrderForm.HasPurchaseAuthority = CanPurchaseCarfaxReport;
                CarfaxOrderForm.DefaultDisplayInHotList = DefaultDisplayInCarfaxHotListings;
                CarfaxOrderForm.DefaultReportType = DefaultCarfaxReportType;
            }
            else
            {
                CarfaxReportView.Visible = false;
            }
        }

        #region AutoCheck Web Service Properties

        /// <remarks>
        /// This is needed as this control is used in two places on the VPA but
        /// only one is shown.  By caching the information in the Context.Items
        /// dictionary we are reducing the page load time by 1 second. Yay.
        /// </remarks>
        private void LoadAutoCheckInformation()
        {
            object latch = Context.Items["LoadAutoCheckInformation#Latch"];

            if (latch == null)
            {
                IsAutoCheckReportVisible = true;

                AutoCheckWebService service = new AutoCheckWebService();

                service.UserIdentityValue = new AutoCheckUserIdentity();

                service.UserIdentityValue.UserName = Thread.CurrentPrincipal.Identity.Name;

                try
                {
                    CanPurchaseAutoCheckReport = service.CanPurchaseReport(DealerId);
                }
                catch (WebException)
                {
                    IsAutoCheckReportVisible = false;
                    //if (exception.Status == WebExceptionStatus.ConnectFailure)
                    //{
                    //    /* ================================================= */
                    //    /* = Service is not available, so Hide the Control = */
                    //    /* ================================================= */
                    //    IsAutoCheckReportVisible = false;
                    //}
                    //else
                    //{
                    //   throw;
                    //}
                }

                Context.Items["LoadAutoCheckInformation#Latch"] = true;
            }
        }

        private bool IsAutoCheckReportVisible
        {
            get { return (bool)Context.Items["IsAutoCheckReportVisible"]; }
            set { Context.Items["IsAutoCheckReportVisible"] = value; }
        }

        private bool CanPurchaseAutoCheckReport
        {
            get { return (bool)Context.Items["CanPurchaseAutoCheckReport"]; }
            set { Context.Items["CanPurchaseAutoCheckReport"] = value; }
        }

        #endregion

        protected void SetupAutoCheckPreferences()
        {
            LoadAutoCheckInformation();

            if (IsAutoCheckReportVisible)
            {
                AutoCheckReportView.Visible = true;

                AutoCheckOrderForm.HasPurchaseAuthority = CanPurchaseAutoCheckReport;
            }
            else
            {
                AutoCheckReportView.Visible = false;
            }
        }

        protected void CarfaxReportView_Click(object sender, EventArgs e)
        {
            if (CarfaxOrderForm.Visible)
            {
                HideCarfaxReportForm();
            }
            else
            {
                ShowCarfaxReportForm();
            }
        }

        protected void CarfaxOrderForm_Inserted(object sender, ReportFormInsertedEventArgs e)
        {
            CarfaxReportView.DataBind();

            RaiseReportChanged(this, EventArgs.Empty);

            CarfaxOrderForm.Visible = false;

            CarfaxReportView.CssClass = CarfaxReportView.CssClass.Replace(" carfax_form_open", string.Empty);
        }

        protected void CarfaxOrderForm_Updated(object sender, ReportFormUpdatedEventArgs e)
        {
            CarfaxReportView.DataBind();

            CarfaxOrderForm.DataBind();

            RaiseReportChanged(this, EventArgs.Empty);
        }

        protected void CarfaxDataSource_Selecting(object sender, ReportDataSourceSelectingEventArgs e)
        {
            e.InputParameters["DealerId"] = DealerId;

            e.InputParameters["Vin"] = Vin;
        }

        protected void CarfaxDataSource_Updating(object sender, ReportDataSourceMethodEventArgs e)
        {
            e.InputParameters["DealerId"] = DealerId;

            e.InputParameters["Vin"] = Vin;
        }

        protected void CarfaxDataSource_Inserting(object sender, ReportDataSourceMethodEventArgs e)
        {
            e.InputParameters["DealerId"] = DealerId;

            e.InputParameters["Vin"] = Vin;
        }

        protected void AutoCheckReportView_Click(object sender, EventArgs e)
        {
            if (AutoCheckOrderForm.Visible)
            {
                HideAutoCheckReportForm();
            }
            else
            {
                ShowAutoCheckReportForm();
            }
        }

        protected void AutoCheckOrderForm_Inserted(object sender, ReportFormInsertedEventArgs e)
        {
            AutoCheckReportView.DataBind();
            AutoCheckOrderForm.Visible = false;

            RaiseReportChanged(this, EventArgs.Empty);

            AutoCheckReportView.CssClass = CarfaxReportView.CssClass.Replace(" autocheck_form_open", string.Empty);
        }

        protected void AutoCheckOrderForm_Updated(object sender, ReportFormUpdatedEventArgs e)
        {
            AutoCheckReportView.DataBind();
            AutoCheckOrderForm.DataBind();

            RaiseReportChanged(this, EventArgs.Empty);
        }

        protected void AutoCheckDataSource_Selecting(object sender, ReportDataSourceSelectingEventArgs e)
        {
            e.InputParameters["DealerId"] = DealerId;

            e.InputParameters["Vin"] = Vin;
        }

        protected void AutoCheckDataSource_Inserting(object sender, ReportDataSourceMethodEventArgs e)
        {
            e.InputParameters["DealerId"] = DealerId;

            e.InputParameters["Vin"] = Vin;
        }

        protected void AutoCheckOrderForm_DataBound(object sender, EventArgs e)
        {
            AutoCheckOrderForm.ReportUrl =
                string.Format("/support/Reports/AutoCheckReport.aspx?DealerId={0}&Vin={1}", DealerId, Vin);
        }

        protected void CarfaxReportView_PreRender(object sender, EventArgs e)
        {
            if (CarfaxOrderForm.ReportType != CarfaxReportType.Undefined)
            {
                CarfaxReportView.Attributes["ondblclick"] = string.Format("window.open('{0}','{1}')", CarfaxOrderForm.ReportUrl, "CarfaxReport"); 
            }
        }

        protected void AutoCheckReportView_PreRender(object sender, EventArgs e)
        {
            try
            {
                AutoCheckReportView.Attributes["ondblclick"] = string.Format("window.open('{0}','{1}')", AutoCheckOrderForm.ReportUrl, "AutoCheckReport"); 
            }
            catch (Exception ex)
            {
                /* =================================================
             * Don't throw the expected NULL ReferenceException
             * as this means the reprot is not yet ordered, PM
             * ================================================= */
                if (typeof(NullReferenceException) != ex.GetType())
                {
                    throw;
                }
            }
        }
    }
}