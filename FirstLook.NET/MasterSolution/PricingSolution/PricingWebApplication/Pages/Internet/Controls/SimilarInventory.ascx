<%@ Control Language="C#" AutoEventWireup="true" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.Controls.Controls_SimilarInventory" Codebehind="SimilarInventory.ascx.cs" %>
<asp:Panel ID="SimilarInventoryTablePanel" runat="server" CssClass="invListings clearfix">
    <asp:HiddenField ID="ShowAllYears" runat="server" Value="False" />
    <asp:ObjectDataSource
			ID="SimilarInventoryTableObjectDataSource"
			runat="server"
			TypeName="FirstLook.Pricing.DomainModel.Internet.DataSource.SimilarInventoryTableDataSource"
			SelectMethod="FindByOwnerHandleAndVehicleHandle"
			SelectCountMethod="CountByOwnerHandleAndVehicleHandle"
			MaximumRowsParameterName="maximumRows"
			StartRowIndexParameterName="startRowIndex"
			SortParameterName="sortColumns"
			EnablePaging="true">
		<SelectParameters>
			<asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
			<asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="vehicleHandle" Type="String" QueryStringField="vh" />
			<asp:ControlParameter ConvertEmptyStringToNull="true" Name="isAllYears" ControlID="ShowAllYears" Type="Boolean" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource
			ID="SimilarInventoryDescriptionObjectDataSource"
			runat="server"
			TypeName="FirstLook.Pricing.DomainModel.Internet.DataSource.SimilarInventoryTableDataSource"
			SelectMethod="FindDescriptionByOwnerHandleAndVehicleHandle">
		<SelectParameters>
			<asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
			<asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="vehicleHandle" Type="String" QueryStringField="vh" />
			<asp:ControlParameter ConvertEmptyStringToNull="true" Name="isAllYears" ControlID="ShowAllYears" Type="Boolean" />
		</SelectParameters>
	</asp:ObjectDataSource>
    <asp:FormView ID="FormView1" runat="server" DefaultMode="ReadOnly" DataSourceID="SimilarInventoryDescriptionObjectDataSource" AllowPaging="false" CssClass="title">
        <ItemTemplate>
            <p>
		        Current Inventory
		        <%# ProcessDescription(
		            DataBinder.Eval(Container.DataItem, "VehicleDescription"),
		                            GreaterThanOne(DataBinder.Eval(Container.DataItem, "InventoryTotalCount"))) %>
		        <%# DataBinder.Eval(Container.DataItem, "InventoryTotalUnitCost", "{0:C0}")%>
		        in Inventory
		        <asp:LinkButton ID="YearToggleButton" runat="server" Text='<%# (string.Equals(ShowAllYears.Value, bool.TrueString)) ?  "Show Single Year" : "Show All Years" %>' CssClass="show_all_years" OnClick="YearToggleButton_Click" />
	        </p>
        </ItemTemplate>
    </asp:FormView>
    <asp:Panel ID="SimilarInventoryTableBodyPanel" runat="server" CssClass="invListings-bd clearfix">
        <asp:GridView
                ID="SimilarInventoryTableGridView"
                runat="server"
                AutoGenerateColumns="false"
                AlternatingRowStyle-CssClass="odd"
                AllowPaging="true" 
                AllowSorting="true"
                CssClass="grid inventory"
                DataSourceID="SimilarInventoryTableObjectDataSource"
                OnDataBound="SimilarInventoryTableGridView_DataBound"
                PageSize="5">
            <PagerStyle CssClass="gridPager" />
            <Columns>
                <asp:BoundField 
                    DataField="Age"
                    HeaderText="Age"
                    SortExpression="Age" />
                <asp:BoundField 
                    DataField="VehicleDescription"
                    HeaderText="Vehicle Information"
                    ItemStyle-Width="100"
                    SortExpression="VehicleDescription" />
                <asp:BoundField 
                    DataField="VehicleColor" 
                    HeaderText="Color" 
                    SortExpression="VehicleColor" />
                <asp:BoundField 
                    DataField="VehicleMileage" 
                    DataFormatString="{0:#,###,###;--;--}" 
                    HeaderText="Mileage" 
                    SortExpression="VehicleMileage"
                    HtmlEncode="False" />
                <asp:BoundField 
                    DataField="UnitCost" 
                    DataFormatString="{0:$#,##0;--;--}" 
                    HeaderText="Unit Cost" 
                    SortExpression="UnitCost"
                    ConvertEmptyStringToNull="true"
                    NullDisplayText="--"
                    HtmlEncode="False" /> 
                <asp:BoundField 
                    DataField="BookVersusCost" 
                    DataFormatString="{0:$#,##0;($#,##0);--}" 
                    HeaderText="Book Vs Cost" 
                    SortExpression="BookVersusCost"
                    ConvertEmptyStringToNull="true"
                    NullDisplayText="--"
                    HtmlEncode="False" /> 
                <asp:BoundField 
                    DataField="ListPrice" 
                    DataFormatString="{0:$#,##0;--;--}" 
                    HeaderText="Internet Price" 
                    SortExpression="ListPrice"
                    ConvertEmptyStringToNull="true"
                    NullDisplayText="--"
                    HtmlEncode="False" />
                <asp:BoundField 
                    DataField="PctAvgMarketPrice" 
                    HeaderText="% Market Avg." 
                    DataFormatString="{0:#' %';--;--}"
                    HtmlEncode="false"
                    SortExpression="PctAvgMarketPrice" />
                <asp:TemplateField HeaderText="Certified?" SortExpression="Certified">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "Certified").ToString() == "True" ? "Yes" : "No" %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="T/P" SortExpression="TradeOrPurchase">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "TradeOrPurchase").ToString() == "1" ? "P" : "T" %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:HyperLinkField
                    HeaderText="Stock #" 
                    DataNavigateUrlFields="StockNumber"
                    DataNavigateUrlFormatString="/IMT/EStock.go?stockNumberOrVin={0}&isPopup=true"
                    DataTextField="StockNumber"
                    DataTextFormatString="{0}"
                    Target="estock"
                    SortExpression="StockNumber" />
            </Columns>
        </asp:GridView>
    </asp:Panel>
</asp:Panel>
