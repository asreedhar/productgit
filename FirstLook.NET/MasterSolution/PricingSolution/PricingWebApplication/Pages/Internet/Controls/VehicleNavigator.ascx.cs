﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using FirstLook.Pricing.DomainModel.Internet.DataSource;

namespace FirstLook.Pricing.WebApplication.Pages.Internet.Controls
{
    public partial class VehicleNavigator : UserControl, IScriptControl
    {
        private string OwnerHandle
        {
            get { return Request.Params["oh"]; }
        }

        private string InsertUser
        {
            get { return Context.User.Identity.Name; }
        }

        protected void VehicleSearchButton_Click(object sender, EventArgs e)
        {
            HandleLookupDataSource finder = new HandleLookupDataSource();
            string stockNumber = VehicleSearchTextBox.Text;
            if (!stockNumber.Equals("Stock #"))
            {
                try
                {
                    string vehicleHandle;
                    string searchHandle;
                    finder.FindByOwnerHandleAndStockNumber(OwnerHandle, stockNumber, InsertUser, out vehicleHandle,
                                                           out searchHandle);

                    Response.Redirect(string.Format("{0}?oh={1}&vh={2}&sh={3}", Request.UrlReferrer.AbsolutePath, OwnerHandle, vehicleHandle,
                                                    searchHandle));
                }
                catch (Exception)
                {
                    VehicleSearchWarningLabel.Visible = true;
                }
            }
        }

        private ScriptManager manager;

        protected void Page_PreRender(Object sender, EventArgs e)
        {
            if (!DesignMode)
            {
                manager = ScriptManager.GetCurrent(Page);

                if (manager != null)
                    manager.RegisterScriptControl(this);
                else
                    throw new ApplicationException("You must have a ScriptManager!");
            }
            if (VehicleSearchTextBox.Text == string.Empty)
            {
                VehicleSearchWarningLabel.Visible = false;
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);

            if (!DesignMode)
                manager.RegisterScriptDescriptors(this);
        }


        #region Implementation of IScriptControl

        public IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {
            ScriptBehaviorDescriptor vehicleNavigator =
                new ScriptBehaviorDescriptor("FirstLook.Pricing.Website.VehicleNavigator",
                                             VehicleNavigatorControl.ClientID);

            vehicleNavigator.AddProperty("next_prev_class", "next_prev_nav");
            vehicleNavigator.AddProperty("next_class", "next");
            vehicleNavigator.AddProperty("prev_class", "prev");

            return new [] {vehicleNavigator};
        }

        public IEnumerable<ScriptReference> GetScriptReferences()
        {
            return new[] {new ScriptReference("~/Public/Scripts/Controls/VehicleNavigator.debug.js")};
        }

        #endregion
    }
}