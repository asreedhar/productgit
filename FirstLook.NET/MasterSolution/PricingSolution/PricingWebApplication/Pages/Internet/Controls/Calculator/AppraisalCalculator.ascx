<%@ Control Language="C#" AutoEventWireup="True" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.Controls.Calculator.Controls_Calculator_AppraisalCalculator" Codebehind="AppraisalCalculator.ascx.cs" %>

<%@ Register Src="~/Pages/Internet/Controls/PriceSpinBox.ascx" TagPrefix="Controls" TagName="PriceSpinBox" %>

<pwc:MarketPricing ID="MarketPricingData" runat="server" />

<div class="top_wrapper">
<div>
	<h2 id="Feedback" runat="server" OnPreRender="FeedBack_PreRender"></h2>

    <cwc:MessageBox ID="WhatsThisDialog" runat="server"
        TitleText="Market Appraisal Value"
        Top="85" Left="35" Width="505" Height="185" AllowDrag="false" AllowResize="false" AllowCancelPostBack="true"
        Hidden="true">
        <ItemTemplate>
        <p style="font-size: 0.8em"><b>MAX APPRAISAL VALUE</b> calculates the highest appraisal value for the unit based on the retail pricing value at the high end of the Market Pricing Power range.</p>
        <p style="font-size: 0.8em">Market Pricing Power is the upper range this unit can be priced and still drive online consumer traffic.  
            This range recommends optimal initial internet price for this unit. </p>
        <p style="font-size: 0.8em">You should consider the following:</p>
        <ul style="font-size: 0.8em">
            <li> - Price higher than the range for units with exceptional condition, low miles per model year or premium equipment.</li>
            <li> - Price lower than the range for units with rough condition or minimal equipment.</li>
        </li>
        </ItemTemplate>
        <Buttons>
            <cwc:DialogButton ButtonType="Button" ButtonCommand="Ok" Text="OK" IsCancel="true" IsDefault="true" />
        </Buttons>
    </cwc:MessageBox>

	<ul class="control">
		<li class="mktPrce-price">
            <span class="input_unit"> $ </span>
			<asp:TextBox ID="AppraisalValueTextBox" runat="server" Text='<%# FormatCurrency(AppraisalValue) %>' Columns="7" MaxLength="8" OnTextChanged="AppraisalValueTextBox_TextChanged"></asp:TextBox>
			<asp:Label ID="AppraisalValueLabel" runat="server" AssociatedControlID="AppraisalValueTextBox" Text="Current Appraisal"></asp:Label>
			<cwc:MoveNextBehaviour runat="server" TargetControlID="AppraisalValueTextBox" ID="AppraisalValueTextBox_MoveNext" />
		</li>
		<li class="mktPrce-est-recon">
			<asp:TextBox ID="EstimatedReconditioningCostTextBox" runat="server" Text='<%# FormatCurrency(EstimatedReconditionCost) %>' Columns="7" MaxLength="8" OnTextChanged="EstimatedReconditioningCostTextBox_TextChanged"></asp:TextBox>
			<asp:Label ID="EstimatedReconditioningCostLabel" runat="server" AssociatedControlID="EstimatedReconditioningCostTextBox" Text="Est. Recon"></asp:Label>
			<cwc:MoveNextBehaviour runat="server" TargetControlID="EstimatedReconditioningCostTextBox" ID="EstimatedReconditioningCostTextBox_MoveNext" />
		</li>
		<li class="mktPrce-target-gross">
			<asp:TextBox ID="TargetGrossProfitTextBox" runat="server" Text='<%# FormatCurrency(TargetGrossProfit) %>' Columns="7" MaxLength="8" OnTextChanged="TargetGrossProfitTextBox_TextChanged" OnPreRender="TargetGrossProfitTextBox_PreRender"></asp:TextBox>
			<asp:Label ID="TargetGrossProfitLabel" runat="server" AssociatedControlID="TargetGrossProfitTextBox" Text="Target Gross Profit"></asp:Label>
			<cwc:MoveNextBehaviour runat="server" TargetControlID="TargetGrossProfitTextBox" ID="TargetGrossProfitTextBox_MoveNext" />
        </li>
        <li class="mktPrce-target-profit">
            <asp:Label ID="TargetSellingPrice1Label" runat="server" AssociatedControlID="TargetSellingPrice1TextBox">
                <asp:TextBox ID="TargetSellingPrice1TextBox" runat="server" Text='<%# FormatCurrency(TargetSellingPrice, "--") %>' CssClass="readonly" ReadOnly="true"></asp:TextBox>
                Target Selling Price
            </asp:Label>
        </li>
    </ul>
	<ul class="control second_control">
        <li runat="server" visible="<%# HasMileageAdjustedSearch %>" class="mktPrce-max-market"> 
            <span class="value" runat="server" id="MaxThePriceHtmlGeneric"><%# FormatCurrency(AdjustedMaxPrice == 0 ? null : AdjustedMaxPrice, "--") %></span>
            <span class="label">Max Appraisal Value</span>
            <a id="mpp_help" class="help_text" title="Click For Help">Whats this?</a>
        </li>
		<li class="mktPrce-avg">
			<asp:TextBox ID="PctAvgMarketPriceTextBox" Columns="3" runat="server" Text='<%# FormatPercentage(PctAvgMarketPrice,"NA") %>' ReadOnly="true"></asp:TextBox>
			<asp:Label ID="PctAvgMarketPriceLabel" runat="server" Text="% Market Average" AssociatedControlID="PctAvgMarketPriceTextBox"></asp:Label>
        </li>
        <li class="mktPrce-save">
            <span class="button"><asp:LinkButton ID="CalculateRepriceButton" runat="server" Text="Save Appraisal" OnClick="CalculateRepriceButton_Click" /></span>
            <p><br /></p>
        </li>
	</ul>
</div>

<div class="hr"><hr /></div>

<div id="SliderContainerHtmlGenericControl" runat="server">
</div>

    <p class="rank_control">
    <Controls:PriceSpinBox ID="PriceSpinBox" runat="server" DataSourceID="MarketPricingListObjectDataSource" OnDataBound="PriceSpinBox_DataBound" />
    </p>

<ul id="market_summary">
	<li class="listings"><asp:Label ID="Label1" runat="server" Text='<%# MarketPricingData.NumListings %>' /> Listings</li>
	<li class="market_day_supply"><asp:Label ID="Label3" runat="server" Text='<%# MarketPricingData.MarketDaySupply %>' /> Market Days Supply</li>
 	<li class="avg_mileage">Average Mileage: <asp:Label ID="Label2" runat="server" Text='<%# string.Format("{0:0,0}",MarketPricingData.AvgVehicleMileage) %>' /></li>	
</ul>
</div>

<ul id="calculator_summary">
    <li class="target_selling_price">
        <asp:TextBox ID="TargetSellingPriceTextBox" runat="server" Text='<%# FormatCurrency(TargetSellingPrice, "--") %>' CssClass="readonly" ReadOnly="true"></asp:TextBox>
        <asp:Label ID="TargetSellingPriceLabel" runat="server" Text="Target Selling Price" AssociatedControlID="TargetSellingPriceTextBox" />
    </li>
    <li runat="server" class="jd_power" visible='<%# MarketPricingData.JDPowerAverageSalePrice.HasValue %>'>
        <asp:TextBox ID="JDPowerAvgSellingPriceTextBox" runat="server" Text='<%# FormatCurrency(MarketPricingData.JDPowerAverageSalePrice) %>' ReadOnly="true" CssClass="readonly"></asp:TextBox>
        <asp:Label ID="JDPowerAvgSellingPriceLabel" runat="server" Text="Avg. PIN Selling Price" AssociatedControlID="JDPowerAvgSellingPriceTextBox" />
    </li>
    <li class="avg_selling_price">
        <p id="avg_selling_price">--</p>
        <label>Avg. Store Selling Price</label>
    </li>
    <li class="naaa_price">
        <asp:TextBox ID="NaaaPriceTextBox" runat="server" Text='<%# FormatCurrency(MarketPricingData.NaaaAverageSalePrice, "--") %>' ReadOnly="true" CssClass="readonly" />
        <asp:Label ID="NaaaPriceLabel" runat="server" Text="Auction Price" AssociatedControlID="NaaaPriceTextBox" />
    </li>
</ul>
