<%@ Control Language="C#" AutoEventWireup="true" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.Controls.Calculator.Controls_Calculator_Calculator" Codebehind="Calculator.ascx.cs" %>

<%@ Register Src="~/Pages/Internet/Controls/Calculator/AppraisalCalculator.ascx" TagPrefix="Controls" TagName="AppraisalCalculator" %>
<%@ Register Src="~/Pages/Internet/Controls/Calculator/InventoryCalculator.ascx" TagPrefix="Controls" TagName="InventoryCalculator" %>

<Controls:InventoryCalculator
        ID="InventoryCalculator"
        runat="server"
        OnPriceChanging="Calculator_PriceChanging"
        OnPriceChanged="Calculator_PriceChanged"
        Visible="false" />

<Controls:AppraisalCalculator
        ID="AppraisalCalculator"
        runat="server"
        OnPriceChanging="Calculator_PriceChanging"
        OnPriceChanged="Calculator_PriceChanged"
        Visible="false" />
