<%@ Control Language="C#" AutoEventWireup="true" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.Controls.Calculator.Controls_Calculator_MaxPriceConfirmation" Codebehind="MaxPriceConfirmation.ascx.cs" %>

<cwc:Dialog ID="RepriceWarningDialog" runat="server"
		Top="120" Left="520" Width="290" Height="150" AllowResize="false" AllowDrag="false"
		TitleText="Max Price Confirmation" OnButtonClick="RepriceWarningDialog_ButtonClick" AllowCancelPostBack="true">
	<ItemTemplate>
		<p>
			MAXED List Price <em><asp:Label ID="ListPriceLabel" runat="server" /></em> will result in a Potential
			Gross Profit of <em><asp:Label ID="PotentialGrossProfitLabel" runat="server" /></em> which is below
			the Minimum Gross Profit of <em><asp:Label ID="MinimumGrossProfitLabel" runat="server" /></em>.			
		</p>
		<hr />
		<p class="confirmation">Select <strong>'OK'</strong> to accept this List Price or <strong>'Cancel'</strong> to set a new List Price.</p>
	</ItemTemplate>
	<Buttons>
		<cwc:DialogButton ButtonType="Button" ButtonCommand="Ok" Text="Ok" IsDefault="true" />
		<cwc:DialogButton ButtonType="Button" ButtonCommand="Cancel" Text="Cancel" IsCancel="true" />
	</Buttons>
</cwc:Dialog>