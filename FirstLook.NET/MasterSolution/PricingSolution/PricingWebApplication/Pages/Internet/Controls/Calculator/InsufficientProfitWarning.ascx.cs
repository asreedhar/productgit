using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Common.WebControls.UI;

namespace FirstLook.Pricing.WebApplication.Pages.Internet.Controls.Calculator
{
    public partial class Controls_Calculator_InsufficientProfitWarning : UserControl
    {
        public event EventHandler AcceptPriceChange;

        public event EventHandler CancelPriceChange;

        public void OpenDialog(int listPrice, int potentialGrossProfit, int minGrossProfit)
        {
            RepriceWarningDialog.Visible = true;
            ListPrice = listPrice;
            PotentialGrossProfit = potentialGrossProfit;
            MinGrossProfit = minGrossProfit;
        }

        public void CloseDialog()
        {
            ViewState.Clear();

            RepriceWarningDialog.Visible = false;
        }    

        protected void RepriceWarningDialog_ButtonClick(object sender, DialogButtonClickEventArgs e)
        {
            switch (e.Button)
            {
                case DialogButtonCommand.Ok:
                    if (AcceptPriceChange != null)
                        AcceptPriceChange(this, EventArgs.Empty);
                    break;
                case DialogButtonCommand.Cancel:
                    if (CancelPriceChange != null)
                        CancelPriceChange(this, EventArgs.Empty);
                    break;
                default:
                    throw new NotImplementedException("The appraisal calculator does not allow the price to be set.");
            }
        }

        #region Properties

        public int ListPrice
        {
            get
            {
                object value = ViewState["ListPrice"];
                if (value == null)
                    return 0;
                return (int) value;
            }
            protected set
            {
                if (ListPrice != value)
                {
                    ViewState["ListPrice"] = value;	
                }
                Label listPriceLabel = RepriceWarningDialog.FindControl("ListPriceLabel") as Label;
                if (listPriceLabel != null)
                {
                    listPriceLabel.Text = String.Format("{0:$#,##0}", value);
                }
            }
        }

        public int PotentialGrossProfit
        {
            get
            {
                object value = ViewState["PotentialGrossProfit"];
                if (value == null)
                    return 0;
                return (int)value;
            }
            protected set
            {
                if (PotentialGrossProfit != value)
                {
                    ViewState["PotentialGrossProfit"] = value;				                
                }
                Label potentialGrossProfitLabel = RepriceWarningDialog.FindControl("PotentialGrossProfitLabel") as Label;
                if (potentialGrossProfitLabel != null)
                {
                    potentialGrossProfitLabel.Text = String.Format("{0:$#,##0}", value);
                    if (value < 0)
                        potentialGrossProfitLabel.Style["color"] = "red";
                    else
                        potentialGrossProfitLabel.Style.Remove("color");
                }
            }
        }

        public int MinGrossProfit
        {
            get
            {
                object value = ViewState["MinGrossProfit"];
                if (value == null)
                    return 0;
                return (int)value;
            }
            set
            {
                if (MinGrossProfit != value)
                {
                    ViewState["MinGrossProfit"] = value;
                }
                Label minGrossProfitLabel = RepriceWarningDialog.FindControl("MinGrossProfitLabel") as Label;
                if (minGrossProfitLabel != null)
                {
                    minGrossProfitLabel.Text = String.Format("{0:$#,##0}", value);
                }
            }
        }

        #endregion
    }
}