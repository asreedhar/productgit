using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web.UI;
using FirstLook.Common.Core.Utilities;
using FirstLook.DomainModel.Oltp;
using FirstLook.Pricing.DomainModel;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Distribution;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;
using FirstLook.Pricing.WebControls;
using FirstLook.Pricing.WebControls.UserControls;

namespace FirstLook.Pricing.WebApplication.Pages.Internet.Controls.Calculator
{
    public partial class Controls_Calculator_InventoryCalculator : WebControls.UserControls.Calculator
    {
        private const int MinPrice = 0;
        private const int MaxPrice = 999999;

        private const double SMALL_PRICE_CHANGE_LOW_MULTIPLIER = 0.9;
        private const double SMALL_PRICE_CHANGE_HIGH_MULTIPLIER = 1.1;

        private bool IsDirty = false;

        #region Properties
    
        public int? InventoryUnitCost
        {
            get
            {
                return MarketPricingData.InventoryUnitCost;
            }
        }

        public int? InventoryListPrice
        {
            get
            {
                return MarketPricingData.InventoryListPrice;
            }
            protected set
            {
                if (value != InventoryListPrice)
                {
                    MarketPricingData.InventoryListPrice = value;
                    OnPropertyChanged("InventoryListPrice");
                }

            }
        }

        public override string RepricePostbackScript
        {
            get
            {
                return Page.ClientScript.GetPostBackClientHyperlink(InventoryRepriceButton, "");
            }
        }

        public override string PriceClientId
        {
            get { return NewInventoryListPriceTextBox.ClientID; }
        }

        public int? PotentialGrossProfit
        {
            get
            {
                return InventoryListPrice - InventoryUnitCost;
            }
        }

        public int? NewInventoryListPrice
        {
            get
            {
                return (int?) ViewState["NewInventoryListPrice"];
            }
            set
            {
                if (value != NewInventoryListPrice)
                {
                    RaisePriceChangedEvent(NewInventoryListPrice, value);
                    ViewState["NewInventoryListPrice"] = value;
                    OnPropertyChanged("NewInventoryListPrice");
                }
            }
        }

        public int? NewPotentialGrossProfit
        {
            get
            {
                return NewInventoryListPrice - InventoryUnitCost;
            }
        }

        public int? PctAvgMarketPrice
        {
            get
            {
                return (int?) ViewState["PctAvgMarketPrice"];
            }
            set
            {
                if (value != PctAvgMarketPrice)
                {
                    ViewState["PctAvgMarketPrice"] = value;
                    OnPropertyChanged("PctAvgMarketPrice");
                }
            }
        }

        public override PricingRisk PricingRisk
        {
            get
            {
                return MarketPricingData.GetPricingRisk(NewInventoryListPrice);
            }
        }

        private bool HasMileageAdjustedSearch
        {
            get
            {
                if (ViewState["HasMileageAdjustedSearch"] == null)
                {
                    var packageDetails = PackageDetails.GetDealerPricingPackageDetails(OwnerHandle,
                                                                                       SearchHandle,
                                                                                       VehicleHandle);
                    ViewState["HasMileageAdjustedSearch"] = packageDetails.HasMileageAdjustedSearch;
                }

                return (bool) ViewState["HasMileageAdjustedSearch"];
            }
        }
        #endregion

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            PropertyChanged += Page_PropertyChanged;
        }

        protected override void Page_DataBound(object sender, EventArgs e)
        {            
            RaisePropertyChangedEvents = false;

            // store the values in view state so we do not have to return to the db
            MarketPricingData.DataSource = new object[] {DataItem};
            MarketPricingData.DataBind();

            // initialize properties
            PctAvgMarketPrice = MarketPricingData.PctAvgMarketPrice;
            NewInventoryListPrice = MarketPricingData.InventoryListPrice;

            PriceSpinBox.Price = MarketPricingData.InventoryListPrice;
            PriceSpinBox.Mileage = MarketPricingData.VehicleMileage ?? 0;
            PriceSpinBox.EnableRankChangeButtons = IsInventory() || IsSalesToolVehicle();

            RaisePropertyChangedEvents = true;

            IsDirty = true;            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            PctAvgMarketPriceTextBox.ReadOnly = !MarketPricingData.HasMarket();

            MaxThePriceButton.Enabled = MarketPricingData.HasMarket() && (IsInventory() || IsSalesToolVehicle());

            MaxThePriceButton.Text = HasMileageAdjustedSearch ? "MAX Price" : "Optimize Price";

            if (IsDirty)
            {
                DataBindChildren();
            }
        }

        private void Page_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (string.Compare(e.PropertyName, "NewInventoryListPrice", true) == 0)
            {
                PctAvgMarketPrice = MarketPricingData.CalculatePctAvgMarketPrice(NewInventoryListPrice, PctAvgMarketPrice);
            }
            else if (string.Compare(e.PropertyName, "PctAvgMarketPrice", true) == 0)
            {
                if (!IsDirty)
                {
                    // Because is the least accurate way to arrive at a NewInventoryListPrice we never want
                    // to calculate it after a NewInventoryListPrice has been changed, PM
                    NewInventoryListPrice = MarketPricingData.CalculatePrice(PctAvgMarketPrice, NewInventoryListPrice); 
                } 
                else
                {
                    PctAvgMarketPrice = MarketPricingData.CalculatePctAvgMarketPrice(NewInventoryListPrice, PctAvgMarketPrice);
                }               
            }

            IsDirty = true;
        }

        protected void PctAvgMarketPriceTextBox_TextChanged(object sender, EventArgs e)
        {
            PctAvgMarketPrice = Int32Helper.ToNullableInt32(RemoveCurrencyFormatting(PctAvgMarketPriceTextBox.Text));
        }

        protected void NewInventoryListPriceTextBox_TextChanged(object sender, EventArgs e)
        {
            NewInventoryListPrice = Int32Helper.ToNullableInt32(RemoveCurrencyFormatting(NewInventoryListPriceTextBox.Text));        
        }

        protected void InventoryRepriceButton_Click(object sender, EventArgs e)
        {
            if (IsPriceValid(NewInventoryListPrice))
            {
                var owner = Owner.GetOwner(OwnerHandle);
                var businessUnit = BusinessUnitFinder.Instance().Find(owner.OwnerEntityId);
                //var hasFirstLook30Upgrade = businessUnit.HasDealerUpgrade(Upgrade.FirstLook30);

                ChangePrice(InventoryListPrice, NewInventoryListPrice);
                //Supports only Firstlook 3.0 now.
                //if (IsSmallPriceChange(InventoryListPrice, NewInventoryListPrice) || hasFirstLook30Upgrade)
                //{
                //    ChangePrice(InventoryListPrice, NewInventoryListPrice);
                //}
                //else
                //{
                //    OverrideMarketAnalysis.CloseDialog();
                //    InsufficientProfitWarning.CloseDialog();
                //    LargePriceChangeWarning.OpenDialog(InventoryListPrice, NewInventoryListPrice.Value);
                //}
            }
        }

        protected void LargePriceChangeWarning_Accept(object sender, EventArgs e)
        {
            if (IsPriceValid(LargePriceChangeWarning.NewListPrice))
            {
                ChangePrice(LargePriceChangeWarning.OldListPrice, LargePriceChangeWarning.NewListPrice);

                LargePriceChangeWarning.CloseDialog();
            }
        }

        protected void LargePriceChangeWarning_Cancel(object sender, EventArgs e)
        {
            CancelPriceChange();
            LargePriceChangeWarning.CloseDialog();
        }

        protected void MaxThePriceButton_Click(object s, EventArgs e)
        {
            NewInventoryListPrice = MarketPricingData.MaxPrice();
            if (MarketPricingData.HasInsufficientProfit())
            {
                OverrideMarketAnalysis.CloseDialog();
                LargePriceChangeWarning.CloseDialog();
                InsufficientProfitWarning.OpenDialog(
                    MarketPricingData.MaxPrice(),
                    MarketPricingData.ProfitAtMaxPrice(),
                    MarketPricingData.MinGrossProfit.GetValueOrDefault());
            }
            else
            {
                InsufficientProfitWarning_Accept(s, e);
            }
        }

        protected void InsufficientProfitWarning_Accept(object sender, EventArgs e)
        {
            // update the interface with the new values
            PctAvgMarketPrice = MarketPricingData.MaxPctMarketAvg.GetValueOrDefault();
            NewInventoryListPrice = MarketPricingData.MaxPrice();

            // close the dialog
            InsufficientProfitWarning.CloseDialog();

            // tell every one else the price has changed
            RaisePriceChangedEvent(InventoryListPrice, NewInventoryListPrice);
        }

        protected void InsufficientProfitWarning_Cancel(object sender, EventArgs e)
        {
            CancelPriceChange();
            InsufficientProfitWarning.CloseDialog();
        }


        public override void DataBind()
        {
            base.DataBind();

            UpdateCurrentLabels();
        }

        #endregion

        #region Price Control

        public override int? Price
        {
            get { return NewInventoryListPrice; }
            set { NewInventoryListPrice = value; }
        }

        protected void ChangePrice(int? oldPrice, int? newPrice)
        {
            if (IsInventory())
            {
                RaisePriceChangingEvent();

                if (Identity.IsUser())
                {
                    VehicleInteractionAuditCommand.Reprice(
                        OwnerHandle, VehicleHandle, SearchHandle, Login,
                        oldPrice.GetValueOrDefault(), newPrice.GetValueOrDefault());
                }
            
                InventoryRepriceCommand.Execute(
                    Login,
                    MarketPricingData.VehicleEntityID,
                    oldPrice.GetValueOrDefault(),
                    newPrice.GetValueOrDefault(),
                    false);

                DistributeRepriceCommand.Execute(OwnerHandle, VehicleHandle, newPrice.GetValueOrDefault());
            }

            PriceSpinBox.Price = NewInventoryListPrice;

            InventoryListPrice = NewInventoryListPrice;

            UpdateCurrentLabels();

            //While updating , remove all the keys from memcache which were generated while getting the marketListing.----------------------
            PricingAnalyticsClient.ClearMemCache(OwnerHandle);

            RaisePriceChangedEvent(oldPrice, newPrice);
        }

        protected void UpdateCurrentLabels()
        {
            CurrentPctAvgMarketPriceTextBox.Text = string.Format("Current: {0}", FormatPercentage(PctAvgMarketPrice, "NA"));

            InventoryListPriceLabel.Text = string.Format("Current: {0}", FormatCurrency(InventoryListPrice));
        }

        protected void CancelPriceChange()
        {
            NewInventoryListPrice = InventoryListPrice;
        }    

        #endregion

        #region Helper Methods

        internal static bool IsPriceValid(int? newPrice)
        {
            if (newPrice.HasValue)
                return newPrice >= MinPrice && newPrice <= MaxPrice;
            return false;
        }

        internal static bool IsSmallPriceChange(int? oldPrice, int? newPrice)
        {
            if (oldPrice.HasValue && newPrice.HasValue)
            {
                return (
                           newPrice < SmallPriceChangeThresholdHigh(oldPrice.Value) &&
                           newPrice > SmallPriceChangeThresholdLow(oldPrice.Value)
                       );
            }
            return false;
        }

        /// <summary>
        /// Exposed for other internal clients.
        /// </summary>
        /// <param name="oldPrice"></param>
        /// <returns></returns>
        internal static double SmallPriceChangeThresholdLow(int oldPrice)
        {
            return oldPrice * SMALL_PRICE_CHANGE_LOW_MULTIPLIER;
        }

        /// <summary>
        /// Exposed for other internal clients.  Should be kept in sync with SMALL_PRICE_CHANGE_HIGH_MULTIPLIER.
        /// </summary>
        /// <returns></returns>
        internal static string LargePriceChangeIncreaseText()
        {
            return "more than 110% of";
        }

        /// <summary>
        /// Exposed for other internal clients.
        /// </summary>
        /// <param name="oldPrice"></param>
        /// <returns></returns>
        internal static double SmallPriceChangeThresholdHigh(int oldPrice)
        {
            return oldPrice * SMALL_PRICE_CHANGE_HIGH_MULTIPLIER;            
        }

        /// <summary>
        /// Exposed for other internal clients.  Should be kept in sync with SMALL_PRICE_CHANGE_LOW_MULTIPLIER.
        /// </summary>
        /// <returns></returns>
        internal static string LargePriceChangeDecreaseText()
        {
            return "less than 90% of";
        }

        #endregion

        #region IScriptControl Members

        public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {
            ScriptBehaviorDescriptor descriptor = new ScriptBehaviorDescriptor("FirstLook.Pricing.Website.InventoryCalculator", PctAvgMarketPriceTextBox.ClientID);
            descriptor.AddProperty("unitCost", InventoryUnitCost);
            descriptor.AddProperty("price", Price);
            descriptor.AddProperty("currentPrice", InventoryListPrice);
            descriptor.AddProperty("avgMarketPrice", MarketPricingData.AvgMarketPrice);
            descriptor.AddProperty("minMarketPrice", MarketPricingData.MinMarketPrice);
            descriptor.AddProperty("maxMarketPrice", MarketPricingData.MaxMarketPrice);
            descriptor.AddProperty("minPctMarketAvg", MarketPricingData.MinPctMarketAvg);
            descriptor.AddProperty("minPctMarketValue", MarketPricingData.MinPctMarketValue);
            descriptor.AddProperty("maxPctMarketAvg", MarketPricingData.MaxPctMarketAvg);
            descriptor.AddProperty("maxPctMarketValue", MarketPricingData.MaxPctMarketValue);
            descriptor.AddProperty("hasMarket", MarketPricingData.HasMarket());
            descriptor.AddProperty("hasMileageAdjustedSearch", HasMileageAdjustedSearch);

            descriptor.AddElementProperty("percentageTextBox", PctAvgMarketPriceTextBox.ClientID);
            descriptor.AddElementProperty("priceTextBox", NewInventoryListPriceTextBox.ClientID);
            descriptor.AddElementProperty("potentialProfitLabel", PotentialGrossProfitTextBox.ClientID);
            descriptor.AddElementProperty("sliderContainer", SliderContainerHtmlGenericControl.ClientID);
            descriptor.AddElementProperty("saveButton", InventoryRepriceButton.ClientID);
            descriptor.AddElementProperty("helpDialog", WhatsThisDialog.ClientID);
            return new ScriptDescriptor[] { descriptor };
        }

        public override IEnumerable<ScriptReference> GetScriptReferences()
        {
            ScriptReference rapheal = new ScriptReference("~/Public/Scripts/Lib/raphael.js");
            ScriptReference library = new ScriptReference("~/Public/Scripts/Controls/Pricing.js");
            ScriptReference behavour = new ScriptReference("~/Public/Scripts/Controls/InventoryCalculator.js");
            return new ScriptReference[] { rapheal, library, behavour }; 
        }

        #endregion

        protected void PriceSpinBox_PriceChanged(object sender, PriceChangedEventArgs e)
        {
            RaisePriceChangedEvent(e.OldPrice, e.NewPrice);
        }
    }
}
