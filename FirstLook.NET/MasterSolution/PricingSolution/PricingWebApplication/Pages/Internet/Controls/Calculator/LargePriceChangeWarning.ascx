<%@ Control Language="C#" AutoEventWireup="true" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.Controls.Calculator.Controls_Calculator_LargePriceChangeWarning" Codebehind="LargePriceChangeWarning.ascx.cs" %>

<cwc:Dialog ID="RepriceWarningDialog" runat="server"
		Top="120" Left="520" Width="290" Height="150" AllowResize="false" AllowDrag="false"
		TitleText="Large Price Change Warning"
		Visible="false" OnButtonClick="RepriceWarningDialog_ButtonClick" AllowCancelPostBack="true">
	<ItemTemplate>
		<p>
			WARNING: New List Price <em><asp:Label ID="NewListPriceLabel" runat="server" /></em> is
			<em><asp:Label ID="PriceChangeLabel" runat="server" /></em> the Original Price of
			<em><asp:Label ID="OldListPriceLabel" runat="server" /></em>.
		</p>
	</ItemTemplate>
	<Buttons>
		<cwc:DialogButton ButtonType="Button" ButtonCommand="Ok" IsDefault="true" />
		<cwc:DialogButton ButtonType="Button" ButtonCommand="Cancel" IsCancel="false" />
	</Buttons>
</cwc:Dialog>