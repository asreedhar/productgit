using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Common.WebControls.UI;

namespace FirstLook.Pricing.WebApplication.Pages.Internet.Controls.Calculator
{
    public partial class Controls_Calculator_LargePriceChangeWarning : UserControl
    {
        public event EventHandler AcceptPriceChange;

        public event EventHandler CancelPriceChange;

        private const string CurrencyFormat = "{0:$###,##0;($###,##0);$0}";

        private bool IsDirty = false;


        public void OpenDialog(int? oldListPrice, int newListPrice)
        {
            RepriceWarningDialog.Visible = true;

            OldListPrice = oldListPrice;
            NewListPrice = newListPrice;

            IsDirty = true;
        }

        public void CloseDialog()
        {
            ViewState.Clear();

            RepriceWarningDialog.Visible = false;
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (IsDirty)
            {
                Label newListPriceLabel = RepriceWarningDialog.FindControl("NewListPriceLabel") as Label;
                Label oldListPriceLabel = RepriceWarningDialog.FindControl("OldListPriceLabel") as Label;
                Label priceChangeLabel = RepriceWarningDialog.FindControl("PriceChangeLabel") as Label;
                if (OldListPrice.HasValue)
                {
                    if (NewListPrice > Controls_Calculator_InventoryCalculator.SmallPriceChangeThresholdHigh( OldListPrice.Value ))
                    {
                        // A large price increase
                        priceChangeLabel.Text = Controls_Calculator_InventoryCalculator.LargePriceChangeIncreaseText();
                    }
                    else
                    {
                        // A large price decrease
                        priceChangeLabel.Text = Controls_Calculator_InventoryCalculator.LargePriceChangeDecreaseText();
                    }
                }
                else
                {
                    priceChangeLabel.Text = "more than";
                }
			
                newListPriceLabel.Text = string.Format(CurrencyFormat, NewListPrice); 

                if (OldListPrice.HasValue)
                    oldListPriceLabel.Text = string.Format(CurrencyFormat, OldListPrice.Value);
                else
                    oldListPriceLabel.Text = "N/A";
            }
        }

        protected void RepriceWarningDialog_ButtonClick(object sender, DialogButtonClickEventArgs e)
        {
            switch (e.Button)
            {
                case DialogButtonCommand.Ok:
                    if (AcceptPriceChange != null)
                        AcceptPriceChange(this, EventArgs.Empty);
                    break;
                case DialogButtonCommand.Cancel:
                    if (CancelPriceChange != null)
                        CancelPriceChange(this, EventArgs.Empty);
                    break;
                default:
                    throw new NotImplementedException("The RepriceWarningDialog Dialog does not impletement " + e.Button.ToString() + " Button");
            }
        }

        public int? OldListPrice
        {
            get
            {
                return (int?) ViewState["OldListPrice"];
            }
            protected set
            {
                if (value != OldListPrice)
                {
                    ViewState["OldListPrice"] = value;
                    IsDirty = true;
                }
            }
        }

        public int NewListPrice
        {
            get
            {
                object value = ViewState["NewListPrice"];
                if (value == null)
                    return 0;
                return (int) value;
            }
            protected set
            {
                if (NewListPrice != value)
                {
                    ViewState["NewListPrice"] = value;
                    IsDirty = true;
                }
            }
        }
    }
}