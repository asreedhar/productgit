<%@ Control Language="C#" AutoEventWireup="True" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.Controls.Calculator.Controls_Calculator_InventoryCalculator" Codebehind="InventoryCalculator.ascx.cs" %>

<%@ Register Src="../PriceSpinBox.ascx" TagPrefix="Controls" TagName="PriceSpinBox" %>
<%@ Register Src="InsufficientProfitWarning.ascx" TagPrefix="luc" TagName="InsufficientProfitWarning" %>
<%@ Register Src="LargePriceChangeWarning.ascx" TagPrefix="luc" TagName="LargePriceChangeWarning" %>
<%@ Register Src="OverrideMarketAnalysis.ascx" TagPrefix="luc" TagName="OverrideMarketAnalysis" %>

<pwc:MarketPricing ID="MarketPricingData" runat="server" />

<div class="top_wrapper">
<div>
    <h2 id="Feedback" CssClass="feedback" runat="server" OnPreRender="FeedBack_PreRender"></h2>
    <luc:OverrideMarketAnalysis ID="OverrideMarketAnalysis" runat="server" />
    <luc:InsufficientProfitWarning ID="InsufficientProfitWarning" runat="server" OnAcceptPriceChange="InsufficientProfitWarning_Accept" OnCancelPriceChange="InsufficientProfitWarning_Cancel" />
    <luc:LargePriceChangeWarning ID="LargePriceChangeWarning" runat="server" OnAcceptPriceChange="LargePriceChangeWarning_Accept" OnCancelPriceChange="LargePriceChangeWarning_Cancel" />
    <cwc:MessageBox ID="WhatsThisDialog" runat="server"
        TitleText="Market Pricing Power"
        Top="85" Left="35" Width="505" Height="185" AllowDrag="false" AllowResize="false" AllowCancelPostBack="true"
        Hidden="true">
        <ItemTemplate>
            <p style="font-size:0.8em">Market Pricing Power is the upper range this unit can be priced and still drive online consumer traffic.</p>
            <p style="font-size:0.8em">This range recommends optimal initial internet price for this unit.</p>
            <p style="font-size:0.8em">You should consider the following</p>
            <ul style="font-size:0.8em;">
                <li> - Price higher that the range for units with exceptional condition, <br /> &nbsp;&nbsp;&nbsp;low miles per model year or premium equipment</li>
                <li> - Price lower than the range for units with rough condition or minimal equipment.</li>
            </ul>
        </ItemTemplate>
        <Buttons>
            <cwc:DialogButton ButtonType="Button" ButtonCommand="Ok" Text="OK" IsCancel="true" IsDefault="true" />
        </Buttons>
    </cwc:MessageBox>


    <ul class="control">
		<li class="mktPrce-price">
            <span class="input_unit"> $ </span>
			<asp:TextBox ID="NewInventoryListPriceTextBox" MaxLength="8" runat="server" Text='<%# FormatComma(NewInventoryListPrice) %>' OnTextChanged="NewInventoryListPriceTextBox_TextChanged"></asp:TextBox>
			<asp:Label ID="NewInventoryListPriceLabel" runat="server" Text="Internet Price" AssociatedControlID="NewInventoryListPriceTextBox"></asp:Label>
			<asp:Label ID="InventoryListPriceLabel" runat="server" CssClass="current"></asp:Label>
            <cwc:MoveNextBehaviour runat="server" TargetControlID="NewInventoryListPriceTextBox" ID="NewInventoryListPriceTextBox_MoveNext" />
		</li>
        <li class="mktPrce-max">
            <span class="button"><asp:LinkButton ID="MaxThePriceButton" OnClick="MaxThePriceButton_Click" runat="server" /></span>
            <a id="mpp_help" class="help_text" title="Click For Help">Whats this?</a>
        </li>
		<li class="mktPrce-avg">
            <asp:TextBox ID="PctAvgMarketPriceTextBox" runat="server" MaxLength="3" Text='<%# MarketPricingData.HasMarket() ? FormatPercentage(PctAvgMarketPrice,"NA") : "NA" %>' OnTextChanged="PctAvgMarketPriceTextBox_TextChanged"></asp:TextBox>
            <span class="input_unit"> % </span>
			<asp:Label ID="PctAvgMarketPriceLabel" runat="server" Text="Market Average" AssociatedControlID="PctAvgMarketPriceTextBox" />
			<asp:Label ID="CurrentPctAvgMarketPriceTextBox" runat="server" CssClass="current" />
			<cwc:MoveNextBehaviour runat="server" TargetControlID="PctAvgMarketPriceTextBox" ID="PctAvgMarketPriceTextBox_MoveNext" />
		</li>
		<li class="mktPrce-save">
            <span class="button"><asp:LinkButton ID="InventoryRepriceButton" runat="server" Text="Save Price" OnClick="InventoryRepriceButton_Click" /></span>
            <p><br /></p>
		</li>
    </ul>
</div>

<div class="hr"><hr /></div>

<div id="SliderContainerHtmlGenericControl" runat="server">
</div>

    <p class="rank_control">
    <Controls:PriceSpinBox ID="PriceSpinBox" runat="server" DataSourceID="MarketPricingListObjectDataSource" Price='<%# NewInventoryListPrice %>' />
    </p>

<ul id="market_summary">
	<li class="listings"><asp:Label ID="Label1" runat="server" Text='<%# MarketPricingData.NumListings %>' /> Listings</li>
	<li class="market_day_supply"><asp:Label ID="Label3" runat="server" Text='<%# MarketPricingData.MarketDaySupply %>' /> Market Days Supply</li>
 	<li class="avg_mileage">Average Mileage: <asp:Label ID="Label2" runat="server" Text='<%# string.Format("{0:0,0}",MarketPricingData.AvgVehicleMileage) %>' /></li>	
</ul>
</div>

<ul id="calculator_summary">
    <li >
        <h6>Calculator</h6>
    </li>
    <li class="internet_price">
        <asp:TextBox ID="InternetPriceTextBox" CssClass="mkt-internet-price readonly" runat="server" Text='<%# FormatCurrency(InventoryListPrice) %>' ReadOnly="true" />
        <asp:Label ID="InternetPriceLabel" runat="server" Text="Internet Price" />
    </li>
    <li class="unit_cost">
		<asp:TextBox ID="UnitCostTextBox" CssClass="mkt-Prce-price readonly" runat="server" Text='<%# FormatCurrency(InventoryUnitCost) %>' ReadOnly="true" />
		<asp:Label ID="UnitCostLabel" AssociatedControlID="UnitCostTextBox" runat="server" Text="Unit Cost" />
    </li>
    <li class="potential_gross_profit">
		<asp:TextBox ID="PotentialGrossProfitTextBox" runat="server" Text='<%# FormatCurrency(NewPotentialGrossProfit, "--") %>' CssClass="readonly" ReadOnly="true" />
		<asp:Label ID="PotentialGrossProfitLabel" runat="server" Text="Potential Gross Profit" AssociatedControlID="PotentialGrossProfitTextBox"></asp:Label>
    </li>
</ul>
