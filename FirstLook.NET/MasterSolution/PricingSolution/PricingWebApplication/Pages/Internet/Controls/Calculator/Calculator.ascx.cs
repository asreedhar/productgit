using System;
using System.Collections;
using FirstLook.Pricing.WebControls.UserControls;

namespace FirstLook.Pricing.WebApplication.Pages.Internet.Controls.Calculator
{
    public partial class Controls_Calculator_Calculator : VehicleEntityTypeSwitchControl
    {
        #region Properties
    
        public int NumListings
        {
            get { return FindCalculator().NumListings; }
        }

        public int SearchRadius
        {
            get { return FindCalculator().SearchRadius; }
        }

        public int? Price
        {
            get { return FindCalculator().Price; }
            set { FindCalculator().Price = value; }
        }

        public string RepricePostbackScript
        {
            get
            {
                return FindCalculator().RepricePostbackScript;
            }
        }

        public string PriceClientId
        {
            get { return FindCalculator().PriceClientId; }
        }

        #endregion

        #region Events

        protected object PriceChangingEvent = new object();

        protected object PriceChangedEvent = new object();

        protected object PriceSavedEvent = new object();

        public event EventHandler PriceChanging
        {
            add
            {
                Events.AddHandler(PriceChangingEvent, value);
            }
            remove
            {
                Events.RemoveHandler(PriceChangingEvent, value);
            }
        }

        public event PriceChangedEventHandler PriceChanged
        {
            add
            {
                Events.AddHandler(PriceChangedEvent, value);
            }
            remove
            {
                Events.RemoveHandler(PriceChangedEvent, value);
            }
        }

        public event PriceChangedEventHandler PriceSaved
        {
            add
            {
                Events.AddHandler(PriceSavedEvent, value);
            }
            remove
            {
                Events.RemoveHandler(PriceSavedEvent, value);
            }
        }

        protected void Calculator_PriceChanging(object sender, EventArgs e)
        {
            EventHandler handler = (EventHandler)Events[PriceChangingEvent];
            if (handler != null)
                handler(this, e);
        }

        protected void Calculator_PriceChanged(object sender, PriceChangedEventArgs e)
        {
            PriceChangedEventHandler handler = (PriceChangedEventHandler) Events[PriceChangedEvent];
            if (handler != null)
                handler(this, e);
        }

        protected void Calculator_PriceSaved(object sender, PriceChangedEventArgs e)
        {
            PriceChangedEventHandler handler = (PriceChangedEventHandler)Events[PriceSavedEvent];
            if (handler != null)
                handler(this, e);
        }

        public void OnPriceRankChanged(object sender, PriceRankChangedEventArgs e)
        {
            FindCalculator().OnPriceRankChanged(sender, e);
        }

        #endregion

        #region Data Binding
    
        protected override void PerformDataBinding(IEnumerable data)
        {
            base.PerformDataBinding(data);

            WebControls.UserControls.Calculator calculator = FindCalculator();
            calculator.Visible = true;
            calculator.DataSource = data;
            calculator.DataBind();
        }

        #endregion

        #region Helper Methods
    
        private WebControls.UserControls.Calculator FindCalculator()
        {
            if (IsInventory || IsSalesToolVehicle)
            {
                return (WebControls.UserControls.Calculator) InventoryCalculator;
            }
            else if (IsAppraisal || IsInGroupVehicle || IsOnlineAuctionVehicle)
            {
                return (WebControls.UserControls.Calculator) AppraisalCalculator;
            }
            throw new ApplicationException("No Calculator Enabled");
        }

        #endregion
    }
}