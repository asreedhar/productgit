using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Common.WebControls.UI;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel;
using FirstLook.Pricing.WebControls;
using FirstLook.Pricing.WebControls.UserControls;

namespace FirstLook.Pricing.WebApplication.Pages.Internet.Controls.Calculator
{
    public partial class Controls_Calculator_OverrideMarketAnalysis : ApplicationUserControl, IScriptControl
    {
        private bool IsDirty = false;

        public DateTime? CategorizationOverrideExpiryDate
        {
            get
            {
                return (DateTime?) ViewState["CategorizationOverrideExpiryDate"];
            }
            protected set
            {
                if (Nullable.Compare(CategorizationOverrideExpiryDate,value) != 0)
                {
                    ViewState["CategorizationOverrideExpiryDate"] = value;
                    IsDirty = true;
                }
            }
        }

        public int? DaysFromNow
        {
            get
            {
                if (CategorizationOverrideExpiryDate.HasValue)
                    return CategorizationOverrideExpiryDate.Value.Subtract(DateTime.Today).Days;
                return null;
            }
        }

        protected string CancelPanel
        {
            get
            {
                return (string) ViewState["CancelPanel"];
            }
            set
            {
                ViewState["CancelPanel"] = value;
            }
        }

        protected DateTime? CancelDate
        {
            get
            {
                return (DateTime?)ViewState["CancelDate"];
            }
            set
            {
                ViewState["CancelDate"] = value;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack && !IsInventory())
            {
                CategorizationOverrideExpiryDate = DecisionForm.GetVehiclePricingDecisionInput(Context,Request).CategorizationOverrideExpiryDate;

                CancelDate = CategorizationOverrideExpiryDate;

                if (CategorizationOverrideExpiryDate.HasValue)
                {
                    SetVisibilty("On");
                }
                else
                {
                    SetVisibilty("Off");
                }
            }
        }

        public override void DataBind()
        {
            // not data bound in any way. stupid inheritence.
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            ScriptManager.GetCurrent(Page).RegisterScriptControl(this);

            if (IsDirty)
                DataBindChildren();
        }

        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);

            ScriptManager.GetCurrent(Page).RegisterScriptDescriptors(this);
        }

        protected void EditButton_Click(object sender, EventArgs e)
        {
            SetVisibilty("Edit");
        }

        protected void RemoveButton_Click(object sender, EventArgs e)
        {
            VehiclePricingDecisionInput input = DecisionForm.GetVehiclePricingDecisionInput(Context,Request);
            input.ClearCategorizationOverride();
            input.Save();
            CategorizationOverrideExpiryDate = null;
            CancelDate = null;
            SetVisibilty("Off");
        }

        protected void AddButton_Click(object sender, EventArgs e)
        {
            SetVisibilty("Edit");
        }

        protected void OverrideMarketAnalysisEditDialog_Click(object sender, DialogButtonClickEventArgs e)
        {
            VehiclePricingDecisionInput input = DecisionForm.GetVehiclePricingDecisionInput(Context, Request);

            switch (e.Button)
            {			
                case DialogButtonCommand.Ok:
                    if (CategorizationOverrideExpiryDate.HasValue)
                    {
                        input.OverrideCategorization(CategorizationOverrideExpiryDate.Value);
                        input.Save();
                        CancelDate = CategorizationOverrideExpiryDate;
                        SetVisibilty("On");
                    }
                    break;
                case DialogButtonCommand.Cancel:
                    input = DecisionForm.GetVehiclePricingDecisionInput(Context, Request);
                    CategorizationOverrideExpiryDate = input.CategorizationOverrideExpiryDate;
                    SetVisibilty(CancelPanel);
                    break;		
                default:
                    throw new NotImplementedException("The OverideMarketAnylsis Dialog does not impletement " + e.Button + " Button");
            }
        }

        protected void DaysFromNowTextBox_TextChanged(object sender, EventArgs e)
        {
            int daysFromNow;

            if (Int32.TryParse(((TextBox)sender).Text, out daysFromNow))
            {
                CategorizationOverrideExpiryDate = DateTime.Today.AddDays(daysFromNow);
            }
        }

        protected void DaysFromNowTextBox_PreRender(object sender, EventArgs e)
        {
            TextBox daysFromNowTextBox = sender as TextBox;
            if (daysFromNowTextBox != null)
            {
                daysFromNowTextBox.Text = DaysFromNow.ToString();
            }
        }

        protected void CategorizationOverrideExpiryDateTextBox_PreRender(object sender, EventArgs e)
        {
            TextBox expiryDateTextBox = sender as TextBox;
            if (expiryDateTextBox != null)
            {
                expiryDateTextBox.Text = string.Format("{0:MM/dd/yyyy}", CategorizationOverrideExpiryDate);
            }
        }

        protected void CategorizationOverrideExpiryDateTextBox_TextChanged(object sender, EventArgs e)
        {
            DateTime date;

            if (DateTime.TryParse(((TextBox)sender).Text, out date))
            {
                CategorizationOverrideExpiryDate = date;
            }
        }

        protected void SetVisibilty(string panel)
        {
            if (string.Compare(panel, "On") == 0)
            {
                CancelPanel = panel;
                Show(OverrideMarketAnalysisOnPanel);
                Hide(AddButton);
                OverrideMarketAnalysisEditDialog.Hidden = true;            
            }
            else if (string.Compare(panel, "Off") == 0)
            {
                CancelPanel = panel;
                Hide(OverrideMarketAnalysisOnPanel);
                Show(AddButton);
                OverrideMarketAnalysisEditDialog.Hidden = true;            
            }
            else
            {
                CancelPanel = CancelDate.HasValue ? "On" : "Off";
                Hide(OverrideMarketAnalysisOnPanel);
                Hide(AddButton);
                OverrideMarketAnalysisEditDialog.Hidden = false;
            }
        }

        private static void Show(WebControl control)
        {
            control.Style["display"] = "";
        }

        private static void Hide(WebControl control)
        {
            control.Style["display"] = "none";
        }

        public void CloseDialog()
        {
            OverrideMarketAnalysisEditDialog.Hidden = true;
        }

        #region IScriptControl Members

        public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {		
            ScriptBehaviorDescriptor descriptor = new ScriptBehaviorDescriptor("FirstLook.Pricing.Website.OverrideMarketAnalysis", OverrideMarketAnalysisPanels.ClientID);
            // panels
            descriptor.AddElementProperty("EditDialog", OverrideMarketAnalysisEditDialog.ClientID);
            descriptor.AddElementProperty("OnPanel", OverrideMarketAnalysisOnPanel.ClientID);

            // data regions
            descriptor.AddElementProperty("ExpiryDateTextBox", OverrideMarketAnalysisEditDialog.FindControl("CategorizationOverrideExpiryDateTextBox").ClientID);
            descriptor.AddElementProperty("DaysFromNowTextBox", OverrideMarketAnalysisEditDialog.FindControl("DaysFromNowTextBox").ClientID);

            descriptor.AddElementProperty("DateLabel", OverrideMarketAnalysisOnPanel.ClientID);

            // images
            descriptor.AddElementProperty("DateImage", OverrideMarketAnalysisEditDialog.FindControl("DateImage").ClientID);			

            // warnings
            descriptor.AddElementProperty("DateInPastError", OverrideMarketAnalysisEditDialog.FindControl("DateInPastError").ClientID);
            descriptor.AddElementProperty("DateInFutureError", OverrideMarketAnalysisEditDialog.FindControl("DateInFutureError").ClientID);
            descriptor.AddElementProperty("DateFormatError", OverrideMarketAnalysisEditDialog.FindControl("DateFormatError").ClientID);
            descriptor.AddElementProperty("DaysFormatError", OverrideMarketAnalysisEditDialog.FindControl("DaysFormatError").ClientID);

            // Button
            descriptor.AddElementProperty("AddButton", AddButton.ClientID);
            descriptor.AddElementProperty("EditButton", EditButton.ClientID);
            descriptor.AddElementProperty("RemoveButton", RemoveButton.ClientID);

            return new ScriptDescriptor[] { descriptor };
        }

        public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
        {
            return new ScriptReference[] { new ScriptReference("~/Public/Scripts/Controls/OverrideMarketAnalysis.js") }; 
        }

        #endregion
    }
}