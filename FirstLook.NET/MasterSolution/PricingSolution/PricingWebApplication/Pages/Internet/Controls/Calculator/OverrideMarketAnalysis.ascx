<%@ Control Language="C#" AutoEventWireup="true" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.Controls.Calculator.Controls_Calculator_OverrideMarketAnalysis" Codebehind="OverrideMarketAnalysis.ascx.cs" %>

<asp:Panel ID="OverrideMarketAnalysisPanels" CssClass="inline" style="float: left;" runat="server">
    
<asp:LinkButton ID="AddButton" runat="server" ToolTip="Override Analysis" Text="Override Analysis..." OnClick="AddButton_Click" CssClass="override_analysis" />

<asp:Panel ID="OverrideMarketAnalysisOnPanel" CssClass="emphasis" runat="server">
    <p>
		Analysis overridden until <strong><asp:Label ID="CategorizationOverrideExpiryDateLabel" runat="server" Text='<%# string.Format("{0:MM/dd/yyyy}", CategorizationOverrideExpiryDate) %>'></asp:Label></strong>.
		<asp:LinkButton ID="EditButton" runat="server" ToolTip="Edit" Text="Edit" OnClick="EditButton_Click" /> |
		<asp:LinkButton ID="RemoveButton" runat="server" ToolTip="Remove" Text="Remove" OnClick="RemoveButton_Click" />
    </p>
</asp:Panel>
	        
<cwc:Dialog ID="OverrideMarketAnalysisEditDialog" runat="server"
		TitleText="Override Analysis"
		Top="125" Left="520" Width="300" Height="150" AllowDrag="false" AllowResize="false" AllowCancelPostBack="true"
		Hidden="true" OnButtonClick="OverrideMarketAnalysisEditDialog_Click">
	<ItemTemplate>
            <p runat="server" id="DateInPastError" class="warning" style="display: none;">The override expiry date must be in the future.</p>
            <p runat="server" id="DateInFutureError" class="warning" style="display: none;">The override expiry date must be less than 30 days from now.</p>
            <p runat="server" id="DateFormatError" class="warning" style="display: none;">The override expiry date must be of the format mm/dd/yyyy.</p>
            <p runat="server" id="DaysFormatError" class="warning" style="display: none;">The days from now must be a positive integer.</p>
            <p>
            Suppress pricing alerts until <asp:TextBox ID="DaysFromNowTextBox" runat="server" OnPreRender="DaysFromNowTextBox_PreRender" OnTextChanged="DaysFromNowTextBox_TextChanged" MaxLength="2" Width="1.7em" />
            days from now which is on <asp:TextBox ID="CategorizationOverrideExpiryDateTextBox" runat="server" OnPreRender="CategorizationOverrideExpiryDateTextBox_PreRender" OnTextChanged="CategorizationOverrideExpiryDateTextBox_TextChanged" MaxLength="10" Width="5.5em" />
            <asp:Image ID="DateImage" ImageUrl="~/Public/Images/icon_calendar.gif" runat="server" />
            </p>
	</ItemTemplate>
	<Buttons>
		<cwc:DialogButton ButtonType="Button" ButtonCommand="Ok" Text="OK" IsDefault="true" />
		<cwc:DialogButton ButtonType="Button" ButtonCommand="Cancel" Text="Cancel" IsCancel="true" />
	</Buttons>
</cwc:Dialog>

</asp:Panel>
