using System;
using System.ComponentModel;
using System.Web.UI;
using FirstLook.Common.Core.Utilities;
using FirstLook.Common.WebControls.Helpers;
using FirstLook.Pricing.WebControls;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel;
using FirstLook.Pricing.WebControls.UserControls;

namespace FirstLook.Pricing.WebApplication.Pages.Internet.Controls.Calculator
{
    public partial class Controls_Calculator_AppraisalCalculator : WebControls.UserControls.Calculator
    {
        private bool IsDirty = false;

        #region Properties
    
        public int? PctAvgMarketPrice
        {
            get
            {
                return (int?) ViewState["PctAvgMarketPrice"];
            }
            set
            {
                if (value != PctAvgMarketPrice)
                {
                    ViewState["PctAvgMarketPrice"] = value;
                    OnPropertyChanged("PctAvgMarketPrice");
                }
            }
        }

        public int? AppraisalValue
        {
            get
            {
                return (int?) ViewState["AppraisalValue"];
            }
            set
            {
                if (value != AppraisalValue)
                {
                    ViewState["AppraisalValue"] = value;
                    OnPropertyChanged("AppraisalValue");
                }
            }
        }

        public int? CurrentAppraisalValue
        {
            get
            {
                return (int?) ViewState["CurrentAppraisalValue"];
            }
            set
            {
                if (value != CurrentAppraisalValue)
                {
                    ViewState["CurrentAppraisalValue"] = value;
                    OnPropertyChanged("CurrentAppraisalValue");
                }
            }
        }

        public int? EstimatedReconditionCost
        {
            get
            {
                return (int?) ViewState["EstimatedReconditionCost"];
            }
            set
            {
                if (value != EstimatedReconditionCost)
                {
                    ViewState["EstimatedReconditionCost"] = value;
                    OnPropertyChanged("EstimatedReconditionCost");
                }
            }
        }

        public int? TargetGrossProfit
        {
            get
            {
                return (int?) ViewState["TargetGrossProfit"];
            }
            set
            {
                if (value != TargetGrossProfit)
                {
                    ViewState["TargetGrossProfit"] = value;
                    OnPropertyChanged("TargetGrossProfit");
                }
            }
        }

        public int? TargetSellingPrice
        {
            get
            {
                return (int?) ViewState["TargetSellingPrice"];
            }
            set
            {
                if (value != TargetSellingPrice)
                {
                    RaisePriceChangedEvent(TargetSellingPrice, value);
                    ViewState["TargetSellingPrice"] = value;
                    OnPropertyChanged("TargetSellingPrice");
                }
            }
        }

        public int? VehicleRank
        {
            get
            {
                return (int?) ViewState["VehicleRank"];
            }
            set
            {
                if (value != VehicleRank)
                {
                    ViewState["VehicleRank"] = value;
                    OnPropertyChanged("VehicleRank");
                }
            }
        }

        public int? VehicleCount
        {
            get
            {
                return (int?) ViewState["VehicleCount"];
            }
            set
            {
                if (value != VehicleCount)
                {
                    ViewState["VehicleCount"] = value;
                    OnPropertyChanged("VehicleCount");
                }
            }
        }

        public override string RepricePostbackScript
        {
            get
            {
                return String.Empty;
            }
        }

        public override string PriceClientId
        {
            get { return string.Empty; }
        }

        public override PricingRisk PricingRisk
        {
            get 
            {
                return MarketPricingData.GetPricingRisk(TargetSellingPrice);
            }
        }

        public int? NaaaProfit
        {
            get
            {
                return MarketPricingData.NaaaAverageSalePrice-AppraisalValue;
            }
        }

        public int? NaaaProfitWithRecon
        {
            get
            {
                return MarketPricingData.NaaaAverageSalePrice - (AppraisalValue+EstimatedReconditionCost);
            }
        }

        protected bool HasMileageAdjustedSearch
        {
            get
            {
                if (ViewState["HasMileageAdjustedSearch"] == null)
                {
                    var packageDetails = PackageDetails.GetDealerPricingPackageDetails(OwnerHandle,
                                                                                       SearchHandle,
                                                                                       VehicleHandle);
                    ViewState["HasMileageAdjustedSearch"] = packageDetails.HasMileageAdjustedSearch;
                }

                return (bool)ViewState["HasMileageAdjustedSearch"];
            }
        }

        protected int? AdjustedMaxPrice
        {
            get
            {
                int? maxPrice = MarketPricingData.MaxPrice();
                if (EstimatedReconditionCost.HasValue)
                {
                    maxPrice -= EstimatedReconditionCost;
                }
                if (TargetGrossProfit.HasValue)
                {
                    maxPrice -= TargetGrossProfit;
                }
                if (maxPrice < 0)
                {
                    maxPrice = null;
                }

                return maxPrice;
            }
        }

        #endregion

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            PropertyChanged += Page_PropertyChanged;
        }

        protected override void Page_DataBound(object sender, EventArgs e)
        {
            // store the values in view state so we do not have to return to the db
            MarketPricingData.DataSource = new object[] { DataItem };
            MarketPricingData.DataBind();

            // set up property labels if from relevant post back
            if (!IsSearchSummaryTabContainerPost)
            {
                PctAvgMarketPrice = MarketPricingData.PctAvgMarketPrice;
                AppraisalValue = MarketPricingData.AppraisalValue;
                CurrentAppraisalValue = MarketPricingData.AppraisalValue;
                EstimatedReconditionCost = MarketPricingData.EstimatedReconditioningCost;
                TargetGrossProfit = MarketPricingData.TargetGrossProfit;
                VehicleCount = MarketPricingData.NumComparableListings + 1;

                PriceSpinBox.Price = TargetSellingPrice;
                PriceSpinBox.Mileage = MarketPricingData.VehicleMileage ?? 0;

                if (!IsAppraisal())
                {
                    AppraisalValueLabel.Text = "Max Purchase Amount";
                    EstimatedReconditioningCostLabel.Text = "Additional Costs (Est)";
                    CalculateRepriceButton.Text = "Calculate";
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            DecisionForm.SaveVehiclePricingDecisionInput(Context);

            if (IsDirty)
            {
                DataBindChildren();
            }
        }

        private void Page_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (string.Compare(e.PropertyName, "AppraisalValue", true) == 0 ||
                string.Compare(e.PropertyName, "TargetGrossProfit", true) == 0 ||
                string.Compare(e.PropertyName, "EstimatedReconditioningCost", true) == 0)
            {
                TargetSellingPrice = NewTargetSellingPrice();
            }
            else if (string.Compare(e.PropertyName, "TargetSellingPrice", true) == 0)
            {
                PctAvgMarketPrice = MarketPricingData.CalculatePctAvgMarketPrice(TargetSellingPrice, PctAvgMarketPrice);

                PriceSpinBox.Price = TargetSellingPrice;
            }

            if (!IsAppraisal()) // the appraisal calculator get's used for S&A vehicles too
            {
                VehiclePricingDecisionInput input = DecisionForm.GetVehiclePricingDecisionInput(Context, Request);

                if (string.Compare(e.PropertyName, "AppraisalValue", true) == 0)
                {
                    input.AppraisalValue = AppraisalValue;
                }
                else if (string.Compare(e.PropertyName, "TargetGrossProfit", true) == 0)
                {
                    input.TargetGrossProfit = TargetGrossProfit;
                }
                else if (string.Compare(e.PropertyName, "EstimatedReconditioningCost", true) == 0)
                {
                    input.EstimatedAdditionalCosts = EstimatedReconditionCost;
                }
            }

            IsDirty = true;
        }


        private Boolean IsSearchSummaryTabContainerPost
        { 
            get
            {
                return (
                            Request.Form["__EVENTTARGET"] != null &&
                            Request.Form["__EVENTTARGET"].EndsWith("SearchSummary$SearchSummaryTabContainer")
                       );
            }
        }

        protected void CalculateRepriceButton_Click(object sender, EventArgs e)
        {
            Calculate();

            RaisePriceChangingEvent();
        }

        protected void AppraisalValueTextBox_TextChanged(object sender, EventArgs e)
        {
            AppraisalValue = Int32Helper.ToNullableInt32(RemoveCurrencyFormatting(AppraisalValueTextBox.Text));
        }

        protected void EstimatedReconditioningCostTextBox_TextChanged(object sender, EventArgs e)
        {
            EstimatedReconditionCost = Int32Helper.ToNullableInt32(RemoveCurrencyFormatting(EstimatedReconditioningCostTextBox.Text));
        }

        protected void TargetGrossProfitTextBox_TextChanged(object sender, EventArgs e)
        {
            TargetGrossProfit = Int32Helper.ToNullableInt32(RemoveCurrencyFormatting(TargetGrossProfitTextBox.Text));
        }

        protected void CalculateButton_Click(object sender, EventArgs e)
        {
            Calculate();
        }

        protected void PriceSpinBox_DataBound(object sender, EventArgs e)
        {
            VehicleRank = PriceSpinBox.Rank;
        }

        #endregion

        #region Calculator
    
        public override void Calculate()
        {
            TargetSellingPrice = NewTargetSellingPrice();
        }

        private int? NewTargetSellingPrice()
        {
            return Acc(Acc(Acc(null, AppraisalValue), EstimatedReconditionCost), TargetGrossProfit);
        }

        private static int? Acc(int? a, int? b)
        {
            if (b.HasValue)
            {
                if (a.HasValue)
                {
                    return a + b;
                }
                else
                {
                    return b;
                }
            }
            return a;
        }

        public override void OnPriceRankChanged(object sender, PriceRankChangedEventArgs e)
        {
            VehicleRank = e.Rank;
        }

        #endregion

        #region PriceControl
    
        public override int? Price
        {
            get
            {
                return TargetSellingPrice;
            }
            set
            {
                // throw new NotImplementedException("The appraisal calculator does not allow the price to be set.");
            }
        }

        #endregion

        #region IScriptControl Members

        public override System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {
            ScriptBehaviorDescriptor descriptor = new ScriptBehaviorDescriptor("FirstLook.Pricing.Website.AppraisalCalculator", PctAvgMarketPriceTextBox.ClientID);
            // properties
            descriptor.AddProperty("appraisalValue", AppraisalValue);
            descriptor.AddProperty("currentAppraisalValue", CurrentAppraisalValue);
            descriptor.AddProperty("estimatedReconditioningCost", EstimatedReconditionCost);
            descriptor.AddProperty("targetGrossProfit", TargetGrossProfit);
            descriptor.AddProperty("minMarketPrice", MarketPricingData.MinMarketPrice);
            descriptor.AddProperty("avgMarketPrice", MarketPricingData.AvgMarketPrice);
            descriptor.AddProperty("maxMarketPrice", MarketPricingData.MaxMarketPrice);
            descriptor.AddProperty("hasMarket", MarketPricingData.HasMarket());
            descriptor.AddProperty("auctionProfitAmount", NaaaProfit);
            descriptor.AddProperty("auctionPrice", MarketPricingData.NaaaAverageSalePrice);
            descriptor.AddProperty("auctionProfitWithReconAmount", NaaaProfitWithRecon);
            descriptor.AddProperty("rank", VehicleRank);
            descriptor.AddProperty("hasMileageAdjustedSearch", HasMileageAdjustedSearch);
            descriptor.AddProperty("minPctMarketAvg", MarketPricingData.MinPctMarketAvg);
            descriptor.AddProperty("maxPctMarketAvg", MarketPricingData.MaxPctMarketAvg);
            descriptor.AddProperty("maxAppraisalValue", MarketPricingData.MaxPrice());

            // elements
            descriptor.AddElementProperty("percentageTextBox", PctAvgMarketPriceTextBox.ClientID);
            descriptor.AddElementProperty("appraisalValueTextBox", AppraisalValueTextBox.ClientID);
            descriptor.AddElementProperty("estimatedReconditioningCostTextBox", EstimatedReconditioningCostTextBox.ClientID);
            descriptor.AddElementProperty("targetGrossProfitTextBox", TargetGrossProfitTextBox.ClientID);
            descriptor.AddElementProperty("targetSellingPriceTextBox", TargetSellingPriceTextBox.ClientID);
            descriptor.AddElementProperty("targetSellingPrice1TextBox", TargetSellingPrice1TextBox.ClientID);
            descriptor.AddElementProperty("sliderContainer", SliderContainerHtmlGenericControl.ClientID);
            descriptor.AddElementProperty("saveButton", CalculateRepriceButton.ClientID);
            descriptor.AddElementProperty("helpDialog", WhatsThisDialog.ClientID);
            descriptor.AddElementProperty("maxAppraisalValueElement", MaxThePriceHtmlGeneric.ClientID);
            
            return new ScriptDescriptor[] { descriptor };
        }

        public override System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
        {
            ScriptReference rapheal = new ScriptReference("~/Public/Scripts/Lib/raphael.js");
            ScriptReference library = new ScriptReference("~/Public/Scripts/Controls/Pricing.js");           
            ScriptReference behavour = new ScriptReference("~/Public/Scripts/Controls/AppraisalCalculator.js");
            return new ScriptReference[] { rapheal, library, behavour }; 
        }

        #endregion

        protected void TargetGrossProfitTextBox_PreRender(object sender, EventArgs e)
        {
            if (TargetGrossProfit < 0)
            {
                CssClassHelper.AddClass(TargetGrossProfitTextBox, "negative_value");
            }
            else
            {
                CssClassHelper.RemoveClass(TargetGrossProfitTextBox, "negative_value");
            }
        }
    }
}
