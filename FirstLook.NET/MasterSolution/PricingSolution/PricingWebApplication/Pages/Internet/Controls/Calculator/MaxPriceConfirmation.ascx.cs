using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Common.WebControls.UI;

namespace FirstLook.Pricing.WebApplication.Pages.Internet.Controls.Calculator
{
    public partial class Controls_Calculator_MaxPriceConfirmation : UserControl
    {
        public event EventHandler AcceptPriceChange;

        public event EventHandler CancelPriceChange;

        public void OpenDialog(int listPrice, int potentialGrossProfit, int minGrossProfit)
        {
            Visible = true;

            ListPrice = listPrice;
            Label listPriceLabel = RepriceWarningDialog.FindControl("ListPriceLabel") as Label; 
            listPriceLabel.Text = String.Format("{0:$#,##0}", listPrice);

            Label potentialGrossProfitLabel = RepriceWarningDialog.FindControl("PotentialGrossProfitLabel") as Label;
            potentialGrossProfitLabel.Text = String.Format("{0:$#,##0}", potentialGrossProfit);
            if (potentialGrossProfit < 0)
                potentialGrossProfitLabel.Style["color"] = "red";

            Label minimumGrossProfitLabel = RepriceWarningDialog.FindControl("MinimumGrossProfitLabel") as Label;
            minimumGrossProfitLabel.Text = String.Format("{0:$#,##0}", minGrossProfit);
        }

        public void CloseDialog()
        {
            ViewState.Remove("ListPrice");

            RepriceWarningDialog.Visible = false;
        }

        public int ListPrice
        {
            get
            {
                object value = ViewState["ListPrice"];
                if (value == null)
                    return 0;
                return (int)value;
            }
            protected set { ViewState["ListPrice"] = value; }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (IsPostBack)
                RepriceWarningDialog.Visible = false;
        }

        protected void RepriceWarningDialog_ButtonClick(object sender, DialogButtonClickEventArgs e)
        {
            switch (e.Button)
            {
                case DialogButtonCommand.Ok:
                    if (AcceptPriceChange != null)
                        AcceptPriceChange(this, EventArgs.Empty);
                    break;
                case DialogButtonCommand.Cancel:
                    if (CancelPriceChange != null)
                        CancelPriceChange(this, EventArgs.Empty);
                    break;
                default:
                    throw new NotImplementedException("The RepriceWarningDialog Dialog does not impletement " + e.Button.ToString() + " Button");
            }
        }
    }
}