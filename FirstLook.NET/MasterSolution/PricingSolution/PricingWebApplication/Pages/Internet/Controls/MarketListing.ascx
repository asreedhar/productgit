<%@ Control Language="C#" AutoEventWireup="true" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.Controls.Controls_MarketListing" Codebehind="MarketListing.ascx.cs" %>

<div class="market_listings">
	<div class="inner">
	<cwc:CollapsiblePanel ID="MarketListingCollapsiblePanel" runat="server" Title="Market Listings" Collapsed="true" LazyItemTemplate="true" ButtonID="MarketListingsLinkButton">
		<HeaderTemplate>
			<div class="market_listings_hd">
				<asp:LinkButton ID="MarketListingsLinkButton" runat="server">Market Listings</asp:LinkButton>
				<asp:Label ID="MarketListingsSummaryLabel" runat="server" OnPreRender="MarketListingsSummaryLabel_PreRender"></asp:Label>
				<span class="call_to_action">click here to open</span>
			</div>
		</HeaderTemplate>
		<ItemTemplate>
			<asp:LinkButton ID="MarketListingFindVehicleLinkButton" runat="server" Text="Find My Vehicle" style="position:relative; float:right; bottom:12px; right: 20px" OnClick="MarketListingFindVehicleLinkButton_Click" />

			<asp:Panel runat="server" ID="SimpleSearchOverview" CssClass="simple_search_overview" OnPreRender="SimpleSearchOverview_PreRender">
                <h6>Precision Search: </h6>
                <ul>
                    <li class="segment <%# SegmentCssClass() %>">Segment</li>
                    <li class="bodytype <%# BodyTypeCssClass() %>">Body Type</li>
                    <li class="trim <%# TrimCssClass() %>">Trim</li>
                    <li class="engine <%# EngineCssClass() %>">Engine</li>
                    <li class="transmission <%# TransmissionCssClass() %>">Transmission</li>
                    <li class="drivetraim <%# DriveTrainCssClass() %>">Drive Train</li>
                    <li class="fueltype <%# FuelTypeCssClass() %>">Fuel Type</li>
                    <li class="doors <%# DoorsCssClass() %>">Doors</li>
                    <li class="certified <%# CertifiedCssClass() %>">Certified</li>
                    <li class="color <%# ColorCssClass() %>">Color</li>
                </ul>
            </asp:Panel>

			<asp:GridView
					ID="MarketListingGridView"
					runat="server"
					OnInit="MarketListingGridView_Init"
					OnDataBinding="MarketListingGridView_DataBinding"
					OnDataBound="MarketListingGridView_DataBound"
					OnRowDataBound="MarketListingGridView_RowDataBound"
					OnPreRender="MarketListingGridView_PreRender"
					Width="720px"
					PageSize="25"
					AlternatingRowStyle-CssClass="odd"
					AutoGenerateColumns="false"
					AllowPaging="true"
					AllowSorting="true"
					CssClass="grid small">
				<PagerStyle CssClass="gridPager" />
				<Columns>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:Panel ID="OpenVehicleDescriptionButton" runat="server" CssClass="vehicleDescriptionButton" AlternateText="Open Vehicle Description"/>
							<div style="position:relative;">
							<asp:Panel ID="VehicleDescriptionPanel" runat="server" CssClass="clearfix vehicleDescription" style="display:none;">

								<asp:Panel ID="Panel2" runat="server">
									<ul>
										<li>
											<strong><asp:Label ID="ListingVehicleDescriptionValue" runat="server" Text='<%# Eval("ListingVehicleDescription") %>'></asp:Label></strong>
										</li>
										<li>
											<strong><asp:Label ID="ColorLabel" runat="server" Text="Color" AssociatedControlID="ColorValue"></asp:Label>:</strong>
											<asp:Label ID="ColorValue" runat="server" Text='<%# IfEmpty(Eval("ListingVehicleColor"), "Unknown") %>'></asp:Label>
										</li>
										<li>
											<strong><asp:Label ID="StockLabel" runat="server" Text="Stock #" AssociatedControlID="StockValue"></asp:Label>:</strong>
											<asp:Label ID="StockValue" runat="server" Text='<%# string.Format("{0} ({1})", IfEmpty(Eval("ListingStockNumber"), "--"), IfEmpty(Eval("ListingVin"), "--")) %>'></asp:Label>
										</li>
									</ul>
								</asp:Panel>
								<asp:Panel ID="DescriptionPanel" CssClass="section" runat="server">
									<p>
										<strong><asp:Label ID="DescriptionLabel" runat="server" Text="Seller's Description" AssociatedControlID="DescriptionValue"></asp:Label>:</strong>
										<asp:Label ID="DescriptionValue" runat="server" Text='<%# EncodeHtml(IfEmpty(Eval("ListingSellerDescription"), "None")) %>'></asp:Label>
									</p>
								</asp:Panel>
								<asp:Panel ID="OptionPanel" runat="server" CssClass="section" Visible='<%# !string.IsNullOrEmpty(IfEmpty(Eval("ListingOptions"), string.Empty)) %>'>
									<p>
										<strong><asp:Label ID="OptionLabel" runat="server" Text="Options" AssociatedControlID="OptionValue"></asp:Label>:</strong>
										<asp:Label ID="OptionValue" runat="server" Text='<%# EncodeHtml(Eval("ListingOptions")) %>'></asp:Label>
									</p>
								</asp:Panel>
							</asp:Panel>
							</div>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField
						DataField="Age"
						HeaderText="Age"
						SortExpression="Age" />
					<asp:BoundField
						DataField="Seller"
						HeaderText="Seller"
						SortExpression="Seller" />
					<asp:BoundField
						DataField="VehicleDescription"
						HeaderText="Vehicle Information"
						SortExpression="VehicleDescription" />
					<asp:BoundField
						DataField="ListingCertified"
						HeaderText="Certified"
						HtmlEncode="false"
						SortExpression="ListingCertified" />
					<asp:BoundField
						DataField="VehicleColor"
						HeaderText="Color"
						SortExpression="VehicleColor" />
					<asp:BoundField
						DataField="VehicleMileage"
						DataFormatString="{0:#,###,###;--;--}"
						HeaderText="Mileage"
						SortExpression="VehicleMileage"
						HtmlEncode="False" />
					<asp:BoundField
						DataField="ListPrice"
						DataFormatString="{0:$#,##0;--;--}"
						HeaderText="Internet Price"
						SortExpression="ListPrice"
						ConvertEmptyStringToNull="true"
						NullDisplayText="--"
						HtmlEncode="False" />
					<asp:BoundField
						DataField="PctAvgMarketPrice"
						HeaderText="% Market Avg."
						DataFormatString="{0:#' %';--;--}"
						HtmlEncode="false"
						SortExpression="PctAvgMarketPrice" />
					 <asp:BoundField
						DataField="DistanceFromDealer"
						HeaderText="Distance"
						DataFormatString="{0:#### mi;--;--}"
						HtmlEncode="false"
						SortExpression="DistanceFromDealer" />					
				</Columns>
			</asp:GridView>
		</ItemTemplate>
	</cwc:CollapsiblePanel>
	</div>
</div>
