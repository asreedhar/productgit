﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchSummaryTray.ascx.cs" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.Controls.SearchSummaryTray" %>

<div class="search_tray"><div class="inner">
    <h6>Searching:</h6>
    <p><asp:Literal ID="SearchSummaryText" runat="server" OnPreRender="SearchSummaryText_PreRender" /></p>
    <asp:HyperLink ID="ModifySearchLink" runat="server"
            Text="Modify Search"
            OnInit="ModifySearchLink_Init" />
</div></div>