using System;
using System.Collections;
using System.Globalization;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;
using FirstLook.Pricing.WebControls.UserControls;

namespace FirstLook.Pricing.WebApplication.Pages.Internet.Controls
{
    public partial class Controls_PricingInformation : PriceControl
    {
        #region Properties

        private int DealerId
        {
            get
            {
                return Identity.GetDealerId(Request.QueryString["oh"]);
            }
        }

        private int? ListPrice
        {
            get { return (int?)ViewState["ListPrice"]; }
            set
            {
                if (value != ListPrice)
                {
                    ViewState["ListPrice"] = value;
                }
            }
        }

        private int? MinMarketPrice
        {
            get { return (int?)ViewState["MinMarketPrice"]; }
            set
            {
                if (value != MinMarketPrice)
                {
                    ViewState["MinMarketPrice"] = value;
                }
            }
        }

        private int? AvgMarketPrice
        {
            get { return (int?)ViewState["AvgMarketPrice"]; }
            set
            {
                if (value != AvgMarketPrice)
                {
                    ViewState["AvgMarketPrice"] = value;
                }
            }
        }

        private int? MaxMarketPrice
        {
            get { return (int?)ViewState["MaxMarketPrice"]; }
            set
            {
                if (value != MaxMarketPrice)
                {
                    ViewState["MaxMarketPrice"] = value;
                }
            }
        }

        private int? SearchRadius
        {
            get { return (int?)ViewState["SearchRadius"]; }
            set
            {
                if (value != SearchRadius)
                {
                    ViewState["SearchRadius"] = value;
                }
            }
        }

        private string EStockCardUrl
        {
            get { return (string)ViewState["EStockCardUrl"]; }
            set
            {
                if (value != EStockCardUrl)
                {
                    ViewState["EStockCardUrl"] = value;
                }
            }
        }

        private bool HasKbbAsBook
        {
            get { return ViewState["HasKbbAsBook"] != null; }
            set
            {
                if (value)
                {
                    ViewState["HasKbbAsBook"] = true;
                }
                else
                {
                    ViewState.Remove("HasKbbAsBook");
                }
            }
        }

        private bool HasKbbConsumerTool
        {
            get { return ViewState["HasKbbConsumerTool"] != null; }
            set
            {
                if (value)
                {
                    ViewState["HasKbbConsumerTool"] = true;
                }
                else
                {
                    ViewState.Remove("HasKbbConsumerTool");
                }
            }
        }

        private string KbbLogoUrl
        {
            get { return (string)ViewState["KbbLogoUrl"]; }
            set
            {
                if (value != KbbLogoUrl)
                {
                    ViewState["KbbLogoUrl"] = value;
                }
            }
        }

        private bool? HasAccurateKbbPrice
        {
            get
            {
                if (ViewState["HasAccurateKbbPrice"] == null)
                {
                    return null;
                }

                return (bool)ViewState["HasAccurateKbbPrice"];
            }
            set
            {
                if (value == null) ViewState.Remove("HasAccurateKbbPrice");
                else ViewState["HasAccurateKbbPrice"] = value;
            }
        }

        private int? KbbPrice
        {
            get { return (int?)ViewState["KbbPrice"]; }
            set
            {
                if (value != KbbPrice)
                {
                    ViewState["KbbPrice"] = value;
                }
            }
        }

        private string KbbPublicationDate
        {
            get { return (string)ViewState["KbbPublicationDate"]; }
            set
            {
                if (value != KbbPublicationDate)
                {
                    ViewState["KbbPublicationDate"] = value;
                }
            }
        }

        private bool? HasAccurateNadaPrice
        {
            get
            {
                if (ViewState["HasAccurateNadaPrice"] == null)
                {
                    return null;
                }

                return (bool)ViewState["HasAccurateNadaPrice"];
            }
            set
            {
                if (value == null) ViewState.Remove("HasAccurateNadaPrice");
                else ViewState["HasAccurateNadaPrice"] = value;
            }
        }

        private int? NadaPrice
        {
            get { return (int?)ViewState["NadaPrice"]; }
            set
            {
                if (value != NadaPrice)
                {
                    ViewState["NadaPrice"] = value;
                }
            }
        }

        private string NadaPublicationDate
        {
            get { return (string)ViewState["NadaPublicationDate"]; }
            set
            {
                if (value != NadaPublicationDate)
                {
                    ViewState["NadaPublicationDate"] = value;
                }
            }
        }

        private int? EdmundsPrice
        {
            get { return (int?)ViewState["EdmundsPrice"]; }
            set
            {
                if (value != EdmundsPrice)
                {
                    ViewState["EdmundsPrice"] = value;
                }
            }
        }

        private string JDPowerUrl
        {
            get { return (string) ViewState["JDPowerUrl"]; }
            set
            {
                if (value != JDPowerUrl)
                {
                    ViewState["JDPowerUrl"] = value;
                }
            }
        }

        private int? JDPowerAvgSellingPrice
        {
            get { return (int?) ViewState["JDPowerAvgSellingPrice"]; }
            set
            {
                if (value != JDPowerAvgSellingPrice)
                {
                    ViewState["JDPowerAvgSellingPrice"] = value;
                }
            }
        }

        private string JDPowerPeriodBeginDate
        {
            get { return (string) ViewState["JDPowerPeriodBeginDate"]; }
            set
            {
                if (value != JDPowerPeriodBeginDate)
                {
                    ViewState["JDPowerPeriodBeginDate"] = value;
                }
            }
        }

        private int? StoreAvgSellingPrice
        {
            get { return (int?) ViewState["StoreAvgSellingPrice"]; }
            set
            {
                if (value != StoreAvgSellingPrice)
                {
                    ViewState["StoreAvgSellingPrice"] = value;
                }
            }
        }

        private string PerformanceAnalyzerUrl
        {
            get { return (string)ViewState["PerformanceAnalyzerUrl"]; }
            set
            {
                if (value != PerformanceAnalyzerUrl)
                {
                    ViewState["PerformanceAnalyzerUrl"] = value;
                }
            }
        }

        private string StoreBeginDate
        {
            get { return (string)ViewState["StoreBeginDate"]; }
            set
            {
                if (value != StoreBeginDate)
                {
                    ViewState["StoreBeginDate"] = value;
                }
            }
        }

        private string StoreEndDate
        {
            get { return (string)ViewState["StoreEndDate"]; }
            set
            {
                if (value != StoreEndDate)
                {
                    ViewState["StoreEndDate"] = value;
                }
            }
        }

        private int? KbbVersusListPrice
        {
            //null > 0 -> false, so return null.
            get { return ListPrice > 0 ? KbbPrice - ListPrice : null; }
        }

        private int? NadaVersusListPrice
        {
            get { return ListPrice > 0 ? NadaPrice - ListPrice : null; }
        }

        private int? EdmundsVersusListPrice
        {
            get { return ListPrice > 0 ? EdmundsPrice - ListPrice : null; }
        }

        private int? JDPowerVersusListPrice
        {
            get { return ListPrice > 0 ? JDPowerAvgSellingPrice - ListPrice : null; }
        }

        private int? StoreAvgSellingVersusListPrice
        {
            get { return ListPrice > 0 ? StoreAvgSellingPrice - ListPrice : null; }
        }

        public bool HasJDPower
        {
            get { return Convert.ToBoolean(ViewState["HasJDPower"]); }
            set
            {
                if (value != HasJDPower)
                {
                    ViewState["HasJDPower"] = value;
                }
            }
        }


        public bool HasEdmundsTmv
        {
            get { return Convert.ToBoolean(ViewState["HasEdmundsTmv"]); }
            set
            {
                if (value != HasEdmundsTmv)
                {
                    ViewState["HasEdmundsTmv"] = value;
                }
            }
        }

        public override int? Price
        {
            get { return ListPrice; }
            set { ListPrice = value; }
        }

        #endregion

        protected override void PerformDataBinding(IEnumerable data)
        {
            if (data != null)
            {
                IEnumerator en = data.GetEnumerator();
                if (en.MoveNext())
                {
                    PricingInformation information = (PricingInformation) en.Current;

                    ListPrice = information.ListPrice;

                    MinMarketPrice = information.MinMarketPrice;
                    AvgMarketPrice = information.AvgMarketPrice;
                    MaxMarketPrice = information.MaxMarketPrice;
                    SearchRadius = information.SearchRadius;

                    HasKbbAsBook = information.HasKbbAsBook;
                    HasKbbConsumerTool = information.HasKbbConsumerTool;

                    if (!string.IsNullOrEmpty(information.StockNumber))
                    {
                        EStockCardUrl = string.Format(CultureInfo.InvariantCulture,
                                               "/IMT/EStock.go?stockNumberOrVin={0}&isPopup=true",
                                               information.StockNumber);
                    }
                    else
                    {
                        EStockCardUrl = string.Empty;
                    }

                    KbbLogoUrl = GetKbbUrl();

                    KbbPrice = information.KbbPrice;
                    KbbPublicationDate = information.KbbPublicationDate;

                    HasAccurateKbbPrice = information.KbbPriceAccurate;

                    NadaPrice = information.NadaPrice;
                    NadaPublicationDate = information.NadaPublicationDate;
                    HasAccurateNadaPrice = information.NadaPriceAccurate;

                    EdmundsPrice = information.EdmundsPrice;

                    //JDPowerUrl =
                    //    string.Format(
                    //        CultureInfo.InvariantCulture,
                    //        "/pricing/Pages/JDPower/JDPower.aspx?token=DEALER_SYSTEM_COMPONENT&vehicleCatalogId={0}&modelYear={1}",
                    //        information.VehicleCatalogId, information.ModelYear);
                    //JDPowerAvgSellingPrice = information.JdPowerPrice;
                    //JDPowerPeriodBeginDate = information.JdPowerPeriodBeginDate.ToString();

                    StoreAvgSellingPrice = information.StorePrice;
                    PerformanceAnalyzerUrl =
                        string.Format(
                            "/NextGen/PricingAnalyzer.go?groupingDescriptionId={0}&year={1}&stockNumber={2}&inventoryItem=true",
                            information.GroupingDescriptionId,
                            information.ModelYear,
                            information.StockNumber);

                    StoreBeginDate = string.Format(CultureInfo.InvariantCulture, "{0:MM/dd/yyyy}",
                                                   information.StorePeriodBeginDate);
                    StoreEndDate = string.Format(CultureInfo.InvariantCulture, "{0:MM/dd/yyyy}",
                                                 information.StorePeriodEndDate);

                    DataBindChildren();
                }
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            PopulateConsumerGuides();
            //PopulateJDPower();
            PopulatePricingAnalyzer();

            //if (!HasJDPower || IsSalesToolVehicle())
            //{
            //    JDPowerHyperLink.NavigateUrl = string.Empty;
            //    JDPowerLogoLink.NavigateUrl = string.Empty;
            //    JDPowerHyperLink.CssClass += " noHref";
            //    JDPowerLogoLink.CssClass += " noHref";
            //}
        }

        protected void PopulateConsumerGuides()
        {
            StockCardLink.NavigateUrl = EStockCardUrl;
            NADALink.NavigateUrl = EStockCardUrl;
            KBBLogoLink.NavigateUrl = KbbLogoUrl;

            if (!HasEdmundsTmv)
            {
                TMVLink.NavigateUrl = string.Empty;
                TMVLink.CssClass += " noHref";
            }
            else
            {
                TMVLink.NavigateUrl =
                    string.Format("/pricing/Pages/Edmunds/TmvCalculator.aspx?oh={0}&vh={1}", Request.QueryString["oh"],
                                  Request.QueryString["vh"]);
                TMVLink.Attributes["onclick"] =
                    string.Format(
                        "OpenPopup(this.href,this.target,function ReloadData() {{ window.document.getElementById('{0}').click(); }}); return false;",
                                  RefreshData.ClientID);
            }

            KBB_RetailPrice.Text = FormatCurrency(KbbPrice);
            KBB_Dataset.Text = KbbPublicationDate;
            if (HasAccurateKbbPrice.HasValue && !HasAccurateKbbPrice.Value)
            {
                KBB_RetailPrice.CssClass += " inaccurate_value";
                KBB_RetailPrice.ToolTip = "Inaccurate Value";

                KBB_RetailPrice.NavigateUrl = KbbLogoUrl;
            }
            else
            {
                KBB_RetailPrice.CssClass.Replace(" inaccurate_value", string.Empty);
                KBB_RetailPrice.ToolTip = string.Empty;

                KBB_RetailPrice.NavigateUrl = string.Empty;
            }


            NADA_RetailPrice.Text = FormatCurrency(NadaPrice);
            NADA_Dataset.Text = NadaPublicationDate;
            if (HasAccurateNadaPrice.HasValue && !HasAccurateNadaPrice.Value)
            {
                NADA_RetailPrice.CssClass += " inaccurate_value";
                NADA_RetailPrice.ToolTip = "Inaccurate Value";

                NADA_RetailPrice.NavigateUrl = EStockCardUrl;
            }
            else
            {
                NADA_RetailPrice.CssClass.Replace(" inaccurate_value", string.Empty);
                NADA_RetailPrice.ToolTip = string.Empty;
                NADA_RetailPrice.NavigateUrl = string.Empty;
            }

            TMV_RetailPrice.Text = FormatCurrency(EdmundsPrice);
            if (EdmundsPrice == null && HasEdmundsTmv)
            {
                TMV_RetailPrice.CssClass += " no_value";
                TMV_RetailPrice.ToolTip = "TMV Has No Value";
                TMV_RetailPrice.NavigateUrl = string.Format("/pricing/Pages/Edmunds/TmvCalculator.aspx?oh={0}&vh={1}", Request.QueryString["oh"],
                                  Request.QueryString["vh"]);
                TMVLink.Attributes["onclick"] =
                    string.Format("OpenPopup(this.href,this.target,function ReloadData() {{ window.document.getElementById('{0}').click(); }}); return false;",
                                  RefreshData.ClientID);
            }
            else
            {
                TMV_RetailPrice.CssClass = TMV_RetailPrice.CssClass.Replace(" no_value", string.Empty);

                TMV_RetailPrice.NavigateUrl = string.Empty;
                TMV_RetailPrice.Attributes["onclick"] = string.Empty;
            }

            if (KbbVersusListPrice != null || NadaVersusListPrice != null || EdmundsVersusListPrice != null)
            {
                ConsumerGuideFooter_Panel.Visible = true;
            }

            if (KbbVersusListPrice != null)
            {
                KBBFooter_Panel.Visible = true;
                KBB_ProfitLabel.Text = FormatAbsCurrency(KbbVersusListPrice);
                KBB_AboveBelowLabel.Text = GetAboveBelowText(KbbVersusListPrice);
            }

            if (NadaVersusListPrice != null)
            {
                NADAFooter_Panel.Visible = true;
                NADA_ProfitLabel.Text = FormatAbsCurrency(NadaVersusListPrice);
                NADA_AboveBelowLabel.Text = GetAboveBelowText(NadaVersusListPrice);
            }

            if (EdmundsPrice != null)
            {
                TMVFooter_Panel.Visible = true;
                TMV_ProfitLabel.Text = FormatAbsCurrency(EdmundsVersusListPrice);
                TMV_AboveBelowLabel.Text = GetAboveBelowText(EdmundsVersusListPrice);
            }
        }

        //protected void PopulateJDPower()
        //{
        //    JDPowerHyperLink.NavigateUrl = JDPowerUrl;
        //    JDPowerLogoLink.NavigateUrl = JDPowerUrl;
        //    JDPower_AvgSellingPrice.Text = FormatAbsCurrency(JDPowerAvgSellingPrice);
        //    JDPower_BeginDate.Text = JDPowerPeriodBeginDate;

        //    if (JDPowerVersusListPrice != null)
        //    {
        //        JDPowerFooter_Panel.Visible = true;
        //        JDPower_ProfitLabel.Text = FormatAbsCurrency(JDPowerVersusListPrice);
        //        JDPower_AboveBelowLabel.Text = GetAboveBelowText(JDPowerVersusListPrice);
        //    }
        //}

        protected void PopulatePricingAnalyzer()
        {
            PerformanceAnalyzerHyperLink.NavigateUrl = PerformanceAnalyzerUrl;
            Store_AvgSellingPrice.Text = FormatCurrency(StoreAvgSellingPrice);
            Store_BeginDate.Text = StoreBeginDate;
            Store_EndDate.Text = StoreEndDate;

            if (StoreAvgSellingPrice != null)
            {
                StorePerformanceFooter_Panel.Visible = true;
                StorePerformance_ProfitLabel.Text = FormatAbsCurrency(StoreAvgSellingVersusListPrice);
                StorePerformance_AboveBelowLabel.Text = GetAboveBelowText(StoreAvgSellingVersusListPrice);
            }
        }

        private string GetKbbUrl()
        {
            string kbbUrl = EStockCardUrl;

            VehicleType vehType = VehicleTypeCommand.Execute(VehicleHandle);

            //PER VINCE MEASE:
            //
            //if dealer does not have KBB as a Guide Book and does not have KBB Stand Alone -> link to KBB.com
            //else
            //      if Appraisal: 
            //          if dealer has KBB as a Guide Book or has KBB Stand Alone -> link to the KBB Consumer Tool.  
            //      if Inventory:
            //          if dealer has KBB as a Guide Book and has KBB Stand Alone -> link to estock card
            //          else if dealer has KBB Stand Alone -> link to the KBB Consumer Tool

            if (!HasKbbAsBook && !HasKbbConsumerTool)
            {
                kbbUrl = "http://www.kbb.com/";
            }
            else
            {
                if (vehType == VehicleType.Appraisal || (vehType == VehicleType.Inventory && !HasKbbAsBook))
                {
                    string vin = VehicleHelper.GetVin(OwnerHandle, VehicleHandle);
                    int? mileage = null;

                    if (vehType == VehicleType.Inventory)
                    {
                        Inventory inv = Inventory.GetInventory(OwnerHandle, VehicleHandle);
                        mileage = inv.Mileage;
                    }
                    else if (vehType == VehicleType.Appraisal)
                    {
                        Appraisal app = Appraisal.GetAppraisal(OwnerHandle, VehicleHandle);
                        mileage = app.Mileage;
                    }

                    if (mileage.HasValue)
                    {
                        kbbUrl = String.Format(CultureInfo.InvariantCulture,
                                               "/IMT/KbbConsumerValueLandingAction.go?dealerId={0}&vin={1}&mileage={2}",
                                               DealerId, vin, mileage);
                    }
                }
            }

            return kbbUrl;
        }

        protected static string FormatCurrency(object value)
        {
            if (value == null || value.Equals(DBNull.Value))
                return "--";
            return string.Format("{0:#,###,##0;(#,###,##0);0}", value);
        }

        protected static string FormatAbsCurrency(object value)
        {
            if (value == null || value.Equals(DBNull.Value))
                return "--";
            return string.Format("{0:$#,###,##0;$#,###,##0;$0}", value);
        }

        protected static string GetAboveBelowText(int? value)
        {
            return (value > 0) ? "below" : "above";
        }

        protected void RefreshData_Click(object sender, EventArgs e)
        {
            RequiresDataBinding = true;
        }
    }
}