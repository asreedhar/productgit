﻿using System;
using System.Collections;
using System.Collections.Generic;
using FirstLook.DomainModel.Lexicon.Categorization;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search;
using FirstLook.Pricing.WebControls;

namespace FirstLook.Pricing.WebApplication.Pages.Internet.Controls
{
    public partial class SearchSummaryTray : System.Web.UI.UserControl
    {
        #region Properties        

        private string ModifySearchUrl
        {
            get
            {
                return SearchHelper.GetModifySearchUrl(Request);
            }
        }

        private SearchType _activeSearchType;

        public SearchType ActiveSearchType
        {
            get
            {
                if (_activeSearchType == SearchType.Undefined)
                {
                    _activeSearchType = SearchHelper.GetSearchSummaryCollection(Context).ActiveSearchSummary.SearchType;
                }

                return _activeSearchType;
            }
            set
            {
                _activeSearchType = value;
            }
        }

        private bool IsPrecisionSearch
        {
            get
            {
                return ActiveSearchType == SearchType.Precision;
            }
        }

        private SearchSummaryInfo SearchSummaryInfo
        {
            get
            {
                return SearchHelper.GetSearchSummaryInfo(Context);
            }
        }

        private Catalog Catalog
        {
             get
             {
                 return SearchSummaryInfo.Catalog;
             }
        }

        private string _searchSummaryInfoText;

        private string SearchSummaryInfoText
        {
            get
            {
                if (!IsPrecisionSearch)
                {
                    return SearchSummaryInfo.YearMakeModel;
                }

                if (string.IsNullOrEmpty(_searchSummaryInfoText))
                {
                    List<String> attributes = new List<String>();

                    const string seriesTitle = "Trim";
                    const string engineTitle = "Engine";
                    const string transmissionTitle = "Transmission";
                    const string driveTrainTitle = "Drive Train";
                    const string passengerDoorTitle = "Doors";
                    const string bodyTypeTitle = "Body Type";
                    const string fuelTypeTile = "Fuel Type";
                    const string segmentTitle = "Segment";

                    attributes.Add(GetAttributeText(SearchSummaryInfo.Series, SearchSummaryInfo.Catalog.Series,
                                                    typeof (Series), seriesTitle));
                    attributes.Add(GetAttributeText(SearchSummaryInfo.Engines, SearchSummaryInfo.Catalog.Engines,
                                                    typeof (Engine), engineTitle));
                    attributes.Add(GetAttributeText(SearchSummaryInfo.Transmissions, SearchSummaryInfo.Catalog.Transmissions,
                                                    typeof (Transmission), transmissionTitle));
                    attributes.Add(GetAttributeText(SearchSummaryInfo.DriveTrains, SearchSummaryInfo.Catalog.DriveTrains,
                                                    typeof (DriveTrain), driveTrainTitle));
                    attributes.Add(GetAttributeText(SearchSummaryInfo.FuelTypes, SearchSummaryInfo.Catalog.FuelTypes,
                                                    typeof (FuelType), fuelTypeTile));
                    attributes.Add(GetAttributeText(SearchSummaryInfo.PassengerDoors,
                                                    SearchSummaryInfo.Catalog.PassengerDoors,
                                                    typeof (PassengerDoor), passengerDoorTitle));
                    if (Catalog.Segments.Count == 1 && (Catalog.Segments[0].IsTruck || Catalog.Segments[0].IsVan))
                    {
                        attributes.Add(GetAttributeText(SearchSummaryInfo.BodyTypes, SearchSummaryInfo.Catalog.BodyTypes,
                                                        typeof (BodyType), bodyTypeTitle));
                    }
                    else
                    {
                        attributes.Add(GetAttributeText(SearchSummaryInfo.Segments, SearchSummaryInfo.Catalog.Segments,
                                                        typeof (Segment), segmentTitle));
                    }

                    _searchSummaryInfoText = string.Join(" | ", attributes.ToArray());
                }

                return _searchSummaryInfoText;
            }
        }

        #endregion       

        public void ResetSearchProperties()
        {
            _activeSearchType = SearchType.Undefined;
            _searchSummaryInfoText = null;
        }

        private static string GetAttributeText(ICollection<INameable> searchSelections, ICollection catalog, Type type, string title)
        {
            const string allTitle = "All ";

            if (searchSelections.Count == catalog.Count && catalog.Count != 1)
            {
                return allTitle + title + (title.EndsWith("s") ? "es" : "s");
            }
            return GetNames(searchSelections, type, title);
        }

        private static string GetNames(ICollection<INameable> namableList, Type type, string title)
        {
            List<string> names = new List<string>();

            Dictionary<string, string> replacements = new Dictionary<string, string>();

            if (namableList.Count > 0 && type == typeof(Engine))
            {
                replacements.Add("Cylinder", "Cyl");
                replacements.Add("Engine", string.Empty);
            }

            if (namableList.Count > 0 && type == typeof(BodyType))
            {
                replacements.Add("pickup", string.Empty);
                replacements.Add("Pickup", string.Empty);
            }

            foreach (INameable nameable in namableList)
            {
                if (nameable == null)
                {
                    names.Add("Other " + title);
                    continue;
                }

                string name = nameable.Name;

                if (replacements.Count > 0)
                {                    
                    foreach (KeyValuePair<string, string> replacement in replacements)
                    {
                        name = name.Replace(replacement.Key, replacement.Value);
                    }                    
                }
                
                if (string.IsNullOrEmpty(name))
                {
                    names.Add("Base " + title);
                }
                else
                {
                    names.Add(name);
                }
            }

            string suffix = string.Empty;

            if (namableList.Count > 0 && type == typeof(PassengerDoor))
            {
                suffix = " Doors";
            }

            names.RemoveAll(string.IsNullOrEmpty);

            names.Sort();

            return string.Join(", ", names.ToArray()) + suffix;
        }

        protected void ModifySearchLink_Init(object sender, EventArgs e)
        {
            ModifySearchLink.NavigateUrl = ModifySearchUrl;
        }

        protected void SearchSummaryText_PreRender(object sender, EventArgs e)
        {
            SearchSummaryText.Text = SearchSummaryInfoText;
        }
    }
}