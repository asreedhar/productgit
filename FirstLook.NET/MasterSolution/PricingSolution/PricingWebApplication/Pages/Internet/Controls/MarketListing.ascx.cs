using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Common.WebControls;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search;
using FirstLook.Pricing.WebControls;

namespace FirstLook.Pricing.WebApplication.Pages.Internet.Controls
{
    public partial class Controls_MarketListing : UserControl, IScriptControl
    {
        private ScriptManager manager;

        #region Properties

        public string DataSourceID
        {
            get { return MarketListingGridView.DataSourceID; }
            set { MarketListingGridView.DataSourceID = value; }
        }

        public string SearchDataSourceID
        {
            get { return (string)ViewState["SearchDataSourceID"]; }
            set { ViewState["SearchDataSourceID"] = value; }
        }

        public SearchType SearchType
        {
            get { return (SearchType)ViewState["SearchType"]; }
            set { ViewState["SearchType"] = value; }
        }

        protected int NumListings
        {
            get
            {
                SearchSummaryCollection collection = SearchHelper.GetSearchSummaryCollection(Context);

                foreach (SearchSummary summary in collection)
                {
                    if (summary.SearchType == SearchType)
                    {
                        return summary.Units + 1;
                    }
                }

                return collection.ActiveSearchSummary.Units + 1;
            }
        }

        protected int SearchRadius
        {
            get
            {
                return SearchHelper.GetSearch(Context).Distance.Value;
            }
        }
        #endregion

        #region Page Events

        public override void DataBind()
        {
            if (!MarketListingCollapsiblePanel.Collapsed)
            {
                MarketListingGridView.PageIndex = 0;

                descriptors.Clear();

                base.DataBind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SearchType = SearchHelper.GetSearchSummaryCollection(Context).ActiveSearchSummary.SearchType;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!DesignMode)
            {
                manager = ScriptManager.GetCurrent(Page);

                if (manager != null)
                    manager.RegisterScriptControl(this);
                else
                    throw new ApplicationException("You must have a ScriptManager!");
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);

            if (!DesignMode)
                manager.RegisterScriptDescriptors(this);
        }

        protected void MarketListingGridView_Init(object s, EventArgs e)
        {
            MarketListingGridView.Columns[2].Visible = !IsSellerNameSuppressed.Execute(Request.QueryString["oh"]);
        }

        protected void MarketListingGridView_DataBinding(object sender, EventArgs e)
        {
            descriptors.Clear();
        }

        protected void MarketListingGridView_DataBound(object s, EventArgs e)
        {
            MarketListingGridView.Visible = MarketListingGridView.Rows.Count != 0;
        }

        protected void MarketListingGridView_RowDataBound(object s, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell th in e.Row.Cells)
                {
                    if (th.HasControls())
                    {
                        LinkButton lnk = (LinkButton)th.Controls[0];
                        if (lnk != null && MarketListingGridView.SortExpression == lnk.CommandArgument)
                        {
                            th.CssClass += (MarketListingGridView.SortDirection == SortDirection.Ascending ? "ascending" : "descending");
                        }
                    }
                }
            }
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (DataBinder.GetPropertyValue(e.Row.DataItem, "PctAvgMarketPrice").ToString() == string.Empty ||
                    DataBinder.GetPropertyValue(e.Row.DataItem, "PctAvgMarketPrice").ToString() == "0")
                {
                    foreach (TableCell td in e.Row.Cells)
                    {
                        if (td.Text.Contains("--"))
                            td.ToolTip = "Vehicle not used in calculating market average price.";
                    }
                }

                if ((bool) DataBinder.GetPropertyValue(e.Row.DataItem, "IsAnalyzedVehicle"))
                {
                    e.Row.CssClass += " analyzed";
                }

                bool value;
                string text = DataBinder.GetPropertyValue(e.Row.DataItem, "ListingCertified", null);
                if (bool.TryParse(text, out value) && value)
                {
                    e.Row.Cells[4].CssClass = " certified";
                }
                else
                {
                    e.Row.Cells[4].CssClass = " not_certified";
                }
            }
        }

        protected void MarketListingGridView_PreRender(object s, EventArgs e)
        {
            if (MarketListingGridView.Visible)
            {
                foreach (GridViewRow row in MarketListingGridView.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        ScriptBehaviorDescriptor descriptor = new ScriptBehaviorDescriptor("FirstLook.Pricing.Website.MarketListing", row.Cells[0].FindControl("OpenVehicleDescriptionButton").ClientID);
                        descriptor.AddElementProperty("panel", row.Cells[0].FindControl("VehicleDescriptionPanel").ClientID);
                        descriptors.Add(descriptor);
                    }
                }
            }
        }

        protected void MarketListingFindVehicleLinkButton_Click(object sender, EventArgs e)
        {
            IDataSource source = DataBoundControlHelper.FindDataSourceControl(this, SearchDataSourceID);

            string ex = MarketListingGridView.SortExpression;
            if (MarketListingGridView.SortDirection == SortDirection.Descending && !string.IsNullOrEmpty(ex))
                ex += " DESC";

            DataSourceSelectArguments arguments = new DataSourceSelectArguments(
                ex,
                MarketListingGridView.PageSize * MarketListingGridView.PageIndex,
                MarketListingGridView.PageSize);

            source.GetView(string.Empty).Select(arguments, MarketListingFindVehicleLinkButton_ClickCallback);
        }

        protected void MarketListingFindVehicleLinkButton_ClickCallback(IEnumerable data)
        {
            if (data != null)
            {
                IEnumerator en = data.GetEnumerator();

                if (en.MoveNext())
                {
                    int rowIndex = (int)en.Current;

                    MarketListingGridView.PageIndex = rowIndex / MarketListingGridView.PageSize;
                }
            }
        }

        #endregion

        #region IScriptControl Members

        private readonly List<ScriptDescriptor> descriptors = new List<ScriptDescriptor>();

        public IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {
            return descriptors;
        }

        public IEnumerable<ScriptReference> GetScriptReferences()
        {
            return new ScriptReference[] { new ScriptReference("~/Public/Scripts/Controls/Pricing.js"), new ScriptReference("~/Public/Scripts/Controls/MarketListing.js") };
        }

        #endregion

        protected static string EncodeHtml(object value)
        {
            if (value == null || value == DBNull.Value)
                return string.Empty;
            else if (value is string)
                return EncodeHtml((string)value);
            return EncodeHtml(value.ToString());
        }

        protected static string EncodeHtml(string text)
        {
            if (string.IsNullOrEmpty(text))
                return string.Empty;
            return HttpUtility.HtmlEncode(text);
        }

        protected static string IfEmpty(object value, string replacement)
        {
            if (value == null || value.Equals(DBNull.Value))
                return replacement;
            string text = value.ToString();
            if (string.IsNullOrEmpty(text))
                return replacement;
            return text;
        }

        protected void MarketListingsSummaryLabel_PreRender(object sender, EventArgs e)
        {
            ((ITextControl)sender).Text = string.Format("{0} results within {1} miles", NumListings, SearchRadius);
        }

        #region Search Summary Check Boxes

        private const string EnabledClass = "enabled";

        private const string DisabledClass = "disabled";

        private SearchSummary SearchSummary
        {
            get
            {
                if (SearchType == SearchType.YearMakeModel)
                {
                    return SearchHelper.GetSearchSummaryCollection(Context).YearMakeModelSearchSummary;
                }
                else
                {
                    return SearchHelper.GetSearchSummaryCollection(Context).PrecisionSearchSummary;
                }
            }
        }

        protected string SegmentCssClass()
        {
            return (SearchSummary.IsSegmentSpecified) ? EnabledClass : DisabledClass;
        }

        protected string BodyTypeCssClass()
        {
            return (SearchSummary.IsBodyTypeSpecified) ? EnabledClass : DisabledClass;
        }

        protected string TrimCssClass()
        {
            return (SearchSummary.IsTrimSpecified) ? EnabledClass : DisabledClass;
        }

        protected string EngineCssClass()
        {
            return (SearchSummary.IsEngineSpecified) ? EnabledClass : DisabledClass;
        }

        protected string TransmissionCssClass()
        {
            return (SearchSummary.IsTransmissionSpecified) ? EnabledClass : DisabledClass;
        }

        protected string DriveTrainCssClass()
        {
            return (SearchSummary.IsDriveTrainSpecified) ? EnabledClass : DisabledClass;
        }

        protected string FuelTypeCssClass()
        {
            return (SearchSummary.IsFuelTypeSpecified) ? EnabledClass : DisabledClass;
        }

        protected string DoorsCssClass()
        {
            return (SearchSummary.IsDoorsSpecified) ? EnabledClass : DisabledClass;
        }

        protected string CertifiedCssClass()
        {
            return (SearchSummary.IsCertificationSpecified) ? EnabledClass : DisabledClass;
        }

        protected string ColorCssClass()
        {
            return (SearchSummary.IsColorSpecified) ? EnabledClass : DisabledClass;
        }

        #endregion

        protected void SimpleSearchOverview_PreRender(object sender, EventArgs e)
        {
            SimpleSearchOverview.Visible = (SearchType == SearchType.Precision);
        }
    }
}