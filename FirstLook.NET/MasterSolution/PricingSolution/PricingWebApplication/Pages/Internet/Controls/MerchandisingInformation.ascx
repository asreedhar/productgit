<%@ Control Language="C#" AutoEventWireup="true" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.Controls.ControlsMerchandisingInformation" Codebehind="MerchandisingInformation.ascx.cs" %>

<%@ Register Src="~/Pages/Internet/Controls/MysteryShopping.ascx" TagPrefix="Controls" TagName="MysteryShopping" %>
<%@ Register Src="~/Pages/Internet/Controls/VehicleHistoryReport.ascx" TagPrefix="Controls" TagName="VehicleHistoryReport" %>

<asp:Panel runat="server" ID="MerchandisingInformation" CssClass="vpa_merchandising_tiles box_360">
    <div class="content">
        <h3><asp:Literal ID="MerchandisingBrandLiteral" runat="server" /></h3>        
        
        <Controls:VehicleHistoryReport ID="VehicleHistoryReport" runat="server" OnReportChanged="VehicleHistoryReport_ReportChanged" /> 
        
        <Controls:MysteryShopping ID="MysteryShopping" runat="server" />
        <asp:Panel ID="InternetAdvertisementPanel" runat="server" CssClass="internet-advertisement-field">
            <asp:HyperLink ID="PhotoManagerHyperlink" runat="server" Text="Photo Manager" CssClass="photo_manager" Target="Photos"></asp:HyperLink>
            
            <h4>Highlights</h4>                        
            
            <asp:ObjectDataSource ID="AdvertisementToolBarDataSource" runat="server"
                    TypeName="FirstLook.Pricing.DomainModel.Internet.ObjectModel.Document.AdvertisementToolBarProvider+DataSource"
                    SelectMethod="Select">
                <SelectParameters>
                    <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
                    <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="vehicleHandle" Type="String" QueryStringField="vh" />
                </SelectParameters>
            </asp:ObjectDataSource>
            
            <asp:ObjectDataSource ID="EquipmentToolBarDataSource" runat="server"
                    TypeName="FirstLook.Pricing.DomainModel.Internet.ObjectModel.Document.AdvertisementEquipmentToolBarProvider+DataSource"
                    SelectMethod="Select">
                <SelectParameters>
                    <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
                    <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="vehicleHandle" Type="String" QueryStringField="vh" />
                </SelectParameters>
            </asp:ObjectDataSource>
            
            <asp:ObjectDataSource ID="NotesToolBarDataSource" runat="server"
                    TypeName="FirstLook.Pricing.DomainModel.Internet.ObjectModel.Document.NotesToolBarProvider+DataSource"
                    SelectMethod="Select">
                <SelectParameters>
                    <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
                    <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="vehicleHandle" Type="String" QueryStringField="vh" />
                </SelectParameters>
            </asp:ObjectDataSource>
            
            <pwc:ToolBar runat="server" ID="AdvertisementDocumentToolbar" Visible="false"
                    AutoGenerateItems="false"
                    CssClass="ui-textarea-topper-toolbar" 
                    DataSourceID="AdvertisementToolBarDataSource"
                    HasRoundedCorners="true"
                    OnCommand="AdvertisementDocumentToolbar_OnCommand"
                    OnDataBound="AdvertisementDocumentToolbar_OnDataBound">
                <Items>
                    <pwc:ToolBarDropDown CssClass="vehicle_history carfax_and_autocheck" Name="VehicleHistory" Text="Vehicle History" AutoGenerateItems="false">
                        <DropDownItems>
                                <pwc:ToolBarButton CssClass="ui-branded-button carfax" Name="Carfax" Text="Carfax" />
                                <pwc:ToolBarButton CssClass="ui-branded-button carfax-oneowner" Name="Carfax$OneOwner" Text="Carfax One-Owner" />
                                <pwc:ToolBarButton CssClass="ui-branded-button carfax-bbg" Name="Carfax$BuyBackGuarantee" Text="Carfax Buy Back Guarantee" />
                                <pwc:ToolBarButton CssClass="ui-branded-button carfax-no-accident" Name="Carfax$NoAccident" Text="Carfax No Accident" />
                                <pwc:ToolBarButton CssClass="ui-branded-button carfax-clean-title" Name="Carfax$CleanTitle" Text="Carfax Clean Title" />                           
                                <pwc:ToolBarButton CssClass="ui-branded-button autocheck" Name="AutoCheck" Text="AutoCheck" />
                                <pwc:ToolBarButton CssClass="ui-branded-button autocheck-one-owner" Name="AutoCheck$OneOwner" Text="AutoCheck One Owner" />
                                <pwc:ToolBarButton CssClass="ui-branded-button autocheck-assured" Name="AutoCheck$Assured" Text="AutoCheck Assured" />
                                <pwc:ToolBarButton CssClass="ui-branded-button autocheck-score" Name="AutoCheck$Score" Text="AutoCheck Score" />
                                <pwc:ToolBarButton CssClass="ui-branded-button autocheck-clean-title" Name="AutoCheck$CleanTitle" Text="AutoCheck Clean Tile" />
                        </DropDownItems>
                    </pwc:ToolBarDropDown>
                    <pwc:ToolBarDropDown CssClass="vehicle_information" Name="VehicleInformation" Text="Vehicle Info" AutoGenerateItems="false">
                        <DropDownItems>
                            <pwc:ToolBarButton Name="Certified" Text="Certified" />
                            <pwc:ToolBarButton Name="YearMakeModel" Text="Year Make Model" />
                        </DropDownItems>
                    </pwc:ToolBarDropDown>
                    <pwc:ToolBarDropDown CssClass="fl_360_pricing" Name="Pricing" Text="360&#0176; Pricing" AutoGenerateItems="false">
                        <DropDownItems>
                            <pwc:ToolBarButton CssClass="ui-branded-button tmv" Name="EdmundsTMV" Text="Edmunds TMV" />
                            <pwc:ToolBarButton CssClass="ui-branded-button nada" Name="NADA" Text="NADA" />
                            <pwc:ToolBarButton CssClass="ui-branded-button kbb" Name="KelleyBlueBook" Text="Kelly Blue Book" />
                        </DropDownItems>
                    </pwc:ToolBarDropDown>
                    <pwc:ToolBarDropDown CssClass="taglines" Name="Tagline" Text="Snippets" AutoGenerateItems="true" />
                </Items>
            </pwc:ToolBar>
            
            <pwc:DocumentEditor runat="server" ID="AdvertisementDocumentEditor" 
                    AllowNewLines="false"
                    DataKeyNames="OwnerHandle,VehicleHandle"
                    DefaultMode="ReadOnly"
                    DeleteText="Restore Default"
                    JavaScriptEditsAtCursor="true"
                    HasDocumentProperties="true"
                    HasScrollBars="false"
                    HasFooterText="true"
                    OnModeChanging="AdvertisementDocumentEditor_ModeChanging"
                    OnUpdating="AdvertisementDocumentEditor_Updating"
                    OnUpdated="AdvertisementDocumentEditor_Updated"
                    ShowConfirmationDialog="true"
                    UpdateText="Save">
                <ToolBars>
                    <pwc:DocumentEditorToolBar ControlID="AdvertisementDocumentToolbar" />
                    <pwc:DocumentEditorToolBar ControlID="EquipmentDocumentToolBar" />
                </ToolBars>
            </pwc:DocumentEditor>
                       
            <span runat="server" id="EquipmentDocumentToolBarLabelSpan" visible="false" class="equipment_label">Options: </span>
            
            <pwc:ToolBar runat="server" ID="EquipmentDocumentToolBar"
                    CssClass="ui-textarea-bl-links"
                    DataSourceID="EquipmentToolBarDataSource"
                    Visible="false">
                <Items>
                    <pwc:ToolBarButton Name="HighValueEquipment" Text="High Value" />
                    <pwc:ToolBarButton Name="StandardEquipment" Text="Standard" />
                </Items>
            </pwc:ToolBar>
            
            <cwc:Dialog ID="AdvertisementTextEditorMaxLengthWarningDialog" OnButtonClick="AdvertisementTextEditorMaxLengthWarningDialog_ButtonClick" runat="server" 
                    AllowDrag="false"
                    AllowResize="false" 
                    AllowCancelPostBack="false"
                    TitleText="Internet Advertisment Too Long" 
                    Width="300"
                    Height="100" 
                    Left="340"
                    Top="40"
                    Visible="false">
                <ItemTemplate>
                    <p><strong class="warning">Warning:</strong> Internet Advertisments have a maximum of <em>2000</em> characters. Please modify your ad and resave.</p>
                </ItemTemplate>
                <Buttons>
                    <cwc:DialogButton ButtonCommand="Cancel" IsCancel="true" />
                </Buttons>    
            </cwc:Dialog>
            
            <cwc:Dialog ID="AdvertisementTextEditorEmtpyWarningDialog" OnButtonClick="AdvertisementTextEditorEmtpyWarningDialog_ButtonClick" runat="server" 
                    AllowDrag="false"
                    AllowResize="false" 
                    AllowCancelPostBack="false"
                    TitleText="Internet Advertisment Is Empty" 
                    Width="400"
                    Height="150" 
                    Left="340"
                    Top="30"
                    Visible="false">
                <ItemTemplate>
                    <p><strong class="warning">Warning:</strong> Your ad Highlights are empty. If you use FirstLook EDT services, an ad with blank Highlights will be submitted for Internet Distribution.</p>
                    <p>Click <em>OK</em> to continue with empty ad Highlights, or <em>Cancel</em> to go back and write your Highlights.</p>
                </ItemTemplate>
                <Buttons>
                    <cwc:DialogButton ButtonCommand="Ok" />
                    <cwc:DialogButton ButtonCommand="Cancel" IsCancel="true" />
                </Buttons>    
            </cwc:Dialog>
            
        </asp:Panel>
        <asp:Panel ID="NotesPanel" runat="server" CssClass="search-notes-field">
      
            <h4><asp:Literal ID="NotesHeaderLiteral" runat="server" Text="Notes" /></h4>
            
            <pwc:ToolBar runat="server" ID="NotesDocumentToolbar" 
                    AutoGenerateItems="false"
                    CssClass="ui-textarea-topper-toolbar hidden"
                    DataSourceID="NotesToolBarDataSource"
                    HasRoundedCorners="true" 
                    OnCommand="NotesDocumentToolbar_Command"
                    Visible="true">
                    <Items>
                        <pwc:ToolBarButton CssClass="copy_ad_to_notes" Name="CopyAdToNotes" Text="Copy Ad to Notes"></pwc:ToolBarButton>
                        <pwc:ToolBarButton Name="JDPower" Text="JD Power"></pwc:ToolBarButton>
                        <pwc:ToolBarButton Name="StoreAverageSellingPrice" Text="Latest Store Performance"></pwc:ToolBarButton>
                    </Items>
            </pwc:ToolBar>
            
            <pwc:DocumentEditor runat="server" ID="NotesDocumentEditor" Visible="true"
                    AllowNewLines="true"
                    DefaultMode="Edit" 
                    DeleteText="Clear" 
                    UpdateText="Save"
                    HasDocumentProperties="false"
                    HasScrollBars="false"
                    HasFooterText="false"
                    ShowConfirmationDialog="false"
                    OnlyShowToolBarOnFocus="true"
                    OnUpdating="NotesDocumentEditor_Updating"
                    JavaScriptEditsAtCursor="true">
                <ToolBars>
                    <pwc:DocumentEditorToolBar ControlID="NotesDocumentToolbar" />
                </ToolBars>
            </pwc:DocumentEditor>
            
            <cwc:Dialog ID="PricingNotesFieldMaxLengthWarningDialog" OnButtonClick="PricingNotesFieldMaxLengthWarningDialog_ButtonClick" runat="server" 
                    AllowDrag="false"
                    AllowResize="false" 
                    AllowCancelPostBack="false"
                    TitleText="Notes Too Long" 
                    Width="300"
                    Height="100" 
                    Left="560"
                    Top="40"
                    Visible="false">
                <ItemTemplate>
                    <p><strong class="warning">Warning:</strong> Notes have a maximum of <em>500</em> characters. Please modify your notes and resave.</p>
                </ItemTemplate>
                <Buttons>
                    <cwc:DialogButton ButtonCommand="Cancel" IsCancel="true" />
                </Buttons>    
            </cwc:Dialog>            
            
        </asp:Panel>
        <span class="corner"></span>
    </div>
</asp:Panel>
