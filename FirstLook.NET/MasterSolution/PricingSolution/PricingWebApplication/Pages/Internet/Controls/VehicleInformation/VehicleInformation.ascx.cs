using System;
using System.Web.UI;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;

namespace FirstLook.Pricing.WebApplication.Pages.Internet.Controls.VehicleInformation
{
    public partial class Control_VehicleInformation_VehicleInformation : UserControl
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            switch (VehicleTypeCommand.Execute(Request.QueryString["vh"]))
            {
                case VehicleType.Appraisal:
                    Appraisal.Visible = true;
                    break;
                case VehicleType.DealerGroupListing:
                    DealerGroupListing.Visible = true;
                    break;
                case VehicleType.InternetListing:
                    InternetListing.Visible = true;
                    break;
                case VehicleType.Inventory:
                    Inventory.Visible = true;
                    break;
                case VehicleType.OnlineAuctionListing:
                    OnlineAuctionListing.Visible = true;
                    break;
            }
        }
    }
}