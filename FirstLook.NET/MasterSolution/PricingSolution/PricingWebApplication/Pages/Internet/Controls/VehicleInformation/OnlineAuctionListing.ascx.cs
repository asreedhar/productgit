using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;

namespace FirstLook.Pricing.WebApplication.Pages.Internet.Controls.VehicleInformation
{
    public partial class Controls_VehicleInformation_OnlineAuctionListing : UserControl
    {
		protected void AuctionDistanceLabel_DataBinding(object sender, EventArgs e)
        {
            Label auctionDistanceLabel = sender as Label;
            if (auctionDistanceLabel != null)
            {
                IDataItemContainer dataItemContainer = auctionDistanceLabel.NamingContainer as IDataItemContainer;

                if (dataItemContainer != null)
                {
                    OnlineAuctionListing listing = (OnlineAuctionListing)dataItemContainer.DataItem;

                    if (listing.AuctionDistance == null)
                    {
                        auctionDistanceLabel.Text = "Unknown";
                    }
                    else
                    {
                        auctionDistanceLabel.Text = string.Format("{0} miles", listing.AuctionDistance);
                    }
                }
            }
        }
    }
}