using System;
using System.Web.UI;

namespace FirstLook.Pricing.WebApplication.Pages.Internet.Controls.VehicleInformation
{
    public partial class Controls_VehicleInformation_StockNumber : UserControl
    {
        private string _text = string.Empty;
        private bool _link = false;

        public bool IsLink
        {
            get { return _link; }
            set { _link = value; }
        }

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (!string.IsNullOrEmpty(Text))
            {
                if (IsLink)
                {
                    StockNumber.Text = Text;
                    StockNumber.NavigateUrl = string.Format("/IMT/EStock.go?stockNumberOrVin={0}&isPopup=true", Text);
                    StockNumber.Attributes.Add("onclick", "OpenPopup(this.href,this.target); return false;");
                }
                else
                {
                    StockNumber.CssClass = "disabled";
                }
            }
        }
    }
}