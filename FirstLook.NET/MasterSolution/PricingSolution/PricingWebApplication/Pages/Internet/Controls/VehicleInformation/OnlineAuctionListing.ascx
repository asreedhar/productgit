<%@ Control Language="C#" AutoEventWireup="true" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.Controls.VehicleInformation.Controls_VehicleInformation_OnlineAuctionListing" Codebehind="OnlineAuctionListing.ascx.cs" %>

<%@ Register Src="~/Pages/Internet/Controls/VehicleInformation/RiskLightImage.ascx"
        TagPrefix="Controls"
        TagName="RiskLightImage" %>

<%@ Register Src="~/Pages/Internet/Controls/VehicleInformation/ColorLabel.ascx"
        TagPrefix="Controls"
        TagName="ColorLabel" %>

<%@ Register Src="~/Pages/Internet/Controls/VehicleInformation/StockNumber.ascx"
        TagPrefix="Controls"
        TagName="StockNumber" %>

<asp:ObjectDataSource ID="VehicleDataSource" runat="server"
        TypeName="FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle.OnlineAuctionListing+DataSource"
        SelectMethod="Select">
    <SelectParameters>
        <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
        <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="vehicleHandle" Type="String" QueryStringField="vh" />
    </SelectParameters>
</asp:ObjectDataSource>

<div runat="server" id="VehicleInformationBar" class="vhclDtl clearfix">
	
<asp:FormView ID="VehicleFormView" runat="server"
        AllowPaging="false"
        DataKeyNames="Id"
        DataSourceID="VehicleDataSource"
        DefaultMode="ReadOnly">
    <HeaderTemplate>
        <div class="vhclDtl_wrapper">
            <h1>
                <Controls:RiskLightImage ID="RiskLightImage1" runat="server" Risk='<%# Eval("Risk") %>' />
                <asp:Label ID="VehicleDescriptionLabel" runat="server" Text='<%# Eval("Description") %>' />
            </h1>
            <dl>
                <dt class="vin">VIN:</dt>
                <dd class="vin">
                    <asp:Label ID="VinLabel" runat="server" Text='<%# Eval("Vin") %>' /></dd>
                <dt class="location">Location:</dt>
                <dd class="location"><asp:Label ID="AuctionLocationLabel" runat="server" Text='<%# Eval("AuctionLocation") %>' /></dd>
                <dt class="distance">Distance:</dt>
                <dd class="distance"><asp:Label ID="AuctionDistanceLabel" runat="server" OnDataBinding="AuctionDistanceLabel_DataBinding"/></dd>
            </dl>
        </div>
    </HeaderTemplate>
        
    <ItemTemplate>
        <dl class="info">
            <dt class="color">Color:</dt>
            <dd class="color"><Controls:ColorLabel ID="ColorLabel" runat="server" Color='<%# Eval("ExteriorColor") %>' /></dd>
            <dt class="mileage">Mileage</dt>
            <dd class="mileage"><asp:Label ID="VehicleMileageLabel" runat="server" Text='<%# Eval("Mileage", "{0:###,##0}") %>' /></dd>
            <dt class="bidPrice">Bid Price:</dt>
            <dd class="bidPrice"><asp:Label ID="AuctionBidPriceLabel" runat="server" Text='<%# Eval("AuctionBidPrice", "{0:$#,##0,($#,##0),N/A}") %>' /></dd>
        </dl>
    </ItemTemplate>
    
</asp:FormView>

</div>