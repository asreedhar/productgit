using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml.Serialization;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Utilities;
using FirstLook.Common.WebControls.UI;
using FirstLook.DomainModel.Lexicon.Decoding;
using FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types.TransferObjects;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;

namespace FirstLook.Pricing.WebApplication.Pages.Internet.Controls.VehicleInformation
{
    public partial class Controls_VehicleInformation_Inventory : UserControl, IScriptControl
    {

        // If you change the Text values of these items, be sure to change the same values in the control (ascx file).
        protected readonly string NotCertifiedText = "Not Certified";
        protected readonly string CertifiedText = "Certified";
        protected readonly string CertifiedLabelText = "OEM Certified";

        private bool shouldAnimateHeight;

        private bool HasTransferPricing
        {
            get
            {
                if (ViewState["HasTransferPricing"] == null)
                    return false;
                return (bool)ViewState["HasTransferPricing"];
            }
            set
            {
                if (!value)
                    ViewState.Remove("HasTransferPricing");
                else
                    ViewState["HasTransferPricing"] = true;
            }
        }

        private int? MaximumTransferPrice
        {
            get
            {
                return (int?)ViewState["MaximumTransferPrice"];
            }
            set
            {
                if (value == null)
                {
                    ViewState.Remove("MaximumTransferPrice");
                }
                else
                {
                    ViewState["MaximumTransferPrice"] = value;
                }
            }
        }

        private bool CanSpecifyRetailOnly
        {
            get
            {
                if (ViewState["CanSpecifyRetailOnly"] == null)
                {
                    return false;
                }

                return true;
            }
            set
            {
                if (!value)
                {
                    ViewState.Remove("CanSpecifyRetailOnly");
                }

                ViewState["CanSpecifyRetailOnly"] = value;
            }
        }

        private IEnumerable<CatalogEntry> _catalogEntryList;

        private IEnumerable<CatalogEntry> CatalogEntryList
        {
            get
            {
                if (_catalogEntryList == null)
                {
                    ObjectDataSource ds = VehicleFormView.FindControl("CatalogDataSource") as ObjectDataSource;

                    if (ds != null)
                    {
                        ds.Select();
                    }
                }

                return _catalogEntryList;
            }
        }

        private Inventory _inventory;

        private Inventory InventoryData
        {
            get
            {
                if (_inventory == null)
                {
                    _inventory = Inventory.GetInventory(Request.QueryString["oh"], Request.QueryString["vh"]);
                }
                return _inventory;
            }
        }

        private IEnumerable<CertifiedProgramInfoTO> _cpoPrograms;
        private IEnumerable<CertifiedProgramInfoTO> CpoPrograms
        {
            get
            {
                if (_cpoPrograms == null)
                {
                    var certifiedProgamCommand = new Merchandising.DomainModel.Marketing.Commands.CertifiedProgamsForDealerMakeCommand(InventoryData.BusinessUnitId, InventoryData.Make);
                    AbstractCommand.DoRun(certifiedProgamCommand);
                    _cpoPrograms = certifiedProgamCommand.Programs;
                }

                return _cpoPrograms;

            }
        }

        protected void CatalogDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            _catalogEntryList = (IEnumerable<CatalogEntry>)e.ReturnValue;
        }

        protected void VehicleFormView_Updating(object sender, FormViewUpdateEventArgs e)
        {
             if (!(VehicleCertification.ValidateCertifiedId( e.NewValues["CertificationNumber"] as string,InventoryData.Manufacturer.Name )))
             {
                 e.Cancel = true;
             }
            
            string mileage = e.NewValues["Mileage"] as string;
            if (mileage != null)
            {
                e.NewValues["Mileage"] = RemoveNumberFormatting(mileage);
            }

            string transferPrice = e.NewValues["TransferPrice"] as string;
            if (transferPrice != null)
            {
                e.NewValues["TransferPrice"] = RemoveNumberFormatting(transferPrice);
            }

            DropDownList styles = VehicleFormView.FindControl("BodyStyleDropDownList") as DropDownList;
            if (styles != null)
            {
                e.NewValues["VehicleCatalogId"] = styles.SelectedValue;
            }

            DropDownList certifieDropDownList = VehicleFormView.FindControl("ddlCertifiedPrograms") as DropDownList;
            var certifiedProgramId = Int32.Parse(certifieDropDownList.SelectedValue.Split('|')[0]);
            if (certifieDropDownList != null)
            {
                e.NewValues["certifiedProgramId"] = certifiedProgramId;
            }

            var isManufacturerProgram = CpoPrograms.Any(x => x.Id == certifiedProgramId && x.ProgramType == "MFR");


            e.NewValues["certified"] = certifiedProgramId == 0 || isManufacturerProgram;

            if (HasTransferPricing)
            {
                int? newTransferPrice = Int32Helper.ToNullableInt32(e.NewValues["TransferPrice"]);

                if (MaximumTransferPrice.HasValue)
                {
                    if (newTransferPrice.HasValue)
                    {
                        if (newTransferPrice.Value > MaximumTransferPrice.Value)
                        {
                            e.Cancel = true;
                        }
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
            }
            else
            {
                e.NewValues["TransferPrice"] = null;
                e.NewValues["TransferPriceForRetailOnly"] = true;
            }

            string certificationNumber = e.NewValues["CertificationNumber"] as string;
            if (certificationNumber != null)
            {
                e.NewValues["CertificationNumber"] = isManufacturerProgram ? RemoveNumberFormatting(certificationNumber) : null;
            }

        }

        protected void VehicleFormView_Updated(object sender, FormViewUpdatedEventArgs e)
        {
            if (e.Exception != null)
            {
                VehicleFormView.FindControl("SaveErrorDialog").Visible = true;

                e.ExceptionHandled = true;
            }
            _inventory = null;
        }

        protected void VehicleFormView_DataBound(object sender, EventArgs e)
        {
            TransferPricingRules rules = TransferPricingRules.GetTransferPricingRules(
                Request.QueryString["oh"], Request.QueryString["vh"]);

            HasTransferPricing = rules.HasTransferPricing;
            MaximumTransferPrice = rules.MaximumTransferPrice;
            CanSpecifyRetailOnly = rules.CanSpecifyRetailOnly;

            if (HasTransferPricing)
            {
                WebControl checkbox = VehicleFormView.FindControl("ForRetailOnlyCheckBox") as WebControl;

                if (checkbox != null)
                {
                    checkbox.Enabled = rules.CanSpecifyRetailOnly;
                }
            }
            else
            {
                Control fieldset = VehicleFormView.FindControl("TranserPriceFieldSet");

                if (fieldset != null)
                {
                    fieldset.Visible = false;
                }
            }

            SetupCertification();
            SetCertifiedLabelText();


        }

        protected void VehicleFormView_ModeChanging(object sender, FormViewModeEventArgs e)
        {
            if (e.NewMode == FormViewMode.Edit)
            {
                shouldAnimateHeight = true;
            }
        }

        protected void VehicleFormViewEditWrapper_PreRender(object sender, EventArgs e)
        {
            string only_show_with_javascript = " only_show_with_javascript";
            WebControl vehicleFormViewEditWrapper = VehicleFormView.FindControl("VehicleFormViewEditWrapper") as WebControl;

            if (shouldAnimateHeight)
            {
                if (vehicleFormViewEditWrapper != null)
                {
                    vehicleFormViewEditWrapper.CssClass += only_show_with_javascript;
                }
            }
            else
            {
                if (vehicleFormViewEditWrapper != null)
                {
                    vehicleFormViewEditWrapper.CssClass = vehicleFormViewEditWrapper.CssClass.Replace(only_show_with_javascript, string.Empty);
                }
            }

        }

        protected void BodyStyleDropDownList_PreRender(object sender, EventArgs e)
        {
            PopulateVehicleCatalogTable(sender as DropDownList);
        }

        #region Helper Methods

        protected static string RemoveNumberFormatting(string text)
        {
            if (string.IsNullOrEmpty(text))
                return null;
            else if (string.Compare("N/A", text, StringComparison.OrdinalIgnoreCase) == 0)
                return null;
            else if (string.Compare("NA", text, StringComparison.OrdinalIgnoreCase) == 0)
                return null;
            else if (string.Compare("--", text, StringComparison.OrdinalIgnoreCase) == 0)
                return null;
            else
                return text.Replace(",", "");
        }

        protected void PopulateVehicleCatalogTable(DropDownList dropDownListDriver)
        {
            const string emptyValue = "--";

            Literal engine = VehicleFormView.FindControl("BodyStyleEngineLiteral") as Literal;
            Literal transmission = VehicleFormView.FindControl("BodyStyleTransmissionLiteral") as Literal;
            Literal driveType = VehicleFormView.FindControl("BodyStyleDriveType") as Literal;
            Literal fuelType = VehicleFormView.FindControl("BodyStyleFuelType") as Literal;
            Literal doors = VehicleFormView.FindControl("BodyStyleDoors") as Literal;

            SetText(engine, emptyValue);
            SetText(transmission, emptyValue);
            SetText(driveType, emptyValue);
            SetText(fuelType, emptyValue);
            SetText(doors, emptyValue);

            if (dropDownListDriver != null)
            {
                ListItem activeListItem = dropDownListDriver.SelectedItem;

                if (activeListItem != null)
                {
                    int id = Int32Helper.ToInt32(activeListItem.Value);

                    foreach (CatalogEntry entry in CatalogEntryList)
                    {
                        if (entry.Id == id)
                        {
                            SetText(engine, entry.Engine != null ? entry.Engine.Name : emptyValue);

                            SetText(transmission, entry.Transmission != null ? entry.Transmission.Name : emptyValue);

                            SetText(driveType, entry.DriveTrain != null ? entry.DriveTrain.Name : emptyValue);

                            SetText(fuelType, entry.FuelType != null ? entry.FuelType.Name : emptyValue);

                            SetText(doors, entry.PassengerDoor != null ? entry.PassengerDoor.Name : emptyValue);
                        }
                    }
                }
            }
        }

        private static void SetText(ITextControl literal, string value)
        {
            if (literal != null)
            {
                literal.Text = value;
            }
        }

        #endregion

        #region Page Events

        private ScriptManager manager;

        protected void Page_PreRender(Object sender, EventArgs e)
        {

            if (!DesignMode)
            {
                manager = ScriptManager.GetCurrent(Page);

                if (manager != null)
                    manager.RegisterScriptControl(this);
                else
                    throw new ApplicationException("You must have a ScriptManager!");
            }

        }

        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);

            if (!DesignMode)
                manager.RegisterScriptDescriptors(this);

        }

        #endregion

        #region IScriptControl Members

        public IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {
            if (VehicleFormView.CurrentMode == FormViewMode.Edit)
            {
                ScriptBehaviorDescriptor editorJS = new ScriptBehaviorDescriptor("FirstLook.Pricing.Website.VehicleInformation", VehicleInformationBar.ClientID);

                editorJS.AddElementProperty("edit_wrapper", VehicleFormView.FindControl("VehicleFormViewEditWrapper").ClientID);
                editorJS.AddElementProperty("orginal_mileage_input", VehicleFormView.FindControl("OriginalMileageHiddenField").ClientID);
                editorJS.AddElementProperty("mileage_textbox", VehicleFormView.FindControl("MileageTextBox").ClientID);
                editorJS.AddElementProperty("invalid_mileage_error", VehicleFormView.FindControl("InvalidMileageError").ClientID);
                editorJS.AddElementProperty("mileage_changed_warning", VehicleFormView.FindControl("MileageChangeWarning").ClientID);
                editorJS.AddElementProperty("save_button", VehicleFormView.FindControl("SaveButton").ClientID);
                editorJS.AddProperty("should_animate_height", shouldAnimateHeight);
                editorJS.AddElementProperty("edit_wrapper_current_height_input", VehicleInformationBarCurrentHeight.ClientID);

                if (HasTransferPricing)
                {
                    editorJS.AddElementProperty("transfer_price_textbox", VehicleFormView.FindControl("TransferPriceTextBox").ClientID);
                    editorJS.AddElementProperty("transfer_price_high_error", VehicleFormView.FindControl("TransferPriceTooHigh").ClientID);
                    editorJS.AddElementProperty("invalid_transfer_price_error", VehicleFormView.FindControl("TransferPriceInvalid").ClientID);
                    editorJS.AddProperty("maximum_transfer_price", MaximumTransferPrice);
                }

                return new ScriptDescriptor[] { editorJS };
            }
            else
            {
                return new ScriptDescriptor[] { };
            }

        }

        public IEnumerable<ScriptReference> GetScriptReferences()
        {
            ScriptReference editorJS = new ScriptReference("~/Public/Scripts/Controls/VehicleInformation.js");

            if (VehicleFormView.CurrentMode == FormViewMode.Edit)
            {
                return new ScriptReference[]
                           {
                               editorJS,
                               new ScriptReference("FirstLook.Common.WebControls.Timer.js", "FirstLook.Common.WebControls"),
                               new ScriptReference("FirstLook.Common.WebControls.Animations.Animation.js", "FirstLook.Common.WebControls"),
                               new ScriptReference("FirstLook.Common.WebControls.Animations.PropertyAnimation.js", "FirstLook.Common.WebControls"),
                               new ScriptReference("FirstLook.Common.WebControls.Animations.InterpolatedAnimation.js", "FirstLook.Common.WebControls"),
                               new ScriptReference("FirstLook.Common.WebControls.Animations.LengthAnimation.js", "FirstLook.Common.WebControls")                
                           };
            }
            else
            {
                return new ScriptReference[] { };
            }
        }

        #endregion

        protected void MaximumTransferPriceLiteral_DataBinding(object sender, EventArgs e)
        {
            Literal maximumTransferPriceLiteral = sender as Literal;

            if (maximumTransferPriceLiteral != null)
            {
                maximumTransferPriceLiteral.Text = maximumTransferPriceLiteral.Text = string.Format("${0:0,0}", MaximumTransferPrice);
            }
        }

        protected void SaveErrorDialog_ButtonClick(object sender, DialogButtonClickEventArgs e)
        {
            Dialog saveErrorDialog = sender as Dialog;

            if (saveErrorDialog != null)
            {
                saveErrorDialog.Visible = false;
            }
        }

        private void SetupCertification()
        {
            var certificateDropdown = VehicleFormView.FindControl("ddlCertifiedPrograms") as DropDownList;
            if (certificateDropdown != null)
            {
                certificateDropdown.DataSource = CpoPrograms;
                certificateDropdown.DataBind();
                var CertifiedIdLabel = VehicleFormView.FindControl("Certificate") as Label;
                CertifiedIdLabel.Text = string.Format("{0} CertifiedID ", InventoryData.Make);

                if (certificateDropdown.SelectedItem.Text == CertifiedText)
                    changeSelection(certificateDropdown);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "certifiedNumber", GetCertificationJavaScript(), true);
        }

        private void changeSelection(DropDownList list)
        {
            int manufacturerCount = CpoPrograms.Count(cp => cp.ProgramType == "MFR");

            if (list != null && manufacturerCount == 1)
            {
                SelectDropDownListItemByProgramId(list, CpoPrograms.FirstOrDefault(cp => cp.ProgramType == "MFR").Id);
            }

        }

        protected void ddlCertifiedPrograms_DataBound(object sender, EventArgs e)
        {

            var certificateDropdown = VehicleFormView.FindControl("ddlCertifiedPrograms") as DropDownList;
            if (certificateDropdown != null)
            {
                var selected = false;

                // We should have a program to select - either one persisted or one auto selected
                if (InventoryData != null && InventoryData.CertifiedProgramId != null)
                {
                    selected = SelectDropDownListItemByProgramId(certificateDropdown,
                        Convert.ToInt32(InventoryData.CertifiedProgramId));
                }

                if (!selected)
                {
                    // We didn't select a program, so we need to select the best item.
                    SelectDropDownListItemByText(certificateDropdown,
                        InventoryData.Certified ? CertifiedText : NotCertifiedText);
                }
            }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="list">List to search for programId</param>
        /// <param name="programId">ProgramId to find</param>
        /// <returns>A value indicating whether or not the programId was found in the itemsList and selected.</returns>
        private bool SelectDropDownListItemByProgramId(DropDownList list, int programId)
        {
            /* 
             * selectedValue = "[programId]|[requiresCertifiedId]"
             * example: "95|True"
             * split on a pipe and take the first value as the programId
             */

            bool found = false;
            foreach (ListItem i in list.Items)
            {
                i.Selected = i.Value.Split('|')[0].Equals(programId.ToString(CultureInfo.InvariantCulture));
                if (i.Selected && !found)
                {
                    found = true;
                }
            }
            return found;
        }

        private void SelectDropDownListItemByText(DropDownList list, string itemText)
        {
            foreach (ListItem i in list.Items)
            {
                i.Selected = i.Text == itemText;
            }
        }

        private void SetCertifiedLabelText()
        {
            var label = VehicleFormView.FindControl("CertifiedLabel") as Label;
            if (label != null)
            {
                if (InventoryData.CertifiedProgramId != null)
                {
                    foreach (var cpoProgram in CpoPrograms)
                    {
                        if (
                            cpoProgram.Key.Split('|')[0].Equals(
                                InventoryData.CertifiedProgramId.ToString()))
                        {
                            label.Text = cpoProgram.Name;
                        }
                    }

                }
                else
                {
                    label.Text = InventoryData.Certified ? CertifiedLabelText : NotCertifiedText;

                    int manufacturerCount = CpoPrograms.Count(cp => cp.ProgramType == "MFR");

                    if (manufacturerCount == 1 && label.Text.Equals(CertifiedLabelText))
                    {
                        label.Text = CpoPrograms.FirstOrDefault(cp => cp.ProgramType == "MFR").Name;
                    }
                }

                
            }
        }

        private string GetCertificationJavaScript()
        {
            DropDownList ddlCertifieDownList = VehicleFormView.FindControl("ddlCertifiedPrograms") as DropDownList;
            HtmlControl displayCertifiedId = VehicleFormView.FindControl("displayCertifiedId") as HtmlControl;
            TextBox txtCertificationNumber = VehicleFormView.FindControl("txtCertifiedId") as TextBox;

            StringBuilder scriptBuilder = new StringBuilder();
            if (ddlCertifieDownList != null && displayCertifiedId != null)
            {
                scriptBuilder.Append("function ddlCertifiedId() {var showIdBox = 'false';");
                scriptBuilder.Append("if ($('#" + ddlCertifieDownList.ClientID + "').val() &&");
                scriptBuilder.Append("($('#" + ddlCertifieDownList.ClientID + "').val().indexOf('|') > -1)) {");
                scriptBuilder.Append("showIdBox = $('#" + ddlCertifieDownList.ClientID + "').val().split('|')[1].toLowerCase();");
                scriptBuilder.Append("} if (showIdBox == 'true') {");
                scriptBuilder.Append("$('#" + displayCertifiedId.ClientID + "').show();");
                scriptBuilder.Append(" } else {$('#" + displayCertifiedId.ClientID + "').hide();}");
                scriptBuilder.Append(" }");
                scriptBuilder.Append("function validate(){");
                scriptBuilder.Append("if($('" + displayCertifiedId.ClientID + "').css('display') != 'none'){");
                scriptBuilder.Append("var certifiedId = $('#" + txtCertificationNumber.ClientID + "').val();");
                scriptBuilder.Append("if (certifiedId.length != 0 && certifiedId.match('^[0-9]{6,8}$') == null) {");
                scriptBuilder.Append("alert('CertifiedId is not a valid format. A Certified Id should be a number 6 to 8 digits long.');");
                scriptBuilder.Append("return false;}");
            
                scriptBuilder.Append("}return true;}");
                scriptBuilder.Append("$(document).ready(function () {ddlCertifiedId();});");
            }
            return scriptBuilder.ToString();
        }
    }
}