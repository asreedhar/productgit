<%@ Control Language="C#" AutoEventWireup="true" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.Controls.VehicleInformation.Controls_VehicleInformation_StockNumber" Codebehind="StockNumber.ascx.cs" %>

<asp:HyperLink ID="StockNumber" runat="server"
        CssClass="stock_number"
        Text="-"
        Target="EStock" />
