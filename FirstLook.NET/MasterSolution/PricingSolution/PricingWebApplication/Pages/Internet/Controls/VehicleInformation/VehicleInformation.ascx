<%@ Control Language="C#" AutoEventWireup="true" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.Controls.VehicleInformation.Control_VehicleInformation_VehicleInformation" Codebehind="VehicleInformation.ascx.cs" %>

<%@ Register Src="Appraisal.ascx"
        TagPrefix="Controls"
        TagName="Appraisal" %>

<%@ Register Src="DealerGroupListing.ascx"
        TagPrefix="Controls"
        TagName="DealerGroupListing" %>

<%@ Register Src="InternetListing.ascx"
        TagPrefix="Controls"
        TagName="InternetListing" %>

<%@ Register Src="Inventory.ascx"
        TagPrefix="Controls"
        TagName="Inventory" %>

<%@ Register Src="OnlineAuctionListing.ascx"
        TagPrefix="Controls"
        TagName="OnlineAuctionListing" %>

<Controls:Appraisal ID="Appraisal" runat="server" Visible="false" />

<Controls:DealerGroupListing ID="DealerGroupListing" runat="server" Visible="false" />

<Controls:InternetListing ID="InternetListing" runat="server" Visible="false" />

<Controls:Inventory ID="Inventory" runat="server" Visible="false" />

<Controls:OnlineAuctionListing ID="OnlineAuctionListing" runat="server" Visible="false" />
