<%@ Control Language="C#" AutoEventWireup="true" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.Controls.VehicleInformation.Controls_VehicleInformation_RiskLightImage" Codebehind="RiskLightImage.ascx.cs" %>

<asp:Image ID="RiskLight" runat="server"
        EnableViewState="false"
        ImageAlign="Middle" />
