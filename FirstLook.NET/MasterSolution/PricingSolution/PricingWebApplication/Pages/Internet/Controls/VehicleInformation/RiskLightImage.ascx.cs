using System;
using System.Web.UI;

namespace FirstLook.Pricing.WebApplication.Pages.Internet.Controls.VehicleInformation
{
    public partial class Controls_VehicleInformation_RiskLightImage : UserControl
    {
        private int _risk;

        public int Risk
        {
            get { return _risk; }
            set { _risk = value; }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            switch (Risk)
            {
                case 0:
                    RiskLight.ImageUrl = "~/Public/Images/light_unknown.png";
                    RiskLight.ToolTip = "Unknown Risk";
                    break;
                case 1:
                    RiskLight.ImageUrl = "~/Public/Images/light_red.png";
                    RiskLight.ToolTip = "High Risk";
                    break;
                case 2:
                    RiskLight.ImageUrl = "~/Public/Images/light_yellow.png";
                    RiskLight.ToolTip = "Medium Risk";
                    break;
                case 3:
                    RiskLight.ImageUrl = "~/Public/Images/light_green.png";
                    RiskLight.ToolTip = "Low Risk";
                    break;
            }
        }

        protected override void LoadViewState(object savedState)
        {
            Pair pair = (Pair) savedState;

            base.LoadViewState(pair.First);

            _risk = (int) pair.Second;
        }

        protected override object SaveViewState()
        {
            return new Pair(base.SaveViewState(), _risk);
        }
    }
}