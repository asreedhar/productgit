using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Utilities;
using FirstLook.DomainModel.Lexicon.Decoding;

namespace FirstLook.Pricing.WebApplication.Pages.Internet.Controls.VehicleInformation
{
    public partial class Controls_VehicleInformation_Appraisal : UserControl, IScriptControl
    {
        private bool shouldAnimateHeight = false;

        private IEnumerable<CatalogEntry> _catalogEntryList;

        private IEnumerable<CatalogEntry> CatalogEntryList
        {
            get
            {
                if (_catalogEntryList == null)
                {
                    ObjectDataSource ds = VehicleFormView.FindControl("CatalogDataSource") as ObjectDataSource;

                    if (ds != null)
                    {
                        ds.Select();
                    }
                }

                return _catalogEntryList;
            }
        }

        protected void CatalogDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            _catalogEntryList = (IEnumerable<CatalogEntry>)e.ReturnValue;
        }

        protected void VehicleFormView_Updating(object sender, FormViewUpdateEventArgs e)
        {
            string mileage = e.NewValues["Mileage"] as string;
            if (mileage != null)
            {
                e.NewValues["Mileage"] = RemoveMileageFormatting(mileage);
            }
            DropDownList styles = VehicleFormView.FindControl("BodyStyleDropDownList") as DropDownList;
            if (styles != null)
            {
                e.NewValues["VehicleCatalogId"] = styles.SelectedValue;
            }
        }

        protected void VehicleFormView_ModeChanging(object sender, FormViewModeEventArgs e)
        {
            if (e.NewMode == FormViewMode.Edit)
            {
                shouldAnimateHeight = true;
            }
        }

        protected void VehicleFormViewEditWrapper_PreRender(object sender, EventArgs e)
        {
            string only_show_with_javascript = " only_show_with_javascript";
            WebControl vehicleFormViewEditWrapper = VehicleFormView.FindControl("VehicleFormViewEditWrapper") as WebControl;

            if (shouldAnimateHeight)
            {
                if (vehicleFormViewEditWrapper != null)
                {
                    vehicleFormViewEditWrapper.CssClass += only_show_with_javascript;
                }
            }
            else
            {
                if (vehicleFormViewEditWrapper != null)
                {
                    vehicleFormViewEditWrapper.CssClass = vehicleFormViewEditWrapper.CssClass.Replace(only_show_with_javascript, string.Empty);
                }
            }
        }

        protected void BodyStyleDropDownList_PreRender(object sender, EventArgs e)
        {
            PopulateVehicleCatalogTable(sender as DropDownList);
        }

        #region Helper Methods

        protected static string RemoveMileageFormatting(string text)
        {
            if (string.IsNullOrEmpty(text))
                return null;
            else if (string.Compare("N/A", text, StringComparison.OrdinalIgnoreCase) == 0)
                return null;
            else if (string.Compare("NA", text, StringComparison.OrdinalIgnoreCase) == 0)
                return null;
            else if (string.Compare("--", text, StringComparison.OrdinalIgnoreCase) == 0)
                return null;
            else
                return text.Replace(",", "");
        }

        protected void PopulateVehicleCatalogTable(DropDownList dropDownListDriver)
        {
            const string emptyValue = "--";

            Literal engine = VehicleFormView.FindControl("BodyStyleEngineLiteral") as Literal;
            Literal transmission = VehicleFormView.FindControl("BodyStyleTransmissionLiteral") as Literal;
            Literal driveType = VehicleFormView.FindControl("BodyStyleDriveType") as Literal;
            Literal fuelType = VehicleFormView.FindControl("BodyStyleFuelType") as Literal;
            Literal doors = VehicleFormView.FindControl("BodyStyleDoors") as Literal;

            SetText(engine, emptyValue);
            SetText(transmission, emptyValue);
            SetText(driveType, emptyValue);
            SetText(fuelType, emptyValue);
            SetText(doors, emptyValue);

            if (dropDownListDriver != null)
            {
                ListItem activeListItem = dropDownListDriver.SelectedItem;

                if (activeListItem != null)
                {
                    int id = Int32Helper.ToInt32(activeListItem.Value);

                    foreach (CatalogEntry entry in CatalogEntryList)
                    {
                        if (entry.Id == id)
                        {
                            SetText(engine, entry.Engine != null ? entry.Engine.Name : emptyValue);

                            SetText(transmission, entry.Transmission != null ? entry.Transmission.Name : emptyValue);

                            SetText(driveType, entry.DriveTrain != null ? entry.DriveTrain.Name : emptyValue);

                            SetText(fuelType, entry.FuelType != null ? entry.FuelType.Name : emptyValue);

                            SetText(doors, entry.PassengerDoor != null ? entry.PassengerDoor.Name : emptyValue);
                        }
                    }
                }
            }
        }

        private static void SetText(ITextControl literal, string value)
        {
            if (literal != null)
            {
                literal.Text = value;
            }
        }

        #endregion

        #region Page Events

        private ScriptManager manager;

        protected void Page_PreRender(Object sender, EventArgs e)
        {
            if (!DesignMode)
            {
                manager = ScriptManager.GetCurrent(Page);

                if (manager != null)
                    manager.RegisterScriptControl(this);
                else
                    throw new ApplicationException("You must have a ScriptManager!");
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);

            if (!DesignMode)
                manager.RegisterScriptDescriptors(this);
        }

        #endregion

        #region IScriptControl Members

        public IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {
            if (VehicleFormView.CurrentMode == FormViewMode.Edit)
            {
                ScriptBehaviorDescriptor editorJS = new ScriptBehaviorDescriptor("FirstLook.Pricing.Website.VehicleInformation", VehicleInformationBar.ClientID);
                editorJS.AddElementProperty("edit_wrapper", VehicleFormView.FindControl("VehicleFormViewEditWrapper").ClientID);
                editorJS.AddElementProperty("mileage_textbox", VehicleFormView.FindControl("MileageTextBox").ClientID);
                editorJS.AddElementProperty("save_button", VehicleFormView.FindControl("SaveButton").ClientID);
                editorJS.AddProperty("should_animate_height", shouldAnimateHeight);
                editorJS.AddElementProperty("edit_wrapper_current_height_input", VehicleInformationBarCurrentHeight.ClientID);

                return new ScriptDescriptor[] { editorJS };
            }
            else
            {
                return new ScriptDescriptor[] { };
            }

        }

        public IEnumerable<ScriptReference> GetScriptReferences()
        {
            ScriptReference editorJS = new ScriptReference("~/Public/Scripts/Controls/VehicleInformation.js");

            if (VehicleFormView.CurrentMode == FormViewMode.Edit)
            {
                return new ScriptReference[]
                           {
                               editorJS,
                               new ScriptReference("FirstLook.Common.WebControls.Timer.js", "FirstLook.Common.WebControls"),
                               new ScriptReference("FirstLook.Common.WebControls.Animations.Animation.js", "FirstLook.Common.WebControls"),
                               new ScriptReference("FirstLook.Common.WebControls.Animations.PropertyAnimation.js", "FirstLook.Common.WebControls"),
                               new ScriptReference("FirstLook.Common.WebControls.Animations.InterpolatedAnimation.js", "FirstLook.Common.WebControls"),
                               new ScriptReference("FirstLook.Common.WebControls.Animations.LengthAnimation.js", "FirstLook.Common.WebControls")                
                           };
            }
            else
            {
                return new ScriptReference[] { };
            }
        }

        #endregion
    }
}