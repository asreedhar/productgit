using System;
using System.Web.UI;

namespace FirstLook.Pricing.WebApplication.Pages.Internet.Controls.VehicleInformation
{
    public partial class Controls_VehicleInformation_ColorLabel : UserControl
    {
        private string _color = string.Empty;

        public string Color
        {
            get { return _color; }
            set { _color = value; }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (Color.Length >= 20)
            {
                VehicleColorLabel.ToolTip = Color;

                if (Color.Contains(","))
                {
                    VehicleColorLabel.Text = Color.Substring(0, Color.IndexOf(","));
                }
                else if (Color.Contains(" "))
                {
                    VehicleColorLabel.Text = Color.Substring(0, Color.IndexOf(" "));
                }
                else
                {
                    VehicleColorLabel.Text = Color.Substring(0, 20);
                }
            }
            else
            {
                VehicleColorLabel.Text = Color;
            }
        }
    }
}