<%@ Control Language="C#" AutoEventWireup="true" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.Controls.VehicleInformation.Controls_VehicleInformation_Appraisal" Codebehind="Appraisal.ascx.cs" %>

<%@ Register Src="~/Pages/Internet/Controls/VehicleInformation/RiskLightImage.ascx"
        TagPrefix="Controls"
        TagName="RiskLightImage" %>

<%@ Register Src="~/Pages/Internet/Controls/VehicleInformation/ColorLabel.ascx"
        TagPrefix="Controls"
        TagName="ColorLabel" %>
        
<asp:ObjectDataSource ID="VehicleDataSource" runat="server"
        TypeName="FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle.Appraisal+DataSource"
        SelectMethod="Select"
        UpdateMethod="Update">
    <SelectParameters>
        <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
        <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="vehicleHandle" Type="String" QueryStringField="vh" />
    </SelectParameters>
    <UpdateParameters>
        <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
        <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="vehicleHandle" Type="String" QueryStringField="vh" />
    </UpdateParameters>
</asp:ObjectDataSource>

<div runat="server" id="VehicleInformationBar" class="vhclDtl clearfix">
	
<asp:HiddenField runat="server" ID="VehicleInformationBarCurrentHeight" />

<asp:FormView ID="VehicleFormView" runat="server"
        AllowPaging="false"
        DataKeyNames="Id"
        DataSourceID="VehicleDataSource"
        DefaultMode="ReadOnly"
        OnModeChanging="VehicleFormView_ModeChanging"
        OnItemUpdating="VehicleFormView_Updating">
        
    <HeaderTemplate>
        <div class="vhclDtl_wrapper">
	        <h1>
	            <Controls:RiskLightImage ID="RiskLight" runat="server" Risk='<%# Eval("Risk") %>' />
		        <asp:Label ID="VehicleDescriptionLabel" runat="server" Text='<%# Eval("Description") %>'></asp:Label>
	        </h1>
	        <dl>
		        <dt class="vin">VIN:</dt>
		        <dd class="vin"><asp:Label ID="VinLabel" runat="server" Text='<%# Eval("Vin") %>' /></dd>
	        </dl>
        </div>
        <span class="tradein-or-purchase">Potential Trade-in</span>
    </HeaderTemplate>
    
    <ItemTemplate>
        <asp:Button runat="server" ID="EditVehicleInfoButton" CssClass="edit-vehicle-info" title="Edit Vehicle Info" CommandName="Edit" />
        <dl class="info">
	        <dt class="color">Color:</dt>
	        <dd class="color"><Controls:ColorLabel ID="VehicleColorLabel" runat="server" Color='<%# Eval("ExteriorColor") %>' /></dd>
	        <dt class="mileage">Mileage:</dt>
	        <dd class="mileage"><asp:Label ID="VehicleMileageLabel" runat="server" Text='<%# Eval("Mileage", "{0:###,##0}") %>' /></dd>
        </dl>	        
    </ItemTemplate>
    
    <EditItemTemplate>
        <asp:Button runat="server" ID="EditVehicleInfoButton" CssClass="edit-vehicle-info" title="Edit Vehicle Info" CommandName="Cancel" />
    
        <asp:ObjectDataSource ID="ColorDataSource" runat="server"
            TypeName="FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle.ColorCollection+DataSource"
            SelectMethod="Select">
        </asp:ObjectDataSource>

        <asp:ObjectDataSource ID="InteriorDataSource" runat="server"
                TypeName="FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle.InteriorTypeCollection+DataSource"
                SelectMethod="Select">
        </asp:ObjectDataSource>
    
        <asp:ObjectDataSource ID="CatalogDataSource" runat="server"
                TypeName="FirstLook.DomainModel.Lexicon.Decoding.Catalog+DataSource"
                SelectMethod="GetCatalogEntries"
                OnSelected="CatalogDataSource_Selected">
             <SelectParameters>
                <asp:ControlParameter ControlID="ModelYearHiddenField" Name="modelYear" />
                <asp:ControlParameter ControlID="MakeHiddenField" Name="makeId" />
                <asp:ControlParameter ControlID="LineHiddenField" Name="lineId" />
                <asp:ControlParameter ControlID="ModelHiddenField" Name="modelId" />
                <asp:ControlParameter ControlID="CatalogHiddenField" Name="vehicleCatalogId" />
             </SelectParameters>
        </asp:ObjectDataSource>
        
        <asp:HiddenField runat="server" ID="ModelYearHiddenField" Value='<%# Eval("DecodingClassification.ModelYear") %>' />
        <asp:HiddenField runat="server" ID="MakeHiddenField" Value='<%# Eval("DecodingClassification.MakeId") %>' />
        <asp:HiddenField runat="server" ID="LineHiddenField" Value='<%# Eval("DecodingClassification.LineId") %>' />
        <asp:HiddenField runat="server" ID="ModelHiddenField" Value='<%# Eval("DecodingClassification.ModelId") %>' />
        <asp:HiddenField runat="server" ID="CatalogHiddenField" Value='<%# Eval("DecodingClassification.Id") %>' />
    
        <asp:Panel runat="server" id="VehicleFormViewEditWrapper" CssClass="edit-info clearfix" OnPreRender="VehicleFormViewEditWrapper_PreRender">
            <table border="0" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
    		            <th class="col_body_style">Body Style</th>
    		            <th class="col_engine">Engine</th>
    		            <th class="col_transmission">Transmission</th>
    		            <th class="col_drive_type">Drive Type</th>
    		            <th class="col_fuel_type">Fuel Type</th>
    		            <th class="col_doors">Doors</th>
		            </tr>
                </thead>
    	        <tbody>
    	            <tr>
    		            <td class="col_body_style">
    		                <asp:DropDownList runat="server" ID="BodyStyleDropDownList" 
    		                        DataSourceID="CatalogDataSource" 
    		                        DataTextField="Name" 
    		                        DataValueField="ID" 
    		                        AutoPostBack="true"
    		                        SelectedValue='<%# Eval("DecodingClassification.Id") %>' 
    		                        OnPreRender="BodyStyleDropDownList_PreRender">
    		                    <asp:ListItem Text="Please select an option below..." Value="-1" />
    		                </asp:DropDownList>
    		            </td>
    		            <td class="col_engine">
    		                <asp:Literal runat="server" ID="BodyStyleEngineLiteral"></asp:Literal>
    		            </td>
    		            <td class="col_transmission">
    			            <asp:Literal runat="server" ID="BodyStyleTransmissionLiteral"></asp:Literal>
    		            </td>
    		            <td class="col_drive_type">
    			            <asp:Literal runat="server" ID="BodyStyleDriveType"></asp:Literal>
    		            </td>
    		            <td class="col_fuel_type">
    			            <asp:Literal runat="server" ID="BodyStyleFuelType"></asp:Literal>
    		            </td>
    		            <td class="col_doors">
    			            <asp:Literal runat="server" ID="BodyStyleDoors"></asp:Literal>
    		            </td>
		            </tr>
		        </tbody>
            </table>
            <div class="hr"><hr /></div>
            <fieldset class="color_and_mileage">
    	        <asp:Label runat="server" ID="ExteriorColorLabel" Text="Color" AssociatedControlID="ExteriorColorDropDownList" />
    	        <asp:DropDownList runat="server" ID="ExteriorColorDropDownList" DataSourceID="ColorDataSource" SelectedValue='<%# Bind("ExteriorColor") %>' />
            	<br />
            	<div>
    	            <asp:Label runat="server" ID="MileageLabel" Text="Mileage" AssociatedControlID="MileageTextBox" />
    	            <asp:HiddenField runat="server" ID="OriginalMileageHiddenField" Value='<%# Eval("Mileage", "{0:###,##0;N/A}") %>' />
                    <asp:TextBox runat="server" ID="MileageTextBox" Text='<%# Bind("Mileage", "{0:###,##0;N/A}") %>' />
                    <asp:Image ID="Image1" runat="server" CssClass="validation_warning attention_icon" SkinID="MediumAttentionIcon" />
                    <p runat="server" id="MileageChangeWarning" class="validation_warning" style="display: none;">Book values may be inaccurate due to mileage change.</p>
                    <p runat="server" id="InvalidMileageError" class="validation_warning" style="display: none;">Mileage Must be a number.</p>
                </div>
            </fieldset>
            <fieldset class="interior">
                <asp:Label runat="server" ID="InteriorColorLabel" Text="Interior Color" AssociatedControlID="InterorColorDropDownList" />
                <asp:DropDownList runat="server" ID="InterorColorDropDownList" DataSourceID="ColorDataSource" SelectedValue='<%# Bind("InteriorColor") %>' />
                <br />
                <asp:Label runat="server" ID="InteriorTypeLabel" Text="Interior Type" AssociatedControlID="InteriorTypeDropDownList" />
                <asp:DropDownList runat="server" ID="InteriorTypeDropDownList" DataSourceID="InteriorDataSource" SelectedValue='<%# Bind("InteriorType") %>' />
            </fieldset>
            <fieldset class="controls">
                <span class="button cancel-button">
                    <asp:Button runat="server" ID="CancelButton" Text="Cancel" CommandName="Cancel" />
                </span>
                
                <span class="button save-button">
                    <asp:Button runat="server" ID="SaveButton" Text="Save" CommandName="Update" />
                </span>
            </fieldset>
        </asp:Panel>
    </EditItemTemplate>
    
</asp:FormView>

</div>