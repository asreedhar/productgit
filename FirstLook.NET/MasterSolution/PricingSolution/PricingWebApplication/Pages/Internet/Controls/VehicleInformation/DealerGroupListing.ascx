<%@ Control Language="C#" AutoEventWireup="true" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.Controls.VehicleInformation.Controls_VehicleInformation_DealerGroupListing" Codebehind="DealerGroupListing.ascx.cs" %>

<%@ Register Src="~/Pages/Internet/Controls/VehicleInformation/RiskLightImage.ascx"
        TagPrefix="Controls"
        TagName="RiskLightImage" %>

<%@ Register Src="~/Pages/Internet/Controls/VehicleInformation/ColorLabel.ascx"
        TagPrefix="Controls"
        TagName="ColorLabel" %>

<%@ Register Src="~/Pages/Internet/Controls/VehicleInformation/StockNumber.ascx"
        TagPrefix="Controls"
        TagName="StockNumber" %>

<asp:ObjectDataSource ID="VehicleDataSource" runat="server"
        TypeName="FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle.DealerGroupListing+DataSource"
        SelectMethod="Select">
    <SelectParameters>
        <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
        <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="vehicleHandle" Type="String" QueryStringField="vh" />
    </SelectParameters>
</asp:ObjectDataSource>

<div runat="server" id="VehicleInformationBar" class="vhclDtl clearfix">
	
<asp:HiddenField runat="server" ID="VehicleInformationBarCurrentHeight" />

<asp:FormView ID="VehicleFormView" runat="server"
        AllowPaging="false"
        DataKeyNames="Id"
        DataSourceID="VehicleDataSource"
        DefaultMode="ReadOnly">
        
    <HeaderTemplate>
        <div class="vhclDtl_wrapper">
            <h1>
                <Controls:RiskLightImage ID="RiskLightImage1" runat="server" Risk='<%# Eval("Risk") %>' />
                <asp:Label ID="VehicleDescriptionLabel" runat="server" Text='<%# Eval("Description") %>'></asp:Label>
            </h1>
            <dl class="vin">
                <dt>VIN:</dt>
                <dd>
                    <asp:Label ID="VinLabel" runat="server" Text='<%# Eval("Vin") %>' /></dd>
            </dl>
        </div>
        <asp:Label ID="CertifiedLabel" runat="server" Visible='<%# Eval("Certified") %>' Text="Certified" CssClass="certified" />
        <asp:Label ID="TradeOrPurchaseLabel" runat="server" Text='<%# Eval("DealType") %>' CssClass='tradein-or-purchase' />
    </HeaderTemplate>
    
    <ItemTemplate>
        <dl class="info">
            <dt class='days'>Days:</dt>
            <dd class='days'><asp:Label ID="AgeLabel" runat="server" Text='<%# Eval("Age") %>' /></dd>
            <dt class='color'>Color:</dt>
            <dd class='color'><Controls:ColorLabel ID="ColorLabel" runat="server" Color='<%# Eval("ExteriorColor") %>' /></dd>
            <dt class='mileage'>Mileage:</dt>
            <dd class='mileage'><asp:Label ID="VehicleMileageLabel" runat="server" Text='<%# Eval("Mileage", "{0:###,##0;N/A}") %>' /></dd>
        </dl>
    </ItemTemplate>
    
    <FooterTemplate>
        <Controls:StockNumber ID="StockNumber" runat="server"
                    Text='<%# Eval("StockNumber") %>'
                    IsLink="true" />
    </FooterTemplate>
</asp:FormView>

</div>
