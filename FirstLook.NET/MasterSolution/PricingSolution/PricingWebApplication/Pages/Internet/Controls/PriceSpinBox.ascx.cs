using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Web.UI;
using FirstLook.Pricing.WebControls.UserControls;

namespace FirstLook.Pricing.WebApplication.Pages.Internet.Controls
{
    public partial class Controls_PriceSpinBox : PriceControl
    {
        private bool IsDirty = false;

        private PriceList priceList;

        #region Properties

        public int? Rank
        {
            get
            {
                return (int?)ViewState["Rank"];
            }
            protected set
            {
                if (value != Rank)
                {
                    ViewState["Rank"] = value;
                    OnPropertyChanged("Rank");
                }
            }
        }

        public int MinimumRank
        {
            get
            {
                return 1;
            }
        }

        public int MaximumRank
        {
            get
            {
                return Values.Length + 1;
            }
        }

        public int[][] Values
        {
            get
            {
                object value = ViewState["Values"];
                if (value == null)
                    return new int[0][];
                return (int[][])value;
            }
            protected set
            {
                if (CompareIntArrays(Values, value) == false)
                {
                    ViewState["Values"] = value;
                    OnPropertyChanged("Values");
                }
            }
        }

        public override int? Price
        {
            get
            {
                return (int?)ViewState["Price"];
            }
            set
            {
                if (Nullable.Compare(value, Price) != 0)
                {
                    int? prev = Price;
                    ViewState["Price"] = value;
                    OnPropertyChanged("Price");
                    RaisePriceChangedEvent(prev, value);
                }
            }
        }

        public int Mileage
        {
            get
            {
                object value = ViewState["Mileage"];
                if (value == null)
                    return 0;
                return (int)value;
            }
            set
            {
                if (value != Mileage)
                {
                    ViewState["Mileage"] = value;
                    OnPropertyChanged("Mileage");
                }
            }
        }

        public bool EnableRankChangeButtons
        {
            get
            {
                return Convert.ToBoolean(ViewState["EnableRankChangeButtons"]);
            }
            set
            {
                ViewState["EnableRankChangeButtons"] = value;
            }
        }

        #endregion

        #region Page Life-Cycle and Events

        private static readonly object EventPriceRankChanged = new object();

        [Category("Behavior")]
        public event PriceRankChangedEventHandler PriceRankChanged
        {
            add { Events.AddHandler(EventPriceRankChanged, value); }
            remove { Events.RemoveHandler(EventPriceRankChanged, value); }
        }

        protected void RaisePriceRankChangedEvent()
        {
            PriceRankChangedEventHandler handler = Events[EventPriceRankChanged] as PriceRankChangedEventHandler;
            if (handler != null)
                handler(this, new PriceRankChangedEventArgs(MinimumRank, MaximumRank, Rank.GetValueOrDefault()));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            PropertyChanged += Page_PropertyChanged;

            priceList = PriceList.GetPriceList(Values);
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (EnableRankChangeButtons && HasSufficientData())
            {
                IncreaseVehicleRankButton.Visible = true;
                DecreaseVehicleRankButton.Visible = true;

                IncreaseVehicleRankButton.Enabled = Rank <= MaximumRank;
                DecreaseVehicleRankButton.Enabled = Rank >= MinimumRank;
            }
            else
            {
                IncreaseVehicleRankButton.Visible = false;
                DecreaseVehicleRankButton.Visible = false;
            }

            if (IsDirty)
            {
                DataBindChildren();
            }
        }

        private bool HasSufficientData()
        {
            return Values.Length >= 5; // If you update the 5 please also update the HasSufficientData funciton in the JS, PM
        }

        protected void Page_DataBound(object sender, EventArgs e)
        {
            RaisePropertyChangedEvents = false;

            DataView view = DataItem as DataView;

            if (view != null)
            {
                int[][] values = new int[view.Count][];

                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = new int[2];
                    values[i][0] = Convert.ToInt32(view[i]["ListPrice"]);
                    values[i][1] = Convert.ToInt32(view[i]["Mileage"]);
                }

                Values = values;

                priceList = PriceList.GetPriceList(Values);
            }

            CalculateRank();

            RaisePropertyChangedEvents = true;

            IsDirty = true;
        }

        protected void Page_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            string propertyName = e.PropertyName;

            if (string.Compare(propertyName, "Rank", true) == 0)
            {
                RaisePriceRankChangedEvent();
            }
            else if (string.Compare(propertyName, "Price", true) == 0
                     || string.Compare(propertyName, "Mileage", true) == 0
                     || string.Compare(propertyName, "Values", true) == 0)
            {
                CalculateRank();
            }

            IsDirty = true;
        }

        private void CalculateRank()
        {
            if (HasSufficientData() && Price.HasValue)
            {
                Rank = priceList.Rank(Price.Value, Mileage);
            }
            else
            {
                Rank = null;
            }
        }

        protected void IncreaseVehicleRankButton_Click(object sender, EventArgs e)
        {
            Price = priceList.NextPrice(Price.Value, Mileage);
        }

        protected void DecreaseVehicleRankButton_Click(object sender, EventArgs e)
        {
            Price = priceList.LastPrice(Price.Value, Mileage);
        }

        protected override void PerformDataBinding(IEnumerable data)
        {
            if (data != null)
            {
                DataItem = data;
                Page_DataBound(this, EventArgs.Empty);
                DataBindChildren();
            }
        }

        protected string HideInsufficientData(string value)
        {
            return (HasSufficientData() ? value : "--");
        }

        #endregion

        #region Helper Methods

        private static bool CompareIntArrays(int[] intA, int[] intB)
        {
            if ((intA != null) || (intB != null))
            {
                if ((intA == null) || (intB == null))
                {
                    return false;
                }
                if (intA.Length != intB.Length)
                {
                    return false;
                }
                for (int i = 0; i < intA.Length; i++)
                {
                    if (intA[i] != intB[i])
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private static bool CompareIntArrays(int[][] intA, int[][] intB)
        {
            if ((intA != null) || (intB != null))
            {
                if ((intA == null) || (intB == null))
                {
                    return false;
                }
                if (intA.Length != intB.Length)
                {
                    return false;
                }
                for (int i = 0; i < intA.Length; i++)
                {
                    if (CompareIntArrays(intA[i], intB[i]) == false)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private class PriceList
        {
            private const int Offset = 10;

            private readonly List<PriceListItem> items;

            public PriceList(int size)
            {
                items = new List<PriceListItem>(size);
            }

            public static PriceList GetPriceList(int[][] values)
            {
                PriceList list = new PriceList(values.Length);
                foreach (int[] pair in values)
                    list.Add(pair[0], pair[1]);
                return list;
            }

            public void Add(int price, int mileage)
            {
                Add(new PriceListItem(price, mileage));
            }

            private void Add(PriceListItem item)
            {
                int idx = BinarySearch(item);
                if (idx < 0)
                    items.Insert(~idx, item);
                else
                    items.Insert(idx, item);
            }

            public int Rank(int price, int mileage)
            {
                int i = BinarySearch(new PriceListItem(price, mileage));
                if (i < 0)
                    i = ~i;
                return i + 1;
            }

            public int NextPrice(int price, int mileage)
            {
                int x, y;
                int i = BinarySearch(new PriceListItem(price, mileage));
                if (i < 0)
                    x = ~i;
                else
                    x = NextPrice(i);
                y = NextPrice(x);
                if (x < y)
                {
                    int newPrice = Price(x, y);
                    if (newPrice == items[x].Price)
                    {
                        newPrice = newPrice + 1;
                    }
                    return newPrice;
                }
                else
                {
                    return items[items.Count - 1].Price + Offset;
                }
            }

            public int LastPrice(int price, int mileage)
            {
                int x, y;
                int i = BinarySearch(new PriceListItem(price, mileage));
                if (i < 0)
                {
                    y = ~i;
                    if (y >= items.Count)
                        y = items.Count - 1;
                    if (items[y].Price > price)
                        y = LastPrice(y);
                }
                else
                {
                    y = LastPrice(i);
                }
                x = LastPrice(y);
                if (x < y)
                {
                    int newPrice = Price(x, y);
                    if (newPrice == items[y].Price)
                    {
                        newPrice = newPrice - 1;
                    }
                    return newPrice;
                }
                else
                {
                    return items[0].Price - Offset;
                }
            }

            private int BinarySearch(PriceListItem item)
            {
                return items.BinarySearch(item);
            }

            private int NextPrice(int index)
            {
                for (int i = 0, j = items.Count; i < j; i++)
                    if (items[i].Price > items[index].Price)
                        return i;
                return Int32.MinValue;
            }

            private int LastPrice(int index)
            {
                for (int i = index - 1; i >= 0; i--)
                    if (items[i].Price < items[index].Price)
                        return i;
                return Int32.MaxValue;
            }

            private int Price(int x, int y)
            {
                int delta = items[y].Price - items[x].Price;
                if (delta > Offset)
                    return items[y].Price - Offset;
                else
                    return items[x].Price + ((int)Math.Round(delta / 2.0d));
            }

            class PriceListItem : IEquatable<PriceListItem>, IComparable<PriceListItem>
            {
                private readonly int price;
                private readonly int mileage;

                public PriceListItem(int price, int mileage)
                {
                    this.price = price;
                    this.mileage = mileage;
                }

                public int Price
                {
                    get { return price; }
                }

                public int Mileage
                {
                    get { return mileage; }
                }

                public bool Equals(PriceListItem priceListItem)
                {
                    if (priceListItem == null) return false;
                    return price == priceListItem.price && mileage == priceListItem.mileage;
                }

                public override bool Equals(object obj)
                {
                    if (ReferenceEquals(this, obj)) return true;
                    return Equals(obj as PriceListItem);
                }

                public override int GetHashCode()
                {
                    return price + 29 * mileage;
                }

                public int CompareTo(PriceListItem other)
                {
                    if (other == null)
                        return 1;
                    int c = Price.CompareTo(other.Price);
                    if (c == 0)
                        c = Mileage.CompareTo(other.Mileage);
                    return c;
                }
            }
        }

        #endregion

        #region IScriptControl Members

        public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {
            ScriptBehaviorDescriptor descriptor = new ScriptBehaviorDescriptor("FirstLook.Pricing.Website.PriceSpinBox", VehicleRankTextBox.ClientID); // arbitary visible element
            //descriptor.AddElementProperty("priceLabel", PriceLabel.ClientID);
            descriptor.AddElementProperty("priceRankLabel", VehicleRankTextBox.ClientID);
            descriptor.AddElementProperty("increaseRankButton", IncreaseVehicleRankButton.ClientID);
            descriptor.AddElementProperty("decreaseRankButton", DecreaseVehicleRankButton.ClientID);
            descriptor.AddProperty("rank", Rank);
            descriptor.AddProperty("price", Price);
            descriptor.AddProperty("mileage", Mileage);
            descriptor.AddProperty("values", Values);
            descriptor.AddProperty("enabled", EnableRankChangeButtons && HasSufficientData());
            return new ScriptDescriptor[] { descriptor };
        }

        public override IEnumerable<ScriptReference> GetScriptReferences()
        {
            return new ScriptReference[]
                       {
                           new ScriptReference("~/Public/Scripts/Controls/Pricing.js"),
                           new ScriptReference("~/Public/Scripts/Controls/PriceSpinBox.js")
                       };
        }

        #endregion
    }
}