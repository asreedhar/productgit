<%@ Control Language="C#" AutoEventWireup="true" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.Controls.Controls_SearchSummary" Codebehind="SearchSummary.ascx.cs" %>

<%@ Register Src="~/Pages/Internet/Controls/QuickModifySearch.ascx" TagName="QuickModifySearch" TagPrefix="Controls" %>

<cwc:TabContainer ID="SearchSummaryTabContainer" CssClass="vpa_search_tile" ActiveTabIndex="0" runat="server" OnActiveTabChanged="SearchSummaryTabContainer_ActiveTabChanged" OnActiveTabChanging="SearchSummaryTabContainer_ActiveTabChanging" >
	<cwc:TabPanel ID="YearMakeModelSummary" Text='<%# string.Format("Overall Search Results ({0})", NumberOfYearMakeModelResults) %>' CssClass="tab-overall-summary" runat="server">
	    <div class="ui-tab-content-wrapper">
	        <Controls:QuickModifySearch ID="YearMakeModelQuickModifySearch" runat="server" IsYearMakeModelSearch="true" OnSearchChanged="QuickModifySearch_SearchChanged" />
	    </div>
	</cwc:TabPanel>
	<cwc:TabPanel ID="PrecisionSearchResults" Text='<%# string.Format("Precision Search Results ({0})", NumberOfPrecisionResults) %>' CssClass="tab-precision" runat="server">
		<div class="ui-tab-content-wrapper">
	        <Controls:QuickModifySearch ID="PrecisionQuickModifySearch" runat="server" IsYearMakeModelSearch="false" OnSearchChanged="QuickModifySearch_SearchChanged" />
		</div>
	</cwc:TabPanel>
</cwc:TabContainer>
