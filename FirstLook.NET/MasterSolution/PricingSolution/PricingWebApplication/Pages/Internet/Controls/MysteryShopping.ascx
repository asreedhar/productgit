<%@ Control Language="C#" AutoEventWireup="true" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.Controls.Controls_MysteryShopping" Codebehind="MysteryShopping.ascx.cs" %>


<ul class="vpa_internet_shopper clearfix">
	<li class="first"><asp:HyperLink ClientIDMode="static" ID="AutoTraderHyperLink" runat="server" CssClass="listingProviderLink" Text="AutoTrader.com" Target="AutoTrader" OnPreRender="AutoTraderHyperLink_PreRender" /></li>
	<li><asp:HyperLink ClientIDMode="static" ID="CarsDotComHyperLink" runat="server" CssClass="listingProviderLink" Text="Cars.com" Target="Cars" OnPreRender="CarsDotComHyperLink_PreRender" /></li>
	<li><asp:HyperLink ClientIDMode="static" ID="CarsoupHyperlink" ToolTip="Limited Market Coverage: Mystery Shopping requires region selections for Carsoup" runat="server" CssClass="listingProviderLink" Text="Carsoup" Target="Carsoup" OnPreRender="CarsoupHyperlink_PreRender" /></li>
</ul>