using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.Common.WebControls.UI;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Marketing;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Distribution;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.KBB;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;
using FirstLook.Pricing.WebControls;

namespace FirstLook.Pricing.WebApplication.Pages.Internet.Controls
{
    public partial class ControlsMerchandisingInformation : UserControl, IScriptControl
    {
        #region Injected Properties
        public IPhotoServices PhotoServices { get; set; }
        #endregion

        public string AdvertisementDataSourceID
        {
            get { return AdvertisementDocumentEditor.DataSourceID; }
            set { AdvertisementDocumentEditor.DataSourceID = value; }
        }

        public string NotesDataSourceID
        {
            get { return NotesDocumentEditor.DataSourceID; }
            set { NotesDocumentEditor.DataSourceID = value; }
        }

        public string NotesBrand
        {
            get
            {
                string value = (string) ViewState["NotesBrand"];
                if (string.IsNullOrEmpty(value))
                    return string.Empty;
                return value;
            }
            set
            {
                if (NotesBrand != value)
                {
                    ViewState["NotesBrand"] = value;
                }
            }
        }

        public string MerchandisingBrand
        {
            get
            {
                string value = (string) ViewState["MerchandisingBrand"];
                if (string.IsNullOrEmpty(value))
                    return string.Empty;
                return value;
            }
            set
            {
                if (MerchandisingBrand != value)
                {
                    ViewState["MerchandisingBrand"] = value;
                }
            }
        }


        private VehicleType VehicleEntityType
        {
            get
            {
                return VehicleTypeCommand.Execute(Request.QueryString["vh"]);
            }
        }

        private int VehicleEntityId
        {
            get
            {
                return VehicleIdentityCommand.Execute(Request.QueryString["vh"]);
            }	
        }

        public bool HasInternetMysteryShopping
        {
            get
            {
                object value = ViewState["HasInternetMysteryShopping"];
                if (value == null)
                    return false;
                return (bool) value;
            }
            set
            {
                if (value != HasInternetMysteryShopping)
                {
                    ViewState["HasInternetMysteryShopping"] = value;
                }
            }
        }

        public bool HasNotes
        {
            get
            {
                object value = ViewState["HasNotes"];
                if (value == null)
                    return false;
                return (bool)value;
            }
            set
            {
                if (value != HasNotes)
                {
                    ViewState["HasNotes"] = value;
                }
            }
        }

        public bool HasInternetAdvertisement
        {
            get
            {
                object value = ViewState["HasInternetAdvertisement"];
                if (value == null)
                    return false;
                return (bool)value;
            }
            set
            {
                if (value != HasInternetAdvertisement)
                {
                    ViewState["HasInternetAdvertisement"] = value;
                }
            }
        }

        public bool HasOneClick
        {
            get
            {
                object value = ViewState["HasOneClick"];
                if (value == null)
                    return false;
                return (bool)value;
            }
            set
            {
                if (value != HasOneClick)
                {
                    ViewState["HasOneClick"] = value;
                }
            }
        }

        protected bool ConfirmedSaveEmptyAd
        {
            get
            {
                object value = ViewState["ConfirmedSaveEmptyAd"];
                if (value == null)
                    return false;
                return (bool)value;
            }
            set
            {
                if (value != ConfirmedSaveEmptyAd)
                {
                    ViewState["ConfirmedSaveEmptyAd"] = value;
                }
            }
        }


        /// <summary>
        /// Needed so the Distribution can send price and Description, PM
        /// </summary>
        public int CurrentPrice
        {
            get
            {
                object value = ViewState["CurrentPrice"];
                if (value == null)
                {
                    return 0;
                }
                return (int) value;
            }
            set
            {
                if (value != CurrentPrice)
                {
                    ViewState["CurrentPrice"] = value;
                }
            }
        }

        private void Page_LoadComplete(object sender, EventArgs e)
        {
            Page.LoadComplete -= Page_LoadComplete;

            MysteryShopping.Visible = HasInternetMysteryShopping;

            if (!string.IsNullOrEmpty(MerchandisingBrand)) 
            {
                MerchandisingBrandLiteral.Text = MerchandisingBrand;
            }

            if (HasNotes)
            {
                NotesPanel.Visible = true;

                NotesDocumentToolbar.Visible = HasOneClick &&
                                               (NotesDocumentEditor.CurrentMode == DocumentEditorMode.Edit ||
                                                NotesDocumentEditor.CurrentMode == DocumentEditorMode.Insert);

                if (!string.IsNullOrEmpty(NotesBrand))
                {
                    NotesHeaderLiteral.Text = NotesBrand;
                }            
            }
            else
            {
                NotesPanel.Visible = false;
            }

            if (HasInternetAdvertisement && (VehicleEntityType == VehicleType.Inventory || VehicleEntityType == VehicleType.InternetListing))
            {
                bool showToolbar = HasOneClick &&
                                   (AdvertisementDocumentEditor.CurrentMode == DocumentEditorMode.Edit ||
                                    AdvertisementDocumentEditor.CurrentMode == DocumentEditorMode.Insert);

                InternetAdvertisementPanel.Visible = true;

                AdvertisementDocumentToolbar.Visible = showToolbar;

                EquipmentDocumentToolBarLabelSpan.Visible = showToolbar;

                EquipmentDocumentToolBar.Visible = showToolbar;
            }
            else
            {
                InternetAdvertisementPanel.Visible = false;

                if (HasNotes)
                {
                    if (NotesDocumentToolbar.Items.Count == 3)
                    {
                        NotesDocumentToolbar.Items.RemoveAt(0);
                    }
                }
            }

            if (VehicleEntityType == VehicleType.Inventory)
            {
                var ownerHandle = Request.Params[CommonKeys.OWNER_HANDLE];
                var vehicleHandle = Request.Params[CommonKeys.VEHICLE_HANDLE];
                var businessUnit = HttpContext.Current.Items[BusinessUnit.HttpContextKey] as BusinessUnit;
                var inventory = Inventory.GetInventory(ownerHandle, vehicleHandle);

                PhotoManagerHyperlink.NavigateUrl = PhotoServices.GetPhotoManagerUrl(businessUnit.Id, inventory.Vin, inventory.StockNumber, PhotoManagerContext.MAX);

                PhotoManagerHyperlink.Attributes.Add("onclick", "OpenPopup(this.href,this.target); return false;");
            }
            else
            {
                PhotoManagerHyperlink.Enabled = false;
            }
        }

        protected void NotesPanel_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "CopyAdToNotes")
            {
                NotesDocumentEditor.BodyText += "***\nInternet Ad\n***\n";
                NotesDocumentEditor.BodyText += AdvertisementDocumentEditor.BodyText;
                NotesDocumentEditor.BodyText += "\n---\n";
                NotesDocumentEditor.BodyText += AdvertisementDocumentEditor.FooterText;
            }
        }

        protected void NotesTextEditor_Updating(object sender, CancelEventArgs e)
        {
            int length = NotesDocumentEditor.BodyText.Length;
            if (length > 500)
            {
                e.Cancel = true;
                PricingNotesFieldMaxLengthWarningDialog.Visible = true;

            }
            else
            {
                e.Cancel = false;
            }
        }

        protected void AdvertisementDocumentEditor_Updating(object sender, CancelEventArgs e)
        {
            int length = AdvertisementDocumentEditor.BodyText.Length + AdvertisementDocumentEditor.FooterText.Length;
            if (length > 2000)
            {
                e.Cancel = true;
                AdvertisementTextEditorMaxLengthWarningDialog.Visible = true;
            }        
            else
            {
                e.Cancel = false;
            }
        }

        protected void AdvertisementDocumentEditor_Updated(object sender, DocumentEditorUpdatedEventArgs e)
        {
            if (e.Exception == null || e.ExceptionHandled && VehicleEntityType == VehicleType.Inventory)
            {
                string newAdvertisementText = AdvertisementDocumentEditor.BodyText + AdvertisementDocumentEditor.FooterText;
                DistributeAdvertisementCommand.Execute(Request.QueryString["oh"], Request.QueryString["vh"], newAdvertisementText, CurrentPrice);
            }
        }

        protected void AdvertisementDocumentEditor_ModeChanging(object sender, DocumentEditorModeEventArgs e)
        {
            if (e.NewMode == DocumentEditorMode.Edit)
            {
                AdvertisementDocumentToolbar.Visible = HasOneClick;
                EquipmentDocumentToolBar.Visible = HasOneClick;
                EquipmentDocumentToolBarLabelSpan.Visible = HasOneClick;
            }
            else if (e.NewMode == DocumentEditorMode.Confirmation)
            {
                if (AdvertisementDocumentEditor.BodyText.Length == 0 && !ConfirmedSaveEmptyAd)
                {
                    e.Cancel = true;
                    AdvertisementTextEditorEmtpyWarningDialog.Visible = true;
                }

                AdvertisementDocumentToolbar.Visible = HasOneClick;
                EquipmentDocumentToolBar.Visible = HasOneClick;
                EquipmentDocumentToolBarLabelSpan.Visible = HasOneClick;
            }
            else
            {
                AdvertisementDocumentToolbar.Visible = false;
                EquipmentDocumentToolBar.Visible = false;
                EquipmentDocumentToolBarLabelSpan.Visible = false;
            }

            // Reset ConfirmedSaveEmptyAd flags
            ConfirmedSaveEmptyAd = false;
        }

        protected void NotesDocumentEditor_ModeChanging(object sender, DocumentEditorModeEventArgs e)
        {
            if (e.NewMode == DocumentEditorMode.Edit)
            {
                NotesDocumentToolbar.Visible = HasOneClick;
            }
            else if (e.NewMode == DocumentEditorMode.Confirmation)
            {            
                NotesDocumentToolbar.Visible = HasOneClick;
            }
            else
            {
                NotesDocumentToolbar.Visible = false;
            }
        }

        public void DataBindTextEditors()
        {
            if (AdvertisementDocumentEditor.Visible)
            {
                AdvertisementDocumentEditor.DataBind();
            }

            if (NotesDocumentEditor.Visible)
            {
                NotesDocumentEditor.DataBind();
            }
        }

        #region Page Events

        private ScriptManager _manager;

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.LoadComplete += Page_LoadComplete;
        }

        public void OnPriceChange()
        {
            if (AdvertisementDocumentToolbar.Visible)
            {
                AdvertisementDocumentToolbar.DataBind(); 
            }
            if (AdvertisementDocumentEditor.Visible)
            {
                AdvertisementDocumentEditor.DataBind();
            }
        }

        protected void VehicleHistoryReport_ReportChanged(object sender, EventArgs e)
        {
            if (AdvertisementDocumentToolbar.Visible)
            {
                AdvertisementDocumentToolbar.DataBind();
            }
            if (AdvertisementDocumentEditor.Visible)
            {
                AdvertisementDocumentEditor.DataBind();
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!DesignMode)
            {
                _manager = ScriptManager.GetCurrent(Page);

                if (_manager != null)
                    _manager.RegisterScriptControl(this);
                else
                    throw new ApplicationException("You must have a ScriptManager!");
            }
        }
    
    
        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);

            if (!DesignMode)
                _manager.RegisterScriptDescriptors(this);
        }
        #endregion

        #region IScriptControl

        public IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {
            if (NotesDocumentEditor.Visible && AdvertisementDocumentEditor.Visible && HasOneClick)
            {
                ScriptBehaviorDescriptor descriptor = new ScriptBehaviorDescriptor("FirstLook.Pricing.Website.MerchandisingTile", MerchandisingInformation.ClientID);

                descriptor.ID = MerchandisingInformation.ClientID;

                if (NotesDocumentEditor.Visible)
                {
                    descriptor.AddComponentProperty("notes_text_editor", NotesDocumentEditor.ClientID); 
                }

                if (NotesDocumentToolbar.Visible)
                {
                    descriptor.AddComponentProperty("notes_top_toolbar", NotesDocumentToolbar.ClientID); 
                }

                if (AdvertisementDocumentEditor.Visible)
                {
                    descriptor.AddComponentProperty("ad_text_editor", AdvertisementDocumentEditor.ClientID); 
                }

                if (AdvertisementDocumentToolbar.Visible)
                {
                    descriptor.AddComponentProperty("ad_top_toolbar", AdvertisementDocumentToolbar.ClientID);    
                }                

                descriptor.AddProperty("client_side_copy", true);

                return new ScriptBehaviorDescriptor[] { descriptor }; 
            }
            return new ScriptBehaviorDescriptor[] {};
        }

        public IEnumerable<ScriptReference> GetScriptReferences()
        {
            if (NotesDocumentEditor.Visible && AdvertisementDocumentEditor.Visible && HasOneClick)
            {
                ScriptReference scriptReference = new ScriptReference("~/Public/Scripts/Controls/MerchandisingInformation.js");
                return new ScriptReference[] { scriptReference }; 
            }
            return new ScriptReference[] {};
        }

        #endregion

        protected void PricingNotesFieldMaxLengthWarningDialog_ButtonClick(object sender, DialogButtonClickEventArgs e)
        {
            if (e.Button == DialogButtonCommand.Cancel)
            {
                PricingNotesFieldMaxLengthWarningDialog.Visible = false;
            }
        }

        protected void AdvertisementTextEditorMaxLengthWarningDialog_ButtonClick(object sender, DialogButtonClickEventArgs e)
        {
            if (e.Button == DialogButtonCommand.Cancel)
            {
                AdvertisementTextEditorMaxLengthWarningDialog.Visible = false;
            }
        }

        protected void AdvertisementTextEditorEmtpyWarningDialog_ButtonClick(object sender, DialogButtonClickEventArgs e)
        {
            if (e.Button == DialogButtonCommand.Cancel)
            {
                ConfirmedSaveEmptyAd = false;
                AdvertisementTextEditorEmtpyWarningDialog.Visible = false;
            }
            else if (e.Button == DialogButtonCommand.Ok)
            {
                ConfirmedSaveEmptyAd = true;
                AdvertisementTextEditorEmtpyWarningDialog.Visible = false;
                AdvertisementDocumentEditor.ChangeMode(DocumentEditorMode.Confirmation);
            }
        }

        protected void AdvertisementDocumentToolbar_OnCommand(object sender, CommandEventArgs e)
        {
            ToolBar toolBar = sender as ToolBar;

            if (toolBar != null)
            {
                ToolBarButton button = toolBar.Items.FindItem((string)e.CommandArgument) as ToolBarButton;
			
                if (button != null)
                {
                    AdvertisementDocumentEditor.BodyText += button.BodyText;
                    AdvertisementDocumentEditor.FooterText += button.FooterText;
                }
            }
        }

        protected void AdvertisementDocumentToolbar_OnDataBound(object sender, EventArgs e)
        {
            ToolBarButton autoCheckButton = AdvertisementDocumentToolbar.Items.FindItem("AutoCheck$Score") as ToolBarButton;

            if (autoCheckButton != null)
            {
                if (autoCheckButton.Enabled)
                {
                 /* ====================================================
                 * Pulls the numeric score out of string from body text
                 * AutoCheck Score 93 (Similar Vehicles Score 91 - 92)
                 * ====================================================*/
                    autoCheckButton.Text = autoCheckButton.BodyText.Substring(16, 3); 
                }
                else
                {
                    autoCheckButton.Text = "-";
                }
            }

            ToolBarButton kbbButton = AdvertisementDocumentToolbar.Items.FindItem("KelleyBlueBook") as ToolBarButton;

            if (kbbButton != null && !kbbButton.Enabled)
            {
                KelleyBlueBookReport kbbReport = KelleyBlueBookReportCommand.TryFetch(Request.QueryString["oh"], Request.QueryString["vh"]);

                if (kbbReport != null)
                {
                    int value =  kbbReport.Valuations.GetValue(KelleyBlueBookCategory.Retail,
                                                               KelleyBlueBookCondition.NotApplicable).Value;
                    int difference = CurrentPrice - value;

                    kbbButton.Enabled = true;

                    kbbButton.BodyText = string.Format("{0:$#,###,##0;$#,###,##0;$0}", difference);
                    kbbButton.BodyText += (difference > 0) ? " above" : " below";
                    kbbButton.BodyText += " KBB Retail";

                    kbbButton.FooterText += "Kelley Blue Book, " + kbbReport.Publication.Region + ", " +
                                            kbbReport.Publication.ValidFrom.ToString("d/m/yyyy");

                } 
            }
        }
    
        protected void NotesDocumentToolbar_Command(object sender, CommandEventArgs e)
        {
            ToolBar toolBar = sender as ToolBar;

            if (toolBar != null)
            {
                ToolBarButton button = toolBar.Items.FindItem((string) e.CommandArgument) as ToolBarButton;

                if (button != null)
                {
                    NotesDocumentEditor.BodyText += button.BodyText;
                }
            }
        }

        protected void NotesDocumentEditor_Updating(object sender, CancelEventArgs e)
        {
            int length = NotesDocumentEditor.BodyText.Length;
            if (length > 500)
            {
                e.Cancel = true;
                PricingNotesFieldMaxLengthWarningDialog.Visible = true;
            }
            else
            {
                e.Cancel = false;
            }
        }       
    }
}
