﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VehicleNavigator.ascx.cs" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.Controls.VehicleNavigator" %>

<div id="VehicleNavigatorControl" runat="server" class="clearfix">
    <ul class="next_prev_nav">
        <li class="prev"></li>
        <li class="next"></li>
    </ul>

    <asp:UpdatePanel ID="VehicleNavigatorUpdatePanel" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="VehicleSearchButton" EventName="Click" />
    </Triggers>
        <ContentTemplate>
    <fieldset>
    <asp:Label ID="VehicleSearchWarningLabel" runat="server"
        CssClass="warning"
                Text="Stock # Not Found" />
            <span class="overlabelContainer">    
            <asp:Label ID="VehicleSearchTextBoxLabel" runat="server"
                AssociatedControlID="VehicleSearchTextBox"
                CssClass="overlabel"
                Text="Stock #" />
            <asp:TextBox ID="VehicleSearchTextBox" runat="server" />
            <cwc:TextBoxOverLabelExtender ID="VehicleSearchTextBoxOverLabelExtender" runat="server"
                TargetControlID="VehicleSearchTextBox"
                LabelID="VehicleSearchTextBoxLabel" />
            </span>
    <span class="button"><asp:Button ID="VehicleSearchButton" runat="server" 
        Text="Search"
        OnClick="VehicleSearchButton_Click" /></span>
    </fieldset>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>