using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstLook.Pricing.WebApplication.Pages.Internet.Controls
{
    public partial class Controls_SimilarInventory : UserControl
    {
        #region Properties

        public override bool Visible
        {
            get
            {
                return base.Visible;
            }
            set
            {
                base.Visible = value;
                SimilarInventoryTablePanel.Visible = value;
            }
        }

        public bool EnableStockCardLink
        {
            get
            {
                return Convert.ToBoolean(ViewState["EnableStockCardLink"]);
            }
            set
            {
                if (EnableStockCardLink != value)
                {
                    ViewState["EnableStockCardLink"] = value;
                }
            }
        }

        #endregion

        protected void SimilarInventoryTableGridView_DataBound(object sender, EventArgs e)
        {
            if (!EnableStockCardLink)
            {
                HyperLinkField link = (HyperLinkField)SimilarInventoryTableGridView.Columns[10];
                link.DataNavigateUrlFormatString = ":";
            }
        }

        protected static string ProcessDescription(object value, bool pluralize)
        {
            string[] values = value.ToString().Split(' ');

            string text = values[0] + " " + values[1] + " " + values[2];

            if (pluralize)
            {
                if (text.EndsWith("S"))
                    text = text + "es";
                else
                    text = text + "s";
            }

            return text;
        }

        protected static bool GreaterThanOne(object value)
        {
            if (value != null && value != DBNull.Value)
            {
                return Convert.ToInt32(value) > 0;
            }
            return false;
        }

        protected void YearToggleButton_Click(object sender, EventArgs e)
        {
            if (string.Equals(ShowAllYears.Value, bool.TrueString))
            {
                ShowAllYears.Value = bool.FalseString;
                SimilarInventoryTableGridView.PageIndex = 0;
            }
            else
            {
                ShowAllYears.Value = bool.TrueString;
                SimilarInventoryTableGridView.PageIndex = 0;
            }
        }
    }
}