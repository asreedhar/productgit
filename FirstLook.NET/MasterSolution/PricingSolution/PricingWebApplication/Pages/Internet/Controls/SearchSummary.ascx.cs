using System;
using System.Web.UI;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search;
using FirstLook.Pricing.WebControls;

namespace FirstLook.Pricing.WebApplication.Pages.Internet.Controls
{
    public partial class Controls_SearchSummary : UserControl
    {
        private static readonly object EventSearchChanged = new object();

        private static readonly object EventSearchTypeChanged = new object();

        public event EventHandler SearchChanged
        {
            add
            {
                Events.AddHandler(EventSearchChanged, value);
            }
            remove
            {
                Events.AddHandler(EventSearchChanged, value);
            }
        }

        protected void OnSearchChanged()
        {
            EventHandler handler = (EventHandler)Events[EventSearchChanged];
            if (handler != null)
                handler(this, EventArgs.Empty);
        }

        public event EventHandler<SearchTypeEventArgs> SearchTypeChanged
        {
            add
            {
                Events.AddHandler(EventSearchTypeChanged, value);
            }
            remove
            {
                Events.AddHandler(EventSearchTypeChanged, value);
            }
        }

        protected void OnSearchTypeChanged(SearchTypeEventArgs e)
        {
            EventHandler<SearchTypeEventArgs> handler = (EventHandler<SearchTypeEventArgs>)Events[EventSearchTypeChanged];
            if (handler != null)
                handler(this, e);
        }

        protected int NumberOfYearMakeModelResults
        {
            get
            {
                object value = ViewState["NumberOfYearMakeModelResults"];
                if (value == null)
                    return default(int);
                return (int)value;
            }
            set
            {
                if (NumberOfYearMakeModelResults != value)
                {
                    ViewState["NumberOfYearMakeModelResults"] = value;
                }
            }
        }

        protected int NumberOfPrecisionResults
        {
            get
            {
                object value = ViewState["NumberOfPrecisionResults"];
                if (value == null)
                    return default(int);
                return (int)value;
            }
            set
            {
                if (NumberOfPrecisionResults != value)
                {
                    ViewState["NumberOfPrecisionResults"] = value;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SetSearchResultUnits();    

                if (Request.UrlReferrer != null && Request.UrlReferrer.LocalPath.Contains("ModifySearch.aspx"))
                {
                    SearchSummaryTabContainer.ActiveTabIndex = 1;
                    
                    SearchSummaryTabContainer_ActiveTabChanged(sender, new EventArgs());
                }
            }
        }

        protected void QuickModifySearch_SearchChanged(object sender, EventArgs e)
        {
            OnSearchChanged();

            SetSearchResultUnits();
        }

        protected void SearchSummaryTabContainer_ActiveTabChanging(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                if (SearchSummaryTabContainer.ActiveTabIndex == 0)
                {
                    YearMakeModelQuickModifySearch.MarshallSearchCriteria();
                }
                else
                {
                    PrecisionQuickModifySearch.MarshallSearchCriteria();
                }
            }
        }

        protected void SearchSummaryTabContainer_ActiveTabChanged(object sender, EventArgs e)
        {
            if (SearchSummaryTabContainer.ActiveTabIndex == 0)
            {
                YearMakeModelQuickModifySearch.PopulateSearchCriteria();

                OnSearchTypeChanged(SearchTypeEventArgs.YearMakeModel);
            }
            else
            {
                PrecisionQuickModifySearch.PopulateSearchCriteria();

                SearchSummaryCollection collection = SearchHelper.GetSearchSummaryCollection(Context);

                SearchType searchType = collection.PrecisionSearchSummary.SearchType;

                OnSearchTypeChanged(new SearchTypeEventArgs(searchType));
            }
        }

        private void SetSearchResultUnits()
        {
            SearchSummaryCollection collection = SearchHelper.GetSearchSummaryCollection(Context);
            NumberOfYearMakeModelResults = collection.YearMakeModelSearchSummary.Units;
            NumberOfPrecisionResults = collection.PrecisionSearchSummary.Units;
            if (collection.ActiveSearchSummary.SearchType == SearchType.YearMakeModel)
            {
                SearchSummaryTabContainer.ActiveTabIndex = 0;
            }
            else
            {
                SearchSummaryTabContainer.ActiveTabIndex = 1;
            }
        }
    }
}