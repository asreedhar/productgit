using System;
using System.Web.UI;
using FirstLook.Pricing.DomainModel.Commands.Impl;
using FirstLook.Pricing.WebControls;

namespace FirstLook.Pricing.WebApplication.Pages.Internet.Controls
{
    public partial class Controls_MysteryShopping : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private int ModelYear
        {
            get { return SearchHelper.GetSearch(Context).ModelYear; }
        }

        private int SearchRadius
        {
            get
            {
                return SearchHelper.GetSearch(Context).Distance.Value;
            }
        }

        private string AutoTraderUrl()
        {
            var oh = Request.QueryString["oh"];
            var sh = Request.QueryString["sh"];
            var vh = Request.QueryString["vh"];

            return MysteryShoppingUrlBuilder.AutoTraderUrl(oh, sh, vh, ModelYear, SearchRadius);
        }

        private string CarsoupUrl()
        {
            var oh = Request.QueryString["oh"];
            var sh = Request.QueryString["sh"];
            var vh = Request.QueryString["vh"];

            return MysteryShoppingUrlBuilder.CarSoupUrl(oh, sh, vh, ModelYear, SearchRadius);
        }

        private string CarsUrl()
        {
            var oh = Request.QueryString["oh"];
            var sh = Request.QueryString["sh"];
            var vh = Request.QueryString["vh"];

            return MysteryShoppingUrlBuilder.CarsUrl(oh, sh, vh, SearchRadius);
        }

        protected void AutoTraderHyperLink_PreRender(object sender, EventArgs e)
        {
            string url = AutoTraderUrl();
            AutoTraderHyperLink.NavigateUrl = url;
            AutoTraderHyperLink.Enabled = !string.IsNullOrEmpty(url);
        }

        protected void CarsDotComHyperLink_PreRender(object sender, EventArgs e)
        {
            string url = CarsUrl();
            CarsDotComHyperLink.NavigateUrl = url;
            CarsDotComHyperLink.Enabled = !string.IsNullOrEmpty(url);
        }

        protected void CarsoupHyperlink_PreRender(object sender, EventArgs e)
        {
            string url = CarsoupUrl();
            CarsoupHyperlink.NavigateUrl = url;
            CarsoupHyperlink.Enabled = !string.IsNullOrEmpty(url);
        }
    }
}