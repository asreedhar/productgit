<%@ Control Language="C#" AutoEventWireup="true" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.Controls.Controls_VehicleHistoryReport" Codebehind="VehicleHistoryReport.ascx.cs" %>

<div class="carfax_report vhr_report">
    <vwc:CarfaxReportDataSource ID="CarfaxDataSource" runat="server" 
            OnSelecting="CarfaxDataSource_Selecting"
            OnUpdating="CarfaxDataSource_Updating"
            OnInserting="CarfaxDataSource_Inserting">
        <Parameters>
            <asp:Parameter Name="DealerId" Type="Int32" />
            <asp:Parameter Name="Vin" Type="String"  />
        </Parameters>
    </vwc:CarfaxReportDataSource>
            
    <vwc:CarfaxReportView ID="CarfaxReportView" runat="server" DataSourceID="CarfaxDataSource"
            OnClick="CarfaxReportView_Click"
            OnPreRender="CarfaxReportView_PreRender"
            AutoGenerateInspectionItems="false"
            CombineUnbrandedInspectionItems="true" />
            
    <vwc:CarfaxReportForm ID="CarfaxOrderForm" runat="server" DataSourceID="CarfaxDataSource" Visible="false"
            OnInserted="CarfaxOrderForm_Inserted"
            OnUpdated="CarfaxOrderForm_Updated" />
</div>

<div class="autocheck_report vhr_report">
    <vwc:AutoCheckReportDataSource ID="AutoCheckDataSource" runat="server"
            OnSelecting="AutoCheckDataSource_Selecting"
            OnInserting="AutoCheckDataSource_Inserting">
        <Parameters>
            <asp:Parameter Name="DealerId" Type="Int32" />
            <asp:Parameter Name="Vin" Type="String"  />
        </Parameters>
    </vwc:AutoCheckReportDataSource>
        
    <vwc:AutoCheckReportView ID="AutoCheckReportView" runat="server" DataSourceID="AutoCheckDataSource"
            OnClick="AutoCheckReportView_Click"
            OnPreRender="AutoCheckReportView_PreRender"
            AutoGenerateInspectionItems="false"
            ShowAutoCheckAssured="false"
            ShowAutoCheckScore="false"
            ShowCleanTitle="false"
            ShowOneOwner="false"
            CombineUnbrandedInspectionItems="false" />
            
    <vwc:AutoCheckReportForm ID="AutoCheckOrderForm" runat="server" DataSourceID="AutoCheckDataSource" Visible="false"
            OnDataBound="AutoCheckOrderForm_DataBound"
            OnInserted="AutoCheckOrderForm_Inserted"
            OnUpdated="AutoCheckOrderForm_Updated" />
</div>