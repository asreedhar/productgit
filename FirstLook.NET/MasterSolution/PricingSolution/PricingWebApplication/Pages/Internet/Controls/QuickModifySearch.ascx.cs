using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Utilities;
using FirstLook.DomainModel.Lexicon.Categorization;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search;
using FirstLook.Pricing.WebControls;

namespace FirstLook.Pricing.WebApplication.Pages.Internet.Controls
{
    public partial class Controls_QuickModifySearch : UserControl
    {
        private static readonly object EventSearchChanged = new object();

        public event EventHandler SearchChanged
        {
            add
            {
                Events.AddHandler(EventSearchChanged, value);
            }
            remove
            {
                Events.AddHandler(EventSearchChanged, value);
            }
        }

        protected void OnSearchChanged()
        {
            EventHandler handler = (EventHandler)Events[EventSearchChanged];
            if (handler != null)
                handler(this, EventArgs.Empty);
        }

        public bool IsYearMakeModelSearch
        {
            get
            {
                object value = ViewState["IsYearMakeModelSearch"];
                if (value == null)
                    return true;
                return (bool)value;
            }
            set
            {
                if (IsYearMakeModelSearch != value)
                {
                    ViewState["IsYearMakeModelSearch"] = value;
                }
            }
        }

        public void MarshallSearchCriteria()
        {
            if (_searchBinderInitialized)
            {
                SearchBinder.MarshallSearchCriteria();
            }
        }

        public void PopulateSearchCriteria()
        {
            if (_searchBinderInitialized)
            {
                SearchBinder.PopulateSearchCriteria();
            }
        }

        private SearchSummaryInfo SearchSummaryInfo
        {
            get
            {
                return SearchHelper.GetSearchSummaryInfo(Context);
            }
        }

        private SearchBinder _searchBinder;

        private bool _searchBinderInitialized;

        private SearchBinder SearchBinder
        {
            get
            {
                if (_searchBinder == null)
                {
                    _searchBinder = new SearchBinder(Search);
                }

                return _searchBinder;
            }
        }

        private Search Search
        {
            get
            {
                return SearchHelper.GetSearch(Context);
            }
        }

        private OwnerPreferences _ownerPreferences;

        private OwnerPreferences OwnerPreferences
        {
            get
            {
                if (_ownerPreferences == null)
                {
                    _ownerPreferences = OwnerPreferences.GetOwnerPreferences(Request.Params["oh"]);
                }

                return _ownerPreferences;
            }
        }

        protected string VehicleDescriptionText
        {
            get
            {
                string vehicleDescriptionText = SearchSummaryInfo.YearMakeModel;

                if (IsYearMakeModelSearch || SearchSummaryInfo.Catalog.Series.Count == SearchSummaryInfo.Series.Count)
                {
                    vehicleDescriptionText += " All Trims";
                }
                else
                {
                    vehicleDescriptionText += " " + GetNames(SearchSummaryInfo.Series, "Trim");
                }

                return vehicleDescriptionText;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            // register pre-population
            Page.InitComplete += Page_InitComplete;
        }

        private void Page_InitComplete(object sender, EventArgs e)
        {
            // unregister self
            Page.InitComplete -= Page_InitComplete;
            // search radius
            SearchBinder.SearchDistanceTextBox = SearchDistanceTextBox;
            SearchBinder.SearchDistanceSliderExtender = SearchDistanceSliderExtender;
            SearchBinder.SearchDistanceSliderLabelExtender = SearchDistanceSliderLabelExtender;
            // mileage
            SearchBinder.SearchMileageStart = SearchMileageStart;
            SearchBinder.SearchMileageEnd = SearchMileageEnd;
            // check boxes
            SearchBinder.SearchCertified = SearchCertified;
            SearchBinder.SearchColor = SearchColor;
            // register event listeners
            SearchBinder.RegisterEventListeners();
            // flag initialization complete
            _searchBinderInitialized = true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (_searchBinderInitialized)
                {
                    SearchBinder.PopulateSearchCriteria();
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            DataBindChildren();
        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (_searchBinderInitialized)
            {
                SearchBinder.Dispose();
            }
        }

        protected void RestoreSystemSearchButton_Click(object sender, EventArgs e)
        {
            Page.LoadComplete += RestoreSystemSearch;

            SearchHelper.ClearSearch(Context);
        }

        private void RestoreSystemSearch(object sender, EventArgs e)
        {
            Page.LoadComplete -= RestoreSystemSearch;

            Search.RestorePrecisionSearch(SearchHelper.GetSearchKey(Context));

            IsPrecisionTrimSearchImageVisible();

            OnSearchChanged();
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            Page.LoadComplete += QuickSearch;

            SearchHelper.ClearSearch(Context);
        }

        private void QuickSearch(object sender, EventArgs e)
        {
            Page.LoadComplete -= QuickSearch;

            SearchBinder.UpdateSearch(Context.User.Identity.Name);

            OnSearchChanged();
        }

        protected void RestoreSystemSearchButton_PreRender(object sender, EventArgs e)
        {
            if (IsYearMakeModelSearch)
            {
                RestoreSystemSearchButton.Visible = false;
            }
            else
            {
                SearchSummaryCollection searches = SearchHelper.GetSearchSummaryCollection(Context);

                RestoreSystemSearchButton.Enabled = !searches.PrecisionSearchSummary.IsDefaultSearch;
            }
        }

        protected void SearchSummaryDetailDataSource_Load(object sender, EventArgs e)
        {
            if (IsYearMakeModelSearch)
            {
                SearchSummaryDetailDataSource.SelectParameters["searchTypeId"].DefaultValue = "1";
            }
            else
            {
                SearchSummaryDetailDataSource.SelectParameters["searchTypeId"].DefaultValue = "4";
            }
        }

        protected void SearchSummaryDetailGridView_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            SearchSummaryDetail searchSummaryDetail = e.Row.DataItem as SearchSummaryDetail;
            if (searchSummaryDetail != null)
            {
                if (searchSummaryDetail.IsSearch)
                    e.Row.CssClass += " is_search";

                if (searchSummaryDetail.IsReference)
                    e.Row.CssClass += " is_reference";
            }
        }

        protected void SearchGeneratedByLabel_PreRender(object sender, EventArgs e)
        {
            if (IsYearMakeModelSearch)
                SearchGeneratedByLabel.Visible = false;
        	
            if (!string.IsNullOrEmpty(Search.UpdateUser)) 
                SearchGeneratedByLabel.Text = Search.UpdateUser + " | ";
        }

        protected void SearchModifiedOnLabel_PreRender(object sender, EventArgs e)
        {
            if (IsYearMakeModelSearch)
                SearchModifiedOnLabel.Visible = false;
        	
            string innerText = string.Format("{0:MMM }", Search.UpdateDate);
            innerText += Int32Helper.FormatAsOrdinal(Search.UpdateDate.Day);
            SearchModifiedOnLabel.Text = innerText;
        }
    
        protected void GeneratedByLiteral_PreRender(object sender, EventArgs e)
        {
            if (IsYearMakeModelSearch)
                GeneratedByLiteral.Visible = false;
        }

        protected static string SanitizeSearchSummaryDescription(object o)
        {
            string s = o as string;
            if (s != null)
            {
                string endingMatch = " pickup";
                if (s.ToLower().EndsWith(endingMatch))
                    s = s.Remove(s.Length - endingMatch.Length, endingMatch.Length);

                return s; 
            }
            else
            {
                return string.Empty;
            }
        }

        protected void SearchCheckBoxList_PreRender(object sender, EventArgs e)
        {
            if (IsYearMakeModelSearch)
            {
                SearchCheckBoxList.Visible = false;
            }
        }

        private static string GetNames(IEnumerable<INameable> namableList, string title)
        {
            List<string> names = new List<string>();           

            foreach (INameable nameable in namableList)
            {
                if (nameable == null)
                {
                    names.Add("Other " + title);
                }
                else if (string.IsNullOrEmpty(nameable.Name))
                {
                    names.Add("Base " + title);
                }
                else
                {
                    names.Add(nameable.Name);
                }
            }

            names.Sort();

            return string.Join(", ", names.ToArray());
        }

        protected void IsPrecisionTrimSearchImage_PreRender(object sender, EventArgs e)
        {
            IsPrecisionTrimSearchImageVisible();
        }

        private void IsPrecisionTrimSearchImageVisible()
        {
            IsPrecisionTrimSearchImage.Visible = !OwnerPreferences.SuppressTrimMatchStatus && SearchSummaryInfo.IsPrecisionTrimSearch && !IsYearMakeModelSearch;
        }
    }
}