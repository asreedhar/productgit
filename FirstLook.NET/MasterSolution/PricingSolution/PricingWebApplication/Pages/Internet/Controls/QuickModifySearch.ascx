<%@ Control Language="C#" AutoEventWireup="true" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.Controls.Controls_QuickModifySearch" Codebehind="QuickModifySearch.ascx.cs" %>

<asp:Image ID="IsPrecisionTrimSearchImage" runat="server" 
    CssClass="is_series_match"
    SkinID="GreenCheck"
    OnPreRender="IsPrecisionTrimSearchImage_PreRender" />
<h3><asp:Literal ID="VehicleDescriptionLabel" runat="server" Text='<%# VehicleDescriptionText %>' /></h3>

<div class="search-ctrls">
    <div id="search-ctrls-distance-slider" class="clearself">
		<div class="left">
		    Within:
		</div>
		<div class="left">
			<div class="SearchDistanceSlider">
                <asp:TextBox ID="SearchDistanceTextBox" runat="server" CssClass="hidden"></asp:TextBox>
                <cwc:SliderExtender ID="SearchDistanceSliderExtender" runat="server"
                    TargetControlID="SearchDistanceTextBox"
                    EnableHandleAnimation="true"
                    HandleCssClass="ui-slider-handle"
                    HandleImageUrl="../../Public/Images/distance_slider_handle.png"
                    RailCssClass="ui-slider-1"
                    Minimum="1"
                    Maximum="10"
                    Steps="10" />
                <cwc:SliderLabelExtender ID="SearchDistanceSliderLabelExtender" runat="server"
                    TargetControlID="SearchDistanceTextBox"
                    LabelID="SearchDistanceSliderLabel"
                    LabelFormat="{0} miles" />
            </div>
		</div>
		<div id="search-modify-ranges-distance-label" class="clearfix left">
		    <asp:Label ID="SearchDistanceSliderLabel" runat="server" Text="miles"></asp:Label>
		</div>
	</div>
	<div id="search-modify-mileage">
		<label>Mileage:
            <asp:DropDownList ID="SearchMileageStart"
                runat="server"
                DataTextField="Value"
                DataValueField="Value"
                DataTextFormatString="{0:###,##0}" />
        </label>
        <label>
            to
            <asp:DropDownList ID="SearchMileageEnd" 
                runat="server"
                DataTextField="Value"
                DataValueField="Value"
                DataTextFormatString="{0:###,##0}" />
            <pwc:DropDownListValueExtender ID="SearchMileageExtender" runat="server"
                TargetControlID="SearchMileageStart"
                DropDownListID="SearchMileageEnd" />
        </label>
		    <asp:LinkButton ID="SearchButton" runat="server"
		        OnClick="SearchButton_Click"
		        Text="search"
		        CssClass="search_btn" />
	</div>
	
</div>

<div class="vpa_search_list">

<h5>Precision Matches:</h5>

<ul id="SearchCheckBoxList" runat="server" class="modify_search_attributes" OnPreRender="SearchCheckBoxList_PreRender">
    <li>
        <asp:CheckBox runat="server" ID="SearchCertified" Text="Certified" TextAlign="Right" />
    </li>
    <li>
        <asp:CheckBox runat="server" ID="SearchColor" Text="Color" TextAlign="Right" />
    </li>
</ul>

<asp:ObjectDataSource ID="SearchSummaryDetailDataSource" runat="server"
        OnLoad="SearchSummaryDetailDataSource_Load"
        TypeName="FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search.SearchSummaryDetailCollection+DataSource"
        SelectMethod="GetSearchSummaryDetails">
    <SelectParameters>
        <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
        <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="searchHandle" Type="String" QueryStringField="sh" />
        <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="vehicleHandle" Type="String" QueryStringField="vh" />
        <asp:Parameter Name="searchTypeId" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>

<asp:GridView ID="SearchSummaryDetailGridView" runat="server"
        DataSourceID="SearchSummaryDetailDataSource"
        AllowPaging="false" AllowSorting="false" AutoGenerateColumns="false"
        OnRowDataBound="SearchSummaryDetailGridView_OnRowDataBound"
        CssClass="vpa_search_summary">
    <Columns>
        <asp:TemplateField ItemStyle-CssClass="col_desc">
            <ItemTemplate>                
                <asp:Literal ID="DescriptionLiteral" runat="server" Text='<%# SanitizeSearchSummaryDescription(Eval("Description")) %>'></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField 
            DataField="NumberOfListings" 
            DataFormatString="{0:#,###,###;--;--}" 
            ReadOnly="true" 
            AccessibleHeaderText="# Listings" 
            HeaderText="Listings" 
            ItemStyle-CssClass="col_listing"
            HtmlEncode="false" />
        <asp:BoundField 
            DataField="MarketDaySupply" 
            DataFormatString="{0:#,###,###;--;--}" 
            ReadOnly="true" 
            AccessibleHeaderText="Market Day Supply" 
            HeaderText="Days Supply" 
            ItemStyle-CssClass="col_supply"
            HtmlEncode="false" />
        <asp:BoundField 
            DataField="AverageListPrice" 
            DataFormatString="{0:$#,###,###;--;--}" 
            ReadOnly="true" 
            AccessibleHeaderText="Avg. List Price" 
            HeaderText="Avg. Price" 
            ItemStyle-CssClass="col_price"
            HtmlEncode="false" />
    </Columns>
</asp:GridView>
</div>

<p class="generated_by">
    <asp:Literal ID="GeneratedByLiteral" runat="server" Text="Generated By:" OnPreRender="GeneratedByLiteral_PreRender" />
    <asp:Label ID="SearchGeneratedByLabel" runat="server" OnPreRender="SearchGeneratedByLabel_PreRender" Text="Firstlook | " />
    <asp:Label ID="SearchModifiedOnLabel" runat="server" OnPreRender="SearchModifiedOnLabel_PreRender" />
</p>

<div class='search_action_links'>
    <asp:LinkButton ID="RestoreSystemSearchButton" runat="server"
        Text="Restore System Search"
        CssClass="paneLink"
        OnPreRender="RestoreSystemSearchButton_PreRender"
        OnClick="RestoreSystemSearchButton_Click" />
</div>

