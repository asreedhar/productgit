﻿using System;
using System.Collections.Generic;
using System.Web.UI;

namespace FirstLook.Pricing.WebApplication.Pages.Internet.Controls
{
    public partial class VehicleNavigationItem : UserControl, IScriptControl
    {
        public string VehicleTooltip { get; set; }
        public string VehicleText { get; set; }
        public string VehicleUrl { get; set; }
        public string GridViewId { get; set; }
        public bool Hidden { get; set; }
        public int? NewPageIndex { get; set; } 

        private ScriptManager Manager
        {
            get
            {
                if (_manager == null)
                {
                    _manager = ScriptManager.GetCurrent(Page);
                    if (_manager == null)
                    {
                        throw new ApplicationException("You must have a ScriptManager!");
                    }
                }

                return _manager;
            }
        }

        #region Page Events

        private ScriptManager _manager;

        protected void Page_PreRender(Object sender, EventArgs e)
        {
            if (!DesignMode)
            {
                Manager.RegisterScriptControl(this);
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (Hidden)
            {
                VehiclePricingAnalyzerLink.Style.Add(HtmlTextWriterStyle.Display, "none");
            }

            VehiclePricingAnalyzerLink.NavigateUrl = VehicleUrl;
            VehiclePricingAnalyzerLink.Attributes["onclick"] = "OpenPopup(this.href, this.target);";

            if (!DesignMode && !string.IsNullOrEmpty(VehicleUrl))
                Manager.RegisterScriptDescriptors(this);

            base.Render(writer);
        }

        #endregion

        #region Implementation of IScriptControl

        public IEnumerable<ScriptDescriptor> GetScriptDescriptors()
        {
            ScriptBehaviorDescriptor vehicleNavigationItem = new ScriptBehaviorDescriptor("FirstLook.Pricing.Website.VehicleNavigationItem", VehiclePricingAnalyzerLink.ClientID);
            vehicleNavigationItem.AddProperty("text", VehicleText);
            vehicleNavigationItem.AddProperty("url", ResolveClientUrl(VehicleUrl));
            vehicleNavigationItem.AddProperty("tooltip", VehicleTooltip);

            if (NewPageIndex.HasValue && !string.IsNullOrEmpty(GridViewId))
            {
                vehicleNavigationItem.AddProperty("grid_view_id", GridViewId);
                vehicleNavigationItem.AddProperty("new_page_index", NewPageIndex);
            }

            return new[] {vehicleNavigationItem};
        }

        public IEnumerable<ScriptReference> GetScriptReferences()
        {
            if (_manager.ScriptMode == ScriptMode.Release)
            {
                return new[] { new ScriptReference("~/Public/Scripts/Controls/VehicleNavigationItem.js") };    
            }
            return new[] { new ScriptReference("~/Public/Scripts/Controls/VehicleNavigationItem.debug.js") };    
        }

        #endregion
    }
}