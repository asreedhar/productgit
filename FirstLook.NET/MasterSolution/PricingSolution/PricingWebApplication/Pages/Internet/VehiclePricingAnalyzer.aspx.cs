using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Utilities;
using FirstLook.Common.WebControls.UI;
using FirstLook.DomainModel.Oltp;
using FirstLook.Internal;
using FirstLook.Pricing.DomainModel.Internet.DataSource;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;
using FirstLook.Pricing.WebControls;
using FirstLook.Pricing.WebControls.AccessControl;
using FirstLook.Pricing.WebControls.UserControls;

namespace FirstLook.Pricing.WebApplication.Pages.Internet
{
    public partial class VehiclePricingAnalyzer : Page
    {
        
        #region Request Properties
    
        private string OwnerHandle
        {
            get { return Request.QueryString["oh"]; }
        }

        private string VehicleHandle
        {
            get { return Request.QueryString["vh"]; }
        }

        private string SearchHandle
        {
            get { return Request.QueryString["sh"]; }
        }

        private string Login
        {
            get { return User.Identity.Name; }
        }

        #endregion

        protected string BusinessUnitID { get; private set; }
        protected int HasFirstLook3 { get; private set; }

        private PackageDetails Details;

        protected int UnitsInStock
        {
            get
            {
                int? unitsInStock = Int32Helper.ToNullableInt32(ViewState["UnitsInStock"]);

                if (unitsInStock == null)
                {
                    ViewState["UnitsInStock"] =
                        unitsInStock =
                        new SimilarInventoryTableDataSource().CountByOwnerHandleAndVehicleHandle(OwnerHandle, VehicleHandle, false);
                }
            

                return unitsInStock.GetValueOrDefault();
            }
        }

        protected string UnitsInStockText
        {
            get
            {
                if (UnitsInStock == 1) return string.Format("1 Unit in Stock");

                return string.Format("{0} Units in Stock", UnitsInStock);
            }
        }

        #region Page Events

        protected void Page_Init(object sender, EventArgs e)
        {
            Details = PackageDetails.GetDealerPricingPackageDetails(OwnerHandle, SearchHandle, VehicleHandle);
            ApplyPageConfiguration();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Identity.IsUser())
                {
                    VehicleInteractionAuditCommand.Review(OwnerHandle, VehicleHandle, SearchHandle, Login);
                }

                BusinessUnit dealer = HttpContext.Current.Items[BusinessUnit.HttpContextKey] as BusinessUnit;

                if (dealer != null)
                {

                    VehicleType type = VehicleTypeCommand.Execute(VehicleHandle);

                    CVATabs.Visible = Details.Id >= 5 && dealer.HasDealerUpgrade(Upgrade.Marketing) && type == VehicleType.Inventory;
                    page_branding.Visible = !CVATabs.Visible;
                }
                else
                {
                    CVATabs.Visible = false;
                    page_branding.Visible = true;
                }
            }

            PreRenderComplete += CustomPagePreRenderComplete;
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            SimilarInventory.EnableStockCardLink = !Calculator.IsSalesToolVehicle;
        }

        private void ShowFirstlook3()
        {
            var owner = Owner.GetOwner(OwnerHandle);
            var businessUnit = BusinessUnitFinder.Instance().Find(owner.OwnerEntityId);
            VehicleType vehicleType = VehicleTypeCommand.Execute(VehicleHandle);


            // if the business Unit is null, it probably means that the owner type was set to "seller",
            // and that there is no business unit for it.
            if (businessUnit == null)
            {
                return;
            }

            //Supports only Firstlook 3.0 now.
            //var hasFirstLook30Upgrade = businessUnit.HasDealerUpgrade(Upgrade.FirstLook30);

            // write these out for GA tracking.
            BusinessUnitID = businessUnit.Id.ToString();
            //HasFirstLook3 = hasFirstLook30Upgrade ? 1 : 0;

            XUACompatibleTag.Visible = true;
            Stylesheet_FL30.Visible = true;

            //Supports only Firstlook 3.0 now.
            //if (hasFirstLook30Upgrade)
            //{
                legacyCalculator.Style.Add("display", "none");
                var inventory = Inventory.GetInventory(OwnerHandle, VehicleHandle);
                string bootStrap;
                using (
                    TextReader textReader =
                        new StreamReader(
                            typeof (FirstLookAssemblyInfo).Assembly.GetManifestResourceStream(
                                "FirstLook.Pricing.WebApplication.Pages.Internet.GaugeBootStrapScript.txt")))
                {
                    bootStrap = textReader.ReadToEnd();
                    var priceId = "#" + Calculator.PriceClientId;
                    var postScript = Calculator.RepricePostbackScript.Replace("javascript:", "");
                    bootStrap = String.Format(bootStrap, (inventory.ListPrice == null ? 0 : inventory.ListPrice),
                                              inventory.UnitCost, inventory.Mileage, (int) owner.OwnerEntityType,
                                              owner.OwnerEntityId, (int)vehicleType, inventory.Id, Login, priceId, postScript);
                }

                Page.ClientScript.RegisterStartupScript(GetType(), "Gauge-BootStrap", bootStrap, true);
            //}
        }

        protected void CustomPagePreRenderComplete(object sender, EventArgs e)
        {
            PreRenderComplete -= CustomPagePreRenderComplete;

            ShowFirstlook3();

            MerchandisingInformation.CurrentPrice = PricingPanels.Price.GetValueOrDefault(); /// MerchandisingInformation should be a Price Control, PM

            VehicleNavigator.Visible = Calculator.IsInventory;

            vpa.Attributes["class"] += Calculator.IsAppraisal ? " appraisal_view" : "";

            vpa.Attributes["class"] += Calculator.IsInventory ? " inventory_view" : "";

            vpa.Attributes["class"] += Calculator.IsInGroupVehicle ? " in_group_view" : "";

            vpa.Attributes["class"] += Calculator.IsOnlineAuctionVehicle ? " auction_view" : "";

            vpa.Attributes["class"] += Calculator.IsSalesToolVehicle ? " sales_view" : "";

            const string template = @"Sys.Application.add_init(function() {{VPA.VehicleEntityID={0};VPA.VehicleEntityTypeID={1};}});";

            string script = string.Format(template, Calculator.VehicleEntityID, Calculator.VehicleEntityTypeID);

            Page.ClientScript.RegisterStartupScript(GetType(), "Vehicle-Bootstrap", script, true);        
        }

        #endregion

        #region Element Events

        protected void SearchTypeID_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadSearchTypeID();
            }
        }

        private void LoadSearchTypeID()
        {
            SearchSummaryCollection collection = SearchHelper.GetSearchSummaryCollection(Context);

            SearchSummary summary = collection.ActiveSearchSummary;

            if (summary != null)
            {
                switch (summary.SearchType)
                {
                    case SearchType.YearMakeModel:
                        SearchTypeID.Value = "1";
                        break;
                    case SearchType.Precision:
                        SearchTypeID.Value = "4";
                        break;
                }
            }
        }

        protected void MarketPricingObjectDataSource_ObjectCreated(object sender, ObjectDataSourceEventArgs e)
        {
            ((MarketPricingDataSource)e.ObjectInstance).SetCache(Context.Items);
        }

        protected void SearchSummary_SearchChanged(object sender, EventArgs e)
        {
            // update the search type

            LoadSearchTypeID();

            if (IsPostBack)
            {
            RefreshUpdatePanels();
            }

            // force data binding as parameters have not changed

            Calculator.DataBind();

            if (Details.HasPricingTile)
                PricingPanels.DataBind();
        
            MarketListings.DataBind();
        }

        protected void SearchSummary_SearchTypeChanged(object sender, SearchTypeEventArgs e)
        {
            switch (e.SearchType)
            {
                case SearchType.YearMakeModel:
                    SearchTypeID.Value = "1";
                    break;
                case SearchType.Precision:
                    SearchTypeID.Value = "4";
                    break;
                default:
                    throw new ArgumentException("SearchType is not initialized", "e");
            }

                MarketListings.SearchType = e.SearchType; 
                SearchSummaryTray.ActiveSearchType = e.SearchType; 

            if (IsPostBack)
            {
            RefreshUpdatePanels();
            }
        }

        protected void Calculator_PriceChanging(object sender, EventArgs e)
        {
            Search.SetSearchTypeId(SearchHelper.GetSearchKey(Context), int.Parse(SearchTypeID.Value));
        }

        protected void Calculator_PriceChanged(object sender, PriceChangedEventArgs e)
        {
            PricingPanels.Price = e.NewPrice;

            if (Page.IsPostBack && MerchandisingInformationUpdatePanel.Visible)
            {
                MerchandisingInformation.CurrentPrice = e.NewPrice.GetValueOrDefault(); /// MerchandisingInformation should be a Price Control, PM

                MerchandisingInformation.OnPriceChange();

                MerchandisingInformationUpdatePanel.Update();
            }
        }

        #endregion

        #region WebMethods

        [WebMethod]
        public static void SaveVehiclePricingDecisionInput(string ownerHandle, string vehicleHandle, int? appraisalValue, int? estimatedReconditioningCost, int? targetGrossProfit)
        {
            VehiclePricingDecisionInput input = VehiclePricingDecisionInput.GetVehiclePricingDecisionInput(ownerHandle, vehicleHandle);
            input.AppraisalValue = appraisalValue;
            input.EstimatedAdditionalCosts = estimatedReconditioningCost;
            input.TargetGrossProfit = targetGrossProfit;
            input.Save();
        } 

        #endregion

        protected void SelectedEquipmentTabContainer_Init(object sender, EventArgs e)
        {
            VehicleInformationDataSource source = new VehicleInformationDataSource();

            DataTable providers = source.FindProvidersByOwnerHandleAndVehicleHandle(OwnerHandle);

            var uniqueProviders = providers.AsEnumerable().GroupBy(p => p["ProviderId"]).Select(g => g.First());
            bool setActiveTab = true;
            foreach (var provider in uniqueProviders)
            {
                int providerId = (int)provider["ProviderId"];

                TabPanel tabPanel = new TabPanel();
                tabPanel.ID = string.Format("EquipmentProvider-{0}", providerId);
                tabPanel.Text = string.Format("View {0} Equipment", provider["ProviderName"]);

                DataTable equipment = source.FindEquipmentByOwnerHandleAndVehicleHandle(
                    OwnerHandle, VehicleHandle, providerId);

                if (equipment.Rows.Count > 0)
                {
                    HtmlGenericControl ul = new HtmlGenericControl("ul");
                    tabPanel.Controls.Add(ul);

                    for (int j = 0, k = equipment.Rows.Count; j < k; j++)
                    {
                        DataRow item = equipment.Rows[j];

                        HtmlGenericControl li = new HtmlGenericControl("li");
                        li.InnerHtml = (string)item["OptionName"];
                        ul.Controls.Add(li);
                    }
                }
                else
                {
                    HtmlGenericControl empty = new HtmlGenericControl("p");
                    empty.InnerHtml = "This vehicle has no options selected.";
                    tabPanel.Controls.Add(empty);
                }

                HtmlGenericControl copyright = new HtmlGenericControl("p");
                copyright.InnerHtml = (string)provider["ProviderCopyright"];
                tabPanel.Controls.Add(copyright);

                SelectedEquipmentTabContainer.Tabs.Add(tabPanel);
                if (setActiveTab)
                {
                    setActiveTab = false;
                    SelectedEquipmentTabContainer.ActiveTabIndex = 0;
                }
            }
            
            
        }

        private void RefreshUpdatePanels()
        {
            SearchTypeUpdatePanel.Update();

            MerchandisingInformationUpdatePanel.Update();

            CalculatorUpdatePanel.Update();

            PricingPanelsUpdatePanel.Update();

            MarketListingsUpdatePanel.Update();
        }

        protected void PricingPanels_DataBinding(object sender, EventArgs e)
        {
            MerchandisingInformation.DataBindTextEditors();
        }
    
        private void ApplyPageConfiguration()
        {
            Page.Title = Details.PageBrand;

            vpa.Attributes["class"] += " package_" + Details.Id;

            PageBranding.Text = Details.PageBrand;
            PageBrandingTabLiteral.Text = Details.PageBrand;

            PricingPanels.Visible = Details.HasPricingTile;

            // If they don't have the pricing panel, show the VHR under the panel
            VehicleHistoryReport.Visible = !Details.HasPricingTile;

            if (Details.HasPricingTile)
            {
                PricingPanels.HasJDPower = Identity.HasJdPowerUpgrade(Login, OwnerHandle);
                PricingPanels.HasEdmundsTmv = Identity.HasEdmundsTmvUpgrade(Login, OwnerHandle);
            }

            if (Details.HasInternetAdvertisement || Details.HasNotes)
            {
                MerchandisingInformation.HasInternetMysteryShopping = Details.HasMysteryShoppingTile;
                MerchandisingInformation.NotesBrand = Details.NotesBrand;
                MerchandisingInformation.MerchandisingBrand = Details.MerchandisingBrand;
                MerchandisingInformation.HasNotes = Details.HasNotes;
                MerchandisingInformation.HasInternetAdvertisement = Details.HasInternetAdvertisement;
                MerchandisingInformation.HasOneClick = Details.HasOneClick;
            } 
            else
            {
                MerchandisingInformationUpdatePanel.Visible = false;

                MysteryShopping.Visible = Details.HasMysteryShoppingTile;
                Visible = true;
            }    	
        }
    }
}
