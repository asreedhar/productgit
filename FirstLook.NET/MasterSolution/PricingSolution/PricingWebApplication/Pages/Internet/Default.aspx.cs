using System;
using System.Diagnostics;
using System.Web;
using System.Web.UI;
using FirstLook.Common.Core.Utilities;
using FirstLook.DomainModel.Oltp;
using FirstLook.Pricing.DomainModel.Internet.DataSource;

namespace FirstLook.Pricing.WebApplication.Pages.Internet
{
    public partial class Default : Page 
    {
        private const string DealerToken = "DEALER_SYSTEM_COMPONENT";

        private const string InventoryUrl = "~/InventoryPricingAnalyzer.aspx";

        private const string VehicleUrl = "~/VehiclePricingAnalyzer.aspx";

        private const string SellerUrl = "~/ChooseSeller.aspx";

        private const string ErrorUrl = "~/ErrorPage.aspx?Error=410";

        protected void Page_Load(object sender, EventArgs e)
        {
            // pass on knowlegde of coming from max. this is an example of how NOT to do something.
 
            if (!string.IsNullOrEmpty(Request.QueryString["HALSESSIONID"]))
            {
                if (Request.Cookies["_session_id"] == null)
                {
                    Response.Cookies.Add(new HttpCookie("_session_id", Request.QueryString["HALSESSIONID"]));
                }
            }

            // route the user to the correct page

            bool forbidden = true;

            string token = Request.QueryString["token"];

            if (User.IsInRole("Administrator") || User.IsInRole("AccountRepresentative"))
            {
                if (!string.IsNullOrEmpty(token) && token.Equals(DealerToken))
                {
                    forbidden = EnterDealerTool();
                }
                else
                {
                    forbidden = EnterSellerTool();
                }
            }
            else if (User.IsInRole("User"))
            {
                forbidden = EnterDealerTool();
            }
            else if (User.IsInRole("Dummy"))
            {
                LogDummyUser();
            }

            if (forbidden)
            {
                Response.StatusCode = 403;
                Response.Status = "403 Forbidden";
            }
        }

        private bool EnterDealerTool()
        {
            BusinessUnit dealer = Context.Items[BusinessUnit.HttpContextKey] as BusinessUnit;

            if (dealer != null)
            {
                string pageName = Request.Params["pageName"];
                string ownerHandle   = Request.QueryString["oh"];
                string vehicleHandle = Request.QueryString["vh"];
                string searchHandle  = null;
                string insertUser    = Context.User.Identity.Name;

                if (!string.IsNullOrEmpty(ownerHandle) && !string.IsNullOrEmpty(vehicleHandle))
                {
                    try
                    {
                        searchHandle = new HandleLookupDataSource().FindByOwnerHandleAndVehicleHandle(
                            ownerHandle, vehicleHandle, insertUser);
                    }
                    catch
                    {
                        Response.Redirect(ErrorUrl, true);

                        return false;
                    }
                }
                else
                {
                    string inventoryId = Request.Params["inventoryId"];
                    string stockNumber = Request.Params["stockNumber"];
                    string appraisalId = Request.Params["appraisalId"];
                    string vehicleEntityTypeId = Request.Params["vehicleEntityTypeId"];
                    string vehicleEntityId = Request.Params["vehicleEntityId"];
                
                    ownerHandle = new OwnerHandleDataSource().FindByDealerId(dealer.Id);

                    try
                    {
                        HandleLookupDataSource finder = new HandleLookupDataSource();

                        if (!string.IsNullOrEmpty(inventoryId))
                        {
                            finder.FindByOwnerHandleAndVehicle(ownerHandle, 1,
                                                               Int32Helper.ToInt32(inventoryId),
                                                               insertUser,
                                                               out vehicleHandle, out searchHandle);
                        }
                        else if (!string.IsNullOrEmpty(stockNumber))
                        {
                            finder.FindByOwnerHandleAndStockNumber(ownerHandle, stockNumber, insertUser,
                                                                   out vehicleHandle, out searchHandle);
                        }
                        else if (!string.IsNullOrEmpty(appraisalId))
                        {
                            finder.FindByOwnerHandleAndVehicle(ownerHandle, 2,
                                                               Int32Helper.ToInt32(appraisalId), insertUser,
                                                               out vehicleHandle, out searchHandle);
                        }
                        else if (!(string.IsNullOrEmpty(vehicleEntityTypeId) || string.IsNullOrEmpty(vehicleEntityId)))
                        {
                            finder.FindByOwnerHandleAndVehicle(ownerHandle,
                                                               Int32Helper.ToInt32(vehicleEntityTypeId),
                                                               Int32Helper.ToInt32(vehicleEntityId),
                                                               insertUser,
                                                               out vehicleHandle, out searchHandle);
                        }
                    }
                    catch (Exception)
                    {
                        searchHandle = null;
                        vehicleHandle = null;
                    }
                }

                string url;

                if (!string.IsNullOrEmpty(pageName))
                {
                    url = "~/" + pageName + "?oh=" + ownerHandle;
                }
                else if (string.IsNullOrEmpty(searchHandle) || string.IsNullOrEmpty(vehicleHandle))
                {
                    url = InventoryUrl + "?oh=" + ownerHandle;
                }
                else
                {
                    url = VehicleUrl + "?oh=" + ownerHandle + "&sh=" + searchHandle + "&vh=" + vehicleHandle;
                }

                if (!string.IsNullOrEmpty(Request.QueryString["popup"]))
                    url += "&popup=true";

                if (!string.IsNullOrEmpty(Request.QueryString["referrer"]))
                    url += "&referrer=" + Request.QueryString["referrer"];

                Response.Redirect(url, true);

                return false;
            }

            return true;
        }

        private bool EnterSellerTool()
        {
            Response.Redirect(SellerUrl, true);

            return false;
        }

        private void LogDummyUser()
        {
            const string sourceName = "Pricing";

            const string logName = "ApplicationLog";

            if (!EventLog.SourceExists(sourceName))
            {
                EventLog.CreateEventSource(sourceName, logName);
            }

            EventLog log = new EventLog();
            log.Source = sourceName;
            log.WriteEntry(string.Format("User {0} is configured with MemberType of 'Dummy'", User.Identity.Name), EventLogEntryType.Error);
        }
    }
}