using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Utilities;
using FirstLook.DomainModel.Oltp;

namespace FirstLook.Pricing.WebApplication.Pages.Internet
{
    public partial class ChooseDealer : Page
    {
        //
        // Support Methods
        //

        private static readonly string DealerGroupToken = SoftwareSystemComponentStateFacade.DealerGroupComponentToken;

        private static readonly string DealerToken = SoftwareSystemComponentStateFacade.DealerComponentToken;

        protected SoftwareSystemComponentState SoftwareSystemComponentState
        {
            get { return (SoftwareSystemComponentState)Context.Items[SoftwareSystemComponentStateFacade.HttpContextKey]; }
            set { Context.Items[SoftwareSystemComponentStateFacade.HttpContextKey] = value; }
        }

        //
        // Page Initialize
        //

        protected void Page_Init(object sender, EventArgs e)
        {
            if (User.IsInRole("Administrator") || User.IsInRole("AccountRepresentative"))
            {
                SoftwareSystemComponentState = LoadState(DealerGroupToken);
            }
            else
            {
                Response.Redirect("~/Default.aspx", true);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void DealerGroupFilter_TextChanged(object sender, EventArgs e)
        {
            DealerGroupGridView.SelectedIndex = -1;
            DealerFilter.Text = "";
            DealerFilter_TextChanged(sender, e);
        }

        protected void DealerGroupGridView_PageIndexChanged(object sender, EventArgs e)
        {
            DealerGroupFilter_TextChanged(sender, e);
        }

        protected void DealerFilter_TextChanged(object sender, EventArgs e)
        {
            DealerGridView.SelectedIndex = -1;

            AppraisalFilter_TextChanged(sender, e);
        }

        protected void DealerGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("EnterDealer"))
            {
                int selectedIndex = Int32Helper.ToInt32(e.CommandArgument);

                GridView grid = sender as GridView;

                if (grid != null)
                {
                    int dealerId = Int32Helper.ToInt32(grid.DataKeys[selectedIndex]["Id"]);

                    BusinessUnit dealer = BusinessUnitFinder.Instance().Find(dealerId);

                    if (dealer != null)
                    {
                        SetupSoftwareSystemComponentState(dealer);

                        Response.Redirect("~/Default.aspx?token=DEALER_SYSTEM_COMPONENT&NoWait=true", true);
                    }
                }
            }
            else if (e.CommandName.Equals("EnterMarketStocking"))
            {
                int selectedIndex = Int32Helper.ToInt32(e.CommandArgument);

                GridView grid = sender as GridView;

                if (grid != null)
                {
                    int dealerId = Int32Helper.ToInt32(grid.DataKeys[selectedIndex]["Id"]);

                    BusinessUnit dealer = BusinessUnitFinder.Instance().Find(dealerId);

                    if (dealer != null)
                    {
                        SetupSoftwareSystemComponentState(dealer);

                        Response.Redirect("~/Default.aspx?token=DEALER_SYSTEM_COMPONENT&NoWait=true&pageName=Pages/Market/Default.aspx", true);
                    }
                }
            }
        }

        protected void DealerGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                GridView view = sender as GridView;

                if (view != null)
                {
                    BusinessUnit dealer = e.Row.DataItem as BusinessUnit;

                    if (dealer != null)
                    {
                        bool hasMarket = dealer.HasDealerUpgrade(Upgrade.MarketStocking);

                        e.Row.Cells[1].Enabled = hasMarket;

                        bool hasInternet = dealer.HasDealerUpgrade(Upgrade.PingTwo);

                        e.Row.Cells[2].Enabled = hasInternet;
                        e.Row.Cells[3].Enabled = hasInternet;
                    }
                }
            }
        }

        protected void DealerGridView_PageIndexChanged(object sender, EventArgs e)
        {
            DealerFilter_TextChanged(sender, e);
        }

        protected void AppraisalGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("PING"))
            {
                int selectedIndex = Int32Helper.ToInt32(e.CommandArgument);

                GridView grid = sender as GridView;

                if (grid != null)
                {
                    string vehicleEntityID = grid.DataKeys[selectedIndex]["VehicleEntityID"].ToString();

                    int dealerId = Int32Helper.ToInt32(DealerGridView.DataKeys[DealerGridView.SelectedIndex]["Id"]);

                    BusinessUnit dealer = BusinessUnitFinder.Instance().Find(dealerId);

                    if (dealer != null)
                    {
                        SetupSoftwareSystemComponentState(dealer);

                        Response.Redirect("~/Default.aspx?token=DEALER_SYSTEM_COMPONENT&vehicleEntityTypeId=2&vehicleEntityId=" + vehicleEntityID, true);
                    }
                }
            }
        }

        protected void AppraisalGridView_PageIndexChanged(object sender, EventArgs e)
        {
            AppraisalFilter_TextChanged(sender, e);
        }

        protected void AppraisalFilter_TextChanged(object sender, EventArgs e)
        {
            AppraisalGridView.SelectedIndex = -1;
        }

        private void SetupSoftwareSystemComponentState(BusinessUnit dealer)
        {
            SoftwareSystemComponentState state = LoadState(DealerToken);
            state.DealerGroup.SetValue(dealer.DealerGroup());
            state.Dealer.SetValue(dealer);
            state.Member.SetValue(null);
            state.Save();
        }

        private SoftwareSystemComponentState LoadState(string componentToken)
        {
            SoftwareSystemComponentState state = SoftwareSystemComponentStateFacade.FindOrCreate(User.Identity.Name, componentToken);

            if (state == null)
            {
                Member member = MemberFinder.Instance().FindByUserName(User.Identity.Name);

                SoftwareSystemComponent component = SoftwareSystemComponentFinder.Instance().FindByToken(componentToken);

                state = new SoftwareSystemComponentState();
                state.AuthorizedMember.SetValue(member);
                state.SoftwareSystemComponent.SetValue(component);
            }

            return state;
        }        
    }
}