<%@ Page Language="C#" AutoEventWireup="true" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.InventoryPricingAnalyzer" Codebehind="InventoryPricingAnalyzer.aspx.cs" Theme="Leopard" %>
<%@ OutputCache Location="None" VaryByParam="None" %>
<%@ Register Src="Controls/VehicleNavigationItem.ascx" TagPrefix="Controls" TagName="VehicleNavigationItem" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Inventory Pricing Analyzer</title>
    <script type="text/javascript" src="../../Public/Scripts/Lib/Combined.js"></script>
    <script type="text/javascript">
        function pageLoad() {
            var td1 = $('.red-background').parent();
            td1.addClass('red-background');
            var td2 = $('.green-background').parent();
            td2.addClass('green-background');
            var td3 = $('.yellow-background').parent();
            td3.addClass('yellow-background');
        }

        $().ready(function () {
            $('.dropMenu select').hide();
        });

    </script>
</head>
<body>
    <form id="ipa" runat="server">
    
        <owc:HedgehogTracking ID="Hedgehog" runat="server" />
        
        <asp:ScriptManager ID="scriptManager" EnablePartialRendering="true" LoadScriptsBeforeUI="false" runat="server" ScriptPath="/resources/Scripts">
             <Scripts>
                <asp:ScriptReference Path="../../Public/Scripts/Pages/global.js" NotifyScriptLoaded="true" />
                <asp:ScriptReference Path="../../Public/Scripts/Pages/IPA.js" NotifyScriptLoaded="true" />
            </Scripts>
        </asp:ScriptManager>
    
        <input type="hidden" id="LastVPALink" runat="server" />
        
        <span id="ContentPanel">

        <div id="doc3">
			<div id='hd'>
				<asp:SiteMapDataSource ID="ApplicationSiteMapDataSource" runat="server" ShowStartingNode="false" SiteMapProvider="ApplicationSiteMap" />	
				<asp:Menu id="application_menu_container" runat="server" CssClass="smallCaps blue" Orientation="Horizontal" DataSourceID="ApplicationSiteMapDataSource" />
				<asp:SiteMapDataSource ID="MemberSiteMapDataSource" runat="server" ShowStartingNode="false" SiteMapProvider="MemberSiteMap" />
				<asp:Menu ID="member_menu_container" runat="server" CssClass="ltGrey" Orientation="Horizontal" DataSourceID="MemberSiteMapDataSource" />
				<asp:Menu ID="seller_menu_container" runat="server" CssClass="ltGrey" Orientation="Horizontal" Visible="false">
					<Items>
						<asp:MenuItem NavigateUrl="~/Pages/Internet/ChooseSeller.aspx" Text="Choose Seller" />
					</Items>
				</asp:Menu>
			</div>		
    		
		    <div id="bd">
		        			
			    <div id="pageMenu">
    				<div id="pageTitle">
			            <h2><asp:Literal runat="server" Text="-" ID="DealerName" /></h2>
			        </div>
			        <asp:HyperLink ID="MarketStockingHyperlink" runat="server" Visible="false">Market Velocity Stocking Guide</asp:HyperLink>
			        <div id="PlanningReportLinks" runat="server">
    				    <a href="javascript:OpenPopup('/NextGen/ActionPlansReportSubmit.tx?timePeriodMode=CURRENT&daysBack=1&reportType=repricing&thirdParty=undefined&endDate=undefined','PL')">Repricing Action Plan</a>  |  <a href="javascript:OpenPopup('/IMT/PricingListDisplayAction.go','RAP')">Pricing List</a>
				    </div>
			    </div>
			    
			    <asp:UpdatePanel ID="ChartUpdatePanel" runat="server" ChildrenAsTriggers="true">
			        <ContentTemplate>
    			
			            <div id="chartHeading">
			                <div id="SaleStrategyContainer" class="dropMenu left" runat="server">
			                    <b>Show:</b> <span><asp:Literal ID="SalesStrategyLabel" runat="server" /></span>
                                <asp:DropDownList ID="SaleStrategyList" runat="server" AutoPostBack="True" OnPreRender="SalesStrategyList_PreRender" OnSelectedIndexChanged="SalesStrategyList_SelectedIndexChanged">
                                    <asp:ListItem Text="All Inventory" Selected="true" Value="A" />
                                    <asp:ListItem Text="Retail Inventory"  Value="R" />
                                </asp:DropDownList>
			                </div>
				            <div class="dropMenu">
				                <b><asp:Literal ID="InventoryTypeLiteral" runat="server">Current Inventory</asp:Literal> by</b> <span><asp:Literal ID="ModeLabel" runat="server" /></span>
				                <%-- filterMode --%>
	                            <asp:DropDownList OnPreRender="ModeList_PreRender" ID="ModeList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ModeList_SelectedIndexChanged">
                                    <asp:ListItem Text="Age" Selected="True" Value="A" />
                                    <asp:ListItem Text="Pricing Risk"  Value="R" />
                                    <asp:ListItem Text="All Inventory" Value="N" />
                                </asp:DropDownList>
				            </div>
				            <h1><asp:Label runat="server" Text="Inventory Units" ID="GraphSubtitle" /></h1>
			            </div>

			            <div id="chart">
        			        
                            <asp:ObjectDataSource
                                    ID="PricingGraphObjectDataSource"
                                    TypeName="FirstLook.Pricing.DomainModel.Internet.DataSource.PricingGraphDataSource"
                                    SelectMethod="FindByOwnerHandleAndModeAndSaleStrategy"
                                    runat="server">
                                <SelectParameters>
                                    <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
                                    <asp:ControlParameter ConvertEmptyStringToNull="True" Name="mode" Type="String" ControlID="ModeList" />
                                    <asp:ControlParameter ConvertEmptyStringToNull="True" Name="saleStrategy" Type="String" ControlID="SaleStrategyList" />
                                </SelectParameters>
                            </asp:ObjectDataSource>      
                            
                            <cwc:BarChart ID="PricingBarChart" runat="server"
                                    CssClass="pricing_bar_chart"
                                    DataSourceID="PricingGraphObjectDataSource"
                                    ColumnNameField="Name"
                                    ColumnValueField="Units"
                                    ColumnValueFormat="{0} Units"
                                    ColumnOnClickField="ColumnIndex"
                                    ColumnOnClickFormat="UpdateColumnIndex({0});"
                                    Visible="false"
                                    OnPreRender="PricingBarChart_PreRender">
                                <YAxis 
                                    Steps="4" 
                                    Modulo="5"
                                    GridLineColor="#bbd2e3"
                                    GridLineWidth="2px" />
                            </cwc:BarChart>
                            
                            <cwc:BarChart ID="RiskBarChart" runat="server"
                                    DataSourceID="PricingGraphObjectDataSource"
                                    CssClass="risk_bar_chart"
                                    ColumnNameField="Name"
                                    ColumnValueField="Percentage"
                                    ColumnValueTextField="Units"
                                    ColumnValueFormat="{0} Units"
                                    ColumnOnClickField="ColumnIndex"
                                    ColumnOnClickFormat="UpdateColumnIndex({0});"
                                    ShowPercentage="True"
                                    Visible="false"
                                    OnPreRender="RiskBarChart_PreRender">
                                <YAxis 
                                    Steps="4" 
                                    Modulo="25"
                                    Max="100"
                                    GridLineColor="#bbd2e3" 
                                    GridLineWidth="2px" />
                            </cwc:BarChart>                              
                            
			            </div>

                        <asp:HiddenField ID="ColumnIndex" runat="server" Value="0" />
                        
                        <asp:Button ID="PostBackButton" style="display: none;" runat="server" OnClick="PostBackButton_Click" />
                        
                        <asp:HiddenField ID="FilterColumnIndex" runat="server" Value="0" />

                        <asp:ObjectDataSource
                                ID="PricingGraphSummaryObjectDataSource"
                                TypeName="FirstLook.Pricing.DomainModel.Internet.DataSource.PricingGraphSummaryDataSource"
                                SelectMethod="FindByOwnerHandle"
                                runat="server">
                            <SelectParameters>
                                <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
                                <asp:ControlParameter ConvertEmptyStringToNull="True" Name="saleStrategy" Type="String" ControlID="SaleStrategyList" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        
                        <asp:Repeater ID="InventoryFactsRepeater" runat="server" DataSourceID="PricingGraphSummaryObjectDataSource" OnItemDataBound="InventoryFactsRepeater_ItemDataBound">
                            <HeaderTemplate>
                                <div id="inventoryFacts">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <span class="fact"><span><%# DataBinder.Eval(Container.DataItem, "UnitsInStock")%></span> Units in Stock</span>
				                <span id="InventoryFactsRepeater_DaySupply" runat="server" class="fact"><span><%# DataBinder.Eval(Container.DataItem, "DaysSupply")%></span> Days Supply</span>
				                <span class="fact"><span><%# DataBinder.Eval(Container.DataItem, "AvgListPrice", "{0:c0}")%></span> Avg. Internet Price</span>
				                <span class="fact"><span><%# string.Format("{0:0'%';N/A;N/A}", GetNullSafeInt(DataBinder.Eval(Container.DataItem, "AvgPctMarketValue")))%></span> Market Avg.</span>
				                <span class="fact last"><span><%# string.Format("{0:0;N/A;N/A}", GetNullSafeInt(DataBinder.Eval(Container.DataItem, "MarketDaySupply")))%></span> Days Supply - Market</span>
                            </ItemTemplate>
                            <FooterTemplate>
                                </div>
                            </FooterTemplate>
                        </asp:Repeater>

                        <%-- mode --%>
                        <asp:HiddenField ID="FilterMode" runat="server" Value="R" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                
                <asp:UpdatePanel ID="UpdatePanel1" runat="server"
                        ChildrenAsTriggers="true">
                    <Triggers>
                        <asp:PostBackTrigger ControlID="saveAsExcelButton" />
                    </Triggers>
                    <ContentTemplate>
			            <div id="gridHeading">
			                <asp:ObjectDataSource
                                    ID="InventoryTableSummaryObjectDataSource"
                                    TypeName="FirstLook.Pricing.DomainModel.Internet.DataSource.InventoryTableSummaryDataSource"
                                    SelectMethod="FindByOwnerHandleAndModeAndSaleStrategyAndFilters"
                                    runat="server">
                                <SelectParameters>
                                    <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
                                    <asp:ControlParameter ConvertEmptyStringToNull="True" Name="mode" Type="String" ControlID="FilterMode" />
                                    <asp:ControlParameter ConvertEmptyStringToNull="True" Name="saleStrategy" Type="String" ControlID="SaleStrategyList" />
                                    <asp:ControlParameter ConvertEmptyStringToNull="True" Name="filterMode" Type="String" ControlID="ModeList" />
                                    <asp:ControlParameter ConvertEmptyStringToNull="True" Name="filterColumnIndex" Type="Int32" ControlID="ColumnIndex" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
        			    
				            <div class="dropMenu left">
				                <b runat="server" id="InventoryTableTitle" OnPreRender="InventoryTableTitle_PreRender"></b>
				                <span><asp:Literal ID="FilterColumnLabel" runat="server" /></span>
				                <asp:DropDownList runat="server"
				                    ID="FilterColumnList" 
				                    DataSourceID="InventoryTableSummaryObjectDataSource" 
				                    DataTextField="Label" 
				                    DataValueField="Value" 
				                    AutoPostBack="True"
				                    OnPreRender="FilterColumnList_PreRender"
				                    OnSelectedIndexChanged="FilterColumnList_SelectedIndexChanged" 
				                    OnDataBound="FilterColumnList_DataBound" />
				            </div>
        				    
				            <asp:LinkButton ID="refreshButton" CssClass="button right" runat="server" OnClick="RefreshButton_Click" />
    				        <asp:LinkButton ID="saveAsExcelButton" runat="server" CssClass="button right" OnClick="ReportExportExcel_Click" /> 
				            <asp:LinkButton ID="stockOrModelSearchButton" CssClass="button right" runat="server" OnClick="StockOrModelSearchButton_Click" />
        				    
				            <%-- Hack flag to switch modes between searching and clicking the flash graph. --%>
				            <asp:HiddenField ID="CustomSearchFlag" runat="server" Value="False"/>
            				
				            <div class="right overlabelContainer">
				                Find: 
				                    <asp:Label ID="stockOrModelTextBoxLabel" runat="server"
				                            AssociatedControlID="stockOrModelTextBox"
				                            CssClass="overlabel"
				                            Text="STOCK# OR YEAR, MAKE, MODEL" />
				                    <asp:TextBox ID="stockOrModelTextBox" AutoPostBack="true" OnTextChanged="stockOrModelTextBox_TextChanged" CssClass="textBox" runat="server" />
				                    <cwc:TextBoxOverLabelExtender ID="StockOrModelTextBoxExtender" runat="server" TargetControlID="stockOrModelTextBox" LabelID="stockOrModelTextBoxLabel" />
				            </div>
            				
			            </div>
			        </ContentTemplate>
			    </asp:UpdatePanel>
			            
                <asp:UpdatePanel ID="TableUpdatePanel" runat="server"
                        ChildrenAsTriggers="true">                    
                    <ContentTemplate>  

                        
                        <div id="inventoryTable" class="pricingInventoryTable">
                        
                            <asp:ObjectDataSource runat="server"
                                   ID="InventoryTableObjectDataSource"
                                   TypeName="FirstLook.Pricing.DomainModel.Internet.DataSource.InventoryTableDataSource"
                                   SelectMethod="FindByOwnerHandleAndModeAndColumnIndexAndFilters"
                                   SelectCountMethod="CountByOwnerHandleAndModeAndColumnIndexAndFilters"
                                   EnablePaging="true"
                                   EnableViewState="false"
                                   MaximumRowsParameterName="maximumRows"
                                   StartRowIndexParameterName="startRowIndex"
                                   SortParameterName="sortColumns"
                                   OnSelecting="InventoryTableObjectDataSource_Selecting"
                                   OnSelected="InventoryTableObjectDataSource_Selected">
                                <SelectParameters>
                                    <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
                                    <asp:ControlParameter ConvertEmptyStringToNull="True" Name="mode" Type="String" ControlID="ModeList" />
                                    <asp:ControlParameter ConvertEmptyStringToNull="True" Name="saleStrategy" Type="String" ControlID="SaleStrategyList" />
                                    <asp:ControlParameter ConvertEmptyStringToNull="True" Name="columnIndex" Type="Int32" ControlID="ColumnIndex" />
                                    <asp:ControlParameter ConvertEmptyStringToNull="True" Name="filterMode" Type="String" ControlID="FilterMode" />
                                    <asp:ControlParameter ConvertEmptyStringToNull="True" Name="filterColumnIndex" Type="Int32" ControlID="FilterColumnList" />
                                    <asp:Parameter ConvertEmptyStringToNull="True" Name="inventoryFilter" Type="String" DefaultValue="" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                           
                            
                            <Controls:VehicleNavigationItem ID="PrevVehicleNavigationItem" runat="server" Hidden="true" />

                            <asp:GridView runat="server"
                                UseAccessibleHeader="true"
                                ID="InventoryTableGridView"
                                DataSourceID="InventoryTableObjectDataSource"
                                CssClass="grid inventory-pricing-grid"
                                AlternatingRowStyle-CssClass="odd"
                                AllowPaging="true"
                                AllowSorting="true"
                                AutoGenerateColumns="False"
                                PageSize="25"
                                OnRowCommand="InventoryTableGridView_Command"
                                OnLoad="InventoryTableGridView_Load"
                                OnRowDataBound="InventoryTableGridView_RowDataBound">
                                <PagerStyle CssClass="gridPager" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="8" />
                                <HeaderStyle CssClass="center" />
                                <Columns>
                                    <asp:BoundField 
                                        AccessibleHeaderText="Age" 
                                        HeaderText="Age" 
                                        DataField="Age" 
                                        SortExpression="Age" />
                                    <asp:TemplateField
                                        AccessibleHeaderText="Risk"
                                        HeaderText="Risk"
                                        SortExpression="RiskLight">
                                        <ItemTemplate>
                                            <div id="RiskLightImage" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField 
                                        AccessibleHeaderText="Vehicle Description" 
                                        HeaderText="Vehicle Description" 
                                        SortExpression="VehicleDescription">
                                        <ItemTemplate>
                                            <asp:Literal Text='<%#Eval("VehicleDescription")%>' ID="VehicleDescriptionText" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField 
                                        AccessibleHeaderText="Color" 
                                        HeaderText="Color" 
                                        DataField="VehicleColor" 
                                        SortExpression="VehicleColor" />
                                    <asp:BoundField 
                                        AccessibleHeaderText="Mileage" 
                                        HeaderText="Mileage" 
                                        DataField="VehicleMileage" 
                                        DataFormatString="{0:#,###,###;N/A}" 
                                        SortExpression="VehicleMileage"
                                        HtmlEncode="false" />
                                    <asp:BoundField 
                                        AccessibleHeaderText="Unit Cost" 
                                        HeaderText="Unit Cost" 
                                        DataField="UnitCost" 
                                        DataFormatString="{0:$#,##0}" 
                                        SortExpression="UnitCost"
                                        HtmlEncode="false" />
                                    <asp:TemplateField 
                                        AccessibleHeaderText="Internet Price" 
                                        HeaderText="Internet Price" 
                                        SortExpression="ListPrice">
                                        <ItemTemplate>
                                            <input type="hidden" id="HiddenVehicleHandle" runat="server" value='<%# Eval("VehicleHandle") %>' class='<%# Eval("VehicleHandle") %>' />
                                            <asp:Label ID="ListPriceText" Text='<%# String.Format("{0:$#,##0}", Eval("ListPrice")) %>' runat="server" />&nbsp;
                                            <Controls:VehicleNavigationItem ID="VehicleNavigationItem" runat="server"
                                                 VehicleText='<%# Eval("StockNumber") %>'
                                                 VehicleTooltip='<%# Eval("VehicleDescription") %>'
                                                 VehicleUrl='<%# string.Format("~/Pages/Internet/VehiclePricingAnalyzer.aspx?oh={0}&vh={1}&sh={2}", Request.QueryString["oh"], Eval("VehicleHandle"), Eval("SearchHandle")) %>' />                                                                                       
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField 
                                        AccessibleHeaderText="PIN Avg. Sale Price" 
                                        HeaderText="PIN Avg. Sale Price" 
                                        DataField="AvgPinSalePrice" 
                                        DataFormatString="{0:$#,##0}" 
                                        SortExpression="AvgPinSalePrice"
                                        HtmlEncode="false" />
                                    <asp:BoundField 
                                        AccessibleHeaderText="% of Market Avg." 
                                        HeaderText="% of Market Avg." 
                                        DataField="PctAvgMarketPrice" 
                                        SortExpression="PctAvgMarketPrice"
                                        HtmlEncode="false"
                                        DataFormatString="{0:0'%'}" />
                                    <asp:TemplateField
                                        AccessibleHeaderText="Change in Market" 
                                        HeaderText="Change in Market" 
                                        SortExpression="ChangePctAvgMarketPrice">
                                        <ItemTemplate>
                                            <asp:Label ID="ChangePctAvgMarketPriceText" ToolTip='<%# Eval("LastViewDate", "Change in Mkt. since {0:d}") %>' Text='<%# FormatEmptyString(Eval("ChangePctAvgMarketPrice", "{0:0\"%\";-0\"%\";0\"%\"}")) %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField 
                                        AccessibleHeaderText="Market Days Supply" 
                                        HeaderText="Market Days Supply" 
                                        DataField="MarketDaysSupply" 
                                        SortExpression="MarketDaysSupply"
                                        HtmlEncode="false"
                                        DataFormatString="{0:N0}" />
                                    <asp:BoundField
                                        AccessibleHeaderText="Precision Trim Search"
                                        HeaderText="Precision Trim Search"
                                        DataField="PrecisionTrimSearch"
                                        SortExpression="PrecisionTrimSearch" />
                                   <asp:TemplateField
                                        AccessibleHeaderText="Stock Number"
                                        HeaderText="Stock Number"
                                        SortExpression="StockNumber">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="EStockCardHyperlink" runat="server" Target="estock" Text='<%# Eval("StockNumber","{0}") %>' NavigateUrl='<%# Eval("StockNumber","/IMT/EStock.go?stockNumberOrVin={0}&isPopup=true") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField
                                        AccessibleHeaderText="Price Comparisons"
                                        HeaderText="Price Comparisons"
                                        ItemStyle-CssClass="price-comparison-image">
                                        <ItemTemplate>
                                        <span id="ComparisonColor" runat="server">
                                            <asp:Literal Text='<%#Eval("PriceComparisons")%>' ID="LtPriceComparison" runat="server" />
                                            </span>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:BoundField 
                                        AccessibleHeaderText="Make" 
                                        HeaderText="Make" 
                                        DataField="Make" 
                                        HtmlEncode="false"
                                        Visible="False" />
                                </Columns>
                            </asp:GridView>
                            
                            <Controls:VehicleNavigationItem ID="NextVehicleNavigationItem" runat="server" Hidden="true" />
                            
                            <div class="gridViewRowCount">
                                <asp:Literal OnPreRender="InventoryTableGridViewRowCount_PreRender" ID="InventoryTableGridViewRowCount" runat="server" />
                            </div>
                                                
                        </div>                        
                
                    </ContentTemplate>
                </asp:UpdatePanel>

		    </div>
    		<cwc:PageFooter ID="SiteFooter" runat="server" />
	    </div>
    
        </span>
            
        <div id="progress">
            
            <img src="../../Public/Images/loading.gif" alt="Loading" /> Please wait...
            
        </div>
        
    </form>
</body>
</html>
