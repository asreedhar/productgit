//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace FirstLook.Pricing.WebApplication.Pages.Internet {
    
    
    public partial class ModifySearch {
        
        /// <summary>
        /// vpa control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm vpa;
        
        /// <summary>
        /// Hedgehog control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::FirstLook.DomainModel.Oltp.WebControls.HedgehogTracking Hedgehog;
        
        /// <summary>
        /// MyScriptManager control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.ScriptManager MyScriptManager;
        
        /// <summary>
        /// VehicleInformation control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::FirstLook.Pricing.WebApplication.Pages.Internet.Controls.VehicleInformation.Control_VehicleInformation_VehicleInformation VehicleInformation;
        
        /// <summary>
        /// SearchUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel SearchUpdatePanel;
        
        /// <summary>
        /// NumberOfResultsLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal NumberOfResultsLiteral;
        
        /// <summary>
        /// NumberOfOverallResults control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal NumberOfOverallResults;
        
        /// <summary>
        /// SearchObjectDataSource control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ObjectDataSource SearchObjectDataSource;
        
        /// <summary>
        /// SearchDataView control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::FirstLook.Pricing.WebControls.SearchDataView SearchDataView;
        
        /// <summary>
        /// ModelYearHf control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField ModelYearHf;
        
        /// <summary>
        /// MakeHf control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField MakeHf;
        
        /// <summary>
        /// LineHf control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField LineHf;
        
        /// <summary>
        /// CatalogDVSelectionMessageLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label CatalogDVSelectionMessageLabel;
        
        /// <summary>
        /// CatalogDVSelectionMessageLabelFadingExtender control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::FirstLook.Common.WebControls.Extenders.FadingUserMessageLabel CatalogDVSelectionMessageLabelFadingExtender;
        
        /// <summary>
        /// CatalogDS control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::FirstLook.DomainModel.Lexicon.Categorization.WebControls.CatalogDataSource CatalogDS;
        
        /// <summary>
        /// CatalogFacetDv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::FirstLook.DomainModel.Lexicon.Categorization.WebControls.CatalogFacetDataView CatalogFacetDv;
        
        /// <summary>
        /// CatalogUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel CatalogUpdatePanel;
        
        /// <summary>
        /// ReturnToVpaHyperLink control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink ReturnToVpaHyperLink;
        
        /// <summary>
        /// SummaryNumberOfResultsLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal SummaryNumberOfResultsLiteral;
        
        /// <summary>
        /// SummaryNumberOfOverallResultsLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal SummaryNumberOfOverallResultsLiteral;
        
        /// <summary>
        /// OwnerPreferencesDataSource control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ObjectDataSource OwnerPreferencesDataSource;
        
        /// <summary>
        /// MarketSummaryPricingTable control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::FirstLook.Pricing.WebControls.MarketSummaryTable MarketSummaryPricingTable;
        
        /// <summary>
        /// MarketSummaryMileageTable control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::FirstLook.Pricing.WebControls.MarketSummaryTable MarketSummaryMileageTable;
        
        /// <summary>
        /// MarketListingObjectDataSource control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ObjectDataSource MarketListingObjectDataSource;
        
        /// <summary>
        /// ModelConfigurationIDLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label ModelConfigurationIDLabel;
        
        /// <summary>
        /// MarketListingGridView control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView MarketListingGridView;
        
        /// <summary>
        /// SiteFooter control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::FirstLook.Common.WebControls.UI.PageFooter SiteFooter;
    }
}
