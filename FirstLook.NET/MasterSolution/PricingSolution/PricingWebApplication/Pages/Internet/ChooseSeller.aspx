<%@ Page Language="C#" AutoEventWireup="true" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.ChooseSeller" Codebehind="ChooseSeller.aspx.cs" Theme="Leopard" %>

<%@ OutputCache Location="None" VaryByParam="None" %>

<%@ Register Assembly="FirstLook.Common.WebControls" Namespace="FirstLook.Common.WebControls.UI" TagPrefix="cwc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Choose Seller</title>   
</head>
<body>
    <form id="chooseSeller" runat="server" defaultfocus="ZipCode" defaultbutton="SubmitPricingSearchForm">
    <asp:ScriptManager ID="ScriptManager" runat="server" LoadScriptsBeforeUI="false" ScriptPath="/resources/Scripts">
        <Scripts>
            <asp:ScriptReference Path="~/Public/Scripts/Lib/Combined.js" />
            <asp:ScriptReference Path="~/Public/Scripts/Pages/global.js" />
        </Scripts>
    </asp:ScriptManager>

    <div id="doc3">
    <br />
    <div id="bd">

    <h1 id="pageHead"><span>Ping II</span> Market Pricing Analyzer:</h1>
    <asp:ValidationSummary ID="ValidationSummary" runat="server" DisplayMode="BulletList" ValidationGroup="FilterValidationGroup" />
    <asp:RegularExpressionValidator ID="ZipCodeRegularExpressionValidator" runat="server" ErrorMessage="Zipcode must be 3 to 5 numeric characters" ValidationGroup="FilterValidationGroup" ControlToValidate="ZipCode" SetFocusOnError="true" ValidationExpression="\d{3,5}" Display="None" />
    <div id="ResultsTable">
    <table id="PricingSearchForm">
        <tr>
            <td>Zipcode<br />
            <asp:TextBox ID="ZipCode" AccessKey="1" runat="server"></asp:TextBox>
            </td>
            <td>Dealership<br />
            <asp:TextBox ID="Dealership" AccessKey="2" runat="server"></asp:TextBox>
            </td>
            <td>Distance<br />
            <asp:DropDownList ID="Distance" AccessKey="4" runat="server">
                <asp:ListItem Text="0" Value="0" />
                <asp:ListItem Text="25" Value="25" />
                <asp:ListItem Text="50" Value="50" />
                <asp:ListItem Text="75" Value="75" />
                <asp:ListItem Text="100" Value="100" />
            </asp:DropDownList>
            </td>
            <td><br />
            <asp:Button ID="SubmitPricingSearchForm" AccessKey="5" Text="Search" runat="server" CausesValidation="true" ValidationGroup="FilterValidationGroup" OnClick="SubmitPricingSearchForm_Click" />
            </td>
            <td runat="server" ID="JDPowerTableCell">JD Power Region<br />
            <asp:ObjectDataSource 
                id="JDPowerRegionTableDataSource" 
                TypeName="FirstLook.Pricing.DomainModel.Internet.DataSource.JDPowerRegionTableDataSource"
                runat="server"
                SelectMethod="SelectJDPowerRegion">
            </asp:ObjectDataSource>
            <asp:DropDownList ID="JDPowerRegionDropDownList"
                AppendDataBoundItems="true"
                DataSourceID="JDPowerRegionTableDataSource"
                runat="Server"
                DataTextField="regionName"
                DataValueField="jdPowerRegionId">
                <asp:ListItem Text="Please Select" Value="0"></asp:ListItem>
            </asp:DropDownList>
            </td>
        </tr>
    </table>
    
    <span class="right" id="AlphaSearchLinks" visible="false" runat="server"><a href="javascript:UpdateColumnIndex(1);">A-G</a>|<a href="javascript:UpdateColumnIndex(2);">H-N</a>|<a href="javascript:UpdateColumnIndex(3);">O-U</a>|<a href="javascript:UpdateColumnIndex(4);">Q-Z</a>|<a href="javascript:UpdateColumnIndex(0);">Show All</a></span>
    
    <h3>Search Results: <asp:Label ID="NumberOfSearchResults" runat="server" Text="-" /></h3>
 
    <asp:HiddenField ID="ColumnIndex" runat="server" Value="0" />
    
    <asp:Button ID="PostBackButton" style="display: none;" runat="server" />
    
     <asp:ObjectDataSource
                    ID="SellerTableObjectDataSource"
                    runat="server"
                    TypeName="FirstLook.Pricing.DomainModel.Internet.DataSource.SellerTableDataSource"
                    SelectMethod="FindByZipCodeAndDealershipNameAndFilters"
                    SelectCountMethod="CountByZipCodeAndDealershipNameAndFilters"
                    EnablePaging="true"
                    MaximumRowsParameterName="maximumRows"
                    StartRowIndexParameterName="startRowIndex"
                    SortParameterName="sortColumns"
                    OnSelected="SellerTableObjectDataSource_Selected">
                <SelectParameters>
                    <asp:ControlParameter ConvertEmptyStringToNull="True" Name="zipCode" Type="String" ControlID="ZipCode" />
                    <asp:ControlParameter ConvertEmptyStringToNull="True" Name="dealershipName" Type="String" ControlID="Dealership" />
                    <asp:ControlParameter ConvertEmptyStringToNull="True" Name="jdPowerRegionId" Type="Int32" ControlID="JDPowerRegionDropDownList" />
                    <asp:ControlParameter ConvertEmptyStringToNull="True" Name="searchRadius" Type="Int32" ControlID="Distance" />
                    <asp:ControlParameter ConvertEmptyStringToNull="True" Name="columnIndex" Type="Int32" ControlID="ColumnIndex" />
                </SelectParameters>
            </asp:ObjectDataSource>
    
    <asp:GridView ID="SellerTableGridView" 
            DataSourceID="SellerTableObjectDataSource" 
            runat="server" 
            CssClass="grid" 
            AllowSorting="true" 
            AllowPaging="true" 
            AutoGenerateColumns="false"
            OnRowCommand="SellerTableGridView_RowCommand"
            OnRowDataBound="SellerTableGridView_RowDataBound"
            OnDataBound="SellerTableGridView_DataBound">
        <PagerStyle CssClass="gridPager" />
        <PagerSettings Mode="NumericFirstLast" 
            PageButtonCount="8" />
        <Columns>
            <asp:BoundField DataField="SellerName" HeaderText="Dealership Name" SortExpression="SellerName" />
            <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" />
            <asp:BoundField DataField="City" HeaderText="City" SortExpression="City" />
            <asp:BoundField DataField="State" HeaderText="State" SortExpression="State" />
            <asp:BoundField DataField="ZipCode" HeaderText="ZIP" SortExpression="ZipCode" />
            <asp:ButtonField DataTextField="UnitsListed" HeaderText="Units On Web" SortExpression="UnitsListed" ButtonType="Link" Text="Process" CommandName="Process" />
            <asp:BoundField DataField="LastScraped" DataFormatString="{0:MM/dd hh:mm tt}" HtmlEncode="false" HeaderText="Last Updated" SortExpression="LastScraped" />
            <asp:TemplateField HeaderText="Status">
                <ItemTemplate>
                    <asp:HiddenField ID="SellerID" runat="server" />
                    <asp:HiddenField ID="OwnerHandle" runat="server" />
                    <asp:HiddenField ID="AggregationStatusID" runat="server" />
                    <asp:Label ID="StatusText" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    </div>
    </div>
    <cwc:PageFooter ID="SiteFooter" runat="server" />
    </div>
    </form>
</body>
</html>
