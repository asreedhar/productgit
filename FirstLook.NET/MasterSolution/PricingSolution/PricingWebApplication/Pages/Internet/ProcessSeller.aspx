<%@ Page Language="C#" AutoEventWireup="true" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.ProcessSeller" Codebehind="ProcessSeller.aspx.cs" Theme="Leopard" %>

<%@ OutputCache Location="None" VaryByParam="None" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Process Seller</title>
    <script type="text/javascript" src="../../Public/Scripts/Lib/Combined.js"></script>
    <script type="text/javascript" src="../../Public/Scripts/Pages/global.js"></script>
    <script type="text/javascript" src="../../Public/Scripts/Pages/ProcessSeller.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrap">
    <asp:ScriptManager ID="scriptManager" EnablePartialRendering="true" runat="server" ScriptPath="/resources/Scripts"/>

    <asp:UpdatePanel ID="upProgressIndicator" runat="server">

        <ContentTemplate>
        <div id="hd">
        </div>
        <div id="bd">
            <div id="Progress">

            <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="350" height="350">
                <param name="movie" value="../../Public/Media/ping.swf">
                <embed src="../../Public/Media/ping.swf" type="application/x-shockwave-flash"></embed>
            </object>
            
            <h3>Analyzing Inventory</h3><br />
           
            <div id="VehicleCount">
                <span id="CurrentVehicleNumber"> </span> of <asp:Label ID="UnitsOnWebListingsTotalCount" runat="server" Text="-"></asp:Label>
            </div>
            <span id="CompletingContainer" style="display: none;">Completing Analysis ...</span>
            <span id="VinContainer">VIN: <span id="Vin"></span></span><br />
            <div id="ProgressBarContainer">
                <div id="ProgressBarFill"></div>
            </div>
            <span id="VehicleContainer"><span id="VehicleDescription"></span><br />
            <span id="VehicleMileage"> </span>&nbsp;miles</span>
            </div>
        </div>
         <asp:ObjectDataSource
               ID="UnitsOnWebListingsDataSource"
               TypeName="FirstLook.Pricing.DomainModel.Internet.DataSource.SellerInventoryTableDataSource"
               OnSelected="UnitsOnWebListingsDataSource_Selected"
               SelectMethod="FindBySellerId"
               runat="server">
            <SelectParameters>
                <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <asp:Repeater ID="UnitsOnWebRepeater" runat="server" DataSourceID="UnitsOnWebListingsDataSource">
            <HeaderTemplate><span id="vehicleArray" style="display: none;">[</HeaderTemplate>
            <ItemTemplate>"<%# DataBinder.Eval(Container.DataItem, "VehicleDescription") %>:<%# DataBinder.Eval(Container.DataItem, "VehicleMileage") %>:<%# DataBinder.Eval(Container.DataItem, "VIN") %>"</ItemTemplate>
            <SeparatorTemplate>,</SeparatorTemplate>
            <FooterTemplate>]</span></FooterTemplate>
        </asp:Repeater>
        </ContentTemplate>
    </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
