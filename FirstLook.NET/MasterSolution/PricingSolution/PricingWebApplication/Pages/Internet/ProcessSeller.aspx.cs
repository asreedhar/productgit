using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstLook.Pricing.WebApplication.Pages.Internet
{
    public partial class ProcessSeller : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                Response.Redirect(string.Format("~/Pages/Internet/InventoryPricingAnalyzer.aspx?oh={0}", Request.QueryString["oh"]), true);
            }
        }

        protected void UnitsOnWebListingsDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            DataTable table = e.ReturnValue as DataTable;

            if (table != null)
            {
                UnitsOnWebListingsTotalCount.Text = table.Rows.Count.ToString();
            }
        }
    }
}