<%@ Page Language="C#" AutoEventWireup="true" Inherits="FirstLook.Pricing.WebApplication.Pages.Internet.ChooseDealer" Codebehind="ChooseDealer.aspx.cs" %>

<%@ OutputCache Location="None" VaryByParam="None" %>

<%@ Register Assembly="FirstLook.DomainModel.Oltp" Namespace="FirstLook.DomainModel.Oltp.WebControls" TagPrefix="owc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Choose Dealer</title>
    <style type="text/css">
fieldset
{
    border: 1px solid #0066FF;
    float: left;
    padding: 5px;
    margin: 5px;
}
legend
{
    color: #0066FF;
    padding: 2px 6px
}
thead
{
    background-color: #0066FF;
}
table,td
{
    border: 1px solid black;
}
tr.table-row-selected
{
    background-color: #BBBBBB;
}
.table-row-pagination
{
    text-align: center;
}
    </style>
</head>
<body>
    <form id="form1" runat="server" defaultfocus="DealerGroupFilter" defaultbutton="DealerGroupFilterButton">
        <fieldset>
            <legend>Dealer Groups</legend>
            <div>
                <asp:Label
                    ID="DealerGroupFilterLabel"
                    AssociatedControlID="DealerGroupFilter"
                    runat="server"
                    Text="Filter Name: ">
                </asp:Label>
                <asp:TextBox
                    ID="DealerGroupFilter"
                    runat="server"
                    AutoPostBack="true"
                    AccessKey="1"
                    TabIndex="1"
                    ToolTip="Dealer Group Filter"
                    OnTextChanged="DealerGroupFilter_TextChanged">
                </asp:TextBox>
                <asp:Button ID="DealerGroupFilterButton" runat="server" Text="Filter" TabIndex="2" />
            </div>
            <div>
                <asp:ObjectDataSource
                    ID="DealerGroupDataSource"
                    TypeName="FirstLook.DomainModel.Oltp.BusinessUnitFinder"
                    SelectMethod="FindAllDealerGroupsForMember"
                    runat="server">
                    <SelectParameters>
                        <owc:MemberParameter Name="member" Type="Object" AllowImpersonation="false" />
                        <asp:ControlParameter Name="filter" Type="string" ControlID="DealerGroupFilter" DefaultValue="" ConvertEmptyStringToNull="true" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:GridView ID="DealerGroupGridView" runat="server"
                        DataKeyNames="Id"
                        DataSourceID="DealerGroupDataSource"
                        AutoGenerateColumns="false"
                        AllowPaging="true"
                        PageSize="20"
                        OnPageIndexChanged="DealerGroupGridView_PageIndexChanged">
                    <Columns>
                        <asp:BoundField DataField="Name" HeaderText="Name" />
                        <asp:CommandField ButtonType="Button" SelectText="Show Dealers" ShowSelectButton="true" />
                    </Columns>
                </asp:GridView>
            </div>
        </fieldset>
        <fieldset>
            <legend>Dealers</legend>
            <div>
                <asp:Label
                    ID="DealerFilterLabel"
                    AssociatedControlID="DealerFilter"
                    runat="server"
                    Text="Filter Name: ">
                </asp:Label>
                <asp:TextBox
                    ID="DealerFilter"
                    runat="server"
                    AutoPostBack="true"
                    AccessKey="2"
                    TabIndex="3"
                    ToolTip="Dealer Filter"
                    OnTextChanged="DealerFilter_TextChanged">
                </asp:TextBox>
                <asp:Button ID="DealerFilterButton" runat="server" Text="Filter" TabIndex="4" />
            </div>
            <div>
                <asp:ObjectDataSource
                    ID="DealerDataSource"
                    TypeName="FirstLook.DomainModel.Oltp.BusinessUnitFinder"
                    SelectMethod="FindAllDealersForMemberAndDealerGroup"
                    runat="server">
                    <SelectParameters>
                        <owc:MemberParameter Name="member" Type="Object" AllowImpersonation="false" />
                        <asp:ControlParameter Name="dealerGroupId" Type="Int32" ControlID="DealerGroupGridView" DefaultValue="" ConvertEmptyStringToNull="true" />
                        <asp:ControlParameter Name="filter" Type="string" ControlID="DealerFilter" DefaultValue="" ConvertEmptyStringToNull="true" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:GridView ID="DealerGridView" runat="server"
                        DataKeyNames="Id"
                        DataSourceID="DealerDataSource"
                        AutoGenerateColumns="false"
                        AllowPaging="true"
                        PageSize="20"
                        UseAccessibleHeader="true"
                        OnRowCommand="DealerGridView_RowCommand"
                        OnRowDataBound="DealerGridView_RowDataBound"
                        OnPageIndexChanged="DealerGridView_PageIndexChanged">
                    <Columns>
                        <asp:BoundField DataField="Name" HeaderText="Name" />
                        <asp:ButtonField ButtonType="Button" Text="Enter Market Stocking" CommandName="EnterMarketStocking" />
                        <asp:ButtonField ButtonType="Button" Text="Enter Ping" CommandName="EnterDealer" />
                        <asp:CommandField ButtonType="Button" SelectText="Appraisals" ShowSelectButton="true" />
                    </Columns>
                </asp:GridView>
            </div>
        </fieldset>
        <fieldset>
            <legend>Appraisals</legend>
            <div>
                <asp:Label
                    ID="AppraisalFilterLabel"
                    AssociatedControlID="AppraisalFilter"
                    runat="server"
                    Text="Filter Name: ">
                </asp:Label>
                <asp:TextBox
                    ID="AppraisalFilter"
                    runat="server"
                    AutoPostBack="true"
                    AccessKey="3"
                    TabIndex="5"
                    ToolTip="Appraisal Filter"
                    OnTextChanged="AppraisalFilter_TextChanged">
                </asp:TextBox>
                <asp:Button ID="AppraisalFilterButton" runat="server" Text="Filter" TabIndex="6" />
            </div>
            <div>
                <!-- Hypocrite Warning: Do not use the sql data source in any production product! -->
                <asp:SqlDataSource ID="AppraisalDataSource" runat="server"
                        SelectCommand="
                            WITH AppraisalView AS
                            (
                                SELECT  BusinessUnitID      = A.BusinessUnitID,
                                        VehicleEntityID 	= A.AppraisalID,
	                                    VehicleDescription	= CAST(V.VehicleYear AS VARCHAR) + ' ' + MMG.Make + ' ' + MMG.Model + COALESCE(' ' + V.VehicleTrim,'')
	                                    
	                            FROM	[IMT]..Appraisals A
                                JOIN    [IMT]..Vehicle V ON A.VehicleID = V.VehicleID
                                JOIN    [IMT]..MakeModelGrouping MMG ON V.MakeModelGroupingID = MMG.MakeModelGroupingID
                                	
                                WHERE	DATEDIFF(DD,A.DateCreated,GETDATE()) BETWEEN 0 AND 90
                                AND     A.AppraisalStatusID = 1
                            )
                            SELECT	A.VehicleEntityID,
                                    A.VehicleDescription
                                    
                            FROM	AppraisalView A
                            	
                            WHERE	A.BusinessUnitID = @DealerId
                            AND     (@Filter IS NULL OR @Filter = '' OR UPPER(A.VehicleDescription) LIKE UPPER('%' + @Filter + '%'))
                        "
                        SelectCommandType="Text"
                        ConnectionString="<%$ ConnectionStrings:IMT %>">
                    <SelectParameters>
                        <asp:ControlParameter Name="DealerId" Type="Int32" ControlID="DealerGridView" DefaultValue="" ConvertEmptyStringToNull="true" />
                        <asp:ControlParameter Name="Filter" Type="string" ControlID="AppraisalFilter" DefaultValue="" ConvertEmptyStringToNull="false" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:GridView ID="AppraisalGridView" runat="server"
                        DataKeyNames="VehicleEntityID"
                        DataSourceID="AppraisalDataSource"
                        AutoGenerateColumns="false"
                        AllowPaging="true"
                        PageSize="20"
                        UseAccessibleHeader="true"
                        OnRowCommand="AppraisalGridView_RowCommand"
                        OnPageIndexChanged="AppraisalGridView_PageIndexChanged">
                    <Columns>
                        <asp:BoundField DataField="VehicleDescription" HeaderText="Description" />
                        <asp:ButtonField ButtonType="Button" Text="PING" CommandName="PING" />                        
                    </Columns>
                </asp:GridView>
            </div>
        </fieldset>
    </form>
</body>
</html>
