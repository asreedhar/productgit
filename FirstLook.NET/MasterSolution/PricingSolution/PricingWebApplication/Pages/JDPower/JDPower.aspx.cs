using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Csla.Web;
using FirstLook.DomainModel.Oltp;
using FirstLook.Pricing.DomainModel.JDPower.DataSource;

namespace FirstLook.Pricing.WebApplication.Pages.JDPower
{
    public partial class JDPower : Page
    {
        private const string PowerRegion_ID = "2";
        private const string RollUpRegion_ID = "1";
        private const string NationalRegion_ID = "0";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitializeDataReferenceDataControls();
                InitializeFilterDataControls();
            }
        }

        private void InitializeFilterDataControls()
        {
            // Explicitly data binds the controls
            // whose selected values parameterize 
            // the generation of the sales report. 
            // Needs to be done explicitly here, 
            // before the DataGrid control of the 
            // sales report is bound
            JDMinYears.DataBind();
            JDMaxYears.DataBind();
            JDMinMileageRange.DataBind();
            JDMaxMileageRange.DataBind();

            JDMinYears_Init();
            JDMaxYears_Init();
            JDMinMileageRange_Init();
            JDMaxMileageRange_Init();
            JDTimePeriod_Init();
        }

        private void InitializeDataReferenceDataControls()
        {
            #region DataChoice Controls

            JDTimePeriod.DataBind();
            JDFranchiseRadio.DataBind();

            JDFranchiseRadio_Init();
            SelectedRegionID.Value = PowerRegion_ID;

            #endregion

            JDVehicleText_Init(); // Reference Data
            DealerNameText_Init();
        }

        #region OnClicks

        protected void PowerRegionButton_Click(object sender, EventArgs e)
        {
            SelectedRegionID.Value = PowerRegion_ID;
            PowerRegionButton.CssClass = "active";
            RollUpRegionButton.CssClass = "none";
            NationalRegionButton.CssClass = "none";
        }

        protected void RollUpRegionButton_Click(object sender, EventArgs e)
        {
            SelectedRegionID.Value = RollUpRegion_ID;
            RollUpRegionButton.CssClass = "active";
            PowerRegionButton.CssClass = "none";
            NationalRegionButton.CssClass = "none";
        }

        protected void NationalRegionButton_Click(object sender, EventArgs e)
        {
            SelectedRegionID.Value = NationalRegion_ID;
            NationalRegionButton.CssClass = "active";
            PowerRegionButton.CssClass = "none";
            RollUpRegionButton.CssClass = "none";
        }

        #endregion OnClicks

        #region Data Access

        protected void JDPowerRegionSelectorPanel_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                JDPowerRegionPanel regions = (JDPowerRegionPanel) Context.Items[JDPowerRegionPanel.HttpContextKey];
                if (regions == null)
                {
                    SoftwareSystemComponentState state =
                        (SoftwareSystemComponentState) Context.Items[SoftwareSystemComponentStateFacade.HttpContextKey];
                    if (state != null)
                    {
                        regions = JDPowerRegionPanel.GetPanel(state.Dealer.GetValue().Id);
                    }
                    else
                    {
                        throw new NullReferenceException("SoftwareSystemComponentState can not be null");
                    }
                }
                PowerRegionButton.Text = regions.PowerRegion.Name;
                PowerRegionButton.CssClass = "active";
                RollUpRegionButton.Text = regions.RollUpRegion.Name;
                NationalRegionButton.Text = "National";

                SelectedRegionID.Value = PowerRegion_ID; // track cur selected region
            }
        }

        protected JDPowerMileageRangePanel GetMileageRangePanel()
        {
            JDPowerMileageRangePanel panel = Context.Items["MileageRangePanel"] as JDPowerMileageRangePanel;
            if (panel == null)
            {
                panel = JDPowerMileageRangePanel.GetPanel();
            }
            return panel;
        }

        protected void MileageRange_Select(object sender, SelectObjectArgs e)
        {
            e.BusinessObject = GetMileageRangePanel().MileageRanges;
        }

        protected JDPowerColorPanel GetColorPanel()
        {
            JDPowerColorPanel panel = Context.Items["ColorPanel"] as JDPowerColorPanel;
            if (panel == null)
            {
                panel = JDPowerColorPanel.GetPanel();
            }
            return panel;
        }

        protected void Color_Select(object sender, SelectObjectArgs e)
        {
            e.BusinessObject = GetColorPanel().Colors;
        }

        private JDPowerTimePeriodPanel GetJDPowerTimePeriodPanel()
        {
            JDPowerTimePeriodPanel panel = Context.Items["JDPowerTimePeriodPanel"] as JDPowerTimePeriodPanel;
            if (panel == null)
            {
                panel = JDPowerTimePeriodPanel.GetPanel();
            }
            return panel;
        }

        protected void JDPowerTimePeriodPanel_Select(object sender, SelectObjectArgs e)
        {
            e.BusinessObject = GetJDPowerTimePeriodPanel().TimePeriodCollection;
        }

        #endregion Data Access

        #region init
    
        private void DealerNameText_Init()
        {
            SoftwareSystemComponentState state = (SoftwareSystemComponentState)Context.Items[SoftwareSystemComponentStateFacade.HttpContextKey];
            if (state != null)
            {
                DealerName.Text = state.Dealer.GetValue().Name;
            }
        }

        private void JDVehicleText_Init()
        {
            string vehicleCatalogId = Request.Params["vehicleCatalogId"];
            if (vehicleCatalogId == null)
            {
                vehicleCatalogId = "-1";
            }
            JDVehicle.Text = Request.Params["modelYear"] + " " + JDPowerMarketTool.SelectModelName(Int32.Parse(vehicleCatalogId));
        }

        protected void JDTimePeriod_Init()
        {
            string timePeriodId = Request.Params["timePeriodId"];
            if (timePeriodId == null)
            {
                timePeriodId = "1";
            }

            ListItem item = JDTimePeriod.Items.FindByValue(timePeriodId);
            JDTimePeriod.SelectedIndex = JDTimePeriod.Items.IndexOf(item);
        }

        protected void JDMinYears_Init()
        {
            string modelYear = Request.Params["modelYear"];
            if (modelYear != null)
            {
                JDMinYears.SelectedValue = modelYear;
                JDMaxYears.SelectedIndex = JDMinYears.SelectedIndex + 1;
            }
        }

        protected void JDMaxYears_Init()
        {
            JDMaxYears.SelectedIndex = JDMinYears.SelectedIndex;
        }

        protected void JDMinMileageRange_Init()
        {
            JDMinMileageRange.SelectedIndex = 0; // min mileage range
        }    

        protected void JDMaxMileageRange_Init()
        {
            int maxIndex = JDMaxMileageRange.Items.Count - 1;
            JDMaxMileageRange.SelectedIndex = maxIndex; //  >= 125000 mileage range
            JDMaxMileageRange.Items[maxIndex].Text = "125000+"; // FB 2541 - nicer display value than 999999 
        }

        protected void JDFranchiseRadio_Init()
        {
            JDFranchiseRadio.SelectedIndex = 0; // default to all dealers
        }
        #endregion init

        #region GridView Rendering

        protected void JDPowerTopSalesReport_DataBound(object sender, EventArgs e)
        {
            CheckForEmptyCells(JDPowerTopSalesReport);
        }

        protected void JDPowerBottomSalesReport_DataBound(object sender, EventArgs e)
        {
            CheckForEmptyCells(JDPowerBottomSalesReport);
        }

        private void CheckForEmptyCells(GridView g)
        {
            const string emptyCellDollarValue = "$0";
            const string emptyCellText = "-";
            const string emptyCellCssClass = "warning";
            bool notEnoughData = false;
            GridViewRow r = g.Rows[0];
            if (r.RowType == DataControlRowType.DataRow)
            {
                foreach (TableCell tc in r.Cells)
                {
                    if (string.Equals(tc.Text, emptyCellDollarValue) || string.Equals(tc.Text, emptyCellText) || string.Equals(tc.Text, "&nbsp;")
                        || string.Equals(tc.Text, "$") || string.Equals(tc.Text, "0") || string.Equals(tc.Text, string.Empty))
                    {
                        notEnoughData = true;
                        tc.Text = emptyCellText;
                        // May need to update to CssClassHelper.AddClass if this TableCell stats needing more then one CssClass
                        tc.CssClass = emptyCellCssClass; 
                    }
                }
            }
            if (notEnoughData)
            {
                NotEnoughDataMessage.Visible = true;
                GrossFIProductsFormulaMessage.Visible = false;
            } else
            {
                NotEnoughDataMessage.Visible = false;
                GrossFIProductsFormulaMessage.Visible = true;
            }
        }

        #endregion GridView Rendering

        protected void DetailedSummaryUpdatePanel_PreRender(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                CheckForEmptyCells(JDPowerBottomSalesReport); 
            }
        }
    }
}