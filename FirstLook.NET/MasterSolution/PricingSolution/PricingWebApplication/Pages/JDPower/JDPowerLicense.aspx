<%@ Page Language="C#" AutoEventWireup="true" Inherits="FirstLook.Pricing.WebApplication.Pages.JDPower.JDPowerLicense" Codebehind="JDPowerLicense.aspx.cs" Theme="Planate" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>J.D. Power</title>
    <script src="../../Public/Scripts/JDPowerLicense.js" type="text/javascript"></script>
</head>
<body>
        <form id="jdpowerlicense" runat="server" >
            <div id="header">
                <h3>Initial Tool Terms &amp; Agreement</h3><br />
            </div>
            <div id="content">
                <asp:TextBox Rows="10" runat="server" TextMode="MultiLine" ReadOnly="true" ID="LicenseAgreementTextBox" meta:resourcekey="LicenseAgreementTextBox"/><br />
                <asp:LinkButton ID="SubmitLicenseButton" runat="server" CssClass="right submitLicenseButton" OnClick="SubmitLicenseButton_Click"/>
                <asp:HyperLink ID="CancelLicenseButton" runat="server" CssClass="right cancelLicenseButton" />
                <asp:CheckBox ID="AcceptanceCheckbox" runat="server" />
                <asp:Label ID="AcceptanceLabel" runat="server" AssociatedControlID="AcceptanceCheckbox" meta:resourcekey="AcceptanceLabel"/>
                <span id="AcceptanceWarning" class="warning" style="display: none;">
                    <asp:Literal ID="MustAcceptWarningLiteral" runat="server" meta:resourcekey="MustAcceptWarningLiteral" />
                 </span>
            </div>
        </form>
</body>
</html>