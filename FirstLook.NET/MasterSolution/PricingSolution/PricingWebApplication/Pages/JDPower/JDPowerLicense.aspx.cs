using System;
using System.Web.UI;
using FirstLook.DomainModel.Oltp;

namespace FirstLook.Pricing.WebApplication.Pages.JDPower
{
    public partial class JDPowerLicense : Page
    {
        private const string CALLBACK_PARAM = "callback";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void SubmitLicenseButton_Click(object sender, EventArgs e)
        {
            if (AcceptanceCheckbox.Checked)
            {
                MustAcceptWarningLiteral.Text = User.Identity.Name;

                LicenseFacade.AcceptLicense(License.JDPowerLicenseName, User.Identity.Name);

                string returnUrl = Request.Params[CALLBACK_PARAM];
                if (returnUrl == null || returnUrl.Equals(String.Empty))
                {
                    returnUrl = "~/JDPower.aspx?" + Request.QueryString;
                }
                else
                {
                    returnUrl = returnUrl + "?" + Request.QueryString;
                }

                Response.Redirect(returnUrl, true);
            }
        }
    }
}