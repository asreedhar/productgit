<%@ Page Language="C#" AutoEventWireup="true" Inherits="FirstLook.Pricing.WebApplication.Pages.JDPower.JDPower" Codebehind="JDPower.aspx.cs" Theme="Planate" %>
<%@ Register TagPrefix="owc" Assembly="FirstLook.DomainModel.Oltp" Namespace="FirstLook.DomainModel.Oltp.WebControls"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head" runat="server">
    <title>J.D. Power</title>
</head>
<body>
    <form id="jdpowerform" runat="server">
            <owc:HedgehogTracking ID="Hedgehog" runat="server" />
            <asp:ScriptManager ID="scriptManager" runat="server" EnablePartialRendering="true" LoadScriptsBeforeUI="false" ScriptPath="/resources/Scripts">
                <Scripts>
                    <asp:ScriptReference Path="../../Public/Scripts/Pages/JDPower.js" NotifyScriptLoaded="true" />
                </Scripts>
            </asp:ScriptManager>
<div id="header">
    <h1 id="pageTitle">JD Power Market Appraiser</h1>
    <div id="dealerInfo">
        
        <h2><asp:Literal ID="DealerName" runat="server" Text="Dealer Name" /></h2>
        <h3>Franchises: </h3>
        <asp:ObjectDataSource
            ID="JDPower_Franchise_DataSource"
            TypeName="FirstLook.Pricing.DomainModel.JDPower.DataSource.JDPowerMarketTool"
            SelectMethod="SelectFranchises"
            runat="server">
            <SelectParameters>
                <owc:DealerParameter Name="dealer" Type="Object" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <asp:Repeater ID="JDFranchiseList"
            DataSourceID="JDPower_Franchise_DataSource"
            runat="server">
            <HeaderTemplate><span id="JDFranchiseList"></HeaderTemplate>
            <ItemTemplate>
            <%# Container.DataItem %>
            </ItemTemplate>
            <SeparatorTemplate>, </SeparatorTemplate>
            <FooterTemplate></span></FooterTemplate>
        </asp:Repeater>
    </div>
</div>
<div id="content">
<fieldset id="dataChoice">
    <strong class='label'>View Power Data For:</strong>
    <asp:ObjectDataSource
        ID="JDPower_FranchiseRadio_DataSource"
        TypeName="FirstLook.Pricing.DomainModel.JDPower.DataSource.JDPowerMarketTool"
        SelectMethod="SelectAvailableFranchiseGroups"
        runat="server">
        <SelectParameters>
            <owc:DealerParameter Name="dealer" Type="Object" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:RadioButtonList runat="server"
        ID="JDFranchiseRadio"
        DataSourceID="JDPower_FranchiseRadio_DataSource"
        DataTextField="Name"
        DataValueField="Value"
        AutoPostBack="false"
        RepeatLayout="Flow"
        RepeatDirection="Horizontal">
    </asp:RadioButtonList>
    
    <br />
    
    <csla:CslaDataSource 
        ID="JDPower_TimePeriod_DataSource"
        runat="server"         
        TypeName="FirstLook.JDPower.DomainModel.DataSource.JDPowerTimePeriodPanel"
        OnSelectObject="JDPowerTimePeriodPanel_Select">
    </csla:CslaDataSource>

    <asp:DropDownList
        ID="JDTimePeriod"
        DataSourceID="JDPower_TimePeriod_DataSource"
        AutoPostBack="false"
        DataTextField="Label"
        DataValueField="Id"
        runat="server">
    </asp:DropDownList>
    <asp:Button ID="PowerDataButton" runat="server" CssClass="goButton" />
</fieldset>
<div id="dataTables">

<asp:Panel id="JDPowerRegionSelectorPanel" OnLoad="JDPowerRegionSelectorPanel_Load" runat="server" CssClass="businessUnitSelector">
    <ul>
        <li><asp:LinkButton ID="PowerRegionButton" OnClick="PowerRegionButton_Click" runat="server" Text="Power JDPowerFranchise" /></li>
        <li><asp:LinkButton ID="RollUpRegionButton" OnClick="RollUpRegionButton_Click" runat="server" Text="Roll Up JDPowerFranchise" /></li>
        <li><asp:LinkButton ID="NationalRegionButton" OnClick="NationalRegionButton_Click" runat="server" Text="National" /></li>
    </ul>
</asp:Panel>

<fieldset id="reference_data">
	<legend><asp:Literal ID="JDVehicle" runat="server"/>:</legend>
	
	<asp:UpdatePanel ID="JDPowerTopSalesReportUpdatePanel" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
	<ContentTemplate>
	<asp:HiddenField ID="SelectedRegionID" runat="server" />
    <asp:ObjectDataSource
	        ID="JDPowerTopSalesReport_DataSource"
	        TypeName="FirstLook.Pricing.DomainModel.JDPower.DataSource.JDPowerSalesReportPanel"
	        SelectMethod="GetSalesReport" 
	        runat="server">
	        <SelectParameters>
	            <asp:ControlParameter ControlID="JDTimePeriod" Name="periodId" Type="Int32" ConvertEmptyStringToNull="true"/>
	            <asp:Parameter Name="minMileageBucketId" DefaultValue="1"  Type="Int32" ConvertEmptyStringToNull="true" />
	            <asp:Parameter Name="maxMileageBucketId" DefaultValue="26"  Type="Int32" ConvertEmptyStringToNull="true" />
	            <asp:ControlParameter ControlID="SelectedRegionID" Name="geographicAggregationLevel" Type="Int32" ConvertEmptyStringToNull="true"/>
	            <asp:QueryStringParameter Name="minVehicleYear" QueryStringField="modelYear" Type="Int32" ConvertEmptyStringToNull="true" />
	            <asp:QueryStringParameter Name="maxVehicleYear" QueryStringField="modelYear" Type="Int32" ConvertEmptyStringToNull="true" />
	            <asp:QueryStringParameter Name="vehicleCatalogId" QueryStringField="vehicleCatalogId" Type="Int32" ConvertEmptyStringToNull="true" />
	            <asp:ControlParameter ControlID="JDFranchiseRadio" Name="similarFranchises"  Type="Int32" ConvertEmptyStringToNull="true"/>
	            <owc:DealerParameter Name="dealer" Type="Object" />
	        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:GridView
        ID="JDPowerTopSalesReport"
        OnDataBound="JDPowerTopSalesReport_DataBound"
        CssClass="jddata"
        runat="server"
        DataSourceID="JDPowerTopSalesReport_DataSource"
        AutoGenerateColumns="False"
        UseAccessibleHeader="true"
        AllowSorting="false">
    
        <Columns>
            <asp:BoundField DataField="AverageACV" DataFormatString="{0:$#,###,###}" HeaderText="Avg. Trade-in ACV" HtmlEncode="false"  ItemStyle-CssClass="acv" HeaderStyle-CssClass="acv" />
            <asp:BoundField DataField="AverageSellingPrice" DataFormatString="{0:$#,##0}" HeaderText="Avg. Selling Price" HtmlEncode="false" />
            <asp:BoundField DataField="AverageRetailGrossProfit" DataFormatString="{0:$#,##0}" HeaderText="Avg. Retail Gross Profit" HtmlEncode="false" />
            <asp:BoundField DataField="AverageDaysToSale" DataFormatString="{0:N0}" HeaderText="Avg. Days to Sale" HtmlEncode="false" />
            <asp:BoundField DataField="AverageMileage" DataFormatString="{0:#,###,###}" HeaderText="Avg. Mileage" HtmlEncode="false" />
            <asp:BoundField DataField="GrossFandI" DataFormatString="{0:$#,##0}" HeaderText="Gross F&amp;I Products*" SortExpression="AvgGrossFIProducts" HtmlEncode="false" />
        </Columns>
    
    </asp:GridView>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="PowerRegionButton" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="RollUpRegionButton" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="NationalRegionButton" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="PowerDataButton" EventName="Click" />
    </Triggers>
    </asp:UpdatePanel>
</fieldset>

<fieldset id="filtered_data">
    <div class="filters"> 
    <fieldset><legend>Year Range</legend>
    <label>Start:
	<asp:ObjectDataSource
        ID="JDPower_ModelYear_DataSource"
        TypeName="FirstLook.Pricing.DomainModel.JDPower.DataSource.JDPowerMarketTool"
        SelectMethod="SelectModelYears"
        runat="server">
        <SelectParameters>
            <asp:QueryStringParameter Name="vehicleCatalogId" QueryStringField="vehicleCatalogId" Type="Int32"/>
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:DropDownList
        ID="JDMinYears"
        DataSourceID="JDPower_ModelYear_DataSource"
        AutoPostBack="false"
        CssClass="yearSelect"
        runat="server">
    </asp:DropDownList></label>
    
    <label>To:
    <asp:DropDownList
        ID="JDMaxYears"
        DataSourceID="JDPower_ModelYear_DataSource"
        AutoPostBack="false"
        CssClass="yearSelect"
        runat="server">
    </asp:DropDownList></label>
    </fieldset>
    
    <fieldset><legend>Mileage</legend>
    <label>Start: 
    <csla:CslaDataSource
        runat="server"
        TypeName="FirstLook.JDPower.DomainModel.DataSource.JDPowerMileageRangePanel"
        OnSelectObject="MileageRange_Select"
        ID="JDPower_MileageRange_DataSource"/>
    <asp:DropDownList
        ID="JDMinMileageRange"
        DataSourceID="JDPower_MileageRange_DataSource"
        AutoPostBack="false"
        runat="server"
        CssClass="mileageSelect"
        DataTextField="Min" DataValueField="id" >
    </asp:DropDownList></label>
    
    <label>End:
    <asp:DropDownList
        ID="JDMaxMileageRange"
        DataSourceID="JDPower_MileageRange_DataSource"
        AutoPostBack="false"
        runat="server"
        CssClass="mileageSelect"
        DataTextField="Max" DataValueField="id" >
    </asp:DropDownList></label>
    </fieldset>
    
    <asp:Button ID="SubmitFiltersButton" runat="server" CssClass="filterButton" />
    </div>
    
    <asp:UpdatePanel ID="DetailedSummaryUpdatePanel" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional" OnPreRender="DetailedSummaryUpdatePanel_PreRender">
        <ContentTemplate>
            <asp:ObjectDataSource
	            ID="JDPowerMarketTool_DetailedSummary_DataSource"
	            TypeName="FirstLook.Pricing.DomainModel.JDPower.DataSource.JDPowerSalesReportPanel"
	            SelectMethod="GetSalesReport" 
	            runat="server">
	            <SelectParameters>
	                <asp:ControlParameter ControlID="JDTimePeriod" Name="periodId" Type="Int32" ConvertEmptyStringToNull="true"/>
	                <asp:ControlParameter ControlID="JDMinMileageRange" Name="minMileageBucketId" Type="Int32" ConvertEmptyStringToNull="true"/>
	                <asp:ControlParameter ControlID="JDMaxMileageRange" Name="maxMileageBucketId" Type="Int32" ConvertEmptyStringToNull="true"/>
	                <asp:ControlParameter ControlID="SelectedRegionID" Name="geographicAggregationLevel" Type="Int32" ConvertEmptyStringToNull="true"/>
	                <asp:ControlParameter ControlID="JDMinYears"  Name="minVehicleYear" Type="Int32" ConvertEmptyStringToNull="true"/>
	                <asp:ControlParameter ControlID="JDMaxYears" Name="maxVehicleYear"  Type="Int32" ConvertEmptyStringToNull="true"/>            
	                <asp:QueryStringParameter Name="vehicleCatalogId" QueryStringField="vehicleCatalogId" Type="Int32" ConvertEmptyStringToNull="true" />
	                <asp:ControlParameter ControlID="JDFranchiseRadio" Name="similarFranchises"  Type="Int32" ConvertEmptyStringToNull="true"/>
	                <owc:DealerParameter Name="dealer" Type="Object" />
	            </SelectParameters>
            </asp:ObjectDataSource>
            
            <asp:GridView
                ID="JDPowerBottomSalesReport"
                OnDataBound="JDPowerBottomSalesReport_DataBound"
                CssClass="jddata"
                runat="server"
                DataSourceID="JDPowerMarketTool_DetailedSummary_DataSource"
                AutoGenerateColumns="False"
                UseAccessibleHeader="true"
                AllowSorting="false">
            
                <Columns>
                    <asp:BoundField DataField="AverageACV" DataFormatString="{0:$#,###,###}" NullDisplayText="-" HeaderText="Avg. Trade-in ACV" HtmlEncode="false" ItemStyle-CssClass="acv" HeaderStyle-CssClass="acv" />
                    <asp:BoundField DataField="AverageSellingPrice" DataFormatString="{0:$#,##0}" NullDisplayText="-" HeaderText="Avg. Selling Price" HtmlEncode="false" />
                    <asp:BoundField DataField="AverageRetailGrossProfit" DataFormatString="{0:$#,##0}" NullDisplayText="-" HeaderText="Avg. Retail Gross Profit" HtmlEncode="false" />
                    <asp:BoundField DataField="AverageDaysToSale" DataFormatString="{0:N0}" NullDisplayText="-" HeaderText="Avg. Days to Sale" HtmlEncode="false" />
                    <asp:BoundField DataField="AverageMileage" DataFormatString="{0:#,###,###}" NullDisplayText="-" HeaderText="Avg. Mileage" HtmlEncode="false" />
                    <asp:BoundField DataField="GrossFandI" DataFormatString="{0:$#,##0}" NullDisplayText="-" HeaderText="Gross F&amp;I Products*" SortExpression="AvgGrossFIProducts" HtmlEncode="false" />
                </Columns>
            
            </asp:GridView>
            
            <asp:Label ID="GrossFIProductsFormulaMessage" runat="server" Visible="false" meta:resourcekey="GrossFIProductsFormulaMessage" /><br />
            <asp:Label ID="NotEnoughDataMessage" CssClass="warning" EnableViewState="false" runat="server" Visible="false" meta:resourcekey="NotEnoughDataMessage" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="SubmitFiltersButton" EventName="Click" />
            
            <asp:AsyncPostBackTrigger ControlID="PowerRegionButton" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="RollUpRegionButton" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="NationalRegionButton" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="PowerDataButton" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
        
</fieldset>
</div>    
</div>

    </form>
</body>
</html>
