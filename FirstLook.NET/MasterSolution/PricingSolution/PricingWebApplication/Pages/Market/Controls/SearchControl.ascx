<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchControl.ascx.cs" Inherits="FirstLook.Pricing.WebApplication.Pages.Market.Controls.SearchControl" %>

<asp:Label ID="PreSalesWithinInSliderLabel" runat="server" Text="Sales Within" AssociatedControlID="SalesWithInDropDownList"></asp:Label>
<asp:DropDownList ID="SalesWithInDropDownList" runat="server" />
<cwc:SliderExtender ID="SalesWithInDropDownListSlider" runat="server"
        TargetControlID="SalesWithInDropDownList"
        HandleCssClass="ui-slider-handle"
        HandleImageUrl="../../Public/Images/distance_slider_handle.png"
        RailCssClass="ui-slider"
        RaiseChangeOnlyOnMouseUp="false" />
<asp:Label ID="PostSalesWithinInSliderLabel" runat="server" Text="Days" AssociatedControlID="SalesWithInDropDownList"></asp:Label>
<cwc:SliderLabelExtender ID="PostSalesWithinInSliderLabelExtender" runat="server"
        TargetControlID="SalesWithInDropDownList"
        LabelID="PostSalesWithinInSliderLabel"
        LabelFormat="{0} days" />
        
<asp:Label ID="PreWithInRadiusLabel" runat="server" Text="Within Radius" AssociatedControlID="WithInRadiusDropDownList"></asp:Label> 
<asp:DropDownList ID="WithInRadiusDropDownList" runat="server" />
<cwc:SliderExtender ID="WithInRadiusDropDownListSlider" runat="server"
        TargetControlID="WithInRadiusDropDownList"
        HandleCssClass="ui-slider-handle"
        HandleImageUrl="../../Public/Images/distance_slider_handle.png"
        RailCssClass="ui-slider"
        RaiseChangeOnlyOnMouseUp="false" />
<asp:Label ID="PostWithInRadiusLabel" runat="server" Text="miles" AssociatedControlID="WithInRadiusDropDownList" CssClass="radius_report"></asp:Label> 
<cwc:SliderLabelExtender ID="WithInRadiusSliderLabelExtender" runat="server"
        TargetControlID="WithInRadiusDropDownList"
        LabelID="PostWithInRadiusLabel"
        LabelFormat="{0} miles" />
       
<span class="button">
<asp:Button ID="UpdateSearch" runat="server" Text="Update Search" OnClick="UpdateSearchButtonClick" />
</span>