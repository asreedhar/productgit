using System;
using System.Web.UI.WebControls;
using FirstLook.Pricing.DomainModel.Market;

namespace FirstLook.Pricing.WebApplication.Pages.Market.Controls
{
    public partial class SearchControl : System.Web.UI.UserControl
    {
        #region Properties

        public int DistanceBucketId
        {
            get
            {
                return SearchRadius.Id;
            }
        }

        private static string WaitPage
        {
            get
            {
                return "~/Pages/Market/Wait.aspx";
            }
        }

        private string OwnerHandle
        {
            get
            {
                return Request.QueryString["oh"];
            }
        }

        private String InsertUser
        {
            get
            {
                return Page.User.Identity.Name;
            }
        }

        private Owner _owner;
        private Owner Owner
        {
            get
            {
                if (string.IsNullOrEmpty(OwnerHandle))
                {
                    throw new NullReferenceException("Must have an OwnerHandle");
                }

                if (_owner == null)
                {
                    Owner.DataSource dataSource = new Owner.DataSource();
                    _owner = dataSource.Select(OwnerHandle);
                }

                return _owner;
            }
        }

        private string ReportHandle
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["rh"]))
                {
                    return Request.QueryString["rh"];
                }

                throw new UriFormatException("QueryString must contain a valid ReportHandle (rh)");
            }
        }

        private ReportParameters _reportParameters;
        private ReportParameters ReportParameters
        {
            get
            {
                if (_reportParameters == null)
                {
                    ReportParameters.DataSource dataSource = new ReportParameters.DataSource();
                    _reportParameters = dataSource.Select(ReportHandle);
                }

                return _reportParameters;
            }
        }

        private BasisPeriod _basisPeriod;
        private BasisPeriod BasisPeriod
        {
            get
            {
                if (_basisPeriod == null)
                {
                    _basisPeriod = ReportParameters.BasisPeriod;
                }

                return _basisPeriod;
            }
        }

        private SearchRadius _searchRadius;
        private SearchRadius SearchRadius
        {
            get
            {
                if (_searchRadius == null)
                {
                    _searchRadius = ReportParameters.SearchRadius;
                }

                return _searchRadius;
            }
        }

        private SearchRadiusCollection _searchRadiusCollection;
        private SearchRadiusCollection SearchRadiusCollection
        {
            get
            {
                if (_searchRadiusCollection == null)
                {
                    SearchRadiusCollection.DataSource dataSource = new SearchRadiusCollection.DataSource();
                    _searchRadiusCollection = dataSource.Select();
                }

                return _searchRadiusCollection;
            }
        }

        private BasisPeriodCollection _basisPeriodCollection;
        private BasisPeriodCollection BasisPeriodCollection
        {
            get
            {
                if (_basisPeriodCollection == null)
                {
                    BasisPeriodCollection.DataSource dataSource = new BasisPeriodCollection.DataSource();
                    _basisPeriodCollection = dataSource.Select();
                }

                return _basisPeriodCollection;
            }
        } 

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.LoadComplete += BindBasisPeriodSlider;

            Page.LoadComplete += BindSearchRadiusSlider;
        }

        protected void BindBasisPeriodSlider(object sender, EventArgs e)
        {
            foreach (BasisPeriod period in BasisPeriodCollection)
            {
                SalesWithInDropDownList.Items.Add(new ListItem(period.Name, period.Id.ToString()));
            }

            SalesWithInDropDownList.SelectedValue = BasisPeriod.Id.ToString();
        }

        protected void BindSearchRadiusSlider(object sender, EventArgs e)
        {
            foreach (SearchRadius radius in SearchRadiusCollection)
            {
                WithInRadiusDropDownList.Items.Add(new ListItem(radius.Name, radius.Id.ToString()));
            }

            WithInRadiusDropDownList.SelectedValue = SearchRadius.Id.ToString();
        }



        protected void UpdateSearchButtonClick(object sender, EventArgs e)
        {
            string reportHandle = RequestReport.Execute(Owner,
                                                        BasisPeriodCollection[SalesWithInDropDownList.SelectedIndex],
                                                        SearchRadiusCollection[WithInRadiusDropDownList.SelectedIndex],
                                                        InsertUser);

            bool ready = ReportStatus.Execute(OwnerHandle, reportHandle);

            string redirectPage = ready ? Request.Path : WaitPage;

            Response.Redirect(string.Format("{0}?oh={1}&rh={2}", redirectPage, OwnerHandle, reportHandle), true);
        }
    }
}