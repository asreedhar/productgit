using System;
using System.Data;
using FirstLook.DomainModel.Oltp;
using FirstLook.Pricing.DomainModel.Internet.DataSource;

namespace FirstLook.Pricing.WebApplication.Pages.Market
{
    public partial class Market : System.Web.UI.MasterPage
    {
        #region Properties
        private string OwnerHandle
        {
            get
            {
                return Request.QueryString["oh"];
            }
        }

        public bool IsSalesTool
        {
            get
            {
                return (Context.Items[BusinessUnit.HttpContextKey] == null);
            }
        }

        private string _dealerNameText; 
        #endregion

        protected void Page_Init(object sender, EventArgs e)
        {
            DataTable ownerNameTable = new OwnerNameDataSource().FindByOwnerHandle(OwnerHandle);

            foreach (DataRow row in ownerNameTable.Rows)
            {
                _dealerNameText = Convert.ToString(row["OwnerName"]);
            }

            if (IsSalesTool)
            {
                FlashLocateLinkList.Visible = false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DealerNameLiteral.Text = _dealerNameText;
        }

        
    }
}
