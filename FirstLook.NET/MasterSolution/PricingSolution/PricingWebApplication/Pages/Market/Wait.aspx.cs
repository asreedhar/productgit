using System;

using System.Web.UI.HtmlControls;
using FirstLook.Pricing.DomainModel.Market;

namespace FirstLook.Pricing.WebApplication.Pages.Market
{
    public partial class Wait : System.Web.UI.Page
    {
        #region Properties

        protected static string WaitText
        {
            get { return "ANALYZING INTERNET VELOCITY"; }
        }

        private static string ReportPage
        {
            get { return "~/Pages/Market/Report.aspx"; }
        }

        private string OwnerHandle
        {
            get { return Request.QueryString["oh"]; }
        }

        private string ReportHandle
        {
            get { return Request.QueryString["rh"]; }
        }

        #endregion

        protected void Page_Init(object sender, EventArgs e)
        {
            Page.Title = "Please wait while your report is generating...";

            HtmlMeta metaRefresh = new HtmlMeta();
            metaRefresh.HttpEquiv = "refresh";
            metaRefresh.Content = "15";

            HtmlMeta metaEnter = new HtmlMeta();
            metaEnter.HttpEquiv = "Page-Enter";
            metaEnter.Content = "progid:DXImageTransform.Microsoft.Fade(Duration=0)";

            HtmlMeta metaExit = new HtmlMeta();
            metaExit.HttpEquiv = "Page-Exit";
            metaExit.Content = "progid:DXImageTransform.Microsoft.Fade(Duration=0)";

            Header.Controls.Add(metaRefresh);
            Header.Controls.Add(metaEnter);
            Header.Controls.Add(metaExit);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            bool ready = ReportStatus.Execute(OwnerHandle, ReportHandle);

            if (ready)
            {
                string url = string.Format("{0}?oh={1}&rh={2}", ReportPage, OwnerHandle, ReportHandle);

                Response.Redirect(url, true);
            }
        }

    }
}