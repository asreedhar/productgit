<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Wait.aspx.cs" Inherits="FirstLook.Pricing.WebApplication.Pages.Market.Wait" MasterPageFile="~/Pages/Market/Market.Master" Theme="Ectype" %>

<asp:Content ContentPlaceHolderID="ControlsContentPlaceHolder" ID="ControlsContent" runat="server">

<div id="please_wait">
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="550" height="400">
    <param name="movie" value="../../Public/Media/Waiting.swf" />
    <param name="quality" value="high" />
    <param name="allowScriptAccess" value="sameDomain" />
    <param name="FlashVars" value="loadingText=<%= WaitText %>" />
    <embed src="../../Public/Media/Waiting.swf" 
        type="application/x-shockwave-flash" 
        allowScriptAccess="sameDomain"
        quality="high" 
        FlashVars="loadingText=<%= WaitText %>" />
</object>
</div>

</asp:Content>