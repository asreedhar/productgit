<%@ Page Language="C#" AutoEventWireup="true" Codebehind="Search.aspx.cs" Inherits="FirstLook.Pricing.WebApplication.Pages.Market.Search" Theme="Leopard"  %>

<%@ OutputCache Location="None" VaryByParam="None" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Market Velocity Stocking Guide - Market Listings</title>
</head>
<body>
    <form id="vpa" class="vpa_modify_search" runat="server">
        <owc:HedgehogTracking ID="Hedgehog" runat="server" />
        <asp:ScriptManager ID="SearchScriptManager" runat="server"
                EnablePartialRendering="true" 
                LoadScriptsBeforeUI="false"
                ScriptPath="/resources/Scripts">
            <Scripts>
                <asp:ScriptReference Path="~/Public/Scripts/Lib/Combined.js" NotifyScriptLoaded="true" />
                <asp:ScriptReference Path="~/Public/Scripts/Lib/DD_Roundies.js" NotifyScriptLoaded="true" />
                <asp:ScriptReference Path="~/Public/Scripts/Pages/global.js" NotifyScriptLoaded="true" />
                <asp:ScriptReference Path="~/Public/Scripts/Pages/ModifySearch.js" NotifyScriptLoaded="true" />
            </Scripts>
        </asp:ScriptManager>
        <owc:CatalogDataSource ID="ReferenceDataSource" runat="server">
            <SelectParameters>
                <asp:QueryStringParameter QueryStringField="my" Name="ModelYearId" Type="Int32" />
                <asp:QueryStringParameter QueryStringField="ma" Name="MakeId" Type="Int32" />
                <asp:QueryStringParameter QueryStringField="li" Name="LineId" Type="Int32" />
            </SelectParameters>
        </owc:CatalogDataSource>
        <asp:ObjectDataSource
                ID="MarketListingObjectDataSource"
                runat="server"
                TypeName="FirstLook.Pricing.DomainModel.Internet.DataSource.MarketListingDataSource"
                SelectMethod="FindByOwnerHandleAndParameters"
                EnableCaching="true"
                CacheDuration="60"
                CacheExpirationPolicy="Sliding"
                FilterExpression="ModelConfigurationID IN ({0})"
                SortParameterName="sortColumns"
                OnSelected="MarketListingObjectDataSourceSelected">
             <SelectParameters>
                <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
                <asp:QueryStringParameter QueryStringField="my" Name="ModelYear" Type="Int32" />
                <asp:QueryStringParameter QueryStringField="ma" Name="MakeId" Type="Int32" />
                <asp:QueryStringParameter QueryStringField="li" Name="LineId" Type="Int32" />
                <asp:QueryStringParameter QueryStringField="db" Name="DistanceBucketId" Type="Int32" />
             </SelectParameters>
             <FilterParameters>
                <asp:ControlParameter ControlID="CatalogFacetDv" PropertyName="ModelConfigurationFilter" DefaultValue="-1" Type="string" />
             </FilterParameters>
        </asp:ObjectDataSource>
        <div id="wrap" class="clearfix">
            <div id="hd">
                <h1><asp:Label ID="VehicleDescriptionLabel"  runat="server" Text="{YYYY Make Model Family (Series)}" /></h1>
	        </div>
	        <div id="search_wrapper" class="clearfix">
	            <div id="search_controls">
	                <asp:UpdatePanel ID="SearchUpdatePanel" runat="server">
    	                <ContentTemplate>    	                
                            <owc:CatalogFacetDataView ID="CatalogFacetDv" runat="server"
                                    CssClass="catalog_data_view"
                                    DataSourceID="ReferenceDataSource"
                                    FlyoutPreviewButtonText="Update"
                                    OnCatalogDataBound="CatalogDvCatalogDataBound" />
    	                </ContentTemplate>
    	            </asp:UpdatePanel>
                </div>
                <div id="results_feedback">
	                <asp:UpdatePanel runat="server" ID="CatalogUpdatePanel">
	                    <ContentTemplate>
	                        <div class="market_overview content_group clearfix">
	                            <div class="header">
	                                <h3>Market Summary</h3>
	                                <p class="text_summary">
	                                    <strong><asp:Literal runat="server" ID="SummaryNumberOfResultsLiteral" Text="Unknown" /></strong>
	                                    precision listings out of 
	                                    <em><asp:Literal runat="server" ID="SummaryNumberOfOverallResultsLiteral" Text="Unknown" /></em>
	                                    overall results
	                                </p>
	                                <pwc:MarketSummaryTable 
	                                    ID="MarketSummaryPricingTable"
	                                    CssClass="pricing_summary_table"
	                                    runat="server"
	                                    DataSourceID="MarketListingObjectDataSource"
	                                    ColumnName="Market Pricing"	                       
	                                    ValueFormat="{0:$#,0;($#,0),--}"
	                                    ShowReference="false"/>
                                    <pwc:MarketSummaryTable 
                                        ID="MarketSummaryMileageTable"
                                        CssClass="mileage_summary_table"
                                        runat="server"
                                        DataSourceID="MarketListingObjectDataSource"
                                        ColumnName="Market Mileage"
                                        ValueFormat="{0:#,0;($#,0);--}" />
                                </div>
                            </div>
                            <h3>Market Listings</h3>
                            <asp:GridView
					                ID="MarketListingGridView"
					                runat="server"
					                DataSourceID="MarketListingObjectDataSource"
					                Width="720px"
					                PageSize="25"
					                AlternatingRowStyle-CssClass="odd"
					                AutoGenerateColumns="false"
					                AllowPaging="true"
					                AllowSorting="true"
					                CssClass="grid small"
					                EnableViewState="false"
					                OnRowDataBound="MarketListingGridViewRowDataBound">
				                <PagerStyle CssClass="gridPager" />
				                <Columns>
					                <asp:BoundField
						                DataField="Age"
						                HeaderText="Age"
						                SortExpression="Age" />
					                <asp:BoundField
					                    ItemStyle-CssClass="seller"
					                    HtmlEncode="false"
					                    DataField="Seller"
					                    HeaderText="Seller"
					                    SortExpression="Seller" />
					                <asp:BoundField
					                    ItemStyle-CssClass="vehicle_description"
						                DataField="VehicleDescription"
						                HtmlEncode="false"
						                HeaderText="Vehicle<br/> Information"
						                SortExpression="VehicleDescription" />
					                <asp:BoundField
						                DataField="ListingCertified"
						                HeaderText="Certified"
						                HtmlEncode="false"
						                SortExpression="ListingCertified" />
					                <asp:BoundField
						                DataField="VehicleColor"
						                HeaderText="Color"
						                SortExpression="VehicleColor" />
					                <asp:BoundField
						                DataField="VehicleMileage"
						                DataFormatString="{0:#,###,###;--;--}"
						                HeaderText="Mileage"
						                SortExpression="VehicleMileage"
						                HtmlEncode="False" />
					                <asp:BoundField
						                DataField="ListPrice"
						                DataFormatString="{0:$#,##0;--;--}"
						                HeaderText="Internet<br/> Price"
						                SortExpression="ListPrice"
						                ConvertEmptyStringToNull="true"
						                NullDisplayText="--"
						                HtmlEncode="False" />
					                <asp:BoundField
						                DataField="PctAvgMarketPrice"
						                HeaderText="%&nbsp;Market<br/> Avg."
						                DataFormatString="{0:#' %';--;--}"
						                HtmlEncode="false"
						                SortExpression="PctAvgMarketPrice" />
					                 <asp:BoundField
						                DataField="DistanceFromDealer"
						                HeaderText="Distance"
						                DataFormatString="{0:#### mi;--;--}"
						                HtmlEncode="false"
						                SortExpression="DistanceFromDealer" />					
				                </Columns>
			                </asp:GridView>
	                    </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <cwc:PageFooter ID="SiteFooter" runat="server" ShowChromeCopyright="True" />
    </form>
</body>
</html>
