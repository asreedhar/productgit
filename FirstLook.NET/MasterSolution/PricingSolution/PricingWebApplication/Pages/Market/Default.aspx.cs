using System;
using FirstLook.Pricing.DomainModel.Market;

namespace FirstLook.Pricing.WebApplication.Pages.Market
{
    public partial class Default : System.Web.UI.Page
    {
        #region Properties

        private const string ReportPage = "~/Pages/Market/Report.aspx";

        private const string WaitPage = "~/Pages/Market/Wait.aspx";

        private const string DefaultBasisPeriod = "90";

        private const string DefaultSearchRadius = "100";

        private string OwnerHandle
        {
            get
            {
                return Request.QueryString["oh"];
            }
        }

        private Owner _owner;

        private Owner Owner
        {
            get
            {
                if (string.IsNullOrEmpty(OwnerHandle))
                {
                    throw new NullReferenceException("Must have an OwnerHandle");
                }

                if (_owner == null)
                {
                    _owner = Owner.GetOwner(OwnerHandle);
                }

                return _owner;
            }
        }

        private BasisPeriod _basisPeriod;

        private BasisPeriod BasisPeriod
        {
            get
            {
                if (_basisPeriod == null)
                {
                    BasisPeriodCollection basisPeriodCollection = BasisPeriodCollection.GetBasisPeriods();

                    foreach (BasisPeriod period in basisPeriodCollection)
                    {
                        if (period.Name == DefaultBasisPeriod)
                        {
                            _basisPeriod = period;
                        }
                    }
                    
                    if (_basisPeriod == null)
                    {
                        throw new IndexOutOfRangeException(string.Format("BasisPeriodCollection must contain a default value of {0}", DefaultBasisPeriod));
                    }
                }

                return _basisPeriod;
            }
        }

        private SearchRadius _searchRadius;

        private SearchRadius SearchRadius
        {
            get
            {
                if (_searchRadius == null)
                {
                    SearchRadiusCollection searchRadiusCollection = SearchRadiusCollection.GetSearchRadiuses();

                    foreach (SearchRadius radius in searchRadiusCollection)
                    {
                        if (radius.Name == DefaultSearchRadius)
                        {
                            _searchRadius = radius;
                        }
                    }

                    if (_searchRadius == null)
                    {
                        throw new IndexOutOfRangeException(
                            string.Format("SearchRadiusCollection must contain a default value of {0}",
                                          DefaultSearchRadius));
                    }
                }

                return _searchRadius;
            }
        } 

        private String InsertUser
        {
            get
            {
                return Page.User.Identity.Name;
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            string reportHandle = RequestReport.Execute(Owner, BasisPeriod, SearchRadius, InsertUser);

            bool ready = ReportStatus.Execute(OwnerHandle, reportHandle);

            string redirectPage = ready ? ReportPage : WaitPage;

            Response.Redirect(string.Format("{0}?oh={1}&rh={2}", redirectPage, OwnerHandle, reportHandle), true);
        }
    }
}
