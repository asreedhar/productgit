<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="List.aspx.cs" Inherits="FirstLook.Pricing.WebApplication.Pages.Market.List" MasterPageFile="~/Pages/Market/Market.Master" Theme="Ectype" %>

<%@ Register Src="~/Pages/Market/Controls/SearchControl.ascx" TagPrefix="Controls" TagName="SearchControl" %>

<asp:Content ContentPlaceHolderID="ControlsContentPlaceHolder" ID="ControlsContent" runat="server">

<div id="search_controls">
    <Controls:SearchControl ID="SearchControl" runat="server" />
</div>
    
<div id="navigation_controls">
    <ul class="tabs">
        <li class="market">
            <asp:HyperLink ID="ReportHyperLink" runat="server" OnLoad="ReportHyperLinkLoad"
                NavigateUrl="~/Pages/Market/Report.aspx" Text="Market" />
        </li>
        <li class="shopping_list active">
            <asp:HyperLink ID="ListHyperLink" runat="server" OnLoad="ListHyperLinkLoad"
                NavigateUrl="~/Pages/Market/List.aspx" Text="Shopping List" />
        </li>
    </ul>
</div>

<asp:UpdatePanel ID="ListUpdatePanel" runat="server" 
        ChildrenAsTriggers="true">
    <ContentTemplate>

        <asp:ObjectDataSource ID="PurchaseListDataSource" runat="server"
                TypeName="FirstLook.Pricing.DomainModel.Market.PurchaseList+DataSource"
                SelectMethod="Select"
                UpdateMethod="Update"
                EnableCaching="true"
                CacheExpirationPolicy="Sliding"
                CacheDuration="300"
                CacheKeyDependency="PurchaseListDataSourceCacheKey">
            <SelectParameters>
                <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
                <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="reportHandle" Type="String" QueryStringField="rh" />
            </SelectParameters>
            <UpdateParameters>
                <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
                <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="reportHandle" Type="String" QueryStringField="rh" />
            </UpdateParameters>
        </asp:ObjectDataSource>

        <pwc:PurchaseListTable ID="PurchaseListTable" runat="server"
                AllowSorting="true"
                OpenSearchAsPopUp="true"
                OpenFlashLocateAsPopup="true"
                DataSourceID="PurchaseListDataSource"
                ShowUnitsSold="false" />
        
        <p class="row_controls">
            <span class="button">
                <asp:Button ID="PurchaseListTableClearAllButton" runat="server"
                        Text="Clear All"
                        OnClick="PurchaseListTableClearAllButton_Click" />
            </span>
        </p>
        
        <p class="save_controls">                    
            <span class="button">
                <asp:Button ID="PurchaseListTableResetButton" runat="server"
                        Text="cancel"
                        OnClick="PurchaseListTableResetButtonClick" />
            </span>
                    
            <span class="button">
                <asp:Button ID="PurchaseListTableUpdateButton" runat="server"
                        Text="Save Changes to Purchase List"
                        OnClick="PurchaseListTableUpdateButtonClick" />
            </span>
        </p>
                
    </ContentTemplate>
</asp:UpdatePanel>

<p class="print_controls">
    <span class="button">
        <asp:Button runat="server" ID="PrintButton" OnClick="PrintButton_Click" Text="Print Purchase List" />
    </span>
</p>

</asp:Content>
