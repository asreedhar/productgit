<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Report.aspx.cs" Inherits="FirstLook.Pricing.WebApplication.Pages.Market.Report" MasterPageFile="~/Pages/Market/Market.Master" Theme="Ectype" %>

<%@ OutputCache Location="None" VaryByParam="None" %>

<%@ Register Src="~/Pages/Market/Controls/SearchControl.ascx" TagPrefix="Controls" TagName="SearchControl" %>

<asp:Content ContentPlaceHolderID="ControlsContentPlaceHolder" ID="ControlsContent" runat="server">

<div id="search_controls">
    <Controls:SearchControl ID="SearchControl" runat="server" />
</div>
  
<asp:UpdatePanel ID="ReportUpdatePanel" runat="server" 
        ChildrenAsTriggers="true" UpdateMode="Conditional">
    <ContentTemplate>
    
    <div id="navigation_controls">
        <ul class="tabs">
            <li class="market active">
                <asp:HyperLink ID="ReportHyperLink" runat="server" OnLoad="ReportHyperLinkLoad"
                    NavigateUrl="~/Pages/Market/Report.aspx" Text="Market" EnableViewState="false" />
            </li>
            <li class="shopping_list shopping_list_inactive">
                <asp:HyperLink ID="ListHyperLink" runat="server" OnLoad="ListHyperLinkLoad"
                    NavigateUrl="~/Pages/Market/List.aspx" Text="Shopping List" EnableViewState="false" />
            </li>
        </ul>
    </div>
    
    <asp:ObjectDataSource id="MarketReportDataSource" runat="server"
            TypeName="FirstLook.Pricing.DomainModel.Market.Report+DataSource"
            SelectMethod="Select"
            EnableCaching="true"
            CacheDuration="300"
            CacheExpirationPolicy="Sliding"
            CacheKeyDependency="MarketReportDataSourceCacheKey">
        <SelectParameters>
            <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
            <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="reportHandle" Type="String" QueryStringField="rh" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <pwc:MarketReportTable ID="MarketReportTable" runat="server"
            DataSourceID="MarketReportDataSource"
            AllowPaging="true"
            AllowSorting="true"
            AllowFiltering="true"
            ShowUnitsSold="false"
            PageSize="20"
            OpenSearchAsPopUp="true"
            OnRowQuantityChanged="MarketReportTableRowQuantityChanged" />
                
    </ContentTemplate>
</asp:UpdatePanel>

<p class="print_controls">
    <span class="button">
        <asp:Button runat="server" ID="PrintButton" OnClick="PrintButton_Click" Text="Print Market Stocking Guide" />
    </span>
</p>

<cwc:Dialog ID="PrintSelectionDialog" runat="server"
        AllowDrag="true"
        AllowResize="true"
        TitleText="Choose your print Market Report"
        Width="400"
        Height="150"
        Top="450"
        Left="450"
        Visible="false"
        CssClass="print_dialog"
        OnButtonClick="PrintSelectionDialogClick">
    <ItemTemplate>
        <p>
        <span class="button">
            <asp:Button runat="server" ID="PrintTopVehicleButton" Text="Print Top 25 Year Make Models" OnClick="PrintTopVehicleButtonClick" />
        </span>
        <span class="button">
            <asp:Button runat="server" ID="PrintSegmentButton" Text="Print Top 25 By Segment" OnClick="PrintSegmentButtonClick" />
        </span>
        <span class="button">
            <asp:Button runat="server" ID="PrintCurrentButton" Text="Print Current Page" OnClick="PrintCurrentButtonClick" />
        </span>
        </p>
    </ItemTemplate>
    <Buttons>
        <cwc:DialogButton ButtonCommand="Cancel" IsCancel="true" />
    </Buttons>
</cwc:Dialog>
        
</asp:Content>