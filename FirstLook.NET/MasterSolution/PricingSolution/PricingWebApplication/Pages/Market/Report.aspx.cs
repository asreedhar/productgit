using System;
using System.Collections;
using FirstLook.Reports.GoogleAnalytics;
using System.Collections.Generic;
using FirstLook.Common.WebControls.UI;
using FirstLook.Pricing.WebControls;
using FirstLook.Pricing.DomainModel.Market;

namespace FirstLook.Pricing.WebApplication.Pages.Market
{
    public partial class Report : System.Web.UI.Page, IGAnalytics
    {
        #region Properties

        private string OwnerHandle
        {
            get
            {
                return Request.QueryString["oh"];
            }
        }

        private string ReportHandle
        {
            get
            {
                return Request.QueryString["rh"];
            }
        }

        private string ChangeUser
        {
            get
            {
                return Page.User.Identity.Name;
            }
        }

        private bool IsDebug
        {
            get
            {
                return Request.QueryString["debug"] == "please";
            }
        }

        private bool? _hasPurchaseList;

        private bool HasPurchaseList
        {
            get
            {
                if (!_hasPurchaseList.HasValue)
                {
                    _hasPurchaseList = PurchaseListExists.Execute(OwnerHandle);
                }
                return _hasPurchaseList.Value;
            }
        }

        #endregion

        protected void Page_PreInit(object sender, EventArgs e)
        {
            IsAnalyicsEnabled = true;
            this.PageTitle = "Market Velocity Stocking Guide";
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (IsAnalyicsEnabled)
            {
                Page.GetGoogleAnalyticsScript();
            }
            
            Form.Attributes["class"] = "report";

            if (IsDebug)
            {
                MarketReportTable.ShowUnitsSold = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Page.Cache["MarketReportDataSourceCacheKey"] = DateTime.Today;
            }

            MarketReportTable.DistanceBucketID = SearchControl.DistanceBucketId;
        }

        protected void MarketReportTableRowQuantityChanged(object sender, MarketReportTableRowQuantityChangedEventArgs e)
        {
            UpdatePurchaseListItem.Execute(OwnerHandle, e.ReportItem.ModelYear, e.ReportItem.ModelFamilyId,
                                           e.ReportItem.SegmentId, e.ReportItem.ModelConfigurationId, e.Quantity,
                                           e.Notes, ChangeUser);

            ListHyperLink.Enabled = true;

            ClearMarketReportDataSourceCache();
        }

        protected void ListHyperLinkLoad(object sender, EventArgs e)
        {
            ListHyperLink.Enabled = HasPurchaseList;

            ListHyperLink.NavigateUrl += string.Format("?oh={0}&rh={1}", OwnerHandle, ReportHandle);
        }

        protected void ReportHyperLinkLoad(object sender, EventArgs e)
        {
            ReportHyperLink.NavigateUrl += string.Format("?oh={0}&rh={1}", OwnerHandle, ReportHandle);
        }

        protected void PrintButton_Click(object sender, EventArgs e)
        {
            PrintSelectionDialog.Visible = true;
        }

        protected void PrintTopVehicleButtonClick(object sender, EventArgs e)
        {
            PrintReportBuilder builder = GetPrintReportBuilder();

            byte[] document = builder.BuildTopVehiclesReport();

            SendPdfResponse(document);
        }        

        protected void PrintSegmentButtonClick(object sender, EventArgs e)
        {
            PrintReportBuilder builder = GetPrintReportBuilder();

            byte[] document = builder.BuildSegmentsReport();

            SendPdfResponse(document);
        }

        protected void PrintCurrentButtonClick(object sender, EventArgs e)
        {
            PrintReportBuilder builder = GetPrintReportBuilder();

            builder.PageSize = MarketReportTable.PageSize;
            builder.PageIndex = MarketReportTable.PageIndex;
            builder.SortDirection = MarketReportTable.SortDirection;
            builder.SortExpression = MarketReportTable.SortExpression;
            builder.ExpandedReportItems = MarketReportTable.ExpandedReportItems;

            byte[] document = builder.BuildFilteredReport();

            SendPdfResponse(document);
        }

        protected void PrintSelectionDialogClick(object sender, DialogButtonClickEventArgs e)
        {
            PrintSelectionDialog.Visible = false;
        }

        private PrintReportBuilder GetPrintReportBuilder()
        {
            string[] styleSheets = new string[]
                                       {
                                           string.Format("App_Themes/{0}/{1}",
                                                         Page.Theme, "boilerplate.css"),
                                           string.Format("App_Themes/{0}/{1}",
                                                         Page.Theme, "prince_xml.css")
                                       };

            IEnumerable data = MarketReportDataSource.Select();

            PrintReportBuilder builder = new PrintReportBuilder(
                Owner.GetOwner(OwnerHandle),
                (ReportItemCollection)data,
                ReportParameters.GetReportParameters(ReportHandle),
                MarketReportTable.Filters,
                styleSheets
                );

            builder.ShowUnitsSold = MarketReportTable.ShowUnitsSold;
            return builder;
        }

        private void SendPdfResponse(byte[] document)
        {
            Response.Clear();
            Response.ClearHeaders();
            Response.AddHeader("Content-Disposition", string.Format("attachment; filename=Report-{0:MM-dd-yy}.pdf", DateTime.Today));
            Response.AddHeader("Content-Length", document.Length.ToString());
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(document);
            Response.End();
        }

        protected void ClearMarketReportDataSourceCache()
        {
            Page.Cache.Remove("MarketReportDataSourceCacheKey");

            Page.Cache["MarketReportDataSourceCacheKey"] = DateTime.Today;
        }

        #region IGAnalytics Members

        public string PageTitle
        {
            get;
            set;
        }

        public bool IsAnalyicsEnabled
        {
            get;
            set;
        }

        #endregion
    }
}
