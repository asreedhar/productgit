using System;
using System.Configuration;
using System.Web.UI;
using FirstLook.DomainModel.Oltp;
using FirstLook.Pricing.DomainModel.Market;
using Owner=FirstLook.Pricing.DomainModel.Market.Owner;

namespace FirstLook.Pricing.WebApplication.Pages.Market
{
    public partial class List : Page
    {
        #region Properties

        private string OwnerHandle
        {
            get
            {
                return Request.QueryString["oh"];
            }
        }

        private string ReportHandle
        {
            get
            {
                return Request.QueryString["rh"];
            }
        }

        public bool IsSalesTool
        {
            get
            {
                return (Context.Items[BusinessUnit.HttpContextKey] == null);
            }
        }

        #endregion

        protected void Page_Init(object sender, EventArgs e)
        {
            Page.Title = "Market Velocity Stocking Guide: Purchase List";
            Form.Attributes["class"] = "list";

            if (IsSalesTool)
            {
                PurchaseListTable.ShowFlashLocate = false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Cache["PurchaseListDataSourceCacheKey"] = DateTime.Today;
            }

            PurchaseListTable.DistanceBucketID = SearchControl.DistanceBucketId;
        }

        protected void ListHyperLinkLoad(object sender, EventArgs e)
        {
            ListHyperLink.NavigateUrl += string.Format("?oh={0}&rh={1}", OwnerHandle, ReportHandle);
        }

        protected void ReportHyperLinkLoad(object sender, EventArgs e)
        {
            ReportHyperLink.NavigateUrl += string.Format("?oh={0}&rh={1}", OwnerHandle, ReportHandle);
        }

        protected void PurchaseListTableUpdateButtonClick(object sender, EventArgs e)
        {
            PurchaseListTable.Update();

            ClearPurchaseListDataSourceCache();
        }

        protected void PurchaseListTableClearAllButton_Click(object sender, EventArgs e)
        {
            PurchaseListTable.ClearAllItems();

            ClearPurchaseListDataSourceCache();
        }

        protected void PrintButton_Click(object sender, EventArgs e)
        {
            string[] styleSheets = new string[]
                                       {
                                           string.Format("{0}/App_Themes/{1}/{2}",
                                                         ConfigurationManager.AppSettings["prince_xml_assests_host"],
                                                         Page.Theme, "boilerplate.css"),
                                           string.Format("{0}/App_Themes/{1}/{2}",
                                                         ConfigurationManager.AppSettings["prince_xml_assests_host"],
                                                         Page.Theme, "prince_xml.css")
                                       };


            PrintPurchaseListBuilder builder = new PrintPurchaseListBuilder(
                Owner.GetOwner(OwnerHandle),
                PurchaseList.GetPurchaseList(OwnerHandle, ReportHandle),
                ReportParameters.GetReportParameters(ReportHandle),
                styleSheets);

            builder.ShowUnitsSold = PurchaseListTable.ShowUnitsSold;

            byte[] document = builder.Build();

            Response.Clear();
            Response.ClearHeaders();
            Response.AddHeader("Content-Disposition", "attachment; filename=PurchaseList.pdf");
            Response.AddHeader("Content-Length", document.Length.ToString());
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(document);
            Response.End();
        }

        protected void PurchaseListTableResetButtonClick(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("{0}?oh={1}&rh={2}", Request.Path, OwnerHandle, ReportHandle));
        }

        protected void ClearPurchaseListDataSourceCache()
        {
            Cache.Remove("PurchaseListDataSourceCacheKey");

            Cache["PurchaseListDataSourceCacheKey"] = DateTime.Today;
        }
    }
}
