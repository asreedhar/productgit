using System;
using System.Data;
using System.Globalization;
using System.Web.Caching;
using System.Web.UI.WebControls;
using FirstLook.DomainModel.Lexicon.Categorization;
using FirstLook.DomainModel.Lexicon.Categorization.WebControls;

namespace FirstLook.Pricing.WebApplication.Pages.Market
{
    public partial class Search : System.Web.UI.Page
    {
        private string OwnerHandle
        {
            get { return Request.QueryString["oh"]; }
        }

        private string ReportHandle
        {
            get { return Request.QueryString["rh"]; }
        }

        private string GenerateCacheKey()
        {
            return OwnerHandle + ":" +
                   ReportHandle + ":" +
                   Request.QueryString["my"] + ":" +
                   Request.QueryString["ma"] + ":" +
                   Request.QueryString["li"] + ":" +
                   ":OverallSearchCount";
        }

        private int? OverallSearchCount
        {
            get { return (int?) Cache[GenerateCacheKey()]; }
            set
            {
                Cache.Insert(
                    GenerateCacheKey(),
                    value,
                    null,
                    Cache.NoAbsoluteExpiration,
                    new TimeSpan(0, 0, MarketListingObjectDataSource.CacheDuration));
            }
        }

        private int SearchCount
        {
            get
            {
                return MarketSummaryPricingTable.NumberOfListings;
            }            
        }

        private int _modelYear;
        private int _makeId;
        private int _lineId;

        public int ModelYear
        {
            get
            {
                if (_modelYear == 0)
                {
                    _modelYear = int.Parse(Request.QueryString["my"]);
                }

                return _modelYear;
            }
        }

        public int MakeId
        {
            get
            {
                if (_makeId == 0)
                {
                    _makeId = int.Parse(Request.QueryString["ma"]);
                }

                return _makeId;
            }
        }

        public int LineId
        {
            get
            {
                if (_lineId == 0)
                {
                    _lineId = int.Parse(Request.QueryString["li"]);
                }

                return _lineId;
            }
        }

        private int _modelConfigurationId;
        private int _modelFamilyId;
        private int _segmentId;

        protected int ModelConfigurationId
        {
            get
            {
                if (_modelConfigurationId == 0)
                {
                    string text = Request.QueryString["mc"];

                    int.TryParse(text, out _modelConfigurationId);
                }

                return _modelConfigurationId;
            }
        }

        protected bool HasModelConfigurationId
        {
            get
            {
                string text = Request.QueryString["mc"];

                return !string.IsNullOrEmpty(text) && !Equals("0", text);
            }
        }

        public int ModelFamilyId
        {
            get
            {
                if (_modelFamilyId == 0)
                {
                    string text = Request.QueryString["mf"];

                    int.TryParse(text, out _modelFamilyId);
                }

                return _modelFamilyId;
            }
        }

        protected bool HasModelFamilyId
        {
            get
            {
                return !string.IsNullOrEmpty(Request.QueryString["mf"]);
            }
        }

        public int SegmentId
        {
            get
            {
                if (_segmentId == 0)
                {
                    string text = Request.QueryString["se"];

                    int.TryParse(text, out _segmentId);
                }

                return _segmentId;
            }
        }

        protected bool HasSegmentId
        {
            get
            {
                return !string.IsNullOrEmpty(Request.QueryString["se"]);
            }
        }

        private Catalog _catalog;

        public Catalog Catalog
        {
            get
            {
                if (_catalog == null)
                {
                    _catalog = Catalog.GetCatalog(ModelYear, MakeId, LineId);
                }

                return _catalog;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            Page.PreRenderComplete += Page_PreRenderComplete;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CatalogFacetDv.VinPattern = Request.QueryString["VinPattern"];

                if (HasModelConfigurationId)
                {
                    CatalogFacetDv.ModelConfigurationId = ModelConfigurationId;
                }

                VehicleDescriptionLabel.Text = string.Format(
                    CultureInfo.InvariantCulture,
                    "{0} {1} {2} {3}",
                    ModelYear,
                    Catalog.Make.Name,
                    Catalog.ModelFamilies.GetItem(ModelFamilyId).Name,
                    Catalog.Segments.GetItem(SegmentId).Name);
            }
        }

        private void Page_PreRenderComplete(object sender, EventArgs e)
        {
            UpdatePageCounts();
        }

        protected void MarketListingObjectDataSourceSelected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue != null)
            {
                DataTable table = e.ReturnValue as DataTable;

                if (table != null)
                {
                    OverallSearchCount = table.Rows.Count;
                }
            }
        }

        protected void CatalogDvCatalogDataBound(object sender, CatalogDataBoundEventArgs e)
        {
            bool selected, target = false;

            if (HasModelConfigurationId)
            {
                selected = ModelConfigurationId == e.ModelConfigurationId;

                target = e.ModelConfigurationId == ModelConfigurationId;
            }
            else
            {
                ModelConfiguration configuration = Catalog.ModelConfigurations.GetItem(e.ModelConfigurationId);

                selected = (configuration.Model.ModelFamily.Id == ModelFamilyId &&
                             configuration.Model.Segment.Id == SegmentId);
            }

            e.Selected = selected;

            e.Target = target;
        }

        protected void UpdatePageCounts()
        {
            SummaryNumberOfResultsLiteral.Text = string.Format("{0:d}", SearchCount);

            if (OverallSearchCount.HasValue)
            {
                SummaryNumberOfOverallResultsLiteral.Text = string.Format("{0:d}", OverallSearchCount);
            }
        }

        protected void MarketListingGridViewRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[3].CssClass = (e.Row.Cells[3].Text == "False") ? "not_certified" : "certified";

                e.Row.Cells[1].Text = PreventTextWidows(e.Row.Cells[1].Text);
            }
        }

        protected string PreventTextWidows(string text)
        {
            if (text.Contains(" "))
            {
                int idx = text.LastIndexOf(' ');
                text = text.Remove(idx, 1);
                text = text.Insert(idx, "&nbsp;");
            }
            return text;
        }
    }
}