using System;

public partial class Preferences_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Redirect("~/Pages/Preferences/AdvertisementGenerator.aspx?oh=" + Request.Params["oh"]);
    }
}
