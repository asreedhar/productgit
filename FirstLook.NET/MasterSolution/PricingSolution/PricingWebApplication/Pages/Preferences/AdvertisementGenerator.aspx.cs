using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Utilities;
using FirstLook.Common.WebControls.UI;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.AdvertisementGenerator;

using Csla.Validation;
using FirstLook.Pricing.WebControls.AccessControl;


public partial class Preferences_AdvertisementGenerator : Page
{
    private const string startingNodeUrl = "/pricing/Pages/Preferences/AdvertisementGenerator.aspx";

    private int DealerId
    {
        get
        {
            return Identity.GetDealerId(Request.Params["oh"]);
        }
    }

    private AdvertisementGeneratorSetting AdGenSetting
    {
        get
        {
            return ViewState["AdGenSetting"] as AdvertisementGeneratorSetting;
        }
        set
        {
            ViewState["AdGenSetting"] = value;
        }
    }

    protected bool EnableGeneration
    {
        get
        {
            return AdGenSetting.EnableGeneration;
        }
    }

    protected bool EnableImport
    {
        get
        {
            return AdGenSetting.EnableImport;
        }
    }

    protected bool EnablePricingSettings
    {
        get
        {
            return ViewState["EnablePricingSettings"] != null && EnableGeneration;
        }
        set
        {
            if (!value)
                ViewState.Remove("EnablePricingSettings");
            else
                ViewState["EnablePricingSettings"] = true;
        }
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AdGenSetting = AdvertisementGeneratorSetting.GetAdvertisementGeneratorSetting(DealerId, true);
            DisableGenerationRadioButton.Checked = !AdGenSetting.EnableImport && !AdGenSetting.EnableGeneration;
            EnableGenerationRadioButton.Checked = !DisableGenerationRadioButton.Checked;

            ImportAdvertisementCheckBox.Enabled =
                GenerateAdvertisementCheckBox.Enabled = !DisableGenerationRadioButton.Checked;            

            ImportAdvertisementCheckBox.Checked = AdGenSetting.EnableImport;
            GenerateAdvertisementCheckBox.Checked = AdGenSetting.EnableGeneration;

            LoadComplete += AdvertisementProviderSettingCollectionGridView_RowCommand_LoadComplete;
            LoadComplete += AdvertisementFragmentSettingCollectionGridView_RowCommand_LoadComplete;
            LoadComplete += ValuationProviderSettingCollectionGridView_RowCommand_LoadComplete;
        }
        else
        {
            PreRenderComplete += CollectGridViewCheckBoxes;
        }
    }

    private void CollectGridViewCheckBoxes(object sender, EventArgs e)
    {
        PreRenderComplete -= CollectGridViewCheckBoxes;

        CollectAdvertisementProviderSelected();
        CollectAdvertisementFragmentSelected();
        CollectValuationProviderSettingSelected();
    }

    #region MainForm

    protected void ImportAdvertisementCheckBox_CheckChanged(object sender, EventArgs e)
    {
        AdGenSetting.EnableImport = ImportAdvertisementCheckBox.Checked;

        LoadComplete += AdvertisementProviderSettingCollectionGridView_RowCommand_LoadComplete;
    }

    protected void GenerateAdvertisementCheckBox_CheckChanged(object sender, EventArgs e)
    {
        AdGenSetting.EnableGeneration = GenerateAdvertisementCheckBox.Checked;

        ValuationSelectorDropDownList.Enabled = AdGenSetting.EnableGeneration;
        ValuationDifferenceDropDownList.Enabled = AdGenSetting.EnableGeneration;

        LoadComplete += ValuationProviderSettingCollectionGridView_RowCommand_LoadComplete;
        LoadComplete += AdvertisementFragmentSettingCollectionGridView_RowCommand_LoadComplete;
    }

    protected void DisableGenerationRadioButton_CheckChanged(object sender, EventArgs e)
    {
        if (DisableGenerationRadioButton.Checked)
        {
            EnableGenerationRadioButton.Checked = false;

            ImportAdvertisementCheckBox.Checked = GenerateAdvertisementCheckBox.Checked = false;

            ImportAdvertisementCheckBox.Enabled = GenerateAdvertisementCheckBox.Enabled = false;

            ValuationSelectorDropDownList.Enabled = ValuationDifferenceDropDownList.Enabled = false;

            AdGenSetting.EnableGeneration = AdGenSetting.EnableImport = false;
        }
        else
        {
            ImportAdvertisementCheckBox.Enabled = GenerateAdvertisementCheckBox.Enabled = true;
        }

        LoadComplete += AdvertisementProviderSettingCollectionGridView_RowCommand_LoadComplete;
        LoadComplete += AdvertisementFragmentSettingCollectionGridView_RowCommand_LoadComplete;
        LoadComplete += ValuationProviderSettingCollectionGridView_RowCommand_LoadComplete;
    }

    #endregion MainForm

    #region AdvertisementProviderSetting Gridview

    protected void AdvertisementProviderSettingCollectionGridView_PreRender(object sender, EventArgs e)
    {
        if (EnableImport)
        {
            AdvertisementProviderSettingCollectionGridView.CssClass = AdvertisementProviderSettingCollectionGridView.CssClass.Replace(" disabled", string.Empty);
        }
        else
        {
            AdvertisementProviderSettingCollectionGridView.CssClass += " disabled";
        }

        if (AdvertisementProviderSettingCollectionGridView.Rows.Count > 0)
        {
            AdvertisementProviderSettingCollectionGridView.Rows[0].Cells[2].Text = string.Empty;
            AdvertisementProviderSettingCollectionGridView.Rows[AdvertisementProviderSettingCollectionGridView.Rows.Count - 1].Cells[3].Text =
                string.Empty;
        }
    }

    protected void AdvertisementProviderSettingCollectionGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            (e.Row.FindControl("AdvertisementProviderSettingName") as Literal).Text = (e.Row.DataItem as AdvertisementProviderSetting).AdvertisementProvider.Name;
            (e.Row.FindControl("AdvertisementProviderSettingCheckBox") as CheckBox).Checked = (e.Row.DataItem as AdvertisementProviderSetting).Selected;
        }
    }

    protected void AdvertisementProviderSettingCollectionGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("IncreaseRank") || e.CommandName.Equals("DecreaseRank"))
        {
            int selectedIndex = Int32Helper.ToInt32(e.CommandArgument);

            GridView grid = sender as GridView;

            if (grid != null)
            {
                if (e.CommandName.Equals("IncreaseRank"))
                {
                    AdGenSetting.AdvertisementProviderSettings.Swap(selectedIndex, selectedIndex- 1);
                }
                else
                {
                    AdGenSetting.AdvertisementProviderSettings.Swap(selectedIndex, selectedIndex + 1);
                }

                LoadComplete += AdvertisementProviderSettingCollectionGridView_RowCommand_LoadComplete;
            }
        }
    }

    private void AdvertisementProviderSettingCollectionGridView_RowCommand_LoadComplete(object sender, EventArgs e)
    {
        LoadComplete -= AdvertisementProviderSettingCollectionGridView_RowCommand_LoadComplete;

        AdvertisementProviderSettingCollectionGridView.DataSource = AdGenSetting.AdvertisementProviderSettings;
        AdvertisementProviderSettingCollectionGridView.DataBind();
    }

    #endregion AdvertisementProviderSetting Gridview

    #region AdvertisementFragmentSetting Gridview

    protected void AdvertisementFragmentSettingCollectionGridView_PreRender(object sender, EventArgs e)
    {
        if (EnableGeneration)
        {
            AdvertisementFragmentSettingCollectionGridView.CssClass = AdvertisementFragmentSettingCollectionGridView.CssClass.Replace(" disabled", string.Empty);
        }
        else
        {
            AdvertisementFragmentSettingCollectionGridView.CssClass += " disabled";
        }

        if (AdvertisementFragmentSettingCollectionGridView.Rows.Count > 0)
        {
            AdvertisementFragmentSettingCollectionGridView.Rows[0].Cells[2].Text = string.Empty;
            AdvertisementFragmentSettingCollectionGridView.Rows[AdvertisementFragmentSettingCollectionGridView.Rows.Count - 1].Cells[3].Text =
                string.Empty;
        }
    }

    protected void AdvertisementFragmentSettingCollectionGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            AdvertisementFragmentSetting advertisementFragmentSetting = e.Row.DataItem as AdvertisementFragmentSetting;
            CheckBox checkBox = e.Row.FindControl("AdvertisementFragmentSettingCheckBox") as CheckBox;
            if (advertisementFragmentSetting != null)
            {
                (e.Row.FindControl("AdvertisementFragmentSettingName") as Literal).Text = advertisementFragmentSetting.AdvertisementFragment.Name;
                if (checkBox != null)
                {
                    checkBox.Checked = advertisementFragmentSetting.Selected;

                    if (advertisementFragmentSetting.AdvertisementFragment.ID == 1)
                    {
                        VehicleDescriptionHelpExtender.TargetControlID = checkBox.UniqueID;
                    }
                    else if (advertisementFragmentSetting.AdvertisementFragment.ID == 2)
                    {
                        VehicleHistoryReportHelpExtender.TargetControlID = checkBox.UniqueID;
                    }
                    else if (advertisementFragmentSetting.AdvertisementFragment.ID == 3)
                    {
                        CertifiedHelpExtender.TargetControlID = checkBox.UniqueID;
                    }
                    else if (advertisementFragmentSetting.AdvertisementFragment.ID == 4)
                    {
                        PricingDataHelpExtender.TargetControlID = checkBox.UniqueID;
                        EnablePricingSettings = checkBox.Checked;
                        checkBox.Attributes["rel"] = ValuationProviderSettingCollectionGridView.UniqueID;
                    }
                    else if (advertisementFragmentSetting.AdvertisementFragment.ID == 5)
                    {
                        EquipmentHelpExtender.TargetControlID = checkBox.UniqueID;
                    }
                    else if (advertisementFragmentSetting.AdvertisementFragment.ID == 6)
                    {
                        SnippetHelpExtener.TargetControlID = checkBox.UniqueID;
                    }
                }
            }
        }
    }
    
    protected void AdvertisementFragmentSettingCollectionGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("IncreaseRank") || e.CommandName.Equals("DecreaseRank"))
        {
            int selectedIndex = Int32Helper.ToInt32(e.CommandArgument);

            GridView grid = sender as GridView;

            if (grid != null)
            {
                if (e.CommandName.Equals("IncreaseRank"))
                {
                    AdGenSetting.AdvertisementFragmentSettings.Swap(selectedIndex, selectedIndex- 1);
                }
                else
                {
                    AdGenSetting.AdvertisementFragmentSettings.Swap(selectedIndex, selectedIndex + 1);
                }

                LoadComplete += AdvertisementFragmentSettingCollectionGridView_RowCommand_LoadComplete;
            }
        }
    }

    private void AdvertisementFragmentSettingCollectionGridView_RowCommand_LoadComplete(object sender, EventArgs e)
    {
        LoadComplete -= AdvertisementFragmentSettingCollectionGridView_RowCommand_LoadComplete;

        AdvertisementFragmentSettingCollectionGridView.DataSource = AdGenSetting.AdvertisementFragmentSettings;
        AdvertisementFragmentSettingCollectionGridView.DataBind();
    }

    #endregion AdvertisementFragmentSetting Gridview

    #region ValuationProviderSetting Gridview

    protected void ValuationProviderSettingCollectionGridView_PreRender(object sender, EventArgs e)
    {
        if (EnablePricingSettings)
        {
            ValuationProviderSettingCollectionGridView.CssClass = ValuationProviderSettingCollectionGridView.CssClass.Replace(" disabled", string.Empty);
        }
        else
        {
            ValuationProviderSettingCollectionGridView.CssClass += " disabled";
        }

        ValuationSelectorDropDownList.Enabled = ValuationDifferenceDropDownList.Enabled = EnablePricingSettings;

        if (ValuationProviderSettingCollectionGridView.Rows.Count > 0)
        {
            ValuationProviderSettingCollectionGridView.Rows[0].Cells[2].Text = string.Empty;
            ValuationProviderSettingCollectionGridView.Rows[ValuationProviderSettingCollectionGridView.Rows.Count - 1].Cells[3].Text =
                string.Empty;
        }
    }

    protected void ValuationProviderSettingCollectionGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            (e.Row.FindControl("ValuationProviderSettingName") as Literal).Text = (e.Row.DataItem as ValuationProviderSetting).ValuationProvider.Name;
            (e.Row.FindControl("ValuationProviderSettingCheckBox") as CheckBox).Checked = (e.Row.DataItem as ValuationProviderSetting).Selected;
        }
    }

    protected void ValuationProviderSettingCollectionGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("IncreaseRank") || e.CommandName.Equals("DecreaseRank"))
        {
            int selectedIndex = Int32Helper.ToInt32(e.CommandArgument);

            GridView grid = sender as GridView;

            if (grid != null)
            {
                if (e.CommandName.Equals("IncreaseRank"))
                {
                    AdGenSetting.ValuationProviderSettings.Swap(selectedIndex, selectedIndex - 1);
                }
                else
                {
                    AdGenSetting.ValuationProviderSettings.Swap(selectedIndex, selectedIndex + 1);
                }

                LoadComplete += ValuationProviderSettingCollectionGridView_RowCommand_LoadComplete;
            }
        }
    }

    private void ValuationProviderSettingCollectionGridView_RowCommand_LoadComplete(object sender, EventArgs e)
    {
        LoadComplete -= ValuationProviderSettingCollectionGridView_RowCommand_LoadComplete;

        ValuationProviderSettingCollectionGridView.DataSource = AdGenSetting.ValuationProviderSettings;
        ValuationProviderSettingCollectionGridView.DataBind();
    }

    #endregion ValuationProviderSetting Gridview

    #region Save Cancel

    protected void CollectAdvertisementProviderSelected()
    {
        int rowIdx = 0;
        foreach (AdvertisementProviderSetting setting in AdGenSetting.AdvertisementProviderSettings)
        {
            CheckBox checkbox =
                AdvertisementProviderSettingCollectionGridView.Rows[rowIdx++].FindControl("AdvertisementProviderSettingCheckBox") as
                CheckBox;
            if (checkbox != null)
            {
                setting.Selected = checkbox.Checked;
            }
            else
            {
                throw new InvalidOperationException("Every Row in AdvertisementProviderSettingCollectionGridView must be selectable");
            }
        }
    }

    protected void CollectAdvertisementFragmentSelected()
    {
        int rowIdx = 0;
        foreach (AdvertisementFragmentSetting setting in AdGenSetting.AdvertisementFragmentSettings)
        {
            CheckBox checkbox =
                AdvertisementFragmentSettingCollectionGridView.Rows[rowIdx++].FindControl("AdvertisementFragmentSettingCheckBox") as
                CheckBox;
            if (checkbox != null)
            {
                setting.Selected = checkbox.Checked;
            }
            else
            {
                throw new InvalidOperationException("Every Row in AdvertisementFragmentSettingCollectionGridView must be selectable");
            }
        }
    }

    protected void CollectValuationProviderSettingSelected()
    {
        int rowIdx = 0;
        foreach (ValuationProviderSetting setting in AdGenSetting.ValuationProviderSettings)
        {
            CheckBox checkbox =
                ValuationProviderSettingCollectionGridView.Rows[rowIdx++].FindControl("ValuationProviderSettingCheckBox") as
                CheckBox;
            if (checkbox != null)
            {
                setting.Selected = checkbox.Checked;
            }
            else
            {
                throw new InvalidOperationException("Every Row in ValuationProviderSettingCollectionGridView must be selectable");
            }
        }
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        LoadComplete += SaveAdGenSettings;
    }

    protected void SaveAdGenSettings(object sender, EventArgs e)
    {
        LoadComplete -= SaveAdGenSettings;

        if (!AdGenSetting.IsValid)
        {
            ShowValidationErrors();
        }
        else
        {
            AdGenSetting.Save();
        }
    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        PostBackRedirect();
    }

    #endregion Save Cancel

    #region Validation

    private void ShowValidationErrors()
    {
        BulletedList errorList = ValidationErrorDialog.FindControl("ErrorList") as BulletedList;

        if (errorList != null)
        {
            errorList.Items.Clear();

            foreach (BrokenRule rule in AdGenSetting.BrokenRulesCollection)
            {
                errorList.Items.Add(rule.Description);
            }

            bool inError = errorList.Items.Count > 0;

            ValidationErrorDialog.Hidden = !inError;
        }
    }

    protected void ValidationErrorDialog_ButtonClick(object sender, DialogButtonClickEventArgs e)
    {
        ValidationErrorDialog.Hidden = true;
    }

    #endregion Validation

    protected void PostBackRedirect()
    {
        if (!scriptManager.IsInAsyncPostBack)
        {
            // This Function is used to prevent refresh errors with form and gridviews
            // without this deleteing a row and refreshing the page will cause another row to be deleted, PM
            Response.Redirect(Request.Url.ToString(), true);
        }
    }

    #region SiteMaps

    protected void PreferencesSiteMapDataSource_DataBound(object sender, MenuEventArgs e)
    {
        MenuItem item = e.Item;

        if (item.NavigateUrl == startingNodeUrl + "?oh=" + Request.QueryString["oh"])
        {
            item.Selected = true;
        }
    }

    #endregion SiteMaps

    protected void ValuationSelectorDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        int id = Int32Helper.ToInt32(ValuationSelectorDropDownList.SelectedValue);
        AdGenSetting.ValuationSelector = ValuationSelectorCollection.GetValuationSelectorCollection().GetItem(id);
    }

    protected void ValuationSelectorDropDownList_DataBound(object sender, EventArgs e)
    {
        ValuationSelectorDropDownList.SelectedValue = AdGenSetting.ValuationSelector.ID.ToString();
    }

    protected void ValuationDifferenceDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        int id = Int32Helper.ToInt32(ValuationDifferenceDropDownList.SelectedValue);
        AdGenSetting.ValuationDifference = ValuationDifferenceCollection.GetValuationDifferenceCollection().GetItem(id);
    }

    protected void ValuationDifferenceDropDownList_DataBound(object sender, EventArgs e)
    {
        ValuationDifferenceDropDownList.SelectedValue = AdGenSetting.ValuationDifference.ID.ToString();
    }

    protected void AdvertisementFragmentSettingCheckBox_CheckChanged(object sender, EventArgs e)
    {
        CheckBox checkBox = sender as CheckBox;

        if (checkBox != null)
        {
            if (checkBox.Attributes["rel"] == ValuationProviderSettingCollectionGridView.UniqueID)
            {
                EnablePricingSettings = checkBox.Checked;
                LoadComplete += ValuationProviderSettingCollectionGridView_RowCommand_LoadComplete;
            }
        }
    }
}