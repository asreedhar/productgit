<%@ Page Language="C#" AutoEventWireup="true" Inherits="Preferences_AdvertisementGenerator" Codebehind="AdvertisementGenerator.aspx.cs" Theme="Leopard" %>

<%@ OutputCache Location="None" VaryByParam="None" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Pre-Owned Performance Management Center - Advertisement Generator Settings</title>
</head>
<body>
    <form id="AdvertisementGeneratorForm" runat="server" class="preference_page">
    	
    	<asp:ScriptManager ID="scriptManager" EnablePartialRendering="true" EnablePageMethods="true" runat="server" LoadScriptsBeforeUI="false" ScriptPath="/resources/Scripts">
            <Scripts>
                <asp:ScriptReference Path="~/Public/Scripts/Lib/Combined.js" NotifyScriptLoaded="true" />
                <asp:ScriptReference Path="~/Public/Scripts/Lib/DD_Roundies.js" NotifyScriptLoaded="true" />
                <asp:ScriptReference Path="~/Public/Scripts/Pages/global.js" NotifyScriptLoaded="true" />
                <asp:ScriptReference Path="~/Public/Scripts/Pages/PreferencePage.js" NotifyScriptLoaded="true" />
            </Scripts>
        </asp:ScriptManager>

    	<owc:HedgehogTracking ID="Hedgehog" runat="server" />
    	
    	<div id="hd">
    		<asp:SiteMapDataSource ID="ApplicationSiteMapDataSource" runat="server"
				    ShowStartingNode="false"
				    SiteMapProvider="ApplicationSiteMap" />
		    <asp:Menu id="application_menu_container" runat="server"
				    CssClass="smallCaps blue"
				    Orientation="Horizontal"
				    DataSourceID="ApplicationSiteMapDataSource" />
		    <asp:SiteMapDataSource ID="MemberSiteMapDataSource" runat="server"
				    ShowStartingNode="false"
				    SiteMapProvider="MemberSiteMap" />
    	</div>
    	
        <div id="bd">
        	
        	<asp:SiteMapDataSource ID="PreferencesSiteMapDataSource" runat="server"
				    ShowStartingNode="false"
				    SiteMapProvider="PreferencesSiteMap" />
		    <asp:Menu id="PreferencesMenu" runat="server"
		            CssClass="preference_nav"
		            MaximumDynamicDisplayLevels="1"
				    Orientation="Horizontal"
				    OnMenuItemDataBound="PreferencesSiteMapDataSource_DataBound"
				    DataSourceID="PreferencesSiteMapDataSource" />
            <asp:SiteMapDataSource ID="PreferencesSiteSecondLevelMapDataSource" runat="server"
				    ShowStartingNode="false"
				    StartingNodeUrl="/pricing/Pages/Preferences/"
				    SiteMapProvider="PreferencesSiteMap" /> 
		    <asp:Menu id="PreferencesSecondLevelMenu" runat="server"
		            CssClass="preference_nav secondary_preference_nav"
		            MaximumDynamicDisplayLevels="1"
				    Orientation="Vertical"
				    OnMenuItemDataBound="PreferencesSiteMapDataSource_DataBound"
				    DataSourceID="PreferencesSiteSecondLevelMapDataSource" />
            
            <asp:UpdatePanel runat="server" ChildrenAsTriggers="true">
                <ContentTemplate>
                
                    <fieldset class="advertisement_generation_form vform">
                        <legend>
                            Advertisement Generator Settings
                        </legend>
                        
                        <ul class="radio_group">
                            <li>
                                <asp:RadioButton runat="server" AutoPostBack="true" ID="DisableGenerationRadioButton" 
                                        OnCheckedChanged="DisableGenerationRadioButton_CheckChanged" 
                                        GroupName="EnableGenerationGroup" 
                                        Text="Do not generate an advertisement" />                        
                                
                                <div runat="server" id="DisableGenerationRadioButtonHelpExtended" class="help_text help_arrow_left">
                                    <span class="help_arrow"></span>
                                    <p>Internet Highlights will <em>NOT</em> be automatically generated in the Internet Advertising Accelerator. 
                                    If your Internet Inventory Distribution provider accepts Internet Descriptions,
                                    you will need to manually create all Highlights for inclusion in the FirstLook data feed export.</p>
                                </div>
                                
                                <cwc:HelpTextExtender ID="DisableGenerationRadioButtonHelpExtendedExtender" runat="server" 
                                    TargetControlID="DisableGenerationRadioButton"
                                    HelperControlID="DisableGenerationRadioButtonHelpExtended"
                                    ShowHelperTextEventTrigger="Checked" />                                                                
                            </li>
                            <li>
                                <asp:RadioButton runat="server" AutoPostBack="true" ID="EnableGenerationRadioButton" 
                                        OnCheckedChanged="DisableGenerationRadioButton_CheckChanged" 
                                        GroupName="EnableGenerationGroup" 
                                        Text="Auto generate Internet Advertisements from Providers" />                                                                
                            </li>                    
                        </ul>
                        
                        <p class="checkbox">
                            <asp:CheckBox runat="server" 
                                AutoPostBack="true" 
                                ID="ImportAdvertisementCheckBox" 
                                Text="Import advertisement"
                                OnCheckedChanged="ImportAdvertisementCheckBox_CheckChanged" />
                                
                            <div runat="server" id="ImportAdvertisementCheckBoxHelp" class="help_text help_arrow_left help_even">
                                <span class="help_arrow"></span>
                                <p>FirstLook will attempt to find your inventory online at the websites listed.</p>
                                <p>If we cannot find a vehicle, you may elect to pre-populate the Internet Highlights using 
                                the <em class="page_reference">1-Click Data + VIN Explosion</em> preferences below. This will most 
                                often occur when new vehicles enter inventory or when your dealership does not use one of the 
                                included listing websites.</p>
                            </div>
                            
                             <cwc:HelpTextExtender ID="ImportAdvertisementCheckBoxHelpExtender" runat="server" 
                                TargetControlID="ImportAdvertisementCheckBox"
                                HelperControlID="ImportAdvertisementCheckBoxHelp"
                                ShowHelperTextEventTrigger="Checked" />
                        </p>                           
                                                        
                        <asp:GridView ID="AdvertisementProviderSettingCollectionGridView" runat="server"
                                Caption="Advertisement Providers"
                                CssClass="grid import_advertisement_report"
                                AlternatingRowStyle-CssClass="odd"
                                AutoGenerateColumns="false"
                                AutoGenerateDeleteButton="false"
                                AutoGenerateEditButton="false"
                                AutoGenerateSelectButton="false"
                                OnPreRender="AdvertisementProviderSettingCollectionGridView_PreRender"
                                OnRowDataBound="AdvertisementProviderSettingCollectionGridView_RowDataBound"
                                OnRowCommand="AdvertisementProviderSettingCollectionGridView_RowCommand">
                            <Columns>
                                <asp:TemplateField
                                    ItemStyle-CssClass="selected"
                                    HeaderStyle-CssClass="selected">
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ID="AdvertisementProviderSettingCheckBox" Enabled='<%# EnableImport %>' AutoPostBack="true" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Literal runat="server" ID="AdvertisementProviderSettingName" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:ButtonField CommandName="IncreaseRank" ButtonType="Button" 
                                    ItemStyle-CssClass="move_up"
                                    HeaderStyle-CssClass="move_up"
                                    Text="&uarr;" 
                                    ShowHeader="false" />
                                <asp:ButtonField CommandName="DecreaseRank" ButtonType="Button" 
                                    ItemStyle-CssClass="move_down"
                                    HeaderStyle-CssClass="move_down"
                                    Text="&darr;"
                                    ShowHeader="false" />
                            </Columns>
                        </asp:GridView>
                        
                        <p class="checkbox">
                            <asp:CheckBox runat="server" 
                                AutoPostBack="true" 
                                ID="GenerateAdvertisementCheckBox" 
                                Text="Generate advertisement" 
                                OnCheckedChanged="GenerateAdvertisementCheckBox_CheckChanged" />
                                
                            <div runat="server" id="GenerateAdvertisementCheckBoxHelp" class="help_text help_arrow_left help_even">
                                <span class="help_arrow"></span>
                                <p>FirstLook will automatically generate Internet Descriptions in the Internet Advertising Accelerator 
                                using the content selected for inclusion defined below.</p>
                                
                                <hr />
                                
                                <dl runat="server" id="VehicleDescriptionHelper">
                                    <dt>Vehicle Description:</dt>
                                    <dd>Includes the vehicle's Year, Make, Model, Segment, and Trim.</dd>
                                </dl>
                                
                                <dl runat="server" id="VehicleHistoryReportHelper">
                                    <dt>Vehicle History Reports (VHR):</dt>
                                    <dd>Include positive <abbr title="Vehicle History Reports">VHR</abbr> reports from CARFAX or AutoCheck in your ads. 
                                    Your dealership must subscribe to <abbr title="Vehicle History Reports">VHR</abbr> services separately.</dd>
                                </dl>
                                
                                <dl runat="server" id="PricingDataHelper">
                                    <dt>360&deg; Pricing Data:</dt>
                                    <dd>
                                     <p>Highlight a vehicle's value relative to book values commonly used by consumers.</p>
                                      
                                     <dl>
                                        <dt>Threshold:</dt>
                                        <dd>FirstLook will not report your Internet Price as less than the referenced book value unless
                                         the difference is $100 or more (i.e. "Priced $100 <em>below</em> Kelley Blue Book").</dd>

                                        <dt>Pricing Selector($):</dt>
                                        <dd>
                                            <p>FirstLook will only include one 360&deg; Pricing reference value in your ads.</p>
                                            <dl>
                                                <dt>First:</dt>
                                                <dd>Order the books below and FirstLook will use the first book where the relative
                                                 value is greater than or equal to the threshold set above.</dd>
                                                 
                                                <dt>Best:</dt>
                                                <dd>Select the books below to use for analysis (order does not matter) and FirstLook will use the 
                                                book with the greatest difference between the <span class="app_reference">Internet Price</span> and book value</dd>
                                            </dl>
                                        </dd>
                                     </dl>                                                                         
                                     
                                    </dd>
                                </dl>
                                
                                <dl runat="server" id="EquipmentHelper">
                                    <dt>Equipment:</dt>
                                    <dd>Include each vehicle's high-value and standard equipment options in your ads. You can configure formatting and select a book from which to derive options on the Equipment Settings page.</dd>
                                </dl>
                                
                                <dl runat="server" id="CertifiedHelper">
                                    <dt>Certified:</dt>
                                    <dd>Highlight the value of Certified vehicles by labeling them as such in your ads.</dd>
                                </dl>
                                
                                <dl runat="server" id="SnippetHelper">
                                    <dt>Snippet:</dt>
                                    <dd>Include your designated default snippet in all of your advertisements. Please see the <span class="page_reference">Snippet
                                     Preferences manager</span>.</dd>
                                </dl>
                                
                            </div>
                            
                             <cwc:HelpTextExtender ID="GenerateAdvertisementCheckBoxHelpTextExtender" runat="server" 
                                TargetControlID="GenerateAdvertisementCheckBox"
                                HelperControlID="GenerateAdvertisementCheckBoxHelp"
                                ShowHelperTextEventTrigger="Checked" />
                                
                             <cwc:HelpTextExtender ID="VehicleDescriptionHelpExtender" runat="server"
                                HelperControlID="VehicleDescriptionHelper"
                                ShowHelperTextEventTrigger="Checked" />
                                
                             <cwc:HelpTextExtender ID="VehicleHistoryReportHelpExtender" runat="server"
                                HelperControlID="VehicleHistoryReportHelper"
                                ShowHelperTextEventTrigger="Checked" />
                             
                             <cwc:HelpTextExtender ID="PricingDataHelpExtender" runat="server"
                                HelperControlID="PricingDataHelper"
                                ShowHelperTextEventTrigger="Checked" />
                                
                             <cwc:HelpTextExtender ID="EquipmentHelpExtender" runat="server"
                                HelperControlID="EquipmentHelper"
                                ShowHelperTextEventTrigger="Checked" />
                             
                             <cwc:HelpTextExtender ID="CertifiedHelpExtender" runat="server"
                                HelperControlID="CertifiedHelper"
                                ShowHelperTextEventTrigger="Checked" />
                                
                             <cwc:HelpTextExtender ID="SnippetHelpExtener" runat="server"
                                HelperControlID="SnippetHelper"
                                ShowHelperTextEventTrigger="Checked" />
                        </p>
                        
                        <asp:GridView ID="AdvertisementFragmentSettingCollectionGridView" runat="server"
                                Caption="1-Click Data + VIN Explosion"
                                CssClass="grid advertisement_items_report"
                                AlternatingRowStyle-CssClass="odd"
                                AutoGenerateColumns="false"
                                AutoGenerateDeleteButton="false"
                                AutoGenerateEditButton="false"
                                AutoGenerateSelectButton="false"
                                OnPreRender="AdvertisementFragmentSettingCollectionGridView_PreRender"
                                OnRowDataBound="AdvertisementFragmentSettingCollectionGridView_RowDataBound"
                                OnRowCommand="AdvertisementFragmentSettingCollectionGridView_RowCommand">
                            <Columns>
                                <asp:TemplateField
                                    ItemStyle-CssClass="selected"
                                    HeaderStyle-CssClass="selected">
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ID="AdvertisementFragmentSettingCheckBox" Enabled='<%# EnableGeneration %>' AutoPostBack="true" OnCheckedChanged="AdvertisementFragmentSettingCheckBox_CheckChanged" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Literal runat="server" ID="AdvertisementFragmentSettingName" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:ButtonField CommandName="IncreaseRank" ButtonType="Button" 
                                    ItemStyle-CssClass="move_up"
                                    HeaderStyle-CssClass="move_up"
                                    Text="&uarr;" 
                                    ShowHeader="false" />
                                <asp:ButtonField CommandName="DecreaseRank" ButtonType="Button" 
                                    ItemStyle-CssClass="move_down"                                        
                                    HeaderStyle-CssClass="move_down"
                                    Text="&darr;"
                                    ShowHeader="false" />                                    
                            </Columns>
                        </asp:GridView>                        
                            
                        <p class="pricing_report_dropdown">
                            <asp:ObjectDataSource runat="server" ID="ValuationDifferenceDataSource"
                                    TypeName="FirstLook.Pricing.DomainModel.Internet.ObjectModel.AdvertisementGenerator.ValuationDifferenceCollection+DataSource"
                                    SelectMethod="Select">
                            </asp:ObjectDataSource>
                        
                            <label>Set Book Threshold : 
                                <asp:DropDownList runat="server" ID="ValuationDifferenceDropDownList" 
                                        DataTextField="Amount" 
                                        DataValueField="Id"
                                        Enabled='<%# EnablePricingSettings %>'
                                        DataSourceID="ValuationDifferenceDataSource"
                                        OnSelectedIndexChanged="ValuationDifferenceDropDownList_SelectedIndexChanged"
                                        OnDataBound="ValuationDifferenceDropDownList_DataBound" />
                            </label>
                        </p>
                            
                        <p class="pricing_report_dropdown">
                            <asp:ObjectDataSource runat="server" ID="ValuationSelectorDataSource"
                                    TypeName="FirstLook.Pricing.DomainModel.Internet.ObjectModel.AdvertisementGenerator.ValuationSelectorCollection+DataSource"
                                    SelectMethod="Select">
                            </asp:ObjectDataSource>
                        
                            <label>Pricing Selector ($) : 
                                <asp:DropDownList runat="server" ID="ValuationSelectorDropDownList" 
                                        DataTextField="Name" 
                                        DataValueField="Id"
                                        DataSourceID="ValuationSelectorDataSource"
                                        Enabled='<%# EnablePricingSettings %>'
                                        OnSelectedIndexChanged="ValuationSelectorDropDownList_SelectedIndexChanged"
                                        OnDataBound="ValuationSelectorDropDownList_DataBound" />
                            </label>
                        </p>  

                        <asp:GridView ID="ValuationProviderSettingCollectionGridView" runat="server"
                                Caption="360 Pricing"
                                CssClass="grid generate_pricing_report"
                                AlternatingRowStyle-CssClass="odd"
                                AutoGenerateColumns="false"
                                AutoGenerateDeleteButton="false"
                                AutoGenerateEditButton="false"
                                AutoGenerateSelectButton="false"
                                OnPreRender="ValuationProviderSettingCollectionGridView_PreRender"
                                OnRowDataBound="ValuationProviderSettingCollectionGridView_RowDataBound"
                                OnRowCommand="ValuationProviderSettingCollectionGridView_RowCommand">
                            <Columns>
                                <asp:TemplateField
                                    ItemStyle-CssClass="selected"
                                    HeaderStyle-CssClass="selected">
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ID="ValuationProviderSettingCheckBox" Enabled='<%# EnablePricingSettings %>' AutoPostBack="true" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Literal runat="server" ID="ValuationProviderSettingName" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:ButtonField CommandName="IncreaseRank" ButtonType="Button" 
                                    ItemStyle-CssClass="move_up"
                                    HeaderStyle-CssClass="move_up"
                                    Text="&uarr;" 
                                    ShowHeader="false" />
                                <asp:ButtonField CommandName="DecreaseRank" ButtonType="Button" 
                                    ItemStyle-CssClass="move_down"
                                    HeaderStyle-CssClass="move_down"
                                    Text="&darr;"
                                    ShowHeader="false" />
                            </Columns>
                        </asp:GridView>
            	        </fieldset>
                        <fieldset class="controls">
                            <span class="button insert_button"><asp:Button ID="SaveButton" runat="server" OnClick="SaveButton_Click" Text="Save" /></span>
                            <span class="button cancel_button"><asp:Button ID="CancelButton" runat="server" OnClick="CancelButton_Click" Text="Cancel" /></span>
                        </fieldset>
                    
                    <cwc:Dialog runat="server" ID="ValidationErrorDialog"
                           Visible="true" Hidden="true"
                           TitleText="Valiation Error"
                           Top="300" Left="310" Width="290" Height="150" AllowResize="false" AllowDrag="true"
                           OnButtonClick="ValidationErrorDialog_ButtonClick">
                        <ItemTemplate>
                            <asp:BulletedList ID="ErrorList" runat="server">
                            </asp:BulletedList>
                        </ItemTemplate>
                        
                        <Buttons>
                            <cwc:DialogButton ButtonType="button" ButtonCommand="Ok" Text="Ok" IsDefault="true" />
                        </Buttons>
                    </cwc:Dialog>
            
                
                </ContentTemplate>
            </asp:UpdatePanel>
            
            <div id="progress">
                <div>
                    <img src="../../Public/Images/loading.gif" alt="Loading ... Please Wait" /> Please wait...
                </div>
            </div>
            
        </div>
        <cwc:PageFooter ID="SiteFooter" runat="server" />
    </form>
</body>
</html>
