using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Common.WebControls.UI;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel;
using FirstLook.Pricing.WebControls.AccessControl;

public partial class Preferences_EquipmentSettings : Page
{
    private const string startingNodeUrl = "/pricing/Pages/Preferences/EquipmentSettings.aspx";

    private int DealerId
    {
        get { return Identity.GetDealerId(Request.Params["oh"]); }
    }

	protected void EquipmentSettingsDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
	{
		if (e.ReturnValue == null)
		{
            EquipmentSettingsFormView.ChangeMode(FormViewMode.Insert);
		}
	}

    protected void EquipmentSettingsFormView_ItemInserting(object sender, FormViewInsertEventArgs e)
	{
        string highValueEquipmentPrefix = e.Values["HighValueEquipmentPrefix"] as string;

        //save as string for error handling later if not int-parsable
        string highValueEquipmentThreshold = e.Values["HighValueEquipmentThreshold"] as string;
        string spacerText = e.Values["SpacerText"] as string;
        string standardEquipmentPrefix = e.Values["StandardEquipmentPrefix"] as string;
        bool inError = ValidateValues(highValueEquipmentPrefix, highValueEquipmentThreshold, spacerText, standardEquipmentPrefix);
		e.Cancel = inError;
	}

	protected void EquipmentSettingsFormView_ItemInserted(object sender, FormViewInsertedEventArgs e)
	{
		if (e.Exception != null)
		{
			HandleException(e.Exception);
			e.KeepInInsertMode = true;
			e.ExceptionHandled = true;
		}
		else
		{
			e.KeepInInsertMode = false;

			PostBackRedirect();
		}
	}

	protected void EquipmentSettingsFormView_ItemUpdating(object sender, FormViewUpdateEventArgs e)
	{
        string highValueEquipmentPrefix = e.NewValues["HighValueEquipmentPrefix"] as string;
        string highValueEquipmentThreshold = e.NewValues["HighValueEquipmentThreshold"] as string;
        string spacerText = e.NewValues["SpacerText"] as string;
        string standardEquipmentPrefix = e.NewValues["StandardEquipmentPrefix"] as string;

        bool inError = ValidateValues(highValueEquipmentPrefix, highValueEquipmentThreshold, spacerText, standardEquipmentPrefix);
        e.Cancel = inError;
	}

	protected void EquipmentSettingsFormView_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
	{
		if (e.Exception != null)
		{
			HandleException(e.Exception);
			e.KeepInEditMode = true;
			e.ExceptionHandled = true;
		}
		else
		{
			e.KeepInEditMode = false;

			PostBackRedirect();
		}
	}

	private bool ValidateValues(string highValueEquipmentPrefix, string highValueEquipmentThreshold, string spacerText, string standardEquipmentPrefix)
	{
	    BulletedList errorList = ValidationErrorDialog.FindControl("ErrorList") as BulletedList;

        if (errorList != null)
        {
            errorList.Items.Clear();

            if (!string.IsNullOrEmpty(highValueEquipmentThreshold))
            {
                int threshold;

                if (int.TryParse(highValueEquipmentThreshold, out threshold))
                {
                	if (threshold < 0)
                	{
						errorList.Items.Add(new ListItem("High Value Equipment Threshold must be greater or equal to 0"));
                	}
                }
				else
                {
                    errorList.Items.Add(new ListItem("High Value Equipment Threshold must be a number"));
                }
            }

            bool inError = errorList.Items.Count > 0;

            ValidationErrorDialog.Hidden = !inError;

            return inError;
        }

		return true;
	}

	private void HandleException(Exception exception)
	{
        BulletedList errorList = ValidationErrorDialog.FindControl("ErrorList") as BulletedList;

        if (errorList != null)
        {
            errorList.Items.Clear();

        	Exception item = exception;

			while (item != null)
			{
				string message = item.Message;

				if (!string.IsNullOrEmpty(message))
				{
					if (!message.Contains("Exception has been thrown by the target of an invocation"))
					{
						errorList.Items.Add(message);
					}
				}

				item = item.InnerException;
			}
            
            bool inError = errorList.Items.Count > 0;

            ValidationErrorDialog.Hidden = !inError;
        }
	}

    protected void ValidationErrorDialog_ButtonClick(object sender, DialogButtonClickEventArgs e)
    {
        ValidationErrorDialog.Hidden = true;
    }

	protected void Page_Load(object sender, EventArgs e)
    {
	    EquipmentSettingsDataSource.SelectParameters["dealerId"].DefaultValue = DealerId.ToString();
        EquipmentSettingsDataSource.InsertParameters["dealerId"].DefaultValue = DealerId.ToString();
        EquipmentSettingsDataSource.UpdateParameters["dealerId"].DefaultValue = DealerId.ToString();

        AdvertisementEquipmentProviderCollectionObjectDataSource.SelectParameters["dealerId"].DefaultValue = DealerId.ToString();
    }

    protected void PostBackRedirect()
    {
        if (!scriptManager.IsInAsyncPostBack)
        {
            // This Function is used to prevent refresh errors with form and gridviews
            // without this deleteing a row and refreshing the page will cause another row to be deleted, PM
            Response.Redirect(Request.Url.ToString(), true);
        }
    }

	protected void PreferencesSiteMapDataSource_DataBound(object sender, MenuEventArgs e)
    {
        MenuItem item = e.Item;

        if (item.NavigateUrl == startingNodeUrl + "?oh=" + Request.QueryString["oh"])
        {
            item.Selected = true;
        }
    }

    protected void EquipmentSettingsFormView_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        if (e.NewMode == FormViewMode.Insert)
        {
            Button insertButton = EquipmentSettingsFormView.FindControl("InsertButton") as Button;

            if (insertButton != null)
            {
                insertButton.Enabled = false;
            }
        }
    }
}
