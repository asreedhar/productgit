<%@ Page Language="C#" AutoEventWireup="true" Inherits="Preferences_EquipmentSettings" Codebehind="EquipmentSettings.aspx.cs" Theme="Leopard" %>

<%@ OutputCache Location="None" VaryByParam="None" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Pre-Owned Performance Management Center - Manage Equipment Settings</title>
</head>
<body>
    <form id="EquipmentSettingsForm" runat="server" class="preference_page">
    	
    	<asp:ScriptManager ID="scriptManager" EnablePartialRendering="true" EnablePageMethods="true" runat="server" LoadScriptsBeforeUI="false" ScriptPath="/resources/Scripts">
            <Scripts>
                <asp:ScriptReference Path="~/Public/Scripts/Lib/Combined.js" NotifyScriptLoaded="true" />
                <asp:ScriptReference Path="~/Public/Scripts/Lib/DD_Roundies.js" NotifyScriptLoaded="true" />
                <asp:ScriptReference Path="~/Public/Scripts/Pages/global.js" NotifyScriptLoaded="true" />
                <asp:ScriptReference Path="~/Public/Scripts/Pages/PreferencePage.js" NotifyScriptLoaded="true" />
            </Scripts>
        </asp:ScriptManager>

    	<owc:HedgehogTracking ID="Hedgehog" runat="server" />
    	
    	<div id="hd">
    		<asp:SiteMapDataSource ID="ApplicationSiteMapDataSource" runat="server"
				    ShowStartingNode="false"
				    SiteMapProvider="ApplicationSiteMap" />
		    <asp:Menu id="application_menu_container" runat="server"
				    CssClass="smallCaps blue"
				    Orientation="Horizontal"
				    DataSourceID="ApplicationSiteMapDataSource" />
		    <asp:SiteMapDataSource ID="MemberSiteMapDataSource" runat="server"
				    ShowStartingNode="false"
				    SiteMapProvider="MemberSiteMap" />
    	</div>
    	
        <div id="bd">
        	
        	<asp:SiteMapDataSource ID="PreferencesSiteMapDataSource" runat="server"
				    ShowStartingNode="false"
				    SiteMapProvider="PreferencesSiteMap" />
		    <asp:Menu id="PreferencesMenu" runat="server"
		            CssClass="preference_nav"
		            MaximumDynamicDisplayLevels="1"
				    Orientation="Horizontal"
				    OnMenuItemDataBound="PreferencesSiteMapDataSource_DataBound"
				    DataSourceID="PreferencesSiteMapDataSource" />
            <asp:SiteMapDataSource ID="PreferencesSiteSecondLevelMapDataSource" runat="server"
				    ShowStartingNode="false"
				    StartingNodeUrl="/pricing/Pages/Preferences/"
				    SiteMapProvider="PreferencesSiteMap" /> 
		    <asp:Menu id="PreferencesSecondLevelMenu" runat="server"
		            CssClass="preference_nav secondary_preference_nav"
		            MaximumDynamicDisplayLevels="1"
				    Orientation="Vertical"
				    OnMenuItemDataBound="PreferencesSiteMapDataSource_DataBound"
				    DataSourceID="PreferencesSiteSecondLevelMapDataSource" />

            <asp:ObjectDataSource ID="EquipmentSettingsDataSource" runat="server"
                    TypeName="FirstLook.Pricing.DomainModel.Internet.ObjectModel.AdvertisementGenerator.AdvertisementEquipmentSetting+DataSource"
                    SelectMethod="Select"
                    InsertMethod="Insert"
                    UpdateMethod="Update"
                    OnSelected="EquipmentSettingsDataSource_Selected">
                <SelectParameters>
                    <asp:Parameter Name="dealerId" Type="Int32" ConvertEmptyStringToNull="true" />
                </SelectParameters>
                <InsertParameters>
                    <asp:Parameter Name="HighValueEquipmentThreshold" Type="Int32" DefaultValue="150" />
                    <asp:Parameter Name="dealerId" Type="Int32" ConvertEmptyStringToNull="true" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="HighValueEquipmentThreshold" Type="Int32" DefaultValue="150" />
                    <asp:Parameter Name="dealerId" Type="Int32" ConvertEmptyStringToNull="true" />
                </UpdateParameters>
            </asp:ObjectDataSource>
            
            <asp:ObjectDataSource ID="AdvertisementEquipmentProviderCollectionObjectDataSource" runat="server"
                    TypeName="FirstLook.Pricing.DomainModel.Internet.ObjectModel.AdvertisementGenerator.AdvertisementEquipmentProviderCollection+DataSource"
                    SelectMethod="Select">
                <SelectParameters>
                    <asp:Parameter Name="dealerId" Type="Int32" ConvertEmptyStringToNull="true" />
                </SelectParameters>
            </asp:ObjectDataSource>


            <asp:UpdatePanel runat="server" ID="FormViewUpdatePanel" ChildrenAsTriggers="true">
                <ContentTemplate>
                
                    <asp:FormView ID="EquipmentSettingsFormView" runat="server"
                            CssClass="form_view"
                            DataSourceID="EquipmentSettingsDataSource"
                            DefaultMode="Edit"
                            OnModeChanging="EquipmentSettingsFormView_ModeChanging"
                            OnItemInserting="EquipmentSettingsFormView_ItemInserting"
                            OnItemInserted="EquipmentSettingsFormView_ItemInserted"
                            OnItemUpdating="EquipmentSettingsFormView_ItemUpdating"
                            OnItemUpdated="EquipmentSettingsFormView_ItemUpdated">
                        
                        <InsertItemTemplate>
                            <fieldset class="equipment_form hform">
    	                        <legend>
    		                        Update Equipment Settings
    	                        </legend>
                        	    
    	                        <p class="desc">Settings on this page impact the text output both by the 1-Click tool bar and the automatic ad builder in the Internet Advertising Accelerator.</p>
    	                        <p>
                                    <asp:Label runat="server" ID="InsertEquipmentSettingsSpacerTextLabel" Text="Spacer Text :" EnableViewState="false" AssociatedControlID="InsertEquipmentSettingsSpacerTextTextBox" />
                                    <asp:TextBox ID="InsertEquipmentSettingsSpacerTextTextBox" runat="server" Text='<%# Bind("SpacerText") %>' MaxLength="5" />
                                    
                                    <div runat="server" id="InsertEquipmentSettingsSpacerTextTextBoxHelp" class="help_text help_arrow_left help_even">
                                        <span class="help_arrow"></span>
                                        <p>Add up to <em>5 characters</em> that will be inserted between attributes of your ads to add visual 
                                        flare and distinction. For instance, adding &ldquo; <em class="example">***</em> &rdquo; in the field will cause an ad for a 
                                        Certified, 2005 Honda Accord to be output in the automatic ad builder as, 
                                        &ldquo; <em class="example">*** 2005 Honda Accord *** Certified ***</em> &rdquo;
                                        <br />
                                        <small>(plus whatever additional attributes are available for the vehicle).</small></p>
                                    </div>
                                    
                                    <cwc:HelpTextExtender runat="server" ID="InsertEquipmentSettingsSpacerTextTextBoxHelper"
                                        TargetControlID="InsertEquipmentSettingsSpacerTextTextBox"
                                        HelperControlID="InsertEquipmentSettingsSpacerTextTextBoxHelp"
                                        ShowHelperTextEventTrigger="OnFocus"
                                        HideHelperTextEventTrigger="OnBlur" />
                                </p>
                                <p>
                                    <asp:Label ID="InsertEquipmentSettingsEquipmentProviderLabel" runat="server" Text="Vehicle Options Source :" AssociatedControlID="InsertEquipmentSettingsEquipmentProviderDropDownList" EnableViewState="false" />
                                    <asp:DropDownList ID="InsertEquipmentSettingsEquipmentProviderDropDownList" DataTextField="Name" DataValueField="ID" DataSourceID="AdvertisementEquipmentProviderCollectionObjectDataSource" runat="server" Text='<%# Bind("EquipmentProviderID") %>' />
                                    
                                    <div runat="server" id="InsertEquipmentSettingsEquipmentProviderDropDownListHelp" class="help_text help_arrow_left help_even">
                                        <span class="help_arrow"></span>
                                        <p>Select the book from which equipment is derived for use in your ads.</p>
                                    </div>
                                    
                                    <cwc:HelpTextExtender runat="server" ID="InsertEquipmentSettingsEquipmentProviderDropDownListHelper"
                                        TargetControlID="InsertEquipmentSettingsEquipmentProviderDropDownList"
                                        HelperControlID="InsertEquipmentSettingsEquipmentProviderDropDownListHelp"
                                        ShowHelperTextEventTrigger="OnFocus"
                                        HideHelperTextEventTrigger="OnBlur" />
        	                    </p>
                                <p>
                                    <asp:Label runat="server" ID="InsertEquipmentSettingsHighValueEquipmentThresholdLabel" Text="High Value Equipment Threshold :" EnableViewState="false" AssociatedControlID="InsertEquipmentSettingsHighValueEquipmentThresholdTextBox" />
                                    <asp:TextBox ID="InsertEquipmentSettingsHighValueEquipmentThresholdTextBox" runat="server" Text='<%# Bind("HighValueEquipmentThreshold") %>' MaxLength="20" />
                                    
                                    <div runat="server" id="InsertEquipmentSettingsHighValueEquipmentThresholdTextBoxHelp" class="help_text help_arrow_left help_even">
                                        <span class="help_arrow"></span>
                                        <p>Using the designated book above, FirstLook will use this threshold to segregate options based on price and automatically bring high-value options to the top of the list in order to add impact.</p>
                                    </div>
                                    
                                    <cwc:HelpTextExtender runat="server" ID="InsertEquipmentSettingsHighValueEquipmentThresholdTextBoxHelper"
                                        TargetControlID="InsertEquipmentSettingsHighValueEquipmentThresholdTextBox"
                                        HelperControlID="InsertEquipmentSettingsHighValueEquipmentThresholdTextBoxHelp"
                                        ShowHelperTextEventTrigger="OnFocus"
                                        HideHelperTextEventTrigger="OnBlur" />
                                </p>
                                <p>
                                    <asp:Label runat="server" ID="InsertEquipmentSettingsHighValueEquipmentPrefixLabel" Text="High Value Equipment Prefix :" EnableViewState="false" AssociatedControlID="InsertEquipmentSettingsHighValueEquipmentPrefixTextBox" />
                                    <asp:TextBox ID="InsertEquipmentSettingsHighValueEquipmentPrefixTextBox" runat="server" Text='<%# Bind("HighValueEquipmentPrefix") %>' MaxLength="20" />
                                    
                                    <div runat="server" id="InsertEquipmentSettingsHighValueEquipmentPrefixTextBoxHelp" class="help_text help_arrow_left help_even">
                                        <span class="help_arrow"></span>
                                        <p>Create impact with a strong statement to lead into your high-value options list created using 
                                        the threshold above. For instance, <em class="example">&ldquo; This vehicle comes LOADED with: &rdquo;</em> which would then be followed 
                                        by a comma separated list of the vehicle's high value equipment.</p>
                                    </div>
                                    
                                    <cwc:HelpTextExtender runat="server" ID="InsertEquipmentSettingsHighValueEquipmentPrefixTextBoxHelper"
                                        TargetControlID="InsertEquipmentSettingsHighValueEquipmentPrefixTextBox"
                                        HelperControlID="InsertEquipmentSettingsHighValueEquipmentPrefixTextBoxHelp"
                                        ShowHelperTextEventTrigger="OnFocus"
                                        HideHelperTextEventTrigger="OnBlur" />
                                </p>
                                <p>
                                    <asp:Label ID="InsertEquipmentSettingsStandardEquipmentPrefixLabel" runat="server" Text="Standard Equipment Prefix :" AssociatedControlID="InsertEquipmentSettingsStandardEquipmentPrefixTextBox" EnableViewState="false" />
                                    <asp:TextBox ID="InsertEquipmentSettingsStandardEquipmentPrefixTextBox" runat="server" Text='<%# Bind("StandardEquipmentPrefix") %>' MaxLength="20" />
                                    
                                    <div runat="server" id="InsertEquipmentSettingsStandardEquipmentPrefixTextBoxHelp" class="help_text help_arrow_left help_even">
                                        <span class="help_arrow"></span>
                                        <p>Again, using the lists created by the threshold, you may designate a prefix to lead into the 
                                        standard options available on the vehicle but likely deserving of less prominence in the 
                                        ad <small>(i.e. power door locks).</small></p>
                                    </div>
                                    
                                    <cwc:HelpTextExtender runat="server" ID="InsertEquipmentSettingsStandardEquipmentPrefixTextBoxHelper"
                                        TargetControlID="InsertEquipmentSettingsStandardEquipmentPrefixTextBox"
                                        HelperControlID="InsertEquipmentSettingsStandardEquipmentPrefixTextBoxHelp"
                                        ShowHelperTextEventTrigger="OnFocus"
                                        HideHelperTextEventTrigger="OnBlur" />
        	                    </p>
                            </fieldset>

                            <fieldset class="controls">
                                <span class="button insert_button"><asp:Button ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert" /></span>
                                <span class="button cancel_button"><asp:Button ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" /></span>
                            </fieldset>
                        </InsertItemTemplate>
                        
                        <EditItemTemplate>
                            <fieldset class="equipment_form hform">
    	                        <legend>
    		                        Update Equipment Settings
    	                        </legend>
                        	    
	                            <p class="desc">Settings on this page impact the text output both by the 1-Click tool bar and the automatic ad builder in the Internet Advertising Accelerator.</p>
	                            
	                            <p>
                                    <asp:Label runat="server" ID="EditEquipmentSettingsSpacerTextLabel" Text="Spacer Text :" EnableViewState="false" AssociatedControlID="EditEquipmentSettingsSpacerTextTextBox" />
                                    <asp:TextBox ID="EditEquipmentSettingsSpacerTextTextBox" runat="server" Text='<%# Bind("SpacerText") %>' MaxLength="20" />
                                    
                                    <div runat="server" id="EditEquipmentSettingsSpacerTextTextBoxHelp" class="help_text help_arrow_left help_even">
                                        <span class="help_arrow"></span>
                                        <p>Add up to <em>5 characters</em> that will be inserted between attributes of your ads to add visual 
                                        flare and distinction. For instance, adding &ldquo; <em class="example">***</em> &rdquo; in the field will cause an ad for a 
                                        Certified, 2005 Honda Accord to be output in the automatic ad builder as, 
                                        &ldquo; <em class="example">*** 2005 Honda Accord *** Certified ***</em> &rdquo;
                                        <br />
                                        <small>(plus whatever additional attributes are available for the vehicle).</small></p>
                                    </div>
                                    
                                    <cwc:HelpTextExtender runat="server" ID="EditEquipmentSettingsSpacerTextTextBoxHelper"
                                        TargetControlID="EditEquipmentSettingsSpacerTextTextBox"
                                        HelperControlID="EditEquipmentSettingsSpacerTextTextBoxHelp"
                                        ShowHelperTextEventTrigger="OnFocus"
                                        HideHelperTextEventTrigger="OnBlur" />
                                </p>
                                <p>
                                    <asp:Label ID="EditEquipmentSettingsEquipmentProviderLabel" runat="server" Text="Vehicle Options Source :" AssociatedControlID="EditEquipmentSettingsEquipmentProviderDropDownList" EnableViewState="false" />
                                    <asp:DropDownList ID="EditEquipmentSettingsEquipmentProviderDropDownList" DataTextField="Name" DataValueField="ID" DataSourceID="AdvertisementEquipmentProviderCollectionObjectDataSource" runat="server" Text='<%# Bind("EquipmentProviderID") %>' />
                                    
                                    <div runat="server" id="EditEquipmentSettingsEquipmentProviderDropDownListHelp" class="help_text help_arrow_left help_even">
                                        <span class="help_arrow"></span>
                                        <p>Select the book from which equipment is derived for use in your ads.</p>
                                    </div>
                                    
                                    <cwc:HelpTextExtender runat="server" ID="EditEquipmentSettingsEquipmentProviderDropDownListHelper"
                                        TargetControlID="EditEquipmentSettingsEquipmentProviderDropDownList"
                                        HelperControlID="EditEquipmentSettingsEquipmentProviderDropDownListHelp"
                                        ShowHelperTextEventTrigger="OnFocus"
                                        HideHelperTextEventTrigger="OnBlur" />
        	                    </p>
        	                    <p>
                                    <asp:Label runat="server" ID="EditEquipmentSettingsHighValueEquipmentThresholdLabel" Text="High Value Equipment Threshold :" EnableViewState="false" AssociatedControlID="EditEquipmentSettingsHighValueEquipmentThresholdTextBox" />
                                    <asp:TextBox ID="EditEquipmentSettingsHighValueEquipmentThresholdTextBox" runat="server" Text='<%# Bind("HighValueEquipmentThreshold") %>' MaxLength="5" />
                                    
                                    <div runat="server" id="EditEquipmentSettingsHighValueEquipmentThresholdTextBoxHelp" class="help_text help_arrow_left help_even">
                                        <span class="help_arrow"></span>
                                        <p>Using the designated book above, FirstLook will use this threshold to segregate options based on price and automatically bring high-value options to the top of the list in order to add impact.</p>
                                    </div>
                                    
                                    <cwc:HelpTextExtender runat="server" ID="EditEquipmentSettingsHighValueEquipmentThresholdTextBoxHelper"
                                        TargetControlID="EditEquipmentSettingsHighValueEquipmentThresholdTextBox"
                                        HelperControlID="EditEquipmentSettingsHighValueEquipmentThresholdTextBoxHelp"
                                        ShowHelperTextEventTrigger="OnFocus"
                                        HideHelperTextEventTrigger="OnBlur" />
                                </p>
                                <p>
                                    <asp:Label runat="server" ID="EditEquipmentSettingsHighValueEquipmentPrefixLabel" Text="High Value Equipment Prefix :" EnableViewState="false" AssociatedControlID="EditEquipmentSettingsHighValueEquipmentPrefixTextBox" />
                                    <asp:TextBox ID="EditEquipmentSettingsHighValueEquipmentPrefixTextBox" runat="server" Text='<%# Bind("HighValueEquipmentPrefix") %>' MaxLength="150" />
                                    
                                    <div runat="server" id="EditEquipmentSettingsHighValueEquipmentPrefixTextBoxHelp" class="help_text help_arrow_left help_even">
                                        <span class="help_arrow"></span>
                                        <p>Create impact with a strong statement to lead into your high-value options list created using 
                                        the threshold above. For instance, <em class="example">&ldquo; This vehicle comes LOADED with: &rdquo;</em> which would then be followed 
                                        by a comma separated list of the vehicle's high value equipment.</p>
                                    </div>
                                    
                                    <cwc:HelpTextExtender runat="server" ID="EditEquipmentSettingsHighValueEquipmentPrefixTextBoxHelper"
                                        TargetControlID="EditEquipmentSettingsHighValueEquipmentPrefixTextBox"
                                        HelperControlID="EditEquipmentSettingsHighValueEquipmentPrefixTextBoxHelp"
                                        ShowHelperTextEventTrigger="OnFocus"
                                        HideHelperTextEventTrigger="OnBlur" />
                                </p>                                
                                <p>
                                    <asp:Label ID="EditEquipmentSettingsStandardEquipmentPrefixLabel" runat="server" Text="Standard Equipment Prefix :" AssociatedControlID="EditEquipmentSettingsStandardEquipmentPrefixTextBox" EnableViewState="false" />
                                    <asp:TextBox ID="EditEquipmentSettingsStandardEquipmentPrefixTextBox" runat="server" Text='<%# Bind("StandardEquipmentPrefix") %>' MaxLength="150" />
                                    
                                    <div runat="server" id="EditEquipmentSettingsStandardEquipmentPrefixTextBoxHelp" class="help_text help_arrow_left help_even">
                                        <span class="help_arrow"></span>
                                        <p>Again, using the lists created by the threshold, you may designate a prefix to lead into the 
                                        standard options available on the vehicle but likely deserving of less prominence in the 
                                        ad <small>(i.e. power door locks).</small></p>
                                    </div>
                                    
                                    <cwc:HelpTextExtender runat="server" ID="EditEquipmentSettingsStandardEquipmentPrefixTextBoxHelper"
                                        TargetControlID="EditEquipmentSettingsStandardEquipmentPrefixTextBox"
                                        HelperControlID="EditEquipmentSettingsStandardEquipmentPrefixTextBoxHelp"
                                        ShowHelperTextEventTrigger="OnFocus"
                                        HideHelperTextEventTrigger="OnBlur" />
        	                    </p>
                            </fieldset>

                            <fieldset class="controls">
                                <span class="button update_button"><asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update" /></span>
                                <span class="button cancel_button"><asp:Button ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" /></span>
                            </fieldset>
                        </EditItemTemplate>
                        
                    </asp:FormView>
                    
                    <cwc:Dialog runat="server" ID="ValidationErrorDialog"
                           Visible="true" Hidden="true"
                           TitleText="Validation Error"
                           Top="300" Left="310" Width="290" Height="150" AllowResize="false" AllowDrag="true"
                           OnButtonClick="ValidationErrorDialog_ButtonClick">
                        <ItemTemplate>
                            <asp:BulletedList ID="ErrorList" runat="server">
                            </asp:BulletedList>
                        </ItemTemplate>
                        
                        <Buttons>
                            <cwc:DialogButton ButtonType="button" ButtonCommand="Ok" Text="Ok" IsDefault="true" />
                        </Buttons>
                    </cwc:Dialog>    
                   
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        
        <cwc:PageFooter ID="SiteFooter" runat="server" />
    </form>
</body>
</html>
