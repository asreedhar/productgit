<%@ Page Language="C#" AutoEventWireup="true" Inherits="Preferences_TextFragments" Codebehind="TextFragments.aspx.cs" Theme="Leopard" %>

<%@ OutputCache Location="None" VaryByParam="None" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Page-Enter" content="RevealTrans(Duration=0,Transition=0)" />
    <meta http-equiv="Page-Exit" content="RevealTrans(Duration=0,Transition=0)" />
    <title>Pre-Owned Performance Management Center - Manage Taglines</title>
</head>
<body>
    <form id="TextFragmentForm" runat="server" class="preference_page">
    	
    	<asp:ScriptManager ID="scriptManager" EnablePartialRendering="true" EnablePageMethods="true" runat="server" LoadScriptsBeforeUI="false" ScriptPath="/resources/Scripts">
            <Scripts>
                <asp:ScriptReference Path="~/Public/Scripts/Lib/Combined.js" NotifyScriptLoaded="true" />
                <asp:ScriptReference Path="~/Public/Scripts/Lib/DD_Roundies.js" NotifyScriptLoaded="true" />
                <asp:ScriptReference Path="~/Public/Scripts/Pages/global.js" NotifyScriptLoaded="true" />
                <asp:ScriptReference Path="~/Public/Scripts/Pages/PreferencePage.js" NotifyScriptLoaded="true" />
            </Scripts>
        </asp:ScriptManager>

    	<owc:HedgehogTracking ID="Hedgehog" runat="server" />
    	
    	<div id="hd">
    		<asp:SiteMapDataSource ID="ApplicationSiteMapDataSource" runat="server"
				    ShowStartingNode="false"
				    SiteMapProvider="ApplicationSiteMap" />
		    <asp:Menu id="application_menu_container" runat="server"
				    CssClass="smallCaps blue"
				    Orientation="Horizontal"
				    DataSourceID="ApplicationSiteMapDataSource" />
		    <asp:SiteMapDataSource ID="MemberSiteMapDataSource" runat="server"
				    ShowStartingNode="false"
				    SiteMapProvider="MemberSiteMap" />
    	</div>
    	
        <div id="bd">
        	
        	<asp:SiteMapDataSource ID="PreferencesSiteMapDataSource" runat="server"
				    ShowStartingNode="false"
				    SiteMapProvider="PreferencesSiteMap" />
		    <asp:Menu id="PreferencesMenu" runat="server"
		            CssClass="preference_nav"
		            MaximumDynamicDisplayLevels="1"
				    Orientation="Horizontal"
				    OnMenuItemDataBound="PreferencesSiteMapDataSource_DataBound"
				    DataSourceID="PreferencesSiteMapDataSource" />
            <asp:SiteMapDataSource ID="PreferencesSiteSecondLevelMapDataSource" runat="server"
				    ShowStartingNode="false"
				    StartingNodeUrl="/pricing/Pages/Preferences/"
				    SiteMapProvider="PreferencesSiteMap" /> 
		    <asp:Menu id="PreferencesSecondLevelMenu" runat="server"
		            CssClass="preference_nav secondary_preference_nav"
		            MaximumDynamicDisplayLevels="1"
				    Orientation="Vertical"
				    OnMenuItemDataBound="PreferencesSiteMapDataSource_DataBound"
				    DataSourceID="PreferencesSiteSecondLevelMapDataSource" />
        
            <asp:ObjectDataSource ID="TextFragmentCollectionDataSource" runat="server"
                    TypeName="FirstLook.Pricing.DomainModel.Internet.ObjectModel.TextFragment.TextFragmentCollection+DataSource"
                    SelectMethod="Select"
                    DeleteMethod="Delete"
                    OnSelected="TextFragmentCollectionDataSource_Selected">
                <SelectParameters>
                    <asp:QueryStringParameter Name="ownerHandle" QueryStringField="oh" Type="String" ConvertEmptyStringToNull="true" />
                </SelectParameters>
                <DeleteParameters>
                    <asp:QueryStringParameter Name="ownerHandle" QueryStringField="oh" Type="String" ConvertEmptyStringToNull="true" />
                </DeleteParameters>
            </asp:ObjectDataSource>
            <asp:ObjectDataSource ID="TextFragmentDataSource" runat="server"
                    TypeName="FirstLook.Pricing.DomainModel.Internet.ObjectModel.TextFragment.TextFragment+DataSource"
                    SelectMethod="Select"
                    InsertMethod="Insert"
                    UpdateMethod="Update"
                    OnSelected="TextFragmentDataSource_Selected">
                <SelectParameters>
                    <asp:QueryStringParameter Name="ownerHandle" QueryStringField="oh" Type="String" ConvertEmptyStringToNull="true" />
                    <asp:ControlParameter ControlID="TextFragmentCollectionGridView" Name="textFragmentId" Type="Int32" DefaultValue="-1" />
                </SelectParameters>
                <InsertParameters>
                    <asp:QueryStringParameter Name="ownerHandle" QueryStringField="oh" Type="String" ConvertEmptyStringToNull="true" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:QueryStringParameter Name="ownerHandle" QueryStringField="oh" Type="String" ConvertEmptyStringToNull="true" />
                    <asp:ControlParameter ControlID="TextFragmentCollectionGridView" Name="textFragmentId" Type="Int32" DefaultValue="-1" />
                </UpdateParameters>
            </asp:ObjectDataSource>
            
            <asp:UpdatePanel runat="server" ID="FormViewUpdatePanel" ChildrenAsTriggers="true">
                <ContentTemplate>
                
                    <asp:FormView ID="TextFragmentFormView" runat="server"
                            CssClass="form_view"
                            DataSourceID="TextFragmentDataSource"
                            DefaultMode="Insert"
                            OnModeChanging="TextFragmentFormView_ModeChanging"
                            OnItemInserting="TextFragmentFormView_ItemInserting"
                            OnItemInserted="TextFragmentFormView_ItemInserted"
                            OnItemUpdating="TextFragmentFormView_ItemUpdating"
                            OnItemUpdated="TextFragmentFormView_ItemUpdated">
                        
                        <InsertItemTemplate>
                            <fieldset class="snippet_form vform">
    	                        <legend>
    		                        Add a New Snippet
    	                        </legend>
                        	    
    	                        <p class="desc">Your Snippet Library allows you to manage the snippets available in the 1-Click Toolbar of the Internet Advertising Accelerator.</p>

        	                    <p>
                                    <asp:Label runat="server" ID="InsertTextFragmentNameLabel" Text="Name :" AssociatedControlID="InsertTextFragmentNameTextBox" EnableViewState="false" />
                                    <asp:TextBox ID="InsertTextFragmentNameTextBox" runat="server" Text='<%# Bind("Name") %>' MaxLength="20" />
    		                    </p>

    		                    <p>
                                    <asp:Label runat="server" Id="InsertTextFragmentTextLabel" Text="Snippet :" AssociatedControlID="InsertTextFragmentTextTextBox" EnableViewState="false" />
                                    <asp:TextBox ID="InsertTextFragmentTextTextBox" runat="server" TextMode="MultiLine" Text='<%# Bind("Text") %>' Columns="50" Rows="7" />
                                </p>
                            </fieldset>

                            <fieldset class="controls">
                                <span class="button insert_button"><asp:Button ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert" /></span>
                                <span class="button cancel_button"><asp:Button ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" /></span>
                            </fieldset>
                        </InsertItemTemplate>
                        
                        <EditItemTemplate>
                            <fieldset class="snippet_form vform">
    	                        <legend>
    		                        Edit Snippet
    	                        </legend>
                        	    
	                            <p class="desc">Your Snippet Library allows you manage the snippets available in the 1-Click Toolbar of the Internet Advertising Accelerator.</p>
                    	        
                                <asp:HiddenField ID="EditTextFragmentIDHiddenField" runat="server" Value='<%# Bind("TextFragmentID") %>' />

                                <p>
                                    <asp:Label runat="server" ID="EditTextFragmentNameLabel" Text="Name :" EnableViewState="false" AssociatedControlID="EditTextFragmentNameTextBox" />
                                    <asp:TextBox ID="EditTextFragmentNameTextBox" runat="server" Text='<%# Bind("Name") %>' MaxLength="20" />
                                </p>

                                <p>
                                    <asp:Label ID="EditTextFragmentTextLabel" runat="server" Text="Snippet :" AssociatedControlID="EditTextFragmentTextTextBox" EnableViewState="false" />
                                    <asp:TextBox ID="EditTextFragmentTextTextBox" runat="server" TextMode="MultiLine" Text='<%# Bind("Text") %>' Columns="50" Rows="7" />
        	                    </p>
                            </fieldset>

                            <fieldset class="controls">
                                <span class="button update_button"><asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update" /></span>
                                <span class="button cancel_button"><asp:Button ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" /></span>
                            </fieldset>
                        </EditItemTemplate>
                        
                    </asp:FormView>
                    
                    <cwc:Dialog runat="server" ID="ValidationErrorDialog"
                           Visible="true" Hidden="true"
                           TitleText="Valiation Error"
                           Top="300" Left="310" Width="290" Height="150" AllowResize="false" AllowDrag="true"
                           OnButtonClick="ValidationErrorDialog_ButtonClick">
                        <ItemTemplate>
                            <asp:BulletedList ID="ErrorList" runat="server">
                            </asp:BulletedList>
                        </ItemTemplate>
                        
                        <Buttons>
                            <cwc:DialogButton ButtonType="button" ButtonCommand="Ok" Text="Ok" IsDefault="true" />
                        </Buttons>
                    </cwc:Dialog>    
                   
                </ContentTemplate>
            </asp:UpdatePanel>
            
            <asp:UpdatePanel runat="server" ID="GridViewUpdatePanel" ChildrenAsTriggers="true">
                <ContentTemplate>
                            
                    <asp:GridView ID="TextFragmentCollectionGridView" runat="server"
                            CssClass="grid snippet_report"
                            AlternatingRowStyle-CssClass="odd"
                            AutoGenerateColumns="false"
                            AutoGenerateDeleteButton="false"
                            AutoGenerateEditButton="false"
                            AutoGenerateSelectButton="false"
                            DataKeyNames="TextFragmentID"
                            DataSourceID="TextFragmentCollectionDataSource"
                            OnPreRender="TextFragmentCollectionGridView_PreRender"
                            OnDataBound="TextFragmentCollectionGridView_DataBound"
                            OnRowCommand="TextFragmentCollectionGridView_RowCommand"
                            OnRowDataBound="TextFragmentCollectionGridView_RowDataBound">
                        <Columns>                           
                            <asp:CommandField ShowHeader="false" ButtonType="Button" 
                                ShowSelectButton="true" 
                                ShowCancelButton="false" 
                                ShowDeleteButton="false" 
                                ShowEditButton="false" 
                                HeaderStyle-CssClass="select"
                                ItemStyle-CssClass="select"
                                SelectText="Edit" />
                            <asp:BoundField AccessibleHeaderText="Name" HeaderText="Name" 
                                ItemStyle-CssClass="snippet_name"
                                HeaderStyle-CssClass="snippet_name"
                                DataField="Name" 
                                ReadOnly="true" />
                            <asp:BoundField AccessibleHeaderText="Snippet" HeaderText="Snippet" 
                                ItemStyle-CssClass="snippet_text"
                                HeaderStyle-CssClass="snippet_text"
                                DataField="Text" 
                                ReadOnly="true" />
                            <asp:ButtonField ShowHeader="false" ButtonType="Button"
                                HeaderStyle-CssClass="hidden"
                                ItemStyle-CssClass="hidden"
                                CommandName="SetDefault"
                                Text="Set Default" />
                            <asp:TemplateField AccessibleHeaderText="Advertisement Generator Snippet" HeaderText="Advertisement Generator Snippet"
                                    ItemStyle-CssClass="snippet_is_default"
                                    HeaderStyle-CssClass="snippet_is_default">
                                <ItemTemplate>
                                    <asp:Literal runat="server" ID="RadioButtonLiteral" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:ButtonField CommandName="IncreaseRank" ButtonType="Button" 
                                ItemStyle-CssClass="move_up"
                                HeaderStyle-CssClass="move_up"
                                Text="&uarr;" 
                                ShowHeader="false" />
                            <asp:ButtonField CommandName="DecreaseRank" ButtonType="Button" 
                                ItemStyle-CssClass="move_down"
                                HeaderStyle-CssClass="move_down"
                                Text="&darr;"
                                ShowHeader="false" />
                            <asp:CommandField ShowHeader="false" ButtonType="Button" 
                                ShowSelectButton="false" 
                                ShowCancelButton="false" 
                                ShowDeleteButton="true" 
                                ShowEditButton="false" 
                                HeaderStyle-CssClass="delete only_show_with_javascript"
                                ItemStyle-CssClass="delete only_show_with_javascript"
                                DeleteText="&#215;" />
                        </Columns>
                    </asp:GridView>
                                
                </ContentTemplate>          
            </asp:UpdatePanel>
                        
        </div>
        
        <cwc:PageFooter ID="SiteFooter" runat="server" />
    </form>
</body>
</html>
