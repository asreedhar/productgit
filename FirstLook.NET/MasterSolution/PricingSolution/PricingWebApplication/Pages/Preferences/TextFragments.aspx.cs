using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Utilities;
using FirstLook.Common.WebControls.UI;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.TextFragment;

public partial class Preferences_TextFragments : Page
{
    private const string startingNodeUrl = "/pricing/Pages/Preferences/TextFragments.aspx";
    private bool canInsert = true;

    protected void Page_Render(object sender, EventArgs e)
    {
        //Page.ClientScript.RegisterForEventValidation(new PostBackOptions(TextFragmentCollectionGridView, "SetDefault"));
    }
	
	protected void TextFragmentCollectionDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
	{
		TextFragmentCollection collection = e.ReturnValue as TextFragmentCollection;

		if (collection == null || collection.Count == 0)
		{
			TextFragmentCollectionGridView.Visible = false;
		}
		else
		{
			TextFragmentCollectionGridView.Visible = true;
		}
	}

	protected void TextFragmentDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
	{
		if (e.ReturnValue == null)
		{
			TextFragmentFormView.ChangeMode(FormViewMode.Insert);
		}
	}

	protected void TextFragmentCollectionGridView_DataBound(object sender, EventArgs e)
	{
	    canInsert = TextFragmentCollectionGridView.Rows.Count >= 8;
	}

	protected void TextFragmentCollectionGridView_RowCommand(object sender, GridViewCommandEventArgs e)
	{
		if (e.CommandName.Equals("IncreaseRank") || e.CommandName.Equals("DecreaseRank"))
		{
			int selectedIndex = Int32Helper.ToInt32(e.CommandArgument);

			GridView grid = sender as GridView;

			if (grid != null)
			{
				int textFragmentId = Int32Helper.ToInt32(
					TextFragmentCollectionGridView.DataKeys[
						selectedIndex]["TextFragmentID"]);

				TextFragmentCollection collection = TextFragmentCollection.GetTextFragmentCollection(Request.QueryString["oh"]);

				if (e.CommandName.Equals("IncreaseRank"))
				{
					collection.IncreaseRank(textFragmentId);
				}
				else
				{
					collection.DecreaseRank(textFragmentId);
				}
				
				PostBackRedirect();
			}
		}
        else if (e.CommandName.Equals("SetDefault"))
        {
            int selectedIndex = Int32Helper.ToInt32(e.CommandArgument);

            GridView grid = sender as GridView;

            if (grid != null)
            {
                int textFragmentId = Int32Helper.ToInt32(
                    TextFragmentCollectionGridView.DataKeys[
                        selectedIndex]["TextFragmentID"]);

                TextFragmentCollection collection = TextFragmentCollection.GetTextFragmentCollection(Request.QueryString["oh"]);

                collection.SetDefault(textFragmentId);

                collection.Save();

                PostBackRedirect();
            }
        }
        else if (e.CommandName.Equals("Select"))
        {
            TextFragmentFormView.ChangeMode(FormViewMode.Edit);
        }
	}

    protected void TextFragmentFormView_ItemInserting(object sender, FormViewInsertEventArgs e)
	{
		string name = e.Values["Name"] as string;
		string text = e.Values["Text"] as string;
		bool inError = ValidateValues(name, text);
		e.Cancel = inError;
	}

	protected void TextFragmentFormView_ItemInserted(object sender, FormViewInsertedEventArgs e)
	{
		if (e.Exception != null)
		{
			HandleException(e.Exception);
			e.KeepInInsertMode = true;
			e.ExceptionHandled = true;
		}
		else
		{
			e.KeepInInsertMode = false;

			TextFragmentCollectionGridView.SelectedIndex = -1;

			PostBackRedirect();
		}
	}

	protected void TextFragmentFormView_ItemUpdating(object sender, FormViewUpdateEventArgs e)
	{
		string name = e.NewValues["Name"] as string;
		string text = e.NewValues["Text"] as string;
		bool inError = ValidateValues(name, text);
		e.Cancel = inError;
	}

	protected void TextFragmentFormView_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
	{
		if (e.Exception != null)
		{
			HandleException(e.Exception);
			e.KeepInEditMode = true;
			e.ExceptionHandled = true;
		}
		else
		{
			e.KeepInEditMode = false;

			TextFragmentCollectionGridView.SelectedIndex = -1;

			PostBackRedirect();
		}
	}

	private bool ValidateValues(string name, string text)
	{
	    BulletedList errorList = ValidationErrorDialog.FindControl("ErrorList") as BulletedList;

        if (errorList != null)
        {
            errorList.Items.Clear();

            if (string.IsNullOrEmpty(name))
            {
                errorList.Items.Add(new ListItem("Name cannot be empty"));
            }
            else if (name.Length > 20)
            {
                errorList.Items.Add(new ListItem("Name must be no greater than 20 characters"));
            }

            if (string.IsNullOrEmpty(text))
            {
                errorList.Items.Add(new ListItem("Text cannot be empty"));
            }
            else if (text.Length > 200)
            {
                errorList.Items.Add(new ListItem("Text must be no greater than 200 characters"));
            }

            bool inError = errorList.Items.Count > 0;

            ValidationErrorDialog.Hidden = !inError;

            return inError;
        }
        else
        {
            throw new NullReferenceException();
        }
	}

	private void HandleException(Exception exception)
	{
        BulletedList errorList = ValidationErrorDialog.FindControl("ErrorList") as BulletedList;

        if (errorList != null)
        {
            errorList.Items.Clear();

            errorList.Items.Add(exception.InnerException.Message);

            bool inError = errorList.Items.Count > 0;

            ValidationErrorDialog.Hidden = !inError;
        }
	}

    protected void ValidationErrorDialog_ButtonClick(object sender, DialogButtonClickEventArgs e)
    {
        ValidationErrorDialog.Hidden = true;
    }

    protected void PostBackRedirect()
    {
        if (scriptManager.IsInAsyncPostBack)
        {
            TextFragmentCollectionGridView.DataBind();
        }
        else
        {
            // This Function is used to prevent refresh errors with form and gridviews
            // without this deleteing a row and refreshing the page will cause another row to be deleted, PM
            Response.Redirect(Request.Url.ToString(), true);
        }
    }

	protected void PreferencesSiteMapDataSource_DataBound(object sender, MenuEventArgs e)
    {
        MenuItem item = e.Item;

        if (item.NavigateUrl == startingNodeUrl + "?oh=" + Request.QueryString["oh"])
        {
            item.Selected = true;
        }
    }

    protected void TextFragmentFormView_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        if (!canInsert && e.NewMode == FormViewMode.Insert)
        {
            Button insertButton = TextFragmentFormView.FindControl("InsertButton") as Button;

            if (insertButton != null)
            {
                insertButton.Enabled = false;
            }
        }
    }

    protected void TextFragmentCollectionGridView_PreRender(object sender, EventArgs e)
    {
        if (TextFragmentCollectionGridView.Rows.Count > 0)
        {
            TextFragmentCollectionGridView.Rows[0].Cells[5].Text = string.Empty;
            TextFragmentCollectionGridView.Rows[TextFragmentCollectionGridView.Rows.Count - 1].Cells[6].Text =
                string.Empty;
        }
    }

    protected void TextFragmentCollectionGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Literal radioButtonLiteral = (Literal)e.Row.FindControl("RadioButtonLiteral");
            WebControl webControl = sender as WebControl;
            TextFragment textFragment = e.Row.DataItem as TextFragment;

            if (radioButtonLiteral != null && webControl != null && textFragment != null)
            {
                radioButtonLiteral.Text =
                    string.Format(
                        "<input type='radio' name='IsDefaultRadioGroup' id='{0}${1}' value='{1}' {2} onclick=\"{3}\"/>",
                        "SetDefault",
                        e.Row.RowIndex,
                        textFragment.IsDefault ? "checked='checked'" : "",
                        "javascript:__doPostBack('TextFragmentCollectionGridView','SetDefault$" + e.Row.RowIndex + "')");
            }
        }
    }
}
