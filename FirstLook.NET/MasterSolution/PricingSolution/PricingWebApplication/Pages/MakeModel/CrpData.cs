﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CrpData
/// </summary>
[Serializable]
public class CrpData
{
    public Mk mk { get; set; }
    public List<Md> mds { get; set; }
}