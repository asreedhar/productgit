﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;
using System.Web.UI;


namespace FirstLook.Pricing.WebApplication.Pages.MakeModel
{
    public partial class MakeModels : Page
    {
        #region Private Variables

        private string connectionString = string.Empty;
        private string carsUrl = string.Empty;
        private int? providerId;

        #endregion

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                string[] stringSeparators = new string[] { "," };

                string[] result =
                    Convert.ToString(ConfigurationManager.AppSettings["UserAccess"]).Split(stringSeparators,
                                                                                           StringSplitOptions.None);

                foreach (string strName in result)
                {
                    if (User.Identity.Name == strName)
                    {
                        btnSubmit.Visible = true;
                    }
                }

                

            }
           

            connectionString = Convert.ToString(ConfigurationManager.ConnectionStrings["Market"]);
            providerId = Convert.ToInt32(ConfigurationManager.AppSettings["ProviderId"]);
            carsUrl = Convert.ToString(ConfigurationManager.AppSettings["CarsUrl"]);

        }

        #endregion

        #region Custom Events

        /// <summary>
        /// This method converts the Jason to dotnet classes
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            string CarsjasonData = string.Empty;
            CarsjasonData = GetCarsMakeModel();

            JavaScriptSerializer jss = new JavaScriptSerializer();
            RootObject rootObject = jss.Deserialize<RootObject>(CarsjasonData);

            GetJasonData(rootObject);

        }

        #endregion

        # region Connects the Casr Url for MakeModel Data

        /// <summary>
        /// This method connects the Casr Url for MakeModel Data
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        public string GetCarsMakeModel()
        {
            string responseData = string.Empty;

            try
            {

                WebRequest request =
                    WebRequest.Create(carsUrl);
                IWebProxy proxy = request.Proxy;

                if (proxy != null)
                {

                    //request.Proxy =WebRequest.DefaultWebProxy;
                    string proxyuri = proxy.GetProxy(request.RequestUri).ToString();
                    request.UseDefaultCredentials = true;
                    request.Proxy = new WebProxy(proxyuri, false);
                    request.Proxy.Credentials = CredentialCache.DefaultCredentials;
                    request.Method = "GET";
                    request.ContentType = "text/json";

                    WebResponse response = request.GetResponse();

                    StreamReader streamReader = new
                        StreamReader(response.GetResponseStream());

                    responseData = streamReader.ReadToEnd();

                }
            }
            catch (Exception)
            {
                Response.Write("Problem in connecting to Cars.com");
            }
            return responseData;


        }

        #endregion

        # region Method passes the Json Data

        /// <summary>
        /// Method passes the Jasan Data
        /// </summary>
        /// <param name="rootObject"></param>

        /// <returns></returns>
        public void GetJasonData(RootObject rootObject)
        {
            int updateResult = 0;
            SqlConnection con = new SqlConnection(connectionString);

            try
            {

                con.Open();
            }
            catch (Exception)
            {
                Response.Write("Problem in connecting to database");
                updateResult = 2;
            }
            if(updateResult==0)
            {
                foreach (CrpData data in rootObject.crpData)
                {
                    string make = Convert.ToString(data.mk.n);
                    string makeid = Convert.ToString(data.mk.id);

                    foreach (Md md in data.mds)
                    {

                       updateResult =  UpdateMakeModel(make, makeid, md.dn, md.did, con);
                    }
                }
            }
            con.Close();
            if (updateResult == 1)
            {
                lblStatus.Text = "Successfully Updated";
            }
        }

        #endregion

        # region Updates the Make Model in Database

        /// <summary>
        /// This method is used to Make Model
        /// </summary>
        /// <param name="make"></param>
        /// <param name="makeid"></param>
        /// <param name="model"></param>
        /// <param name="modelid"></param>
        /// <returns></returns>
        public int UpdateMakeModel(string make, string makeid, string model, string modelid, SqlConnection con)
        {
            try
            {

                SqlCommand cmd = new SqlCommand("Pricing.InsertUpdateMakeModel", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Make", make);
                cmd.Parameters.AddWithValue("@MakeId", makeid);
                cmd.Parameters.AddWithValue("@Model", model);
                cmd.Parameters.AddWithValue("@ModelId", modelid);
                cmd.Parameters.AddWithValue("@ProviderId", providerId);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                //Debug.WriteLine(model + "-" + modelid + DateTime.Now);
            }
            catch (Exception)
            {
                Response.Write("Problem in updating" + make + " and " + model);
                return 0;
            }
            return 1;
        }

        #endregion
    }
}