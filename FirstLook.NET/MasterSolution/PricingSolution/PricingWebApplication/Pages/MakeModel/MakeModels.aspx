﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MakeModels.aspx.cs" Inherits="FirstLook.Pricing.WebApplication.Pages.MakeModel.MakeModels" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <script src="../../Public/Scripts/Lib/jquery.js" type="text/javascript"></script>
    
    <title>MakeModel</title>
    <style type="text/css">

    .modal
    {

        position: fixed;
        top: 0;
        left: 0;
        background-color: black;
        z-index: 99;
        opacity: 0.8;
        filter: alpha(opacity=80);
        -moz-opacity: 0.8;
        min-height: 100%;
        width: 100%;
    }
    .loading
    {
        font-family: Arial;
        font-size: 10pt;
        border: 5px solid #67CFF5;
        width: 200px;
        height: 100px;
        display: none;
        position: fixed;
        background-color: White;
        z-index: 999;
    }

</style>

<script type="text/javascript">
    $(document).ready(function() {

        
        $("#btnSubmit").click(function() {

            setTimeout(function() {

                var modal = $('<div />');

                modal.addClass("modal");

                $('body').append(modal);

                var loading = $(".loading");

                loading.show();

                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);

                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);

                loading.css({ top: top, left: left });

            }, 200);

        });

    });


</script>
  
</head>
<body>
    <form id="frmModel" runat="server">
    <div>
     <table>
        <tr>
            
            <td>
                <asp:Button ID="btnSubmit" Text="Submitt" runat="server"  Visible="False"
                    onclick="btnSubmit_Click"/>

            </td>
        </tr>
       <tr>
           
           <td>
               <asp:Label runat="server" ID="lblStatus" ></asp:Label>
           </td>
       </tr>
    </table>
    </div>
 <div class="loading" align="center">

    Updating. Please wait.<br />

    <br />

    <img src="Images/loader.gif" alt="" />

</div>
    </form>
</body>
</html>
