﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Utilities;
using FirstLook.Common.WebControls.UI;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Marketing;
using FirstLook.Pricing.DomainModel.Internet.DataSource;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search;
using FirstLook.Pricing.WebControls;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator
{
    public partial class Preview : BasePage
    {
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (HasMaxAdUpgrade())
            {
                CustomizeMAXLink.Visible = true;

                ManageSessionValues();

                CustomizeMAXLink.NavigateUrl = 
                    string.Format("/pricing/Pages/MaxMarketing/Preferences/Dealer/MarketAnalysisDealerPreferences.aspx?{0}", 
                        Request.QueryString);
                
                MaxAdLink.Visible = true;
                MaxAdLink.NavigateUrl =
                    string.Format("/merchandising/Workflow/RedirectToInventoryItem.aspx?bu={0}&inv={1}",
                                  BusinessUnitObject.Id, InventoryObject.Id);
            }

            SimilarInventory.EnableStockCardLink = true; // TODO: Wire this up correctly --> !Calculator.IsSalesToolVehicle;
        }

        private void ManageSessionValues()
        {
            if(!string.IsNullOrEmpty(Request.QueryString[CommonKeys.OWNER_HANDLE]))
            {
                Session.Remove(CommonKeys.OWNER_HANDLE);
                Session.Add(CommonKeys.OWNER_HANDLE, Request.QueryString[CommonKeys.OWNER_HANDLE]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString[CommonKeys.SEARCH_HANDLE]))
            {
                Session.Remove(CommonKeys.SEARCH_HANDLE);
                Session.Add(CommonKeys.SEARCH_HANDLE, Request.QueryString[CommonKeys.SEARCH_HANDLE]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString[CommonKeys.VEHICLE_HANDLE]))
            {
                Session.Remove(CommonKeys.VEHICLE_HANDLE);
                Session.Add(CommonKeys.VEHICLE_HANDLE, Request.QueryString[CommonKeys.VEHICLE_HANDLE]);
            }
        }

        protected override void OnPreRenderComplete(EventArgs e)
        {
            base.OnPreRenderComplete(e);

            VehicleYearModelCtl.SetLogo( CertifiedBenefitsPreview.CertifiedProgramLogo );
            VehicleYearModelCtl.SetCarFaxIconVisible(ConsumerHighlightsPreview.ShowCarFaxOneOwnerLogo);
        }

        protected void SelectedEquipmentTabContainer_Init(object sender, EventArgs e)
        {
            VehicleInformationDataSource source = new VehicleInformationDataSource();

            DataTable providers = source.FindProvidersByOwnerHandleAndVehicleHandle(Request["oh"]);

            var uniqueProviders = providers.AsEnumerable().GroupBy(p => p["ProviderId"]).Select(g => g.First());
            bool setActiveTab = true;

            foreach (var provider in uniqueProviders)
            {
                

                int providerId = (int)provider["ProviderId"];

                TabPanel tabPanel = new TabPanel
                                        {
                                            ID = string.Format("EquipmentProvider-{0}", providerId),
                                            Text = string.Format("View {0} Equipment", provider["ProviderName"])
                                        };

                DataTable equipment = source.FindEquipmentByOwnerHandleAndVehicleHandle(
                    Request["oh"], Request["vh"], providerId);

                if (equipment.Rows.Count > 0)
                {
                    HtmlGenericControl ul = new HtmlGenericControl("ul");
                    tabPanel.Controls.Add(ul);

                    for (int j = 0, k = equipment.Rows.Count; j < k; j++)
                    {
                        DataRow item = equipment.Rows[j];

                        HtmlGenericControl li = new HtmlGenericControl("li") {InnerHtml = (string) item["OptionName"]};
                        ul.Controls.Add(li);
                    }
                }
                else
                {
                    HtmlGenericControl empty = new HtmlGenericControl("p")
                                                   {
                                                       InnerHtml = "This vehicle has no options selected."
                                                   };
                    tabPanel.Controls.Add(empty);
                }

                HtmlGenericControl copyright = new HtmlGenericControl("p")
                                                   {
                                                       InnerHtml = (string) provider["ProviderCopyright"]
                                                   };
                tabPanel.Controls.Add(copyright);

                SelectedEquipmentTabContainer.Tabs.Add(tabPanel);

                if (setActiveTab)
                {
                    setActiveTab = false;
                    SelectedEquipmentTabContainer.ActiveTabIndex = 0;
                }
            }
        }

        private bool HasMaxAdUpgrade()
        {
            bool maxAdActive = false;

            if (BusinessUnitObject != null)
            {
                maxAdActive = BusinessUnitObject.HasDealerUpgrade(Upgrade.Merchandising);
            }

            return maxAdActive;
        }

        protected int UnitsInStock
        {
            get
            {
                int? unitsInStock = Int32Helper.ToNullableInt32(ViewState["UnitsInStock"]);

                if (unitsInStock == null)
                {
                    ViewState["UnitsInStock"] =
                        unitsInStock =
                        new SimilarInventoryTableDataSource().CountByOwnerHandleAndVehicleHandle(Request["oh"], Request["vh"], false);
                }


                return unitsInStock.GetValueOrDefault();
            }
        }

        protected string PricingBrand
        {
            get { 
                string pricingBrand = ViewState["PricingBrand"] as string;

                if (string.IsNullOrEmpty(pricingBrand))
                {
                    var packageDetails = PackageDetails.GetDealerPricingPackageDetails(Common.Handles.OwnerHandle,
                                                                                 Common.Handles.SearchHandle,
                                                                                 Common.Handles.VehicleHandle);
                    ViewState["PricingBrand"] = pricingBrand = packageDetails.PageBrand.ToUpper();
                }

                return pricingBrand;
            }
        }

        protected string UnitsInStockText
        {
            get
            {
                if (UnitsInStock == 1) return string.Format("1 Unit in Stock");

                return string.Format("{0} Units in Stock", UnitsInStock);
            }
        }

        protected void SearchTypeID_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadSearchTypeId();
            }
        }

        private void LoadSearchTypeId()
        {
            SearchSummaryCollection collection = SearchHelper.GetSearchSummaryCollection(Context);

            SearchSummary summary = collection.ActiveSearchSummary;

            if (summary != null)
            {
                switch (summary.SearchType)
                {
                    case SearchType.YearMakeModel:
                        SearchTypeID.Value = "1";
                        break;
                    case SearchType.Precision:
                        SearchTypeID.Value = "4";
                        break;
                }
            }
        }


        // Adjust layout of RadioButtonList
        protected void IncludeCheckBoxList_PreRender(object sender, EventArgs e)
        {
            ((CheckBoxList)sender).Items[0].Attributes.CssStyle.Add("display", "block");
            ((CheckBoxList)sender).Items[1].Attributes.CssStyle.Add("display", "block");
            ((CheckBoxList)sender).Items[2].Attributes.CssStyle.Add("display", "block");
            ((CheckBoxList)sender).Items[3].Attributes.CssStyle.Add("margin-right", "20px");
        }

     
    }
}
