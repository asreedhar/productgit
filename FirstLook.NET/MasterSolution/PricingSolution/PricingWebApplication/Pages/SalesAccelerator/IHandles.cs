namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator
{
    public interface IHandles
    {
        string OwnerHandle { get; set; }
        string VehicleHandle { get; set; }
        string SearchHandle { get; set; }
    }
}