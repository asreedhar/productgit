<%@ Page Theme="" EnableViewState="false" Language="C#" AutoEventWireup="True" CodeBehind="Print.aspx.cs" Inherits="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Print" %>

<%@ Register TagName="DealerSummary" TagPrefix="sawc" Src="~/Pages/SalesAccelerator/Controls/DealerSummary.ascx" %>
<%@ Register TagName="VehicleImage" TagPrefix="sawc" Src="~/Pages/SalesAccelerator/Controls/VehicleImage.ascx" %>
<%@ Register TagName="VehicleYearModel" TagPrefix="sawc" Src="~/Pages/SalesAccelerator/Controls/VehicleYearModel.ascx" %>
<%@ Register TagName="VehicleSummary" TagPrefix="sawc" Src="~/Pages/SalesAccelerator/Controls/VehicleSummary.ascx" %>
<%@ Register TagName="ConsumerHighlights" TagPrefix="sawc" Src="~/Pages/SalesAccelerator/Controls/ConsumerHighlights.ascx" %>
<%@ Register TagName="VehicleFeatures" TagPrefix="sawc" Src="~/Pages/SalesAccelerator/Controls/VehicleFeatures.ascx" %>
<%@ Register TagName="CertifiedBenefits" TagPrefix="sawc" Src="~/Pages/SalesAccelerator/Controls/CertifiedBenefits.ascx" %>
<%@ Register TagName="PricingAnalysis" TagPrefix="sawc" Src="~/Pages/SalesAccelerator/Controls/PricingAnalysis.ascx" %>
<%@ Register TagName="MarketListings" TagPrefix="sawc" Src="~/Pages/SalesAccelerator/Controls/MarketListings.ascx" %>
<%@ Register TagName="OfferFooter" TagPrefix="sawc" Src="~/Pages/SalesAccelerator/Controls/OfferFooter.ascx" %>


<html>
<body>

<form id="PrintForm" runat="server">

<sawc:DealerSummary ID="DealerSummary1" runat="server" Visible="false"/>
<sawc:VehicleYearModel id="VehicleYearModel1" runat="server" Visible="false"/>
<sawc:VehicleImage ID="VehicleImage1" runat="server" Visible="false"/>
<sawc:VehicleSummary ID="VehicleSummary1" runat="server" Visible="false"/>
<sawc:VehicleFeatures ID="VehicleFeatures1" runat="server" Visible="false"/>
<sawc:ConsumerHighlights ID="ConsumerHighlights1" runat="server" Visible="false"/>
<sawc:CertifiedBenefits ID="CertifiedBenefits1" runat="server" Visible="false"/>
<sawc:PricingAnalysis ID="PricingAnalysis1" runat="server" Visible="false"/>
<sawc:MarketListings ID="MarketListings1" runat="server" Visible="false"/>

</form>

</body>
</html>