﻿using System;
 
namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator
{
    [Serializable]
    public class CustomerPacketPublishingOptions
    {
        public CustomerPacketPublishingOptions()
        {
            Customer = new Customer();
            SalesPerson = new SalesPerson();

            DownPayment = 0;
            MonthlyPayment = 0;

            SalesPacketOptions = 0;
            CarFaxReport = false;
            AutoCheckReport = false;

            Print = false;
            Save = false;
            Email = false;
        }

        public Customer Customer {get;set;}
        public SalesPerson SalesPerson { get; set; }

        public SalesPackOptions SalesPacketOptions { get; set; }
        public bool CarFaxReport { get; set; }
        public bool AutoCheckReport { get; set; }

        public int DownPayment { get; set; }
        public int MonthlyPayment { get; set; }

        public bool Print { get; set; }
        public bool Save { get; set; }
        public bool Email { get; set; }

    }

    [Flags]
    public enum SalesPackOptions
    {
        MarketComparison = 0x1,
        SellingSheet = 0x2
    }

    [Serializable]
    public class Customer
    {
        public Customer()
        {
            FirstName = string.Empty;
            LastName = string.Empty;
            PhoneNumber = string.Empty;
            Email = string.Empty;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
    }

    [Serializable]
    public class SalesPerson
    {
        public SalesPerson()
        {
            Name = string.Empty;
        }

        public string Name { get; set; }
    }

}
