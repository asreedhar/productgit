﻿using System;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator
{
    [Serializable]
    public enum NumberType
    {
        Number,
        Currency
    }
}