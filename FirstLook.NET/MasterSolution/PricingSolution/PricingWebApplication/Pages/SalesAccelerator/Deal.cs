﻿using System;
using FirstLook.Common.Core;
using FirstLook.Pricing.DomainModel.Marketing.AdditionalSettings;
using FirstLook.Pricing.DomainModel.Marketing.MarketAnalysis;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator
{
    [Serializable]
    public class Deal : IDeal
    {
        // Ctor
        public Deal(ILogger logger, string ownerHandle, string vehicleHandle, string searchHandle)
        {
            OwnerHandle = ownerHandle;
            VehicleHandle = vehicleHandle;
            SearchHandle = searchHandle;

            Logger = logger;
        }

        // Private member variables
        private CustomerPacketPublishingOptions _customerPacketOptions;
        private ValueAnalyzerPacketPublishingOptions _valueAnalyzerPacketOptions;
        private int? _offerPrice;
        private string _validUntilDate;
        private string _dealerDisclaimer;
        private Packets? _packetType;
        private IMarketAnalysisData _marketAnalysisData;

        // Injected dependencies
        public ILogger Logger { get; set; }

        // Properties
        public String OwnerHandle { get; set; }
        public string VehicleHandle { get; set; }
        public string SearchHandle { get; set; }

        public CustomerPacketPublishingOptions CustomerPacketOptions
        {
            get
            {
                if( _customerPacketOptions == null )
                {
                    _customerPacketOptions = new CustomerPacketPublishingOptions() { SalesPacketOptions = SalesPackOptions.SellingSheet };
                }

                return _customerPacketOptions;

            }
            set { _customerPacketOptions = value; }
        }

        public ValueAnalyzerPacketPublishingOptions ValueAnalyzerPacketOptions
        {
            get
            {
                if( _valueAnalyzerPacketOptions == null )
                {
                    _valueAnalyzerPacketOptions = new ValueAnalyzerPacketPublishingOptions();
                }
                return _valueAnalyzerPacketOptions;

            }
            set { _valueAnalyzerPacketOptions = value; }
        }

        public int OfferPrice
        {
            get
            {
                if( !_offerPrice.HasValue )
                {
                    try
                    {
                        // Get the list price (aka "current internet price")
                        MarketAnalysisPricingInformation pricingInformation =
                            MarketAnalysisPricingInformation.GetMarketAnalysisPricingInformation(OwnerHandle, VehicleHandle,
                                                                                                 SearchHandle);

                        int? listPrice = pricingInformation.ListPrice;

                        // If null was encountered, use 0.
                        _offerPrice = listPrice.GetValueOrDefault(0);
                    }
                    catch (Exception ex)
                    {
                        Logger.Log(ex);
                        _offerPrice = 0;
                    }
                }
                return _offerPrice.Value;

            }
            set { _offerPrice = value; }
        }

        public string ValidUntilDate
        {
            get
            {
                if ( String.IsNullOrEmpty(_validUntilDate) )
                {
                    // Get the dealer "valid until" setting
                    int daysValid = DealerGeneralPreference.Exists(OwnerHandle) ?
                                    DealerGeneralPreference.GetDealerGeneralPreference(OwnerHandle).DaysValidFor : 0;

                    // Valid until date is today + daysValid
                    DateTime validUntilDate = DateTime.Now.AddDays(daysValid);

                    // Append values.
                    _validUntilDate = validUntilDate.ToShortDateString();
                }

                return _validUntilDate;
            }
            set { _validUntilDate = value; }
        }

        public string DealerDisclaimer
        {
            get
            {
                if (  _dealerDisclaimer == null )
                {
                    // Get the dealer disclaimer setting
                    _dealerDisclaimer = DealerGeneralPreference.Exists(OwnerHandle) ? 
                        DealerGeneralPreference.GetDealerDisclaimer(OwnerHandle) : string.Empty;
                }

                return _dealerDisclaimer;
            }
            set { _dealerDisclaimer = value; }
        }

        public Packets PacketType
        {
            get
            {
                if (!_packetType.HasValue)
                {
                    throw new ApplicationException("Could not determine packet type.");
                }

                return _packetType.Value;
            }
            set { _packetType = value; }
        }

        public IMarketAnalysisData MarketAnalysis
        {
            get
            {
                if( _marketAnalysisData == null )
                {
                    _marketAnalysisData = new MarketAnalysisData();

                    _marketAnalysisData.DealerPreference =
                        MarketAnalysisDealerPreference.GetOrCreateMarketAnalysisDealerPreference(OwnerHandle);

                    _marketAnalysisData.VehiclePreference =
                        MarketAnalysisVehiclePreference.GetOrCreateMarketAnalysisVehiclePreference(OwnerHandle,
                                                                                                   VehicleHandle);

                    _marketAnalysisData.Pricing =
                        MarketAnalysisPricingInformation.GetMarketAnalysisPricingInformation(
                            _marketAnalysisData.VehiclePreference, SearchHandle);
                }

                return _marketAnalysisData;
            }
            set { _marketAnalysisData = value; }
        }

        public void Reset()
        {
            _customerPacketOptions = null;
            _valueAnalyzerPacketOptions = null;
            _offerPrice = null;
            _validUntilDate = null;
            _dealerDisclaimer = null;
            _packetType = Packets.SalesPacket;
            _marketAnalysisData = null;
        }

        public void ResetMarketAnalysis()
        {
            _marketAnalysisData = null;
        }
    }

    
}
