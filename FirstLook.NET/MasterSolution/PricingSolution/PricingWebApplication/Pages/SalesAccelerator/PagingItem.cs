﻿using System;


namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator
{
    public class PagingItem
    {
        public string DisplayPage { get; set; }
        public int PageNumber { get; set; }
        public bool IsLink { get; set; }
        public bool IsSelected { get; set; }
    }
}
