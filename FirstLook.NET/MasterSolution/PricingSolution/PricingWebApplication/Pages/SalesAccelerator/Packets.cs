﻿using System;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator
{
    [Serializable]
    public enum Packets
    {
        SalesPacket,
        ValueAnalyzer
    }
}