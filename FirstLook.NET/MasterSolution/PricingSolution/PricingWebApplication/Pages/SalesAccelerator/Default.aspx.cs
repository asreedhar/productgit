using System;
using System.Web.UI;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator
{
    public partial class Default : Page
    {
        private const string PreviewPage = "~/Pages/MaxMarketing/Preview.aspx";

        private string OwnerHandle
        {
            get
            {
                return Request.QueryString["oh"];
            }
        }

        private string VehicleHandle
        {
            get
            {
                return Request.QueryString["vh"];
            }
        }

        private string SearchHandle
        {
            get
            {
                return Request.QueryString["sh"];
            }
        }


        protected void Page_init(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(OwnerHandle) || string.IsNullOrEmpty(VehicleHandle))
            {
                throw new ApplicationException("Must have an Owner Handle and Vehicle Handle");
            }

            Response.Redirect(string.Format("{0}?oh={1}&vh={2}&sh={3}", PreviewPage, OwnerHandle, VehicleHandle, SearchHandle), true);
        }
    }
}
