﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="Preview.aspx.cs" Inherits="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Preview" %>

<%@ Register Src="~/Pages/Internet/Controls/VehicleInformation/VehicleInformation.ascx" TagPrefix="Controls" TagName="VehicleInformation" %>

<%@ Register Src="~/Pages/Internet/Controls/VehicleNavigator.ascx" TagPrefix="Controls" TagName="VehicleNavigator" %>

<%@ Register Src="~/Pages/Internet/Controls/SimilarInventory.ascx" TagPrefix="Controls" TagName="SimilarInventory" %>

<%@ Register Src="~/Pages/SalesAccelerator/Controls/PreviewNavigationForm.ascx" TagPrefix="cva" TagName="PreviewNavigationForm" %>

<%@ Register Src="~/Pages/SalesAccelerator/Controls/DealerSummary.ascx" TagPrefix="cva" TagName="DealerSummary" %>

<%@ Register Src="~/Pages/SalesAccelerator/Controls/VehicleYearModel.ascx" TagPrefix="cva" TagName="VehicleYearModel" %>

<%@ Register Src="~/Pages/SalesAccelerator/Controls/VehicleImage.ascx" TagPrefix="cva" TagName="VehicleImage" %>

<%@ Register Src="~/Pages/SalesAccelerator/Controls/VehicleSummary.ascx" TagPrefix="cva" TagName="VehicleSummary" %>

<%@ Register Src="~/Pages/SalesAccelerator/Controls/VehicleFeatures.ascx" TagPrefix="cva" TagName="VehicleFeatures" %>
<%@ Register Src="~/Pages/SalesAccelerator/Controls/Preferences/VehicleFeaturesPreference.ascx" TagPrefix="cva" TagName="VehicleFeaturesPreference" %>

<%@ Register Src="~/Pages/SalesAccelerator/Controls/ConsumerHighlights.ascx" TagPrefix="cva" TagName="ConsumerHighlights" %>
<%@ Register Src="~/Pages/SalesAccelerator/Controls/Preferences/HighlightsVehiclePreference.ascx" TagPrefix="cva" TagName="HighlightsVehiclePreference" %>

<%@ Register Src="~/Pages/SalesAccelerator/Controls/VehicleDescription.ascx" TagPrefix="cva" TagName="VehicleDescription" %>
<%@ Register Src="~/Pages/SalesAccelerator/Controls/Preferences/AdPreviewVehiclePreference.ascx" TagPrefix="cva" TagName="AdPreviewVehiclePreference" %>

<%@ Register Src="~/Pages/SalesAccelerator/Controls/CertifiedBenefits.ascx" TagPrefix="cva" TagName="CertifiedBenefits" %>
<%@ Register Src="~/Pages/SalesAccelerator/Controls/Preferences/BenefitsVehiclePreference.ascx" TagPrefix="cva" TagName="BenefitsVehiclePreference" %>

<%@ Register Src="~/Pages/SalesAccelerator/Controls/PaymentsInformation.ascx" TagPrefix="cva" TagName="PaymentsInformation" %>

<%@ Register Src="~/Pages/SalesAccelerator/Controls/PricingAnalysis.ascx" TagPrefix="cva" TagName="PricingAnalysis" %>
<%@ Register Src="~/Pages/SalesAccelerator/Controls/Preferences/MarketAnalysisVehiclePreference.ascx" TagPrefix="cva" TagName="MarketAnalysisVehiclePreference" %>

<%@ Register Src="~/Pages/SalesAccelerator/Controls/MarketListings.ascx" TagPrefix="cva" TagName="MarketListings" %>
<%@ Register Src="~/Pages/SalesAccelerator/Controls/Preferences/MarketListingVehiclePreference.ascx" TagPrefix="cva" TagName="MarketListingVehiclePreference" %>

<%@ Register Src="~/Pages/SalesAccelerator/Controls/OfferFooter.ascx" TagPrefix="cva" TagName="OfferFooter" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>MAX Merchandising</title>
</head>
<body>
<form id="form1" runat="server">
<asp:ScriptManager ID="PreferencesScriptManager" runat="server" 
        EnablePartialRendering="true" 
        LoadScriptsBeforeUI="false">
    <Scripts>
        <asp:ScriptReference Path="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" />
        <asp:ScriptReference Path="https://ajax.googleapis.com/ajax/libs/jqueryui/1.7.3/jquery-ui.min.js" />
        <asp:ScriptReference Path="~/Public/Scripts/Lib/jquery.tablesorter.js" />
        <asp:ScriptReference Path="~/Public/Scripts/Lib/json2.js" />
        <asp:ScriptReference Path="~/Public/Scripts/Lib/DefaultButtonMozillaFix.js" />
        <asp:ScriptReference Path="~/Public/Scripts/Pages/SA.main.js?rev=8.0" /><%-- ScriptMode="Debug" --%>
    </Scripts>
</asp:ScriptManager>
<owc:HedgehogTracking ID="Hedgehog" runat="server" />
<div id="Container">
    <div id="VehicleInfoHeader" class="moduleContainer">
        <div id="header">
            <div id="vpa">
                <Controls:VehicleNavigator ID="VehicleNavigator" runat="server" />
                <div id="hd" class="clearfix">
                    <asp:UpdatePanel ID="VehicleInformationUpdatePanel" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                        <ContentTemplate>
                            <Controls:VehicleInformation ID="VehicleInformation" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="conditional">
                    <ContentTemplate>
                        <asp:ObjectDataSource ID="NationalAuctionPanelDataSource" runat="server" TypeName="FirstLook.Pricing.DomainModel.Internet.DataSource.NationalAuctionParametersDataSource" SelectMethod="FindByOwnerHandleAndVehicleHandle">
                            <SelectParameters>
                                <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
                                <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="vehicleHandle" Type="String" QueryStringField="vh" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <cwc:TabContainer ID="HeaderTabs" CssClass="vpa_header_info_tabs" runat="server" HasEmptyState="true">
                            <cwc:TabPanel ID="NationalAustionTab" Text="NAAA Auction" CssClass="national_auction_tab" runat="server">
                                <owc:NationalAuctionPanel ID="NationalAuctionPanel" runat="server" DataSourceID="NationalAuctionPanelDataSource" CssClass="vpa_auction_data" />
                            </cwc:TabPanel>
                            <cwc:TabPanel ID="SelectedEquipmentTab" Text="Selected Equipment" CssClass="selected_equipment_tab" runat="server">
                                <cwc:TabContainer ID="SelectedEquipmentTabContainer" runat="server" CssClass="vpa_selected_equipment" OnInit="SelectedEquipmentTabContainer_Init">
                                    <!-- populated in the oninit callback -->
                                </cwc:TabContainer>
                            </cwc:TabPanel>
                            <cwc:TabPanel ID="UnitsInStockTab" Text='<%# UnitsInStockText %>' CssClass="units_in_stock_tab" runat="server">
                                <Controls:SimilarInventory ID="SimilarInventory" runat="server" />
                            </cwc:TabPanel>
                        </cwc:TabContainer>
                    </ContentTemplate>
                </asp:UpdatePanel>
                
                <asp:UpdatePanel ID="SearchTypeUpdatePanel" runat="server" UpdateMode="conditional">
                    <ContentTemplate>
                        <asp:HiddenField ID="SearchTypeID" runat="server" Value="1" OnInit="SearchTypeID_Init" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                
                <asp:ObjectDataSource ID="MarketPricingListObjectDataSource" runat="server" TypeName="FirstLook.Pricing.DomainModel.Internet.DataSource.MarketPricingDataSource"
             SelectMethod="FindListByOwnerHandleAndSearchHandle">
                <SelectParameters>
                    <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="ownerHandle" Type="String" QueryStringField="oh" />
                    <asp:QueryStringParameter ConvertEmptyStringToNull="True" Name="searchHandle" Type="String" QueryStringField="sh" />
                    <asp:ControlParameter ConvertEmptyStringToNull="True" Name="searchTypeId" Type="Int32" ControlID="SearchTypeID" />
                </SelectParameters>
            </asp:ObjectDataSource>
            </div>
        </div>
    </div>
    <div id="PricingTabs">
        <ul class="selected1">
            <li id="IAATab"><a href="<%= Request.ApplicationPath + "/Pages/Internet/VehiclePricingAnalyzer.aspx?" + Request.QueryString %>"><span><%= PricingBrand %></span></a></li>
            <li id="CVATab"><a href="#" onclick="return false;"><span>MAX MARKETING</span></a>
            <asp:HyperLink runat="server" ID="CustomizeMAXLink" Visible="false" CssClass="small">MAX Display Rules</asp:HyperLink>
            <asp:Hyperlink runat="server" ID="MaxAdLink" Visible="false" NavigateUrl="/merchandising/Workflow/Inventory.aspx" CssClass="small" Target="_top">Go to MAX AD &gt;</asp:Hyperlink></li>
        </ul>
    </div>
    <div id="CVAContent" class="clearfix">
        <cva:PreviewNavigationForm ID="PreviewNavigationForm" runat="server" />
        <div id="CVAModules" class="clearfix">
            <div id="MarketComparisonHead">
                <h2>Market Comparison Analysis</h2>
            </div>
            <div id="DealerSummary" class="clearfix<%= DealerSummary.ShowDealerLogo ? string.Empty : " noDealerLogo" %>">
                <pwc:ExtendedUpdatePanel ID="DealerSummaryPanel" runat="server">
                    <ContentTemplate>
                        <cva:DealerSummary ID="DealerSummary" runat="server" />
                    </ContentTemplate>
                </pwc:ExtendedUpdatePanel>
            </div>
            <div id="VehicleYearModel" class="clearfix">
                <pwc:ExtendedUpdatePanel ID="VehicleYearModelPanel" runat="server">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="HighlightsVehiclePreference" />
                    </Triggers>
                    <ContentTemplate>
                        <cva:VehicleYearModel ID="VehicleYearModelCtl" runat="server" />
                    </ContentTemplate>
                </pwc:ExtendedUpdatePanel>
            </div>
            <div id="VehicleSpecs" class="clearfix">
                <div id="VehicleSummaryPanel" class="clearfix<%= VehicleImage.DisplayPhoto ? string.Empty : " noVehiclePhoto" %>">

                    <pwc:ExtendedUpdatePanel ID="VehicleImagePanel" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="PreviewNavigationForm" />
                        </Triggers>
                        <ContentTemplate>
                            <cva:VehicleImage ID="VehicleImage" runat="server" />
                        </ContentTemplate>
                    </pwc:ExtendedUpdatePanel>
                    
                    <cva:VehicleSummary ID="VehicleSummary" runat="server" />
                </div>
                
                <pwc:ExtendedUpdatePanel ID="FeaturesUpdatePanel" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="PreviewNavigationForm" />
                        <asp:AsyncPostBackTrigger ControlID="MarketAnalysisVehiclePreference" />
                    </Triggers>
                    <ContentTemplate>
                        <div id="VehicleFeatures" class="clearfix moduleContainer">
                            
                            <div id="EditFeaturesPanel" class="editModulePanel">
                                <h3>EDIT EQUIPMENT</h3>
                                <cva:VehicleFeaturesPreference ID="VehicleFeaturesPreference" runat="server" />
                            </div>
                            
                            <div id="ViewFeaturesPanel" class="viewModulePanel clearfix">
                                <cva:VehicleFeatures ID="VehicleFeaturesPreview" runat="server" />
                            </div>
                        </div>
                    </ContentTemplate>
                </pwc:ExtendedUpdatePanel>
                
                <pwc:ExtendedUpdatePanel ID="HighlightsUpdatePanel" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="PreviewNavigationForm" />
                    </Triggers>
                    <ContentTemplate>
                        <div id="ConsumerHighlights" class="clearfix moduleContainer">
                            
                            <div id="EditHighlightsPanel" class="editModulePanel">
                                <h3>EDIT HIGHLIGHTS</h3>
                                <cva:HighlightsVehiclePreference id="HighlightsVehiclePreference" runat="server" />
                            </div>
                            
                            <div id="ViewHighlightsPanel" class="viewModulePanel">
                                <cva:ConsumerHighlights ID="ConsumerHighlightsPreview" runat="server" />
                            </div>
                            
                        </div>
                    </ContentTemplate>
                </pwc:ExtendedUpdatePanel>
                
                <pwc:ExtendedUpdatePanel ID="VehicleDescriptionUpdatePanel" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="PreviewNavigationForm" />
                    </Triggers>
                    <ContentTemplate>
                        <div id="VehicleDescription" class="clearfix moduleContainer">
                            <div id="EditVehicleDescription" class="editModulePanel">
                                <h3>EDIT AD PREVIEW</h3>
                                <cva:AdPreviewVehiclePreference ID="AdPreviewVehiclePreference" runat="server" />
                            </div>
                            
                            <div id="ViewVehicleDescription">
                                <div class="viewModulePanel">
                                    <cva:VehicleDescription ID="VehicleDescriptionPreview" runat="server" />
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </pwc:ExtendedUpdatePanel>
                
                <div id="VehicleSummaryBtm">&nbsp;</div>
            </div>
            
            <pwc:ExtendedUpdatePanel ID="CertifiedBenefitsUpdatePanel" runat="server" UpdateMode="Conditional">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="PreviewNavigationForm" />
                </Triggers>
                <ContentTemplate>
                    <div id="CertifiedBenefits" class="clearfix moduleContainer">
                            
                        <div id="EditCertifiedBenefitsPanel" class="editModulePanel">
                            <h3>EDIT CERTIFIED BENEFITS</h3>
                            <cva:BenefitsVehiclePreference id="BenefitsVehiclePreference" runat="server" />
                        </div>
                        
                        <div id="ViewCertifiedBenefitsPanel" class="viewModulePanel clearfix">
                            <cva:CertifiedBenefits ID="CertifiedBenefitsPreview" runat="server" />
                        </div>
                    </div>
                </ContentTemplate>
            </pwc:ExtendedUpdatePanel>
            
            <pwc:ExtendedUpdatePanel ID="PaymentInfoUpdatePanel" runat="server" UpdateMode="Conditional">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="PreviewNavigationForm" />
                </Triggers>
                <ContentTemplate>
                    <div class="viewModulePanel">
                        <cva:PaymentsInformation id="PaymentsInformation" runat="server"></cva:PaymentsInformation>
                    </div>
                </ContentTemplate>
            </pwc:ExtendedUpdatePanel>

            <pwc:ExtendedUpdatePanel ID="PricingAnalysisUpdatePanel" runat="server" UpdateMode="Conditional">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="PreviewNavigationForm" />
                    <asp:AsyncPostBackTrigger ControlID="VehicleFeaturesPreference" />
                </Triggers>
                <ContentTemplate>        
                    <div id="PricingAnalysis" class="clearfix moduleContainer">
                            
                        <div id="EditPricingAnalysisPanel" class="editModulePanel">
                            <h3>EDIT PRICING ANALYSIS</h3>
                            <cva:MarketAnalysisVehiclePreference id="MarketAnalysisVehiclePreference" runat="server" />
                        </div>
                        
                        <div id="ViewPricingAnalysisPanel" class="viewModulePanel">
				            <cva:PricingAnalysis ID="PricingAnalysisPreview" runat="server" ShowPriceList="true" />
                        </div>
                    </div>
                </ContentTemplate>
            </pwc:ExtendedUpdatePanel>
            
            <pwc:ExtendedUpdatePanel ID="MarketListingsUpdatePanel" runat="server" UpdateMode="Conditional">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="PreviewNavigationForm" />
                </Triggers>
                <ContentTemplate>
                    <div id="MarketListings" class="clearfix moduleContainer">
                        
                        <div id="EditMarketListingsPanel" class="editModulePanel">
                            <h3>EDIT MARKET LISTINGS</h3>
                            <cva:MarketListingVehiclePreference id="MarketListingVehiclePreference" runat="server" />
                        </div>
                        
                        <div id="ViewMarketListingsPanel" class="viewModulePanel">
                            <cva:MarketListings ID="MarketListingsPreview" runat="server" LayoutKey="SellingSheet" />
                        </div>
                        
                        <div id="MarketComparisonListings" class="viewModulePanel">
                            <cva:MarketListings ID="MarketListings1" runat="server" LayoutKey="MarketComparison" />
                        </div>
                        
                    </div>
                </ContentTemplate>
            </pwc:ExtendedUpdatePanel>
            
            <pwc:ExtendedUpdatePanel ID="FooterPanel" runat="server" UpdateMode="Conditional">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="PreviewNavigationForm" />
                </Triggers>
                <ContentTemplate>
                    <cva:OfferFooter ID="OfferFooter" runat="server" />
                </ContentTemplate>
            </pwc:ExtendedUpdatePanel>
        </div>
    </div>
</div>
</form>
</body>
</html>
