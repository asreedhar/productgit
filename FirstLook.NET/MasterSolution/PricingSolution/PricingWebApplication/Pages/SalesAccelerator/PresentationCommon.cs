using System;
using System.Configuration;
using System.Threading;
using System.Web;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.IOC;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Marketing;
using FirstLook.Merchandising.DomainModel.Marketing.Commands;
using FirstLook.Merchandising.DomainModel.Marketing.Deal;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;
using FirstLook.Merchandising.DomainModel.Marketing.ValueAnalyzer;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;
using FirstLook.Pricing.DomainModel.Marketing.Presentation;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator
{
    /// <summary>
    /// Common behavior shared between BasePage and BaseControl.
    /// </summary>
    internal class PresentationCommon : IPresentationCommon
    {
        private readonly int COMMON_CACHE_EXPIRATION = int.Parse( ConfigurationManager.AppSettings["common_cache_expiration"] );

        private readonly HttpRequest _request;
        private ValidationFlags _validationFlags;
        private IDeal _deal;

        // Injected dependencies
        public ICache CustomCache { get; set;}
        public ILogger CustomLog { get; set;}
        public IMarketListingSearchAdapter SearchAdpater { get; set; }
        public IPresentationUtility Utility { get; set; }

        public IHandles Handles { get; private set; }

        public BusinessUnit BusinessUnitObject
        {
            get { return HttpContext.Current.Items[BusinessUnit.HttpContextKey] as BusinessUnit; }
        }

        public Inventory InventoryObject
        {
            get { return Inventory.GetInventory(Handles.OwnerHandle, Handles.VehicleHandle); }
        }

        public IDeal Deal
        {
            get
            {
                if( _deal == null )
                {
                    _deal = CustomCache.Get(DealCacheKey()) as IDeal ??
                        new Deal(CustomLog, SearchAdpater, Handles.OwnerHandle, Handles.VehicleHandle, Handles.SearchHandle);                    
                }

                // Put it in the cache
                CustomCache.Set(DealCacheKey(), _deal, TimeSpan.FromSeconds(COMMON_CACHE_EXPIRATION));

                return _deal;
            }
            set
            {
                _deal = value;
                CustomCache.Set(DealCacheKey(), _deal, TimeSpan.FromSeconds(COMMON_CACHE_EXPIRATION));
            }
        }

        private string DealCacheKey()
        {
            return String.Format("PresentationCommon.Deal:oh={0};vh={1};sh={2};uid={3}", Handles.OwnerHandle, Handles.VehicleHandle,
                                 Handles.SearchHandle, UserName);
        }

        internal protected PresentationCommon(HttpRequest request) : this(request, ValidationFlags.All)
        {
        }

        internal protected PresentationCommon(HttpRequest request, ValidationFlags flags)
        {
            _request = request;
            _validationFlags = flags;

            Utility = new PresentationUtility();

            // Get dependencies.
            Registry.BuildUp(this);
        }

        internal protected void OnInit()
        {
            // Pull simple values from request or params.
            var ownerHandle = _request.Params[CommonKeys.OWNER_HANDLE];
            var vehicleHandle = _request.Params[CommonKeys.VEHICLE_HANDLE];
            var searchHandle = _request.Params[CommonKeys.SEARCH_HANDLE];

            if ((_validationFlags & ValidationFlags.OwnerHandle) == ValidationFlags.OwnerHandle && string.IsNullOrEmpty(ownerHandle))
                throw new ApplicationException("Owner handle could not be determined from the request.");

            if ((_validationFlags & ValidationFlags.VehicleHandle) == ValidationFlags.VehicleHandle && string.IsNullOrEmpty(vehicleHandle))
                throw new ApplicationException("Vehicle handle could not be determined from the request.");

            if ((_validationFlags & ValidationFlags.SearchHandle) == ValidationFlags.SearchHandle && string.IsNullOrEmpty(searchHandle))
                throw new ApplicationException("Search handle could not be determined from the request.");

            Handles = new Handles
                          {OwnerHandle = ownerHandle, SearchHandle = searchHandle, VehicleHandle = vehicleHandle};
        }

        public void Log(Exception e)
        {
            CustomLog.Log(e);
        }

        // Merge these to domain object - get them out of here.
        public ValueAnalyzerVehiclePreferenceTO GetValueAnalyzerPreference()
        { 
            var command = new GetValueAnalyzerVehiclePreference(Handles.OwnerHandle, Handles.VehicleHandle);
            AbstractCommand.DoRun(command);
            return command.Preference;
        }

        public void SaveValueAnalyzerPreference(ValueAnalyzerVehiclePreferenceTO preference)
        {
            var command = new SetValueAnalyzerVehiclePreference(preference, UserName);
            AbstractCommand.DoRun(command);
        }

        public string UserName
        {
            get
            {
                return Thread.CurrentPrincipal.Identity.Name;
            }
        }
    }
}
