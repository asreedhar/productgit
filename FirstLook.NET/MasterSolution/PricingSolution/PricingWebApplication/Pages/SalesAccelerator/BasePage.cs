using System;
using FirstLook.Common.Core;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Marketing.Deal;
using FirstLook.Merchandising.DomainModel.Marketing.ValueAnalyzer;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;
using FirstLook.Pricing.DomainModel.Marketing.Presentation;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator
{
    public abstract class BasePage : System.Web.UI.Page, IPresentationCommon
    {
        private PresentationCommon _common;

        internal PresentationCommon Common
        {
            get { return _common ?? (_common = CreatePresentationCommon()); }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            // Create a new common presentation object and initialize it.
            _common = CreatePresentationCommon();
            _common.OnInit();
        }

  
        protected override void OnUnload(EventArgs e)
        {
            base.OnUnload(e);

            // Cache the deal.
            if( _common != null )
            {
                _common.Deal = Deal;                
            }
            else
            {
                // Can't even log without Common.
//                CustomLog.Log("PresentationCommon was null.", this);
            }
        }

        internal virtual PresentationCommon CreatePresentationCommon()
        {
            return new PresentationCommon(Request);
        }

        #region Implementation of IPresentationCommon

        public ICache CustomCache
        {
            get { return Common.CustomCache; }
        }

        public ILogger CustomLog
        {
            get { return Common.CustomLog; }
        }

        public IDeal Deal
        {
            get { return Common.Deal; }
            set { Common.Deal = value; }
        }
         
        public void Log(Exception e)
        {
            Common.Log(e);
        }

        public IPresentationUtility Utility
        {
            get { return Common.Utility; }
        }

        public IHandles Handles
        {
            get { return Common.Handles; }
        }

        public BusinessUnit BusinessUnitObject
        {
            get { return Common.BusinessUnitObject; }
        }

        public Inventory InventoryObject
        {
            get { return Common.InventoryObject; }
        }

        public string UserName
        {
            get { return Common.UserName; }
        }

        public ValueAnalyzerVehiclePreferenceTO GetValueAnalyzerPreference()
        {
            return Common.GetValueAnalyzerPreference();
        }

        public void SaveValueAnalyzerPreference(ValueAnalyzerVehiclePreferenceTO preference)
        {
            Common.SaveValueAnalyzerPreference(preference);
        }

        #endregion
    }
}
