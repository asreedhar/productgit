﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Preferences.Dealer
{
    /// <summary>
    /// Consumer highlights dealer preferences page.
    /// </summary>
    public partial class ConsumerHighlightsDealerPreferences : BasePage
    {
        #region Properties

        /// <summary>
        /// Dealer preferences for consumer highlights.
        /// </summary>
        private ConsumerHighlightsDealerPreference Preference
        {
            get
            {
                if (_preference == null)
                {
                    _preference = ConsumerHighlightsDealerPreference.GetOrCreateDealerPreference(Handles.OwnerHandle);
                }
                return _preference;
            }
        }
        private ConsumerHighlightsDealerPreference _preference;

        /// <summary>
        /// We require the owner handle.
        /// </summary>
        /// <returns></returns>
        internal override PresentationCommon CreatePresentationCommon()
        {
            return new PresentationCommon(Request, ValidationFlags.OwnerHandle);
        }

        #endregion

        #region Lifecycle

        /// <summary>
        /// Load the page. If not a postback, populate the list boxes.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindControls();
            }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Bind the controls to their datasources
        /// </summary>
        private void BindControls()
        {
            FilterCircleRatingsDDL.SelectedValue = Preference.MinimumJdPowerCircleRating.ToString();

            HighlightsSectionsListBox.DataSource = Preference.HighlightSections;
            HighlightsSectionsListBox.DataBind();

            DisplayedExpertsAwardsListBox.DataSource = Preference.DisplayedSnippetSources;
            DisplayedExpertsAwardsListBox.DataBind();

            HiddenExpertsAwardsListBox.DataSource = Preference.HiddenSnippetSources;
            HiddenExpertsAwardsListBox.DataBind();

            VehicleHistoryReportDDL.SelectedValue = Preference.MaxVHRItemsInitalDisplay.ToString();

            DisplayedVehicleHistoryReportsListBox.DataSource = Preference.DisplayedVehicleHistoryReportSources;
            DisplayedVehicleHistoryReportsListBox.DataBind();

            HiddenVehicleHistoryReportsListBox.DataSource = Preference.HiddenVehicleHistoryReportSources;
            HiddenVehicleHistoryReportsListBox.DataBind();
        }
        #endregion

        #region Highlight Section Button Clicks

        /// <summary>
        /// Move selected highlight sections up after clicking the 'Move Up' button.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        protected void MoveSectionUp_Click(object sender, EventArgs e)
        {
            foreach (int i in HighlightsSectionsListBox.GetSelectedIndices())
            {
                if (i > 0)
                {
                    ListItem item = HighlightsSectionsListBox.Items[i];
                    HighlightsSectionsListBox.Items.RemoveAt(i);
                    HighlightsSectionsListBox.Items.Insert(i - 1, item);
                }
            } 
        }

        /// <summary>
        /// Move selected highlight sections down after clicking the 'Move Down' button.        
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        protected void MoveSectionDown_Click(object sender, EventArgs e)
        {
            foreach (int i in HighlightsSectionsListBox.GetSelectedIndices())
            {
                if (i < HighlightsSectionsListBox.Items.Count - 1)
                {
                    ListItem item = HighlightsSectionsListBox.Items[i];
                    HighlightsSectionsListBox.Items.RemoveAt(i);
                    HighlightsSectionsListBox.Items.Insert(i + 1, item);
                }
            }
        }

        #endregion

        #region Snippet Source Button Clicks

        /// <summary>
        /// Move selected expert award sources up after clicking the 'Move Up' button.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        protected void MoveExpertAwardSourceUp_Click(object sender, EventArgs e)
        {
            foreach (int i in DisplayedExpertsAwardsListBox.GetSelectedIndices())
            {
                if (i > 0)
                {
                    ListItem item = DisplayedExpertsAwardsListBox.Items[i];
                    DisplayedExpertsAwardsListBox.Items.RemoveAt(i);
                    DisplayedExpertsAwardsListBox.Items.Insert(i - 1, item);
                }
            }
        }

        /// <summary>
        /// Move selected expert award sources down after clicking the 'Move Down' button.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        protected void MoveExpertAwardSourceDown_Click(object sender, EventArgs e)
        {
            foreach (int i in DisplayedExpertsAwardsListBox.GetSelectedIndices())
            {
                if (i < DisplayedExpertsAwardsListBox.Items.Count - 1)
                {
                    ListItem item = DisplayedExpertsAwardsListBox.Items[i];
                    DisplayedExpertsAwardsListBox.Items.RemoveAt(i);
                    DisplayedExpertsAwardsListBox.Items.Insert(i + 1, item);
                }
            }
        }

        /// <summary>
        /// Move selected snippet sources from the displayed list box to the hidden list box, and vice versa.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        protected void AddRemoveExpertAwardSource_Click(object sender, EventArgs e)
        {            
            // Reverse sort the list of selected indices to prevent removal issues; i.e. removing from a list i+1 then 
            // i is safe for the indices. Removing i then i+1 will cause i+1 to point to the wrong index.
            List<int> selectedIndices = new List<int>(DisplayedExpertsAwardsListBox.GetSelectedIndices());
            selectedIndices.Sort();
            selectedIndices.Reverse();            

            // Move items selected in the displayed list box to the hidden list box.
            foreach (int i in selectedIndices)            
            {
                ListItem item = DisplayedExpertsAwardsListBox.Items[i];
                DisplayedExpertsAwardsListBox.Items.RemoveAt(i);                

                int hiddenIndex = i < HiddenExpertsAwardsListBox.Items.Count ? i : HiddenExpertsAwardsListBox.Items.Count;
                HiddenExpertsAwardsListBox.Items.Insert(hiddenIndex, item);     
          
                item.Selected = false;
            }

            // Reverse sort the list of selected indices to prevent removal issues; i.e. removing from a list i+1 then 
            // i is safe for the indices. Removing i then i+1 will cause i+1 to point to the wrong index.
            selectedIndices = new List<int>(HiddenExpertsAwardsListBox.GetSelectedIndices());
            selectedIndices.Sort();
            selectedIndices.Reverse();

            // Move items selected in the hidden list box to the displayed list box.
            foreach (int i in selectedIndices)            
            {
                ListItem item = HiddenExpertsAwardsListBox.Items[i];
                HiddenExpertsAwardsListBox.Items.RemoveAt(i);                

                int displayedIndex = i < DisplayedExpertsAwardsListBox.Items.Count ? i : DisplayedExpertsAwardsListBox.Items.Count;
                DisplayedExpertsAwardsListBox.Items.Insert(displayedIndex, item);

                item.Selected = false;
            }
        }

        #endregion

        #region Vehicle History Report Source Button Clicks

        /// <summary>
        /// Move selected expert award sources up after clicking the 'Move Up' button.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        protected void MoveVehicleHistoryReportSourceUp_Click( object sender, EventArgs e )
        {
            foreach( int i in DisplayedVehicleHistoryReportsListBox.GetSelectedIndices() )
            {
                if( i > 0 )
                {
                    ListItem item = DisplayedVehicleHistoryReportsListBox.Items[i];
                    DisplayedVehicleHistoryReportsListBox.Items.RemoveAt( i );
                    DisplayedVehicleHistoryReportsListBox.Items.Insert( i - 1, item );
                }
            }
        }

        /// <summary>
        /// Move selected expert award sources down after clicking the 'Move Down' button.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        protected void MoveVehicleHistoryReportSourceDown_Click( object sender, EventArgs e )
        {
            foreach( int i in DisplayedVehicleHistoryReportsListBox.GetSelectedIndices() )
            {
                if( i < DisplayedVehicleHistoryReportsListBox.Items.Count - 1 )
                {
                    ListItem item = DisplayedVehicleHistoryReportsListBox.Items[i];
                    DisplayedVehicleHistoryReportsListBox.Items.RemoveAt( i );
                    DisplayedVehicleHistoryReportsListBox.Items.Insert( i + 1, item );
                }
            }
        }

        /// <summary>
        /// Move selected snippet sources from the displayed list box to the hidden list box, and vice versa.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        protected void AddRemoveVehicleHistoryReportSource_Click( object sender, EventArgs e )
        {
            // Reverse sort the list of selected indices to prevent removal issues; i.e. removing from a list i+1 then 
            // i is safe for the indices. Removing i then i+1 will cause i+1 to point to the wrong index.
            List<int> selectedIndices = new List<int>( DisplayedVehicleHistoryReportsListBox.GetSelectedIndices() );
            selectedIndices.Sort();
            selectedIndices.Reverse();

            // Move items selected in the displayed list box to the hidden list box.
            foreach( int i in selectedIndices )
            {
                ListItem item = DisplayedVehicleHistoryReportsListBox.Items[i];
                DisplayedVehicleHistoryReportsListBox.Items.RemoveAt( i );

                int hiddenIndex = i < HiddenVehicleHistoryReportsListBox.Items.Count ? i : HiddenVehicleHistoryReportsListBox.Items.Count;
                HiddenVehicleHistoryReportsListBox.Items.Insert( hiddenIndex, item );

                item.Selected = false;
            }

            // Reverse sort the list of selected indices to prevent removal issues; i.e. removing from a list i+1 then 
            // i is safe for the indices. Removing i then i+1 will cause i+1 to point to the wrong index.
            selectedIndices = new List<int>( HiddenVehicleHistoryReportsListBox.GetSelectedIndices() );
            selectedIndices.Sort();
            selectedIndices.Reverse();

            // Move items selected in the hidden list box to the displayed list box.
            foreach( int i in selectedIndices )
            {
                ListItem item = HiddenVehicleHistoryReportsListBox.Items[i];
                HiddenVehicleHistoryReportsListBox.Items.RemoveAt( i );

                int displayedIndex = i < DisplayedVehicleHistoryReportsListBox.Items.Count ? i : DisplayedVehicleHistoryReportsListBox.Items.Count;
                DisplayedVehicleHistoryReportsListBox.Items.Insert( displayedIndex, item );

                item.Selected = false;
            }
        }

        #endregion

        #region Save and Cancel Button Clicks

        /// <summary>
        /// Save the preference choices.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        protected void Save_Click(object sender, EventArgs e)
        {
            // Set the minimum circle rating.
            Preference.MinimumJdPowerCircleRating = double.Parse(FilterCircleRatingsDDL.SelectedValue);
            Preference.MaxVHRItemsInitalDisplay = int.Parse(VehicleHistoryReportDDL.SelectedValue);
            
            // Set the highlight section orderings.
            foreach (HighlightSection section in Preference.HighlightSections)
            {
                ListItem item = HighlightsSectionsListBox.Items.FindByText(section.Name);
                section.Rank = HighlightsSectionsListBox.Items.IndexOf(item) + 1;
            }

            // Put all the snippet sources from the preference object in one place.
            Dictionary<string, SnippetSource> snippetSources = new Dictionary<string, SnippetSource>();

            foreach (SnippetSource snippetSource in Preference.DisplayedSnippetSources)
            {
                snippetSources[snippetSource.Type.ToString()] = snippetSource;
            }
            foreach (SnippetSource snippetSource in Preference.HiddenSnippetSources)
            {
                snippetSources[snippetSource.Type.ToString()] = snippetSource;
            }

            // Clear out the preference's hidden and shown lists.
            Preference.DisplayedSnippetSources.Clear();
            Preference.HiddenSnippetSources.Clear();

            // Add the contents of the displayed list box to the preference's displayed list.            
            foreach (ListItem item in DisplayedExpertsAwardsListBox.Items)
            {
                SnippetSource marketingTextSource = snippetSources[item.Value];
                
                marketingTextSource.Rank = DisplayedExpertsAwardsListBox.Items.IndexOf(item) + 1;
                marketingTextSource.IsDisplayed = true;

                Preference.DisplayedSnippetSources.Add(marketingTextSource);
            }

            // Add the contents of the hidden list box to the preference's hidden list.
            foreach (ListItem item in HiddenExpertsAwardsListBox.Items)
            {
                SnippetSource marketingTextSource = snippetSources[item.Value];
                
                marketingTextSource.Rank = HiddenExpertsAwardsListBox.Items.IndexOf(item) + 1;
                marketingTextSource.IsDisplayed = false;

                Preference.HiddenSnippetSources.Add(marketingTextSource);
            }


            var vehicleHistorySources = new Dictionary<string, VehicleHistoryReportSource>();
            Array.ForEach(Preference.DisplayedVehicleHistoryReportSources.ToArray(), x => vehicleHistorySources.Add(x.VehicleHistoryReportInspectionId.ToString(), x));
            Array.ForEach(Preference.HiddenVehicleHistoryReportSources.ToArray(), x => vehicleHistorySources.Add(x.VehicleHistoryReportInspectionId.ToString(), x));

            Preference.DisplayedVehicleHistoryReportSources.Clear();
            Preference.HiddenVehicleHistoryReportSources.Clear();
            
            foreach (ListItem item in DisplayedVehicleHistoryReportsListBox.Items)
            {
                vehicleHistorySources[item.Value].Rank = DisplayedVehicleHistoryReportsListBox.Items.IndexOf(item) + 1;
                vehicleHistorySources[item.Value].IsDisplayed = true;
                Preference.DisplayedVehicleHistoryReportSources.Add(vehicleHistorySources[item.Value]);
            }

            int rank = DisplayedVehicleHistoryReportsListBox.Items.Count + 1;
            foreach (ListItem item in HiddenVehicleHistoryReportsListBox.Items)
            {
                vehicleHistorySources[item.Value].Rank = rank;
                vehicleHistorySources[item.Value].IsDisplayed = false;
                Preference.HiddenVehicleHistoryReportSources.Add(vehicleHistorySources[item.Value]);
                rank++;
            }

            
            Preference.Save();
            _preference = null;
        }

        /// <summary>
        /// Discard the changes made on the page.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            _preference = null;

            BindControls();
        }
        #endregion


    }
}
