using System;
using System.Web.UI;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Preferences.Dealer
{
    public partial class Default : Page
    {
        #region Properties

        private const string RedirectPage = "~/Pages/MaxMarketing/Preferences/Dealer/EquipmentPreferences.aspx";

        private string OwnerHandle
        {
            get { return Request.QueryString["oh"]; }
        }

        #endregion

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(OwnerHandle))
            {
                Response.Redirect(string.Format("{0}?oh={1}", RedirectPage, OwnerHandle), true); 
            }
        }
    }
}
