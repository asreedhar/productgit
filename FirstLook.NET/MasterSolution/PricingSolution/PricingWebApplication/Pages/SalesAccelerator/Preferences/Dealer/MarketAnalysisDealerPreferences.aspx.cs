using System;
using System.Web.UI.WebControls;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Preferences.Dealer
{
    public partial class MarketingAnalysisPreferences : BasePage
    {
        /// <summary>
        /// We require the owner handle.
        /// </summary>
        /// <returns></returns>
        internal override PresentationCommon CreatePresentationCommon()
        {
            return new PresentationCommon(Request, ValidationFlags.OwnerHandle);
        }


        #region Private Member Variables
        private MarketAnalysisDealerPreference _preference;
        #endregion

        #region Private Properties
        
        private MarketAnalysisDealerPreference Preference
        {
            get
            {
                if (_preference == null)
                {
                    _preference = MarketAnalysisDealerPreference.GetOrCreateMarketAnalysisDealerPreference(Handles.OwnerHandle);
                }
                return _preference;
            }
        }
        #endregion

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // initial load, populate the controls from the preference object
                BindControls();
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            Boolean enableCustomCurrentPrice = Preference.UseCustomCurrentInternetPrice &&
                                               !String.IsNullOrEmpty(currentInternetPriceCustomName.Text);
            displayCurrentInternetPriceCustom.Checked = enableCustomCurrentPrice;
            currentInternetPriceCustomName.Enabled = enableCustomCurrentPrice;
            displayCurrentInternetPrice.Checked = !enableCustomCurrentPrice;

            Boolean enableOfferPriceCustom = Preference.UseCustomOfferPrice &&
                                               !String.IsNullOrEmpty(offerPriceCustomName.Text);
            displayOfferPriceCustom.Checked = enableOfferPriceCustom;
            offerPriceCustomName.Enabled = enableOfferPriceCustom;
            displayOfferPrice.Checked = !enableOfferPriceCustom;            
        }


        protected void priceMoveUp_Click(object sender, EventArgs e)
        {
            // loop through our price provider listbox, move the selected items(s) down
            foreach (int i in priceProviderList.GetSelectedIndices())
            {
                if (i > 0)
                {
                    ListItem li = priceProviderList.Items[i];
                    priceProviderList.Items.RemoveAt(i);
                    priceProviderList.Items.Insert(i - 1, li);
                }
            }            
        }

        protected void priceMoveDown_Click(object sender, EventArgs e)
        {
            // loop through our price provider listbox, move the selected item(s) up
            foreach (int i in priceProviderList.GetSelectedIndices())
            {
                if (i < priceProviderList.Items.Count - 1)
                {
                    ListItem li = priceProviderList.Items[i];
                    priceProviderList.Items.RemoveAt(i);
                    priceProviderList.Items.Insert(i + 1, li);
                }
            }
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            // update our preference with values from the page
            Preference.CurrentInternetPriceDisplayName = currentInternetPriceCustomName.Text;
            Preference.OfferPriceDisplayName = offerPriceCustomName.Text;
            Preference.NumberOfPricesRequiredForPricingGauge = Int32.Parse( NumberOfPrices.SelectedValue );
            Preference.UseCustomComparisonPricePriority = displayOrderPriorityCustom.Checked;

            //We always make sure that if there is no text for the Display Names
            //use custom is set to false.
            
            Preference.UseCustomCurrentInternetPrice =
                displayCurrentInternetPriceCustom.Checked &&
                !String.IsNullOrEmpty(Preference.CurrentInternetPriceDisplayName);

            Preference.UseCustomOfferPrice =
                displayOfferPriceCustom.Checked &&
                !String.IsNullOrEmpty(Preference.OfferPriceDisplayName);

            foreach (PriceProvider priceProvider in Preference.PriceProviders)
            {
                ListItem item = priceProviderList.Items.FindByText(priceProvider.Description);
                priceProvider.Rank = priceProviderList.Items.IndexOf(item) + 1;
            }

            foreach (var listItem in ComparisonDisplayView.Items)
            {
                var desc = (Label)listItem.FindControl("lblDescription");
                var aboveRdo = (RadioButton)listItem.FindControl("rdoVarianceDirectionAbove");
                var belowRdo = (RadioButton)listItem.FindControl("rdoVarianceDirectionBelow");
                var varianceAmountTxtBox = (TextBox)listItem.FindControl("txtVarianceAmount");
                var varianceAmount = varianceAmountTxtBox.Text;

                var provider = Preference.PriceProviders.Find(m => m.Description.ToUpperInvariant().Equals(desc.Text.ToUpperInvariant()));
                if (provider != null)
                {
                    provider.VarianceDirection = (belowRdo.Checked) ? MarketAnalysisVarianceDirection.Below : MarketAnalysisVarianceDirection.Above;
                    provider.VarianceDollarAmount = int.Parse(varianceAmount);
                }
            }
            // save the prefrerence
            Preference.Save();
        }

        /// <summary>
        /// Discard the changes made on the page.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            _preference = null;

            BindControls();
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Bind the controls to their datasources
        /// </summary>
        private void BindControls()
        {
            currentInternetPriceCustomName.Text = Preference.CurrentInternetPriceDisplayName;
            offerPriceCustomName.Text = Preference.OfferPriceDisplayName;
            NumberOfPrices.SelectedValue = Preference.NumberOfPricesRequiredForPricingGauge.ToString();
            displayOrderPriorityCustom.Checked = Preference.UseCustomComparisonPricePriority;
            displayOrderPriorityPrice.Checked = !displayOrderPriorityCustom.Checked;
            displayOfferPrice.Checked = !Preference.UseCustomOfferPrice;
            displayOfferPriceCustom.Checked = Preference.UseCustomOfferPrice;
            displayCurrentInternetPrice.Checked = !Preference.UseCustomCurrentInternetPrice;
            displayCurrentInternetPriceCustom.Checked = Preference.UseCustomCurrentInternetPrice;
            priceProviderList.DataSource = Preference.PriceProviders;
            priceProviderList.DataBind();

            ComparisonDisplayView.DataSource = Preference.PriceProviders;
            ComparisonDisplayView.DataBind();
        }
        #endregion

        protected void SetPriceComparisonItemValues(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem)
            {
                return;
            }

            var priceProvider = e.Item.DataItem as PriceProvider;

            if (priceProvider == null)
            {
                return;
            }

            var description = (Label)e.Item.FindControl("lblDescription");

            var varianceAbove = (RadioButton)e.Item.FindControl("rdoVarianceDirectionAbove");
            var varianceBelow = (RadioButton)e.Item.FindControl("rdoVarianceDirectionBelow");
            varianceAbove.GroupName = varianceBelow.GroupName = String.Format("rdoGroup_{0}", priceProvider.Description);

            var varianceAmount = (TextBox)e.Item.FindControl("txtVarianceAmount");
                
            if (description != null)
                description.Text = priceProvider.Description;
            if (varianceAmount != null)
                varianceAmount.Text = priceProvider.VarianceDollarAmount.ToString();

            varianceBelow.Checked = priceProvider.VarianceDirection == MarketAnalysisVarianceDirection.Below;
            varianceAbove.Checked = priceProvider.VarianceDirection == MarketAnalysisVarianceDirection.Above;
        }
    }
}