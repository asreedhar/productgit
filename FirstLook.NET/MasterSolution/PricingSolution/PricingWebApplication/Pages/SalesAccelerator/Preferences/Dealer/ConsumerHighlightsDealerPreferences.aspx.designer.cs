﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.4206
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Preferences.Dealer {
    
    
    public partial class ConsumerHighlightsDealerPreferences {
        
        /// <summary>
        /// HighlightsSectionsListBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ListBox HighlightsSectionsListBox;
        
        /// <summary>
        /// MoveSectionsUpButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button MoveSectionsUpButton;
        
        /// <summary>
        /// MoveSectionsDownButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button MoveSectionsDownButton;
        
        /// <summary>
        /// FilterCircleRatingsDDL control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList FilterCircleRatingsDDL;
        
        /// <summary>
        /// DisplayedExpertsAwardsListBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ListBox DisplayedExpertsAwardsListBox;
        
        /// <summary>
        /// MoveExpertAwardSourceUpButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button MoveExpertAwardSourceUpButton;
        
        /// <summary>
        /// MoveExpertAwardSourceDownButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button MoveExpertAwardSourceDownButton;
        
        /// <summary>
        /// AddRemoveExpertAwardSourceButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button AddRemoveExpertAwardSourceButton;
        
        /// <summary>
        /// HiddenExpertsAwardsListBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ListBox HiddenExpertsAwardsListBox;
        
        /// <summary>
        /// VehicleHistoryReportDDL control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList VehicleHistoryReportDDL;
        
        /// <summary>
        /// DisplayedVehicleHistoryReportsListBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ListBox DisplayedVehicleHistoryReportsListBox;
        
        /// <summary>
        /// MoveUpVehicleHistoryReportButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button MoveUpVehicleHistoryReportButton;
        
        /// <summary>
        /// MoveDownVehicleHistoryReportButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button MoveDownVehicleHistoryReportButton;
        
        /// <summary>
        /// AddRemoveVehicleHistoryReportButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button AddRemoveVehicleHistoryReportButton;
        
        /// <summary>
        /// HiddenVehicleHistoryReportsListBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ListBox HiddenVehicleHistoryReportsListBox;
        
        /// <summary>
        /// Save control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button Save;
        
        /// <summary>
        /// Cancel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button Cancel;
    }
}
