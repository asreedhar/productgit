<%@ Page Language="C#" Title="Additional Dealer Settings" MasterPageFile="~/Pages/SalesAccelerator/Preferences/Dealer/DealerPreference.Master" AutoEventWireup="True" CodeBehind="DealerGeneralPreferences.aspx.cs" Inherits="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Preferences.Dealer.DealerGeneralPreferences" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadPlaceHolder" runat="server">
<style type="text/css">
#GeneralPreferenceFormView { font-family:Verdana,Tahoma,Arial,sans-serif; color:#355fab; width:725px !important; }
#GeneralPreferenceFormView legend { margin-top:20px; margin-bottom:-10px; font-weight:bold !important; }
#GeneralPreferenceFormView legend p { margin-top:10px; margin-bottom:-10px; }
#GeneralPreferenceFormView .snippet_form { padding-bottom:0px; }
#GeneralPreferenceFormView .snippet_form legend { font-weight:normal; font-size:1em; color:#355fab; }
#GeneralPreferenceFormView fieldset.controls { margin-top:2em; width:430px; }
#GeneralPreferenceFormView fieldset p, #GeneralPreferenceFormView fieldset div { margin-left:10px; }
#GeneralPreferenceFormView .listBox { float:left; margin-bottom:5px; }
#GeneralPreferenceFormView .radio { margin-left:10px; }

div.fieldLabelPair { margin-bottom:5px; }
div.fieldLabelPair input, div.fieldLabelPair select { padding:2px; font-size:1.2em; }
div.fieldLabelPair textarea { padding:0px !important; }
div.fieldLabelPair label { display:block; float:left; width:105px; font-weight:normal; color:#355fab;  }

div.fieldLabelPair iframe { border:0; width:550px; height:250px; }

</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
<div id="GeneralPreferenceFormView" class="form_view preference_page">
    <fieldset id="DealershipInfo" class="snippet_form vform">
        <legend>Dealership Information</legend>
        <div class="fieldLabelPair">
            <label for="framePhoto">Dealership Logo:</label>
            <iframe id="framePhoto" scrolling="no" runat="server"></iframe>
        </div>
        <div class="fieldLabelPair">
            <label for="<%= AddressLine1.ClientID %>">Address Line 1:</label>
            <asp:TextBox ID="AddressLine1" runat="Server" Width="295px"></asp:TextBox>
        </div>
        <div class="fieldLabelPair">
            <label for="<%= AddressLine2.ClientID %>">Address Line 2:</label>
            <asp:TextBox ID="AddressLine2" runat="Server" Width="295px"></asp:TextBox>
        </div>
        <div class="fieldLabelPair">
            <label>City/State/Zip</label>
            <asp:TextBox ID="City" runat="Server"></asp:TextBox>
            <asp:DropDownList ID="StateDDL" runat="Server" DataTextField="abbr" DataValueField="abbr" AppendDataBoundItems="true">
                <asp:ListItem Value="" Text=""></asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox ID="ZipCode" runat="server" Width="70px" MaxLength="10"></asp:TextBox>
        </div>
        <div class="fieldLabelPair">
            <label for="<%= DealerUrl.ClientID %>">Dealership Url:</label>
            <asp:TextBox ID="DealerUrl" runat="Server" Width="295px"></asp:TextBox>
            <asp:RegularExpressionValidator ID="DealerUrlValidator" ControlToValidate="DealerUrl" ValidationExpression="^(https?):(//).*" ErrorMessage="Url must start with http:// or https://" EnableClientScript="false" runat="server" />
        </div>
    </fieldset>
    <fieldset id="SalesInformation" class="snippet_form vform">
        <legend>Sales Information</legend>
        <div class="fieldLabelPair">
            <label for="<%= SalesEmailAddress.ClientID %>">Email Address:</label>
            <asp:TextBox ID="SalesEmailAddress" runat="server" Width="295px"></asp:TextBox>
        </div>
        <div class="fieldLabelPair">
            <label for="<%= SalesPhoneNumber.ClientID %>">Phone Number:</label>
            <asp:TextBox ID="SalesPhoneNumber" runat="server" Width="295px"></asp:TextBox>
        </div>
        <div class="fieldLabelPair">
            <label for="<%= ExtendedTagLine.ClientID %>">Extended Tag Line:</label>
            <asp:TextBox ID="ExtendedTagLine" runat="server" TextMode="multiLine" Width="295px" MaxLength="2000"></asp:TextBox>
            <asp:RegularExpressionValidator ID="ExtendedTagLineValidator" ControlToValidate="ExtendedTagLine" ValidationExpression="^[\s\S]{0,2000}$" EnableClientScript="false" ErrorMessage="2000 characters max" runat="server"/>
        </div>
    </fieldset>
    
    <fieldset id="WebPDFSettings" class="snippet_form vform">
        <legend>Web PDF Settings</legend>
        <div class="fieldLabelPair">
            <label for="<%= DisplayContactInfoOnWebPdfCheckBox.ClientID %>">Display Phone and Email on Web PDF?</label>
            <asp:CheckBox ID="DisplayContactInfoOnWebPdfCheckBox" runat="server" />
        </div>
    
    </fieldset>
    
    
    <fieldset id="Disclaimers" class="snippet_form vform">
        <legend>Disclaimers</legend>
        <div class="fieldLabelPair">
            <label for="<%= ValidFor.ClientID %>">Valid For:</label>
            <asp:DropDownList ID="ValidFor" runat="server">
                <asp:ListItem Value="0"></asp:ListItem>
                <asp:ListItem Value="1"></asp:ListItem>
                <asp:ListItem Value="2"></asp:ListItem>
                <asp:ListItem Value="3"></asp:ListItem>
                <asp:ListItem Value="4"></asp:ListItem>
                <asp:ListItem Value="5"></asp:ListItem>
                <asp:ListItem Value="6"></asp:ListItem>
                <asp:ListItem Value="7"></asp:ListItem>
                <asp:ListItem Value="8"></asp:ListItem>
                <asp:ListItem Value="9"></asp:ListItem>
                <asp:ListItem Value="10"></asp:ListItem>
                <asp:ListItem Value="11"></asp:ListItem>
                <asp:ListItem Value="12"></asp:ListItem>
                <asp:ListItem Value="13"></asp:ListItem>
                <asp:ListItem Value="14"></asp:ListItem>
            </asp:DropDownList>
            <span>&nbsp;day(s)</span>
        </div>
        <div class="fieldLabelPair">
            <label for="<%= Disclaimer.ClientID %>">Disclaimer:</label>
            <asp:TextBox ID="Disclaimer" runat="server" TextMode="multiline" Width="295px"></asp:TextBox>
            <asp:RegularExpressionValidator ID="DisclaimerValidator" ControlToValidate="Disclaimer" ValidationExpression="^[\s\S]{0,500}$" EnableClientScript="false" ErrorMessage="500 characters max" runat="server"/>
        </div>

    </fieldset>
    <fieldset class="controls">
        <span class="button update_button"><asp:Button ID="Save" runat="server" Text="Update" OnClick="Save_Click" /></span>
        <span class="button cancel_button"><asp:Button ID="Cancel" runat="server" Text="Cancel" /></span>
    </fieldset>
</div>
</asp:Content>