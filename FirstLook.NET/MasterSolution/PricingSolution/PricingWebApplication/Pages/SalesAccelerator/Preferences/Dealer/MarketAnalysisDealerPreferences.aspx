<%@ Page Language="C#" Title="Pricing Analysis Dealer Preferences" MasterPageFile="~/Pages/SalesAccelerator/Preferences/Dealer/DealerPreference.Master" AutoEventWireup="True" CodeBehind="MarketAnalysisDealerPreferences.aspx.cs" Inherits="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Preferences.Dealer.MarketingAnalysisPreferences"  %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadPlaceHolder" runat="server">
<style type="text/css">
#MarketAnalysisFormView { font-family:Verdana,Tahoma,Arial,sans-serif; color:#355fab; padding-top:10px; }
#MarketAnalysisFormView label { margin-left:2px; position:relative; top: -2px; font-weight:normal; color:#355fab; }
#MarketAnalysisFormView legend { margin-top:10px; margin-bottom:-10px; }
#MarketAnalysisFormView legend p { margin-top:10px; margin-bottom:-10px; }
#MarketAnalysisFormView .snippet_form { padding-bottom:0px; }
#MarketAnalysisFormView .snippet_form legend { font-weight:normal; font-size:1em; color:#355fab; }
#MarketAnalysisFormView fieldset.controls { margin-top:2em; width:430px; }
#MarketAnalysisFormView fieldset p, #MarketAnalysisFormView fieldset div { margin-left:10px; }
#MarketAnalysisFormView .listBox { float:left; margin-bottom:5px; }
#MarketAnalysisFormView .radio { margin-left:10px; }
#DisplayOrder .firstChild { margin-left:0px; }
#positionButtons { float:left;  }
#positionButtons input { float:left; clear:left; width:76px; font-size:.8em; margin-bottom:5px; }
.fieldLabelPair { clear:left; margin-bottom:10px; }
#MarketAnalysisFormView fieldset .wideform div { margin-left: 0;} 
.priceComparisonItem { padding: 5px 0; margin: 5px 0;}
.priceSourceLabel { font-weight: bold; }
.txtBoxVarianceAmount { width: 3em; }
.varianceDirRadios 
{
    width: 275px;
    padding: 5px 0;
}
.valueAmount  
{
    float: right;
    left: 10px;
    position: relative;
    top: 10px;
}
</style>
<script type="text/javascript">
      <!--
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
      //-->
   </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
    <div id="MarketAnalysisFormView" class="form_view preference_page">
    
        <fieldset class="snippet_form vform" id="PricingGauge">
            <legend>Pricing Gauge</legend>
            <p>Display Pricing Gauge only if 
            <asp:DropDownList ID="NumberOfPrices" runat="server">
                <asp:ListItem Value="1"></asp:ListItem>
                <asp:ListItem Value="2"></asp:ListItem>
                <asp:ListItem Value="3"></asp:ListItem>
            </asp:DropDownList>
             or more comparison prices are found.</p>
        </fieldset>
        
        <fieldset class="snippet_form vform" id="DisplayOrder">
            <legend>Consumer Packet Display Priority Order</legend>    
            <div class="fieldLabelPair">
                <asp:RadioButton ID="displayOrderPriorityCustom" runat="server" 
                Text="Display based on priority order below" 
                Checked='<%# Bind("UseCustomComparisonPricePriority") %>' 
                GroupName="PriceSortOption" CssClass="radio firstChild" />            
                <asp:RadioButton ID="displayOrderPriorityPrice" runat="server" 
                Text="Display in descending price order" 
                Checked='<%# !Boolean.Parse(Eval("UseCustomComparisonPricePriority").ToString()) %>'
                GroupName="PriceSortOption" CssClass="radio" />
            </div>
            <div class="fieldLabelPair">
                <asp:ListBox ID="priceProviderList" runat="server" Rows="7" DataTextField="Description" DataValueField="Rank" CssClass="listBox">
                </asp:ListBox>
                <div id="positionButtons">
                    <asp:Button ID="priceMoveUp" runat="server" Text="Move Up" OnClick="priceMoveUp_Click" />
                    <asp:Button ID="priceMoveDown" runat="server" Text="Move Down" OnClick="priceMoveDown_Click" />
                </div>
            </div>
        </fieldset>
        
        <fieldset class="snippet_form vform">
            <legend>Price Comparison Display Criteria</legend>
            <div class="fieldLabelPair wideform">
                <asp:ListView runat="server" ID="ComparisonDisplayView" OnItemDataBound="SetPriceComparisonItemValues">
                    <LayoutTemplate>
                        <div id="itemPlaceholder" runat="server"></div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <div class="priceComparisonItem">
                            Display the <asp:Label runat="server" ID="lblDescription" CssClass="priceSourceLabel"></asp:Label> 
                            comparison when my price is:
                            <div class="valueAmount">
                                $<asp:TextBox runat="server" ID="txtVarianceAmount" onkeypress="return isNumberKey(event)" CssClass="txtBoxVarianceAmount"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="varianceAmountValidator" ControlToValidate="txtVarianceAmount" runat="server" ErrorMessage="Only Numbers allowed" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                            </div>
                            <div class="varianceDirRadios">
                                <asp:RadioButton runat="server" ID="rdoVarianceDirectionBelow" Text="below the comparison amount by <em>at least</em>" />
                                <br/>
                                <asp:RadioButton runat="server" ID="rdoVarianceDirectionAbove" Text="above the comparison amount by <em>at most</em>" />
                            </div>
                        </div>
                    </ItemTemplate>    
                </asp:ListView>
            </div>
        </fieldset>
        
        <fieldset class="snippet_form vform" id="DisplayNames">
            <legend>Display Names</legend>            
            <%--<asp:ScriptManager ID="scriptManager" EnablePartialRendering="true" LoadScriptsBeforeUI="false"  runat="server">
            </asp:ScriptManager>--%>
            <div class="indent">
                <div class="fieldLabelPair">
                    <asp:RadioButton ID="displayCurrentInternetPrice" runat="server" GroupName="CurrentInternetPrice" /> 
                    <label for="<%= displayCurrentInternetPrice.ClientID %>">Current Internet Price</label><br />
                    <asp:RadioButton ID="displayCurrentInternetPriceCustom" runat="server" GroupName="CurrentInternetPrice" />
                    <asp:TextBox ID="currentInternetPriceCustomName" runat="server" Text='<%# Bind("CurrentInternetPriceDisplayName") %>' MaxLength="30"/>
                    <asp:CustomValidator ID="CurrentInternetPriceCustomNameCustomValidator2" runat="server" ControlToValidate="currentInternetPriceCustomName" ErrorMessage="Please enter a custom name." ValidateEmptyText="true" ClientValidationFunction="SalesAcceleratorDealerPrefs.ValidateCustomNames"></asp:CustomValidator>
                    <pwc:RadioEnabledTextBoxExtender ID="radioEnabledTextBoxExtenderInternetPrice" runat="server" RadioButtonId="displayCurrentInternetPriceCustom" TargetControlID="currentInternetPriceCustomName" />
                </div>
                <div class="fieldLabelPair">
                    <asp:RadioButton ID="displayOfferPrice" runat="server" GroupName="OfferPrice" />                
                    <label for="<%= displayOfferPrice.ClientID %>">Offer Price</label><br />
                    <asp:RadioButton ID="displayOfferPriceCustom" runat="server" GroupName="OfferPrice" />                
                    <asp:TextBox ID="offerPriceCustomName" runat="server" Text='<%# Bind("OfferPriceDisplayName") %>' MaxLength="18"/>
                    <asp:CustomValidator ID="OfferPriceCustomNameCustomValidator2" runat="server" ControlToValidate="offerPriceCustomName" ErrorMessage="Please enter a custom name." ValidateEmptyText="true" ClientValidationFunction="SalesAcceleratorDealerPrefs.ValidateCustomNames"></asp:CustomValidator>
                    <pwc:RadioEnabledTextBoxExtender ID="radioEnabledTextBoxExtenderOfferPrice" runat="server" RadioButtonId="displayOfferPriceCustom" TargetControlID="offerPriceCustomName" />
                </div>
            </div>
        </fieldset>
        
        <fieldset class="controls">
            <span class="button update_button"><asp:Button ID="save" runat="server" Text="Update" OnClick="Save_Click" /></span>
            <span class="button cancel_button"><asp:Button ID="cancel" runat="server" Text="Cancel" OnClick="Cancel_Click" /></span>
        </fieldset>
    </div>
        
</asp:Content>
