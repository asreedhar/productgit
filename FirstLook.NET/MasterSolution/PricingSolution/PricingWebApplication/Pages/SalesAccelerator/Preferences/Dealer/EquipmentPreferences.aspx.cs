using System;
using FirstLook.Merchandising.DomainModel.Marketing.Equipment;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Preferences.Dealer
{
    public partial class EquipmentPreferences : BasePage
    {
        /// <summary>
        /// We require the owner handle.
        /// </summary>
        /// <returns></returns>
        internal override PresentationCommon CreatePresentationCommon()
        {
            return new PresentationCommon(Request, ValidationFlags.OwnerHandle);
        }


        protected void ProviderList_DataBound(object sender, EventArgs e)
        {
            EquipmentProviderAssignmentList assignmentList =
                EquipmentProviderAssignmentList.GetEquipmentProviders(Handles.OwnerHandle);

            if (assignmentList != null)
            {
                foreach (EquipmentProviderAssignment assignment in assignmentList)
                {
                    if (assignment.IsDefault)
                    {
                        ProviderList.SelectedValue = assignment.EquipmentProviderId.ToString("G");

                        break;
                    }
                }
            }
        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            EquipmentProviderAssignmentList assignmentList =
                EquipmentProviderAssignmentList.GetEquipmentProviders(Handles.OwnerHandle);

            int id;

            if (int.TryParse(ProviderList.SelectedValue, out id))
            {
                foreach (EquipmentProviderAssignment assignment in assignmentList)
                {
                    if (assignment.EquipmentProviderId == id)
                    {
                        assignment.IsDefault = true;
                    }
                    else
                    {
                        assignment.IsDefault = false;
                    }
                }
            }

            assignmentList.Save();
        }

        protected void CancelButton_Click(object sender, EventArgs e)
        {
            ProviderList.DataBind();
        }
    }
}