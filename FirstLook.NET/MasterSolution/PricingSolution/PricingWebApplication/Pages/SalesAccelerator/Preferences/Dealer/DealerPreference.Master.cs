using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Marketing;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Preferences.Dealer
{
    public partial class DealerPreference : MasterPage
    {
        private string _vehicleHandle;
        private string _searchHandle;

        protected string VehicleHandle
        {
            get
            {
                if (string.IsNullOrEmpty(_vehicleHandle) && (Session[CommonKeys.VEHICLE_HANDLE] != null))
                    _vehicleHandle = Session[CommonKeys.VEHICLE_HANDLE].ToString();

                return _vehicleHandle;
            }
        }

        protected string SearchHandle
        {
            get
            {
                if (string.IsNullOrEmpty(_searchHandle) && (Session[CommonKeys.SEARCH_HANDLE] != null))
                    _searchHandle = Session[CommonKeys.SEARCH_HANDLE].ToString();

                return _searchHandle;
            }
        }
        protected string OwnerHandle
        {
            get { return Request.QueryString[CommonKeys.OWNER_HANDLE]; }
        } 

        protected string StartingNodeUrl
        {
            get
            {
                return Request.Url.LocalPath;
            }
        }

        protected void PreferencesSiteMapDataSource_DataBound(object sender, MenuEventArgs e)
        {
            MenuItem item = e.Item;

            if (item.NavigateUrl == string.Format("{0}?oh={1}", StartingNodeUrl, OwnerHandle))
            {
                item.Selected = true;
            }
        }

        private bool HasMaxAdUpgrade()
        {
            bool maxAdActive = false;
            var pc = new PresentationCommon(Request);
            if (pc.BusinessUnitObject != null)
            {
                maxAdActive = pc.BusinessUnitObject.HasDealerUpgrade(Upgrade.Merchandising);
            }

            return maxAdActive;
        }

        protected void AddReturnToMaxSection(object sender, EventArgs e)
        {
            if (HasMaxAdUpgrade() && !(new [] {OwnerHandle, VehicleHandle, SearchHandle}.Any(string.IsNullOrEmpty)))
            {
                var previewArgs = String.Format("oh={0}&vh={1}&sh={2}", OwnerHandle, VehicleHandle, SearchHandle);
                application_menu_container.Items.Add(new MenuItem("MAX Merchandising", "MAX Merchandising", null, "/pricing/Pages/MaxMarketing/Preview.aspx?" + previewArgs));
            }
        }
    }
}