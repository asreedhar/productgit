using System;
using System.Web.UI.WebControls;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Preferences.Dealer
{
    public partial class MarketListingDealerPreference : BasePage
    {
        /// <summary>
        /// We require the owner handle.
        /// </summary>
        /// <returns></returns>
        internal override PresentationCommon CreatePresentationCommon()
        {
            return new PresentationCommon(Request, ValidationFlags.OwnerHandle);
        }


        protected void MarketListingPreferenceObjectDataSource_Updating(object sender, ObjectDataSourceMethodEventArgs e)
        {

            //Replace the parameters to the right names
            e.InputParameters["ownerHandle"] = e.InputParameters["OwnerHandle"];
            e.InputParameters["isDisplayed"] = e.InputParameters["IsDisplayed"];
            e.InputParameters["mileageDeltaAbove"] = e.InputParameters["MileageDeltaAbove"];
            e.InputParameters["mileageDeltaBelow"] = e.InputParameters["MileageDeltaBelow"];
            e.InputParameters["priceDeltaAbove"] = e.InputParameters["PriceDeltaAbove"];
            e.InputParameters["priceDeltaBelow"] = e.InputParameters["PriceDeltaBelow"];
        }


        protected void MarketListingPreferenceObjectDataSource_Updated(object sender, ObjectDataSourceStatusEventArgs e)
        {
            HiddenField hf =
                FormViewMarketListingPreference.FindControl("HiddenFieldOwnerHandle") as HiddenField;

            Response.Redirect(Request.CurrentExecutionFilePath + "?oh=" + hf.Value);
        }

        protected void DropDownCurrencyFormatHelper(object sender, EventArgs e)
        {
            ((DropDownList)sender).Items.FindByText("$Unlimited").Text = "Unlimited";
        }
    }
}
