﻿<%@ Page Language="C#" Title="Consumer Highlights Dealer Preferences" MasterPageFile="~/Pages/SalesAccelerator/Preferences/Dealer/DealerPreference.Master" AutoEventWireup="True" CodeBehind="ConsumerHighlightsDealerPreferences.aspx.cs" Inherits="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Preferences.Dealer.ConsumerHighlightsDealerPreferences" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadPlaceHolder" runat="server">
<style type="text/css">
    #HighlightsPreferenceFormView { color:#355fab; width:725px !important; }
    #HighlightsPreferenceFormView h4 { margin-left:10px; font-weight:bold !important; }
    #HighlightsPreferenceFormView legend { margin-top:20px; margin-bottom:-10px; font-weight:bold !important; }
    #HighlightsPreferenceFormView legend p { margin-top:10px; margin-bottom:-10px; }
    #HighlightsPreferenceFormView .snippet_form { padding-bottom:0px; }
    #HighlightsPreferenceFormView .snippet_form legend { font-weight:normal; font-size:1em; color:#355fab; }
    #HighlightsPreferenceFormView fieldset.controls { margin-top:3em; width:565px; }
    #HighlightsPreferenceFormView fieldset p, #GeneralPreferenceFormView fieldset div { margin-left:10px; }
    
    #HighlightsPreferenceFormView .listBox { width:275px; height:100px; border:#999 solid 1px !important; }
    
    .fieldLabelPair, .fieldButtons { margin-left:10px; }
    .fieldLabelPair label { color:#355fab; font-weight:normal; }
    
    #HighlightSectionsFields label, #ReviewsAwardsSources label, #VehicleHistoryReports label { display:block; margin-bottom:.5em; }
    #HighlightSectionsFields .fieldLabelPair, #ReviewsAwardsSources .fieldLabelPair, #VehicleHistoryReports .fieldLabelPair { float:left; }
    #HighlightSectionsFields .fieldButtons, #ReviewsAwardsSources .fieldButtons, #VehicleHistoryReports .fieldButtons { float:left; margin:30px 0px 0 5px; text-align:center; width:115px; }
    #HighlightSectionsFields .fieldButtons input, #ReviewsAwardsSources .fieldButtons input, #VehicleHistoryReports input { display:block; margin:5px auto 0; width:70px; font-size:0.8em; }
</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
<div id="HighlightsPreferenceFormView" class="form_view preference_page">
    <h4>Vehicle Highlights Display Preferences and Priority Order</h4>
    
    <fieldset id="HighlightSectionsFields" class="snippet_form vform">
        <div class="fieldLabelPair">
            <label for="<%= HighlightsSectionsListBox.ClientID %>">Sections:</label>
            <asp:ListBox ID="HighlightsSectionsListBox" runat="server" SelectionMode="Single" CssClass="listBox" DataTextField="Name" DataValueField="Type">                
            </asp:ListBox>
        </div>
        <div class="fieldButtons">
            <asp:Button ID="MoveSectionsUpButton" runat="server" Text="Move Up" OnClick="MoveSectionUp_Click"/>
            <asp:Button ID="MoveSectionsDownButton" runat="server" Text="Move Down" OnClick="MoveSectionDown_Click"/>
        </div>
    </fieldset>
    
    <fieldset id="RatingsFilterFields" class="snippet_form vform">
        <legend>JDPower.com Ratings</legend>
        <div class="fieldLabelPair">
            <label for="<%= FilterCircleRatingsDDL.ClientID %>">Minimum circle rating for display</label>
            <asp:DropDownList ID="FilterCircleRatingsDDL" runat="server">
                <asp:ListItem Text="1"></asp:ListItem>
                <asp:ListItem Text="1.5"></asp:ListItem>
                <asp:ListItem Text="2"></asp:ListItem>
                <asp:ListItem Text="2.5"></asp:ListItem>
                <asp:ListItem Text="3"></asp:ListItem>
                <asp:ListItem Text="3.5"></asp:ListItem>
                <asp:ListItem Text="4"></asp:ListItem>
                <asp:ListItem Text="4.5"></asp:ListItem>
                <asp:ListItem Text="5"></asp:ListItem>
            </asp:DropDownList>
        </div>
    </fieldset>
    
    <fieldset id="ReviewsAwardsSources" class="snippet_form vform">
        <legend>Expert Review Sources</legend>
        <div class="fieldLabelPair">
            <label for="<%= DisplayedExpertsAwardsListBox.ClientID %>">Displayed:</label>
            <asp:ListBox ID="DisplayedExpertsAwardsListBox" runat="server" SelectionMode="Single" CssClass="listBox" DataTextField="Name" DataValueField="Type">
            </asp:ListBox>
        </div>
        <div class="fieldButtons">
            <asp:Button ID="MoveExpertAwardSourceUpButton" runat="server" Text="Move Up" OnClick="MoveExpertAwardSourceUp_Click"/>
            <asp:Button ID="MoveExpertAwardSourceDownButton" runat="server" Text="Move Down" OnClick="MoveExpertAwardSourceDown_Click"/>
            <asp:Button ID="AddRemoveExpertAwardSourceButton" runat="server" Text="Add/Remove" OnClick="AddRemoveExpertAwardSource_Click"/>
        </div>
        <div class="fieldLabelPair">
            <label for="<%= HiddenExpertsAwardsListBox.ClientID %>">Hidden:</label>
            <asp:ListBox ID="HiddenExpertsAwardsListBox" runat="server" SelectionMode="Single" CssClass="listBox" DataTextField="Name" DataValueField="Type">                
            </asp:ListBox>
        </div>
    </fieldset>
    
    <fieldset id="VehicleHistoryReports" class="snippet_form vform">
			<legend>Vehicle History Reports</legend>
			<div class="fieldLabelPair">
				<label style="display:inline" for="<%= VehicleHistoryReportDDL.ClientID %>">Maximum number of reports</label>
				<asp:DropDownList ID="VehicleHistoryReportDDL" runat="server">
					<asp:ListItem Text="1"></asp:ListItem>
					<asp:ListItem Text="2"></asp:ListItem>
					<asp:ListItem Text="3"></asp:ListItem>
					<asp:ListItem Text="4"></asp:ListItem>
					<asp:ListItem Text="5"></asp:ListItem>
					<asp:ListItem Text="6"></asp:ListItem>
					<asp:ListItem Text="7"></asp:ListItem>
					<asp:ListItem Text="8"></asp:ListItem>
				</asp:DropDownList>
            </div>
            <br style="clear:left;line-height:3em;" />
			<div class="fieldLabelPair">
				<label for="<%= DisplayedVehicleHistoryReportsListBox.ClientID %>">Displayed:</label>
				<asp:ListBox ID="DisplayedVehicleHistoryReportsListBox" runat="server" SelectionMode="Single" CssClass="listBox" DataTextField="Description" DataValueField="VehicleHistoryReportInspectionId">
				</asp:ListBox>
			</div>
			<div class="fieldButtons">
				<asp:Button ID="MoveUpVehicleHistoryReportButton" runat="server" Text="Move Up" OnClick="MoveVehicleHistoryReportSourceUp_Click" />
				<asp:Button ID="MoveDownVehicleHistoryReportButton" runat="server" Text="Move Down" OnClick="MoveVehicleHistoryReportSourceDown_Click" />
				<asp:Button ID="AddRemoveVehicleHistoryReportButton" runat="server" Text="Add/Remove" OnClick="AddRemoveVehicleHistoryReportSource_Click" />
			</div>
			<div class="fieldLabelPair">
				<label for="<%= HiddenVehicleHistoryReportsListBox.ClientID %>">Hidden:</label>
				<asp:ListBox ID="HiddenVehicleHistoryReportsListBox" runat="server" SelectionMode="Single" CssClass="listBox" DataTextField="Description" DataValueField="VehicleHistoryReportInspectionId">
				</asp:ListBox>
			</div>
    </fieldset>
    
    <fieldset class="controls">
			<span class="button update_button"><asp:Button ID="Save" runat="server" Text="Update" OnClick="Save_Click" /></span>
			<span class="button cancel_button"><asp:Button ID="Cancel" runat="server" Text="Cancel" OnClick="Cancel_Click" /></span>
    </fieldset>
</div>
</asp:Content>