<%@ Page Language="C#" Title="Market Listing Dealer Preferences" MasterPageFile="~/Pages/SalesAccelerator/Preferences/Dealer/DealerPreference.Master" AutoEventWireup="true" CodeBehind="MarketListingDealerPreference.aspx.cs" Inherits="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Preferences.Dealer.MarketListingDealerPreference" %>

<asp:Content ContentPlaceHolderID="HeadPlaceHolder" runat="server">
<style type="text/css">
#MarketListingFormView label {font-weight: normal; color:#355fab;}
#MarketListingFormView { font-family:Verdana,Tahoma,Arial,sans-serif; color:#355fab; }
#MarketListingFormView .checkbox label { margin-left:5px; position:relative; top: -2px; font-weight:normal; color:#355fab; }
#MarketListingFormView .snippet_form { padding-bottom:0px; }
#MarketListingFormView .snippet_form legend { font-weight:normal; font-size:1em; color:#355fab; }
#MarketListingFormView .snippet_form td { padding:3px 5px 3px 25px; }
#MarketListingFormView fieldset.controls { margin-top:2em; width:430px; }
ul.checkbox { list-style-type:none; margin-left:25px; }
ul.checkbox li { margin-bottom:5px; }
</style>

<!--[if lt IE 8]>
<style type="text/css">
#MarketListingFormView fieldset.controls
{
	padding-top:2em;
}
</style>
<![endif]-->
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
<div id="MarketListingFormView" class="form_view preference_page">
    <asp:FormView ID="FormViewMarketListingPreference" runat="server" DataSourceID="MarketListingPreferenceObjectDataSource" Height="147px" Width="465px" DefaultMode="Edit">
        <EditItemTemplate>
            <asp:HiddenField ID="HiddenFieldOwnerHandle" runat="server" Value='<%# Bind("OwnerHandle") %>' />
	        <fieldset class="snippet_form vform">
	             Enable Market Listings: <asp:CheckBox ID="DisplayCheckBox" runat="server" Checked='<%# Bind("IsDisplayed") %>' />
	            <table>
	                <tr>
	                    <th>Compare similar vehicles in market listings</th>
	                    <th style="text-align:center">Threshold</th>
	                </tr>
	                <tr>
	                    <td>Above my customer offer by more than:</td>
	                    <td>
	                        <asp:DropDownList ID="DropDownListAboveOffer" runat="server" DataSourceID="ObjectDataSourceRange" AppendDataBoundItems="True" DataTextFormatString="${0}" SelectedValue='<%# Bind("PriceDeltaAbove") %>' DataTextField="Name" DataValueField="Val" OnDataBound="DropDownCurrencyFormatHelper"></asp:DropDownList>
	                    </td>
	                </tr>
	                <tr>
	                    <td>Below my customer offer by more than:</td>
	                    <td>
	                        <asp:DropDownList ID="DropDownListBelowOffer" runat="server" DataSourceID="ObjectDataSourceRange" AppendDataBoundItems="True" DataTextFormatString="${0}" SelectedValue='<%# Bind("PriceDeltaBelow") %>'  DataTextField="Name" DataValueField="Val" OnDataBound="DropDownCurrencyFormatHelper"></asp:DropDownList>
	                    </td>
	                </tr>
	                <tr>
	                    <td>Above my mileage by more than:</td>
	                    <td>
	                        <asp:DropDownList ID="DropDownListMileageAbove" runat="server" Width="80px" DataSourceID="ObjectDataSourceRange" SelectedValue='<%# Bind("MileageDeltaAbove") %>' DataTextField="Name" DataValueField="Val"></asp:DropDownList> miles
	                    </td>
	                </tr>
	                <tr>
	                    <td>Below my mileage by less than:</td>
	                    <td>
	                        <asp:DropDownList ID="DropDownListMileageBelow" runat="server" Width="80px" DataSourceID="ObjectDataSourceRange" SelectedValue='<%# Bind("MileageDeltaBelow") %>' DataTextField="Name" DataValueField="Val"></asp:DropDownList> miles
	                    </td>
	                </tr>
	            </table>
	            </fieldset>
                <asp:ObjectDataSource ID="ObjectDataSourceRange" runat="server" SelectMethod="SelectRange" TypeName="FirstLook.Merchandising.DomainModel.Marketing.MarketListings.MarketListingPreference+DataSourceRange" OldValuesParameterFormatString="original_{0}"></asp:ObjectDataSource>
                <fieldset class="controls">
                    <span class="button update_button"><asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:Button></span>
                    <span class="button cancel_button"><asp:Button ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:Button></span>
                </fieldset>
        </EditItemTemplate>
        <ItemTemplate>
            <asp:HiddenField ID="HiddenFieldOwnerHandle" runat="server" Value='<%# Bind("OwnerHandle") %>' />
	        <fieldset class="snippet_form vform">
	            <table>
	                <tr>
	                    <th>Compare similar vehicles in market listings</th>
	                    <th style="text-align:center">Threshold</th>
	                </tr>
	                <tr>
	                    <td>Above my customer offer by more than:</td>
	                    <td>
	                        <asp:DropDownList ID="DropDownListAboveOffer" runat="server" DataSourceID="ObjectDataSourceRange" AppendDataBoundItems="True" DataTextFormatString="${0}" SelectedValue='<%# Bind("PriceDeltaAbove") %>' DataTextField="Name" DataValueField="Val"></asp:DropDownList>
	                    </td>
	                </tr>
	                <tr>
	                    <td>Below my customer offer by more than:</td>
	                    <td>
	                        <asp:DropDownList ID="DropDownListBelowOffer" runat="server" DataSourceID="ObjectDataSourceRange" AppendDataBoundItems="True" DataTextFormatString="${0}" SelectedValue='<%# Bind("PriceDeltaBelow") %>'  DataTextField="Name" DataValueField="Val"></asp:DropDownList>
	                    </td>
	                </tr>
	                <tr>
	                    <td>Above my mileage by more than:</td>
	                    <td>
	                        <asp:DropDownList ID="DropDownListMileageAbove" runat="server" Width="80px" DataSourceID="ObjectDataSourceRange" SelectedValue='<%# Bind("MileageDeltaAbove") %>' DataTextField="Name" DataValueField="Val"></asp:DropDownList> miles
	                    </td>
	                </tr>
	                <tr>
	                    <td>Below my mileage by less than:</td>
	                    <td>
	                        <asp:DropDownList ID="DropDownListMileageBelow" runat="server" Width="80px" DataSourceID="ObjectDataSourceRange" SelectedValue='<%# Bind("MileageDeltaBelow") %>' DataTextField="Name" DataValueField="Val"></asp:DropDownList> miles
	                    </td>
	                </tr>
	            </table>
	            </fieldset>
	            <asp:ObjectDataSource ID="ObjectDataSourceRange" runat="server" SelectMethod="SelectRange" TypeName="FirstLook.Merchandising.DomainModel.Marketing.MarketListings.MarketListingPreference+DataSourceRange" OldValuesParameterFormatString="original_{0}"></asp:ObjectDataSource>
        </ItemTemplate>
    </asp:FormView>
    <asp:ObjectDataSource ID="MarketListingPreferenceObjectDataSource" runat="server"
        OldValuesParameterFormatString="original_{0}" SelectMethod="Select" TypeName="FirstLook.Merchandising.DomainModel.Marketing.MarketListings.MarketListingPreference+DataSource" UpdateMethod="Update" OnUpdating="MarketListingPreferenceObjectDataSource_Updating" OnUpdated="MarketListingPreferenceObjectDataSource_Updated">
        <SelectParameters>
            <asp:QueryStringParameter Name="ownerHandle" QueryStringField="oh" Type="String" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="ownerHandle" Type="String" />
            <asp:Parameter Name="isDisplayed" Type="Boolean" />
            <asp:Parameter Name="mileageDeltaAbove" Type="Int32" />
            <asp:Parameter Name="mileageDeltaBelow" Type="Int32" />
            <asp:Parameter Name="priceDeltaAbove" Type="Int32" />
            <asp:Parameter Name="priceDeltaBelow" Type="Int32" />
        </UpdateParameters>
    </asp:ObjectDataSource>


</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
