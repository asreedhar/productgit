<%@ Page Language="C#" Title="Vehicle Features Dealer Preferences" AutoEventWireup="true" CodeBehind="EquipmentPreferences.aspx.cs" Inherits="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Preferences.Dealer.EquipmentPreferences" MasterPageFile="~/Pages/SalesAccelerator/Preferences/Dealer/DealerPreference.Master" %>

<%@ OutputCache Location="None" VaryByParam="None" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadPlaceHolder" runat="server">
<style type="text/css">
#FeaturesFormView label { font-weight:normal; color:#355FAB; }
</style>
</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
    <div id="FeaturesFormView" class="form_view preference_page">
        <asp:ObjectDataSource ID="ProviderDataSource" runat="server"
            TypeName="FirstLook.Merchandising.DomainModel.Marketing.Equipment.EquipmentProviderAssignmentList+DataSource"
            SelectMethod="Select">
            <SelectParameters>
                <asp:QueryStringParameter ConvertEmptyStringToNull="true" Name="ownerHandle" Type="String" QueryStringField="oh" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <fieldset class="snippet_form vform">
            <label for="<%= ProviderList.ClientID %>">Default Vehicle Features Source:&nbsp;</label>            
            <asp:DropDownList runat="server" ID="ProviderList"
                    AppendDataBoundItems="false"
                    DataSourceID="ProviderDataSource"
                    DataTextField="Name"
                    DataValueField="Id"
                    OnDataBound="ProviderList_DataBound">
            </asp:DropDownList>
        </fieldset>
        <fieldset class="controls">
            <span class="button update_button"><asp:Button runat="server" ID="SaveButton" OnClick="SaveButton_Click" Text="Update" ToolTip="Save Default Equipment Provider Selection" /></span>
            <span class="button cancel_button"><asp:Button runat="server" ID="CancelButton" OnClick="CancelButton_Click" Text="Cancel" ToolTip="Cancel Changes to Default Equipment Provider" /></span>
        </fieldset>
    </div>
</asp:Content>