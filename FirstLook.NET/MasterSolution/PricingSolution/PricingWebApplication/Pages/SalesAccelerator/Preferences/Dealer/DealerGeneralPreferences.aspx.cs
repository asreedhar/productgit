using System;
using System.Configuration;
using FirstLook.Merchandising.DomainModel.Marketing.AdditionalSettings;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Preferences.Dealer
{
    public partial class DealerGeneralPreferences : BasePage
    {
        private DealerGeneralPreference _preference;

        protected void Page_Load(object sender, EventArgs e)
        {
            _preference = DealerGeneralPreference.GetOrCreateDealerGeneralPreference(Handles.OwnerHandle);

            if(!IsPostBack)
            {
                Utility.LoadStateDropDownList(StateDDL);
                
                // Load controls.
                AddressLine1.Text = _preference.AddressLine1;
                AddressLine2.Text = _preference.AddressLine2;
                City.Text = _preference.City;

                StateDDL.SelectedValue = _preference.StateCode;
                ZipCode.Text = _preference.ZipCode;
                DealerUrl.Text = _preference.Url;
                SalesEmailAddress.Text = _preference.SalesEmailAddress;
                SalesPhoneNumber.Text = _preference.SalesPhoneNumber;
                ExtendedTagLine.Text = _preference.ExtendedTagline;

                ValidFor.SelectedValue = _preference.DaysValidFor.ToString();
                Disclaimer.Text = _preference.Disclaimer;

                DisplayContactInfoOnWebPdfCheckBox.Checked = _preference.DisplayContactInfoOnWebPdf;
            }

            // Load iframe
            String photoManagerUrl = ConfigurationManager.AppSettings["photo_manager_url"];

            //remove the id part of the vehicle handle which can be:
            //1 - Inventory
            //2 - Appraisal 
            //3 - ATC
            //4 - Inventory part of the group but not in the same business entity ( dealership).
            //5 - Market listing Vehicle id
            //6 - Auction
            // Get the dealerId.
            if (Identity.IsDealer(Handles.OwnerHandle))
            {
                int parentEntityId = Identity.GetDealerId(Handles.OwnerHandle);
                const string PHOTO_TYPE_ID = "3";

                string urlParameters = String.Format("parentEntityId={0}&photoTypeId={1}", parentEntityId, PHOTO_TYPE_ID);
                framePhoto.Attributes.Add("src", photoManagerUrl + urlParameters);                
            }
            else
            {
                // Hide the frame.
                framePhoto.Visible = false;
            }

        }

        internal override PresentationCommon CreatePresentationCommon()
        {
            return new PresentationCommon(Request, ValidationFlags.OwnerHandle);
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                _preference.AddressLine1 = AddressLine1.Text;
                _preference.AddressLine2 = AddressLine2.Text;
                _preference.City = City.Text;
                _preference.DaysValidFor = Int32.Parse(ValidFor.SelectedValue);
                _preference.Disclaimer = Disclaimer.Text;
                _preference.DisplayContactInfoOnWebPdf = DisplayContactInfoOnWebPdfCheckBox.Checked;
                _preference.ExtendedTagline = ExtendedTagLine.Text.Replace("�", "\"").Replace("�", "\"");
                _preference.SalesEmailAddress = SalesEmailAddress.Text;
                _preference.SalesPhoneNumber = SalesPhoneNumber.Text;
                _preference.StateCode = StateDDL.SelectedValue;
                _preference.ZipCode = ZipCode.Text;
                _preference.Url = DealerUrl.Text;
                
                _preference.Save();
            }
        }

        
    }
}
