using System;
using System.Threading;
using FirstLook.Merchandising.DomainModel.Marketing.Commands;
using FirstLook.Merchandising.DomainModel.Marketing.Deal;
using FirstLook.Merchandising.DomainModel.Marketing.Documents;
using FirstLook.Merchandising.DomainModel.Marketing.Pdf;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Utilities;
using System.Collections.ObjectModel;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator
{
    public partial class Print : BasePage
    {    
        protected override void OnPreRenderComplete(EventArgs e)
        {
            base.OnPreRenderComplete(e);

            var pdfManager = PdfManager.FromDeal(Deal, CustomLog);
            var xml = pdfManager.GetXml();

            var pdfCollection = pdfManager.ToPdfParts();
            // Merge multiple pdf documents into a single docuemnt.
            var document = PdfManager.MergePdf(pdfCollection);

            // Audit the xml and pdf byte stream.                                    
            var auditCommand = new AuditPrintCommand(xml, Handles.OwnerHandle, Handles.VehicleHandle);
            AbstractCommand.DoRun(auditCommand);

            // "Publish" the pdf to the web if desired - before the scripts are added to it.
            if (Deal.PacketType == Packets.ValueAnalyzer && Deal.ValueAnalyzerPacketOptions.PostToWebsite ) // and option to publish is true.
            {
                SaveDocumentXml(xml);
                PublishDocument(document);
            }

            // Push javascript into the pdf if needed
            document = ScriptActionsToPdf(pdfCollection, document);

            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AppendHeader( "Content-Length", document.Length.ToString() );
            Response.BinaryWrite( document );            
            Response.End();
        }

        private byte[] ScriptActionsToPdf(Collection<byte[]> pdfCollection, byte[] nonScriptedPdf)
        {
            byte[] document = nonScriptedPdf;

            bool hasScript = false;

            //Check in the PDF to see whether it's in a browser.
            string script = "var show = this.external; ";
            //Client date and time in seconds.
            script += "var t = new Date(); ";
            script += string.Format( "t = Date.UTC({0},{1},{2},{3},{4},{5})/1000; ", 
                "t.getUTCFullYear()", "t.getUTCMonth()", "t.getUTCDate()","t.getUTCHours()", "t.getUTCMinutes()", "t.getUTCSeconds()" );
            //Server date and time in seconds.
            DateTime dt = DateTime.UtcNow;
            script += string.Format( "var t2 = Date.UTC({0},{1},{2},{3},{4},{5})/1000; ",
                dt.Year, (dt.Month-1), dt.Day, dt.Hour, dt.Minute, dt.Second );
            //Get the difference.
            script += "if( t > t2 ) { t = t-t2; } else { t = t2-t; } ";
            //Time limit of 5 minutes (between server time and client time) to perform javascript actions.
            script += "if( t < 5*60 ) { show = true; } ";

            if (Deal.PacketType == Packets.SalesPacket && Deal.CustomerPacketOptions.Email)
            {
                string email = Deal.CustomerPacketOptions.Customer.Email;
                script += "if( show ) { this.mailDoc( { bUI:false, cTo:'" + email + "', cSubject:'MAX Marketing printout' } ); } ";
                hasScript = true;
            }

            if ((Deal.PacketType == Packets.ValueAnalyzer && Deal.ValueAnalyzerPacketOptions.Print) ||
                (Deal.PacketType == Packets.SalesPacket && Deal.CustomerPacketOptions.Print))
            {
                script += "if( show ) { this.print(true); } ";
                hasScript = true;
            }

            if (hasScript)
                document = PdfDocumentWrapper.Merge(pdfCollection, script);

            return document;
        }

        private void SaveDocumentXml(string xml)
        {
            InventoryDocument doc = InventoryDocument.GetOrCreate(Handles.OwnerHandle, Handles.VehicleHandle);
            doc.Xml = xml;
            doc.Save();
        }

        private void PublishDocument(byte[] document)
        {
            var command =
                new PublishValueAnalyzerDocument(Handles.OwnerHandle, Handles.VehicleHandle, document, Thread.CurrentPrincipal.Identity.Name, CustomLog);
            AbstractCommand.DoRun(command);
        }
    }

}
