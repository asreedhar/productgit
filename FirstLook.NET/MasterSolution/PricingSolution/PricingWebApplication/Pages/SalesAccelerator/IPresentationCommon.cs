using System;
using FirstLook.Common.Core;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Marketing.Deal;
using FirstLook.Merchandising.DomainModel.Marketing.ValueAnalyzer;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;
using FirstLook.Pricing.DomainModel.Marketing.Presentation;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator
{
    public interface IPresentationCommon
    {
        ICache CustomCache { get;}
        ILogger CustomLog { get; }
        void Log(Exception e);

        IPresentationUtility Utility { get; }
        IDeal Deal { get; set; }
        IHandles Handles { get; }

        BusinessUnit BusinessUnitObject { get; }
        Inventory InventoryObject { get; }

        string UserName { get; }

        ValueAnalyzerVehiclePreferenceTO GetValueAnalyzerPreference();
        void SaveValueAnalyzerPreference(ValueAnalyzerVehiclePreferenceTO preference);        
    }
}