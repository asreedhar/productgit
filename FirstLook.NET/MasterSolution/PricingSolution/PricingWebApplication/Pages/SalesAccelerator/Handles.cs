﻿using System;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator
{
    [Serializable]
    public class Handles : IHandles
    {
        public string OwnerHandle { get; set; }
        public string VehicleHandle { get; set; }
        public string SearchHandle { get; set; }
    }
}
