﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator
{
    [Flags]
    public enum ValidationFlags
    {
        OwnerHandle = 0x1,
        VehicleHandle = 0x2,
        SearchHandle = 0x4,
        All = OwnerHandle | VehicleHandle | SearchHandle
    }
}
