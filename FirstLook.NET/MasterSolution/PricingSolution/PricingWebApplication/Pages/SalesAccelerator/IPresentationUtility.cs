﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Xml;
using FirstLook.Merchandising.DomainModel.Marketing;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator
{
    public interface IPresentationUtility
    {
        void LoadStateDropDownList(DropDownList dropDownList);
        string FormatNumber(string value, NumberType type);
        string WriteNode(XmlNode node);
        string AppendHandles(string uri, string ownerHandle, string vehicleHandle, string searchHandle);
    }

    internal class PresentationUtility : IPresentationUtility
    {

        public string FormatNumber(string value, NumberType type)
        {
            switch (type)
            {
                case NumberType.Number:
                    return string.IsNullOrEmpty(value) ? string.Empty : string.Format("{0:#,##0}", int.Parse(value));
                case NumberType.Currency:
                    return string.IsNullOrEmpty(value) ? string.Empty : string.Format("{0:$#,##0;($#,##0);$0}", int.Parse(value));
            }
            return string.Empty;
        }

        public void LoadStateDropDownList(DropDownList dropDownList)
        {
            var xmlDs = new XmlDataSource
            {
                DataFile = HttpContext.Current.Server.MapPath("~/Pages/SalesAccelerator/USStates.xml")
            };
            dropDownList.DataSource = xmlDs;
            dropDownList.DataBind();
        }

        public string WriteNode(XmlNode node)
        {
            return node == null ? string.Empty : node.InnerText;
        }

        // A utility method.
        public string AppendHandles(string uri, string ownerHandle, string vehicleHandle, string searchHandle)
        {
            return String.Format("{0}?{1}={2}&{3}={4}&{5}={6}",
                                    uri,
                                    CommonKeys.OWNER_HANDLE, ownerHandle,
                                    CommonKeys.VEHICLE_HANDLE, vehicleHandle,
                                    CommonKeys.SEARCH_HANDLE, searchHandle);
        }

    }
}