﻿using System;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls
{
    public partial class PaymentsInformation : BaseUserControl
    {
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // If we won't re-render our output, we can avoid a bunch of work.
            if (!WillReRender())
            {
                return;
            }

            // Get the lease and own data.
            var lease = Deal.LeaseInformation;
            var own = Deal.OwnInformation;

            // lease
            if (lease.IsDisplayed)
            {
                LeasePaymentsInfoPanel.Visible = true;
                PaymentsInformationPanel.Visible = true;

                LeaseMonthlyPaymentsLiteral.Text = lease.PaymentAmount;
                LeaseTermsLiteral.Text = lease.PaymentTerms;
                LeaseDownLiteral.Text = lease.DownPaymentAmount;

                LeaseInterestRateLiteral.Text = lease.ShowInterestRate ? lease.InterestRate : string.Empty;
                LeaseInterestRatePanel.Visible = lease.ShowInterestRate;
            }
            else
            {
                LeasePaymentsInfoPanel.Visible = false;
            }

            // own
            if (own.IsDisplayed)
            {
                OwnPaymentsInfoPanel.Visible = true;
                PaymentsInformationPanel.Visible = true;

                OwnMonthlyPaymentsLiteral.Text = own.PaymentAmount;
                OwnTermsLiteral.Text = own.PaymentTerms;
                OwnDownLiteral.Text = own.DownPaymentAmount;

                OwnInterestRateLiteral.Text = own.ShowInterestRate ? own.InterestRate : string.Empty;
                OwnInterestRatePanel.Visible = own.ShowInterestRate;
            }
            else
            {
                OwnPaymentsInfoPanel.Visible = false;
            }

            if (!lease.IsDisplayed && !own.IsDisplayed)
            {
                PaymentsInformationPanel.Visible = false;
            }

        }

        
    }
}