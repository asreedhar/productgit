using System;
using Domain = FirstLook.Merchandising.DomainModel.Marketing.AdPreview;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls
{
    public partial class VehicleDescription : BaseUserControl
    {
        private Domain.AdPreviewVehiclePreference _preference;

        private Domain.AdPreviewVehiclePreference Preference
        {
            get
            {
                if (_preference == null)
                    _preference = Domain.AdPreviewVehiclePreference.GetOrCreate(Handles.VehicleHandle, Handles.OwnerHandle);

                return _preference;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            
            // If we won't re-render our output, we can avoid a bunch of work.
            if (!WillReRender())
            {
                return;
            }

            try
            {
                AdvertisementLabel.Text = Preference.GetTruncatedAdvertisment();
                VehicleDescriptionContentPanel.Visible = !string.IsNullOrEmpty(Preference.Advertisement);

                // Value Analyzer preferences
                var valueAnalyzerPreference = GetValueAnalyzerPreference();
                ToggleDisplayOnSellingSheetCheckBox.Checked = valueAnalyzerPreference.DisplayVehicleDescription;
                
                VehicleDescriptionContentPanel.Visible = true;
            }
            catch (Exception ex)
            {
                Log(ex);
                Visible = false;
            }
        }

        protected void ToggleDisplayDescriptionCheckBox_CheckChanged(object sender, EventArgs e)
        {
            bool displayDescription = ToggleDisplayOnSellingSheetCheckBox.Checked;

            // Get/Save value analyzer preference.
            var preference = GetValueAnalyzerPreference();
            preference.DisplayVehicleDescription = displayDescription;
            SaveValueAnalyzerPreference( preference );
        }
    }
}