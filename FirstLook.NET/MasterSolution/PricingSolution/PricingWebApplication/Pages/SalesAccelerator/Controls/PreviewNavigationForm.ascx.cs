using System;
using System.Web.UI;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Marketing.Deal;
using InventoryCalculator = FirstLook.Pricing.WebApplication.Pages.Internet.Controls.Calculator.Controls_Calculator_InventoryCalculator;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;
using System.Threading;
using System.Xml;
using System.Web.Routing;
using System.Web;
using Member=FirstLook.VehicleHistoryReport.DomainModel.Member;
using MemberType=FirstLook.VehicleHistoryReport.DomainModel.MemberType;
using System.Text.RegularExpressions;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls
{
    public partial class PreviewNavigationForm : BaseUserControl
    {

        private void SetupValidUntilDateValidator()
        {
            ValidDateTextBoxValidator.MinimumValue = DateTime.Today.ToShortDateString();
            ValidDateTextBoxValidator.MaximumValue = DateTime.Today.AddDays(14).ToShortDateString();
            ValidDateTextBoxValidator.ErrorMessage = @"Please enter a date from today through " +
                                                     DateTime.Today.AddDays(14).ToLongDateString();
        }

        private static bool GetWebSitePdfActive()
        {
            bool webSitePdfActive = false;
            var dealer = HttpContext.Current.Items[BusinessUnit.HttpContextKey] as BusinessUnit;
            if (dealer != null)
                webSitePdfActive = dealer.HasDealerUpgrade(Upgrade.WebSitePDF);

            return webSitePdfActive;
        }

        private static bool GetWebSitePdfMarketListingsActive()
        {
            bool webSitePdfMarketListingsActive = false;
            var dealer = HttpContext.Current.Items[BusinessUnit.HttpContextKey] as BusinessUnit;
            if (dealer != null)
                webSitePdfMarketListingsActive = dealer.HasDealerUpgrade(Upgrade.WebSitePDFMarketListings);

            return webSitePdfMarketListingsActive;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // If we won't re-render our output, we can avoid a bunch of work.
            if (!WillReRender())
            {
                return;
            }

            WebSitePdfActive.Value = GetWebSitePdfActive().ToString();
            WebSitePdfMarketListingsActive.Value = GetWebSitePdfMarketListingsActive().ToString();

            ValidDateTextBox.Text = Deal.ValidUntilDate;
            OfferPriceTextBox.Text = Deal.OfferPrice.ToString();
            DatePickerIconPathHidden.Value = Request.ApplicationPath + "/App_Themes/Lion/Images/icon_calendar.gif";

            // Load the urls.
            LoadLinksToPublishedPacket();

            // Initialize values from cache if they exist.
            LoadFormFromCustomerPacketPublishingOption();
            LoadFormWithOfferSummary();
            LoadPaymentInformation();
            LoadFormFromValueAnalyzerPacketPublishingOptions();

            // Offer price thresholds and warnings
            OriginalOfferPrice.Value = OfferPriceTextBox.Text; // OfferPrice.ToString();

            OfferPriceHigh.Value = InventoryCalculator.SmallPriceChangeThresholdHigh(int.Parse(OfferPriceTextBox.Text)).ToString(); // OfferPrice
            OfferPriceLow.Value = InventoryCalculator.SmallPriceChangeThresholdLow(int.Parse(OfferPriceTextBox.Text)).ToString(); // OfferPrice

            OfferPriceHighWarning.Value = InventoryCalculator.LargePriceChangeIncreaseText();
            OfferPriceLowWarning.Value = InventoryCalculator.LargePriceChangeDecreaseText();

            // Set min/max values for valid until date validator
            SetupValidUntilDateValidator();

            MarketListingsType.SelectedIndex = GetValueAnalyzerPreference().UseLongMarketingListingLayout ? 1 : 0;

        }

        private void LoadLinksToPublishedPacket()
        {
            var preference = GetValueAnalyzerPreference();

            if( string.IsNullOrEmpty( preference.LongUrl ) )
            {
                ViewCurrentPacketLink.NavigateUrl = string.Empty;
                ViewCurrentPacketLink.Visible = false;                
            }
            else
            {
                ViewCurrentPacketLink.NavigateUrl = preference.LongUrl;
                ViewCurrentPacketLink.Visible = true;                
            }

            if ( string.IsNullOrEmpty( preference.ShortUrl ) )
            {
                TinyUrl.Visible = false;
            }
            else
            {
                TinyUrl.Text = preference.ShortUrl;
                TinyUrl.Visible = true;
            }
        }


        protected void RefreshPreviewButton_OnClick(object sender, EventArgs e)
        {
            // Update values
            if( !Page.IsValid ) { return; }

            ExtractCustomerPacketPublishingOptions();
            ExtractOfferSummaryFromForm();
            ExtractPaymentInformationFromForm();
        }
        
        protected void PrintSheetButton_OnClick(object sender, EventArgs e)
        {
            // Update values
            if( !Page.IsValid ) { return; }

            Deal.PacketType = Packets.SalesPacket;

            if (PreviewSheetDDL.SelectedValue == "customer")
            {
                Deal.ShowEquipmentOnMarketComparison( EquipmentOnComparisonCkb.Checked );
            }
            else
            {
                Deal.ShowEquipmentOnMarketComparison(false);
            }
            
            ExtractCustomerPacketPublishingOptions();
            ExtractOfferSummaryFromForm();
            ExtractPaymentInformationFromForm();

            RegisterPopupWindow();
        }

        private string BuildPrintUrl()
        {
            string pdfName = "Print";
            var doc = Page.FindControl("VehicleYearModelCtl") as IXmlDoc;
            if(doc != null)
            {
                string yearMakeModel = String.Empty;
                XmlNode yearModel = doc.XmlDoc.SelectSingleNode("/vehicleyearmodel/vehiclesummary/yearmodel");
                if (yearModel != null)
                    yearMakeModel = yearModel.InnerText;

                string fullname = string.Empty;
                if(!String.IsNullOrEmpty(Deal.CustomerPacketOptions.Customer.FirstName) &&  !String.IsNullOrEmpty(Deal.CustomerPacketOptions.Customer.LastName))
                    fullname = Deal.CustomerPacketOptions.Customer.FirstName + '-' + Deal.CustomerPacketOptions.Customer.LastName;
                if (!String.IsNullOrEmpty(yearMakeModel))
                {
                    pdfName = string.Format("{0}{1:dd-MMMM-yyyy}_{2}{3}",
                        string.IsNullOrEmpty(fullname) ? string.Empty : fullname + '_',
                        DateTime.Today,
                        string.IsNullOrEmpty(yearMakeModel) ? string.Empty : yearMakeModel.Replace(" ", "_"),
                        string.IsNullOrEmpty(Deal.CustomerPacketOptions.SalesPerson.Name) ? string.Empty : "_Offer_By_" + Deal.CustomerPacketOptions.SalesPerson.Name.Replace(" ", "-")
                        );
                }
            }
            //strip illegal characters
            pdfName = Regex.Replace(pdfName, @"[^A-Za-z0-9 \-_]", String.Empty);

            //use routes to construct url
            var parameters = new RouteValueDictionary{{"page", pdfName}};
            var virtualPathData = RouteTable.Routes.GetVirtualPath(null, "PdfCreator", parameters);

            return virtualPathData == null ? 
                string.Empty : 
                Utility.AppendHandles(virtualPathData.VirtualPath, Handles.OwnerHandle, Handles.VehicleHandle, Handles.SearchHandle);
        }

        private void RegisterPopupWindow()
        {
            var printUrl = BuildPrintUrl();

            if( printUrl.Length == 0 )
            {
                CustomLog.Log("Could not build print url.", this);
                return;
            }

            const string KEY = "OpenPrintWindow";
            const string FEATURES = "directories=no,menubar=no,status=no,titlebar=no,toolbar=no,resizable=1,scrollbars=1";
            const string WINDOW_NAME = "_blank";
            var script = string.Format("window.open('{0}','{1}','{2};');", printUrl, WINDOW_NAME, FEATURES);
            Type type = GetType();

            string carFaxUrl, autoCheckUrl;
            BuildVhrUrls(out carFaxUrl, out autoCheckUrl);

            if ((Deal.PacketType == Packets.SalesPacket && Deal.CustomerPacketOptions.CarFaxReport) ||
                (Deal.PacketType == Packets.ValueAnalyzer && Deal.ValueAnalyzerPacketOptions.CarFaxReport))

            {
                if (!string.IsNullOrEmpty(carFaxUrl))
                {
                    string carfaxScript = string.Format("window.open('{0}');", carFaxUrl);
                    script += carfaxScript;
                }
            }

            if ((Deal.PacketType == Packets.SalesPacket && Deal.CustomerPacketOptions.AutoCheckReport) ||
                (Deal.PacketType == Packets.ValueAnalyzer && Deal.ValueAnalyzerPacketOptions.AutoCheckReport))

            {
                string autoCheckScript = string.Format("window.open('{0}');", autoCheckUrl);
                script += autoCheckScript;
            }

            // Are we in an async postback?
            var scriptManger = ScriptManager.GetCurrent(Page);

            if (scriptManger != null && scriptManger.IsInAsyncPostBack )
            {
                ScriptManager.RegisterStartupScript(Page, type, KEY, script, true);
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(type, KEY, script, true);                
            }
        }

        private void BuildVhrUrls(out string carFaxUrl, out string autoCheckUrl)
        {
            carFaxUrl = string.Empty;

            // get the dealerid
            int dealerId = DomainModel.Internet.ObjectModel.Identity.GetDealerId(Handles.OwnerHandle);

            // get the member (user)
            var member = GetMember(Thread.CurrentPrincipal.Identity.Name);

            // get the account for the given member (either dealer account or system account depending on the member/user)
            CarfaxAccount carfaxAccount = GetAccount(dealerId, member);

            // get the vehicle's vin 
            Inventory inv = Inventory.GetInventory(Handles.OwnerHandle, Handles.VehicleHandle);

            if (carfaxAccount != null)
            {
                // construct the url to carfax
                carFaxUrl = string.Format(
                        "http://www.carfaxonline.com/cfm/Display_Dealer_Report.cfm?partner=FLK_0&UID={0}&vin={1}",
                        carfaxAccount.UserName,
                        inv.Vin);
            }

            // construct the url to autocheck
            autoCheckUrl = string.Format("/support/Reports/AutoCheckReport.aspx?DealerId={0}&Vin={1}", dealerId, inv.Vin);
        }

        private static Member GetMember(string userName)
        {
            // this was copied (and modified slightly) from FirstLook.VehicleHistoryReport.WebService.Services.CarfaxWebService
            return Member.Exists(userName) ? Member.GetMember(userName) : null;
        }

        private static CarfaxAccount GetAccount(int dealerId, Member member)
        {
            // this was copied from FirstLook.VehicleHistoryReport.WebService.Services.CarfaxWebService
            switch (member.MemberType)
            {
                case MemberType.User:
                    if (DealerAccount.Exists(dealerId))
                    {
                        return DealerAccount.GetDealerAccount(dealerId).Account;
                    }
                    break;
                case MemberType.Administrator:
                    if (SystemAccount.Exists())
                    {
                        return SystemAccount.GetSystemAccount().Account;
                    }
                    break;
            }

            return null;
        }

        private void ExtractPaymentInformationFromForm()
        {
            var lease = new PaymentInformation
                            {
                                IsDisplayed = LeaseCheckbox.Checked,
                                PaymentAmount = LeasePaymentsAmount.Text,
                                PaymentTerms = LeasePaymentsTerm.Text,
                                DownPaymentAmount = LeaseDownPaymentAmount.Text,
                                InterestRate = LeaseInterestRate.Text,
                                ShowInterestRate = ShowLeaseInterestRateCheckBox.Checked
                            };

            var own = new PaymentInformation
                          {
                              IsDisplayed = OwnCheckbox.Checked,
                              PaymentAmount = OwnPaymentsAmount.Text,
                              PaymentTerms = OwnPaymentsTerm.Text,
                              DownPaymentAmount = OwnDownPaymentAmount.Text,
                              InterestRate = OwnInterestRate.Text,
                              ShowInterestRate = ShowOwnInterestRateCheckBox.Checked
                          };

            Deal.LeaseInformation = lease;
            Deal.OwnInformation = own;
        }

        private void ExtractCustomerPacketPublishingOptions()
        {
            var options = new CustomerPacketPublishingOptions();
            options.Customer = new Customer
                               {
                                   FirstName = FirstNameTextBox.Text,
                                   Email = EmailTextBox.Text,
                                   LastName = LastNameTextBox.Text,
                                   PhoneNumber = PhoneTextBox.Text
                               };
            options.SalesPerson = new SalesPerson {Name = SalespersonTextBox.Text};

            options.AutoCheckReport = AutoCheckReportCkb.Checked;
            options.CarFaxReport = AddCarFaxReportCkb.Checked;

            // OR the option into the bitmask.
            if (IncludeSellingSheetCkb.Checked)
                options.SalesPacketOptions |= SalesPackOptions.SellingSheet;
            if (AddMarketComparisonCkb.Checked)
                options.SalesPacketOptions |= SalesPackOptions.MarketComparison;

            options.Email = PDFActionsCkbList.Items.FindByValue("Email").Selected;
            options.Print = PDFActionsCkbList.Items.FindByValue("Print").Selected;

            // Cache the option
            Deal.CustomerPacketOptions = options;
        }

        private void ExtractOfferSummaryFromForm()
        {
            // Must retrieve new valid until date via request.form b/c the field is readonly.
            Deal.ValidUntilDate = Request.Form[ValidDateTextBox.UniqueID];

            int offerPrice;
            try
            {
                offerPrice = int.Parse( OfferPriceTextBox.Text );
            }
            catch (Exception)
            {
                offerPrice = int.Parse( Regex.Replace(OfferPriceTextBox.Text,"[^0-9]","" ) );
            }

            Deal.OfferPrice = offerPrice;
        }

        private void ExtractValueAnalyzerPacketPublishingOptions()
        {

            var preference = GetValueAnalyzerPreference();

            var options = new ValueAnalyzerPacketPublishingOptions();
            options.AutoCheckReport = AnalyzerPacketHistoryReports.Items.FindByValue("AutoCheck").Selected;
            options.CarFaxReport = AnalyzerPacketHistoryReports.Items.FindByValue("CarFax").Selected;
            options.DisplayMarketListingsPermission = GetWebSitePdfMarketListingsActive();

            options.PostToWebsite = VAPDFActionsCkbList.Items.FindByValue("Publish").Selected;
            options.Print = VAPDFActionsCkbList.Items.FindByValue("Print").Selected;

            preference.UseLongMarketingListingLayout = MarketListingsType.Items.FindByValue("long").Selected;
            SaveValueAnalyzerPreference(preference);

            // cache the option
            Deal.ValueAnalyzerPacketOptions = options;
        }

        private void LoadFormFromValueAnalyzerPacketPublishingOptions()
        {
            var options = Deal.ValueAnalyzerPacketOptions;
            AnalyzerPacketHistoryReports.Items.FindByValue("AutoCheck").Selected = options.AutoCheckReport;
            AnalyzerPacketHistoryReports.Items.FindByValue("CarFax").Selected = options.CarFaxReport;

            VAPDFActionsCkbList.Items.FindByValue("Publish").Selected = options.PostToWebsite;
            VAPDFActionsCkbList.Items.FindByValue("Print").Selected = options.Print;
        }

        private void LoadFormWithOfferSummary()
        {
            ValidDateTextBox.Text = Deal.ValidUntilDate;
            OfferPriceTextBox.Text = Deal.OfferPrice.ToString();
        }

        private void LoadFormFromCustomerPacketPublishingOption()
        {
            var customer = Deal.CustomerPacketOptions.Customer;

            FirstNameTextBox.Text = customer.FirstName;
            LastNameTextBox.Text = customer.LastName;
            PhoneTextBox.Text = customer.PhoneNumber;
            EmailTextBox.Text = customer.Email;
            SalespersonTextBox.Text = Deal.CustomerPacketOptions.SalesPerson.Name;

            var options = Deal.CustomerPacketOptions;

            AutoCheckReportCkb.Checked = options.AutoCheckReport;
            AddCarFaxReportCkb.Checked = options.CarFaxReport;

            // FB 11767: Removed the behavior that checks the Selling Sheet/Market Comparison checkboxes by default
           
            PDFActionsCkbList.Items.FindByValue("Email").Selected = options.Email;
            PDFActionsCkbList.Items.FindByValue("Print").Selected = options.Print;
        }

        private void LoadPaymentInformation()
        {
            var lease = Deal.LeaseInformation;
            var own = Deal.OwnInformation;

            LeaseCheckbox.Checked = lease.IsDisplayed;
            LeasePaymentsAmount.Text = lease.PaymentAmount;
            LeasePaymentsTerm.Text = lease.PaymentTerms;

            LeaseDownPaymentAmount.Text = lease.DownPaymentAmount;
            LeaseInterestRate.Text = lease.InterestRate;
            ShowLeaseInterestRateCheckBox.Checked = lease.ShowInterestRate;

            OwnCheckbox.Checked = own.IsDisplayed;
            OwnPaymentsAmount.Text = own.PaymentAmount;
            OwnPaymentsTerm.Text = own.PaymentTerms;
            OwnDownPaymentAmount.Text = own.DownPaymentAmount;
            OwnInterestRate.Text = own.InterestRate;
            ShowOwnInterestRateCheckBox.Checked = own.ShowInterestRate;
        }

        protected void PublishValueAnalyzerButton_OnClick(object sender, EventArgs e)
        {
            // Update values
            if (!Page.IsValid) 
                return;

            var dealer = HttpContext.Current.Items[BusinessUnit.HttpContextKey] as BusinessUnit;
            if (dealer != null && ! dealer.HasDealerUpgrade(Upgrade.WebSitePDF))
                throw new ApplicationException("The upgrade required for Web PDF is not active.");

            Deal.PacketType = Packets.ValueAnalyzer;
            ExtractValueAnalyzerPacketPublishingOptions();
            ExtractOfferSummaryFromForm();
            RegisterPopupWindow();
        }


        protected void ResetDealButton_OnClick(object sender, EventArgs e)
        {
            Deal.Reset();
        }
    }
}