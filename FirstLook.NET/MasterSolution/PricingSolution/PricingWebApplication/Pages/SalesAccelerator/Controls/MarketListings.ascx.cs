using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Xml;
using FirstLook.Common.Core.Xml;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;
using PricingPresenters;
using System.Linq;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls
{
    /// <summary>
    /// The MarketListings control can be used by itself or it can be embedded in other controls.  It exposes two primary
    /// modes: read-only and edit mode.  The behavior and layout of the control is slightly different between these modes.
    /// The control doesn't actually save anything while in edit mode.  Rather, it exposes information about which listings
    /// the user has marked as "hidden" or "displayed" to the caller so that they might take action.  It also exposes info
    /// about the sort order and whether the sort is ascending.
    /// </summary>
    public partial class MarketListings : BaseUserControl, IMarketListingsView, IXmlDoc
    {
        #region Injected dependencies
        
        public MarketListingsPresenter Presenter { get; set; }

        #endregion
        
        #region IMarketListingsView

        public LayoutKeys LayoutKey { get; set; }

        public bool StyleMarketComparisonEmpty { get; set;}
        public bool StyleSellingSheetEmpty { get; set; }

        public bool DisplaySellingSheetPanel { get; set; }
        public bool DisplayMarketComparisonPanel { get; set; }

        public bool ShowDisplayedColumn { get; set; }
        public bool ShowDealerName { get; set; }

        public void StylePanels()
        {
            const string VISIBILITY_KEY = "visible";

            MarketComparisonLayout.Attributes.Remove(VISIBILITY_KEY);
            SellingSheetLayout.Attributes.Remove(VISIBILITY_KEY);

            // Set panel visibility.
            MarketComparisonLayout.Visible = LayoutKey == LayoutKeys.MarketComparison;
            SellingSheetLayout.Visible = LayoutKey == LayoutKeys.SellingSheet ||
                                         LayoutKey == LayoutKeys.VehiclePreference;
            VehiclePreferenceListingsFieldsPanel.Visible = LayoutKey == LayoutKeys.VehiclePreference;

            // Set panel empty styles                                  
            if (StyleMarketComparisonEmpty)
            {
                MarketComparisonLayout.CssClass += " emptyModule";
                MarketComparisonLayout.Attributes.Add("title", "This module will NOT be printed.");
            }
            else
            {
                MarketComparisonLayout.CssClass = MarketComparisonLayout.CssClass.Replace("emptyModule", String.Empty);
                MarketComparisonLayout.Attributes.Remove("title");
            }

            if (StyleSellingSheetEmpty)
            {
                SellingSheetLayout.CssClass += " emptyModule";
                SellingSheetLayout.Attributes.Add("title", "This module will NOT be printed.");
            }
            else
            {
                SellingSheetLayout.CssClass = SellingSheetLayout.CssClass.Replace("emptyModule", String.Empty);
                SellingSheetLayout.Attributes.Remove("title");
            }
        }

        public bool EditMode
        {
            get
            {
                return LayoutKey == LayoutKeys.VehiclePreference;
            }
        }

        public SortCode? Sortcode
        {
            get
            {
                try
                {
                    return (SortCode)Enum.Parse(typeof(SortCode), "ListPrice");
                }
                catch (Exception)
                {
                    return null;
                }
            }
            set
            {
                //SortCodeField.Value = value.ToString();
            }
        }

        public bool? Sortascending
        {
            get
            {
                try
                {
                    return Boolean.Parse("false");
                }
                catch (Exception)
                {
                    return null;
                }
            }
            set
            {
                //SortAscendingField.Value = value.ToString();
            }
        }
       

        public void Bind()
        {
            VehicleComparisonRepeater1.DataSource = XmlDoc.SelectNodes("/marketlistings/marketlisting");
            VehicleComparisonRepeater2.DataSource = XmlDoc.SelectNodes("/marketlistings/marketlisting");            

            VehicleComparisonRepeater1.DataBind();
            VehicleComparisonRepeater2.DataBind();

            if (!IsPostBack)
            {
                // Bind selected listings hidden fields
                XmlNodeList selectedVehicleNodes = XmlDoc.SelectNodes("/marketlistings/marketlisting/vehicle/vehicleId");
                if (selectedVehicleNodes != null)
                {
                    var list = (from XmlNode node in selectedVehicleNodes select node.InnerText).ToList();
                    BindSelectedItems(list);
                }
            }
        }

        private void BindSelectedItems(IList<int> displayedIDs)
        {
            BindSelectedItems(displayedIDs.Select(x => x.ToString()).ToList());
        }

        private void BindSelectedItems(IList<string> displayedIDs)
        {
            string selectedVehicleIds = string.Empty;
            foreach (string ids in displayedIDs)
                selectedVehicleIds += ids + ",";
            
            if (!String.IsNullOrEmpty(selectedVehicleIds))
                selectedVehicleIds = selectedVehicleIds.Remove(selectedVehicleIds.LastIndexOf(","));

            CurrentSelectedItems.Value = selectedVehicleIds;
            SetOrignalItems();
        }

        public void RemoveAddSelectedItems(MarketListingVehiclePreference preference)
        {
            preference.RemoveAddSelectedItems(CurrentSelectedItems.Value, OriginalSelectedItems.Value);
        }

        public void SetOrignalItems()
        {
            OriginalSelectedItems.Value = CurrentSelectedItems.Value;
        }

        public void ResetOrignalAndSelectedItems(MarketListingVehiclePreference preference)
        {
            BindSelectedItems(preference.DisplayListingsIDs);
        }

        public void SetActiveFilters(string filters)
        {
            ActiveFiltersHidden.Value = filters;
        }

        /// <summary>
        /// We expose the rendered xml to the page via the XmlDoc.
        /// </summary>
        public SerializableXmlDocument XmlDoc { get; set; }

        /// <summary>
        /// Set this to use arbitrary search criteria (e.g. stuff that hasn't been saved).  Otherwise, we will
        /// try to locate a preference and use it as the search criteria.
        /// </summary>
        public IMarketListingFilterSpecification SearchSpecification { get; set; }


        #endregion

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // If we won't re-render our output, we can avoid a bunch of work.
            if (!WillReRender())
            {
                return;
            }

            // Tell the presenter about the view.
            Presenter.View = this;

            Presenter.PreRender();
        }

        /// <summary>
        /// Show/hide Dealer, Certified and Savings column placeholders
        /// </summary>
        protected void VehicleComparisonRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            // Show the display column if we are in "edit mode".  Hide it otherwise.
            var displayColumn = e.Item.FindControl("DisplayColumnPlaceHolder") as PlaceHolder;
            if (displayColumn != null)
                displayColumn.Visible = ShowDisplayedColumn;

            var dealerColumn = e.Item.FindControl("DisplayDealerPlaceHolder") as PlaceHolder;
            if (dealerColumn != null)
                dealerColumn.Visible = ShowDealerName;

        }

        #region Implementation of IInventoryView

        public string OwnerHandle
        {
            get { return Handles.OwnerHandle; }
        }

        public string VehicleHandle
        {
            get { return Handles.VehicleHandle; }
        }

        public int OfferPrice
        {
            get { return Deal.OfferPrice; }
        }

        #endregion
    }
}