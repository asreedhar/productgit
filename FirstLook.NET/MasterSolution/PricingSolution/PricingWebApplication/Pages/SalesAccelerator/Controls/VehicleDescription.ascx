<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="VehicleDescription.ascx.cs" 
    Inherits="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls.VehicleDescription" %>

<div class="previewPanel clearfix">
    <asp:CheckBox ID="ToggleDisplayOnSellingSheetCheckBox" runat="server" AutoPostBack="false" CssClass="displayOnSheetCkb" Text="Display on Consumer Packet?" OnCheckedChanged="ToggleDisplayDescriptionCheckBox_CheckChanged" />
    <asp:HiddenField ID="UseLongLayoutHidden" runat="server" />
    
    <asp:Panel ID="VehicleDescriptionContentPanel" runat="server" CssClass="vehicleDescriptionContent">
        <a class="moduleEditLink" href="#">Edit Ad Preview</a>
        <h4>Ad Preview:</h4>
        <p> <asp:Literal ID="AdvertisementLabel" runat="server" /></p>
    </asp:Panel>
</div>