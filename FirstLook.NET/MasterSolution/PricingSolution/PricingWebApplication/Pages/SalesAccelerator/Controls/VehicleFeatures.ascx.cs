using System;
using FirstLook.Common.Core.Xml;
using FirstLook.Merchandising.DomainModel.Marketing.Deal;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls
{
    public partial class VehicleFeatures : BaseUserControl, IXmlDoc
    {
        /// <summary>
        /// Make sure the control is visible so we keep re-evaluating the equipment list
        /// in case the data changes.
        /// </summary>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            Visible = true;
        }

        /// <summary>
        /// Determine the correct equipment provider for the reference vehicle and
        /// whether the control is visible.
        /// </summary>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            // If we won't re-render our output, we can avoid a bunch of work.
            if (!WillReRender())
            {
                return;
            }

            if (Visible)
            {
                var vehicleFeatures = Deal.GetVehicleFeatures();
                Deal.SaveXml(vehicleFeatures);

                if (vehicleFeatures.Equipment.Count > 0)
                {
                    var xmlDoc = Deal.GetXml(NodeType.vehicleequipment);
                    VehicleFeaturesRepeater.DataSource = xmlDoc.SelectNodes("/vehicleequipment/item");

                    VehicleFeaturesRepeater.DataBind();
                }
                else
                {
                    VehicleFeaturesPreviewPanel.Attributes["class"] += " emptyModule";
                    VehicleFeaturesPreviewPanel.Attributes.Add("title", "This module will NOT be printed.");
                }
            }
        }

        SerializableXmlDocument IXmlDoc.XmlDoc
        {
            get { return Deal.GetXml(NodeType.vehicleequipment); }
        }
    }
}