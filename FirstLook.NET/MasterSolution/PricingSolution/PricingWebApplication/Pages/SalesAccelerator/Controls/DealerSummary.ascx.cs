using System;
using FirstLook.Common.Core.Xml;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls
{
    public partial class DealerSummary : BaseUserControl, IXmlDoc    
    {
        public readonly SerializableXmlDocument XmlDoc = new SerializableXmlDocument();

        public bool ShowDealerLogo { get; set; }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // If we won't re-render our output, we can avoid a bunch of work.
            if (!WillReRender())
            {
                return;
            }

            try
            {
                var dealerSummary = Deal.GetDealerSummary();

                if (dealerSummary.Xml.Length > 0)
                {
                    XmlDoc.LoadXml(dealerSummary.Xml);

                    DealerLogoImage.ImageUrl = dealerSummary.DisplayUrl;
                    DealerLogoImage.AlternateText = XmlDoc.SelectSingleNode("/dealer/name").InnerText ?? string.Empty;
                    DealerLogoImage.Visible = !string.IsNullOrEmpty(dealerSummary.DisplayUrl);
                    DealerLogoImage.Height = dealerSummary.DisplayHeight;
                    DealerLogoImage.Width = dealerSummary.DisplayWidth;

                    Visible = true;

                    DealerDescriptionP.Visible = XmlDoc.SelectNodes("dealer/description[text() != '']").Count > 0;
                    DealerContactP.Visible = XmlDoc.SelectNodes("dealer/contact/*[text() != '']").Count > 0
                        || XmlDoc.SelectNodes("dealer/url[text() != '']").Count > 0;

                    DisplayContactInfoOnPdfHidden.Value = XmlDoc.SelectSingleNode("dealer/displayContactInfoOnWebPdf").InnerText;

                    Deal.SaveXml(dealerSummary);

                }
                else
                {
                    Visible = false;
                }

                ShowDealerLogo = DealerLogoImage.Visible;

            }
            catch (Exception ex)
            {
                Log(ex);
                Visible = false;
            }
        }

        SerializableXmlDocument IXmlDoc.XmlDoc
        {
            get { return XmlDoc; }
        }
    }
}