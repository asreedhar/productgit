﻿using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits;
using FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls.Preferences
{
    public partial class BenefitsVehiclePreference : BaseUserControl
    {
        private bool _isCertified;
        private CertifiedVehiclePreference _preference;

        private CertifiedVehiclePreference Preference
        {
            get
            {
                if (_preference == null)
                {
                    try
                    {
                        _preference = CertifiedVehiclePreference.GetOrCreateCertifiedVehiclePreference(Handles.OwnerHandle, Handles.VehicleHandle);
                    }
                    catch (Exception e)
                    {
                        Log(e);
                    }
                }
                return _preference;
            }
            set { _preference = value; }
        }

        protected string BenefitProgramName
        {
            get
            {
                return Preference != null ? Preference.ProgramName : "Certified Benefits";
            }
        }

        protected IEnumerable<ICertifiedVehicleBenefit> GetBenefits()
        {
            return Preference != null ? Preference.GetBenefits() : new List<ICertifiedVehicleBenefit>();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            _isCertified = InitIsCertifiedInventory();

            VehicleIsCertified.Value = _isCertified ? "1" : "0";
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // If we won't re-render our output, we can avoid a bunch of work.
            if (!WillReRender())
            {
                return;
            }

            if (Preference != null)
            {


                if (!_isCertified)
                {
                    const string certified =
                        "Certified benefits will not be printed on the Website PDF because this vehicle is not marked as certified.";

                    if (!MessagingLabel.Text.Contains(certified))
                    {
                        MessagingLabel.Text += certified;
                    }

                    MessagingLabel.Visible = true;
                }

                CPOAssociationExists.Value = "1";
            }
            else
            {
                MessagingLabel.Text = @"There is currently no association between your dealership, this vehicle and a certified benefits program. ";
                MessagingLabel.Visible = true;
                CPOAssociationExists.Value = "0";
            }

        }

        protected void ToggleDisplayOnSellingSheetCheckBox_CheckChanged(object sender, EventArgs e)
        {
            if (Preference != null)
            {
                Preference.IsDisplayed = !Preference.IsDisplayed;
                Preference.Save();
            }
        }

        protected void ToggleDisplayOnSellingSheetCheckBox_PreRender(object sender, EventArgs e)
        {
            ToggleDisplayOnSellingSheetCheckBox.Checked = Preference != null && Preference.IsDisplayed;
        }

        protected void ResetToDefaultsButton_OnClick(object sender, EventArgs e)
        {
            if (Preference != null && !Preference.IsNew)
            {
                // Delete the preference.
                Preference.Delete();
                Preference.Save();
            }

            Preference = null;
        }



        private bool InitIsCertifiedInventory()
        {
            bool isInventory, isCertified;

            // Is this vehicle certified?
            try
            {
                var inventory = Inventory.GetInventory(Handles.OwnerHandle, Handles.VehicleHandle);
                isCertified = inventory.Certified || inventory.CertifiedProgramId.HasValue;
                isInventory = true;
            }
            catch (Exception)
            {
                // this is not an inventory item.
                isInventory = false;
                isCertified = false;
            }

            return isInventory && isCertified;
        }
    }
}