﻿using System;
using System.Collections.Generic;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls.Preferences
{
    public partial class MarketAnalysisVehiclePreference : BaseUserControl
    {

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // If we won't re-render our output, we can avoid a bunch of work.
            if (!WillReRender())
            {
                return;
            }

            // set the properties of our control based on preference data
            ShowPricingGauge.Checked = Deal.MarketAnalysis.VehiclePreference.IsGaugeDisplayed;

        }

        protected void ToggleDisplayOnSellingSheetCheckBox_CheckChanged(object sender, EventArgs e)
        {
            Deal.MarketAnalysis.VehiclePreference.IsDisplayed = !Deal.MarketAnalysis.VehiclePreference.IsDisplayed;
            //Preference.Save();
        }

        protected void ToggleDisplayOnSellingSheetCheckBox_PreRender(object sender, EventArgs e)
        {
            ToggleDisplayOnSellingSheetCheckBox.Checked = Deal.MarketAnalysis.VehiclePreference.IsDisplayed;
        }

        protected void ResetToDefaultsButton_OnClick(object sender, EventArgs e)
        {
            // Reset the deal's market analysis
            Deal.ResetMarketAnalysis();

            // Delete the underlying vehicle preference.
            var preference = GetVehiclePreference();
            preference.Delete();
            preference.Save();
        }

       

        protected void SaveButton_OnClick(object sender, EventArgs e)
        {
            // set the values from the page
            Deal.MarketAnalysis.VehiclePreference.IsGaugeDisplayed = ShowPricingGauge.Checked;

            
            var selectedPriceProviders = PricingAnalysis1.SelectedPriceProviderIds;
            Deal.MarketAnalysis.VehiclePreference.HasOverriddenPriceProviders = PricingAnalysis1.UseOverriddenPrices;

            var providers = new List<MarketAnalysisPriceProviders>();
            foreach (var id in selectedPriceProviders)
            {
                if (Enum.IsDefined(typeof(MarketAnalysisPriceProviders), id))
                    providers.Add((MarketAnalysisPriceProviders) Enum.ToObject(typeof(MarketAnalysisPriceProviders), id));
                else
                {
                    throw new ApplicationException("The value of " + id +
                                                   " was not a valid MarketAnalysisPriceProvider value.");
                }
            }

            Deal.MarketAnalysis.VehiclePreference.SelectedPriceProviders = providers;
 
            // Only save changes to the domain (and db) if the user has specified to save the changes.  Otherwise, we will simply cache
            // the changes for the duration of "the current deal".
            if (PricingAnalysis1.SavePriceOverrides())
            {
                // Update the domain with the cached preference.
                var preference = GetVehiclePreference();
                preference.UpdateFrom(Deal.MarketAnalysis.VehiclePreference);                
                preference.Save();
                Deal.ResetMarketAnalysis();
            }
        }

        private Merchandising.DomainModel.Marketing.MarketAnalysis.MarketAnalysisVehiclePreference GetVehiclePreference()
        {
            return Merchandising.DomainModel.Marketing.MarketAnalysis.MarketAnalysisVehiclePreference.
                GetOrCreateMarketAnalysisVehiclePreference(Handles.OwnerHandle, Handles.VehicleHandle);
        }

        protected void RefreshPricingAnalysisButton_Click(object sender, EventArgs e)
        {
            Deal.MarketAnalysis.Pricing = null;
        }
    }
}