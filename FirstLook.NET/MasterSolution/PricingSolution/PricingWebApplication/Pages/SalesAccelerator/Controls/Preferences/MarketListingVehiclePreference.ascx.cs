﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel.Marketing.Deal;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;
using Domain = FirstLook.Merchandising.DomainModel.Marketing.MarketListings;
using FirstLook.Common.Core;
using System.Linq;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls.Preferences
{
    public partial class MarketListingVehiclePreference : BaseUserControl, IMarketListingFilterSpecification, IMarketListingSortSpecification
    {
        private const int DisplayedPagesLinks = 7;

        private Domain.MarketListingVehiclePreference _preference;

        public IMarketListingSearchAdapter SearchAdapter { get; set; }
        public ILogger Logger {get; set;}

        public Domain.MarketListingVehiclePreference Preference
        {
            get
            {
                if (_preference == null)
                {
                    _preference = Domain.MarketListingVehiclePreference.GetOrCreate(Handles.VehicleHandle, Handles.OwnerHandle, Deal.OfferPrice);
                    MarketListings1.RemoveAddSelectedItems(_preference);
                }
                return _preference;
            }
        }

        public int CurrentPage
        {
            get
            {
                if(ViewState["CurrentPage"] == null)
                    return 1;

                return (int) ViewState["CurrentPage"];
            }
            set
            {
                ViewState["CurrentPage"] = value;
            }
        }

        public SortCode Sortcode
        {
            get
            {
                if (ViewState["SortCode"] == null)
                    return SortCode.ListPrice;

                return (SortCode)ViewState["SortCode"];
            }
            set
            {
                ViewState["SortCode"] = value;
            }
        }

        public bool SortAscending
        {
            get
            {
                if (ViewState["SortAscending"] == null)
                    return false;

                return (bool)ViewState["SortAscending"];
            }
            set
            {
                ViewState["SortAscending"] = value;
            }
        }

        public int MileageHigh
        {
            get
            {
                int mileageHigh = int.Parse(MileageFilterHighDropDownList.SelectedValue);
                int mileageLow = int.Parse(MileageFilterLowDropDownList.SelectedValue);

                return mileageHigh >= mileageLow ? mileageHigh : mileageLow;
            }
        }

        public int MileageLow
        {
            get
            {
                int mileageHigh = int.Parse(MileageFilterHighDropDownList.SelectedValue);
                int mileageLow = int.Parse(MileageFilterLowDropDownList.SelectedValue);

                return mileageLow <= mileageHigh ? mileageLow : mileageHigh;
            }
        }

        public int PriceHigh
        {
            get
            {
                int priceHigh = int.Parse(PriceFilterHighDropDownList.SelectedValue);
                int priceLow = int.Parse(PriceFilterLowDropDownList.SelectedValue);

                return priceHigh >= priceLow ? priceHigh : priceLow;
            }
        }

        public int PriceLow
        {
            get
            {
                int priceHigh = int.Parse(PriceFilterHighDropDownList.SelectedValue);
                int priceLow = int.Parse(PriceFilterLowDropDownList.SelectedValue);

                return priceLow <= priceHigh ? priceLow : priceHigh;
            }
        }

        public string DealerName
        {
            get
            {
                if(DealerFilterDropDownList.SelectedIndex == 0)
                    return null;
                
                return DealerFilterDropDownList.SelectedValue;
            }
        }

        public string[] Colors
        {
            get
            {   
                return (from ListItem item in ColorFilterCheckBoxList.Items
                                        where item.Selected
                                        select item.Value).ToArray();
            }
        }

        public string TrimLevel
        {
            get
            {
                if (TrimFilterDropDownList.SelectedIndex == 0)
                    return null;

                return TrimFilterDropDownList.SelectedValue;
            }
        }

        public int PageSize
        {
            get
            {
                return 20;
            }
        }

        private string HeaderCssClass
        {
            get
            {
                return SortAscending ? "headerSortDown" : "headerSortUp";
            }
        }


        protected override void OnPreRender( EventArgs e )
        {
            base.OnPreRender( e );

            // If we won't re-render our output, we can avoid a bunch of work.
            if( !WillReRender() )
            {
                return;
            }

            int filteredCount;
            var listings = PerformSearch(
                this,
                this,
                Preference.UserName,
                out filteredCount
            );

            EmptyDataSetLabel.Visible = listings.Count() == 0;

            ListingRepeater.DataSource = listings;
            ListingRepeater.DataBind();

            PagingRepeater.DataSource = GetPagingData( filteredCount );
            PagingRepeater.DataBind();
            PagingRepeater.Visible = !EmptyDataSetLabel.Visible;

            PagingLabel.Text = GetPagingLabel( filteredCount );

            var activeFilter = new ListingsActiveFilter( this );
            MarketListings1.SetActiveFilters( activeFilter.ToJson() );
        }


        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if(!IsPostBack)
            {
                BindFilters();
                MarketListingsPriceColumnSortHeader.Attributes.Add("class", HeaderCssClass);
                DisplayDealerNameCheckbox.Checked = Preference.ShowDealerName;
                DisplayAdPreviewCheckbox.Checked = Deal.ShowAdPreviews();
            }
        }

        private void BindFilters()
        {
            var allListings = SearchAdapter.Search( Handles.VehicleHandle, Handles.OwnerHandle, Preference.UserName );

            BindColorFilter( allListings );
            BindDealerFilter( allListings );
            BindTrimFilter( allListings );

            BindMileageFilter( allListings );
            BindPriceFilter( allListings );
        }

        private void BindMileageFilter( IEnumerable<IMarketListing> allListings )
        {
            int minMileage = allListings.Min(x => x.Mileage) ?? int.MinValue;
            int maxMileage = allListings.Max(x => x.Mileage) ?? int.MaxValue;

            int min = (minMileage / Domain.MarketListingVehiclePreference.MileageIncrements) * Domain.MarketListingVehiclePreference.MileageIncrements;
            int minSelectedValue = Domain.MarketListingVehiclePreference.GetMileageLowFilter(Preference.OwnerHandle, Preference.VehicleHandle);
            int max = ((int)Math.Ceiling(maxMileage / (double)Domain.MarketListingVehiclePreference.MileageIncrements)) * Domain.MarketListingVehiclePreference.MileageIncrements;
            int maxSelectedValue = max;
            
            if (minSelectedValue < min)
                min = minSelectedValue;

            for (int x = min; x <= max; x += Domain.MarketListingVehiclePreference.MileageIncrements)
            {
                MileageFilterLowDropDownList.Items.Add(x.ToString());
                MileageFilterHighDropDownList.Items.Add(x.ToString());
            }

            MileageFilterLowDropDownList.SelectedValue = minSelectedValue.ToString();
            MileageFilterHighDropDownList.SelectedValue = maxSelectedValue.ToString();
        }

        private void BindPriceFilter(IEnumerable<IMarketListing> allListings)
        {
            int minPrice = allListings.Min(x => x.ListPrice) ?? int.MinValue;
            int maxPrice = allListings.Max(x => x.ListPrice) ?? int.MaxValue;

            int min = (minPrice / Domain.MarketListingVehiclePreference.PriceIncrements) * Domain.MarketListingVehiclePreference.PriceIncrements;
            int minSelectedValue = min;
            int max = ((int)Math.Ceiling(maxPrice / (double)Domain.MarketListingVehiclePreference.PriceIncrements)) * Domain.MarketListingVehiclePreference.PriceIncrements;
            int maxSelectedValue = max;

            for (int x = min; x <= max; x += Domain.MarketListingVehiclePreference.PriceIncrements)
            {
                PriceFilterLowDropDownList.Items.Add(new ListItem(string.Format("${0}", x), x.ToString()));
                PriceFilterHighDropDownList.Items.Add(new ListItem(string.Format("${0}", x), x.ToString()));
            }

            PriceFilterLowDropDownList.SelectedValue = minSelectedValue.ToString();
            PriceFilterHighDropDownList.SelectedValue = maxSelectedValue.ToString();
        }

        private void BindColorFilter(IEnumerable<IMarketListing> allListings)
        {
            var colors = allListings.Select(x => x.Color).OrderBy(x => x).Distinct();
            ColorFilterCheckBoxList.DataSource = colors;
            ColorFilterCheckBoxList.DataBind();
            foreach (ListItem item in ColorFilterCheckBoxList.Items)
                item.Selected = true;
        }

        private void BindDealerFilter(IEnumerable<IMarketListing> allListings)
        {
            var dealers = allListings.Select(x => x.DealerName).OrderBy(x => x).Distinct();
            DealerFilterDropDownList.DataSource = dealers;
            DealerFilterDropDownList.DataBind();
            DealerFilterDropDownList.Items.Insert(0, "All");
            DealerFilterDropDownList.SelectedIndex = 0;            
        }

        private void BindTrimFilter(IEnumerable<IMarketListing> allListings)
        {
            var trim = allListings.Select(x => x.VehicleName).OrderBy(x => x).Distinct();
            TrimFilterDropDownList.DataSource = trim;
            TrimFilterDropDownList.DataBind();
            TrimFilterDropDownList.Items.Insert(0, "All");
            TrimFilterDropDownList.SelectedIndex = 0;
        }

        private IEnumerable<IMarketListing> PerformSearch(IMarketListingFilterSpecification filterSpecification,
                                 IMarketListingSortSpecification sortSpecification, string userName, out int filteredListingCount)
        {
            filteredListingCount = 0;
            try
            {
                var marketListings = SearchAdapter.Search(Handles.VehicleHandle, Handles.OwnerHandle, filterSpecification, userName);
                filteredListingCount = marketListings.Count();

                marketListings = marketListings.OrderBySortCode(sortSpecification.Sortcode, sortSpecification.SortAscending);
                marketListings = marketListings.Skip((CurrentPage-1)*PageSize).Take(PageSize);

                return marketListings;
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                return new List<IMarketListing>();
            }
        }

        private string GetPagingLabel(int totalItems)
        {
            int upperBound = (CurrentPage*PageSize) > totalItems ? totalItems : (CurrentPage*PageSize);
            int lowerBound = totalItems != 0 ? ((CurrentPage - 1) * PageSize) + 1 : 0;
            return string.Format("Showing {0} to {1} of {2}", lowerBound, upperBound, totalItems);
        }

        private IList<PagingItem> GetPagingData(int totalItems)
        {
            const int LeftRightPageCount = DisplayedPagesLinks / 2;
            var pagingItems = new List<PagingItem>();
            int totalPages = (int)Math.Ceiling(totalItems / (double)PageSize);
            if (totalPages == 1)
                return pagingItems;


            int previousPage = CurrentPage - 1 <= 0 ? 1 : CurrentPage - 1;
            int nextPage = CurrentPage + 1 >= totalPages ? totalPages : CurrentPage + 1;

            if (totalPages > DisplayedPagesLinks)
            {
                pagingItems.Add(new PagingItem() {IsLink = true, DisplayPage = "&lt;&lt;", IsSelected = false, PageNumber = 1});
                pagingItems.Add(new PagingItem() {IsLink = true, DisplayPage = "&lt;", IsSelected = false, PageNumber = previousPage});
            }

            //front elipse
            if (CurrentPage - LeftRightPageCount > 1 && totalPages > DisplayedPagesLinks)
                pagingItems.Add(new PagingItem() { IsLink = false, DisplayPage = "...", IsSelected = false, PageNumber = -1 });

            
            //add left items if near end
            int addLeftItems = CurrentPage + LeftRightPageCount > totalPages ? (CurrentPage + LeftRightPageCount) - totalPages : 0;
            int startPage = CurrentPage - (LeftRightPageCount + addLeftItems) <= 1 ? 1 : CurrentPage - (LeftRightPageCount + addLeftItems);
            
            int displayPages = startPage + (DisplayedPagesLinks - 1) > totalPages ? totalPages : startPage + (DisplayedPagesLinks - 1);
            for (int page = startPage; page <= displayPages; page++)
            {
                var pagingItem = new PagingItem() { IsLink = true, PageNumber = page, DisplayPage = page.ToString(), IsSelected = (CurrentPage == page)};
                pagingItems.Add(pagingItem);
            }

            int rightEllipseCount = 0;
            if (CurrentPage - LeftRightPageCount > 1)
                rightEllipseCount = CurrentPage + LeftRightPageCount;
            else
                rightEllipseCount = DisplayedPagesLinks;

            if (rightEllipseCount < totalPages)
                pagingItems.Add(new PagingItem() { IsLink = false, DisplayPage = "...", IsSelected = false, PageNumber = -1 });


            if (totalPages > DisplayedPagesLinks)
            {
                pagingItems.Add(new PagingItem() { IsLink = true, DisplayPage = "&gt;", IsSelected = false, PageNumber = nextPage });
                pagingItems.Add(new PagingItem() { IsLink = true, DisplayPage = "&gt;&gt;", IsSelected = false, PageNumber = totalPages });
            }

            return pagingItems;
        }

        protected void PagingRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var pagingData = e.Item.DataItem as PagingItem;
            LinkButton button = e.Item.FindControl("PageLinkButton") as LinkButton;
            
            if(pagingData.IsSelected)
                button.CssClass = "selectedPage";

            var linkPlaceholder = e.Item.FindControl("LinkPlaceHolder") as PlaceHolder;
            var nonLinkPlaceHolder = e.Item.FindControl("NonLinkPlaceHolder") as PlaceHolder;

            linkPlaceholder.Visible = pagingData.IsLink;
            nonLinkPlaceHolder.Visible = !pagingData.IsLink;
        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            Preference.ShowDealerName = DisplayDealerNameCheckbox.Checked;
            Deal.ShowAdPreviews( DisplayAdPreviewCheckbox.Checked );

            // Save the pref.
            Preference.Save();
            MarketListings1.SetOrignalItems();

            _preference = null;
        }

        protected void PageClicked(object sender, EventArgs e)
        {
            var linkButton = sender as LinkButton;
            if (linkButton != null)
                CurrentPage = int.Parse(linkButton.Attributes["pageNumber"]);
        }

        protected void LinkButtonResetToDefaults_Click(object sender, EventArgs e)
        {
            Preference.Delete();
            Preference.Save();
            _preference = null;
            MarketListings1.ResetOrignalAndSelectedItems(Preference);

            DisplayDealerNameCheckbox.Checked = Preference.ShowDealerName;
            bool showAdPreviewsDefault = new PrintOverridesDTO().ShowAdPreviews;
            DisplayAdPreviewCheckbox.Checked = showAdPreviewsDefault;
            Deal.ShowAdPreviews(showAdPreviewsDefault);
        }

        protected void ToggleDisplayOnSellingSheetCheckBox_CheckChanged(object sender, EventArgs e)
        {
            Preference.IsDisplayed = !Preference.IsDisplayed;
            Preference.Save();
        }

        protected void ToggleDisplayOnSellingSheetCheckBox_PreRender(object sender, EventArgs e)
        {
            ToggleDisplayOnSellingSheetCheckBox.Checked = Preference.IsDisplayed;
        }

        private void ClearHeaderCss()
        {
            foreach (HtmlControl control in FilterRow.Controls)
                control.Attributes.Remove("class");
        }

        protected void OnDealerColumnSortClick(object sender, EventArgs e)
        {
            ClearHeaderCss();
            if (Sortcode == SortCode.DealerName)
                SortAscending = !SortAscending;

            Sortcode = SortCode.DealerName;
            MarketListingsDealerColumnHeader.Attributes.Add("class", HeaderCssClass);
        }

        protected void OnVehicleColumnSortClick(object sender, EventArgs e)
        {
            ClearHeaderCss();
            if(Sortcode == SortCode.VehicleName)
                SortAscending = !SortAscending;

            Sortcode = SortCode.VehicleName;
            MarketListingsVehicleColumnHeader.Attributes.Add("class", HeaderCssClass);
        }

        protected void OnColorColumnSortClick(object sender, EventArgs e)
        {
            ClearHeaderCss();
            if (Sortcode == SortCode.Color)
                SortAscending = !SortAscending;

            Sortcode = SortCode.Color;
            MarketListingsColorColumnHeader.Attributes.Add("class", HeaderCssClass);
        }

        protected void OnMileageColumnSortClick(object sender, EventArgs e)
        {
            ClearHeaderCss();
            if (Sortcode == SortCode.Mileage)
                SortAscending = !SortAscending;

            Sortcode = SortCode.Mileage;
            MarketListingsMileageColumnHeader.Attributes.Add("class", HeaderCssClass);
        }

        protected void OnCertifiedColumnClick(object sender, EventArgs e)
        {
            ClearHeaderCss();
            if (Sortcode == SortCode.IsCertified)
                SortAscending = !SortAscending;

            Sortcode = SortCode.IsCertified;
            MarketListingsCertifiedColumnHeader.Attributes.Add("class", HeaderCssClass);
        }

        protected void OnPriceColumnSortClick(object sender, EventArgs e)
        {
            ClearHeaderCss();
            if (Sortcode == SortCode.ListPrice)
                SortAscending = !SortAscending;

            Sortcode = SortCode.ListPrice;
            MarketListingsPriceColumnSortHeader.Attributes.Add("class", HeaderCssClass);
        }

        protected void OnFilterClick(object sender, EventArgs e)
        {
            CurrentPage = 1;
        }

        protected void OnClearFiltersClick(object sender, EventArgs e)
        {
            CurrentPage = 1;

            foreach (ListItem item in ColorFilterCheckBoxList.Items)
                item.Selected = true;

            DealerFilterDropDownList.SelectedIndex = 0;
            TrimFilterDropDownList.SelectedIndex = 0;

            MileageFilterLowDropDownList.SelectedIndex = 0;
            MileageFilterHighDropDownList.SelectedIndex = MileageFilterHighDropDownList.Items.Count - 1;

            PriceFilterLowDropDownList.SelectedIndex = 0;
            PriceFilterHighDropDownList.SelectedIndex = PriceFilterHighDropDownList.Items.Count - 1;
        }

        private class ListingsActiveFilter
        {
            private MarketListingVehiclePreference Parent { get; set; }
            public ListingsActiveFilter(MarketListingVehiclePreference parent)
            {
                Parent = parent;
            }

            public bool DealerNameFilterActive
            {
                get
                {
                    return Parent.DealerName != null;
                }
            }


            public bool TrimLevelFilterActive
            {
                get
                {
                    return Parent.TrimLevel != null;
                }
            }


            public bool ColorsFilterActive
            {
                get
                {
                    return Parent.Colors.Length != Parent.ColorFilterCheckBoxList.Items.Count;
                }
            }

            public bool MileageFilterActive
            {
                get
                {
                    return !((Parent.MileageFilterLowDropDownList.SelectedIndex == 0 &&
                             Parent.MileageFilterHighDropDownList.SelectedIndex == Parent.MileageFilterHighDropDownList.Items.Count - 1) ||
                            (Parent.MileageFilterHighDropDownList.SelectedIndex == 0 &&
                             Parent.MileageFilterLowDropDownList.SelectedIndex == Parent.MileageFilterLowDropDownList.Items.Count - 1));
                }
            }

            public bool PriceFilterActive
            {
                get
                {
                    return !((Parent.PriceFilterLowDropDownList.SelectedIndex == 0 &&
                             Parent.PriceFilterHighDropDownList.SelectedIndex == Parent.PriceFilterHighDropDownList.Items.Count - 1) ||
                            (Parent.PriceFilterHighDropDownList.SelectedIndex == 0 &&
                             Parent.PriceFilterLowDropDownList.SelectedIndex == Parent.PriceFilterLowDropDownList.Items.Count - 1));
                }
            }
        }

        

    }
}