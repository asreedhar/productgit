﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="MarketAnalysisVehiclePreference.ascx.cs" Inherits="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls.Preferences.MarketAnalysisVehiclePreference" %>
<%@ Register TagName="PricingAnalysis" TagPrefix="sawc" Src="~/Pages/SalesAccelerator/Controls/PricingAnalysis.ascx" %>

<div id="PricingAnalysisVehiclePreferences">
    <asp:CheckBox ID="ToggleDisplayOnSellingSheetCheckBox" runat="server" AutoPostBack="false" CssClass="displayOnSheetCkb" Text="Display on Consumer Packet?" OnCheckedChanged="ToggleDisplayOnSellingSheetCheckBox_CheckChanged" OnPreRender="ToggleDisplayOnSellingSheetCheckBox_PreRender" />
    <asp:Label ID="MessagingLabel" runat="server" CssClass="messagingLabel" Visible="false"></asp:Label>
    <div id="PricingAnalysisPreferenceContent">
        <asp:CheckBox ID="ShowPricingGauge" runat="server" Text="Show Pricing Gauge" CssClass="showGaugeCkb" />
        <sawc:PricingAnalysis ID="PricingAnalysis1" runat="server" ShowPriceList="false" EditMode="true"/>
    </div>
    <div class="prefPageActions clearfix">
        <asp:LinkButton ID="ResetToDefaultsButton" runat="server" Text="Reset to defaults" OnClick="ResetToDefaultsButton_OnClick" OnClientClick="SalesAccelerator.ResetModuleEditGlobals();" CssClass="resetBtn"></asp:LinkButton>
        <asp:Button ID="PricingAnalysisPreferenceCancelBtn" runat="server" CssClass="cancelBtn" Text="Cancel" />
        <asp:Button ID="SaveAndCloseButton" runat="server" OnClick="SaveButton_OnClick" CssClass="saveBtn closeBtn" Text="Save & Close" />
        <asp:Button ID="SaveButton" runat="server" OnClick="SaveButton_OnClick" CssClass="saveBtn" Text="Save" />
        
        <div class="reloadAfterPopUpFields" style="display:none;">
            <asp:LinkButton ID="RefreshPricingAnalysisButton" runat="server" OnClick="RefreshPricingAnalysisButton_Click" CssClass="saveBtn" >Refresh Pricing Analysis</asp:LinkButton>
            <input type="hidden" id="PricingAnalysisRefreshButtonClientIdHidden" value='<%= RefreshPricingAnalysisButton.ClientID %>' />
            <input type="hidden" id="PricingAnalysisRefreshButtonUniqueIdHidden" value='<%= RefreshPricingAnalysisButton.UniqueID %>' />
        </div>
    </div>
</div>
