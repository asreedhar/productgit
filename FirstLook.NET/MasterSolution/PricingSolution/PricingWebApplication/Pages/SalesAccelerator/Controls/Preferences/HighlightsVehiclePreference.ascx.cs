﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Extensions;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls.Preferences
{
    public partial class HighlightsVehiclePreference : BaseUserControl
    {
        #region Properties

        public event EventHandler Click;
        
        /// <summary>
        /// Consumer highlight vehicle preferences.
        /// </summary>
        private ConsumerHighlightsVehiclePreference Preference
        {
            get
            {
                if (_preference == null)
                {
                    try
                    {
                        _preference = ConsumerHighlightsVehiclePreference.GetOrCreateConsumerHighlightsPreference(Handles.OwnerHandle, Handles.VehicleHandle);
                    }
                    catch (Exception e)
                    {
                        Log(e);
                    }
                }
                return _preference;
            }
            set { _preference = value; }
        }
        private ConsumerHighlightsVehiclePreference _preference;

        #endregion

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // If we won't re-render our output, we can avoid a bunch of work.
            if (!WillReRender())
            {
                return;
            }

            BindHighlights();

            IncludeCarFaxOneOwnerIconCheckBox.Checked = Preference.IncludeCarfaxOneOwnerIcon;
            ToggleDisplayOnSellingSheetCheckBox.Checked = Preference.IsDisplayed;
        }

        protected void ToggleDisplayOnSellingSheetCheckBox_CheckChanged(object sender, EventArgs e)
        {
            Preference.IsDisplayed = !Preference.IsDisplayed;
            Preference.Save();
        }

       
        protected void ResetToDefaultsButton_OnClick(object sender, EventArgs e)
        {
            Preference.Delete();
            Preference.Save();
            Preference = null;
            BindHighlights();
        }

        /// <summary>
        /// Update a collection of highlights to the database.
        /// </summary>
        /// <param name="editedHighlights">Collection to save to the database.</param>
        /// <param name="sectionType">Type of highlights these are.</param>
        private void UpdateHighlights(IEnumerable<ConsumerHighlightDTO> editedHighlights, HighlightSectionType sectionType)
        {
            // Get the preference
            ConsumerHighlightsVehiclePreference preference = Preference;

            foreach (ConsumerHighlightDTO highlight in editedHighlights)
            {
                // Update the highlight.
                if (highlight.Key != null && preference != null)
                {
                    if (!highlight.IsNew)
                    {
                        // Free text.
                        if (sectionType == HighlightSectionType.FreeText)
                        {
                            preference.UpdateHighlight(highlight.Key, highlight.IsDisplayed, highlight.Text, highlight.Rank, highlight.IsDeleted);
                        }
                        // Circle ratings.
                        else if (sectionType == HighlightSectionType.CircleRatings)
                        {
                            preference.UpdateHighlight(highlight.Key, highlight.IsDisplayed, null, highlight.Rank, false);
                        }
                        // Vehicle history.
                        else if (sectionType == HighlightSectionType.VehicleHistory)
                        {
                            preference.UpdateHighlight(highlight.Key, highlight.IsDisplayed, highlight.Text, highlight.Rank, false);
                        }
                        // Snippets.
                        else if (sectionType == HighlightSectionType.Snippets)
                        {
                            preference.UpdateHighlight(highlight.Key, highlight.IsDisplayed, null, highlight.Rank, false);
                        }
                    }
                    else
                    {
                        if (sectionType == HighlightSectionType.FreeText && !highlight.IsDeleted)
                        {
                            preference.CreateFreeTextHighlight(highlight.IsDisplayed, highlight.Text, highlight.Rank);
                        }
                    }
                }
                else
                {
                    throw new ApplicationException("One or more controls were not found as expected.");
                }
            }
        }

        /// <summary>
        /// Save the highlight preferences.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        protected void SaveButton_OnClick(object sender, EventArgs e)
        {            
            ConsumerHighlightsVehiclePreference preference = Preference;

            // Update free text consumer highlights.
            var editedHighlights = FreeTextHighlightsItemsHidden.Value.FromJson<List<ConsumerHighlightDTO>>();
            UpdateHighlights(editedHighlights, HighlightSectionType.FreeText);

            // Update circle rating highlights.
            editedHighlights = JDPowerHighlightsItemsHidden.Value.FromJson<List<ConsumerHighlightDTO>>();
            UpdateHighlights(editedHighlights, HighlightSectionType.CircleRatings);

            // Update snippet highlights.
            editedHighlights = SnippetHighlightsItemsHidden.Value.FromJson<List<ConsumerHighlightDTO>>();
            UpdateHighlights(editedHighlights, HighlightSectionType.Snippets);

            // Update vehicle history highlights.
            editedHighlights = VehicleHistoryHighlightsItemsHidden.Value.FromJson<List<ConsumerHighlightDTO>>();
            UpdateHighlights(editedHighlights, HighlightSectionType.VehicleHistory);                       
            preference.IncludeCarfaxOneOwnerIcon = IncludeCarFaxOneOwnerIconCheckBox.Checked;

            // Save the preferences.
            preference.Save();
            Preference = null;

            // Tell lists to rebind.
            BindHighlights();

            if (Click != null) Click(this, e);
        }

        /// <summary>
        /// Bind all the consumer highlight types.
        /// </summary>
        private void BindHighlights()
        {            
            // JDPOWER CIRCLE RATINGS.

            // Split highlights into displayed and available lists
            var displayedCircleRatings = Preference.CircleRatingHighlights.Where(eq => eq.IsDisplayed).OrderBy(eq => eq.Rank);
            var availableCircleRatings = Preference.CircleRatingHighlights.Where(eq => !eq.IsDisplayed).OrderBy(eq => eq.Rank);

            // Bind highlights to repeater
            DisplayedJDPowerHighlightsRepeater.DataSource = displayedCircleRatings;
            AvailableJDPowerHighlightsRepeater.DataSource = availableCircleRatings;
            DisplayedJDPowerHighlightsRepeater.DataBind();
            AvailableJDPowerHighlightsRepeater.DataBind();

            // assign highlights to hidden fields
            JDPowerHighlightsItemsHidden.Value = Preference.CircleRatingHighlights.ToJson();

            // FREE TEXT.

            // Split highlights into displayed and available lists
            var displayedFreeText = Preference.FreeTextHighlights.Where(eq => eq.IsDisplayed).OrderBy(eq => eq.Rank);
            var availableFreeText = Preference.FreeTextHighlights.Where(eq => !eq.IsDisplayed).OrderBy(eq => eq.Rank);

            // Bind highlights to repeater
            DisplayedFreeTextHighlightsRepeater.DataSource = displayedFreeText;
            AvailableFreeTextHighlightsRepeater.DataSource = availableFreeText;
            DisplayedFreeTextHighlightsRepeater.DataBind();
            AvailableFreeTextHighlightsRepeater.DataBind();

            // assign highlights to hidden fields
            FreeTextHighlightsItemsHidden.Value = Preference.FreeTextHighlights.ToJson();

            // SNIPPETS.

            // Split highlights into displayed and available lists
            var displayedSnippets = Preference.SnippetHighlights.Where(eq => eq.IsDisplayed).OrderBy(eq => eq.Rank);
            var availableSnippets = Preference.SnippetHighlights.Where(eq => !eq.IsDisplayed).OrderBy(eq => eq.Rank);

            // Bind highlights to repeater
            DisplayedSnippetHighlightsRepeater.DataSource = displayedSnippets;
            AvailableSnippetHighlightsRepeater.DataSource = availableSnippets;
            DisplayedSnippetHighlightsRepeater.DataBind();
            AvailableSnippetHighlightsRepeater.DataBind();

            // assign highlights to hidden fields
            SnippetHighlightsItemsHidden.Value = Preference.SnippetHighlights.ToJson();

            // VEHICLE HISTORY.

            // Split highlights into displayed and available lists
            var displayedVehicleHistory = Preference.VehicleHistoryHighlights.Where(eq => eq.IsDisplayed).OrderBy(eq => eq.Rank);
            var availableVehicleHistory = Preference.VehicleHistoryHighlights.Where(eq => !eq.IsDisplayed).OrderBy(eq => eq.Rank);

            // Bind highlights to repeater
            DisplayedVehicleHistoryHighlightsRepeater.DataSource = displayedVehicleHistory;
            AvailableVehicleHistoryHighlightsRepeater.DataSource = availableVehicleHistory;
            DisplayedVehicleHistoryHighlightsRepeater.DataBind();
            AvailableVehicleHistoryHighlightsRepeater.DataBind();

            // assign highlights to hidden fields
            VehicleHistoryHighlightsItemsHidden.Value = Preference.VehicleHistoryHighlights.ToJson();
        }        

        /// <summary>
        /// Data transfer object for consumer highlights.
        /// </summary>
        private class ConsumerHighlightDTO : IConsumerHighlight
        {
            #region Implementation of ISurrogateKey

            public string Key { get; set; }

            #endregion

            #region Implementation of IConsumerHighlight

            public DateTime? ExpirationDate { get; private set; }
            public bool IsDisplayed { get; set; }
            public string Text { get; set; }
            public int ConsumerHighlightId { get; private set; }
            public int VehiclePreferenceId { get; private set; }
            public HighlightSectionType HighlightSection { get; private set; }
            public HighlightType Provider { get; private set; }
            public int? ProviderSourceRowId { get; private set; }
            public int Rank { get; set; }
            public bool IsDeleted { get; set; }
            public bool IsNew { get; set; }
            public bool CanDelete { get; set; }
            public bool CanEdit { get; set; }

            #endregion
        }
        
    }
}