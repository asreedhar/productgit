﻿using System;
using Domain = FirstLook.Merchandising.DomainModel.Marketing.AdPreview;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls.Preferences
{
    public partial class AdPreviewVehiclePreference : BaseUserControl
    {
        private Domain.AdPreviewVehiclePreference _preference;

        private Domain.AdPreviewVehiclePreference Preference
        {
            get
            {
                if (_preference == null)
                    _preference = Domain.AdPreviewVehiclePreference.GetOrCreate(Handles.VehicleHandle, Handles.OwnerHandle);

                return _preference;
            }
        }

        public int CharacterLimitHidden
        {
            get
            {
                int result = 0;
                int.TryParse(CharCountHiddenField.Value, out result);
                return result;
            }
            set
            {
                CharCountHiddenField.Value = value.ToString();
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // If we won't re-render our output, we can avoid a bunch of work.
            if (!WillReRender())
            {
                return;
            }

            try
            {
                CharacterLimitHidden = Preference.CharacterLimit;
                AdvertisementLabel.Text = Preference.Advertisement;
                if( string.IsNullOrEmpty( Preference.Advertisement ) )
                {
                    AdPreviewVehiclePreferencesPanel.Style.Add( "display", "none" );
                    AdvertisementLabel.Text = "No Ad Preview available for this vehicle";
                }
                else
                {
                    AdPreviewVehiclePreferencesPanel.Style.Add("display", "block");
                }
            }
            catch (Exception ex)
            {
                Log(ex);
                Visible = false;
            }
        }

        protected void SaveButton_OnClick(object sender, EventArgs e)
        {
            Preference.CharacterLimit = CharacterLimitHidden;
            Preference.Save();
        }

        protected void ResetToDefaultsButton_OnClick(object sender, EventArgs e)
        {

        }
    }
}