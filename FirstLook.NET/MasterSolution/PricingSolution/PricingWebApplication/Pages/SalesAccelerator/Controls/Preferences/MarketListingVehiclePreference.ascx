﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="MarketListingVehiclePreference.ascx.cs" 
    Inherits="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls.Preferences.MarketListingVehiclePreference" %>
<%@ Import Namespace="FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search" %>
<%@ Import Namespace="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator"%>
<%@ Register Src="~/Pages/SalesAccelerator/Controls/MarketListings.ascx" TagPrefix="cva" TagName="MarketListings" %>

<div id="MarketListingsVehiclePreferences">
    <asp:CheckBox ID="ToggleDisplayOnSellingSheetCheckBox" runat="server" AutoPostBack="false" CssClass="displayOnSheetCkb" 
     Text="Display on Consumer Packet?" OnCheckedChanged="ToggleDisplayOnSellingSheetCheckBox_CheckChanged" 
     OnPreRender="ToggleDisplayOnSellingSheetCheckBox_PreRender" />
        
    <asp:Label ID="MessagingLabel" runat="server" CssClass="messagingLabel" Visible="false"></asp:Label>

    <div id="MarketListingsForm" class="clearfix">
        
        <div id="DisplayDealerNameCheckbox">
            <asp:CheckBox ID="DisplayDealerNameCheckbox" runat="server" Text="Show Dealer Name" TextAlign="Right" />
        </div>
  
        <div id="DisplayAdPreviewCheckboxDiv">
            <asp:CheckBox ID="DisplayAdPreviewCheckbox" runat="server" Text="Show Ad Previews" TextAlign="Right" />
        </div>
        
        <ul id="MarketListingsTabs" class="clearfix">
            <li class="selected" id="PowerSearchResultsTab"><a href="#">Power Search</a></li>
            <li id="SelectedListingsTab"><a href="#">Preview</a></li>
        </ul>
        
    </div>
    
    <div id="MarketListingsControl" class="clear">
    
        <div id="MarketListingsSearchPanel" class="marketListingsTabContent">
            
            <div class="tableContainer">
                <table id="MarketListingsPreferenceTable">
                    <colgroup>
                        <col width="9%" />
                        <col width="21%" />
                        <col width="21%" />
                        <col width="9%" />
                        <col width="9%" />
                        <col width="11%" />
                        <col width="11%" />
                        <col width="9%" />
                    </colgroup>
                    <thead>
                        <tr class="filters row1">
                            <td colspan="8">
                                <asp:Button ID="ClearFiltersButton" runat="server" Text="Clear Filters" CssClass="small" style="float:right;" OnClick="OnClearFiltersClick"/>
                                <span class="title">Filter Search Results:</span>
                            </td>
                        </tr>
                        <tr class="filters row2">
                            <td>&nbsp;</td>
                            <td>
                                <a href="#" class="filterPanelLink">&nbsp;<br />Dealer Name<span class="button"></span></a>
                                <div id="DealerFilterPanel" class="filterPanel">
                                    <asp:DropDownList ID="DealerFilterDropDownList" runat="server">
                                    </asp:DropDownList><br />
                                    <asp:Button ID="DealerFilterButton" runat="server" Text="Filter" CssClass="filterBtn" OnClick="OnFilterClick"/>
                                </div>
                            </td>
                            <td>
                                <a href="#" class="filterPanelLink">&nbsp;<br />Trim Level<span class="button"></span></a>
                                <div id="TrimFilterPanel" class="filterPanel">
                                    <asp:DropDownList ID="TrimFilterDropDownList" runat="server">
                                    </asp:DropDownList><br />
                                    <asp:Button ID="TrimFilterButton" runat="server" Text="Filter" CssClass="filterBtn" OnClick="OnFilterClick"/>
                                </div>
                            </td>
                            <td>
                                <a href="#" class="filterPanelLink">&nbsp;<br />Color<span class="button"></span></a>
                                <div id="ColorFilterPanel" class="filterPanel">
                                    <asp:CheckBoxList ID="ColorFilterCheckBoxList" runat="server" RepeatLayout="Flow" RepeatColumns="3">
                                    </asp:CheckBoxList><br />
                                    <asp:Button ID="ColorFilterButton" runat="server" Text="Filter" CssClass="filterBtn" OnClick="OnFilterClick"/>
                                </div>
                            </td>
                            <td>
                                <a href="#" class="filterPanelLink">Mileage<br />Range<span class="button"></span></a>
                                <div id="MileageFilterPanel" class="filterPanel">
                                    <asp:DropDownList ID="MileageFilterLowDropDownList" runat="server">
                                    </asp:DropDownList>&nbsp;to
                                    <asp:DropDownList ID="MileageFilterHighDropDownList" runat="server">
                                    </asp:DropDownList><br />
                                    <asp:Button ID="MileageFilterButton" runat="server" Text="Filter" CssClass="filterBtn" OnClick="OnFilterClick"/>
                                </div>
                            </td>
                            <td>&nbsp;</td>
                            <td>
                                <a href="#" class="filterPanelLink">Price<br />Range<span class="button"></span></a>
                                <div id="PriceFilterPanel" class="filterPanel">
                                    <div style="white-space:nowrap">
                                        <asp:DropDownList ID="PriceFilterLowDropDownList" runat="server">
                                        </asp:DropDownList>&nbsp;to
                                        <asp:DropDownList ID="PriceFilterHighDropDownList" runat="server">
                                        </asp:DropDownList>
                                    </div><br />
                                    <asp:Button ID="PriceFilterButton" runat="server" Text="Filter" CssClass="filterBtn" OnClick="OnFilterClick"/>
                                </div>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr ID="FilterRow" runat="server">
                            <th runat="server">Selected</th>          
                            <th align="left" ID="MarketListingsDealerColumnHeader" runat="server"><asp:LinkButton ID="MarketListingsDealerColumnSortLink" runat="server" OnClick="OnDealerColumnSortClick">Dealership<span class="arrowIcon"></span></asp:LinkButton></th>                        
                            <th align="left" ID="MarketListingsVehicleColumnHeader" runat="server"><asp:LinkButton  ID="MarketListingsVehicleColumnSortLink" runat="server" OnClick="OnVehicleColumnSortClick">Vehicle<span class="arrowIcon"></span></asp:LinkButton></th>
                            <th ID="MarketListingsColorColumnHeader" runat="server"><asp:LinkButton  ID="MarketListingsColorColumnSortLink" runat="server" OnClick="OnColorColumnSortClick">Color<span class="arrowIcon"></span></asp:LinkButton></th>
                            <th ID="MarketListingsMileageColumnHeader" runat="server"><asp:LinkButton ID="MarketListingsMileageColumnSortLink" runat="server" OnClick="OnMileageColumnSortClick">Mileage<span class="arrowIcon"></span></asp:LinkButton></th>
                            <th ID="MarketListingsCertifiedColumnHeader" runat="server"><asp:LinkButton ID="MarketListingsCertifiedColumnSortLink" runat="server" OnClick="OnCertifiedColumnClick">Certified?<span class="arrowIcon"></span></asp:LinkButton></th>
                            <th ID="MarketListingsPriceColumnSortHeader" runat="server"><asp:LinkButton  ID="MarketListingsPriceColumnSortLink" runat="server" OnClick="OnPriceColumnSortClick">Internet Price<span class="arrowIcon"></span></asp:LinkButton></th>
                            <th ID="MarketListingsSavingsColumnSortHeader" runat="server">Price Savings</th>
                        </tr>
                    </thead>
                    <tbody>
                         <asp:Repeater ID="ListingRepeater" runat="server">
                            <ItemTemplate>
                                <tr id='<%# ((IMarketListing)Container.DataItem).VehicleId %>' class='even <%# Preference.DisplayListingsIDs.IndexOf(((IMarketListing)Container.DataItem).VehicleId) >= 0 ? "selected" : string.Empty %>'>
                                
                                    <td align="center">
                                        <input type="checkbox" ID="DisplayListingCheckbox" runat="server" value='<%# ((IMarketListing)Container.DataItem).VehicleId %>' checked='<%# Preference.DisplayListingsIDs.IndexOf(((IMarketListing)Container.DataItem).VehicleId) >= 0%>' />
                                    </td>
                                
                                    <td class="dealer"><%# Eval("DealerName")%></td>
                                    <td><%# Eval("VehicleName")%></td>
                                    <td align="center"><%# Eval("Color")%></td>
                                    <td align="center"><%# Utility.FormatNumber(Eval("Mileage").ToString(), NumberType.Number)%></td>
                                    <td align="center" class="certified"><%# bool.Parse(Eval("IsCertified").ToString()) ? "<div class=\"checkmark\"><span>Yes</span></div>" : string.Empty%></td>
                                    <td align="center"><%# Utility.FormatNumber(Eval("ListPrice").ToString(), NumberType.Currency)%></td>
                                    <td align="center" class="savings"><span class='<%# (int.Parse(Eval("ListPrice").ToString()) - Deal.OfferPrice).CompareTo(0) < 0 ? "red" : "" %>'>You save <strong><%# Utility.FormatNumber((int.Parse(Eval("ListPrice").ToString()) - Deal.OfferPrice).ToString(), NumberType.Currency)%></strong></span></td>
                                  
                                </tr>
                                <tr id='<%# ((IMarketListing)Container.DataItem).VehicleId %>' class="even vehicleDescription" style="display:none;">
                                    <td colspan="8"><p><%# string.IsNullOrEmpty(Eval("VehicleDescription").ToString().Trim()) ? "No ad preview found." : Eval("VehicleDescription").ToString() %></p></td>
                                </tr>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tr id='<%# ((IMarketListing)Container.DataItem).VehicleId %>' class='odd <%# Preference.DisplayListingsIDs.IndexOf(((IMarketListing)Container.DataItem).VehicleId) >= 0 ? "selected" : string.Empty %>'>

                                    <td align="center">
                                        <input type="checkbox" ID="DisplayListingCheckbox" runat="server" value='<%# ((IMarketListing)Container.DataItem).VehicleId %>' checked='<%# Preference.DisplayListingsIDs.IndexOf(((IMarketListing)Container.DataItem).VehicleId) >= 0%>' />
                                    </td>
                                    <td class="dealer"><%# Eval("DealerName")%></td>
                                    <td><%# Eval("VehicleName")%></td>
                                    <td align="center"><%# Eval("Color")%></td>
                                    <td align="center"><%# Utility.FormatNumber(Eval("Mileage").ToString(), NumberType.Number)%></td>
                                    <td align="center" class="certified"><%# bool.Parse(Eval("IsCertified").ToString()) ? "<div class=\"checkmark\"><span>Yes</span></div>" : string.Empty%></td>
                                    <td align="center"><%# Utility.FormatNumber(Eval("ListPrice").ToString(), NumberType.Currency)%></td>
                                    <td align="center" class="savings"><span class='<%# (int.Parse(Eval("ListPrice").ToString()) - Deal.OfferPrice).CompareTo(0) < 0 ? "red" : "" %>'>You save <strong><%# Utility.FormatNumber((int.Parse(Eval("ListPrice").ToString()) - Deal.OfferPrice).ToString(), NumberType.Currency)%></strong></span></td>
                                   
                                </tr>
                                <tr id='<%# ((IMarketListing)Container.DataItem).VehicleId %>' class="odd vehicleDescription" style="display:none;">
                                    <td colspan="8"><p><%# string.IsNullOrEmpty(Eval("VehicleDescription").ToString().Trim()) ? "No ad preview found." : Eval("VehicleDescription").ToString() %></p></td>
                                </tr>
                            </AlternatingItemTemplate>
                        </asp:Repeater>
                
                    </tbody>
                </table>
            </div>
        <asp:Label ID="EmptyDataSetLabel" runat="server" CssClass="emptyDataSetLabel">No Market Listings to Display (Please try adjusting your filters to allow more search results.)</asp:Label>
        
        <asp:Label ID="PagingLabel" runat="server" CssClass="pagingLabel"></asp:Label>
        <div id="MarketListingsPagingDiv">
            <asp:Repeater ID="PagingRepeater" OnItemDataBound="PagingRepeater_OnItemDataBound" runat="server">
                <ItemTemplate>
                    <asp:PlaceHolder ID="LinkPlaceHolder" runat="server">
                        <asp:LinkButton ID="PageLinkButton" pageNumber='<%# Eval("PageNumber")%>' runat="server" OnClick="PageClicked"><%# Eval("DisplayPage")%></asp:LinkButton>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="NonLinkPlaceHolder" runat="server">
                        <asp:Label Text='<%# Eval("DisplayPage")%>' runat="server"></asp:Label>
                    </asp:PlaceHolder>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        
    </div>
    
    <div id="MarketListingsSelectedPanel" class="clear marketListingsTabContent tableContainer" style="display:none">
        <cva:MarketListings ID="MarketListings1" runat="server" LayoutKey="VehiclePreference"/>
    </div>
        
    </div>

    <div class="prefPageActions clearfix">
    
        <asp:LinkButton ID="LinkButtonResetToDefaults" runat="server" OnClick="LinkButtonResetToDefaults_Click" OnClientClick="SalesAccelerator.ResetModuleEditGlobals();" CssClass="resetBtn">Reset to Top 10 Listings</asp:LinkButton>
        
        <asp:Button ID="FeaturesPreferenceCancelBtn" runat="server" class="cancelBtn" Text="Cancel" />
        <asp:Button ID="SaveAndCloseButton" runat="server" OnClick="ButtonSave_Click" CssClass="saveBtn marketSaveBtn closeBtn" Text="Save & Close" />
        <asp:Button ID="ButtonSave" runat="server" OnClick="ButtonSave_Click" CssClass="saveBtn marketSaveBtn" Text="Save" />
        
        <div class="reloadAfterPopUpFields" style="display:none;">
            <asp:LinkButton ID="RefreshMarketListingsButton" runat="server" CssClass="saveBtn">Refresh Market Listings</asp:LinkButton>
            <input type="hidden" id="MarketListingsRefreshButtonClientIdHidden" value='<%= RefreshMarketListingsButton.ClientID %>' />
            <input type="hidden" id="MarketListingsRefreshButtonUniqueIdHidden" value='<%= RefreshMarketListingsButton.UniqueID %>' />
        </div>
        
    </div>

</div>
