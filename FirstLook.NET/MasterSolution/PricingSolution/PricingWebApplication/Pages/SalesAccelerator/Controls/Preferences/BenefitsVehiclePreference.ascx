﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="BenefitsVehiclePreference.ascx.cs" Inherits="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls.Preferences.BenefitsVehiclePreference" %>

<div id="CertifiedVehiclePreferences">
    <asp:CheckBox ID="ToggleDisplayOnSellingSheetCheckBox" runat="server" AutoPostBack="false" CssClass="displayOnSheetCkb" Text="Display on Consumer Packet?" OnCheckedChanged="ToggleDisplayOnSellingSheetCheckBox_CheckChanged" OnPreRender="ToggleDisplayOnSellingSheetCheckBox_PreRender" />
    <input type="hidden" runat="server" id="VehicleIsCertified" class="CPOV" />
    <input type="hidden" runat="server" id="CPOAssociationExists" class="CPOASSOC" />
    <asp:Label ID="MessagingLabel" runat="server" CssClass="messagingLabel" Visible="false"></asp:Label>
</div>
<div class="prefPageActions clearfix">
    <asp:LinkButton ID="ResetToDefaultsButton" runat="server" Text="Reset to defaults" OnClick="ResetToDefaultsButton_OnClick" OnClientClick="SalesAccelerator.ResetModuleEditGlobals();" CssClass="resetBtn"></asp:LinkButton>
</div>