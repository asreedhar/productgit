﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdPreviewVehiclePreference.ascx.cs" Inherits="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls.Preferences.AdPreviewVehiclePreference" %>

<input type="hidden" runat="server" id="CharCountHiddenField" class="length" />

<div id="AdPreviewVehiclePreferences">
    <asp:Label ID="MessagingLabel" runat="server" CssClass="messagingLabel" Visible="false"></asp:Label>
    
    <asp:Panel ID="AdPreviewVehiclePreferencesPanel" runat="server" Visible="true">
    <div id="AdjustPreviewLengthButtons">
        <label>Set Ad Preview character length:</label>
        <button id="SetAdPreview150Button">150</button>
        <button id="SetAdPreview250Button">250</button>
        &nbsp;&nbsp;
        <label>Adjust:</label>
        <button id="SubtractWordButton">- word</button>
        <button id="AddWordButton">+ word</button>
    </div>
    </asp:Panel>
    
    <div id="EditAdPreviewCopyDiv">
        <asp:Literal ID="AdvertisementLabel" runat="server" />
    </div>
</div>
<div class="prefPageActions clearfix">
    <%--<asp:LinkButton ID="ResetToDefaultsButton" runat="server" Text="Reset to defaults" OnClick="ResetToDefaultsButton_OnClick" OnClientClick="SalesAccelerator.ResetModuleEditGlobals();" CssClass="resetBtn"></asp:LinkButton>--%>
    <asp:Button ID="AdPreviewCancelBtn" runat="server" CssClass="cancelBtn" Text="Cancel" />
    <asp:Button ID="SaveAndCloseButton" runat="server" OnClick="SaveButton_OnClick" Text="Save & Close" CssClass="saveBtn adPreviewSaveBtn closeBtn" />
    <asp:Button ID="SaveButton" runat="server" OnClick="SaveButton_OnClick" Text="Save" CssClass="saveBtn adPreviewSaveBtn" />
</div>