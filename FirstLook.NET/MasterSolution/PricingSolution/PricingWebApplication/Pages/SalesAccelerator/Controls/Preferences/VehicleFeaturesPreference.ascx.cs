﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.DTO;
using FirstLook.Merchandising.DomainModel.Marketing.Equipment;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls.Preferences
{
    public partial class VehicleFeaturesPreference : BaseUserControl
    {
        // We will interact with the equipment domain via a facde.
        private EquipmentFacade _facade;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            _facade = new EquipmentFacade(Handles.OwnerHandle, Handles.VehicleHandle);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // If we won't re-render our output, we can avoid a bunch of work.
            if (!WillReRender())
            {
                return;
            }

            // Load the provider pulldown
            LoadProviderDropdownList(_facade);


            BindFeatures();
  
        }

        private void BindFeatures()
        {
            // Bind the selected repeater
            var displayed = Equipment(_facade).Where(eq => eq.Selected).OrderBy(eq => eq.Position);


            DisplayedFeaturesRepeater.DataSource = displayed;
            DisplayedFeaturesRepeater.DataBind();

            FeaturesDisplayedItemsHidden.Value = GetKeyList(displayed.Cast<ISurrogateKey>());

            // Bind the available features repeater
            var available = Equipment(_facade).Where(eq => !eq.Selected).OrderBy(eq => eq.Name);

            AvailableFeaturesRepeater.DataSource = available;
            AvailableFeaturesRepeater.DataBind();

            FeaturesAvailableItemsHidden.Value = GetKeyList(available.Cast<ISurrogateKey>());
        }

        protected void SaveButton_OnClick(object sender, EventArgs e)
        {
            // The "displayed" line items will all be set to display.  The keys are already sorted by rank.
            IEnumerable<int> displayedItems = GetDisplayedLineItems();

            // The "available" benefit line items will all be set to not display and will all have a rank of 0.
            IEnumerable<int> availableItems = GetAvailableLineItems();

            // Save the preference
            int? providerId = SelectedProviderId();
            if (providerId.HasValue)
            {
                _facade.SaveSelected(providerId.Value, displayedItems, availableItems);
            }
        }

        protected void ToggleDisplayOnSellingSheetCheckBox_CheckChanged(object sender, EventArgs e)
        {
            _facade.IsDisplayed = ToggleDisplayOnSellingSheetCheckBox.Checked;
        }

        protected void ResetToDefaultsButton_OnClick(object sender, EventArgs e)
        {
            ProviderList.Items.Clear();
            _facade.ResetToDefaults();
        }

        protected void ToggleDisplayOnSellingSheetCheckBox_PreRender(object sender, EventArgs e)
        {
            ToggleDisplayOnSellingSheetCheckBox.Checked = _facade.IsDisplayed;
        }

        #region Private

        private IEnumerable<EquipmentDTO> Equipment(EquipmentFacade facade)
        {
            // We can only retrieve equipment if provider pulldown has a valid value.
            int? providerId = SelectedProviderId();
            if( !providerId.HasValue )
            {
                // we cannot proceed - return an empty list.
                return new List<EquipmentDTO>();
            }

            var equipment = facade.GetEquipment( providerId.Value );
            if (equipment == null)
            {
                return new List<EquipmentDTO>();
            }

            // Get equipment for the selected provider.
            return equipment;
        }

        private int? SelectedProviderId()
        {           
            if( ProviderList.SelectedValue.Length > 0 )
            {
                return Int32.Parse(ProviderList.SelectedValue);                
            }

            return null;
        }

        private void LoadProviderDropdownList(EquipmentFacade facade)
        {
            // Get the providers
            var providers = facade.GetProviders();

            // Which provider is currently selected?
            int? selectedId = SelectedProviderId();

            // Which provider is recorded in the db as the default?
            int? providerValue = facade.GetSelectedProviderId();

            // Load the provider drop down
            ProviderList.DataSource = providers;
            ProviderList.DataTextField = "Name";
            ProviderList.DataValueField = "EquipmentProviderID";

            var selectedIsAvailable = (from p in providers where p.EquipmentProviderID == selectedId select p).Any();
            var defaultIsAvailable = (from p in providers where p.EquipmentProviderID == providerValue select p).Any();
            
            // Select the appropriate value - use the selected value if it is set and still available.
            // If not, try the default (if it has a value and is still available)
            // If not, just pick one...the first one.
            if (selectedId.HasValue && selectedIsAvailable)
            {
                // Use the current selection for this vehicle
                ProviderList.SelectedValue = selectedId.Value.ToString();
            }
            else if (providerValue.HasValue && defaultIsAvailable)
            {
                // use the default for the dealer
                ProviderList.SelectedValue = providerValue.Value.ToString();
            }
            else
            {
                // just pick the first one
                ProviderList.SelectedIndex = 0;
            }

            ProviderList.DataBind();

            SetProviderLink();
        }

        /// <summary>
        /// Set the provider link based on the selected equipment provider
        /// </summary>
        private void SetProviderLink()
        {
            var selectedProviderId = SelectedProviderId();

            if (selectedProviderId.HasValue)
            {
                var provider = (EquipmentProvider.Code)Enum.ToObject(typeof(EquipmentProvider.Code), selectedProviderId.Value);

                ProviderLink.NavigateUrl = _facade.GetProviderUrl(provider);

                if (!string.IsNullOrEmpty(ProviderLink.NavigateUrl))
                {
                    string linkText = string.Empty;

                    ProviderLink.Text = "Select Equipment";
                    ProviderLink.Target = "_blank";
                    ProviderLink.CssClass = "bookout";

                    ProviderLink.Visible = true;

                    return;
                }
            }

            ProviderLink.Visible = false;
        }

        /// <summary>
        /// Returns the keys of the displayed benefit line items.  They are sorted by rank.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<int> GetDisplayedLineItems()
        {
            return CsvStringsToInts( FeaturesDisplayedItemsHidden.Value );
        }

        /// <summary>
        /// Returns the keys of the available benefit line items.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<int> GetAvailableLineItems()
        {
            return CsvStringsToInts( FeaturesAvailableItemsHidden.Value );
        }

        private static IEnumerable<int> CsvStringsToInts( string csvStrings )
        {
            if (csvStrings.Length == 0) 
                return new List<int>();

            var strList = csvStrings
                            .Split(new[] { ',' })
                            .ToList();

            return strList.ConvertAll<int>(Int32.Parse);
        }

        #endregion

        protected void RefreshEquipmentButton_Click(object sender, EventArgs e)
        {
            Deal.MarketAnalysis.Pricing = null;
        }
    }
}