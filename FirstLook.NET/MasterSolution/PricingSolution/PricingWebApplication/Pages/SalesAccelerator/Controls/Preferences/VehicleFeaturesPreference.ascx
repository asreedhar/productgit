﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="VehicleFeaturesPreference.ascx.cs" Inherits="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls.Preferences.VehicleFeaturesPreference" %>

<div id="FeaturesVehiclePreferences">
    <asp:CheckBox ID="ToggleDisplayOnSellingSheetCheckBox" runat="server" AutoPostBack="false" CssClass="displayOnSheetCkb" Text="Display on Consumer Packet?" 
        OnCheckedChanged="ToggleDisplayOnSellingSheetCheckBox_CheckChanged" OnPreRender="ToggleDisplayOnSellingSheetCheckBox_PreRender" /> 
    <asp:Label ID="MessagingLabel" runat="server" CssClass="messagingLabel" Visible="false"></asp:Label>

    <div id="Providers">

        <asp:Label AssociatedControlID="ProviderList" runat="server" ID="ProviderListLabel0" Text="Use" />
        <asp:DropDownList runat="server" ID="ProviderList"
                AppendDataBoundItems="false"
                AutoPostBack="true"  >
        </asp:DropDownList>
        <asp:Label AssociatedControlID="ProviderList" runat="server" ID="ProviderListLabel1" Text="for your equipment." />
        <asp:HyperLink ID="ProviderLink" runat="server"></asp:HyperLink>
    
    </div>

    <div id="FeaturesItems">
        <h4>Displayed <span class="small">(Drag and drop to reorder items or move them to/from the available bin.)</span></h4>
        <input type="hidden" id="FeaturesDisplayedItemsHidden" runat="server" class="displayed" />
        <input type="hidden" id="FeaturesAvailableItemsHidden" runat="server" class="available" />
        <asp:Repeater ID="DisplayedFeaturesRepeater" runat="server">
            <HeaderTemplate><ul class="displayed clearfix sortable"></HeaderTemplate>
            <FooterTemplate></ul></FooterTemplate>
            <ItemTemplate>
                <li id="<%# Eval("Key") %>"><%# Eval("Name") %></li>
            </ItemTemplate>
        </asp:Repeater>
        <div class="availableDiv">
            <h4>Available</h4>
            <asp:Repeater ID="AvailableFeaturesRepeater" runat="server">
                <HeaderTemplate><ul class="available clearfix sortable"></HeaderTemplate>
                <FooterTemplate></ul></FooterTemplate>
                <ItemTemplate>
                    <li id="<%# Eval("Key") %>"><%# Eval("Name")%></li>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</div>
<div class="prefPageActions clearfix">
    <asp:LinkButton ID="ResetToDefaultsButton" runat="server" Text="Reset to defaults" OnClick="ResetToDefaultsButton_OnClick" OnClientClick="SalesAccelerator.ResetModuleEditGlobals();" CssClass="resetBtn"></asp:LinkButton>
    <asp:Button ID="FeaturesCancelBtn" runat="server" CssClass="cancelBtn" Text="Cancel" />
    <asp:Button ID="SaveAndCloseButton" runat="server" OnClick="SaveButton_OnClick" Text="Save & Close" CssClass="saveBtn featuresSaveBtn closeBtn" />
    <asp:Button ID="SaveButton" runat="server" OnClick="SaveButton_OnClick" Text="Save" CssClass="saveBtn featuresSaveBtn" />
    
    <div class="reloadAfterPopUpFields" style="display:none;">
        <asp:LinkButton ID="RefreshEquipmentButton" runat="server" OnClick="RefreshEquipmentButton_Click" CssClass="saveBtn">Refresh Equipment</asp:LinkButton>
        <input type="hidden" id="EquipmentRefreshButtonClientIdHidden" value='<%= RefreshEquipmentButton.ClientID %>' />
        <input type="hidden" id="EquipmentRefreshButtonUniqueIdHidden" value='<%= RefreshEquipmentButton.UniqueID %>' />
    </div>

</div>
