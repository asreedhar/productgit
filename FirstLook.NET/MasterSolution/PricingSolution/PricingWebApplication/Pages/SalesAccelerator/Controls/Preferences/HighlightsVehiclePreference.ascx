﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="HighlightsVehiclePreference.ascx.cs" Inherits="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls.Preferences.HighlightsVehiclePreference" %>

<div id="HighlightsVehiclePreferences">
    <asp:CheckBox ID="ToggleDisplayOnSellingSheetCheckBox" runat="server" AutoPostBack="false" CssClass="displayOnSheetCkb" Text="Display on Consumer Packet?" OnCheckedChanged="ToggleDisplayOnSellingSheetCheckBox_CheckChanged"  />
        
    <asp:Label ID="MessagingLabel" runat="server" CssClass="messagingLabel" Visible="false"></asp:Label>
    
    <div id="HighlightTabSet">
        <ul id="HighlightTabs" class="clearfix">
            <li id="JDPowerTab"><a class="leftTab">J.D. Power</a></li>
            <li id="ReviewsAwardsTab"><a>Expert Reviews</a></li>
            <li id="VHRTab"><a>CARFAX &amp; AutoCheck</a></li>
            <li id="HighlightsTab"><a class="rightTab">Additional Highlights</a></li>
        </ul>
        <div id="ConsumerHighlightsTabContent" class="clearfix">            
            
            <div id="HighlightsTabContent" class="highlightTabContent moduleContainer">
                <asp:Panel ID="FreeTextFormPanel" runat="server" DefaultButton="DeadDefaultButton">
                    <h4>Displayed <span class="small">(Double-click item to edit. Drag and drop item to reorder, delete or move.)</span> <a href="#" class="addHighlight">+ <span>Add New</span></a></h4>
                    <input type="hidden" id="FreeTextHighlightsItemsHidden" runat="server" class="items" />
                    <!-- TODO: Populate ConsumerHighlightProviderIdHidden.value from server side -->
                    <input type="hidden" id="FreeTextHighlightProviderIdHidden" runat="server" value="1" class="providerId" />
                    <asp:Repeater ID="DisplayedFreeTextHighlightsRepeater" runat="server">
                        <HeaderTemplate><ul class="displayed clearfix sortable"></HeaderTemplate>
                        <FooterTemplate></ul></FooterTemplate>
                        <ItemTemplate>
                            <li id="<%# Eval("Key") %>"><%# Eval("Text") %></li>
                        </ItemTemplate>
                    </asp:Repeater>
                    
                    <%-- This button is used as the default button for this panel to 
                         achieve desired behavior when adding/editing free text highlights --%>
                    <asp:Button ID="DeadDefaultButton" runat="server" Text="" OnClientClick="$('.addHighlight')[0].focus();return false;" style="display:none" />
                    
                    <div class="clearfix">
                    <div class="availableDiv">
                        <h4>Available</h4>
                        <asp:Repeater ID="AvailableFreeTextHighlightsRepeater" runat="server">
                            <HeaderTemplate><ul class="available clearfix sortable"></HeaderTemplate>
                            <FooterTemplate></ul></FooterTemplate>
                            <ItemTemplate>
                                <li id="<%# Eval("Key") %>"><%# Eval("Text")%></li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="deletedDiv">
                        <h4>Deleted</h4>
                        <ul class="deleted clearfix sortable"></ul>
                    </div>
                </div>
                </asp:Panel>
                
            </div>
            
            <div id="JDPowerTabContent" class="highlightTabContent moduleContainer">
                <h4>Displayed <span class="small">(Drag and drop to reorder or move to/from the available bin.)</span></h4>
                <input type="hidden" id="JDPowerHighlightsItemsHidden" runat="server" class="items" />
                <!-- TODO: Populate ConsumerHighlightProviderIdHidden.value from server side -->
                <input type="hidden" id="JDPowerHighlightProviderIdHidden" runat="server" value="1" class="providerId" />
                <asp:Repeater ID="DisplayedJDPowerHighlightsRepeater" runat="server">
                    <HeaderTemplate><ul class="displayed clearfix sortable"></HeaderTemplate>
                    <FooterTemplate></ul></FooterTemplate>
                    <ItemTemplate>
                        <li id="<%# Eval("Key") %>"><%# Eval("RatingDescription") %>: <span class="stars<%# Eval("RatingValue").ToString().Trim().Replace(".", "") %>"><%# Eval("RatingValue") %></span></li>
                    </ItemTemplate>
                </asp:Repeater>
                
                <div class="clearfix">
                    <div class="availableDiv">
                        <h4>Available</h4>
                        <asp:Repeater ID="AvailableJDPowerHighlightsRepeater" runat="server">
                            <HeaderTemplate><ul class="available clearfix sortable"></HeaderTemplate>
                            <FooterTemplate></ul></FooterTemplate>
                            <ItemTemplate>
                                <li id="<%# Eval("Key") %>"><%# Eval("RatingDescription")%>: <span class="stars<%# Eval("RatingValue").ToString().Trim().Replace(".", "") %>"><%# Eval("RatingValue") %></span></li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            
            </div>
            
            <div id="ReviewsAwardsTabContent" class="highlightTabContent moduleContainer">
                <h4>Displayed <span class="small">(Drag and drop to reorder or move to/from the available bin.)</span></h4>
                <input type="hidden" id="SnippetHighlightsItemsHidden" runat="server" class="items" />
                <!-- TODO: Populate ConsumerHighlightProviderIdHidden.value from server side -->
                <input type="hidden" id="SnippetHighlightsProviderIdHidden" runat="server" value="1" class="providerId" />
                <asp:Repeater ID="DisplayedSnippetHighlightsRepeater" runat="server">
                    <HeaderTemplate><ul class="displayed clearfix sortable"></HeaderTemplate>
                    <FooterTemplate></ul></FooterTemplate>
                    <ItemTemplate>
                        <li id="<%# Eval("Key") %>"><span class="source"><%# Eval("SnippetSourceName")%>:</span> <%# Eval("Snippet") %></li>
                    </ItemTemplate>
                </asp:Repeater>
                
                <div class="clearfix">
                    <div class="availableDiv">
                        <h4>Available </h4>
                        <asp:Repeater ID="AvailableSnippetHighlightsRepeater" runat="server">
                            <HeaderTemplate><ul class="available clearfix sortable"></HeaderTemplate>
                            <FooterTemplate></ul></FooterTemplate>
                            <ItemTemplate>
                                <li id="<%# Eval("Key") %>"><span class="source"><%# Eval("SnippetSourceName")%>:</span> <%# Eval("Snippet")%></li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
            
            <div id="VHRTabContent" class="highlightTabContent moduleContainer">
                <div id="CarFaxCheckBoxDiv">
                    <asp:CheckBox ID="IncludeCarFaxOneOwnerIconCheckBox" runat="server" Text="Include CARFAX 1-Owner icon?" />
                </div> 
                <h4 class="displayed">Displayed <span class="small">(Drag and drop to reorder or move to/from the available bin.)</span></h4>
                <input type="hidden" id="VehicleHistoryHighlightsItemsHidden" runat="server" class="items" />
                <!-- TODO: Populate ConsumerHighlightProviderIdHidden.value from server side -->
                <input type="hidden" id="VehicleHistoryHighlightsProviderIdHidden" runat="server" value="1" class="providerId" />
                <asp:Repeater ID="DisplayedVehicleHistoryHighlightsRepeater" runat="server">
                    <HeaderTemplate><ul class="displayed clearfix sortable"></HeaderTemplate>
                    <FooterTemplate></ul></FooterTemplate>
                    <ItemTemplate>
                        <li id="<%# Eval("Key") %>"><%# Eval("Text") %></li>
                    </ItemTemplate>
                </asp:Repeater>
                
                <div class="clearfix">
                    <div class="availableDiv">
                        <h4>Available</h4>
                        <asp:Repeater ID="AvailableVehicleHistoryHighlightsRepeater" runat="server">
                            <HeaderTemplate><ul class="available clearfix sortable"></HeaderTemplate>
                            <FooterTemplate></ul></FooterTemplate>
                            <ItemTemplate>
                                <li id="<%# Eval("Key") %>"><%# Eval("Text")%></li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    
    
</div>
<div class="prefPageActions clearfix">
    <asp:LinkButton ID="ResetToDefaultsButton" runat="server" Text="Reset to defaults" OnClick="ResetToDefaultsButton_OnClick" OnClientClick="SalesAccelerator.ResetModuleEditGlobals();" CssClass="resetBtn"></asp:LinkButton>
    <asp:Button ID="HighlightsPreferenceCancelBtn" runat="server" class="cancelBtn" Text="Cancel" />
    <asp:Button ID="SaveAndCloseButton" runat="server" OnClick="SaveButton_OnClick" Text="Save & Close" CssClass="saveBtn closeBtn" />
    <asp:Button ID="SaveButton" runat="server" OnClick="SaveButton_OnClick" Text="Save" CssClass="saveBtn" />
</div>
