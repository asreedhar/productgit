<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VehicleSummary.ascx.cs" 
    Inherits="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls.VehicleSummary" EnableViewState="false" %>

<div id="VehicleSummaryDetails">
    <div class="columnA">
        <ul>
            <li><label>Exterior Color:</label><span><%= Utility.WriteNode(XmlDoc.SelectSingleNode("/vehiclesummary/color"))%></span></li>
            <li><label>Mileage:</label><%= string.Format("{0:#,##0}", int.Parse(Utility.WriteNode(XmlDoc.SelectSingleNode("/vehiclesummary/mileage"))))%></li>
            <li id="EngineInfo"><label>Engine:</label><span><%= Utility.WriteNode(XmlDoc.SelectSingleNode("/vehiclesummary/engine"))%></span></li>
            <li><label>Transmission:</label><span><%= Utility.WriteNode(XmlDoc.SelectSingleNode("/vehiclesummary/transmission"))%></span></li>
            <li><label>Drive Train:</label><span><%= Utility.WriteNode(XmlDoc.SelectSingleNode("/vehiclesummary/drivetrain"))%></span></li>
        </ul>
    </div>
    <div class="columnB">
        <ul>
            <li>Stock #: <%= Utility.WriteNode(XmlDoc.SelectSingleNode("/vehiclesummary/stocknumber"))%></li>
            <li>VIN: <%= Utility.WriteNode(XmlDoc.SelectSingleNode("/vehiclesummary/vin"))%></li>
        </ul>
    </div>
</div>