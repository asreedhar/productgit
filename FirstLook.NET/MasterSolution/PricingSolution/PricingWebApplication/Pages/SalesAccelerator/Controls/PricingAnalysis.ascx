<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="PricingAnalysis.ascx.cs" 
    Inherits="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls.PricingAnalysis" %>
<%@ Import Namespace="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator"%>

<% if (ShowPriceList) { %>
<ul id="PricingList" class="clearfix">
    <li class="first-child"><label><%= Utility.WriteNode(XmlDoc.SelectSingleNode("/pricing/gaugeprices/price[@isOfferPrice='true']/text"))%>:</label> <%= Utility.FormatNumber(Utility.WriteNode(XmlDoc.SelectSingleNode("/pricing/gaugeprices/price[@isOfferPrice='true']/amount")), NumberType.Currency)%> <span ID="PaymentsSpan" runat="server" class="payments"></span></li>    
    <asp:Repeater ID="OtherPricesHeadList" runat="server">
        <ItemTemplate>
            <li><label><%# XPath("text") %>:</label> <%# Utility.FormatNumber(XPath("amount").ToString(), NumberType.Currency)%></li>
        </ItemTemplate>
    </asp:Repeater>
</ul>
<% } %>

<asp:Panel ID="PricingPanel" runat="server" CssClass="gaugeTableContainer clearfix">
    <a class="moduleEditLink" href="#">Edit 360&deg; Pricing Analysis</a>
    <% if (! EditMode) { %>
    <h3>360&deg; Pricing Analysis</h3>
    <asp:Panel id="GaugePanel" runat="server" Visible="false">
        <div id="Gauge">
            <div id="GaugeItems">
                <asp:Repeater ID="GaugePricesRepeater" runat="server">
                    <HeaderTemplate><ul></HeaderTemplate>
                    <FooterTemplate></ul></FooterTemplate>
                    <ItemTemplate>
                        <li class='<%# bool.Parse(XPath("@isOfferPrice").ToString()) ? "offerPrice" : string.Empty %>' style='width:<%# XPath("gauge/@width") %>px;left:<%# XPath("gauge/@position") %>px'><span><%# XPath("text") %><strong><%# Utility.FormatNumber(XPath("amount").ToString(), NumberType.Currency)%></strong></span></li>
                    </ItemTemplate>
                </asp:Repeater>
                <div id="GaugeBreakDiv" runat="server" visible="false" class="gaugeBreakDiv">&nbsp;</div>
                <div class="clearBoth">&nbsp;</div>
            </div>
        </div>
    </asp:Panel>
    <% } %>
    
    <asp:CheckBox ID="OverrideAutomaticPriceProviderSelection" runat="server" Text="Manually control comparison prices displayed" />
    <asp:CheckBox ID="SavePriceProviderOverrides" runat="server" Text="Save the selections for this vehicle" CssClass="overridePriceSelection" />
    
    <asp:Repeater ID="PriceComparisonRepeater" runat="server" OnItemDataBound="PriceComparisonRepeater_ItemDataBound">
        <HeaderTemplate>
            <div id="PricingAnalysisTableContainer">
            <table>
                <% if (EditMode) { %>
                <tr>
                    <th style="width:5%"></th>
                    <th style="width:50%" align="left">Source</th>
                    <th style="width:10%">Price</th>
                    <th style="width:35%">Savings</th>
                </tr>
                <% } else { %>                
                <tr>
                    <th style="width:20%"><label class="date">(AS OF <%= DateTime.Now.ToShortDateString() %>)</label></th>
                    <th style="width:40%"></th>
                    <th style="width:10%"></th>
                    <th style="width:30%"><label class="price">YOUR PRICE IS:</label></th>
                </tr>
                <% } %>                
            </HeaderTemplate>
        <FooterTemplate></table></div></FooterTemplate>
        <ItemTemplate>
            <tr class="comparisonRow odd">
                <% if (EditMode) { %>
                    <td><asp:CheckBox ID="SelectProviderCheckBox" runat="server" ProviderId='<%# XPath("providerId") %>' /></td>
                <% } else { %>         
                    <td class="logo"><img src="<%# XPath("logo") %>" alt="<%# XPath("text") %>" height="<%# XPath("logo/@height") %>" width="<%# XPath("logo/@width") %>" /></td>                       
                <% } %>                
                <td class="label">
                    <label><%# XPath("text") %>:</label>
                    <asp:HyperLink ID="ProviderLink" runat="server" Text="Book Out Now" CssClass="bookout" Visible="false"></asp:HyperLink>
                </td>
                <td class="price" align="center">
                    <asp:Label ID="PriceLabel" runat="server" Visible="true" Text='<%# XPath("amount").ToString()%>'></asp:Label>
                </td>
                <td class="delta <%# int.Parse(XPath("delta").ToString()).CompareTo(0) < 0 ? "red" : "" %>" align="center">
                <strong><%# XPath("savingsValue").ToString()%></strong><br />
                <span><%# XPath("savingsText").ToString() %></span></td>
            </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
            <tr class="comparisonRow even">
                    <% if (EditMode) { %>
                        <td><asp:CheckBox ID="SelectProviderCheckBox" runat="server" ProviderId='<%# XPath("providerId") %>' /></td>
                    <% } else { %>         
                        <td class="logo"><img src="<%# XPath("logo") %>" alt="<%# XPath("text") %>" height="<%# XPath("logo/@height") %>" width="<%# XPath("logo/@width") %>" /></td>                       
                    <% } %>                
                <td class="label">
                    <label><%# XPath("text") %>:</label>
                    <asp:HyperLink ID="ProviderLink" runat="server" Text="Book Out Now" CssClass="bookout" Visible="false"></asp:HyperLink>
                </td>
                <td class="price" align="center">
                    <asp:Label ID="ListPriceLabel" runat="server" Visible="true" Text='<%# XPath("amount").ToString()%>'></asp:Label>
                </td>
                <td class="delta <%# int.Parse(XPath("delta").ToString()).CompareTo(0) < 0 ? "red" : "" %>" align="center">
                <strong><%# XPath("savingsValue").ToString()%></strong><br />
                <span><%# XPath("savingsText").ToString() %></span></td>
            </tr>
        </AlternatingItemTemplate>
    </asp:Repeater>
    <div id="PricingAnalysisBtm">&nbsp;</div>
</asp:Panel>