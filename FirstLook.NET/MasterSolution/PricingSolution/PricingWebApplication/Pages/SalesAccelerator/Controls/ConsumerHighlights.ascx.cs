using System;
using System.Xml;
using FirstLook.Common.Core.Xml;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights;
using FirstLook.Merchandising.DomainModel.Marketing.Deal;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls
{
    /// <summary>
    /// Consumer highlights control
    /// </summary>
    public partial class ConsumerHighlights : BaseUserControl, IXmlDoc
    {
        // Whether the xml has any highlights of a given type.
        public bool HasSnippets;
        public bool HasCircleRatings;
        public bool HasFreeText;
        public bool HasVehicleHistory;
        public bool HasCarFaxVehicleHistory;
        public bool HasAutoCheckVehicleHistory;

        public bool ShowCarFaxOneOwnerLogo
        {
            get
            {
                bool result = false;
                bool.TryParse(ShowCarFaxOneOwnerHidden.Value, out result);
                return result;
            }
        }

        /// <summary>
        /// Prior to rendering this control, bind the highlight repeaters.
        /// </summary>
        /// <param name="e">Event arguments.</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // If we won't re-render our output, we can avoid a bunch of work.
            if (!WillReRender())
            {
                return;
            }

            var consumerHighlights = Deal.GetConsumerHighlights();

            // Setup rank of each highlights div.
            CircleRatingsDiv.Attributes.Add("Rank", consumerHighlights.Preference.SectionOrdering.IndexOf(HighlightSectionType.CircleRatings).ToString());
            ConsumerHighlightsDiv.Attributes.Add("Rank", consumerHighlights.Preference.SectionOrdering.IndexOf(HighlightSectionType.FreeText).ToString());
            ReviewsAwardsDiv.Attributes.Add("Rank", consumerHighlights.Preference.SectionOrdering.IndexOf(HighlightSectionType.Snippets).ToString());
            VHRHighlightsDiv.Attributes.Add("Rank", consumerHighlights.Preference.SectionOrdering.IndexOf(HighlightSectionType.VehicleHistory).ToString());

            Deal.SaveXml(consumerHighlights);
            var consumerHighlightsXmlDoc = Deal.GetXml(NodeType.consumerhighlights);
            
            // Bind the repeaters for each highlight type.
            HandleConsumerHighlights(consumerHighlights.Preference, consumerHighlightsXmlDoc);

            VHRHighlightsDiv.Visible = HasVehicleHistory;
            CircleRatingsDiv.Visible = HasCircleRatings;
            ReviewsAwardsDiv.Visible = HasSnippets;
            ConsumerHighlightsDiv.Visible = HasFreeText;

            // Show VHR logos
            CARFAXLogo.Visible = HasCarFaxVehicleHistory;
            AutoCheckLogo.Visible = HasAutoCheckVehicleHistory;

            ConsumerHighlightsPreviewPanel.Attributes["class"] = ConsumerHighlightsPreviewPanel.Attributes["class"].Replace(" emptyModule", string.Empty);
            // If there are no highlights, change the module's presentation.
            if (!HasVehicleHistory && !HasCircleRatings && !HasSnippets && !HasFreeText)
            {
                ConsumerHighlightsPreviewPanel.Attributes["class"] += " emptyModule";
                ConsumerHighlightsPreviewPanel.Attributes.Add("title", "This module will NOT be printed.");
            }
            
            ConsumerHighlightsPreviewPanel.Visible = true;
        }

        #region Highlight Data Binding

        /// <summary>
        /// Generate xml for all consumer highlights. Doesn't do anything else (like bind this xml to a repeater).
        /// </summary>
        /// <param name="vehiclePreference">Vehicle preferences.</param>
        protected void HandleConsumerHighlights(ConsumerHighlightsVehiclePreference vehiclePreference, SerializableXmlDocument xmlDoc)
        {
           
            // Set the circle ratings highlights.
            XmlNode circleRatingsNode = xmlDoc.SelectSingleNode("consumerhighlights/circleratinghighlights");
            if (circleRatingsNode != null && circleRatingsNode.HasChildNodes)
            {
                CircleRatingHighlightRepeater.DataSource = xmlDoc.SelectNodes("consumerhighlights/circleratinghighlights/highlight");
                CircleRatingHighlightRepeater.DataBind();
                HasCircleRatings = true;
            }
            else
            {
                HasCircleRatings = false;
            }

            // Set the free text highlights.
            XmlNode freeTextNode = xmlDoc.SelectSingleNode("consumerhighlights/freetexthighlights");

            if (freeTextNode != null && freeTextNode.HasChildNodes)
            {
                FreeTextHighlightRepeater.DataSource = xmlDoc.SelectNodes("consumerhighlights/freetexthighlights/highlight");
                FreeTextHighlightRepeater.DataBind();
                HasFreeText = true;
            }
            else
            {
                HasFreeText = false;
            }

            // Set the snippet highlights.
            XmlNode snippetsNode = xmlDoc.SelectSingleNode("consumerhighlights/snippethighlights");

            if (snippetsNode != null && snippetsNode.HasChildNodes)
            {
                SnippetHighlightRepeater.DataSource = xmlDoc.SelectNodes("consumerhighlights/snippethighlights/highlight");
                SnippetHighlightRepeater.DataBind();
                HasSnippets = true;
            }
            else
            {
                HasSnippets = false;
            }

            // Set the vehicle history highlights.
            XmlNode vehicleHistoryNode = xmlDoc.SelectSingleNode("consumerhighlights/vehiclehistoryhighlights");

            // Set ShowCarFaxLogo property.
            if (vehicleHistoryNode != null && vehicleHistoryNode.Attributes.Count > 0)
            {
                string showCarFaxLogoAttr = Utility.WriteNode(xmlDoc.SelectSingleNode("consumerhighlights/vehiclehistoryhighlights/@showCarFaxLogo"));
                ShowCarFaxOneOwnerHidden.Value = string.IsNullOrEmpty(showCarFaxLogoAttr) ? "false" : showCarFaxLogoAttr.ToLower();
            }

            if (vehicleHistoryNode != null && vehicleHistoryNode.HasChildNodes)
            {
                VehicleHistoryHighlightRepeater.DataSource = xmlDoc.SelectNodes("consumerhighlights/vehiclehistoryhighlights/highlight");
                VehicleHistoryHighlightRepeater.DataBind();
                HasVehicleHistory = true;
                HasCarFaxVehicleHistory = vehicleHistoryNode.SelectNodes("//text[contains(., 'CARFAX')]").Count > 0;
                HasAutoCheckVehicleHistory = vehicleHistoryNode.SelectNodes("//text[contains(., 'AutoCheck')]").Count > 0;
            }
        }       

        #endregion

        SerializableXmlDocument IXmlDoc.XmlDoc
        {
            get { return Deal.GetXml(NodeType.consumerhighlights); }
        }

    }
}