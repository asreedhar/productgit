using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using FirstLook.Common.Core.Xml;
using FirstLook.Merchandising.DomainModel.Marketing.Deal;
using FirstLook.Merchandising.DomainModel.Marketing.Equipment;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls
{
    public partial class PricingAnalysis : BaseUserControl, IXmlDoc
    {
        private EquipmentFacade _equipmentFacade;

        /// <summary>
        /// This is a temporary field used to turn off the bookout links via config.
        /// It will be removed no later than the next release (5.20). 
        /// ~ DH, release 5.19, 2010-10-19
        /// </summary>
        private readonly bool _showComparisonBookoutLinks =
            bool.Parse(ConfigurationManager.AppSettings["PricingAnalysis.ShowComparisonBookoutLinks"]);

        public bool EditMode { get; set; }

        internal bool SavePriceOverrides()
        {
            return SavePriceProviderOverrides.Checked;
        }

        public SerializableXmlDocument XmlDoc = new SerializableXmlDocument();

        public bool UseOverriddenPrices
        {
            get { return OverrideAutomaticPriceProviderSelection.Checked; }
        }

        public bool ShowPriceList { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            
            // we need an equipment facade for the bookout links.
            _equipmentFacade = new EquipmentFacade(Handles.OwnerHandle, Handles.VehicleHandle);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // If we won't re-render our output, we can avoid a bunch of work.
            if (!WillReRender())
            {
                return;
            }

            // Try to get data from the domain.
            try
            {
                var pricingAnalysis = Deal.GetPricingAnalysis();

                // Refactoring
                OverrideAutomaticPriceProviderSelection.Visible = EditMode; // must remain "Visible" to capture the state on postback
                SavePriceProviderOverrides.Visible = EditMode;

                if (OverrideAutomaticPriceProviderSelection.Visible)
                {
                    OverrideAutomaticPriceProviderSelection.Checked = true;//vehiclePreference.HasOverriddenPriceProviders;
                    OverrideAutomaticPriceProviderSelection.Style["display"] = "none";
                }

                PaymentsSpan.InnerText = FormatPaymentsText();

                Deal.SaveXml(pricingAnalysis);
            }
            catch (Exception ex)
            {
                Log( ex );
                Visible = false;
                return;
            }

            XmlDoc = Deal.GetXml(NodeType.pricing);

            if (!EditMode && ! HasGaugePrices && ! HasComparisonPrice )
            {
                PricingPanel.CssClass = "emptyModule";
                PricingPanel.Attributes.Add("title", "This module will NOT be printed.");
            }
            else
            {
                PricingPanel.CssClass = "gaugeTableContainer roundbox clearfix";
                PricingPanel.Attributes.Remove("title");
            }

            PricingPanel.Visible = true;

            // Get the nodes.
            XmlNode showGaugeNode = XmlDoc.SelectSingleNode("/pricing/gaugeprices/@showGauge");
            XmlNode breakPositionNode = XmlDoc.SelectSingleNode("/pricing/gaugeprices/@breakPosition");

            if (showGaugeNode != null && showGaugeNode.Value == "true")
            {
                GaugePanel.Visible = true;

                if (breakPositionNode != null)
                {
                    string breakPosition = breakPositionNode.Value;

                    if (!string.IsNullOrEmpty(breakPosition) && int.Parse(breakPosition) > 0)
                    {
                        GaugeBreakDiv.Attributes.Add("style",
                                                     "left:" +
                                                     breakPositionNode.Value +
                                                     "px;");
                        GaugeBreakDiv.Visible = true;
                    }
                }

                GaugePricesRepeater.DataSource = XmlDoc.SelectNodes("/pricing/gaugeprices/price");
                GaugePricesRepeater.DataBind();
            }
            else
            {
                GaugePanel.Visible = false;
            }

            // Bind the controls to data.
            OtherPricesHeadList.DataSource = XmlDoc.SelectNodes("/pricing/gaugeprices/price[@displayHead='true' and @isOfferPrice != 'true']");
            OtherPricesHeadList.DataBind();

            PriceComparisonRepeater.DataSource = ComparisonPrices;
            PriceComparisonRepeater.DataBind();

            // Remove preview panel css class
            PricingPanel.CssClass = EditMode ? PricingPanel.CssClass.Replace("roundbox ", "") : PricingPanel.CssClass;

            // We should be visible.
            Visible = true;
        }


        private string FormatPaymentsText()
        {
            int down = Deal.CustomerPacketOptions.DownPayment;
            int monthly = Deal.CustomerPacketOptions.MonthlyPayment;

            StringBuilder formattedTextSb = new StringBuilder();
            if (down > 0 || monthly > 0)
            {
                formattedTextSb.Append("(");
                formattedTextSb.Append(down > 0 ? "$" + down + "/down" : string.Empty);
                formattedTextSb.Append(down > 0 && monthly > 0 ? " " : string.Empty);
                formattedTextSb.Append(monthly > 0 ? "$" + monthly + "/mo" : string.Empty);
                formattedTextSb.Append(")");
            }

            return formattedTextSb.ToString();

        }

        protected void PriceComparisonRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (EditMode &&  (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem ))
            {
                var xmlNode = (XmlNode) e.Item.DataItem;
                XElement price = XElement.Parse(xmlNode.OuterXml);
                XElement pricingDoc = XElement.Parse(xmlNode.OwnerDocument.OuterXml);

                var provider = (MarketAnalysisPriceProviders) int.Parse(price.Element("providerId").Value);

                if (_showComparisonBookoutLinks)
                {
                    // guidebook url check
                    SetupGuidebookUrl(e, price, provider);
                }

                // set the selected checkbox state
                bool selected = Boolean.Parse(price.Attribute("selected").Value);
                ((CheckBox)e.Item.FindControl("SelectProviderCheckBox")).Checked = selected;
            }
        }

        private void SetupGuidebookUrl(RepeaterItemEventArgs e, XElement price, MarketAnalysisPriceProviders provider)
        {
            var link = (HyperLink)e.Item.FindControl("ProviderLink");

            string providerUrl = GetProviderUrl(provider);

            if (providerUrl.Length > 0)
            {
                link.Visible = true;
                link.NavigateUrl = providerUrl;

                if (price.Element("isaccurate") != null)
                {
                    if (bool.Parse(price.Element("isaccurate").Value))
                    {   // the price is accurate
                        link.ToolTip = string.Empty;
                        link.CssClass.Replace(" inaccurate", string.Empty);
                    }
                    else
                    {   // the price is not accurate
                        link.ToolTip = "Inaccurate value (will not show up on gauge).";
                        link.CssClass += " inaccurate";
                    }
                }
            }
            else
            {
                link.Visible = false;
            }
        }

        private string GetProviderUrl(MarketAnalysisPriceProviders provider)
        {
            // KBB
            if (provider == MarketAnalysisPriceProviders.KelleyBlueBookRetailValue)
            {
                return _equipmentFacade.KbbUrl ?? String.Empty;
            }

            // Guidebooks
            if (        provider == MarketAnalysisPriceProviders.NADARetailValue
                    ||  provider == MarketAnalysisPriceProviders.Galves
                    ||  provider == MarketAnalysisPriceProviders.BlackBook)
            {
                return _equipmentFacade.EStockCardUrl;
            }

            // Edmunds
            if (provider == MarketAnalysisPriceProviders.EdmundsTrueMarketValue)
            {
                return _equipmentFacade.EdmundsTmvUrl;
            }

            // JD Power
            if (provider == MarketAnalysisPriceProviders.JDPowerPINAverageInternetPrice)
            {
                return _equipmentFacade.JDPowerUrl;
            }

            return string.Empty;
        }


        // TODO: Move
        internal string TrimApplicationPath(string path)
        {
            // If the path begins with the application path, remove it.
            string lowerPath = path.ToLower();
            string lowerAppPath = Request.ApplicationPath.ToLower();

            string returnPath = lowerPath.StartsWith( lowerAppPath ) ? lowerPath.Substring(lowerAppPath.Length - 1) : lowerPath;

            return returnPath;

        }

        public IEnumerable<int> SelectedPriceProviderIds
        {
            get
            {
                List<int> selectedProviderIds = new List<int>();

                if (EditMode)
                {
                    foreach (RepeaterItem repeaterItem in PriceComparisonRepeater.Items)
                    {
                        var selectProvider = (CheckBox)repeaterItem.FindControl("SelectProviderCheckBox");
                        if (selectProvider.Checked)
                        {
                            int providerId;
                            bool success = Int32.TryParse(selectProvider.Attributes["ProviderId"], out providerId);
                            if (success)
                            {
                                selectedProviderIds.Add(providerId);
                            }
                        }
                    }
                    return selectedProviderIds;
                }
                
                return null;
            }
        }

        SerializableXmlDocument IXmlDoc.XmlDoc
        {
            get { return XmlDoc; }
        }

        /// <summary>
        /// Returns a boolean indicating whether or not we have enough prices to render a gauge.
        /// Right now, we need at least two prices to serve as the endpoints.
        /// </summary>
        private bool HasGaugePrices
        {
            get { return GaugePrices.Count > 1; }
        }

        private bool HasComparisonPrice
        {
            get { return ComparisonPrices.Count > 0; }
        }

        private XmlNodeList _gaugePrices;

        private XmlNodeList GaugePrices
        {
            get
            {
                if (_gaugePrices == null)
                {
                    _gaugePrices = XmlDoc.SelectNodes("/pricing/gaugeprices/price");
                }
                return _gaugePrices;
            }
        }

        private XmlNodeList _comparisonPrices;

        private XmlNodeList ComparisonPrices
        {
            get
            {
                if (_comparisonPrices == null)
                {
                _comparisonPrices = 
                    EditMode ? XmlDoc.SelectNodes("/pricing/comparisonprices/price") 
                            : XmlDoc.SelectNodes("/pricing/comparisonprices/price[@selected='true']"); 
                }
                return _comparisonPrices;
            }
        }

        protected XmlNodeList GetListPrices(object dataItem)
        {
            var node = (XmlLinkedNode) dataItem;
            if (node != null)
            {
                return node.OwnerDocument.SelectNodes("/pricing/listprices/listprice");
            }

            return null;
        }
    }
}