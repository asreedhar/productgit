using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using FirstLook.Common.Core.DTO;
using FirstLook.DomainModel.Oltp;
using FirstLook.Merchandising.DomainModel.Marketing.Deal;
using FirstLook.Merchandising.DomainModel.Marketing.ValueAnalyzer;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;
using FirstLook.Pricing.DomainModel.Marketing.Presentation;
using FirstLook.Pricing.WebControls;

using FirstLook.Common.Core.Extensions;

using FirstLook.Common.Core;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls
{
    public class BaseUserControl : UserControl, IPresentationCommon
    {
        private PresentationCommon _common;

     
        protected override void OnLoad(EventArgs e)
        {
 	         base.OnLoad(e);

             var page = Page as BasePage;
             if (page != null)
             {
                 _common = page.Common;
             }
        }

        /// <summary>
        /// Will the control's output be rendered to the browser?  In cases where the control is contained within
        /// an updatepanel, it will post-back, but not re-render its content.  This is a good opportunity to avoid
        /// work.
        /// 
        /// Note: This is only reliable after the page's OnLoadComplete event has fired.
        /// </summary>
        /// <returns></returns>
        protected virtual bool WillReRender()
        {
            ScriptManager sm = ScriptManager.GetCurrent(Page);

            // This is a normal post-back, so the control will re-render.
            if (sm == null || !sm.IsInAsyncPostBack)
            {
                return true;
            }

            // Find the nearest container panel.
            ExtendedUpdatePanel panel = FindContainerPanel();

            if (panel != null)
            {
                // Ask the panel whether it will re-render.
                return panel.WillReRender(sm.AsyncPostBackSourceElementID);
            }

            // Reasonable default.
            return true;
        }

        /// <summary>
        /// Find the first containing panel.
        /// </summary>
        /// <returns></returns>
        private ExtendedUpdatePanel FindContainerPanel()
        {
            Control parent = Parent;

            while (parent != null)
            {
                if (parent is ExtendedUpdatePanel)
                {
                    return parent as ExtendedUpdatePanel;
                }
                parent = parent.Parent;
            }
            return null;
        }

        /// <summary>
        /// Get csv list of keys from module data items.
        /// </summary>
        /// <returns>string</returns>
        public string GetKeyList(IEnumerable<ISurrogateKey> itemList)
        {
            return itemList.Select( i => i.Key ).ToDelimitedString(",");
        }

        #region Implementation of IPresentationCommon

        public ICache CustomCache
        {
            get { return _common.CustomCache; }
        }

        public ILogger CustomLog
        {
            get { return _common.CustomLog; }
        }

        public void Log(Exception e)
        {
            _common.Log(e);
        }

        public IPresentationUtility Utility
        {
            get { return _common.Utility; }
        }

        public IDeal Deal
        {
            get { return _common.Deal; }
            set { _common.Deal = value; }
        }

        public IHandles Handles
        {
            get { return _common.Handles; }
        }

        public BusinessUnit BusinessUnitObject
        {
            get { return _common.BusinessUnitObject;  }
        }

        public Inventory InventoryObject
        {
            get { return _common.InventoryObject; }
        }

        public string UserName
        {
            get { return _common.UserName; }
        }

        public ValueAnalyzerVehiclePreferenceTO GetValueAnalyzerPreference()
        {
            return _common.GetValueAnalyzerPreference();
        }

        public void SaveValueAnalyzerPreference(ValueAnalyzerVehiclePreferenceTO preference)
        {
            _common.SaveValueAnalyzerPreference(preference);
        }

       

        #endregion
    }



}
