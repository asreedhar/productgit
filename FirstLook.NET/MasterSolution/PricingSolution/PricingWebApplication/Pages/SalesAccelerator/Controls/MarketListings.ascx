<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="MarketListings.ascx.cs" 
    Inherits="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls.MarketListings" %>
<%@ Import Namespace="FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search" %>
<%@ Import Namespace="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator"%>

<asp:Panel ID="SellingSheetLayout" runat="server" Visible="true" CssClass="previewPanel clearfix">
    <a class="moduleEditLink" href="#">Edit Market Listings</a>
    <h3>Comparison Cars in Market:</h3>
    
    <asp:Panel ID="VehiclePreferenceListingsFieldsPanel" runat="server" Visible="false">
        <input type="hidden" ID="OriginalSelectedItems" runat="server" />
        <input type="hidden" ID="CurrentSelectedItems" runat="server" />
        <input type="hidden" ID="ActiveFiltersHidden" class="activeFiltersHidden" runat="server" />
    </asp:Panel>

    <asp:Repeater ID="VehicleComparisonRepeater1" runat="server" OnItemDataBound="VehicleComparisonRepeater_OnItemDataBound">
        <HeaderTemplate>
            <table>
                <thead>
                    <tr>
                        <asp:PlaceHolder ID="DisplayColumnPlaceHolder" runat="server">
                            <th align="left"></th>
                        </asp:PlaceHolder>
                        
                        <asp:PlaceHolder ID="DisplayDealerPlaceHolder" runat="server">
                            <th sortCode="<%= SortCode.DealerName %>" align="left" class="dealer">Dealership</th>
                        </asp:PlaceHolder>
                        
                        <th sortCode="<%= SortCode.VehicleName %>"  align="left">Vehicle</th>
                        <th sortCode="<%= SortCode.Color %>">Color</th>
                        <th sortCode="<%= SortCode.Mileage %>">Mileage</th>
                        <th sortCode="<%= SortCode.IsCertified %>">Certified?</th>
                        <th sortCode="<%= SortCode.ListPrice %>">Internet Price</th>
                        <th sortCode="<%= SortCode.ListPrice %>">Your Price Savings</th>                        
                    </tr>
                </thead>
                <tbody>
        </HeaderTemplate> 
        <FooterTemplate>
                </tbody>
            </table>
            <asp:Label ID="EmptyDataSetLabel" CssClass="emptyDataSetLabel" runat="server" Visible='<%# VehicleComparisonRepeater1.Items.Count == 0 %>'>No Market Listings selected</asp:Label>
        </FooterTemplate>
        <ItemTemplate>
            <tr class="even" id='<%# XPath("vehicle/vehicleId") %>'>
                <asp:PlaceHolder ID="DisplayColumnPlaceHolder" runat="server">
                    <td align="center">
                       <input type="checkbox" id="SelectedListingCheckbox" value='<%# XPath("vehicle/vehicleId") %>' runat="server" checked="checked" />
                    </td>
                </asp:PlaceHolder>
                
                <asp:PlaceHolder ID="DisplayDealerPlaceHolder" runat="server">
                    <td class="dealer"><%# XPath("dealer/name")%></td>
                </asp:PlaceHolder>
                
                <td><%# XPath("vehicle/name")%></td>
                <td align="center"><%# XPath("vehicle/color")%></td>
                <td align="center"><%# Utility.FormatNumber(XPath("vehicle/mileage").ToString(), NumberType.Number)%></td>
                <td align="center" class="certified"><%# bool.Parse(XPath("vehicle/certified").ToString()) ? "<div class=\"checkmark\"><span>Yes</span></div>" : string.Empty%></td>
                <td align="center"><%# Utility.FormatNumber(XPath("vehicle/price").ToString(), NumberType.Currency)%></td>
                <td align="center" class="savings"><span class='<%# int.Parse(XPath("vehicle/savings").ToString()).CompareTo(0) < 0 ? "red" : "" %>'>You save <strong><br /><%# Utility.FormatNumber(XPath("vehicle/savings").ToString(), NumberType.Currency)%></strong></span></td>
            </tr>
            <asp:PlaceHolder ID="DisplayDescriptionPlaceHolder" runat="server">
                <tr class="even vehicleDescription" id='<%# XPath("vehicle/vehicleId") %>'>
                    <td colspan="8"><p><%# string.IsNullOrEmpty(XPath("vehicle/description").ToString().Trim()) ? "No ad preview found." : XPath("vehicle/description").ToString()%></p></td>
                </tr>
            </asp:PlaceHolder>
        </ItemTemplate>
        <AlternatingItemTemplate>
            <tr class="odd" id='<%# XPath("vehicle/vehicleId") %>'>
                <asp:PlaceHolder ID="DisplayColumnPlaceHolder" runat="server">
                    <td align="center">
                        <input type="checkbox" id="SelectedListingCheckbox" value='<%# XPath("vehicle/vehicleId") %>' runat="server" checked="checked" />
                    </td>
                </asp:PlaceHolder>
            
                <asp:PlaceHolder ID="DisplayDealerPlaceHolder" runat="server">
                    <td class="dealer"><%# XPath("dealer/name")%></td>    
                </asp:PlaceHolder>
                
                <td><%# XPath("vehicle/name")%></td>
                <td align="center"><%# XPath("vehicle/color")%></td>
                <td align="center"><%# Utility.FormatNumber(XPath("vehicle/mileage").ToString(), NumberType.Number)%></td>
                <td align="center" class="certified"><%# bool.Parse(XPath("vehicle/certified").ToString()) ? "<div class=\"checkmark\"><span>Yes</span></div>" : string.Empty%></td>
                <td align="center"><%# Utility.FormatNumber(XPath("vehicle/price").ToString(), NumberType.Currency)%></td>
                <td align="center" class="savings"><span class='<%# int.Parse(XPath("vehicle/savings").ToString()).CompareTo(0) < 0 ? "red" : "" %>'>You save <strong><br /><%# Utility.FormatNumber(XPath("vehicle/savings").ToString(), NumberType.Currency)%></strong></span></td>
            </tr>
            <asp:PlaceHolder ID="DisplayDescriptionPlaceHolder" runat="server">
                 <tr class="odd vehicleDescription" id='<%# XPath("vehicle/vehicleId") %>'>
                    <td colspan="8"><p><%# string.IsNullOrEmpty(XPath("vehicle/description").ToString().Trim()) ? "No ad preview found." : XPath("vehicle/description").ToString()%></p></td>
                </tr>
            </asp:PlaceHolder>
        </AlternatingItemTemplate>
    </asp:Repeater>
</asp:Panel>

<asp:Panel ID="MarketComparisonLayout" runat="server" Visible="true" CssClass="clearfix">
    <a class="moduleEditLink" href="#">Edit Market Listings</a>
    <h3>Compare With:</h3>
    <asp:Repeater ID="VehicleComparisonRepeater2" runat="server" OnItemDataBound="VehicleComparisonRepeater_OnItemDataBound">
        <HeaderTemplate><table></HeaderTemplate>
        <FooterTemplate></table></FooterTemplate>
        <ItemTemplate>
            <tr>
                <td class="mcItemHeader">
                    <ul>
                        <li class="vehicleMakeModel">
                            <span class="highlight"><%# XPath("vehicle/name")%></span>
                            <ul>
                                <li>Color: <%# XPath("vehicle/color")%></li>
                                <li>Mileage: <%# Utility.FormatNumber(XPath("vehicle/mileage").ToString(), NumberType.Number)%></li>
                                <%# bool.Parse(XPath("vehicle/certified").ToString()) ? "<li><strong>Certified</strong></li>" : string.Empty%>
                            </ul>
                        </li>
                        
                        <asp:PlaceHolder ID="DisplayDealerPlaceHolder" runat="server">
                            <li class="vehicleDealer">
                                Dealership: <span class="highlight"><%# XPath("dealer/name")%></span>
                            </li>
                        </asp:PlaceHolder>
                        
                        <li class="vehiclePrice">
                            Their Price: <%# Utility.FormatNumber(XPath("vehicle/price").ToString(), NumberType.Currency)%><br />
                            <span class='savings<%# int.Parse(XPath("vehicle/savings").ToString()).CompareTo(0) < 0 ? " red" : " green" %>'>You save <strong><%# Utility.FormatNumber(XPath("vehicle/savings").ToString(), NumberType.Currency)%></strong></span>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td class="mcItemDesc"><p><%# string.IsNullOrEmpty(XPath("vehicle/description").ToString().Trim()) ? "No ad preview found." : XPath("vehicle/description").ToString()%></p></td>
            </tr>
        </ItemTemplate>
    </asp:Repeater>
</asp:Panel>