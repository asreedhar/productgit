<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="VehicleImage.ascx.cs" Inherits="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls.VehicleImage" %>

<div id="VehiclePhoto" class="moduleContainer">
    <asp:CheckBox ID="ToggleDisplayOnSellingSheetCheckBox" runat="server" AutoPostBack="false" CssClass="displayOnSheetCkb" Text="Display on Consumer Packet?" OnCheckedChanged="ToggleDisplayOnSellingSheetCheckBox_CheckChanged" />
    
    <asp:HiddenField ID="PhotoUrl" runat="server" />
    <asp:HiddenField ID="PhotoHeight" runat="server" />
    <asp:HiddenField ID="PhotoWidth" runat="server" />
    <asp:HiddenField ID="PhotoManagerUrl" runat="server" />
    
    <asp:Image id="VehiclePhotoImage" runat="server" alt="Vehicle Image" />
    <a id="EditVehiclePhotoImageLink" runat="server" class="vehiclePhotoEditLink" href="#">Edit</a>
           
    <div class="reloadAfterPopUpFields" style="display:none;">
        <asp:LinkButton ID="RefreshVehicleImageButton" runat="server" CssClass="saveBtn closeBtn">Refresh Vehicle Photo</asp:LinkButton>
        <input type="hidden" id="VehicleImageRefreshButtonClientIdHidden" value='<%= RefreshVehicleImageButton.ClientID %>' />
        <input type="hidden" id="VehicleImageRefreshButtonUniqueIdHidden" value='<%= RefreshVehicleImageButton.UniqueID %>' />
    </div>
    
</div>