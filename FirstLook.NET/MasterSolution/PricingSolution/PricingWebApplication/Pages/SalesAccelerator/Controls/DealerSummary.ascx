<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="DealerSummary.ascx.cs" 
    Inherits="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls.DealerSummary" EnableViewState="false" %>

<input type="hidden" runat="server" id="DisplayContactInfoOnPdfHidden" class="showContactInfoOnWebsitePdf" />

<div class="logo">
    <asp:Image ID="DealerLogoImage" runat="server" />
</div>
<div class="copy">
    
    <p id="DealerDescriptionP" runat="server">
        <%= Utility.WriteNode(XmlDoc.SelectSingleNode("/dealer/description"))%>
    </p>
    
    <p id="DealerContactP" runat="server" class="address">
        <%= Utility.WriteNode(XmlDoc.SelectSingleNode("/dealer/contact/address")) + (string.IsNullOrEmpty(XmlDoc.SelectSingleNode("/dealer/contact/address").InnerText) ? string.Empty : " &middot; ")%>
        <%= Utility.WriteNode(XmlDoc.SelectSingleNode("/dealer/contact/city")) + (string.IsNullOrEmpty(XmlDoc.SelectSingleNode("/dealer/contact/city").InnerText) ? string.Empty : ", ")%>
        <%= Utility.WriteNode(XmlDoc.SelectSingleNode("/dealer/contact/state"))%> 
        <%= Utility.WriteNode(XmlDoc.SelectSingleNode("/dealer/contact/zip")) + (string.IsNullOrEmpty(XmlDoc.SelectSingleNode("/dealer/contact/zip").InnerText) ? string.Empty : "<span class=\"dealerContactInfo\"> &middot; </span>")%>
        <span class="dealerContactInfo"><%= Utility.WriteNode(XmlDoc.SelectSingleNode("/dealer/contact/email")) + (string.IsNullOrEmpty(XmlDoc.SelectSingleNode("/dealer/contact/email").InnerText) ? string.Empty : " &middot; ")%></span>
        <% if (!string.IsNullOrEmpty(XmlDoc.SelectSingleNode("/dealer/url").InnerText))
           { %>
               <a href="<%= Utility.WriteNode(XmlDoc.SelectSingleNode("/dealer/url")) %>" target="_blank"><%= Utility.WriteNode(XmlDoc.SelectSingleNode("/dealer/url"))%></a> &middot; 
        <% } %>
        <span class="dealerContactInfo"><%= Utility.WriteNode(XmlDoc.SelectSingleNode("/dealer/contact/phone"))%> </span>
    </p>
    
</div>