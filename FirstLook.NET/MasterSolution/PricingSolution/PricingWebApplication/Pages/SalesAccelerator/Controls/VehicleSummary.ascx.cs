using System;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Xml;
using FirstLook.Merchandising.DomainModel.Marketing.Commands;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls
{
    public partial class VehicleSummary : BaseUserControl, IRenderXml, IXmlDoc
    {
        public SerializableXmlDocument XmlDoc = new SerializableXmlDocument();

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // If we won't re-render our output, we can avoid a bunch of work.
            if (!WillReRender())
            {
                return;
            }

            try
            {
                string xml = AsXml();

                if (xml.Length > 0)
                {
                    XmlDoc.LoadXml(xml);
                    Visible = true;
                }
                else
                {
                    Visible = false;
                }

            }
            catch (Exception ex)
            {
                Log(ex);
                Visible = false;
            }
        }

      

        public string AsXml()
        {
            GetVehicleSummaryCommand command = new GetVehicleSummaryCommand(Handles.OwnerHandle, Handles.VehicleHandle);
            AbstractCommand.DoRun( command );

            string xml = command.VehicleSummary;
            return xml;
        }

        SerializableXmlDocument IXmlDoc.XmlDoc
        {
            get { return XmlDoc; }
        }
    }
}