﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentsInformation.ascx.cs" Inherits="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls.PaymentsInformation" %>

<asp:Panel ID="PaymentsInformationPanel" runat="server" Visible="false">
    <div id="PaymentsInformationDiv">
        
        <asp:Panel ID="LeasePaymentsInfoPanel" runat="server">
            <div id="LeasePaymentsInfoDiv" class="termPayments">
                <strong>LEASE FOR</strong>
                <span class="monthlyPayments">$<asp:Literal ID="LeaseMonthlyPaymentsLiteral" runat="server"></asp:Literal>/month</span>
                <span class="downPaymentAndTerms">
                    for <asp:Literal ID="LeaseTermsLiteral" runat="server"></asp:Literal>
                    months with $<asp:Literal ID="LeaseDownLiteral" runat="server"></asp:Literal> down
                </span>
            </div>
            <asp:Panel ID="LeaseInterestRatePanel" runat="server">
                <div id="LeaseInterestRate" class="interestRate">
                    (<asp:Literal ID="LeaseInterestRateLiteral" runat="server"></asp:Literal>% annual percentage rate)
                </div>
            </asp:Panel>
        </asp:Panel>
        
        <asp:Panel ID="OwnPaymentsInfoPanel" runat="server">
            <div id="OwnPaymentsInfoDiv" class="termPayments">
                <strong>OWN FOR</strong>
                <span class="monthlyPayments">$<asp:Literal ID="OwnMonthlyPaymentsLiteral" runat="server"></asp:Literal>/month</span>
                <span class="downPaymentAndTerms">
                    for <asp:Literal ID="OwnTermsLiteral" runat="server"></asp:Literal>
                    months with $<asp:Literal ID="OwnDownLiteral" runat="server"></asp:Literal> down
                </span>
            </div>
            <asp:Panel ID="OwnInterestRatePanel" runat="server">
                <div id="OwnInterestRate" class="interestRate">
                    (<asp:Literal ID="OwnInterestRateLiteral" runat="server"></asp:Literal>% annual percentage rate)
                </div>
            </asp:Panel>
        </asp:Panel>
        
    </div>
</asp:Panel>