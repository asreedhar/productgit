using System;
using FirstLook.Common.Core.Xml;
using FirstLook.Merchandising.DomainModel.Marketing.Deal;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls
{
    public partial class VehicleYearModel : BaseUserControl, IXmlDoc 
    {
        private bool _showPrice, _showDealer;

        public bool ShowPrice
        {
            get { return _showPrice; }
            set { _showPrice = value; }
        }

        public bool ShowDealer
        {
            get { return _showDealer; }
            set { _showDealer = value; }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // If we won't re-render our output, we can avoid a bunch of work.
            if (!WillReRender())
            {
                return;
            }

            try
            {
                var vehicleYearModel = Deal.GetVehicleYearModel();
                if (vehicleYearModel.Xml.Length > 0)
                {
                    Visible = true;
                    Deal.SaveXml(vehicleYearModel);
                }
                else
                {
                    Visible = false;
                }
            }
            catch (Exception ex)
            {
                Log(ex);
                Visible = false;
            }
        }

        public void SetCarFaxIconVisible(bool visible)
        {
            CarFaxOneOwnerIconImage.Visible = visible;
        }

        public void SetLogo(string logoUrl)
        {
            if (string.IsNullOrEmpty(logoUrl))
            {
                CertifiedProgramLogoImage.Visible = false;
                return;
            }

            CertifiedProgramLogoImage.ImageUrl = logoUrl;
            CertifiedProgramLogoImage.Visible = true;
        }

        public SerializableXmlDocument XmlDoc
        {
            get
            {
                return Deal.GetXml(NodeType.vehicleyearmodel);
            }
        }
    }
}