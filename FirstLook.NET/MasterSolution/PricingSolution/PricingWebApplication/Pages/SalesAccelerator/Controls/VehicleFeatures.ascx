<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="VehicleFeatures.ascx.cs" 
    Inherits="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls.VehicleFeatures" EnableViewState="false" %>

<div id="VehicleFeaturesPreviewPanel" runat="server" class="previewPanel clearfix">
    <a class="moduleEditLink" href="#">Edit Equipment</a>
    <h4>Equipment:</h4>
    <asp:Repeater ID="VehicleFeaturesRepeater" runat="server">
        <HeaderTemplate><ul class="clearfix"></HeaderTemplate>
        <FooterTemplate></ul></FooterTemplate>
        <ItemTemplate>
            <li><%# XPath(".") %></li>
        </ItemTemplate>
    </asp:Repeater>
</div>
