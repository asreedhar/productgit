<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ConsumerHighlights.ascx.cs" 
    Inherits="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls.ConsumerHighlights" %>
<%@ Import Namespace="FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights"%>

<div id="ConsumerHighlightsPreviewPanel" runat="server" class="clearfix previewPanel highlightsPreview">
    <a class="moduleEditLink" href="#">Edit Vehicle Highlights</a>
    <h4 class="emptySetOnly">Highlights:</h4>
    <asp:HiddenField ID="ShowCarFaxOneOwnerHidden" runat="server" />    

    <div id="CircleRatingsDiv" runat="server" class="clearfix highlightSection">
        <h4>JDPower.com Ratings:</h4>
        <asp:Repeater ID="CircleRatingHighlightRepeater" runat="server">
            <HeaderTemplate><ul id="CircleRatingHighlightsUL"></HeaderTemplate>
            <FooterTemplate></ul></FooterTemplate>
            <ItemTemplate>
                <li><%# XPath("description") %>: <span title="<%# string.IsNullOrEmpty(XPath("value").ToString()) ? XPath("value") : XPath("value") + " Circles" %>" class="stars<%# XPath("value").ToString().Trim().Replace(".", "") %>"><%# XPath("value") %></span></li>
            </ItemTemplate>
        </asp:Repeater>
    </div>

    <div id="ConsumerHighlightsDiv" runat="server" class="clearfix highlightSection">
        <h4>Additional Highlights:</h4>
        <asp:Repeater ID="FreeTextHighlightRepeater" runat="server">
            <HeaderTemplate><ul id="FreeTextHighlightsUL"></HeaderTemplate>
            <FooterTemplate></ul></FooterTemplate>
            <ItemTemplate>
                <li><%# XPath("text") %></li>
            </ItemTemplate>
        </asp:Repeater>
    </div>

    <div id="ReviewsAwardsDiv" runat="server" class="clearfix highlightSection">
        <h4>Expert Reviews:</h4>
        <asp:Repeater ID="SnippetHighlightRepeater" runat="server">
            <HeaderTemplate><ul id="SnippetHighlightsUL"></HeaderTemplate>
            <FooterTemplate></ul></FooterTemplate>
            <ItemTemplate>
                <li><span class="source"><%# XPath("source") %>:</span> <%# XPath("snippet") %></li>
            </ItemTemplate>
        </asp:Repeater>
    </div>

    <div id="VHRHighlightsDiv" runat="server" class="clearfix highlightSection">
        <h4>
            Vehicle History Highlights:
            <asp:Image ID="CARFAXLogo" runat="server" ImageUrl="~/Public/Images/logo_Carfax1.gif" ImageAlign="AbsMiddle" Visible="true" style="margin-left:0.5em;" />
            <asp:Image ID="AutoCheckLogo" runat="server" ImageUrl="~/Public/Images/logo_Autocheck1.gif" ImageAlign="AbsMiddle" Visible="true" style="margin-left:0.5em;" />
        </h4>
        <asp:Repeater ID="VehicleHistoryHighlightRepeater" runat="server">
            <HeaderTemplate><ul id="VHRHighlightsUL"></HeaderTemplate>
            <FooterTemplate></ul></FooterTemplate>
            <ItemTemplate>
                <li><%# XPath("text") %></li>
            </ItemTemplate>
        </asp:Repeater>
    </div>

</div>