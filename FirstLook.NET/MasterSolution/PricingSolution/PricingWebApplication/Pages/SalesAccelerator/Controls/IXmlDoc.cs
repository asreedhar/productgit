﻿using FirstLook.Common.Core.Xml;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls
{
    public interface IXmlDoc
    {
        SerializableXmlDocument XmlDoc { get; }
    }
}
