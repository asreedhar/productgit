<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="OfferFooter.ascx.cs" 
    Inherits="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls.OfferFooter" %>

<div id="CVAFooter">
    
    <asp:PlaceHolder ID="CustomerInfo" runat="server">
        <div id="CustomerInfo">
            <h3>Offer valid until <asp:Literal ID="ValidUntilDateLiteral" runat="server"></asp:Literal>  for:</h3>
            <ul>
                <li><asp:Literal ID="CustomerName" runat="server" /></li>
                <li>Phone: <asp:Literal ID="CustomerPhoneNumber" runat="server" /></li>
                <li>Email: <asp:Literal ID="CustomerEmail" runat="server" /></li>
            </ul>
        </div>
    </asp:PlaceHolder>


    <asp:PlaceHolder id="SalespersonInfo" runat="server">
        <div id="SalespersonInfo">
            <h3>Prepared by:</h3>
            <ul>
                <li><asp:Literal ID="SalesPersonName" runat="server" /></li>
                <li><asp:Literal ID="DealerName" runat="server" /></li>
            </ul>
        </div>
    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="Disclaimer" runat="server">
        <p class="disclaimer"><asp:Literal ID="DealerDisclaimerLiteral" runat="server" /></p>
    </asp:PlaceHolder>
</div>