<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="CertifiedBenefits.ascx.cs" 
Inherits="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls.CertifiedBenefits" EnableViewState="false" %>

<div class="previewPanel clearfix roundBtmBox">
    <h4><%= Utility.WriteNode(XmlDoc.SelectSingleNode("/certifiedbenefits/@name")).ToUpper() %> BENEFITS</h4>
    <asp:Repeater ID="CertifiedFeaturesRepeater" runat="server">
        <HeaderTemplate><ul class="clearfix"></HeaderTemplate>
        <FooterTemplate></ul></FooterTemplate>
        <ItemTemplate>
            <li><%# XPath(".") %></li>
        </ItemTemplate>
    </asp:Repeater>
    <div id="CertifiedFeaturesBtm">&nbsp;</div>
</div>