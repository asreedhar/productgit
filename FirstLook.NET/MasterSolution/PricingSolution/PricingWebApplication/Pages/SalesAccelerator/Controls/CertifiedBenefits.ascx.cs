using System;
using FirstLook.Common.Core.Xml;
using FirstLook.Merchandising.DomainModel.Marketing.Deal;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls
{
    public partial class CertifiedBenefits : BaseUserControl, IXmlDoc
    {
        public SerializableXmlDocument XmlDoc
        {
            get { return Deal.GetXml(NodeType.certifiedbenefits); }
        }

        public string CertifiedProgramLogo { get; set; }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // By default, we don't show the benefits.
            Visible = false;

            var certifiedBenefits = Deal.GetCertifiedBenefits();
            if (certifiedBenefits.CertifiedExists)
            {
                // Save the preference XML for both certified and non-certified vehicles.
                try
                {
                    Deal.SaveXml(certifiedBenefits);
                    var xmlDoc = Deal.GetXml(NodeType.certifiedbenefits);

                    CertifiedProgramLogo = xmlDoc.SelectSingleNode("/certifiedbenefits/@logoPath").Value;
                    CertifiedFeaturesRepeater.DataSource = xmlDoc.SelectNodes("/certifiedbenefits/benefit");
                    CertifiedFeaturesRepeater.DataBind();

                    Visible = true;
                }
                catch (Exception ex)
                {
                    // On error conditions, we shouldn't load any xml.                 
                    Visible = false;
                    Log(ex);
                }

            }
        }
    }
}