<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="PreviewNavigationForm.ascx.cs" Inherits="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls.PreviewNavigationForm" %>

<%@ Register Src="~/Pages/Internet/Controls/VehicleHistoryReport.ascx" TagPrefix="Controls" TagName="VehicleHistoryReport" %>

<div id="CVASettingsForm">
    <asp:HiddenField ID="WebSitePdfAutoSave" runat="server" />
	<asp:HiddenField ID="WebSitePdfActive" runat="server" />
	<asp:HiddenField ID="WebSitePdfMarketListingsActive" runat="server" />
	<input type="hidden" id="SalesPacketMode" runat="server" class="salesPacketMode" />
    <fieldset id="SwitchViewFields">
        <span class="bold">Edit</span>
        <asp:DropDownList ID="PreviewSheetDDL" runat="server" CssClass="previewSheetDDL">
            <asp:ListItem Text="Sales Packet" Value="customer"></asp:ListItem>
            <asp:ListItem Text="MAX Info Displayed" Value="website"></asp:ListItem>
        </asp:DropDownList>
    </fieldset>
    
    <asp:Panel ID="CustomerPacketFormPanel" runat="server" DefaultButton="PrintSheetButton">
        <div id="SellingSheetFields">
            <fieldset id="PacketOptions">
                <legend>Sales Packet Options</legend>
                <div id="SalesPacketPreviewWidget">
                    <span>View/Edit:</span>
                    <ul>
                        <li id="SellingSheetSelector" class="selected"><a href="#" name="sales">Selling Sheet</a></li>
                        <li id="MktComparisonSelector"><a href="#" name="market">Market Comparison</a></li>
                    </ul>
                </div>
                <div id="SellingSheetIncludes">
                    <label>Include on Selling Sheet:</label>
                    <asp:CheckBoxList ID="PreviewIncludesCkbList" runat="server" CssClass="ckbList" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Flow" TextAlign="right">
                        <%--<asp:ListItem Text="Dealer Summary"></asp:ListItem>--%>
                        <asp:ListItem Text="Vehicle Photo" Value="photo"></asp:ListItem>
                        <asp:ListItem Text="Equipment" Value="equipment"></asp:ListItem>
                        <asp:ListItem Text="Vehicle Highlights" Value="highlights"></asp:ListItem>
                        <asp:ListItem Text="Certified Benefits" Value="certified"></asp:ListItem>
                        <%--<asp:ListItem Text="Ad Preview" Value="adpreview"></asp:ListItem>--%>
                        <asp:ListItem Text="360&amp;deg; Pricing Analysis" Value="analysis"></asp:ListItem>
                        <%--<asp:ListItem Text="Payment" Value="payment"></asp:ListItem>--%>
                    </asp:CheckBoxList>
                </div>

                <div id="MarketComparisonIncludes">
                    <label>Include on Market Comparison:</label>
                    <div class="ckbList">
                        <asp:CheckBox ID="EquipmentOnComparisonCkb" runat="server" TextAlign="Right" Text="Equipment" Checked="false" />
                    </div>
                </div>
            </fieldset>
            
            <fieldset id="VHRWidgets">
                <legend>Vehicle History Reports</legend>
                <pwc:ExtendedUpdatePanel ID="VHRExtendedUpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="VHRReports" class="vhrWidgets moduleContainer">
                            <Controls:VehicleHistoryReport ID="VehicleHistoryReport" runat="server"/>
                        </div>
                    </ContentTemplate>
                </pwc:ExtendedUpdatePanel>
            </fieldset>
            
            <fieldset id="OfferDetails">
                <legend>Customer Details</legend>
                <div id="PreviewForm" class="moduleContainer">
                    <pwc:ExtendedUpdatePanel ID="PreviewFormUpdatePanel" UpdateMode="Conditional" runat="server">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ResetDealButton" /> 
                        </Triggers>
                        <ContentTemplate>
                            <div id="CustomerInfoDiv">
                                <label>Customer First Name</label>
                                <asp:TextBox ID="FirstNameTextBox" runat="server" Width="180px" class="text custFName"></asp:TextBox>
                                <label>Last Name</label>
                                <asp:TextBox ID="LastNameTextBox" runat="server" Width="180px" class="text custLName"></asp:TextBox>
                                <label>Phone</label>
                                <asp:TextBox ID="PhoneTextBox" runat="server" Width="180px" class="text custPhone"></asp:TextBox>
                                <label>Email</label>
                                <asp:TextBox ID="EmailTextBox" runat="server" Width="180px" class="text custEmail"></asp:TextBox>
                                <label>Salesperson</label>
                                <asp:TextBox ID="SalespersonTextBox" runat="server" Width="180px" class="text salesName"></asp:TextBox>
                                <label>Offer Price</label>
                                <asp:Panel ID="PreviewFormOfferPricePanel" runat="server" DefaultButton="RefreshPreviewButton2">
                                    <asp:HiddenField ID="OriginalOfferPrice" runat="server" />
                                    <asp:HiddenField ID="OfferPriceLow" runat="server" />
                                    <asp:HiddenField ID="OfferPriceLowWarning" runat="server" />
                                    <asp:HiddenField ID="OfferPriceHigh" runat="server" />
                                    <asp:HiddenField ID="OfferPriceHighWarning" runat="server" />
                                
                                    <asp:TextBox ID="OfferPriceTextBox" runat="server" Width="80px" class="offerPrice floatLeft text"></asp:TextBox>
                                    <span id="OfferPriceError" style="display:none;" class="error"><br />Offer price must be greater than zero.</span>
                                </asp:Panel>
                                <label class="clear">Valid Until</label>
                                <asp:TextBox ID="ValidDateTextBox" runat="server" Width="80px" CssClass="datePickerText text"></asp:TextBox>
                                <div class="ie6DatePicker clearfix"><a href="#" class="datePickerButton" onclick="$('#<%= ValidDateTextBox.ClientID %>').focus();return false;" style="width:14px;overflow:hidden;padding:0px;"><span>Choose Date</span></a></div>
                                <asp:RangeValidator ID="ValidDateTextBoxValidator" runat="server" ControlToValidate="ValidDateTextBox" Type="Date" Display="dynamic" CssClass="clear"></asp:RangeValidator>
                                <input type="hidden" ID="DatePickerIconPathHidden" runat="server" class="datePickerIconPath" />
                            </div>
                            <div id="PaymentInfoForm">
                                <h4>Payments</h4>
                                <div id="LeasePaymentsInfo">
                                    <div class="checkboxDiv">
                                        <asp:CheckBox ID="LeaseCheckbox" runat="server" Text="Lease" TextAlign="Right" CssClass="bold" />
                                    </div>
                                    <div class="paymentsPerMonth">
                                        $<asp:TextBox ID="LeasePaymentsAmount" runat="server"></asp:TextBox>
                                        <label for="<%= LeasePaymentsAmount.ClientID %>">/month</label>
                                    </div>
                                    <div class="paymentsTerm">
                                        <asp:TextBox ID="LeasePaymentsTerm" runat="server"></asp:TextBox>
                                        <label for="<%= LeasePaymentsTerm.ClientID %>">months</label>
                                    </div>
                                    <div class="downPayment">
                                        $<asp:TextBox ID="LeaseDownPaymentAmount" runat="server"></asp:TextBox>
                                        <label for="<%= LeaseDownPaymentAmount.ClientID %>">down</label>
                                    </div>
                                    <div class="interestRate">
                                        %<asp:TextBox ID="LeaseInterestRate" runat="server"></asp:TextBox>
                                        <label for="<%= LeaseInterestRate.ClientID %>">interest</label>
                                    </div>
                                    <div class="checkboxDiv">
                                        <asp:CheckBox ID="ShowLeaseInterestRateCheckBox" runat="server" Text="Show interest rate" CssClass="floatRight" TextAlign="Right" />
                                    </div>
                                </div>
                                <div id="OwnPaymentsInfo" class="clearfix">
                                    <div class="checkboxDiv">
                                        <asp:CheckBox ID="OwnCheckbox" runat="server" Text="Own" TextAlign="Right" CssClass="bold" />
                                    </div>
                                    <div class="paymentsPerMonth">
                                        $<asp:TextBox ID="OwnPaymentsAmount" runat="server"></asp:TextBox>
                                        <label for="<%= OwnPaymentsAmount.ClientID %>">/month</label>
                                    </div>
                                    <div class="paymentsTerm">
                                        <asp:TextBox ID="OwnPaymentsTerm" runat="server"></asp:TextBox>
                                        <label for="<%= OwnPaymentsAmount.ClientID %>">months</label>
                                    </div>
                                    <div class="downPayment">
                                        $<asp:TextBox ID="OwnDownPaymentAmount" runat="server"></asp:TextBox>
                                        <label for="<%= OwnDownPaymentAmount.ClientID %>">down</label>
                                    </div>
                                    <div class="interestRate">
                                        %<asp:TextBox ID="OwnInterestRate" runat="server"></asp:TextBox>
                                        <label for="<%= OwnInterestRate.ClientID %>">interest</label>
                                    </div>
                                    <div class="checkboxDiv">
                                        <asp:CheckBox ID="ShowOwnInterestRateCheckBox" runat="server" Text="Show interest rate" CssClass="floatRight" TextAlign="Right" />
                                    </div>
                                </div>
                            </div>
                            <div>
                                <asp:Button ID="RefreshPreviewButton2" runat="server" Text="Update Deal" OnClick="RefreshPreviewButton_OnClick" CssClass="imageButton updateDeal" />
                                <asp:LinkButton ID="ResetDealButton" runat="server" Text="Start Over" CssClass="resetDeal" OnClick="ResetDealButton_OnClick"  />
                            </div>
                        </ContentTemplate>
                    </pwc:ExtendedUpdatePanel>
                </div>
            </fieldset>
            
            <fieldset id="PublishOptions" class="printOptions">
                <legend>Publishing Options</legend>
                <div id="PrintOptionsDiv">
                    <div class="printSheetIncludes">
                        <div id="SelectPdfErrorMessage" class="error" style="display:none;">Please choose a sheet to publish<br />(Selling Sheet and/or Market Comparison).</div>
                        <label>Include in Sales Packet:</label><br />
                        <asp:Checkbox ID="IncludeSellingSheetCkb" CssClass="sellingSheet" runat="server" value="includesellingsheeet" TextAlign="Right" ToolTip="Check this box to include the Selling Sheet in your Sales Packet" Text="Selling Sheet" /><br />
                        <asp:CheckBox ID="AddMarketComparisonCkb" CssClass="mktComparison" runat="server" TextAlign="Right" ToolTip="Check this box to include the Market Comparison in your Sales Packet" Text="Market Comparison" /><br />
                        
                        <asp:CheckBox ID="AddCarFaxReportCkb" runat="server" TextAlign="Right" Text="CARFAX Report" /><br />
                        <asp:CheckBox ID="AutoCheckReportCkb" runat="server" TextAlign="Right" Text="AutoCheck Report" /><br />
                    </div>
                    <hr />
                    <asp:CheckBoxList ID="PDFActionsCkbList" runat="server" CssClass="pdfActionsCkbList" RepeatLayout="Flow" RepeatColumns="3" RepeatDirection="Horizontal">
                        <asp:ListItem Value="Print"><span title="Opens a print dialog from within the PDF viewer.">Print&nbsp;&nbsp;</span></asp:ListItem>
                        <asp:ListItem Value="Email"><span title="Opens the default email client from within the PDF viewer.">Email&nbsp;&nbsp;</span></asp:ListItem>
                    </asp:CheckBoxList>
                    
                    <div id="PDFActionsErrorMessage" class="error" style="display:none;">Please choose Print and/or Email.</div>
                    
                     <pwc:ExtendedUpdatePanel ID="PublishCustomerPacketPanel"  UpdateMode="Conditional" runat="server">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ResetDealButton" /> 
                        </Triggers>
                     
                        <ContentTemplate>
                            <div class="formButtons">
                                <asp:LinkButton ID="PrintSheetButton" OnClick="PrintSheetButton_OnClick" OnClientClick="return SalesAccelerator.SubmitPrintRequest(event);" runat="server" Text="Print" CssClass="print-sheet-button"><span>Print</span></asp:LinkButton>
                            </div>
                        </ContentTemplate>
                    </pwc:ExtendedUpdatePanel>
                    <p class="terms" style="font-size:0.8em;clear:both;margin-bottom:1em;">Approval Terms &amp; Conditions: I hereby acknowledge that I have confirmed the accuracy of all of the information about this vehicle contained herein and will use it only as permitted by law. I approve submission of this information for public distribution.</p>
                    <span class="small">Adobe Reader is required to view the published document. <a href="http://get.adobe.com/reader/" onclick="window.open(this.href);return false;">Get Adobe Reader</a></span>

                    <input type="hidden" id="CarfaxUrlHidden" runat="server" class="CarfaxUrlHidden" />
                    <input type="hidden" id="CarfaxUserIdHidden" runat="server" class="CarfaxUrlHidden" />
                    <input type="hidden" id="VinHidden" runat="server" class="CarfaxUrlHidden" />
                    <input type="hidden" id="DealerIdHidden" runat="server" class="AutoCheckUrlHidden" />
                    <input type="hidden" id="AutoCheckUrlHidden" runat="server" class="AutoCheckUrlHidden" />
                    
                </div>
            </fieldset>
        </div>
    </asp:Panel>
    
    <asp:Panel ID="ValueAnalyzerFormPanel" runat="server" DefaultButton="PublishValueAnalyzerButton">
        <div id="WebsitePdfFields">
            <fieldset>
                <legend>MAX Website PDF Options</legend>
                
                <asp:HyperLink ID="ViewCurrentPacketLink" runat="server" CssClass="currentPacketLink" NavigateUrl="" onclick="window.open(this.href);return false;">View current MAX Website PDF packet for this vehicle</asp:HyperLink>
                <asp:TextBox ID="TinyUrl" runat="server" ReadOnly="true" CssClass="tinyUrlText"></asp:TextBox>
                
                <div id="WebsitePdfIncludes">
                    <span>Include on MAX Website PDF:</span>
                    <asp:CheckBoxList ID="WebsitePdfIncludesCkbList" runat="server" CssClass="ckbList" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Flow" TextAlign="right">
                        <asp:ListItem Text="Vehicle Photo"></asp:ListItem>
                        <asp:ListItem Text="Equipment"></asp:ListItem>
                        <asp:ListItem Text="Vehicle Highlights"></asp:ListItem>
                        <asp:ListItem Text="Ad Preview"></asp:ListItem>
                        <asp:ListItem Text="Certified Benefits"></asp:ListItem>
                        <asp:ListItem Text="360&amp;deg; Pricing Analysis"></asp:ListItem>
                        <asp:ListItem Text="Market Listings"></asp:ListItem>
                    </asp:CheckBoxList>
                    <asp:RadioButtonList CssClass="mktListingRadButtons" ID="MarketListingsType" runat="server" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Flow" TextAlign="Right">
                        <asp:ListItem Value="short">Short Version</asp:ListItem>
                        <asp:ListItem Value="long">Long Version</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </fieldset>
            
            <fieldset id="VHRWidgets2">
                <legend>Vehicle History Reports</legend>
                <pwc:ExtendedUpdatePanel ID="VHRExtendedUpdatePanel2" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                            <div id="VHRReports2" class="vhrWidgets moduleContainer">
                                <Controls:VehicleHistoryReport ID="VehicleHistoryReport1" runat="server"/>
                            </div>
                    </ContentTemplate>
                </pwc:ExtendedUpdatePanel>
            </fieldset>
            
            <fieldset class="printOptions">
                <legend>Publishing Options</legend>
                <div id="PublishOptionsDiv">
                    <div id="WebsitePdfAddOns">
                        <label>Include in MAX Website PDF:</label>
                        <asp:CheckBoxList ID="AnalyzerPacketHistoryReports" runat="server" CssClass="ckbList" RepeatDirection="Vertical" RepeatLayout="Flow">
                            <asp:ListItem Text="CARFAX Report" Value="CarFax"></asp:ListItem>
                            <asp:ListItem Text="AutoCheck Report" Value="AutoCheck"></asp:ListItem>
                        </asp:CheckBoxList>
                    </div>
                    <hr />
                    <asp:CheckBoxList ID="VAPDFActionsCkbList" runat="server" CssClass="pdfActionsCkbList" RepeatLayout="Flow" RepeatColumns="3" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Print&nbsp;&nbsp;" Value="Print"></asp:ListItem>
                        <asp:ListItem Text="Post Online&nbsp;&nbsp;" Value="Publish" Selected="True"></asp:ListItem>
                    </asp:CheckBoxList>
                    <div id="PublishActionsErrorMessage" class="error" style="display:none;">Please choose Print and/or Post online.</div>
                    
                     <pwc:ExtendedUpdatePanel ID="ValueAnalyzerPublishingPanel" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            <div class="formButtons">
                                <asp:LinkButton ID="PublishValueAnalyzerButton" OnClick="PublishValueAnalyzerButton_OnClick" OnClientClick="return SalesAccelerator.SubmitPrintRequest(event);" runat="server" Text="Print" CssClass="print-sheet-button"><span>Publish</span></asp:LinkButton>
                            </div>
                        </ContentTemplate>
                    </pwc:ExtendedUpdatePanel>
                    <p class="terms" style="font-size:0.8em;clear:both;margin-bottom:1em;">Approval Terms &amp; Conditions: I hereby acknowledge that I have confirmed the accuracy of all of the information about this vehicle contained herein and will use it only as permitted by law. I approve submission of this information for public distribution.</p>
                    <span class="small">Adobe Reader is required to view the published document. <a href="http://get.adobe.com/reader/" onclick="window.open(this.href);return false;">Get Adobe Reader</a></span>
                </div>
            </fieldset>
        </div>
    </asp:Panel>    
</div>