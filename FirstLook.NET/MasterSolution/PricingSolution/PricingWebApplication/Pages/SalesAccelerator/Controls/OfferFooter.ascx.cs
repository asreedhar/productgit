using System;
using System.Web.UI.WebControls;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls
{
    public class OfferFooter : BaseUserControl
    {
        // Properties
        public Literal CustomerName { get; set; }
        public Literal CustomerEmail { get; set; }
        public Literal CustomerPhoneNumber { get; set; }
        public Literal SalesPersonName { get; set; }
        public Literal DealerName { get; set; }

        public Literal ValidUntilDateLiteral { get; set; }
        public Literal DealerDisclaimerLiteral { get; set; }

        public LayoutKey Layout { get; set; }

        // Section placeholders
        public PlaceHolder CustomerInfo { get; set; }
        public PlaceHolder SalespersonInfo { get; set; }
        public PlaceHolder Disclaimer { get; set; }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // If we won't re-render our output, we can avoid a bunch of work.
            if (!WillReRender())
            {
                return;
            }

            // Load data to the controls.
            LoadControlData();

            // hide/show sections
            ShowHideSections();
        }

        private void LoadControlData()
        {
            var customer = Deal.CustomerPacketOptions.Customer;

            CustomerName.Text = customer.FirstName + " " + customer.LastName; 
            CustomerEmail.Text = customer.Email;
            CustomerPhoneNumber.Text = customer.PhoneNumber;
            SalesPersonName.Text = Deal.CustomerPacketOptions.SalesPerson.Name; 
            DealerName.Text = string.Empty;

            ValidUntilDateLiteral.Text = Deal.ValidUntilDate;
            DealerDisclaimerLiteral.Text = Deal.DealerDisclaimer;
        }

        private void ShowHideSections()        
        {
            // begin by assuming no sections are displayed
            MarkPlaceHoldersNotVisible();

            // Make sections visible as appropriate
            if (!string.IsNullOrEmpty(CustomerName.Text.Trim()) && Layout != LayoutKey.WindowSticker)
            {
                CustomerInfo.Visible = true;
            }

            if (!string.IsNullOrEmpty(SalesPersonName.Text.Trim()) && Layout != LayoutKey.WindowSticker)
            {
                SalespersonInfo.Visible = true;
            }

            if (!string.IsNullOrEmpty(Deal.DealerDisclaimer.Trim()))
            {
                Disclaimer.Visible = true;
            }
        }

        private void MarkPlaceHoldersNotVisible()
        {
            CustomerInfo.Visible = false;
            SalespersonInfo.Visible = false;
            Disclaimer.Visible = false;
        }


        public enum LayoutKey
        {
            SellingSheet, MarketComparison, WindowSticker
        }
 
    }
}