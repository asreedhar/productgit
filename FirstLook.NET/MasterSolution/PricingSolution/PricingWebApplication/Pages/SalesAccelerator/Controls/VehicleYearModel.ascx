<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="VehicleYearModel.ascx.cs" 
    Inherits="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls.VehicleYearModel" EnableViewState="false" %>
<%@ Import Namespace="FirstLook.Pricing.WebApplication.Pages.SalesAccelerator"%>

<div id="VehicleDealerInfo">
<h2><%= Utility.WriteNode(XmlDoc.SelectSingleNode("/vehicleyearmodel/vehiclesummary/yearmodel"))%></h2>
<% if (ShowDealer && XmlDoc.SelectSingleNode("/vehicleyearmodel/dealer/name") != null) { %>
<p class="dealer">Dealership: <strong><%= Utility.WriteNode(XmlDoc.SelectSingleNode("/vehicleyearmodel/dealer/name"))%></strong></p>
<% } %>
</div>
<div id="VehicleLogos">
    <asp:Image ID="CarFaxOneOwnerIconImage" runat="server" Visible="false" AlternateText="" ImageUrl="/pricing/Public/Images/logo_CarFaxOneOwner.gif"  />
    <asp:Image ID="CertifiedProgramLogoImage" runat="server" Visible="false" AlternateText="" CssClass="cpoLogo" />
</div>