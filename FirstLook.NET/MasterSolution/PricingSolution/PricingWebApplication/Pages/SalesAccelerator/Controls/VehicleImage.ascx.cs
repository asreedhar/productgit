using System;
using System.Configuration;
using System.Threading;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Core.PhotoServices;
using FirstLook.Common.Core.Xml;
using FirstLook.Merchandising.DomainModel.Marketing.Commands;
using FirstLook.Merchandising.DomainModel.Marketing.Deal;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator.Controls
{
    public partial class VehicleImage : BaseUserControl, IXmlDoc
    {

        public bool DisplayPhoto 
        { 
            get
            {
                return ToggleDisplayOnSellingSheetCheckBox.Checked;   
            }
        }

        private const string PHOTO_TYPE_ID = "2";

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            var service = Registry.Resolve<IPhotoServices>();

            // If we won't re-render our output, we can avoid a bunch of work.
            if (!WillReRender())
            {
                return;
            }

            var vehicleImage = Deal.GetVehicleImage();

            // Embed the photo manger url as a hidden field.
            PhotoManagerUrl.Value = service.GetPhotoManagerUrl(BusinessUnitObject.Id, InventoryObject.Vin,
                                                                InventoryObject.StockNumber, PhotoManagerContext.MAX);

            // Setup the image dimensions and the source.
            VehiclePhotoImage.ImageUrl = vehicleImage.DisplayUrl;
            VehiclePhotoImage.Height = vehicleImage.DisplayHeight;
            VehiclePhotoImage.Width = vehicleImage.DisplayWidth;

            ToggleDisplayOnSellingSheetCheckBox.Checked = vehicleImage.PhotoPreference.IsDisplayed;

            Deal.SaveXml(vehicleImage);
        }

        protected void HiddenSubmit_Click(object sender, EventArgs e)
        {
            return;
        }

        protected void ToggleDisplayOnSellingSheetCheckBox_CheckChanged(object sender, EventArgs e)
        {
            string userName = Thread.CurrentPrincipal.Identity.Name;

            SetPhotoVehiclePreference command = new SetPhotoVehiclePreference(Handles.OwnerHandle, Handles.VehicleHandle,
                                                                              ToggleDisplayOnSellingSheetCheckBox.Checked, userName);
            AbstractCommand.DoRun(command);
        }

        SerializableXmlDocument IXmlDoc.XmlDoc
        {
            get { return Deal.GetXml(NodeType.vehiclephoto); }
        }
    }

   
}