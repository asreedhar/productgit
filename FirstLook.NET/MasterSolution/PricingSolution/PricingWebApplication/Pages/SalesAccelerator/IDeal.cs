using System;
using FirstLook.Common.Core;
using FirstLook.Pricing.DomainModel.Marketing.MarketAnalysis.Types;

namespace FirstLook.Pricing.WebApplication.Pages.SalesAccelerator
{
    public interface IDeal
    {
        //ILogger Logger { get; set; }
        String OwnerHandle { get; set; }
        string VehicleHandle { get; set; }
        string SearchHandle { get; set; }

        CustomerPacketPublishingOptions CustomerPacketOptions { get; set; }
        ValueAnalyzerPacketPublishingOptions ValueAnalyzerPacketOptions { get; set; }
        
        int OfferPrice { get; set; }
        string ValidUntilDate { get; set; }
        string DealerDisclaimer { get; set; }
        Packets PacketType { get; set; }

        IMarketAnalysisData MarketAnalysis { get; set; }

        void Reset();
        void ResetMarketAnalysis();
    }

    public interface IMarketAnalysisData
    {
        IMarketAnalysisVehiclePreference VehiclePreference { get; set; }
        IMarketAnalysisDealerPreference DealerPreference { get; set; }
        IMarketAnalysisPricingInformation Pricing { get; set; }
    }

    [Serializable]
    internal class MarketAnalysisData : IMarketAnalysisData
    {
        #region Implementation of IMarketAnalysisData

        public IMarketAnalysisVehiclePreference VehiclePreference { get; set; }
        public IMarketAnalysisDealerPreference DealerPreference { get; set; }
        public IMarketAnalysisPricingInformation Pricing { get; set; }

        #endregion
    }
}