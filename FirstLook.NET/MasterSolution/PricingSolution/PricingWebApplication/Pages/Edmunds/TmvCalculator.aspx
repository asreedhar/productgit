<%@ Page Language="C#" AutoEventWireup="true" Inherits="TmvCalculator" Codebehind="TmvCalculator.aspx.cs" Theme="Acquiesce" %>

<%@ OutputCache Location="None" VaryByParam="None" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Edmunds TMV Used Car Appraiser</title>
</head>
<body>
    <form id="form1" runat="server">
        <owc:HedgehogTracking ID="Hedgehog" runat="server" />
        <asp:ScriptManager ID="scriptManager" EnablePartialRendering="true" runat="server" LoadScriptsBeforeUI="false" ScriptPath="/resources/Scripts">
            <Scripts>
                <asp:ScriptReference Path="/resources/Scripts/jQuery/1.2.2/jquery-1.2.2.js" />
                <asp:ScriptReference Path="~/Public/Scripts/Pages/OptionListButton.js" />
                <asp:ScriptReference Path="~/Public/Scripts/Pages/Tmv.js" />
            </Scripts>
        </asp:ScriptManager>
        <div id="container" class="clearfix">            
            <div id="print_control">
                <h6>Show Values</h6>
                <label><input type="checkbox" name="printTradeIn" id="printTradeIn" /> Trade-In</label>
                <label><input type="checkbox" name="printDealerRetail" id="printDealerRetail" /> Dealer Retail</label>
                <asp:CheckBox ID="PrintCertifiedCheckBox" runat="server" Text="Certified" />
                <a id="print" href="javascript:window.print();">print</a>
            </div>
            <asp:Image ID="Image1" ImageUrl="~/Public/Images/tmv_edmunds_logo.gif" CssClass="right" runat="server" />
            <h3>Used Car Appraiser</h3>         
            <p><h4><asp:Label ID="VehicleDescription" runat="server" /></h4>
            <asp:DropDownList ID="TrimDropDown" DataTextField="Value" DataValueField="Id" runat="server" OnDataBound="TrimDropDown_DataBound" OnSelectedIndexChanged="TrimDropDown_Changed" AutoPostBack="true" /></p>
            <p><asp:Label Text="Mileage: " runat="server" ID="MileagePromptLabel" Visible="true"/><asp:Label ID="Mileage" runat="server" /> miles </p>
            <p>Vehicle Condition: <asp:DropDownList ID="ConditionDropDown" DataTextField="Value" DataValueField="Id" OnDataBound="ConditionDropDown_DataBound" OnSelectedIndexChanged="ConditionDropDown_Changed" runat="server" /> Color : <asp:DropDownList ID="ColorDropDown" DataTextField="Value" DataValueField="Id" OnSelectedIndexChanged="ColorDropDown_Changed" OnDataBound="ColorDropDown_DataBound" runat="server" /></p>
            <p>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:Image ID="ExpandOptionsButton" runat="server" ImageUrl="~/Public/Images/expand_arrow.gif" /> 
                    <asp:LinkButton ID="ExpandOptionsLink" runat="server" Text="Update Additional Options" OnClick="ToggleOptions_Click" />
                    <asp:CheckBoxList ID="OptionList"
                        DataTextField="Description"
                        DataValueField="Id"
                        RepeatDirection="Horizontal"
                        RepeatColumns="4"
                        runat="server" 
                        OnDataBound="OptionList_DataBound"
                        OnSelectedIndexChanged="OptionList_SelectedIndexChanged"
                        Visible="false"/>   
                     
                     
                     <script type="text/javascript">

                        Sys.Application.add_load(function () {
                            $create(FirstLook.EdmundsTmv.Website.OptionListButton, {'OptionList':$get('OptionList')}, null, null, $get("ExpandOptionsLink"));
                        })
                     </script> 
                     
                 </ContentTemplate>
            </asp:UpdatePanel>
            </p>
            <asp:Button ID="ShowValues" Visible="true" CssClass="button" Text="Update Pricing Report" runat="server" OnClientClick="return validSelectionCheck();" OnClick="ShowValues_Click" /><br />
            <div id="TmvValuesDiv" visible="false" runat="server">
            <asp:Table ID="TmvValuesTable" CellSpacing="0" CssClass="stripedTable pricingReportTable" runat="server">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell></asp:TableHeaderCell>
                    <asp:TableHeaderCell ColumnSpan="2" HorizontalAlign="Right">
                        <asp:Image ID="TmvValuesImg" ImageUrl="~/Public/Images/tmv_pricing.gif" runat="server" />
                    </asp:TableHeaderCell>
                </asp:TableHeaderRow> 
                <asp:TableHeaderRow ID="HeaderLabelRow">
                    <asp:TableHeaderCell>
                        <asp:Label ID="VehicleDescription_ValueLabel" runat="server"/>
                    </asp:TableHeaderCell>
                    <asp:TableHeaderCell>
                        Trade-In
                    </asp:TableHeaderCell>
                    <asp:TableHeaderCell>
                        Dealer Retail
                    </asp:TableHeaderCell>
                </asp:TableHeaderRow>
                <asp:TableRow ID="BasePriceRow">
                    <asp:TableCell>
                        National Base Price
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="TradeInBase_ValueLabel" runat="server" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="DealerRetailBase_ValueLabel" runat="server" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow ID="EquipmentRow" runat="server">
                    <asp:TableCell>
                       Optional Equipment
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="TradeInEquipmentBase_ValueLabel" runat="server" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="DealerRetailEquipmentBase_ValueLabel" runat="server" />
                    </asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow ID="ColorRow">
                    <asp:TableCell>
                       Color Adjustmemt<br />
                       <asp:Label ID="ColorDescription_ValueLabel" CssClass="smaller" runat="server" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="TradeInColorBase_ValueLabel" runat="server" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="DealerRetailsColorBase_ValueLabel" runat="server" />
                    </asp:TableCell>
                </asp:TableRow>  
                <asp:TableRow ID="RegionalAdjustmentRow">
                    <asp:TableCell>
                       Regional Adjustment<br />
                        <asp:Label ID="RegionDescription_ValueLabel" CssClass="smaller" runat="server" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="TradeInRegionalAdjustmentBase_ValueLabel" runat="server" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="DealerRetailRegionalAdjustmentBase_ValueLabel" runat="server" />
                    </asp:TableCell>
                </asp:TableRow>           
                <asp:TableRow ID="MileageAdjustmentRow">
                    <asp:TableCell>
                       Mileage Adjustment<br />
                       <asp:Label ID="MileageDescription_ValueLabel" CssClass="smaller" runat="server" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="TradeInMileageAdjustmentBase_ValueLabel" runat="server" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="DealerRetailMileageAdjustmentBase_ValueLabel" runat="server" />
                    </asp:TableCell>
                </asp:TableRow>       
                <asp:TableRow ID="ConditionAdjustmentRow">
                    <asp:TableCell>
                       Condition Adjustment<br />
                       <asp:Label ID="ConditionDescription_ValueLabel" CssClass="smaller" runat="server" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="TradeInConditionAdjustmentBase_ValueLabel" runat="server" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="DealerRetailConditionAdjustmentBase_ValueLabel" runat="server" />
                    </asp:TableCell>
                </asp:TableRow>     
                <asp:TableFooterRow CssClass="totalRow" ID="TotalRow">
                    <asp:TableCell>
                           Total <small>(click icon to copy to clipboard)</small>
                    </asp:TableCell>
                    <asp:TableCell>
                            <a title="Copy to Clipboard" href="javascript:copyValue('<%=TradeInTotal_ValueLabel.ClientID%>')"><asp:Image ID="Image3" ImageUrl="~/Public/Images/copy.gif" runat="server" /></a>
                        <asp:Label ID="TradeInTotal_ValueLabel" runat="server" />
                    </asp:TableCell>
                    <asp:TableCell>
                            <a title="Copy to Clipboard" href="javascript:copyValue('<%=DealerRetailTotal_ValueLabel.ClientID%>')"><asp:Image ID="Image4" ImageUrl="~/Public/Images/copy.gif" runat="server" /></a>
                        <asp:Label ID="DealerRetailTotal_ValueLabel" runat="server" />
                    </asp:TableCell>
                </asp:TableFooterRow>                                                                                     
                    <asp:TableFooterRow CssClass="totalRow" ID="CertifiedTotalRow">
                    <asp:TableCell>
                       Certified Used Vehicle
                    </asp:TableCell>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Right">
                            <a title="Copy to Clipboard" href="javascript:copyValue('<%=Certified_ValueLabel.ClientID%>')"><asp:Image ID="Image5" ImageUrl="~/Public/Images/copy.gif" runat="server" /></a>
                        <asp:Label ID="Certified_ValueLabel" runat="server" />
                    </asp:TableCell>
                </asp:TableFooterRow>                                    
            </asp:Table>
        </div>
        </div>
        
    </form>
</body>
</html>
