using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using FirstLook.DomainModel.Oltp;
using FirstLook.Pricing.DomainModel.Edmunds.ObjectModel;

public partial class TmvCalculator : Page
{
    private const string CantProcessVehicleError = "~/ErrorPage.aspx?Error=422";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PopulateEdmundsTmvSelectCriteria();
            PopulateEdmundsOptions();
            if (!GetTmvInputRecord().IsNew)
            {
                PopulateEdmundsTmvValuePanel();
                TmvValuesDiv.Visible = true;
            }
        }
        configureAppearance();
    }

    private void configureAppearance()
    {
        // Show values button display msg
        if (!GetTmvInputRecord().IsNew)
        {
            ShowValues.Text = "Update Pricing Report";
        } else
        {
            ShowValues.Text = "Get Pricing Report";
        }

        // Highlight mileage if its 0
        int mileage;
        if (Int32.TryParse(Mileage.Text, out mileage))
        {
            if (mileage == 0)
            {
                MileagePromptLabel.ForeColor = System.Drawing.Color.Red;
                MileagePromptLabel.Font.Bold = true;
                MileagePromptLabel.Font.Italic = true;
            }
        }

        // toggle option related controls (can't click if there are no options)
        EdmundsTmvOptionPanel panel = GetEdmundsOptionPanel();
        bool optionsExist = panel.Options.Count > 0;
        ExpandOptionsButton.Enabled = optionsExist;
        ExpandOptionsLink.Enabled = optionsExist;
//        OptionList.Visible = optionsExist;

        // only Enable ShowValues button if a valid trim is selected
        if (TrimDropDown.SelectedValue.Length == 0)
        {
            ShowValues.Enabled = false;
            TmvValuesDiv.Visible = false;
        } else
        {
            // other methods are resp for turning the TmvValues Div on 
            ShowValues.Enabled = true;
        }

        // values are not valid without valid color/cond selection
        if (ColorDropDown.SelectedValue.Length == 0 || ConditionDropDown.SelectedValue.Length == 0)
        {
            TmvValuesDiv.Visible = false;
        }


    }

    private EdmundsTmvPanel GetEdmundsTmvPanel()
    {
        EdmundsTmvPanel panel = (EdmundsTmvPanel) Context.Items["EdmundsTmvPanel"];
        if (panel == null)
        {
            panel = EdmundsTmvPanel.GetSearchPanel(GetTmvInputRecord());
            if (panel.ResultsAvailable && panel.Trims.Count > 0)
            {
                Context.Items["EdmundsTmvPanel"] = panel;
            }
            else
            {
                Response.Redirect(CantProcessVehicleError);
            }
        }
        return panel;
    }

    private EdmundsTmvOptionPanel GetEdmundsOptionPanel()
    {
        EdmundsTmvOptionPanel panel;

        int selectedEdumdsStyleId;

        if (Int32.TryParse(TrimDropDown.SelectedValue, out selectedEdumdsStyleId))
        {
            panel = (EdmundsTmvOptionPanel) Context.Items["EdmundsOptionPanel"];
            if (panel == null || panel.EdmundsStyleId != selectedEdumdsStyleId)
            {
                panel = EdmundsTmvOptionPanel.GetEdmundsTmvOptionPanel(selectedEdumdsStyleId);
                Context.Items["EdmundsOptionPanel"] = panel;
            }
        }
        else
        {
            panel = EdmundsTmvOptionPanel.EmptyEdmundsTmvOptionPanel();
        }

        return panel;
    }


    private EdmundsTmvValuesPanel GetEdmundsTmvValuesPanel()
    {
        EdmundsTmvValuesPanel panel = (EdmundsTmvValuesPanel) Context.Items["EdmundsTmvValuesPanel"];
        if (panel == null)
        {
            EdmundsTmvInputRecord input = GetTmvInputRecord();
            panel =
                EdmundsTmvValuesPanel.GetEdmundsValuePanel(input.OwnerHandle,
                                                           ConvertSelectedOptionsToCommaDelimitedString(
                                                               GetTmvInputRecord().Options), input.Trim.Id,
                                                           input.Color.Id,
                                                           input.Condition.Id,
                                                           Convert.ToInt32(Mileage.Text.Replace(",", "")), input.VehicleHandle);
            Context.Items["EdmundsTmvValuesPanel"] = panel;
        }

        return panel;
    }

    private EdmundsTmvInputRecord GetTmvInputRecord()
    {
        EdmundsTmvInputRecord tmvInputRecord = (EdmundsTmvInputRecord) Context.Items["TmvInputRecord"];
        if (tmvInputRecord == null)
        {
            tmvInputRecord = LoadPreferences();
            Context.Items.Add("TmvInputRecord", tmvInputRecord);
        }
        return tmvInputRecord;
    }

    /// <summary>
    /// Try and retrieve existing an EdmundsTmvInputRecord.
    /// </summary>
    /// <returns></returns>
    private EdmundsTmvInputRecord LoadPreferences()
    {
        EdmundsTmvInputRecord tmvInputRecord;

        string oh = Request.QueryString["oh"];
        string vh = Request.QueryString["vh"];

        if (!String.IsNullOrEmpty(oh) && !String.IsNullOrEmpty(vh))
        {
            tmvInputRecord = EdmundsTmvInputRecord.GetInputRecord(oh, vh);
        }
        else
        {
            throw new SystemException(
                "Missing OwnerHandle and/or Vehicle Handle. These are required attributes for the Edmunds TMV Calculator.");
        }
        return tmvInputRecord;
    }

    private void PopulateEdmundsOptions()
    {
        EdmundsTmvOptionPanel panel = GetEdmundsOptionPanel();

        OptionList.DataSource = panel.Options;
        OptionList.DataBind();
    }

    private void PopulateEdmundsTmvSelectCriteria()
    {
        EdmundsTmvPanel panel = GetEdmundsTmvPanel();

        VehicleDescription.Text = panel.VehicleDescription;

        TrimDropDown.DataSource = panel.Trims;
        TrimDropDown.DataBind();

        ConditionDropDown.DataSource = panel.Conditions;
        ConditionDropDown.DataBind();

        ColorDropDown.DataSource = panel.Colors;
        ColorDropDown.DataBind();

        Mileage.Text = String.Format("{0:n0}", panel.Mileage);

        ExpandOptionsButton.Attributes["onclick"] = "document.getElementById('ExpandOptionsLink').click()";
    }

    #region Data Bound Methods

    protected void TrimDropDown_DataBound(object sender, EventArgs e)
    {
        if (!GetTmvInputRecord().IsNew)
        {
            TrimDropDown.SelectedValue = Convert.ToString(GetTmvInputRecord().Trim.Id);
        } 
        
        if (TrimDropDown.Items.Count > 1)
        {
            TrimDropDown.Items.Insert(0, new ListItem("-- Select --", "", true));
        }
    }

    protected void OptionList_DataBound(object sender, EventArgs e)
    {
        if (!GetTmvInputRecord().IsNew)
        {
            foreach (OptionalEquipment option in GetTmvInputRecord().Options)
            {
                foreach (ListItem item in OptionList.Items)
                {
                    if (Convert.ToInt64(item.Value) == option.Id)
                    {
                        item.Selected = true;
                        break;
                    }
                }
            }
            OptionList.Visible = true;
            ExpandOptionsButton.ImageUrl = "~/Public/Images/drop_arrow.gif";
        }
    }

    protected void ConditionDropDown_DataBound(object sender, EventArgs e)
    {
        if (!GetTmvInputRecord().IsNew)
        {
            ConditionDropDown.SelectedValue = Convert.ToString(GetTmvInputRecord().Condition.Id);
        }

        if (ConditionDropDown.Items.Count > 1)
        {
            ConditionDropDown.Items.Insert(0, new ListItem("-- Select --", "", true));
        }
    }

    protected void ColorDropDown_DataBound(object sender, EventArgs e)
    {
        if (!GetTmvInputRecord().IsNew)
        {
            ColorDropDown.SelectedValue = Convert.ToString(GetTmvInputRecord().Color.Id);
        }
        else
        {
            // try and match if we can
            string selectedColor = GetEdmundsTmvPanel().Color;
            foreach (Color color in GetEdmundsTmvPanel().Colors)
            {
                if (color.Value.Equals(selectedColor, StringComparison.CurrentCultureIgnoreCase))
                {
                    ColorDropDown.SelectedValue = Convert.ToString(color.Id);
                }
            }
        }

        if (ColorDropDown.Items.Count > 1)
        {
            ColorDropDown.Items.Insert(0, new ListItem("-- Select --", "", true));
        }

    }

    #endregion

    #region Event Methods

    protected void ToggleOptions_Click(object sender, EventArgs e)
    {
        if (!OptionList.Visible)
        {
            openOptions();
        }
        else
        {
            closeOptions();
        }
    }

    private void openOptions()
    {
        OptionList.Visible = true;
        ExpandOptionsButton.ImageUrl = "~/Public/Images/drop_arrow.gif";
    }

    private void closeOptions()
    {
        OptionList.Visible = false;
        ExpandOptionsButton.ImageUrl = "~/Public/Images/expand_arrow.gif";
    }

    protected void ShowValues_Click(object sender, EventArgs e)
    {
        if (TrimDropDown.SelectedValue.Length > 0 && ColorDropDown.SelectedValue.Length > 0 && ConditionDropDown.SelectedValue.Length > 0)
        {
            TmvValuesDiv.Visible = true;
            LoadComplete += PersistChanges;
        } 

    }

    private void PersistChanges(object sender, EventArgs e)
    {
        LoadComplete -= PersistChanges;

        updateColor();
        updateCondition();
        updateTrim();
        updateSelectedOptions();

        GetTmvInputRecord().Save();

        PopulateEdmundsTmvValuePanel();
    }

    private void PopulateEdmundsTmvValuePanel()
    {
        EdmundsTmvValuesPanel panel = GetEdmundsTmvValuesPanel();

        // calc values
        VehicleDescription_ValueLabel.Text = VehicleDescription.Text;
        // base
        TradeInBase_ValueLabel.Text = String.Format("{0:c0}", panel.BaseValue.TradeInValue);
        DealerRetailBase_ValueLabel.Text = String.Format("{0:c0}", panel.BaseValue.RetailValue);
        // equipment
        TradeInEquipmentBase_ValueLabel.Text = String.Format("{0:c0}", panel.EquipmentTotal.TradeInValue);
        DealerRetailEquipmentBase_ValueLabel.Text = String.Format("{0:c0}", panel.EquipmentTotal.RetailValue);
        PopulateOptionValueRows();

        // color
        TradeInColorBase_ValueLabel.Text = String.Format("{0:c0}", panel.ColorTotal.TradeInValue);
        DealerRetailsColorBase_ValueLabel.Text = String.Format("{0:c0}", panel.ColorTotal.RetailValue);
        ColorDescription_ValueLabel.Text = ColorDropDown.SelectedItem.Text;

        // region
        TradeInRegionalAdjustmentBase_ValueLabel.Text = String.Format("{0:c0}", panel.RegionalTotal.TradeInValue);
        DealerRetailRegionalAdjustmentBase_ValueLabel.Text = String.Format("{0:c0}", panel.RegionalTotal.RetailValue);
        BusinessUnit dealer = Context.Items[BusinessUnit.HttpContextKey] as BusinessUnit;
        if (dealer != null)
        {
            RegionDescription_ValueLabel.Text = "For Zip Code " + dealer.ZipCode;
        }

        // mileage
        TradeInMileageAdjustmentBase_ValueLabel.Text = String.Format("{0:c0}", panel.MileageTotal.TradeInValue);
        DealerRetailMileageAdjustmentBase_ValueLabel.Text = String.Format("{0:c0}", panel.MileageTotal.RetailValue);
        MileageDescription_ValueLabel.Text = Mileage.Text + " miles";

        // condition
        TradeInConditionAdjustmentBase_ValueLabel.Text = String.Format("{0:c0}", panel.ConditionTotal.TradeInValue);
        DealerRetailConditionAdjustmentBase_ValueLabel.Text = String.Format("{0:c0}", panel.ConditionTotal.RetailValue);
        ConditionDescription_ValueLabel.Text = ConditionDropDown.SelectedItem.Text;


        // total
        TradeInTotal_ValueLabel.Text = String.Format("{0:c0}", panel.Total.TradeInValue);
        DealerRetailTotal_ValueLabel.Text = String.Format("{0:c0}", panel.Total.RetailValue);

        // certified
        if (panel.CertifiedTotal != -1)
        {
            Certified_ValueLabel.Text = String.Format("{0:c0}", panel.CertifiedTotal);
        }
        else
        {
            Certified_ValueLabel.Text = "N/A";
        }

        if (panel.IsCertifiedVehicle)
        {
            Certified_ValueLabel.BackColor = System.Drawing.Color.Yellow;
            PrintCertifiedCheckBox.Checked = true;
        }
        else
        {
            DealerRetailTotal_ValueLabel.BackColor = System.Drawing.Color.Yellow;
        }
    }

    /// <summary>
    /// Iterates over current selected options and adds rows to the value table for display.
    /// Appends all rows as child rows under the parent row.
    /// </summary>
    private void PopulateOptionValueRows()
    {
        int equipRowIndex = TmvValuesTable.Rows.GetRowIndex(EquipmentRow);
        EdmundsTmvValuesPanel panel = GetEdmundsTmvValuesPanel();
        foreach (TmvOptionValue value in panel.EquipmentValues)
        {
            TableRow equip1Row = new TableRow();
            equip1Row.ID = "OptionValue_" + value.Id;
            TableCell equip1Cell1 = new TableCell();
            equip1Cell1.CssClass = "smaller";
            equip1Cell1.Text = value.Label;
            TableCell equip1Cell2 = new TableCell();
            equip1Cell2.Text = String.Format("{0:c0}", value.TradeInValue);
            equip1Cell2.CssClass = "smaller";
            TableCell equip1Cell3 = new TableCell();
            equip1Cell3.Text = String.Format("{0:c0}", value.RetailValue);
            equip1Cell3.CssClass = "smaller";

            equip1Row.Cells.Add(equip1Cell1);
            equip1Row.Cells.Add(equip1Cell2);
            equip1Row.Cells.Add(equip1Cell3);
            equip1Row.Visible = true;

            TmvValuesTable.Rows.AddAt(equipRowIndex + 1, equip1Row);
        }
    }


    protected void ColorDropDown_Changed(object sender, EventArgs e)
    {
        updateColor();
    }

    private void updateColor()
    {
        if (ColorDropDown.SelectedValue.Length > 0)
        {
            foreach (Color color in GetEdmundsTmvPanel().Colors)
            {
                if (color.Id == Convert.ToInt32(ColorDropDown.SelectedValue))
                {
                    GetTmvInputRecord().Color = color;
                }
            }
        }
    }

    protected void ConditionDropDown_Changed(object sender, EventArgs e)
    {
        updateCondition();
    }

    private void updateCondition()
    {
        if (ConditionDropDown.SelectedValue.Length > 0)
        {
            foreach (Condition condition in GetEdmundsTmvPanel().Conditions)
            {
                if (condition.Id == Convert.ToInt32(ConditionDropDown.SelectedValue))
                {
                    GetTmvInputRecord().Condition = condition;
                    break;
                }
            }
        }
    }

    protected void TrimDropDown_Changed(object sender, EventArgs e)
    {
        int selectedEdumdsStyleId;

        if (Int32.TryParse(TrimDropDown.SelectedValue, out selectedEdumdsStyleId))
        {
            TmvValuesDiv.Visible = false;
            updateTrim();
            PopulateEdmundsOptions();
        }
        else
        {
            closeOptions();
        }
    }

    private void updateTrim()
    {
        foreach (Trim trim in GetEdmundsTmvPanel().Trims)
        {
            if (trim.Id == Convert.ToInt32(TrimDropDown.SelectedValue))
            {
                GetTmvInputRecord().Trim = trim;
            }
        }
    }

    protected void OptionList_SelectedIndexChanged(object sender, EventArgs e)
    {
        updateSelectedOptions();
    }

    private void updateSelectedOptions()
    {
        List<long> optionIds = new List<long>();

        foreach (ListItem item in OptionList.Items)
        {
            if (item.Selected)
                optionIds.Add(Convert.ToInt64(item.Value));
        }

        // container for unselected options
        List<OptionalEquipment> removed = new List<OptionalEquipment>();
        foreach (OptionalEquipment option in GetTmvInputRecord().Options)
        {
            if (!optionIds.Contains(option.Id))
            {
                removed.Add(option);
            }
        }

        List<OptionalEquipment> toAdd = new List<OptionalEquipment>();

        foreach (long optionId in optionIds)
        {
            if (GetTmvInputRecord().Options.GetItem(optionId) == null)
            {
                toAdd.Add(GetOptionById(optionId, GetEdmundsOptionPanel().Options));
            }
        }

        foreach (OptionalEquipment option in removed)
        {
            GetTmvInputRecord().Options.Remove(option);
        }

        foreach (OptionalEquipment option in toAdd)
        {
            GetTmvInputRecord().Options.Add(option);
        }
    }

    #endregion

    private static OptionalEquipment GetOptionById(long optionId, IEnumerable<OptionalEquipment> options)
    {
        OptionalEquipment result = null;
        foreach (OptionalEquipment option in options)
        {
            if (option.Id == optionId)
            {
                result = option;
                break;
            }
        }
        return result;
    }

    private static string ConvertSelectedOptionsToCommaDelimitedString(IEnumerable<OptionalEquipment> optionCollection)
    {
        StringBuilder sb = new StringBuilder();
        foreach (OptionalEquipment option in optionCollection)
        {
            if (option.Selected)
            {
                sb.Append(option.Id + ",");
            }
        }
        string result = sb.ToString();
        if (result.Length > 0)
        {
            // remove the last comma
            result = result.Remove(result.Length - 1, 1);
        }
        return result;
    }

}