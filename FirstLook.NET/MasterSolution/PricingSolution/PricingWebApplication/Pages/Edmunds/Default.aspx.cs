using System;

namespace FirstLook.Pricing.WebApplication.Pages.Edmunds
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            Response.Redirect("TmvCalculator.aspx?" + Request.QueryString);
        }
    }
}
