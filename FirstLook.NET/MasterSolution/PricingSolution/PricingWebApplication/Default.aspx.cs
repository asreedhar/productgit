using System;
using System.Diagnostics;
using System.Text;
using System.Web;
using FirstLook.Common.Core.Utilities;
using FirstLook.DomainModel.Oltp;
using FirstLook.Pricing.DomainModel.Internet.DataSource;

namespace FirstLook.Pricing.WebApplication
{
    public partial class Default : System.Web.UI.Page
    {
        private const string DealerToken = "DEALER_SYSTEM_COMPONENT";

        private const string InventoryUrl = "~/Pages/Internet/InventoryPricingAnalyzer.aspx";

        private const string VehicleUrl = "~/Pages/Internet/VehiclePricingAnalyzer.aspx";

        private const string SellerUrl = "~/Pages/Internet/ChooseSeller.aspx";

        private const string ErrorUrl = "~/ErrorPage.aspx?Error=410";

        protected void Page_Load(object sender, EventArgs e)
        {
            // pass on knowlegde of coming from max. this is an example of how NOT to do something.

            if (!string.IsNullOrEmpty(Request.QueryString["HALSESSIONID"]))
            {
                if (Request.Cookies["_session_id"] == null)
                {
                    Response.Cookies.Add(new HttpCookie("_session_id", Request.QueryString["HALSESSIONID"]));
                }
            }

            // route the user to the correct page

            bool forbidden = true;

            string token = Request.QueryString["token"];

            if (User.IsInRole("Administrator") || User.IsInRole("AccountRepresentative"))
            {
                if (!string.IsNullOrEmpty(token) && token.Equals(DealerToken))
                {
                    forbidden = EnterDealerTool();
                }
                else
                {
                    forbidden = EnterSellerTool();
                }
            }
            else if (User.IsInRole("User"))
            {
                forbidden = EnterDealerTool();
            }
            else if (User.IsInRole("Dummy"))
            {
                LogDummyUser();
            }

            if (forbidden)
            {
                Response.StatusCode = 403;
                Response.Status = "403 Forbidden";
            }
        }

        private bool EnterDealerTool()
        {
            BusinessUnit dealer = Context.Items[BusinessUnit.HttpContextKey] as BusinessUnit;

            if (dealer != null)
            {
                string pageName = Request.Params["pageName"];
                string ownerHandle = Request.QueryString["oh"];
                string vehicleHandle = Request.QueryString["vh"];
                string searchHandle = null;
                string insertUser = Context.User.Identity.Name;
                string q = Request.QueryString["q"];

                bool hasQuery = !string.IsNullOrEmpty(q);

                bool noPageSpecified = string.IsNullOrEmpty(pageName);

                bool isEdmundsPage = !noPageSpecified && pageName.Contains("Pages/Edmunds");

                bool isJdPowerPage = !noPageSpecified && pageName.Contains("Pages/JDPower");

                bool isMarketPage = !noPageSpecified && pageName.Contains("Pages/Market");

                bool isPreferencePage = !noPageSpecified && pageName.Contains("Pages/Preferences");

                bool isMarketingPreferencePage = !noPageSpecified && (pageName.Contains("Pages/SalesAccelerator/Preferences/Dealer") || pageName.Contains("Pages/MaxMarketing/Preferences/Dealer"));

                if (!string.IsNullOrEmpty(ownerHandle) && !string.IsNullOrEmpty(vehicleHandle))
                {
                    // This code branch is for the Pricing Alert emails that specify both
                    // the owner handle and the vehicle handle and we need to lookup a
                    // search handle so we can take them to the VPA

                    try
                    {
                        searchHandle = new HandleLookupDataSource().FindByOwnerHandleAndVehicleHandle(
                            ownerHandle, vehicleHandle, insertUser);
                    }
                    catch
                    {
                        Response.Redirect(ErrorUrl, true);

                        return false;
                    }
                }
                else
                {
                    // so we're missing the owner handle, vehicle handle or both; lets make
                    // an effort to figure them out ...

                    string inventoryId = Request.Params["inventoryId"];
                    string stockNumber = Request.Params["stockNumber"];
                    string appraisalId = Request.Params["appraisalId"];
                    string vehicleEntityTypeId = Request.Params["vehicleEntityTypeId"];
                    string vehicleEntityId = Request.Params["vehicleEntityId"];                    

                    Owner owner = Owner.GetOwner(dealer.Id);

                    ownerHandle = owner.Handle;

                    if (isEdmundsPage || isJdPowerPage)
                    {
                        // these applications do not require the PING II upgrade and so we
                        // cannot rely on the pricing procedures and have to build the
                        // vehicle handles manually.  this code should be limited to the
                        // database which is the one place that (SHOULD) perform this
                        // translation (so we can change it in one not many locations).

                        if (!string.IsNullOrEmpty(inventoryId))
                        {
                            vehicleHandle = "1" + inventoryId;
                        }
                        else if (!string.IsNullOrEmpty(appraisalId))
                        {
                            vehicleHandle = "2" + appraisalId;
                        }
                        else if (!(string.IsNullOrEmpty(vehicleEntityTypeId) || string.IsNullOrEmpty(vehicleEntityId)))
                        {
                            vehicleHandle = vehicleEntityTypeId + vehicleEntityId;
                        }
                    }
                    else if (isMarketPage)
                    {
                        // the market report does *not* need vehicle or search handles

                        vehicleHandle = null;

                        searchHandle = null;
                    }
                    else // otherwise you are PING II and these lookups will work
                    {
                        HandleLookupDataSource finder = new HandleLookupDataSource();

                        try
                        {
                            if (!string.IsNullOrEmpty(inventoryId))
                            {
                                finder.FindByOwnerHandleAndVehicle(ownerHandle, 1,
                                                                   Int32Helper.ToInt32(inventoryId),
                                                                   insertUser,
                                                                   out vehicleHandle, out searchHandle);
                            }
                            else if (!string.IsNullOrEmpty(stockNumber))
                            {
                                finder.FindByOwnerHandleAndStockNumber(ownerHandle, stockNumber, insertUser,
                                                                       out vehicleHandle, out searchHandle);
                            }
                            else if (!string.IsNullOrEmpty(appraisalId))
                            {
                                finder.FindByOwnerHandleAndVehicle(ownerHandle, 2,
                                                                   Int32Helper.ToInt32(appraisalId), insertUser,
                                                                   out vehicleHandle, out searchHandle);
                            }
                            else if (!(string.IsNullOrEmpty(vehicleEntityTypeId) || string.IsNullOrEmpty(vehicleEntityId)))
                            {
                                finder.FindByOwnerHandleAndVehicle(ownerHandle,
                                                                   Int32Helper.ToInt32(vehicleEntityTypeId),
                                                                   Int32Helper.ToInt32(vehicleEntityId),
                                                                   insertUser,
                                                                   out vehicleHandle, out searchHandle);
                            }
                        }
                        catch (Exception)
                        {
                            Response.Redirect("~/ErrorPage.aspx?Error=423", true);
                        }

                        try
                        {
                            // If we have a query and it is a stockNumber... go directly to the VPA, PM
                            if (hasQuery)
                            {
                                finder.FindByOwnerHandleAndStockNumber(ownerHandle, q, insertUser,
                                                                       out vehicleHandle, out searchHandle);
                            }
                        }
                        catch
                        {
                            /* No Nothing */
                        }
                    }
                }

                string vehicleCatalogId = Request.Params["vehicleCatalogId"];

                string modelYear = Request.Params["modelYear"];

                string page = VehicleUrl;

                if (!string.IsNullOrEmpty(pageName))
                {
                    page = "~/" + pageName;
                }

                StringBuilder sb = new StringBuilder().Append(page).Append("?");

                if (!string.IsNullOrEmpty(vehicleCatalogId) && !string.IsNullOrEmpty(modelYear))
                {
                    sb.Append("vehicleCatalogId=").Append(vehicleCatalogId).Append("&modelYear=").Append(modelYear);
                }
                else
                {
                    if (!string.IsNullOrEmpty(ownerHandle))
                    {
                        sb.Append("oh=").Append(ownerHandle).Append("&");
                    }

                    bool missingVehicleHandle = false, missingSearchHandle = false;

                    if (!string.IsNullOrEmpty(vehicleHandle))
                    {
                        sb.Append("vh=").Append(vehicleHandle).Append("&");
                    }
                    else
                    {
                        missingVehicleHandle = true;
                    }

                    if (!string.IsNullOrEmpty(searchHandle))
                    {
                        sb.Append("sh=").Append(searchHandle).Append("&");
                    }
                    else
                    {
                        missingSearchHandle = true;
                    }

                    bool breakUserExpectations = false; // (*fingers crossed*)

                    if (!isMarketPage && !isPreferencePage && !isMarketingPreferencePage && missingVehicleHandle)
                    {
                        breakUserExpectations = true;
                    }

                    if (!isMarketPage && !isPreferencePage && !isMarketingPreferencePage && !isEdmundsPage && missingSearchHandle)
                    {
                        breakUserExpectations = true;
                    }

                    if (breakUserExpectations)
                    {
                        if (hasQuery)
                        {
                            sb.Append("&q=").Append(Uri.EscapeDataString(q));
                        }

                        sb.Replace(page, InventoryUrl); 
                    }
                }

                if (!string.IsNullOrEmpty(Request.QueryString["popup"]))
                {
                    sb.Append("&popup=true");
                }

                if (!string.IsNullOrEmpty(Request.QueryString["referrer"]))
                {
                    sb.Append("&referrer=").Append(Request.QueryString["referrer"]);
                }

                string url = sb.ToString();

                Response.Redirect(url, true);

                return false;
            }

            return true;
        }

        private bool EnterSellerTool()
        {
            Response.Redirect(SellerUrl, true);

            return false;
        }

        private void LogDummyUser()
        {
            const string sourceName = "Pricing";

            const string logName = "ApplicationLog";

            if (!EventLog.SourceExists(sourceName))
            {
                EventLog.CreateEventSource(sourceName, logName);
            }

            EventLog log = new EventLog();
            log.Source = sourceName;
            log.WriteEntry(string.Format("User {0} is configured with MemberType of 'Dummy'", User.Identity.Name), EventLogEntryType.Error);
        }
    }
}