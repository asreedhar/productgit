﻿define(function (require) {

    function Gauge(sliderText) {

        var _self = this;
        var _sliderText = sliderText;
        var _isDragDealerCreated;
        this.SliderMoved = [];

        var MakePowerZone = function (graphic_resource, _r) {
            var suggested_area = _r.path(graphic_resource.power_zone);
            suggested_area.attr({
                "fill": "90-#36b449-#a2dcaa",
                "stroke": "#2c9b3c",
                "clip": graphic_resource.base_path
            });

            return suggested_area;
        };
        var GetX = function (min_market, m, y) {
            return ((y - min_market) / m) / 100;
        };
        var GetY = function (min_market, m, x) {
            return (x * m) * 100 + min_market;
        };
        var MakeGraphic = function (graphic_resource, _r, _o) {
            var path = graphic_resource.base_path,
            shadow = _r.path(path),
            bar = _r.path(path),
            set = _r.set();

            set.push(shadow, bar);
            set.push(bar);

            shadow.attr({
                "stroke": "none",
                "fill": "90-#d5d9e2-#adb0b5-#d5d9e2",
                "translation": "2,3"
            });
            shadow.scale(1.005, 1.005);

            bar.attr({
                "fill": "90-#fcb912-#fdeca6",
                "stroke": "#e0c400"
            });

            return set;
        };
        var MakeNamedMarker = function (graphic_resource, pct, label, amt, box, _r) {
            var width = box.getBBox().width,
            x_angle_length = graphic_resource.offset.angle_offset,
            left_offset = graphic_resource.offset.left,
            x = Math.round((width - (x_angle_length * 2)) * pct) + left_offset,
            path = ["M", x, " ", (graphic_resource.offset.top - 1), " L", x, " ", (graphic_resource.offset.top + graphic_resource.height + 4)].join(""),
            bar = _r.path(path).attr({
                "stroke": "#a39568",
                "stroke-width": 2
            }),
            t1 = _r.text(x, graphic_resource.offset.top + graphic_resource.height + 11, label).attr({
                "font-size": 10,
                "fill": "#737373"
            }),
            t2 = _r.text(x, graphic_resource.offset.top + graphic_resource.height + 22, "$" + _.comma(amt)).attr({
                "font-size": 12,
                "font-weight": "bold",
                "fill": "#41455e"
            });
            s = _r.set();
            s.push(bar, t1, t2);

            return s;
        };
        var MakeMakerBars = function (graphic_resource, num, box, _r, color, min, max, shrink) {
            var x_angle_length = graphic_resource.offset.angle_offset,
            x_offset = graphic_resource.offset.left,
            width = box.getBBox().width - (2 * x_angle_length),
            set = _r.set(),
            values,
            i = 0,
            last = 0;

            min = min !== 0 ? min || -Infinity : 0;
            max = max !== 0 ? max || Infinity : 0;

            function get_marker_path(pct, y1, y2) {
                if (pct === Infinity) pct = 100;
                var x = pct * width + x_angle_length;
                return {
                    x: x,
                    y1: y1,
                    y2: y2
                };
            }

            function toLinePath(obj) {
                return ["M", obj.x, " ", obj.y1, " L", obj.x, " ", obj.y2].join("");
            }

            for (i = 0; i <= num; i++) {
                values = get_marker_path(i / num, graphic_resource.offset.top, graphic_resource.height + graphic_resource.offset.top);
                if (values.x >= min && values.x <= max) {
                    last = i;
                    set.push(_r.path(toLinePath(values)).attr({
                        "stroke": color
                    }));
                }
            }

            if (shrink) {
                var slope = 0.5;
                var off = 0;
                while (true) {
                    values = get_marker_path(++last / num, graphic_resource.offset.top, graphic_resource.height + graphic_resource.offset.top);
                    off = slope * (values.x - max);
                    values.y1 = graphic_resource.offset.top + off;
                    values.y2 = graphic_resource.height + graphic_resource.offset.top - off;
                    if (values.y1 >= values.y2) {
                        break;
                    }
                    set.push(_r.path(toLinePath(values)).attr({
                        "stroke": color
                    }));
                }
            }

            return set;
        };
        var SetArea = function (graphic_resource, green_bars, suggested_area, start, end, avg, min, max, _r, b) {
            var old_path = suggested_area.attr("path") + "",
            // To String Hack
            new_path = old_path,
            x_angle_length = graphic_resource.offset.angle_offset,
            left_offset = graphic_resource.offset.left,
            width = b.getBBox().width - (2 * x_angle_length),
            path_start = width * (avg * (start * 0.01) - min) / (max - min) + left_offset,
            path_end = width * (avg * (end * 0.01) - min) / (max - min) + left_offset;

            if (path_start < x_angle_length) path_start = x_angle_length; // Min is the low point of the slider, 
            if (path_end > width) path_end = width; // Max is the high point of the slider,
            if (path_end < 0) {
                path_start = path_end = 0;
                suggested_area.hide();
            }
            new_path = graphic_resource.update_start(new_path, path_start);
            new_path = graphic_resource.update_end(new_path, path_end);

            if (green_bars.remove) green_bars.remove();
            green_bars = MakeMakerBars(graphic_resource, 40, b, _r, "#2c9b3c", path_start, path_end, graphic_resource.should_shrink);

            suggested_area.attr({
                "path": new_path
            });
        };

        this.Create = function (data) {

            this.Destroy();


            var sliderContainer = $("#avg_graphic", $(document.getElementById("ping_overview"))).get(0);
            var r = new Raphael(sliderContainer, 450, 96);
            var o = $(sliderContainer).offset();
            var b;
            var min_mark;
            var avg_mark;
            var max_mark;
            var min_market = data.Pricing.MarketPrice.Minimum;
            var avg_market = data.Pricing.MarketPrice.Average;
            var max_market = data.Pricing.MarketPrice.Maximum;
            var range_market = max_market - min_market;
            var m = range_market / 100;
            var green_bars = {};
            var width;

            var path_resource = {
                skinny: {
                    base_path: "M0 37 L504 37 L504 57 L0 57 Z",
                    power_zone: "M0 37 L50 37 L50 57 L50 57 L0 57 Z",
                    update_start: function (path, point) {
                        point = Math.round(point); // Round to simplify parsing, PM
                        return path.replace(/(\w)(\d+)([\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?)(\w)(\d+)([\,|\s]\d+[\s|\,]?)/, "$1" + point + "$3$4" + point + "$6");
                    },
                    update_end: function (path, point) {
                        point = Math.round(point); // Round to simplify parsing, PM
                        return path.replace(/(\w\d+[\,|\s]\d+[\s|\,]?\w)(\d+)([\,|\s]\d+[\s|\,]?\w)(\d+)([\,|\s]\d+[\s|\,]?\w)(\d+)([\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?)/, "$1" + point + "$3" + (point) + "$5" + point + "$7");
                    },
                    height: 20,
                    offset: {
                        top: 37,
                        left: 27,
                        angle_offset: 27
                    },
                    should_shrink: false,
                    handle: "", //"M6 36 L12 43 L12 60 L0 60 L0 43 Z",
                    handle_disabled: "", //"M0 46 L6 60 L12 46 L6 33 Z",
                    handle_width: 20,
                    warning_text: 20
                },
                fat: {
                    base_path: "M27 36 L444 36 L417 63 L0 63 Z",
                    power_zone: "M0 36 L50 36 L85 49 L50 63 L0 63 Z",
                    update_start: function (path, point) {
                        point = Math.round(point); // Round to simplify parsing, PM
                        return path.replace(/(\w)(\d+)([\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?)(\w)(\d+)([\,|\s]\d+[\s|\,]?)/, "$1" + point + "$3$4" + point + "$6");
                    },
                    update_end: function (path, point) {
                        point = Math.round(point); // Round to simplify parsing, PM
                        return path.replace(/(\w\d+[\,|\s]\d+[\s|\,]?\w)(\d+)([\,|\s]\d+[\s|\,]?\w)(\d+)([\,|\s]\d+[\s|\,]?\w)(\d+)([\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?)/, "$1" + point + "$3" + (point + 30) + "$5" + point + "$7");
                    },
                    height: 27,
                    offset: {
                        top: 36,
                        left: 27,
                        angle_offset: 27
                    },
                    should_shrink: true,
                    handle: "", //"M6 30 L12 37 L12 64 L0 64 L0 37 Z",
                    handle_message: "APPRAISAL VALUE",
                    handle_disabled: "", //"M0 49 L6 67 L12 49 L6 32 Z",
                    handle_width: 20,
                    warning_text: 54
                }
            },
            graphic_resource = path_resource.fat,
            //TODO update for MMP
            hasSufficentListings = data.Pricing.NumComparableListings >= 5,
            handle = {},
            handle_text = {},
            enabled = true;

            b = MakeGraphic(graphic_resource, r, o);
            MakeMakerBars(graphic_resource, 40, b, r, "#f7ae09");

            if (hasSufficentListings) {
                SetArea(graphic_resource, green_bars, MakePowerZone(graphic_resource, r), data.Pricing.PctMarketAvg.Minimum,
                        data.Pricing.PctMarketAvg.Maximum, avg_market, min_market, max_market, r, b);
                if (min_market < max_market) {
                    min_mark = MakeNamedMarker(graphic_resource, 0.0, "LOW", min_market, b, r);
                    avg_mark = MakeNamedMarker(graphic_resource, GetX(min_market, m, avg_market), "AVG.", avg_market, b, r);
                    max_mark = MakeNamedMarker(graphic_resource, 1.0, "HIGH", max_market, b, r);
                }
            }

            function show_bubble() { }
            function hide_bubble() { }
            function format_comma(value) { }

            if (hasSufficentListings && (min_market < max_market)) {
                // dragdealer maths
                var mp_low = data.Pricing.MarketPrice.Minimum,
                    mp_high = data.Pricing.MarketPrice.Maximum,
                    mp_range = (mp_high - mp_low),
                // this represents the total area including the "bleed" area outside the low and high, as a ratio
                    bleed_ratio = (1 / 0.875),
                    dd_range = mp_range * bleed_ratio,
                    half_bleed_diff = (dd_range - mp_range) / 2.0,
                    dd_low = mp_low - half_bleed_diff,
                    dd_high = mp_high + half_bleed_diff;
                // trial-and-error tweaks
                bleed_ratio = 1 / 0.880;
                dd_range = mp_range * bleed_ratio;
                _self.dd_low = dd_low;
                _self.dd_range = dd_range;
                $('#gauge_html_parts').show();

            } else { // NOT hasSufficentListings
                $('#gauge_html_parts').hide();

                // render warning text.
                Gauge._displayFewResultsWarningMessage(b, r);
                if (!_self.dragdealer) {
                    _isDragDealerCreated = false;
                }
            }

            // dragdealer interactive ui (declare once and re-use)

            if (!_self.dragdealer || !_isDragDealerCreated) {
                _self.price_from_position = function (x) {
                    var raw_price = _self.dd_low + x * _self.dd_range;
                    return Math.round(raw_price / 10) * 10 - 1; // round to one dollar less than nearest ten dollar increment
                }
                _self.position_from_price = function (price) {
                    return (price - _self.dd_low) / _self.dd_range;
                }
                _self.update_handle_label_position = function (dragdealer_obj, x) {
                    var label_offset = x * dragdealer_obj.bounds.xRange;
                    var label_width_left = $('#container .slider_value_display .slider_value').width();
                    var label_width_right = $('#container .slider_value_display label').width() + 5/*via layout.css*/;
                    if (label_offset < label_width_left)
                        label_offset += (label_width_left - label_offset);
                    if (label_offset > (dragdealer_obj.bounds.xRange - label_width_right))
                        label_offset -= (label_offset - (dragdealer_obj.bounds.xRange - label_width_right));
                    $('#container .slider_value_display').css({
                        'left': label_offset + 'px'
                    });
                }
                _self.dragdealer = new Dragdealer($('.dragdealer#gauge')[0], { // $('#gauge .dragdealer')[0], {
                    slide: false,
                    speed: 100, // speed must be 100 if slide is false for the setValue() function to work, for some reason
                    animationCallback: function (x, y) {
                        if (!this.dragging) return; // suppress normal behavior if value was changed via dragdealer API
                        _self.update_handle_label_position(this, x);
                        for (var i = 0; i < _self.SliderMoved.length; i++) {
                            _self.SliderMoved[i](_self.price_from_position(x));
                        }
                    }
                });
                // public
                _self.update_slider = function (price) {

                    var x = _self.position_from_price(price);
                    _self.dragdealer.setValue(x);
                    _self.update_handle_label_position(_self.dragdealer, x);

                }
                _self.enable_slider = function () {

                    _self.dragdealer.enable();

                };
                _self.disable_slider = function () {

                    _self.dragdealer.disable();

                };
                // handle label text
                $('#container .slider_value_label').text(_sliderText);
                // click events - css support
                $('#container .dragdealer .handle').mousedown(function () {
                    $(this).addClass('dragging');
                });
                $(document).mouseup(function () {
                    $('#container .dragdealer .handle').removeClass('dragging');
                });

                if (hasSufficentListings) {
                    _isDragDealerCreated = true;
                }

            }

        };

        this.Destroy = function () {
            // cleanup
            $('#avg_graphic > *').remove();
            $('#gauge_html_parts').hide();
        }
    };

    Gauge._displayFewResultsWarningMessage = function (b, _r) {
        var t = _r.text(230, 15, "There is a limited number of cars in the search results.\nReview Market Listings below or increase Search Radius.");
        t.attr({
            'font-family': 'arial',
            'font-weight': 800,
            'font-size': 14,
            'fill': "#ad8c56"
        });
    };

    return Gauge;
});
