﻿define(function (require) {

    // IMPORTS
    var ko = require("Globals/knockout-2.1.0");
    var Gauge = require("ViewModels/Gauge");
    var Internet = require("Models/Internet");
    var SearchTypeDto = require("Models/SearchTypeDto");
    var SearchArgumentsDto = require("Models/SearchArgumentsDto");
    var SortColumnDto = require("Models/SortColumnDto");
    var MarketListingsFetchArgumentsDto = require("Models/MarketListingsFetchArgumentsDto");
    var SearchUpdateArgumentsDto = require("Models/SearchUpdateArgumentsDto");
    var SearchDto = require("Models/SearchDto");
    var MarketPricingListFetchArgumentsDto = require("Models/MarketPricingListFetchArgumentsDto");
    var VehiclePricingDecisionInputDto = require("Models/VehiclePricingDecisionInputDto");
    var MysteryShoppingParametersDto = require("Models/MysteryShoppingParametersDto");


    // PRIVATE CLASSES
    function ListOption(content, id) {
        this.content = content;
        this.id = id;
    }

    // PUBLIC CLASS
    function ViewModel(initArgs) {

        // PRIVATE VARIABLES
        var _self = this;
        this.isPricing = initArgs.isPricing;
        var _internet = new Internet();
        var _savedSearch = null;
        var _gauge = new Gauge(initArgs.gaugeTitle);
        var _isOverallTabVisible = true;
        var _showListings = false;
        var _needNewListingsSearch = true;
        var _gaugeReadyEventHandlers = [];
        var _optimalPrice = null;

        // when is the gauge ready?
        this.searchReady = ko.observable();
        this.estimatedReconReady = ko.observable();
        this.targetGrossProfitReady = ko.observable();
        this.mysteryShoppingUrlsReady = ko.observable();

        this.gaugeReady = ko.observable();

        // Initiaize un-specified numeric values to reasonable defaults.
        if (_.isUndefined(initArgs.estimated_recon) || _.isNull(initArgs.estimated_recon)) {
            initArgs.estimated_recon = 0;
        }

        if (_.isUndefined(initArgs.appraisal_value) || _.isNull(initArgs.appraisal_value)) {
            initArgs.appraisal_value = 0;
        }

        if (_.isUndefined(initArgs.target_gross_profit) || _.isNull(initArgs.target_gross_profit)) {
            initArgs.target_gross_profit = 0;
        }

        if (!_.isUndefined(initArgs.GaugeReadyHandler) && !_.isNull(initArgs.GaugeReadyHandler)) {
            _gaugeReadyEventHandlers.push(initArgs.GaugeReadyHandler);
        }

        function callBackGaugeReadyEventHandlers() {

            // don't raise this multiple times
            if (_self.gaugeReady() == true) {
                return;
            }

            if (_self.searchReady() && _self.estimatedReconReady() && _self.targetGrossProfitReady() && _self.mysteryShoppingUrlsReady()) {
                _self.gaugeReady(true);

                for (var i = 0; i < _gaugeReadyEventHandlers.length; i++) {
                    _gaugeReadyEventHandlers[i]();
                }
            }

        }

        _self.format_money = function (intVal, def) {
            if (typeof def === 'undefined')
                def = '$0';
            if (intVal >= 0)
                return _.moneyOrSymbol(intVal, def);
            else
                return '(' + _.moneyOrSymbol(Math.abs(intVal), def) + ')';
        };
        _self.parse_money = function (moneyStr) {
            if (moneyStr == 'N/A')
                return 0;
            else
                return (
                    (moneyStr.indexOf("-") <= 1 && moneyStr.indexOf("-") >= 0) ||
                        (moneyStr.indexOf("(") == 0 && moneyStr.indexOf(")") == moneyStr.length - 1)) ?
                    -1 * _.toInt(moneyStr) :
                    _.toInt(moneyStr);
        };
        _self.format_number = function (intVal, def) {
            if (typeof def === 'undefined')
                def = '0';
            if (_.isUndefined(intVal)) {
                return def;
            }
            if (intVal >= 0)
                return _.comma(intVal);
            else
                return '(' + _.comma(Math.abs(intVal)) + ')';
        };

        _self.searchReady.subscribe(callBackGaugeReadyEventHandlers);
        _self.estimatedReconReady.subscribe(callBackGaugeReadyEventHandlers);
        _self.targetGrossProfitReady.subscribe(callBackGaugeReadyEventHandlers);
        _self.mysteryShoppingUrlsReady.subscribe(callBackGaugeReadyEventHandlers);

        function bindSearchCheckBoxes(searchResultsDto) {
            _self.onlyCertified(searchResultsDto.Search.MatchCertified);
            _self.onlyWhite(searchResultsDto.Search.MatchColor);
            //_self.onlyNextYear(searchResultsDto.Search.NextYear);
            //_self.onlyPreviousYear(searchResultsDto.Search.PreviousYear);
        }

        function bindHighMileage(searchResultsDto) {
            var mileages = searchResultsDto.Search.Mileages;

            if (searchResultsDto.Search.HighMileage != null) {
                //round to nearest 10's place. The Highmileage is stored as HighMileage - 1 in the database.
                roundedValue = Math.round(searchResultsDto.Search.HighMileage.Value / 10) * 10
            }
            else {
                roundedValue = null;
            }

            var highMileage = [];

            var selectedHighMileage = null;
            for (var i = 0; i < mileages.length; i++) {
                var listOption = new ListOption(_.comma(mileages[i].Value), mileages[i].Value);
                highMileage.push(listOption);
                if (roundedValue != null && roundedValue == mileages[i].Value) {
                    selectedHighMileage = listOption;
                }
            }

            var listOption = new ListOption("Unlimited", null);
            highMileage.push(listOption);
            if (selectedHighMileage == null) {
                selectedHighMileage = listOption;
            }

            _self.highMileage(highMileage);
            _self.selectedHighMileage(selectedHighMileage);
        }

        function bindLowMileage(searchResultsDto) {
            var mileages = searchResultsDto.Search.Mileages;
            var lowMileage = [];

            var roundedValue = searchResultsDto.Search.LowMileage.Value;

            var selectedLowMileage;
            for (var i = 0; i < mileages.length; i++) {
                var listOption = new ListOption(_.comma(mileages[i].Value), mileages[i].Value);
                lowMileage.push(listOption);
                if (roundedValue == mileages[i].Value) {
                    selectedLowMileage = listOption;
                }
            }

            _self.lowMileage(lowMileage);
            _self.selectedLowMileage(selectedLowMileage);
        }

        function bindSearchDistance(searchResultsDto) {

            var distances = searchResultsDto.Search.Distances;
            var withinMileage = [];

            var selectedDistance;
            for (var i = 0; i < distances.length; i++) {
                var listOption = new ListOption(_.comma(distances[i].Value) + "mi", distances[i].Id);
                withinMileage.push(listOption);
                if (searchResultsDto.Search.Distance.Id == distances[i].Id) {
                    selectedDistance = listOption;
                }
            }

            _self.withinMileage(withinMileage);
            _self.selectedWithinMileage(selectedDistance);
        }

        //Appraisal Value
        this.appraisalValue = ko.observable(initArgs.appraisal_value);
        this.appraisalValueFormatted = ko.computed({
            read: function () {
                return _self.format_number(_self.appraisalValue());
            },
            write: function (value) {
                _self.setAppraisalValue(_self.parse_money(value));
            },
            owner: this
        });

        this.currentAppraisalValue = ko.observable(initArgs.appraisal_value);
        this.currentAppraisalValueFormatted = ko.computed({
            read: function () {
                return _self.format_money(_self.currentAppraisalValue(), "N/A");
            }
        });

        this.setAppraisalValue = function (value) {
            // Appraisal Value+Estimated Recon+Target Gross=Target Selling
            _self.appraisalValue(value);
            _self.internetPrice(value + _self.targetGrossProfit() + _self.estimatedRecon());
        };

        //Appraisal Target Profit
        this.targetGrossProfit = ko.observable(initArgs.target_gross_profit);
        this.targetGrossProfitFormatted = ko.computed({
            read: function () {
                return _self.format_number(_self.targetGrossProfit());
            },
            write: function (value) {
                _self.targetGrossProfit(_self.parse_money(value));
                _gauge.update_slider(_self.targetSellingPrice());
            }
            //            ,owner: this
        });
        // when the target gross changes, update the slider.
        this.targetGrossProfit.subscribe(function () {
            _gauge.update_slider(_self.targetSellingPrice());
        });

        //Appraisal Recon
        this.estimatedRecon = ko.observable(initArgs.estimated_recon);
        this.estimatedReconFormatted = ko.computed({
            read: function () {
                return _self.format_number(_self.estimatedRecon());
            },
            write: function (value) {
                _self.estimatedRecon(_self.parse_money(value));
                _gauge.update_slider(_self.targetSellingPrice());
            },
            owner: this
        });

        //Appraisal Selling price
        this.targetSellingPrice = ko.computed({
            read: function () {
                return parseInt(_self.appraisalValue()) + parseInt(_self.targetGrossProfit()) + parseInt(_self.estimatedRecon());
            }
        });
        this.targetSellingPriceFormatted = ko.computed(function () {
            return _self.format_money(_self.targetSellingPrice());
        });

        //Internet Price
        this.internetPrice = ko.observable(initArgs.internet_price);
        this.internetPriceFormatted = ko.computed({
            read: function () {
                return _self.format_money(_self.internetPrice());
            },
            write: function (value) {
                _self.internetPrice(_self.parse_money(value));
            }
        });

        // "price" context-agnostic
        this.setPrice = function (value) {
            if (_self.isPricing) {
                _self.internetPrice(value);
            } else {
                // Appraisal Value+Estimated Recon+Target Gross=Target Selling
                _self.setAppraisalValue(value - _self.targetGrossProfit() - _self.estimatedRecon());
            }

            // sync prices
            _self.syncPrices();
        };
        this.getPrice = ko.computed(function () {
            if (_self.isPricing) return _self.internetPrice();
            return _self.targetSellingPrice();
        });
        this.getPriceFormatted = ko.computed(function () {
            return _self.format_money(_self.getPrice());
        });

        _self.currentPrice = ko.observable(initArgs.internet_price);
        _self.currentPriceFormatted = ko.computed(function () {
            return _self.format_money(_self.currentPrice());
        });

        _self.hide_potential_gross_profit = ko.computed(function () {
            return (_self.internetPrice() == 0);
        });

        // Market average

        // the actual average price
        this.marketAveragePrice = ko.observable();

        // the percent
        this.marketAverageFormatted = ko.computed({
            read: function () {
                var percent = Math.round((_self.getPrice() / _self.marketAveragePrice()) * 100);
                //                alert(percent);
                return _.percentOrSymbol(percent, "0%");
            },
            write: function (value) {
                value = parseFloat(value.replace('%', ''));
                // calculate the price
                var newPrice = Math.round(_self.marketAveragePrice() * (value / 100));
                //alert(newPrice);
                _self.setPrice(newPrice);
            }
        });



        //Unit Cost
        this.unitCost = ko.observable(initArgs.unit_cost);
        this.unitCostFormatted = ko.computed(function () {
            return _self.format_money(_self.unitCost());
        });

        //gross profit
        this.grossProfit = ko.computed(function () {
            return _self.internetPrice() - _self.unitCost();
        });
        this.grossProfitFormatted = ko.computed(function () {
            return _self.format_money(_self.grossProfit());
        });


        // KNOCKOUT VARIABLES (PUBLIC)
        this.listingsInclude = ko.observable();
        this.averageMileage = ko.observable();
        this.currentMileage = ko.observable();
        this.marketListings = ko.observable();
        this.marketDaysSupply = ko.observable();

        this.overallResultsText = ko.observable();
        this.precisionResultsText = ko.observable();
        this.overallButtonValue = ko.observable();
        this.precisionButtonValue = ko.observable();
        this.overallComparableUnits = ko.observable();
        this.precisionComparableUnits = ko.observable();
        this.overallTabVisible = ko.observable(true);
        this.precisionTabVisible = ko.observable(false);
        this.loading = ko.observable("pointless_design cf");
        this.showLoading = ko.observable(false);
        this.showMarketLoading = ko.observable(false);

        this.marketListingsData = ko.observableArray([]);

        this.autoTraderUrl = ko.observable();
        this.carsDotComUrl = ko.observable();
        this.carSoupUrl = ko.observable();


        //
        // Ranking/Scoring variables - they all start with marketPricingList
        //

        // These are the base arrays which are calculated from the market pricing list data. They do not include
        // this vehicle's price. They are recalculated when the pricing list changes.
        this.marketPricingList = ko.observableArray([]); // the priced listings used for ranking
        this.marketPricingListDistinct = ko.observableArray();
        this.marketPricingListScores = ko.observableArray();

        // These are based on the original arrays. These are recalculated when price changes. 
        // They take into account (and include) the current price in the price list, allowing us to easily score
        // the current vehicle at its current price against all other prices.
        this.marketPricingList_Inclusive = ko.observableArray([]);
        this.marketPricingListDistinct_Inclusive = ko.observableArray([]);
        this.marketPricingListScores_Inclusive = ko.observableArray([]);

        // the value of our current score 
        this.marketPricingListScore = ko.observable();

        // the index of our current price in marketPricingListDistinct_Inclusive. Useful when it's time to change the price.
        this.marketPricingListDistinctIndex = ko.observable();

        this.searchType = ko.observable(); // 1=overall, 4=precision, SearchType.Overall, SearchType.Precision

        this.withinMileage = ko.observableArray([]);
        this.selectedWithinMileage = ko.observable();
        this.selectedLowMileage = ko.observable();
        this.selectedHighMileage = ko.observable();
        this.lowMileage = ko.observableArray([]);
        this.highMileage = ko.observableArray([]);
        this.generatedBy = ko.observable();
        this.searchDate = ko.observable();
        this.overallTabClass = ko.observable("current");
        this.precisionTabClass = ko.observable("tab_box Static_1");
        this.onlyCertified = ko.observable(false);
        this.onlyWhite = ko.observable(false);
       // this.onlyPreviousYear = ko.observable(false);
        //this.onlyCurrentYear = ko.observable(true);
       // this.onlyNextYear = ko.observable(false);
       // this.prev_year = ko.observable(false);
       // this.current_year = ko.observable(false);
       // this.next_year = ko.observable(false);
        this.viewListingsClass = ko.observable("hidden");
        this.viewListingsText = ko.observable("click to view listings");
        this.searchCheckBoxesStyle = ko.observable('visibility:hidden');


        // Find the first value in the sorted array that is > the specified value.
        this.findFirstLargerValue = function (value, values) {

            if (values.length == 0) {
                //                alert('empty values array!');
                return null;
            }

            var length = values.length;
            var myValue = value;

            for (var i = 0; i <= length; i++) {
                if (i == length) {
                    return { index: length - 1, value: values[i - 1] }; 
                }
                var nextValue = values[i];

                if (nextValue > myValue) {
                    return { index: i, value: nextValue };
                }
            }
            //            alert('no values are > the supplied value.');
            return null;
        };


        this.fireAppraisalValuesChanged = function () {
            var values = {
                appraisalValue: _self.appraisalValue(),   // newValue
                estimatedRecon: _self.estimatedRecon(),
                targetGrossProfit: _self.targetGrossProfit()
            };

            GaugeComponent.fireAppraisalValuesChanged(values);
        };

        //this updates GaugeComponent API
        this.appraisalValue.subscribe(_self.fireAppraisalValuesChanged);
        this.estimatedRecon.subscribe(_self.fireAppraisalValuesChanged);
        this.targetGrossProfit.subscribe(_self.fireAppraisalValuesChanged);

        this.fireMysteryShoppingUrlsChanged = function () {
            var values = {
                autoTraderUrl: _self.autoTraderUrl(),
                carsDotComUrl: _self.carsDotComUrl(),
                carSoupUrl: _self.carSoupUrl()
            };

            GaugeComponent.fireMysteryShoppingUrlsChanged(values);
        };

        //this updates GaugeComponent API
        this.autoTraderUrl.subscribe(_self.fireMysteryShoppingUrlsChanged);
        this.carsDotComUrl.subscribe(_self.fireMysteryShoppingUrlsChanged);
        this.carSoupUrl.subscribe(_self.fireMysteryShoppingUrlsChanged);

        // WEB SERVICE EVENT HANDLERS (PRIVATE)
        var internet_searchCompleted = function (args) {
            var searchResultsDto = args;
            //var cyear = args.Search.ModelYear;
           // var pyear = (args.Search.ModelYear) - 1;
           // var nyear = (args.Search.ModelYear) + 1;
            //            alert("internet_searchCompleted() called");

            var modifySearchLink = '/pricing/Pages/Internet/ModifySearch.aspx?oh=' + searchResultsDto.Search.OwnerHandle + '&vh=' + searchResultsDto.Search.VehicleHandle + '&sh=' + searchResultsDto.Search.SearchHandle;
            $('.modify_search_link').attr('href', modifySearchLink);

            var primarySummary = _.find(searchResultsDto.SearchSummaries, function (ss) { return ss.IsPrimary; });

            var searchSumInfo = searchResultsDto.SearchSummaryInfo;
            if (primarySummary.SearchType == SearchTypeDto.Overall) {
                var text = primarySummary.Description;
            }
            else {
                var text = _.compact([searchSumInfo.Segment, searchSumInfo.Trim, searchSumInfo.Doors, searchSumInfo.Engine, searchSumInfo.DriveTrain, searchSumInfo.FuelType, searchSumInfo.Transmission]).join(" | ");
            }
            _self.listingsInclude(text.replace(/\s{2,}/g, ''));

            // load the market pricing list.
            _self.searchType = primarySummary.SearchType;
            //            alert("search type calculated from internet_searchCompleted(): " + _self.searchType);

            _needNewListingsSearch = true;
            if (_showListings)
                _self.load_market_listings();

            _self.load_market_pricing_list();

            // calculate the optimal price.
            calculate_optimal_price(searchResultsDto);


            _self.averageMileage(primarySummary.Mileage.HasAverage ? _.comma(primarySummary.Mileage.Average) : "--");

            var mileage;

            mileage = initArgs.currentMileage;

            var mileageFinal = _.isNumber(mileage) ? mileage : null;

            _self.currentMileage(mileageFinal != null ? _.comma(mileageFinal) : "--");

            _self.marketListings(_.comma(primarySummary.Units));

            var gaugeData = primarySummary.SearchType == SearchTypeDto.Overall ? searchResultsDto.Overall : searchResultsDto.Precision;

            if (primarySummary.SearchType == SearchTypeDto.Overall) {
                _self.overallTabClass("current");
                _self.precisionTabClass("tab_box Static_1");
                _isOverallTabVisible = true;
            }
            else {
                _self.overallTabClass("tab_box Static_1");
                _self.precisionTabClass("current");
                _isOverallTabVisible = false;
            }

            _isOverallTabVisible ? _self.searchCheckBoxesStyle('visibility:hidden') : _self.searchCheckBoxesStyle('visibility:visible');

            _gauge.Create(gaugeData);

            // now that the gauge is loaded, we can load the values into the calculator.
            _self.loadPricingDecisionInputs();

            _self.marketDaysSupply(gaugeData.Pricing.MarketDaySupply);
            _self.marketAveragePrice(gaugeData.Pricing.MarketPrice.Average); // capture the average price

            _self.overallResultsText("Overall Results ");
            _self.precisionResultsText("Precision Results ");

            _self.overallButtonValue(SearchTypeDto.Overall);
            _self.precisionButtonValue(SearchTypeDto.Precision);

            var p = _.find(searchResultsDto.SearchSummaries, function (ss) { return ss.SearchType === SearchTypeDto.Precision; });
            var o = _.find(searchResultsDto.SearchSummaries, function (ss) { return ss.SearchType === SearchTypeDto.Overall; });
            _self.overallComparableUnits(o.Units);
            _self.precisionComparableUnits(p.Units);

            _self.overallTabVisible(searchResultsDto.Overall.IsPrimary);
            _self.precisionTabVisible(searchResultsDto.Precision.IsPrimary);

           // _self.current_year(cyear);
           // _self.prev_year(pyear);
          //  _self.next_year(nyear);

            _savedSearch = searchResultsDto.Search;

            bindSearchDistance(searchResultsDto);
            bindLowMileage(searchResultsDto);
            bindHighMileage(searchResultsDto);
            bindSearchCheckBoxes(searchResultsDto);

            var targetPrice = initArgs.isPricing ? _self.internetPrice() : _self.targetSellingPrice();
            //            alert(targetPrice);
            _gauge.update_slider(targetPrice);

            _self.generatedBy(searchResultsDto.Search.UpdateUser || "System");

            _self.searchDate(_.asmxDate(searchResultsDto.Search.UpdateDate));

            // update mystery shopping urls
            _self.loadMysteryShoppingUrls();

            _self.loading("pointless_design cf");
            _self.showLoading(false);

            _self.searchReady(true);
        };


        var calculate_optimal_price = function (searchResultsDto) {

            //max, avg prices... used for optimal price.
            var avg = _self.searchType == SearchTypeDto.Overall ?
                searchResultsDto.Overall.Pricing.MarketPrice.Average :
                searchResultsDto.Precision.Pricing.MarketPrice.Average;

            var max = _self.searchType == SearchTypeDto.Overall ?
                searchResultsDto.Overall.Pricing.PctMarketAvg.Maximum :
                searchResultsDto.Precision.Pricing.PctMarketAvg.Maximum;

            //            alert("Average: " + avg);
            //            alert("Max Pct Mkt Avg: " + max);

            var optimal = Math.round((avg * max) / 100);
            //            alert("Optimal price: " + optimal);

            _optimalPrice = optimal;
        };

        this.highlightedRowIndex = ko.observable(null);

        this.dataTableUI = ko.observable(null);


        var internet_marketListingFetchCompleted = function (args) {

            // check if a pending search was invoked while this latest search was processing
            // and if so, run that search now.
            if (_self.pendingMarketListingsFetch()) {
                var dto = _self.pendingMarketListingsFetch();
                _self.pendingMarketListingsFetch(null); // pop the pending dto off the 'queue'
                _internet.MarketListingsFetchAsync(dto); // run it
                return; // do not render the stale search
            }

            _self.highlightedRowIndex(args.HighlightedRowIndex);

            var dataTableArray = [];
            for (var i = 0; i < args.MarketListings.length; i++) {
                var marketListing = args.MarketListings[i];

                dataTableArray.push([
                    {   // virtual column used as container for expandable detail section
                        ListingColor: marketListing.ListingColor,
                        ListingStockNumber: marketListing.ListingStockNumber,
                        ListingSellerDescription: marketListing.ListingSellerDescription
                    },
                    marketListing.Age,
                    marketListing.Seller,
                    marketListing.VehicleDescription,
                    marketListing.ListingVin,
                    marketListing.IsCertified,
                    marketListing.VehicleColor,
                    marketListing.VehicleMileage,
                    marketListing.ListPrice,
                    marketListing.PercentMarketAverage,
                    marketListing.DistanceFromDealer,
                ]);
            }


            _self.dataTableUI($('section#market_listings table.full.data.listings').dataTable({
                aaData: dataTableArray,
                bJQueryUI: false,
                bAutoWidth: false,
                bDeferRender: true,
                aaSorting: [],
                bSortClasses: false,
                sPaginationType: 'full_numbers',
                fnCreatedRow: function (nRow, aData, iDataIndex) {
                    $(nRow).addClass((_self.highlightedRowIndex() == iDataIndex) ? 'highlighted' : 'listing');
                },
                aoColumns: [
                    {   // seller description (object) (hidden)
                        sTitle: '',
                        bVisible: true,
                        bSortable: false,
                        bSearchable: false,
                        mData: function (oData, type, val) {
                            if (type === 'display')
                                return '<button class="button light" type="button" onclick="$.fn.dataTable.toggleDetail(this)">+</button>';
                            else
                                return '';
                        }
                    },
                    {   // age
                        sTitle: 'AGE',
                        sType: 'numeric',
                        mData: function (oData, type, val) {
                            if (type === 'display')
                                return _.comma(oData[1/*age*/]);
                            else
                                return oData[1/*age*/];
                        }
                    },
                    {   // seller
                        sTitle: 'SELLER'
                    },
                    {   // veh. desc.
                        sTitle: 'VEHICLE DESCRIPTION'
                    },
                    {   // VIN
                        sTitle: 'VIN'
                    },
                    {   // certified
                        sTitle: 'CERTIFIED',
                        sType: 'string',
                        fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).addClass('certified').addClass(oData[5/*certified*/] ? 'true' : 'false');
                        },
                        mData: function (oData, type, val) {
                            if (type === 'display')
                                return null;
                            else
                                return (oData[5/*certified*/] ? '1' : '0');
                        }
                    },
                    {   // color
                        sTitle: 'COLOR'
                    },
                    {   // mileage
                        sTitle: 'MILEAGE',
                        sType: 'numeric',
                        mData: function (oData, type, val) {
                            if (type === 'display')
                                return _.comma(oData[7/*mileage*/]);
                            else
                                return oData[7/*mileage*/];
                        }
                    },
                    {   // 'net price
                        sTitle: 'INTERNET PRICE',
                        sType: 'numeric',
                        mData: function (oData, type, val) {
                            if (type === 'display')
                                return _.moneyOrSymbol(oData[8/*internet price*/], '$0');
                            else
                                return oData[8/*internet price*/];
                        }
                    },
                    {   // pct. mkt. avg.
                        sTitle: '% MKT.AVG.',
                        sType: 'numeric',
                        mData: function (oData, type, val) {
                            if (type === 'display')
                                return _.comma(oData[9/*pct mkt avg*/]);
                            else
                                return oData[9/*pct mkt avg*/];
                        }
                    },
                    {   // distance
                        sTitle: 'DISTANCE',
                        sType: 'numeric',
                        mData: function (oData, type, val) {
                            if (type === 'display')
                                return _.comma(oData[10/*distance*/]);
                            else
                                return oData[10/*distance*/];
                        }
                    }
                ]
            }));

            // visualize default sort as it is expected to arrive from the DB
            $('#container #market_listings table.full.data.listings tr th').removeClass('sorting_asc').removeClass('sorting_desc').addClass('sorting');
            $('#container #market_listings table.full.data.listings tr th:nth-child(11)').addClass('sorting_asc'); // distance, ascending

            _self.showMarketLoading(false);
        };

        _self.dataTable_formatListingDetails = function (oTable, nTr) {
            var aData = oTable.fnGetData(nTr);
            var detail_html =
                '<div class="ad" style="display: block;">' +
                    '<span data-bind="text: listingDescription">' + aData[3/*VehicleDescription*/] + '</span>' +
                    '<dl>' +
                    '<dt class="color">Color</dt>' +
                    '<dd class="color"><span data-bind="text: listingColor">' + aData[0].ListingColor + '</span></dd>' +
                    '<dt class="stock_num">Stock#</dt>' +
                    '<dd class="stock_num"><span data-bind="text: stockNumber">' + aData[0].ListingStockNumber + '</span></dd>' +
                    '</dl>' +
                    '<p class="sellers_description">' +
                    '<span class="sellers_description_label">Seller\'s Description</span>' +
                    '<span data-bind="text: sellerDescription">' + aData[0].ListingSellerDescription + '</span>' +
                    '</p>' +
                    '</div>';
            return detail_html;
        };

        $.fn.dataTable.toggleDetail = function (nButton) {
            var oTable = _self.dataTableUI();
            var nTr = $(nButton).closest('tr');
            if (nTr.length > 0)
                nTr = nTr[0];
            else
                return; // button not in a table
            if (oTable.fnIsOpen(nTr)) {
                oTable.fnClose(nTr);
                nButton.innerText = '+';
            } else {
                oTable.fnOpen(nTr, _self.dataTable_formatListingDetails(oTable, nTr), 'seller_listing_detail');
                nButton.innerText = '-';
            }
        }


        _self.standard_numeric_sort = function (a, b) {
            return a - b;
        };

        // Rank/score the prices & collect distinct values.  Scoring is "standard competition ranking" (1224).
        // See: http://en.wikipedia.org/wiki/Ranking
        var calculate_price_scores = function (prices) {

            // outputs
            var distinctPrices = [];
            var scores = [];

            // sort
            prices.sort(_self.standard_numeric_sort);

            // base case
            var score = 1;
            if (prices.length > 0) {
                // distinct, score 0
                distinctPrices.push(prices[0]);
                scores.push(score);
            }

            for (var j = 1; j < prices.length; j++) {
                if (prices[j] === prices[j - 1]) {
                    // not distinct, so they have the same score
                    scores.push(score);
                } else {
                    // distinct values, different scores
                    distinctPrices.push(prices[j]);

                    score = j + 1;
                    scores.push(score);
                }
            }

            return {
                sortedPrices: prices,
                distinctPrices: distinctPrices,
                priceScores: scores
            };
        };

        // Set the base pricing list arrays.
        var internet_marketPricingListFetchCompleted = function (args) {

            //            alert("internet_marketPricingListFetchCompleted() called");

            var prices = [];

            for (var i = 0; i < args.PriceRanks.length; i++) {
                var priceRank = args.PriceRanks[i];

                var price = priceRank.Price;
                //                alert("price: " + price + "; mileage: " + mileage );

                // if it has positive price, push it into our ranking array
                if (price > 0) {
                    //                    alert("adding ranked market listing price");
                    //_self.marketPricingList.push(price);
                    prices.push(price);
                }

                //                alert("marketPricingList count: " + _self.marketPricingList().length);
            }

            // calc the scores
            var results = calculate_price_scores(prices);

            // clear base arrays
            _self.marketPricingList.removeAll();
            _self.marketPricingListDistinct.removeAll();
            _self.marketPricingListScores.removeAll();

            _self.marketPricingList(results.sortedPrices);
            _self.marketPricingListScores(results.priceScores);
            _self.marketPricingListDistinct(results.distinctPrices);

            //            alert("pricing list: " + _self.marketPricingList().toString() + 
            //                  "; pricing scores: " + _self.marketPricingListScores().toString() + 
            //                  "; distinct market prices: " + _self.marketPricingListDistinct().toString());

            // calulcate positions.
            calculate_inclusive_marketPrice_arrays();

            //            alert("pricing list inclusive: " + _self.marketPricingList_Inclusive().toString() + 
            //                  "; pricing scores inclusive: " + _self.marketPricingListScores_Inclusive().toString() + 
            //                  "; distinct market prices inclusive: " + _self.marketPricingListDistinct_Inclusive().toString());

            //            alert("position: " + _self.marketPricingListScore() + "; distinct index: " + _self.marketPricingListDistinctIndex() );
        };

        // This needs to run whenever the price changes.  It updates the _Inclusive arrays, sets the MarketPricingListScore and Index.
        var calculate_inclusive_marketPrice_arrays = function () {

            //            alert('calculate_inclusive_marketPrice_arrays() called');

            // these arrays need to be recalculated whenever the price changes.

            // clear them.
            _self.marketPricingList_Inclusive.removeAll();
            _self.marketPricingListDistinct_Inclusive.removeAll();
            _self.marketPricingListScores_Inclusive.removeAll();

            //            alert("_self.marketPricingList().length: " + _self.marketPricingList().length);

            // Make a copy of the price array, add our price to it, recalc
            var price = _self.getPrice();
            var prices = _self.marketPricingList().slice();
            prices.push(price);

            //            alert("_self.marketPricingList().length: " + _self.marketPricingList().length);
            //            alert("prices.length: " + prices.length);

            var results = calculate_price_scores(prices);
            _self.marketPricingList_Inclusive(results.sortedPrices);
            _self.marketPricingListDistinct_Inclusive(results.distinctPrices);
            _self.marketPricingListScores_Inclusive(results.priceScores);

            // what is the index of our current price?
            var index = _self.marketPricingList_Inclusive.indexOf(price);

            // what position (score) does this put us in?
            _self.marketPricingListScore(_self.marketPricingListScores_Inclusive()[index]);

            // what is the index of our current price among the distinct prices? use this to find the next price
            // when it's time to move up or down.
            _self.marketPricingListDistinctIndex(_self.marketPricingListDistinct_Inclusive.indexOf(price));

            //            alert("marketPricingList_Inclusive index: " + index + "; marketPricingListScore: " + _self.marketPricingListScore() + "; marketPricingListDistinctIndex: " + _self.marketPricingListDistinctIndex());

        };

        var setRank = function (position) {

            //            alert('setting position to: ' + position);

            // make sure we aren't out of bounds
            if (position < 0 || position > _self.marketPricingListScores()[_self.marketPricingListScores().length] + 1) {
                //                alert('position out of bounds');
                return;
            }

            // find the price in the specified position
            var scoreIndex = _self.marketPricingListScores.indexOf(position);
            var scorePrice = _self.marketPricingList()[scoreIndex];
            var newPrice = scorePrice - 1;

            // watch out for price sequences of n, n+1, n+2, etc...
            if (newPrice == _self.getPrice()) {
                newPrice = newPrice + 2;  
            }

            //            alert("new position: " + position + "; scoreIndex: " + scoreIndex + "; scorePrice: " + scorePrice + "; newPrice: " + newPrice );

            _self.setPrice(newPrice);  // this will trigger syncPrices() which will re-calculate the scores.

        };

        // TODO: update position and index whenever price changes.


        var internet_saveVehiclePricingDecisionInputCompleted = function (args) {
            //            alert("save completed");
        };

        var internet_searchUpdateCompleted = function (args) {
            var searchResultsDto = args;
           // var cyear = args.Search.ModelYear;
           // var pyear = (args.Search.ModelYear) - 1;
           // var nyear = (args.Search.ModelYear) + 1;
            var primarySummary = _.find(searchResultsDto.SearchSummaries, function (ss) { return ss.IsPrimary; });

            // Replace the "Listings Include:" text.
            var searchSumInfo = searchResultsDto.SearchSummaryInfo;
            if (primarySummary.SearchType == SearchTypeDto.Overall) {
                var text = primarySummary.Description;
            }
            else {
                var text = _.compact([searchSumInfo.Segment, searchSumInfo.Trim, searchSumInfo.Doors, searchSumInfo.Engine, searchSumInfo.DriveTrain, searchSumInfo.FuelType, searchSumInfo.Transmission]).join(" | ");
            }
            _self.listingsInclude(text.replace(/\s{2,}/g, ''));

            // load the market pricing list.
            _self.searchType = primarySummary.SearchType;
            //            alert("search type calculated from internet_searchUpdateCompleted(): " + _self.searchType);

            _needNewListingsSearch = true;
            if (_showListings)
                _self.load_market_listings();

            _self.load_market_pricing_list();

            // calculate the optimal price.
            calculate_optimal_price(searchResultsDto);

            _self.averageMileage(primarySummary.Mileage.HasAverage ? _.comma(primarySummary.Mileage.Average) : "--");

            var mileage;

            mileage = initArgs.currentMileage;

            var mileageFinal = _.isNumber(mileage) ? mileage : null;

            _self.currentMileage(mileageFinal != null ? _.comma(mileageFinal) : "--");

            _self.marketListings(_.comma(primarySummary.Units));

            // var gaugeData = primarySummary.SearchType == 1 ? searchResultsDto.Overall : searchResultsDto.Precision;
            ///////////////////////////////////////////////////////////////////

            var gaugeData = primarySummary.SearchType == SearchTypeDto.Overall ? searchResultsDto.Overall : searchResultsDto.Precision;
            if (primarySummary.SearchType == SearchTypeDto.Overall) {
                _self.overallTabClass("current");
                _self.precisionTabClass("tab_box Static_1");
                _isOverallTabVisible = true;
            }
            else {
                _self.overallTabClass("tab_box Static_1");
                _self.precisionTabClass("current");
                _isOverallTabVisible = false;
            }

            _isOverallTabVisible ? _self.searchCheckBoxesStyle('visibility:hidden') : _self.searchCheckBoxesStyle('visibility:visible');
            _gauge.Create(gaugeData);

            // now that the gauge is loaded, we can load the values into the calculator.
            _self.loadPricingDecisionInputs();

            _self.marketDaysSupply(gaugeData.Pricing.MarketDaySupply);

            // capture the avg price
            _self.marketAveragePrice(gaugeData.Pricing.MarketPrice.Average);

            _self.overallResultsText("Overall Results ");
            _self.precisionResultsText("Precision Results ");

            _self.overallButtonValue(SearchTypeDto.Overall);
            _self.precisionButtonValue(SearchTypeDto.Precision);

            var p = _.find(searchResultsDto.SearchSummaries, function (ss) { return ss.SearchType === SearchTypeDto.Precision; });
            var o = _.find(searchResultsDto.SearchSummaries, function (ss) { return ss.SearchType === SearchTypeDto.Overall; });
            _self.overallComparableUnits(o.Units);
            _self.precisionComparableUnits(p.Units);

            _self.overallTabVisible(searchResultsDto.Overall.IsPrimary);
            _self.precisionTabVisible(searchResultsDto.Precision.IsPrimary);

           // _self.current_year(cyear);
           // _self.prev_year(pyear);
          //  _self.next_year(nyear);

            _savedSearch = searchResultsDto.Search;

            bindSearchDistance(searchResultsDto);
            bindLowMileage(searchResultsDto);
            bindHighMileage(searchResultsDto);
            bindSearchCheckBoxes(searchResultsDto);

            var targetPrice = initArgs.isPricing ? _self.internetPrice() : _self.targetSellingPrice();
            //            alert(targetPrice);
            _gauge.update_slider(targetPrice);


            _self.generatedBy(searchResultsDto.Search.UpdateUser || "System");

            _self.searchDate(_.asmxDate(searchResultsDto.Search.UpdateDate));

            _self.loading("pointless_design cf");
            _self.showLoading(false);

            if (_self.dataTableUI()) {
                _self.dataTableUI().fnClearTable();
                _self.dataTableUI().fnDestroy();
                _self.dataTableUI(null);
            }
            if (_self.marketListingsData)
                _self.marketListingsData.removeAll();

            // updated mystery shopper urls
            _self.loadMysteryShoppingUrls();
        };

        // KNOCKOUT EVENT HANDLERS (PUBLIC)
        this.findMyVehicle = function () {
            if (!_self.dataTableUI())
                return;

            var my_vehicle_original_data_index = _self.highlightedRowIndex();
            var datatables_sorted_indices = _self.dataTableUI().fnSettings().aiDisplayMaster;
            var my_vehicle_current_data_index = $.inArray(my_vehicle_original_data_index, datatables_sorted_indices);
            var pagination_page_length = _self.dataTableUI().fnSettings()._iDisplayLength;
            var target_page_index = Math.floor(my_vehicle_current_data_index / pagination_page_length);
            _self.dataTableUI().fnPageChange(target_page_index);
        };

        this.viewListingsClicked = function () {
            if (!_showListings) {
                _self.viewListingsText("click to hide listings");
                _self.viewListingsClass("white_box");
                _showListings = true;

                if (_needNewListingsSearch) {
                    _self.load_market_listings();
                }

            }
            else {
                _self.viewListingsText("click to show listings");
                _self.viewListingsClass("hidden");
                _showListings = false;
            }
        };

        this.overallClicked = function () {
            if (_isOverallTabVisible == false) {
                _self.loading("loading");
                _self.showLoading(true);

                _self.overallTabClass("current");
                _self.precisionTabClass("tab_box Static_1");
                _isOverallTabVisible = true;

                var withinData = _self.selectedWithinMileage().id;
                _savedSearch.Distance = _.find(_savedSearch.Distances, function (d) { return d.Id == withinData; });

                var lowMileageData = _self.selectedLowMileage().id;
                _savedSearch.LowMileage = _.find(_savedSearch.Mileages, function (d) { return d.Value == lowMileageData; });

                var highMileageData = _self.selectedHighMileage().id;
                _savedSearch.HighMileage = _.find(_savedSearch.Mileages, function (d) { return d.Value == highMileageData; });
                if (_savedSearch.HighMileage) {
                    _savedSearch.HighMileage.Value -= 1; // we save to highmileage as one less. Legacy page does this for some reason
                }

                _savedSearch.MatchCertified = _self.onlyCertified();
                _savedSearch.MatchColor = _self.onlyWhite();
               // _savedSearch.CurrentYear = _self.onlyCurrentYear();
               // _savedSearch.PreviousYear = _self.onlyPreviousYear();
               // _savedSearch.NextYear = _self.onlyNextYear();
                _savedSearch.SearchType = SearchTypeDto.Precision;

                var searchUpdateArgumentsDto = new SearchUpdateArgumentsDto();
                searchUpdateArgumentsDto.OwnerEntityTypeId = initArgs.OwnerEntityTypeId;
                searchUpdateArgumentsDto.OwnerEntityId = initArgs.OwnerEntityId;
                searchUpdateArgumentsDto.VehicleEntityTypeId = initArgs.VehicleEntityTypeId;
                searchUpdateArgumentsDto.VehicleEntityId = initArgs.VehicleEntityId;
                searchUpdateArgumentsDto.InsertUser = initArgs.InsertUser;
                searchUpdateArgumentsDto.Search = _savedSearch;
                searchUpdateArgumentsDto.Search.UpdateDate = new Date();
                searchUpdateArgumentsDto.SearchType = SearchTypeDto.Overall;

                _internet.SearchUpdateAsync(searchUpdateArgumentsDto);
            }
        };

        this.precisionClicked = function () {
            if (_isOverallTabVisible == true) {
                _self.loading("loading");
                _self.showLoading(true);

                _self.overallTabClass("tab_box Static_1");
                _self.precisionTabClass("current");
                _isOverallTabVisible = false;

                var withinData = _self.selectedWithinMileage().id;
                _savedSearch.Distance = _.find(_savedSearch.Distances, function (d) { return d.Id == withinData; });

                var lowMileageData = _self.selectedLowMileage().id;
                _savedSearch.LowMileage = _.find(_savedSearch.Mileages, function (d) { return d.Value == lowMileageData; });

                var highMileageData = _self.selectedHighMileage().id;
                _savedSearch.HighMileage = _.find(_savedSearch.Mileages, function (d) { return d.Value == highMileageData; });
                if (_savedSearch.HighMileage) {
                    _savedSearch.HighMileage.Value -= 1; // we save to highmileage as one less. Legacy page does this for some reason
                }

                _savedSearch.MatchCertified = _self.onlyCertified();
                _savedSearch.MatchColor = _self.onlyWhite();
                //_savedSearch.CurrentYear = _self.onlyCurrentYear();
               // _savedSearch.PreviousYear = _self.onlyPreviousYear();
              //  _savedSearch.NextYear = _self.onlyNextYear();
                _savedSearch.SearchType = SearchTypeDto.Precision;

                var searchUpdateArgumentsDto = new SearchUpdateArgumentsDto();
                searchUpdateArgumentsDto.OwnerEntityTypeId = initArgs.OwnerEntityTypeId;
                searchUpdateArgumentsDto.OwnerEntityId = initArgs.OwnerEntityId;
                searchUpdateArgumentsDto.VehicleEntityTypeId = initArgs.VehicleEntityTypeId;
                searchUpdateArgumentsDto.VehicleEntityId = initArgs.VehicleEntityId;
                searchUpdateArgumentsDto.InsertUser = initArgs.InsertUser;
                searchUpdateArgumentsDto.Search = _savedSearch;
                searchUpdateArgumentsDto.Search.UpdateDate = new Date();
                searchUpdateArgumentsDto.SearchType = SearchTypeDto.Precision;

                _internet.SearchUpdateAsync(searchUpdateArgumentsDto);

            }
        };

        this.searchClicked = function () {

            _self.loading("loading");
            _self.showLoading(true);

            var withinData = _self.selectedWithinMileage().id;
            _savedSearch.Distance = _.find(_savedSearch.Distances, function (d) { return d.Id == withinData; });

            var lowMileageData = _self.selectedLowMileage().id;
            _savedSearch.LowMileage = _.find(_savedSearch.Mileages, function (d) { return d.Value == lowMileageData; });

            var highMileageData = _self.selectedHighMileage().id;
            _savedSearch.HighMileage = _.find(_savedSearch.Mileages, function (d) { return d.Value == highMileageData; });
            if (_savedSearch.HighMileage) {
                _savedSearch.HighMileage.Value -= 1; // we save to highmileage as one less. Legacy page does this for some reason
            }

            _savedSearch.MatchCertified = _self.onlyCertified();
            _savedSearch.MatchColor = _self.onlyWhite();
           // _savedSearch.CurrentYear = _self.onlyCurrentYear();
           // _savedSearch.PreviousYear = _self.onlyPreviousYear();
          //  _savedSearch.NextYear = _self.onlyNextYear();

            var searchUpdateArgumentsDto = new SearchUpdateArgumentsDto();
            searchUpdateArgumentsDto.OwnerEntityTypeId = initArgs.OwnerEntityTypeId;
            searchUpdateArgumentsDto.OwnerEntityId = initArgs.OwnerEntityId;
            searchUpdateArgumentsDto.VehicleEntityTypeId = initArgs.VehicleEntityTypeId;
            searchUpdateArgumentsDto.VehicleEntityId = initArgs.VehicleEntityId;
            searchUpdateArgumentsDto.InsertUser = initArgs.InsertUser;

            if (_isOverallTabVisible == true) {
                _savedSearch.SearchType = SearchTypeDto.Overall;
                searchUpdateArgumentsDto.SearchType = SearchTypeDto.Overall;
            }
            else {
                _savedSearch.SearchType = SearchTypeDto.Precision;
                searchUpdateArgumentsDto.SearchType = SearchTypeDto.Precision;
            }

            searchUpdateArgumentsDto.Search = _savedSearch;
            searchUpdateArgumentsDto.Search.UpdateDate = new Date();

            _internet.SearchUpdateAsync(searchUpdateArgumentsDto);

        };

        //
        // This is how the whole thing gets started.
        //        
        this.buttonClicked = function () {
            _self.loading("loading");
            _self.showLoading(true);

            var searchArgumentsDto = new SearchArgumentsDto();
            searchArgumentsDto.OwnerEntityTypeId = initArgs.OwnerEntityTypeId;
            searchArgumentsDto.OwnerEntityId = initArgs.OwnerEntityId;
            searchArgumentsDto.VehicleEntityTypeId = initArgs.VehicleEntityTypeId;
            searchArgumentsDto.VehicleEntityId = initArgs.VehicleEntityId;
            searchArgumentsDto.InsertUser = initArgs.InsertUser;
            _internet.SearchAsync(searchArgumentsDto);
        };

        this.loadMysteryShoppingUrls = function () {

//            alert('loadMysteryShoppingUrls() called');
            var mysteryShoppingParametersDto = new MysteryShoppingParametersDto();
            mysteryShoppingParametersDto.OwnerEntityTypeId = initArgs.OwnerEntityTypeId;
            mysteryShoppingParametersDto.OwnerEntityId = initArgs.OwnerEntityId;
            mysteryShoppingParametersDto.VehicleEntityTypeId = initArgs.VehicleEntityTypeId;
            mysteryShoppingParametersDto.VehicleEntityId = initArgs.VehicleEntityId;
            mysteryShoppingParametersDto.UserId = initArgs.InsertUser;

            mysteryShoppingParametersDto.SearchRadius = _savedSearch.Distance.Value;

            _internet.GetMysteryShoppingUrlsAsync(mysteryShoppingParametersDto);
        };

        var internet_getMysteryShoppingUrlsCompleted = function (args) {
//            alert('internet_getMysteryShoppingUrlsCompleted() called.');
            // update observed properties.
            _self.autoTraderUrl(args.AutoTraderUrl);
            _self.carSoupUrl(args.CarSoupUrl);
            _self.carsDotComUrl(args.CarsUrl);

            _self.mysteryShoppingUrlsReady(true);
        };

        this.loadPricingDecisionInputs = function () {
            // 
            var vehiclePricingDto = new VehiclePricingDecisionInputDto();
            vehiclePricingDto.OwnerEntityTypeId = initArgs.OwnerEntityTypeId;
            vehiclePricingDto.OwnerEntityId = initArgs.OwnerEntityId;
            vehiclePricingDto.VehicleEntityTypeId = initArgs.VehicleEntityTypeId;
            vehiclePricingDto.VehicleEntityId = initArgs.VehicleEntityId;
            vehiclePricingDto.UserId = initArgs.InsertUser;
            _internet.GetVehiclePricingDecisionInputFetchAsync(vehiclePricingDto);

        };


        this.pendingMarketListingsFetch = ko.observable(null);

        this.load_market_listings = function () {
            _needNewListingsSearch = false;

            var marketListingsFetchArgumentsDto = new MarketListingsFetchArgumentsDto();
            marketListingsFetchArgumentsDto.OwnerEntityTypeId = initArgs.OwnerEntityTypeId;
            marketListingsFetchArgumentsDto.OwnerEntityId = initArgs.OwnerEntityId;
            marketListingsFetchArgumentsDto.VehicleEntityTypeId = initArgs.VehicleEntityTypeId;
            marketListingsFetchArgumentsDto.VehicleEntityId = initArgs.VehicleEntityId;
            marketListingsFetchArgumentsDto.InsertUser = initArgs.InsertUser;
            marketListingsFetchArgumentsDto.SearchType = _self.searchType;
            marketListingsFetchArgumentsDto.MaximumRows = 100000;
            marketListingsFetchArgumentsDto.StartRowIndex = 0;

            // TO DO: get sort cols
            marketListingsFetchArgumentsDto.SortColumns = [];
            marketListingsFetchArgumentsDto.SortColumns.push(SortColumnDto.Create("DistanceFromDealer", true/*asc*/));

            marketListingsFetchArgumentsDto.HighlightVehicle = true;

            // decide whether to fetch immediately or wait on the current search to finish
            if (_self.showMarketLoading()) { // if already loading
                // set the pending search. multiple pending searches will be intentionally discarded.
                _self.pendingMarketListingsFetch(marketListingsFetchArgumentsDto);
            } else { // not already loading
                _self.showMarketLoading(true);
                _internet.MarketListingsFetchAsync(marketListingsFetchArgumentsDto);
            }
        };

        // load market price list for ranking.
        this.load_market_pricing_list = function () {
            // load the price list for rankings.
            var marketPricingListFetchArgumentsDto = new MarketPricingListFetchArgumentsDto();
            marketPricingListFetchArgumentsDto.OwnerEntityTypeId = initArgs.OwnerEntityTypeId;
            marketPricingListFetchArgumentsDto.OwnerEntityId = initArgs.OwnerEntityId;
            marketPricingListFetchArgumentsDto.VehicleEntityTypeId = initArgs.VehicleEntityTypeId;
            marketPricingListFetchArgumentsDto.VehicleEntityId = initArgs.VehicleEntityId;
            marketPricingListFetchArgumentsDto.InsertUser = initArgs.InsertUser;

            marketPricingListFetchArgumentsDto.SearchType = _self.searchType;

            _internet.MarketPricingListFetchAsync(marketPricingListFetchArgumentsDto);
        };


        this.savePrice = function () {
            var price = _self.internetPrice();
            GaugeComponent.fireSavePrice(price);
            _self.currentPrice(price); // update 'current' price which is initial or last-saved
        };

        // This function is responsible for keeping all the price controls in sync.
        this.syncPrices = function () {

            var price = _self.getPrice();

            calculate_inclusive_marketPrice_arrays();

            // update the gauge with the correct price.
            _gauge.update_slider(price);

        };

        // The syncPrices function should be called whenever the price changes.
        _self.internetPrice.subscribe(this.syncPrices);
        _self.targetSellingPrice.subscribe(this.syncPrices);


        var internet_getVehiclePricingDecisionInputFetchCompleted = function (args) {

            /*
            alert("internet_vehiclePricingDecisionInputFetchCompleted() called");

            alert("Gauge Estimated recon: " + _self.estimatedRecon());
            alert("Gauge Target gross: " + _self.targetGrossProfit());

            alert("Saved Estimated recon: " + args.EstimatedAdditionalCosts);
            alert("Saved Total Gross: " + args.TargetGrossProfit);
            */

            // load target gross
            if (!_.isNull(args.TargetGrossProfit)) {
                _self.targetGrossProfit(args.TargetGrossProfit);
                //                alert("setting target gross profit to: " + args.TargetGrossProfit);
            }

            if (_.isNull(_self.estimatedRecon()) || (_self.estimatedRecon() == 0 && args.EstimatedAdditionalCosts > 0)) {
                _self.estimatedRecon(args.EstimatedAdditionalCosts);
                //alert("setting estimated recon to: " + args.EstimatedAdditionalCosts);
            }

            _self.estimatedReconReady(true);
            _self.targetGrossProfitReady(true);
        };




        // REGISTER EVENT HANDLERS
        _internet.SearchCompleted.push(internet_searchCompleted);
        _internet.MarketListingFetchCompleted.push(internet_marketListingFetchCompleted);
        _internet.SearchUpdateCompleted.push(internet_searchUpdateCompleted);
        _internet.MarketPricingListFetchCompleted.push(internet_marketPricingListFetchCompleted);
        _internet.GetVehiclePricingDecisionInputCompleted.push(internet_getVehiclePricingDecisionInputFetchCompleted);
        _internet.SaveVehiclePricingDecisionInputCompleted.push(internet_saveVehiclePricingDecisionInputCompleted);
        _internet.GetMysteryShoppingUrlsCompleted.push(internet_getMysteryShoppingUrlsCompleted);

        _gauge.SliderMoved.push(function (value) {

            _self.setPrice(value);

        });

        _self.increment_rank = function () {
            if (_self.marketPricingList().length == 0 || _self.marketPricingListScore() == 1) {
                return;
            }
            // what is the previous score?
            var scoreIndex = _self.marketPricingListScores.indexOf(_self.marketPricingListScore());
            if (scoreIndex == -1) {
                // set to max
                scoreIndex = _self.marketPricingListScores().length;
            }
            var newScore = _self.marketPricingListScores()[scoreIndex - 1];
            setRank(newScore);
        };

        _self.decrement_rank = function () {
            if (_self.marketPricingList().length == 0 || _self.marketPricingListScore() > _self.marketPricingList().length) {
                return;
            }
            // what is the next score?
            var nextScore = _self.findFirstLargerValue(_self.marketPricingListScore(), _self.marketPricingListScores());
            if (nextScore == null) {
                // no larger scores; do nothing
                return;
            }
            setRank(nextScore.value);
        };

        _self.rankDisplayCurrent = ko.computed(function () {
            return _self.marketPricingListScore();
        });

        _self.rankDisplayCount = ko.computed(function () {
            return _self.marketPricingList().length + 1;
        });


        //this exposes the current appraisal value
        GaugeComponent.registerAppraisalValues(function () {

            return {
                appraisalValue: _self.appraisalValue(),
                estimatedRecon: _self.estimatedRecon(),
                targetGrossProfit: _self.targetGrossProfit()
            };

        });

        GaugeComponent.registerMysteryShoppingUrls(function () {

            return {
                autoTraderUrl: _self.autoTraderUrl(),
                carsDotComUrl: _self.carsDotComUrl(),
                carSoupUrl: _self.carSoupUrl()
            };

        });

        // this exposes the save behavior to the component.
        GaugeComponent.resisterSaveAppraisal(function () {

            //            alert("saving appraisal value");

            var saveDto = new VehiclePricingDecisionInputDto();
            saveDto.OwnerEntityTypeId = initArgs.OwnerEntityTypeId;
            saveDto.OwnerEntityId = initArgs.OwnerEntityId;
            saveDto.VehicleEntityTypeId = initArgs.VehicleEntityTypeId;
            saveDto.VehicleEntityId = initArgs.VehicleEntityId;
            saveDto.UserId = initArgs.InsertUser;

            saveDto.EstimatedAdditionalCosts = _self.estimatedRecon();
            saveDto.TargetGrossProfit = _self.targetGrossProfit();

            _internet.SaveVehiclePricingDecisionInputFetchAsync(saveDto);
        });


        GaugeComponent.registerSetOptimalPrice(function f() {
            //            alert("setting price to " + _optimalPrice);
            _self.setPrice(_optimalPrice);

        });

    };

    return ViewModel;

});