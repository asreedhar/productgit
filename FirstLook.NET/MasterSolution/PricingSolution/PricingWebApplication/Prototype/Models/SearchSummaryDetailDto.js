﻿define(function (require) {

    var SampleSummaryDto = require("Models/SampleSummaryDto");

    function SearchSummaryDetailDto() {
        this.Description = null;
        this.HasMarketDaySupply = null;
        this.MarketDaySupply = null;
        this.IsReference = null;
        this.IsSearch = null;
        this.NumberOfListings = null;
        this.ListPrice = null;
        this.Mileage = null;
    };

    SearchSummaryDetailDto.fromJSON = function (json) {
        var searchSummaryDetailDto = new SearchSummaryDetailDto();
        return searchSummaryDetailDto;
    }

    return SearchSummaryDetailDto;

});