﻿define(function (require) {

    var SampleSummaryDto = require("Models/SampleSummaryDto");

    function MarketPricingDto() {
        this.VehicleEntityTypeId = null;
        this.VehicleEntityId = null;
        this.HasMileage = null;
        this.Mileage = null;
        this.HasMarketDaySupply = null;
        this.MarketDaySupply = null;
        this.HasInventoryUnitCost = null;
        this.InventoryUnitCost = null;
        this.HasInventoryListPrice = null;
        this.InventoryListPrice = null;
        this.HasPctAvgMarketPrice = null;
        this.PctAvgMarketPrice = null;
        this.NumListings = null;
        this.NumComparableListings = null;
        this.HasAppraisalValue = null;
        this.AppraisalValue = null;
        this.HasEstimatedReconditioningCost = null;
        this.EstimatedReconditioningCost = null;
        this.HasTargetGrossProfit = null;
        this.TargetGrossProfit = null;
        this.HasSearchRadius = null;
        this.SearchRadius = null;
        this.Vin = null;
        this.VehicleCatalogID = null;
        this.MarketPrice = null;
        this.VehicleMileage = null;
        this.PctMarketAvg = null;
        this.GrossProfit = null;
        this.PctMarketValue = null;
        this.ComparableMarketPrice = null;
        this.JDPowerSalePrice = null;
        this.NaaaSalePrice = null;
    };

    MarketPricingDto.fromJSON = function (json) {
        var marketPricingDto = new MarketPricingDto();
        return marketPricingDto;
    }

    return MarketPricingDto;

});