﻿define(function (require) {

    function CallWebService(serviceUrl, data, successFunction) {
        
        if (!('toJSON' in data)) {
            throw "data object in Globals.CallWebService does not implement toJSON method";
        }

        var ajaxSetup = { "url": serviceUrl };


        var servicesAjax = { "type": "POST", "dataType": "json", "processData": false, "contentType": "application/json;charset=utf-8" };
        var ajaxSetup = $.extend({}, servicesAjax, ajaxSetup, {});

        ajaxSetup.error = function (xhrObj) {
            alert('Web Service Request Failure\nError code:' + xhrObj.status + '\nMessage: ' + xhrObj.statusText);
        }

        //debugger;
        var tempData = "" + data.toJSON();
        tempData = tempData.replace(/}$/, ",\"apiKey\":\"a545e2ef-9ac0-4339-9220-8d443d5d2557\"}");
        ajaxSetup.data = tempData;

        function onFetchSuccess(json) {
            setTimeout(function () {
                successFunction(json.d);
            }, 1);
        }
        ajaxSetup.success = onFetchSuccess;
        $.ajax(ajaxSetup);

    };


    function Internet() {

        var self = this;

        this.SearchCompleted = [];
        this.MarketListingFetchCompleted = [];
        this.SearchUpdateCompleted = [];
        this.MarketPricingListFetchCompleted = [];
        this.GetVehiclePricingDecisionInputCompleted = [];
        this.SaveVehiclePricingDecisionInputCompleted = [];
        this.GetHandlesCompleted = [];
        this.GetMysteryShoppingUrlsCompleted = [];

        this.SearchAsync = function (arguments) {
            CallWebService(Internet.BaseUrl + "Search", arguments, onSearchCompleted);
        };
        var onSearchCompleted = function (args) {
            for (var i = 0; i < self.SearchCompleted.length; i++) {
                self.SearchCompleted[i](args);
            }
        };
        this.SearchUpdateAsync = function (arguments) {
            CallWebService(Internet.BaseUrl + "SearchUpdate", arguments, onSearchUpdateCompleted);
        };
        var onSearchUpdateCompleted = function (args) {
            for (var i = 0; i < self.SearchUpdateCompleted.length; i++) {
                self.SearchUpdateCompleted[i](args);
            }
        };
        this.MarketListingsFetchAsync = function (arguments) {
            CallWebService(Internet.BaseUrl + "MarketListingsFetch", arguments, onMarketListingFetchCompleted);
        };
        var onMarketListingFetchCompleted = function (args) {
            for (var i = 0; i < self.MarketListingFetchCompleted.length; i++) {
                self.MarketListingFetchCompleted[i](args);
            }
        };

        this.MarketPricingListFetchAsync = function (arguments) {
            //            alert("MarketPricingListFetchAsync() called");
            CallWebService(Internet.BaseUrl + "MarketPricingListFetch", arguments, onMarketPricingListFetchCompleted);
        };
        var onMarketPricingListFetchCompleted = function (args) {
            // call backs
            for (var i = 0; i < self.MarketPricingListFetchCompleted.length; i++) {
                self.MarketPricingListFetchCompleted[i](args);
            }
        };


        this.GetVehiclePricingDecisionInputFetchAsync = function (arguments) {
            //            alert("Internet.GetVehiclePricingDecisionInputFetchAsync() called.");
            CallWebService(Internet.BaseUrl + "GetVehiclePricingDecisionInput", arguments, onGetVehiclePricingDecisionInputCompleted);
        };
        var onGetVehiclePricingDecisionInputCompleted = function (args) {
            //            alert("onGetVehiclePricingDecisionInputCompleted() called.");

            // call backs
            for (var i = 0; i < self.GetVehiclePricingDecisionInputCompleted.length; i++) {
                self.GetVehiclePricingDecisionInputCompleted[i](args);
            }
        };


        this.SaveVehiclePricingDecisionInputFetchAsync = function (arguments) {
            //            alert("calling Internet.SaveVehiclePricingDecisionInputFetchAsync()");
            CallWebService(Internet.BaseUrl + "SaveVehiclePricingDecisionInput", arguments, onSaveVehiclePricingDecisionInputCompleted);
        };
        var onSaveVehiclePricingDecisionInputCompleted = function (args) {
            // call backs
            for (var i = 0; i < self.SaveVehiclePricingDecisionInputCompleted.length; i++) {
                self.SaveVehiclePricingDecisionInputCompleted[i](args);
            }
        };

        this.GetHandlesAsync = function (arguments) {
            CallWebService(Internet.BaseUrl + "GetHandles", arguments, onGetHandlesCompleted);
        };
        var onGetHandlesCompleted = function (args) {
            // call backs
            for (var i = 0; i < self.GetHandlesCompleted.length; i++) {
                self.GetHandlesCompleted[i](args);
            }
        };

        this.GetMysteryShoppingUrlsAsync = function (arguments) {
            CallWebService(Internet.BaseUrl + "MysteryShoppingLinksNoHandles", arguments, onGetMysteryShoppingUrlsCompleted);
        };
        var onGetMysteryShoppingUrlsCompleted = function (args) {
            // call backs
            for (var i = 0; i < self.GetMysteryShoppingUrlsCompleted.length; i++) {
                self.GetMysteryShoppingUrlsCompleted[i](args);
            }
        };


    };

    Internet.BaseUrl = "/pricing/Services/Internet.asmx/";

    return Internet;
});