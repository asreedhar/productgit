﻿define(function (require) {

    function MarketListingsFetchArgumentsDto() {

        var self = this;

        this.OwnerEntityTypeId = null;
        this.OwnerEntityId = null;
        this.VehicleEntityTypeId = null;
        this.VehicleEntityId = null;
        this.InsertUser = null;
        this.SearchType = null;
        this.MaximumRows = null;
        this.StartRowIndex = null;
        this.SortColumns = [];
        this.HighlightVehicle = null;

        this.toJSON = function () {
            var toJSON = {};
            toJSON.arguments = {};
            toJSON.arguments.type = "FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes.MarketListingsFetchArgumentsDto";
            toJSON.arguments.OwnerEntityTypeId = self.OwnerEntityTypeId;
            toJSON.arguments.OwnerEntityId = self.OwnerEntityId;
            toJSON.arguments.VehicleEntityTypeId = self.VehicleEntityTypeId;
            toJSON.arguments.VehicleEntityId = self.VehicleEntityId;
            toJSON.arguments.InsertUser = self.InsertUser;
            toJSON.arguments.SearchType = self.SearchType;
            toJSON.arguments.MaximumRows = self.MaximumRows;
            toJSON.arguments.StartRowIndex = self.StartRowIndex;
            toJSON.arguments.SortColumns = [];
            for (var i = 0; i < self.SortColumns.length; i++) {
                var sortColumn = {};
                sortColumn.ColumnName = self.SortColumns[i].ColumnName;
                sortColumn.Ascending = self.SortColumns[i].Ascending;
                toJSON.arguments.SortColumns.push(sortColumn);
            }
            toJSON.arguments.HighlightVehicle = self.HighlightVehicle;

            return JSON.stringify(toJSON);
        };
    };

    return MarketListingsFetchArgumentsDto;

});