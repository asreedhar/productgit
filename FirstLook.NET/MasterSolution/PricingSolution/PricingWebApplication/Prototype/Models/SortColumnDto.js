﻿define(function (require) {

    function SortColumnDto() {
        this.ColumnName = null;
        this.Ascending = null;
    };

    SortColumnDto.fromJSON = function (json) {
        var sortColumnDto = new SortColumnDto();
        return sortColumnDto;
    }

    SortColumnDto.Create = function (columnName, ascending) {
        sortColumnDto = new SortColumnDto();
        sortColumnDto.ColumnName = columnName;
        sortColumnDto.Ascending = ascending;
        return sortColumnDto;
    }

    return SortColumnDto;

});