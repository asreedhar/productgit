﻿define(function (require) {

    function SearchArgumentsDto() { 
        this.OwnerEntityTypeId = null;
        this.OwnerEntityId = null;
        this.VehicleEntityTypeId = null;
        this.VehicleEntityId = null;
        this.InsertUser = null;

        this.toJSON = function () {
            var toJSON = {};
            toJSON.arguments = {};
            toJSON.arguments.type = "FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes.SearchArgumentsDto";
            toJSON.arguments.OwnerEntityTypeId = this.OwnerEntityTypeId;
            toJSON.arguments.OwnerEntityId = this.OwnerEntityId;
            toJSON.arguments.VehicleEntityTypeId = this.VehicleEntityTypeId;
            toJSON.arguments.VehicleEntityId = this.VehicleEntityId;
            toJSON.arguments.InsertUser = this.InsertUser;

            return JSON.stringify(toJSON);
        }
    };

    return SearchArgumentsDto;

});