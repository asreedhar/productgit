﻿define(function (require) {

    var DistanceDto = require("Models/DistanceDto");
    var MileageDto = require("Models/MileageDto");
    var CatalogEntryDto = require("Models/CatalogEntryDto");

    function SearchDto() {
        this.SearchHandle = null;
        this.OwnerHandle = null;
        this.VehicleHandle = null;
        this.ModelYear = null;
        this.MakeId = null;
        this.LineId = null;
        this.ModelConfigurationId = null;
        //this.CurrentYear = null;
        //this.PreviousYear = null;
       // this.NextYear = null;
        this.Distance = null;
        this.LowMileage = null;
        this.HighMileage = null;
        this.MatchColor = null;
        this.MatchCertified = null;
        this.UpdateUser = null;
        this.UpdateDate = null;
        this.Distances = [];
        this.Mileages = [];
        this.CatalogEntries = [];
    };

    SearchDto.fromJSON = function (json) {
        var searchDto = new SearchDto();
        return searchDto;
    }

    return SearchDto;
});