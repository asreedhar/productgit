﻿define(function (require) {

    var SearchSummaryDto = require("Models/SearchSummaryDto");
    var SearchDetailDto = require("Models/SearchDetailDto");
    var SearchDto = require("Models/SearchDto");
    var SearchSummaryInfoDto = require("Models/SearchSummaryInfoDto");

    function SearchResultsDto() {
        this.SearchSummaries = [];
        this.Overall = null;
        this.Precision = null;
        this.Search = null;
        this.SearchSummaryInfo = null;
    };

    SearchResultsDto.fromJSON = function (json) {
        var searchResultsDto = new SearchResultsDto();
        return searchResultsDto;
    }

    return SearchResultsDto;

});