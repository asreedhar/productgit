﻿define(function (require) {

    function DistanceDto() {
        this.Id = null;
        this.Value = null;
    };

    DistanceDto.fromJSON = function (json) {
        var distanceDto = new DistanceDto();
        return distanceDto;
    }

    DistanceDto.Create = function (id, value) {
        var distanceDto = new DistanceDto();
        distanceDto.Id = id;
        distanceDto.Value = value;

        return distanceDto;
    }

    return DistanceDto;
});