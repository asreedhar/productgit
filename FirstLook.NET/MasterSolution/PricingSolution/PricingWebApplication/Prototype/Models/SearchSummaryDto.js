﻿define(function (require) {

    var SearchTypeDto = require("Models/SearchTypeDto");
    var SampleSummaryDto = require("Models/SampleSummaryDto");

    function SearchSummaryDto() {
        this.SearchType = null;
        this.Description = null;
        this.Units = null;
        this.ComparableUnits = null;
        this.ListPrice = null;
        this.Mileage = null;
        this.IsPrimary = null;
        this.IsDefault = null;
        this.IsCertificationSpecified = null;
        this.IsColorSpecified = null;
        this.IsDoorsSpecified = null;
        this.IsDriveTrainSpecified = null;
        this.IsEngineSpecified = null;
        this.IsFuelTypeSpecified = null;
        this.IsSegmentSpecified = null;
        this.IsBodyTypeSpecified = null;
        this.IsTransmissionSpecified = null;
        this.IsTrimSpecified = null;
    };

    SearchSummaryDto.fromJSON = function (json) {
        var searchSummaryDto = new SearchSummaryDto();
        return searchSummaryDto;
    }

    return SearchSummaryDto;

});