﻿define(function (require) {

    var SearchSummaryDetailDto = require("Models/SearchSummaryDetailDto");
    var MarketPricingDto = require("Models/MarketPricingDto");

    function SearchDetailDto() {
        this.SearchSummaryDetail = [];
        this.Pricing = new MarketPricingDto();
    };

    SearchDetailDto.fromJSON = function (json) {
        var searchDetailDto = new SearchDetailDto();
        return searchDetailDto;
    }

    return SearchDetailDto;

});