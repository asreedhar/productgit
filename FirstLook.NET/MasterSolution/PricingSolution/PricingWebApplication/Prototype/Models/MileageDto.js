﻿define(function (require) {

    function MileageDto() {
        this.Value = null;
    };

    MileageDto.fromJSON = function (json) {
        var mileageDto = new MileageDto();
        return mileageDto;
    }

    MileageDto.Create = function (value) {
        var mileageDto = new MileageDto();
        mileageDto.Value = value;
        return mileageDto;
    }

    return MileageDto;
});