﻿define(function (require) {

    function HandleDto() {

        var self = this;

        this.OwnerEntityTypeId = null;
        this.OwnerEntityId = null;
        this.VehicleEntityTypeId = null;
        this.VehicleEntityId = null;
        this.UserId = null;

        this.OwnerHandle = null;
        this.VehicleHandle = null;
        this.SearchHandle = null;


        this.toJSON = function () {
            var toJSON = {};
            toJSON.arguments = {};
            toJSON.arguments.type = "FirstLook.Pricing.DomainModel.Commands.TransferObjects.HandleDto";
            toJSON.arguments.OwnerEntityTypeId = self.OwnerEntityTypeId;
            toJSON.arguments.OwnerEntityId = self.OwnerEntityId;
            toJSON.arguments.VehicleEntityTypeId = self.VehicleEntityTypeId;
            toJSON.arguments.VehicleEntityId = self.VehicleEntityId;
            toJSON.arguments.UserId = self.UserId;

            toJSON.arguments.OwnerHandle = self.OwnerHandle;
            toJSON.arguments.VehicleHandle = self.VehicleHandle;
            toJSON.arguments.SearchHandle = self.SearchHandle;

            return JSON.stringify(toJSON);
        };
    };

    return HandleDto;

});