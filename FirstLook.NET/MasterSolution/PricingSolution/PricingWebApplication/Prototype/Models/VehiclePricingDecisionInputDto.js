﻿define(function (require) {

    function VehiclePricingDecisionInputDto() {

        var self = this;

        this.OwnerEntityTypeId = null;
        this.OwnerEntityId = null;
        this.VehicleEntityTypeId = null;
        this.VehicleEntityId = null;
        this.UserId = null;
        this.EstimatedAdditionalCosts = null;
        this.TargetGrossProfit = null;


        this.toJSON = function () {
            var toJSON = {};
            toJSON.arguments = {};
            toJSON.arguments.type = "FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes.VehiclePricingDecisionInputDto";
            toJSON.arguments.OwnerEntityTypeId = self.OwnerEntityTypeId;
            toJSON.arguments.OwnerEntityId = self.OwnerEntityId;
            toJSON.arguments.VehicleEntityTypeId = self.VehicleEntityTypeId;
            toJSON.arguments.VehicleEntityId = self.VehicleEntityId;
            toJSON.arguments.UserId = self.UserId;
            toJSON.arguments.EstimatedAdditionalCosts = self.EstimatedAdditionalCosts;
            toJSON.arguments.TargetGrossProfit = self.TargetGrossProfit;

            return JSON.stringify(toJSON);
        };
    };

    return VehiclePricingDecisionInputDto;

});