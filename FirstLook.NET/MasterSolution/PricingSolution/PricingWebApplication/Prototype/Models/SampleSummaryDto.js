﻿define(function (require) {

    function SampleSummaryDto() {
        this.HasMinimum = null;
        this.Minimum = null;
        this.HasAverage = null;
        this.Average = null;
        this.HasMaximum = null;
        this.Maximum = null;
        this.SampleSize = null;
    };

    SampleSummaryDto.fromJSON = function (json) {
        var sampleSummaryDto = new SampleSummaryDto();
        return sampleSummaryDto;
    }

    return SampleSummaryDto;

});