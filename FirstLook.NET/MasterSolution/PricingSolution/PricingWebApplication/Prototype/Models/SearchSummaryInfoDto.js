﻿define(function (require) {

    function SearchSummaryInfoDto() {
        this.Trim = null;
        this.Engine = null;
        this.Transmission = null;
        this.DriveTrain = null;
        this.Doors = null;
        this.BodyType = null;
        this.FuelType = null;
        this.Segment = null;
        this.YearMakeModel = null;
    };

    SearchSummaryInfoDto.fromJSON = function (json) {
        var searchSummaryInfoDto = new SearchSummaryInfoDto();
        return searchSummaryInfoDto;
    }

    return SearchSummaryInfoDto;

});