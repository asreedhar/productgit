﻿define(function (require) {

    function SearchTypeDto() {
    };

    SearchTypeDto.Overall = 1;

    SearchTypeDto.Precision = 4;

    SearchTypeDto.fromJSON = function (json) {
        var searchTypeDto = new SearchTypeDto();
        return searchTypeDto;
    }

    return SearchTypeDto;

});