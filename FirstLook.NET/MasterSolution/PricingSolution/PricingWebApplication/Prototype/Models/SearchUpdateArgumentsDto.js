﻿define(function (require) {

    function SearchUpdateArgumentsDto() {

        var self = this;

        this.OwnerEntityTypeId = null;
        this.OwnerEntityId = null;
        this.VehicleEntityTypeId = null;
        this.VehicleEntityId = null;
        this.InsertUser = null;
        this.Search = null;
        this.SearchType = null;

        this.toJSON = function () {
            var toJSON = {};
            toJSON.arguments = {};
            toJSON.arguments.type = "FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes.SearchUpdateArgumentsDto";
            toJSON.arguments.OwnerEntityTypeId = self.OwnerEntityTypeId;
            toJSON.arguments.OwnerEntityId = self.OwnerEntityId;
            toJSON.arguments.VehicleEntityTypeId = self.VehicleEntityTypeId;
            toJSON.arguments.VehicleEntityId = self.VehicleEntityId;
            toJSON.arguments.InsertUser = self.InsertUser;
            toJSON.arguments.Search = self.Search;
            toJSON.arguments.SearchType = self.SearchType;

            return JSON.stringify(toJSON);
        }
    };

    return SearchUpdateArgumentsDto;

});