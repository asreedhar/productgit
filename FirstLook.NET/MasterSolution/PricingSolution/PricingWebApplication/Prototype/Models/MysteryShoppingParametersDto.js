﻿define(function (require) {

    function MysteryShoppingParametersDto() {

        var self = this;

        this.OwnerEntityTypeId = null;
        this.OwnerEntityId = null;
        this.VehicleEntityTypeId = null;
        this.VehicleEntityId = null;
        this.UserId = null;
        this.SearchRadius = null;
//        this.ModelYear = null;


        this.toJSON = function () {
            var toJSON = {};
            toJSON.arguments = {};
            toJSON.arguments.type = "FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes.MysteryShoppingParametersDto";
            toJSON.arguments.OwnerEntityTypeId = self.OwnerEntityTypeId;
            toJSON.arguments.OwnerEntityId = self.OwnerEntityId;
            toJSON.arguments.VehicleEntityTypeId = self.VehicleEntityTypeId;
            toJSON.arguments.VehicleEntityId = self.VehicleEntityId;
            toJSON.arguments.UserId = self.UserId;
            toJSON.arguments.SearchRadius = self.SearchRadius;
//            toJSON.arguments.ModelYear = self.ModelYear;


            return JSON.stringify(toJSON);
        };
    };

    return MysteryShoppingParametersDto;

});