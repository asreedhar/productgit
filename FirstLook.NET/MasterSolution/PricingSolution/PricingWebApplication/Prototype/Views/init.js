﻿
requirejs.config({
    baseUrl: "/pricing/prototype/",
    waitSeconds: 20, //sometimes getting timeouts
    urlArgs: "bust=" + (new Date()).getTime()
});

define(function (require) {
    ko = require("Globals/knockout-2.1.0");
    ViewModel = require("ViewModels/ViewModel");
    var init_args = GaugeComponent.getArgs();
    viewModelInstance = new ViewModel(init_args);
    ko.applyBindings(viewModelInstance);

    $("#calculator #optimize_price").unbind("click").click(function () {
        GaugeComponent.setOptimalPrice();
        return false;
    });

    if ($(".appraisal_table .history .box_dark").length > 0) {
        $(".appraisal_table .history .box_dark").attr("id", "appraisalHistory").dialog({ autoOpen: false, title: "Appraisal History",width :500 });
        $("#appraisalHistory .hdr").hide();
        $("#calculator #viewHistory").click(function () {
            $("#appraisalHistory").dialog('open');
            return false;
        });
    } else {
        $("#calculator #viewHistory").hide();
        $("#calculator #last_saved_appraisal").hide();
    }

    $("#market_listings .ad_button button").live("click", function () {
        var target = $(this).parent().find(".ad");
        if (target.hasClass("hidden")) {
            $(this).text("-");
            target.show().removeClass("hidden");
        } else {
            $(this).text("+");
            target.hide().addClass("hidden");
        }
    });

    $("#market_listings .ad_button button").live({
        mouseenter: function () {
            var target = $(this).parent().find(".ad");
            target.show();
        },
        mouseleave: function () {
            var target = $(this).parent().find(".ad");
            if (target.hasClass("hidden")) {
                target.hide();
            }
        }
    });

    $('input[placeholder]').each(function () {
        var input = $(this);

        if (input.val() === '') {
            input.addClass("placeholdertext");
            input.val(input.attr('placeholder'));
        }

        $(input).focus(function () {
            if (input.val() === input.attr('placeholder')) {
                input.val('');
                input.removeClass("placeholdertext");
            }
        });

        $(input).blur(function () {
            if (input.val() === '' || input.val() === input.attr('placeholder')) {
                input.val(input.attr('placeholder'));
                input.addClass("placeholdertext");
            }
        });
    });

    viewModelInstance.buttonClicked();
});
