﻿var GaugeComponent = (function () {

    var savePriceChangeEvents = [],
        appraisalValuesChangedEvents = [],
        mysteryShoppingUrlsChangedEvents = [],
        getAppraisalValueFunc,
        getMysteryShoppingUrlsFunc,
        saveAppraisalFunc,
        optimzePriceFunc,
        initArgs = {};

    var viewUrl = "/pricing/prototype/Views/PartialView.htm";
    var requiresInit = "/pricing/prototype/Views/init";
    var aspAuthLandingPage = "/pricing/AspAuth.aspx";


    function authBootStrap(forward, selector) {

        //this is to get the .ASPXAUTH cookie set for non .NET pages. We use a invisible IFrame to load first.
        //Once that is done we load the PartialView.html

        if (!initArgs.isPricing) {
            $(selector).html('<iframe style="display: none"></iframe>');

            $(selector + ' iframe').load(function () {
                forward();
            });

            $(selector + ' iframe').attr('src', aspAuthLandingPage);
        }
        else {
            forward();
        }
    }

    function LoadContent(selector) {

        if (selector) {

            /* 
            calling JQuery.html strips strips <script> tags which requires.js needs to bootstrap itself. It then loads the .js files with unique id's (no caching)
            requires.js looks for a script tag with data-main and then loads that .js file first. It then follows the require links inside that definition and loads those.
            It turns out you can put that data-main attribute on any script so we add it to the html that includes this .js file
            */
            //adding data-main tag to myself.
            var scriptTag = $("script").filter(function () {
                var match = null,
                    source = $(this).attr("src");
                if (source) {
                    match = source.match (/GaugeComponent\.js($|\?.*$)/gi);
                }
                return match != null;
            });

            scriptTag.attr("data-main", requiresInit);

            authBootStrap(function () {

                $.ajax({
                    url: viewUrl,
                    cache: false,
                    success: function (data) {
                        $(selector).html(data);
                    }
                });

            }, selector);
        }

    };

    return {

        savePrice: function (f) {
            savePriceChangeEvents.push(f);
        },

        fireSavePrice: function (newPrice) {
            for (var x = 0; x < savePriceChangeEvents.length; x++) {
                savePriceChangeEvents[x](newPrice);
            }
        },

        fireAppraisalValuesChanged: function (value) {
            //alert("fireAppraisalValuesChanged() called. " + appraisalValuesChangedEvents.length + " handlers registered.");
            for (var x = 0; x < appraisalValuesChangedEvents.length; x++) {
                //alert("calling back appraisalValuesChangedEvent handler");
                appraisalValuesChangedEvents[x](value);
            }
        },

        fireMysteryShoppingUrlsChanged: function (value) {
            for (var x = 0; x < mysteryShoppingUrlsChangedEvents.length; x++) {
                mysteryShoppingUrlsChangedEvents[x](value);
            }
        },

        appraisalValuesChanged: function (f) {
            appraisalValuesChangedEvents.push(f);
        },

        mysteryShoppingUrlsChanged: function (f) {
            mysteryShoppingUrlsChangedEvents.push(f);
        },

        registerAppraisalValues: function (f) {
            getAppraisalValueFunc = f;
        },

        getAppraisalValues: function () {
            return getAppraisalValueFunc();
        },

        registerMysteryShoppingUrls: function (f) {
            getMysteryShoppingUrlsFunc = f;
        },
        getMysteryShoppingUrls: function () {
            return getMysteryShoppingUrlsFunc();
        },

        resisterSaveAppraisal: function (f) {
            saveAppraisalFunc = f;
        },
        saveAppraisal: function () {
            //            alert("GaugeComponent.saveAppraisal() called.");
            return saveAppraisalFunc();
            // saveAppraisalFunc();
        },

        registerSetOptimalPrice: function (f) {
            optimzePriceFunc = f;
        },
        setOptimalPrice: function () {
            return optimzePriceFunc();
        },

        initApprasial: function (args, selector) {
            initArgs = args;
            initArgs.isPricing = false;
            initArgs.gaugeTitle = "TARGET SELLING PRICE";
            LoadContent(selector);
        },

        initPricing: function (args, selector) {
            initArgs = args;
            initArgs.isPricing = true;
            initArgs.gaugeTitle = "INTERNET PRICE";
            LoadContent(selector);
        },

        getArgs: function () {
            return initArgs;
        }
    };

})();
