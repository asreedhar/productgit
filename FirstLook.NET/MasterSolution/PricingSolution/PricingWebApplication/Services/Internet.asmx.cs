﻿using System;
using System.ComponentModel;
using System.Data;
using System.Net;
using System.Web.Script.Services;
using System.Web.Services;
using Csla;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.Common.Data;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;
using FirstLook.Pricing.WebApplication.Pages.Internet.Controls;
using ICommandFactory = FirstLook.Pricing.DomainModel.Commands.ICommandFactory;
using IDataConnection = FirstLook.Common.Core.Data.IDataConnection;

namespace FirstLook.Pricing.WebApplication.Services
{
    /// <summary>
    /// Summary description for Internet
    /// </summary>
    [WebService(Namespace = "http://api.firstlook.biz/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    [ScriptService]
    public class Internet : WebService
    {
        private static string API_KEY = "a545e2ef-9ac0-4339-9220-8d443d5d2557";

        [WebMethod]
        public SearchResultsDto Search(SearchArgumentsDto arguments, string apiKey)
        {
            if (apiKey != API_KEY )
            {
                this.Context.Response.StatusCode = ( int )HttpStatusCode.Unauthorized;
                //this.Context.Response.End();

                return null;
            }

            ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();

            ICommand<SearchResultsDto, SearchArgumentsDto> command = factory.CreateSearchCommand();

            return ExecuteCommand(command, arguments);
        }

        [WebMethod]
        public SearchResultsDto SearchUpdate(SearchUpdateArgumentsDto arguments, string apiKey)
        {
            if (apiKey != API_KEY)
            {
                this.Context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;

                return null;
            }

            ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();

            ICommand<SearchResultsDto, SearchUpdateArgumentsDto> command = factory.CreateSearchUpdateCommand();

            return ExecuteCommand(command, arguments);
        }

        [WebMethod]
        public MarketListingsFetchResultsDto MarketListingsFetch(MarketListingsFetchArgumentsDto arguments, string apiKey)
        {
            if (apiKey != API_KEY)
            {
                this.Context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;

                return null;
            }

            ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();

            ICommand<MarketListingsFetchResultsDto, MarketListingsFetchArgumentsDto> command = factory.CreateMarketListingsFetchCommand();

            return ExecuteCommand(command, arguments);
        }

        [WebMethod]
        public MarketPricingListFetchResultsDto MarketPricingListFetch(MarketPricingListFetchArgumentsDto arguments, string apiKey)
        {
            if (apiKey != API_KEY)
            {
                this.Context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;

                return null;
            }

            ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();

            ICommand<MarketPricingListFetchResultsDto, MarketPricingListFetchArgumentsDto> command = factory.CreateMarketPricingListFetchCommand();

            return ExecuteCommand(command, arguments);
        }

        [WebMethod]
        public InventoryRepriceResultsDto InventoryReprice(InventoryRepriceArgumentsDto arguments, string apiKey)
        {
            this.Context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;

            return null;

/*
            ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();

            ICommand<InventoryRepriceResultsDto, InventoryRepriceArgumentsDto> command =
                factory.CreateInventoryRepriceCommand();

            return ExecuteCommand(command, arguments);
*/
        }

        [WebMethod]
        public FetchInternetPreferencesResultDto FetchInternetPreferences(FetchInternetPreferencesArgumentsDto arguments, string apiKey)
        {
            if (apiKey != API_KEY)
            {
                this.Context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;

                return null;
            }

            ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();

            ICommand<FetchInternetPreferencesResultDto, FetchInternetPreferencesArgumentsDto> command =
                factory.CreateFetchInternetPreferencesCommand();

            return ExecuteCommand(command, arguments);
        }

        [WebMethod]
        public MysteryShoppingLinksResultDto MysteryShoppingLinks(MysteryShoppingLinksArgumentsDto arguments, string apiKey)
        {
            if (apiKey != API_KEY)
            {
                this.Context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;

                return null;
            }

            ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();

            ICommand<MysteryShoppingLinksResultDto, MysteryShoppingLinksArgumentsDto> command =
                factory.CreateMysteryShoppingLinksCommand();

            return ExecuteCommand(command, arguments);
        }

        /// <summary>
        /// Get the mystery shopping links given ownerEntity, vehicleEntity.  Looks up handles and calls MysteryShoppingLinks().
        /// </summary>
        /// <param name="arguments"></param>
        /// <returns></returns>
        [WebMethod]
        public MysteryShoppingUrlsDto MysteryShoppingLinksNoHandles(MysteryShoppingParametersDto arguments, string apiKey)
        {
            if (apiKey != API_KEY)
            {
                this.Context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;

                return null;
            }

            // call GetHandles() to get the search handle, owner handle, vehicle handle
            var handleDto = new HandleDto();
            handleDto.OwnerEntityId = arguments.OwnerEntityId;
            handleDto.OwnerEntityTypeId = arguments.OwnerEntityTypeId;
            handleDto.VehicleEntityId = arguments.VehicleEntityId;
            handleDto.VehicleEntityTypeId = arguments.VehicleEntityTypeId;
            handleDto.UserId = arguments.UserId;

            var handles = GetHandles(handleDto, apiKey);

            string oh = handles.OwnerHandle;
            string sh = handles.SearchHandle;
            string vh = handles.VehicleHandle;

            // Look up model year.  Find a faster way to do this.
            var pricingInfo = DataPortal.Fetch<PricingInformation>(new PricingInformation.Criteria(oh, vh, sh, 1));
            var modelYear = pricingInfo.ModelYear;

            // Use existing command to construct urls.
            var links = MysteryShoppingLinks(
                new MysteryShoppingLinksArgumentsDto
                        {   
                            ModelYear = modelYear, 
                            OwnerHandle = oh,
                            SearchHandle = sh,
                            SearchRadius = arguments.SearchRadius, 
                            VehicleHandle = vh
                        }, apiKey);

            var mysteryShoppingUrlsDto = new MysteryShoppingUrlsDto();
            mysteryShoppingUrlsDto.AutoTraderUrl = links.AutoTraderUrl;
            mysteryShoppingUrlsDto.CarSoupUrl = links.CarSoupUrl;
            mysteryShoppingUrlsDto.CarsUrl = links.CarsDotComUrl;

            return mysteryShoppingUrlsDto;
        }

        [WebMethod]
        public void SaveVehiclePricingDecisionInput(VehiclePricingDecisionInputDto arguments, string apiKey)
        {
            if (apiKey != API_KEY)
            {
                this.Context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;

                return;
            }

            ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();

            ICommand<VehiclePricingDecisionInputDto> command =
                factory.CreateSaveVehiclePricingDecisionInputCommand();

            command.Execute(arguments);          
        } 

        [WebMethod]
        public VehiclePricingDecisionInputDto GetVehiclePricingDecisionInput(VehiclePricingDecisionInputDto arguments, string apiKey)
        {
            if (apiKey != API_KEY)
            {
                this.Context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;

                return null;
            }

            ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();

            ICommand<VehiclePricingDecisionInputDto, VehiclePricingDecisionInputDto> command =
                factory.CreateGetVehiclePricingDecisionInputCommand();

            return ExecuteCommand(command, arguments);
        }

        [WebMethod]
        public HandleDto GetHandles(HandleDto arguments, string apiKey)
        {
            if (apiKey != API_KEY)
            {
                this.Context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;

                return null;
            }

            ICommandFactory factory = GetResolver().Resolve<ICommandFactory>();

            ICommand<HandleDto, HandleDto> command =
                factory.CreateHandleLookupCommand();

            return ExecuteCommand(command, arguments);
        }

        

        #region Helper

        protected static IResolver GetResolver()
        {
            IResolver resolver = RegistryFactory.GetResolver();

            return resolver;
        }

        protected static TResult ExecuteCommand<TResult, TParameter>(ICommand<TResult, TParameter> command, TParameter args)
        {
            ICommandExecutor executor = GetResolver().Resolve<ICommandExecutor>();

            ICommandResult<TResult> result = executor.Execute(command, args);

            return result.Result;
        }

        #endregion
    }
}
