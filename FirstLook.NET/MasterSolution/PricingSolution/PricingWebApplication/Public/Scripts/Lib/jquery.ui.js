
/* Copyright (c) 2007 Paul Bakaus (paul.bakaus@googlemail.com) and Brandon Aaron (brandon.aaron@gmail.com || http://brandonaaron.net)
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 *
 * $LastChangedDate: 2007-11-14 20:20:00 +0000 (Wed, 14 Nov 2007) $
 * $Rev: 3824 $
 *
 * Version: @VERSION
 *
 * Requires: jQuery 1.2+
 */

(function($){
	
$.dimensions = {
	version: '@VERSION'
};

// Create innerHeight, innerWidth, outerHeight and outerWidth methods
$.each( [ 'Height', 'Width' ], function(i, name){
	
	// innerHeight and innerWidth
	$.fn[ 'inner' + name ] = function() {
		if (!this[0]) return;
		
		var torl = name == 'Height' ? 'Top'    : 'Left',  // top or left
		    borr = name == 'Height' ? 'Bottom' : 'Right'; // bottom or right
		
		return this.is(':visible') ? this[0]['client' + name] : num( this, name.toLowerCase() ) + num(this, 'padding' + torl) + num(this, 'padding' + borr);
	};
	
	// outerHeight and outerWidth
	$.fn[ 'outer' + name ] = function(options) {
		if (!this[0]) return;
		
		var torl = name == 'Height' ? 'Top'    : 'Left',  // top or left
		    borr = name == 'Height' ? 'Bottom' : 'Right'; // bottom or right
		
		options = $.extend({ margin: false }, options || {});
		
		var val = this.is(':visible') ? 
				this[0]['offset' + name] : 
				num( this, name.toLowerCase() )
					+ num(this, 'border' + torl + 'Width') + num(this, 'border' + borr + 'Width')
					+ num(this, 'padding' + torl) + num(this, 'padding' + borr);
		
		return val + (options.margin ? (num(this, 'margin' + torl) + num(this, 'margin' + borr)) : 0);
	};
});

// Create scrollLeft and scrollTop methods
$.each( ['Left', 'Top'], function(i, name) {
	$.fn[ 'scroll' + name ] = function(val) {
		if (!this[0]) return;
		
		return val != undefined ?
		
			// Set the scroll offset
			this.each(function() {
				this == window || this == document ?
					window.scrollTo( 
						name == 'Left' ? val : $(window)[ 'scrollLeft' ](),
						name == 'Top'  ? val : $(window)[ 'scrollTop'  ]()
					) :
					this[ 'scroll' + name ] = val;
			}) :
			
			// Return the scroll offset
			this[0] == window || this[0] == document ?
				self[ (name == 'Left' ? 'pageXOffset' : 'pageYOffset') ] ||
					$.boxModel && document.documentElement[ 'scroll' + name ] ||
					document.body[ 'scroll' + name ] :
				this[0][ 'scroll' + name ];
	};
});

$.fn.extend({
	position: function() {
		var left = 0, top = 0, elem = this[0], offset, parentOffset, offsetParent, results;
		
		if (elem) {
			// Get *real* offsetParent
			offsetParent = this.offsetParent();
			
			// Get correct offsets
			offset       = this.offset();
			parentOffset = offsetParent.offset();
			
			// Subtract element margins
			offset.top  -= num(elem, 'marginTop');
			offset.left -= num(elem, 'marginLeft');
			
			// Add offsetParent borders
			parentOffset.top  += num(offsetParent, 'borderTopWidth');
			parentOffset.left += num(offsetParent, 'borderLeftWidth');
			
			// Subtract the two offsets
			results = {
				top:  offset.top  - parentOffset.top,
				left: offset.left - parentOffset.left
			};
		}
		
		return results;
	},
	
	offsetParent: function() {
		var offsetParent = this[0].offsetParent;
		while ( offsetParent && (!/^body|html$/i.test(offsetParent.tagName) && $.css(offsetParent, 'position') == 'static') )
			offsetParent = offsetParent.offsetParent;
		return $(offsetParent);
	}
});

function num(el, prop) {
	return parseInt($.css(el.jquery?el[0]:el,prop))||0;
};

})(jQuery);




// jQuery UI - Mouse ----------------------------------------------- //
(function($) {

//If the UI scope is not availalable, add it
$.ui = $.ui || {};

//Add methods that are vital for all mouse interaction stuff (plugin registering)
$.extend($.ui, {
	plugin: {
		add: function(w, c, o, p) {
			var a = $.ui[w].prototype; if(!a.plugins[c]) a.plugins[c] = [];
			a.plugins[c].push([o,p]);
		},
		call: function(instance, name, arguments) {
			var c = instance.plugins[name]; if(!c) return;
			var o = instance.interaction ? instance.interaction.options : instance.options;
			var e = instance.interaction ? instance.interaction.element : instance.element;
			
			for (var i = 0; i < c.length; i++) {
				if (o[c[i][0]]) c[i][1].apply(e, arguments);
			}	
		}	
	}
});

$.fn.mouseInteractionDestroy = function() {
	this.each(function() {
		if($.data(this, "ui-mouse")) $.data(this, "ui-mouse").destroy(); 	
	});
}

$.ui.mouseInteraction = function(el,o) {

	if(!o) var o = {};
	this.element = el;
	$.data(this.element, "ui-mouse", this);
	
	this.options = {};
	$.extend(this.options, o);
	$.extend(this.options, {
		handle : o.handle ? ($(o.handle, el)[0] ? $(o.handle, el) : $(el)) : $(el),
		helper: o.helper || 'original',
		preventionDistance: o.preventionDistance || 0,
		dragPrevention: o.dragPrevention ? o.dragPrevention.toLowerCase().split(',') : ['input','textarea','button','select','option'],
		cursorAt: { top: ((o.cursorAt && o.cursorAt.top) ? o.cursorAt.top : 0), left: ((o.cursorAt && o.cursorAt.left) ? o.cursorAt.left : 0), bottom: ((o.cursorAt && o.cursorAt.bottom) ? o.cursorAt.bottom : 0), right: ((o.cursorAt && o.cursorAt.right) ? o.cursorAt.right : 0) },
		cursorAtIgnore: (!o.cursorAt) ? true : false, //Internal property
		appendTo: o.appendTo || 'parent'			
	})
	o = this.options; //Just Lazyness
	
	if(!this.options.nonDestructive && (o.helper == 'clone' || o.helper == 'original')) {

		// Let's save the margins for better reference
		o.margins = {
			top: parseInt($(el).css('marginTop')) || 0,
			left: parseInt($(el).css('marginLeft')) || 0,
			bottom: parseInt($(el).css('marginBottom')) || 0,
			right: parseInt($(el).css('marginRight')) || 0
		};

		// We have to add margins to our cursorAt
		if(o.cursorAt.top != 0) o.cursorAt.top = o.margins.top;
		if(o.cursorAt.left != 0) o.cursorAt.left += o.margins.left;
		if(o.cursorAt.bottom != 0) o.cursorAt.bottom += o.margins.bottom;
		if(o.cursorAt.right != 0) o.cursorAt.right += o.margins.right;
		
		
		if(o.helper == 'original')
			o.wasPositioned = $(el).css('position');
		
	} else {
		o.margins = { top: 0, left: 0, right: 0, bottom: 0 };
	}
	
	var self = this;
	this.mousedownfunc = function(e) { // Bind the mousedown event
		return self.click.apply(self, [e]);	
	}
	o.handle.bind('mousedown', this.mousedownfunc);
	
	//Prevent selection of text when starting the drag in IE
	if($.browser.msie) $(this.element).attr('unselectable', 'on');
	
}

$.extend($.ui.mouseInteraction.prototype, {
	plugins: {},
	currentTarget: null,
	lastTarget: null,
	timer: null,
	slowMode: false,
	init: false,
	destroy: function() {
		this.options.handle.unbind('mousedown', this.mousedownfunc);
	},
	trigger: function(e) {
		return this.click.apply(this, arguments);
	},
	click: function(e) {

		var o = this.options;
		
		window.focus();
		if(e.which != 1) return true; //only left click starts dragging
	
		// Prevent execution on defined elements
		var targetName = (e.target) ? e.target.nodeName.toLowerCase() : e.srcElement.nodeName.toLowerCase();
		for(var i=0;i<o.dragPrevention.length;i++) {
			if(targetName == o.dragPrevention[i]) return true;
		}

		//Prevent execution on condition
		if(o.startCondition && !o.startCondition.apply(this, [e])) return true;

		var self = this;
		this.mouseup = function(e) { return self.stop.apply(self, [e]); }
		this.mousemove = function(e) { return self.drag.apply(self, [e]); }

		var initFunc = function() { //This function get's called at bottom or after timeout
			$(document).bind('mouseup', self.mouseup);
			$(document).bind('mousemove', self.mousemove);
			self.opos = [e.pageX,e.pageY]; // Get the original mouse position
		}
		
		if(o.preventionTimeout) { //use prevention timeout
			if(this.timer) clearInterval(this.timer);
			this.timer = setTimeout(function() { initFunc(); }, o.preventionTimeout);
			return false;
		}
	
		initFunc();
		return false;
		
	},
	start: function(e) {
		
		var o = this.options; var a = this.element;
		o.co = $(a).offset(); //get the current offset
			
		this.helper = typeof o.helper == 'function' ? $(o.helper.apply(a, [e,this]))[0] : (o.helper == 'clone' ? $(a).clone()[0] : a);

		if(o.appendTo == 'parent') { // Let's see if we have a positioned parent
			var cp = a.parentNode;
			while (cp) {
				if(cp.style && ($(cp).css('position') == 'relative' || $(cp).css('position') == 'absolute')) {
					o.pp = cp;
					o.po = $(cp).offset();
					o.ppOverflow = !!($(o.pp).css('overflow') == 'auto' || $(o.pp).css('overflow') == 'scroll'); //TODO!
					break;	
				}
				cp = cp.parentNode ? cp.parentNode : null;
			};
			
			if(!o.pp) o.po = { top: 0, left: 0 };
		}
		
		this.pos = [this.opos[0],this.opos[1]]; //Use the relative position
		this.rpos = [this.pos[0],this.pos[1]]; //Save the absolute position
		
		if(o.cursorAtIgnore) { // If we want to pick the element where we clicked, we borrow cursorAt and add margins
			o.cursorAt.left = this.pos[0] - o.co.left + o.margins.left;
			o.cursorAt.top = this.pos[1] - o.co.top + o.margins.top;
		}



		if(o.pp) { // If we have a positioned parent, we pick the draggable relative to it
			this.pos[0] -= o.po.left;
			this.pos[1] -= o.po.top;
		}
		
		this.slowMode = (o.cursorAt && (o.cursorAt.top-o.margins.top > 0 || o.cursorAt.bottom-o.margins.bottom > 0) && (o.cursorAt.left-o.margins.left > 0 || o.cursorAt.right-o.margins.right > 0)) ? true : false; //If cursorAt is within the helper, set slowMode to true
		
		if(!o.nonDestructive) $(this.helper).css('position', 'absolute');
		if(o.helper != 'original') $(this.helper).appendTo((o.appendTo == 'parent' ? a.parentNode : o.appendTo)).show();

		// Remap right/bottom properties for cursorAt to left/top
		if(o.cursorAt.right && !o.cursorAt.left) o.cursorAt.left = this.helper.offsetWidth+o.margins.right+o.margins.left - o.cursorAt.right;
		if(o.cursorAt.bottom && !o.cursorAt.top) o.cursorAt.top = this.helper.offsetHeight+o.margins.top+o.margins.bottom - o.cursorAt.bottom;
	
		this.init = true;	

		if(o._start) o._start.apply(a, [this.helper, this.pos, o.cursorAt, this, e]); // Trigger the start callback
		this.helperSize = { width: $(this.helper).outerWidth(), height: $(this.helper).outerHeight() }; //Set helper size property
		return false;
					
	},
	stop: function(e) {			
		
		var o = this.options; var a = this.element; var self = this;

		$(document).unbind('mouseup', self.mouseup);
		$(document).unbind('mousemove', self.mousemove);

		if(this.init == false) return this.opos = this.pos = null;
		if(o._beforeStop) o._beforeStop.apply(a, [this.helper, this.pos, o.cursorAt, this, e]);

		if(this.helper != a && !o.beQuietAtEnd) { // Remove helper, if it's not the original node
			$(this.helper).remove(); this.helper = null;
		}
		
		if(!o.beQuietAtEnd) {
			//if(o.wasPositioned)	$(a).css('position', o.wasPositioned);
			if(o._stop) o._stop.apply(a, [this.helper, this.pos, o.cursorAt, this, e]);
		}

		this.init = false;
		this.opos = this.pos = null;
		return false;
		
	},
	drag: function(e) {

		if (!this.opos || ($.browser.msie && !e.button)) return this.stop.apply(this, [e]); // check for IE mouseup when moving into the document again
		var o = this.options;
		
		this.pos = [e.pageX,e.pageY]; //relative mouse position
		if(this.rpos && this.rpos[0] == this.pos[0] && this.rpos[1] == this.pos[1]) return false;
		this.rpos = [this.pos[0],this.pos[1]]; //absolute mouse position
		
		if(o.pp) { //If we have a positioned parent, use a relative position
			this.pos[0] -= o.po.left;
			this.pos[1] -= o.po.top;	
		}
		
		if( (Math.abs(this.rpos[0]-this.opos[0]) > o.preventionDistance || Math.abs(this.rpos[1]-this.opos[1]) > o.preventionDistance) && this.init == false) //If position is more than x pixels from original position, start dragging
			this.start.apply(this,[e]);			
		else {
			if(this.init == false) return false;
		}
	
		if(o._drag) o._drag.apply(this.element, [this.helper, this.pos, o.cursorAt, this, e]);
		return false;
		
	}
});

})(jQuery);





// jQuery UI - Slider ----------------------------------------------- //
(function($) {

//Web Forms 2.0
if(window['webforms']) {
	$(document).ready(function() {
		
		$("input").each(function() {
			if(this.getAttribute("type") == "range") {
				var cur = $(this);
				var slider = $("<div class='ui-slider'></div>").css({ width: cur.innerWidth()+"px", height: cur.innerHeight()+"px" }).insertAfter(cur);
				var handle = $("<div class='ui-slider-handle'></div>").appendTo(slider);


				slider.css({
					"position": cur.css("position") == "absolute" ? "absolute" : "relative",
					"left": cur.css("left"),
					"right": cur.css("right"),
					"zIndex": cur.css("zIndex"),
					"float": cur.css("float"),
					"clear": cur.css("clear")
				});
				cur.css({ position: "absolute", opacity: 0, top: "-1000px", left: "-1000px" });
				
				slider.slider({
					maxValue: cur.attr("max"),
					minValue: cur.attr("min"),
					startValue: this.getAttribute("value"),
					stepping: cur.attr("step"),
					change: function(e, ui) { cur[0].value = ui.value; cur[0].setAttribute("value", ui.value); }
				});
				
				slider = slider.sliderInstance();
				
				cur.bind("keydown", function(e) {
					var o = slider.interaction.options;
					switch(e.keyCode) {
						case 37:
							slider.moveTo(slider.interaction.curValue+o.minValue-(o.stepping || 1));
							break;
						case 39:
							slider.moveTo(slider.interaction.curValue+o.minValue+(o.stepping || 1));
							break;	
					}
					if(e.keyCode != 9) return false;
				});
				
			};	
		});
			
	});
}

//Make nodes selectable by expression
$.extend($.expr[':'], { slider: "(' '+a.className+' ').indexOf(' ui-slider ') != -1" });

$.fn.slider = function(o) {
	return this.each(function() {
		new $.ui.slider(this, o);
	});
}

//Macros for external methods that support chaining
var methods = "destroy,enable,disable,moveTo".split(",");
for(var i=0;i<methods.length;i++) {
	var cur = methods[i], f;
	eval('f = function() { var a = arguments; return this.each(function() { if(jQuery(this).is(".ui-slider")) jQuery.data(this, "ui-slider")["'+cur+'"](a); }); }');
	$.fn["slider"+cur.substr(0,1).toUpperCase()+cur.substr(1)] = f;
};

//get instance method
$.fn.sliderInstance = function() {
	if($(this[0]).is(".ui-slider")) return $.data(this[0], "ui-slider");
	return false;
};

$.ui.slider = function(el, o) {
	
	var options = {};
	o = o || {};
	$.extend(options, o);
	$.extend(options, {
		axis: o.axis || (el.offsetWidth < el.offsetHeight ? 'vertical' : 'horizontal'),
		maxValue: parseInt(o.maxValue) || 100,
		minValue: parseInt(o.minValue) || 0,
		startValue: parseInt(o.startValue) || 0,
		_start: function(h, p, c, t, e) {
			self.start.apply(t, [self, e]); // Trigger the start callback				
		},
		_beforeStop: function(h, p, c, t, e) {
			self.stop.apply(t, [self, e]); // Trigger the start callback
		},
		_drag: function(h, p, c, t, e) {
			self.drag.apply(t, [self, e]); // Trigger the start callback
		},
		startCondition: function() {
			return !self.disabled;
		}			
	});

	var self = this;
	var o = options;
	$.data(el, "ui-slider", this);
	o.stepping = parseInt(o.stepping) || (o.steps ? o.maxValue/o.steps : 0);
	o.realValue = (o.maxValue - o.minValue);


	this.handle = options.handle ? $(options.handle, el) : $('.ui-slider-handle', el);
	if(this.handle.length == 1) {
		this.interaction = new $.ui.mouseInteraction(this.handle[0], options);
		this.multipleHandles = false;
	} else {
		this.interactions = [];
		this.handle.each(function() {
			self.interactions.push(new $.ui.mouseInteraction(this, options));
		});
		this.multipleHandles = true;
	}
	
	this.element = el;
	$(this.element).addClass("ui-slider");
	
	
	if(o.axis == 'horizontal') {
		this.parentSize = $(this.element).outerWidth() - this.handle.outerWidth();
		this.prop = 'left';
	}
	
	if(o.axis == 'vertical') {
		this.parentSize = $(this.element).outerHeight() - this.handle.outerHeight();
		this.prop = 'top';
	}
	
	if(!this.multipleHandles) {
		$(el).bind('click', function(e) { self.click.apply(self, [e]); });
		if(!isNaN(o.startValue)) this.moveTo(o.startValue,options.realValue, null, false);
	}
	
}

$.extend($.ui.slider.prototype, {
	currentTarget: null,
	lastTarget: null,
	destroy: function() {
		$(this.element).removeClass("ui-slider").removeClass("ui-slider-disabled");
		this.interaction.destroy();
	},
	enable: function() {
		$(this.element).removeClass("ui-slider-disabled");
		this.disabled = false;
	},
	disable: function() {
		$(this.element).addClass("ui-slider-disabled");
		this.disabled = true;
	},
	nonvalidRange: function(self) {

		for(var i=0;i<this.interactions.length;i++) {
			if(self == this.interactions[i]) {
				if(this.interactions[i-1]) {
					if(this.interactions[i-1].curValue > this.interactions[i].curValue) return this.interactions[i-1].curValue;
				}
				
				if(this.interactions[i+1]) {
					if(this.interactions[i+1].curValue < this.interactions[i].curValue) return this.interactions[i+1].curValue;
				}
			}
		}
		
		return false;
		
	},
	prepareCallbackObj: function(self,m) {
		
		var cur = this;
		var func = function() {
			var retVal = [];
			for(var i=0;i<cur.interactions.length;i++) {
				retVal.push((cur.interactions[i].curValue || 0)+self.options.minValue);
			}
			return retVal;
		};
		
		return {
			handle: self.helper,
			pixel: m,
			value: self.curValue+self.options.minValue,
			values: this.multipleHandles ? func() : self.curValue+self.options.minValue,
			slider: self	
		}			
	},
	click: function(e) {
		var o = this.interaction.options;
		var pointer = [e.pageX,e.pageY];
		var offset = $(this.interaction.element).offsetParent().offset({ border: false });
		if(this.interaction.element == e.target || this.disabled) return;
		
		this.interaction.pickValue = this.interaction.curValue;
		this.drag.apply(this.interaction, [this, e, [pointer[0]-offset.left-this.handle[0].offsetWidth/2,pointer[1]-offset.top-this.handle[0].offsetHeight/2]]);
		
		if(this.interaction.pickValue != this.interaction.curValue)
			$(this.element).triggerHandler("slidechange", [e, this.prepareCallbackObj(this.interaction)], o.change);
			
	},
	start: function(that, e) {
		
		var o = this.options;
		$(that.element).triggerHandler("slidestart", [e, that.prepareCallbackObj(this)], o.start);
		this.pickValue = this.curValue;
		
		return false;
					
	},
	stop: function(that, e) {			
		
		var o = this.options;
		$(that.element).triggerHandler("slidestop", [e, that.prepareCallbackObj(this)], o.stop);
		if(this.pickValue != this.curValue) $(that.element).triggerHandler("slidechange", [e, that.prepareCallbackObj(this)], o.change);

		return false;
		
	},
	drag: function(that, e, pos) {

		var o = this.options;
		this.pos = pos || [this.pos[0]-this.element.offsetWidth/2, this.pos[1]-this.element.offsetHeight/2];
		
		if(o.axis == 'horizontal') var m = this.pos[0];
		if(o.axis == 'vertical')   var m = this.pos[1];
		
		
		var p = that.parentSize;
		var prop = that.prop;

		if(m < 0) m = 0;
		if(m > p) m = p;

		this.curValue = (Math.round((m/p)*o.realValue));
		if(o.stepping) {
			this.curValue = Math.round(this.curValue/o.stepping)*o.stepping;
			m = ((this.curValue)/o.realValue) * p;
		}
		
		if(that.interactions) {
			nonvalidRange = that.nonvalidRange(this);
			if(nonvalidRange) {
				this.curValue = nonvalidRange;
				m = ((this.curValue)/o.realValue) * p;
			}
		}
		
		$(this.element).css(prop, m+'px');
		$(that.element).triggerHandler("slide", [e, that.prepareCallbackObj(this,m)], o.slide);
		return false;
		
	},
	moveTo: function(value,scale,changeslide,p) {	// renamed from goto to moveTo as goto is reserved javascript word
		
		if(this.multipleHandles) return false; //TODO: Multiple handle moveTo function
		
		var o = this.interaction.options;
		var offset = $(this.interaction.element).offsetParent().offset({ border: false });
		this.interaction.pickValue = this.interaction.curValue;
		value = value-o.minValue;
		
		var modifier = scale || o.realValue;
		
		if(!p) var p = this.parentSize;
		var prop = this.prop;
		
		var m = Math.round(((value)/modifier) * p);

		if(m < 0) m = 0;
		if(m > p) m = p;
		
		this.interaction.curValue = (Math.round((m/p)*o.realValue));
		if(o.stepping) {
			this.interaction.curValue = Math.round(this.interaction.curValue/o.stepping)*o.stepping;
			m = ((this.interaction.curValue)/o.realValue) * p;
		}

		$(this.interaction.element).css(prop, m+'px');
		
		if(!changeslide && this.interaction.pickValue != this.interaction.curValue && !p)
			$(this.element).triggerHandler("slidechange", [null, this.prepareCallbackObj(this.interaction)], o.change);
		
		if(changeslide)
			$(this.element).triggerHandler("slide", [null, this.prepareCallbackObj(this.interaction)], o.slide);

	}
});

})(jQuery);




// jQuery UI - Dialog ----------------------------------------------- //
(function($)
{
	//If the UI scope is not availalable, add it
	$.ui = $.ui || {};

	$.fn.dialog = function(o) {
		return this.each(function() {
			if (!$(this).is(".ui-dialog")) new $.ui.dialog(this, o);
		});
	}
	$.fn.dialogOpen = function() {
		return this.each(function() {
			var contentEl;
			if ($(this).parents(".ui-dialog").length) contentEl = this;
			if (!contentEl && $(this).is(".ui-dialog")) contentEl = $('.ui-dialog-content', this)[0];
			$.ui.dialogOpen(contentEl)
		});
	}
	$.fn.dialogClose = function() {
		return this.each(function() {
			var contentEl;
			if ($(this).parents(".ui-dialog").length) contentEl = this;
			if (!contentEl && $(this).is(".ui-dialog")) contentEl = $('.ui-dialog-content', this)[0];
			$.ui.dialogClose(contentEl);
		});
	}
	
	$.ui.dialog = function(el, o) {
		
		var options = {
			width: 300,
			height: 200,
			position: 'center',
			buttons: [],
			modal: false,
			drag: true,
			resize: true,
			shadow: false, // It's quite slow
			parent: "form:first"
		};
		var o = o || {}; $.extend(options, o); //Extend and copy options
		this.element = el; var self = this; //Do bindings
		$.data(this.element, "ui-dialog", this);

		var uiDialogContent = $(el).addClass('ui-dialog-content')
			.wrap(document.createElement('div'))
			.wrap(document.createElement('div'));
		var uiDialogContainer = uiDialogContent.parent().addClass('ui-dialog-container').css({position: 'relative'});
		var uiDialog = uiDialogContainer.parent()
			.addClass('ui-dialog').addClass(uiDialogContent.attr('className'))
			.css({position: 'absolute', width: options.width, height: options.height});
    
        if (options.modal == false && options.resize == true) {
		    uiDialog.append("<div class='ui-resizable-n ui-resizable-handle'></div>")
                .append("<div class='ui-resizable-s ui-resizable-handle'></div>")
                .append("<div class='ui-resizable-e ui-resizable-handle'></div>")
                .append("<div class='ui-resizable-w ui-resizable-handle'></div>")
                .append("<div class='ui-resizable-ne ui-resizable-handle'></div>")
                .append("<div class='ui-resizable-se ui-resizable-handle'></div>")
                .append("<div class='ui-resizable-sw ui-resizable-handle'></div>")
                .append("<div class='ui-resizable-nw ui-resizable-handle'></div>");
      
		    uiDialog.resizable();
		}

		uiDialogContainer.prepend('<div class="ui-dialog-titlebar"></div>');
		var uiDialogTitlebar = $('.ui-dialog-titlebar', uiDialogContainer);
		var title = (options.title) ? options.title : (uiDialogContent.attr('title')) ? uiDialogContent.attr('title') : '';
		uiDialogTitlebar.append('<span class="ui-dialog-title">' + title + '</span>');
		uiDialogTitlebar.append('<div class="ui-dialog-titlebar-close"></div>');
		$('.ui-dialog-titlebar-close', uiDialogTitlebar)
			.hover(function() { $(this).addClass('ui-dialog-titlebar-close-hover'); }, 
			       function() { $(this).removeClass('ui-dialog-titlebar-close-hover'); })
			.mousedown(function(ev) {
				ev.stopPropagation();
			})
			.click(function() {
				self.close();
			});
		var l = 0;
		$.each(options.buttons, function() { l = 1; return false; });
        if (l == 1) {
		    uiDialog.append('<div class="ui-dialog-buttonpane"></div>');
		    var uiDialogButtonPane = $('.ui-dialog-buttonpane', uiDialog);
		    $.each(options.buttons, function(name, value) {
		    	var btn = $(document.createElement('button')).text(name).click(value);
		    	uiDialogButtonPane.append(btn);
		    });
		}
        
        if (options.modal == false && options.drag == true) {
		    uiDialog.draggable({ handle: '.ui-dialog-titlebar' });
        }
        
		this.open = function() {
			var wnd = $(window), top = 0, left = 0;
			switch (options.position) {
				case 'center':
					top = (wnd.height() / 2) - (uiDialog.height() / 2);
					left = (wnd.width() / 2) - (uiDialog.width() / 2);
					break;
				case 'left':
				    top = (wnd.height() / 2) - (uiDialog.height() / 2);
				    left = 0;
				    break;
				case 'top':
    			    top = 0;
					left = (wnd.width() / 2) - (uiDialog.width() / 2);
					break;
			}
			uiDialog.css({top: top, left: left});
			uiDialog.appendTo(options.parent).show();
		};

		this.close = function() {
			uiDialog.hide();
		};

		uiDialog.show();
		this.open();
        if (options.shadow && $.fn.shadow != undefined) {
            uiDialog.shadow();
        }
	}

	$.ui.dialogOpen = function(el) {
		$.data(el, "ui-dialog").open();
	}

	$.ui.dialogClose = function(el) {
		$.data(el, "ui-dialog").close();
	}


})(jQuery);




// jQuery UI - Resizeable ----------------------------------------------- //
(function($) {

	//Make nodes selectable by expression
	$.extend($.expr[':'], { resizable: "(' '+a.className+' ').indexOf(' ui-resizable ')" });

	
	$.fn.resizable = function(o) {
		return this.each(function() {
			if(!$(this).is(".ui-resizable")) new $.ui.resizable(this,o);	
		});
	}

	//Macros for external methods that support chaining
	var methods = "destroy,enable,disable".split(",");
	for(var i=0;i<methods.length;i++) {
		var cur = methods[i], f;
		eval('f = function() { var a = arguments; return this.each(function() { if(jQuery(this).is(".ui-resizable")) jQuery.data(this, "ui-resizable")["'+cur+'"](a); if(jQuery(this.parentNode).is(".ui-resizable")) jQuery.data(this, "ui-resizable")["'+cur+'"](a); }); }');
		$.fn["resizable"+cur.substr(0,1).toUpperCase()+cur.substr(1)] = f;
	};
	
	//get instance method
	$.fn.resizableInstance = function() {
		if($(this[0]).is(".ui-resizable") || $(this[0].parentNode).is(".ui-resizable")) return $.data(this[0], "ui-resizable");
		return false;
	};
	
	
	$.ui.resizable = function(el,o) {
		
		var options = {}; o = o || {}; $.extend(options, o); //Extend and copy options
		this.element = el; var self = this; //Do bindings
		$.data(this.element, "ui-resizable", this);
		
		if(options.proxy) {
			var helper = function(e,that) {
				var helper = $('<div></div>').css({
					width: $(this).width(),
					height: $(this).height(),
					position: 'absolute',
					left: that.options.co.left,
					top: that.options.co.top
				}).addClass(that.options.proxy);
				return helper;
			}	
		} else {
			var helper = "original";	
		}
		
		//Destructive mode wraps the original element
		if(el.nodeName.match(/textarea|input|select|button|img/i)) options.destructive = true;
		if(options.destructive) {
			
			$(el).wrap('<div class="ui-wrapper"  style="position: relative; width: '+$(el).outerWidth()+'px; height: '+$(el).outerHeight()+';"></div>');
			var oel = el;
			el = el.parentNode; this.element = el;
			
			//Move margins to the wrapper
			$(el).css({ marginLeft: $(oel).css("marginLeft"), marginTop: $(oel).css("marginTop"), marginRight: $(oel).css("marginRight"), marginBottom: $(oel).css("marginBottom")});
			$(oel).css({ marginLeft: 0, marginTop: 0, marginRight: 0, marginBottom: 0});
			
			o.proportionallyResize = o.proportionallyResize || [];
			o.proportionallyResize.push(oel);
			
			var b = [parseInt($(oel).css('borderTopWidth')),parseInt($(oel).css('borderRightWidth')),parseInt($(oel).css('borderBottomWidth')),parseInt($(oel).css('borderLeftWidth'))];
		} else {
			var b = [0,0,0,0];	
		}
		
		if(options.destructive || !$(".ui-resizable-handle",el).length) {
			//Adding handles (disabled not so common ones)
			var t = function(a,b) { $(el).append("<div class='ui-resizable-"+a+" ui-resizable-handle' style='"+b+"'></div>"); };
			//t('n','top: '+b[0]+'px;');
			t('e','right: '+b[1]+'px;'+(options.zIndex ? 'z-index: '+options.zIndex+';' : ''));
			t('s','bottom: '+b[1]+'px;'+(options.zIndex ? 'z-index: '+options.zIndex+';' : ''));
			//t('w','left: '+b[3]+'px;');
			t('se','bottom: '+b[2]+'px; right: '+b[1]+'px;'+(options.zIndex ? 'z-index: '+options.zIndex+';' : ''));
			//t('sw','bottom: '+b[2]+'px; left: '+b[3]+'px;');
			//t('ne','top: '+b[0]+'px; right: '+b[1]+'px;');
			//t('nw','top: '+b[0]+'px; left: '+b[3]+'px;');
		}
		
		
		
		//If other elements should be modified, we have to copy that array
		options.modifyThese = [];
		if(o.proportionallyResize) {
			options.proportionallyResize = o.proportionallyResize.slice(0);
			var propRes = options.proportionallyResize;

			for(var i in propRes) {
				
				if(propRes[i].constructor == String)
					propRes[i] = $(propRes[i], el);
				
				if(!$(propRes[i]).length) continue;
				
				
				var x = $(propRes[i]).width() - $(el).width();
				var y = $(propRes[i]).height() - $(el).height();
				options.modifyThese.push([$(propRes[i]),x,y]);
			}

		}
		
		options.handles = {};
		if(!o.handles) o.handles = { n: '.ui-resizable-n', e: '.ui-resizable-e', s: '.ui-resizable-s', w: '.ui-resizable-w', se: '.ui-resizable-se', sw: '.ui-resizable-sw', ne: '.ui-resizable-ne', nw: '.ui-resizable-nw' };
		
		for(var i in o.handles) { options.handles[i] = o.handles[i]; } //Copying the object
		
		for(var i in options.handles) {
			
			if(options.handles[i].constructor == String)
				options.handles[i] = $(options.handles[i], el);
			
			if(!$(options.handles[i]).length) continue;
				
			$(options.handles[i]).bind('mousedown', function(e) {
				self.interaction.options.axis = this.resizeAxis;
			})[0].resizeAxis = i;
			
		}
		
		//If we want to auto hide the elements
		if(o.autohide)
			$(this.element).addClass("ui-resizable-autohide").hover(function() { $(this).removeClass("ui-resizable-autohide"); }, function() { if(self.interaction.options.autohide && !self.interaction.init) $(this).addClass("ui-resizable-autohide"); });
	

		$.extend(options, {
			helper: helper,
			nonDestructive: true,
			dragPrevention: 'input,button,select',
			minHeight: options.minHeight || 50,
			minWidth: options.minWidth || 100,
			startCondition: function(e) {
				if(self.disabled) return false;
				for(var i in options.handles) {
					if($(options.handles[i])[0] == e.target) return true;
				}
				return false;
			},
			_start: function(h,p,c,t,e) {
				self.start.apply(t, [self, e]); // Trigger the start callback				
			},
			_beforeStop: function(h,p,c,t,e) {
				self.stop.apply(t, [self, e]); // Trigger the stop callback
			},
			_drag: function(h,p,c,t,e) {
				self.drag.apply(t, [self, e]); // Trigger the start callback
			}			
		});
		
		//Initialize mouse interaction
		this.interaction = new $.ui.mouseInteraction(el,options);
		
		//Add the class for themeing
		$(this.element).addClass("ui-resizable");
		
	}
	
	$.extend($.ui.resizable.prototype, {
		plugins: {},
		prepareCallbackObj: function(self) {
			return {
				helper: self.helper,
				resizable: self,
				axis: self.options.axis,
				options: self.options
			}			
		},
		destroy: function() {
			$(this.element).removeClass("ui-resizable").removeClass("ui-resizable-disabled");
			this.interaction.destroy();
		},
		enable: function() {
			$(this.element).removeClass("ui-resizable-disabled");
			this.disabled = false;
		},
		disable: function() {
			$(this.element).addClass("ui-resizable-disabled");
			this.disabled = true;
		},
		start: function(that, e) {
			this.options.originalSize = [$(this.element).width(),$(this.element).height()];
			this.options.originalPosition = $(this.element).css("position");
			this.options.originalPositionValues = $(this.element).position();

			this.options.modifyThese.push([$(this.helper),0,0]);
			
			$(that.element).triggerHandler("resizestart", [e, that.prepareCallbackObj(this)], this.options.start);			
			return false;
		},
		stop: function(that, e) {			
			
			var o = this.options;

			$(that.element).triggerHandler("resizestop", [e, that.prepareCallbackObj(this)], this.options.stop);	

			if(o.proxy) {
				$(this.element).css({
					width: $(this.helper).width(),
					height: $(this.helper).height()
				});
				
				if(o.originalPosition == "absolute" || o.originalPosition == "fixed") {
					$(this.element).css({
						top: $(this.helper).css("top"),
						left: $(this.helper).css("left")
					});					
				}
			}
			return false;
			
		},
		drag: function(that, e) {

			var o = this.options;
			var rel = (o.originalPosition != "absolute" && o.originalPosition != "fixed");
			var co = rel ? o.co : this.options.originalPositionValues;
			var p = o.originalSize;

			this.pos = rel ? [this.rpos[0]-o.cursorAt.left, this.rpos[1]-o.cursorAt.top] : [this.pos[0]-o.cursorAt.left, this.pos[1]-o.cursorAt.top];

			var nw = p[0] + (this.pos[0] - co.left);
			var nh = p[1] + (this.pos[1] - co.top);
		
			if(o.axis) {
				switch(o.axis) {
					case 'e':
						nh = p[1];
						break;
					case 's':
						nw = p[0];
						break;
					case 'n':
					case 'ne':

						
						if(!o.proxy && (o.originalPosition != "absolute" && o.originalPosition != "fixed"))
							return false;
						
						if(o.axis == 'n') nw = p[0];
						var mod = (this.pos[1] - co.top); nh = nh - (mod*2);
						mod = nh <= o.minHeight ? p[1] - o.minHeight : (nh >= o.maxHeight ? 0-(o.maxHeight-p[1]) : mod);
						$(this.helper).css('top', co.top + mod);
						break;
						
					case 'w':
					case 'sw':

						if(!o.proxy && (o.originalPosition != "absolute" && o.originalPosition != "fixed"))
							return false;
						
						if(o.axis == 'w') nh = p[1];
						var mod = (this.pos[0] - co.left); nw = nw - (mod*2);
						mod = nw <= o.minWidth ? p[0] - o.minWidth : (nw >= o.maxWidth ? 0-(o.maxWidth-p[0]) : mod);
						$(this.helper).css('left', co.left + mod);
						break;
						
					case 'nw':
						
						if(!o.proxy && (o.originalPosition != "absolute" && o.originalPosition != "fixed"))
							return false;
	
						var modx = (this.pos[0] - co.left); nw = nw - (modx*2);
						modx = nw <= o.minWidth ? p[0] - o.minWidth : (nw >= o.maxWidth ? 0-(o.maxWidth-p[0]) : modx);
						
						var mody = (this.pos[1] - co.top); nh = nh - (mody*2);
						mody = nh <= o.minHeight ? p[1] - o.minHeight : (nh >= o.maxHeight ? 0-(o.maxHeight-p[1]) : mody);

						$(this.helper).css({
							left: co.left + modx,
							top: co.top + mody
						});
						
						break;
				}	
			}

			if(e.shiftKey) nh = nw * (p[1]/p[0]);
			
			if(o.minWidth) nw = nw <= o.minWidth ? o.minWidth : nw;
			if(o.minHeight) nh = nh <= o.minHeight ? o.minHeight : nh;
			
			if(o.maxWidth) nw = nw >= o.maxWidth ? o.maxWidth : nw;
			if(o.maxHeight) nh = nh >= o.maxHeight ? o.maxHeight : nh;
			
			if(e.shiftKey) nh = nw * (p[1]/p[0]);

			var modifier = $(that.element).triggerHandler("resize", [e, that.prepareCallbackObj(this)], o.resize);
			if(!modifier) modifier = {};
			
			for(var i in this.options.modifyThese) {
				var c = this.options.modifyThese[i];
				c[0].css({
					width: modifier.width ? modifier.width+c[1] : nw+c[1],
					height: modifier.height ? modifier.height+c[2] : nh+c[2]
				});
			}
			return false;
			
		}
	});

})($);





// jQuery UI - Draggable ----------------------------------------------- //
(function($) {

	//Make nodes selectable by expression
	$.extend($.expr[':'], { draggable: "(' '+a.className+' ').indexOf(' ui-draggable ')" });


	//Macros for external methods that support chaining
	var methods = "destroy,enable,disable".split(",");
	for(var i=0;i<methods.length;i++) {
		var cur = methods[i], f;
		eval('f = function() { var a = arguments; return this.each(function() { if(jQuery(this).is(".ui-draggable")) jQuery.data(this, "ui-draggable")["'+cur+'"](a); }); }');
		$.fn["draggable"+cur.substr(0,1).toUpperCase()+cur.substr(1)] = f;
	};
	
	//get instance method
	$.fn.draggableInstance = function() {
		if($(this[0]).is(".ui-draggable")) return $.data(this[0], "ui-draggable");
		return false;
	};

	$.fn.draggable = function(o) {
		return this.each(function() {
			new $.ui.draggable(this, o);
		});
	}
	
	$.ui.ddmanager = {
		current: null,
		droppables: [],
		prepareOffsets: function(t, e) {
			var dropTop = $.ui.ddmanager.dropTop = [];
			var dropLeft = $.ui.ddmanager.dropLeft;
			var m = $.ui.ddmanager.droppables;
			for (var i = 0; i < m.length; i++) {
				if(m[i].item.disabled) continue;
				m[i].offset = $(m[i].item.element).offset();
				if (t && m[i].item.options.accept(t.element)) //Activate the droppable if used directly from draggables
					m[i].item.activate.call(m[i].item, e);
			}
		},
		fire: function(oDrag, e) {
			
			var oDrops = $.ui.ddmanager.droppables;
			var oOvers = $.grep(oDrops, function(oDrop) {
				
				if (!oDrop.item.disabled && $.ui.intersect(oDrag, oDrop, oDrop.item.options.tolerance))
					oDrop.item.drop.call(oDrop.item, e);
			});
			$.each(oDrops, function(i, oDrop) {
				if (!oDrop.item.disabled && oDrop.item.options.accept(oDrag.element)) {
					oDrop.out = 1; oDrop.over = 0;
					oDrop.item.deactivate.call(oDrop.item, e);
				}
			});
		},
		update: function(oDrag, e) {
			
			if(oDrag.options.refreshPositions) $.ui.ddmanager.prepareOffsets();
			
			var oDrops = $.ui.ddmanager.droppables;
			var oOvers = $.grep(oDrops, function(oDrop) {
				if(oDrop.item.disabled) return false; 
				var isOver = $.ui.intersect(oDrag, oDrop, oDrop.item.options.tolerance)
				if (!isOver && oDrop.over == 1) {
					oDrop.out = 1; oDrop.over = 0;
					oDrop.item.out.call(oDrop.item, e);
				}
				return isOver;
			});
			$.each(oOvers, function(i, oOver) {
				if (oOver.over == 0) {
					oOver.out = 0; oOver.over = 1;
					oOver.item.over.call(oOver.item, e);
				}
			});
		}
	};
	
	$.ui.draggable = function(el, o) {
		
		var options = {};
		$.extend(options, o);
		var self = this;
		$.extend(options, {
			_start: function(h, p, c, t, e) {
				self.start.apply(t, [self, e]); // Trigger the start callback				
			},
			_beforeStop: function(h, p, c, t, e) {
				self.stop.apply(t, [self, e]); // Trigger the start callback
			},
			_drag: function(h, p, c, t, e) {
				self.drag.apply(t, [self, e]); // Trigger the start callback
			},
			startCondition: function(e) {
				return !(e.target.className.indexOf("ui-resizable-handle") != -1 || self.disabled);	
			}			
		});
		
		$.data(el, "ui-draggable", this);
		
		if (options.ghosting == true) options.helper = 'clone'; //legacy option check
		$(el).addClass("ui-draggable");
		this.interaction = new $.ui.mouseInteraction(el, options);
		
	}
	
	$.extend($.ui.draggable.prototype, {
		plugins: {},
		currentTarget: null,
		lastTarget: null,
		destroy: function() {
			$(this.interaction.element).removeClass("ui-draggable").removeClass("ui-draggable-disabled");
			this.interaction.destroy();
		},
		enable: function() {
			$(this.interaction.element).removeClass("ui-draggable-disabled");
			this.disabled = false;
		},
		disable: function() {
			$(this.interaction.element).addClass("ui-draggable-disabled");
			this.disabled = true;
		},
		prepareCallbackObj: function(self) {
			return {
				helper: self.helper,
				position: { left: self.pos[0], top: self.pos[1] },
				offset: self.options.cursorAt,
				draggable: self,
				options: self.options	
			}			
		},
		start: function(that, e) {
			
			var o = this.options;
			$.ui.ddmanager.current = this;
			
			$.ui.plugin.call(that, 'start', [e, that.prepareCallbackObj(this)]);
			$(this.element).triggerHandler("dragstart", [e, that.prepareCallbackObj(this)], o.start);
			
			if (this.slowMode && $.ui.droppable && !o.dropBehaviour)
				$.ui.ddmanager.prepareOffsets(this, e);
			
			return false;
						
		},
		stop: function(that, e) {			
			
			var o = this.options;
			
			$.ui.plugin.call(that, 'stop', [e, that.prepareCallbackObj(this)]);
			$(this.element).triggerHandler("dragstop", [e, that.prepareCallbackObj(this)], o.stop);

			if (this.slowMode && $.ui.droppable && !o.dropBehaviour) //If cursorAt is within the helper, we must use our drop manager
				$.ui.ddmanager.fire(this, e);

			$.ui.ddmanager.current = null;
			$.ui.ddmanager.last = this;

			return false;
			
		},
		drag: function(that, e) {

			var o = this.options;

			$.ui.ddmanager.update(this, e);

			this.pos = [this.pos[0]-o.cursorAt.left, this.pos[1]-o.cursorAt.top];

			$.ui.plugin.call(that, 'drag', [e, that.prepareCallbackObj(this)]);
			var nv = $(this.element).triggerHandler("drag", [e, that.prepareCallbackObj(this)], o.drag);

			var nl = (nv && nv.left) ? nv.left : this.pos[0];
			var nt = (nv && nv.top) ? nv.top : this.pos[1];
			
			$(this.helper).css('left', nl+'px').css('top', nt+'px'); // Stick the helper to the cursor
			return false;
			
		}
	});

})($);
