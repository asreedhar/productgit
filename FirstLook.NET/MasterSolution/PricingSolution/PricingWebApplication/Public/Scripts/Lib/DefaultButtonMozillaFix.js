﻿/* This method is identical to the method in the MS javascript lib except that it adds a click function to anchors in non-IE browsers */
/* This may need to be registered with the script manager in order to override default function */
function WebForm_FireDefaultButton(event, target) {
  if (event.keyCode == 13 && !(event.srcElement && (event.srcElement.tagName.toLowerCase() == "textarea"))) {
    var defaultButton;
    if (__nonMSDOMBrowser) {
      defaultButton = document.getElementById(target);
    } else {
      defaultButton = document.all[target];
    }
    /* This is the only addition to this method, the rest is identical to MS version.*/
    if (defaultButton && typeof(defaultButton.click) == "undefined") {
      defaultButton.click = function() {
        var result = true;
        if (defaultButton.onclick) 
          result = b.onclick();
        if (typeof(result) == "undefined" || result)
          eval(unescape(defaultButton.href));
      }
    }

    if (defaultButton && typeof(defaultButton.click) != "undefined") {
      defaultButton.click();
      event.cancelBubble = true;
      if (event.stopPropagation) event.stopPropagation();
      return false;
    }
  }
  return true;
}   