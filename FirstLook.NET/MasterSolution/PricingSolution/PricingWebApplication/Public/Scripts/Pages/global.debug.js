try {
	Sys.Application.add_load(OnLoad);
} catch(e) {
	$().ready(function() {
		OnLoad();
	});
}

function OnLoad() {
	// make sortble table header cells clickable
	MakeSortableTableHeaderCellsClickable();
}

function MakeSortableTableHeaderCellsClickable() {
	$("table.grid th").each(function() {
		if ($(this).children("a")) {
			// set css here instead of stylesheet to avoid giving pointer cursor to unsortable column heads.
			$(this).css("cursor", "pointer");
			$(this).click(function() {
				$("a", this).get(0).click();
			});
		}
	});
}

if (typeof Sys !== "undefined") {
	var PopupManager, OpenPopup;
	$create(FirstLook.Page.ProgressManager, null, null, null, null);
	PopupManager = $create(FirstLook.Page.PopupManager, null, null, null, null);

	PopupManager.registerWindowSize("EStock", 890, 600);
	PopupManager.registerWindowSize("Photos", 720, 500);
	PopupManager.registerWindowSize("RAP", 805, 600);
	PopupManager.registerWindowSize("PL", 805, 800);
	PopupManager.registerWindowSize("TMV", 805, 800);
	PopupManager.registerWindowSize("VPA", 1009, 830);

	OpenPopup = FirstLook.Page.PopupManager.open;
};
