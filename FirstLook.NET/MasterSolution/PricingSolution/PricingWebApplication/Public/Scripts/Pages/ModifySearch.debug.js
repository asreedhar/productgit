function ModifySearch() {
    this.updating = false;

    this.init();
};
ModifySearch.prototype = {
    init: function() {
        this._onApplicationLoadDelegate = Function.createDelegate(this, this._onApplicationLoad);

        Sys.Application.add_load(this._onApplicationLoadDelegate);

        if ( !! Sys && !!Sys.WebForms && !!Sys.WebForms.PageRequestManager) {
            var instance = Sys.WebForms.PageRequestManager.getInstance();
            this._onInitializeRequestDelegate = Function.createDelegate(this, this._onInitializeRequest);
            this._onBeginRequestDelegate = Function.createDelegate(this, this._onBeginRequest);
            this._onEndRequestDelegate = Function.createDelegate(this, this._onEndRequest);

            instance.add_initializeRequest(this._onInitializeRequestDelegate);
            instance.add_beginRequest(this._onBeginRequestDelegate);
            instance.add_endRequest(this._onEndRequestDelegate);
        }
    },
    _onInitializeRequest: function(requestManager, args) {
        if (requestManager.get_isInAsyncPostBack())
        args.set_cancel(true);
    },
    _onBeginRequest: function(requestManager, args) {
        this.updating = true;
    },
    _onEndRequest: function(requestManager, args) {
        this.updating = false;
    },
    _onApplicationLoad: function() {
        this.add_roundies();
    },
    add_roundies: function() {
        if ( !! DD_roundies) {
            DD_roundies.addRule('span.save_precision_changes', '5px');
            DD_roundies.addRule('span.preview_market_listings', '5px');
            DD_roundies.addRule('div.vin_fly_out div.content', '0 10px 10px 0');
            DD_roundies.addRule('div.market_overview', '10px');
        }
    }
};

var modifySearch = new ModifySearch();
Sys.Application.notifyScriptLoaded();