function JDPower () {
	var	_maxYearsSelect,
		_minYearsSelect,
		_maxMileageRangeSelect,
		_minMileageRangeSelect,
		_body,
		_tabs;
	this.initialize();
}
JDPower.prototype = {
	initialize:function () {
		this._maxYearsSelect = $get('JDMaxYears');
		this._minYearsSelect = $get('JDMinYears');
		this._maxMileageRangeSelect = $get('JDMaxMileageRange');
		this._minMileageRangeSelect = $get('JDMinMileageRange');
		this._tabs = [
			$get("PowerRegionButton"),
			$get("RollUpRegionButton"),
			$get("NationalRegionButton")
		];
		this._body = document.getElementsByTagName('body')[0];
		
		this._onYearChangeDelegate = Function.createDelegate(this, this._onYearChange);
		this._onMileageChangeDelegate = Function.createDelegate(this, this._onMileageChange);
		this._onTabChangeDelegate = Function.createDelegate(this, this._onTabChange);
		
		$addHandler(this._maxYearsSelect, 'change', this._onYearChangeDelegate);
		$addHandler(this._minYearsSelect, 'change', this._onYearChangeDelegate);
		$addHandler(this._maxMileageRangeSelect, 'change', this._onMileageChangeDelegate);
		$addHandler(this._minMileageRangeSelect, 'change', this._onMileageChangeDelegate);
		for (var i=0,l=this._tabs.length; i < l; i++) {
			$addHandler(this._tabs[i], 'click', this._onTabChangeDelegate);
		};
		
		this._onDisposeDelegate = Function.createDelegate(this, this.dispose);
		$addHandler(this._body, 'unload', this.dispose);
	},
	dispose: function () {
		$removeHandler(this._maxYearsSelect, 'change', this._onYearChangeDelegate);
		$removeHandler(this._minYearsSelect, 'change', this._onYearChangeDelegate);
		$removeHandler(this._maxMileageRangeSelect, 'change', this._onMileageChangeDelegate);
		$removeHandler(this._minMileageRangeSelect, 'change', this._onMileageChangeDelegate);		
		for (var i=0,l=this._tabs.length; i < l; i++) {
			$removeHandler(this._tabs[i], 'click', this._onTabChangeDelegate);
		};
		$removeHandler(this._body, 'unload', this._dispose);
		delete this._onYearChangeDelegate;
		delete this._onMileageChangeDelegate;
		delete this._onDisposeDelegate;
	},
	_onTabChange: function (e) {
		for (var i=0,l=this._tabs.length; i < l; i++) {
			Sys.UI.DomElement.removeCssClass(this._tabs[i], 'active');
		};
		Sys.UI.DomElement.addCssClass(e.target, 'active');
	},	
	_onYearChange: function (e) {
		if (e.target.id == 'JDMaxYears') {
		    if (this._maxYearsSelect.selectedIndex < this._minYearsSelect.selectedIndex) {
		        this._minYearsSelect.selectedIndex = this._maxYearsSelect.selectedIndex;
		    }
		} else {
		    if (this._minYearsSelect.selectedIndex > this._maxYearsSelect.selectedIndex) {
		        this._maxYearsSelect.selectedIndex = this._minYearsSelect.selectedIndex;
		    }
		}
	},
	_onMileageChange: function (e) {
		 if (e.target.id == 'JDMaxMileageRange') {
	        if (this._maxMileageRangeSelect.selectedIndex < this._minMileageRangeSelect.selectedIndex) {
	            this._minMileageRangeSelect.selectedIndex = this._maxMileageRangeSelect.selectedIndex;
	        }
	    } else {
	        if (this._minMileageRangeSelect.selectedIndex > this._maxMileageRangeSelect.selectedIndex) {
	            this._maxMileageRangeSelect.selectedIndex = this._minMileageRangeSelect.selectedIndex;
	        }
	    }
	}
};
JDPower.ShowAJAXProgress = function(requestManager, args) {    
	var updatePanels = requestManager._updatePanelClientIDs,
	    trigger = args.get_postBackElement();
	for (var i=0,l=updatePanels.length; i < l; i++) {
		if (trigger.id != "SubmitFiltersButton" || "DetailedSummaryUpdatePanel" == updatePanels[i]) {
			Sys.UI.DomElement.addCssClass($get(updatePanels[i]), 'loading');
		}
	}
};
JDPower.HideAJAXProgress = function(requestManager, args) {
	var updatePanels = requestManager._updatePanelClientIDs;
	for (var i=0,l=updatePanels.length; i < l; i++) {
		Sys.UI.DomElement.removeCssClass($get(updatePanels[i]), 'loading');
	}
};
new JDPower();
Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(JDPower.ShowAJAXProgress);
Sys.WebForms.PageRequestManager.getInstance().add_endRequest(JDPower.HideAJAXProgress);
Sys.Application.notifyScriptLoaded();