﻿function addEvent( obj, type, fn ) {
	if (obj.addEventListener) {
		obj.addEventListener( type, fn, false );
	}
	else if (obj.attachEvent) {
		obj["e"+type+fn] = fn;
		obj[type+fn] = function() { obj["e"+type+fn]( window.event ); }
		obj.attachEvent( "on"+type, obj[type+fn] );
	}
	else {
		obj["on"+type] = obj["e"+type+fn];
	}
}

function CreateCloseBoxes() {
	var submitButton = document.getElementById('SubmitLicenseButton'),
		cancelButton = document.getElementById('CancelLicenseButton'),
		acceptanceCheckBox = document.getElementById('AcceptanceCheckbox'),
		acceptanceWarning = document.getElementById('AcceptanceWarning');
	
	function submitClick(e) {
		if (!acceptanceCheckBox.checked) {		
			acceptanceWarning.style.display = "";
			submitButton.blur();
			e.returnValue = false;
		} else {
			acceptanceWarning.style.display = "none";
			e.returnValue = true;
		}
		return e.returnValue;
	}
	
	function cancelClick(e) {
		var answer = confirm("Are you sure you want to cancel?")
	    if(answer && window.name.length > 0){
		    window.close();
	    } else if(answer && window.name.length == 0) {
	        history.go(-2);
	    }
	    e.returnValue = false;
	    return e.returnValue;
	}
	
	addEvent(submitButton, 'click', submitClick);
	addEvent(cancelButton, 'click', cancelClick);
}

addEvent(window, 'load', CreateCloseBoxes);