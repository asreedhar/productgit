var Market = function() {
    this.OnLoad();
    Sys.Application.add_unload(this.OnUnload);
};

// =========================
// = Public Static Methods =
// =========================
Market.expandButtonClick = function(ev) {
    if (ev.target.className.indexOf("button") != -1) {
        if (ev.target.children[0] && ev.target.children[0].click) {
            ev.target.children[0].click();
        }
    }
};
Market.OpenPopup = function(url, target, windowCloseCallBack) {
    var width;
    var height;

    switch (target) {
    case "FlashLocate":
    	width = 995;
    	height = 720;
    	break;
    default:
        width = 800;
        height = 600;
        break;
    }

    var w = window.open(url, target, 'status=1,scrollbars=1,toolbar=0,resizable=1,width=' + width + ',height=' + height, '');
    if ( !! windowCloseCallBack) {
        bindWindowCloseCallBack(w, windowCloseCallBack);
    };
};
Market.bindWindowCloseCallBack = function(w, windowCloseCallBack) {
    var parentWindow = window;
    // =========================================================================
    // = These (Non-Library) Event Handlers are needed for cross window events =
    // =========================================================================
    var addEvent = (function() {
        if (window.attachEvent) {
            return function(el, type, action) {
                return el.attachEvent("on" + type, action);
            };
        }
        else {
            return function(el, type, action) {
                return el.addEventListener(type, action, false);
            };
        }
    })();
    var removeEvent = (function() {
        if (window.detachEvent) {
            return function(el, type, action) {
                return el.detachEvent("on" + type, action);
            };
        }
        else {
            return function(el, type, action) {
                return el.removeEventListener(type, action, false);
            };
        }
    })();
    var onChildWindowLoad = function() {
        var childForms = w.document.getElementsByTagName("form");
        var onChildWindowUnloadCalback = function() {
            removeEvent(w, "unload", onChildWindowUnloadCalback);
            parentWindow.setTimeout(windowCloseCallBack, 0);
        };
        var onChildWindowUnload = function() {
            removeEvent(w, "unload", onChildWindowUnload);
            removeEvent(w, "load", onChildWindowLoad);
            for (var i = 0, l = childForms.length; i < l; i++) {
                removeEvent(childForms[i], "submit", onChildFormSubmit);
            };
            parentWindow.setTimeout(function() {
                try {
                    if ( !! w)
                    addEvent(w, "load", onChildWindowLoad);
                } catch(err) {
                    // ====================================================================
                    // = Do nothing... with the timeout you can get permission errors
                    // = as the window might be in a closing state, PM
                    // ====================================================================
                    }
            },
            0);
        };
        var onChildFormSubmit = function() {
            addEvent(w, "unload", onChildWindowUnloadCalback);
        };

        for (var i = 0, l = childForms.length; i < l; i++) {
            addEvent(childForms[i], "submit", onChildFormSubmit);
        };

        addEvent(w, "unload", onChildWindowUnload);
    };

    addEvent(w, "load", onChildWindowLoad);
};

// ===================
// = Private Methods =
// ===================
Market.prototype = {
    OnLoad: function() {
        this.SetBackgroundCache();
        if (Sys !== undefined && Sys.Net.WebRequestManager !== undefined) {
            this.ShowProgressOnRequest();
        }
        this.AddRoundedButtons();
        this.AddRoundFilters();
        Sys.UI.DomEvent.addHandler(document.body, 'click', Market.expandButtonClick);
        this.HideAJAXProgress();
    },
    OnUnload: function() {
        Sys.UI.DomEvent.removeHandler(document.body, 'click', Market.expandButtonClick);
    },
    ShowAJAXProgress: function() {
        document.getElementById('progress').style.display = "block";
    },
    HideAJAXProgress: function() {
        document.getElementById('progress').style.display = "none";
    },
    ShowProgressOnRequest: function() {
        Sys.Net.WebRequestManager.add_invokingRequest(this.ShowAJAXProgress);
        Sys.Net.WebRequestManager.add_completedRequest(this.HideAJAXProgress);
    },
    HideProgressOnRequest: function() {
        Sys.Net.WebRequestManager.remove_invokingRequest(this.ShowAJAXProgress);
        Sys.Net.WebRequestManager.remove_completedRequest(this.HideAJAXProgress);
    },
    SetBackgroundCache: function() {
        try {
            document.execCommand("BackgroundImageCache", false, true);
        } catch(e) {}
    },
    AddRoundedButtons: function() {
    	DD_roundies.addRule('#search_controls span.button', '3px');
        DD_roundies.addRule('.row_controls span.button', '3px');
        DD_roundies.addRule('.print_controls span.button', '3px');
        DD_roundies.addRule('.print_dialog span.button', '3px');
        DD_roundies.addRule('.save_controls span.button', '3px');
    },
    AddRoundFilters: function() {
    	DD_roundies.addRule('div.filter fieldset', '0 0 5px 5px');
    }
};

Sys.Application.add_load(function() {
    new Market();
});