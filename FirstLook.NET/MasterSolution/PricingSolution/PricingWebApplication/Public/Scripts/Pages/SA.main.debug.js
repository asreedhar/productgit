
var SalesAccelerator = function() {};

SalesAccelerator.PAGE_LENGTH_THRESHOLD = 954;
SalesAccelerator.PAGE_LENGTH_WARNING_ICON = "<span class=\"warningIcon\" title=\"This sheet will likely not fit on one page.\"></span>";
SalesAccelerator.OFFER_PRICE_WARNING_ICON = "<span class=\"errorIcon offerPriceWarning\" title=\"In order to print, the offer price must be greater than zero.\"></span>";
SalesAccelerator.pageRequestManager = Sys.WebForms.PageRequestManager.getInstance();
SalesAccelerator.returnMode = undefined;
SalesAccelerator.postBackElementId = undefined;
SalesAccelerator.moduleContainerId = undefined;
SalesAccelerator.EditModuleHtml = undefined;
SalesAccelerator.ViewStateSnapshot = undefined;
SalesAccelerator.EventValidationSnapshot = undefined;

/// ====================================================================
/// ===  Start: Offer price functions  =================================
/// ====================================================================
/// Checks for positive number in offer price textbox.
SalesAccelerator.IsValidOfferPrice = function() {
	var offerPriceTextBox = $("#OfferDetails .offerPrice:first");
    var offerPrice = parseInt(offerPriceTextBox.val(), 10);

    if (offerPrice < 1 || isNaN(offerPrice)) {
        if ($(".offerPriceWarning").length === 0) {
            $("#OfferDetails .offerPrice:first").after(SalesAccelerator.OFFER_PRICE_WARNING_ICON);
            return false;
        }
    } else {
        $(".offerPriceWarning:first").replaceWith("");
        return true;
    }
    
};

/// Displays offer price warning if offer price delta exceeds a certain percentage.
SalesAccelerator.CheckOfferPriceDelta = function() {
    
    var originalOfferPrice = parseInt($("#PreviewNavigationForm_OriginalOfferPrice").val().replace(/[^\d\.]/gi, ""), 10);
    var offerPrice = parseInt($("#OfferDetails .offerPrice").val().replace(/[^\d\.]/gi, ""), 10);
    var offerPriceLow = parseInt($("#PreviewNavigationForm_OfferPriceLow").val().replace(/[^\d\.]/gi, ""), 10);
    var offerPriceHigh = parseInt($("#PreviewNavigationForm_OfferPriceHigh").val().replace(/[^\d\.]/gi, ""), 10);
    var warningPriceDialog = $("#OfferPriceWarningDialog");
    var warningTextSnippet;
    
    // make sure the warning dialog element is gone
    warningPriceDialog.remove();
    
    if (offerPrice < offerPriceLow) {
        // set too low warning
        warningTextSnippet = $("#PreviewNavigationForm_OfferPriceLowWarning").val();
    } else if (offerPrice > offerPriceHigh) {
        // set too high warning
        warningTextSnippet = $("#PreviewNavigationForm_OfferPriceHighWarning").val();
    }
    
    if (warningTextSnippet !== undefined) {
        var dialogDiv = document.createElement("div");
        
        // format offer price and original offer price
        offerPrice = offerPrice.toLocaleString().lastIndexOf(".") > -1 ? offerPrice.toLocaleString().substring(0, offerPrice.toLocaleString().lastIndexOf(".")) : offerPrice.toLocaleString();
        originalOfferPrice = originalOfferPrice.toLocaleString().lastIndexOf(".") > -1 ? originalOfferPrice.toLocaleString().substring(0, originalOfferPrice.toLocaleString().lastIndexOf(".")) : originalOfferPrice.toLocaleString();
        
        // setup warning dialog
        dialogDiv.setAttribute("id", "OfferPriceWarningDialog");
        dialogDiv.innerHTML = "WARNING: New Offer Price <strong><em>$" + offerPrice +
                                 "</em></strong> is <strong><em>" + warningTextSnippet + "</em></strong> the original Original Price of <strong><em>$" +
                                 originalOfferPrice + "</em>.</strong>";
        
        $("body").append(dialogDiv);
        
        $("#OfferPriceWarningDialog").dialog({ closeText: "", dialogClass: "warningDialog", title: "Large Price Change Warning", modal: true, buttons: { "Ok": function() { $(this).dialog("close"); $("#OfferPriceWarningDialog").remove(); WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("PreviewNavigationForm$RefreshPreviewButton2", "", true, "", "", false, true)); }, "Cancel": function() { $("#OfferDetails .offerPrice").val(originalOfferPrice.toString().replace(",", "")); $(this).dialog("close"); $("#OfferPriceWarningDialog").remove(); } } });   
        return false;
    } else {
        return true;
    }

};

/// Manages offer price refresh click.
SalesAccelerator.RefreshOfferPrice = function() {
    var offerPriceError = $("#OfferPriceError");
    offerPriceError.hide();
    var isValidOfferPrice = SalesAccelerator.IsValidOfferPrice();
    if (isValidOfferPrice) {
        var isAcceptableOfferPriceDelta = SalesAccelerator.CheckOfferPriceDelta();
        if (isAcceptableOfferPriceDelta) {
            WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("PreviewNavigationForm$RefreshPreviewButton2", "", true, "", "", false, true));
        } else {
            return false;
        }
    } else {
        offerPriceError.show();
        return false;
    }
    return true;
};

/// Enables/Disables payments form fields
SalesAccelerator.TogglePaymentFormFields = function() {

    // get checkbox state
    var isLeaseChecked = $("#LeasePaymentsInfo .checkboxDiv:first input:checkbox").is(":checked");
    var isOwnChecked = $("#OwnPaymentsInfo .checkboxDiv:first input:checkbox").is(":checked");
    
    // get elements to be disabled
    var leaseFormElms = $("#LeasePaymentsInfo input:text");
    var ownFormElms = $("#OwnPaymentsInfo input:text");
    
    // add interest rate checkbox to elements array
    leaseFormElms.push($("#LeasePaymentsInfo input:checkbox")[1]);
    ownFormElms.push($("#OwnPaymentsInfo input:checkbox")[1]);
    
    // show hide lease form elements
    if (isLeaseChecked) {
        $(leaseFormElms).removeAttr("disabled");
    } else {
        $(leaseFormElms).attr("disabled", "disabled");
    }
    
    // show hide own form elements
    if (isOwnChecked) {
        $(ownFormElms).removeAttr("disabled");
    } else {
        $(ownFormElms).attr("disabled", "disabled");
    }
    
};

/// ============================================================
/// ===  End: Offer price functions  ===========================
/// ===  Start: Print page functions  ==========================
/// ============================================================

/// Manages publish button click.
SalesAccelerator.SubmitPrintRequest = function(e) {
    
    var actionsCkbsArray, actionsErrorMessage;
    var requestedPacket = $(".previewSheetDDL").val();
    
    var offerPriceError = $("#OfferPriceError");
    var selectSheetError = $("#SelectPdfErrorMessage");
    
    $(selectSheetError).hide();
    
    // set locals based on selected packet
    switch (requestedPacket) {
    
        case "customer":
            // Ensure there is a packet selected to print
            var sellingSheetChecked = $("#PublishOptions .printSheetIncludes .sellingSheet input").is(":checked");
            var marketComparisonChecked = $("#PublishOptions .printSheetIncludes .mktComparison input").is(":checked");

            if (!sellingSheetChecked && !marketComparisonChecked) {
                $(selectSheetError).show();
                return false;
            }
            actionsCkbsArray = $("#PrintOptionsDiv .pdfActionsCkbList input:checked");
            actionsErrorMessage = $("#PDFActionsErrorMessage");
            break;
            
        case "website":
            actionsCkbsArray = $("#PublishOptionsDiv .pdfActionsCkbList input:checked");
            actionsErrorMessage = $("#PublishActionsErrorMessage");
            break;
            
    }
    
    // Validate offer price.
    offerPriceError.hide();    
    var isValidOfferPrice = SalesAccelerator.IsValidOfferPrice();
    if (isValidOfferPrice) {
    
        var isAcceptableOfferPriceDelta = SalesAccelerator.CheckOfferPriceDelta();
        
        if (!isAcceptableOfferPriceDelta) { return false; }
        
    } else {
        offerPriceError.show();
        return false;
    }
    
    // Validate action checkboxes have at at least one selection
    actionsErrorMessage.hide();
    if (actionsCkbsArray.length === 0) {
        actionsErrorMessage.show();
        return false;
    }
    
    $(".print-sheet-button").addClass("disabled");
    $(".printOptions").find("p.terms").before("<span class=\"publishingMessage\">Publishing ...</span>");
    
    return true;
};

/// ============================================================
/// ===  End: Print page functions  ============================
/// ===  Start: Module ranking/sorting functions  ==============
/// ============================================================

/// Updates hidden fields with module items rank & display properties.
SalesAccelerator.UpdateRankedSortables = function(moduleId) {

    var module, 
        displayedItems, availableItems, 
        displayedItemsList, availableItemsList, 
        displayedItemsField, availableItemsField;
    
    // set locals
    module = $("#" + moduleId);
    displayedItemsList = $(module).find("ul[class*=displayed]")[0];
    availableItemsList = $(module).find("ul[class*=available]")[0];
    displayedItemsField = $(module).find("input[class*=displayed]")[0];
    availableItemsField = $(module).find("input[class*=available]")[0];
    
    // custom update sortables logic
    switch (moduleId) {
    
        case "HighlightsTabContent":            
            UpdateJsonItems(true);
            SalesAccelerator.EqualizeColumnHeights( [ "#HighlightsTabContent ul.available", "#HighlightsTabContent ul.deleted" ] );
            return;
            
        case "JDPowerTabContent":
            UpdateJsonItems();
            return;
            
        case "ReviewsAwardsTabContent":
            UpdateJsonItems();
            return;
            
        case "VHRTabContent":
            UpdateJsonItems();
            return;
            
        default:
            UpdateKeyedItems();
            return;
    }
    
    function UpdateKeyedItems() {
    
        // parse available items
        availableItemsField.value = "";
        availableItems = $(availableItemsList).children();
        for (var j = 0; j < availableItems.length; j++) {
            availableItemsField.value += $(availableItemsList).children()[j].id;
            if (j < availableItems.length - 1) { availableItemsField.value += ","; }
        }
        
        // parse displayed items
        displayedItemsField.value = "";
        displayedItems = $(displayedItemsList).children();
        for (var k = 0; k < displayedItems.length; k++) {
            displayedItemsField.value += $(displayedItemsList).children()[k].id;
            if (k < displayedItems.length - 1) { displayedItemsField.value += ","; }
        }
        
    }
    
    function UpdateJsonItems(canDelete) {
        // set default canDelete value
        if (canDelete === undefined) { canDelete = false; }
    
        var deletedItemsList;
        var combinedItemsField = $(module).find("input[type=hidden].items")[0];
        
        // Support deletable items
        if (canDelete) {
            deletedItemsList = $(module).find("ul[class*=deleted]")[0];        
        }

        // parse items
        var currentItems = JSON.parse(combinedItemsField.value);
        for (var h = 0; h < currentItems.length; h++) {
        
            var itemKey = currentItems[h].Key;
            
            // find item in lists
            var isDisplayed = $(displayedItemsList).find("li[id=" + itemKey + "]").length > 0;
            var isAvailable = $(availableItemsList).find("li[id=" + itemKey + "]").length > 0;
            var isDeleted;
            
            // Support deletable items
            if (canDelete) {
                isDeleted = $(deletedItemsList).find("li[id=" + itemKey + "]").length > 0;
            }
            
            if (isDisplayed) {
                currentItems[h].IsDisplayed = true;
                if (canDelete) { currentItems[h].IsDeleted = false; }
                var listItem = $("li[id=" + itemKey + "]");
                currentItems[h].Rank = $(displayedItemsList).find("li").index(listItem) + 1;
            } else if (isAvailable) {
                currentItems[h].IsDisplayed = false;
                if (canDelete) { currentItems[h].IsDeleted = false; }
                currentItems[h].Rank = -1;
            } else if (isDeleted) {
                currentItems[h].IsDisplayed = false;
                currentItems[h].IsDeleted = true;
                currentItems[h].Rank = -1;
            }
            
        }
        
        combinedItemsField.value = JSON.stringify(currentItems);
    }
    
};

/// Applies jQuery sort functions to modules.
SalesAccelerator.MakeSortable = function(moduleId, tagName) {

    if (tagName.toString().toLowerCase() === "ul") {
        $("#" + moduleId + " " + tagName + ".available").sortable({ distance: 10, placeholder: "liPlaceholder", connectWith: ["#" + moduleId + " ul.sortable"], opacity: 0.8, revert: true, tolerance: "pointer", zIndex: 5000000,  update: function() { SalesAccelerator.UpdateRankedSortables(moduleId); SalesAccelerator.SetupSortingShortcuts(moduleId); } }).disableSelection();
        $("#" + moduleId + " " + tagName + ".displayed").sortable({ distance: 10, placeholder: "liPlaceholder", connectWith: ["#" + moduleId + " ul.sortable"], opacity: 0.8, revert: true, tolerance: "pointer", zIndex: 5000000, update: function() { SalesAccelerator.UpdateRankedSortables(moduleId); SalesAccelerator.SetupSortingShortcuts(moduleId); } }).disableSelection();
        $("#" + moduleId + " " + tagName + ".deleted").sortable({ distance: 10, placeholder: "liPlaceholder", connectWith: ["#" + moduleId + " ul.sortable"], opacity: 0.8, revert: true, tolerance: "pointer", zIndex: 5000000, update: function() { SalesAccelerator.UpdateRankedSortables(moduleId); SalesAccelerator.SetupSortingShortcuts(moduleId); } }).disableSelection();
    }

};

/// Orders Highlight sources by rank
SalesAccelerator.OrderHighlightSources = function() {
    
    var previewPanel = $("#ViewHighlightsPanel");
    var rankedDivs = $(previewPanel).find(".previewPanel div[rank]");
    
    rankedDivs.sort(function(a, b) {
        var compA = $(a).attr("rank");
        var compB = $(b).attr("rank");
        return (compA < compB) ? -1 : (compA > compB) ? 1 : 0;
    });

    $.each(rankedDivs, function(idx, itm) { previewPanel.append(itm); });
    
};

/// Applies mouseover move/delete functions to sortable list items
SalesAccelerator.SetupSortingShortcuts = function(moduleId) {
    if (moduleId !== "") {

        var isFreeText = (moduleId === "HighlightsTabContent");
        
        $("#" + moduleId + " ul.displayed li").each( function() {
            $(this).find("a").remove();
            
            $(this).append("<a href=\"#\" onclick=\"SalesAccelerator.MoveListItem(this.parentNode, 'available', '" + moduleId + "');return false;\" class=\"moveToAvailable\" title=\"Move to available bin\">V</a>");
            if (isFreeText) {
                $(this).append("<a href=\"#\" onclick=\"SalesAccelerator.MoveListItem(this.parentNode, 'deleted', '" + moduleId + "');return false;\" class=\"moveToDeleted\" title=\"Move to deleted bin\">X</a>");
            }
        });

        $("#" + moduleId + " ul.available li").each( function() {
            $(this).find("a").remove();
            
            $(this).append("<a href=\"#\" onclick=\"SalesAccelerator.MoveListItem(this.parentNode, 'displayed', '" + moduleId + "');return false;\" class=\"moveToDisplayed\" title=\"Move to displayed bin\">^</a>");
            if (isFreeText) {
                $(this).append("<a href=\"#\" onclick=\"SalesAccelerator.MoveListItem(this.parentNode, 'deleted', '" + moduleId + "');return false;\" class=\"moveToDeleted\" title=\"Move to deleted bin\">X</a>");
            }
        });

        $("#" + moduleId + " ul.deleted li").each( function() {
            $(this).find("a").remove();
            
            $(this).append("<a href=\"#\" onclick=\"SalesAccelerator.MoveListItem(this.parentNode, 'available', '" + moduleId + "');return false;\" class=\"moveToAvailable\" title=\"Move to available bin\"><</a>");
            $(this).append("<a href=\"#\" onclick=\"SalesAccelerator.MoveListItem(this.parentNode, 'displayed', '" + moduleId + "');return false;\" class=\"moveToDisplayed\" title=\"Move to displayed bin\">^</a>");
        });
        
    }
};

SalesAccelerator.MoveListItem = function(item, bin, moduleId) {
    
    var newitem = $(item).removeAttr("style").clone();
     
    $(item).remove();
    $("#" + moduleId + " ul." + bin).prepend(newitem);    
    
    SalesAccelerator.SetupSortingShortcuts(moduleId);
    if (moduleId === "HighlightsTabContent") { SalesAccelerator.EqualizeColumnHeights( [ "#HighlightsTabContent ul.available", "#HighlightsTabContent ul.deleted" ] ); }
    
    SalesAccelerator.UpdateRankedSortables(moduleId);
    
};

SalesAccelerator.MarketListingsCheckboxClick = function(target) {
    
    var isChecked = $(target).is(":checked");
    var vehicleId = $(target).val();
    
    var selectedListingsPanel = $("#MarketListingsSelectedPanel:first");
    var selectedListingsTBody = $(selectedListingsPanel).find("tbody:first");
    var searchListingsTBody = $("#MarketListingsPreferenceTable tbody:first");
    
    var searchListRow = $(searchListingsTBody).find("input[type=checkbox][value='" + vehicleId + "']").parents("tr:first");
    var selectedListRow = $(selectedListingsTBody).find("input[type=checkbox][value='" + vehicleId + "']").parents("tr:first");
    
    var editedListingsHiddenField = $(selectedListingsPanel).find("input[type=hidden]")[1];
    var editedListingsHiddenFieldValuesArr = $(editedListingsHiddenField).val() === "" ? [] : $(editedListingsHiddenField).val().split(",");
    
    if (isChecked) {
    
        // Handle emptyModule class
        $(selectedListingsPanel).find(".previewPanel.emptyModule").removeClass("emptyModule");
        $(selectedListingsPanel).find(".emptyDataSetLabel").hide();
        
        // Add new row to selected tab table & set checkbox event handler
        var newRow = $(target).parents("tr:first").clone().appendTo($(selectedListingsPanel).find("tbody:first"));
        
        // Hide dealer name row if Show Dealer Name checkbox is unchecked
        if (!$("#DisplayDealerNameCheckbox input:checkbox").is(":checked")) { $($(newRow).find("td")[1]).hide(); }
        
        // Add description row
        var newDescRow = $(target).parents("tr:first").next("tr").clone().appendTo($(selectedListingsPanel).find("tbody:first"));
        $(newDescRow).show();
        
        // Reorder rows
        var selectedListingsRows = $(selectedListingsPanel).find("tbody:first tr").not(".vehicleDescription");
        var selectedListingsDescriptionRows = $(selectedListingsPanel).find("tbody:first tr.vehicleDescription");
        selectedListingsRows.sort(function(a,b) {
            var keyA = parseInt($(a).find("td")[6].innerHTML.replace("$", "").replace(",", ""), 10);
            var keyB = parseInt($(b).find("td")[6].innerHTML.replace("$", "").replace(",", ""), 10);
            
            if (keyA > keyB) { return -1; }
            if (keyA < keyB) { return 1; }
            return 0;
        });
        $(selectedListingsTBody).children().remove();
        $.each(selectedListingsRows, function(index, row) {
            $(selectedListingsTBody).append(row);
        });
        
        // Reappend description rows
        $.each(selectedListingsDescriptionRows, function(index, row) {
            var vehicleId = $(row).attr("id");
            $(selectedListingsTBody).find("tr[id=" + vehicleId + "]").after(row);
        });
        
        // Style search list rows
        $(searchListRow).addClass("selected");
        
        // Update hidden field values array
        if ( jQuery.inArray(vehicleId, editedListingsHiddenFieldValuesArr) < 0 ) {
            editedListingsHiddenFieldValuesArr.push(vehicleId);
        }
        
    } else {
    
        // Manage search/selected rows
        if (searchListRow.length > 0) {
            $(searchListRow).removeClass("selected");
            $(searchListRow).find("input[type=checkbox]")[0].checked = false;
        }
        $(selectedListRow).next("tr").remove();
        $(selectedListRow).remove();
        
        // Update hidden field values array
        if ( jQuery.inArray(vehicleId, editedListingsHiddenFieldValuesArr) > -1 ) {
            editedListingsHiddenFieldValuesArr.splice($.inArray(vehicleId, editedListingsHiddenFieldValuesArr), 1);
        }
        
        // Handle emptyModule class
        if ($(selectedListingsPanel).find("tbody tr").length === 0) {
            $(selectedListingsPanel).find(".previewPanel").addClass("emptyModule");
            $(selectedListingsPanel).find(".emptyDataSetLabel").show();
        }
        
    }
    
    // Style selected list rows
    var allSelectedListingsRows = $(selectedListingsPanel).find("tbody tr");
    $(allSelectedListingsRows).removeClass("odd").removeClass("even");
    
    var classKey = 0;
    for (var i = 0; i < allSelectedListingsRows.length; i++) {
        if (classKey === 0 || classKey === 1) {
            $(allSelectedListingsRows[i]).addClass("even");
            classKey++;
        } else {
            $(allSelectedListingsRows[i]).addClass("odd");
            classKey++;
        }
        classKey = classKey > 3 ? 0 : classKey;
    }
    
    // Update hidden field value
    $(editedListingsHiddenField).val(editedListingsHiddenFieldValuesArr.join(","));
    
    // Rebind checkbox handler
    $("#MarketListingsPreferenceTable tbody input[type=checkbox], #MarketListingsSelectedPanel tbody input[type=checkbox]").unbind("click").click( function() {
        SalesAccelerator.MarketListingsCheckboxClick($(this));
    });
};

/// ============================================================
/// ===  End: Module ranking/sorting functions  ================
/// ===  Start: Module show/hide functions  ====================
/// ============================================================

/// Sets up form module checkboxes based on hidden toggle checkboxes in modules.
SalesAccelerator.SetDisplayCheckboxes = function() {

    var checkboxes = $("#SellingSheetIncludes input[type=checkbox], #WebsitePdfIncludes input[type=checkbox]");
    
    if (!checkboxes) { return; }
    
    for (var i = 0; i < checkboxes.length; i++) {
        var moduleName = $(checkboxes[i]).next("label").text().trim().toLowerCase();
        
        switch (moduleName) {
        
            case "vehicle photo":
                checkboxes[i].checked = $("#VehiclePhoto").find(".displayOnSheetCkb input")[0].checked;
                // keep this checkbox in sync with correspnding checkbox on Website PDF
                $("#WebsitePdfIncludes label:contains('Vehicle Photo')").prev("input[type=checkbox]").checked = checkboxes[i].checked;
                if (!checkboxes[i].checked) { $("#VehiclePhoto").hide(); }
                break;
                
            case "equipment":
                checkboxes[i].checked = $("#EditFeaturesPanel").find(".displayOnSheetCkb input")[0].checked;
                // keep this checkbox in sync with correspnding checkbox on Website PDF
                $("#WebsitePdfIncludes label:contains('Equipment')").prev("input[type=checkbox]").checked = checkboxes[i].checked;
                if (!checkboxes[i].checked) { $("#FeaturesUpdatePanel").hide(); }
                break;
                
            case "vehicle highlights":
                checkboxes[i].checked = $("#EditHighlightsPanel").find(".displayOnSheetCkb input")[0].checked;
                // keep this checkbox in sync with correspnding checkbox on Website PDF
                $("#WebsitePdfIncludes label:contains('Vehicle Highlights')").prev("input[type=checkbox]").checked = checkboxes[i].checked;
                if (!checkboxes[i].checked) { $("#HighlightsUpdatePanel").hide(); }
                break;
                
            case "ad preview":
                checkboxes[i].checked = $("#VehicleDescription").find(".displayOnSheetCkb input")[0].checked;
                // keep this checkbox in sync with correspnding checkbox on Website PDF
                $("#WebsitePdfIncludes label:contains('Ad Highlights')").prev("input[type=checkbox]").checked = checkboxes[i].checked;
                if (!checkboxes[i].checked) { $("#VehicleDescriptionUpdatePanel").hide(); }
                break;
                
            case "certified benefits":
            
                var message = $("#CertifiedVehiclePreferences .messagingLabel").text();
                var vehicleIsCertified = $("#CertifiedVehiclePreferences .CPOV:first").val() === "1" || $(checkboxes[i]).attr("checked");
                var cpoAssociationExists = $("#CertifiedVehiclePreferences .CPOASSOC:first").val() === "1";
                var hasCertifiedMessage = message.replace(" ", "").length > 0;
                
                // initially show certified benefits based on the preference if the vehicle is certified.
                checkboxes[i].checked = $("#EditCertifiedBenefitsPanel").find(".displayOnSheetCkb input:first").is(":checked") || vehicleIsCertified;
                // make sure the hidden checkbox is synched up with the visible checkbox
                $("#EditCertifiedBenefitsPanel").find(".displayOnSheetCkb input")[0].checked = checkboxes[i].checked;
                
                if ($(checkboxes[i]).parents("#WebsitePdfIncludes").length > 0) {   // -- website pdf checkbox --

                    if (hasCertifiedMessage) {
                        $(checkboxes[i]).attr("disabled", "disabled");
                        $(checkboxes[i]).removeAttr("checked");
                        checkboxes[i].title = message;
                        $(checkboxes[i]).next("label").css("color", "#a0a0a0");
                        $(checkboxes[i]).next("label").attr("title", message);
                        $("#CertifiedBenefitsUpdatePanel").hide();
                    }
                    
                } else {  
                    // -- sales packet checkbox --
                    // For the sales packet, if there is no association between the dealership 
                    // and the selected vehicle's make (regardless of the vehicle's certification 
                    // status), we can't show it as certified.  It is only in this case that we 
                    // disable the certified benefits checkbox on the sales packet.
                    if (! cpoAssociationExists) {
                        $(checkboxes[i]).attr("disabled", "disabled");
                        checkboxes[i].title = message;
                        $(checkboxes[i]).next("label").css("color", "#a0a0a0");
                        $(checkboxes[i]).next("label").attr("title", message);
                        $("#CertifiedBenefitsUpdatePanel").hide();
                    }
                }
                
                break;
                
            case "market listings":
                checkboxes[i].checked = $("#EditMarketListingsPanel").find(".displayOnSheetCkb input")[0].checked;
                if (!checkboxes[i].checked) { $("#MarketListings").hide(); $(".mktListingRadButtons input").attr("disabled", "true"); }
                else { $(".mktListingRadButtons input").removeAttr("disabled"); }
                
                // keep this checkbox in sync with correspnding checkbox on Website PDF
                $("#WebsitePdfIncludes label:contains('Market Listings')").prev("input[type=checkbox]").checked = checkboxes[i].checked;

                break;
                
            default:
                if (moduleName.indexOf("pricing analysis") > -1) {
                
                    checkboxes[i].checked = $("#EditPricingAnalysisPanel").find(".displayOnSheetCkb input")[0].checked;
                    if (!checkboxes[i].checked) { $("#PricingAnalysisPreview_PricingPanel").hide(); }
                    
                    // keep this checkbox in sync with correspnding checkbox on Website PDF
                    $("#WebsitePdfIncludes label:contains('Pricing Analysis')").prev("input[type=checkbox]").checked = checkboxes[i].checked;
                
                }
                break;
                
        }
    }
    
};

/// Toggles edit functionality for Vehicle Photos module.
SalesAccelerator.TogglePhotoEdit = function(turnOn) {

    var module = $("#VehiclePhoto");
    
    if (turnOn) {
    
        $(module).find("a.vehiclePhotoEditLink").fadeIn(400);
        $(module).bind("mouseover", function() {
            $(this).css("cursor", "pointer");
            $(module).find("a.vehiclePhotoEditLink").click( SalesAccelerator.OpenChildWindow );
            $(module).find("img").click( SalesAccelerator.OpenChildWindow );
        });
        
    } else {
    
        $(module).css("cursor", "default");
        $(module).unbind("mouseover");
        $(module).find("a.vehiclePhotoEditLink").fadeOut(400);
        $(module).find("img").unbind("click");
        
    }
};

SalesAccelerator.OpenChildWindow = function() {
    
    var refreshButtonClientId, refreshButtonUniqueId, url, options;
    var newWindow, poller;
    
    var moduleId = $(this).parents(".moduleContainer").attr("id");
    
    url = $(this).attr("href");
    options = ["width=700", "height=550", "menubar=false", "location=false", "toolbar=false", "resizable=yes", "scrollbars=yes"];

    switch (moduleId)
    {
        case "MarketListings":
            refreshButtonClientId = $("#MarketListingsRefreshButtonClientIdHidden").val();
            refreshButtonUniqueId = $("#MarketListingsRefreshButtonUniqueIdHidden").val();
            break;
            
        case "PricingAnalysis":
            refreshButtonClientId = $("#PricingAnalysisRefreshButtonClientIdHidden").val();
            refreshButtonUniqueId = $("#PricingAnalysisRefreshButtonUniqueIdHidden").val();
            break;
            
        case "VehicleFeatures":
            refreshButtonClientId = $("#EquipmentRefreshButtonClientIdHidden").val();
            refreshButtonUniqueId = $("#EquipmentRefreshButtonUniqueIdHidden").val();
            break;
            
        case "VehiclePhoto":
            refreshButtonClientId = $("#VehicleImageRefreshButtonClientIdHidden").val();
            refreshButtonUniqueId = $("#VehicleImageRefreshButtonUniqueIdHidden").val();
            url = $("#VehicleImage_PhotoManagerUrl").val();
            break;
    }
    
    newWindow = window.open(url, "MaxChildWindow", options);
    
    poller = window.setInterval(PollChildWindow, 500);
    
    function PollChildWindow() {
        if (newWindow.closed) {
            clearInterval(poller);
            ReloadModule();
        }
    }
        
    function ReloadModule()
    {
        WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(refreshButtonUniqueId, "", true, "", "", false, true));
    }
    
    return false;
    
};

/// Toggle Market Listings layout for value analyzer view
SalesAccelerator.ToggleMarketListingVersion = function(event) {
    
    switch ($(".mktListingRadButtons input:checked").val()) {
    
        case "short":
            $("#MarketComparisonListings").hide();
            $("#ViewMarketListingsPanel").show();
            break;
            
        case "long":
            $("#ViewMarketListingsPanel").hide();
            $("#MarketComparisonListings").show();
            break;
            
    }
    
    SalesAccelerator.SetPageBreaks();
    
};

/// ============================================================
/// ===  End: Module show/hide functions  ======================
/// ===  Start: Page length warning/indicator functions  =======
/// ============================================================

/// Adds page break indicators
SalesAccelerator.SetPageBreaks = function() {

    var contentDiv = $( "#CVAModules" );
    var contentDivHeight = $( contentDiv ).height();
    
    $(".pageBreak").remove();
    
    // TODO: Account for empty modules
    if ( contentDivHeight > SalesAccelerator.PAGE_LENGTH_THRESHOLD ) {
    
        var breakCount = parseInt( contentDivHeight / SalesAccelerator.PAGE_LENGTH_THRESHOLD, 10 );
        
        for ( var i = 0; i < breakCount; i++ ) {
        
            var div = document.createElement( "div" );
            var span = document.createElement( "span" );
            
            div.className = "pageBreak";
            div.style.top = ( SalesAccelerator.PAGE_LENGTH_THRESHOLD * ( i + 1 ) ).toString() + "px";
            
            span.setAttribute( "title", "Approximate location of page break." );
            span.innerHTML = "page break";
            
            div.appendChild( span );
            $(contentDiv).append( div );
            
            // add another indicator to the left side
            var div2 = $(div).clone();
            $(div2).css("left", "2px");
            $(contentDiv).append( div2 );
            
        }
    }
    
};

/// Returns total height of all 'empty' modules to exclude from page length indicator.
SalesAccelerator.EmptyModulesHeight = function() {
    var emptyModulesHeight = 0;
    var emptyModules = $(".emptyModule:visible");
    
    for (var i = 0; i < emptyModules.length; i++) {
        emptyModulesHeight += $(emptyModules[i]).height();
    }
    
    return emptyModulesHeight;
};

/// ============================================================
/// ===  End: Page length warning/indicator functions  =========
/// ===  Start: Sheet view and edit mode functions  ============
/// ============================================================

/// Displays desired document - Sales Packet (Selling Sheet / Market Comparison) or Website PDF.
SalesAccelerator.ToggleSheetView = function(speed, sheet) {

    speed = speed === 0 ? speed : 500;
    
    var maxForm = $("#CVASettingsForm");
    var maxModules = $("#CVAModules");

    // which view are we showing
    var selectedPacket = $(".previewSheetDDL").val();
    var selectedPacketHiddenValue = $(".salesPacketMode").val();
    var selectedSalesSheet = (sheet !== undefined ? sheet : (selectedPacketHiddenValue === "" ? "sales" : selectedPacketHiddenValue) );
    
    // update the hidden field with the sales packet view
    $(".salesPacketMode").val( selectedSalesSheet );
    
    // is website pdf market listings upgrade active?
    var hasMarketListingUpgrade = $("#PreviewNavigationForm_WebSitePdfMarketListingsActive").val() === "True";
    
    // respect dealer upgrade status
    if (selectedPacket === "website" && $("#PreviewNavigationForm_WebSitePdfActive").val() === "False") {
        SalesAccelerator.ShowCustomDialog("WebsitePdfActive");
        return false;
    }
    
    // which modules are on
    var showPhotoSS = $("#SellingSheetIncludes label:contains('Vehicle Photo')").prev("input[type=checkbox]").is(":checked");
    var showEquipmentSS = $("#SellingSheetIncludes label:contains('Equipment')").prev("input[type=checkbox]").is(":checked");
    var showHighlightsSS = $("#SellingSheetIncludes label:contains('Vehicle Highlights')").prev("input[type=checkbox]").is(":checked");
    var showCertifiedSS = $("#SellingSheetIncludes label:contains('Certified Benefits')").prev("input[type=checkbox]").is(":checked");
    var showPricingSS = $("#SellingSheetIncludes label:contains('Pricing Analysis')").prev("input[type=checkbox]").is(":checked");
    var showPaymentsSS = $("#LeasePaymentsInfo .checkboxDiv:first input[type=checkbox]").is(":checked") || $("#OwnPaymentsInfo .checkboxDiv:first input[type=checkbox]").is(":checked");
    var showEquipmentMC = $("#MarketComparisonIncludes label:contains('Equipment')").prev("input[type=checkbox]").is(":checked");
    var showDescriptionWebPDF = $("#WebsitePdfIncludes label:contains('Ad Preview')").prev("input[type=checkbox]").is(":checked");
    var showMarketListingsWebPDF = $("#WebsitePdfIncludes label:contains('Market Listings')").prev("input[type=checkbox]").is(":checked");
    var webPDFListingsFormat = $(".mktListingRadButtons input:checked").val();
    
    $("#WebsitePdfFields, #SellingSheetFields").hide();
    $("#ConsumerPacketAddOns .marketComparisonCkb span:first label:first").css("font-weight", "normal");

    // get some additional certified benefits related info
    var message = $("#CertifiedVehiclePreferences .messagingLabel").text();
    var vehicleIsCertified = $("#CertifiedVehiclePreferences .CPOV:first").val() === "1";
    var cpoAssociationExists = $("#CertifiedVehiclePreferences .CPOASSOC:first").val() === "1";
    var hasCertifiedMessage = message.replace(" ", "").length > 0;
    var salesCertCheckbox = $("#SellingSheetIncludes .ckbList label:contains('Certified Benefits')").prev("input")[0];
    
    $(maxForm).slideUp(speed, function() {
        
        $(maxForm).slideDown(speed, function() {

            switch (selectedPacket) {
            
                case "customer":
                    switch (selectedSalesSheet) {
                        case "sales":

                        if (cpoAssociationExists) {
                            $(salesCertCheckbox).removeAttr("disabled");
                            $(salesCertCheckbox).removeAttr("title");
                            $(salesCertCheckbox).next("label").removeClass("color");
                            $(salesCertCheckbox).next("label").removeAttr("title");
                            if (salesCertCheckbox.checked) {
                                $("#CertifiedBenefitsUpdatePanel").show();
                            } else {
                                $("#CertifiedBenefitsUpdatePanel").hide();
                            }
                            
                        } else {
                            $(salesCertCheckbox).attr("disabled", "disabled");
                            $(salesCertCheckbox).attr("title", message);
                            $(salesCertCheckbox).next("label").css("color", "#a0a0a0");
                            $(salesCertCheckbox).next("label").attr("title", message);
                            $("#CertifiedBenefitsUpdatePanel").hide();
                        }
                            
                        // show the cpo logo if this vehicle is certified or they have selected certified benefits
                        if( showCertifiedSS || ( vehicleIsCertified && cpoAssociationExists)){
                            $("#VehicleLogos .cpoLogo").show();
                        } else{
                            $("#VehicleLogos .cpoLogo").hide();
                        }
                        
                        $("#MarketComparisonIncludes").hide();
                        $("#SellingSheetIncludes").show();
                        break;
                        
                        case "market":
                            $("#SellingSheetIncludes").hide();
                            $("#MarketComparisonIncludes").show();
                            $("#ConsumerPacketAddOns .marketComparisonCkb span:first label:first").css("font-weight", "bold");
                            break;
                    }
                    $("#SellingSheetFields").slideDown(speed);
                    
                    break;
                
                case "website":
                    $("#WebsitePdfFields").slideDown(speed);
                    if (!hasMarketListingUpgrade) {
                        $("#WebsitePdfIncludes .ckbList input:last, #WebsitePdfIncludes .ckbList label:last").hide();
                        $("#WebsitePdfIncludes .mktListingRadButtons").hide();
                    }
                    
                    // show the cpo logo only if this vehicle is certified
                    if(vehicleIsCertified && cpoAssociationExists) {
                        $("#VehicleLogos .cpoLogo").show();
                    } else {
                        $("#VehicleLogos .cpoLogo").hide();
                    }   
                    
                    // handle certified checkbox
                    if (hasCertifiedMessage) {
                        $("#SellingSheetIncludes .ckbList label:contains('Certified Benefits')").prev("input").attr("disabled", "disabled");
                    }
                                             
                    break;
                    
            }
            
        });
        
    });
    
    $("#MarketComparisonHead, #DealerSummary, .dealerContactInfo, #VehiclePhoto").hide();
    $("#FeaturesUpdatePanel, #HighlightsUpdatePanel, #VehicleDescriptionUpdatePanel, #CertifiedBenefitsUpdatePanel").hide();
    $("#PaymentInfoUpdatePanel, #PricingAnalysisPreview_PricingPanel").hide();
    $("#MarketListings, #ViewMarketListingsPanel, #MarketComparisonListings, #FooterPanel").hide();
    
    $(maxModules).slideToggle(speed, function() {

        switch (selectedPacket) {
            
            case "customer":
            
                switch (selectedSalesSheet) {
                    case "sales":
                        // style radio buttons
                        $("#SellingSheetSelector").addClass("selected");
                        $("#MktComparisonSelector").removeClass("selected");
                        
                        $("#DealerSummary, .dealerContactInfo" + (showPhotoSS ? ", #VehiclePhoto" : "") + 
                            (showEquipmentSS ? ", #FeaturesUpdatePanel" : "") + 
                            (showHighlightsSS ? ", #HighlightsUpdatePanel" : "") + 
                            (showCertifiedSS ? ", #CertifiedBenefitsUpdatePanel" : "") + 
                            (showPaymentsSS ? ", #PaymentInfoUpdatePanel" : "") +
                            (showPricingSS ? ", #PricingAnalysisPreview_PricingPanel" : "") + ", #FooterPanel").show();
                        break;
                    
                    case "market":
                        // style radio buttons
                        $("#SellingSheetSelector").removeClass("selected");
                        $("#MktComparisonSelector").addClass("selected");
                        
                        $("#MarketComparisonHead, #VehicleDescriptionUpdatePanel" + (showPhotoSS ? ", #VehiclePhoto" : "") + 
                            (showEquipmentMC ? ", #FeaturesUpdatePanel" : "") + 
                            ", #MarketListings, #MarketComparisonListings, #FooterPanel").show();
                        
                        break;
                }
                
                break;
            
            case "website":
    
                $("#DealerSummary" + (showPhotoSS ? ", #VehiclePhoto" : "") + 
                    (showEquipmentSS ? ", #FeaturesUpdatePanel" : "") + 
                    (showHighlightsSS ? ", #HighlightsUpdatePanel" : "") + 
                    (showDescriptionWebPDF ? ", #VehicleDescriptionUpdatePanel" : "") + 
                    (showCertifiedSS && vehicleIsCertified && ! hasCertifiedMessage ? ", #CertifiedBenefitsUpdatePanel" : "") + 
                    (showPricingSS ? ", #PricingAnalysisPreview_PricingPanel" : "")).show();
                    
                // respect contact info on pdf preference
                if ($("#DealerSummaryPanel .showContactInfoOnWebsitePdf").val().toLowerCase() === "true") {
                    $(".dealerContactInfo").show();
                }
                
                // respect website pdf market listings upgrade
                if (!hasMarketListingUpgrade) {
                     $("#MarketListings").hide();
                } else {
                    if (showMarketListingsWebPDF) {
                        $("#MarketListings" + (webPDFListingsFormat === "long" ? ", #MarketComparisonListings" : ", #ViewMarketListingsPanel")).show();
                    }
                }
                break;
            
            default:        
                // TODO: throw error
        }        
        
        $(maxModules).slideToggle(speed, SalesAccelerator.SetPageBreaks );
    });

};

SalesAccelerator.showEditPanel = function(moduleContainerId) {
    
    // find requested module
    var moduleContainer;
    
    if ($(this).parents(".moduleContainer").length > 0) {
        moduleContainer = $(this).parents(".moduleContainer:first").parent();
        moduleContainerId = $(moduleContainer).attr("id");
    } else {
         moduleContainer = $("#" + moduleContainerId);
    }
    
    // initialize quick move icons
    if (moduleContainerId === "HighlightsUpdatePanel") {
        SalesAccelerator.SetupSortingShortcuts("HighlightsTabContent");
    } else {
        SalesAccelerator.SetupSortingShortcuts($(moduleContainer).attr("id"));
    }
    
    // Set module default state globals
    if (SalesAccelerator.EditModuleHtml === undefined) {
        // TODO: Refactor these lines as they are duplicated in onPageLoaded()
        SalesAccelerator.EditModuleHtml = $(moduleContainer).html();
        SalesAccelerator.ViewStateSnapshot = $("#__VIEWSTATE").val();
        SalesAccelerator.EventValidationSnapshot = $("#__EVENTVALIDATION").val();
    }
    
    // hide/show    
    moduleContainer.find(".viewModulePanel").hide();
    moduleContainer.find(".editModulePanel").show();
    
    $(moduleContainer).before("<div id=\"ModalEditPlaceholder\"></div>");
    
    /// Set custom heights
    var modalHeight = 480;
    var dialogCssClass = "modalEdit";
    switch (moduleContainerId) {
        
        case "MarketListingsUpdatePanel":
            modalHeight = 600;
            dialogCssClass = "marketListingsModalEdit";
            break;
        
    }
    
    // IE6 select fix
    $(".previewSheetDDL").css("visibility", "hidden");
    
    var editModal = $(moduleContainer).dialog({ autoOpen:false, closeOnEscape: false, close: SalesAccelerator.handleModalEditClose, dialogClass: dialogCssClass, draggable: false, height: modalHeight, modal: true, resizable: false, title: $(moduleContainer).find(".editModulePanel").find("h3:first").text(), width: 710 });
    editModal.dialog("open");
    $(editModal).parent().appendTo($("form:first"));
    SalesAccelerator.BindUIEventHandlers(moduleContainerId);
    $(moduleContainer).find(".editModulePanel .cancelBtn").unbind("click").click( function(event) { SalesAccelerator.CancelModuleEdit(event); return false; } );
    
    /// Update position to keep box centered
    //$(editModal).parent().css("top", "50%").css("left", "50%").css("margin-top", "-" + ($(editModal).parent().height()/2).toString()).css("margin-left", "-" + ($(editModal).parent().width()/2).toString());
    
    /// Additional customizations
    switch (moduleContainerId) {
        
        case "MarketListingsUpdatePanel":
            $(".modalEdit").css("height", "600px !important");
            break;
        
    }
    
    // equalize free-text highlights available/deleted column heights
    if ($(this).parents("#HighlightsUpdatePanel").length > 0) {
        SalesAccelerator.EqualizeColumnHeights( [ "#HighlightsTabContent ul.available", "#HighlightsTabContent ul.deleted" ] );
    }
    
    return false;
    
};

SalesAccelerator.handleModalEditClose = function() {
    
    var showMarketListingsWebPDF = $("#WebsitePdfIncludes label:contains('Market Listings')").prev("input[type=checkbox]").is(":checked");
    var webPDFListingsFormat = $(".mktListingRadButtons input:checked").val();
    
    $(this).removeAttr("style").removeAttr("class");
    $(this).show();
    
    if ($(this).attr("id") === "MarketListingsUpdatePanel") {
        if ($(".previewSheetDDL").val() === "customer") {
            $("#MarketComparisonListings").show();
        } else {
            $("#MarketListings" + (webPDFListingsFormat === "long" ? ", #MarketComparisonListings" : ", #ViewMarketListingsPanel")).show();
        }
    } else {
        $(this).find(".viewModulePanel").show();
    }
    $(this).find(".editModulePanel").hide();
    
    $("#ModalEditPlaceholder").replaceWith($(this).clone("false"));
    
    $(this).dialog("destroy");
    $(this).remove();
    
    SalesAccelerator.ResetModuleEditGlobals();
    SalesAccelerator.BindUIEventHandlers("ModuleContainer");
    SalesAccelerator.BindUIEventHandlers( $(this).find(".moduleContainer").attr("id") );
    
    // IE6 select fix
    $(".previewSheetDDL").css("visibility", "visible");
    
};

SalesAccelerator.CancelModuleEdit = function(e) {
    
    // Get clicked element and set module.
    var event = e || window.event;
    var target = event.target || event.srcElement;
    var module = $(target).parents(".moduleContainer").parent();
    
    // Reload html and reset viewstate/eventvalidation
    $(module).html(SalesAccelerator.EditModuleHtml);
    $("#__VIEWSTATE")[0].value = SalesAccelerator.ViewStateSnapshot;
    $("#__EVENTVALIDATION")[0].value = SalesAccelerator.EventValidationSnapshot;
    
    $(module).dialog("close");
    
    return false;
    
};

/// Resets Module edit globals.
SalesAccelerator.ResetModuleEditGlobals = function() {

    SalesAccelerator.EditModuleHtml = undefined;
    SalesAccelerator.ViewStateSnapshot = undefined;
    SalesAccelerator.EventValidationSnapshot = undefined;
    
};

/// Sets up Market Listings filter message
SalesAccelerator.SetMarketListingsFiltersMessage = function() {

    var marketListingsFilters = JSON.parse($(".activeFiltersHidden").val());
    var activeFiltersMessage = "";
    for (var prop in marketListingsFilters) {
        if (marketListingsFilters[prop] === true) {
            activeFiltersMessage += GetFriendlyName(prop.toString().replace("FilterActive", "")) + ", ";
        }
    }
    
    if (activeFiltersMessage.length > 0) {
        activeFiltersMessage = "The following filters are applied: <strong>" + 
            activeFiltersMessage.substring(0, activeFiltersMessage.lastIndexOf(",")) + "</strong>";
        $("#MarketListingsFiltersMessage").remove();
        $("#MarketListingsSearchPanel tr.filters.row1 td:first").append("<div id=\"MarketListingsFiltersMessage\">" + activeFiltersMessage + "</div>");
    }
    
    function GetFriendlyName(propName) {
        
        switch (propName) {
                        
            case "DealerName":
                return "Dealer Name";
                        
            case "TrimLevel":
                return "Trim Level";
                
            case "Colors":
                return "Color";
            
            case "Mileage":
                return "Mileage Range";
            
            case "Price":
                return "Price Range";
            
        }
        
        return propName;   
    }
    
};

SalesAccelerator.ToggleFilterPanel = function(target) {

    var panel = $(target).next("div.filterPanel");
    
    $("#MarketListingsPreferenceTable .filters.row2 a").removeClass("open");
    
    if ($(panel).css("display") === "block") {
        $(panel).hide();
    } else {
        $("div.filterPanel").hide();
        $(target).addClass("open");
        $(panel).show();
    }
    
};

SalesAccelerator.SetAdPreviewLength = function(length) {
    
    var whitespaceRegEx = /(\s+|[\,])/gi;
    var disallowTrailingChars = /[\,]$/gi;
    
    var charLengthField = $("#EditVehicleDescription input:hidden.length:first");
    var fullAdCopy = $("#EditAdPreviewCopyDiv:first").text().trim();
    var adPreviewCopyDiv = $("#EditAdPreviewCopyDiv:first");
    
    var newPreviewString = "";
    var leftSide, rightSide;
    
    // if length is not passed, get from field
    if ((typeof(length)).toString() === "object") {
        length = parseInt($(charLengthField).val(), 10);
    }
    
    if ($(this).attr("id") === "SubtractWordButton") {
        while (fullAdCopy.charAt(length).match(whitespaceRegEx)) {
            length--;
        }
        if (!fullAdCopy.charAt(length).match(whitespaceRegEx)) {
            leftSide = fullAdCopy.substring(0, length);
            rightSide = fullAdCopy.substring(length + 1, fullAdCopy.length - 1);
            length -= leftSide.split(whitespaceRegEx)[leftSide.split(whitespaceRegEx).length - 1].length;
            length--;
        }
    } else {
        if ($(this).attr("id") === "AddWordButton") {
            while (fullAdCopy.charAt(length).match(whitespaceRegEx)) {
                length++;
            }
        }
        if (!fullAdCopy.charAt(length).match(whitespaceRegEx) && length < fullAdCopy.length) {
            leftSide = fullAdCopy.substring(0, length);
            rightSide = fullAdCopy.substring(length + 1, fullAdCopy.length - 1);
            length = length + ((rightSide.split(whitespaceRegEx)[0]).length) + 1;
        }
    }
    
    // ensure string ends with 'acceptable' character
    while (fullAdCopy.substring(0, length).match(disallowTrailingChars)) {
        length--;
    }
    
    // ensure index is never less than zero or greater than full length
    if (length < 0) { length = 0; }
    if (length > fullAdCopy.length) { length = fullAdCopy.length; }
    
    // set length
    newPreviewString = "<span class=\"highlight\">" + fullAdCopy.substr(0, length) + "</span>" + fullAdCopy.substring(length, fullAdCopy.length + 1);
    adPreviewCopyDiv.html(newPreviewString);
    
    charLengthField.val(length);
    
    return false;
};

/// ============================================================
/// ===  End: Sheet view and edit mode functions  ==============
/// ===  Start: Additional Highlights free text functions  =====
/// ============================================================

/// Sets up textbox for editing a Consumer Highlight.
SalesAccelerator.EditListItem = function() {
    
    // remove 'quick move' buttons from list item
    $(this).find("a").remove();

    // Store and remove item text
    var text = $(this).text();
    $(this).text("");
    
    // Change state of LI being edited
    $(this).unbind("dblclick");
    $(this).addClass("editing");
    
    // Setup and append textbox
    var textbox = document.createElement("input");
    $(textbox).addClass("text");
    $(textbox).val(text);
    $(textbox).attr("storeText", text);
    
    // cleanup when editing is complete
    $(textbox).blur(function() {
        var listItem = $(this).parent();
        $(listItem).text($(this).val());
        $(listItem).removeClass("editing");
        if ($(this).val().trim() === "") {
            $(listItem).text($(this).attr("storeText"));
        } else {
            // update JSON
            var combinedItemsField = $("#HighlightsTabContent").find("input[type=hidden]")[0];
            var thisKey = listItem.attr("id");
            var currentItems = JSON.parse(combinedItemsField.value);
            var thisObject;
            for (var i = 0; i < currentItems.length; i++) {
                if (thisKey === currentItems[i].Key) { thisObject = currentItems[i]; break; }
            }
            thisObject.Text = $(this).val();
            combinedItemsField.value = JSON.stringify(currentItems);
        }
        $(listItem).dblclick(SalesAccelerator.EditListItem);
        $(this).remove();
        $(listItem).parent().disableSelection();
        
        SalesAccelerator.SetupSortingShortcuts("HighlightsTabContent");
    });
    
    // add textbox to list item
    $(this).append(textbox);
    
    // Select text
    $(this).parent().enableSelection();
    textbox.focus();
    textbox.select();
};

/// Sets up textbox for adding a Consumer Highlight.
SalesAccelerator.AddListItem = function(e) {

    // Create list item and textbox
    var newListItem = document.createElement("li");
    var newKey = guid();
    var textbox = document.createElement("input");
    $(newListItem).attr("id", newKey);
    $(newListItem).addClass("editing");
    
    // Setup textbox
    $(textbox).addClass("text");
    if (document.all) {
        $(textbox).bind("blur", function() {
            var text = $(this).val();
            var listItem = $(this).parent();
            if (text.replace(' ', '').length > 0) {
                $(listItem).text($(this).val());
                $(listItem).removeClass("editing");
                $(listItem).dblclick(SalesAccelerator.EditListItem);
                
                var newHighlight = {
                    ConsumerHighlightId: null,
                    Text: $(this).val(),
                    Rank: $(listItem).siblings().length + 1,
                    IsDeleted: false,
                    IsDisplayed: true,
                    IsNew: true,
                    HighlightProvider: $("#HighlightsTabContent").find("input[type=hidden].providerId")[0].value,
                    CanEdit: true,
                    CanDelete: true,
                    ExpirationDate: null,
                    Key: newKey
                };
                
                // Add to hidden field
                var combinedItemsField = $("#HighlightsTabContent").find("input[type=hidden].items")[0];
                var objArrayFromJSON = JSON.parse(combinedItemsField.value);
                objArrayFromJSON.push(newHighlight);
                combinedItemsField.value = JSON.stringify(objArrayFromJSON);
                
                SalesAccelerator.SetupSortingShortcuts("HighlightsTabContent");
                
            } else {
                $(listItem).remove();
            }
            $(listItem).parent().disableSelection();
         });
    } else {
        // Used this ugly line b/c jquery bind didn't work in FF
        // (2nd parameter should be minified version of statements in jquery bind blur above)
        $(textbox).attr("onblur", "var text=$(this).val();var listItem=$(this).parent();if(text.replace(' ','').length>0){$(listItem).text($(this).val());$(listItem).removeClass('editing');$(listItem).dblclick(SalesAccelerator.EditListItem);var newHighlight={ConsumerHighlightId:null,Text:$(this).val(),Rank:$(listItem).siblings().length+1,IsDeleted:false,IsDisplayed:true,IsNew:true,HighlightProvider:$('#HighlightsTabContent').find('input[type=hidden].providerId')[0].value,CanEdit:true,CanDelete:true,ExpirationDate:null,Key:'" + newKey + "'};var combinedItemsField=$('#HighlightsTabContent').find('input[type=hidden].items')[0];var objArrayFromJSON=JSON.parse(combinedItemsField.value);objArrayFromJSON.push(newHighlight);combinedItemsField.value=JSON.stringify(objArrayFromJSON);}else{$(listItem).remove();}$(listItem).parent().disableSelection();");
    }
    
    // Get UL element
    var list = $(this).parent().siblings("ul.displayed");
    
    // Append new elements
    $(newListItem).append(textbox);
    list.append(newListItem);
    
    // Set focus
    $(list).enableSelection();
    $(list).find("input").focus();
    
    e.preventDefault();
    
};

/// ============================================================
/// ===  End: Additional Highlights free text functions  =======
/// ===  Start: Generic UI functionality  ======================
/// ============================================================

SalesAccelerator.ShowCustomDialog = function(dialog) {
    var dialogDiv = document.createElement("div");
    
    switch (dialog) {
    
        case "WebsitePdfActive":
            // setup warning dialog
            dialogDiv.setAttribute("id", dialog + "WarningDialog");
            dialogDiv.innerHTML = "<p>The Website PDF upgrade is not active for this account. Please ask your FirstLook representative about an upgrade to access the Website PDF.</p>";
            
            $("body").append(dialogDiv);
            $("#" + dialog + "WarningDialog").dialog({ closeText: "", dialogClass: "warningDialog", title: "Website PDF Upgrade Required", modal: true, buttons: { "Ok": function() { $(this).dialog("close"); $("#" + dialog + "WarningDialog").remove(); $(".previewSheetDDL").val("customer"); } } });
            break;
            
        case "VehicleNotCertified":
            // setup warning dialog
            dialogDiv.setAttribute("id", dialog + "WarningDialog");
            dialogDiv.innerHTML = "<p>This vehicle is not marked as certified in the system. Please confirm that you intend to offer this vehicle as certified and are approving the Selling Sheet for this vehicle as a certified vehicle.</p>";
            $(dialogDiv).dialog({ closeText: "", dialogClass: "certifiedDialog", title: "Vehicle Is Not Certified", modal: true , close: function() {
                        $(this).dialog("destroy");
                        $("#VehicleNotCertifiedWarningDialog").remove();
                        $(".certifiedDialog").remove();
                    }, buttons: { "Ok": function() {
                        $(this).dialog("close");
                        $("#CertifiedBenefitsUpdatePanel").show();
                        $("#VehicleLogos .cpoLogo").show();
                    }, "Cancel": function() {
                        $(this).dialog("close");
                        $("#SellingSheetIncludes .ckbList label:contains('Certified Benefits')").prev("input")[0].checked = false;
                        $("#EditCertifiedBenefitsPanel").find(".displayOnSheetCkb input")[0].checked = false;
                        $("#CertifiedBenefitsUpdatePanel").hide(); $("#VehicleLogos .cpoLogo").hide();
                    }
                } });
            break;

        
    }
    
};

SalesAccelerator.EqualizeColumnHeights = function(columns) {
    
    var tallest = 0;
    
    for ( var i = 0; i < columns.length; i++ ) {
        $(columns[i]).css("height", "auto");
        var height = $(columns[i]).height();
        if (height > tallest) { tallest = height; }
    }
    
    for ( var j = 0; j < columns.length; j++ ) {
        $(columns[j]).height(tallest);
    }
    
};

/// ============================================================
/// ===  Start: Generic UI functionality  ======================
/// ===  Start: Module tab functions  ==========================
/// ============================================================

/// Handles display of Highlights tab content
SalesAccelerator.HighlightTabClick = function(target) {
    $("#ConsumerHighlightsTabContent .highlightTabContent").hide();
    $("#HighlightTabs li").removeClass("selected");
    $(target).parent().addClass("selected");
    
    var tabId = ($(target).parent().attr("id"));
    
    switch (tabId) {
        case "JDPowerTab":
            SalesAccelerator.SetupSortingShortcuts("JDPowerTabContent");
            $("#JDPowerTabContent").show();
            break;
        case "ReviewsAwardsTab":
            SalesAccelerator.SetupSortingShortcuts("ReviewsAwardsTabContent");
            $("#ReviewsAwardsTabContent").show();
            break;
        case "VHRTab":
            SalesAccelerator.SetupSortingShortcuts("VHRTabContent");
            $("#VHRTabContent").show();
            break;
        case "HighlightsTab":
            SalesAccelerator.SetupSortingShortcuts("HighlightsTabContent");
            $("#HighlightsTabContent").show();
            break;
    }
};

/// Handles display of Market Listings tab content
SalesAccelerator.MarketListingsTabClick = function(target) {

    $("#MarketListingsControl .marketListingsTabContent").hide();
    $("#MarketListingsTabs li").removeClass("selected");
    $(target).parent().addClass("selected");
    
    var tabId = ($(target).parent().attr("id"));
    
    switch (tabId) {
        case "PowerSearchResultsTab":
            $("#MarketListingsSearchPanel").show();
            break;
        case "SelectedListingsTab":
            $("#MarketListingsSelectedPanel").show();
            break;
    }
    
};

/// ============================================================
/// ===  End: Vehicle Highlights tab functions  ================
/// ===  Start: AJAX event handlers ============================
/// ============================================================

SalesAccelerator.onInitializeRequest = function(sender, args) {

    $(".pageBreak").remove();
    $(".moduleEditLink").hide();
    
    // set globals
    SalesAccelerator.postBackElementId = args.get_postBackElement().id;
    if (SalesAccelerator.postBackElementId === undefined) { return; }
    var postBackElement = $get(SalesAccelerator.postBackElementId);
    SalesAccelerator.moduleContainerId = $(postBackElement).parents(".moduleContainer:first").attr("id");
    
    // set return mode (edit/preview)
    SalesAccelerator.returnMode = "Preview";
    
        // if save button (not save & close button) causes postback
        if ($(postBackElement).hasClass("saveBtn") && !$(postBackElement).hasClass("closeBtn")) { SalesAccelerator.returnMode = "Edit"; }
        
        // if equipment provider drop-down causes postback
        if (SalesAccelerator.postBackElementId === "VehicleFeaturesPreference_ProviderList") { SalesAccelerator.returnMode = "Edit"; }
        
        // if market listings filter (or clear filters) button causes postback
        var filterButtonRegEx = /MarketListingVehiclePreference_[a-z]+Filter[s]*Button/i;
        if (filterButtonRegEx.test(SalesAccelerator.postBackElementId)) { SalesAccelerator.returnMode = "Edit"; }
        
        // if market listings sort button causes postback
        var sortButtonRegEx = /MarketListingVehiclePreference_MarketListings[a-z]+ColumnSortLink/i;
        if (sortButtonRegEx.test(SalesAccelerator.postBackElementId)) { SalesAccelerator.returnMode = "Edit"; }
        
        // if market listings paging button causes postback
        var pageButtonRegEx = /MarketListingVehiclePreference_PagingRepeater_[a-z0-9]+_PageLinkButton/i;
        if (pageButtonRegEx.test(SalesAccelerator.postBackElementId)) { SalesAccelerator.returnMode = "Edit"; }
        
        // if reset to defaults link causes postback
        var resetToDefaultsRegEx = /\w+ResetToDefaults\w*/;
        if (resetToDefaultsRegEx.test(SalesAccelerator.postBackElementId)) { SalesAccelerator.returnMode = "Edit"; }
    
    // Reset relevant globals on 'Save' click
    if ($(postBackElement).hasClass("saveButton")) { SalesAccelerator.ResetModuleEditGlobals(); }
    
};

SalesAccelerator.onBeginRequest = function(sender, args) {

    if (SalesAccelerator.postBackElementId === undefined) { return; }
    var postBackElement = $get(SalesAccelerator.postBackElementId);
    var moduleContainer = $(postBackElement).parents(".moduleContainer:first").parent();
    $("input").blur();

    if (SalesAccelerator.moduleContainerId === "VehicleInfoHeader") {
        moduleContainer = $(postBackElement).parents(".moduleContainer:first");
        moduleContainer.append("<div class=\"pleaseWaitPagePanel\"><table style=\"height:" + $(moduleContainer).height() + "px;\"><tr><td><p>Please wait ...</p></td></tr><table></div>");
    } else if (SalesAccelerator.moduleContainerId === "PreviewForm") {
        // add please wait message to payment info, pricing analysis and market listings
        if ($("#PricingAnalysis .viewModulePanel").html().trim() !== "") {
            $("#PricingAnalysis").append("<div class=\"pleaseWaitPanel\" style=\"width:651px;\"><table style=\"height:" + $("#PricingAnalysis").height() + "px;width:651px;\"><tr><td><p>Please wait ...</p></td></tr><table></div>");
        }
        if ($("#MarketListings .viewModulePanel").html().trim() !== "") {
            $("#MarketListings").append("<div class=\"pleaseWaitPanel\" style=\"width:651px;\"><table style=\"height:" + $("#MarketListings").height() + "px;width:651px;\"><tr><td><p>Please wait ...</p></td></tr><table></div>");
        }
        if ($("#PaymentInfoUpdatePanel .viewModulePanel").html().trim() !== "") {
            $("#PaymentInfoUpdatePanel .viewModulePanel").append("<div class=\"pleaseWaitPanel\" style=\"width:651px;\"><table style=\"height:" + $("#PaymentsInformationDiv").height() + "px;width:651px;\"><tr><td><p>Please wait ...</p></td></tr><table></div>");
        }
    } else if (SalesAccelerator.moduleContainerId === "VehiclePhoto") {
        $("#VehiclePhoto").append("<div class=\"pleaseWaitPanel\"><table style=\"width:100%;height:" + $("#VehiclePhoto").height() + "px;\"><tr><td><p style=\"white-space:nowrap\">Please wait ...</p></td></tr></table></div>");
    } else {
        moduleContainer.parent().append("<div class=\"pleaseWaitPanel\"><table style=\"width:100%;height:" + $(moduleContainer).parent().height() + "px;\"><tr><td><p>Please wait ...</p></td></tr></table></div>");
    }
    
};

SalesAccelerator.onPageLoaded = function(sender, args) {

    Page.setupCheckOfferPrice();
    Page.setupDatePicker();
    
    SalesAccelerator.BindUIEventHandlers("PreviewForm");
    SalesAccelerator.BindUIEventHandlers("ModuleContainer");
    $(".moduleEditLink").show();
    
    // Add 'close' buttons to VHR Widgets drop-downs
    $(".carfax_report .vhr_report_form").prepend("<a href=\"#\" class=\"closeVhrWidgetLink\" onclick=\"$(this).parents('.moduleContainer').find('input.carfax_button:first').click();return false;\">Close</a>");
    $(".autocheck_report .vhr_report_form").prepend("<a href=\"#\" class=\"closeVhrWidgetLink\" onclick=\"$(this).parents('.moduleContainer').find('input.autocheck_button:first').click();return false;\">Close</a>");
        
    // Setup Vehicle Features (Equipment)
    SalesAccelerator.BindUIEventHandlers("VehicleFeatures");
    
    // Setup Consumer Highlights
    Page.OrderConsumerHighlights();
    SalesAccelerator.BindUIEventHandlers("ConsumerHighlights");
    $("#HighlightsTab").addClass("selected");
    $("#HighlightsTabContent").show();
    SalesAccelerator.OrderHighlightSources();
    
        // Disable Consumer Highlights 'Include CarFax 1-Owner icon' checkbox
        if ($("#ViewHighlightsPanel li:contains(CARFAX 1-Owner)").length === 0) {
            var checkbox = $("#CarFaxCheckBoxDiv input:checkbox");
            $(checkbox).attr("disabled", "disabled");
            $(checkbox).removeAttr("checked");
            $(checkbox).siblings("label").addClass("greyed");
        } else {
            $("#CarFaxCheckBoxDiv input:checkbox").removeAttr("disabled");
        }
        
    // Setup Ad Preview (vehicle description)
    SalesAccelerator.BindUIEventHandlers("VehicleDescription");
    SalesAccelerator.SetAdPreviewLength(parseInt($("#EditVehicleDescription input:hidden.length:first").val(), 10));
    
    // Setup Certified Benefits
    SalesAccelerator.BindUIEventHandlers("CertifiedBenefits");
    
    // Setup Payments form
    SalesAccelerator.TogglePaymentFormFields();
    SalesAccelerator.BindUIEventHandlers("PaymentInfoForm");
    
    // Setup Pricing Analysis
    SalesAccelerator.BindUIEventHandlers("PricingAnalysis");
    
    // Setup Market Listings
    SalesAccelerator.BindUIEventHandlers("MarketListings");
         
    // Setup Market Listings Display Ad Preview checkbox
    if ($("#DisplayAdPreviewCheckboxDiv input[type=checkbox]").is(":checked")) {
        $("tr.vehicleDescription, tr.vehicleDescription td, #MarketComparisonListings tr td.mcItemDesc").show();
    } else {
        $("tr.vehicleDescription, tr.vehicleDescription td, #MarketComparisonListings tr td.mcItemDesc").hide();
    }
    
    // Setup Market Listings Display Dealer Name checkbox
    if ($("#DisplayDealerNameCheckbox input[type=checkbox]").is(":checked")) {
        $("#MarketListingsSelectedPanel .dealer").show();
    } else {
        $("#MarketListingsSelectedPanel .dealer").hide();
    }
    
    // Setup Market Listings Power Search active filters
    SalesAccelerator.SetMarketListingsFiltersMessage();
    
    $(".pleaseWaitPanel").remove();
    $(".pleaseWaitPagePanel").remove();
    $(".publishingMessage").remove();
    $(".print-sheet-button").removeClass("disabled");
    
    SalesAccelerator.SetDisplayCheckboxes();
    
    // reload page if vehicle info header is saved
    if (SalesAccelerator.postBackElementId === "VehicleInformation_Inventory_VehicleFormView_SaveButton") {
        $("#Container").css("visibility", "hidden");
        document.location.reload(true);
    }
    
    // which view are we showing
    var selectedPacketHiddenValue = $(".salesPacketMode").val();
    var selectedSalesSheet = selectedPacketHiddenValue === "" ? "sales" : selectedPacketHiddenValue;
    
    // update the hidden field with the sales packet view
    $(".salesPacketMode").val( selectedSalesSheet );

    SalesAccelerator.ToggleSheetView(0);
    
    if (SalesAccelerator.returnMode === "Edit") {
        
        // hack to fix FB Case 13448
        if (SalesAccelerator.postBackElementId === "MarketListingVehiclePreference_LinkButtonResetToDefaults") {
            $("#MarketListings").show();
            $("#WebsitePdfIncludes label:contains('Market Listings')").prev("input[type=checkbox]").attr("checked", "true");
        }
        // hack to fix same problem with certified benefits
        if (SalesAccelerator.postBackElementId === "BenefitsVehiclePreference_ResetToDefaultsButton") {
            $("#CertifiedBenefitsUpdatePanel").show();
            $("#SellingSheetIncludes label:contains('Certified Benefits')").prev("input[type=checkbox]").attr("checked", "true");
        }
    
        $("#" + SalesAccelerator.moduleContainerId).find(".viewModulePanel").hide();
        $("#" + SalesAccelerator.moduleContainerId).find(".editModulePanel").show();
        
        // TODO: Refactor these lines as they are duplicated in showEditPanel()
        SalesAccelerator.EditModuleHtml = $("#" + SalesAccelerator.moduleContainerId).parent().html();
        SalesAccelerator.ViewStateSnapshot = $("#__VIEWSTATE").val();
        SalesAccelerator.EventValidationSnapshot = $("#__EVENTVALIDATION").val();
        
    } else {
        $(".ui-dialog-content").dialog("close");
    }
    
    // Setup photo editing
    SalesAccelerator.TogglePhotoEdit(true);
    
    // Handle dynamic css for vehicle photo layout
    if (SalesAccelerator.moduleContainerId === "VehiclePhoto" && $("#VehiclePhoto").find(".displayOnSheetCkb input")[0].checked === false) {
        $("#VehicleSummaryPanel").addClass("noVehiclePhoto");
    }
    
    // Display container
    $("#Container").show();
    
    SalesAccelerator.SetPageBreaks();

    // Setup Miscellaneous Page Defaults
    $(".previewSheetDDL").val("website");
    SalesAccelerator.ToggleSheetView( 0 ); // immediate

    // +-- MAX Info Display
    //     +-- Publishing Options
    //         +-- Post Online => Checked
    $("#PreviewNavigationForm_VAPDFActionsCkbList_1")[0].checked = true;

    // Setup "Approve/Publish to turn Green when something changes on the page"
    $("input").change( function() {
        $("#CVASettingsForm .print-sheet-button").addClass("NOTICEME");
    });
};

SalesAccelerator.onEndRequest = function(sender, args) {

    if (args.get_error()) {
        var message = "An unknown error has occurred.\r\n\r\nClick 'OK' to reload the page and try again."; //args.get_error().message;
        if (confirm(message)) {
            $("#Container").hide();
            document.location.reload(true);
        }
    }
        
    // reset globals
    SalesAccelerator.returnMode = undefined;
    SalesAccelerator.postBackElementId = undefined;
    SalesAccelerator.moduleContainerId = undefined;
    
};

SalesAccelerator.BindUIEventHandlers = function(module) {
        
    var turnGreen = function() {
        $("#CVASettingsForm .print-sheet-button").addClass("NOTICEME");
    };

    switch(module) {
    
        case "CertifiedBenefits":
            SalesAccelerator.MakeSortable("CertifiedBenefits", "ul");
            break;
    
        case "ConsumerHighlights":
            $("#HighlightsTabContent .displayed li").unbind("dblclick").dblclick(SalesAccelerator.EditListItem);
            $(".addHighlight").unbind("click").click(SalesAccelerator.AddListItem);
            $("#HighlightTabs li a").unbind("click").click( function() {
                SalesAccelerator.HighlightTabClick($(this));
            });
            
            // Set up Free-Text Highlight functionality.
            $("#HighlightsTabContent .displayed li").unbind("dblclick").dblclick(SalesAccelerator.EditListItem);
            $(".addHighlight").unbind("click").click(SalesAccelerator.AddListItem);
                       
            SalesAccelerator.MakeSortable("HighlightsTabContent", "ul");
            SalesAccelerator.MakeSortable("JDPowerTabContent", "ul");
            SalesAccelerator.MakeSortable("ReviewsAwardsTabContent", "ul");
            SalesAccelerator.MakeSortable("VHRTabContent", "ul");
            break;
            
        case "ModuleContainer":
            $(".moduleEditLink").unbind("click").click(SalesAccelerator.showEditPanel).click(turnGreen);
            $(".cancelBtn").unbind("click").click( function(event) { SalesAccelerator.CancelModuleEdit(event); return false; } );
            break;
    
        case "MarketListings":
            // Setup Market Listings tabs.
            $("#MarketListingsTabs li a").unbind("click").click( function() {
                SalesAccelerator.MarketListingsTabClick($(this)); return false;
            });
            
            // Setup Market Listings filter panels.
            $("#MarketListingsPreferenceTable .filterPanelLink").unbind("click").click( function() {
                SalesAccelerator.ToggleFilterPanel($(this));
                $("body").click( function(event) {
                    if (!$(event.target).hasClass("filterPanel") && $(event.target).parents(".filterPanel:first").length === 0) {
                        $(".filterPanel").hide();
                        $(".filterPanelLink").removeClass("open");
                        $("body").unbind("click");
                    }
                });
                return false;
            });
            
            // Setup Market Listings Add/Remove
            $("#MarketListingsPreferenceTable tbody input[type=checkbox], #MarketListingsSelectedPanel tbody input[type=checkbox]").unbind("click").click( function() {
                SalesAccelerator.MarketListingsCheckboxClick($(this));
            });
            
            // Setup Search List rows to be clickable
            $("#MarketListingsPreferenceTable tbody tr").unbind("click").click( function(event) {
                if ($(this).hasClass("vehicleDescription")) { return; } // disable this for ad preview rows
                if (event.target.type !== "checkbox") {
                    $(":checkbox", this)[0].checked = !$(":checkbox", this)[0].checked;
                    $(":checkbox", this).triggerHandler("click");
                }
            });
            
            // Setup Market Listings Display Ad Preview checkbox
            $("#DisplayAdPreviewCheckboxDiv input[type=checkbox]").unbind("click").click( function() {
                if ($(this).is(":checked")) {
                    $("tr.vehicleDescription, tr.vehicleDescription td, #MarketComparisonListings tr td.mcItemDesc").show();
                } else {
                    $("tr.vehicleDescription, tr.vehicleDescription td, #MarketComparisonListings tr td.mcItemDesc").hide();
                }
            });
            
            // Setup Market Listings Show Dealer Name checkbox
            $("#DisplayDealerNameCheckbox input[type=checkbox]").unbind("click").click( function() {
                if ($(this).is(":checked")) {
                    $("#MarketListingsSelectedPanel .dealer").show();
                } else {
                    $("#MarketListingsSelectedPanel .dealer").hide();
                }
            });
            
            // Setup async postback when user opens and closes dealer preference
            $("#MarketListingsVehiclePreferences .prefPageActions a.dealerPreference").unbind("click").click( SalesAccelerator.OpenChildWindow );
            
            break;
            
        case "PaymentInfoForm":
            $("#LeasePaymentsInfo input:checkbox:first, #OwnPaymentsInfo input:checkbox:first").unbind("click").click( SalesAccelerator.TogglePaymentFormFields );
            break;
            
        case "PreviewForm":
            // Setup sheet view drop-down-list.
            $(".previewSheetDDL").change(SalesAccelerator.ToggleSheetView);
            $("#SalesPacketPreviewWidget ul li a").unbind("click").click( function() { SalesAccelerator.ToggleSheetView(0, $(this).attr("name")); return false; } );

            $("#SellingSheetIncludes input[type=checkbox]").click(Page.ToggleModuleView);
            $("#WebsitePdfIncludes input[type=checkbox]").click(Page.ToggleModuleView);

            $("#SellingSheetIncludes .ckbList label:contains('Certified Benefits')").prev("input:first").unbind("click").bind("click", function() {
                if ($("#CertifiedVehiclePreferences .CPOV")[0].value !== "1" && $(this).is(":checked")) {
                    SalesAccelerator.ShowCustomDialog("VehicleNotCertified");
                }
            }).bind("click", Page.ToggleModuleView);
                            
            $("#SellingSheetIncludes input:checkbox, #MarketComparisonIncludes input:checkbox, #SellingSheetIncludes label, #MarketComparisonIncludes label, #WebsitePdfIncludes input:checkbox, #WebsitePdfIncludes label").hover( function() {
                            
                var labelText = $(this).is(":checkbox") ? $(this).next("label").text().toLowerCase() : $(this).text().toLowerCase();

                switch (labelText) {
                    case "vehicle photo":
                        $("#VehiclePhoto").addClass("highlightModule");
                        break;
                        
                    case "equipment":
                        $("#VehicleFeatures .viewModulePanel").addClass("highlightModule");
                        break;
                        
                    case "vehicle highlights":
                        $("#ConsumerHighlights .viewModulePanel").addClass("highlightModule");
                        break;
                        
                    case "ad preview":
                        $("#VehicleDescription .viewModulePanel").addClass("highlightModule");
                        break;
                        
                    case "certified benefits":
                        $("#CertifiedBenefits .viewModulePanel").addClass("highlightModule");
                        break;
                        
                    case "market listings":
                        $("#MarketListings .viewModulePanel").addClass("highlightModule");
                        break;
                        
                    default:
                        if (labelText.indexOf("pricing analysis") > -1) {
                            $("#PricingAnalysis .viewModulePanel").addClass("highlightModule");
                        }
                        break;
                        
                }
            });
            
            $("#SellingSheetIncludes input:checkbox, #MarketComparisonIncludes input:checkbox, #SellingSheetIncludes label, #MarketComparisonIncludes label, #WebsitePdfIncludes input:checkbox, #WebsitePdfIncludes label").mouseout( function() {
            
                var labelText = $(this).is(":checkbox") ? $(this).next("label").text().toLowerCase() : $(this).text().toLowerCase();
                
                switch (labelText) {
                    case "vehicle photo":
                        $("#VehiclePhoto").removeClass("highlightModule");
                        break;
                        
                    case "equipment":
                        $("#VehicleFeatures .viewModulePanel").removeClass("highlightModule");
                        break;
                        
                    case "vehicle highlights":
                        $("#ConsumerHighlights .viewModulePanel").removeClass("highlightModule");
                        break;
                        
                    case "certified benefits":
                        $("#CertifiedBenefits .viewModulePanel").removeClass("highlightModule");
                        break;
                        
                    case "ad preview":
                        $("#VehicleDescription .viewModulePanel").removeClass("highlightModule");
                        break;
                        
                    case "market listings":
                        $("#MarketListings .viewModulePanel").removeClass("highlightModule");
                        break;
                        
                    default:
                        if (labelText.indexOf("pricing analysis") > -1) {
                            $("#PricingAnalysis .viewModulePanel").removeClass("highlightModule");
                        }
                        break;
                        
                }
            });
            
            $("#PreviewForm .updateDeal").unbind("click").click( function() { return SalesAccelerator.RefreshOfferPrice(); } );
            $("#PreviewForm .updateDeal").unbind("keypress").keypress( function() { return SalesAccelerator.RefreshOfferPrice(); } );
            $("#MarketComparisonIncludes input[type=checkbox]").unbind("click").click(Page.ToggleModuleView);
            $(".mktListingRadButtons input").unbind("click").click(SalesAccelerator.ToggleMarketListingVersion);
            break;
            
        case "PricingAnalysis":
            $("#PricingAnalysisVehiclePreferences table a.bookout").unbind("click").click( SalesAccelerator.OpenChildWindow );
            break;
    
        case "VehicleDescription":
            $("#SetAdPreview150Button").unbind("click").click( function() { SalesAccelerator.SetAdPreviewLength(150); return false; } );
            $("#SetAdPreview250Button").unbind("click").click( function() { SalesAccelerator.SetAdPreviewLength(250); return false; } );
            $("#SubtractWordButton").unbind("click").click( SalesAccelerator.SetAdPreviewLength );
            $("#AddWordButton").unbind("click").click( SalesAccelerator.SetAdPreviewLength );
            break;
    
        case "VehicleFeatures":
            SalesAccelerator.MakeSortable("VehicleFeatures", "ul");
            $("#VehicleFeatures #Providers a.bookout").unbind("click").click( SalesAccelerator.OpenChildWindow );
            break;
    
        case "VehiclePhoto":
            $("#VehiclePhoto").find("a.vehiclePhotoEditLink").unbind("click").click( SalesAccelerator.OpenChildWindow );
            $("#VehiclePhoto").find("img").unbind("click").click( SalesAccelerator.OpenChildWindow );
            $("#VehiclePhoto").unbind("click").click(turnGreen);
            break;
    
    }
    
};

/// ============================================================
/// ===  End: AJAX event handlers  =============================
/// ===  Start: Non-static methods  ============================
/// ============================================================

SalesAccelerator.prototype = {

    /// Initializes client-side page functionality.
    onReady: function(e) {
        
        SalesAccelerator.IsValidOfferPrice();
        this.setupTextFieldScrubbing();
                
        // Setup AJAX event handlers.
        SalesAccelerator.pageRequestManager.add_initializeRequest(SalesAccelerator.onInitializeRequest);
        SalesAccelerator.pageRequestManager.add_beginRequest(SalesAccelerator.onBeginRequest);
        SalesAccelerator.pageRequestManager.add_pageLoaded(SalesAccelerator.onPageLoaded);
        SalesAccelerator.pageRequestManager.add_endRequest(SalesAccelerator.onEndRequest);

    },
    
    /// Order Consumer Highlight sections
    OrderConsumerHighlights: function() {
        var container = $(".highlightsPreview")[0];
        var sections = $(container).find(".highlightSection");
        
        for (var i = 0; i < sections.length; i++) {
            $(container).prepend($(container).find(".highlightSection[rank=" + i + "]"));
        }
    },
    
    /// Show/Hide individual modules using checkbox list in left-hand column form
    ToggleModuleView: function() {
    
        // make sure this is a 'toggle module' checkbox
        if ($(this).parents(".ckbList").length < 1) { return; }
        
        var module, checkboxPanel;
        var moduleName = $(this).next("label").text().trim().toLowerCase();
        var display = $(this)[0].checked;
        var connectedCkbList;
        if ($(this).parent(".ckbList").parent("div")[0]) {
             connectedCkbList = $(this).parent(".ckbList").parent("div")[0].id === "SellingSheetIncludes" ? "WebsitePdfIncludes" : "SellingSheetIncludes";
        }
        
        switch (moduleName) {
            case "vehicle photo":
                module = $("#VehiclePhoto");
                if (display) { $("#VehicleSummaryPanel").removeClass("noVehiclePhoto"); }
                else { $("#VehicleSummaryPanel").addClass("noVehiclePhoto"); }
                checkboxPanel = module;
                
                // keep this checkbox in sync with correspnding checkbox on Website PDF
                if (connectedCkbList !== undefined) {
                    $("#" + connectedCkbList + " label:contains('Vehicle Photo')").prev("input[type=checkbox]")[0].checked = display;
                }
                break;
                
            case "equipment":
                module = $("#FeaturesUpdatePanel");
                checkboxPanel = $("#EditFeaturesPanel");
                
                if (connectedCkbList !== undefined && $(this).parent(".ckbList").parent("div")[0].id !== "MarketComparisonIncludes") { // keep this checkbox in sync with correspnding checkbox on Website PDF
                    $("#" + connectedCkbList + " label:contains('Equipment')").prev("input[type=checkbox]")[0].checked = display;
                }
                break;
                
            case "vehicle highlights":
                module = $("#HighlightsUpdatePanel");
                checkboxPanel = $("#EditHighlightsPanel");
                
                if (connectedCkbList !== undefined) { // keep this checkbox in sync with correspnding checkbox on Website PDF
                    $("#" + connectedCkbList + " label:contains('Vehicle Highlights')").prev("input[type=checkbox]")[0].checked = display;
                }
                break;
                
            case "ad preview":
                module = $("#VehicleDescriptionUpdatePanel");
                checkboxPanel = $("#VehicleDescriptionUpdatePanel");
                
                if (connectedCkbList !== undefined && $("#" + connectedCkbList + " label:contains('Ad Preview')").length > 0) { // keep this checkbox in sync with correspnding checkbox on Website PDF
                    $("#" + connectedCkbList + " label:contains('Ad Preview')").prev("input[type=checkbox]")[0].checked = display;
                }
                break;
                
            case "certified benefits":
                module = $("#CertifiedBenefitsUpdatePanel");
                checkboxPanel = $("#EditCertifiedBenefitsPanel");
                
                if (connectedCkbList !== undefined) { // keep this checkbox in sync with correspnding checkbox on Website PDF
                    
                    var message = $("#CertifiedVehiclePreferences .messagingLabel").text().trim();
                    var vehicleIsCertified = $("#CertifiedVehiclePreferences .CPOV")[0].value === "1";
                    var hasCertifiedMessage = message.length > 0;
                    var cpoAssociationExists = $("#CertifiedVehiclePreferences .CPOASSOC")[0].value === "1";
                    
                    // we only sync the website pdf certified checkbox if the vehicle is indeed certified and there is no certified message
                    if (connectedCkbList === "SellingSheetIncludes" || (connectedCkbList === "WebsitePdfIncludes" && vehicleIsCertified && ! hasCertifiedMessage)) {
                        $("#" + connectedCkbList + " label:contains('Certified Benefits')").prev("input[type=checkbox]")[0].checked = display;
                    }
                    
                    // show the cpo logo if this vehicle is certified or they have selected certified benefits
                    if(display || (vehicleIsCertified && cpoAssociationExists)) {
                        $("#VehicleLogos .cpoLogo").show();
                    } else {
                        $("#VehicleLogos .cpoLogo").hide();
                    }                    
                }
                break;
                
            case "market listings":
                if (display) { $("#MarketListings").show(); $(".mktListingRadButtons input").removeAttr("disabled"); }
                else { $(".mktListingRadButtons input").attr("disabled", "disabled"); }

                if ($(".previewSheetDDL").val() === "customer") {
                    module = $("#ViewMarketListingsPanel")[0];
                } else {
                
                    if ($(".mktListingRadButtons input:checked").val() === "short") {
                        module = $("#ViewMarketListingsPanel")[0];
                    } else {
                        module = $("#MarketComparisonListings")[0];
                    }
                    
                }
                
                checkboxPanel = $("#EditMarketListingsPanel");
                
                break;
                
            default:
                if (moduleName.indexOf("pricing analysis") > -1) {
                    module = $("#PricingAnalysisPreview_PricingPanel")[0];
                    checkboxPanel = $("#EditPricingAnalysisPanel");
                    
                    // keep this checkbox in sync with correspnding checkbox on Website PDF
                    if (connectedCkbList !== undefined) {
                        $("#" + connectedCkbList + " label:contains('Pricing Analysis')").prev("input[type=checkbox]")[0].checked = display;
                    }
                }
                break;
        }
        
        // Check/uncheck corresponding panel hidden checkbox.
        if ($(this).parents("#MarketComparisonIncludes").length === 0) {
            $(checkboxPanel).find(".displayOnSheetCkb input")[0].checked = $(this).is(":checked");
        }
        
        // Show/hide module.
        var callback = function() { SalesAccelerator.SetPageBreaks(); };
        if (module !== undefined) {
            if (display) { $(module).slideDown( callback ); }
            else { $(module).slideUp( callback ); }
        }
        
    },
    
    /// Sets up jQuery datepicker in left-hand form.
    setupDatePicker: function() {
        $('.datePickerText').datepicker({
            buttonImage: this.getDatePickerButtonImage(),
            minDate: new Date(),
            maxDate: this.getMaxDateDate(),
            nextText: '&gt;',
            prevText: '&lt;',
            onSelect: function() {}
		});
    },
    
    /// Returns max date value (today + 14) to setupDatePicker.
    getMaxDateDate: function() {
        if (! !! this._maxDateDate) {
            var maxDateDate = new Date();
			maxDateDate.setDate(maxDateDate.getDate() + 14);
			this._maxDateDate = maxDateDate;
        }
        return this._maxDateDate;
    },
    
    /// Returns datepicker icon path from hidden field to setupDatePicker.
    getDatePickerButtonImage: function() {
        return $(".datePickerIconPath").val();
    },
    
    /// Adds blur event handler to offer price textbox that validates offer price.
    setupCheckOfferPrice: function() {
        $("#OfferDetails .offerPrice:first").blur(function() {
	        SalesAccelerator.IsValidOfferPrice();
	    });
    },
    
    /// Prevents entry of tags into textboxes.
    setupTextFieldScrubbing: function() {
	    $("input[type='text'], textarea").change(function() {
	        var inputValue = $(this).val();
	        var targetAngleBracket = /<\S/gi;
	        while (targetAngleBracket.test(inputValue)) {
	            inputValue = inputValue.substring(0, targetAngleBracket.lastIndex - 1) + " " + inputValue.substring(targetAngleBracket.lastIndex - 1, inputValue.length);
	        }
	        $(this).val(inputValue);
	    });
    }
};

var Page;
$(document).ready(function() {
	Page = new SalesAccelerator();
	Page.onReady();
});

function S4() {
   return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
}

function guid() {
   return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
}