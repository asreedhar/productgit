$().ready(function() {
    $("table.stripedTable tr:odd").addClass("odd");
    $("table.pricingReportTable tr td:first-child, table.pricingReportTable tr th:first-child,").addClass("highlight");
    new PrintControl();
});

function validSelectionCheck(e) {
    var sCondLen = document.getElementById('ConditionDropDown').options[document.getElementById('ConditionDropDown').selectedIndex].value.length;
    var sColLen = document.getElementById('ColorDropDown').options[document.getElementById('ColorDropDown').selectedIndex].value.length;
    if (sCondLen == 0 || sColLen == 0) {
        alert('Please select a valid Condition and Color');
        return false;
    }
}
function copyValue(e) {
    document.body.innerHTML += "<input type='hidden' id='temporaryField' value='" + document.getElementById(e).innerHTML + "'/>";
    document.getElementById('temporaryField').value = document.getElementById('temporaryField').value.replace('$', '').replace(',', '');
    var objectToCopy = document.getElementById('temporaryField');
    if (document.all) {
        var text = objectToCopy.createTextRange();
        text.execCommand("Copy");
    }
    objectToCopy.parentNode.removeChild(objectToCopy);
}

function EventDelegate(fn, scope) {
	return function (e) { fn.call(scope,e); };
}

function PrintControl() {
	this.printStyleBlock = this.getPrintStyleBlock();
    this.table = this.getTable();
    this.tradeInCol = this.getTradeInCol();
    this.dealerRetailCol = this.getDealerRetailCol();
    this.checkboxes = this.getCheckboxes();
    
    this.addClasses();
    this.bindEvents();
    if (window.opener) {
    	this.loadInitState(window.opener.document.location.href.toString());
    };
    this.onChange({target:undefined});
}
PrintControl.prototype = {
    selector: "table.pricingReportTable",
    printRules: {
    	hideTradeIn: ".tradeIn { display: none; }",
    	hideDealerRetail: ".dealerRetail { display: none; }",
    	hideCertified: "#CertifiedTotalRow { display: none; }"
    },
    getTable: function() {
        return $(this.selector).get(0);
    },
    getTradeInCol: function() {
        return $("tr td:nth-child(2), tr th:nth-child(2)", this.table).not('[colSpan!=1]');
    },
    getDealerRetailCol: function() {
        return $("tr td:nth-child(3), tr th:nth-child(3)", this.table).not('[colSpan!=1]');
    },
    getCheckboxes: function () {
    	var checkboxes = {};
    	checkboxes.tradeIn = document.getElementById('printTradeIn');
    	checkboxes.dealerRetail = document.getElementById('printDealerRetail');
    	checkboxes.certified = document.getElementById('PrintCertifiedCheckBox');
    	return checkboxes;
    },
    getPrintStyleBlock:function () {
    	var printStyles = $('style[media=print]'), style;
    	if (printStyles.length != 0) {
    		return printStyles.get(0);
    	} else {
    		style = document.createElement('style');
	    	$(style).attr({
	    		'type':'text/css',
	    		'media':'print'
			});
	    	$('head').append(style);
	    	return style;
    	}    	
    },
    loadInitState: function (openingUrl) {
    	if (openingUrl.indexOf('TradeManager') || 
    		openingUrl.indexOf('GuideBook')) {
    		this.checkboxes.tradeIn.checked = true;
		} else {
			this.checkboxes.dealerRetail.checked = true;
		}
    },    
    addClasses: function () {
    	this.tradeInCol.addClass('tradeIn');
    	this.dealerRetailCol.addClass('dealerRetail');
    },
    bindEvents: function () {
    	this.changeDelegate = new EventDelegate(this.onChange, this);
    	
    	$(this.checkboxes.tradeIn).bind('click', this.changeDelegate);
    	$(this.checkboxes.dealerRetail).bind('click', this.changeDelegate);
    	$(this.checkboxes.certified).bind('click', this.changeDelegate);
    },
    writePrintCss:function () {
    	var rules = [];
    	if (!this.checkboxes.tradeIn.checked)
    		rules.push(this.printRules.hideTradeIn);
    	if (!this.checkboxes.dealerRetail.checked)
    		rules.push(this.printRules.hideDealerRetail);
    	if (!this.checkboxes.certified.checked)
    		rules.push(this.printRules.hideCertified);
    		
    	try {
    		this.printStyleBlock.styleSheet.cssText = rules;
    	} catch (er) {
    		for (var i=0,l=this.printStyleBlock.styleSheet.cssRules.length; i < l; i++) {
    			this.printStyleBlock.styleSheet.deleteRule(i);
    		};
    		for (var i=0,l=rules.length; i < l; i++) {
    			this.printStyleBlock.styleSheet.insertRule(rules[i], i);
    		};
    	}    	
    },
    onChange: function (e) {
    	if (!this.checkboxes.dealerRetail.checked) {
    		$(this.checkboxes.certified).next('label').add(this.checkboxes.certified).hide();
    		this.certifiedState = this.checkboxes.certified.checked;
    		this.checkboxes.certified.checked = false;
    	} else {
    		$(this.checkboxes.certified).next('label').add(this.checkboxes.certified).show();
    		if (e.target === this.checkboxes.dealerRetail) {
				this.checkboxes.certified.checked = !!this.certifiedState;
				this.certifiedState = undefined;
			}
    	}
    	this.writePrintCss();
    }
    
};