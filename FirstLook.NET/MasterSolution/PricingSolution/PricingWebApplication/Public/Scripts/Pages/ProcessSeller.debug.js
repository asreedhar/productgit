
var State = {
    intervalId: 0,
    index: -1,
    vehicles: [],
    size: function () {
        return State.vehicles.length;
    },
    hasNext: function () {
        return State.index+1 < State.vehicles.length;
    },
    next: function () {
        var text = State.vehicles[++State.index].split(":");
        return { description: text[0], mileage: text[1], vin: text[2] };
    },
    currentPosition: function () {
        return State.index + 1;
    }
};

function updateCurrentVehicle() {
    if (State.hasNext()) {
        var vehicle = State.next();
        $("#VehicleDescription").html(vehicle.description);
        $("#VehicleMileage").html(vehicle.mileage);
        $("#Vin").html(vehicle.vin);
        $("#CurrentVehicleNumber").html(State.currentPosition() + " ");
        var barwidth = (State.currentPosition() / State.size() * 100) + "%";
        $("#ProgressBarFill").css( { width: barwidth });
    }
    else {
        clearInterval(intervalId);
        $("#VinContainer").hide();
        $("#VehicleContainer").hide();
        $("#CompletingContainer").show();
        form1.submit();
    }
}

$().ready(function() {
    State.vehicles = eval(document.getElementById("vehicleArray").innerHTML);
    intervalId = setInterval("updateCurrentVehicle()", 10000 / State.size());
});
