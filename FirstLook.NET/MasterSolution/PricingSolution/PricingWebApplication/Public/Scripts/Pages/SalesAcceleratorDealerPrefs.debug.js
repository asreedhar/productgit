

var SalesAcceleratorDealerPrefs = function() {
	this.onReady();
};

SalesAcceleratorDealerPrefs.ValidateCustomNames = function(sender, args) {
    if ($("#" + sender.controltovalidate).prev("input[type=radio]")[0].checked) {
        if (args.Value.replace(" ", "") === "") {
            args.IsValid = false;
            return;
        }
    }
    args.IsValid = true;
};

// ======================
// = Non-Static Methods =
// ======================
SalesAcceleratorDealerPrefs.prototype = {
    onReady: function(e) {
        this.setupTextFieldScrubbing();
        this.AddListBoxOptionTitles();
    },
    
    /// Adds a space after any opening angle bracket to avoid .NET event validation error
    setupTextFieldScrubbing: function() {
	    $("input[type='text'], textarea").change(function() {
	        var inputValue = $(this).val();
	        var targetAngleBracket = /<\S/gi;
	        while (targetAngleBracket.test(inputValue)) {
	            inputValue = inputValue.substring(0, targetAngleBracket.lastIndex - 1) + " " + inputValue.substring(targetAngleBracket.lastIndex - 1, inputValue.length);
	        }
	        $(this).val(inputValue);
	    });
    },
    
    /// Adds title attributes to list-box options (in specified list-boxes)
    AddListBoxOptionTitles: function() {
        
        var listBoxRefs = "#VehicleHistoryReports .listBox";
        $(listBoxRefs).find("option").each(function() {
            $(this).attr("title", $(this).text());
        });
        
    }
};

var Page;
$(document).ready(function() {
	Page = new SalesAcceleratorDealerPrefs();
});
