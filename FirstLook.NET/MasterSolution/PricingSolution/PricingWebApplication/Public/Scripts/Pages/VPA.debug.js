/**
 * Full Page Functions for the VPA
 * Paul Monson	
 * @version 2
 * @requires FirstLook.Utility
 * @requires FirstLook.Page
 * @requires DD_Roundies
 */
var pageLoadStatus = false,VPA = {},Page;
VPA.prototype = {
	VehicleEntityID: null,
	VehicleEntityTypeID: null
}
if ( !! Sys) {
    Type.registerNamespace("FirstLook.Pricing");
    /**
	 * @constructor
	 */
    FirstLook.Pricing.VPA = function() {
        FirstLook.Pricing.VPA.initializeBase(this);
    };
    FirstLook.Pricing.VPA.prototype = {
        parentWin: null,
        load: function() {
            pageLoadStatus = true;
            this.roundButtons();
        },
        unload: function() {
            this.updateWindowOpener();
        },
        updateWindowOpener: function() {
        	if (VPA.VehicleEntityTypeID == 2) {
                // only for VPA for an appraisal (code to interact with TA/TM on edge)
                var elm = document.getElementById('Calculator_AppraisalCalculator_AppraisalValueTextBox');
                if (elm != null) {
                    var aV = elm.value;
                    if (FirstLook.Pricing.VPA.parentWin != null && aV != null && FirstLook.Pricing.VPA.parentWin.updateAppraisalValue != null) {
                        FirstLook.Pricing.VPA.parentWin.updateAppraisalValue(aV.toString().replace(/[^0-9]/g, ""));
                    }
                }
            } else if (VPA.VehicleEntityTypeID == 1) {
                if (window.opener && !window.opener.closed) {
                    if (! (typeof(window.opener.bPageReload) == "undefined")) {
                        // can this be moved into the if statement below?
                        window.opener.bPageReload = true;
                    }
                    if (window.opener.location.pathname == '/NextGen/InventoryPlan.go') {
                        var id = VPA.VehicleEntityID;
                        window.opener.page.handleDirty(id);
                    }
                }
            }
        },
        /**
         * Describe what this method does
         * @private
         * @param {String} paramName Describe this parameter
         * @returns Describe what it returns
         * @type String
         */
        
        updateAppraisalValue: function(par, aV) {
            FirstLook.Pricing.VPA.parentWin = par;
            if (aV != null && aV > 0) {
                var componentArray = Sys.Application.getComponents();
                for (var i = 0, l = componentArray.length; i < l; i++) {
                    var component = componentArray[i];
                    if (FirstLook.Pricing.Website.AppraisalCalculator.isInstanceOfType(component)) {
                        component.set_appraisalValue(aV);
                    }
                }
            }
        },
        /**
		 * Round buttons on the Page
		 * @public
		 */
        roundButtons: function() {
            DD_roundies.addRule('.ui-toolbar-box-outer .ui-toolbar-items', '10px');
            DD_roundies.addRule('div.carfax_report div.vhr_report_form', '5px 0 5px 5px');
            DD_roundies.addRule('div.autocheck_report div.vhr_report_form', '0 5px 5px 5px');
            DD_roundies.addRule('fieldset.controls span.button', '3px');
            DD_roundies.addRule('#vpa div.market_listings', '3px');
            DD_roundies.addRule('ul.control span.button', '3px');
        }
    }

    FirstLook.Pricing.VPA.registerClass("FirstLook.Pricing.VPA", FirstLook.Page.Base);

    Sys.Application.add_init(function() {
        Page = $create(FirstLook.Pricing.VPA, null, null, null, null);

        /**
         * Legacy Code Bindings
         */
        VPA.UpdateAppraisalValue = Page.updateAppraisalValue;
    })
};
