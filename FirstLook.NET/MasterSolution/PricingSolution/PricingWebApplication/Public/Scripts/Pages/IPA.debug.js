var Page,
PopupManager,
UpdateColumnIndex;

if ( !! Sys) {
// ==================
// = Define Objects =
// ==================
	Type.registerNamespace("FirstLook.Pricing");
	FirstLook.Pricing.IPA = function() {
		FirstLook.Pricing.IPA.initializeBase(this);
	};
	FirstLook.Pricing.IPA.prototype = {
		load: function() {
			this.createDropMenus();
			this.modifyVPALink();
			this.restoreSelectedRow();
			this.fixIEButtonBorder();
			this.autoSelectInputs();
			this.bindDistributionButtonClicks();
		},
		fixIEButtonBorder: function() {
			$("div.button a, button").click(function() {
				this.blur();
			});
		},
		autoSelectInputs: function() {
			//modify inputs to auto-select
			$("input").focus(function() {
				$(this).filter("input:text:enabled").not("[readonly]").select();
			});
		},
		createDropMenus: function() {
			$("." + this.get_dropMenuClass()).each(function() {

				$(this).children("select").each(function() {
					$(this).hide();
				});
				var span = $(this).children("span");
				var slct = $(this).children("select");
				$(span).mouseover(function() {
					$(span).hide();
					$(slct).show();

					$(slct).blur(function() {
						$(this).prev("span").show();
						$(this).hide();
						$(this).unbind();
					});
					$(slct).mouseout(function(e) {
						if (e.relatedTarget) {
							$(this).prev("span").show();
							$(this).hide();
							$(this).unbind();
						}
					});
				});
			});
		},
		updateColumnIndex: function(colIdx) {
			$("#ColumnIndex").val(colIdx);
			$("#PostBackButton").trigger("click");
		},
		restoreSelectedRow: function() {
			var el = this.get_lastMenuVPALink(),
			selectedClassName = this.get_selectedClassName(),
			tr;
			if ($(el).val().length > 0) {
				tr = $("." + $(el).val()).parent().parent();
				$(tr).addClass(selectedClassName);
			}
		},
		modifyVPALink: function() {
			var className = "." + this.get_pingIconClass(),
			lastVPALink = this.get_lastMenuVPALink(),
			selectedClassName = this.get_selectedClassName();

			//make entire row clickable
			$(className).each(function() {
				var pingIcon = this;

				$(this).parents("tr").click(function(f) {
                    if (f.target.tagName != "A" && f.target.tagName != "INPUT" && f.target.tagName != "LABEL") {
						pingIcon.onclick.apply(pingIcon);
                        f.stopPropagation();
                        f.preventDefault();
					}
				});
			}).click(function(ee) { 
                ee.stopPropagation(); 
                ee.preventDefault(); 
            });
		},
		bindDistributionButtonClicks: function() {
			$('span.export_error label').bind('click', this.onDistributionStatusClick)
		},
		get_distributionErrorbox: function() {
			if (!!!this._distributionErrorbox) {
				this._distributionErrorbox = $find("DistributorMessageBox$MessageBox");
			};
			return this._distributionErrorbox;
		},
		onDistributionStatusClick: function(evt) {
			Page.updateDistributionErrorText(this.title, evt.pageY || (evt.clientY + document.body.scrollTop + document.documentElement.scrollTop));
			evt.preventDefault();
			evt.stopPropagation();
		},
		updateDistributionErrorText: function(text, posy) {
			var distributionErrorbox = Page.get_distributionErrorbox();
			distributionErrorbox.set_MessageText(text);
						
			if (!isNaN(posy)) distributionErrorbox.get_element().style.top = posy+"px";
			
			distributionErrorbox.show();
		}
	};
	FirstLook.Utility.createGetterSetters(FirstLook.Pricing.IPA, ["dropMenuClass", "lastMenuVPALink", "selectedClassName", "pingIconClass", "distributionErrorbox", "distributionDraggabelBehavior"]);
	FirstLook.Pricing.IPA.registerClass("FirstLook.Pricing.IPA", FirstLook.Page.Base);


// ======================
// = Initialize Objects =
// ======================
	Sys.Application.add_init(function() {
		Page = $create(FirstLook.Pricing.IPA, {
			"dropMenuClass": "dropMenu",
			"lastMenuVPALink": $get("LastVPALink"),
			"selectedClassName": "selected",
			"pingIconClass": "pingIcon"
		},
		null, null, null);

		UpdateColumnIndex = Page.updateColumnIndex;
	})
	Sys.Application.notifyScriptLoaded();
};
