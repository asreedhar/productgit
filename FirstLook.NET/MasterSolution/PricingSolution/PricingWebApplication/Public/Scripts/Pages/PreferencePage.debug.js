﻿var PreferencePage = function() {
	this.onReady();
};
PreferencePage.prototype = {
	onRefresh:	function() {
		this.addRoundies();
		this.addHoverToTr();
		this.addTitleToSortArrows();
		this.setFocus();
	},
	onReady: function() {
		$('body').addClass('has_javascript');
		this.addRoundies();
		this.addTitleToSortArrows();
		this.setFocus();
	},
	addHoverToTr: function() {
		var over = function(e) { $(this).addClass('hover'); };
		var out = function(e) { $(this).removeClass('hover'); };
		$('tr').bind("mouseenter", over).bind("mouseleave", out);
	},
	addTitleToSortArrows: function() {
		$('td.move_down').attr({title:"Move Down"});
		$('td.move_up').attr({title:"Move Up"});
		$('td.delete').attr({title:"Delete"});
	},
	addRoundies: function() {
		DD_roundies.addRule('fieldset.controls span.button', '3px');
	},
	setFocus: function() {
		$("input[type=text]:first").focus();
	}
};

var preferencePage;

Sys.Application.add_load(function() {
	if (! !!preferencePage) {
		preferencePage = new PreferencePage();
	} else {
		preferencePage.onRefresh();
	}	
});