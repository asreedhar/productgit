﻿// ===============================
// = This should be a webcontrol =
// ===============================


Type.registerNamespace('FirstLook.EdmundsTmv.Website');


FirstLook.EdmundsTmv.Website.OptionListButton = function (element)
{
    FirstLook.EdmundsTmv.Website.OptionListButton.initializeBase(this,[element]);
    this._OptionList = null;
}

FirstLook.EdmundsTmv.Website.OptionListButton.prototype =
{
    initialize: function() {
        FirstLook.EdmundsTmv.Website.OptionListButton.callBaseMethod(this, 'initialize');
        $addHandlers(this.get_element(), { 'click' : this._onClick }, this);
    },
    dispose : function() {
        $clearHandlers(this.get_element());
        FirstLook.EdmundsTmv.Website.OptionListButton.callBaseMethod(this, 'dispose');
    },
    get_OptionList: function() {
        return this._OptionList;
    },
    set_OptionList: function(value) {
        this._OptionList = value;
    },
    _onClick: function(e) {
        var panel = this.get_OptionList();
        if (panel) {
            if (panel.style.display == 'block' || panel.style.display == '') {
                panel.style.display = 'none';
                document.getElementById('ExpandOptionsButton').src = "Public/Images/expand_arrow.gif";
            }
            else {
                panel.style.display = 'block';
                document.getElementById('ExpandOptionsButton').src = "Public/Images/drop_arrow.gif";
            }
            e.preventDefault();
        }
    }
}

FirstLook.EdmundsTmv.Website.OptionListButton.registerClass('FirstLook.EdmundsTmv.Website.OptionListButton', Sys.UI.Behavior);

Sys.Application.notifyScriptLoaded();

