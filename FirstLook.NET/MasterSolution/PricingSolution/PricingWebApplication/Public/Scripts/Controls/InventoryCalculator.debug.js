/**
 * Descibe this class
 * @author Paul Monson
 * @author Simon Swenmouth
 * @version 2
 * @requires FirstLook.Utility
 * @requires Pricing
 */

if ( !! Sys) {
    Type.registerNamespace('FirstLook.Pricing.Website');

    /**
	 * Initialize Numeric values to 0
	 */
    FirstLook.Pricing.Website.InventoryCalculator = function(element) {
        FirstLook.Pricing.Website.InventoryCalculator.initializeBase(this, [element]);
        this._price = 0;
        this._currentPrice = 0;
        this._unitCost = 0;
        this._avgMarketPrice = 0;
        this._minMarketPrice = 0;
        this._maxMarketPrice = 0;
        this._minPctMarketAvg = 0;
        this._minPctMarketValue = 0;
        this._maxPctMarketAvg = 0;
        this._maxPctMarketValue = 0;
    };
    FirstLook.Pricing.Website.InventoryCalculator.prototype = {
        initialize: function() {
            FirstLook.Pricing.Website.InventoryCalculator.callBaseMethod(this, "initialize");
            this.create_slider();
        },
        /**
	     * Bind Proptery change, input change events, and subscribe to the PriceChange Conversation
	     */
        updated: function() {
            this.add_propertyChanged(this.getDelegate("onPropertyChange"));

            this.addHandler(this.get_priceTextBox(), "change", this.getDelegate("onPriceTextChange"));
            this.addHandler(this.get_percentageTextBox(), "change", this.getDelegate("onPercentageTextChange"));
            if (this.get_hasMileageAdjustedSearch()) {
                this.addHandler(document.getElementById("mpp_help"), "click", this.getDelegate("toggle_help_dialog"));
            }

            $conversation(FirstLook.Pricing.Website.PriceChangeSubscription).subscribe(this);

            this.showPriceHasChanged();

            FirstLook.Pricing.Website.InventoryCalculator.callBaseMethod(this, "updated");
        },
        dispose: function() {
            this.removeHandlers();
            this.clearDelegates();

            $conversation(FirstLook.Pricing.Website.PriceChangeSubscription).unsubscribe(this);

            FirstLook.Pricing.Website.InventoryCalculator.callBaseMethod(this, "dispose");
        },
        /**
         * Create Slider Graphic
         */
        create_slider: function() {
            var sliderContainer = this.get_sliderContainer(),
            calculator = this,
            r = new Raphael(sliderContainer, 510, 96),
            o = $(sliderContainer).offset(),
            b,
            min_mark,
            avg_mark,
            max_mark,
            min_market = this.get_minMarketPrice(),
            avg_market = this.get_avgMarketPrice(),
            max_market = this.get_maxMarketPrice(),
            range_market = max_market - min_market,
            m = range_market / 100,
            green_bars = {},
            width,
            path_resource = {
                skinny: {
                    title: "",
                    title_help: "",
                    base_path: "M0 37 L504 37 L504 57 L0 57 Z",
                    power_zone: "M0 37 L50 37 L50 57 L50 57 L0 57 Z",
                    update_start: function (path, point) {
                        point = Math.round(point); // Round to simplify parsing, PM
                        return path.replace(/(\w)(\d+)([\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?)(\w)(\d+)([\,|\s]\d+[\s|\,]?)/, "$1"+point+"$3$4"+point+"$6");
                    },
                    update_end: function (path,point) {
                        point = Math.round(point); // Round to simplify parsing, PM
                        return path.replace(/(\w\d+[\,|\s]\d+[\s|\,]?\w)(\d+)([\,|\s]\d+[\s|\,]?\w)(\d+)([\,|\s]\d+[\s|\,]?\w)(\d+)([\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?)/, "$1"+point+"$3"+(point)+"$5"+point+"$7");
                    },
                    height: 20,
                    offset: {
                        top: 37,
                        left: 27,
                        angle_offset: 27
                    },
                    should_shrink: false,
                    handle: "M6 36 L12 43 L12 60 L0 60 L0 43 Z",
                    handle_width: 20,
                    warning_text: 20
                },
                fat: {
                    title: "MARKET\n PRICING POWER",
                    title_help: "Market Pricing Power is the upper range this unit\n can be priced and still drive online consumer traffic.\n Click \"Whats this\?\" above for details.",
                    base_path: "M27 37 L504 37 L477 64 L0 64 Z",
                    power_zone: "M0 37 L50 37 L85 50 L50 64 L0 64 Z",
                    update_start: function (path,point) {
                        point = Math.round(point); // Round to simplify parsing, PM
                        return path.replace(/(\w)(\d+)([\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?)(\w)(\d+)([\,|\s]\d+[\s|\,]?)/, "$1"+point+"$3$4"+point+"$6");
                    },
                    update_end: function (path,point) {
                        point = Math.round(point); // Round to simplify parsing, PM
                        return path.replace(/(\w\d+[\,|\s]\d+[\s|\,]?\w)(\d+)([\,|\s]\d+[\s|\,]?\w)(\d+)([\,|\s]\d+[\s|\,]?\w)(\d+)([\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?)/, "$1"+point+"$3"+(point+30)+"$5"+point+"$7");
                    },
                    height: 27,
                    offset: {
                        top: 37,
                        left: 27,
                        angle_offset: 27
                    },
                    should_shrink: true,
                    handle: "M6 36 L12 43 L12 65 L0 65 L0 43 Z",
                    handle_width: 20,
                    warning_text: 54
                }
            },
            graphic_resource = path_resource[this.get_hasMileageAdjustedSearch() ? "fat" : "skinny"],
            hasSufficentListings = this.get_hasMarket(),
            title;


            b = make_graphic(r, o);
            make_maker_bars(40, b, r, "#f7ae09");


            if (hasSufficentListings) {
                title = r.text(0, 20, graphic_resource.title);
                title.attr({
                    fill: "#52bc62",
                    "font-family": "'Arial Black'",
                    "font-size": 12,
                    "font-style": "italic"
                });
                if (title.node.parentElement) {
                    title.node.parentElement.title = graphic_resource.title_help;
                    title.node.parentElement.style.cursor = "help";
                }

                set_area(make_power_zone(r), this.get_minPctMarketAvg(), this.get_maxPctMarketAvg(), avg_market, min_market, max_market, r, b);

                min_mark = make_named_marker(0.0, "LOW", min_market, b, r);
                avg_mark = make_named_marker(get_x(avg_market), "AVG.", avg_market, b, r);
                max_mark = make_named_marker(1.0, "HIGH", max_market, b, r);
            } else {
                title = r.text(250, graphic_resource.warning_text, "There is a limited number of cars in the search results.\n Review Market Listings below or increase Search Radius.");
                title.attr({
                    fill: "#886728",
                    "font-family": "'Arial Black'",
                    "font-size": 12
                });
            }


            function get_x(y) {
                // Get _pct
                // Slider roughly works like a slope intercept function y = xm + b, PM
                return ((y-min_market) / m)/100;
            }
            function get_y(x) {
                // Get _dollar
                // Slider roughly works like a slope intercept function y = xm + b, PM
                return (x * m)*100 + min_market;
            }
            function comma(value) {
                value = value.toString();
                var regex = /(\d+)(\d{3})/;
                while (regex.test(value)) {
                    value = value.replace(regex, '$1,$2');
                }
                return value;
            }
            function make_graphic(_r, _o) {
                var path = graphic_resource.base_path,
                shadow = _r.path(path),
                bar = _r.path(path),
                set = _r.set();

                set.push(shadow, bar);

                shadow.attr({
                    "stroke": "none",
                    "fill": "90-#d5d9e2-#adb0b5-#d5d9e2",
                    "translation": "2,3"
                });
                shadow.scale(1.005, 1.4);

                bar.attr({
                    "fill": "90-#fcb912-#fdeca6",
                    "stroke": "#e0c400"
                });

                return set;
            }
            function make_power_zone(_r) {
                var suggested_area = _r.path(graphic_resource.power_zone);
                suggested_area.attr({
                    "fill": "90-#36b449-#a2dcaa",
                    "stroke": "#a2dcaa",
                    "clip": graphic_resource.base_path
                });

                return suggested_area;
            }
            function make_named_marker(pct, label, amt, box, _r) { 
                var width = box.getBBox().width,
                x_angle_length = graphic_resource.offset.angle_offset,
                left_offset = graphic_resource.offset.left,
                x = Math.round((width - (x_angle_length * 2)) * pct) + left_offset,
                path = ["M", x, " ",(graphic_resource.offset.top-1)," L", x, " ", (graphic_resource.offset.top + graphic_resource.height + 4)].join(""),
                bar = _r.path(path).attr({
                    "stroke": "#a39568",
                    "stroke-width": 2
                }),
                t1 = _r.text(x, graphic_resource.offset.top + graphic_resource.height + 11, label).attr({
                    "font-size": 11,
                    "fill": "#737373"
                }),
                t2 = _r.text(x, graphic_resource.offset.top + graphic_resource.height + 22, "$" + comma(amt)).attr({
                    "font-size": 12,
                    "font-weight": "bold",
                    "fill": "#41455e"
                });
                s = _r.set();
                s.push(bar, t1, t2);

                return s;
            }
            function make_maker_bars(num, box, _r, color, min, max, shrink) {
                var x_angle_length = graphic_resource.offset.angle_offset,
                x_offset = graphic_resource.offset.left,
                width = box.getBBox().width - (2 * x_angle_length),
                set = _r.set(),
                values,
                i = 0,
                last = 0;

                min = min !== 0 ? min || - Infinity: 0;
                max = max !== 0 ? max || Infinity: 0;

                function get_marker_path(pct, y1, y2) {
                    if (pct === Infinity) pct = 100;
                    var x = pct * width + x_angle_length;
                    return {
                        x: x,
                        y1: y1,
                        y2: y2
                    };
                }

                function toLinePath(obj) {
                    return ["M", obj.x, " ", obj.y1, " L", obj.x, " ", obj.y2].join("");
                }

                for (i = 0; i <= num; i++) {
                    values = get_marker_path(i / num, graphic_resource.offset.top, graphic_resource.height + graphic_resource.offset.top);
                    if (values.x >= min && values.x <= max) {
                        last = i;
                        set.push(_r.path(toLinePath(values)).attr({
                            "stroke": color
                        }));
                    }
                }

                if (shrink) {
                    var slope = 0.5;
                    var off = 0;
                    while (true) {
                        values = get_marker_path(++last / num, graphic_resource.offset.top, graphic_resource.height + graphic_resource.offset.top);
                        off = slope * (values.x - max);
                        values.y1 = graphic_resource.offset.top + off;
                        values.y2 = graphic_resource.height + graphic_resource.offset.top - off;
                        if (values.y1 > values.y2) {
                            break;
                        }
                        set.push(_r.path(toLinePath(values)).attr({
                            "stroke": color
                        }));
                    } 
                }

                return set;
            }
            function set_title(title) {}
            function show_bubble() {}
            function hide_bubble() {}
            function format_comma(value) {}
            function set_area(suggested_area, start, end, avg, min, max, _r, b) {
                var old_path = suggested_area.attr("path")+"", // To String Hack
                new_path = old_path,
                x_angle_length = graphic_resource.offset.angle_offset,
                left_offset = graphic_resource.offset.left,
                width = b.getBBox().width - (2 * x_angle_length),
                path_start = width * (avg*(start * 0.01)-min)/(max-min) + left_offset,
                path_end = width * (avg*((end+1) * 0.01)-min)/(max-min) + left_offset; // MaxPrice is calculated at the highest point of the percetage, not the lowest so lets round up, PM

                if (path_start < x_angle_length) path_start = x_angle_length; // Min is the low point of the slider, 
                if (path_end > width) path_end = width; // Max is the high point of the slider,

                new_path = graphic_resource.update_start(new_path, path_start);
                new_path = graphic_resource.update_end(new_path, path_end);

                if (green_bars.remove) green_bars.remove();
                green_bars = make_maker_bars(40, b, _r, "#2c9b3c", path_start, path_end, graphic_resource.should_shrink);

                suggested_area.attr({
                    "path": new_path
                });

                title.attr({
                    x: path_start+((path_end-path_start)/2)
                });
            }
            function make_handle(min, max, b, _r) {
                var handle = _r.path(graphic_resource.handle),
                width = b.getBBox().width; 
                if (handle.paper.raphael.type == "VML") {
                    handle.node.UNSELECTABLE = "on"; // Fuck you IE please doen't load you're "accelerators", PM
                }

                handle.attr({
                    stroke: "#969daf",
                    "stroke-linecap": "square",
                    "stroke-linejoin": "miter",
                    fill: "90-#fff-#d1d1d1-#f7f7f5-#fff"
                });


                var drag = {
                    start: function(x, y) {
                        handle._is_sliding = true;
                        this.ox = x;
                        this.offset = this.getBBox();
                        this.attr({
                            "fill": "#fff",
                            "fill-opacity": 0.8
                        });
                        /*b_set.hide();
                        b_set.attr("opacity", 0);*/
                    },
                    end: function(x, y) {
                        handle._is_sliding = false;
                        this.attr({
                            "fill": "90-#fff-#d1d1d1-#f7f7f5-#fff",
                            "fill-opacity": 1
                        });
                        /// if > half the slider... flip me
                        /*b_set.show();
                        b_set.animate({
                            opacity: 1
                        },
                        1000); */
                    },
                    move: function(dx, dy) {
                        var d = this.ox + dx,
                        pos = this.attr("translation").x,
                        nx = d - pos - o.left - (graphic_resource.handle_width/2),
                        a = (pos-27+6)/(width-59),
                        can_move_left = nx > 0 || a >= -0.02,
                        can_move_right = nx < 0 || a <= 1.02;

                        if (can_move_left && can_move_right) { 
                            this.translate(nx, 0);
                            calculator.set_price(Math.round(get_y(a)/10)*10);
                        }
                    }
                };

                handle.drag(drag.move, drag.start, drag.end);

                return handle;
            }
            this.update_slider = function() {};
            if (hasSufficentListings) {
                var handle = make_handle(min_market, max_market, b, r);
                this.update_slider = (function(handle, min, max) {
                    var width = b.getBBox().width - graphic_resource.offset.angle_offset; 
                    var isVML = handle.paper.raphael.type == "VML";
                    return function(price) {
                        if (price < min) price = min;
                        var center = width * get_x(price),
                        current = handle.attr("translation").x,
                        isOutOfBoundsLower = center < 0,
                        isOutOfBoundsUpper = center > width - graphic_resource.offset.left;

                        if (isOutOfBoundsUpper) center = width - graphic_resource.offset.left;
                        if (isOutOfBoundsLower) center = 0;

                        if (isOutOfBoundsUpper || isOutOfBoundsLower) {
                            handle.attr({
                                fill: "#fff",
                                "fill-opacity": 0.5
                            });
                        }

                        if (!handle._is_sliding) {
                            if (isVML) {
                                handle.translate(center+graphic_resource.offset.left-graphic_resource.handle_width - current, 0);
                            } else {
                                handle.translate(center+graphic_resource.offset.left-graphic_resource.handle_width, 0);
                            }
                        }
                    }
                })(handle, min_market, max_market, b);

                this.update_slider(this.get_price());
            }
        },
        toggle_help_dialog: function() {
            $(this.get_helpDialog()).toggle();
        },
        /*
	     * Custom Setter ensures price is numeric
	     */
        set_price: function(value) {
            if (this._price !== value && !isNaN(value)) {
                this._price = value;
                this.raisePropertyChanged('price');
            }
        },
        /**
	     * Calculates Percentage from current price and market average
 		 */
        get_pct: function() {
            var pct = '';
            if (!isNaN(this.get_avgMarketPrice())) {
                pct = Math.round(this.get_price() / this.get_avgMarketPrice() * 100.0);
                if (pct == Infinity) {
                    pct = 'NA';
                }
            }
            return pct;
       },
        /**
	     * Updates background position for DHTML gauge via percetage.
	     */
        /**
	     * Raise Price Change Event
	     */
        notifyPriceChange: function() {
            $conversation(FirstLook.Pricing.Website.PriceChangeSubscription).notify(this.get_price());
        },
        // ==========
        // = Events =
        // ==========
        onPropertyChange: function(sender, args) {
            var property = args.get_propertyName(),
            pct;

            if (property == 'price') {
                pct = this.get_pct();
                this.get_percentageTextBox().value = pct.toString();
                this.get_priceTextBox().value = String.localeFormat("{0:c0}", Math.round(this.get_price()));
                this.fireEvent(this.get_priceTextBox(), 'change');
                this.get_potentialProfitLabel().value = String.localeFormat("{0:c0}", Math.round(this._price - this._unitCost));
                this.notifyPriceChange();
                this.update_slider(this.get_price());
            }

            this.showPriceHasChanged();
        },
        showPriceHasChanged: function() {
            if (this.get_currentPrice() !== this.get_price()) {
                var thing = $(this.get_saveButton()).parent();
                thing.addClass("has_changed_price");
            }
        },
        /**
	     * Parse User Input for numeric response, and set that as new Price
	     * @param {Event} ev Event Object
	     */
        onPriceTextChange: function(ev) {
            var newPrice = this.get_priceTextBox().value;
            newPrice = parseInt(newPrice.toString().replace(/[^0-9]/g, ""), 10);
            if (newPrice > 999999) {
                newPrice = 999999;
            }
            this.set_price(Math.round(newPrice));
        },
        /**
	     * Parse User Input for numeric response as a percentage
	     * and set the price based off percentage Market Average
	     * @param {Event} ev Event Object
	     */
        onPercentageTextChange: function(ev) {
            var newPercentage = this.get_percentageTextBox().value;
            newPercentage = newPercentage.toString().replace(/[^0-9]/g, "");
            this.set_price(Math.round((newPercentage / 100.0) * this.get_avgMarketPrice()));
        }
    };
    FirstLook.Utility.createGetterSetters(FirstLook.Pricing.Website.InventoryCalculator, ["minMarketPrice", // value
    "avgMarketPrice", // value
    "maxMarketPrice", // value
    "minPctMarketAvg", // value
    "minPctMarketValue", // value
    "maxPctMarketAvg", // value
    "maxPctMarketValue", // value
    "price", // value
    "currentPrice", // value 
    "unitCost", // value
    "priceTextBox", // input
    "percentageTextBox", // input
    "potentialProfitLabel", // input 
    "saveButton", // input 
    "sliderContainer",
    "helpDialog",
    "hasMarket", 
    "hasMileageAdjustedSearch"]);

    FirstLook.Pricing.Website.InventoryCalculator.registerClass('FirstLook.Pricing.Website.InventoryCalculator', Sys.UI.Behavior, FirstLook.Pricing.Website.IPriceControl);
}

Sys.Application.notifyScriptLoaded();
