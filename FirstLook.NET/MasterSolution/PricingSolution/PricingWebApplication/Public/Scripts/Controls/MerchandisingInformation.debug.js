/**
 * Adss the Copy from Notes Behavior and works to ensure both textboxes grow/shrink together.
 * @author Paul Monson
 * @version 2
 * @requires FirstLook.Utility
 */
if ( !! Sys) {
    Type.registerNamespace('FirstLook.Pricing.Website');
    FirstLook.Pricing.Website.MerchandisingTile = function(element) {
        FirstLook.Pricing.Website.MerchandisingTile.initializeBase(this, [element]);
    };
    FirstLook.Pricing.Website.MerchandisingTile.prototype = {
        initialize: function() {
            FirstLook.Pricing.Website.MerchandisingTile.callBaseMethod(this, 'initialize');
        },
        updated: function() {
            var notes = this.get_notes_text_editor(),
            notes_toolbar = this.get_notes_top_toolbar(),
            ads = this.get_ad_text_editor(),
            ads_toolbar = this.get_ad_top_toolbar(),
            bind_client_side_copy = this.get_client_side_copy(),
            distribute_status_view = this.get_distribute_status_view(),
            components = [notes, notes_toolbar, ads, ads_toolbar],
            i;

            if (!!notes_toolbar && !!ads) {
                if (bind_client_side_copy) {
                    this.addHandler(this.get_copy_button(), 'click', this.getDelegate("onCopyButtonClick"));
                }
            };

            FirstLook.Pricing.Website.MerchandisingTile.callBaseMethod(this, "updated");

            for (i = components.length - 1; i >= 0; i--) {
                if (!!components[i]) {
                    components[i].add_propertyChanged(this.getDelegate("onPropertyChanged"));
                }
            };

            if (!!notes && !!ads) {
                ads.raisePropertyChanged("targetHeight");
            };

            if (!!ads && ads.get_current_mode() == FirstLook.Pricing.WebControls.DocumentEditorMode.Edit) {
                ads.focus();
            };

            if (!!!this.get_min_height()) {
                this.set_min_height(90);
            };

            if (!!distribute_status_view) {
				this.addHandler($get(distribute_status_view), 'click', this.getDelegate("onDistributionClick"));
            }
        },
        dispose: function() {
            this.removeHandlers();
            this.clearDelegates();

            FirstLook.Pricing.Website.MerchandisingTile.callBaseMethod(this, "dispose");
        },
        get_copy_button: function() {
            if (!!!this._copy_button) {
                var notes_toolbar = this.get_notes_top_toolbar().get_element(),
                buttons = notes_toolbar.getElementsByTagName("input");
                for (var i = 0, l = buttons.length; i < l; i++) {
                    if (buttons[i].className.indexOf("copy_ad_to_notes") != -1) {
                        this._copy_button = buttons[i];
                    };
                };

                Sys.Debug.assert(!!this._copy_button, "Notes area must contain input with class: copy_ad_to_notes");

                this._copy_button.disabled = false;
                this._copy_button.className = this._copy_button.className.replace("disabled", "");
                this._copy_button.parentNode.className = this._copy_button.parentNode.className.replace("disabled", "");
            };

            return this._copy_button;
        },
        onPropertyChanged: function(sender, args) {
            var property = args.get_propertyName();

            if (property == "targetHeight") {
                var sent_height = sender.get_targetHeight();
                this.trigger_resize(sent_height);
            } else
                if (property == "is_visible") {
                if (sender == this.get_notes_top_toolbar()) {
                    this.trigger_resize(this.get_notes_text_editor().get_targetHeight());
                } else
                    if (sender == this.get_ad_top_toolbar()) {
                    this.trigger_resize(this.get_ad_text_editor().get_targetHeight());
                }
            }
        },
        trigger_resize: function(sent_height) {
            var notes = this.get_notes_text_editor(),
            notes_toolbar = this.get_notes_top_toolbar(),
            ads = this.get_ad_text_editor(),
            ads_toolbar = this.get_ad_top_toolbar(),
            notesHeight = 0,
            adHeight = 0,
            notesToolbarHeight = 0,
            adToolbarHeight = 0,
            newHeight,
            ads_has_focus_or_mouse_over,
            notes_has_focus_or_mouse_over;

            try {
                if (!!notes) {
                    notesHeight = notes.get_targetHeight() - 1;
                }
                if (!!ads) {
                    adHeight = ads.get_targetHeight() - 1;
                }
                if (!!ads_toolbar) {
                    adToolbarHeight += ads_toolbar.get_element().offsetHeight;
                }
                if (!!notes_toolbar) {
                    notesToolbarHeight += notes_toolbar.get_element().offsetHeight;
                }

                newHeight = (adHeight + adToolbarHeight >= notesHeight + notesToolbarHeight) ? adHeight : notesHeight;
                ads_has_focus_or_mouse_over = !!ads.get_has_focus() || !!ads.get_has_mouse_over();
                notes_has_focus_or_mouse_over = !!notes.get_has_focus() || !!notes.get_has_mouse_over();

                if (!(ads_has_focus_or_mouse_over || notes_has_focus_or_mouse_over)) {
                    // ===========================================================================
                    // = This should always be the closing action, as neither textarea has focus =
                    // ===========================================================================
                    newHeight = this.get_min_height();
                }
            } catch (err) {
                // =========================================================================
                // = Depending on the update model, textarea may be updating at this point
                // = throwing a error may cause, error messages for the confimation screen, PM
                // =========================================================================
            }

            // ==============================================================================
            // = These values may become stale durning a Ajax Update null check just before,
            // = it is also important to note that we must convert each value to a bool, or
            // = NaN's will get by, PM
            // ==============================================================================
            if (!(!!newHeight && !!notesHeight && !!adHeight))
                return;

            notes.set_targetHeight(newHeight + adToolbarHeight);
            ads.set_targetHeight(newHeight + notesToolbarHeight);
        },
        onCopyButtonClick: function(e) {
            var ads = this.get_ad_text_editor(),
            text;

            var el = ads.get_element();
            var textareas = el.getElementsByTagName('textarea');
            if (textareas[0]) {
                textarea = textareas[0];

                text = "***\nInternet Ad\n***\n";

                text += textarea.value;

                if (textareas[1] && textareas[1].value) {
                    text += "\n---\n" + textareas[1].value.trim();
                    // trim is non-standard and from ajax.net
                };

                this.get_notes_top_toolbar().set_body_text_for_last_click(text);

                e.preventDefault();
                e.stopPropagation();
            };
        },
        onDistributionClick: function(e) {
            var message_box = $find(this.get_distribute_message_box());
            message_box.set_MessageText(e.target.title);
            message_box.show();
            e.preventDefault();
            e.stopPropagation();
        }
    };
    FirstLook.Utility.createGetterSetters(FirstLook.Pricing.Website.MerchandisingTile, [
        // ========================
        // = TextEditor Component =
        // ========================
        "ad_text_editor",
        "ad_top_toolbar",
        // ========================
        // = TextEditor Component =
        // ========================
        "notes_text_editor",
        "notes_top_toolbar",
        "client_side_copy",
        "is_resizing",
        "new_height",
        "min_height",
        // ==========================
        // = Distribution Component =
        // ==========================
        "distribute_status_view",
        "distribute_message_box"
        ]);

    FirstLook.Pricing.Website.MerchandisingTile.registerClass('FirstLook.Pricing.Website.MerchandisingTile', Sys.UI.Behavior);

    Sys.Application.notifyScriptLoaded();
};