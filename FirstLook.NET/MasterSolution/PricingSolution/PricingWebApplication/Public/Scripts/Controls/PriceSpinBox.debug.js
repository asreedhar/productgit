if ( !! Sys) {

    Type.registerNamespace('FirstLook.Pricing.Website');

    FirstLook.Pricing.Website.PriceListItem = function(price, mileage) {
        this._price = price;
        this._mileage = mileage;
    };
    FirstLook.Pricing.Website.PriceListItem.prototype = {
        compareTo: function(other) {
            if (other) {
                var c = this._compareTo(this.get_price(), other.get_price());
                if (c == 0)
                c = this._compareTo(this.get_mileage(), other.get_mileage());
                return c;
            }
            return 1;
        },
        _compareTo: function(a, b) {
            if (a > b) {
                return 1;
            } else if (a < b) {
                return - 1;
            } else {
                return 0;
            }
        }
    };
    FirstLook.Utility.createGetterSetters(FirstLook.Pricing.Website.PriceListItem, ["price", "mileage"]);

    FirstLook.Pricing.Website.PriceListItem.registerClass('FirstLook.Pricing.Website.PriceListItem');

    /* PriceList */

    FirstLook.Pricing.Website.PriceList = function() {
        this._offset = 10;
        this._items = [];
    };

    FirstLook.Pricing.Website.PriceList.prototype = {
        add: function(item) {
            if (FirstLook.Pricing.Website.PriceListItem.isInstanceOfType(item)) {
                // maintain sort order on insert
                var idx = this.binary_search(item);
                if (idx < 0)
                this._items.splice(~idx, 0, item);
                else
                this._items.splice(idx, 0, item);
            }
            else {
                throw Error.argumentType(
                'item',
                Object.getTypeName(item),
                'FirstLook.Pricing.Website.PriceListItem',
                'PriceList only accepts PriceListItem objects.');
            }
        },
        binary_search: function(item) {
            var low = 0;
            var high = this._items.length - 1;

            while (low <= high) {
                var mid = (low + high) >> 1;
                var midVal = this._items[mid];
                var cmp = midVal.compareTo(item);

                if (cmp < 0)
                low = mid + 1;
                else if (cmp > 0)
                high = mid - 1;
                else
                return mid;
                // key found
            }
            return~low;
            // key not found
        },
        _price: function(x, y) {
            var delta = this._items[y].get_price() - this._items[x].get_price();
            if (delta > this._offset) {
                return this._items[y].get_price() - this._offset;
            }
            else if (delta < 2) {
                return this._items[x].get_price();
            }
            else {
                return this._items[x].get_price() + (delta / 2);
            }
        },
        _next_price: function(idx) {
            if (idx == Number.MIN_VALUE) {
                return Number.MIN_VALUE;
            }
            for (var i = idx + 1, l = this._items.length; i >= 0 && i < l; i++) {
                if (this._items[i].get_price() > this._items[idx].get_price()) {
                    return i;
                }
            }
            return Number.MIN_VALUE;
        },
        next_price: function(price, mileage) {
            // stop changing price when already most expensive
            if (this._items[this._items.length - 1].get_price() < price) {
                return price;
            }
            // variables for next two prices
            var x,
            y;
            // find current location
            var i = this.binary_search(new FirstLook.Pricing.Website.PriceListItem(price, mileage));
            if (i < 0) {
                x = ~i;
            }
            else {
                x = this._next_price(i);
            }
            y = this._next_price(x);
            // determine price
            if (x < y) {
                var newPrice = this._price(x, y);
                if (newPrice == this._items[x].get_price()) {
                    newPrice = newPrice + 1;
                }
                return newPrice;
            }
            else {
                return this._items[this._items.length - 1].get_price() + this._offset;
            }
        },
        _last_price: function(idx) {
            if (idx == Number.MAX_VALUE) {
                return Number.MAX_VALUE;
            }
            for (var i = idx - 1, l = this._items.length; i >= 0 && i < l; i--) {
                if (this._items[i].get_price() < this._items[idx].get_price()) {
                    return i;
                }
            }
            return Number.MAX_VALUE;
        },
        last_price: function(price, mileage) {
            // variables for previous two prices
            var x,
            y;
            // find current location
            var i = this.binary_search(new FirstLook.Pricing.Website.PriceListItem(price, mileage));
            if (i < 0) {
                y = ~i;
                if (y >= this._items.length) {
                    y = this._items.length - 1;
                }
                if (this._items[y].get_price() > price) {
                    y = this._last_price(y);
                }
            }
            else {
                y = this._last_price(i);
            }
            x = this._last_price(y);
            // determine price
            if (x < y) {
                var newPrice = this._price(x, y);
                if (newPrice == this._items[y].get_price()) {
                    newPrice = newPrice - 1;
                }
                return newPrice;
            }
            else {
                return this._items[0].get_price() - this._offset;
            }
        },
        rank: function(price, mileage) {
            var i = this.binary_search(new FirstLook.Pricing.Website.PriceListItem(price, mileage));
            if (i < 0)
            i = ~i;
            return i + 1;
        },
        maximum_rank: function() {
            return this._items.length + 1;
        }
    };

    FirstLook.Pricing.Website.PriceList.registerClass('FirstLook.Pricing.Website.PriceList');

    /* PriceSpinBox */

    FirstLook.Pricing.Website.PriceSpinBox = function(element) {
        FirstLook.Pricing.Website.PriceSpinBox.initializeBase(this, [element]);
        this._enabled = true;
        this._rank = 0;
        this._price = 0;
        this._mileage = 0;
        this._values = [];
        this._priceList = new FirstLook.Pricing.Website.PriceList();
    };

    FirstLook.Pricing.Website.PriceSpinBox.prototype = {
        initialize: function() {
            FirstLook.Pricing.Website.PriceSpinBox.callBaseMethod(this, 'initialize');
        },
        updated: function() {
            this.add_propertyChanged(this.getDelegate("onPropertyChange"));
            for (var i = 0; i < this._values.length; i++) {
                this._priceList.add(new FirstLook.Pricing.Website.PriceListItem(this._values[i][0], this._values[i][1]));
            }
            if (this.get_enabled()) {
                var increaseButton = this.get_increaseRankButton();
                var decreaseButton = this.get_decreaseRankButton();

                this.addHandler(increaseButton, 'click', this.getDelegate("onIncreaseClick"));
                this.addHandler(decreaseButton, 'click', this.getDelegate("onDecreaseClick"));
                increaseButton.disabled = false;
                decreaseButton.disabled = false;
            }
            $conversation(FirstLook.Pricing.Website.PriceChangeSubscription).subscribe(this);
            FirstLook.Pricing.Website.PriceSpinBox.callBaseMethod(this, 'updated');
        },
        dispose: function() {
            this.remove_propertyChanged(this.getDelegate("onPropertyChange"));
            this.removeHandlers();
            this.clearDelegates();
            $conversation(FirstLook.Pricing.Website.PriceChangeSubscription).unsubscribe(this);
            FirstLook.Pricing.Website.PriceSpinBox.callBaseMethod(this, 'dispose');
        },
        onPropertyChange: function(sender, e) {
            if (e.get_propertyName() == 'price') {
                // update the price rank label
                this.set_rank(this._priceList.rank(this.get_price(), this.get_mileage()));
            }
            else if (e.get_propertyName() == 'rank') {
                var formatter = FirstLook.Pricing.Website.StringFormatter.getInstance().formatOrdinal;
                this.get_priceRankLabel().innerHTML = String.localeFormat(
                "{0} of {1:D0}",
                formatter(this.get_rank()),
                this._priceList.maximum_rank());
                // tell the world about the price rank change
                $conversation(FirstLook.Pricing.Website.PriceRankChangeSubscription).notify(this.get_rank());
            }
        },
        onIncreaseClick: function(e) {
            this.set_price(this._priceList.next_price(this.get_price(), this.get_mileage()));
            // tell the world about the price change
            $conversation(FirstLook.Pricing.Website.PriceChangeSubscription).notify(this.get_price());
            e.preventDefault();
            return e.returnValue = false;
        },
        onDecreaseClick: function(e) {
            this.set_price(this._priceList.last_price(this.get_price(), this.get_mileage()));
            // tell the world about the price change
            $conversation(FirstLook.Pricing.Website.PriceChangeSubscription).notify(this.get_price());
            e.preventDefault();
            return e.returnValue = false;
        }
    };
    FirstLook.Utility.createGetterSetters(FirstLook.Pricing.Website.PriceSpinBox, [
    "priceRankLabel", "increaseRankButton", "decreaseRankButton", "enabled", "price", "rank", "mileage", "values"]);

    FirstLook.Pricing.Website.PriceSpinBox.registerClass('FirstLook.Pricing.Website.PriceSpinBox', Sys.UI.Behavior, FirstLook.Pricing.Website.IPriceControl);

    Sys.Application.notifyScriptLoaded();

};

