Type.registerNamespace('FirstLook.Pricing.Website');

/* IPriceControl */

FirstLook.Pricing.Website.IPriceControl = function FirstLook$Pricing$Website$IPriceControl() {
    throw Error.notImplemented();
};

function FirstLook$Pricing$Website$IPriceControl$get_price() {
    throw Error.notImplemented();
}

function FirstLook$Pricing$Website$IPriceControl$set_price(value) {
    throw Error.notImplemented();
}

FirstLook.Pricing.Website.IPriceControl.prototype =
{
    get_price: FirstLook$Pricing$Website$IPriceControl$get_price,
    set_price: FirstLook$Pricing$Website$IPriceControl$set_price
};

FirstLook.Pricing.Website.IPriceControl.registerInterface('FirstLook.Pricing.Website.IPriceControl');

/* IPriceRankControl */

FirstLook.Pricing.Website.IPriceRankControl = function FirstLook$Pricing$Website$IPriceRankControl() {
    throw Error.notImplemented();
};

function FirstLook$Pricing$Website$IPriceRankControl$get_rank() {
    throw Error.notImplemented();
}

function FirstLook$Pricing$Website$IPriceRankControl$set_rank(value) {
    throw Error.notImplemented();
}

FirstLook.Pricing.Website.IPriceRankControl.prototype =
{
    get_rank: FirstLook$Pricing$Website$IPriceRankControl$get_rank,
    set_rank: FirstLook$Pricing$Website$IPriceRankControl$set_rank
};

FirstLook.Pricing.Website.IPriceRankControl.registerInterface('FirstLook.Pricing.Website.IPriceRankControl');

/* IUnitCostControl */

FirstLook.Pricing.Website.IUnitCostControl = function FirstLook$Pricing$Website$IUnitCostControl() {
    throw Error.notImplemented();
};

function FirstLook$Pricing$Website$IUnitCostControl$get_unitCost() {
    throw Error.notImplemented();
}

function FirstLook$Pricing$Website$IUnitCostControl$set_unitCost(value) {
    throw Error.notImplemented();
}

FirstLook.Pricing.Website.IUnitCostControl.prototype =
{
    get_unitCost: FirstLook$Pricing$Website$IUnitCostControl$get_unitCost,
    set_unitCost: FirstLook$Pricing$Website$IUnitCostControl$set_unitCost
};

FirstLook.Pricing.Website.IUnitCostControl.registerInterface('FirstLook.Pricing.Website.IUnitCostControl');

/* IValuationChangeSubscriber */

FirstLook.Pricing.Website.IValuationChangeSubscriber = function FirstLook$Pricing$Website$IValuationChangeSubscriber() {
    throw Error.notImplemented();
};

function FirstLook$Pricing$Website$IValuationChangeSubscriber$valuationChanged(appraisalValue, estimatedReconditioningCost) {
    throw Error.notImplemented();
}

FirstLook.Pricing.Website.IValuationChangeSubscriber.prototype =
{
    valuationChanged: FirstLook$Pricing$Website$IValuationChangeSubscriber$valuationChanged
};

FirstLook.Pricing.Website.IValuationChangeSubscriber.registerInterface('FirstLook.Pricing.Website.IValuationChangeSubscriber');

/* ISubscription */

FirstLook.Pricing.Website.ISubscription = function FirstLook$Pricing$Website$ISubscription() {
    throw Error.notImplemented();
};

function FirstLook$Pricing$Website$ISubscription$accept(subscriber) {
    throw Error.notImplemented();
}

function FirstLook$Pricing$Website$ISubscription$notify(subscriber, command) {
    throw Error.notImplemented();
}

FirstLook.Pricing.Website.ISubscription.prototype =
{
    accept: FirstLook$Pricing$Website$ISubscription$accept,
    notify: FirstLook$Pricing$Website$ISubscription$notify
};

FirstLook.Pricing.Website.ISubscription.registerInterface('FirstLook.Pricing.Website.ISubscription');

/* PriceChangeSubscription */

FirstLook.Pricing.Website.PriceChangeSubscription = function FirstLook$Pricing$Website$PriceChangeSubscription()
{
    FirstLook.Pricing.Website.PriceChangeSubscription.initializeBase(this);
};

function FirstLook$Pricing$Website$PriceChangeSubscription$accept(subscriber) {
    if (subscriber) {
        var subscriberType = Object.getType(subscriber);
        if (subscriberType.implementsInterface(FirstLook.Pricing.Website.IPriceControl)) {
            return true;
        }
    }
    return false;
}

function FirstLook$Pricing$Website$PriceChangeSubscription$notify(subscriber, command) {
    subscriber.set_price(Math.round(command));
}

FirstLook.Pricing.Website.PriceChangeSubscription.prototype =
{
    accept: FirstLook$Pricing$Website$PriceChangeSubscription$accept,
    notify: FirstLook$Pricing$Website$PriceChangeSubscription$notify
};

FirstLook.Pricing.Website.PriceChangeSubscription.registerClass('FirstLook.Pricing.Website.PriceChangeSubscription', Sys.Component, FirstLook.Pricing.Website.ISubscription);

/* PriceRankChangeSubscription */

FirstLook.Pricing.Website.PriceRankChangeSubscription = function FirstLook$Pricing$Website$PriceRankChangeSubscription()
{
    FirstLook.Pricing.Website.PriceRankChangeSubscription.initializeBase(this);
};

function FirstLook$Pricing$Website$PriceRankChangeSubscription$accept(subscriber) {
    if (subscriber) {
        var subscriberType = Object.getType(subscriber);
        if (subscriberType.implementsInterface(FirstLook.Pricing.Website.IPriceRankControl)) {
            return true;
        }
    }
    return false;
}

function FirstLook$Pricing$Website$PriceRankChangeSubscription$notify(subscriber, command) {
    subscriber.set_rank(Math.round(command));
}

FirstLook.Pricing.Website.PriceRankChangeSubscription.prototype =
{
    accept: FirstLook$Pricing$Website$PriceRankChangeSubscription$accept,
    notify: FirstLook$Pricing$Website$PriceRankChangeSubscription$notify
};

FirstLook.Pricing.Website.PriceRankChangeSubscription.registerClass('FirstLook.Pricing.Website.PriceRankChangeSubscription', Sys.Component, FirstLook.Pricing.Website.ISubscription);

/* InventoryListingVisibilitySubscription */

FirstLook.Pricing.Website.InventoryListingVisibilitySubscription = function FirstLook$Pricing$Website$InventoryListingVisibilitySubscription()
{
    FirstLook.Pricing.Website.InventoryListingVisibilitySubscription.initializeBase(this);
};

function FirstLook$Pricing$Website$InventoryListingVisibilitySubscription$accept(subscriber) {
    if (subscriber) {
        if (subscriber.toggle) {
            return true;
        }
    }
    return false;
}

function FirstLook$Pricing$Website$InventoryListingVisibilitySubscription$notify(subscriber, command) {
    subscriber.toggle(command.sender, command.args);
}

FirstLook.Pricing.Website.InventoryListingVisibilitySubscription.prototype =
{
    accept: FirstLook$Pricing$Website$InventoryListingVisibilitySubscription$accept,
    notify: FirstLook$Pricing$Website$InventoryListingVisibilitySubscription$notify
};

FirstLook.Pricing.Website.InventoryListingVisibilitySubscription.registerClass('FirstLook.Pricing.Website.InventoryListingVisibilitySubscription', Sys.Component, FirstLook.Pricing.Website.ISubscription);

/* UnitCostChangeSubscription */

FirstLook.Pricing.Website.UnitCostChangeSubscription = function FirstLook$Pricing$Website$UnitCostChangeSubscription()
{
    FirstLook.Pricing.Website.UnitCostChangeSubscription.initializeBase(this);
};

function FirstLook$Pricing$Website$UnitCostChangeSubscription$accept(subscriber) {
    if (subscriber) {
        var subscriberType = Object.getType(subscriber);
        if (subscriberType.implementsInterface(FirstLook.Pricing.Website.IUnitCostControl)) {
            return true;
        }
    }
    return false;
}

function FirstLook$Pricing$Website$UnitCostChangeSubscription$notify(subscriber, command) {
    subscriber.set_unitCost(Math.round(command));
}

FirstLook.Pricing.Website.UnitCostChangeSubscription.prototype =
{
    accept: FirstLook$Pricing$Website$UnitCostChangeSubscription$accept,
    notify: FirstLook$Pricing$Website$UnitCostChangeSubscription$notify
};

FirstLook.Pricing.Website.UnitCostChangeSubscription.registerClass('FirstLook.Pricing.Website.UnitCostChangeSubscription', Sys.Component, FirstLook.Pricing.Website.ISubscription);

/* ValuationChangeSubscription */

FirstLook.Pricing.Website.ValuationChangeSubscription = function FirstLook$Pricing$Website$ValuationChangeSubscription()
{
    FirstLook.Pricing.Website.ValuationChangeSubscription.initializeBase(this);
};

function FirstLook$Pricing$Website$ValuationChangeSubscription$accept(subscriber) {
    if (subscriber) {
        var subscriberType = Object.getType(subscriber);
        if (subscriberType.implementsInterface(FirstLook.Pricing.Website.IValuationChangeSubscriber)) {
            return true;
        }
    }
    return false;
}

function FirstLook$Pricing$Website$ValuationChangeSubscription$notify(subscriber, command) {
    subscriber.valuationChanged(command.appraisalValue, command.estimatedReconditioningCost);
}

FirstLook.Pricing.Website.ValuationChangeSubscription.prototype =
{
    accept: FirstLook$Pricing$Website$ValuationChangeSubscription$accept,
    notify: FirstLook$Pricing$Website$ValuationChangeSubscription$notify
};

FirstLook.Pricing.Website.ValuationChangeSubscription.registerClass('FirstLook.Pricing.Website.ValuationChangeSubscription', Sys.Component, FirstLook.Pricing.Website.ISubscription);

/* Conversation */

FirstLook.Pricing.Website.Conversation = function FirstLook$Pricing$Website$Conversation()
{
    FirstLook.Pricing.Website.Conversation.initializeBase(this);
    this._subscription = null;
    this._subscribers = new Array();
};

function FirstLook$Pricing$Website$Conversation$updated() {
    if (!this._subscription) {
        throw Error.argumentNull('contract', 'Contract cannot be null');
    }
    FirstLook.Pricing.Website.Conversation.callBaseMethod(this, 'updated');
}

function FirstLook$Pricing$Website$Conversation$dispose() {
    this._subscription = null;
    Array.clear(this._subscribers);
    FirstLook.Pricing.Website.Conversation.callBaseMethod(this, 'dispose');
}

function FirstLook$Pricing$Website$Conversation$get_subscription() {
    return this._subscription;
}

function FirstLook$Pricing$Website$Conversation$set_subscription(value) {
    if (value) {
        var subscriptionType = Object.getType(value);
        if (subscriptionType.implementsInterface(FirstLook.Pricing.Website.ISubscription)) {
            this._subscription = value;
        }
        else {
            throw Error.argumentType('subscription', subscriptionType, FirstLook.Pricing.Website.ISubscription, null);
        }
    }
}

function FirstLook$Pricing$Website$Conversation$notify(command) {
    for (var i = 0, l = this._subscribers.length; i < l; i++) {
        this._subscription.notify(this._subscribers[i], command);
    }
}

function FirstLook$Pricing$Website$Conversation$subscribe(subscriber) {
    if (this._subscription.accept(subscriber)) {
        this._subscribers.push(subscriber);
        return true;
    }
    return false;
}

function FirstLook$Pricing$Website$Conversation$unsubscribe(subscriber) {
    for (var i = 0, l = this._subscribers.length; i < l; i++) {
        if (this._subscribers[i] === subscriber) {
            Array.removeAt(this._subscribers, i);
            return true;
        }
    }
    return false;
}

FirstLook.Pricing.Website.Conversation.prototype =
{
    updated: FirstLook$Pricing$Website$Conversation$updated,
    dispose: FirstLook$Pricing$Website$Conversation$dispose,
    get_subscription: FirstLook$Pricing$Website$Conversation$get_subscription,
    set_subscription: FirstLook$Pricing$Website$Conversation$set_subscription,
    notify: FirstLook$Pricing$Website$Conversation$notify,
    subscribe: FirstLook$Pricing$Website$Conversation$subscribe,
    unsubscribe: FirstLook$Pricing$Website$Conversation$unsubscribe
};

FirstLook.Pricing.Website.Conversation._conversations = new Object();

var $conversation = FirstLook.Pricing.Website.Conversation.getConversation = function FirstLook$Pricing$Website$Conversation$getConversation(subscriptionType) {
    var e = Function._validateParams(arguments, [
        {name: "subscriptionType", type: Function}
    ]);
    if (e) throw e;
    var subscriptionName = subscriptionType.getName();
    var subscription = $find(subscriptionName);
    if (!subscription) {
        subscription = $create(subscriptionType, {id: subscriptionName});
    }
    if (!FirstLook.Pricing.Website.Conversation._conversations.hasOwnProperty(subscriptionName)) {
        FirstLook.Pricing.Website.Conversation._conversations[subscriptionName] = $create(FirstLook.Pricing.Website.Conversation, {subscription: subscription});
    }
    return FirstLook.Pricing.Website.Conversation._conversations[subscriptionName];
};

FirstLook.Pricing.Website.Conversation.registerClass('FirstLook.Pricing.Website.Conversation', Sys.Component);

/* QueryString */

FirstLook.Pricing.Website.QueryString = function FirstLook$Pricing$Website$QueryString(qs)
{
    this._params = new Object();
    this.initialize(qs);
};

FirstLook.Pricing.Website.QueryString.prototype = 
{
    initialize: function(qs) {
        if (qs == null)
		    qs = window.location.search.substring(1,window.location.search.length);
        if (qs.length == 0) 
		    return;
        qs = qs.replace(/\+/g, ' ');
        var pairs = qs.split('&');
        for (var i = 0; i < pairs.length; i++) {
		    var pair = pairs[i].split('=');
		    var name = unescape(pair[0]);
		    var value = (pair.length==2) ? unescape(pair[1]) : name;
		    var values = this._params.hasOwnProperty(name) ? this._params[name] : (new Array());
		    values.push(value);
		    this._params[name] = values;
	    }
    },
    get_parameter: function(name) {
        if (this._params.hasOwnProperty(name)) {
            var values = this._params[name];
            return values[0];
        }
        return null;
    },
    set_parameter: function(name,value) {
        var values = new Array();
        values.push(value);
        this._params[name] = values;
    },
    get_parameterValues: function(key) {
        if (this._params.hasOwnProperty(name)) {
            return this._params[name];
        }
        return null;
    },
    get_parameterMap: function() {
        return this._params;
    },
    toString: function() {
        var str = '';
        for (var property in this._params) {
            var pair = escape(property) + '=' + escape(this._params[property]);
            if (str.length > 0) {
                str += '&';
            }
            str += pair;
        }
        return str;
    }
};

FirstLook.Pricing.Website.QueryString.registerClass('FirstLook.Pricing.Website.QueryString');

/* StringFormatter */

FirstLook.Pricing.Website.StringFormatter = function FirstLook$Pricing$Website$StringFormatter() {};

FirstLook.Pricing.Website.StringFormatter.prototype = 
{
    formatOrdinal: function(value) {
        var len = value.toString().length;
        var suffix = "th";
        if (len == 1) {
            switch (value) {
                case 1: suffix = "st"; break;
                case 2: suffix = "nd"; break;
                case 3: suffix = "rd"; break;
            }
        }
        else if (len > 1) {
            var lastDigit = parseInt(value.toString().charAt(value.toString().length - 1),10);
            var lastTwoDigits = parseInt(value.toString().substring(value.toString().length - 2, value.toString().length),10);
            switch (lastDigit) {
                case 1: suffix = "st"; break;
                case 2: suffix = "nd"; break;
                case 3: suffix = "rd"; break;
            }
            switch (lastTwoDigits) {
                case 11: suffix = "th"; break;
                case 12: suffix = "th"; break;
                case 13: suffix = "th"; break;
            }
        }
        return value.toString() + suffix;
    }
};

FirstLook.Pricing.Website.StringFormatter.getInstance = function FirstLook$Pricing$Website$StringFormatter$getInstance() {
    if (arguments.length !== 0) {
        throw Error.parameterCount();
    }
    if (!FirstLook.Pricing.Website.StringFormatter._instance) {
        FirstLook.Pricing.Website.StringFormatter._instance = new FirstLook.Pricing.Website.StringFormatter();
    }
    return FirstLook.Pricing.Website.StringFormatter._instance;
};

FirstLook.Pricing.Website.StringFormatter.registerClass('FirstLook.Pricing.Website.StringFormatter');

/* End of Script */

Sys.Application.notifyScriptLoaded();
