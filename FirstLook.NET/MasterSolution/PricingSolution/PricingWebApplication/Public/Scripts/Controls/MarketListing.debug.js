/**
 * Descibe this class
 * @author Paul Monson
 * @author Simon Swenmouth
 * @version 2
 * @requires FirstLook.Utility
 */

/*
	TODO this seems to initialize one MarketListing object per row in the Market Listings Table this should be rewritten to have one control
	for the whole table, PM
*/
if ( !! Sys) {
Type.registerNamespace('FirstLook.Pricing.Website');
    FirstLook.Pricing.Website.MarketListing = function(element) {
    FirstLook.Pricing.Website.MarketListing.initializeBase(this,[element]);
    this._rowColor = "";
};
    FirstLook.Pricing.Website.MarketListing.prototype = {
        initialize: function() {
            this._rowColor = "";
            FirstLook.Pricing.Website.MarketListing.callBaseMethod(this, "initialize");
        },
        updated: function() {
            var el = this.get_element();

            this.addHandler(el, "mouseover", this.getDelegate("onMouseOver"));
            this.addHandler(el, "mouseout", this.getDelegate("onMouseOut"));
            this.addHandler(el, "click", this.getDelegate("onClick"));
            this.addHandler(window, "resize", this.getDelegate("onResize"));
    
            FirstLook.Pricing.Website.MarketListing.callBaseMethod(this, "updated");
    },
    dispose: function() {
            this.removeHandlers();
            this.clearDelegates();

            FirstLook.Pricing.Website.MarketListing.callBaseMethod(this, "dispose");
    },
        // ====================
        // = Class Properties =
        // ====================
        rowHold: false,
        // ==========
        // = Events =
        // ==========
        /**
         * Describe what this method does
         * @param {Event} ev Event Object
         */        
        onMouseOver: function(ev) {
        if(this.rowHold == false) {
            var rowElement = this._panel.parentNode.parentNode.parentNode;
            var rowStyle = rowElement.style;
            this._panel.style.display = 'block';
            this._rowColor = rowStyle.backgroundColor;
            if(rowElement.className.indexOf("analyzed") > -1) {
                rowStyle.backgroundColor = "#e3e6ff";
            } else if(rowElement.className.indexOf("odd") > -1) {
                rowStyle.backgroundColor = "#f8f8dd";
            } else {
                rowStyle.backgroundColor = "#ffffee";
            }
            rowStyle.height = (rowElement.scrollHeight + this._panel.scrollHeight) + "px";
        }

    },
        onMouseOut: function(ev) {
        if(this.rowHold == false) {
            var rowStyle = this._panel.parentNode.parentNode.parentNode.style;
            rowStyle.backgroundColor = this._rowColor;
            rowStyle.height = "";
            this._panel.style.display = 'none';
        }
    },
        onClick: function(ev) {
        this.rowHold = !(this.rowHold);
        Sys.UI.DomElement.toggleCssClass(this.get_element(), "open");    
    },
        onResize: function(ev) {
        	var el = this.get_element();
        	el.style.zoom = 1;
        	el.style.zoom = "";
    }
};
    FirstLook.Utility.createGetterSetters(FirstLook.Pricing.Website.MarketListing, ["panel"]);

FirstLook.Pricing.Website.MarketListing.registerClass('FirstLook.Pricing.Website.MarketListing', Sys.UI.Behavior);

Sys.Application.notifyScriptLoaded();
};
