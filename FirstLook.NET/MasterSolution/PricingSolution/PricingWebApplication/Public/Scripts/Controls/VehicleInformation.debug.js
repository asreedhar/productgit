Type.registerNamespace('FirstLook.Pricing.Website');

FirstLook.Pricing.Website.VehicleInformation = function(element) {
    FirstLook.Pricing.Website.VehicleInformation.initializeBase(this, [element]);

    this.delegates = {};
    var props = [
    "edit_wrapper",
    "mileage_textbox",
    "orginal_mileage_input",
    "invalid_mileage_error",
    "mileage_changed_warning",
    "save_button",
    "should_animate_height",
    "edit_wrapper_current_height_input",
    "transfer_price_textbox",
    "maximum_transfer_price",
    "transfer_price_high_error",
    "invalid_transfer_price_error",

    "has_mileage_error",
    "has_mileage_changed_warning",
    "has_maximum_transfer_price_error",
    "transfer_price_error"
    ];


    for (var i = 0, l = props.length; i < l; i++) {
        this.create_getter_setter(props[i]);
    };
};

FirstLook.Pricing.Website.VehicleInformation.prototype = {
    initialize: function() {
        this.delegates["this_onPropertyChagne"] = Function.createDelegate(this, this.onPropertyChange);

        this.add_propertyChanged(this.delegates["this_onPropertyChagne"]);

        this.check_mileage_error();
        this.check_validation_warning();

        FirstLook.Pricing.Website.VehicleInformation.callBaseMethod(this, "initialize");
    },
    updated: function() {
    	this.delegates["mileage_keyup"] = Function.createDelegate(this, this.onMileageKeyup);
    	this.delegates["transfer_price_keyup"] = Function.createDelegate(this, this.onTransferPriceKeyup);
    	
    	this.add_handler(this.get_mileage_textbox(), 'keyup', this.delegates["mileage_keyup"]);
    	if (!!this.get_transfer_price_textbox()) {
    		this.add_handler(this.get_transfer_price_textbox(), 'keyup', this.delegates["transfer_price_keyup"]);
		}
    	
    	this.delegates["save_click"] = Function.createDelegate(this, this.onSaveClick);
    	this.add_handler(this.get_save_button(), "click", this.delegates["save_click"]);

        if (this.get_should_animate_height()) {
        	this.animate_open();
        } else {
        	this.set_current_height( this.get_target_height() );
        }

        FirstLook.Pricing.Website.VehicleInformation.callBaseMethod(this, "updated");
    },
    dispose: function() {
    	this.remove_handlers();

    	this.delegates = undefined;

        FirstLook.Pricing.Website.VehicleInformation.callBaseMethod(this, "dispose");
    },

    // ============================
    // = Componeont Events: start =
    // ============================
    onMileageKeyup: function(e) {
    	this.check_mileage_error();
    	this.check_mileage_bookout_warning();
    },
    onTransferPriceKeyup: function(e) {
    	if (!!this.get_maximum_transfer_price()) {
    		this.check_maximum_transfer_price();
    	};
    	this.check_transfer_price_error();
    },
    onSaveClick: function(e) {
    	if (this.get_has_mileage_error() || this.get_has_maximum_transfer_price_error() || this.get_transfer_price_error()) {
    		e.preventDefault();
    		e.stopPropagation();
    	};
    },
    onPropertyChange: function(sender, args) {
        var property = args.get_propertyName();

        Sys.Debug.assert( !! !FirstLook.Pricing.Website.VehicleInformation["debug_" + property], property);        

        if (property == "has_mileage_error" && this.get_has_mileage_error()) {
        	this.get_invalid_mileage_error().style.display = "";
        	this.get_save_button().style.color = "#807d73";
        } else
        if (property == "has_mileage_error" && !this.get_has_mileage_error()) {
        	this.get_invalid_mileage_error().style.display = "none";
        	this.get_save_button().style.color = "";
        } else 
        if (property == "has_mileage_changed_warning" && this.get_has_mileage_changed_warning()) {
        	this.get_mileage_changed_warning().style.display = "";
        } else 
        if (property == "has_mileage_changed_warning" && !this.get_has_mileage_changed_warning()) {
        	this.get_mileage_changed_warning().style.display = "none";
        } else 
        if (property == "transfer_price_error" && this.get_transfer_price_error()) {
        	this.get_invalid_transfer_price_error().style.display = "";
        	this.get_save_button().style.color = "#807d73";
        } else
        if (property == "transfer_price_error" && !this.get_transfer_price_error()) {
        	this.get_invalid_transfer_price_error().style.display = "none";
        	this.get_save_button().style.color = "";
        } else
        if (property == "has_maximum_transfer_price_error" && this.get_has_maximum_transfer_price_error()) {
        	this.get_transfer_price_high_error().style.display = "";
        	this.get_save_button().style.color = "#807d73";
        } else 
        if (property == "has_maximum_transfer_price_error" && !this.get_has_maximum_transfer_price_error()) {
        	this.get_transfer_price_high_error().style.display = "none";
        	this.get_save_button().style.color = "";
        }
        
        if (property == "has_mileage_error" || property == "has_mileage_changed_warning" 
        || property == "has_maximum_transfer_price_error" || property == "transfer_price_error") {
        	this.check_validation_warning();
        }
        

        if ( !! FirstLook.Pricing.Website.VehicleInformation.Trace || !!FirstLook.Pricing.Website.VehicleInformation["trace_" + property]) {
            Sys.Debug.traceDump(sender["get_" + property](), sender.get_id() + ":" + property);
        };
    },
    // = Componeont Events: end =

    // ==========================
    // = Validation Code: start =
    // ==========================
    check_mileage_bookout_warning: function() {
    	// ====================================================================
    	// = Only show the changed warning if the value is a valid number, PM =
    	// ====================================================================
    	if ( !this.get_has_mileage_error() ) {
    		var mileage_text = this.get_text_value( this.get_mileage_textbox() ),
    		original_mileage_text = this.get_text_value( this.get_orginal_mileage_input() );
    		
    		// ======================================
    		// = Compare as numbers if possible, PM =
    		// ======================================
    		if (this.is_numeric( mileage_text ) && this.is_numeric( original_mileage_text )) {
    			this.set_has_mileage_changed_warning( this.to_number( mileage_text ) != this.to_number( original_mileage_text ) );
    		} else {
    			this.set_has_mileage_changed_warning( mileage_text != original_mileage_text );
    		}
    		
    	} else {
    		this.set_has_mileage_changed_warning( false );
    	}
    },
    check_mileage_error: function() {
    	var mileage_text = this.get_text_value( this.get_mileage_textbox() );
        if (this.get_has_mileage_error() && this.is_numeric( mileage_text )) {
            this.set_has_mileage_error( false );
        } else 
        if (!this.is_numeric( mileage_text )) {
            this.set_has_mileage_error( true );
        };
    },
    check_validation_warning: function() {
    	if (!!this.get_mileage_textbox()) {
	    	if (this.get_has_mileage_error() || this.get_has_mileage_changed_warning()) {
	    		this.show_validation_warning( this.get_mileage_textbox() );
	    	} else {
	    		this.hide_validation_warning( this.get_mileage_textbox() );
	    	}
    	}
    	
    	if (!!this.get_transfer_price_textbox()) {
    		if (this.get_has_maximum_transfer_price_error() || this.get_transfer_price_error()) {
	    		this.show_validation_warning( this.get_transfer_price_textbox() );
	    	} else {
	    		this.hide_validation_warning( this.get_transfer_price_textbox() );
	    	}
    	}
    },
    check_maximum_transfer_price: function() {
    	var transfer_price_text = this.get_text_value( this.get_transfer_price_textbox() );
    	if (transfer_price_text > this.get_maximum_transfer_price()) {
    		this.set_has_maximum_transfer_price_error( true );
    	} else {
    		this.set_has_maximum_transfer_price_error( false );
    	}
    },
    check_transfer_price_error: function() {
    	var transfer_price_text = this.get_text_value( this.get_transfer_price_textbox() );
        if (this.get_transfer_price_error() && this.is_numeric( transfer_price_text )) {
            this.set_transfer_price_error( false );
        } else 
        if (!this.is_numeric( transfer_price_text )) {
            this.set_transfer_price_error( true );
        };
    },
    show_validation_warning: function(input) {
    	Sys.UI.DomElement.addCssClass(this.get_element_parent(input), "validation_error");
    },
    hide_validation_warning: function(input) {
    	Sys.UI.DomElement.removeCssClass(this.get_element_parent(input), "validation_error");
    },
    // = Validation Code: end =

    // ===========================
    // = Animation Height: start =
    // ===========================
    animate_open: function() {
    	var animated_element = this.get_edit_wrapper(),
    	animator = this.get_animator(),
    	starting_height = this.get_current_height();

    	animated_element.style.overflow = "hidden";
    	animated_element.style.visibility = "hidden";
    	animated_element.style.position = "absolute";
    	animated_element.style.display = "block";

    	var ending_height = this.get_target_height();

    	animated_element.style.height = starting_height;

    	animated_element.style.visibility = "";
    	animated_element.style.position = "";

    	animator.stop();

    	animator.set_startValue( starting_height );
    	animator.set_endValue( ending_height );

    	animator.play();
    },
    get_animator: function() {
    	if (!!!this._animator) {
    		this.delegates["animator_ended"] = Function.createDelegate(this, this.animator_ended);
    		this._animator = new AjaxControlToolkit.Animation.LengthAnimation(this.get_edit_wrapper(), .25, 10, 'style', null, 0, 0, 'px');
        	this._animator.set_propertyKey('height');
        	this._animator.add_ended( this.delegates["animator_ended"] );
    	};
    	return this._animator;
    },
    animator_ended: function(sender, args) {
    	this.get_edit_wrapper().style.height = "";
    },
    set_current_height: function( value ) {
    	this.get_edit_wrapper_current_height_input().value = value;
    	this.raisePropertyChanged("current_height");
    },
    get_current_height: function() {
    	return this.get_edit_wrapper_current_height_input().value || 0;
    },
    get_target_height: function() {
    	var el = this.get_edit_wrapper(),
    	running_height = el.offsetHeight;
    	
    	var borderTopHeight = this.get_computed_style(el, "borderTopWidth"),
    	borderBottomHeight = this.get_computed_style(el, "borderBottomWidth");
    	
    	if (!!borderTopHeight && borderTopHeight.match(/px$/)) {
    		running_height += this.to_number(borderTopHeight);
    	};
    	
    	if (!!borderBottomHeight && borderBottomHeight.match(/px$/)) {
    		running_height += this.to_number(borderBottomHeight);
    	};
    	
    	return running_height;
    },
    // = Animation Height: end =

    // =========================
    // = Helper Methods: start =
    // =========================
    create_getter_setter: function(property) {
        // =================================================================
        // = Generic Function to create AJAX.net style getters and setters =
        // =================================================================
        if (this["get_" + property] === undefined) {
            this["get_" + property] = (function(thisProp) {
                return function get_property() {
                    return this[thisProp];
                };
            })("_" + property);
        };
        if (this["set_" + property] === undefined) {
            this["set_" + property] = (function(thisProp, raiseType) {
                return function set_property(value) {
                    if (value !== this[thisProp]) {
                        this[thisProp] = value;
                        this.raisePropertyChanged(raiseType);
                    };
                };
            })("_" + property, property);
        };
    },
    add_handler: function(el, type, fn) {
        if (this._eventCache === undefined) {
            this._eventCache = [];
        }
        this._eventCache.push([el, type, fn]);
        $addHandler(el, type, fn);
    },
    remove_handlers: function() {
        if (this._eventCache !== undefined) {
            for (var i = 0, l = this._eventCache.length; i < l; i++) {
                $removeHandler(this._eventCache[i][0], this._eventCache[i][1], this._eventCache[i][2]);
                this._eventCache[i][0] = undefined;
                this._eventCache[i][1] = undefined;
                this._eventCache[i][2] = undefined;
            };
            delete this._eventCache;
        };
    },
    to_number: function(value) {
		return value.replace(/[^\d\.]/g,"") - 0;
    },
    is_numeric: function(value) {
        return ! isNaN(value.replace(/\,/g, "") - 0);
    },
    get_text_value: function(el) {
        return el.value || el.textContent || el.innerText;
    },
    get_element_parent: function(el) {
    	return el.parentNode || el.parent;
    },
    get_computed_style: function(el, style) {
        if ( !! el.currentStyle) {
            return el.currentStyle[style];
        } else if ( !! document.defaultView && !!document.defaultView.getComputedStyle) {
            return document.defaultView.getComputedStyle(el, "")[style];
        } else {
            return el.style[style];
        }
    }
    // = Helper Methods: end =
};

if (FirstLook.Pricing.Website.VehicleInformation.registerClass !== undefined)
 FirstLook.Pricing.Website.VehicleInformation.registerClass('FirstLook.Pricing.Website.VehicleInformation', Sys.UI.Behavior);

Sys.Application.notifyScriptLoaded();