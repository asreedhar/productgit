/**
 * Descibe this class
 * @author Simon Swenmouth
 * @author Paul Monson
 * @version 2
 * @requires FirstLook.Utility
 */
if ( !! Sys) {
    Type.registerNamespace('FirstLook.Pricing.Website');

    FirstLook.Pricing.Website.OverrideMarketAnalysis = function(element) {
        FirstLook.Pricing.Website.OverrideMarketAnalysis.initializeBase(this, [element]);
        
    };
    FirstLook.Pricing.Website.OverrideMarketAnalysis.prototype = {
        initialize: function() {
            FirstLook.Pricing.Website.OverrideMarketAnalysis.callBaseMethod(this, "initialize");
            this._valid = true;
        },
        updated: function() {
            this.setupDatePicker();

            this.addHandler(this.get_ExpiryDateTextBox(), 'change', this.getDelegate("onDateChange"));
            this.addHandler(this.get_DaysFromNowTextBox(), 'change', this.getDelegate("onDaysChange"));
            this.addHandler(this.get_DateImage(), 'click', this.getDelegate("onTriggerDateChange"));
            this.addHandler(this.get_AddButton(), 'click', this.getDelegate("onAddButtonClick"));
            this.addHandler(this.get_EditButton(), 'click', this.getDelegate("onEditButtonClick"));

            var dialogID = this.get_EditDialog().id;
            var dialogClick = this.getDelegate("onEditDialogButtonClick");
            setTimeout(function() {
                // ====================================
                // = Fucking Chicken and Egg problem  =
                // ====================================
                if ($find(dialogID)) {
                    $find(dialogID).add_click(dialogClick);
                }
            },
            0);
        },
        dispose: function() {        	

            var dialogID = this.get_EditDialog().id;
            var dialogClick = this.getDelegate("onEditDialogButtonClick");
            setTimeout(function() {
                // ====================================
                // = Fucking Chicken and Egg problem  =
                // ====================================
                if ($find(dialogID)) {
                    $find(dialogID).remove_click(dialogClick);
                }
            },
            0);
            
            this.removeHandlers();
            this.clearDelegates();

            FirstLook.Pricing.Website.OverrideMarketAnalysis.callBaseMethod(this, "dispose");
        },
        setupDatePicker: function() {
        	// jQuery datePicker setup
            $(this.get_ExpiryDateTextBox()).datepicker({
                defaultDate: Date.parseLocale(this.get_ExpiryDateTextBox().value, Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern),
                onSelect: this.getDelegate("onDateChange")
            });
        },
        onDaysChange: function(e) {
            this.resetErrorMessages();
            this._valid = false;
            var daysFromNow = parseInt(this.get_DaysFromNowTextBox().value, 10);
            if (isNaN(daysFromNow)) {
                // clear date
                this.get_ExpiryDateTextBox().value = '';
                // set error
                this.get_DaysFormatError().style.display = 'block';
            }
            else {
                this.get_DaysFromNowTextBox().value = daysFromNow;
                // calculate new date
                var nowDate = new Date();
                var newDate = new Date(nowDate.getTime() + (daysFromNow * 1000 * 60 * 60 * 24));
                // update date
                this.get_ExpiryDateTextBox().value = newDate.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern);
                // show value error message
                this._valid = this.validateDays(daysFromNow);
            }
            e.preventDefault();
        },
        onAddButtonClick: function(e) {
        	if ($find(this.get_EditDialog().id)) {
                $find(this.get_EditDialog().id).show();
                this.get_AddButton().style.display = "none";
                this.get_OnPanel().style.display = "none";

                e.preventDefault();
            }
        },
        onEditDialogButtonClick: function(sender, eventArgs) {
            var e = event || window.event;
            if (!e.preventDefault) {
                e.preventDefault = function() {
                    e.returnValue = false;
                    return false;
                };
            }
            if (eventArgs.get_command() == FirstLook.Common.WebControls.UI.DialogButtonCommand.Cancel) {
                sender.hide();
                if (this.get_ExpiryDateTextBox().value == "") {
                    this.get_AddButton().style.display = "";
                } else {
                    this.get_OnPanel().style.display = "";
                }
                e.preventDefault();
            } else {
                if (!this._valid) {
                    alert('Please correct your errors.');
                    e.preventDefault();
                } else {
                    sender.hide();
                }
            }
        },
        onEditButtonClick: function(e) {
            this.showEditDialog(e);
        },
        onDateChange: function(e) {
            this.resetErrorMessages();
            this._valid = false;
            var newDate = Date.parseLocale(this.get_ExpiryDateTextBox().value, Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern);
            if (newDate) {
                var daysFromNow = this.daysElapsed(newDate, new Date());
                this.get_DaysFromNowTextBox().value = daysFromNow;
                this._valid = this.validateDays(daysFromNow);
            }
            else {
                // clear days
                this.get_ExpiryDateTextBox().value = '';
                // set error
                this.get_DateFormatError().style.display = '';
            }
            if (typeof(e) != 'string') {
                e.preventDefault();
            }
        },
        onTriggerDateChange: function(e) {
            this.get_ExpiryDateTextBox().focus(e);
        },
        validateDays: function(daysFromNow) {
            if (daysFromNow > 30) {
                this.get_DateInFutureError().style.display = '';
            }
            else if (daysFromNow < 1) {
                this.get_DateInPastError().style.display = '';
            }
            else {
                return true;
            }
            return false;
        },
        daysElapsed: function(date1, date2) {
            var difference = this.getUTCDate(date1) - this.getUTCDate(date2);
            return difference / 1000 / 60 / 60 / 24;
        },
        y2k: function(number) {
            return (number < 1000) ? number + 1900: number;
        },
        getUTCDate: function(date) {
        	return Date.UTC(this.y2k(date.getYear()), date.getMonth(), date.getDate(), 0, 0, 0);
        },
        resetErrorMessages: function() {
            this.get_DateFormatError().style.display = 'none';
            this.get_DaysFormatError().style.display = 'none';
            this.get_DateInPastError().style.display = 'none';
            this.get_DateInFutureError().style.display = 'none';
        }
    };
    FirstLook.Utility.createGetterSetters(FirstLook.Pricing.Website.OverrideMarketAnalysis, [
        // Dialogs
        "EditDialog", "OnPanel",
        // Elements
        "DaysFromNowTextBox", "ExpiryDateTextBox", "DateLabel", "DateImage",
        "AddButton", "EditButton", "RemoveButton",
        // Error Msg's
        "DateInPastError", "DateInFutureError", "DateFormatError", "DaysFormatError"
        ]);

    FirstLook.Pricing.Website.OverrideMarketAnalysis.registerClass('FirstLook.Pricing.Website.OverrideMarketAnalysis', Sys.UI.Behavior);
    
    Sys.Application.notifyScriptLoaded();
};