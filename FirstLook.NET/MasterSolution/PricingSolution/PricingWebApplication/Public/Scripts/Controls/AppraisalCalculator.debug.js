Type.registerNamespace('FirstLook.Pricing.Website');

FirstLook.Pricing.Website.AppraisalCalculator = function (element) {
    FirstLook.Pricing.Website.AppraisalCalculator.initializeBase(this,[element]);
    // user properties values
    this._appraisalValue = null;
    this._currentAppraisalValue = null;
    this._estimatedReconditioningCost = null;
    this._targetGrossProfit = null;
    this._auctionPrice = null;
    this._auctionProfitAmount = null;
    this._auctionProfitWithReconAmount = null;
    this._hasMarket = false;
    this._maxAppraisalValue = null;
    // constant properties
    this._minMarketPrice = null;
    this._avgMarketPrice = null;
    this._maxMarketPrice = null;
    this._hasMileageAdjustedSearch = null;
    // event driven properties
    this._rank = null;
    // dom elements
    this._percentageTextBox = null;
    this._appraisalValueTextBox = null;
    this._estimatedReconditioningCostTextBox = null;
    this._targetGrossProfitTextBox = null;
    this._targetSellingPriceTextBox = null;
    this._targetSelling1PriceTextBox = null;
    this._saveButton = null;
    this._sliderContainer = null;
    this._maxAppraisalValueElement = null;
};

FirstLook.Pricing.Website.AppraisalCalculator.prototype = {
    initialize: function() {
        FirstLook.Pricing.Website.AppraisalCalculator.callBaseMethod(this, 'initialize');
        this._onPropertyChangeDelegate = Function.createDelegate(this, this._onPropertyChange);
        this._onAppraisalValueTextChangeDelegate = Function.createDelegate(this, this._onAppraisalValueTextChange);
        this._onEstimatedReconditioningCostTextChangeDelegate = Function.createDelegate(this, this._onEstimatedReconditioningCostTextChange);
        this._onTargetGrossProfitTextChangeDelegate = Function.createDelegate(this, this._onTargetGrossProfitTextChange);
        this._onTargetSellingPriceChangeDelegate = Function.createDelegate(this, this._onTargetSellingPriceChange);
        this._onMarketAveragePriceChangeDelegate = Function.createDelegate(this, this._onMarketAveragePriceChange);

        this.create_slider();
    },
    dispose : function() {
        this.remove_propertyChanged(this._onPropertyChangeDelegate);
        var x = ['change','blur'];
        for (var i = 0, l = x.length; i < l; i++) {
            Sys.UI.DomEvent.removeHandler(this.get_appraisalValueTextBox(), x[i], this._onAppraisalValueTextChangeDelegate);
            Sys.UI.DomEvent.removeHandler(this.get_estimatedReconditioningCostTextBox(), x[i], this._onEstimatedReconditioningCostTextChangeDelegate);
            Sys.UI.DomEvent.removeHandler(this.get_targetGrossProfitTextBox(), x[i], this._onTargetGrossProfitTextChangeDelegate);
            Sys.UI.DomEvent.removeHandler(this.get_targetSellingPriceTextBox(), x[i], this._onTargetSellingPriceChangeDelegate);
            Sys.UI.DomEvent.removeHandler(this.get_percentageTextBox(), x[i], this._onMarketAveragePriceChangeDelegate);
        }
        delete this._onPropertyChangeDelegate;
        delete this._onAppraisalValueTextChangeDelegate;
        delete this._onEstimatedReconditioningCostTextChangeDelegate;
        delete this._onTargetGrossProfitTextChangeDelegate;
        delete this._onTargetSellingPriceChangeDelegate;
        delete this._onMarketAveragePriceChangeDelegate;
        $conversation(FirstLook.Pricing.Website.PriceChangeSubscription).unsubscribe(this);
        $conversation(FirstLook.Pricing.Website.PriceRankChangeSubscription).unsubscribe(this);
        FirstLook.Pricing.Website.AppraisalCalculator.callBaseMethod(this, 'dispose');
    },
    updated: function() {
        this.add_propertyChanged(this._onPropertyChangeDelegate);
        this.__fireTargetSellingPriceChangedEvent();
        var x = ['change','blur'];
        for (var i = 0, l = x.length; i < l; i++) {
            Sys.UI.DomEvent.addHandler(this.get_appraisalValueTextBox(), x[i], this._onAppraisalValueTextChangeDelegate);
            Sys.UI.DomEvent.addHandler(this.get_estimatedReconditioningCostTextBox(), x[i], this._onEstimatedReconditioningCostTextChangeDelegate);
            Sys.UI.DomEvent.addHandler(this.get_targetGrossProfitTextBox(), x[i], this._onTargetGrossProfitTextChangeDelegate);
            Sys.UI.DomEvent.addHandler(this.get_targetSellingPriceTextBox(), x[i], this._onTargetSellingPriceChangeDelegate);
            Sys.UI.DomEvent.addHandler(this.get_percentageTextBox(), x[i], this._onMarketAveragePriceChangeDelegate);
        }
        $conversation(FirstLook.Pricing.Website.PriceChangeSubscription).subscribe(this);
        $conversation(FirstLook.Pricing.Website.PriceRankChangeSubscription).subscribe(this);
        if (this.get_hasMileageAdjustedSearch()) {
            Sys.UI.DomEvent.addHandler(document.getElementById("mpp_help"), "click", this.getDelegate("toggle_help_dialog"));
        }

        this.showPriceHasChanged();
        FirstLook.Pricing.Website.AppraisalCalculator.callBaseMethod(this, 'updated');
    },
    get_appraisalValue: function() {
        return this._appraisalValue;
    },
    set_appraisalValue: function(value) {
        if (this._appraisalValue !== value) {
            this._appraisalValue = parseInt(value,10);
            this.raisePropertyChanged('appraisalValue');
        }
    },
    get_currentAppraisalValue: function() {
        return this._currentAppraisalValue;
    },
    set_currentAppraisalValue: function(value) {
        if (this._currentAppraisalValue !== value) {
            this._currentAppraisalValue = parseInt(value,10);
        }
    },
    get_estimatedReconditioningCost: function() {
        return this._estimatedReconditioningCost;
    },
    set_estimatedReconditioningCost: function(value) {
        if (this._estimatedReconditioningCost !== value) {
            this._estimatedReconditioningCost = parseInt(value,10);
            this.raisePropertyChanged('estimatedReconditioningCost');
        }
    },
    get_targetGrossProfit: function() {
        return this._targetGrossProfit;
    },
    set_targetGrossProfit: function(value) {
        if (this._targetGrossProfit !== value) {
            this._targetGrossProfit = parseInt(value,10);
            this.raisePropertyChanged('targetGrossProfit');
        }
    },
    get_auctionPrice:function () {
        return this._auctionPrice;
    },
    set_auctionPrice: function (value) {
        if (this._auctionPrice !== value) {
            this._auctionPrice = parseInt(value, 10);
        }
    },
    get_auctionProfitAmount:function () {
        return this.get_targetSellingPriceTextBox().value.replace(/[^\d]/g,'') - this.get_auctionPrice();
    },
    set_auctionProfitAmount:function (value) {
        if (this._auctionProfitAmount !== value) {
            this._auctionProfitAmount = parseInt(value,10);
            this.raisePropertyChanged('auctionProfitAmount');
        }
    },
    get_auctionProfitWithReconAmount:function () {
        return this._auctionProfitWithReconAmount;
    },
    set_auctionProfitWithReconAmount:function (value) {
        if (this._auctionProfitWithReconAmount !== value) {
            this._auctionProfitWithReconAmount = parseInt(value,10);
            this.raisePropertyChanged('auctionProfitWithReconAmount');
        }
    },
    _accumulate: function(counter, value) {
        if (value) {
            if (counter) {
                return value + counter;
            }
            else {
                return value;
            }
        }
        return counter;
    },
    get_price: function() {
        var price = this._accumulate(
            this._accumulate(this._appraisalValue, this._estimatedReconditioningCost),
            this._targetGrossProfit);
        return price;
    },
    set_price: function(value) {
        /* ignore */
    },
    get_adjustedMaxAppraisalValue: function() {
        return this.get_maxAppraisalValue() - this.get_targetGrossProfit() - this.get_estimatedReconditioningCost();
    },
    get_unitCost: function() {
        return this._accumulate(this._appraisalValue, this._estimatedReconditioningCost);
    },
    set_unitCost: function(value) {
        /* ignore */
    },
    get_rank: function() {
        return this._rank;
    },
    set_rank: function(value) {
        if (this._rank !== value) {
            this._rank = value;
            this.raisePropertyChanged('rank');
        }
    },
    get_avgMarketPrice: function() {
        return this._avgMarketPrice;
    },
    set_avgMarketPrice: function(value) {
        this._avgMarketPrice = value;
    },
    get_maxAppraisalValue: function() {
        return this._maxAppraisalValue;
    },
    set_maxAppraisalValue: function(value) {
        this._maxAppraisalValue = value;
    },
    get_maxAppraisalValueElement: function() {
        return this._maxAppraisalValueElement;
    },
    set_maxAppraisalValueElement: function(value) {
        this._maxAppraisalValueElement = value;
    },
    get_targetSellingPrice1TextBox: function() {
        return this._targetSellingPrice1TextBox;
    },
    set_targetSellingPrice1TextBox: function(value) {
        this._targetSellingPrice1TextBox = value;
    },
    get_targetSellingPriceTextBox: function() {
        return this._targetSellingPriceTextBox;
    },
    set_targetSellingPriceTextBox: function(value) {
        this._targetSellingPriceTextBox = value;
    },
    get_targetGrossProfitTextBox: function() {
        return this._targetGrossProfitTextBox;
    },
    set_targetGrossProfitTextBox: function(value) {
        this._targetGrossProfitTextBox = value;
    },
    get_estimatedReconditioningCostTextBox: function() {
        return this._estimatedReconditioningCostTextBox;
    },
    set_estimatedReconditioningCostTextBox: function(value) {
        this._estimatedReconditioningCostTextBox = value;
    },
    get_appraisalValueTextBox: function() {
        return this._appraisalValueTextBox;
    },
    set_appraisalValueTextBox: function(value) {
        this._appraisalValueTextBox = value;
    },
    get_percentageTextBox: function() {
        return this._percentageTextBox;
    },
    set_percentageTextBox: function(value) {
        this._percentageTextBox = value;
    },
    get_auctionPriceTextBox: function () {
        return this._auctionPriceTextBox;
    },
    set_auctionPriceTextBox: function (value) {
        this._auctionPriceTextBox = value;
    },
    get_saveButton: function() {
        return this._saveButton;
    },
    set_saveButton: function(value) {
        this._saveButton = value;
    },
    get_sliderContainer: function() {
        return this._sliderContainer;
    },
    set_sliderContainer: function(value) {
        this._sliderContainer = value;
    },
    get_hasMileageAdjustedSearch: function() {
        return this._hasMileageAdjustedSearch;
    },
    set_hasMileageAdjustedSearch: function(value) {
        this._hasMileageAdjustedSearch = value;
    },
    get_minMarketPrice: function() {
        return this._minMarketPrice;
    },
    set_minMarketPrice: function(value) {
        this._minMarketPrice = value;
    },
    get_maxMarketPrice: function() {
        return this._maxMarketPrice;
    },
    set_maxMarketPrice: function(value) {
        this._maxMarketPrice = value;
    },
    get_minPctMarketAvg: function() {
        return this._minPctMarketAvg;
    },
    set_minPctMarketAvg: function(value) {
        this._minPctMarketAvg = value;
    },
    get_maxPctMarketAvg: function() {
        return this._maxPctMarketAvg;
    },
    set_maxPctMarketAvg: function(value) {
        this._maxPctMarketAvg = value;
    },
    get_hasMarket: function() {
        return this._hasMarket;
    },
    set_hasMarket: function(value) {
        this._hasMarket = value;
    },
    _onPropertyChange: function(sender, e) {
        var propertyName = e.get_propertyName();
        if (propertyName == 'rank') {
            // update label
            var formatter = FirstLook.Pricing.Website.StringFormatter.getInstance().formatOrdinal;
        }
        else if (propertyName == 'appraisalValue' ||
                 propertyName == 'estimatedReconditioningCost' ||
                 propertyName == 'targetGrossProfit') {
            // labels and text boxes
            var pct = '';
            if (!isNaN(this.get_avgMarketPrice())) {
                pct = Math.round(this.get_price()/this.get_avgMarketPrice()*100.0);
                if (pct == Infinity) { pct = 'NA'; }
            }
            
            this.get_percentageTextBox().value = pct.toString();
            this.__populateTextBox(this.get_appraisalValueTextBox(), this.get_appraisalValue());
            this.__populateTextBox(this.get_estimatedReconditioningCostTextBox(), this.get_estimatedReconditioningCost());
            this.__populateTextBox(this.get_targetGrossProfitTextBox(), this.get_targetGrossProfit());
            this.__populateTextBox(this.get_targetSellingPriceTextBox(), this.get_price());
            this.__populateTextBox(this.get_targetSellingPrice1TextBox(), this.get_price());
            if (this.get_maxAppraisalValueElement() !== null) {
                this.__populateElementText(this.get_maxAppraisalValueElement(), this.get_adjustedMaxAppraisalValue());
            }
            this.__fireTargetSellingPriceChangedEvent();
            this.update_slider(this.get_price()); 
            
            if (!!this.get_appraisalValue()) {
                this.get_percentageTextBox().readOnly = false;
            } else {
                this.get_percentageTextBox().readOnly = true;
            }

            // raise price change
            $conversation(FirstLook.Pricing.Website.PriceChangeSubscription).notify(this.get_price());
            // raise unit cost change
            $conversation(FirstLook.Pricing.Website.UnitCostChangeSubscription).notify(this.get_unitCost());
            // raise a valuation change
            $conversation(FirstLook.Pricing.Website.ValuationChangeSubscription).notify({appraisalValue:this.get_appraisalValue(), estimatedReconditioningCost:this.get_estimatedReconditioningCost()});
            
            try {FirstLook.Page.ProgressManager.get_instance().hideProgressOnRequest();} catch (e) {}
     
            // save data back (in a naughty manner)
            var qs_page = new FirstLook.Pricing.Website.QueryString(window.location.search.substr(1));
            PageMethods.SaveVehiclePricingDecisionInput(
                qs_page.get_parameter('oh'),
                qs_page.get_parameter('vh'),
                this.get_appraisalValue(),
                this.get_estimatedReconditioningCost(),
                this.get_targetGrossProfit());
                
            try {FirstLook.Page.ProgressManager.get_instance().showProgressOnRequest();} catch (e) {}
        }

        this.showPriceHasChanged();
    },
    set_targetSellingPrice: function(value) {
        this.__populateTextBox(this.get_targetSellingPriceTextBox(), value);
        this.__populateTextBox(this.get_targetSellingPrice1TextBox(), value);
        this._onTargetSellingPriceChange();
    },
    get_targetSellingPrice: function() {
        return parseInt(this.get_targetSellingPriceTextBox().value.replace(/[^\d]/g,''),10);
    },
    get_helpDialog: function() {
        return this._helpDialog;
    },
    set_helpDialog: function(value) {
        this._helpDialog = value;
    },
    _onAppraisalValueTextChange: function(e) {
        this.__intTextChange(
            this.get_appraisalValueTextBox(),
            this.get_appraisalValue(),
            this.set_appraisalValue);
    },
    _onEstimatedReconditioningCostTextChange: function(e) {
        this.__intTextChange(
            this.get_estimatedReconditioningCostTextBox(),
            this.get_estimatedReconditioningCost(),
            this.set_estimatedReconditioningCost);     
    },
    _onTargetGrossProfitTextChange: function(e) {
        this.__intTextChange(
            this.get_targetGrossProfitTextBox(),
            this.get_targetGrossProfit(),
            this.set_targetGrossProfit);
    },
    _onTargetSellingPriceChange: function (e) {
        var newTargetSellingPrice = this.get_targetSellingPrice();
        var newTargetGrossProfit = newTargetSellingPrice - this.get_estimatedReconditioningCost() - this.get_appraisalValue();
        var currentTargetGrossProfit = this.get_targetGrossProfit();
        if (typeof currentTargetGrossProfit === "undefined") {
            currentTargetGrossProfit = 0;
        }
        
        if (newTargetGrossProfit != currentTargetGrossProfit && this.get_appraisalValue() != undefined) {
            this.__populateTextBox(this.get_targetGrossProfitTextBox(), newTargetGrossProfit);
            if (newTargetGrossProfit < 0) {
                Sys.UI.DomElement.addCssClass(this.get_targetGrossProfitTextBox(), "negative_value");
            } else {
                Sys.UI.DomElement.removeCssClass(this.get_targetGrossProfitTextBox(), "negative_value");
            }
            this.__intTextChange(
                this.get_targetGrossProfitTextBox(),
                this.get_targetGrossProfit(),
                this.set_targetGrossProfit);
        }       
    },
    _onMarketAveragePriceChange: function (e) {
        var newMarketAveragePct = parseInt(this.get_percentageTextBox().value.replace(/[^\d]/g,''),10);
        var newAppraisalValue = (newMarketAveragePct / 100) * this.get_avgMarketPrice() - this.get_estimatedReconditioningCost() - this.get_targetGrossProfit();
        if (newAppraisalValue > 0 && newMarketAveragePct != Math.round(this.get_price()/this.get_avgMarketPrice()*100.0)) {
            this.__populateTextBox(this.get_appraisalValueTextBox(), newAppraisalValue);
            this.__intTextChange(
                this.get_appraisalValueTextBox(),
                this.get_appraisalValue(),
                this.set_appraisalValue);
        }
    },
    __intTextChange: function(input, original, setter) {
        var text = input.value.replace(/[^0-9]/g,"");
        var isNeg = input.value.indexOf("(") !== -1;
        if (text.length > 0) {
            var value = parseInt(text,10);
            if (isNeg) value *= -1;
            if (!isNaN(value)) {
                setter.apply(this, [value]);
                return;
            }
        } else {
            setter.apply(this, [0]);
        }
        this.__populateTextBox(input, null);
    },
    __populateTextBox: function(input, value) {
        if (isNaN(value) || value == 0) {
            input.value = "";
        }
        else {
            input.value = String.localeFormat("{0:c0}", value);
        }
    },
    __populateElementText: function(el, value, nullText) {
        if (isNaN(value) || value == 0) {
            el.innerText = nullText || "";
        } else {
           el.innerText = String.localeFormat("{0:c0}", value);
        }
    },
    __fireTargetSellingPriceChangedEvent: function() {
        // ===================
        // = Damnit, IE only =
        // ===================
        if (this.get_targetSellingPriceTextBox().fireEvent) {
            this.get_targetSellingPriceTextBox().fireEvent('onchange');
        }
    },
    showPriceHasChanged: function() {
        if (!!!this.__currentTargetSelling) {
            this.__currentTargetSelling = this.get_targetSellingPrice();
        }
        if (this.get_appraisalValue() != null) {
            this.enable_slider();
        } else {
            this.disable_slider();
        }
        if (this.__currentTargetSelling !== this.get_targetSellingPrice()) {
            var thing = $(this.get_saveButton()).parent();
            thing.addClass("has_changed_price");
        }
    },
    create_slider: function() {
        var sliderContainer = this.get_sliderContainer(),
        calculator = this,
        r = new Raphael(sliderContainer, 510, 96),
        o = $(sliderContainer).offset(),
        // Might be able to move to Sys.UI.DomElement.getLocation(el)
        b,
        min_mark,
        avg_mark,
        max_mark,
        min_market = this.get_minMarketPrice(),
        avg_market = this.get_avgMarketPrice(),
        max_market = this.get_maxMarketPrice(),
        range_market = max_market - min_market,
        m = range_market / 100,
        green_bars = {},
        width,
        path_resource = {
            skinny: {
                title: "",
                title_help: "",
                base_path: "M0 37 L504 37 L504 57 L0 57 Z",
                power_zone: "M0 37 L50 37 L50 57 L50 57 L0 57 Z",
                update_start: function (path, point) {
                    point = Math.round(point); // Round to simplify parsing, PM
                    return path.replace(/(\w)(\d+)([\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?)(\w)(\d+)([\,|\s]\d+[\s|\,]?)/, "$1"+point+"$3$4"+point+"$6");
                },
                update_end: function (path,point) {
                    point = Math.round(point); // Round to simplify parsing, PM
                    return path.replace(/(\w\d+[\,|\s]\d+[\s|\,]?\w)(\d+)([\,|\s]\d+[\s|\,]?\w)(\d+)([\,|\s]\d+[\s|\,]?\w)(\d+)([\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?)/, "$1"+point+"$3"+(point)+"$5"+point+"$7");
                },
                height: 20,
                offset: {
                    top: 37,
                    left: 27,
                    angle_offset: 27
                },
                should_shrink: false,
                handle: "M6 36 L12 43 L12 60 L0 60 L0 43 Z",
                handle_disabled: "M0 46 L6 60 L12 46 L6 33 Z",
                handle_width: 20,
                warning_text: 20
            },
            fat: {
                title: "MARKET\n PRICING POWER",
                title_help: "",
                base_path: "M27 37 L504 37 L477 64 L0 64 Z",
                power_zone: "M0 37 L50 37 L85 50 L50 64 L0 64 Z",
                update_start: function (path,point) {
                    point = Math.round(point); // Round to simplify parsing, PM
                    return path.replace(/(\w)(\d+)([\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?)(\w)(\d+)([\,|\s]\d+[\s|\,]?)/, "$1"+point+"$3$4"+point+"$6");
                },
                update_end: function (path,point) {
                    point = Math.round(point); // Round to simplify parsing, PM
                    return path.replace(/(\w\d+[\,|\s]\d+[\s|\,]?\w)(\d+)([\,|\s]\d+[\s|\,]?\w)(\d+)([\,|\s]\d+[\s|\,]?\w)(\d+)([\,|\s]\d+[\s|\,]?\w\d+[\,|\s]\d+[\s|\,]?)/, "$1"+point+"$3"+(point+30)+"$5"+point+"$7");
                },
                height: 27,
                offset: {
                    top: 37,
                    left: 27,
                    angle_offset: 27
                },
                should_shrink: true,
                handle: "M6 36 L12 43 L12 65 L0 65 L0 43 Z",
                handle_disabled: "M0 50 L6 68 L12 50 L6 33 Z",
                handle_width: 20,
                warning_text: 54
            }
        },
        graphic_resource = path_resource[this.get_hasMileageAdjustedSearch() ? "fat" : "skinny"],
        hasSufficentListings = this.get_hasMarket(),
        title,
        handle = {},
        enabled = true;

        b = make_graphic(r, o);
        make_maker_bars(40, b, r, "#f7ae09");


        if (hasSufficentListings) {
            title = r.text(0, 20, graphic_resource.title);
            title.attr({
                fill: "#52bc62",
                "font-family": "'Arial Black'",
                "font-size": 12,
                "font-style": "italic"
            });
            title.node.parentElement.title = graphic_resource.title_help;

            set_area(make_power_zone(r), this.get_minPctMarketAvg(), this.get_maxPctMarketAvg(), avg_market, min_market, max_market, r, b);

            min_mark = make_named_marker(0.0, "LOW", min_market, b, r);
            avg_mark = make_named_marker(get_x(avg_market), "AVG.", avg_market, b, r);
            max_mark = make_named_marker(1.0, "HIGH", max_market, b, r);
        } else {
            title = r.text(250, graphic_resource.warning_text, "There is a limited number of cars in the search results.\n Review Market Listings below or increase Search Radius.");
            title.attr({
                fill: "#886728",
                "font-family": "'Arial Black'",
                "font-size": 12
            });
        }


        function get_x(y) {
            // Get _pct
            // Slider roughly works like a slope intercept function y = xm + b, PM
            return ((y-min_market) / m)/100;
        }
        function get_y(x) {
            // Get _dollar
            // Slider roughly works like a slope intercept function y = xm + b, PM
            return (x * m)*100 + min_market;
        }
        function comma(value) {
            value = value.toString();
            var regex = /(\d+)(\d{3})/;
            while (regex.test(value)) {
                value = value.replace(regex, '$1,$2');
            }
            return value;
        }
        function make_graphic(_r, _o) {
            var path = graphic_resource.base_path,
            shadow = _r.path(path),
            bar = _r.path(path),
            set = _r.set();

            set.push(shadow, bar);

            shadow.attr({
                "stroke": "none",
                "fill": "90-#d5d9e2-#adb0b5-#d5d9e2",
                "translation": "2,3"
            });
            shadow.scale(1.005, 1.4);

            bar.attr({
                "fill": "90-#fcb912-#fdeca6",
                "stroke": "#e0c400"
            });

            return set;
        }
        function make_power_zone(_r) {
            var suggested_area = _r.path(graphic_resource.power_zone);
            suggested_area.attr({
                "fill": "90-#36b449-#a2dcaa",
                "stroke": "#a2dcaa",
                "clip": graphic_resource.base_path
            });

            return suggested_area;
        }
        function make_named_marker(pct, label, amt, box, _r) { 
            var width = box.getBBox().width,
            x_angle_length = graphic_resource.offset.angle_offset,
            left_offset = graphic_resource.offset.left,
            x = Math.round((width - (x_angle_length * 2)) * pct) + left_offset,
            path = ["M", x, " ",(graphic_resource.offset.top-1)," L", x, " ", (graphic_resource.offset.top + graphic_resource.height + 4)].join(""),
            bar = _r.path(path).attr({
                "stroke": "#a39568",
                "stroke-width": 2
            }),
            t1 = _r.text(x, graphic_resource.offset.top + graphic_resource.height + 11, label).attr({
                "font-size": 11,
                "fill": "#737373"
            }),
            t2 = _r.text(x, graphic_resource.offset.top + graphic_resource.height + 22, "$" + comma(amt)).attr({
                "font-size": 12,
                "font-weight": "bold",
                "fill": "#41455e"
            });
            s = _r.set();
            s.push(bar, t1, t2);

            return s;
        }
        function make_maker_bars(num, box, _r, color, min, max, shrink) {
            var x_angle_length = graphic_resource.offset.angle_offset,
            x_offset = graphic_resource.offset.left,
            width = box.getBBox().width - (2 * x_angle_length),
            set = _r.set(),
            values,
            i = 0,
            last = 0;

            min = min !== 0 ? min || - Infinity: 0;
            max = max !== 0 ? max || Infinity: 0;

            function get_marker_path(pct, y1, y2) {
                if (pct === Infinity) pct = 100;
                var x = pct * width + x_angle_length;
                return {
                    x: x,
                    y1: y1,
                    y2: y2
                };
            }

            function toLinePath(obj) {
                return ["M", obj.x, " ", obj.y1, " L", obj.x, " ", obj.y2].join("");
            }

            for (i = 0; i <= num; i++) {
                values = get_marker_path(i / num, graphic_resource.offset.top, graphic_resource.height + graphic_resource.offset.top);
                if (values.x >= min && values.x <= max) {
                    last = i;
                    set.push(_r.path(toLinePath(values)).attr({
                        "stroke": color
                    }));
                }
            }

            if (shrink) {
                var slope = 0.5;
                var off = 0;
                while (true) {
                    values = get_marker_path(++last / num, graphic_resource.offset.top, graphic_resource.height + graphic_resource.offset.top);
                    off = slope * (values.x - max);
                    values.y1 = graphic_resource.offset.top + off;
                    values.y2 = graphic_resource.height + graphic_resource.offset.top - off;
                    if (values.y1 >= values.y2) {
                        break;
                    }
                    set.push(_r.path(toLinePath(values)).attr({
                        "stroke": color
                    }));
                } 
            }

            return set;
        }
        function show_bubble() {}
        function hide_bubble() {}
        function format_comma(value) {}
        function set_area(suggested_area, start, end, avg, min, max, _r, b) {
            var old_path = suggested_area.attr("path")+"", // To String Hack
            new_path = old_path,
            x_angle_length = graphic_resource.offset.angle_offset,
            left_offset = graphic_resource.offset.left,
            width = b.getBBox().width - (2 * x_angle_length),
            path_start = width * (avg*(start * 0.01)-min)/(max-min) + left_offset,
            path_end = width * (avg*(end * 0.01)-min)/(max-min) + left_offset;

            if (path_start < x_angle_length) path_start = x_angle_length; // Min is the low point of the slider, 
            if (path_end > width) path_end = width; // Max is the high point of the slider,

            new_path = graphic_resource.update_start(new_path, path_start);
            new_path = graphic_resource.update_end(new_path, path_end);

            if (green_bars.remove) green_bars.remove();
            green_bars = make_maker_bars(40, b, _r, "#2c9b3c", path_start, path_end, graphic_resource.should_shrink);

            suggested_area.attr({
                "path": new_path
            });

            title.attr({
                x: path_start+((path_end-path_start)/2)
            });
        }
        function make_handle(min, max, b, _r) {
            var handle = _r.path(graphic_resource.handle),
            width = b.getBBox().width; 
            if (handle.paper.raphael.type == "VML") {
                handle.node.UNSELECTABLE = "on"; // Fuck you IE please doen't load you're "accelerators", PM
            }

            handle.attr({
                stroke: "#969daf",
                "stroke-linecap": "square",
                "stroke-linejoin": "miter",
                fill: "90-#fff-#d1d1d1-#f7f7f5-#fff"
            });

            var drag = {
                start: function(x, y) {
                    if (!enabled) return;
                    handle._is_sliding = true;
                    this.ox = x;
                    this.offset = this.getBBox();
                    this.attr({
                        "fill": "#fff",
                        "fill-opacity": 0.8
                    });
                    /*b_set.hide();
                    b_set.attr("opacity", 0);*/
                },
                end: function(x, y) {
                    if (!enabled) return;
                    handle._is_sliding = false;
                    this.attr({
                        "fill": "90-#fff-#d1d1d1-#f7f7f5-#fff",
                        "fill-opacity": 1
                    });
                    /// if > half the slider... flip me
                    /*b_set.show();
                    b_set.animate({
                        opacity: 1
                    },
                    1000); */
                },
                move: function(dx, dy) {
                    if (!enabled) return;
                    var d = this.ox + dx,
                    pos = this.attr("translation").x,
                    nx = d - pos - o.left - (graphic_resource.handle_width/2),
                    a = (pos-27+6)/(width-59),
                    can_move_left = nx > 0 || a >= -0.02,
                    can_move_right = nx < 0 || a <= 1.02;

                    if (can_move_left && can_move_right) { 
                        this.translate(nx, 0);

                        calculator.set_targetSellingPrice(Math.round(get_y(a)/10)*10);
                    }
                }
            };

            handle.drag(drag.move, drag.start, drag.end);

            return handle;
        }
        this.update_slider = function() {};
        this.enable_slider = function() {
            if (!enabled) {
                if (handle.attr) {
                    handle.attr({
                        "fill": "90-#fff-#d1d1d1-#f7f7f5-#fff",
                        "fill-opacity": 1
                    });
                }
                enabled = true;
            }
        }
        this.disable_slider = function() {
            if (enabled) {
                if (handle.attr) {
                    handle.attr({
                        "fill": "#ddd",
                        "fill-opacity": 0.8
                    });
                }
                enabled = false;
            }
        }
        if (hasSufficentListings) {
            handle = make_handle(min_market, max_market, b, r);
            this.enable_slider();
            this.update_slider = (function(handle, min, max) {
                var width = b.getBBox().width - graphic_resource.offset.angle_offset; 
                var isVML = handle.paper.raphael.type == "VML";
                return function(price) {
                    if (price < min || isNaN(price)) price = min;
                    var center = width * get_x(price),
                    current = handle.attr("translation").x,
                    isOutOfBoundsLower = center < 0,
                    isOutOfBoundsUpper = center > width - graphic_resource.offset.left;

                    if (isOutOfBoundsUpper) center = width - graphic_resource.offset.left;
                    if (isOutOfBoundsLower) center = 0;

                    if (isOutOfBoundsUpper || isOutOfBoundsLower) {
                        handle.attr({
                            fill: "#fff",
                            "fill-opacity": 0.5
                        });
                    }

                    if (!handle._is_sliding) {
                        if (isVML) {
                            handle.translate(center+graphic_resource.offset.left-graphic_resource.handle_width - current, 0);
                        } else {
                            handle.translate(center+graphic_resource.offset.left-graphic_resource.handle_width, 0);
                        }
                    }
                }
            })(handle, min_market, max_market, b);

            this.update_slider(this.get_targetSellingPrice()); 
        }
        $("#avg_selling_price").text($(".store_avg_selling_price").text());
    },
    toggle_help_dialog: function() {
        $(this.get_helpDialog()).toggle();
    }
};

FirstLook.Pricing.Website.AppraisalCalculator.registerClass('FirstLook.Pricing.Website.AppraisalCalculator', Sys.UI.Behavior, FirstLook.Pricing.Website.IPriceControl, FirstLook.Pricing.Website.IPriceRankControl, FirstLook.Pricing.Website.IUnitCostControl);

Sys.Application.notifyScriptLoaded();
