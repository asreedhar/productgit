﻿if ( !! Sys) {
    Type.registerNamespace('FirstLook.Pricing.Website');

    // ========
    // = List =
    // ========
    FirstLook.Pricing.Website.VehicleNavigationList = function() {
        if ( !! !FirstLook.Pricing.Website.VehicleNavigationList.__instance) {
            FirstLook.Pricing.Website.VehicleNavigationList.__instance = this;
            FirstLook.Pricing.Website.VehicleNavigationList.initializeBase(this);
        };
        return FirstLook.Pricing.Website.VehicleNavigationList.__instance;
    };
    FirstLook.Pricing.Website.VehicleNavigationList.get_instance = function() {
		return FirstLook.Pricing.Website.VehicleNavigationList.__instance || $create(FirstLook.Pricing.Website.VehicleNavigationList, null, null, null, null);
    };
    FirstLook.Pricing.Website.VehicleNavigationList.prototype = {
        initialize: function() {
            this.set_items([]);
            this.set_last_menu_vpa_link(document.getElementById('LastVPALink'));
            // Ugly I know, just following previous patterns of the IPA..., PM
            this.find_current_item();
            this.highlight_active_row();
            FirstLook.Pricing.Website.VehicleNavigationList.callBaseMethod(this, 'initialize');
        },
        updated: function() {
        	this.add_propertyChanged(this.getDelegate("onPropertyChanged"));
        	FirstLook.Pricing.Website.VehicleNavigationList.callBaseMethod(this, 'updated');
        },
        dispose: function() {
        	this.remove_propertyChanged(this.getDelegate("onPropertyChanged"));
        	delete this._lengthListeners;
        	FirstLook.Pricing.Website.VehicleNavigationList.callBaseMethod(this, 'dispose');
        },
        find_current_item: function() {
            var input = this.get_last_menu_vpa_link(),
            inputValue = input.value,
            items = this.get_items(),
            shouldContinue = true;
            for (var i = items.length - 1; i >= 0; i--) {
                if (shouldContinue && items[i].url.indexOf(inputValue) != -1) {
                    this.set_active_index(i);
                    shouldContinue = false;
                }
            };
        },
        update_active_item_from_location: function(location, trigger_action) {
        	var items = this.get_items(), 
        		in_list = false, 
        		active_item_changed = false;
			this.set_active_index(-1);
			location = location || "";
            for (var i = items.length - 1; i >= 0; i--) {
                if (items[i].get_url().indexOf(location) != -1) {
                	if (this.get_active_index() != i) {
                		this.set_active_index(i);
                		active_item_changed = true;
                	};                    
                    in_list = true;
                } 
                items[i].unhighlight_row();
            };
            if (!!in_list) {
            	this.highlight_active_row(active_item_changed && trigger_action);
            }
            return in_list;
        },
        update_active_item: function(item) {
            var items = this.get_items();
            for (var i = items.length - 1; i >= 0; i--) {
                if (items[i] == item) {
                    this.set_active_index(i);
                };
                items[i].unhighlight_row();
            };
            this.highlight_active_row(true);
        },
        highlight_active_row: function(take_action) {
            var item = this.get_item(this.get_active_index());
            if ( !! item) {
            	item.highlight_row(!!take_action);
            };
        },
        get_item: function(idx) {
            return this.get_items()[idx];
        },
        add: function(item) {
        	var items = this.get_items();
        	var is_unique = true;
        	for (var i=0; i < items.length; i++) {
        		if (items[i].get_url() === item.get_url()) {
        			is_unique = false;
        		};
        	};
        	if (is_unique) {
        		items.push(item);
        		this.raisePropertyChanged("length");
        	};
        },
        remove: function(item) {
        	var items = this.get_items();
        	for (var i = items.length - 1; i >= 0; i--){
        		if (items[i] == item) {
        			items.splice(i,1);
        			break;
        		}
        	};
        	this.raisePropertyChanged("length");
        },
        get_length: function() {
            return this.get_items().length;
        },
        onPropertyChanged: function(sender, args) {
        	var property = args.get_propertyName();
        	
        	if (property == "length" && this._lengthListeners) {
        		for (var i = this._lengthListeners.length - 1; i >= 0; i--){
        			try {
        				this._lengthListeners[i].onLengthChange();
        			} catch (err) {
        				// Do Nothing, Race Conditions of windows closing
        			}
        		};
        	};
        },
        listenToLengthChange: function(obj) {
        	// ==================================================================================
        	// = Reduce Cross Window Chatter... and don't trust AJAX.NET crosswindow events, PM =
        	// ==================================================================================
        	if (!!!this._lengthListeners) {
        		this._lengthListeners = [];	
        	};
        	if (obj) {
        		this._lengthListeners.push(obj);
        	};
        },
        deafToLengthChange: function(obj) {
        	if (!!this._lengthListeners) {
        		for (var i = this._lengthListeners.length - 1; i >= 0; i--){
        			if (obj == this._lengthListeners[i]) {
        				this._lengthListeners.splice(i,1);
        				break;
        			}
        		};
        		if (this._lengthListeners.length == 0) {
        			delete this._lengthListeners;
        		};
        	};
        }
    };

    FirstLook.Utility.createGetterSetters(FirstLook.Pricing.Website.VehicleNavigationList,
    ["items",
    "active_index",
    "last_menu_vpa_link"]);
    FirstLook.Pricing.Website.VehicleNavigationList.registerClass('FirstLook.Pricing.Website.VehicleNavigationList', Sys.Component);


    // ========
    // = Item =
    // ========
    FirstLook.Pricing.Website.VehicleNavigationItem = function(element) {
        FirstLook.Pricing.Website.VehicleNavigationItem.initializeBase(this, [element]);
    };
    FirstLook.Pricing.Website.VehicleNavigationItem.prototype = {
        initialize: function() {
            this.get_vehicle_navigation_list().add(this);
            FirstLook.Pricing.Website.VehicleNavigationItem.callBaseMethod(this, "initialize");
        },
        updated: function() {
        	this.addHandler(this.get_element(), "click", this.getDelegate("onClick"));
        	FirstLook.Pricing.Website.VehicleNavigationItem.callBaseMethod(this, "updated");
        },
        dispose: function() {
        	this.get_vehicle_navigation_list().remove(this);
        	this.removeHandlers();
            this.clearDelegates();
            FirstLook.Pricing.Website.VehicleNavigationItem.callBaseMethod(this, "dispose");
        },
        get_row: function() {
            if ( !! !this._row) {
                this._row = Sys.UI.DomElement.findParentNode(this.get_element(), "tr");
            };
            return this._row;
        },
        highlight_row: function(take_action) {
        	if (this.get_row()) {
        		Sys.UI.DomElement.addCssClass(this.get_row(), "selected");
        	}
        	if (take_action) {
        		this.action();
        	};
        },
        action: function() {
        	var page_index = this.get_new_page_index();
        	if (this.get_grid_view_id() && page_index) {
        		if (FirstLook.Pricing.Website.VehicleNavigationItem.LastPageIndex !== page_index) {
        			FirstLook.Pricing.Website.VehicleNavigationItem.LastPageIndex = page_index;
        			__doPostBack(this.get_grid_view_id(), "Page$"+page_index);
        		};
            }
        },
        unhighlight_row: function() {
        	if (this.get_row()) {
        		Sys.UI.DomElement.removeCssClass(this.get_row(), "selected");
        	};
        },
        get_vehicle_navigation_list: function() {
            if ( !! !this._vehicle_navigation_list) {
                this._vehicle_navigation_list = FirstLook.Pricing.Website.VehicleNavigationList.get_instance();
            }
            return this._vehicle_navigation_list;
        },
        get_url: function() {
        	var el = this.get_element();
        	if (el && el.href) {
        		return el.href;
        	} else {
        		return this._url;
        	}
        },
        onClick: function(e) {
            this.get_vehicle_navigation_list().update_active_item(this);
        }
    };
    FirstLook.Utility.createGetterSetters(FirstLook.Pricing.Website.VehicleNavigationItem, ["text", "url", "tooltip","grid_view_id","new_page_index"]);
    FirstLook.Pricing.Website.VehicleNavigationItem.registerClass('FirstLook.Pricing.Website.VehicleNavigationItem', Sys.UI.Behavior);

    Sys.Application.notifyScriptLoaded();
};