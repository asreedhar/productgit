if ( !! Sys) {
    Type.registerNamespace('FirstLook.Pricing.Website');
    FirstLook.Pricing.Website.VehicleNavigator = function(element) {
        FirstLook.Pricing.Website.VehicleNavigator.initializeBase(this, [element]);
    };
    FirstLook.Pricing.Website.VehicleNavigator.prototype = {
        initialize: function() {
            FirstLook.Pricing.Website.VehicleNavigator.callBaseMethod(this, 'initialize');
        },
        updated: function() {
            this.find_vehicle_navigation_list();
            var list = this.get_vehicle_navigation_list();
            if ( !! list) {
                list.listenToLengthChange(this);
                this.update_active_item_from_location();
                this.populate_prev_link();
                this.populate_next_link();
            };
            FirstLook.Pricing.Website.VehicleNavigator.callBaseMethod(this, 'updated');
        },
        dispose: function() {
            var list = this.get_vehicle_navigation_list();
            if ( !! list) {
                list.update_active_item_from_location();
                list.deafToLengthChange(this);
            }
            this.removeHandlers();
            this.clearDelegates();
            FirstLook.Pricing.Website.VehicleNavigator.callBaseMethod(this, 'dispose');
        },
        get_prev_item: function() {
            if ( !! !this._prev_item) {
                var el = this.get_prev_link(),
                list = this.get_vehicle_navigation_list(),
                index = list.get_active_index();
                if (index == 0) {
                    return;
                };
                this._prev_item = list.get_item(index - 1);
            };
            return this._prev_item;
        },
        get_next_item: function() {
            if ( !! !this._next_item) {
                var el = this.get_prev_link(),
                list = this.get_vehicle_navigation_list(),
                index = list.get_active_index(),
                length = list.get_length();
                if (index == length - 1) {
                    return;
                };
                this._next_item = list.get_item(index + 1);
            };
            return this._next_item;
        },
        populate_next_link: function() {
            var item = this.get_next_item(),
            el = this.get_next_link();

            if ( !! item && !!el) {
                if (this.__next_span) {
                    el.removeChild(this.__next_span);
                    delete this.__next_span;
                }
                this.__next_link = this.populate_link(item, el, "Next ");
                this.addHandler(el, 'click', this.getDelegate('onNextLinkClick'));
            } else if ( !! el && !!!this.__next_span) {
                this.removeHandler(el, 'click', this.getDelegate('onNextLinkClick'));
                if (this.__next_link) {
                    el.removeChild(this.__next_link);
                    delete this.__next_link;
                };
                this.__next_span = document.createElement("SPAN");
                Sys.UI.DomElement.setTextValue(this.__next_span, "LAST CAR");
                el.appendChild(this.__next_span);
            }
        },
        populate_prev_link: function() {
            var item = this.get_prev_item(),
            el = this.get_prev_link();

            if ( !! item && !!el) {
                if (this.__prev_span) {
                    el.removeChild(this.__prev_span);
                    delete this.__prev_span;
                }
                this.__prev_link = this.populate_link(item, el, "Previous ");
                this.addHandler(el, 'click', this.getDelegate('onPrevLinkClick'));
            } else if ( !! el && !!!this.__prev_span) {
                this.removeHandler(el, "click", this.getDelegate('onPrevLinkClick'));
                if (this.__prev_link) {
                    el.removeChild(this.__prev_link);
                    delete this.__prev_link;
                };
                this.__prev_span = document.createElement("SPAN");
                Sys.UI.DomElement.setTextValue(this.__prev_span, "FIRST CAR");
                el.appendChild(this.__prev_span);
            }
        },
        populate_link: function(item, link, prefix) {
            var links = link.getElementsByTagName("A"),
            hasLinks = links.length > 0,
            a = hasLinks ? links[0] : document.createElement("A"),
            url = item.get_url();

            if (a.href != url) {
                Sys.UI.DomElement.setTextValue(a, prefix + item.get_text());
                a.href = item.get_url().replace(/.*(\?.*)/, window.location.pathname + "$1");
                a.title = item.get_tooltip();
                a.target = window.name;
            };

            link.style.display = "";

            if (!hasLinks) {
                link.appendChild(a);
            };

            return a;
        },
        hide_links: function() {
            this.get_prev_link().style.display = "none";
            this.get_next_link().style.display = "none";
        },
        get_prev_link: function() {
            if ( !! !this._prev_link) {
                this._prev_link = this.find_el_by_className(this.get_prev_class());
            };
            return this._prev_link;
        },
        get_next_link: function() {
            if ( !! !this._next_link) {
                this._next_link = this.find_el_by_className(this.get_next_class());
            };
            return this._next_link;
        },
        find_el_by_className: function(className) {
            var el = this.get_element(),
            els = el.getElementsByTagName('*');
            for (var i = els.length - 1; i >= 0; i--) {
                if (els[i].className.indexOf(className) != -1) {
                    return els[i];
                };
            };
        },
        update_active_item_from_location: function(href) {
            return this.get_vehicle_navigation_list().update_active_item_from_location(href || window.location.search.toString(), !!href);
        },
        onPrevLinkClick: function(e) {
            if ( !! FirstLook.Page.ProgressManager) {
                FirstLook.Page.ProgressManager.get_instance().showAjaxProgress();
            };
            this.update_active_item_from_location(e.target.href);
        },
        onNextLinkClick: function(e) {
            if ( !! FirstLook.Page.ProgressManager) {
                FirstLook.Page.ProgressManager.get_instance().showAjaxProgress();
            };
            this.update_active_item_from_location(e.target.href);
        },
        find_vehicle_navigation_list: function() {
            if ( !! window.opener) {
                var o = window.opener;
                if ( !! o.Sys
                && !!o.FirstLook
                && !!o.FirstLook.Pricing
                && !!o.FirstLook.Pricing.Website
                && !!o.FirstLook.Pricing.Website.VehicleNavigationList
                && !!o.FirstLook.Pricing.Website.VehicleNavigationList.get_instance) {
                    this.set_vehicle_navigation_list(o.FirstLook.Pricing.Website.VehicleNavigationList.get_instance());
                };
            }
        },
        onLengthChange: function() {
            this._prev_item = undefined;
            this._next_item = undefined;
            if (this.update_active_item_from_location()) {
                this.populate_prev_link();
                this.populate_next_link();
            } else {
                this.hide_links();
            }
        }
    };
    FirstLook.Utility.createGetterSetters(FirstLook.Pricing.Website.VehicleNavigator, ["prev_class", "next_class", "next_prev_class", "vehicle_navigation_list"]);
    FirstLook.Pricing.Website.VehicleNavigator.registerClass('FirstLook.Pricing.Website.VehicleNavigator', Sys.UI.Behavior);

    Sys.Application.notifyScriptLoaded();
};