using System;
using System.Data;
using FirstLook.Common.Data;
using FirstLook.Pricing.DomainModel;

namespace PricingDomainModel_IntegrationTests.ConsumerHighlights
{
    /// <summary>
    /// Helper function for consumer highlight integration tests in finding appropriate owner and vehicle handles.
    /// </summary>
    internal static class HandleHelper
    {
        /// <summary>
        /// Get the handles of an owner and vehicle for whom no vehicle preferences already exist.
        /// </summary>
        /// <param name="ownerHandle">Owner handle. Output.</param>
        /// <param name="vehicleHandle">Vehicle handle. Output.</param>
        public static void GetOwnerAndVehicle_NoVehiclePreferenceExists(out string ownerHandle, out string vehicleHandle )
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        @"  SELECT	TOP 1 '1' + cast(i.inventoryid as varchar) as vh, o.handle as oh, i.inventoryid, o.ownerid
                            FROM	[IMT].dbo.Inventory I
                            JOIN	Market.Pricing.Owner o ON i.BusinessUnitId = o.OwnerEntityId
                            JOIN	[IMT].dbo.tbl_Vehicle V ON I.VehicleID = V.VehicleID
                            LEFT JOIN	[VehicleCatalog].Categorization.ModelConfiguration_VehicleCatalog MCVC ON V.VehicleCatalogID = MCVC.VehicleCatalogID
                            LEFT JOIN	[VehicleCatalog].Categorization.ModelConfiguration MC ON MCVC.ModelConfigurationID = MC.ModelConfigurationID
                            LEFT JOIN	[VehicleCatalog].Categorization.Model MM ON MC.ModelID = MM.ModelID
                            LEFT JOIN	[VehicleCatalog].Categorization.ModelConfiguration#Description MCD ON MCD.ModelConfigurationID = MC.ModelConfigurationID
                            LEFT JOIN	[VehicleCatalog].FirstLook.VehicleCatalog VC ON V.VehicleCatalogID = VC.VehicleCatalogID
                            LEFT JOIN	[VehicleCatalog].FirstLook.Line VL ON VC.LineID =VL.LineID
                            LEFT JOIN IMT.Marketing.ConsumerHighlightVehiclePreference vp ON vp.VehicleEntityId = i.InventoryId and o.OwnerId = vp.OwnerId
                            LEFT JOIN IMT.Marketing.ConsumerHighlight ch ON ch.ConsumerHighlightVehiclePreferenceId = vp.ConsumerHighlightVehiclePreferenceId
                            WHERE	I.InventoryActive = 1
                            AND	I.InventoryType = 2
                            AND	OwnerTypeID = 1
                            AND vp.ConsumerHighlightVehiclePreferenceId IS NULL";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            ownerHandle = reader.GetGuid(reader.GetOrdinal("oh")).ToString();
                            vehicleHandle = reader.GetString(reader.GetOrdinal("vh"));
                            return;
                        }
                    }
                }
            }

            throw new ApplicationException("no owner/vehicle handles found.");
        }        

        /// <summary>
        /// Get the handle of an owner that does not have any dealer preferences.
        /// </summary>
        /// <param name="ownerHandle">Owner handle. Output.</param>
        public static void GetOwner_NoDealerPreferenceExiats(out string ownerHandle)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        @"  SELECT	TOP 1 o.handle as oh
                            FROM [Market].[Pricing].[Owner] o
                            LEFT JOIN [IMT].[Marketing].[ConsumerHighlightPreference] p ON p.OwnerId = o.OwnerID
                            WHERE o.OwnerTypeID = 1
                            AND p.InsertUserId IS NULL";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            ownerHandle = reader.GetGuid(reader.GetOrdinal("oh")).ToString();                            
                            return;
                        }
                    }
                }
            }

            throw new ApplicationException("no owner handles found.");
            
        }

        /// <summary>
        /// Get the handles of an owner and vehicle which are tied to CarFax.
        /// </summary>
        /// <param name="ownerhandle">Owner handle. Output.</param>
        /// <param name="vehiclehandle">Vehicle handle. Output.</param>
        public static void GetCarfaxOwnerHandleAndVehicleHandle(out string ownerhandle, out string vehiclehandle)
        {

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        @"  SELECT	TOP 1 o.Handle as ownerHandle, '1' + cast(i.inventoryid as varchar) as vehiclehandle
                            FROM	IMT.Carfax.Account_Dealer A
                            JOIN	IMT.Carfax.Vehicle_Dealer D ON D.DealerID = A.DealerID
                            JOIN	IMT.Carfax.Vehicle V ON V.VehicleID = D.VehicleID
                            JOIN	IMT.Carfax.Request R ON R.RequestID = D.RequestID
                            JOIN	IMT.Carfax.Report E ON E.RequestID = R.RequestID
                            JOIN	imt.dbo.businessunit bu on bu.businessunitid = a.dealerid
                            JOIN	market.pricing.owner o on o.ownerentityid = a.dealerid and o.ownertypeid = 1
                            JOIN	imt.dbo.inventory i on i.businessunitid = a.dealerid
                            JOIN	imt.dbo.tbl_vehicle tv on tv.vin = v.vin and i.vehicleid = tv.vehicleid
                            WHERE	E.ExpirationDate >= GETDATE()
                            and i.inventoryactive = 1";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            ownerhandle = reader.GetGuid(reader.GetOrdinal("ownerHandle")).ToString();
                            vehiclehandle = reader.GetString(reader.GetOrdinal("vehiclehandle"));
                        }
                        else
                        {
                            const string msg = "No valid owner/vehicle combination could be found.";
                            Console.WriteLine(msg);
                            throw new ApplicationException(msg);
                        }
                    }

                }
            }

        }

        /// <summary>
        /// Get the handles of an owner and vehicle which are tied to CarFax and are One Owner.
        /// </summary>
        /// <param name="ownerhandle">Owner handle. Output.</param>
        /// <param name="vehiclehandle">Vehicle handle. Output.</param>
        /// <param name="hasOneOwner">Has Carfax One Owner?</param>
        public static void GetCarfaxOwnerHandleAndVehicleHandleWithOneOwner(out string ownerhandle, out string vehiclehandle, 
                                                                            bool hasOneOwner)
        {

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        @"  SELECT	TOP 1 o.Handle as ownerHandle, '1' + cast(i.inventoryid as varchar) as vehiclehandle
                            FROM	IMT.Carfax.Account_Dealer A
                            JOIN	IMT.Carfax.Vehicle_Dealer D ON D.DealerID = A.DealerID
                            JOIN	IMT.Carfax.Vehicle V ON V.VehicleID = D.VehicleID
                            JOIN	IMT.Carfax.Request R ON R.RequestID = D.RequestID
                            JOIN	IMT.Carfax.Report E ON E.RequestID = R.RequestID
		                    JOIN	IMT.Carfax.ReportInspectionResult RIR ON RIR.RequestID = R.RequestID
                            JOIN	imt.dbo.businessunit bu on bu.businessunitid = a.dealerid
                            JOIN	market.pricing.owner o on o.ownerentityid = a.dealerid and o.ownertypeid = 1
                            JOIN	imt.dbo.inventory i on i.businessunitid = a.dealerid
                            JOIN	imt.dbo.tbl_vehicle tv on tv.vin = v.vin and i.vehicleid = tv.vehicleid
                            WHERE	E.ExpirationDate >= GETDATE()
                            and i.inventoryactive = 1
		                    and RIR.ReportInspectionID = 1";

                    command.CommandText += hasOneOwner ? "and RIR.Selected = 1" : "and RIR.Selected <> 1";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            ownerhandle = reader.GetGuid(reader.GetOrdinal("ownerHandle")).ToString();
                            vehiclehandle = reader.GetString(reader.GetOrdinal("vehiclehandle"));
                        }
                        else
                        {
                            string msg = "No valid owner/vehicle combination could be found.";
                            Console.WriteLine(msg);
                            throw new ApplicationException(msg);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Get a dealer identifier and VIN for which there is a CarFax report.
        /// </summary>
        /// <param name="dealerid">Dealer identifier. Output.</param>
        /// <param name="vin">VIN. Output.</param>
        public static void GetCarfaxDealerIdAndVin(out int dealerid, out string vin)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        @"  SELECT	TOP 1 A.DealerId, V.VIN
                            FROM	IMT.Carfax.Account_Dealer A
                            JOIN	IMT.Carfax.Vehicle_Dealer D ON D.DealerID = A.DealerID
                            JOIN	IMT.Carfax.Vehicle V ON V.VehicleID = D.VehicleID
                            JOIN	IMT.Carfax.Request R ON R.RequestID = D.RequestID
                            JOIN	IMT.Carfax.Report E ON E.RequestID = R.RequestID
                            JOIN	imt.dbo.businessunit bu on bu.businessunitid = a.dealerid
                            JOIN	market.pricing.owner o on o.ownerentityid = a.dealerid and o.ownertypeid = 1
                            WHERE	E.ExpirationDate >= GETDATE()";

                    IDataReader reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        dealerid = reader.GetInt32(reader.GetOrdinal("DealerId"));
                        vin = reader.GetString(reader.GetOrdinal("VIN"));
                    }
                    else
                    {
                        Console.WriteLine("No valid dealerid/vin combination could be found.");
                        throw new ApplicationException("No valid dealerid/vin combination could be found.");
                    }

                }
            }
        }
    }
}