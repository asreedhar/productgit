/*
using System;
using System.Collections.Generic;
using System.Data;
using Autofac;
using Csla;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Data;
using FirstLook.Pricing.DomainModel;
using FirstLook.Pricing.DomainModel.Marketing.ConsumerHighlights;
using FirstLook.Pricing.DomainModel.Marketing.ConsumerHighlights.Types;
using FirstLook.Pricing.DomainModel.Marketing.ConsumerHighlights.Types.TransferObjects;
using NUnit.Framework;
using PricingDomainModel_Test;

namespace PricingDomainModel_IntegrationTests.ConsumerHighlights
{
    /// <summary>
    /// Integration tests for consumer highlights vehicle preference data access.
    /// </summary>
    [TestFixture]
    public class ConsumerHighlightsVehiclePreferenceDataAccessIntegrationTests
    {                
        private IConsumerHighlightsVehiclePreferenceDataAccess _dataAccess;

        private ILogger _logger;

        /// <summary>
        /// Set up the test fixture as a whole. Registers mappings with the registry.
        /// </summary>
        [TestFixtureSetUp]
        public void Setup()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<ConsumerHighlightsDealerPreferenceDataAccess>().As<IConsumerHighlightsDealerPreferenceDataAccess>();
            builder.RegisterType<ConsumerHighlightsVehiclePreferenceDataAccess>().As<IConsumerHighlightsVehiclePreferenceDataAccess>();
            builder.RegisterType<ConsoleLogger>().As<ILogger>();
            Registry.RegisterContainer(builder.Build()); 
        }

        /// <summary>
        /// Clean up the test fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TearDown()
        {
            // DELETE EVERYTHING.
            // DeleteAllCertifiedBenefitsData();
        }

        /// <summary>
        /// Setup work done prior to every test.
        /// </summary>
        [SetUp]
        public void TestSetup()
        {
            _dataAccess = Registry.Resolve<IConsumerHighlightsVehiclePreferenceDataAccess>();            
        }

        /// <summary>
        /// Test that data access returns false for an exists check on an owner and vehicle with no preferences.
        /// </summary>
        [Test]
        public void ExistsReturnsFalse()
        {
            string oh;
            string vh;
            HandleHelper.GetOwnerAndVehicle_NoVehiclePreferenceExists(out oh, out vh);

            bool exists = _dataAccess.Exists(oh, vh);
            Assert.IsFalse(exists);
        }

        /// <summary>
        /// Create a new vehicle preference and test that it gets an identifier, signalling insertion was successful.
        /// </summary>
        [Test]
        public void InsertPreference()
        {
            string oh;
            string vh;
            HandleHelper.GetOwnerAndVehicle_NoVehiclePreferenceExists(out oh, out vh);
            
            int vehiclePreferenceId = CreateVehiclePreference(oh, vh);            
            Assert.IsTrue(vehiclePreferenceId > 0);
        }

        /// <summary>
        /// Create a new vehicle preference and then test that checking if it exists returns true.
        /// </summary>
        [Test]
        public void ExistsReturnsTrue()
        {
            string oh;
            string vh;
            HandleHelper.GetOwnerAndVehicle_NoVehiclePreferenceExists(out oh, out vh);
            
            int vehiclePreferenceId = CreateVehiclePreference(oh, vh);            
            Assert.IsTrue(vehiclePreferenceId > 0);
            
            bool exists = _dataAccess.Exists(oh, vh);            
            Assert.IsTrue(exists);
        }

        /// <summary>
        /// Tests that creating a new preference, fetching it, changing some values on it, and updating it all work.
        /// </summary>
        [Test]
        public void InsertAndUpdatePreference()
        {
            string oh;
            string vh;
            HandleHelper.GetOwnerAndVehicle_NoVehiclePreferenceExists(out oh, out vh);
           
            // Create a new preference.
            int vehiclePreferenceId = CreateVehiclePreference(oh, vh);            
            Assert.IsTrue(vehiclePreferenceId > 0);

            // Fetch the newly created preference, and change some values.
            ConsumerHighlightsVehiclePreferenceTO to = _dataAccess.Fetch(oh, vh);
            to.IncludeCarfaxOneOwnerIcon = !to.IncludeCarfaxOneOwnerIcon;
            to.IsDisplayed = !to.IsDisplayed;

            // Update the preference.
            _dataAccess.Update(to, "admin");

            // Fetch the preference we just updated.
            ConsumerHighlightsVehiclePreferenceTO to2 = _dataAccess.Fetch(oh, vh);

            Assert.AreEqual(to.ConsumerHighlightVehiclePreferenceId, to2.ConsumerHighlightVehiclePreferenceId);
            Assert.AreEqual(to.OwnerHandle, to2.OwnerHandle);
            Assert.AreEqual(to.VehicleHandle, to2.VehicleHandle);
            Assert.AreEqual(to.IsDisplayed, to2.IsDisplayed);
            Assert.AreEqual(to.IncludeCarfaxOneOwnerIcon, to2.IncludeCarfaxOneOwnerIcon);

        }

        /// <summary>
        /// Test that creating a new preference and deleting it works.
        /// </summary>
        [Test]
        public void InsertAndDeletePreference()
        {
            string oh;
            string vh;
            HandleHelper.GetOwnerAndVehicle_NoVehiclePreferenceExists(out oh, out vh);

            // Create a new preference.
            int vehiclePreferenceId = CreateVehiclePreference(oh, vh);
            Assert.IsTrue(vehiclePreferenceId > 0);

            // Fetch the newly created preference.
            ConsumerHighlightsVehiclePreferenceTO to = _dataAccess.Fetch(oh, vh);

            // Now delete it and check it no longer exists.
            _dataAccess.Delete(to, "admin");
            bool exists = _dataAccess.Exists(to.OwnerHandle, to.VehicleHandle);
            Assert.IsFalse(exists);
        }

        /// <summary>
        /// Test that inserting new preferences and adding a highlight to them works.
        /// </summary>
        [Test]
        public void InsertFetchAddHighlightSave()
        {
            string oh;
            string vh;
            HandleHelper.GetOwnerAndVehicle_NoVehiclePreferenceExists(out oh, out vh);

            // Create a new preference.
            int vehiclePreferenceId = CreateVehiclePreference(oh, vh);
            Assert.IsTrue(vehiclePreferenceId > 0);

            // Fetch the newly created preference.
            ConsumerHighlightsVehiclePreferenceTO to = _dataAccess.Fetch(oh, vh);
            ConsumerHighlightsDealerPreference dealerPreference =
                ConsumerHighlightsDealerPreference.GetOrCreateDealerPreference(oh);
            to.SectionOrdering = dealerPreference.HighlightSectionTypes;

            const string text = "how do you do?";
            const int rank = 1;
            
            // Add a highlight to the preference we just saved.
            to.FreeTextHighlights.Add(new FreeTextHighlightTO(0, to.ConsumerHighlightVehiclePreferenceId, rank, true, text));

            // Update it and refetch it.            
            _dataAccess.Update(to, "admin");
            ConsumerHighlightsVehiclePreferenceTO to2 = _dataAccess.Fetch(oh, vh);            

            // Check the highlight values.
            Assert.AreEqual(1, to2.FreeTextHighlights.Count);
            Assert.AreEqual(text, to2.FreeTextHighlights[0].Text);
            Assert.AreEqual(rank, to2.FreeTextHighlights[0].Rank);
            Assert.AreEqual(HighlightType.FreeText, to2.FreeTextHighlights[0].Provider);            
            Assert.AreEqual(false, to2.FreeTextHighlights[0].IsDeleted);
            Assert.AreEqual(true, to2.FreeTextHighlights[0].IsDisplayed);

            // Make sure the preferences exist in the database.
            bool exists = _dataAccess.Exists(to.OwnerHandle, to.VehicleHandle);
            Assert.IsTrue(exists);
        }

        /// <summary>
        /// Test that inserting new preferences and adding two highlights to them works.
        /// </summary>
        [Test]
        public void InsertFetchAdd2HighlightsSave()
        {
            string oh;
            string vh;
            HandleHelper.GetOwnerAndVehicle_NoVehiclePreferenceExists(out oh, out vh);

            // Create a new preference.
            int vehiclePreferenceId = CreateVehiclePreference(oh, vh);
            Assert.IsTrue(vehiclePreferenceId > 0);

            // Fetch the newly created preference.
            ConsumerHighlightsVehiclePreferenceTO to = _dataAccess.Fetch(oh, vh);
            ConsumerHighlightsDealerPreference dealerPreference =
                ConsumerHighlightsDealerPreference.GetOrCreateDealerPreference(oh);
            to.SectionOrdering = dealerPreference.HighlightSectionTypes;

            const string text = "how do you do?";
            const string text2 = "how do you do 2?";
            const int rank = 1;
            const int rank2 = 2;

            // Add a highlight to the preference we just saved.
            to.FreeTextHighlights.Add(new FreeTextHighlightTO(0, to.ConsumerHighlightVehiclePreferenceId, rank, true, text));
            to.FreeTextHighlights.Add(new FreeTextHighlightTO(0, to.ConsumerHighlightVehiclePreferenceId, rank2, true, text2));

            // Update it and refetch it.            
            _dataAccess.Update(to, "admin");
            ConsumerHighlightsVehiclePreferenceTO to2 = _dataAccess.Fetch(oh, vh);

            // Check the highlight values.
            Assert.AreEqual(2, to2.FreeTextHighlights.Count);
            Assert.AreEqual(text, to2.FreeTextHighlights[0].Text);
            Assert.AreEqual(rank, to2.FreeTextHighlights[0].Rank);
            Assert.AreEqual(HighlightType.FreeText, to2.FreeTextHighlights[0].Provider);
            Assert.AreEqual(false, to2.FreeTextHighlights[0].IsDeleted);
            Assert.AreEqual(true, to2.FreeTextHighlights[0].IsDisplayed);

            Assert.AreEqual(text2, to2.FreeTextHighlights[1].Text);
            Assert.AreEqual(rank2, to2.FreeTextHighlights[1].Rank);
            Assert.AreEqual(HighlightType.FreeText, to2.FreeTextHighlights[1].Provider);
            Assert.AreEqual(false, to2.FreeTextHighlights[1].IsDeleted);
            Assert.AreEqual(true, to2.FreeTextHighlights[1].IsDisplayed);

            // Make sure the preferences exist in the database.
            bool exists = _dataAccess.Exists(to.OwnerHandle, to.VehicleHandle);
            Assert.IsTrue(exists);
        }

        /// <summary>
        /// Test that inserting new preferences and adding two highlights then deleting one works.
        /// </summary>
        [Test]
        public void InsertFetchAdd2HighlightsSaveUpdate1HighlightDeleteTheOther()
        {
            string oh;
            string vh;
            HandleHelper.GetOwnerAndVehicle_NoVehiclePreferenceExists(out oh, out vh);

            // Create a new preference.
            int vehiclePreferenceId = CreateVehiclePreference(oh, vh);
            Assert.IsTrue(vehiclePreferenceId > 0);

            // Fetch the newly created preference.
            ConsumerHighlightsVehiclePreferenceTO to = _dataAccess.Fetch(oh, vh);
            ConsumerHighlightsDealerPreference dealerPreference =
                ConsumerHighlightsDealerPreference.GetOrCreateDealerPreference(oh);
            to.SectionOrdering = dealerPreference.HighlightSectionTypes;

            const string text = "how do you do?";
            const string text2 = "how do you do 2?";
            const int rank = 1;
            const int rank2 = 2;

            // Add a highlight to the preference we just saved.
            to.FreeTextHighlights.Add(new FreeTextHighlightTO(0, to.ConsumerHighlightVehiclePreferenceId, rank, true, text));
            to.FreeTextHighlights.Add(new FreeTextHighlightTO(0, to.ConsumerHighlightVehiclePreferenceId, rank2, true, text2));

            // Update it and refetch it.            
            _dataAccess.Update(to, "admin");
            ConsumerHighlightsVehiclePreferenceTO to2 = _dataAccess.Fetch(oh, vh);

            // Check the highlight values.
            Assert.AreEqual(2, to2.FreeTextHighlights.Count);
            Assert.AreEqual(text, to2.FreeTextHighlights[0].Text);
            Assert.AreEqual(rank, to2.FreeTextHighlights[0].Rank);
            Assert.AreEqual(HighlightType.FreeText, to2.FreeTextHighlights[0].Provider);
            Assert.AreEqual(false, to2.FreeTextHighlights[0].IsDeleted);
            Assert.AreEqual(true, to2.FreeTextHighlights[0].IsDisplayed);

            Assert.AreEqual(text2, to2.FreeTextHighlights[1].Text);
            Assert.AreEqual(rank2, to2.FreeTextHighlights[1].Rank);
            Assert.AreEqual(HighlightType.FreeText, to2.FreeTextHighlights[1].Provider);
            Assert.AreEqual(false, to2.FreeTextHighlights[1].IsDeleted);
            Assert.AreEqual(true, to2.FreeTextHighlights[1].IsDisplayed);

            // Make sure the preferences exist in the database.
            bool exists = _dataAccess.Exists(to.OwnerHandle, to.VehicleHandle);
            Assert.IsTrue(exists);            

            // Fetch the one we just updated and verified.
            ConsumerHighlightsVehiclePreferenceTO to3 = _dataAccess.Fetch(oh, vh);
            to3.SectionOrdering = dealerPreference.HighlightSectionTypes;

            // modify the second one
            to3.FreeTextHighlights[1].Text = "I am all alone";
            to3.FreeTextHighlights[1].Rank = 1;

            // mark the first one for deletion
            to3.FreeTextHighlights[0].IsDeleted = true;

            _dataAccess.Update(to3, "admin");

            // fetch the one we just updated and verified...
            ConsumerHighlightsVehiclePreferenceTO to4 = _dataAccess.Fetch(oh, vh);

            Assert.AreEqual(1, to4.FreeTextHighlights.Count);
            Assert.AreEqual(1, to4.FreeTextHighlights[0].Rank);
            Assert.IsTrue(to4.FreeTextHighlights[0].Text == "I am all alone");
            Assert.IsTrue(to4.FreeTextHighlights[0].Provider == HighlightType.FreeText);            
            Assert.IsFalse(to4.FreeTextHighlights[0].IsDeleted);
            Assert.IsTrue(to4.FreeTextHighlights[0].IsDisplayed);
        }

        /// <summary>
        /// Test some stuff or something. I don't know. I'm tired of writing these.
        /// </summary>
        [Test]
        public void InsertFetchAddVHRAndFreeTextHighlightsSaveUpdate2HighlightsDelete2Others()
        {
            string oh;
            string vh;
            HandleHelper.GetOwnerAndVehicle_NoVehiclePreferenceExists(out oh, out vh);

            // Create a new preference.
            int vehiclePreferenceId = CreateVehiclePreference(oh, vh);
            Assert.IsTrue(vehiclePreferenceId > 0);

            // Fetch the one we just added.
            ConsumerHighlightsVehiclePreferenceTO to = _dataAccess.Fetch(oh, vh);
            ConsumerHighlightsDealerPreference dealerPreference =
                ConsumerHighlightsDealerPreference.GetOrCreateDealerPreference(oh);
            to.SectionOrdering = dealerPreference.HighlightSectionTypes;

            const string text1 = "?how do you do?";
            const int rank1 = 1;
            const string text2 = "?I am fine.";
            const int rank2 = 2;
            const string text3 = "?Thanks for asking.";
            const int rank3 = 3;
            const string text4 = "?Leave me alone now.";
            const int rank4 = 4;

            //add a highlight to the preference we just saved
            to.FreeTextHighlights.Add(new FreeTextHighlightTO(0, to.ConsumerHighlightVehiclePreferenceId, rank1, true, text1));
            to.FreeTextHighlights.Add(new FreeTextHighlightTO(0, to.ConsumerHighlightVehiclePreferenceId, rank2, true, text2));
            to.FreeTextHighlights.Add(new FreeTextHighlightTO(0, to.ConsumerHighlightVehiclePreferenceId, rank3, true, text3));
            to.FreeTextHighlights.Add(new FreeTextHighlightTO(0, to.ConsumerHighlightVehiclePreferenceId, rank4, true, text4));
            
            to.VehicleHistoryHighlights.Add(new VehicleHistoryHighlightTO(0, to.ConsumerHighlightVehiclePreferenceId, rank1, true, DateTime.Now.AddYears(1), HighlightType.CarFax, text1, "4:1"));
            to.VehicleHistoryHighlights.Add(new VehicleHistoryHighlightTO(0, to.ConsumerHighlightVehiclePreferenceId, rank2, true, DateTime.Now.AddYears(1), HighlightType.CarFax, text2, "4:2"));
            to.VehicleHistoryHighlights.Add(new VehicleHistoryHighlightTO(0, to.ConsumerHighlightVehiclePreferenceId, rank3, true, DateTime.Now.AddYears(1), HighlightType.CarFax, text3, "4:3"));
            to.VehicleHistoryHighlights.Add(new VehicleHistoryHighlightTO(0, to.ConsumerHighlightVehiclePreferenceId, rank4, true, DateTime.Now.AddYears(1), HighlightType.CarFax, text4, "4:4"));
            to.VehicleHistoryHighlights.Add(new VehicleHistoryHighlightTO(0, to.ConsumerHighlightVehiclePreferenceId, rank4 + 1, true, DateTime.Now.AddYears(1), HighlightType.CarFax, text1 + text2, "4:5"));
            to.VehicleHistoryHighlights.Add(new VehicleHistoryHighlightTO(0, to.ConsumerHighlightVehiclePreferenceId, rank4 + 2, true, DateTime.Now.AddYears(1), HighlightType.CarFax, text2 + text4, "4:6"));

            to.VehicleHistoryHighlights.Add(new VehicleHistoryHighlightTO(0, to.ConsumerHighlightVehiclePreferenceId, rank1, true, DateTime.Now.AddYears(1), HighlightType.AutoCheck, text1, "5:1"));
            to.VehicleHistoryHighlights.Add(new VehicleHistoryHighlightTO(0, to.ConsumerHighlightVehiclePreferenceId, rank2, true, DateTime.Now.AddYears(1), HighlightType.AutoCheck, text2, "5:2"));
            to.VehicleHistoryHighlights.Add(new VehicleHistoryHighlightTO(0, to.ConsumerHighlightVehiclePreferenceId, rank3, true, DateTime.Now.AddYears(1), HighlightType.AutoCheck, text3, "5:3"));
            to.VehicleHistoryHighlights.Add(new VehicleHistoryHighlightTO(0, to.ConsumerHighlightVehiclePreferenceId, rank4, true, DateTime.Now.AddYears(1), HighlightType.AutoCheck, text4, "5:4"));
            to.VehicleHistoryHighlights.Add(new VehicleHistoryHighlightTO(0, to.ConsumerHighlightVehiclePreferenceId, rank4 + 1, true, DateTime.Now.AddYears(1), HighlightType.AutoCheck, text1 + text2, "5:5"));
            to.VehicleHistoryHighlights.Add(new VehicleHistoryHighlightTO(0, to.ConsumerHighlightVehiclePreferenceId, rank4 + 2, true, DateTime.Now.AddYears(1), HighlightType.AutoCheck, text2 + text4, "5:6"));

            // Update it.
            _dataAccess.Update(to, "admin");

            // Fetch the one we just added.
            ConsumerHighlightsVehiclePreferenceTO to2 = _dataAccess.Fetch(oh, vh);            
            to2.SectionOrdering = dealerPreference.HighlightSectionTypes;

            Assert.AreEqual(4, to2.FreeTextHighlights.Count);
            Assert.AreEqual(12, to2.VehicleHistoryHighlights.Count);
         
            bool exists = _dataAccess.Exists(to.OwnerHandle, to.VehicleHandle);
            Assert.IsTrue(exists);

            // Fetch the one we just updated and verified.
            ConsumerHighlightsVehiclePreferenceTO to3 = _dataAccess.Fetch(oh, vh);            
            to3.SectionOrdering = dealerPreference.HighlightSectionTypes;

            // Mark the first one for deletion.
            to3.FreeTextHighlights[0].IsDeleted = true;

            // Modify the second one.
            to3.FreeTextHighlights[1].Text = "?XXXXXXXXXXXXXX";
            to3.FreeTextHighlights[1].Rank = 1;

            // Modify the third one.
            to3.VehicleHistoryHighlights[0].Text = "?AAAAA";
            to3.VehicleHistoryHighlights[0].Rank = 1;

            _dataAccess.Update(to3, "admin");

            // fetch the one we just updated and verified...
            ConsumerHighlightsVehiclePreferenceTO to4 = _dataAccess.Fetch(oh, vh);            
            to4.SectionOrdering = dealerPreference.HighlightSectionTypes;

            // Make sure all the marked-for-deletion highlights were deleted.
            foreach (ConsumerHighlightTO h in to2.GetHighlights())
            {
                Assert.IsFalse(h.IsDeleted);
            }
        }

        /// <summary>
        /// Test the creation and deletion of a vehicle history report highlight (deletion should throw an exception).
        /// </summary>
        [Test]
        [ExpectedException(typeof(NotSupportedException))]
        public void InsertFetchAddVHRHighlightAndAttemptToDeleteVHRHighlight()
        {
            string oh;
            string vh;
            HandleHelper.GetOwnerAndVehicle_NoVehiclePreferenceExists(out oh, out vh);

            // Create a preference.
            int vehiclePreferenceId = CreateVehiclePreference(oh, vh);
            Assert.IsTrue(vehiclePreferenceId > 0);

            // Fetch the one we just added.
            ConsumerHighlightsVehiclePreferenceTO to = _dataAccess.Fetch(oh, vh);
            ConsumerHighlightsDealerPreference dealerPreference =
                ConsumerHighlightsDealerPreference.GetOrCreateDealerPreference(oh);
            to.SectionOrdering = dealerPreference.HighlightSectionTypes;

            const string text1 = "how do you do?";
            const int rank1 = 1;

            // Add a highlight to the preference we just saved.
            to.VehicleHistoryHighlights.Add(new VehicleHistoryHighlightTO(0, to.ConsumerHighlightVehiclePreferenceId, rank1, true, DateTime.Now.AddYears(1), HighlightType.CarFax, text1, "4:1"));

            // Update it.
            _dataAccess.Update(to, "admin");

            // Fetch the one we just added.
            ConsumerHighlightsVehiclePreferenceTO to2 = _dataAccess.Fetch(oh, vh);            
            to2.SectionOrdering = dealerPreference.HighlightSectionTypes;

            // Delete the highlight (should fail).
            to2.VehicleHistoryHighlights[0].IsDeleted = true;
            _dataAccess.Update(to2, "admin");

        }

        /// <summary>
        /// Test that the highlight that is saved is the same as the highlight that is retrieved.
        /// </summary>
        [Test]
        public void SaveAndGetAreEqual()
        {
            string oh;
            string vh;
            HandleHelper.GetOwnerAndVehicle_NoVehiclePreferenceExists(out oh, out vh);

            // Create a preference.
            int vehiclePreferenceId = CreateVehiclePreference(oh, vh);
            Assert.IsTrue(vehiclePreferenceId > 0);

            ConsumerHighlightsVehiclePreference preference =
                ConsumerHighlightsVehiclePreference.GetOrCreateConsumerHighlightsPreference(oh, vh);

            preference.CreateFreeTextHighlight(true, "daniels highlight", 1);
            ConsumerHighlightsVehiclePreference savedPref = preference.Save();
            ConsumerHighlightsVehiclePreference gotPref = ConsumerHighlightsVehiclePreference.GetOrCreateConsumerHighlightsPreference(oh, vh);

            Assert.AreEqual(savedPref, gotPref);
        }

        /// <summary>
        /// Test the creation, updating and deletion of highlights.
        /// </summary>
        [Test]
        public void CreateUpdateDeleteHighlights()
        {
            string oh;
            string vh;
            HandleHelper.GetOwnerAndVehicle_NoVehiclePreferenceExists(out oh, out vh);

            // Create a preference.
            int vehiclePreferenceId = CreateVehiclePreference(oh, vh);
            Assert.IsTrue(vehiclePreferenceId > 0);

            // Get the preference
            ConsumerHighlightsVehiclePreference preference =
                ConsumerHighlightsVehiclePreference.GetOrCreateConsumerHighlightsPreference(oh, vh);

            // Create a couple of highlights
            preference.CreateFreeTextHighlight(true, "daniel highlight", 1);
            preference.CreateFreeTextHighlight(true, "zoe highlight", 2);
            
            // Change a preference setting.
            bool includeBefore = preference.IncludeCarfaxOneOwnerIcon;
            preference.IncludeCarfaxOneOwnerIcon = !preference.IncludeCarfaxOneOwnerIcon;

            // Save and re-retrieve the preference.
            preference.Save();
            preference = ConsumerHighlightsVehiclePreference.GetOrCreateConsumerHighlightsPreference(oh, vh);

            // Preference should be clean.
            Assert.That(! preference.IsDirty);

            // We changed this...it should have persisted correctly.
            Assert.IsFalse(includeBefore == preference.IncludeCarfaxOneOwnerIcon);
            Assert.That(preference.HighlightsCount() == 2);

            // Now we will change some highlights.
            // Delete the first one.
            string key = preference.FreeTextHighlights[0].Key;
            preference.UpdateHighlight(key, true, "blah", 1, true);
            
            // Should be dirty now.
            Assert.That(preference.IsDirty);

            // Change the second one.
            key = preference.FreeTextHighlights[1].Key;
            bool hlIsDisplayedBefore = preference.FreeTextHighlights[1].IsDisplayed;
            preference.UpdateHighlight(key, false, "asdfasdf", 1, false);

            // Save.
            preference.Save();
            
            // Should be clean.
            Assert.That(!preference.IsDirty);

            preference = ConsumerHighlightsVehiclePreference.GetOrCreateConsumerHighlightsPreference(oh, vh);

            // We deleted one, should now have a single highlight
            Assert.That(preference.HighlightsCount() == 1);
            Assert.That(hlIsDisplayedBefore != preference.FreeTextHighlights[0].IsDisplayed);
        }

#if false // This test requires an active connection to the CarFax site.
        /// <summary>
        /// Test the retrieval of a CarFax report.
        /// </summary>
        [Test]
        public void GetCarFaxReport()
        {
            int dealerid;
            string vin;

            HandleHelper.GetCarfaxDealerIdAndVin(out dealerid, out vin);

            CarfaxReportTO carfaxReportTO = 
                new ConsumerHighlightsVehiclePreferenceDataAccess().GetCarfaxReport(dealerid, vin);

            Assert.IsNotNull(carfaxReportTO);

            foreach (CarfaxReportInspectionTO inspection in carfaxReportTO.Inspections)
            {
                Console.WriteLine("inspection :" + inspection.Name + ", selected:" + inspection.Selected.ToString());
            }
        }
#endif

        /// <summary>
        /// Test that getting a CarFax report returns highlights.
        /// </summary>
        [Test]
        public void GetCarfaxHighlightsReturnsHighlights()
        {
            string oh;
            string vh;

            HandleHelper.GetCarfaxOwnerHandleAndVehicleHandle(out oh, out vh);

            IList<VehicleHistoryHighlightTO> highlights = 
                new ConsumerHighlightsVehiclePreferenceDataAccess().GetCarfaxHighlights(oh,vh, 1);
            Assert.IsNotNull(highlights);

            foreach (VehicleHistoryHighlightTO highlight in highlights)
            {
                Console.WriteLine("highlight :" + highlight.Text + ", providerid:" + highlight.Provider);
            }
        }

        /// <summary>
        /// Make sure that getting the CarFax report only returns CarFax highlights.
        /// </summary>
        [Test]
        public void GetCarfaxHighlightsReturnsOnlyCarfaxHighlights()
        {
            string oh;
            string vh;

            HandleHelper.GetCarfaxOwnerHandleAndVehicleHandle(out oh, out vh);

            IList<VehicleHistoryHighlightTO> highlights = 
                new ConsumerHighlightsVehiclePreferenceDataAccess().GetCarfaxHighlights(oh,vh, 1);
            Assert.IsNotNull(highlights);

            foreach (VehicleHistoryHighlightTO highlight in highlights)
            {
                Console.WriteLine("highlight :" + highlight.Text + ", providerid:" + highlight.Provider);
                Assert.IsTrue(highlight.Provider == HighlightType.CarFax);
            }
        }

        /// <summary>
        /// Test that an exception is thrown when trying to get a CarFax report for a bogus owner.
        /// </summary>
        [Test]
        [ExpectedException(typeof(DataPortalException))]
        public void GetCarfaxHighlightsThrowsExceptionWithBadOwnerHandle()
        {
            string oh = Guid.NewGuid().ToString();
            string vh = string.Empty;
            
            // The random owner handle should cause a sqlexception to be raised in validation.
            new ConsumerHighlightsVehiclePreferenceDataAccess().GetCarfaxHighlights(oh, vh, 1);
        }

        /// <summary>
        /// Test that an exception is thrown when trying to get a CarFax report for a bogus vehicle.
        /// </summary>
        [Test]
        [ExpectedException(typeof(DataPortalException))]
        public void GetCarfaxHighlightsThrowsExceptionWithGoodOwnerBadVehicle()
        {
            string oh;
            string vh;

            HandleHelper.GetCarfaxOwnerHandleAndVehicleHandle(out oh, out vh);

            // Change the vehicle handle to something bogus.
            vh = "1-2147483648";    // -2147483648 = smallest value allowed for a sql integer

            new ConsumerHighlightsVehiclePreferenceDataAccess().GetCarfaxHighlights(oh, vh, 1);
        }


        /// <summary>
        /// Create and insert into the database a new consumer highlights vehicle preference for the owner and vehicle
        /// with the given handles.
        /// </summary>
        /// <param name="ownerHandle">Owner handle.</param>
        /// <param name="vehicleHandle">Vehicle handle.</param>
        /// <returns>Identifier of the new vehicle preference.</returns>
        private static int CreateVehiclePreference(string ownerHandle, string vehicleHandle)
        {            
            int consumerHighlightVehiclePreferenceId;

            ConsumerHighlightsVehiclePreferenceDataAccess dataAccess =
                new ConsumerHighlightsVehiclePreferenceDataAccess();

            ConsumerHighlightsDealerPreference dealerPreference = 
                ConsumerHighlightsDealerPreference.GetOrCreateDealerPreference(ownerHandle);
            
            ConsumerHighlightsVehiclePreferenceTO to = 
                new ConsumerHighlightsVehiclePreferenceTO(null, ownerHandle, vehicleHandle, false, true, DateTime.Now,
                                                          dealerPreference.HighlightSectionTypes, 
                                                          new List<CircleRatingHighlightTO>(), 
                                                          new List<FreeTextHighlightTO>(), 
                                                          new List<SnippetHighlightTO>(), 
                                                          new List<VehicleHistoryHighlightTO>());
            dataAccess.Insert(to, "admin");

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }


                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        @"SELECT a.ConsumerHighlightVehiclePreferenceId
                        FROM IMT.Marketing.ConsumerHighlightVehiclePreference a
                        INNER JOIN Market.Pricing.Owner b ON b.OwnerId = a.OwnerId
                        INNER JOIN IMT.dbo.Inventory i ON i.InventoryId = a.VehicleEntityId and a.VehicleEntityTypeID = 1
                        WHERE b.Handle = '" +
                        ownerHandle + "' AND a.VehicleEntityID = " +
                        vehicleHandle.Substring(1, vehicleHandle.Length - 1);
                    object result = command.ExecuteScalar();
                    consumerHighlightVehiclePreferenceId = result == null ? 0 : Int32.Parse(result.ToString());
                }
            }
            return consumerHighlightVehiclePreferenceId;
        }               


    }

}
*/
