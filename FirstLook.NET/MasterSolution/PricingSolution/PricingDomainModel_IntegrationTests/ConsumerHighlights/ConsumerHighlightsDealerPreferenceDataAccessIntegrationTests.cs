﻿using System;
using System.Data;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types.TransferObjects;
using FirstLook.Pricing.DomainModel;
using NUnit.Framework;

namespace PricingDomainModel_IntegrationTests.ConsumerHighlights
{
    /// <summary>
    /// Integration tests for consumer highlights dealer preference data access.
    /// </summary>
    [TestFixture]
    public class ConsumerHighlightsDealerPreferenceDataAccessIntegrationTests
    {
        private readonly ConsumerHighlightsDealerPreferenceDataAccess _dataAccess = 
            new ConsumerHighlightsDealerPreferenceDataAccess();        

        /// <summary>
        /// Test that creating a dealer preference works.
        /// </summary>
        [Test]
        public void CreateTest()
        {
            string ownerHandle;
            HandleHelper.GetOwner_NoDealerPreferenceExiats(out ownerHandle);

            ConsumerHighlightsDealerPreferenceTO preference = _dataAccess.Create(ownerHandle);

            Assert.IsNotNull(preference);
            Assert.AreEqual(ownerHandle, preference.OwnerHandle);
        }

        /// <summary>
        /// Test that checking the existence of dealer preferences we know to exist works.
        /// </summary>
        [Test]
        public void ExistsTest()
        {
            string ownerHandle;
            HandleHelper.GetOwner_NoDealerPreferenceExiats(out ownerHandle);

            ConsumerHighlightsDealerPreferenceTO preference = _dataAccess.Create(ownerHandle);
            _dataAccess.Save(preference);
            
            Assert.IsTrue(_dataAccess.Exists(ownerHandle));
        }

        /// <summary>
        /// Test that checking the existence of a dealer preference we know doesn't exist fails.
        /// </summary>
        [Test]
        public void ExistsFailTest()
        {
            string ownerHandle;
            HandleHelper.GetOwner_NoDealerPreferenceExiats(out ownerHandle);            

            Assert.IsFalse(_dataAccess.Exists(ownerHandle));
        }

        /// <summary>
        /// Test that fetching dealer preferences works as expected.
        /// </summary>
        [Test]
        public void FetchTest()
        {
            string ownerHandle;
            HandleHelper.GetOwner_NoDealerPreferenceExiats(out ownerHandle);

            ConsumerHighlightsDealerPreferenceTO preference = _dataAccess.Create(ownerHandle);
            _dataAccess.Save(preference);

            ConsumerHighlightsDealerPreferenceTO preference2 = _dataAccess.Fetch(ownerHandle);

            Assert.AreEqual(preference.OwnerHandle, preference2.OwnerHandle);
        }

        /// <summary>
        /// Test that trying to fetch preferences which do not exist fails.
        /// </summary>
        [Test]
        [ExpectedException(typeof(ApplicationException))]
        public void FetchFailTest()
        {
            string ownerHandle;
            HandleHelper.GetOwner_NoDealerPreferenceExiats(out ownerHandle);

            _dataAccess.Fetch(ownerHandle);            
        }

        /// <summary>
        /// Test that saving preferences works.
        /// </summary>
        [Test]
        public void SaveTest()
        {
            string ownerHandle;
            HandleHelper.GetOwner_NoDealerPreferenceExiats(out ownerHandle);

            ConsumerHighlightsDealerPreferenceTO preference = _dataAccess.Create(ownerHandle);
            _dataAccess.Save(preference);

            double minJdPowerCircleRating;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        @"  SELECT p.UpdateDate, p.MinimumJDPowerCircleRating
                            FROM [IMT].[Marketing].[ConsumerHighlightPreference] p
                            JOIN [Market].[Pricing].[Owner] o on o.OwnerID = p.OwnerId
                            WHERE o.handle = '" + ownerHandle + @"'";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        Assert.IsTrue(reader.Read());
                        Assert.IsTrue(reader.IsDBNull(reader.GetOrdinal("UpdateDate")));                        

                        minJdPowerCircleRating = reader.GetDouble(reader.GetOrdinal("MinimumJDPowerCircleRating"));
                    }
                }
            }

            preference.MinimumJdPowerCircleRating = minJdPowerCircleRating - 0.5;
            if (preference.MinimumJdPowerCircleRating < 0.0)
            {
                preference.MinimumJdPowerCircleRating = 5.0;
            }
            _dataAccess.Save(preference);

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        @"  SELECT p.UpdateDate, p.MinimumJDPowerCircleRating 
                            FROM [IMT].[Marketing].[ConsumerHighlightPreference] p
                            JOIN [Market].[Pricing].[Owner] o on o.OwnerID = p.OwnerId
                            WHERE o.handle = '" + ownerHandle + @"'";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        Assert.IsTrue(reader.Read());
                        Assert.IsFalse(reader.IsDBNull(reader.GetOrdinal("UpdateDate")));
                        Assert.AreEqual(preference.MinimumJdPowerCircleRating, reader.GetDouble(reader.GetOrdinal("MinimumJDPowerCircleRating")));
                    }
                }
            }
        }
    }
}
