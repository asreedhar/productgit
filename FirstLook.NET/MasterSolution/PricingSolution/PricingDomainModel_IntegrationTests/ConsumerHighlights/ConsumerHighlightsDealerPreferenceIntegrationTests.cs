﻿using System.Data;
using Autofac;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types;
using FirstLook.Pricing.DomainModel;
using NUnit.Framework;

namespace PricingDomainModel_IntegrationTests.ConsumerHighlights
{
    /// <summary>
    /// Integration tests for consumer highlights dealer preferences.
    /// </summary>
    [TestFixture]
    public class ConsumerHighlightsDealerPreferenceIntegrationTests
    {
        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<ConsumerHighlightsDealerPreferenceDataAccess>().As<IConsumerHighlightsDealerPreferenceDataAccess>();
            Registry.RegisterContainer(builder.Build());
        }

        /// <summary>
        /// Test that dealer preferences are successfully created when given the handle of an owner that does not 
        /// already have dealer preferences.
        /// </summary>
        [Test]
        public void CreationTest()
        {
            string ownerHandle;
            HandleHelper.GetOwner_NoDealerPreferenceExiats(out ownerHandle);

            ConsumerHighlightsDealerPreference preference = 
                ConsumerHighlightsDealerPreference.GetOrCreateDealerPreference(ownerHandle);

            Assert.IsNotNull(preference);

        }

        /// <summary>
        /// Test that fetching preferences work by creating new ones, saving them and then re-retrieving them.
        /// </summary>
        [Test]
        public void GetTest()
        {
            string ownerHandle;
            HandleHelper.GetOwner_NoDealerPreferenceExiats(out ownerHandle);

            ConsumerHighlightsDealerPreference preference =
                ConsumerHighlightsDealerPreference.GetOrCreateDealerPreference(ownerHandle);

            preference.Save();

            ConsumerHighlightsDealerPreference preference2 =
                ConsumerHighlightsDealerPreference.GetOrCreateDealerPreference(ownerHandle);

            Assert.AreEqual(preference.OwnerHandle, preference2.OwnerHandle);
        }

        /// <summary>
        /// Test that saving dealer preferences work by creating new preferences, saving them to the database and then
        /// querying to make sure they exist.
        /// </summary>
        [Test]
        public void SaveTest()
        {
            string ownerHandle;
            HandleHelper.GetOwner_NoDealerPreferenceExiats(out ownerHandle);

            ConsumerHighlightsDealerPreference preference =
                ConsumerHighlightsDealerPreference.GetOrCreateDealerPreference(ownerHandle);

            preference.Save();

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        @"  SELECT 1 
                            FROM [IMT].[Marketing].[ConsumerHighlightPreference] p
                            JOIN [Market].[Pricing].[Owner] o on o.OwnerID = p.OwnerId
                            WHERE o.handle = '" + ownerHandle + @"'";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        Assert.IsTrue(reader.Read());
                    }
                }
            }
        }        
    }
}
