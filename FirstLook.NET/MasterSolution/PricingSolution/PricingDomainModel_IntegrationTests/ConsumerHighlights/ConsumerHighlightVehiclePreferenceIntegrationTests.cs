using Autofac;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights;
using FirstLook.Merchandising.DomainModel.Marketing.ConsumerHighlights.Types;
using NUnit.Framework;
using PricingDomainModel_Test;

namespace PricingDomainModel_IntegrationTests.ConsumerHighlights
{
    /// <summary>
    /// Consumer highlights vehicle preference integration tests.
    /// </summary>
    [TestFixture]
    public class ConsumerHighlightVehiclePreferenceIntegrationTests
    {
        /// <summary>
        /// Setup work for this test fixture. Registers mappings with the registry.
        /// </summary>
        [TestFixtureSetUp]
        public void Setup()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<ConsumerHighlightsDealerPreferenceDataAccess>().As<IConsumerHighlightsDealerPreferenceDataAccess>();
            builder.RegisterType<ConsumerHighlightsVehiclePreferenceDataAccess>().As<IConsumerHighlightsVehiclePreferenceDataAccess>();
            builder.RegisterType<ConsoleLogger>().As<ILogger>();
            Registry.RegisterContainer(builder.Build());
        }

        /// <summary>
        /// Can preferences be created for an owner and vehicle for whom no preferences already exist?
        /// </summary>
        [Test]
        public void CanGetPreference()
        {
            string oh;
            string vh;

            HandleHelper.GetOwnerAndVehicle_NoVehiclePreferenceExists(out oh, out vh);

            ConsumerHighlightsVehiclePreference preference =
                ConsumerHighlightsVehiclePreference.GetOrCreateConsumerHighlightsPreference(oh, vh);

            Assert.IsNotNull(preference);
        }

#if false // These tests fail because they can't connect to VHR services.
        /// <summary>
        /// Can preferences be retrieved for an owner and vehicle with carfax associations? And are there vehicle
        /// history highlights with those preferences?
        /// </summary>
        [Test]
        public void CanGetPreferenceWithCarfaxHighlights()
        {
            string oh;
            string vh;
            HandleHelper.GetCarfaxOwnerHandleAndVehicleHandle(out oh, out vh);

            ConsumerHighlightsVehiclePreference preference =
                ConsumerHighlightsVehiclePreference.GetOrCreateConsumerHighlightsPreference(oh, vh);

            Assert.IsNotNull(preference);
            Assert.IsNotEmpty(UtilityMethods.GetListOfHighlights(preference.GetHighlights()));
        }         

        /// <summary>
        /// Test the handling of vehicle history highlights.
        /// </summary>
        [Test]
        public void TestVehicleHistoryHighlightKeyAssignment()
        {
            string oh;
            string vh;
            HandleHelper.GetCarfaxOwnerHandleAndVehicleHandle(out oh, out vh);

            ConsumerHighlightsVehiclePreference preference =
                ConsumerHighlightsVehiclePreference.GetOrCreateConsumerHighlightsPreference(oh, vh);

            // We want new preferences, so delete them if they already exist.
            if (!preference.IsNew)
            {
                preference.Delete();
                preference.Save();
                preference = ConsumerHighlightsVehiclePreference.GetOrCreateConsumerHighlightsPreference(oh, vh);
            }
            
            Assert.IsNotNull(preference);
            Assert.That(preference.IsNew);

            // Make sure there are vehicle history highlights.
            List<ConsumerHighlightTO> highlights = UtilityMethods.GetListOfHighlights(preference.GetHighlights());
            Assert.IsNotEmpty(highlights);

            // Every key should be unique in this collection.
            List<string> keys = new List<string>();
            foreach (ConsumerHighlightTO highlight in highlights)
            {                
                Console.WriteLine("highlight.key:" + highlight.Key);
                Assert.That(!keys.Contains(highlight.Key));
            }

            preference.Save();
            Console.WriteLine("highlights saved...");

            // We've already saved these prefs - they shouldn't be new.
            ConsumerHighlightsVehiclePreference preference2 = 
                ConsumerHighlightsVehiclePreference.GetOrCreateConsumerHighlightsPreference(oh, vh);
            Console.WriteLine("highlights fetched...");

            Assert.IsFalse(preference2.IsNew);

            // Make sure there are vehicle history highlights.
            highlights = UtilityMethods.GetListOfHighlights(preference2.GetHighlights());
            Assert.IsNotEmpty(highlights);

            // Every key should be unique in this collection.
            keys = new List<string>();
            foreach (ConsumerHighlightTO highlight in highlights)
            {                
                Console.WriteLine("highlight.key:" + highlight.Key);
                Assert.That(!keys.Contains(highlight.Key));
            }            
        }
#endif

#if false // This test only passes if a highlight has CARFAX 1-Owner...which can't be guarenteed.
        /// <summary>
        /// Test that preferences for a dealer with CarFax One Owner have the right settings.
        /// </summary>
        [Test]
        public void TestCarFaxOneOwnerHasProperXMLBuilt()
        {
            string oh;
            string vh;
            HandleHelper.GetCarfaxOwnerHandleAndVehicleHandleWithOneOwner(out oh, out vh, true);

            ConsumerHighlightsVehiclePreference preference =
                ConsumerHighlightsVehiclePreference.GetOrCreateConsumerHighlightsPreference(oh, vh);

            preference.IncludeCarfaxOneOwnerIcon = true;

            preference.Save();
            
            string xml = preference.AsXml();

            Assert.That(xml.IndexOf("<vehiclehistoryhighlights showCarFaxLogo=\"true\"") != -1);
        }
#endif

        [Test]
        public void TestCarFaxNotOneOwnerHasProperXMLBuilt()
        {
            string oh;
            string vh;
            HandleHelper.GetCarfaxOwnerHandleAndVehicleHandleWithOneOwner(out oh, out vh, false);

            ConsumerHighlightsVehiclePreference preference =
                ConsumerHighlightsVehiclePreference.GetOrCreateConsumerHighlightsPreference(oh, vh);

            preference.IncludeCarfaxOneOwnerIcon = true;

            preference.Save();

            string xml = preference.AsXml();

            Assert.That(xml.StartsWith("<consumerhighlights showCarFaxLogo=\"false\""));
        }
        /*
        [Test]
        public void CanAddAndDeleteHighlightToVehiclePreference()
        {
            // this test was created in response to FB Case 9106

            ConsumerHighlightsVehiclePreference preference = GetNewPreference();
            
            preference.CreateFreeTextHighlight(true, "abc", 1);            
            preference.Save();

            Assert.IsTrue(preference.Highlights.Count == preCount + 1);

            preCount = preference.Highlights.Count;

            string key = preference.Highlights[0].Key;
            bool isDisplayed = preference.Highlights[0].IsDisplayed;
            string text = preference.Highlights[0].Text;
            int rank = preference.Highlights[0].Rank;
            
            // delete the first highlight
            preference.UpdateHighlight(key, isDisplayed, text, rank, true);
            preference.Save();

            preference = ConsumerHighlightsVehiclePreference.GetOrCreateConsumerHighlightsPreference(preference.OwnerHandle, preference.VehicleHandle);

            Assert.IsTrue(preference.Highlights.Count == preCount - 1);

        }*/



        [TestFixtureTearDown]
        public void TearDown()
        {
            // DELETE EVERYTHING.
        }


        private ConsumerHighlightsVehiclePreference GetNewPreference()
        {
            string oh;
            string vh;

            HandleUtilities.GetFirstInventoryHandles(out oh, out vh);

            ConsumerHighlightsVehiclePreference preference = ConsumerHighlightsVehiclePreference.GetOrCreateConsumerHighlightsPreference(oh, vh);

            if (!preference.IsNew)
            {
                preference.Delete();
                preference.Save();
            }

            return ConsumerHighlightsVehiclePreference.GetOrCreateConsumerHighlightsPreference(oh, vh);
        }
    }
}