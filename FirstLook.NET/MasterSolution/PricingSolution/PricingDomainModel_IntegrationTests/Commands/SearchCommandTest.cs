﻿using Autofac;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Core.Registry;
using FirstLook.Merchandising.DomainModel;
using FirstLook.Pricing.DomainModel;
using FirstLook.Pricing.DomainModel.Commands.Impl;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes;
using NUnit.Framework;

namespace PricingDomainModel_IntegrationTests.Commands
{
    [TestFixture]
    public class SearchCommandTest
    {

        private const int OWNER_ENTITY_ID = 102274;
        private const int VEHICLE_ENTITY_ID = 27240414;
        private const int TYPE_ID = 1;
        private const string USER = "SearchCommandTest";

        [SetUp]
        public void Setup()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new CoreModule());
/*
            builder.RegisterModule(new OltpModule());
            builder.RegisterModule(new PhotoServicesModule());
*/

            new PricingDomainRegister().Register(builder);
            new MerchandisingDomainRegistry().Register(builder);
//            new PricingPresentersRegister().Register(builder);

            IContainer container = builder.Build();
            Registry.RegisterContainer(container);
//            _containerProvider = new ContainerProvider(container);

//            RegisterRoutes(RouteTable.Routes);

            //Added for fault reporting (health montioring) integration

            IRegistry registry = RegistryFactory.GetRegistry();

//            registry.Register<Fault.DomainModel.Module>();

            registry.Register<IDataSessionManager, DataSessionManager>(ImplementationScope.Shared);

            //Register command related classes
            registry.Register<FirstLook.Pricing.DomainModel.Commands.Module>();
//            registry.Register<ICommandExecutor, ListeningCommandExecutor>(ImplementationScope.Isolated);
        }

        [Test]
        public void Search()
        {
            SearchCommand sc = new SearchCommand();
            var results = sc.Execute(new SearchArgumentsDto()
                {
                    InsertUser = USER,
                    OwnerEntityId = OWNER_ENTITY_ID,
                    OwnerEntityTypeId = TYPE_ID,
                    VehicleEntityId = VEHICLE_ENTITY_ID,
                    VehicleEntityTypeId = TYPE_ID
                });

            Assert.IsNotNull(results);


        }
    }
}
