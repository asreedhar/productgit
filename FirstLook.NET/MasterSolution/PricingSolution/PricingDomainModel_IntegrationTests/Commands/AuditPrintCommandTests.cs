﻿using System;
using System.Data;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;
using Autofac;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Marketing.Commands;
using FirstLook.Pricing.DomainModel;
using NUnit.Framework;
using ICache = FirstLook.Common.Core.ICache;

namespace PricingDomainModel_IntegrationTests.Commands
{
    /// <summary>
    /// Test that an xml string and a byte stream are properly saved to the database via the AuditPrintCommand.
    /// </summary>
    [TestFixture]
    public class AuditPrintCommandTests
    {
        /// <summary>
        /// Setup this test fixture. Will register the cache and logger.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetup()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<DotNetCacheWrapper>().As<ICache>();
            builder.RegisterType<MockLogger>().As<ILogger>();
            Registry.RegisterContainer(builder.Build());
        }

        /// <summary>
        /// Private helper function to return the stream for a resource with the given name.
        /// </summary>
        /// <param name="resourceName">Name of the resource to read.</param>
        /// <returns>Stream of the resource.</returns>
        private static Stream GetResource(string resourceName)
        {                        
            Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName);

            if (stream == null)
            {
                Assert.Fail("Could not find required required resource with name " + resourceName + ".");
            }

            return stream;
        }

        /// <summary>
        /// Test that an xml string and a byte stream are properly saved to the database via the AuditPrintCommand.
        /// Will run the command and then query the database to verify the values stored are the ones we called the
        /// command with.
        /// </summary>
        [Test]
        public void AuditTest()
        {
            Stream stream;

            // Get the test xml resource.
            stream = GetResource("PricingDomainModel_IntegrationTests.Resources.PrintAuditTest.xml");            
            StreamReader streamReader = new StreamReader(stream);
            string auditXml = streamReader.ReadToEnd();

            // Get the test pdf resource.
            stream = GetResource("PricingDomainModel_IntegrationTests.Resources.PrintAuditTest.pdf");
            byte[] auditPdf = new byte[stream.Length];
            stream.Read(auditPdf, 0, (int)stream.Length);

            // Get the owner and vehicle handles.
            stream = GetResource("PricingDomainModel_IntegrationTests.Resources.ValidHandles.xml");
            XmlSerializer serializer = new XmlSerializer(typeof(ValidHandles));
            TextReader textReader = new StreamReader(stream);
            ValidHandles handles = (ValidHandles)serializer.Deserialize(textReader);            

            string ownerHandle   = handles.OwnerHandle;
            string vehicleHandle = handles.VehicleHandle;

            // Run the audit command.
            AuditPrintCommand auditCommand = new AuditPrintCommand(auditXml, ownerHandle, vehicleHandle);
            AbstractCommand.DoRun(auditCommand);

            // Query the database to verify the above were saved.
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT TOP 1 PrintXML " + 
                                          "FROM Marketing.PrintAudit " +
                                          "ORDER BY InsertDate DESC";

                    IDataReader reader = command.ExecuteReader();

                    if (reader.Read())
                    {
                        string fetchedXml = reader.GetString(reader.GetOrdinal("PrintXML"));
                        
                        Assert.AreEqual(auditXml, fetchedXml);
                    }
                    else
                    {
                        Assert.Fail("Nothing was audited to the database.");
                    }                    
                }
            }
        }    

        /// <summary>
        /// Test that passing in a null xml string and/or a null pdf byte stream don't blow the command up.
        /// </summary>
        [Test]
        public void NullDataTest()
        {
            try
            {
                // Get the owner and vehicle handles.
                Stream stream = GetResource("PricingDomainModel_IntegrationTests.Resources.ValidHandles.xml");
                XmlSerializer serializer = new XmlSerializer(typeof (ValidHandles));
                TextReader textReader = new StreamReader(stream);
                ValidHandles handles = (ValidHandles) serializer.Deserialize(textReader);

                string ownerHandle = handles.OwnerHandle;
                string vehicleHandle = handles.VehicleHandle;

                // Run the audit command.
                AuditPrintCommand auditCommand = new AuditPrintCommand(null, ownerHandle, vehicleHandle);
                AbstractCommand.DoRun(auditCommand);
            }
            catch (Exception)
            {
                Assert.Fail();
            }
        }

        /// <summary>
        /// Test that no exception bubbles up even on invalid handle input.
        /// </summary>
        [Test]
        public void InvalidHandlesTest()
        {
            try
            {
                Stream stream;

                // Get the test xml resource.
                stream = GetResource("PricingDomainModel_IntegrationTests.Resources.PrintAuditTest.xml");
                StreamReader streamReader = new StreamReader(stream);
                string auditXml = streamReader.ReadToEnd();

                // Run the audit command.
                AuditPrintCommand auditCommand = new AuditPrintCommand(auditXml, null, null);
                AbstractCommand.DoRun(auditCommand);
            }
            catch (Exception)
            {
                Assert.Fail("Exception bubbled up on invalid handle input.");                
            }            

            Assert.AreEqual(true, MockLogger.CalledLog);
        }

        #region Helper Types

        /// <summary>
        /// Helper class for reading in valid owner and vehicle handles from an xml resource.
        /// </summary>
        public class ValidHandles
        {
            public string OwnerHandle { get; set; }
            public string VehicleHandle { get; set; }
        }

        /// <summary>
        /// Mock of the logging mechanism.
        /// </summary>
        public class MockLogger : ILogger
        {
            public static bool CalledLog;

            public void Reset()
            {
                CalledLog = false;
            }

            public void Log(Exception e)
            {
                CalledLog = true;
            }

            public void Log(string message, object source)
            {
                CalledLog = true;
            }
        }

        #endregion
    }
}
