﻿using System;
using FirstLook.Pricing.DomainModel.Commands.Impl;
using NUnit.Framework;

namespace PricingDomainModel_IntegrationTests.Commands
{
    [TestFixture]
    public class HandleLookupCommandTests
    {
        private const int OWNER_ENTITY_ID = 102274;
        private const int VEHICLE_ENTITY_ID = 27190243;
        private const int TYPE_ID = 1;

        [Test]
        public void Lookup()
        {
            var c = new HandleLookupCommand();
            var results = c.Execute(OWNER_ENTITY_ID, TYPE_ID, VEHICLE_ENTITY_ID, TYPE_ID, "zbrown");

            Assert.IsNotNull(results);
            Console.WriteLine( "Owner Handle: " + results.OwnerHandle );
            Console.WriteLine( "Vehicle Handle: " + results.VehicleHandle );
            Console.WriteLine( "Search Handle: " + results.SearchHandle );
        }
    }
}
