﻿using System;
using FirstLook.Pricing.DomainModel.Commands.Impl;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes;
using NUnit.Framework;

namespace PricingDomainModel_IntegrationTests.Commands
{
    [TestFixture]
    public class VehiclePricingDecisionInputCommandTests
    {
        private const int OWNER_ENTITY_ID = 102274;
        private const int VEHICLE_ENTITY_ID = 27190243;
        private const int TYPE_ID = 1;

       
        [Test]
        public void SaveAndGet()
        {
            int recon = new Random(DateTime.Now.Millisecond).Next(100, 500);
            int gross = new Random(DateTime.Now.Millisecond).Next(500, 1000);

            CommandFactory factory = new CommandFactory();
            var save = factory.CreateSaveVehiclePricingDecisionInputCommand();

            VehiclePricingDecisionInputDto saveArg = new VehiclePricingDecisionInputDto();
            saveArg.OwnerEntityId = OWNER_ENTITY_ID;
            saveArg.OwnerEntityTypeId = TYPE_ID;
            saveArg.VehicleEntityId = VEHICLE_ENTITY_ID;
            saveArg.VehicleEntityTypeId = TYPE_ID;
            saveArg.EstimatedAdditionalCosts = recon;
            saveArg.TargetGrossProfit = gross;
            saveArg.UserId = "zbrown";

            save.Execute(saveArg);            

            // Now get
            var getCommand = factory.CreateGetVehiclePricingDecisionInputCommand();

            VehiclePricingDecisionInputDto getArg = new VehiclePricingDecisionInputDto();
            getArg.OwnerEntityId = OWNER_ENTITY_ID;
            getArg.OwnerEntityTypeId = TYPE_ID;
            getArg.VehicleEntityId = VEHICLE_ENTITY_ID;
            getArg.VehicleEntityTypeId = TYPE_ID;
            getArg.UserId = "zbrown";

            var result = getCommand.Execute(getArg);
            Assert.IsNotNull(result);

            Console.WriteLine("EstimatedAdditionalCosts: " + result.EstimatedAdditionalCosts);
            Console.WriteLine("OwnerEntityId: " + result.OwnerEntityId);
            Console.WriteLine("OwnerEntityTypeId: " + result.OwnerEntityTypeId);
            Console.WriteLine("TargetGrossProfit: " + result.TargetGrossProfit);
            Console.WriteLine("UserId: " + result.UserId);
            Console.WriteLine("VehicleEntityId: " + result.VehicleEntityId);
            Console.WriteLine("VehicleEntityTypeId: " + result.VehicleEntityTypeId);

            Assert.AreEqual(gross, result.TargetGrossProfit);
            Assert.AreEqual(recon, result.EstimatedAdditionalCosts);
        }
    }
}
