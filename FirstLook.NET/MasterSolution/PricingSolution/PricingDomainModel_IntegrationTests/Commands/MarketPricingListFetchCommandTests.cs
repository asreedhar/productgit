﻿using System;
using FirstLook.Pricing.DomainModel.Commands.Impl;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes;
using NUnit.Framework;

namespace PricingDomainModel_Test.Commands
{
    [TestFixture]
    public class MarketPricingListFetchCommandTests
    {
        [Test]
        public void Execute()
        {
            var c = new MarketPricingListFetchCommand();
            var dto = new MarketPricingListFetchArgumentsDto();
            dto.InsertUser = "zbrown";
            dto.OwnerEntityId = 102274;
            dto.OwnerEntityTypeId = 1;
            dto.VehicleEntityId = 27190243;
            dto.VehicleEntityTypeId = 1;
            dto.SearchType = SearchTypeDto.Precision;

            var results = c.Execute(dto);

            Console.WriteLine(results.PriceRanks.Count);
            Assert.That( results.PriceRanks.Count > 0);

        }
    }
}
