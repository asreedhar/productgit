﻿/*
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Pricing.DomainModel.Commands.Impl;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes;
using FirstLook.Pricing.DomainModel.Internet.DataSource;
using NUnit.Framework;

namespace PricingDomainModel_Test.Commands
{
    [TestFixture]
    public class MarketListingsFetchCommandTests
    {
        private const string INSERT_USER = "zbrown";
        private const int MAX_ROWS = 500;
        private const int OWNER_ENTITY_ID = 102225;
        private const int OWNER_ENTITY_TYPE_ID = 1;
        private const int VEHICLE_ENTITY_ID = 26165120;
        private const int VEHICLE_ENTITY_TYPE_ID = 1;

        [Test]
        [Ignore]    
        public void CountsMatch()
        {
            MarketListingsFetchCommand c = new MarketListingsFetchCommand();

            MarketListingsFetchArgumentsDto searchParams = new MarketListingsFetchArgumentsDto();
            searchParams.HighlightVehicle = true;
            searchParams.InsertUser = INSERT_USER;
            searchParams.MaximumRows = MAX_ROWS;
            searchParams.OwnerEntityId = OWNER_ENTITY_ID;
            searchParams.OwnerEntityTypeId = OWNER_ENTITY_TYPE_ID;
            searchParams.SearchType = SearchTypeDto.Overall;
            searchParams.VehicleEntityId = VEHICLE_ENTITY_ID;
            searchParams.VehicleEntityTypeId = VEHICLE_ENTITY_TYPE_ID;

            var results  = c.Execute( searchParams );
            Assert.IsNotNull(results);

            foreach(var listing in results.MarketListings)
            {
                Console.WriteLine("VIN: {0}, Price: {1}, Mileage: {2}", listing.ListingVin, listing.ListPrice,
                              listing.VehicleMileage);
            }

            // Get handles
            string ownerHandle;
            string vehicleHandle;
            string searchHandle;
            c.CreateHandles(OWNER_ENTITY_ID, VEHICLE_ENTITY_TYPE_ID, VEHICLE_ENTITY_ID, INSERT_USER, out ownerHandle,
                            out vehicleHandle, out searchHandle);

            // Run old search
            MarketPricingDataSource ds = new MarketPricingDataSource();
            var listings = ds.FindListByOwnerHandleAndSearchHandle(ownerHandle, searchHandle, 1);

            // Verify listing counts agree between old and new.
            Assert.AreEqual(listings.Rows.Count, results.MarketListings.Count);
        }

        // Precision search converts to generic
        public void SearchUpgrade()
        {
/*
            // https://betamax.firstlook.biz/pricing/Pages/Internet/VehiclePricingAnalyzer.aspx?oh=ECB5C338-B5C3-DC11-9377-0014221831B0&vh=127175370&sh=DD507342-7701-E211-A127-0022198DB286
            string oh = "ECB5C338-B5C3-DC11-9377-0014221831B0";
            string vh = "127175370";
            string sh = "DD507342-7701-E211-A127-0022198DB286";
#1#
        }


        [Test]
        public void CreateHandles()
        {
            MarketListingsFetchCommand c = new MarketListingsFetchCommand();
            string ownerHandle;
            string vehicleHandle;
            string searchHandle;
            c.CreateHandles(OWNER_ENTITY_ID, VEHICLE_ENTITY_TYPE_ID, VEHICLE_ENTITY_ID, INSERT_USER, out ownerHandle,
                            out vehicleHandle, out searchHandle);

            Assert.IsNotNullOrEmpty(ownerHandle);
            Assert.IsNotNullOrEmpty(vehicleHandle);
            Assert.IsNotNullOrEmpty(searchHandle);

            Console.WriteLine("OwnerHandle: {0}, VehicleHandle: {1}, SearchHandle: {2}", ownerHandle, vehicleHandle, searchHandle);

        }


/*
          internet_price: 19700,
                    unit_cost: 17416,
                    currentMileage: 32705,
                    OwnerEntityTypeId: 1,
                    OwnerEntityId: 102225,
                    VehicleEntityTypeId: 1,
                    VehicleEntityId: 26165120,
                    InsertUser: 'breeve'
#1#


    }
}
*/
