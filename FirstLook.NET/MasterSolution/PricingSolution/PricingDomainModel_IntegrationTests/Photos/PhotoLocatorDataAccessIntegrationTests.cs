using System;
using System.Data;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Marketing.Photos;
using FirstLook.Pricing.DomainModel;
using NUnit.Framework;

namespace PricingDomainModel_IntegrationTests.Photos
{
    [TestFixture]
    public class PhotoLocatorDataAccessIntegrationTests
    {
        private PhotoLocatorDataAccess _dataAccess;
        private string _ownerHandle, _vehicleHandle;

        [TestFixtureSetUp]
        public void Setup()
        {
            _dataAccess = new PhotoLocatorDataAccess();
            HandleUtilities.GetFirstInventoryHandles(out _ownerHandle, out _vehicleHandle);
        }

        [Test]
        public void HandlesPopulated()
        {
            Assert.IsNotEmpty(_ownerHandle);
            Assert.IsNotEmpty(_vehicleHandle);

            Console.WriteLine( "Owner handle: " + _ownerHandle );
            Console.WriteLine("Vehicle handle: " + _vehicleHandle);

        }

        [Test]
        public void CanCreate()
        {        
            Assert.IsNotNull( _dataAccess );
        }

        [Test]
        public void FetchDealerLogoUrl()
        {
            // Get the url from the db.
            string ownerHandle, dbUrl;
            GetFirstHandleAndPhotoUrlFromDatabase(out ownerHandle, out dbUrl);

            // Get the url from the locator.
            string url = _dataAccess.FetchDealerLogoUrl(ownerHandle);
            Assert.IsNotNull(url);
            Assert.AreEqual( dbUrl, url );
            Console.WriteLine("Url = " + url);
        }

        private static void GetFirstHandleAndPhotoUrlFromDatabase(out string ownerHandle, out string url)
        {
            const string query = @"SELECT  Handle, PhotoUrl
                                FROM    dbo.BusinessUnit bu
                                JOIN    dbo.BusinessUnitPhotos bup ON bup.BusinessUnitID = bu.BusinessUnitID 
                                JOIN    dbo.Photos p ON p.PhotoID = bup.PhotoID
                                JOIN    Market.Pricing.Owner mpo ON mpo.OwnerEntityId = bu.BusinessUnitID
                                WHERE   mpo.OwnerTypeId = 1";

           
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = query;

                    IDataReader reader = command.ExecuteReader();

                    Assert.IsTrue(reader.Read());

                    ownerHandle = reader.GetGuid(reader.GetOrdinal("Handle")).ToString();
                    url = reader.GetString(reader.GetOrdinal("PhotoUrl"));
                }
            }
        }
    }
}