﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Autofac;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Implementation;
using FirstLook.Merchandising.DomainModel.Marketing.MarketAnalysis.Types;
using FirstLook.Pricing.DomainModel;
using NUnit.Framework;
using PricingDomainModel_Test;

namespace PricingDomainModel_IntegrationTests.MarketAnalysis
{
    [TestFixture]
    public class MarketAnalysisVehiclePreferenceIntegrationTests
    {

        [TestFixtureSetUp]
        public void Setup()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<MarketAnalysisDealerPreferenceDataAccess>().As<IMarketAnalysisDealerPreferenceDataAccess>();
            builder.RegisterType<MarketAnalysisPricingDataAccess>().As<IMarketAnalysisPricingDataAccess>();
            builder.RegisterType<MarketAnalysisVehiclePreferenceDataAccess>().As<IMarketAnalysisVehiclePreferenceDataAccess>();
            builder.RegisterType<ConsoleLogger>().As<ILogger>();
            Registry.RegisterContainer(builder.Build());
        }

        [Test]
        public void CanGetPreference()
        {
            string oh;
            string vh;

            GetOwnerAndVehicle_NoVehiclePreferenceExists(out oh, out vh);

            MarketAnalysisVehiclePreference preference =
                MarketAnalysisVehiclePreference.GetOrCreateMarketAnalysisVehiclePreference(oh, vh);

            Assert.IsNotNull(preference);
            Assert.IsTrue(preference.IsNew);
        }

        [Test]
        public void CanEditAndSavePriceProvidersForPreference()
        {
            string oh;
            string vh;

            GetOwnerAndVehicle_NoVehiclePreferenceExists(out oh, out vh);

            MarketAnalysisVehiclePreference preference =
                MarketAnalysisVehiclePreference.GetOrCreateMarketAnalysisVehiclePreference(oh, vh);

            List<MarketAnalysisPriceProviders> providers = new List<MarketAnalysisPriceProviders>();

            providers.Add(MarketAnalysisPriceProviders.KelleyBlueBookRetailValue);
            providers.Add(MarketAnalysisPriceProviders.JDPowerPINAverageInternetPrice);

            preference.SelectedPriceProviders = providers;
            preference.HasOverriddenPriceProviders = true;

            preference.Save();

            MarketAnalysisVehiclePreference preference2 =
                            MarketAnalysisVehiclePreference.GetOrCreateMarketAnalysisVehiclePreference(oh, vh);
            
            Assert.IsNotNull(preference2, "preference was null.");
            Assert.IsFalse(preference2.IsNew, "preference was New, but shouldn't be.");
            Assert.IsNotNull(preference2.SelectedPriceProviders, "preference2.SelectedPriceProviders was null.");

        }

        [Test]
        public void CanEditPriceProviderListForPreference()
        {
            string oh;
            string vh;

            GetOwnerAndVehicle_NoVehiclePreferenceExists(out oh, out vh);

            MarketAnalysisVehiclePreference preference =
                MarketAnalysisVehiclePreference.GetOrCreateMarketAnalysisVehiclePreference(oh, vh);

            List<MarketAnalysisPriceProviders> providers = new List<MarketAnalysisPriceProviders>();

            providers.Add(MarketAnalysisPriceProviders.KelleyBlueBookRetailValue);
            providers.Add(MarketAnalysisPriceProviders.JDPowerPINAverageInternetPrice);

            preference.SelectedPriceProviders = providers;
            preference.HasOverriddenPriceProviders = true;

            preference.Save();

            MarketAnalysisVehiclePreference preference2 =
                            MarketAnalysisVehiclePreference.GetOrCreateMarketAnalysisVehiclePreference(oh, vh);

            Assert.IsNotNull(preference2, "preference was null.");
            Assert.IsFalse(preference2.IsNew, "preference was New, but shouldn't be.");
            Assert.IsNotNull(preference2.SelectedPriceProviders, "preference2.SelectedPriceProviders was null.");

            List<MarketAnalysisPriceProviders> newproviders = new List<MarketAnalysisPriceProviders>();
            newproviders.Add(MarketAnalysisPriceProviders.NADARetailValue);

            preference2.SelectedPriceProviders = newproviders;

            preference2.Save();

            MarketAnalysisVehiclePreference preference3 =
                            MarketAnalysisVehiclePreference.GetOrCreateMarketAnalysisVehiclePreference(oh, vh);


            Assert.IsNotNull(preference3);
            Assert.IsNotNull(preference3.SelectedPriceProviders);

            int providerCount = 0;

            Debug.WriteLine("preference3.SelectedPriceProviders:");

            foreach (var provider in preference2.SelectedPriceProviders)
            {
                Debug.WriteLine("Provider: " + provider);
                providerCount++;
            }

            Debug.WriteLine("end of preference3.SelectedPriceProviders");

            Assert.AreEqual(newproviders.Count, providerCount);

            List<MarketAnalysisPriceProviders> testproviders = new List<MarketAnalysisPriceProviders>();
            testproviders.Add(MarketAnalysisPriceProviders.NADARetailValue);

            preference3.SelectedPriceProviders = testproviders;

            Assert.IsFalse(preference3.IsDirty);

        }





        #region Handle Utility Methods
        static void GetOwnerAndVehicle_NoVehiclePreferenceExists(out string ownerHandle, out string vehicleHandle)
        {

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        @"  SELECT	TOP 1 '1' + cast(i.inventoryid as varchar) as vh, o.handle as oh, i.inventoryid, o.ownerid
                            FROM	[IMT].dbo.Inventory I
                            JOIN	Market.Pricing.Owner o ON i.BusinessUnitId = o.OwnerEntityId
                            LEFT JOIN IMT.Marketing.MarketAnalysisVehiclePreference vp 
                            ON vp.VehicleEntityId = i.InventoryID 
                            AND vp.VehicleEntityTypeId = 1
                            AND o.OwnerId = vp.OwnerId
                            WHERE	I.InventoryActive = 1
                            AND	I.InventoryType = 2
                            AND	OwnerTypeID = 1
                            AND vp.MarketAnalysisVehiclePreferenceId IS NULL";

                    IDataReader reader = command.ExecuteReader();

                    if (reader.Read())
                    {
                        ownerHandle = reader.GetGuid(reader.GetOrdinal("oh")).ToString();
                        vehicleHandle = reader.GetString(reader.GetOrdinal("vh"));
                        return;
                    }
                }
            }

            throw new ApplicationException("no owner/vehicle handles found.");
        }

        #endregion
    }
}
