using System.Data;
using Autofac;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Marketing.AdditionalSettings;
using FirstLook.Merchandising.DomainModel.Marketing.AdditionalSettings.Types;
using FirstLook.Pricing.DomainModel;
using NUnit.Framework;

namespace PricingDomainModel_IntegrationTests.AdditionalSettings
{
    /// <summary>
    /// WARNING:  These tests delete all data from IMT.Market.DealerGeneralPreference.  Do NOT run these
    /// tests in an environment in which you care about this data.
    /// </summary>
    [TestFixture]
    public class DealerGeneralPreferenceDataAccessIntegrationTests
    {
        private string _ownerHandle;

        [TestFixtureSetUp]
        public void Setup()
        {
            string vehicleHandle;
            HandleUtilities.GetFirstInventoryHandles(out _ownerHandle, out vehicleHandle);

        }

        #region Reset Test Conditions

        private static void ResetTestConditions()
        {
            ResetRegistryWithDataAccess();
            ResetDataStore();
        }

        private static void ResetRegistryWithDataAccess()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<DealerGeneralPreferenceDataAccess>().As<IDealerGeneralPreferenceDataAccess>();
            Registry.RegisterContainer(builder.Build());
        }

        private static void ResetDataStore()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "Delete From Marketing.DealerGeneralPreference";

                    command.ExecuteNonQuery();
                }
            }
        }

        #endregion

        #region Private Helper Methods

        private DealerGeneralPreference CreateDealerGeneralPreference()
        {
           

            return DealerGeneralPreference.CreateDealerGeneralPreference(_ownerHandle);

        }

        #endregion


        [Test]
        public void CanInsert()
        {
            ResetTestConditions();

            DealerGeneralPreference preference = CreateDealerGeneralPreference();
            Assert.IsTrue( preference.IsNew );
            Assert.IsTrue( preference.IsDirty );

            preference.Save();

            Assert.IsFalse(preference.IsNew);
            Assert.IsFalse(preference.IsDirty);

            // Now verify that it exists.
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "Select * From Marketing.DealerGeneralPreference";

                    IDataReader reader = command.ExecuteReader();

                    Assert.IsTrue( reader.Read() );

                    string foundOwnerHandle = 
                        HandleUtilities.GetOwnerHandleFromOwnerId( reader.GetInt32( reader.GetOrdinal("OwnerId") ));

                    Assert.AreEqual( _ownerHandle, foundOwnerHandle );

                }
            }
        }

        [Test]
        public void CanUpdate()
        {
            ResetTestConditions();

            const string newAddress = "new address";

            DealerGeneralPreference preference = CreateDealerGeneralPreference();
            preference.Save();

            preference.AddressLine1 = newAddress;
            Assert.IsFalse( preference.IsNew );
            Assert.IsTrue( preference.IsDirty );
            preference.Save();


            // Now verify that it was updated.
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "Select * From Marketing.DealerGeneralPreference";

                    IDataReader reader = command.ExecuteReader();

                    Assert.IsTrue(reader.Read());

                    Assert.AreEqual(newAddress, reader["AddressLine1"]);
                    Assert.IsNotNull( reader["UpdateUser"]);
                    Assert.IsNotNull( reader["UpdateDate"]);
                }
            }
        }

        [Test]
        public void CanFetch()
        {
            ResetTestConditions();

            DealerGeneralPreference preference = CreateDealerGeneralPreference();            
            preference.Save();

            DealerGeneralPreference found = 
                DealerGeneralPreference.GetDealerGeneralPreference(preference.OwnerHandle);

            Assert.AreEqual( preference.OwnerHandle, found.OwnerHandle );
            
            Assert.AreEqual( preference.AddressLine1, found.AddressLine1 );
            Assert.AreEqual( preference.AddressLine2, found.AddressLine2 );
            Assert.AreEqual( preference.City, found.City );
            Assert.AreEqual( preference.DaysValidFor, found.DaysValidFor );
            Assert.AreEqual( preference.Disclaimer, found.Disclaimer );
            Assert.AreEqual( preference.ExtendedTagline, found.ExtendedTagline );
            Assert.AreEqual( preference.SalesEmailAddress, found.SalesEmailAddress );
            Assert.AreEqual( preference.SalesPhoneNumber, found.SalesPhoneNumber );
            Assert.AreEqual( preference.StateCode, found.StateCode );
            Assert.AreEqual( preference.ZipCode, found.ZipCode );

            Assert.IsNotNull( found.Name );
        }

        
    }
}