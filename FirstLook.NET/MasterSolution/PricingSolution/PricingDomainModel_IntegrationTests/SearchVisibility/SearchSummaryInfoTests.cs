﻿using System;
using System.Diagnostics;
using Autofac;
using FirstLook.Common.Core.IOC;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search;
using NUnit.Framework;


namespace PricingDomainModel_IntegrationTests.SearchVisibility
{   
    [TestFixture]
    public class SearchSummaryInfoTests
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<SearchSummaryInfoDataAccess>().As<ISearchSummaryInfoDataAccess>();
            Registry.RegisterContainer(builder.Build());
        }

        [Test]
        public void GetSearchSummaryInfo()
        {
            //params
            String ownerHandle, vehicleHandle, searchHandle;
            HandleUtilities.GetFirstInventoryHandles(out ownerHandle,out vehicleHandle,out searchHandle);
            Search  search = Search.GetSearch(new SearchKey(ownerHandle, vehicleHandle, searchHandle));
            SearchSummaryInfo searchSummaryInfo =
                SearchSummaryInfo.GetSearchSummaryInfo
                    (
                        ownerHandle,
                        vehicleHandle,
                        searchHandle,
                        search
                     );
            AssertSearchSummaryInfo(searchSummaryInfo);
            Debug.WriteLine(search.CatalogEntries.Count);
        }


       private void AssertSearchSummaryInfo(SearchSummaryInfo info)
        {
            Assert.AreNotEqual(info, null);
            Assert.AreNotEqual(info.Catalog, null);
        }




    }
}