using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;
using FirstLook.Pricing.DomainModel;


namespace PricingDomainModel_IntegrationTests.MarketListings
{
    internal class MarketListingHandleHelper
    {
        /// <summary>
        /// Retruns the first to be found owner handle with a maket listings (as an out param) and the first associated vehicle handle
        /// </summary>
        /// <param name="ownerHandle"></param>
        /// <returns></returns>
        internal static String GetOwnetHandleWithVehicleHandles(out String ownerHandle)
        {
            ownerHandle = String.Empty;
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.MarketingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        @"Select top 1 convert(varchar(36),o.Handle) as [OwnerHandle], '1' + convert(varchar(36),VehicleEntityId) AS [VehicleHandle]
                            From Market.Pricing.Search s
                            inner join Market.Pricing.Owner o on s.ownerId = o.ownerId
                            inner join IMT.dbo.Inventory i on i.InventoryId = s.VehicleEntityId
                            Where VehicleEntityTypeID = 1
                            and i.InventoryActive = 1
                            and i.InventoryType = 2 -- used cars
                            ";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        //go further only once
                        if (!reader.Read()) throw new DataException("Could not get the market lisitng for the test");

                        ownerHandle = reader.GetString(reader.GetOrdinal("OwnerHandle"));
                        return reader.GetString(reader.GetOrdinal("VehicleHandle"));
                    }
                }
            }
        }

        public static string GetOwnerHandleWithInventoryForWhichNoMarketLisingPreferenceExists()
        {
            string ownerHandle;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.MarketingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        @"SELECT  TOP (1) Market.Pricing.Owner.Handle
                            FROM         Market.Pricing.Owner INNER JOIN
                                Inventory ON Market.Pricing.Owner.OwnerEntityID = Inventory.BusinessUnitID LEFT OUTER JOIN
                                Marketing.MarketListingPreference ON Market.Pricing.Owner.OwnerID = Marketing.MarketListingPreference.OwnerId
                            WHERE     (Market.Pricing.Owner.OwnerTypeID = 1) AND (Marketing.MarketListingPreference.OwnerId IS NULL)";

                    object result = command.ExecuteScalar();
                    if (result == null) return String.Empty;

                    ownerHandle = result.ToString();
                }
            }

            return ownerHandle;
        }



        public static string GetOwnerHandleWithInventoryForWhichMarketLisingPreferenceExists()
        {
            string ownerHandle;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.MarketingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        @"SELECT  TOP (1) Market.Pricing.Owner.Handle
                            FROM         Market.Pricing.Owner INNER JOIN
                                Inventory ON Market.Pricing.Owner.OwnerEntityID = Inventory.BusinessUnitID LEFT OUTER JOIN
                                Marketing.MarketListingPreference ON Market.Pricing.Owner.OwnerID = Marketing.MarketListingPreference.OwnerId
                            WHERE     (Market.Pricing.Owner.OwnerTypeID = 1) AND (Marketing.MarketListingPreference.OwnerId IS NOT NULL)";

                    object result = command.ExecuteScalar();
                    if (result == null) return String.Empty;

                    ownerHandle = result.ToString();
                }
            }

            return ownerHandle;
        }


    }
}