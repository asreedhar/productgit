using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Schema;

namespace PricingDomainModel_Test.MarketListings.XMLValidation
{
    internal class XMLValidator
    {
        /// <summary>
        /// returns the number of XML Violations that have been found
        /// </summary>
        /// <param name="xmlData"></param>
        /// <param name="xsdLocation"></param>
        /// <returns></returns>
        internal static Int32 Validate(String xmlData, String xsdLocation)
        {

            List<String> xmlViolations = new List<String>();
            StringReader xmlStrReader = new StringReader(xmlData);

            XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
            xmlReaderSettings.CheckCharacters = true;
            xmlReaderSettings.CloseInput = true;

            xmlReaderSettings.Schemas.Add(null, xsdLocation);

            xmlReaderSettings.ValidationType = ValidationType.Schema;
            xmlReaderSettings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;

            xmlReaderSettings.ValidationEventHandler += delegate(object sender, ValidationEventArgs args)
                    {
                        String errorMessage =
                            String.Format(
                                "Xml validation failed against schema: '{0}', Message: {1}, Severity: {2}, Exception: {3}",
                                xsdLocation, args.Message, args.Severity,
                                args.Exception);
                        System.Diagnostics.Trace.WriteLine(errorMessage);
                        xmlViolations.Add(errorMessage);
                    };

            try
            {
                using (XmlReader xmlReader = XmlReader.Create(xmlStrReader, xmlReaderSettings))
                {
                    while (xmlReader.Read())
                    {
                        //empty loop - if Read fails it will raise an error via the
                        //ValidationEventHandler wired to the XmlReaderSettings object            
                    }
                    xmlReader.Close();

                }

            }
            catch (Exception ex)
            {

                System.Diagnostics.Trace.WriteLine(String.Format
                    ("The follwing exception was thrown during the creation of the test XmlReader {0} ", ex.Message));
                //rethrow
                throw;
            }

            return xmlViolations.Count;
        }
    }
}
