/*
using System;
using System.Collections.Generic;
using System.Linq;

using FirstLook.Pricing.DomainModel.Marketing.MarketListings.Search;
using NUnit.Framework;
using System.Configuration;

namespace PricingDomainModel_IntegrationTests.MarketListings
{
    [TestFixture]
    public class SearchAdapterTests
    {

        //this constant is according to a specific state of the db on beta and might change over time!
        //private const string EXPECTED_RESULT_XML =
        //    "<marketlistings><marketlisting><dealer><name>Shepard Chevrolet</name><logo></logo></dealer><vehicle><name>2006 Chevrolet Suburban SUV LS</name><color>Blue</color><mileage>27179</mileage><certified>False</certified><price>24999</price><savings>-1</savings><description>2006 CHEVROLET SUBURBAN LS 1500</description></vehicle></marketlisting><marketlisting><dealer><name>BILL KAY CHEVROLET</name><logo></logo></dealer><vehicle><name>2006 Chevrolet Suburban SUV Z71</name><color>White</color><mileage>30768</mileage><certified>False</certified><price>25147</price><savings>147</savings><description>2006 CHEVROLET SUBURBAN 4X4</description></vehicle></marketlisting><marketlisting><dealer><name>JERRY HAGGERTY CHEVROLET</name><logo></logo></dealer><vehicle><name>2006 Chevrolet Suburban SUV LTZ</name><color>White</color><mileage>39481</mileage><certified>True</certified><price>26886</price><savings>1886</savings><description>2006 CHEVROLET SUBURBAN 1500 LTZ</description></vehicle></marketlisting></marketlistings>";

        private String _testOwnerHandle = String.Empty;
        private String _testVehicleHandle = String.Empty;

        private Int32 _testMarketListingPriceLow = 0;
        private Int32 _testMarketListingPriceHigh = 0;
        private Int32 _testMarketListingMileageLow = 0;
        private Int32 _testMarketListingMileageHigh = 0;
        private Int32 _testMarketListingOfferPrice = 0;

        private IEnumerable<IMarketListing> MarketListings { get; set; }


        public SearchAdapterTests()
        {
            TestPrincipal testPrincipal = TestPrincipal.Instance;
        }

        [SetUp]
        public void Setup()
        {
            _testVehicleHandle =
                MarketListingHandleHelper.GetOwnetHandleWithVehicleHandles(out _testOwnerHandle);

            //These can safely be taken from the config as all we doing is provide a wide range
            _testMarketListingPriceLow =
                Convert.ToInt32(ConfigurationManager.AppSettings["TestMarketListingPriceLow"]);
            _testMarketListingPriceHigh =
                Convert.ToInt32(ConfigurationManager.AppSettings["TestMarketListingPriceHigh"]);
            _testMarketListingMileageLow =
                Convert.ToInt32(ConfigurationManager.AppSettings["TestMarketListingMileageLow"]);
            _testMarketListingMileageHigh =
                Convert.ToInt32(ConfigurationManager.AppSettings["TestMarketListingMileageHigh"]);
            _testMarketListingOfferPrice =
                Convert.ToInt32(ConfigurationManager.AppSettings["TestMarketListingOfferPrice"]);

            // Note - it is unlikely this will succeed in the future as it wasn't written against any
            // standard integration database.  ZB
            MarketListings = new SearchAdapter().Search(_testVehicleHandle, _testOwnerHandle,
                _testMarketListingPriceLow, _testMarketListingPriceHigh,
                _testMarketListingMileageLow, _testMarketListingMileageHigh, _testMarketListingOfferPrice);

        }

        [TearDown]
        public void TearDown()
        {
            _testOwnerHandle = String.Empty;
            _testVehicleHandle = String.Empty;

            _testMarketListingPriceLow = 0;
            _testMarketListingPriceHigh = 0;
            _testMarketListingMileageLow = 0;
            _testMarketListingMileageHigh = 0;
            _testMarketListingOfferPrice = 0;
        }

        [Test]
        public void FetchUsingSelect()
        {
            // note - the follwing line assumes we do know that for that
            // vehicle there are valid varket listings.
            // we won't init the db with data for this simple test
            Assert.AreNotEqual(MarketListings.Count(), 0);
        }

       
    }
}
*/
