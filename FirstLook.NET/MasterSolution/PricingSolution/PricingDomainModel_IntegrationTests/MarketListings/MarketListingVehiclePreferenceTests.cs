using System;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings;
using NUnit.Framework;

namespace PricingDomainModel_IntegrationTests.MarketListings
{
    [TestFixture]
    public class MarketListingVehiclePreferenceTests
    {

        [SetUp]
        public void Setup()
        {

        }

        [TearDown]
        public void TearDown()
        {

        }

        //the follwing is actually: FetchDefault
        private MarketListingVehiclePreference DoCreate(String vehicleHandle, String ownetHandle, Int32 offerPrice)
        {
            MarketListingVehiclePreference pref = null;
            if (!MarketListingVehiclePreference.Exists(vehicleHandle,ownetHandle))
            {
                pref = MarketListingVehiclePreference.NewMarketListingVehiclePreference(vehicleHandle, ownetHandle, offerPrice);
            }
            return pref;
        }



        [Test]
        public void Create()
        {
            MarketListingVehiclePreference pref =
                DoCreate("1-7683","76B5C338-B5C3-DC11-9377-0014221831B0", 5000);
            Assert.AreNotEqual(pref, null);
        }


        private MarketListingVehiclePreference DoFetch(String vehicleHandle, String ownerHandle, int offerPrice)
        {
            MarketListingVehiclePreference pref = null;
            if (MarketListingVehiclePreference.Exists(vehicleHandle,ownerHandle))
            {
                pref = MarketListingVehiclePreference.GetMarketListingVehiclePreference(vehicleHandle, ownerHandle, offerPrice);
            }
            return pref;

        }


        [Test]
        public void Fetch()
        {
            MarketListingVehiclePreference pref = DoFetch("1-3465", "6DB5C338-B5C3-DC11-9377-0014221831B0", 10);
            Assert.AreNotEqual(pref, null);
            Assert.AreNotEqual(pref.ID, 0);
        }

        /*[Test]
        public void FetchUsingSelect()
        {
            MarketListingVehiclePreference.DataSource ds = new MarketListingVehiclePreference.DataSource();
            MarketListingVehiclePreference mlvp =
                ds.FetchOrCreate("1-3465","6DB5C338-B5C3-DC11-9377-0014221831B0",0);
            Assert.AreNotEqual(mlvp, null);
        }



        [Test]
        public void FetchUsingSelectOneNotInDb()
        {
            MarketListingVehiclePreference.DataSource ds = new MarketListingVehiclePreference.DataSource();
            MarketListingVehiclePreference mlvp =
                ds.FetchOrCreate("1-7614", "76B5C338-B5C3-DC11-9377-0014221831B0", 500);
            Assert.AreNotEqual(mlvp, null);
        }*/



        [Test]
        public void Update()
        {
            //First call the fetch code
            MarketListingVehiclePreference pref = 
                DoFetch("1-3465", "6DB5C338-B5C3-DC11-9377-0014221831B0", 10);
            Boolean orgDisplay = pref.IsDisplayed;
            pref.IsDisplayed = !pref.IsDisplayed; //reverse

            //Actually update the db
            pref.Save();

            //Fetch again just to verify
            pref =
                DoFetch("1-3465", "6DB5C338-B5C3-DC11-9377-0014221831B0", 10);
            Assert.AreNotEqual(pref.IsDisplayed, orgDisplay);


        }

        /*[Test]
        public void Delete()
        {
            MarketListingVehiclePreference.DataSource ds = new MarketListingVehiclePreference.DataSource();
            MarketListingVehiclePreference mlvp = DoFetch("1-7683", "76B5C338-B5C3-DC11-9377-0014221831B0", 10);
            Assert.AreNotEqual(mlvp, null);
            mlvp.Delete();
            mlvp.Save();

            
        }*/


        [Test]
        public void Insert()
        {
            //First let's get a default one
            MarketListingVehiclePreference pref =
                DoCreate("1-7683", "76B5C338-B5C3-DC11-9377-0014221831B0", 5000);
            Assert.AreNotEqual(pref, null);
            Assert.AreEqual(pref.ID, 0); //this means we have the default
            pref.Save();
            pref = DoFetch("1-7683", "76B5C338-B5C3-DC11-9377-0014221831B0", 10);
            Assert.AreNotEqual(pref, null);
            Assert.AreNotEqual(pref.ID, 0);
        }

        /*[Test]
        public void InsertUsingDs()
        {
            //First let's get a default one
            MarketListingVehiclePreference.DataSource ds = new MarketListingVehiclePreference.DataSource();
            MarketListingVehiclePreference pref =
                ds.FetchOrCreate("1-7683", "76B5C338-B5C3-DC11-9377-0014221831B0", 5000);
            Assert.AreNotEqual(pref, null);
            Assert.AreEqual(pref.ID, 0); //this means we have the default
            ds.Insert("1-7683", "76B5C338-B5C3-DC11-9377-0014221831B0", 5000,
                      true, true, 1550, 1578, 120, 500);
            pref = DoFetch("1-7683", "76B5C338-B5C3-DC11-9377-0014221831B0");
            Assert.AreNotEqual(pref, null);
            Assert.AreNotEqual(pref.ID, 0);
        }*/


    }
}