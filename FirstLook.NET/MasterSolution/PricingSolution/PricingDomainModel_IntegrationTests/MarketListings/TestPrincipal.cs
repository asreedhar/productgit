using System;
using System.Security.Principal;
using System.Threading;

namespace PricingDomainModel_IntegrationTests.MarketListings
{
    class TestPrincipal
    {
        static private String[] _roles = {"Administrator"};
        static readonly TestPrincipal _instance = new TestPrincipal();
        

        private IIdentity _identity = null;
        private IPrincipal _principal = null;


        private TestPrincipal()
        {
            // Create a principal to use for new threads.
            //I am using Benson's ID - at sometime this should be mapped
            //to something meaningfull from dbo.Member like "CPOTestUser"
            _identity = new GenericIdentity("bfung"); 
            _principal = new GenericPrincipal(_identity, _roles);

            AppDomain appDomain = Thread.GetDomain();
            appDomain.SetThreadPrincipal(_principal);
        }

        public static TestPrincipal Instance
        {
            get
            {
                return _instance;
            }
        }

        public override string ToString()
        {
            return "Current Identity Name is: "+ _principal.Identity.Name;
        }
    }
}
