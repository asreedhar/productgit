using System;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings;
using NUnit.Framework;

namespace PricingDomainModel_IntegrationTests.MarketListings
{
    [TestFixture]
    public class MarketListingPreferenceTests
    {


        public MarketListingPreferenceTests()
        {
            TestPrincipal testPrincipal = TestPrincipal.Instance;
        }



        //the follwing is actually: FetchDefault
        private static MarketListingPreference DoCreate(String ownetHandle)
        {
            MarketListingPreference pref = null;
            if (!MarketListingPreference.Exists(ownetHandle))
            {
                pref = MarketListingPreference.NewMarketListingPreference(ownetHandle);
            }
            return pref;
        }



        [Test]
        public void Create()
        {
            String testOwnerHandle =
                MarketListingHandleHelper.GetOwnerHandleWithInventoryForWhichNoMarketLisingPreferenceExists();
            MarketListingPreference pref =
                DoCreate(testOwnerHandle);
            Assert.AreNotEqual(pref, null);
        }


        private static MarketListingPreference DoFetch(String ownerHandle)
        {
            MarketListingPreference pref = null;
            if (MarketListingPreference.Exists(ownerHandle))
            {
                pref = MarketListingPreference.GetMarketListingPreference(ownerHandle);
            }
            return pref;

        }


        [Test]
        public void Fetch()
        {
            String testOwnerHandle =
                MarketListingHandleHelper.GetOwnerHandleWithInventoryForWhichMarketLisingPreferenceExists();
            MarketListingPreference pref = DoFetch(testOwnerHandle);
            Assert.AreNotEqual(pref, null);
            Assert.AreNotEqual(pref.id, 0);
        }

        [Test]
        public void FetchUsingSelect()
        {
            String testOwnerHandle =
                MarketListingHandleHelper.GetOwnerHandleWithInventoryForWhichMarketLisingPreferenceExists();
            MarketListingPreference.DataSource ds = new MarketListingPreference.DataSource();
            MarketListingPreference ml = ds.Select(testOwnerHandle);
            Assert.IsNotNull(ml);
        }




        [Test]
        public void Update()
        {

            String testOwnerHandle =
                MarketListingHandleHelper.GetOwnerHandleWithInventoryForWhichMarketLisingPreferenceExists();
            //First call the fetch code
            MarketListingPreference pref = DoFetch(testOwnerHandle);
            pref.IsDisplayed = !pref.IsDisplayed; //reverse

            //Actually update the db
            pref.Save();
        }

        [Test]
        public void Insert()
        {
            String testOwnerHandle =
                MarketListingHandleHelper.GetOwnerHandleWithInventoryForWhichNoMarketLisingPreferenceExists();

            //First let's get a default one
            MarketListingPreference pref =
                DoCreate(testOwnerHandle);
            Assert.AreNotEqual(pref, null);
            Assert.AreEqual(pref.id, 0); //this means we have the defualt
            pref.Save();
            pref = DoFetch(testOwnerHandle);
            Assert.AreNotEqual(pref, null);
            Assert.AreNotEqual(pref.id, 0);
        }
    }
}