using System;
using Autofac;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.Marketing.Vehicle;
using NUnit.Framework;

namespace PricingDomainModel_IntegrationTests.Vehicle
{
    [TestFixture]
    public class VehicleSummaryIntegrationTests
    {
        [Test]
        public void CanGetVehicleSummary()
        {
            ResetTestConditions();

            string ownerHandle,vehicleHandle;
            HandleUtilities.GetFirstInventoryHandles(out ownerHandle, out vehicleHandle);

            VehicleSummary summary = VehicleSummary.GetVehicleSummary(ownerHandle, vehicleHandle);
            Assert.IsNotNull(summary);

            Assert.AreEqual( ownerHandle, summary.OwnerHandle );
            Assert.AreEqual( vehicleHandle, summary.VehicleHandle );

            Console.WriteLine("Drive Train: " + summary.DriveTrain);
            Console.WriteLine("Engine: " + summary.Engine);
            Console.WriteLine( "Exterior Color: " + summary.ExteriorColor );
            Console.WriteLine( "Mileage: " + summary.Mileage );
            Console.WriteLine( "Owner Handle: " + summary.OwnerHandle );
            Console.WriteLine( "Stock Number: " + summary.StockNumber );
            Console.WriteLine( "Transmission: " + summary.Transmission );
            Console.WriteLine( "Vehicle handle: " +  summary.VehicleHandle );
            Console.WriteLine( "VIN: " + summary.VIN );
            Console.WriteLine("Description: " + summary.Description );
            Console.WriteLine( "Advertisement: " + summary.Advertisement );

        }

        #region Reset Test Conditions

        private void ResetTestConditions()
        {
            ResetRegistryWithMockDataAccess();
        }

        private static void ResetRegistryWithMockDataAccess()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<VehicleSummaryDataAccess>().As<IVehicleSummaryDataAccess>();
            Registry.RegisterContainer(builder.Build());
        }



        #endregion
    }
}