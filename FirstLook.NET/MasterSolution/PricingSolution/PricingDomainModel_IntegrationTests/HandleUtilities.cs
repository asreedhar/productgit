using System;
using System.Data;

using FirstLook.Common.Data;
using FirstLook.Pricing.DomainModel;

namespace PricingDomainModel_IntegrationTests
{
    internal class HandleUtilities
    {

        internal static void GetFirstInventoryHandles(out string ownerHandle, out string vehicleHandle)
        {
            GetIndexedInventoryHandles(out ownerHandle, out vehicleHandle, 1);
        }

        internal static void GetFirstInventoryHandles(out string ownerHandle, out string vehicleHandle, out string searchHandle)
        {
            GetIndexedInventoryHandles(out ownerHandle, out vehicleHandle, out searchHandle, 1);
        }

        internal static void GetIndexedInventoryHandles(out string ownerHandle, out string vehicleHandle, int index)
        {
            if (index < 1)
            {
                throw new ApplicationException("Index must be > 0");
            }

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "Select distinct VehicleHandle, OwnerId from Market.Pricing.Search where VehicleEntityTypeID = 1";
                    //"Select distinct top " + index + " VehicleHandle, OwnerId from Market.Pricing.Search where VehicleEntityTypeID = 1";

                    Console.WriteLine(command.CommandText);

                    IDataReader rdr = command.ExecuteReader();

                    if (rdr == null)
                    {
                        throw new ApplicationException("Inventory records could not be found.");
                    }

                    // Go to the specified row
                    int i = 1;
                    while (i < index)
                    {
                        rdr.Read();
                        i++;
                    }

                    if (rdr.Read())
                    {
                        vehicleHandle = rdr.GetString(rdr.GetOrdinal("VehicleHandle"));
                        int ownerId = rdr.GetInt32(rdr.GetOrdinal("OwnerID"));
                        ownerHandle = GetOwnerHandleFromOwnerId(ownerId);
                    }
                    else
                    {
                        throw new ApplicationException("Could not read inventory record " + i);
                    }

                    if (!rdr.IsClosed) rdr.Close();
                }
            }

        }


        //An overload to retrun the searchHandle as well
        internal static void GetIndexedInventoryHandles(out string ownerHandle, out string vehicleHandle, out String searchHandle, int index)
        {
            if (index < 1)
            {
                throw new ApplicationException("Index must be > 0");
            }

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "Select distinct VehicleHandle, OwnerId, Handle from Market.Pricing.Search where VehicleEntityTypeID = 1";

                    Console.WriteLine(command.CommandText);

                    IDataReader rdr = command.ExecuteReader();

                    if (rdr == null)
                    {
                        throw new ApplicationException("Inventory records could not be found.");
                    }

                    // Go to the specified row
                    int i = 1;
                    while (i < index)
                    {
                        rdr.Read();
                        i++;
                    }

                    if (rdr.Read())
                    {
                        vehicleHandle = rdr.GetString(rdr.GetOrdinal("VehicleHandle"));
                        int ownerId = rdr.GetInt32(rdr.GetOrdinal("OwnerID"));
                        ownerHandle = GetOwnerHandleFromOwnerId(ownerId);
                        searchHandle = rdr.GetGuid(rdr.GetOrdinal("Handle")).ToString();
                    }
                    else
                    {
                        throw new ApplicationException("Could not read inventory record " + i);
                    }

                    if (!rdr.IsClosed) rdr.Close();
                }
            }

        }




        internal static void GetFirstMarketHandles(out string ownerHandle, out string vehicleHandle)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = @"select top 1 VehicleID, Handle
                                            From Listing.Vehicle V
                                            JOIN Pricing.Owner O ON V.SellerID = O.OwnerID";

                    IDataReader rdr = command.ExecuteReader();

                    if (rdr == null)
                    {
                        throw new ApplicationException("First market listing record could not be found.");
                    }
                    if (rdr.Read())
                    {
                        vehicleHandle = "5" + rdr.GetInt32(rdr.GetOrdinal("VehicleID"));
                        ownerHandle = rdr.GetGuid(rdr.GetOrdinal("Handle")).ToString();
                    }
                    else
                    {
                        throw new ApplicationException("First market listing record not found.");
                    }

                    if (!rdr.IsClosed) rdr.Close();
                }
            }
        }

        internal static string GetOwnerHandleFromOwnerId(int ownerId)
        {
            string ownerHandle;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = "Select Handle from Market.Pricing.Owner where OwnerID = " + ownerId;

                    IDataReader rdr = command.ExecuteReader();

                    if (rdr == null)
                    {
                        throw new ApplicationException("Owner record could not be found.");
                    }
                    if (rdr.Read())
                    {
                        ownerHandle = rdr.GetGuid(rdr.GetOrdinal("Handle")).ToString();
                    }
                    else
                    {
                        throw new ApplicationException("Owner record not found.");
                    }

                    if (!rdr.IsClosed) rdr.Close();
                }
            }
            return ownerHandle;
        }
    }
}