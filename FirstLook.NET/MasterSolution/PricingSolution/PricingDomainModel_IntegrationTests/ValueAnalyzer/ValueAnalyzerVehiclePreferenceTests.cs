﻿using System;
using System.Data;
using Autofac;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Marketing.Commands;
using FirstLook.Merchandising.DomainModel.Marketing.ValueAnalyzer;
using FirstLook.Pricing.DomainModel;
using NUnit.Framework;
using ICache = FirstLook.Common.Core.ICache;

namespace PricingDomainModel_IntegrationTests.ValueAnalyzer
{
    [TestFixture]
    public class ValueAnalyzerVehiclePreferenceTests
    {
        private const bool DEFAULT_DISPLAYVEHICLEDESCRIPTION = true;
        private const bool DEFAULT_USELONGMARKETINGLISTINGLAYOUT = false;
        private string _ownerHandle, _vehicleHandle;


        [TestFixtureSetUp]
        public void Setup()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<ValueAnalyzerVehiclePreferenceDataAccess>().As<IValueAnalyzerVehiclePreferenceDataAccess>();
            builder.RegisterType<DotNetCacheWrapper>().As<ICache>();
            builder.RegisterType<MockLogger>().As<ILogger>();
            Registry.RegisterContainer(builder.Build());
        }

        #region DataAccess Tests

        [Test]
        public void GetPreferenceReturnsDefaultWhenNotExists()
        {
            GetHandlesFromDatabase(false, out _ownerHandle, out _vehicleHandle);

            var preference = DataAccess.Get(_ownerHandle, _vehicleHandle);

            AssertIsDefaultPreference(preference);
        }

        [Test]
        public void SaveNewPreference()
        {
            GetHandlesFromDatabase(false, out _ownerHandle, out _vehicleHandle);

            var preference = DataAccess.Get(_ownerHandle, _vehicleHandle);

            AssertIsDefaultPreference(preference);

            //swap the boolean values of the preference
            preference.DisplayVehicleDescription = !preference.DisplayVehicleDescription;
            preference.UseLongMarketingListingLayout = !preference.UseLongMarketingListingLayout;

            // save the preference
            DataAccess.Save(preference, "dhillis");

            //get the preference back (into a new object)
            var preference2 = DataAccess.Get(_ownerHandle, _vehicleHandle);

            Assert.IsNotNull(preference2);

            // make sure it is the same as the other object
            Assert.AreEqual(preference.OwnerHandle, preference2.OwnerHandle);
            Assert.AreEqual(preference.VehicleHandle, preference2.VehicleHandle);
            Assert.AreEqual(preference2.DisplayVehicleDescription, preference.DisplayVehicleDescription);
            Assert.AreEqual(preference2.UseLongMarketingListingLayout, preference.UseLongMarketingListingLayout);

        }

        [Test] public void SaveNewPreferenceWithUrls()
        {
            string shortUrl = "http://slashdot.org/";
            string longUrl = "http://www.roflcat.com/images/cats/server.jpg";

            GetHandlesFromDatabase(false, out _ownerHandle, out _vehicleHandle);

            var preference = DataAccess.Get(_ownerHandle, _vehicleHandle);

            AssertIsDefaultPreference(preference);

            //swap the boolean values of the preference
            preference.DisplayVehicleDescription = !preference.DisplayVehicleDescription;
            preference.UseLongMarketingListingLayout = !preference.UseLongMarketingListingLayout;
            preference.ShortUrl = shortUrl;
            preference.LongUrl = longUrl;

            // save the preference
            DataAccess.Save(preference, "dhillis");

            //get the preference back (into a new object)
            var preference2 = DataAccess.Get(_ownerHandle, _vehicleHandle);

            Assert.IsNotNull(preference2);

            // make sure it is the same as the other object
            Assert.AreEqual(preference.OwnerHandle, preference2.OwnerHandle);
            Assert.AreEqual(preference.VehicleHandle, preference2.VehicleHandle);
            Assert.AreEqual(preference2.DisplayVehicleDescription, preference.DisplayVehicleDescription);
            Assert.AreEqual(preference2.UseLongMarketingListingLayout, preference.UseLongMarketingListingLayout);
            Assert.AreEqual(preference2.ShortUrl, shortUrl);
            Assert.AreEqual(preference2.LongUrl, longUrl);
        }

        [Test]
        public void SaveExistingPreference()
        {
            GetHandlesFromDatabase(false, out _ownerHandle, out _vehicleHandle);

            var preference1 = DataAccess.Get(_ownerHandle, _vehicleHandle);

            Assert.IsNotNull(preference1);

            DataAccess.Save(preference1, "dhillis");

            var preference2 = DataAccess.Get(_ownerHandle, _vehicleHandle);

            Assert.IsNotNull(preference2);

            // swap the boolean properties
            preference2.DisplayVehicleDescription = !preference2.DisplayVehicleDescription;
            preference2.UseLongMarketingListingLayout = !preference2.DisplayVehicleDescription;

            DataAccess.Save(preference2, "dhillis");


            var preference3 = DataAccess.Get(_ownerHandle, _vehicleHandle);

            Assert.IsNotNull(preference3);

            Assert.AreEqual(preference3.OwnerHandle, preference2.OwnerHandle);
            Assert.AreEqual(preference3.VehicleHandle, preference2.VehicleHandle);
            Assert.AreEqual(preference3.DisplayVehicleDescription, preference2.DisplayVehicleDescription);
            Assert.AreEqual(preference3.UseLongMarketingListingLayout, preference2.UseLongMarketingListingLayout);

        }

        #endregion

        private ValueAnalyzerVehiclePreferenceTO GetDefaultPreference(bool existsAlready)
        {
            GetHandlesFromDatabase(false, out _ownerHandle, out _vehicleHandle);

            var getCmd = new GetValueAnalyzerVehiclePreference(_ownerHandle, _vehicleHandle);
            AbstractCommand.DoRun(getCmd);
            var preference = getCmd.Preference;

            return preference;
        }

        #region Command Tests

        [Test]
        public ValueAnalyzerVehiclePreferenceTO GetPreferenceReturnsDefaultWhenNotExists_UsingCommands()
        {
            var preference = GetDefaultPreference(false);
            
            // this preference should have the default values.
            AssertIsDefaultPreference(preference);

            return preference;
        }

        [Test]
        public void SaveNewPreference_UsingCommands()
        {
            var preference = GetPreferenceReturnsDefaultWhenNotExists_UsingCommands();

            // save the preference
            var setCmd = new SetValueAnalyzerVehiclePreference(_ownerHandle, _vehicleHandle, !preference.DisplayVehicleDescription, !preference.UseLongMarketingListingLayout, "dhillis");
            AbstractCommand.DoRun(setCmd);

            //get the preference back (into a new object)
            
            var getCmd = new GetValueAnalyzerVehiclePreference(_ownerHandle, _vehicleHandle);
            AbstractCommand.DoRun(getCmd);
            var preference2 = getCmd.Preference;

            Assert.IsNotNull(preference2);

            // make sure it is the same as the other object
            Assert.AreEqual(preference.OwnerHandle, preference2.OwnerHandle);
            Assert.AreEqual(preference.VehicleHandle, preference2.VehicleHandle);
            // we changed all of the boolean properties, check them to make sure they're different.
            Assert.AreNotEqual(preference2.DisplayVehicleDescription, preference.DisplayVehicleDescription);
            Assert.AreNotEqual(preference2.UseLongMarketingListingLayout, preference.UseLongMarketingListingLayout);

        }

        [Test]
        public void SaveExistingPreference_UsingCommands()
        {
            GetHandlesFromDatabase(false, out _ownerHandle, out _vehicleHandle);

            var getCmd = new GetValueAnalyzerVehiclePreference(_ownerHandle, _vehicleHandle);
            AbstractCommand.DoRun(getCmd);
            var preference1 = getCmd.Preference;

            Assert.IsNotNull(preference1);

            // save the preference (make no changes)
            var setCmd = new SetValueAnalyzerVehiclePreference(_ownerHandle, _vehicleHandle, preference1.DisplayVehicleDescription, preference1.UseLongMarketingListingLayout, "dhillis");
            AbstractCommand.DoRun(setCmd);

            // get it back...now it is an "existing" preference, and we will change this one.
            getCmd = new GetValueAnalyzerVehiclePreference(_ownerHandle, _vehicleHandle);
            AbstractCommand.DoRun(getCmd);
            var preference2 = getCmd.Preference;

            Assert.IsNotNull(preference2);

            // swap the boolean properties
            preference2.DisplayVehicleDescription = !preference2.DisplayVehicleDescription;
            preference2.UseLongMarketingListingLayout = !preference2.DisplayVehicleDescription;

            setCmd = new SetValueAnalyzerVehiclePreference(_ownerHandle, _vehicleHandle, preference2.DisplayVehicleDescription, preference2.UseLongMarketingListingLayout, "dhillis");
            AbstractCommand.DoRun(setCmd);

            var preference3 = DataAccess.Get(_ownerHandle, _vehicleHandle);

            Assert.IsNotNull(preference3);

            Assert.AreEqual(preference3.OwnerHandle, preference2.OwnerHandle);
            Assert.AreEqual(preference3.VehicleHandle, preference2.VehicleHandle);
            Assert.AreEqual(preference3.DisplayVehicleDescription, preference2.DisplayVehicleDescription);
            Assert.AreEqual(preference3.UseLongMarketingListingLayout, preference2.UseLongMarketingListingLayout);

        }

        #endregion

        private static void GetHandlesFromDatabase(bool preferenceExists, out string ownerHandle, out string vehicleHandle)
        {
            string query = 
            @"SELECT TOP 1 
                o.Handle AS OwnerHandle, 
                '1' + CAST(i.InventoryID AS VARCHAR) AS VehicleHandle
            FROM dbo.Inventory i
            JOIN Market.pricing.Owner o
                ON o.OwnerEntityID = i.BusinessUnitID
                AND o.OwnerTypeID = 1
            LEFT JOIN Marketing.ValueAnalyzerVehiclePreference vavp
                ON vavp.OwnerID = o.OwnerID
                AND vavp.VehicleEntityID = i.InventoryID
                AND vavp.VehicleEntityTypeID = 1
            WHERE vavp.OwnerID IS " + (preferenceExists ? "NOT NULL" : "NULL");


            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = query;

                    var reader = command.ExecuteReader();

                    Assert.IsTrue(reader.Read());

                    ownerHandle = reader.GetGuid(reader.GetOrdinal("OwnerHandle")).ToString();
                    vehicleHandle = reader.GetString(reader.GetOrdinal("VehicleHandle"));
                }
            }

            Assert.IsNotEmpty(ownerHandle);
            Assert.IsNotEmpty(vehicleHandle);

            Console.WriteLine("Owner handle: " + ownerHandle);
            Console.WriteLine("Vehicle handle: " + vehicleHandle);
        }

        private void AssertIsDefaultPreference(ValueAnalyzerVehiclePreferenceTO preference)
        {
            Assert.AreEqual(DEFAULT_DISPLAYVEHICLEDESCRIPTION, preference.DisplayVehicleDescription);
            Assert.AreEqual(DEFAULT_USELONGMARKETINGLISTINGLAYOUT, preference.UseLongMarketingListingLayout);
        }

        private static IValueAnalyzerVehiclePreferenceDataAccess DataAccess
        {
            get { return Registry.Resolve<IValueAnalyzerVehiclePreferenceDataAccess>(); }
        }

        /// <summary>
        /// Mock of the logging mechanism.
        /// </summary>
        public class MockLogger : ILogger
        {
            public static bool CalledLog;

            public void Reset()
            {
                CalledLog = false;
            }

            public void Log(Exception e)
            {
                CalledLog = true;
            }

            public void Log(string message, object source)
            {
                CalledLog = true;
            }
        }

    }
}
