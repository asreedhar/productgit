using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using Autofac;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Data;
using FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits;
using FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types;
using FirstLook.Merchandising.DomainModel.Marketing.CertifiedBenefits.Types.TransferObjects;
using FirstLook.Pricing.DomainModel;
using NUnit.Framework;
using PricingDomainModel_Test.TestUtilities;

namespace PricingDomainModel_IntegrationTests.CertifiedBenefits
{
    /// <summary>
    /// Warning: These tests alter database contents.  DO NOT RUN THEM in an environment in which you care about the data.
    /// </summary>
    [TestFixture]
    public class CertifiedVehiclePreferenceDataAccessIntegrationTests
    {
        private const string USER = "firstlook";
        private string _inventoryOwnerHandle, _inventoryVehicleHandle;
        private InsertSummary _insertSummary;
 

        [TestFixtureSetUp]
        public void Setup()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<CertifiedVehiclePreferenceDataAccess>().As<ICertifiedVehiclePreferenceDataAccess>();
            Registry.RegisterContainer(builder.Build());
        }

        

        [TestFixtureTearDown]
        public void TearDown()
        {
            // DELETE EVERYTHING.
            // DeleteAllCertifiedBenefitsData();
        }

        #region GetMakeId Tests

        [Test]
        public void GetMakeIdCanPullFromInventory()
        {
            // Reset the test conditions.
            ResetTestConditions();            

            // Get the make id.
            CertifiedVehiclePreferenceDataAccess dataAccess = new CertifiedVehiclePreferenceDataAccess();            
            int makeId = dataAccess.GetMakeId( _inventoryOwnerHandle, _inventoryVehicleHandle);

            Console.WriteLine("oh=" + _inventoryOwnerHandle);
            Console.WriteLine("vh=" + _inventoryVehicleHandle);
            Console.WriteLine( "makeId=" + makeId );

            // If we made it this far, we were successful.
        }

        [Test]
        public void GetMakeIdCanPullFromMarketListings()
        {
            string ownerHandle, vehicleHandle;

            HandleUtilities.GetFirstMarketHandles(out ownerHandle, out vehicleHandle);

            Assert.IsNotEmpty( ownerHandle );
            Assert.IsNotEmpty( vehicleHandle );

            // Get the make id.
            CertifiedVehiclePreferenceDataAccess dataAccess = new CertifiedVehiclePreferenceDataAccess();
            int makeId = dataAccess.GetMakeId(ownerHandle, vehicleHandle);

            Console.WriteLine("oh=" + ownerHandle);
            Console.WriteLine("vh=" + vehicleHandle);
            Console.WriteLine("makeId=" + makeId);

            // If we made it this far, we were successful.
        }

        [Test]
        [ExpectedException(typeof(ApplicationException))]
        public void GetMakeIdFailsWithBogusHandles()
        {
            string ownerHandle = Guid.Empty.ToString();
            const string vehicleHandle = "12345";

            // Get the make id.
            CertifiedVehiclePreferenceDataAccess dataAccess = new CertifiedVehiclePreferenceDataAccess();
            dataAccess.GetMakeId(ownerHandle, vehicleHandle);
        }

        #endregion

        #region GetCertifiedProgramId Tests

        [Test]
        public void GetCertifiedProgramIdReturnsCorrectProgramId()
        {
            // Reset the test conditions.
            ResetTestConditions();            

            CertifiedVehiclePreferenceDataAccess dataAccess = new CertifiedVehiclePreferenceDataAccess();

            // Verify that we can now get the mapping.
            int? foundProgramId;
            dataAccess.GetApplicableCertfiedProgramId(_inventoryOwnerHandle, _inventoryVehicleHandle, out foundProgramId);

            Assert.IsNotNull( foundProgramId );
            Assert.AreEqual(_insertSummary.ProgramId, foundProgramId.Value);
        }



        #endregion

        #region CertifiedProgramExists Tests

        [Test]
        [ExpectedException(typeof(SqlException))]
        public void OwnerCertifiedProgramExistsThrowsForBogusHandles()
        {
            string ownerHandle = Guid.Empty.ToString();

            CertifiedVehiclePreferenceDataAccess dataAccess = new CertifiedVehiclePreferenceDataAccess();
            dataAccess.OwnerCertifiedProgramExists(ownerHandle, -1);
        }
        
        [Test]
        public void OwnerCertifiedProgramExistsReturnsFalse()
        {
            // Reset the test conditions.
            ResetTestConditions();            

            // Get owner handles that aren't assigned to a program.
            string ownerHandle, vehicleHandle;
            HandleUtilities.GetIndexedInventoryHandles(out ownerHandle, out vehicleHandle, 3);
            Console.WriteLine( "oh=" + ownerHandle );
            Console.WriteLine( "vh=" + vehicleHandle );

            // This should be false.
            CertifiedVehiclePreferenceDataAccess dataAccess = new CertifiedVehiclePreferenceDataAccess();
            bool exists = dataAccess.OwnerCertifiedProgramExists(ownerHandle, _insertSummary.ProgramId);

            Assert.IsFalse( exists );
        }

        [Test]
        public void OwnerCertifiedProgramExistsReturnsTrue()
        {
            // Reset the test conditions.
            ResetTestConditions();            

            // This should be true.
            CertifiedVehiclePreferenceDataAccess dataAccess = new CertifiedVehiclePreferenceDataAccess();
            bool exists = dataAccess.OwnerCertifiedProgramExists(_inventoryOwnerHandle, _insertSummary.ProgramId);

            Assert.IsTrue(exists);
        }

        #endregion

        #region GetOwnerCertifiedProgram Tests

        [Test]
        public void GetCertifiedProgramReturnsValidProgram()
        {
            // Reset the test conditions.
            ResetTestConditions();            

            CertifiedVehiclePreferenceDataAccess dataAccess = new CertifiedVehiclePreferenceDataAccess();

            // Get the program transfer object.
            OwnerCertifiedProgramTO program = dataAccess.GetOwnerCertifiedProgram(_inventoryOwnerHandle, _inventoryVehicleHandle);

            Assert.IsNotNull( program );
            Assert.AreEqual( _insertSummary.ProgramName, program.ProgramName );
        }

        [Test]
        [ExpectedException(typeof(ApplicationException), ExpectedMessage = "No certified benefit program is assigned to this vehicle's make.")]
        public void GetCertifiedProgramThrowsWhenMakeIsNotMappedToProgram()
        {
            string ownerHandle, vehicleHandle;

            HandleUtilities.GetIndexedInventoryHandles(out ownerHandle, out vehicleHandle, 2);

            Assert.IsNotEmpty(ownerHandle);
            Assert.IsNotEmpty(vehicleHandle);

            // Get the program.  This should fail.
            CertifiedVehiclePreferenceDataAccess dataAccess = new CertifiedVehiclePreferenceDataAccess();
            dataAccess.GetOwnerCertifiedProgram(ownerHandle, vehicleHandle);
        }

        #endregion

        #region Exists Tests

        [Test]
        public void ExistsReturnsFalse()
        {
            string ownerHandle, vehicleHandle;

            HandleUtilities.GetIndexedInventoryHandles(out ownerHandle, out vehicleHandle, 2);

            Assert.IsNotEmpty(ownerHandle);
            Assert.IsNotEmpty(vehicleHandle);

            CertifiedVehiclePreferenceDataAccess dataAccess = new CertifiedVehiclePreferenceDataAccess();
            bool exists = dataAccess.Exists(ownerHandle, vehicleHandle);   

            Assert.IsFalse( exists );
        }

        [Test]
        public void ExistsReturnsTrue()
        {
            // Reset the test conditions.
            ResetTestConditions();            

            // Create a preference, then see if it exists.            
            CertifiedVehiclePreferenceDataAccess dataAccess = new CertifiedVehiclePreferenceDataAccess();
            
            // Verify it exists.
            bool exists = dataAccess.Exists(_insertSummary.OwnerHandle, _insertSummary.VehicleHandle);
            Assert.IsTrue(exists);
        }

        [Test]
        [ExpectedException(typeof(SqlException))]
        public void ExistsThrowsWhenPassedBogusHandles()
        {
            string ownerHandle = Guid.Empty.ToString(), vehicleHandle = Guid.Empty.ToString();            

            CertifiedVehiclePreferenceDataAccess dataAccess = new CertifiedVehiclePreferenceDataAccess();
            dataAccess.Exists(ownerHandle, vehicleHandle);
        }

        #endregion

        #region FindOwnerBenefit Tests

        [Test]
        public void FindOwnerBenefit()
        {
			// internal OwnerCertifiedProgramTO(int programId, int? ownerProgramId, string programName, string logoPath, List<OwnerCertifiedProgramBenefitTO> benefits)

            // Create a collection of owner benefits.
            var ownerBenefits = new List<OwnerCertifiedProgramBenefitTO>();
            ownerBenefits.Add( new OwnerCertifiedProgramBenefitTO(0, 1, "0", "", true, 0) );
            ownerBenefits.Add( new OwnerCertifiedProgramBenefitTO(1, 2, "1", "", true, 0) );

            var ownerBenefit = CertifiedVehiclePreferenceDataAccess.FindOwnerBenefit(ownerBenefits, 1);

            Assert.AreEqual( 1, ownerBenefit.OwnerBenefitID );
            Assert.AreEqual( "1", ownerBenefit.Name );
        }

        #endregion

        #region GetCertifiedVehicleBenefitPreferenceID Tests

        [Test]
        [Ignore]
        public void GetCertifiedVehicleBenefitPreferenceID()
        {
            // Insert a preference then find it.
            throw new NotImplementedException();
        }

        #endregion

        #region Transaction Tests

        [Test]
        [Ignore]
        public void TransactionFailureRollsBack()
        {
            // TODO: Rewrite this.
/*          
            // Reset test conditions
            ResetTestConditions();

            CertifiedVehiclePreferenceDataAccess dataAccess = new CertifiedVehiclePreferenceDataAccess();
        

            using (ITransaction txn = dataAccess.BeginTransaction())
            {
                try
                {
                    // Reset the test conditions - creating a new program, etc ...
                    ResetTestConditions();
                    throw new ApplicationException("Simulated exception.");
                }
                catch (ApplicationException)
                {
                    txn.Rollback();
                }
            }
            
            // Summary should not be null, but the program should not exist in the db due to the rollback.
            Assert.IsNotNull( _insertSummary );
            Assert.IsFalse( CertifiedProgramExists( _insertSummary.ProgramId ));

            // Verify that not data was committed.
            OwnerCertifiedProgramTO program = dataAccess.GetOwnerCertifiedProgram(_insertSummary.OwnerHandle, _insertSummary.VehicleHandle);
            Assert.IsNull( program );
*/
        }

        #endregion

        #region Insert/Fetch/Delete Tests

        [Test]
        public void CanInsertFetchAndDeletePreference()
        {
            // Reset the test conditions.
            ResetTestConditions();            

            CertifiedVehiclePreferenceDataAccess dataAccess = new CertifiedVehiclePreferenceDataAccess();

            CertifiedVehiclePreferenceTO preference = _insertSummary.CertifiedVehiclePreference;

            // Verify it was inserted.
            CertifiedVehiclePreferenceTO foundPreference = dataAccess.Fetch(_inventoryOwnerHandle, _inventoryVehicleHandle);

            Assert.AreEqual( preference.OwnerHandle, foundPreference.OwnerHandle );
            Assert.AreEqual( preference.VehicleHandle, foundPreference.VehicleHandle );
            Assert.AreEqual( preference.IsDisplayed, foundPreference.IsDisplayed );
            Assert.AreEqual( preference.OwnerCertifiedProgram.OwnerProgramId, foundPreference.OwnerCertifiedProgram.OwnerProgramId );
            Assert.AreEqual( preference.ProgramName, foundPreference.ProgramName );
            Assert.AreEqual( preference.Benefits.Count, foundPreference.Benefits.Count );
        }


        #endregion


        #region Private Utility Methods

        class InsertSummary
        {
            public string OwnerHandle;
            public string VehicleHandle;
            
            public string ProgramName;
            public int ProgramId;

            public int OwnerProgramId;
            public OwnerCertifiedProgramTO OwnerCertifiedProgram;

            public int MakeId;

            public CertifiedVehiclePreferenceTO CertifiedVehiclePreference;

        }

        private void ResetTestConditions()
        {
            // DELETE ALL CERTIFIED BENEFITS DATA!!! WARNING.
            DeleteAllCertifiedBenefitsData();

            HandleUtilities.GetFirstInventoryHandles(out _inventoryOwnerHandle, out _inventoryVehicleHandle);

            Assert.IsNotEmpty(_inventoryOwnerHandle);
            Assert.IsNotEmpty(_inventoryVehicleHandle);

            CertifiedVehiclePreferenceDataAccess dataAccess = new CertifiedVehiclePreferenceDataAccess();

            using (TransactionScope scope = new TransactionScope())
            {
                // Insert the preference.
                _insertSummary = InsertPreference(dataAccess, _inventoryOwnerHandle, _inventoryVehicleHandle);
                scope.Complete();
            }

            string ownerHandle = _insertSummary.OwnerHandle;
            string vehicleHandle = _insertSummary.VehicleHandle;
            CertifiedVehiclePreferenceTO preference = _insertSummary.CertifiedVehiclePreference;

            // Verify it was inserted.
            CertifiedVehiclePreferenceTO foundPreference = dataAccess.Fetch(ownerHandle, vehicleHandle);

            Assert.AreEqual(preference.OwnerHandle, foundPreference.OwnerHandle);
            Assert.AreEqual(preference.VehicleHandle, foundPreference.VehicleHandle);
            Assert.AreEqual(preference.IsDisplayed, foundPreference.IsDisplayed);
            Assert.AreEqual(preference.OwnerCertifiedProgram.OwnerProgramId, foundPreference.OwnerCertifiedProgram.OwnerProgramId);
            Assert.AreEqual(preference.ProgramName, foundPreference.ProgramName);
            Assert.AreEqual(preference.Benefits.Count, foundPreference.Benefits.Count);
        }

        private static InsertSummary InsertPreference( CertifiedVehiclePreferenceDataAccess dataAccess, string ownerHandle, string vehicleHandle )
        {            
            
            // Create a program
            string programName = StringUtilities.GetRandomString();
            int programId = CreateProgram(programName, programName, true, USER);

            // Assign the program to the vehicle's make.
            int makeId = dataAccess.GetMakeId(ownerHandle, vehicleHandle);
            AssignMakeToProgram(makeId, programId);

            // Assign benefits to the program.
            AddBenefitsToProgram(programId);

            // Assign owner to program.
            int ownerProgramId = AssignOwnerToProgram(ownerHandle, programId, USER);

            // Create the preference - use the domain to merge the owner benefits into our line items.
            CertifiedVehiclePreference preference =
                CertifiedVehiclePreference.CreateCertifiedVehiclePreference(ownerHandle, vehicleHandle);

            // Save the preference.
            CertifiedVehiclePreferenceTO preferenceTo = preference.ToTransferObject();
            dataAccess.Insert(preferenceTo, USER);                
            

            // Build up the summary and return it.
            InsertSummary summary = new InsertSummary();
            summary.OwnerHandle = ownerHandle;
            summary.VehicleHandle = vehicleHandle;
            summary.ProgramName = programName;
            summary.ProgramId = programId;
            summary.OwnerProgramId = ownerProgramId;
            summary.OwnerCertifiedProgram = preferenceTo.OwnerCertifiedProgram;
            summary.MakeId = makeId;
            summary.CertifiedVehiclePreference = preferenceTo;

            return summary;
        }

        private void Delete( ICertifiedVehiclePreferenceDataAccess dataAccess, InsertSummary summary)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                // delete the preference
                dataAccess.Delete(summary.CertifiedVehiclePreference, USER);

                // Delete the Owner Certified Program
                DeleteOwnerProgram( summary.OwnerProgramId );

                // Delete Program/Make mapping.
                DeleteProgramMakeAssignment( summary.MakeId );

                // Delete the Program
                DeleteCertifiedProgram( summary.ProgramId );

                scope.Complete();
            }
        }

        private static int CreateProgram(string name, string text, bool active, string user)
        {
            int programId = 0;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Certified.CertifiedProgram#Insert";

                    CommonMethods.AddWithValue(command, "Name", name, DbType.String);
                    CommonMethods.AddWithValue(command, "Text", text, DbType.String);
                    CommonMethods.AddWithValue(command, "Active", active, DbType.Boolean);
                    CommonMethods.AddWithValue(command, "InsertUser", user, DbType.String);
                    CommonMethods.AddWithValue(command, "CertifiedProgramID", programId, DbType.Int32, ParameterDirection.Output, false);

                    command.ExecuteNonQuery();

                    programId = (int)((SqlParameter)command.Parameters["CertifiedProgramID"]).Value;
                }
            }

            return programId;
        }

        private static IList<int> AddBenefitsToProgram(int programId)
        {
            /*
            [Certified].[CertifiedProgramBenefit#Insert]
                @CertifiedProgramID INT,
                @Name VARCHAR(50),
                @Text VARCHAR(100),
                @Rank SMALLINT,
                @IsProgramHighlight BIT,
                @InsertUser VARCHAR(80),
                @CertifiedProgramBenefitID INT OUTPUT            
            */

            // collection of CertifiedProgramBenefitID values
            IList<int> benefitIDs = new List<int>();

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                // Create a few benefits
                for (int i = 0; i < 3; i++)
                {
                    int certifiedProgramBenefitId = 0;

                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "Certified.CertifiedProgramBenefit#Insert";

                        CommonMethods.AddWithValue(command, "CertifiedProgramID", programId, DbType.Int32);
                        CommonMethods.AddWithValue(command, "Name", StringUtilities.GetRandomString(), DbType.String);
                        CommonMethods.AddWithValue(command, "Text", StringUtilities.GetRandomString(), DbType.String);
                        CommonMethods.AddWithValue(command, "Rank", i, DbType.Int32);
                        CommonMethods.AddWithValue(command, "IsProgramHighlight",  i % 2 == 0, DbType.Boolean);
                        CommonMethods.AddWithValue(command, "InsertUser", USER, DbType.String);
                        CommonMethods.AddWithValue(command, "CertifiedProgramBenefitID", certifiedProgramBenefitId, DbType.Int32,
                                                   ParameterDirection.Output, false);

                        command.ExecuteNonQuery();

                        certifiedProgramBenefitId = (int)((SqlParameter)command.Parameters["CertifiedProgramBenefitID"]).Value;
                        benefitIDs.Add( certifiedProgramBenefitId );
                    }
                }
            }

            return benefitIDs;

        }

        private static void DeleteCertifiedProgram(int programId )
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        @"Delete From Certified.CertifiedProgram Where CertifiedProgramID = " + programId;

                    Console.WriteLine(command.CommandText);

                    command.ExecuteNonQuery();
                }
            }
        }

        private static bool CertifiedProgramExists(int programId )
        {
            bool exists = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        @"Select Count(*) From Certified.CertifiedProgram Where CertifiedProgramID = " + programId;

                    Console.WriteLine(command.CommandText);

                    IDataReader rdr = command.ExecuteReader();

                    if (rdr == null)
                    {
                        throw new ApplicationException("Could not determine whether program exists.");
                    }
                    if (rdr.Read())
                    {
                        int count = rdr.GetInt32(0);
                        Console.WriteLine("Certified.CertifiedProgram Count = " + count);
                        exists = count > 0 ? true : false;
                    }
                    if (!rdr.IsClosed) rdr.Close();                     
                }
            }

            return exists;
        }

        private static int AssignOwnerToProgram(string ownerHandle, int programId, string insertUser)
        {
            int ownerProgramId = 0;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Certified.OwnerCertifiedProgramBenefitCollection#Create";

                    CommonMethods.AddWithValue(command, "OwnerHandle", ownerHandle, DbType.String);
                    CommonMethods.AddWithValue(command, "CertifiedProgramID", programId, DbType.Int32);
                    CommonMethods.AddWithValue(command, "InsertUser", insertUser, DbType.String);
                    CommonMethods.AddWithValue(command, "OwnerCertifiedProgramID", ownerProgramId, DbType.Int32, ParameterDirection.Output, false);

                    command.ExecuteNonQuery();

                    ownerProgramId = (int)((SqlParameter)command.Parameters["OwnerCertifiedProgramID"]).Value;
                }
            }

            return ownerProgramId;
        }

        private static void DeleteOwnerProgram(int programId)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        @"Delete From Certified.OwnerCertifiedProgram Where OwnerCertifiedProgramID = " + programId;

                    Console.WriteLine(command.CommandText);

                    command.ExecuteReader();
                }
            }
        }

        private static void AssignMakeToProgram(int makeID, int programID)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection( Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = 
                        @"Insert Into Certified.CertifiedProgram_Make (MakeID, CertifiedProgramID) 
                          Values( " + makeID + "," + programID + ")";

                    Console.WriteLine( command.CommandText );

                    command.ExecuteNonQuery();
                }
            }
        }

        private static void DeleteProgramMakeAssignment(int makeId)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        @"Delete From Certified.CertifiedProgram_Make Where MakeID = " + makeId;

                    Console.WriteLine(command.CommandText);

                    command.ExecuteNonQuery();
                }
            }
        }


        
        
        

        /// <summary>
        /// WARNING!!! This method will delete ALL certified benefits data.
        /// </summary>
        private static void DeleteAllCertifiedBenefitsData()
        {
            const string sql = @"Use IMT;

                Delete From Certified.OwnerCertifiedProgramBenefit;
                Delete From Certified.OwnerCertifiedProgram;
                Delete From Certified.CertifiedProgramBenefit;
                Delete From Certified.CertifiedProgram_Make;
                Delete From Certified.CertifiedProgram;

                Delete From Marketing.CertifiedVehicleBenefitPreference;
                Delete From Marketing.CertifiedVehicleBenefitLineItem;";

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = sql;

                    Console.WriteLine(command.CommandText);

                    command.ExecuteNonQuery();
                }
            }
        }

        #endregion


    }
}