﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PricingPresenters
{
    public interface IInventoryView
    {
        string OwnerHandle { get; }
        string VehicleHandle { get; }
        int OfferPrice { get; }
    }
}
