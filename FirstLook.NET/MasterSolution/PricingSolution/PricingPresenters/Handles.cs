﻿using System;

namespace FirstLook.Pricing.DomainModel.Marketing.Presentation
{
    [Serializable]
    public class Handles : IHandles
    {
        public string OwnerHandle { get; set; }
        public string VehicleHandle { get; set; }
        public string SearchHandle { get; set; }
    }
}
