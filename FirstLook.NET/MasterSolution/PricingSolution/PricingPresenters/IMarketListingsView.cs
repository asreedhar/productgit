﻿using FirstLook.Common.Core.Web.MVP;
using FirstLook.Common.Core.Xml;
using FirstLook.Merchandising.DomainModel.Marketing.Deal;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;

namespace PricingPresenters
{
    public interface IMarketListingsView : IInventoryView, IView<IMarketListingsView>
    {
        /// <summary>
        /// The presenter will load data into the document.
        /// </summary>
        SerializableXmlDocument XmlDoc { get; set; }

        IDeal Deal { get; }
        /// <summary>
        /// The view must tell the presenter whether it is in edit mode.
        /// </summary>
        bool EditMode { get; }

        /// <summary>
        /// The view should tell the presenter which layout mode it is in.
        /// </summary>
        LayoutKeys LayoutKey { get; set; }

        bool ShowDisplayedColumn { get; set; }
        bool ShowDealerName { get; set; }
        // Presenter will instruct view how to style modules.
        bool StyleSellingSheetEmpty { get; set; }
        bool StyleMarketComparisonEmpty { get; set; }

        // The presenter will specify instructions on what/how to render.
        bool DisplaySellingSheetPanel { get; set; }
        bool DisplayMarketComparisonPanel { get; set; }

        void StylePanels();

        void RemoveAddSelectedItems(MarketListingVehiclePreference preference);
        SortCode? Sortcode { get; set; }
        bool? Sortascending { get; set; }
    }

    public enum LayoutKeys { SellingSheet, MarketComparison, VehiclePreference }

}
