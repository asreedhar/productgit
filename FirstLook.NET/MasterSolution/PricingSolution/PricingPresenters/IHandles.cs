namespace FirstLook.Pricing.DomainModel.Marketing.Presentation
{
    public interface IHandles
    {
        string OwnerHandle { get; set; }
        string VehicleHandle { get; set; }
        string SearchHandle { get; set; }
    }
}