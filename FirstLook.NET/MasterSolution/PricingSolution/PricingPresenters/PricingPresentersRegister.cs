﻿using Autofac;
using FirstLook.Common.Core;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;

namespace PricingPresenters
{
    public class PricingPresentersRegister : IRegistryModule
    {
        public void Register(ContainerBuilder builder)
        {
            // Market Listings
            builder.Register(c => new MarketListingsPresenter(c.Resolve<ILogger>(), c.Resolve<IMarketListingSearchAdapter>()));
        }
    }
}