﻿using System;
using System.Xml;

using FirstLook.Common.Core;
using FirstLook.Common.Core.Web.MVP;
using FirstLook.Common.Core.Xml;
using FirstLook.Merchandising.DomainModel.Marketing.Deal;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings;
using FirstLook.Merchandising.DomainModel.Marketing.MarketListings.Search;

namespace PricingPresenters
{
    public class MarketListingsPresenter : AbstractPresenter<IMarketListingsView>
    {
        private readonly ILogger _logger;
        private readonly IMarketListingSearchAdapter _searchAdapter;
        private IMarketListingFilterSpecification Search { get; set; }
        private MarketListingsInformation _listingsInfo;

        private MarketListingsInformation GetMarketListingsInfo()
        {
            if (_listingsInfo == null)
            {
                _listingsInfo = View.Deal.GetMarketListings();
                if(View.EditMode)
                    View.RemoveAddSelectedItems(_listingsInfo.Preference);
            }

            if (_listingsInfo == null)
            {
                throw new ApplicationException("Could not get or create a market listing vehicle preference.");
            }

            return _listingsInfo;
        }

        public bool IsPreferenceNew
        {
            get
            {
                var listingsInfo = GetMarketListingsInfo();
                return listingsInfo.Preference.IsNew;
            }
        }


        public MarketListingsPresenter(ILogger logger, IMarketListingSearchAdapter searchAdapter)
        {
            if (logger == null || searchAdapter == null)
            {
                throw new ArgumentException("A non-null logger and search adapter are required.");
            }

            _logger = logger;
            _searchAdapter = searchAdapter;
        }

        public override void PreRender()
        {
            base.PreRender();

            // Get the search specification
            var listingsInfo = GetMarketListingsInfo();

            listingsInfo.LoadMarketListings(GetSortSpecification());
            View.Deal.SaveXml(listingsInfo);

            SerializableXmlDocument xmlDoc = View.Deal.GetXml(NodeType.marketlistings);
            LoadViewXml(xmlDoc);

            // Set initial panel visibility.
            SetInitialPanelVisibility();

            // Were any listings loaded into the xml document?
            bool anyListingsLoaded = AnyListingsLoaded(xmlDoc);

            // Conditionally hide or style the selling sheet panel.
            ProcessSellingSheetPanel(anyListingsLoaded);

            // Conditionally hide or style the market comparison panel.
            ProcessMarketComparisonPanel(anyListingsLoaded);

            // Apply panel styles.
            View.StylePanels();

            // Hide/Show columns.
            ShowHideColumns(listingsInfo.Preference);

            // Set the view's sort behavior.
            SetViewSortSpecification();

            // Tell the view to bind.
            View.Bind();
        }

        private void ShowHideColumns(MarketListingVehiclePreference preference)
        {
            // Only show the "displayed" column in vehicle preference layout.
            View.ShowDisplayedColumn = View.LayoutKey == LayoutKeys.VehiclePreference;
            //View.ShowDealerName = ((View.LayoutKey == LayoutKeys.SellingSheet ||
            //                        View.LayoutKey == LayoutKeys.MarketComparison) && preference.ShowDealerName);
            View.ShowDealerName = ((View.LayoutKey == LayoutKeys.SellingSheet ||
                                   View.LayoutKey == LayoutKeys.MarketComparison) && preference.ShowDealerName) || View.LayoutKey == LayoutKeys.VehiclePreference;
        }

        private void ProcessSellingSheetPanel(bool anyListingsLoaded)
        {
            // Visibility already initialized. Set style
            View.StyleSellingSheetEmpty = !anyListingsLoaded;
        }

        private void ProcessMarketComparisonPanel(bool anyListingsLoaded)
        {
            // Visibility already set. Set style
            View.StyleMarketComparisonEmpty = !anyListingsLoaded;
        }

        /// <summary>
        /// We hide/show panels based on the view's LayoutKey.
        /// </summary>
        private void SetInitialPanelVisibility()
        {
            switch (View.LayoutKey)
            {
                case LayoutKeys.SellingSheet:
                    View.DisplaySellingSheetPanel = true;
                    View.DisplayMarketComparisonPanel = false;
                    break;

                case LayoutKeys.MarketComparison:
                    View.DisplaySellingSheetPanel = false;
                    View.DisplayMarketComparisonPanel = true;
                    break;
                
                case LayoutKeys.VehiclePreference:
                    View.DisplaySellingSheetPanel = true;
                    View.DisplayMarketComparisonPanel = false;
                    break;

                default:
                    throw new ApplicationException("Invalid layout key.");
            }
        }

       
        /// <summary>
        /// Set the view's sort specification if needed.
        /// </summary>
        private void SetViewSortSpecification()
        {
            // Set the sort specification if we need to.
            if (View.EditMode && View.Sortcode.HasValue && View.Sortascending.HasValue)
                return;
            
            var sortSpecification = GetSortSpecification();

            View.Sortcode = sortSpecification.Sortcode;
            View.Sortascending = sortSpecification.SortAscending;
        }

        /// <summary>
        /// Return the stored sort specification or use values from the view.
        /// </summary>
        /// <returns></returns>
        private IMarketListingSortSpecification GetSortSpecification()
        {
            // Either use the view's sort specification or use the preference.
            if (View.EditMode && View.Sortcode.HasValue && View.Sortascending.HasValue)
            {
                // Use values from the view.
                return new SortSpecificationDTO
                {
                    Sortcode = View.Sortcode.Value,
                    SortAscending = View.Sortascending.Value
                };                
            }

            // Get the sort specification from the preference
            return GetMarketListingsInfo().Preference;            
        }

        /// <summary>
        /// Does the xml document contain any listings?
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        private static bool AnyListingsLoaded(XmlNode doc)
        {
            var nodes = doc.SelectNodes("marketlistings//*");
            return nodes != null ? nodes.Count > 0 : false;
        }

        private void LoadViewXml(SerializableXmlDocument doc)
        {
            // Load the view's XmlDocument.
            View.XmlDoc = doc;
        }

        private class SortSpecificationDTO : IMarketListingSortSpecification
        {
            public SortCode Sortcode { get; set; }
            public bool SortAscending { get; set; }
        }

    }

    
}
