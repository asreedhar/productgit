﻿using FirstLook.Common.Core.Command;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes;

namespace FirstLook.Pricing.DomainModel.Commands
{
    public interface ICommandFactory
    {
        ICommand<SearchResultsDto, SearchArgumentsDto> CreateSearchCommand();

        ICommand<MarketListingsFetchResultsDto, MarketListingsFetchArgumentsDto> CreateMarketListingsFetchCommand();

        ICommand<SearchResultsDto, SearchUpdateArgumentsDto> CreateSearchUpdateCommand();

        ICommand<MarketPricingListFetchResultsDto, MarketPricingListFetchArgumentsDto> CreateMarketPricingListFetchCommand();

        ICommand<ConsumerHighlightsResultsDto, ConsumerHighlightsArgumentsDto> CreateConsumerHighlightsFetchCommand();

        ICommand<InventoryRepriceResultsDto, InventoryRepriceArgumentsDto> CreateInventoryRepriceCommand();

        ICommand<FetchInternetPreferencesResultDto, FetchInternetPreferencesArgumentsDto> CreateFetchInternetPreferencesCommand();

        ICommand<MysteryShoppingLinksResultDto, MysteryShoppingLinksArgumentsDto> CreateMysteryShoppingLinksCommand();
        

        ICommand<VehiclePricingDecisionInputDto> CreateSaveVehiclePricingDecisionInputCommand();
        ICommand<VehiclePricingDecisionInputDto, VehiclePricingDecisionInputDto> CreateGetVehiclePricingDecisionInputCommand();
        ICommand<HandleDto,HandleDto> CreateHandleLookupCommand();

        ICommand<ReferenceLookupResultsDto, ReferenceLookupArgumentsDto> CreateListingsReferenceCommand();
    }
}
