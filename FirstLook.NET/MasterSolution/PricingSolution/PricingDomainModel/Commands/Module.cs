﻿using FirstLook.Common.Core.Registry;
using FirstLook.Pricing.DomainModel.Commands.Impl;

namespace FirstLook.Pricing.DomainModel.Commands
{
    public class Module : IModule
    {
        #region IModule Members

        public void Configure(IRegistry registry)
        {
            registry.Register<ICommandFactory, CommandFactory>(ImplementationScope.Shared);
        }

        #endregion
    }
}