﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects
{
    [Serializable]
    public class HandleDto
    {
        public string SearchHandle { get; set; }
        public string OwnerHandle { get; set; }
        public string VehicleHandle { get; set; }

        public int OwnerEntityTypeId { get; set; }
        public int OwnerEntityId { get; set; }
        public int VehicleEntityTypeId { get; set; }
        public int VehicleEntityId { get; set; }

        public string UserId { get; set; }

    }
}
