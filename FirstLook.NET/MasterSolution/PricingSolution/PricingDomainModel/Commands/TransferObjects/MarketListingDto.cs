﻿using System;

namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects
{
    [Serializable]
    public class MarketListingDto
    {
        public string Seller { get; set; }

        public bool HasAge { get; set; }

        public int Age { get; set; }

        public string VehicleDescription { get; set; }

        public bool HasVehicleMileage { get; set; }

        public int VehicleMileage { get; set; }

        public bool HasListPrice { get; set; }

        public int ListPrice { get; set; }

        public string VehicleColor { get; set; }

        public bool HasPercentMarketAverage { get; set; }

        public int PercentMarketAverage { get; set; }

        public int DistanceFromDealer { get; set; }

        public bool HasModelConfigurationId { get; set; }

        public int ModelConfigurationId { get; set; }

        public bool HasIsCertified { get; set; }

        public bool IsCertified { get; set; }

        public int RowIndex { get; set; }

        #region Listing Info

        public string ListingSellerDescription { get; set; }

        public string ListingOptions { get; set; }

        public string ListingColor { get; set; }

        public string ListingStockNumber { get; set; }

        public string ListingVin { get; set; }

        #endregion
    }
}
