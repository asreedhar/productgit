﻿using System;

namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects
{
    [Serializable]
    public class SearchSummaryInfoDto
    {
        //precision
        public string Trim { get; set; }

        public string Engine { get; set; }

        public string Transmission { get; set; }

        public string DriveTrain { get; set; }

        public string Doors { get; set; }

        public string BodyType { get; set; }

        public string FuelType { get; set; }

        public string Segment { get; set; }

        //Used for overall
        public string YearMakeModel { get; set; }
    }
}