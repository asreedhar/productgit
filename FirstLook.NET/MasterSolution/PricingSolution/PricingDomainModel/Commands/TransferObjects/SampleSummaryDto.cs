﻿using System;

namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects
{
    [Serializable]
    public class SampleSummaryDto<T>
    {
        public bool HasMinimum { get; set; }

        public T Minimum { get; set; }

        public bool HasAverage { get; set; }

        public T Average { get; set; }

        public bool HasMaximum { get; set; }

        public T Maximum { get; set; }

        public int SampleSize { get; set; }
    }
}
