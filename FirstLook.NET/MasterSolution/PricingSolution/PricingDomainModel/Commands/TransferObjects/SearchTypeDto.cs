﻿using System;

namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects
{
    [Serializable]
    public enum SearchTypeDto
    {
        //TODO: Should I break this down into YMM, YMME, Custom as well?
        Overall = 1,

        Precision = 4 // Compat
    }
}
