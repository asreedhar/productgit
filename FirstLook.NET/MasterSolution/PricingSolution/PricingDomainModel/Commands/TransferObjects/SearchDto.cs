﻿using System;
using System.Collections.Generic;

namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects
{
    [Serializable]
    public class SearchDto
    {
        public string SearchHandle { get; set; }

        public string OwnerHandle { get; set; }

        public string VehicleHandle { get; set; }

        public int ModelYear { get; set; }

        public int MakeId { get; set; }

        public int LineId { get; set; }

        public int ModelConfigurationId { get; set; }

        public DistanceDto Distance { get; set; }

        public MileageDto LowMileage { get; set; }

        public MileageDto HighMileage { get; set; }

        public bool MatchColor { get; set; }

        public bool MatchCertified { get; set; }

        public string UpdateUser { get; set; }

        public DateTime UpdateDate { get; set; }

        public List<DistanceDto> Distances { get; set; }

        public List<MileageDto> Mileages { get; set; }

        public List<CatalogEntryDto> CatalogEntries { get; set; }

        public bool? PreviousYear { get; set; }

        public bool? NextYear { get; set; }

    }
}
