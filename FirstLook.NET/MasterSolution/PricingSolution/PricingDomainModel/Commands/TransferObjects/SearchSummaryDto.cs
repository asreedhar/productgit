﻿using System;

namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects
{
    [Serializable]
    public class SearchSummaryDto
    {
        public SearchTypeDto SearchType { get; set; }

        public string Description { get; set; }

        public int Units { get; set; }

        public int ComparableUnits { get; set; }

        public SampleSummaryDto<int> ListPrice { get; set; }

        public SampleSummaryDto<int> Mileage { get; set; }

        public bool IsPrimary { get; set; }

        public bool IsDefault { get; set; }

        public bool IsCertificationSpecified { get; set; }

        public bool IsColorSpecified { get; set; }

        public bool IsDoorsSpecified { get; set; }

        public bool IsDriveTrainSpecified { get; set; }

        public bool IsEngineSpecified { get; set; }

        public bool IsFuelTypeSpecified { get; set; }

        public bool IsSegmentSpecified { get; set; }

        public bool IsBodyTypeSpecified { get; set; }

        public bool IsTransmissionSpecified { get; set; }

        public bool IsTrimSpecified { get; set; }
    }
}
