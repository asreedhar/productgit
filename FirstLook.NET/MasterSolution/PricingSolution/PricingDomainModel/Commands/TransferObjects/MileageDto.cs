﻿using System;

namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects
{
    [Serializable]
    public class MileageDto
    {
        public int Value { get; set; }
    }
}
