﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects
{
    public class SortColumnMapper
    {
        public string BuildOrderByClause(IEnumerable<ISortColumn> sortColumns, ISet<string> validSortColumns)
        {
            if (sortColumns == null || validSortColumns == null ) 
                return string.Empty;

            var b = new StringBuilder();

            foreach(var col in sortColumns.Where(c => c != null && c.ColumnName != null))
            {
                if(!validSortColumns.Contains(col.ColumnName))
                    throw new InvalidOperationException(
                        string.Format("Invalid sort column '{0}'", col.ColumnName));

                if (b.Length > 0)
                    b.Append(", ");

                b.Append(col.ColumnName);
                b.Append(" ");
                b.Append(col.Ascending ? "ASC" : "DESC");
            }

            return b.ToString();
        }
    }
}