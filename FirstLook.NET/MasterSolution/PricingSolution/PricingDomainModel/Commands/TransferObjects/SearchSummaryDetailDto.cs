﻿using System;

namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects
{
    [Serializable]
    public class SearchSummaryDetailDto
    {
        public string Description { get; set; }

        public bool HasMarketDaySupply { get; set; }

        public int MarketDaySupply { get; set; }

        public bool IsReference { get; set; }

        public bool IsSearch { get; set; }

        public int NumberOfListings { get; set; }

        public SampleSummaryDto<int> ListPrice { get; set; }

        public SampleSummaryDto<int> Mileage { get; set; }
    }
}