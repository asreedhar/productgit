﻿using System;
using System.Collections.Generic;

namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects
{
    [Serializable]
    public class SearchDetailDto
    {
        public List<SearchSummaryDetailDto> SearchSummaryDetail { get; set; }

        public MarketPricingDto Pricing { get; set; }
    }
}
