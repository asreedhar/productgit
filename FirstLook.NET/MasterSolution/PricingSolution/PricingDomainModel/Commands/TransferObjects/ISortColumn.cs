namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects
{
    public interface ISortColumn
    {
        string ColumnName { get; }
        bool Ascending { get; }
    }
}