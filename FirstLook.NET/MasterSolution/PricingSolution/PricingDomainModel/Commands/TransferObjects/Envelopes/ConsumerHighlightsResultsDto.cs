﻿using System;


namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ConsumerHighlightsResultsDto
    {
        public ConsumerHighlightsDto ConsumerHighlights { get; set; }

        public ConsumerHighlightsArgumentsDto Arguments { get; set; }
    }
}



