﻿using System;

namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments required to preform an Inventory Reprice
    /// </summary>
    [Serializable]
    public class InventoryRepriceArgumentsDto
    {
        public string UserName { get; set; }

        public int InventoryId { get; set; }

        public int OldListPrice { get; set; }

        public int NewListPrice { get; set; }
    }
}
