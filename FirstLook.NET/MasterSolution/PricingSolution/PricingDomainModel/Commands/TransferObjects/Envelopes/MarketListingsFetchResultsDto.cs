﻿using System;
using System.Collections.Generic;

namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class MarketListingsFetchResultsDto
    {
        public List<MarketListingDto> MarketListings { get; set; }

        public MarketListingsFetchArgumentsDto Arguments { get; set; }

        public int TotalRowCount { get; set; }

        public int? HighlightedRowIndex { get; set; }
    }
}
