﻿using System;
using System.Collections.Generic;

namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ReferenceLookupResultsDto
    {
        public string[] SearchType { get; set; }

        public List<DistanceDto> Distances { get; set; }
    }
}
