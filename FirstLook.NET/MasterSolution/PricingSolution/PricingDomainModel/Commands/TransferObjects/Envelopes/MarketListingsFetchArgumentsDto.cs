﻿using System;
using System.Collections.Generic;

namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class MarketListingsFetchArgumentsDto
    {
        public int OwnerEntityTypeId { get; set; }

        public int OwnerEntityId { get; set; }

        public int VehicleEntityTypeId { get; set; }

        public int VehicleEntityId { get; set; }

        public string InsertUser { get; set; }

        public SearchTypeDto SearchType { get; set; }

        //Pagination Information
        public int MaximumRows { get; set; }

        public int StartRowIndex { get; set; }

        public List<SortColumnDto> SortColumns { get; set; }

        public bool HighlightVehicle { get; set; }
    }
}
