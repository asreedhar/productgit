﻿using System;

namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class MysteryShoppingLinksArgumentsDto
    {
        public string OwnerHandle { get; set; }

        public string SearchHandle { get; set; }

        public string VehicleHandle { get; set; }

        public int SearchRadius { get; set; }

        public int ModelYear { get; set; }
    }
}
