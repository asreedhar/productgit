﻿namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes
{
    public class MysteryShoppingParametersDto
    {
        public int OwnerEntityTypeId { get; set; }
        public int OwnerEntityId { get; set; }
        public int VehicleEntityTypeId { get; set; }
        public int VehicleEntityId { get; set; }

        public string UserId { get; set; }

        public int SearchRadius { get; set; }
//        public int ModelYear { get; set; }
    }
}
