﻿using System;
using System.Collections.Generic;

namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class MarketPricingListFetchResultsDto
    {
        public List<PairDto> PriceRanks { get; set; }
    }

    [Serializable]
    public struct PairDto
    {
        public int Price { get; set; }
        public int Mileage { get; set; }
    }
}
