﻿using System;

namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Results from an Inventory Reprice Command
    /// </summary>
    [Serializable]
    public class InventoryRepriceResultsDto
    {
        public int NewListPrice { get; set; }

        public int OldListPrice { get; set; }

        public string UserName { get; set; }

        public int InventoryId { get; set; }
    }
}
