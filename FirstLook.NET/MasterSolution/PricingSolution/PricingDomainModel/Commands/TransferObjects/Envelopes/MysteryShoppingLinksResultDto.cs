﻿using System;

namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class MysteryShoppingLinksResultDto
    {
        public MysteryShoppingLinksArgumentsDto Arguments { get; set; }

        public string AutoTraderUrl { get; set; }

        public string CarSoupUrl { get; set; }

        public string CarsDotComUrl { get; set; }
    }
}
