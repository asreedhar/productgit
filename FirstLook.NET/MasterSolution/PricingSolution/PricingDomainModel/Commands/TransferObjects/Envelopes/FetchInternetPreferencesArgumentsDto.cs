﻿using System;

namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class FetchInternetPreferencesArgumentsDto
    {
        public string OwnerHandle { get; set; }

        public string SearchHandle { get; set; }

        public string VehicleHandle { get; set; }
    }
}
