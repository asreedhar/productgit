﻿using System;

namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class FetchInternetPreferencesResultDto
    {
        public FetchInternetPreferencesArgumentsDto Arguments { get; set; }

        public bool HasMarketUpgrade { get; set; }

        public bool HasInternetAdvertising { get; set; }

        public bool HasMileageAdjustedSearch { get; set; }

        public decimal ExcludeLowPriceOutliersMultiplier { get; set; }

        public decimal ExcludeHighPriceOutliersMultiplier { get; set; }

        public bool ExcludeNoPriceFromCalc { get; set; }

        public bool SupressSellerName { get; set; }

        public bool SuppressTrimMatchStatus { get; set; }

		public bool HasJdPowerUpgrade { get; set; }
    }
}
