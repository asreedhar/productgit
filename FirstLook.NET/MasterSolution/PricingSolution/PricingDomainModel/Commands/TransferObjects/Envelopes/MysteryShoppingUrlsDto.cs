﻿namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes
{
    public class MysteryShoppingUrlsDto
    {
        public string AutoTraderUrl { get; set; }
        public string CarsUrl { get; set; }
        public string CarSoupUrl { get; set; }
    }
}
