using System;

namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class VehiclePricingDecisionInputDto
    {
        public int OwnerEntityTypeId { get; set; }
        public int OwnerEntityId { get; set; }
        public int VehicleEntityTypeId { get; set; }
        public int VehicleEntityId { get; set; }

        public string UserId { get; set; }

//        public int? AppraisalValue { get; set; }
        public int? EstimatedAdditionalCosts { get; set; }
        public int? TargetGrossProfit { get; set; }
    }
}