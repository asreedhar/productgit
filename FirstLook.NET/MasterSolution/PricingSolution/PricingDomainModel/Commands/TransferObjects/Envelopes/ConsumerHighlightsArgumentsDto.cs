﻿using System;


namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class ConsumerHighlightsArgumentsDto
    {
        public string OwnerHandle { get; set; }

        public string VehicleHandle { get; set; }
    }
}



