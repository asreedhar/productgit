﻿using System;
using System.Collections.Generic;

namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class SearchResultsDto
    {
        public List<SearchSummaryDto> SearchSummaries { get; set; }

        public SearchDetailDto Overall { get; set; }

        public SearchDetailDto Precision { get; set; }

        public SearchDto Search { get; set; }

        public SearchSummaryInfoDto SearchSummaryInfo { get; set; }
    }
}
