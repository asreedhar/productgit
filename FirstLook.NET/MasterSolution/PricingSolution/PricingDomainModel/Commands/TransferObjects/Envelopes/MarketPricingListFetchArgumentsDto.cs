﻿using System;

namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class MarketPricingListFetchArgumentsDto
    {
        public int OwnerEntityTypeId { get; set; }

        public int OwnerEntityId { get; set; }

        public int VehicleEntityTypeId { get; set; }

        public int VehicleEntityId { get; set; }

        public string InsertUser { get; set; }

        public SearchTypeDto SearchType { get; set; }
    }
}
