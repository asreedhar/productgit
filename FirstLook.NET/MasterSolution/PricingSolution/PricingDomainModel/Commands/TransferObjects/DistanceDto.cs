﻿using System;

namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects
{
    [Serializable]
    public class DistanceDto
    {
        public int Id { get; set; }

        public int Value { get; set; }
    }
}
