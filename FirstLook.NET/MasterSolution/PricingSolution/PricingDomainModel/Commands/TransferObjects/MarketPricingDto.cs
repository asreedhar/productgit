﻿using System;

namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects
{
    [Serializable]
    public class MarketPricingDto
    {
        public byte VehicleEntityTypeId { get; set; }

        public int VehicleEntityId { get; set; }

        public bool HasMileage { get; set; }

        public int Mileage { get; set; }

        public bool HasMarketDaySupply { get; set; }

        public int MarketDaySupply { get; set; }

        public bool HasInventoryUnitCost { get; set; }

        public int InventoryUnitCost { get; set; }

        public bool HasInventoryListPrice { get; set; }

        public int InventoryListPrice { get; set; }

        public bool HasPctAvgMarketPrice { get; set; }

        public int PctAvgMarketPrice { get; set; }

        public int NumListings { get; set; }

        public int NumComparableListings { get; set; }

        public bool HasAppraisalValue { get; set; }

        public int AppraisalValue { get; set; }

        public bool HasEstimatedReconditioningCost { get; set; }

        public int EstimatedReconditioningCost { get; set; }

        public bool HasTargetGrossProfit { get; set; }

        public int TargetGrossProfit { get; set; }

        public bool HasSearchRadius { get; set; }

        public int SearchRadius { get; set; }

        public string Vin { get; set; }

        public int VehicleCatalogID { get; set; }

        public SampleSummaryDto<int> MarketPrice { get; set; }

        public SampleSummaryDto<int> VehicleMileage { get; set; }

        public SampleSummaryDto<int> PctMarketAvg { get; set; }

        public SampleSummaryDto<int> GrossProfit { get; set; }

        public SampleSummaryDto<int> PctMarketValue { get; set; }

        public SampleSummaryDto<int> ComparableMarketPrice { get; set; }

        public SampleSummaryDto<int> JDPowerSalePrice { get; set; }

        public SampleSummaryDto<int> NaaaSalePrice { get; set; }
    }
}