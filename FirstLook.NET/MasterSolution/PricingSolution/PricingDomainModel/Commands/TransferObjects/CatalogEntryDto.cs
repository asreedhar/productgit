﻿using System;

namespace FirstLook.Pricing.DomainModel.Commands.TransferObjects
{
    [Serializable]
    public class CatalogEntryDto
    {
        public int ModelConfigurationId { get; set; }
    }
}
