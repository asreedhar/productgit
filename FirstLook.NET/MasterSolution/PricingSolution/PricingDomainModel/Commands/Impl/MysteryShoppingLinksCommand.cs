﻿using FirstLook.Common.Core.Command;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.MysteryShopping;


namespace FirstLook.Pricing.DomainModel.Commands.Impl
{
    class MysteryShoppingLinksCommand : ICommand<MysteryShoppingLinksResultDto, MysteryShoppingLinksArgumentsDto> 
    {
        public MysteryShoppingLinksResultDto Execute(MysteryShoppingLinksArgumentsDto args)
        {
            IMysteryShoppingWebServiceClient client = new MysteryShoppingWebServiceClient();

            return new MysteryShoppingLinksResultDto()
                       {
                           Arguments = args,
                           AutoTraderUrl = client.GetAutoTraderUrl(args.OwnerHandle, args.SearchHandle, args.VehicleHandle, args.SearchRadius, args.ModelYear),
                           CarsDotComUrl = client.GetCarsDotComUrl(args.OwnerHandle, args.SearchHandle, args.VehicleHandle, args.SearchRadius),
                           CarSoupUrl = client.GetCarSoupUrl(args.OwnerHandle, args.SearchHandle, args.VehicleHandle, args.SearchRadius, args.ModelYear)
                       };
        }




    }
}
