﻿using System;
using FirstLook.Common.Core.Command;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes;

namespace FirstLook.Pricing.DomainModel.Commands.Impl
{
    public class CommandFactory : ICommandFactory
    {
        public ICommand<SearchResultsDto, SearchArgumentsDto> CreateSearchCommand()
        {
            return new SearchCommand();
        }

        public ICommand<MarketListingsFetchResultsDto, MarketListingsFetchArgumentsDto> CreateMarketListingsFetchCommand()
        {
            return new MarketListingsFetchCommand();
        }

        public ICommand<SearchResultsDto, SearchUpdateArgumentsDto> CreateSearchUpdateCommand()
        {
            return new SearchUpdateCommand();
        }

        public ICommand<MarketPricingListFetchResultsDto, MarketPricingListFetchArgumentsDto> CreateMarketPricingListFetchCommand()
        {
            return new MarketPricingListFetchCommand();
        }

        public ICommand<ConsumerHighlightsResultsDto, ConsumerHighlightsArgumentsDto> CreateConsumerHighlightsFetchCommand()
        {
            return new ConsumerHighlightsFetchCommand();
        }

        public ICommand<InventoryRepriceResultsDto, InventoryRepriceArgumentsDto> CreateInventoryRepriceCommand()
        {
            return new InventoryRepriceCommand();
        }

        public ICommand<FetchInternetPreferencesResultDto, FetchInternetPreferencesArgumentsDto> CreateFetchInternetPreferencesCommand()
        {
            return new FetchInternetPreferencesCommand();
        }

        public ICommand<MysteryShoppingLinksResultDto, MysteryShoppingLinksArgumentsDto> CreateMysteryShoppingLinksCommand()
        {
            return new MysteryShoppingLinksCommand();
        }

        public ICommand<VehiclePricingDecisionInputDto> CreateSaveVehiclePricingDecisionInputCommand()
        {
            return new SaveVehiclePricingDecisionInputCommand();
        }

        public ICommand<VehiclePricingDecisionInputDto, VehiclePricingDecisionInputDto> CreateGetVehiclePricingDecisionInputCommand()
        {
            return new GetVehiclePricingDecisionInputCommand();
        }

        public ICommand<HandleDto, HandleDto> CreateHandleLookupCommand()
        {
            return new HandleLookupCommand();
        }

        public ICommand<ReferenceLookupResultsDto, ReferenceLookupArgumentsDto> CreateListingsReferenceCommand()
        {
            return new ReferenceLookupCommand();
        }

    }

   
}
