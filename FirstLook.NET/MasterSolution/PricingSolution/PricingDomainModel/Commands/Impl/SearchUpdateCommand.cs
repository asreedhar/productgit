﻿using System.Collections.Generic;
using System.Threading;
using FirstLook.Common.Core.Command;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes;
using FirstLook.Pricing.DomainModel.Internet.DataSource;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search;



namespace FirstLook.Pricing.DomainModel.Commands.Impl
{
    public class SearchUpdateCommand : ICommand<SearchResultsDto, SearchUpdateArgumentsDto>
    {
        public SearchResultsDto Execute(SearchUpdateArgumentsDto parameters)
        {
            string vehicleHandle, searchHandle;

            string ownerHandle = new OwnerHandleDataSource().FindByDealerId(parameters.OwnerEntityId);

            new HandleLookupDataSource().FindByOwnerHandleAndVehicle(ownerHandle,
                                                                     parameters.VehicleEntityTypeId,
                                                                     parameters.VehicleEntityId,
                                                                     parameters.InsertUser,
                                                                     out vehicleHandle,
                                                                     out searchHandle);


            Search.DataSource searchSource = new Search.DataSource();

            SearchKey key = new SearchKey(ownerHandle, vehicleHandle, searchHandle);

            //Get the search that is going to be updated
            Search search = searchSource.Select(ownerHandle, vehicleHandle, searchHandle);

            Search.SetSearchTypeId(parameters.Search.OwnerHandle, parameters.Search.SearchHandle, parameters.Search.VehicleHandle, (int) parameters.SearchType);

            Mapper.Merge(search, parameters.Search);

            search.UpdateUser = Thread.CurrentPrincipal.Identity.Name;
            
            //search.NextYear = parameters.Search.NextYear;                         //31105

            //search.PreviousYear = parameters.Search.PreviousYear;              //31105

            search.Save();

            //While updating , remove all the keys from memcache which were generated while getting the marketListing.----------------------
            PricingAnalyticsClient.ClearMemCache(ownerHandle);

            //Now build search results based on new search to be returned.

            //Get summary information
            SearchSummaryCollection summary = SearchSummaryCollection.GetSearchSummaryCollection(key);

            //Get detail information for overall and precision
            SearchSummaryDetailCollection.DataSource dataSource = new SearchSummaryDetailCollection.DataSource();

            SearchSummaryDetailCollection overall = dataSource.GetSearchSummaryDetails(ownerHandle, vehicleHandle, searchHandle, Mapper.ToInt(SearchTypeDto.Overall));

            SearchSummaryDetailCollection precision = dataSource.GetSearchSummaryDetails(ownerHandle, vehicleHandle, searchHandle, Mapper.ToInt(SearchTypeDto.Precision));

            SearchDetailDto overallDetail = new SearchDetailDto
            {
                SearchSummaryDetail = (List<SearchSummaryDetailDto>)Mapper.ToDtos(overall)
            };

            SearchDetailDto precisionDetail = new SearchDetailDto
            {
                SearchSummaryDetail = (List<SearchSummaryDetailDto>)Mapper.ToDtos(precision)
            };

            MarketPricingDataSource pricing = new MarketPricingDataSource();

            overallDetail.Pricing = Mapper.ToDto(pricing.FindByOwnerHandleAndVehicleHandleAndSearchHandle(ownerHandle, vehicleHandle, searchHandle, Mapper.ToInt(SearchTypeDto.Overall)));

            precisionDetail.Pricing = Mapper.ToDto(pricing.FindByOwnerHandleAndVehicleHandleAndSearchHandle(ownerHandle, vehicleHandle, searchHandle, Mapper.ToInt(SearchTypeDto.Precision)));

            //Get Search Summary Info
            SearchSummaryInfo searchSummaryInfo = SearchSummaryInfo.GetSearchSummaryInfo
            (
                ownerHandle,
                vehicleHandle,
                searchHandle,
                search
            );

            return new SearchResultsDto
            {
                Overall = overallDetail,
                Precision = precisionDetail,
                Search = Mapper.ToDto(search),
                SearchSummaries = (List<SearchSummaryDto>)Mapper.ToDtos(summary),
                SearchSummaryInfo = Mapper.ToDto(searchSummaryInfo)
            };
        }
    }
}
