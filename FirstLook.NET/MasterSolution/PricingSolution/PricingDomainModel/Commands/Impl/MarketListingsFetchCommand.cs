﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Command;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes;
using FirstLook.Pricing.DomainModel.Internet.DataSource;

namespace FirstLook.Pricing.DomainModel.Commands.Impl
{
    public class MarketListingsFetchCommand : ICommand<MarketListingsFetchResultsDto, MarketListingsFetchArgumentsDto>
    {
        private string _searchHandle;
        private string _ownerHandle;
        private string _vehicleHandle;

        private MarketListingsFetchArgumentsDto _parameters;

        private MarketListingsFetchResultsDto _results;
        private static readonly ISet<string> ValidSortColumns =
            new HashSet<string>( new[]
                                    {
                                        "VehicleID",
                                        "IsAnalyzedVehicle",
                                        "Seller",
                                        "Age",
                                        "VehicleDescription",
                                        "VehicleColor",
                                        "VehicleMileage",
                                        "ListPrice",
                                        "PctAvgMarketPrice",
                                        "DistanceFromDealer",
                                        "ModelConfigurationID",
                                        "ListingCertified"
                                    }, StringComparer.InvariantCultureIgnoreCase );

        public MarketListingsFetchResultsDto Execute (MarketListingsFetchArgumentsDto parameters)
        {
            _parameters = parameters;

            Reset();

            CreateHandles( _parameters.OwnerEntityId, _parameters.VehicleEntityTypeId, _parameters.VehicleEntityId, _parameters.InsertUser,
                            out _ownerHandle, out _vehicleHandle, out _searchHandle);

            PopulateHighlightedVehicle();

            PopulateListingsInformation();

            return _results;
        }

        private void Reset ()
        {
            _ownerHandle = string.Empty;
            _searchHandle = string.Empty;
            _vehicleHandle = string.Empty;

            _results = new MarketListingsFetchResultsDto() { HighlightedRowIndex = null, Arguments = _parameters };
        }


        internal void CreateHandles ( int ownerEntityId, int vehicleEntityTypeId, int vehicleEntityId, string insertUser,
                                        out string ownerHandle, out string vehicleHandle, out string searchHandle)
        {
            ownerHandle = new OwnerHandleDataSource().FindByDealerId( ownerEntityId );
            new HandleLookupDataSource().FindByOwnerHandleAndVehicle( ownerHandle,
                                                                     vehicleEntityTypeId,
                                                                     vehicleEntityId,
                                                                     insertUser,
                                                                     out vehicleHandle,
                                                                     out searchHandle );
        }

        private void PopulateHighlightedVehicle ()
        {
            if ( !_parameters.HighlightVehicle ) return;

            var highlightedRowIndex =
                new MarketListingDataSource().FindVehicleByOwnerHandleAndSearchHandleAndSortColumns( _ownerHandle,
                                                                                                     _searchHandle,
                                                                                                     Mapper.ToInt( _parameters.SearchType ),
                                                                                                     new SortColumnMapper().BuildOrderByClause( _parameters.SortColumns, ValidSortColumns ),
                                                                                                     _parameters.MaximumRows,
                                                                                                     _parameters.StartRowIndex );
            var pageNumber = highlightedRowIndex / _parameters.MaximumRows;
            _results.Arguments.StartRowIndex = pageNumber * _parameters.MaximumRows;
            _results.HighlightedRowIndex = highlightedRowIndex - _parameters.StartRowIndex;
        }

        private void PopulateListingsInformation ()
        {
            var listingsTable =
               new MarketListingDataSource().FindByOwnerHandleAndSearchHandle( _ownerHandle,
                                                                              _searchHandle,
                                                                              Mapper.ToInt( _parameters.SearchType ),
                                                                              new SortColumnMapper().BuildOrderByClause( _parameters.SortColumns, ValidSortColumns ),
                                                                              _parameters.MaximumRows,
                                                                              _parameters.StartRowIndex );

            _results.TotalRowCount = new MarketListingDataSource().CountByOwnerHandleAndSearchHandle( _ownerHandle, _searchHandle,
                                                                                                Mapper.ToInt(
                                                                                                    _parameters.
                                                                                                        SearchType ) );
            _results.MarketListings = (List<MarketListingDto>) Mapper.ToDtos( listingsTable );
        }
    }
}
