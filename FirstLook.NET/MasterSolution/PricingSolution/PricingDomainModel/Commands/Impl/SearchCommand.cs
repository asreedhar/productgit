﻿using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes;
using FirstLook.Pricing.DomainModel.Internet.DataSource;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search;

namespace FirstLook.Pricing.DomainModel.Commands.Impl
{
    public class SearchCommand : ICommand<SearchResultsDto, SearchArgumentsDto>
    {
        public SearchResultsDto Execute(SearchArgumentsDto parameters)
        {
            string vehicleHandle, searchHandle;
            int searchId;

            string ownerHandle = new OwnerHandleDataSource().FindByDealerId(parameters.OwnerEntityId);

            new HandleLookupDataSource().FindByOwnerHandleAndSearchId(ownerHandle,
                                                                       parameters.VehicleEntityTypeId,
                                                                       parameters.VehicleEntityId, out searchId);

            if (searchId > 0)
            {
                new HandleLookupDataSource().FindByOwnerHandleAndLoadSearchResults(ownerHandle,
                                                                     parameters.VehicleEntityTypeId,
                                                                     parameters.VehicleEntityId,
                                                                     out vehicleHandle,
                                                                     out searchHandle);
            }
            else
            {
                new HandleLookupDataSource().FindByOwnerHandleAndVehicle(ownerHandle,
                                                                     parameters.VehicleEntityTypeId,
                                                                     parameters.VehicleEntityId,
                                                                     parameters.InsertUser,
                                                                     out vehicleHandle,
                                                                     out searchHandle);
            }



            Search.DataSource searchSource = new Search.DataSource();

            SearchKey key = new SearchKey(ownerHandle, vehicleHandle, searchHandle);

            Search search = searchSource.Select(ownerHandle, vehicleHandle, searchHandle);

            //Get Search Summary Info
            SearchSummaryInfo searchSummaryInfo = SearchSummaryInfo.GetSearchSummaryInfo
                (
                    ownerHandle,
                    vehicleHandle,
                    searchHandle,
                    search
                );

            //Get summary information
            SearchSummaryCollection summary = SearchSummaryCollection.GetSearchSummaryCollection(key);

            //Get detail information for overall and precision
            SearchSummaryDetailCollection.DataSource dataSource = new SearchSummaryDetailCollection.DataSource();

            SearchSummaryDetailCollection overall = dataSource.GetSearchSummaryDetails(ownerHandle, vehicleHandle, searchHandle, Mapper.ToInt(SearchTypeDto.Overall));

            SearchSummaryDetailCollection precision = dataSource.GetSearchSummaryDetails(ownerHandle, vehicleHandle, searchHandle, Mapper.ToInt(SearchTypeDto.Precision));

            SearchDetailDto overallDetail = new SearchDetailDto
                                                {
                                                    SearchSummaryDetail = (List<SearchSummaryDetailDto>) Mapper.ToDtos(overall)
                                                };

            SearchDetailDto precisionDetail = new SearchDetailDto
                                                  {
                                                      SearchSummaryDetail = (List<SearchSummaryDetailDto>) Mapper.ToDtos(precision)
                                                  };

            MarketPricingDataSource pricing = new MarketPricingDataSource();

            overallDetail.Pricing = Mapper.ToDto(pricing.FindByOwnerHandleAndVehicleHandleAndSearchHandle(ownerHandle, vehicleHandle, searchHandle, Mapper.ToInt(SearchTypeDto.Overall)));

            precisionDetail.Pricing = Mapper.ToDto(pricing.FindByOwnerHandleAndVehicleHandleAndSearchHandle(ownerHandle, vehicleHandle, searchHandle, Mapper.ToInt(SearchTypeDto.Precision)));

            return new SearchResultsDto
                       {
                           Overall = overallDetail,
                           Precision = precisionDetail,
                           Search = Mapper.ToDto(search),
                           SearchSummaries = (List<SearchSummaryDto>) Mapper.ToDtos(summary),
                           SearchSummaryInfo = Mapper.ToDto(searchSummaryInfo)
                       };
        }
    }
}
