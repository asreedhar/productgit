using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Csla;
using FirstLook.Common.Core;
using FirstLook.DomainModel.Lexicon.Categorization;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search;
using FirstLook.Common.Core.Data;

namespace FirstLook.Pricing.DomainModel.Commands.Impl
{
    class Mapper
    {
        #region Mileage

        public static MileageDto ToDto(Mileage obj)
        {
            if (obj == null)
            {
                return null;
            }

            return new MileageDto
            {
                Value = obj.Value
            };
        }

        public static IList<MileageDto> ToDtos(IList<Mileage> objs)
        {
            return objs.Select(ToDto).ToList();
        }


        #endregion

        #region Distance

        public static DistanceDto ToDto(Distance obj)
        {
            if (obj == null)
            {
                return null;
            }

            return new DistanceDto
            {
                Id = obj.Id,
                Value = obj.Value
            };
        }

        public static IList<DistanceDto> ToDtos(IList<Distance> objs)
        {
            return objs.Select(ToDto).ToList();
        }

        #endregion

        #region Search

        public static SearchDto ToDto(Search obj)
        {
            if (obj == null)
            {
                return null;
            }

            return new SearchDto
            {
                Distance = ToDto(obj.Distance),
                Distances = (List<DistanceDto>)ToDtos(obj.Distances),
                HighMileage = ToDto(obj.HighMileage),
                LowMileage = ToDto(obj.LowMileage),
                LineId = obj.LineId,
                MakeId = obj.MakeId,
                MatchCertified = obj.MatchCertified,
                MatchColor = obj.MatchColor,
                Mileages = (List<MileageDto>)ToDtos(obj.Mileages),
                ModelConfigurationId = obj.ModelConfigurationId,
                ModelYear = obj.ModelYear,
                UpdateDate = obj.UpdateDate,
                UpdateUser = obj.UpdateUser,
                SearchHandle = obj.Id.SearchHandle,
                VehicleHandle = obj.Id.VehicleHandle,
                OwnerHandle = obj.Id.OwnerHandle,
                CatalogEntries = (List<CatalogEntryDto>)ToDtos(obj.CatalogEntries)//,
                //PreviousYear=obj.PreviousYear,
                //NextYear = obj.NextYear
            };
        }

        public static void Merge(Search obj, SearchDto dto)
        {
            if (dto == null)
            {
                return;
            }

            if (dto.Distance != null)
            {
                obj.Distance = (from dst in obj.Distances
                                where dst.Id == dto.Distance.Id
                                select dst).FirstOrDefault();
            }

            if (dto.HighMileage != null)
            {
                obj.HighMileage = Mileage.NewMileage(dto.HighMileage.Value); 
            }
            else
            {
                obj.HighMileage = null;
            }

            if (dto.LowMileage != null)
            {
                obj.LowMileage = (from ml in obj.Mileages
                                  where ml.Value == dto.LowMileage.Value
                                  select ml).FirstOrDefault();
            }

            obj.MatchCertified = dto.MatchCertified;

            obj.MatchColor = dto.MatchColor;

            obj.UpdateUser = dto.UpdateUser;

            //if (dto.PreviousYear != null)
            //{
            //    obj.PreviousYear = dto.PreviousYear;

            //}
            //if (dto.NextYear != null)
            //{
            //    obj.NextYear = dto.NextYear;

            //}

            //See if catalog entries have been changed...
            IList<CatalogEntry> toDel = obj.CatalogEntries.Where(o => false == dto.CatalogEntries.Any(d => o.Id == d.ModelConfigurationId)).ToList().ToList();

            foreach (CatalogEntry entry in toDel)
            {
                obj.CatalogEntries.Remove(entry.Id);
            }

            //Add in missing catalog entries
            foreach (IGrouping<bool, CatalogEntryDto> group in dto.CatalogEntries.GroupBy(d => obj.CatalogEntries.Any(o => o.Id == d.ModelConfigurationId)).ToList())
            {
                bool isInsert = !group.Key;

                if (isInsert)
                {
                    foreach (CatalogEntryDto catalogEntryDto in group)
                    {
                        if (!obj.CatalogEntries.Contains(catalogEntryDto.ModelConfigurationId))
                        {
                            obj.CatalogEntries.Assign(catalogEntryDto.ModelConfigurationId);
                        }
                    }
                }
            }

        }

        #endregion

        #region MarketListing

        public static IList<MarketListingDto> ToDtos(DataTable table)
        {
            IList<MarketListingDto> dtos = new List<MarketListingDto>();

            DataTableReader reader = table.CreateDataReader();

            int currentRowIndex = 0;

            while (reader.Read())
            {
                MarketListingDto dto = new MarketListingDto();

                int? age = reader.GetNullableInt32("Age");

                dto.RowIndex = currentRowIndex++;

                if (age != null)
                {
                    dto.HasAge = true;
                    dto.Age = age.GetValueOrDefault();
                }

                dto.DistanceFromDealer = reader.GetInt32(reader.GetOrdinal("DistanceFromDealer"));

                bool? isCertified = reader.GetBoolean(reader.GetOrdinal("ListingCertified"));

                if (isCertified != null)
                {
                    dto.HasIsCertified = true;
                    dto.IsCertified = isCertified.Value;
                }

                int? listPrice = reader.GetNullableInt32("ListPrice");

                if (listPrice != null)
                {
                    dto.HasListPrice = true;
                    dto.ListPrice = listPrice.GetValueOrDefault();
                }

                int? modelConfigurationId = reader.GetNullableInt32("ModelConfigurationID");

                if (modelConfigurationId != null)
                {
                    dto.HasModelConfigurationId = true;
                    dto.ModelConfigurationId = modelConfigurationId.GetValueOrDefault();
                }

                int? pctAvtMarketPrice = reader.GetNullableInt32("PctAvgMarketPrice");

                if (pctAvtMarketPrice != null)
                {
                    dto.HasPercentMarketAverage = true;
                    dto.PercentMarketAverage = pctAvtMarketPrice.GetValueOrDefault();
                }

                int? vehicleMileage = reader.GetNullableInt32("VehicleMileage");

                if (vehicleMileage != null)
                {
                    dto.HasVehicleMileage = true;
                    dto.VehicleMileage = vehicleMileage.GetValueOrDefault();
                }

                dto.Seller = reader.GetString("Seller");

                dto.VehicleDescription = reader.GetString("VehicleDescription");

                dto.VehicleColor = reader.GetString("VehicleColor");

                dto.ListingSellerDescription = reader.GetString("ListingSellerDescription");

                dto.ListingOptions = reader.GetString("ListingOptions");

                dto.ListingColor = reader.GetString("ListingVehicleColor");

                dto.ListingStockNumber = reader.GetString("ListingStockNumber");

                dto.ListingVin = reader.GetString("ListingVin");

                dtos.Add(dto);
            }

            return dtos;
        }

        #endregion

        #region MarketPricingList

        public static IList<PairDto> ToPricingDtos(DataTable table)
        {
            IList<PairDto> dtos = new List<PairDto>();

            DataTableReader reader = table.CreateDataReader();

            while (reader.Read())
            {
                int price = reader.GetInt32(reader.GetOrdinal("ListPrice"));

                int mileage = reader.GetInt32(reader.GetOrdinal("Mileage"));

                PairDto dto = new PairDto { Mileage = mileage, Price = price };

                dtos.Add(dto);
            }

            return dtos;
        }

        #endregion

        #region SearchSummary

        public static SearchSummaryDto ToDto(SearchSummary obj)
        {
            if (obj == null)
            {
                return null;
            }

            return new SearchSummaryDto
            {
                ComparableUnits = obj.ComparableUnits,
                Description = obj.Description,
                IsBodyTypeSpecified = obj.IsBodyTypeSpecified,
                IsCertificationSpecified = obj.IsCertificationSpecified,
                IsColorSpecified = obj.IsColorSpecified,
                IsDefault = obj.IsDefaultSearch,
                IsDoorsSpecified = obj.IsDoorsSpecified,
                IsDriveTrainSpecified = obj.IsDriveTrainSpecified,
                IsEngineSpecified = obj.IsEngineSpecified,
                IsFuelTypeSpecified = obj.IsFuelTypeSpecified,
                IsPrimary = obj.IsPrimarySearch,
                IsSegmentSpecified = obj.IsSegmentSpecified,
                IsTransmissionSpecified = obj.IsTransmissionSpecified,
                IsTrimSpecified = obj.IsTrimSpecified,
                ListPrice = ToDto(obj.ListPrice),
                Mileage = ToDto(obj.Mileage),
                SearchType = ToDto(obj.SearchType),
                Units = obj.Units
            };
        }

        public static IList<SearchSummaryDto> ToDtos(ReadOnlyListBase<SearchSummaryCollection, SearchSummary> objs)
        {
            return objs.Select(ToDto).ToList();
        }

        #endregion

        #region SampleSummary

        public static SampleSummaryDto<int> ToDto(SampleSummary<int> obj)
        {
            if (obj == null)
            {
                return null;
            }

            SampleSummaryDto<int> dto = new SampleSummaryDto<int>();

            if (obj.Maximum != null)
            {
                dto.HasMaximum = true;
                dto.Maximum = obj.Maximum.GetValueOrDefault();
            }

            if (obj.Average != null)
            {
                dto.HasAverage = true;
                dto.Average = obj.Average.GetValueOrDefault();
            }

            if (obj.Minimum != null)
            {
                dto.HasMinimum = true;
                dto.Minimum = obj.Minimum.GetValueOrDefault();
            }

            dto.SampleSize = obj.SampleSize;

            return dto;
        }

        public static SampleSummaryDto<int> ToDto(int? min, int? avg, int? max)
        {
            SampleSummaryDto<int> sampleSummaryDto = new SampleSummaryDto<int>();

            if (min != null)
            {
                sampleSummaryDto.HasMinimum = true;
                sampleSummaryDto.Minimum = min.GetValueOrDefault();
            }

            if (avg != null)
            {
                sampleSummaryDto.HasAverage = true;
                sampleSummaryDto.Average = avg.GetValueOrDefault();
            }

            if (max != null)
            {
                sampleSummaryDto.HasMaximum = true;
                sampleSummaryDto.Maximum = max.GetValueOrDefault();
            }

            return sampleSummaryDto;
        }

        #endregion

        #region SearchType

        public static SearchTypeDto ToDto(SearchType obj)
        {
            SearchTypeDto dto;

            switch (obj)
            {
                case SearchType.Precision:
                    dto = SearchTypeDto.Precision;
                    break;
                case SearchType.YearMakeModel:
                    dto = SearchTypeDto.Overall;
                    break;
                default:
                    dto = SearchTypeDto.Overall;
                    break;
            }

            return dto;
        }

        public static int ToInt(SearchTypeDto dto)
        {
            switch (dto)
            {
                case SearchTypeDto.Precision:
                    return 4;
                case SearchTypeDto.Overall:
                    return 1;
                default:
                    return 0;
            }
        }

        #endregion

        #region SearchSummaryDetail

        public static SearchSummaryDetailDto ToDto(SearchSummaryDetail obj)
        {
            if (obj == null)
            {
                return null;
            }

            SearchSummaryDetailDto dto = new SearchSummaryDetailDto
            {
                ListPrice = new SampleSummaryDto<int>
                {
                    HasAverage = true,
                    Average = obj.AverageListPrice
                },
                Mileage = new SampleSummaryDto<int>
                {
                    HasAverage = true,
                    Average = obj.AverageVehicleMileage
                },
                HasMarketDaySupply = true,
                MarketDaySupply = obj.MarketDaySupply,
                Description = obj.Description,
                IsReference = obj.IsReference,
                IsSearch = obj.IsSearch,
                NumberOfListings = obj.NumberOfListings
            };

            return dto;
        }

        public static IList<SearchSummaryDetailDto> ToDtos(ReadOnlyListBase<SearchSummaryDetailCollection, SearchSummaryDetail> objs)
        {
            return objs.Select(ToDto).ToList();
        }

        #endregion

        #region CatalogEntry

        public static CatalogEntryDto ToDto(CatalogEntry obj)
        {
            if (obj == null)
            {
                return null;
            }

            return new CatalogEntryDto
            {
                ModelConfigurationId = obj.Id
            };
        }

        public static IList<CatalogEntryDto> ToDtos(IList<CatalogEntry> objs)
        {
            return objs.Select(ToDto).ToList();
        }

        #endregion

        #region MarketPricing

        public static MarketPricingDto ToDto(DataTable table)
        {
            MarketPricingDto dto = new MarketPricingDto();

            DataTableReader reader = table.CreateDataReader();

            if (reader.Read())
            {
                dto.VehicleEntityTypeId = reader.GetByte(reader.GetOrdinal("VehicleEntityTypeID"));

                dto.VehicleEntityId = reader.GetInt32(reader.GetOrdinal("VehicleEntityID"));

                int? mileage = reader.GetNullableInt32("VehicleMileage");

                if (mileage != null)
                {
                    dto.HasMileage = true;
                    dto.Mileage = mileage.GetValueOrDefault();
                }

                int? marketDaySupply = reader.GetNullableInt32("MarketDaySupply");

                if (marketDaySupply != null)
                {
                    dto.HasMarketDaySupply = true;
                    dto.MarketDaySupply = marketDaySupply.GetValueOrDefault();
                }

                int? minMarketPrice = reader.GetNullableInt32("MinMarketPrice");

                int? avgMarketPrice = reader.GetNullableInt32("AvgMarketPrice");

                int? maxMarketPrice = reader.GetNullableInt32("MaxMarketPrice");

                dto.MarketPrice = ToDto(minMarketPrice, avgMarketPrice, maxMarketPrice);

                int? minMileage = reader.GetNullableInt32("MinVehicleMileage");

                int? avgMileage = reader.GetNullableInt32("AvgVehicleMileage");

                int? maxMileage = reader.GetNullableInt32("MaxVehicleMileage");

                dto.VehicleMileage = ToDto(minMileage, avgMileage, maxMileage);

                int? inventoryUnitCost = reader.GetNullableInt32("InventoryUnitCost");

                if (inventoryUnitCost != null)
                {
                    dto.HasInventoryUnitCost = true;
                    dto.InventoryUnitCost = inventoryUnitCost.GetValueOrDefault();
                }

                int? inventoryListPrice = reader.GetNullableInt32("InventoryListPrice");

                if (inventoryListPrice != null)
                {
                    dto.HasInventoryListPrice = true;
                    dto.InventoryListPrice = inventoryListPrice.GetValueOrDefault();
                }

                int? minPctMktAvg = reader.GetNullableInt32("MinPctMarketAvg");

                int? maxPctMktAvg = reader.GetNullableInt32("MaxPctMarketAvg");

                dto.PctMarketAvg = ToDto(minPctMktAvg, null, maxPctMktAvg);

                int? minGrossProfit = reader.GetNullableInt32("MinGrossProfit");

                dto.GrossProfit = ToDto(minGrossProfit, null, null);

                int? minPctMktValue = reader.GetNullableInt32("MinPctMarketValue");

                int? maxPctMktValue = reader.GetNullableInt32("MaxPctMarketValue");

                dto.PctMarketValue = ToDto(minPctMktValue, null, maxPctMktValue);

                int? pctAvgMarketPrice = reader.GetNullableInt32("PctAvgMarketPrice");

                if (pctAvgMarketPrice != null)
                {
                    dto.HasPctAvgMarketPrice = true;
                    dto.PctAvgMarketPrice = pctAvgMarketPrice.GetValueOrDefault();
                }

                int? minComparableMarketPrice = reader.GetNullableInt32("MinComparableMarketPrice");

                int? maxComparableMarketPrice = reader.GetNullableInt32("MaxComparableMarketPrice");

                dto.ComparableMarketPrice = ToDto(minComparableMarketPrice, null, maxComparableMarketPrice);

                dto.NumListings = reader.GetInt32(reader.GetOrdinal("NumListings"));

                dto.NumComparableListings = reader.GetInt32(reader.GetOrdinal("NumComparableListings"));

                int? appraisalValue = reader.GetNullableInt32("AppraisalValue");

                if (appraisalValue != null)
                {
                    dto.HasAppraisalValue = true;
                    dto.AppraisalValue = appraisalValue.GetValueOrDefault();
                }

                int? estimatedReconCost = reader.GetNullableInt32("EstimatedReconditioningCost");

                if (estimatedReconCost != null)
                {
                    dto.HasEstimatedReconditioningCost = true;
                    dto.EstimatedReconditioningCost = estimatedReconCost.GetValueOrDefault();
                }

                int? targetGrossProfit = reader.GetNullableInt32("TargetGrossProfit");

                if (targetGrossProfit != null)
                {
                    dto.HasTargetGrossProfit = true;
                    dto.TargetGrossProfit = targetGrossProfit.GetValueOrDefault();
                }

                dto.Vin = reader.GetString("Vin");

                int? jdPowerAvgSalesPrice = reader.GetNullableInt32("JDPowerAverageSalePrice");

                dto.JDPowerSalePrice = ToDto(null, jdPowerAvgSalesPrice, null);

                int? naaaAvgSalesPrice = reader.GetNullableInt32("NaaaAverageSalePrice");

                dto.NaaaSalePrice = ToDto(null, naaaAvgSalesPrice, null);

                int? searchRadius = reader.GetNullableInt32("SearchRadius");

                if (searchRadius != null)
                {
                    dto.HasSearchRadius = true;
                    dto.SearchRadius = searchRadius.GetValueOrDefault();
                }
            }

            return dto;
        }

        #endregion

        #region SearchSummaryInfo

        public static SearchSummaryInfoDto ToDto(SearchSummaryInfo obj)
        {
            if (obj == null)
            {
                return null;
            }

            const string seriesTitle = "Trim";
            const string engineTitle = "Engine";
            const string transmissionTitle = "Transmission";
            const string driveTrainTitle = "Drive Train";
            const string passengerDoorTitle = "Doors";
            const string bodyTypeTitle = "Body Type";
            const string fuelTypeTile = "Fuel Type";
            const string segmentTitle = "Segment";
           // const string modelYearTitle = "Model Year";

            string segment = null;
            string bodyType = null;

            if (obj.Catalog.Segments.Count == 1 && (obj.Catalog.Segments[0].IsTruck || obj.Catalog.Segments[0].IsVan))
            {
                bodyType = GetAttributeText(obj.BodyTypes, obj.Catalog.BodyTypes, typeof(BodyType), bodyTypeTitle);
            }
            else
            {
                segment = GetAttributeText(obj.Segments, obj.Catalog.Segments, typeof(Segment), segmentTitle);
            }

            return new SearchSummaryInfoDto
            {
                BodyType = bodyType,
                Doors = GetAttributeText(obj.PassengerDoors, obj.Catalog.PassengerDoors, typeof(PassengerDoor), passengerDoorTitle),
                DriveTrain = GetAttributeText(obj.DriveTrains, obj.Catalog.DriveTrains, typeof(DriveTrain), driveTrainTitle),
                Engine = GetAttributeText(obj.Engines, obj.Catalog.Engines, typeof(Engine), engineTitle),
                FuelType = GetAttributeText(obj.FuelTypes, obj.Catalog.FuelTypes, typeof(FuelType), fuelTypeTile),
                Segment = segment,
                Transmission = GetAttributeText(obj.Transmissions, obj.Catalog.Transmissions, typeof(Transmission), transmissionTitle),
                Trim = GetAttributeText(obj.Series, obj.Catalog.Series, typeof(Series), seriesTitle),
                YearMakeModel=obj.YearMakeModel
              //  YearMakeModel = GetAttributeText(obj.YearMakeModel, obj.Catalog.ModelYear, typeof(ModelYear), modelYearTitle)
            };
        }

        private static string GetAttributeText(ICollection<INameable> searchSelections, ICollection catalog, Type type, string title)
        {
            const string allTitle = "All ";

            if (searchSelections.Count == catalog.Count && catalog.Count != 1)
            {
                return allTitle + title + (title.EndsWith("s") ? "es" : "s");
            }
            return GetNames(searchSelections, type, title);
        }

        private static string GetNames(ICollection<INameable> namableList, Type type, string title)
        {
            List<string> names = new List<string>();

            Dictionary<string, string> replacements = new Dictionary<string, string>();

            if (namableList.Count > 0 && type == typeof(Engine))
            {
                replacements.Add("Cylinder", "Cyl");
                replacements.Add("Engine", string.Empty);
            }

            if (namableList.Count > 0 && type == typeof(BodyType))
            {
                replacements.Add("pickup", string.Empty);
                replacements.Add("Pickup", string.Empty);
            }

            foreach (INameable nameable in namableList)
            {
                if (nameable == null)
                {
                    names.Add("Other " + title);
                    continue;
                }

                string name = nameable.Name;

                if (replacements.Count > 0)
                {
                    foreach (KeyValuePair<string, string> replacement in replacements)
                    {
                        name = name.Replace(replacement.Key, replacement.Value);
                    }
                }

                if (string.IsNullOrEmpty(name))
                {
                    names.Add("Base " + title);
                }
                else
                {
                    names.Add(name);
                }
            }

            string suffix = string.Empty;

            if (namableList.Count > 0 && type == typeof(PassengerDoor))
            {
                suffix = " Doors";
            }

            names.RemoveAll(string.IsNullOrEmpty);

            names.Sort();

            return string.Join(", ", names.ToArray()) + suffix;
        }

        #endregion
    }
}
