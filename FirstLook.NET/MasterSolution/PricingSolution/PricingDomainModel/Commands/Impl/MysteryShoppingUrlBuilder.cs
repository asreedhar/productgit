﻿using System.Data;
using System.Text;
using FirstLook.Pricing.DomainModel.Internet.DataSource;

namespace FirstLook.Pricing.DomainModel.Commands.Impl
{

    public static class MysteryShoppingUrlBuilder
    {
        private const int AutoTrader = 4;
        private const int CarSoup = 5;
        private const int CarsDotCom = 2;

        public static string AutoTraderUrl(string oh, string sh, string vh, int modelYear, int searchRadius)
        {
            DataTable table = new ListingProviderMakeModelDataSource().FindByOwnerHandleAndVehicleHandleAndProvider(oh,sh,vh,AutoTrader);

            if (table != null && table.Rows.Count > 0)
            {
                DataRow row = table.Rows[0];    // take the first row

                // TODO: Pull url from (MysteryShoppingConfiguration.AutoTraderUrl()
                StringBuilder autoTraderUrl = new StringBuilder();
                autoTraderUrl.Append("http://www.autotrader.com/fyc/searchresults.jsp?advanced=&num_records=25&certified=&isp=y&search=y&lang=en&search_type=used&min_price=&max_price=&rdpage=100");
                autoTraderUrl.Append("&make=").Append(row["ProviderMake"]);
                autoTraderUrl.Append("&model=").Append(row["ProviderModel"]);
                autoTraderUrl.Append("&start_year=").Append(modelYear);
                autoTraderUrl.Append("&end_year=").Append(modelYear);
                autoTraderUrl.Append("&distance=").Append(searchRadius);
                autoTraderUrl.Append("&address=").Append(row["ZipCode"]);
                return autoTraderUrl.ToString();
            }

            return string.Empty;
        }

        public static string CarSoupUrl(string oh, string sh, string vh, int modelYear, int searchRadius)
        {
            DataTable table = new ListingProviderMakeModelDataSource().FindByOwnerHandleAndVehicleHandleAndProvider(oh, sh, vh, CarSoup);

            if (table != null && table.Rows.Count > 0)
            {
                DataRow row = table.Rows[0];    // take the first row
                StringBuilder carsoupUrl = new StringBuilder();

                // TODO: Pull url from (MysteryShoppingConfiguration.CarSoupUrl()
                carsoupUrl.Append("http://www.carsoup.com/used/summary.asp?marketid=89&setid=1&&invtypeid=2&certified=0&SortCol=Distance&sort=Asc&zipCodeSearch=true&7Days=&WithPrices=&WithPhotos=&AllMarkets=&minPrice=&maxPrice=&minMileage=&maxMileage=&dealerID=&rDealerGroupID=&vehicleTypeID=1&bodyStyleTypeID=&thrifties=0&WithHandicap=&BL=&btnZip.x=16&btnZip.y=0");
                carsoupUrl.Append("&makeid=").Append(row["ProviderMake"]);
                carsoupUrl.Append("&modelid=").Append(row["ProviderModel"]);
                carsoupUrl.Append("&minyear=").Append(modelYear);
                carsoupUrl.Append("&maxyear=").Append(modelYear);
                carsoupUrl.Append("&radius=").Append(searchRadius);
                carsoupUrl.Append("&zipcode=").Append(row["ZipCode"]);
                return carsoupUrl.ToString();
            }

            return string.Empty;

        }

        public static string CarsUrl(string oh, string sh, string vh, int searchRadius)
        {
            DataTable table = new ListingProviderMakeModelDataSource().FindByOwnerHandleAndVehicleHandleAndProvider(oh, sh, vh, CarsDotCom);

            if (table != null && table.Rows.Count > 0)
            {
                DataRow row = table.Rows[0];    // take the first row

                // TODO: Pull url from (MysteryShoppingConfiguration.CarsDotComUrl()
                string carsDotComUrl = "http://www.cars.com/go/search/search_results.jsp?numResultsPerPage=25&makeid=%make%&dgid=&pageNumber=0&zc=%zip%&cid=&amid=&certifiedOnly=false&sortorder=descending&searchType=22&criteria=K-%7CE-ALL%7CM-_%make%_%7CH-%7CD-_%model%_%7CN-N%7CR-%distance%%7CI-1%7CP-PRICE+descending%7CQ-descending%7CY-_%year%_%7CX-popular%7CZ-%zip%&modelid=%model%&tracktype=usedcc&sortfield=PRICE+descending&aff=national&dlid=&cname=&largeNumResultsPerPage=500&paId=&aff=national";
                carsDotComUrl = carsDotComUrl.Replace("%make%", row["ProviderMake"].ToString());
                carsDotComUrl = carsDotComUrl.Replace("%model%", row["ProviderModel"].ToString());
                carsDotComUrl = carsDotComUrl.Replace("%zip%", row["ZipCode"].ToString());
                carsDotComUrl = carsDotComUrl.Replace("%distance%", searchRadius.ToString());
                carsDotComUrl = carsDotComUrl.Replace("%year%", row["VehicleYear"].ToString());
                return carsDotComUrl;
            }

            return string.Empty;
        }

    }

    
}