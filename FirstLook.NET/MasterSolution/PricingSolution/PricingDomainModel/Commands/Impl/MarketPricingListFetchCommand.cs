﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Command;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes;
using FirstLook.Pricing.DomainModel.Internet.DataSource;

namespace FirstLook.Pricing.DomainModel.Commands.Impl
{
    public class MarketPricingListFetchCommand : ICommand<MarketPricingListFetchResultsDto, MarketPricingListFetchArgumentsDto>
    {
        public MarketPricingListFetchResultsDto Execute(MarketPricingListFetchArgumentsDto parameters)
        {
            string vehicleHandle, searchHande;

            string ownerHandle = new OwnerHandleDataSource().FindByDealerId(parameters.OwnerEntityId);
            Console.WriteLine("OwnerHandle: {0}", ownerHandle);

            new HandleLookupDataSource().FindByOwnerHandleAndVehicle(ownerHandle,
                                                                     parameters.VehicleEntityTypeId,
                                                                     parameters.VehicleEntityId,
                                                                     parameters.InsertUser,
                                                                     out vehicleHandle,
                                                                     out searchHande);


            DataTable table =
                new MarketPricingDataSource().FindListByOwnerHandleAndSearchHandle(ownerHandle,
                                                                                   searchHande,
                                                                                   Mapper.ToInt(parameters.SearchType));


            return new MarketPricingListFetchResultsDto
                       {
                           PriceRanks = (List<PairDto>) Mapper.ToPricingDtos(table)
                       };
        }
    }
}
