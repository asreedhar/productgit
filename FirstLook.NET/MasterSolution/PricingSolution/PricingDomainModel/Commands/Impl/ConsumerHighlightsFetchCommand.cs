﻿using FirstLook.Common.Core.Command;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Document;


namespace FirstLook.Pricing.DomainModel.Commands.Impl
{
    ///// <summary>
    ///// A command that gets consumer highlights for a vehicle
    ///// </summary>
    public class ConsumerHighlightsFetchCommand : ICommand<ConsumerHighlightsResultsDto, ConsumerHighlightsArgumentsDto>
    {
        public ConsumerHighlightsResultsDto Execute(ConsumerHighlightsArgumentsDto parameters)
        {
            string vehicleHandle = parameters.VehicleHandle;
            string ownerHandle = parameters.OwnerHandle;

            Advertisement.DataSource dataSource = new Advertisement.DataSource();

            ConsumerHighlightsDto highlights = new ConsumerHighlightsDto { Body = dataSource.Select(ownerHandle, vehicleHandle).Body };
     
            return new ConsumerHighlightsResultsDto
            {
                Arguments = parameters,
                ConsumerHighlights = highlights
            };
        }
    }
}
