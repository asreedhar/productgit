﻿using FirstLook.Common.Core.Command;
using FirstLook.DomainModel.Oltp;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel;

namespace FirstLook.Pricing.DomainModel.Commands.Impl
{
    class FetchInternetPreferencesCommand : ICommand<FetchInternetPreferencesResultDto, FetchInternetPreferencesArgumentsDto> 
    {
        public FetchInternetPreferencesResultDto Execute(FetchInternetPreferencesArgumentsDto parameters)
        {
            OwnerPreferences ownerPreferences = OwnerPreferences.GetOwnerPreferences(parameters.OwnerHandle);

            PackageDetails packageDetails = PackageDetails.GetDealerPricingPackageDetails(parameters.OwnerHandle,
                                                                                          parameters.SearchHandle,
                                                                                          parameters.VehicleHandle);

            int businessUnitId = Identity.GetDealerId(parameters.OwnerHandle);

            BusinessUnitFinder businessUnitFinder = new BusinessUnitFinder();

            BusinessUnit businessUnit = businessUnitFinder.Find(businessUnitId);

            bool hasMarketUpgrade = businessUnit.HasDealerUpgrade(Upgrade.Marketing);

            bool hasJdPowerUpgrade = businessUnit.HasDealerUpgrade(Upgrade.JDPowerUsedCarMarketData);

            return new FetchInternetPreferencesResultDto()
                       {
                           Arguments = parameters,
                           SuppressTrimMatchStatus = ownerPreferences.SuppressTrimMatchStatus,
                           SupressSellerName = ownerPreferences.SuppressSellerName,
                           ExcludeHighPriceOutliersMultiplier = ownerPreferences.ExcludeHighPriceOutliersMultiplier,
                           ExcludeLowPriceOutliersMultiplier = ownerPreferences.ExcludeLowPriceOutliersMultiplier,
                           ExcludeNoPriceFromCalc = ownerPreferences.ExcludeNoPriceFromCalc,
                           HasInternetAdvertising = packageDetails.HasInternetAdvertisement,
                           HasMileageAdjustedSearch = packageDetails.HasMileageAdjustedSearch,
                           HasMarketUpgrade = hasMarketUpgrade,
						   HasJdPowerUpgrade = hasJdPowerUpgrade
                       };
        }
    }
}
