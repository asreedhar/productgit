﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.Command;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes;
using OrignalInventoryRepriceCommand=FirstLook.Pricing.DomainModel.Internet.ObjectModel.InventoryRepriceCommand;

namespace FirstLook.Pricing.DomainModel.Commands.Impl
{
    class InventoryRepriceCommand : ICommand<InventoryRepriceResultsDto, InventoryRepriceArgumentsDto>
    {
        /// <summary>
        /// Proxies the Reprice command to the original impl, PM 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public InventoryRepriceResultsDto Execute(InventoryRepriceArgumentsDto parameters)
        {
            OrignalInventoryRepriceCommand.Execute(parameters.UserName, parameters.InventoryId,
                                                   parameters.OldListPrice, parameters.NewListPrice, false);
                // Hard Coded "confirmed" to match previous settings, PM

            return new InventoryRepriceResultsDto()
                       {
                           InventoryId = parameters.InventoryId,
                           OldListPrice = parameters.OldListPrice,
                           NewListPrice = parameters.NewListPrice,
                           UserName = parameters.UserName
                       };
        }
    }
}
