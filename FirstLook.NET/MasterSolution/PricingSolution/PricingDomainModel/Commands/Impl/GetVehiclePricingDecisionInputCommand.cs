﻿using FirstLook.Common.Core.Command;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel;

namespace FirstLook.Pricing.DomainModel.Commands.Impl
{
    public class GetVehiclePricingDecisionInputCommand : ICommand<VehiclePricingDecisionInputDto, VehiclePricingDecisionInputDto>
    {
        public VehiclePricingDecisionInputDto Execute(VehiclePricingDecisionInputDto args)
        {
            var handles = new HandleLookupCommand().Execute(args.OwnerEntityId, args.OwnerEntityTypeId,
                                                              args.VehicleEntityId, args.VehicleEntityTypeId,
                                                              args.UserId);

            VehiclePricingDecisionInput savedResults = VehiclePricingDecisionInput.GetVehiclePricingDecisionInput(handles.OwnerHandle, handles.VehicleHandle);

            var results = new VehiclePricingDecisionInputDto
                {
                    TargetGrossProfit = savedResults.TargetGrossProfit,
                    EstimatedAdditionalCosts = savedResults.EstimatedAdditionalCosts,
                    OwnerEntityId = args.OwnerEntityId,
                    OwnerEntityTypeId = args.OwnerEntityTypeId,
                    UserId = args.UserId,
                    VehicleEntityId = args.VehicleEntityId,
                    VehicleEntityTypeId = args.VehicleEntityTypeId
                };

            return results;
        }
    }
}
