﻿using FirstLook.Common.Core.Command;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel;

namespace FirstLook.Pricing.DomainModel.Commands.Impl
{
    public class SaveVehiclePricingDecisionInputCommand : ICommand<VehiclePricingDecisionInputDto>
    {
        public void Execute(VehiclePricingDecisionInputDto args)
        {

            var handles = new HandleLookupCommand().Execute(args.OwnerEntityId, args.OwnerEntityTypeId,
                                                  args.VehicleEntityId, args.VehicleEntityTypeId,
                                                  args.UserId);

            var input = VehiclePricingDecisionInput.GetVehiclePricingDecisionInput(handles.OwnerHandle, handles.VehicleHandle);
            input.EstimatedAdditionalCosts = args.EstimatedAdditionalCosts;
            input.TargetGrossProfit = args.TargetGrossProfit;
            input.Save();
        }
    }
}
