﻿using FirstLook.Common.Core.Command;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects;
using FirstLook.Pricing.DomainModel.Internet.DataSource;

namespace FirstLook.Pricing.DomainModel.Commands.Impl
{
    public class HandleLookupCommand : ICommand<HandleDto,HandleDto>
    {
        public HandleDto Execute(int ownerEntityId, int ownerEntityTypeId, int vehicleEntityId, int vehicleEntityTypeId, string theUserId)
        {
            string ownerHandle = new OwnerHandleDataSource().FindByDealerId(ownerEntityId);
            string vehicleHandle, searchHandle;

            string userId = string.IsNullOrEmpty(theUserId)
                                ? "HandleLookupCommand"
                                : theUserId;

            new HandleLookupDataSource().FindByOwnerHandleAndVehicle(ownerHandle,
                                                                     vehicleEntityTypeId,
                                                                     vehicleEntityId,
                                                                     userId,
                                                                     out vehicleHandle,
                                                                     out searchHandle);

            var result = new HandleDto
                {
                    OwnerEntityId = ownerEntityId,
                    OwnerEntityTypeId = ownerEntityTypeId,
                    UserId = userId,
                    VehicleEntityId = vehicleEntityId,
                    VehicleEntityTypeId = vehicleEntityTypeId,
                    OwnerHandle = ownerHandle,
                    SearchHandle = searchHandle,
                    VehicleHandle = vehicleHandle
                };

            return result;
        }

        public HandleDto Execute(HandleDto parameters)
        {
            return Execute(parameters.OwnerEntityId, parameters.OwnerEntityTypeId, parameters.VehicleEntityId,
                           parameters.VehicleEntityTypeId, parameters.UserId);

        }
    }
}
