﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes;
using FirstLook.Pricing.DomainModel.Internet.DataSource;

namespace FirstLook.Pricing.DomainModel.Commands.Impl
{
    public class ReferenceLookupCommand : ICommand<ReferenceLookupResultsDto, ReferenceLookupArgumentsDto>
    {
        public ReferenceLookupResultsDto Execute(ReferenceLookupArgumentsDto parameters)
        {
            var results = new ReferenceLookupResultsDto
                              {
                                  Distances =
                                      (List<DistanceDto>)
                                      Mapper.ToDtos(new ReferenceLookupDataSource().GetDistanceCollection()),
                                  SearchType = Enum.GetNames(typeof (SearchTypeDto))
                              };

            return results;
        }
    }
}
