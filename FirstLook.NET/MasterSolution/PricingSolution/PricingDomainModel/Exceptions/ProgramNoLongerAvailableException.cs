﻿using System;

namespace FirstLook.Pricing.DomainModel.Exceptions
{
    public class ProgramNoLongerAvailableException : Exception
    {
        public ProgramNoLongerAvailableException()
        {
        }

        public ProgramNoLongerAvailableException(string message)
            : base(message)
        {
        }

        public ProgramNoLongerAvailableException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
