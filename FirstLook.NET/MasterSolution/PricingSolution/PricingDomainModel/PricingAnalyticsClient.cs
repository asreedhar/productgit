﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using MAX.Caching;
using System.Web;

namespace FirstLook.Pricing.DomainModel
{
    public static class PricingAnalyticsClient
    {
        //private static readonly ILog Log = LogManager.GetLogger(typeof(ReportAnalyticsClient).FullName);

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly ICacheKeyBuilder CacheKeyBuilder = new CacheKeyBuilder();
        private static readonly ICacheWrapper CacheWrapper = new MemcachedClientWrapper();
        private const int CacheHours = 1;
        public static IList<string> MemCacheKeys = new List<string>();        

        public static TValue GetReportData<TValue>(List<string> key) where TValue : class
        {
            string typeName = typeof(TValue).Name;
            key.Add(typeName);
            string cacheKey = CacheKeyBuilder.CacheKey(key);
          
            var obj = CacheWrapper.Get<TValue>(cacheKey);

            return obj;
        }

        public static TValue GetReportDataAndCacheKeys<TValue>(List<string> key, string ownerHandle) where TValue : class
        {
            string typeName = typeof(TValue).Name;
            key.Add(typeName);
            string cacheKey = CacheKeyBuilder.CacheKey(key);

            var obj = CacheWrapper.Get<TValue>(cacheKey);


            //For Market Listings we need to cache all the keys.
            if (MemCacheKeys != null && !MemCacheKeys.Contains(cacheKey)) MemCacheKeys.Add(cacheKey);
            CacheWrapper.Set("MemCacheKeys" + ownerHandle, MemCacheKeys, DateTime.Now.AddHours(CacheHours));  

            return obj;
        }
        public static TValue GetReportData<TValue>(string key) where TValue : class
        {
            var obj = CacheWrapper.Get<TValue>(key);
            return obj;
        }
       

        public static void SetReportData<T>(List<string> key, T data) where T : class
        {
            string typeName = typeof(T).Name;
            key.Add(typeName);
            string cacheKey = CacheKeyBuilder.CacheKey(key);

            // Put it in the cache for an hour
            CacheWrapper.Set(cacheKey, data, DateTime.Now.AddHours(CacheHours));
        }

        public static void SetReportData<T>(string key, T data) where T : class
        {
            // Put null in cache for that particular key(reset)
            CacheWrapper.Set(key, data, DateTime.Now.AddHours(CacheHours));
        }      

        public static void SetReportDataAndCacheKeys<T>(List<string> key, T data,string ownerHandle) where T : class
        {
            string typeName = typeof(T).Name;
            key.Add(typeName);
            string cacheKey = CacheKeyBuilder.CacheKey(key);

            // Put it in the cache for an hour
            CacheWrapper.Set(cacheKey, data, DateTime.Now.AddHours(CacheHours));

            //For Market Listings we need to cache all the keys.
            if(MemCacheKeys!= null && !MemCacheKeys.Contains(cacheKey)) MemCacheKeys.Add(cacheKey);
            CacheWrapper.Set("MemCacheKeys"+ownerHandle, MemCacheKeys, DateTime.Now.AddHours(CacheHours));           
        }

        public static string GetKey<T>(List<string> key, T data) where T : class
        {
            string typeName = typeof(T).Name;
            key.Add(typeName);
            return CacheKeyBuilder.CacheKey(key);
        }

        public static List<string> GetParameterList(Dictionary<string, object> parameterValues)
        {
            List<string> lstParameterList = parameterValues.Select(x => Convert.ToString(x.Value)).ToList<string>();
            return lstParameterList;
        }

        public static void ClearMemCache(string ownerHandle)
        {                       
            try
            {
                MemCacheKeys = GetReportData<List<string>>("MemCacheKeys"+ownerHandle);
            }
            catch (Exception ex)
            {
                Log.Error("An error occured while implementing Memcached Client in ClearMemCache : " + ex.Message);
            }

            if (MemCacheKeys != null)
            {
                foreach (string singelkey in MemCacheKeys)
                {
                    try
                    {
                        SetReportData<DataTable>(singelkey, null);
                    }
                    catch (Exception ex)
                    {
                        Log.Error("An error occured while implementing Memcached Client in ClearMemCache : " + ex.Message);
                    }
                }
                MemCacheKeys = new List<string>();        
            }
            else
            {
                MemCacheKeys = new List<string>();        
            }
        }
    }
}
