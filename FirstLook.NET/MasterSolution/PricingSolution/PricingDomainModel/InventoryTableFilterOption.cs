namespace FirstLook.Pricing.DomainModel
{
    public class InventoryTableFilterOption
    {
        private string label;
        private string value;

        public InventoryTableFilterOption(string label, string value)
        {
            this.label = label;
            this.value = value;
        }

        public string Label
        {
            get { return label; }
            set { label = value; }
        }

        public string Value
        {
            get { return value; }
            set { this.value = value; }
        }
    }
}