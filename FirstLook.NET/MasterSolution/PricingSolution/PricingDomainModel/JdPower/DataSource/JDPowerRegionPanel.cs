using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using Csla;
using FirstLook.Common.Data;
using FirstLook.Pricing.DomainModel.JDPower.ObjectModel;

namespace FirstLook.Pricing.DomainModel.JDPower.DataSource
{
    [Serializable]
    public class JDPowerRegionPanel : ReadOnlyBase<JDPowerRegionPanel>
    {

        public static string HttpContextKey = "JDPowerRegionPanel";

        private readonly Guid guid;
        private JDPowerRegion powerRegion;
        private JDPowerRegion rollUpRegion;

        public JDPowerRegion PowerRegion
        {
            get { return powerRegion; }
            set { powerRegion = value; }
        }

        public JDPowerRegion RollUpRegion
        {
            get { return rollUpRegion; }
            set { rollUpRegion = value; }
        }

        protected override object GetIdValue()
        {
            return guid;
        }

        public JDPowerRegionPanel()
        {
            guid = Guid.NewGuid();
        }

        #region Data Access

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA: Invoked through reflection")]
        private void DataPortal_Fetch(Criteria businessUnitId)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection("Market"))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "JDPower.GetRegionByBusinessUnit";
                    command.AddParameterWithValue("BusinessUnitId", DbType.Int32, false, businessUnitId.BusinessUnitId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            powerRegion = new JDPowerRegion();
                            powerRegion.Id = reader.GetInt32(reader.GetOrdinal("PowerRegionId"));
                            powerRegion.Name = reader.GetString(reader.GetOrdinal("PowerRegionName"));

                            rollUpRegion = new JDPowerRegion();
                            rollUpRegion.Name = reader.GetString(reader.GetOrdinal("RollUpRegionName"));
                        }
                    }
                }
            }
        }

        #endregion

        public static JDPowerRegionPanel GetPanel(int businessUnitId)
        {
            return DataPortal.Fetch<JDPowerRegionPanel>(new Criteria(businessUnitId));
        }


        [Serializable]
        private class Criteria
        {
            private readonly int businessUnitId;

            public Criteria(Int32 id)
            {
                businessUnitId = id;
            }

            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
            public int BusinessUnitId
            {
                get { return businessUnitId; }
            }
        }
    }
}