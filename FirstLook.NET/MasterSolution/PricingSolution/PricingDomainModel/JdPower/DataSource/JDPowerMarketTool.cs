using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using FirstLook.Common.Core.Utilities;
using FirstLook.Common.Data;
using FirstLook.DomainModel.Oltp;

namespace FirstLook.Pricing.DomainModel.JDPower.DataSource
{
    public class JDPowerMarketTool
    {
        #region Private Constants

        private static readonly DataTable RegionParams = EnumHelper.EnumToDataTable(typeof(JDPowerFranchise));

        private static readonly DataTableCallback NullCallback = new DataTableCallback();

        #endregion

        #region Model Name
        public static string SelectModelName(int vehicleCatalogId)
        {
            string result = "UNKNOWN";
            if (vehicleCatalogId > 0)
            {
                using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.InventoryDatabase))
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "dbo.GetMakeModelByVehicleCatalogID";
                        command.AddParameterWithValue("VehicleCatalogId", DbType.Int32, false, vehicleCatalogId);
                        using (IDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                                result = reader.GetString(reader.GetOrdinal("make")) + " " +
                                         reader.GetString(reader.GetOrdinal("model"));
                        }
                    }
                }
            }
            return result;
        }
        #endregion Model Name

        #region Model Years

        [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "Is an ObjectDataSource select-method")]
        public static IList<int> SelectModelYears(int vehicleCatalogId)
        {
            List<int> modelYears = new List<int>();
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "JDPower.GetModelYearByVehicle";
                    command.AddParameterWithValue("VehicleCatalogId", DbType.Int32, false, vehicleCatalogId);
                    using (IDataReader reader = command.ExecuteReader())
                    {
                        int minModelYear = 0;
                        int maxModelYear = 0;

                        while (reader.Read())
                        {
                            minModelYear = reader.GetInt32(reader.GetOrdinal("MinModelYear"));
                            maxModelYear = reader.GetInt32(reader.GetOrdinal("MaxModelYear"));
                        }
                        
                        if (minModelYear == 0)
                            minModelYear = 1995;
                        if (maxModelYear == 0)
                            maxModelYear = DateTime.Today.Year + 1;

                        for (int i = minModelYear; i <= maxModelYear; i++)
                        {
                            modelYears.Add(i);
                        }
                    }
                }
            }
            return modelYears;
        }

        #endregion Model Years

        #region Available Franchise Grouping

        [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "Is an ObjectDataSource select-method")]
        public DataTable SelectAvailableFranchiseGroups(BusinessUnit dealer)
        {
            DataTable regions = RegionParams.Clone();
            foreach (DataRow row in RegionParams.Rows)
            {
                string rowName = Convert.ToString(row["Name"], CultureInfo.InvariantCulture);
                DataRow newRow = regions.NewRow();

                newRow["Name"] = rowName;
                newRow["Value"] = row["Value"];

                regions.Rows.Add(newRow);
            }
            return regions;
        }

        #endregion Available Franchise Grouping

        #region Franchise List

        [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "Is an ObjectDataSource select-method")]
        public IList<string> SelectFranchises(BusinessUnit dealer)
        {
            List<string> franchises = new List<string>();

            if (dealer != null)
            {
                using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.InventoryDatabase))
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "dbo.GetDealerFranchises";
                        command.AddParameterWithValue("BusinessUnitId", DbType.Int32, false, dealer.Id);
                        using (IDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                franchises.Add(reader.GetString(reader.GetOrdinal("FranchiseDescription")));
                            }
                        }
                    }
                }
            }
            return franchises;
        }

        #endregion

    }
}
