using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;
using FirstLook.DomainModel.Oltp;
using FirstLook.Pricing.DomainModel.JDPower.ObjectModel;

namespace FirstLook.Pricing.DomainModel.JDPower.DataSource
{
    public class JDPowerSalesReportPanel : ReadOnlyBase<JDPowerSalesReportPanel>
    {
        private readonly Guid _guid;

        private JDPowerSalesReport _salesReport;

        protected override object GetIdValue()
        {
            return _guid;
        }

        public static JDPowerSalesReport GetSalesReport(int periodId, int minMileageBucketId, int maxMileageBucketId, int geographicAggregationLevel, int maxVehicleYear, int minVehicleYear, int vehicleCatalogId, int similarFranchises, BusinessUnit dealer)
        {
            return DataPortal.Fetch<JDPowerSalesReportPanel>(new Criteria(periodId, minMileageBucketId, maxMileageBucketId, geographicAggregationLevel, maxVehicleYear, minVehicleYear, vehicleCatalogId, similarFranchises, dealer.Id))._salesReport;
        }

        protected virtual void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection seConnection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (seConnection.State == ConnectionState.Closed)
                    seConnection.Open();

                using (IDataCommand seCommand = new DataCommand(seConnection.CreateCommand()))
                {
                    seCommand.CommandType = CommandType.StoredProcedure;
                    seCommand.CommandText = "JDPower.GetSalesReport";
                    seCommand.AddParameterWithValue("BusinessUnitId", DbType.Int32, false, criteria.BusinessUnitId);
                    seCommand.AddParameterWithValue("SimilarFranchises", DbType.Int16, false, criteria.SimilarFranchises);
                    seCommand.AddParameterWithValue("VehicleCatalogId", DbType.Int32, false, criteria.VehicleCatalogId);
                    seCommand.AddParameterWithValue("MinVehicleYear", DbType.Int32, false, criteria.MinVehicleYear);
                    seCommand.AddParameterWithValue("MaxVehicleYear", DbType.Int32, false, criteria.MaxVehicleYear);
                    seCommand.AddParameterWithValue("GeographicAggregationLevel", DbType.Int32, false, criteria.GeographicAggregationLevel);
                    seCommand.AddParameterWithValue("MinMileageBucketId", DbType.Int32, false, criteria.MinMileageBucketId);
                    seCommand.AddParameterWithValue("MaxMileageBucketId", DbType.Int32, false, criteria.MaxMileageBucketId);
                    seCommand.AddParameterWithValue("PeriodId", DbType.Int32, false, criteria.PeriodId);

                    using (IDataReader seReader = seCommand.ExecuteReader())
                    {
                        if (seReader.Read())
                        {
                            string query = seReader.GetString(seReader.GetOrdinal("Query"));

                            using (IDataConnection asConnection = SimpleQuery.ConfigurationManagerConnection(Database.JDPowerDatabase))
                            {
                                if (asConnection.State == ConnectionState.Closed)
                                    asConnection.Open();

                                using (IDbCommand asCommand = asConnection.CreateCommand())
                                {
                                    asCommand.CommandType = CommandType.Text;
                                    asCommand.CommandText = query;
                                    
                                    using (IDataReader asReader = asCommand.ExecuteReader())
                                    {
                                        _salesReport = asReader.Read()
                                               ? JDPowerSalesReport.GetSalesReport(asReader)
                                               : JDPowerSalesReport.NewSalesReport();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public JDPowerSalesReportPanel()
        {
            _guid = Guid.NewGuid();
        }

        protected class Criteria
        {
            private readonly int _businessUnitId;
            private readonly int _similarFranchises;
            private readonly int _vehicleCatalogId;
            private readonly int _minVehicleYear;
            private readonly int _maxVehicleYear;
            private readonly int _geographicAggregationLevel;
            private readonly int _minMileageBucketId;
            private readonly int _maxMileageBucketId;
            private readonly int _periodId;

            public Criteria(int periodId, int minMileageBucketId, int maxMileageBucketId, int geographicAggregationLevel, int maxVehicleYear, int minVehicleYear, int vehicleCatalogId, int similarFranchises, int businessUnitId)
            {
                _periodId = periodId;
                _maxMileageBucketId = maxMileageBucketId;
                _minMileageBucketId = minMileageBucketId;
                _geographicAggregationLevel = geographicAggregationLevel;
                _maxVehicleYear = maxVehicleYear;
                _minVehicleYear = minVehicleYear;
                _vehicleCatalogId = vehicleCatalogId;
                _similarFranchises = similarFranchises;
                _businessUnitId = businessUnitId;
            }

            public int BusinessUnitId
            {
                get { return _businessUnitId; }
            }

            public int SimilarFranchises
            {
                get { return _similarFranchises; }
            }

            public int VehicleCatalogId
            {
                get { return _vehicleCatalogId; }
            }

            public int MinVehicleYear
            {
                get { return _minVehicleYear; }
            }

            public int MaxVehicleYear
            {
                get { return _maxVehicleYear; }
            }

            public int GeographicAggregationLevel
            {
                get { return _geographicAggregationLevel; }
            }

            public int MinMileageBucketId
            {
                get { return _minMileageBucketId; }
            }

            public int MaxMileageBucketId
            {
                get { return _maxMileageBucketId; }
            }

            public int PeriodId
            {
                get { return _periodId; }
            }
        }
    }
}
