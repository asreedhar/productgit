using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Csla;
using FirstLook.Common.Data;
using FirstLook.Pricing.DomainModel.JDPower.ObjectModel;

namespace FirstLook.Pricing.DomainModel.JDPower.DataSource
{
    public class JDPowerTimePeriodPanel : ReadOnlyBase<JDPowerTimePeriodPanel>
    {
         private readonly Guid guid;
        private TimePeriodCollection timePeriodCollection;

        public TimePeriodCollection TimePeriodCollection
        {
            get { return timePeriodCollection; }
            set { timePeriodCollection = value; }
        }

        protected override object GetIdValue()
        {
            return guid;
        }

        public static JDPowerTimePeriodPanel GetPanel()
        {
            return DataPortal.Fetch<JDPowerTimePeriodPanel>();
        }

        private void DataPortal_Fetch()
        {
            
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "JDPower.GetPeriod";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        timePeriodCollection = TimePeriodCollection.GetTimePeriodCollection(reader);
                    }
                }
            }            
        }


        public JDPowerTimePeriodPanel()
        {
            guid = Guid.NewGuid();
        }

    }


}
