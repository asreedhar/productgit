using System.ComponentModel;

namespace FirstLook.Pricing.DomainModel.JDPower.DataSource
{
    public enum JDPowerFranchise
    {
        [Description("All Dealers")]
        Overall = 0,

        [Description("Similar Franchises")]
        SimilarFranchises
    }
}