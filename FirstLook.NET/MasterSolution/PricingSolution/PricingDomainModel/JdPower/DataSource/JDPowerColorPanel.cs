using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Csla;
using FirstLook.Common.Data;
using FirstLook.Pricing.DomainModel.JDPower.ObjectModel;

namespace FirstLook.Pricing.DomainModel.JDPower.DataSource
{
    public class JDPowerColorPanel : ReadOnlyBase<JDPowerColorPanel>
    {

        private readonly Guid guid;
        private ColorCollection colors;

        public ColorCollection Colors
        {
            get { return colors; }
            set { colors = value; }
        }

        protected override object GetIdValue()
        {
            return guid;
        }

        public static JDPowerColorPanel GetPanel()
        {
            return DataPortal.Fetch<JDPowerColorPanel>();
        }

        private void DataPortal_Fetch()
        {
            
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "JDPower.GetColor";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        colors = ColorCollection.GetColorCollection(reader);
                    }
                }
            }            
        }


        public JDPowerColorPanel()
        {
            guid = Guid.NewGuid();
        }

    }

}
