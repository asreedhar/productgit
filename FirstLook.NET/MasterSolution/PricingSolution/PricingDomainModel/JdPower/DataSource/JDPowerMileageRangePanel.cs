using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Text;
using Csla;
using FirstLook.Common.Data;
using FirstLook.Pricing.DomainModel.JDPower.ObjectModel;

namespace FirstLook.Pricing.DomainModel.JDPower.DataSource
{
    public class JDPowerMileageRangePanel : ReadOnlyBase<JDPowerMileageRangePanel>
    {
        private readonly Guid guid;
        private MileageRangeCollection mileageRanges;

        public MileageRangeCollection MileageRanges
        {
            get { return mileageRanges; }
            set { mileageRanges = value; }
        }

        protected override object GetIdValue()
        {
            return guid;
        }

        public static JDPowerMileageRangePanel GetPanel()
        {
            return DataPortal.Fetch<JDPowerMileageRangePanel>();
        }

        private void DataPortal_Fetch()
        {
            
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "JDPower.GetMileageBucket";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        mileageRanges = MileageRangeCollection.GetMileageRangeCollection(reader);
                    }
                }
            }            
        }


        public JDPowerMileageRangePanel()
        {
            guid = Guid.NewGuid();
        }

    }
}