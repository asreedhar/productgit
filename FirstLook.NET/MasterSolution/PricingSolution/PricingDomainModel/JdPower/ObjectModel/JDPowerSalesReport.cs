using System.Data;

namespace FirstLook.Pricing.DomainModel.JDPower.ObjectModel
{
    public class JDPowerSalesReport 
    {
        private decimal _averageAcv;
        private decimal _averageSellingPrice;
        private decimal _averageRetailGrossProfit;
        private decimal _averageDaysToSale;
        private decimal _averageMileage;
        private decimal _grossFandI;

        public decimal AverageACV
        {
            get { return _averageAcv; }
            set { _averageAcv = value; }
        }

        public decimal AverageSellingPrice
        {
            get { return _averageSellingPrice; }
            set { _averageSellingPrice = value; }
        }

        public decimal AverageRetailGrossProfit
        {
            get { return _averageRetailGrossProfit; }
            set { _averageRetailGrossProfit = value; }
        }

        public decimal AverageDaysToSale
        {
            get { return _averageDaysToSale; }
            set { _averageDaysToSale = value; }
        }

        public decimal AverageMileage
        {
            get { return _averageMileage; }
            set { _averageMileage = value; }
        }

        public decimal GrossFandI
        {
            get { return _grossFandI; }
            set { _grossFandI = value; }
        }

        private JDPowerSalesReport()
        {
            // default to zeros
        }

        private JDPowerSalesReport(IDataRecord reader)
        {
            Fetch(reader);
        }

        private static decimal GetColumnAsDecimal(IDataRecord reader, string columnName)
        {
            decimal result = 0;

            if (!reader.IsDBNull(reader.GetOrdinal(columnName)))
            {
                result = reader.GetDecimal(reader.GetOrdinal(columnName));
            }
 
            return result;
        }

        private static decimal Average(IDataRecord record, string numeratorName, string denominatorName)
        {
            decimal numerator = GetColumnAsDecimal(record, numeratorName);

            decimal denominator = GetColumnAsDecimal(record, denominatorName);

            if (denominator > 0)
            {
                return numerator/denominator;
            }

            return 0;
        }

        private void Fetch(IDataRecord record)
        {
            _averageSellingPrice = Average(record,
                "[Measures].[SellingPrice]",
                "[Measures].[SalesCount]");
            _averageAcv = Average(record,
                "[Measures].[TradeActualCashValue]",
                "[Measures].[TradeCount]");
            _averageRetailGrossProfit = Average(record,
                "[Measures].[RetailGrossProfit]",
                "[Measures].[HasRetailProfit]");
            _averageDaysToSale = Average(record,
                "[Measures].[DaysToSale]",
                "[Measures].[HasDaysToSale]");
            _averageMileage = Average(record,
                "[Measures].[Mileage]",
                "[Measures].[HasMileage]");
            _grossFandI = Average(record,
                "[Measures].[GrossFinanceAndInsurance]",
                "[Measures].[SalesCount]");
        }

        public static JDPowerSalesReport GetSalesReport(IDataRecord record)
        {
            return new JDPowerSalesReport(record);
        }

        public static JDPowerSalesReport NewSalesReport()
        {
            return new JDPowerSalesReport();
        }
    }
}
