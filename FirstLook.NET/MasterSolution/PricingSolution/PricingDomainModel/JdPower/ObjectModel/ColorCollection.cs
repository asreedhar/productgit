using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Csla;

namespace FirstLook.Pricing.DomainModel.JDPower.ObjectModel
{
    public class ColorCollection : ReadOnlyListBase<ColorCollection, Color>
    {
        private ColorCollection(IDataReader reader)
        {
            Fetch(reader);
        }

        private void Fetch(IDataReader reader)
        {
            IsReadOnly = false;
            while(reader.Read())
            {
                int id = reader.GetInt32(reader.GetOrdinal("ColorId"));
                String label = reader.GetString(reader.GetOrdinal("Color"));
                Add(new Color(id, label));
            }
            IsReadOnly = true;
        }

        public static ColorCollection GetColorCollection(IDataReader reader)
        {
            return new ColorCollection(reader);
        }
    }
}
