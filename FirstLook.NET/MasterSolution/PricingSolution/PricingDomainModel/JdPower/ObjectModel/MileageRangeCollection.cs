using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Text;
using Csla;

namespace FirstLook.Pricing.DomainModel.JDPower.ObjectModel
{
    public class MileageRangeCollection : ReadOnlyListBase<MileageRangeCollection, MileageRange>
    {

        private MileageRangeCollection(IDataReader reader)
        {
            Fetch(reader);
        }

        private void Fetch(IDataReader reader)
        {
            IsReadOnly = false;
            while(reader.Read())
            {
                int id = reader.GetInt32(reader.GetOrdinal("MileageBucketId"));
                int min = reader.GetInt32(reader.GetOrdinal("MinMileage"));
                int max = reader.GetInt32(reader.GetOrdinal("MaxMileage"));
                string description = reader.GetString(reader.GetOrdinal("MileageDescription"));

                Add(new MileageRange(id, min, max,description));
            }
            IsReadOnly = true;
        }

        public static MileageRangeCollection GetMileageRangeCollection(IDataReader reader)
        {
            return new MileageRangeCollection(reader);
        }
    }
}
