using System;
using System.Data;
using Csla;

namespace FirstLook.Pricing.DomainModel.JDPower.ObjectModel
{
    public class TimePeriodCollection : ReadOnlyListBase<TimePeriodCollection, TimePeriod>
    {
        private TimePeriodCollection(IDataReader reader)
        {
            Fetch(reader);
        }

        private void Fetch(IDataReader reader)
        {
            IsReadOnly = false;
            while(reader.Read())
            {
                int id = reader.GetInt32(reader.GetOrdinal("PeriodId"));
                String label = reader.GetString(reader.GetOrdinal("Description"));
                Add(new TimePeriod(id, label));
            }
            IsReadOnly = true;
        }

        public static TimePeriodCollection GetTimePeriodCollection(IDataReader reader)
        {
            return new TimePeriodCollection(reader);
        }
    }
}
