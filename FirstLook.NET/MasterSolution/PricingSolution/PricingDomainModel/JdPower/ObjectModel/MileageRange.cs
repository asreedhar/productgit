using System;
using System.Collections.Generic;
using System.Text;

namespace FirstLook.Pricing.DomainModel.JDPower.ObjectModel
{
    public class MileageRange
    {
        private int id;
        private int min;
        private int max;
        private string label;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Label
        {
            get { return label; }
            set { label = value; }
        }

        public int Min
        {
            get { return min; }
            set { min = value; }
        }

        public int Max
        {
            get { return max; }
            set { max = value; }
        }

        public MileageRange(int id, int min, int max, string label)
        {
            this.id = id;
            this.min = min;
            this.max = max;
            this.label = label;
        }
    }
}
