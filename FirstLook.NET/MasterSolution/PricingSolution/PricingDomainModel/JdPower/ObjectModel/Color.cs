
namespace FirstLook.Pricing.DomainModel.JDPower.ObjectModel
{
    public class Color
    {
        private int id;
        private string label;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Label
        {
            get { return label; }
            set { label = value; }
        }

        public Color(int id, string label)
        {
            this.id = id;
            this.label = label;
        }
    }
}
