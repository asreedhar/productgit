using System;
using System.Collections.Generic;
using System.Text;

namespace FirstLook.Pricing.DomainModel.JDPower.ObjectModel
{
    [Serializable]
    public class JDPowerRegion
    {
        private int id;
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
    }
}
