﻿using Autofac;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Logging;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search;
using ICache = FirstLook.Common.Core.ICache;

namespace FirstLook.Pricing.DomainModel
{
    public class PricingDomainRegister : IRegistryModule
    {
        public void Register(ContainerBuilder builder)
        {
            // General
            builder.RegisterType<DotNetCacheWrapper>().As<ICache>();
            builder.RegisterType<HealthMonitoringWrapper>().As<ILogger>();

            // Search 
            builder.RegisterType<SearchSummaryInfoDataAccess>().As<ISearchSummaryInfoDataAccess>();
        }
    }
}
