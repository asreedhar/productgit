using System;

namespace FirstLook.Pricing.DomainModel.Market
{
    [FlagsAttribute]
    public enum SortStructure
    {
        Self = 1,
        Children = 2
    }
}
