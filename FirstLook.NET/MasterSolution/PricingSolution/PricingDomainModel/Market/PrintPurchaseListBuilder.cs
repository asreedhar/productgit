using System;
using System.Configuration;
using System.Text;
using FirstLook.Common.Core.Utilities;

namespace FirstLook.Pricing.DomainModel.Market
{
    /// <summary>
    /// PDF builder for purchase lists.
    /// </summary>
    public class PrintPurchaseListBuilder
    {
        private readonly PurchaseList     _purchaseList;
        private readonly Owner            _owner;
        private readonly ReportParameters _reportParameters;
        private readonly string[] _styleSheets;

        /// <summary>
        /// Initialize with the purchase list object from which we will generate a pdf. Also, determine the name of the
        /// owner whose purchase list this is, as that info will be displayed.
        /// </summary>
        /// <param name="purchaseList">Purchase list around which to build the pdf.</param>
        /// <param name="owner">The owner of the report and purchase list.</param>
        /// <param name="reportParameters">Search radius and basis period the report.</param>
        public PrintPurchaseListBuilder(
            Owner owner,
            PurchaseList purchaseList,
            ReportParameters reportParameters)
        {
            _owner = owner;
            _purchaseList = purchaseList;
            _reportParameters = reportParameters;
        }

        public PrintPurchaseListBuilder(
           Owner owner,
           PurchaseList purchaseList,
           ReportParameters reportParameters,
           string[] styleSheets)
        {
            _owner = owner;
            _purchaseList = purchaseList;
            _reportParameters = reportParameters;
            _styleSheets = styleSheets;
        }

        private bool _showUnitsSold = true;

        public bool ShowUnitsSold
        {
            get { return _showUnitsSold; }
            set { _showUnitsSold = value; }
        }

        /// <summary>
        /// Create a pdf from a purchase list. Will first create the html representation of the purchase list, and then
        /// create the pdf byte stream from this html using Prince XML.
        /// </summary>
        /// <returns>A pdf byte stream.</returns>
        public byte[] Build()
        {
            StringBuilder html = new StringBuilder();

            // Head.
            html.Append(@"<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Strict//EN""");
            html.Append(@"""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"">");
            html.Append(@"<html xmlns=""http://www.w3.org/1999/xhtml"">");
            html.Append(@"<head>");
            if (ConfigurationManager.AppSettings["prince_xml_assests_host"] != null)
            {
                html.Append(@"<base href=""");
                html.Append(ConfigurationManager.AppSettings["prince_xml_assests_host"]);
                html.Append(@""" />"); 
            }
            html.Append(@"<meta http-equiv=""Content-type"" content=""text/html; charset=utf-8"" />");
            html.Append(@"<title>Market Stocking Velocity Report- Purchase List</title>");
            html.Append(@"</head>");

            // Body.
            html.Append(@"<body id=""listingsprint"">");
            html.Append(@"<div id=""page_wrapper"">");

            // Page title + dealer name.
            html.Append(@"<h1 id=""branding"">Market Velocity Report</h1>");
            html.Append(@"<div id=""footer"">");
            html.Append(@"<span class=""date"">");
            html.Append(string.Format("{0:M/d/yyyy}", DateTime.Today));
            html.Append(@"</span>");
            html.Append(@"<img src=""pricing/Public/Images/PrintImages/Logo.jpg"" />");
            html.Append(@"</div>");
            html.Append(@"<h2 class=""dealer_name"">");
            html.Append(_owner.Name);
            html.Append(@"</h2>");

            // Purchase list table header.
            html.Append(@"<div class=""shopping_list_table"">");
            html.Append(@"<table border=""0"" cellspacing=""0"" cellpadding=""0"" class=""grid"">");
            html.Append(@"<thead>");
            html.Append(@"<tr>");
            html.Append(@"<th class=""vehicle_info"">Vehicle Information</th>");
            if (ShowUnitsSold)
            {
                html.Append(@"<th class=""units_sold"">Units Sold</th>"); 
            }
            html.Append(@"<th class=""number_of_listings"">Number of Listings</th>");
            html.Append(@"<th class=""avg_market_price"">Avg Internet Price</th>");
            html.Append(@"<th class=""avg_mileage"">Avg Mileage</th>");
            html.Append(@"<th class=""units_in_stock"">Units in Stock</th>");
            html.Append(@"<th class=""quantity"">Quantity</th>");
            html.Append(@"</tr>");
            html.Append(@"</thead>");
            html.Append(@"<tbody>");

            bool isOdd = true;

            // Table data.
            foreach (PurchaseListItem item in _purchaseList)
            {
                // Styling.
                if (isOdd)
                {
                    html.Append(@"<tr class=""odd"">");
                }
                else
                {
                    html.Append(@"<tr>");
                }
                html.Append(@"<td class=""vehicle_info"">");

                // Year.
                html.Append(@"<span class=""year"">");
                html.Append(item.Vehicle.ModelYear);
                html.Append(@"</span> ");

                // Make.
                html.Append(@"<span class=""make"">");
                html.Append(item.Vehicle.Make);
                html.Append(@"</span> ");

                // Model.
                html.Append(@"<span class=""model"">");
                html.Append(item.Vehicle.ModelFamily);
                html.Append(@"</span> ");

                // Segment.
                html.Append(@"<span class=""segment"">");
                html.Append(item.Vehicle.Segment);
                html.Append(@"</span>");

                // Vehicle configuration details.
                VehicleConfiguration vehicleConfiguration = item.Vehicle as VehicleConfiguration;
                if (vehicleConfiguration != null)
                {
                    html.Append(@"<ul>");

                    // Series.                    
                    if (!string.IsNullOrEmpty(vehicleConfiguration.Series))
                    {
                        html.Append(@"<li class=""series"">");
                        html.Append(vehicleConfiguration.Series);
                        html.Append(@"</li>"); 
                    }

                    // Drive train.
                    if (!string.IsNullOrEmpty(vehicleConfiguration.DriveTrain))
                    {
                        html.Append(@"<li class=""drive_train"">");
                        html.Append(vehicleConfiguration.DriveTrain);
                        html.Append(@"</li>"); 
                    }

                    // Engine.
                    if (!string.IsNullOrEmpty(vehicleConfiguration.Engine))
                    {
                        html.Append(@"<li class=""engine"">");
                        html.Append(vehicleConfiguration.Engine);
                        html.Append(@"</li>"); 
                    }

                    // Fuel type.
                    if (!string.IsNullOrEmpty(vehicleConfiguration.FuelType))
                    {
                        html.Append(@"<li class=""fuel_type"">");
                        html.Append(vehicleConfiguration.FuelType);
                        html.Append(@"</li>"); 
                    }

                    // Transmission.
                    if (!string.IsNullOrEmpty(vehicleConfiguration.Transmission))
                    {
                        html.Append(@"<li class=""transmission"">");
                        html.Append(vehicleConfiguration.Transmission);
                        html.Append(@"</li>"); 
                    }

                    html.Append(@"</ul>");
                }
                html.Append(@"</td>");

                if (ShowUnitsSold)
                {
                    // Units sold.
                    html.Append(@"<td class=""units_sold"">");
                    html.Append(item.MarketInformation.UnitsSold);
                    html.Append(@"</td>"); 
                }

                // Number of listings.
                html.Append(@"<td class=""number_of_listings"">");
                html.Append(item.MarketInformation.NumberOfListings);
                html.Append(@"</td>");

                // Average market price.
                html.Append(@"<td class=""avg_market_price"">");
                html.Append(item.MarketInformation.AverageInternetPrice);
                html.Append(@"</td>");

                // Average mileage.
                html.Append(@"<td class=""avg_mileage"">");
                html.Append(item.MarketInformation.AverageInternetMileage);
                html.Append(@"</td>");

                // Units in stock.
                html.Append(@"<td class=""units_in_stock"">");
                html.Append(item.MarketInformation.UnitsInStock);
                html.Append(@"</td>");

                // Quantity.
                html.Append(@"<td class=""quantity""><span class=""box""></span> &times; ");
                html.Append(item.Quantity);
                html.Append(@"</td>");

                html.Append(@"</tr>");

                // Notes.
                html.Append(@"<tr class=""notes");
                if (isOdd)
                {
                    html.Append(@" odd");
                }
                html.Append(@""">");
                if (ShowUnitsSold)
                {
                    html.Append(@"<td colspan=""9""><span class=""line""></span>"); 
                }
                else
                {
                    html.Append(@"<td colspan=""8""><span class=""line""></span>"); 
                }
                html.Append(@"<div class=""notes"">Notes:");
                html.Append(@"<p>");
                html.Append(item.Notes);
                html.Append(@"</p>");
                html.Append(@"</div>");

                // Lot location.
                html.Append(@"<div class=""lot_location"">Lot Location</div>");

                html.Append(@"</td></tr>");

                isOdd = !isOdd;
            }
            html.Append(@"</tbody></table></div></div></body></html>");

            if (_styleSheets == null)
            {
                return PdfHelper.GeneratePdf(html.ToString()); 
            }
            return PdfHelper.GeneratePdf(html.ToString(), _styleSheets); 
        }
    }
}