using System;
using System.Data;

namespace FirstLook.Pricing.DomainModel.Market
{
    /// <summary>
    /// Is a read-only class that extends <see cref="Vehicle"/> adding properties for: body type, series, drive train,
    /// engine, fuel type, passenger doors and transmission. It is identified by model configuration id. The model 
    /// configuration id plus the parent ids can be used to add a vehicle configuration to an owner's purchase list.
    /// </summary>
    [Serializable]
    public class VehicleConfiguration : Vehicle
    {
        #region Business Methods

        private int?   _modelConfigurationId; // Identifier for this vehicle model configuration.
        private string _series;               // Vehicle series.
        private string _bodyType;             // Vehicle body.
        private string _transmission;         // Vehicle transmission.
        private string _engine;               // Vehicle engine.
        private string _fuelType;             // Vehicle fuel type.
        private string _passengerDoors;       // Vehicle passenger doors.
        private string _driveTrain;           // Vehicle drive train.        

        public int? ModelConfigurationId
        {
            get { return _modelConfigurationId; }
        }

        public string Series
        {
            get { return _series; }
        }

        public string BodyType
        {
            get { return _bodyType; }
        }

        public string Transmission
        {
            get { return _transmission; }
        }

        public string Engine
        {
            get { return _engine; }
        }

        public string FuelType
        {
            get { return _fuelType; }
        }

        public string PassengerDoors
        {
            get { return _passengerDoors; }
        }

        public string DriveTrain
        {
            get { return _driveTrain; }
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Create a vehicle configuration object with values from the database.
        /// </summary>
        /// <param name="record">Vehicle configuration values retrieved from the database.</param>
        /// <returns>A new vehicle configuration object.</returns>
        internal static VehicleConfiguration GetVehicleConfiguration(IDataRecord record)
        {
            return new VehicleConfiguration(record);
        }

        /// <summary>
        /// Create a new vehicle configuration object. Private to force creation to go through the 
        /// <see cref="GetVehicleConfiguration(IDataRecord)"/>.
        /// </summary>
        /// <param name="record">Vehicle configuration values retrieved from the database.</param>
        private VehicleConfiguration(IDataRecord record) : base(record)
        {            
            Fetch(record);
        }

        #endregion

        #region Data Access

        /// <summary>
        /// Populate this vehicle configuration object with values retrieved from the database.
        /// </summary>
        /// <param name="record">Vehicle configuration values retrieved from the database.</param>
        private void Fetch(IDataRecord record)
        {
            if (!record.IsDBNull(record.GetOrdinal("ModelConfigurationId")))
            {
                _modelConfigurationId = record.GetInt32(record.GetOrdinal("ModelConfigurationId"));                
            }
            if (!record.IsDBNull(record.GetOrdinal("Series")))
            {
                _series = record.GetString(record.GetOrdinal("Series"));                
            }
            if (!record.IsDBNull(record.GetOrdinal("BodyType")))
            {
                _bodyType = record.GetString(record.GetOrdinal("BodyType"));                
            }
            if (!record.IsDBNull(record.GetOrdinal("Transmission")))
            {
                _transmission = record.GetString(record.GetOrdinal("Transmission"));                
            }
            if (!record.IsDBNull(record.GetOrdinal("Engine")))
            {
                _engine = record.GetString(record.GetOrdinal("Engine"));                
            }
            if (!record.IsDBNull(record.GetOrdinal("FuelType")))
            {
                _fuelType = record.GetString(record.GetOrdinal("FuelType"));                
            }
            if (!record.IsDBNull(record.GetOrdinal("PassengerDoors")))
            {
                _passengerDoors = record.GetString(record.GetOrdinal("PassengerDoors"));                
            }
            if (!record.IsDBNull(record.GetOrdinal("DriveTrain")))
            {
                _driveTrain = record.GetString(record.GetOrdinal("DriveTrain"));                
            }
        }

        #endregion
    }
}
