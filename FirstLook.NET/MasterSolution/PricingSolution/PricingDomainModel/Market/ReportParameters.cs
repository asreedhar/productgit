using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Market
{
    /// <summary>
    /// Is a read-only object that identifies the parameters (basis period, search radius, stock type) used to define
    /// a report's contents. Instances are fetched by both owner handle and report handle and its values are used to
    /// populate the report criteria form.
    /// </summary>
    /// <remarks>
    /// See procedure: <c>Reporting.ReportParameters#Fetch</c>
    /// </remarks>
    [Serializable]
    public class ReportParameters : ReadOnlyBase<ReportParameters>
    {
        #region Business Methods

        private string       _reportHandle; // GUID handle to identify generated report.
        private BasisPeriod  _basisPeriod;  // Time period the report covers.
        private SearchRadius _searchRadius; // Search area the report covers.

        public string ReportHandle
        {
            get { return _reportHandle; }
        }

        public BasisPeriod BasisPeriod
        {
            get { return _basisPeriod; }
        }

        public SearchRadius SearchRadius
        {
            get { return _searchRadius; }
        }

        protected override object GetIdValue()
        {
            return _reportHandle;
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Get the report parameters for the given report.
        /// </summary>
        /// <param name="reportHandle">Identifier of the report to find parameters for.</param>
        /// <returns>A report parameters object.</returns>
        public static ReportParameters GetReportParameters(string reportHandle)
        {
            return DataPortal.Fetch<ReportParameters>(new Criteria(reportHandle));
        }

        /// <summary>
        /// Data source for retrieving report parameters from the database.
        /// </summary>
        [Serializable]
        public class DataSource
        {
            /// <summary>
            /// Method for fetching report parameters from the database. Will hand off to the data portal for 
            /// execution.
            /// </summary>
            /// <param name="handle">Report GUID by which to find its parameters.</param>
            /// <returns>A new <see cref="Owner"/> object.</returns>
            public ReportParameters Select(string handle)
            {
                return GetReportParameters(handle);
            }
        }

        #endregion

        #region Data Access

        /// <summary>
        /// Criteria by which to associate this object to its corresponding database values. Specifically, this is the
        /// report GUID by which to find our report parameters in the db.
        /// </summary>
        protected class Criteria
        {
            private readonly string _handle;

            public Criteria(string handle)
            {
                _handle = handle;
            }

            public string Handle
            {
                get { return _handle; }
            }
        }

        /// <summary>
        /// Go to the database to retrieve the values of this report parameters object. This method is automatically 
        /// called by <see cref="DataPortal.Fetch(object)"/> once it has established itself on the application server.
        /// </summary>
        /// <param name="criteria">Criteria by which to find our owner (i.e. the owner handle).</param>
        protected virtual void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.ReportDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand((connection.CreateCommand())))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Reporting.ReportParameters#Fetch";
                    command.AddParameterWithValue("ReportHandle", DbType.String, false, criteria.Handle);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Fetch(reader);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Populate this report parameters object with values retrieved from the database.
        /// </summary>
        /// <param name="reader">Database values.</param>
        protected void Fetch(IDataReader reader)
        {
            _reportHandle = reader.GetString(reader.GetOrdinal("ReportHandle"));
            _basisPeriod  = BasisPeriod.GetBasisPeriod(reader, "BasisPeriod");
            _searchRadius = SearchRadius.GetSearchRadius(reader, "SearchRadius");
        }

        #endregion
    }
}
