using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Market
{
    /// <summary>
    /// Is a command object that when executed will request a report be generated. The arguments to the command are:
    /// owner handle, basis period id, search radius id and stock type. The restult of the command is the report 
    /// handle. Once the client has the report handle they can poll on the command object <see cref="ReportStatus"/>.
    /// </summary>
    /// <remarks>
    /// See procedure: <c>Reporting.Report#Generate_Client</c>
    /// </remarks>
    [Serializable]
    public class RequestReport : CommandBase
    {
        private readonly string _ownerHandle;    // Handle of the owner requesting the report.
        private readonly int    _basisPeriodId;  // Id of the basis period of time over which to report.
        private readonly int    _searchRadiusId; // Id of the search radius in miles we will report over.
        private string          _reportHandle;   // Handle of the newly created report.
        private readonly string _insertUser;     // Name of the user requesting the report fetch

        /// <summary>
        /// Create this request object with the values needed to generate a report.
        /// </summary>
        /// <param name="ownerHandle">Owner that is requesting the report.</param>
        /// <param name="basisPeriodId">Time period to report on.</param>
        /// <param name="searchRadiusId">Radius in miles to find listings in.</param>
        /// <param name="insertUser">Name of the user requesting the report fetch</param>
        public RequestReport(string ownerHandle, int basisPeriodId, int searchRadiusId, string insertUser)
        {
            _ownerHandle = ownerHandle;
            _basisPeriodId = basisPeriodId;
            _searchRadiusId = searchRadiusId;
            _insertUser = insertUser;
        }

        /// <summary>
        /// Execute this request for a report. Will hand off execution to the data portal on the application server,
        /// and return the handle of the newly created report.
        /// </summary>
        /// <param name="owner">Owner that is requesting the report.</param>
        /// <param name="basisPeriod">Time period over which to report on.</param>
        /// <param name="searchRadius">Radius in miles in which to find listings.</param>
        /// <param name="insertUser">Name of the user requesting the report fetch</param>
        /// <returns>The handle of the newly generated report.</returns>
        public static string Execute(Owner owner, BasisPeriod basisPeriod, SearchRadius searchRadius, string insertUser)
        {
            RequestReport command = new RequestReport(owner.Handle, basisPeriod.Id, searchRadius.Id, insertUser);
            DataPortal.Execute(command);
            return command._reportHandle;
        }

        /// <summary>
        /// Execute this request for report generation. This method will be called by 
        /// <see cref="DataPortal.Execute(CommandBase)"/>, and will set the report handle on completion of execution.
        /// </summary>
        protected override void DataPortal_Execute()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandText = "Reporting.Report#Generate_Client";
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = 180;
                    command.AddParameterWithValue("OwnerHandle", DbType.String, false, _ownerHandle);
                    command.AddParameterWithValue("BasisPeriodId", DbType.Int32, false, _basisPeriodId);
                    command.AddParameterWithValue("SearchRadiusId", DbType.Int32, false, _searchRadiusId);
                    command.AddParameterWithValue("InsertUser", DbType.String, false, _insertUser);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            _reportHandle = reader.GetString(reader.GetOrdinal("ReportHandle"));
                        }
                    }
                }
            }
        }
    }
}
