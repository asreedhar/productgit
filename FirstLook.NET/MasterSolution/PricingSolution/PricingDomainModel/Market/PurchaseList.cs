using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Threading;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Market
{
    /// <summary>
    /// Is an editable parent collection class whose children are the purchase list items made by users of the report. 
    /// Instances are fetched by owner handle and report handle.
    /// </summary>    
    /// <remarks>
    /// See procedure: <c>Purchasing.PurchaseList#Fetch</c>, 
    /// </remarks>
    [Serializable]
    public class PurchaseList : BusinessListBase<PurchaseList, PurchaseListItem>, IReportItemCollection
    {
        #region BusinessMethods

        private string _ownerHandle; // Identifier of the owner whose purchase list this is.

        public string OwnerHandle
        {
            get { return _ownerHandle; }
        }

        public void Remove(int itemId)
        {
            foreach (PurchaseListItem item in this)
            {
                if (item.Id == itemId)
                {
                    Remove(item);
                    break;
                }
            }
        }

        public PurchaseListItem Get(int itemId)
        {
            foreach (PurchaseListItem item in this)
            {
                if (item.Id == itemId)
                {
                    return item;
                }
            }

            return null;
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Does a purchase list exist for the owner identified by the given handle?
        /// </summary>
        /// <param name="ownerHandle">Owner to check the existence of a purchase list for.</param>
        /// <returns>True if a purchase list exists for this owner, false otherwise.</returns>
        public static bool Exists(string ownerHandle)
        {
            return PurchaseListExists.Execute(ownerHandle);
        }

        /// <summary>
        /// Get the purchase list defined by the given owner and report.
        /// </summary>
        /// <param name="ownerHandle">Identifier of the owner whose purchase list we should return.</param>
        /// <param name="reportHandle">Identifier of the owner's report.</param>
        /// <returns>A new purchase list object.</returns>
        public static PurchaseList GetPurchaseList(string ownerHandle, string reportHandle)
        {
            return DataPortal.Fetch<PurchaseList>(new Criteria(ownerHandle, reportHandle));
        }

        /// <summary>
        /// Create a new purchase list for the given owner and report.
        /// </summary>
        /// <param name="ownerHandle">Identifier of the owner we will create a new purchase list for.</param>
        /// <param name="reportHandle">Identifier of the owner's report.</param>
        /// <returns>A new purchase list object.</returns>
        public static PurchaseList NewPurchaseList(string ownerHandle, string reportHandle)
        {
            return DataPortal.Create<PurchaseList>(new Criteria(ownerHandle, reportHandle));
        }

        /// <summary>
        /// Data source for retrieving and manipulating a purchase list in the database.
        /// </summary>
        public class DataSource
        {
            /// <summary>
            /// Method for fetching a purchase list from the database. Will hand off to the data portal for execution.
            /// Will check first that the purchase list actually exists and, if it doesn't, will create it.
            /// </summary>
            /// <param name="ownerHandle">Owner GUID handle.</param>
            /// <param name="reportHandle">Report GUID handle.</param>
            /// <returns>A new <c>PurchaseList</c> object.</returns>
            public PurchaseList Select(string ownerHandle, string reportHandle)
            {
                if (Exists(ownerHandle))
                {
                    return GetPurchaseList(ownerHandle, reportHandle);
                }

                return NewPurchaseList(ownerHandle, reportHandle);
            }

            /// <summary>
            /// Method for updating purchase list items from the database. Will hand off to the data portal for 
            /// execution. If this purchase list doesn't actually exist, we will ignore the update. If it does exist,
            /// we will merge the changes detailed in <c>listInfo</c> into the retrieved purchase list items, and then
            /// update the db.            
            /// </summary>
            public void Update(string ownerHandle, string reportHandle, ICollection<int> deleted, IDictionary<int, int> quantities, IDictionary<int, string> notes)
            {
                string updateUser = Thread.CurrentPrincipal.Identity.Name;

                if (Exists(ownerHandle))
                {
                    PurchaseList list = GetPurchaseList(ownerHandle, reportHandle);

                    foreach (int i in deleted)
                    {
                        list.Remove(i);
                    }

                    foreach (KeyValuePair<int, int> pair in quantities)
                    {
                        PurchaseListItem item = list.Get(pair.Key);
                        if (item != null)
                        {
                            item.Quantity = pair.Value;
                            item.UpdateUser = updateUser;
                        }
                    }

                    foreach (KeyValuePair<int, string> pair in notes)
                    {
                        PurchaseListItem item = list.Get(pair.Key);
                        if (item != null)
                        {
                            item.Notes = pair.Value;
                            item.UpdateUser = updateUser;
                        }
                    }

                    list.Save();
                }
            }
        }

        #endregion

        #region Data Access

        /// <summary>
        /// Criteria by which to associate this object to its corresponding database values. Specifically, this is the
        /// owner GUID and report GUID by which to find our purchase list in the db.
        /// </summary>
        [Serializable]
        protected class Criteria
        {
            private readonly string _ownerHandle;  // Owner GUID.
            private readonly string _reportHandle; // Report GUID.

            /// <summary>
            /// Create a new criteria object for using in retrieving a purchase list from the database.
            /// </summary>
            /// <param name="ownerHandle">Owner GUID.</param>
            /// <param name="reportHandle">Report GUID.</param>
            public Criteria(string ownerHandle, string reportHandle)
            {
                _ownerHandle = ownerHandle;
                _reportHandle = reportHandle;
            }

            public string OwnerHandle
            {
                get { return _ownerHandle; }
            }

            public string ReportHandle
            {
                get { return _reportHandle; }
            }
        }

        /// <summary>
        /// Go to the database to retrieve the values of this purchase list object. This method is automatically called
        /// by <see cref="DataPortal.Fetch(object)"/> once it has established itself on the application server.
        /// </summary>
        /// <param name="criteria">Criteria by which to find our purhcase list (i.e. owner, report handles).</param>
        protected virtual void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.ListDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Purchasing.PurchaseList#Fetch";
                    command.AddParameterWithValue("OwnerHandle", DbType.String, false, criteria.OwnerHandle);
                    command.AddParameterWithValue("ReportHandle", DbType.String, false, criteria.ReportHandle);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        Fetch(reader, criteria.OwnerHandle);                        
                    }
                }
            }
        }

        /// <summary>
        /// Update the values of this purchase list object in the database. This method is automatically called by
        /// <see cref="DataPortal.Update(object)"/> once it has established itself on the application server.
        /// </summary>
        [Transactional(TransactionalTypes.Manual)]
        protected override void DataPortal_Update()
        {                   
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.ListDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        RaiseListChangedEvents = false;

                        // Delete the items on death row.
                        foreach (PurchaseListItem item in DeletedList)
                        {
                            item.DeleteSelf(connection, transaction, OwnerHandle);
                        }
                        DeletedList.Clear();

                        // Upsert all the remaining list items.
                        foreach (PurchaseListItem item in this)
                        {
                            if (item.IsNew)
                            {
                                item.Insert(connection, transaction, OwnerHandle);
                            }
                            else if (item.IsDirty)
                            {
                                item.Update(connection, transaction, OwnerHandle);
                            }
                        }

                        RaiseListChangedEvents = true;
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Populate this purchase list object with values retrieved from the database.
        /// </summary>
        /// <param name="reader">Database values.</param>
        /// <param name="ownerHandle">Handle of the owner whose purchase list this is.</param>
        protected void Fetch(IDataReader reader, string ownerHandle)
        {
            _ownerHandle = ownerHandle;

            while (reader.Read())
            {
                Add(PurchaseListItem.GetPurchaseListItem(reader));
            }
        }

        #endregion

        #region Implementation of IList<IReportItem>

        /// <summary>
        /// Get the index of the given item in this purchase lsit.
        /// </summary>
        /// <param name="item">Item to find index of in this purchase list.</param>
        /// <returns>The index of the item in this collection if it is contained, -1 otherwise.</returns>
        int IList<IReportItem>.IndexOf(IReportItem item)
        {
            for (int i = 0; i < Count; i++)
            {
                if (((IReportItem)Items[i]).Id == item.Id)
                    return i;
            }

            return -1;
        }

        /// <summary>
        /// Not supported.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        void IList<IReportItem>.Insert(int index, IReportItem item)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Getter supported for returning the report item at the given index. Setter is not supported.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        IReportItem IList<IReportItem>.this[int index]
        {
            get { return Items[index]; }
            set { throw new NotSupportedException(); }
        }

        #endregion

        #region IList<IReportItem> Members

        /// <summary>
        /// Not supported.
        /// </summary>
        /// <param name="index"></param>
        void IList<IReportItem>.RemoveAt(int index)
        {
            throw new NotSupportedException();
        }

        #endregion

        #region ICollection<IReportItem> Members

        /// <summary>
        /// Not supported.
        /// </summary>
        /// <param name="item"></param>
        void ICollection<IReportItem>.Add(IReportItem item)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Not suppported.
        /// </summary>
        void ICollection<IReportItem>.Clear()
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Does the given item exist in this collection?
        /// </summary>
        /// <param name="item">Item to check for existence in this collection.</param>
        /// <returns>True if the item exists in this purchase list, false otherwise.</returns>
        bool ICollection<IReportItem>.Contains(IReportItem item)
        {
            return ((IList<IReportItem>)this).IndexOf(item) != -1;
        }

        /// <summary>
        /// Copy items in this collection to the given array.
        /// </summary>
        /// <param name="array">Array to copy to.</param>
        /// <param name="arrayIndex">Index to begin copying at.</param>
        void ICollection<IReportItem>.CopyTo(IReportItem[] array, int arrayIndex)
        {
            int i = arrayIndex;
            foreach (PurchaseListItem item in this)
            {
                array[i++] = item;
            }
        }

        /// <summary>
        /// How many items are in this collection?
        /// </summary>
        int ICollection<IReportItem>.Count
        {
            get { return Items.Count; }
        }

        /// <summary>
        /// Is this collection read-only?
        /// </summary>
        bool ICollection<IReportItem>.IsReadOnly
        {
            get { return true; }
        }

        /// <summary>
        /// Not supported.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        bool ICollection<IReportItem>.Remove(IReportItem item)
        {
            throw new NotSupportedException();
        }

        #endregion

        #region IEnumerable<IReportItem> Members

        /// <summary>
        /// Get an enumerator for this purchase list.
        /// </summary>
        /// <returns>An enumerator over this purchase list.</returns>
        IEnumerator<IReportItem> IEnumerable<IReportItem>.GetEnumerator()
        {
            List<IReportItem> list = new List<IReportItem>();

            foreach (PurchaseListItem item in Items)
            {
                list.Add(item);
            }

            return list.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        /// <summary>
        /// Get an enumerator over this purchase list.
        /// </summary>
        /// <returns>An enumerator over this purchase list.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region IReportItemCollection Members

        public IReportItemCollection Filter(ReportFilterArguments filterArguments)
        {
            throw new NotImplementedException();
        }

        public IReportItemCollection Page(int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Sort this collection of report items by means of the provided expression. Will determine the real type of
        /// the sort expression and sort appropriately.
        /// </summary>
        /// <param name="expression">Expression to sort this collection by.</param>
        /// <param name="ascending">Direction of the sort for the expression.</param>
        public void Sort(SortExpression expression, bool ascending)
        {
            string propertyName = expression.ToString("G");

            IComparer comparer;

            switch (expression)
            {
                case SortExpression.MarketDaysSupply:
                case SortExpression.AverageInternetMileage:
                case SortExpression.AverageInternetPrice:
                case SortExpression.UnitsInStock:
                case SortExpression.UnitsSold:
                case SortExpression.NumberOfListings:                
                    comparer = new MarketInformationComparer(propertyName, ascending);
                    break;
                case SortExpression.ModelYear:
                    comparer = new VehicleComparer<int>(propertyName, ascending);
                    break;
                case SortExpression.Make:
                case SortExpression.ModelFamily:
                    comparer = new VehicleComparer<string>(propertyName, ascending);
                    break;                
                case SortExpression.Quantity:
                    comparer = new QuantityComparer(ascending);
                    break;
                default:
                    comparer = new MarketInformationComparer("MarketDaysSupply", ascending);
                    break;
            }

            Sort(comparer);
        }

        /// <summary>
        /// Sort this report item collection with the given comparer. The comparer was determined by the other sort
        /// method.
        /// </summary>
        /// <param name="comparer">Comparer by which to sort this collection.</param>
        private void Sort(IComparer comparer)
        {            
            ArrayList.Adapter(this).Sort(comparer);
        }

        /// <summary>
        /// Helper base class for comparing report items. Used for sorting.
        /// </summary>
        private abstract class ReportItemComparer : IComparer
        {
            private readonly bool _ascending;

            protected ReportItemComparer(bool @ascending)
            {
                _ascending = ascending;
            }

            public int Compare(object x, object y)
            {
                PurchaseListItem a = (PurchaseListItem)x, b = (PurchaseListItem)y;                

                int compare = Compare(a, b);

                return compare != 0 && !_ascending ? -1 * compare : compare;
            }

            protected abstract int Compare(PurchaseListItem x, PurchaseListItem y);
        }

        /// <summary>
        /// Helper class for comparing market information. Used for sorting.
        /// </summary>
        private class MarketInformationComparer : ReportItemComparer
        {
            private readonly PropertyInfo _property;

            public MarketInformationComparer(string propertyName, bool ascending)
                : base(ascending)
            {
                _property = typeof(MarketInformation).GetProperty(propertyName);
            }

            protected override int Compare(PurchaseListItem x, PurchaseListItem y)
            {
                int? i = (int?)_property.GetValue(x.MarketInformation, null),
                     j = (int?)_property.GetValue(y.MarketInformation, null);

                return Nullable.Compare(i, j);
            }
        }

        /// <summary>
        /// Helper class for comparing vehicles. Used for sorting.
        /// </summary>
        private class VehicleComparer<T> : ReportItemComparer where T : IComparable
        {
            private readonly PropertyInfo _property;

            public VehicleComparer(string propertyName, bool ascending)
                : base(ascending)
            {
                _property = typeof(Vehicle).GetProperty(propertyName);
            }

            protected override int Compare(PurchaseListItem x, PurchaseListItem y)
            {
                T i = (T)_property.GetValue(x.Vehicle, null),
                  j = (T)_property.GetValue(y.Vehicle, null);

                return i.CompareTo(j);
            }
        }

        /// <summary>
        /// Helper class for comparing quantities. Used for sorting.
        /// </summary>
        private class QuantityComparer : ReportItemComparer
        {            
            public QuantityComparer(bool ascending)
                : base(ascending)
            {                
            }

            protected override int Compare(PurchaseListItem x, PurchaseListItem y)
            {                
                return x.Quantity.CompareTo(y.Quantity);
            }
        }

        #endregion
    }
}
