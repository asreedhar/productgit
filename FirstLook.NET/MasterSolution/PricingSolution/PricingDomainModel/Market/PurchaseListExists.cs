using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Market
{
    /// <summary>
    /// Command for determining if a purchase list exists for a given owner.
    /// </summary>
    public class PurchaseListExists : CommandBase
    {
        private readonly string _ownerHandle; // Handle of the owner whose purchase list this would be.
        private bool            _exists;      // Return value - does the list exist?

        /// <summary>
        /// Creates a command object based off the given dealer.
        /// </summary>
        /// <param name="ownerHandle">Handle of the owner whose purchase list this would be.</param>
        public PurchaseListExists(string ownerHandle)
        {
            _ownerHandle = ownerHandle;
        }            

        /// <summary>
        /// Publically accessible call to see if a purchase list for the given owner handle exists.
        /// </summary>
        /// <param name="ownerHandle">Handle of the owner for whom we want to check if a purchase list exists.</param>
        /// <returns>True if the purchase list exists, false otherwise.</returns>
        public static bool Execute(string ownerHandle)
        {
            PurchaseListExists command = new PurchaseListExists(ownerHandle);
            command.DataPortal_Execute();
            return command._exists;            
        }

        /// <summary>
        /// Execute this command. Is called directly by the data portal. Will determine if the purchase list for
        /// the given owner exists in the database.
        /// </summary>
        protected override void DataPortal_Execute()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.ListDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandText = "Purchasing.PurchaseList#Exists";
                    command.CommandType = CommandType.StoredProcedure;
                    command.AddParameterWithValue("OwnerHandle", DbType.String, false, _ownerHandle);                        

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        _exists = reader.Read() ? reader.GetBoolean(reader.GetOrdinal("Exists")) : false;                            
                    }
                }
            }
        }
    }
}
