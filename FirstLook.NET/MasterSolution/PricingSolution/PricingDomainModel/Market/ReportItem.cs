using System;
using System.Collections.Generic;
using System.Data;
using Csla;

namespace FirstLook.Pricing.DomainModel.Market
{
    /// <summary>
    /// Is a read-only class that is a composition of list-information, market-information, vehicle data and optionally
    /// child Report Items (this can be an aggregate of vehicle-configuration children).
    /// </summary>
    [Serializable]
    public class ReportItem : ReadOnlyBase<ReportItem>, IReportItem
    {
        #region Business Methods

        private int                           _id;                // Identifier of this report item.
        private Vehicle                       _vehicle;           // Vehicle that defines this report item.
        private MarketInformation             _marketInformation; // Market info for this vehicle - e.g. days supply.
        private ListInformation               _listInformation;   // List info for this vehicle - e.g. quantity.
        private readonly ReportItemCollection _reportItems;       // Optional child report items.

        public Vehicle Vehicle
        {
            get { return _vehicle; }
        }

        public MarketInformation MarketInformation
        {
            get { return _marketInformation; }
        }

        public ListInformation ListInformation
        {
            get { return _listInformation; }
        }

        public ReportItemCollection ReportItems
        {
            get { return _reportItems; }
        }

        protected override object GetIdValue()
        {
            return _id;
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Create a new report item from the given id and vehicle.
        /// </summary>
        /// <param name="id">Id of this report item.</param>
        /// <param name="vehicle">Vehicle to set for this report item.</param>
        /// <returns>A new report item.</returns>
        internal static ReportItem NewReportItem(int id, Vehicle vehicle)
        {
            return new ReportItem(id, vehicle);
        }

        /// <summary>
        /// Create a new report item object from database values. Report items can be parents to other report items
        /// which changes the type of <see cref="Vehicle"/> object it will have. In the case of this method, we
        /// default to always creating children.        
        /// </summary>
        /// <param name="record">Database values.</param>
        /// <returns>A new report item object.</returns>
        internal static ReportItem GetReportItem(IDataRecord record)
        {            
            return new ReportItem(record);
        }

        /// <summary>
        /// Create a new, empty report item. Private to force construction to go through factory methods.
        /// </summary>
        private ReportItem()
        {
            _reportItems = new ReportItemCollection();
        }

        /// <summary>
        /// Create a new report item object from database values. Private to force creation to go through
        /// <see cref="GetReportItem(IDataRecord)"/>.
        /// </summary>
        /// <param name="record">Database values.</param>
        private ReportItem(IDataRecord record) : this()
        {
            Fetch(record);
        }

        /// <summary>
        /// Create a new parent report item with the given id and vehicle details.
        /// </summary>
        /// <param name="id">Id of this parent report item.</param>
        /// <param name="vehicle">Vehicle details to set for this parent report item.</param>
        private ReportItem(int id, Vehicle vehicle) : this()
        {
            _id = id;
            _vehicle = Vehicle.NewVehicle(vehicle);
            _listInformation = ListInformation.NewListInformation();
            _marketInformation = MarketInformation.NewMarketInformation();
        }

        #endregion

        #region Data Access

        /// <summary>
        /// Populate this report item with values retrieved from the database. The type of <see cref="Vehicle"/> object
        /// created is dependent on whether this report item is a child or not.
        /// </summary>        
        /// <param name="record">Database values.</param>
        private void Fetch(IDataRecord record)
        {
            _id                = record.GetInt32(record.GetOrdinal("Id"));
            _marketInformation = MarketInformation.GetMarketInformation(record);
            _listInformation   = ListInformation.GetListInformation(record);
            _vehicle           = VehicleConfiguration.GetVehicleConfiguration(record);
        }

        #endregion

        #region IReportItem Members

        int IReportItem.Id
        {
            get { return _id; }
        }

        string IReportItem.Make
        {
            get { return Vehicle.Make; }
        }

        int IReportItem.MakeId
        {
            get { return Vehicle.MakeId; }
        }

        int IReportItem.LineId
        {
            get { return Vehicle.LineId; }
        }

        int IReportItem.ModelYear
        {
            get { return Vehicle.ModelYear; }
        }

        string IReportItem.ModelFamily
        {
            get { return Vehicle.ModelFamily; }
        }

        int IReportItem.ModelFamilyId
        {
            get { return Vehicle.ModelFamilyId; }
        }

        string IReportItem.Segment
        {
            get { return Vehicle.Segment;  }
        }

        int IReportItem.SegmentId
        {
            get { return Vehicle.SegmentId;  }
        }

        IList<IReportItem> IReportItem.ReportItems
        {
            get
            {
                List<IReportItem> reportItems = new List<IReportItem>();
                reportItems.AddRange(_reportItems);
                return reportItems;
            }
        }

        int? IReportItem.MarketDaysSupply
        {
            get { return MarketInformation.MarketDaysSupply; }
        }

        int? IReportItem.SmallestMarketDaysSupply
        {
            get { return MarketInformation.SmallestMarketDaysSupply; }
        }

        int? IReportItem.UnitsSold
        {
            get { return MarketInformation.UnitsSold; }
        }

        int? IReportItem.UnitsInStock
        {
            get { return MarketInformation.UnitsInStock; }
        }

        int? IReportItem.NumberOfListings
        {
            get { return MarketInformation.NumberOfListings; }
        }

        int? IReportItem.SumInternetPrice
        {
            get { return MarketInformation.SumInternetMileage; }
        }

        int? IReportItem.AverageInternetPrice
        {
            get { return MarketInformation.AverageInternetPrice; }
        }

        int? IReportItem.SumInternetMileage
        {
            get { return MarketInformation.SumInternetMileage; }
        }

        int? IReportItem.AverageInternetMileage
        {
            get { return MarketInformation.AverageInternetMileage; }
        }        

        int? IReportItem.Quantity
        {
            get { return ListInformation.Quantity; }
        }

        string IReportItem.Notes
        {
            get { return ListInformation.Notes; }
        }

        int? IReportItem.ModelConfigurationId
        {
            get
            {
                VehicleConfiguration vehicleConfiguration = Vehicle as VehicleConfiguration;
                return vehicleConfiguration != null ? vehicleConfiguration.ModelConfigurationId : null;
            }
        }

        string IReportItem.Series
        {
            get
            {
                VehicleConfiguration vehicleConfiguration = Vehicle as VehicleConfiguration;
                return vehicleConfiguration != null ? vehicleConfiguration.Series : null;
            }
        }

        string IReportItem.BodyType
        {
            get
            {
                VehicleConfiguration vehicleConfiguration = Vehicle as VehicleConfiguration;
                return vehicleConfiguration != null ? vehicleConfiguration.BodyType : null;
            }
        }

        string IReportItem.Transmission
        {
            get
            {
                VehicleConfiguration vehicleConfiguration = Vehicle as VehicleConfiguration;
                return vehicleConfiguration != null ? vehicleConfiguration.Transmission : null;
              }
        }

        string IReportItem.Engine
        {
            get
            {
                VehicleConfiguration vehicleConfiguration = Vehicle as VehicleConfiguration;
                return vehicleConfiguration != null ? vehicleConfiguration.Engine : null;
            }
        }

        string IReportItem.FuelType
        {
            get
            {
                VehicleConfiguration vehicleConfiguration = Vehicle as VehicleConfiguration;
                return vehicleConfiguration != null ? vehicleConfiguration.FuelType : null;
            }
        }

        string IReportItem.PassengerDoors
        {
            get
            {
                VehicleConfiguration vehicleConfiguration = Vehicle as VehicleConfiguration;
                return vehicleConfiguration != null ? vehicleConfiguration.PassengerDoors : null;
            }
        }

        string IReportItem.DriveTrain
        {
            get
            {
                VehicleConfiguration vehicleConfiguration = Vehicle as VehicleConfiguration;
                return vehicleConfiguration != null ? vehicleConfiguration.DriveTrain : null;
            }
        }

        #endregion
    }
}
