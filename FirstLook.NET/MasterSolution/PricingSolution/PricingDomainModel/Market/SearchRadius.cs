using System;
using System.Data;
using Csla;

namespace FirstLook.Pricing.DomainModel.Market
{
    /// <summary>
    /// Defines the radius of the circle, in miles, within which we will retrieve vehicle listings. Is a simple 
    /// read-only key value pair.
    /// </summary>    
    /// <remarks>
    /// See procedure: <c>Reporting.SearchRadiusCollection#Fetch</c>
    /// </remarks>
    [Serializable]
    public class SearchRadius : ReadOnlyBase<SearchRadius>
    {
        #region Business Methods

        private int    _id;   // The id of this search radius.
        private string _name; // The name of this radius, i.e. "30" for 30 days.

        public int Id
        {
            get { return _id; }
        }

        public string Name
        {
            get { return _name; }
        }

        protected override object GetIdValue()
        {
            return _id;
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Create a search radius object from database values.
        /// </summary>
        /// <param name="record">Search radius values retrieved from the database.</param>
        /// <param name="prefix">Different procs return differently named things - this abstracts that.</param>
        /// <returns>A new search radius object.</returns>
        internal static SearchRadius GetSearchRadius(IDataRecord record, string prefix)
        {
            return new SearchRadius(record, prefix);
        }

        /// <summary>
        /// Create a search radius object from database values. Is a private to force creation to go through
        /// <see cref="GetSearchRadius(IDataRecord, string)"/>.        
        /// </summary>
        /// <param name="record">Search radius values retrieved from the database.</param>
        /// <param name="prefix">Different procs return differently named things - this abstracts that.</param>
        private SearchRadius(IDataRecord record, string prefix)
        {
            Fetch(record, prefix);
        }

        #endregion

        #region Data Access

        /// <summary>
        /// Populate this object with values from the database. The actual calling to the database and such (i.e. the
        /// <see cref="DataPortal.Fetch(object)"/> call, etc) come from <see cref="SearchRadiusCollection"/>.
        /// </summary>
        /// <param name="record">Search radius values retrieved from the database.</param>
        /// <param name="prefix">Different procs return differently named things - this abstracts that.</param>
        protected void Fetch(IDataRecord record, string prefix)
        {
            _id = record.GetInt32(record.GetOrdinal(prefix + "Id"));
            _name = record.GetString(record.GetOrdinal(prefix + "Name"));
        }

        #endregion
    }
}
