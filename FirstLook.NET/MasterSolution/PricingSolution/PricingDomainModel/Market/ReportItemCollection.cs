using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Reflection;
using Csla;

namespace FirstLook.Pricing.DomainModel.Market
{
    /// <summary>
    /// Is a read-only collection of <see cref="ReportItem"/>s. The class is responsible for generating vehicle 
    /// granularity market information records from the argument vehicle-configuration granularity data-points 
    /// and structuring them in a parent / child relationship (a report item has child report items).
    /// </summary>
    [Serializable]
    public class ReportItemCollection : ReadOnlyListBase<ReportItemCollection, ReportItem>, IReportItemCollection
    {
        #region Business Methods

        private int _basisPeriod = 90; // Used for calculating market days supply.

        public int BasisPeriod
        {
            get { return _basisPeriod; }
        }

        /// <summary>
        /// Add a report item to this collection.
        /// </summary>
        /// <param name="item">Report item to add.</param>
        protected void AddChild(ReportItem item)
        {
            IsReadOnly = false;
            Add(item);
            IsReadOnly = true;
        }

        /// <summary>
        /// Filter this report item collection based on the given arguments.
        /// </summary>
        /// <param name="filterArguments">Arguments to filter this collection by.</param>
        /// <returns>A new, filtered report item collection.</returns>
        public IReportItemCollection Filter(ReportFilterArguments filterArguments)
        {
            if (filterArguments == null)
            {
                return this;
            }

            ReportItemCollection collection = 
                NewReportItemCollection(Filter(this, filterArguments, new List<ReportItem>()), _basisPeriod);

            collection.UpdateReportItemQuantities(this);

            return collection;
        }

        /// <summary>
        /// Filter a collection of report items based on the given filter arguments. Filtered subset is placed in the
        /// provided list.
        /// </summary>
        /// <param name="items">Report item collection to filter.</param>
        /// <param name="filterArguments">Filter arguments.</param>
        /// <param name="matches">Filtered subset of the report item collection.</param>
        /// <returns>Returns <c>matches</c> which has been modified to hold the filtered results.</returns>
        protected List<ReportItem> Filter(ReportItemCollection items, ReportFilterArguments filterArguments, 
                                          List<ReportItem> matches)
        {
            foreach (ReportItem item in items)
            {
                if (item.ReportItems.Count > 0)
                {
                    Filter(item.ReportItems, filterArguments, matches);
                }
                else
                {
                    if (filterArguments.Match(item))
                    {
                        matches.Add(item);
                    }
                }
            }

            return matches;
        }

        public IReportItemCollection Page(int pageIndex, int pageSize)
        {
            int startRowIndex = pageSize * pageIndex;

            int stopRowIndex = startRowIndex + pageSize;

            if (stopRowIndex > Count)
            {
                stopRowIndex = Count;
            }

            List<ReportItem> list = new List<ReportItem>();

            for (int rowIndex = startRowIndex; rowIndex < stopRowIndex; rowIndex++)
            {
                list.Add(this[rowIndex]);
            }

            return new ReportItemCollection(list, _basisPeriod, ReportCollectionStructure.AsIs);
        }

        public void Sort(SortExpression expression, bool ascending)
        {
            Sort(expression, ascending, SortStructure.Self | SortStructure.Children);
        }

        /// <summary>
        /// Sort this collection of report items by means of the provided expression. Will determine the real type of
        /// the sort expression and sort appropriately.
        /// </summary>
        /// <param name="expression">Expression to sort this collection by.</param>
        /// <param name="ascending">Direction of the sort for the expression.</param>
        /// <param name="sortStructure">Flags to Optimize Sort of children vs Parent rows</param>
        public void Sort(SortExpression expression, bool ascending, SortStructure sortStructure)
        {
            IComparer secondaryOrdering;

            if (expression.Equals(SortExpression.SmallestMarketDaysSupply))
            {
                secondaryOrdering = new MarketInformationComparer("MarketDaysSupply", ascending,
                    new MarketInformationComparer("UnitsSold", ascending,
                           new MarketInformationComparer("UnitsInStock", ascending,
                               new MarketInformationComparer("NumberOfListings", ascending,
                                    new MarketInformationComparer("AverageInternetPrice", ascending,
                                        new MarketInformationComparer("AverageInternetMileage", ascending)))))); 
            }
            else
            {
                secondaryOrdering = new MarketInformationComparer("SmallestMarketDaysSupply", ascending,
                    new MarketInformationComparer("MarketDaysSupply", ascending,
                       new MarketInformationComparer("UnitsSold", ascending,
                           new MarketInformationComparer("UnitsInStock", ascending,
                               new MarketInformationComparer("NumberOfListings", ascending,
                                    new MarketInformationComparer("AverageInternetPrice", ascending,
                                        new MarketInformationComparer("AverageInternetMileage", ascending)))))));
            }

            string propertyName = expression.ToString("G");

            IComparer comparer;

            switch (expression)
            {
                case SortExpression.MarketDaysSupply:
                case SortExpression.SmallestMarketDaysSupply:
                case SortExpression.AverageInternetMileage:
                case SortExpression.AverageInternetPrice:
                case SortExpression.UnitsInStock:
                case SortExpression.UnitsSold:
                case SortExpression.NumberOfListings:
                    comparer = new MarketInformationComparer(propertyName, ascending, secondaryOrdering);
                    break;
                case SortExpression.ModelYear:
                    comparer = new VehicleComparer<int>(propertyName, ascending, secondaryOrdering);
                    break;
                case SortExpression.Make:
                case SortExpression.ModelFamily:
                    comparer = new VehicleComparer<string>(propertyName, ascending, secondaryOrdering);
                    break;
                case SortExpression.Segment:
                    comparer = new SegmentComparer(ascending, secondaryOrdering);
                    break;
                default:
                    comparer = new MarketInformationComparer("SmallestMarketDaysSupply", ascending, secondaryOrdering);
                    break;
            }

            Sort(comparer, sortStructure);
        }

        /// <summary>
        /// Sort this report item collection with the given comparer. The comparer was determined by the other sort
        /// method.
        /// </summary>
        /// <param name="comparer">Comparer by which to sort this collection.</param>
        /// <param name="sortStructure">Flags to Optimize Sort of children vs Parent rows</param>
        private void Sort(IComparer comparer, SortStructure sortStructure)
        {
            IsReadOnly = false;

            if ((sortStructure & SortStructure.Self) == SortStructure.Self)
            {
                ArrayList.Adapter(this).Sort(comparer);
            }

            if ((sortStructure & SortStructure.Children) == SortStructure.Children)
            {
                foreach (ReportItem item in Items)
                {
                    item.ReportItems.Sort(comparer, SortStructure.Self);
                }   
            }

            IsReadOnly = true;
        }

        /// <summary>
        /// Helper base class for comparing report items. Used for sorting.
        /// </summary>
        private abstract class ReportItemComparer : IComparer
        {
            private readonly IComparer _comparer;
            private readonly bool _ascending;

            protected bool Ascending
            {
                get { return _ascending; }
            }

            protected ReportItemComparer(bool ascending, IComparer comparer)
            {
                _ascending = ascending;
                _comparer = comparer;
            }

            public int Compare(object x, object y)
            {
                ReportItem a = (ReportItem)x, b = (ReportItem)y;

                int compare = Compare(a, b);

                if (compare == 0 && _comparer != null)
                {
                    compare = _comparer.Compare(x, y);
                }

                return compare != 0 && !_ascending ? -1*compare : compare;
            }

            protected abstract int Compare(ReportItem x, ReportItem y);
        }

        /// <summary>
        /// Helper class for comparing market information. Used for sorting.
        /// </summary>
        private class MarketInformationComparer : ReportItemComparer
        {
            private readonly PropertyInfo _property;

            public MarketInformationComparer(string propertyName)
                : this(propertyName, false, null)
            {
            }

            public MarketInformationComparer(string propertyName, IComparer comparer)
                : this(propertyName, false, comparer)
            {
            }

            public MarketInformationComparer(string propertyName, bool ascending)
                : this(propertyName, ascending, null)
            {
            }

            public MarketInformationComparer(string propertyName, bool ascending, IComparer comparer): base(ascending, comparer)
            {
                _property = typeof(MarketInformation).GetProperty(propertyName);
            }

            protected override int Compare(ReportItem x, ReportItem y)
            {
                int? i = (int?)_property.GetValue(x.MarketInformation, null),
                     j = (int?)_property.GetValue(y.MarketInformation, null);

                if (!i.HasValue)
                {
                    i = Ascending ? int.MaxValue : int.MinValue;
                }
                if (!j.HasValue)
                {
                    j = Ascending ? int.MaxValue : int.MinValue;
                }

                return i.Value.CompareTo(j.Value);
            }
        }

        /// <summary>
        /// Helper class for comparing vehicles. Used for sorting.
        /// </summary>
        private class VehicleComparer<T> : ReportItemComparer where T : IComparable
        {
            private readonly PropertyInfo _property;

            public VehicleComparer(string propertyName, bool ascending, IComparer comparer): base(ascending, comparer)
            {
                _property = typeof(Vehicle).GetProperty(propertyName);
            }

            protected override int Compare(ReportItem x, ReportItem y)
            {
                T i = (T) _property.GetValue(x.Vehicle, null),
                  j = (T) _property.GetValue(y.Vehicle, null);

                return i.CompareTo(j);
            }
        }

        /// <summary>
        /// Helper class for comparing segments. Used for sorting.
        /// </summary>
        private class SegmentComparer : ReportItemComparer
        {            
            public SegmentComparer(bool ascending, IComparer comparer) : base(ascending, comparer)
            {                
            }

            protected override int Compare(ReportItem x, ReportItem y)
            {
                return x.Vehicle.Segment.CompareTo(y.Vehicle.Segment);
            }
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Create a report item collection from a list of report items. 
        /// </summary>
        /// <remarks>
        /// This function is called when a report item collection has been filtered, and the resulting report items 
        /// that match the filters must be put back into a report item collection structure.
        /// </remarks>
        /// <param name="values">List of report items to create a report item collection with.</param>
        /// <param name="basisPeriod"></param>
        /// <returns>A new report item collection object.</returns>
        internal static ReportItemCollection NewReportItemCollection(List<ReportItem> values, int basisPeriod)
        {
            return new ReportItemCollection(values, basisPeriod, ReportCollectionStructure.ParentChild);
        }

        /// <summary>
        /// Create a new report item collection from database values.
        /// </summary>
        /// <param name="reader">Reader for processing database values.</param>
        /// <returns>A new report item collection object</returns>
        internal static ReportItemCollection GetReportItemCollection(IDataReader reader)
        {
            return new ReportItemCollection(reader);
        }

        /// <summary>
        /// Create a new report item collection from database values. Is private, in theory, to force creation to go
        /// through <see cref="ReportItemCollection"/>. However, <see cref="ReportItem"/> needs to initialize its own
        /// private collection object, which requires the other constructor, sadly breaking our idealogical mold.
        /// </summary>
        /// <param name="reader">Reader for processing database values.</param>
        private ReportItemCollection(IDataReader reader)
        {
            Fetch(reader);
        }

        /// <summary>
        /// Constructor just for <see cref="ReportItem"/> to be able to initialize its report item collection member.
        /// Does nothing.
        /// </summary>        
        internal ReportItemCollection()
        {
        }

        internal enum ReportCollectionStructure
        {
            ParentChild,
            OnlyChild,
            AsIs
        }

        /// <summary>
        /// Construct a new report item collection with the given report item values.
        /// </summary>
        /// <remarks>
        /// This is used when filtering a report item collection has created a subset of report items that must then be
        /// put back into a report item collection structure.
        /// </remarks>
        /// <param name="values">Report items to place into a report item collection.</param>
        /// <param name="basisPeriod"></param>
        /// <param name="structure"></param>
        internal ReportItemCollection(IEnumerable<ReportItem> values, int basisPeriod, ReportCollectionStructure structure)
        {
            _basisPeriod = basisPeriod;

            if (structure == ReportCollectionStructure.ParentChild)
            {
                Fetch(values);    
            }
            else if (structure == ReportCollectionStructure.OnlyChild)
            {
                foreach (ReportItem item in values)
                {
                    AddChild(item);
                }
            }
            else if (structure == ReportCollectionStructure.AsIs)
            {
                foreach (ReportItem value in values)
                {
                    Items.Add(value);
                }
            }
            
        }
       

        #endregion

        #region Data Access

        /// <summary>
        /// Fetch a report item collection from the given report items. Will create the parent / child relationship
        /// that is the basis of a report item collection. Will then also ensure that the parent report items have the
        /// correct values.
        /// </summary>
        /// <remarks>
        /// Is called typically because a report item collection has been filtered, and the resulting list of filter-
        /// matching report items need to be put back into a report item collection structure.
        /// </remarks>
        /// <param name="items">List of report items.</param>
        private void Fetch(IEnumerable<ReportItem> items)
        {
            IsReadOnly = false;

            foreach (ReportItem item in items)
            {
                ProcessReportItem(item);
            }

            TallyParentInformation();

            IsReadOnly = true;
        }

        /// <summary>
        /// <para>
        /// Populate this report item collection with values retrieved from the database. Some trickery is done with 
        /// regards to the creation of parents / children. The gist of it is this: report items are stored in a 1 level
        /// tree structure. "Parent" report items do not have vehicle configuration details; "child" report items do.
        /// Child report items are stored in the database; parent items might not be.
        /// </para>
        /// <para>
        /// When parents are not stored in the database, we will automatically create them from their children 
        /// essentially by removing the configuration details. In the course of processing, however, we might find that
        /// this parent actually was returned with the db results (because it had specific quantity and notes). In that
        /// case we need to make sure our parent report item has this correct info.
        /// </para>        
        /// </summary>
        /// <param name="reader">Reader for processing database values.</param>
        private void Fetch(IDataReader reader)
        {
            if (reader.Read())
            {
                IsReadOnly = false;

                _basisPeriod = reader.GetInt16(reader.GetOrdinal("BasisPeriod"));


                if (reader.NextResult())
                {
                    while (reader.Read())
                    {
                        ReportItem reportItem = ReportItem.GetReportItem(reader);

                        ProcessReportItem(reportItem);
                    }
                }

                TallyParentInformation();

                IsReadOnly = true;
            }
        }

        /// <summary>
        /// Process the given child report item by adding it to its parent report item. If the parent does not already
        /// exist, it is created.
        /// </summary>
        /// <param name="child">Child report item to process.</param>
        private void ProcessReportItem(ReportItem child)
        {
            ReportItem parent = null;

            // Find the parent if it already exists.
            foreach (ReportItem item in Items)
            {
                if (item.Vehicle.Equals(child.Vehicle))
                {
                    parent = item;
                }
            }

            // Create the parent if it does not already exist.
            if (parent == null)
            {
                parent = ReportItem.NewReportItem(-(Items.Count + 1), child.Vehicle);

                Add(parent);
            }

            if (child.MarketInformation.MarketDaysSupply.HasValue)
            {
                parent.ReportItems.AddChild(child);
            }
            else
            {
                ReportItem other;

                foreach (ReportItem item in parent.ReportItems)
                {
                    if (((IReportItem) item).Id < 0)
                    {
                        other = item;

                        goto EOS;
                    }
                }

                other = ReportItem.NewReportItem(int.MinValue + Items.Count, parent.Vehicle); // ...

                parent.ReportItems.AddChild(other);

                EOS:

                other.ReportItems.AddChild(child);
            }
        }

        /// <summary>
        /// Rollup sums and averages data for parents from their children's data.
        /// </summary>
        private void TallyParentInformation()
        {
            foreach (ReportItem item in Items)
            {
                TallyInformation(item, item.ReportItems);
            }
        }

        private void TallyInformation(ReportItem parent, IEnumerable<ReportItem> children)
        {
            int sumUnitsSold = 0;
            int sumUnitsInStock = 0;
            int sumListings = 0;
            int sumPriceComparableUnits = 0;
            int sumMileageComparableUnits = 0;
            int sumPrice = 0;
            int sumMileage = 0;

            foreach (ReportItem child in children)
            {
                bool hasChildren = child.ReportItems.Count > 0;                

                if (hasChildren)
                {
                    TallyInformation(child, child.ReportItems);
                }

                MarketInformation marketInfo = child.MarketInformation;

                if (marketInfo.UnitsSold.HasValue)
                {
                    sumUnitsSold += marketInfo.UnitsSold.Value;
                }

                if (marketInfo.UnitsInStock.HasValue)
                {
                    sumUnitsInStock += marketInfo.UnitsInStock.Value;
                }

                if (marketInfo.NumberOfListings.HasValue)
                {
                    sumListings += marketInfo.NumberOfListings.Value;
                }

                if (marketInfo.SumInternetPrice.HasValue)
                {
                    sumPrice += marketInfo.SumInternetPrice.Value;
                }

                if (marketInfo.PriceComparableUnits.HasValue)
                {
                    sumPriceComparableUnits += marketInfo.PriceComparableUnits.Value; 
                }

                if (marketInfo.SumInternetMileage.HasValue)
                {
                    sumMileage += marketInfo.SumInternetMileage.Value;
                }

                if (marketInfo.MileageComparableUnits.HasValue)
                {
                    sumMileageComparableUnits += marketInfo.MileageComparableUnits.Value;
                }

                if (!hasChildren)
                {
                    child.MarketInformation.SmallestMarketDaysSupply = marketInfo.MarketDaysSupply;

                    if (marketInfo.MarketDaysSupply.HasValue)
                    {
                        if (!parent.MarketInformation.SmallestMarketDaysSupply.HasValue)
                        {
                            parent.MarketInformation.SmallestMarketDaysSupply = marketInfo.MarketDaysSupply;
                        }

                        if (marketInfo.MarketDaysSupply.Value < parent.MarketInformation.SmallestMarketDaysSupply.GetValueOrDefault())
                        {
                            parent.MarketInformation.SmallestMarketDaysSupply = marketInfo.MarketDaysSupply.Value;
                        }
                    } 
                }
            }
                
            parent.MarketInformation.AverageInternetMileage = 
                (sumMileageComparableUnits > 0) ? sumMileage / sumMileageComparableUnits : 0;

            parent.MarketInformation.AverageInternetPrice =
                (sumPriceComparableUnits > 0) ? sumPrice / sumPriceComparableUnits : 0;

            parent.MarketInformation.UnitsSold = sumUnitsSold;
            parent.MarketInformation.NumberOfListings = sumListings;
            parent.MarketInformation.SumInternetMileage = sumMileage;
            parent.MarketInformation.SumInternetPrice = sumPrice;
            parent.MarketInformation.MileageComparableUnits = sumMileageComparableUnits;
            parent.MarketInformation.PriceComparableUnits = sumPriceComparableUnits;
            if (sumListings >= 5 && sumUnitsSold >= 20)
            {
                double parameters = (sumListings) / (sumUnitsSold / (double)_basisPeriod);
                parameters = Math.Round(parameters, MidpointRounding.AwayFromZero);
                parent.MarketInformation.MarketDaysSupply = ((IConvertible)parameters).ToInt32(CultureInfo.InvariantCulture);                    
            }
            else
            {
                parent.MarketInformation.MarketDaysSupply = null;
            }

            parent.MarketInformation.UnitsInStock = sumUnitsInStock;
        }

        /// <summary>
        /// Update the quantities for all first level report items (i.e. not the report items that are children of 
        /// these report items).
        /// </summary>
        /// <param name="reader">Quantities retrieved from the database.</param>
        internal void UpdateReportItemQuantities(IDataReader reader)
        {
            while (reader.Read())
            {
                Vehicle vehicle = Vehicle.GetVehicle(reader);

                int quantity = reader.GetInt32(reader.GetOrdinal("Quantity"));

                UpdateReportItemQuantities(vehicle, quantity);
            }
        }

        /// <summary>
        /// Update the quantities of this collection of report items with the values of the passed in collection of
        /// report items.
        /// </summary>
        /// <param name="items">Collection of report items.</param>
        internal void UpdateReportItemQuantities(IEnumerable<ReportItem> items)
        {
            foreach (ReportItem item in items)
            {
                int? quantity = item.ListInformation.Quantity;

                if (quantity.HasValue)
                {
                    UpdateReportItemQuantities(item.Vehicle, quantity.Value);
                }
            }
        }

        /// <summary>
        /// Update the quantity for the 
        /// </summary>
        /// <param name="vehicle"></param>
        /// <param name="quantity"></param>
        private void UpdateReportItemQuantities(Vehicle vehicle, int quantity)
        {
            foreach (ReportItem reportItem in Items)
            {
                if (reportItem.Vehicle.Equals(vehicle))
                {
                    reportItem.ListInformation.Quantity = quantity;

                    break;
                }
            }
        }

        #endregion

        #region Implementation of IList<IReportItem>

        int IList<IReportItem>.IndexOf(IReportItem item)
        {
            for (int i = 0; i < Count; i++)
            {
                if (((IReportItem) Items[i]).Id == item.Id)
                    return i;
            }

            return -1;
        }

        void IList<IReportItem>.Insert(int index, IReportItem item)
        {
            throw new NotSupportedException();
        }

        IReportItem IList<IReportItem>.this[int index]
        {
            get { return Items[index]; }
            set { throw new NotSupportedException(); }
        }

        #endregion

        #region IList<IReportItem> Members

        void IList<IReportItem>.RemoveAt(int index)
        {
            throw new NotSupportedException();
        }

        #endregion

        #region ICollection<IReportItem> Members

        void ICollection<IReportItem>.Add(IReportItem item)
        {
            throw new NotSupportedException();
        }

        void ICollection<IReportItem>.Clear()
        {
            throw new NotSupportedException();
        }

        bool ICollection<IReportItem>.Contains(IReportItem item)
        {
            return ((IList<IReportItem>) this).IndexOf(item) != -1;
        }

        void ICollection<IReportItem>.CopyTo(IReportItem[] array, int arrayIndex)
        {
            int i = arrayIndex;
            foreach (ReportItem item in this)
            {
                array[i++] = item;
            }
        }

        int ICollection<IReportItem>.Count
        {
            get { return Items.Count; }
        }

        bool ICollection<IReportItem>.IsReadOnly
        {
            get { return true; }
        }

        bool ICollection<IReportItem>.Remove(IReportItem item)
        {
            throw new NotSupportedException();
        }

        #endregion

        #region IEnumerable<IReportItem> Members

        IEnumerator<IReportItem> IEnumerable<IReportItem>.GetEnumerator()
        {
            List<IReportItem> list = new List<IReportItem>();

            foreach (ReportItem item in Items)
            {
                list.Add(item);
            }

            return list.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}