using System;
using System.Data;

namespace FirstLook.Pricing.DomainModel.Market
{
    /// <summary>
    /// Contains market information on a vehicle or vehicle-configuration. Its properties are: average internet price, 
    /// average internet mileage, market days supply, number of listings, sum (internet price), sum (internet mileage), 
    /// units in stock and units sold. The sum columns are provided so data on a vehicle-configuration can be rolled up 
    /// to vehicle granularity.
    /// </summary>
    [Serializable]
    public class MarketInformation
    {
        #region Business Methods

        private int? _marketDaysSupply;       // Number-of-days-worth a given vehicle is stocked.
        private int? _smallestMarketDaysSupply; // Smallest Market days supply of a set
        private int? _unitsSold;              // How many of a given vehicle / vehicle-configuration have sold.
        private int? _unitsInStock;           // Number vehicles / vehicle-configurations in stock.
        private int? _numberOfListings;       // Number of market listings.
        private int? _sumInternetPrice;       // Sum of the internet price of all contained vehicle-configurations.
        private int? _averageInternetPrice;   // Average internet price of all contained vehicle-configurations.
        private int? _priceComparableUnits;   // Actual number of listings used to derive average from sum.
        private int? _sumInternetMileage;     // Sum of the internet mileage of all contained vehicle-configurations.
        private int? _averageInternetMileage; // Average internet mileage of all contained vehicle-configurations.
        private int? _mileageComparableUnits; // Actual number of listings used to derive average from sum.        

        public int? MarketDaysSupply
        {
            get { return _marketDaysSupply; }
            internal set { _marketDaysSupply = value; }
        }

        public int? SmallestMarketDaysSupply
        {
            get { return _smallestMarketDaysSupply; }
            internal set { _smallestMarketDaysSupply = value; }
        }

        public int? UnitsSold
        {
            get { return _unitsSold; }
            internal set { _unitsSold = value; }
        }

        public int? UnitsInStock
        {
            get { return _unitsInStock; }
            internal set { _unitsInStock = value; }
        }

        public int? NumberOfListings
        {
            get { return _numberOfListings; }
            internal set { _numberOfListings = value; }
        }

        internal int? SumInternetPrice
        {
            get { return _sumInternetPrice; }
            set { _sumInternetPrice = value; }
        }

        public int? AverageInternetPrice
        {
            get { return _averageInternetPrice; }
            internal set { _averageInternetPrice = value; }
        }

        public int? PriceComparableUnits
        {
            get { return _priceComparableUnits; }
            internal set { _priceComparableUnits = value; }
        }

        internal int? SumInternetMileage
        {
            get { return _sumInternetMileage; }
            set { _sumInternetMileage = value; }
        }

        public int? AverageInternetMileage
        {
            get { return _averageInternetMileage; }
            internal set { _averageInternetMileage = value; }
        }       
        
        public int? MileageComparableUnits
        {
            get { return _mileageComparableUnits; }
            internal set { _mileageComparableUnits = value; }
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Create a new, emtpy market information object.
        /// </summary>
        /// <returns>A market information object.</returns>
        internal static MarketInformation NewMarketInformation()
        {
            return new MarketInformation();
        }

        /// <summary>
        /// Create a market listing information object with values from the database.
        /// </summary>
        /// <param name="record">Market information values retrieved from the database.</param>
        /// <returns>A new market information object.</returns>
        internal static MarketInformation GetMarketInformation(IDataRecord record)
        {
            return new MarketInformation(record);
        }

        /// <summary>
        /// Create an empty market information object. Private to force construction to go through factory methods.
        /// </summary>
        private MarketInformation()
        {
        }

        /// <summary>
        /// Create a new market information object with database values. Private to force create to go through 
        /// <see cref="GetMarketInformation(IDataRecord)"/>.
        /// </summary>
        /// <param name="record">Market information values retrieved from the database.</param>
        private MarketInformation(IDataRecord record)
        {
            Fetch(record);
        }

        #endregion

        #region Data Access

        /// <summary>
        /// Populate this market information object with values retrieved from the database.
        /// </summary>
        /// <param name="record">Market information values retrieved from the database.</param>
        private void Fetch(IDataRecord record)
        {
            if (!record.IsDBNull(record.GetOrdinal("MarketDaysSupply")))
            {
                _marketDaysSupply = record.GetInt32(record.GetOrdinal("MarketDaysSupply"));
            }
            if (!record.IsDBNull(record.GetOrdinal("UnitsSold")))
            {
                _unitsSold = record.GetInt32(record.GetOrdinal("UnitsSold"));
            }
            if (!record.IsDBNull(record.GetOrdinal("UnitsInStock")))
            {
                _unitsInStock = record.GetInt32(record.GetOrdinal("UnitsInStock"));
            }
            if (!record.IsDBNull(record.GetOrdinal("NumberOfListings")))
            {
                _numberOfListings = record.GetInt32(record.GetOrdinal("NumberOfListings"));
            }
            if (!record.IsDBNull(record.GetOrdinal("SumInternetPrice")))
            {
                _sumInternetPrice = record.GetInt32(record.GetOrdinal("SumInternetPrice"));
            }
            if (!record.IsDBNull(record.GetOrdinal("AverageInternetPrice")))
            {
                _averageInternetPrice = record.GetInt32(record.GetOrdinal("AverageInternetPrice"));
            }            
            if (!record.IsDBNull(record.GetOrdinal("SumInternetMileage")))
            {
                _sumInternetMileage = record.GetInt32(record.GetOrdinal("SumInternetMileage"));
            }
            if (!record.IsDBNull(record.GetOrdinal("AverageInternetMileage")))
            {
                _averageInternetMileage = record.GetInt32(record.GetOrdinal("AverageInternetMileage"));
            }

            // Check if the query used returned price- and mileage-comparable units.
            bool hasPriceComparableUnits = false;
            bool hasMileageComparableUnits = false;

            for (int i = 0; i < record.FieldCount; i++ )
            {
                string fieldName = record.GetName(i);
                if (fieldName.Equals("PriceComparableUnits", StringComparison.InvariantCulture))
                {
                    hasPriceComparableUnits = true;
                }
                else if (fieldName.Equals("MileageComparableUnits", StringComparison.InvariantCulture))
                {
                    hasMileageComparableUnits = true;
                }
            }
            if (hasPriceComparableUnits && !record.IsDBNull(record.GetOrdinal("PriceComparableUnits")))
            {
                _priceComparableUnits = record.GetInt32(record.GetOrdinal("PriceComparableUnits"));
            }
            if (hasMileageComparableUnits && !record.IsDBNull(record.GetOrdinal("MileageComparableUnits")))
            {
                _mileageComparableUnits = record.GetInt32(record.GetOrdinal("MileageComparableUnits"));
            }
        }

        #endregion
    }
}
