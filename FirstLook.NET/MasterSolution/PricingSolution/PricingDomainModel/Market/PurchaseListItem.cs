using System;
using System.Collections.Generic;
using System.Data;
using Csla;
using Csla.Validation;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Market
{
    /// <summary>
    /// Is an editable child class whose editable properties (quantity, notes) define how many of a vehicle (or 
    /// vehicle-configuration) are desired. The class has read-only audit columns (insert user/date and optional update
    /// user/date) and a private version property for optimistic locking.
    /// </summary>
    /// <remarks>
    /// See procedures: <c>Purchasing.PurchaseListItem#Insert</c>, <c>Purchasing.PurchaseListItem#Update</c>, 
    /// <c>Purchasing.PurchaseListItem#DeleteSelf</c>.
    /// </remarks>
    [Serializable]
    public class PurchaseListItem : BusinessBase<PurchaseListItem>, IReportItem
    {
        #region Business Methods

        private int               _id;                // Id of the purchase list (not this item).
        private int               _quantity;          // How many of this vehicle are on the list to buy?
        private string            _notes;             // Notes added by the user.
        private Vehicle           _vehicle;           // The vehicle of this purchase list item.
        private MarketInformation _marketInformation; // Market info - e.g. sum mileage, etc.                
        private DateTime          _insertDate;        // Date this item was added to the purchase list.
        private string            _insertUser;        // User that added this vehicle to the purchase list.
        private DateTime?         _updateDate;        // Date this item was updated, if it ever was.
        private string            _updateUser;        // User that updated this vehicle, if it ever was.        
        private byte[]            _version;           // Version of this item.

        public int Id
        {
            get { return _id; }
        }

        public int Quantity
        {
            get { return _quantity; }
            set
            {
                if (value != _quantity)
                {
                    if (value < 0)
                    {
                        _quantity = 0;
                    }
                    else if (value > 99)
                    {
                        _quantity = 99;
                    }
                    else
                    {
                        _quantity = value;   
                    }                    
                    PropertyHasChanged("Quantity");
                }
            }
        }

        public string Notes
        {
            get { return _notes; }
            set
            {
                if (value != _notes)
                {
                    if (value.Length > 2000)
                    {
                        _notes = value.Substring(0, 2000);
                    }
                    else
                    {
                        _notes = value;   
                    }                    
                    PropertyHasChanged("Notes");
                }
            }
        }

        public Vehicle Vehicle
        {
            get { return _vehicle; }
        }        

        public MarketInformation MarketInformation
        {
            get { return _marketInformation; }
        }

        public DateTime InsertDate
        {
            get { return _insertDate; }
        }

        public string InsertUser
        {
            get { return _insertUser; }
        }

        public DateTime? UpdateDate
        {
            get { return _updateDate; }
        }

        public string UpdateUser
        {
            get { return _updateUser; }
            set
            {
                if (value != _updateUser)
                {
                    _updateUser = value;
                    PropertyHasChanged("UpdateUser");
                }
            }
        }

        public byte[] Version
        {
            get { return _version; }
        }        

        protected override object GetIdValue()
        {
            return _id;
        }

        #endregion

        #region Validation

        /// <summary>
        /// Add validation rules to verify this purchase list item object is valid.
        /// </summary>
        protected override void AddBusinessRules()
        {
            ValidationRules.AddRule(CommonRules.IntegerMinValue, new CommonRules.IntegerMinValueRuleArgs("Quantity", 0));            
            ValidationRules.AddRule(CommonRules.StringMaxLength, new CommonRules.MaxLengthRuleArgs("Notes", 2000));
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Create a purchase list item object from database values.
        /// </summary>
        /// <param name="record">Database values.</param>
        /// <returns>A new purchase list item object.</returns>        
        internal static PurchaseListItem GetPurchaseListItem(IDataRecord record)
        {
            return new PurchaseListItem(record);
        }

        /// <summary>
        /// Create a new purchase list item. Private to force use of factory methods.
        /// </summary>
        /// <param name="record">Database values.</param>        
        private PurchaseListItem(IDataRecord record)
        {
            MarkAsChild();
            Fetch(record);
        }

        #endregion

        #region Data Access

        /// <summary>
        /// Populate this purchase list item with values retrieved from the database.
        /// </summary>
        /// <param name="record">Database values to read.</param>
        protected void Fetch(IDataRecord record)
        {            
            _id                = record.GetInt32(record.GetOrdinal("ID"));
            _insertUser        = record.GetString(record.GetOrdinal("InsertUser"));
            _insertDate        = record.GetDateTime(record.GetOrdinal("InsertDate"));
            _version           = (byte[])record.GetValue(record.GetOrdinal("Version"));
            _vehicle           = VehicleConfiguration.GetVehicleConfiguration(record);
            _marketInformation = MarketInformation.GetMarketInformation(record);

            if (!record.IsDBNull(record.GetOrdinal("Quantity")))
            {
                _quantity = record.GetInt32(record.GetOrdinal("Quantity"));
            }
            if (!record.IsDBNull(record.GetOrdinal("Notes")))
            {
                _notes = record.GetString(record.GetOrdinal("Notes"));
            }
            if (!record.IsDBNull(record.GetOrdinal("UpdateUser")))
            {
                _updateUser = record.GetString(record.GetOrdinal("UpdateUser"));
            }
            if (!record.IsDBNull(record.GetOrdinal("UpdateDate")))
            {
                _updateDate = record.GetDateTime(record.GetOrdinal("UpdateDate"));
            }                        

            MarkOld();
        }

        /// <summary>
        /// Delete this purchase list item in the database.
        /// </summary>
        /// <param name="connection">Database connection.</param>
        /// <param name="transaction">Database transaction.</param>
        /// <param name="ownerHandle">Owner handle.</param>
        internal void DeleteSelf(IDbConnection connection, IDbTransaction transaction, string ownerHandle)
        {
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }

            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "Purchasing.PurchaseListItem#DeleteSelf";
                command.Transaction = transaction;

                string deleteUser = System.Threading.Thread.CurrentPrincipal.Identity.Name;
               
                command.AddParameterWithValue("OwnerHandle", DbType.String, false, ownerHandle);
                command.AddParameterWithValue("Id",          DbType.Int32,  false, _id);
                command.AddParameterWithValue("DeleteUser",  DbType.String, false, deleteUser);
                command.AddParameterWithValue("Version",     DbType.Binary, false, _version);

                command.ExecuteNonQuery();
            }

            MarkNew();
        }

        /// <summary>
        /// Insert this purchase list item into the database.
        /// </summary>
        /// <param name="connection">Database connection.</param>
        /// <param name="transaction">Database transaction.</param>
        /// <param name="ownerHandle">Owner handle.</param>
        internal void Insert(IDbConnection connection, IDbTransaction transaction, string ownerHandle)
        {
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }

            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "Purchasing.PurchaseListItem#Insert";
                command.Transaction = transaction;                

                command.AddParameterWithValue("OwnerHandle",          DbType.String, false, ownerHandle);
                command.AddParameterWithValue("ModelYear",            DbType.Int32,  false, _vehicle.ModelYear);
                command.AddParameterWithValue("ModelFamilyID",        DbType.Int32,  false, _vehicle.ModelFamilyId);
                command.AddParameterWithValue("SegmentID",            DbType.Int32,  false, _vehicle.SegmentId);
                command.AddParameterWithValue("ModelConfigurationID", DbType.Int32,  false, ModelConfigurationId);
                command.AddParameterWithValue("Quantity",             DbType.Int32,  false, _quantity);
                command.AddParameterWithValue("Notes",                DbType.String, false, _notes);
                command.AddParameterWithValue("InsertUser",           DbType.String, false, _insertUser);                

                using (IDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        _id = reader.GetInt32(reader.GetOrdinal("Id"));
                        _version = (byte[])reader.GetValue(reader.GetOrdinal("NewVersion"));
                    }
                }
            }
        }

        /// <summary>
        /// Update the database entry for this purchase list item.
        /// </summary>
        /// <param name="connection">Database connection.</param>
        /// <param name="transaction">Database transaction.</param>
        /// <param name="ownerHandle">Owner handle.</param>
        internal void Update(IDbConnection connection, IDbTransaction transaction, string ownerHandle)
        {
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }

            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "Purchasing.PurchaseListItem#Update";
                command.Transaction = transaction;

                command.AddParameterWithValue("OwnerHandle", DbType.String, false, ownerHandle);
                command.AddParameterWithValue("Id",          DbType.Int32,  false, _id);
                command.AddParameterWithValue("Quantity",    DbType.Int32,  false, _quantity);                
                command.AddParameterWithValue("UpdateUser",  DbType.String, false, _updateUser);
                command.AddParameterWithValue("Version",     DbType.Binary, false, _version);

                if (string.IsNullOrEmpty(_notes))
                {
                    command.AddParameterWithValue("Notes", DbType.String, true, DBNull.Value); 
                }
                else
                {
                    command.AddParameterWithValue("Notes", DbType.String, false, _notes); 
                }

                using (IDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {                        
                        _version = (byte[])reader.GetValue(reader.GetOrdinal("NewVersion"));
                    }
                }
            }                    
        }

        #endregion

        #region IReportItem Members

        int IReportItem.Id
        {
            get { return Id; }
        }

        IList<IReportItem> IReportItem.ReportItems
        {
            get { return new List<IReportItem>(); }
        }

        string IReportItem.Make
        {
            get { return Vehicle.Make; }
        }

        int IReportItem.MakeId
        {
            get { return Vehicle.MakeId; }
        }

        int IReportItem.LineId
        {
            get { return Vehicle.LineId; }
        }

        int IReportItem.ModelYear
        {
            get { return Vehicle.ModelYear; }
        }

        string IReportItem.ModelFamily
        {
            get { return Vehicle.ModelFamily; }
        }

        int IReportItem.ModelFamilyId
        {
            get { return Vehicle.ModelFamilyId; }
        }

        string IReportItem.Segment
        {
            get { return Vehicle.Segment; }
        }

        int IReportItem.SegmentId
        {
            get { return Vehicle.SegmentId; }
        }

        public int? ModelConfigurationId
        {
            get
            {
                VehicleConfiguration vehicleConfiguration = Vehicle as VehicleConfiguration;
                return vehicleConfiguration != null ? vehicleConfiguration.ModelConfigurationId : null;
            }
        }

        string IReportItem.Series
        {
            get
            {
                VehicleConfiguration vehicleConfiguration = Vehicle as VehicleConfiguration;
                return vehicleConfiguration != null ? vehicleConfiguration.Series : null;
            }
        }

        string IReportItem.BodyType
        {
            get
            {
                VehicleConfiguration vehicleConfiguration = Vehicle as VehicleConfiguration;
                return vehicleConfiguration != null ? vehicleConfiguration.BodyType : null;
            }
        }

        string IReportItem.Transmission
        {
            get
            {
                VehicleConfiguration vehicleConfiguration = Vehicle as VehicleConfiguration;
                return vehicleConfiguration != null ? vehicleConfiguration.Transmission : null;
            }
        }

        string IReportItem.Engine
        {
            get
            {
                VehicleConfiguration vehicleConfiguration = Vehicle as VehicleConfiguration;
                return vehicleConfiguration != null ? vehicleConfiguration.Engine : null;
            }
        }

        string IReportItem.FuelType
        {
            get
            {
                VehicleConfiguration vehicleConfiguration = Vehicle as VehicleConfiguration;
                return vehicleConfiguration != null ? vehicleConfiguration.FuelType : null;
            }
        }

        string IReportItem.PassengerDoors
        {
            get
            {
                VehicleConfiguration vehicleConfiguration = Vehicle as VehicleConfiguration;
                return vehicleConfiguration != null ? vehicleConfiguration.PassengerDoors : null;
            }
        }

        string IReportItem.DriveTrain
        {
            get
            {
                VehicleConfiguration vehicleConfiguration = Vehicle as VehicleConfiguration;
                return vehicleConfiguration != null ? vehicleConfiguration.DriveTrain : null;
            }
        }

        int? IReportItem.MarketDaysSupply
        {
            get { return MarketInformation.MarketDaysSupply; }
        }

        int? IReportItem.SmallestMarketDaysSupply
        {
            get { return MarketInformation.SmallestMarketDaysSupply; }
        }

        int? IReportItem.UnitsSold
        {
            get { return MarketInformation.UnitsSold; }
        }

        int? IReportItem.UnitsInStock
        {
            get { return MarketInformation.UnitsInStock; }
        }

        int? IReportItem.NumberOfListings
        {
            get { return MarketInformation.NumberOfListings; }
        }

        int? IReportItem.SumInternetPrice
        {
            get { return MarketInformation.SumInternetMileage; }
        }

        int? IReportItem.AverageInternetPrice
        {
            get { return MarketInformation.AverageInternetPrice; }
        }        

        int? IReportItem.SumInternetMileage
        {
            get { return MarketInformation.SumInternetMileage; }
        }

        int? IReportItem.AverageInternetMileage
        {
            get { return MarketInformation.AverageInternetMileage; }
        }        

        int? IReportItem.Quantity
        {
            get { return Quantity; }
        }

        string IReportItem.Notes
        {
            get { return Notes; }
        }

        #endregion
    }
}
