using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Market
{
    /// <summary>
    /// Command object for upserting a purchase list item into a purchase list. Although <see cref="PurchaseList"/> and
    /// <see cref="PurchaseListItem"/> offer similar functionality, this command is for use by a Report page which does
    /// not readily have an instance of a purchase list class.
    /// </summary>
    /// <remarks>
    /// See procedure: <c>PurchaseListItem#Change</c>
    /// </remarks>
    [Serializable]
    public class UpdatePurchaseListItem : CommandBase
    {
        private readonly string _ownerHandle;          // Handle of the owner whose purchase list this is.
        private readonly int    _modelYear;            // Model year of the list item vehicle.
        private readonly int    _modelFamilyId;        // Model family ID of the list item vehicle.
        private readonly int    _segmentId;            // Segment ID of the list item vehicle.
        private readonly int?   _modelConfigurationId; // Possibly null configuration ID of the list item vehicle.
        private readonly int    _quantity;             // How many of this vehicle are on the purchase list?
        private readonly string _notes;                // User-entered notes on this vehicle.
        private readonly string _changeUser;           // User who has made these changes.

        /// <summary>
        /// Create a new update command with the given values.
        /// </summary>
        /// <param name="ownerHandle">Handle of the owner whose purchase list this is.</param>
        /// <param name="modelYear">Model year of the list item vehicle.</param>
        /// <param name="modelFamilyId">Model family ID of the list item vehicle.</param>
        /// <param name="segmentId">Segment ID of the list item vehicle.</param>
        /// <param name="modelConfigurationId">Possibly null configuration ID of the list item vehicle.</param>
        /// <param name="quantity">How many of this vehicle are on the purchase list?</param>
        /// <param name="notes">User-entered notes on this vehicle.</param>
        /// <param name="changeUser">User who has made these changes.</param>
        public UpdatePurchaseListItem(string ownerHandle, int modelYear, int modelFamilyId, int segmentId,
                                      int? modelConfigurationId, int quantity, string notes, string changeUser)
        {
            _ownerHandle          = ownerHandle;
            _modelYear            = modelYear;
            _modelFamilyId        = modelFamilyId;
            _segmentId            = segmentId;
            _modelConfigurationId = modelConfigurationId;
            _quantity             = quantity;
            _notes                = notes;
            _changeUser           = changeUser;
        }

        /// <summary>
        /// Execute the updating of this purchase list item.
        /// </summary>
        /// <param name="ownerHandle">Handle of the owner whose purchase list this is.</param>
        /// <param name="modelYear">Model year of the list item vehicle.</param>
        /// <param name="modelFamilyId">Model family ID of the list item vehicle.</param>
        /// <param name="segmentId">Segment ID of the list item vehicle.</param>
        /// <param name="modelConfigurationId">Possibly null configuration ID of the list item vehicle.</param>
        /// <param name="quantity">How many of this vehicle are on the purchase list?</param>
        /// <param name="notes">User-entered notes on this vehicle.</param>
        /// <param name="changeUser">User who has made these changes.</param>
        public static void Execute(string ownerHandle, int modelYear, int modelFamilyId, int segmentId,
                                   int? modelConfigurationId, int quantity, string notes, string changeUser)
        {
            // to be  consistent with PurchaseListItem.Quantity, PM
            if (quantity > 99)
            {
                quantity = 99;
            }

            UpdatePurchaseListItem command = 
                new UpdatePurchaseListItem(ownerHandle, modelYear, modelFamilyId, segmentId, modelConfigurationId, 
                                           quantity, notes, changeUser);
            DataPortal.Execute(command);
        }                

        /// <summary>
        /// Execute this update command. Called by the data portal. The called proc will determine whether this is an
        /// insert or an update based on whether an entry already exists for this combination of values.
        /// </summary>
        protected override void DataPortal_Execute()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.ListDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandText = "Purchasing.PurchaseListItem#Change";
                    command.CommandType = CommandType.StoredProcedure;

                    command.AddParameterWithValue("OwnerHandle",          DbType.String, false, _ownerHandle);
                    command.AddParameterWithValue("ModelYear",            DbType.Int32,  false, _modelYear);
                    command.AddParameterWithValue("ModelFamilyID",        DbType.Int32,  false, _modelFamilyId);
                    command.AddParameterWithValue("SegmentID",            DbType.Int32,  false, _segmentId);
                    command.AddParameterWithValue("Quantity",             DbType.Int32,  false, _quantity);
                    command.AddParameterWithValue("InsertUser",           DbType.String, false, _changeUser);

                    if (string.IsNullOrEmpty(_notes))
                    {
                        command.AddParameterWithValue("Notes", DbType.String, true, DBNull.Value);
                    }
                    else
                    {
                        command.AddParameterWithValue("Notes", DbType.String, true, _notes);
                    }

                    if (_modelConfigurationId != null)
                    {
                        command.AddParameterWithValue("ModelConfigurationID", DbType.Int32, true, _modelConfigurationId);
                    }
                    else
                    {
                        command.AddParameterWithValue("ModelConfigurationID", DbType.Int32, true, DBNull.Value);
                    }                    

                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
