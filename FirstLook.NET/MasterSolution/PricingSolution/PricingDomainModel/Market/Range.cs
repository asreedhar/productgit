namespace FirstLook.Pricing.DomainModel.Market
{
    /// <summary>
    /// Class for defining a range of values.
    /// </summary>
    public class Range
    {
        public static Range Unlimited = new Range(0, int.MaxValue); // Range over all integer values.

        private readonly int _lower; // Lower bound of this range.

        private readonly int _upper; // Upper bound of this range.

        /// <summary>
        /// Create a range over the given values.
        /// </summary>
        /// <param name="lower">Lower bound to the range.</param>
        /// <param name="upper">Upper bound to the range.</param>
        public Range(int lower, int upper)
        {
            _lower = lower;
            _upper = upper;
        }
        
        public int Lower
        {
            get { return _lower; }
        }

        public int Upper
        {
            get { return _upper; }
        }

        /// <summary>
        /// Is a given value contained in this range?
        /// </summary>
        /// <param name="value">Value to check for in this range.</param>
        /// <returns>True if the value is in the range, false otherwise.</returns>
        public bool Contains(int value)
        {
            return value >= Lower && value <= Upper;
        }

        public bool Equals(Range other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other._lower == _lower && other._upper == _upper;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (Range)) return false;
            return Equals((Range) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (_lower*397) ^ _upper;
            }
        }

        public static bool operator ==(Range left, Range right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Range left, Range right)
        {
            return !Equals(left, right);
        }
    }
}