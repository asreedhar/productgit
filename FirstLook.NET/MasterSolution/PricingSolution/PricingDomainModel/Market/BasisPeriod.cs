using System;
using System.Data;
using Csla;

namespace FirstLook.Pricing.DomainModel.Market
{
    /// <summary>
    /// Defines a time period (i.e. days preceeding today) for which we want to refine our report. Is a simple read-only
    /// key / value pair.    
    /// </summary>
    /// <remarks>
    /// See procedure: <c>Reporting.BasisPeriodCollection#Fetch</c>
    /// </remarks>
    [Serializable]
    public class BasisPeriod : ReadOnlyBase<BasisPeriod>
    {
        #region Business Methods

        private int    _id;   // Identifier of this basis period.
        private string _name; // Name of this basis period.

        public int Id
        {
            get { return _id; }
        }

        public string Name
        {
            get { return _name; }
        }

        protected override object GetIdValue()
        {
            return _id;
        }

        #endregion

        #region Factory Methods
        
        /// <summary>
        /// Create a basis period object from database values.
        /// </summary>
        /// <param name="record">Database row with basis period values.</param>
        /// <param name="prefix">Different procs return differently named things - this abstracts that.</param>
        /// <returns>A new basis period object.</returns>
        internal static BasisPeriod GetBasisPeriod(IDataRecord record, string prefix)
        {
            return new BasisPeriod(record, prefix);            
        }

        /// <summary>
        /// Create a basis period object from database values. Private to force creation to go through
        /// <see cref="GetBasisPeriod(IDataRecord, string)"/>.
        /// </summary>
        /// <param name="record">Database row with basis period values.</param>
        /// <param name="prefix">Different procs return differently named things - this abstracts that.</param>
        private BasisPeriod(IDataRecord record, string prefix)
        {
            Fetch(record, prefix);            
        }       

        #endregion

        #region Data Access
        
        /// <summary>
        /// Populate this basis period object with values retrieved from the database. The actual calling to the 
        /// database and such (i.e. the <see cref="DataPortal.Fetch(object)"/> call, etc.) come from 
        /// <see cref="BasisPeriodCollection"/>.
        /// </summary>
        /// <param name="record">Database values.</param>
        /// <param name="prefix">Different procs return differently named things - this abstracts that.</param>
        protected void Fetch(IDataRecord record, string prefix)
        {
            _id = record.GetInt32(record.GetOrdinal(prefix + "Id"));
            _name = record.GetString(record.GetOrdinal(prefix + "Name"));
        }

        #endregion        
    }
}
