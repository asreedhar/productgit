using System;
using System.Data;

namespace FirstLook.Pricing.DomainModel.Market
{
    /// <summary>
    /// Is a read-only class that describes a vehicle using the properties: model year, model family and segment. The
    /// class has both string and id values for model family and segment. The triplet of ids can be used to add a 
    /// vehicle to an owner's purchase list.
    /// </summary>
    [Serializable]
    public class Vehicle
    {
        #region Business Methods

        private string _make;          // Vehicle make.
        private int    _makeId;        // Vehicle make id.
        private int    _lineId;        // Vehicle line id.
        private int    _modelYear;     // Vehicle model year.
        private string _modelFamily;   // Vehicle model family.
        private int    _modelFamilyId; // Vehicle model family id.
        private string _segment;       // Vehicle segment.
        private int    _segmentId;     // Vehicle segment id.

        public string Make
        {
            get { return _make;  }
        }

        public int MakeId
        {
            get { return _makeId; }
        }

        public int LineId
        {
            get { return _lineId; }
        }

        public int ModelYear
        {
            get { return _modelYear; }
        }

        public string ModelFamily
        {
            get { return _modelFamily; }
        }

        public int ModelFamilyId
        {
            get { return _modelFamilyId; }
        }

        public string Segment
        {
            get { return _segment; }
        }

        public int SegmentId
        {
            get { return _segmentId; }
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Create a new vehicle object from the given vehicle.
        /// </summary>
        /// <param name="vehicle">Vehicle to copy.</param>
        /// <returns>A new vehicle object.</returns>
        internal static Vehicle NewVehicle(Vehicle vehicle)
        {
            return new Vehicle(vehicle);
        }

        /// <summary>
        /// Create a new vehicle object from database values.
        /// </summary>
        /// <param name="record">Vehicle values retrieved from the database.</param>
        /// <returns>A new vehicle object.</returns>
        internal static Vehicle GetVehicle(IDataRecord record)
        {
            return new Vehicle(record);
        }

        /// <summary>
        /// Create a new vehicle object from database values. Protected to force creation to go through 
        /// <see cref="GetVehicle(IDataRecord)"/>.
        /// </summary>
        /// <param name="record">Vehicle values retrieved from the database.</param>
        protected Vehicle(IDataRecord record)
        {
            Fetch(record);
        }

        /// <summary>
        /// Construct this vehicle with values from another.
        /// </summary>
        /// <param name="vehicle">Vehicle object to take the values from.</param>
        protected Vehicle(Vehicle vehicle)
        {
            _make          = vehicle.Make;
            _makeId        = vehicle.MakeId;
            _lineId        = vehicle.LineId;
            _modelYear     = vehicle.ModelYear;
            _modelFamily   = vehicle.ModelFamily;
            _modelFamilyId = vehicle.ModelFamilyId;
            _segmentId     = vehicle.SegmentId;
            _segment       = vehicle.Segment;
        }

        #endregion

        #region Data Access

        /// <summary>
        /// Populate this vehicle object with values retrieved from the database.
        /// </summary>
        /// <param name="record">Vehicle values retrieved from the database.</param>
        private void Fetch(IDataRecord record)
        {
            _make          = record.GetString(record.GetOrdinal("Make"));
            _makeId        = record.GetInt32 (record.GetOrdinal("MakeId"));
            _lineId        = record.GetInt32 (record.GetOrdinal("LineId"));
            _modelYear     = record.GetInt32 (record.GetOrdinal("ModelYear"));
            _modelFamily   = record.GetString(record.GetOrdinal("ModelFamily"));
            _modelFamilyId = record.GetInt32 (record.GetOrdinal("ModelFamilyId"));
            _segment       = record.GetString(record.GetOrdinal("Segment"));
            _segmentId     = record.GetInt32 (record.GetOrdinal("SegmentId"));            
        }

        #endregion

        #region Comparison

        /// <summary>
        /// Are two vehicles equal insofar as having the same values for year, family and segment?
        /// </summary>
        /// <remarks>
        /// This method is sealed so that <see cref="VehicleConfiguration"/> cannot implement it. We will not care if 
        /// two vehicle configuration objects are equal, but we do need to know if the parent values of two vehicle 
        /// configuration objecs are equal. This is for determining the parent-child relationship of report items.
        /// </remarks>
        /// <param name="obj">Other vehicle to compare.</param>
        /// <returns>True if the two vehicle are equal.</returns>
        public override sealed bool Equals(Object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            Vehicle other = obj as Vehicle;
            if (other == null) return false;
            return Equals(other);
        }

        /// <summary>
        /// Implemented only because <see cref="Equals(object)"/> is implemented.
        /// </summary>
        /// <returns>Hash code.</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                int result = _modelFamilyId;
                result = (result*397) ^ _segmentId;
                result = (result*397) ^ _modelYear;
                return result;
            }
        }

        /// <summary>
        /// Is a given vehicle equal to this one? Two objects are equal if they share a reference or if their values
        /// for model family, segment id and model year are the same.
        /// </summary>
        /// <param name="other">Vehicle object to compare against this one.</param>
        /// <returns>True if the given vehicle is equal to this one, false otherwise.</returns>
        public bool Equals(Vehicle other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other._modelFamilyId == _modelFamilyId
                && other._segmentId == _segmentId
                && other._modelYear == _modelYear;
        }

        /// <summary>
        /// Are two vehicles equal?
        /// </summary>
        /// <param name="left">Vehicle to compare.</param>
        /// <param name="right">Vehicle to compare.</param>
        /// <returns>True if the two vehicles are equal, false otherwise.</returns>
        public static bool operator ==(Vehicle left, Vehicle right)
        {
            return Equals(left, right);
        }

        /// <summary>
        /// Are two vehicles not equal?
        /// </summary>
        /// <param name="left">Vehicle to compare.</param>
        /// <param name="right">Vehicle to compare.</param>
        /// <returns>True if the two vehicles are not equal, false otherwise.</returns>
        public static bool operator !=(Vehicle left, Vehicle right)
        {
            return !Equals(left, right);
        }

        #endregion
    }
}
