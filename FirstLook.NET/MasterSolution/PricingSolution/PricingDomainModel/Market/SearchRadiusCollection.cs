using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Market
{
    /// <summary>
    /// Is a read-only set of search radii fetched from the database (no parameters).  A search radius is a simple 
    /// (read-only) key / value pair used to define a reports criteria.
    /// </summary>
    /// <remarks>
    /// See procedure: <c>Reporting.SearchRadiusCollection#Fetch</c>
    /// </remarks>
    [Serializable]
    public class SearchRadiusCollection : ReadOnlyListBase<SearchRadiusCollection, SearchRadius>
    {
        #region Factory Methods

        /// <summary>
        /// Get the list of search radii from the database.
        /// </summary>
        /// <returns>A new collection of search radii.</returns>
        public static SearchRadiusCollection GetSearchRadiuses()
        {
            return DataPortal.Fetch<SearchRadiusCollection>(new Criteria());
        }

        /// <summary>        
        /// Data source class for handling the fetching of a search radius collection from the database.
        /// </summary>
        [Serializable]
        public class DataSource
        {
            /// <summary>
            /// Fetch a collection of search radii from the database. Hands execution off to the data portal, who will
            /// manage the connection to the db server.
            /// </summary>
            /// <returns></returns>
            public SearchRadiusCollection Select()
            {
                return GetSearchRadiuses();
            }
        }

        #endregion

        #region Data Access

        /// <summary>
        /// Criteria by which to associate this object to its corresponding database values. Because this is a 
        /// collection object that wants all search radii from the database, there is no criteria defined by
        /// this helper class to use in passing to <see cref="DataPortal_Fetch(SearchRadiusCollection.Criteria)"/>.
        /// </summary>
        [Serializable]
        protected class Criteria
        {
        }

        /// <summary>
        /// Go to the database to retrieve the search radius. This method is automatically called by 
        /// <see cref="DataPortal.Fetch(object)"/> once it has established itself on the application server.
        /// </summary>
        /// <param name="criteria">Criteria by which to find search radii. Because we want all search radii from the
        /// database, the criteria class is empty.</param>
        protected virtual void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.ReportDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand((connection.CreateCommand())))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Reporting.SearchRadiusCollection#Fetch";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        Fetch(reader);
                    }
                }
            }
        }

        /// <summary>
        /// Populate this object with values from the database. Will create a new <see cref="SearchRadius"/> object for
        /// each row returned.
        /// </summary>
        /// <param name="reader">Database rows that each have search radius values.</param>
        protected void Fetch(IDataReader reader)
        {
            IsReadOnly = false;

            while (reader.Read())
            {
                Add(SearchRadius.GetSearchRadius(reader, string.Empty));
            }

            IsReadOnly = true;
        }

        #endregion
    }
}
