using System.Collections.Generic;

namespace FirstLook.Pricing.DomainModel.Market
{
    /// <summary>
    /// Interface for uniform access of report item values.
    /// </summary>
    public interface IReportItem
    {
        int Id { get; }

        IList<IReportItem> ReportItems { get; }

        #region Vehicle

        string Make { get; }
        int MakeId { get; }
        int LineId { get; }
        int ModelYear { get; }
        string ModelFamily { get; }
        int ModelFamilyId { get; }
        string Segment { get; }
        int SegmentId { get; }

        #endregion

        #region VehicleConfiguration

        int? ModelConfigurationId { get; }
        string Series { get; }
        string BodyType { get; }
        string Transmission { get; }
        string Engine { get; }
        string FuelType { get; }
        string PassengerDoors { get; }
        string DriveTrain { get; }

        #endregion

        #region MarketInformation

        int? MarketDaysSupply { get; }
        int? SmallestMarketDaysSupply { get; }
        int? UnitsSold { get; }
        int? UnitsInStock { get; }
        int? NumberOfListings { get; }
        int? SumInternetPrice { get; }
        int? AverageInternetPrice { get; }        
        int? SumInternetMileage { get; }
        int? AverageInternetMileage { get; }        

        #endregion

        #region ListInformation

        int? Quantity { get; }
        string Notes { get; }

        #endregion
    }
}