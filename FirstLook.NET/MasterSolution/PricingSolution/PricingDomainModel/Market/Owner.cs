using System;
using System.Data;
using Csla;
using Csla.Validation;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Market
{
    /// <summary>    
    /// Is a read-only encapsulation of a single owner, defined by properties <c>name</c> and <c>handle</c>. Instances 
    /// are fetched by handle and the name is used as label text on each page identifying the current owner.
    /// </summary>
    /// <remarks>
    /// See procedure: <c>Reporting.Owner#Fetch</c>
    /// </remarks>
    [Serializable]
    public class Owner : BusinessBase<Owner>
    {
        #region Business Methods
        
        private string _name;   // Name of the owner.
        private string _handle; // GUID for this owner.

        protected override object GetIdValue()
        {
            return _handle;
        }

        public string Name
        {
            get { return _name; }            
        }

        public string Handle
        {
            get { return _handle; }
        }

        #endregion

        #region Validation

        /// <summary>
        /// Add validation rules to verify this owner object is valid.
        /// </summary>
        protected override void AddBusinessRules()
        {
            ValidationRules.AddRule(CommonRules.StringRequired, "Name");
            ValidationRules.AddRule(CommonRules.StringMaxLength, new CommonRules.MaxLengthRuleArgs("Name", 500));

            ValidationRules.AddRule(CommonRules.StringRequired, "Handle");
            ValidationRules.AddRule(CommonRules.StringMaxLength, new CommonRules.MaxLengthRuleArgs("Handle", 36));
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Method for fetching an owner from the database. Will hand off to the data portal for execution.
        /// </summary>
        /// <param name="ownerHandle">GUID by which to find our owner.</param>
        /// <returns>A new <c>Owner</c> object.</returns>
        public static Owner GetOwner(string ownerHandle)
        {
            return DataPortal.Fetch<Owner>(new Criteria(ownerHandle)); 
        }

        /// <summary>
        /// Data source that provides a method for fetching an owner from the database.
        /// </summary>
        [Serializable]
        public class DataSource
        {
            /// <summary>
            /// Method for fetching an owner from the database. Will hand off to the data portal for execution.
            /// </summary>
            /// <param name="handle">GUID by which to find our owner.</param>
            /// <returns>A new <c>Owner</c> object.</returns>
            public Owner Select(string handle)
            {
                return GetOwner(handle);
            }
        }

        #endregion

        #region Data Access

        /// <summary>
        /// Criteria by which to associate this object to its corresponding database values. Specifically, this is the
        /// owner GUID by which to find our owner in the db.
        /// </summary>
        [Serializable]
        protected class Criteria
        {
            private readonly string _handle;

            public Criteria(string handle)
            {
                _handle = handle;
            }

            public string Handle
            {
                get { return _handle; }
            }
        }

        /// <summary>
        /// Go to the database to retrieve the values of this owner object. This method is automatically called by
        /// <see cref="DataPortal.Fetch(object)"/> once it has established itself on the application server.
        /// </summary>
        /// <param name="criteria">Criteria by which to find our owner (i.e. the owner handle).</param>
        protected virtual void DataPortal_Fetch(Criteria criteria)
        {            
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.ReportDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Reporting.Owner#Fetch";
                    command.AddParameterWithValue("OwnerHandle", DbType.String, false, criteria.Handle);                    

                    using (IDataReader reader = command.ExecuteReader())
                    {                        
                        if (reader.Read())
                        {
                            Fetch(reader);                           
                        }                        
                    }
                }
            }
        }

        /// <summary>
        /// Populate this owner object with values retrieved from the database.
        /// </summary>
        /// <param name="record">Owner values retrieved from the database.</param>
        protected void Fetch(IDataRecord record)
        {            
            _name   = record.GetString(record.GetOrdinal("Name"));
            _handle = record.GetString(record.GetOrdinal("Handle"));
        }

        #endregion        
    }
}
