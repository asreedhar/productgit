using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using FirstLook.Common.Core.Utilities;

namespace FirstLook.Pricing.DomainModel.Market
{
    /// <summary>
    /// PDF builder for reports.
    /// </summary>
    public class PrintReportBuilder
    {
        private readonly Owner _owner;
        private readonly ReportItemCollection _reportCollection;
        private readonly ReportParameters _reportParameters;
        private readonly ReportFilterArguments _reportFilters;
        private readonly string[] _stylesheets;

        private readonly List<ReportItem>       _topAllSegments = new List<ReportItem>();
        private readonly List<List<ReportItem>> _topEachSegment = new List<List<ReportItem>>();
        private readonly List<ReportItem>       _filteredReportItems = new List<ReportItem>();

        private int _pageSize = 25;
        private int _pageIndex;

        public int PageSize
        {
            get { return _pageSize; }
            set
            {
                if (value != _pageSize)
                {
                    _pageSize = value;
                }
            }
        }

        public int PageIndex
        {
            get { return _pageIndex;  }
            set
            {
                if (value != _pageIndex)
                {
                    _pageIndex = value;
                }
            }
        }

        private string _sortExpression = string.Empty;
        private SortDirection _sortDirection;

        public string SortExpression
        {
            get { return _sortExpression; }
            set { _sortExpression = value; }
        }

        public SortDirection SortDirection
        {
            get { return _sortDirection; }
            set { _sortDirection = value; }
        }

        private bool _showUnitsSold = true;

        public bool ShowUnitsSold
        {
            get { return _showUnitsSold; }
            set { _showUnitsSold = value; }
        }

        private List<int> _expandedReportItems;

        public List<int> ExpandedReportItems
        {
            get
            {
                if (_expandedReportItems == null)
                {
                    _expandedReportItems = new List<int>();
                }
                return _expandedReportItems;
            }
            set
            {
                _expandedReportItems = value;
            }
        }

        /// <summary>
        /// Initialize with the report object from which we will generate a pdf. Also, calculate the top vehicles 
        /// overall and in each segment by market days supply.
        /// </summary>
        /// <param name="owner">Dealer for whom the report was generated.</param>
        /// <param name="reportCollection">Report around which to build the pdf.</param>
        /// <param name="reportParameters">Search radius and basis period</param>
        /// <param name="reportFilters">Report filters.</param>
        /// <param name="stylesheets">Stylesheets for the report.</param>
        public PrintReportBuilder(
            Owner owner,
            ReportItemCollection reportCollection,
            ReportParameters reportParameters,
            ReportFilterArguments reportFilters,
            string[] stylesheets)
        {
            _owner = owner;
            _reportCollection = reportCollection;
            _reportParameters = reportParameters;
            _reportFilters = reportFilters;
            _stylesheets = stylesheets;
        }

        /// <summary>
        /// Initialize with the report object from which we will generate a pdf. Also, calculate the top vehicles 
        /// overall and in each segment by market days supply.
        /// </summary>
        /// <param name="owner">Dealer for who the report was generated.</param>
        /// <param name="reportCollection">Report around which to build the pdf.</param>
        /// <param name="reportParameters">Search radius and basis period.</param>
        /// <param name="reportFilters">Report filters.</param>
        public PrintReportBuilder(
            Owner owner,
            ReportItemCollection reportCollection,
            ReportParameters reportParameters,
            ReportFilterArguments reportFilters) : this(owner, reportCollection, reportParameters, reportFilters, new string[0])
        {
        }

        /// <summary>
        /// Calculate the top vehicles (by market days supply) overall and for each segment.
        /// </summary>
        private void CalculateTopInSegments()
        {
            ReportItemCollection reportItems = _reportCollection;
            if (reportItems.Count == 0)
            {
                return;
            }

            // PART THE FIRST: Figure out tops across all segments.

            // Collect all children of all parents.
            List<ReportItem> allChildren = new List<ReportItem>();
            foreach (ReportItem parent in reportItems)
            {
                foreach (ReportItem child in parent.ReportItems)
                {
                    if (child.MarketInformation.MarketDaysSupply.HasValue && child.ReportItems.Count == 0)
                    {
                        allChildren.Add(child);
                    }
                }
            }

            // Sort the children on market days supply.
            ReportItemCollection allChildrenItems = new ReportItemCollection(allChildren, _reportCollection.BasisPeriod, ReportItemCollection.ReportCollectionStructure.OnlyChild);
            allChildrenItems.Sort(Market.SortExpression.MarketDaysSupply, true, SortStructure.Self);

            // Collect the first ResultsPerPage report items (if there are that many).
            int count = 0;
            foreach (ReportItem reportItem in allChildrenItems)
            {
                _topAllSegments.Add(reportItem);
                if (++count >= PageSize)
                {
                    break;
                }
            }

            // PART THE SECOND: Figure out tops per segment.

            // Sort the children on segment now.
            allChildrenItems.Sort(Market.SortExpression.Segment, true, SortStructure.Self);
            int currentSegmentId = allChildrenItems[0].Vehicle.SegmentId;

            for (int j = 0; j < allChildrenItems.Count; )
            {
                // Collect all vehicles for the current segment.
                List<ReportItem> temp = new List<ReportItem>();
                while (j < allChildrenItems.Count && allChildrenItems[j].Vehicle.SegmentId == currentSegmentId)
                {
                    temp.Add(allChildrenItems[j++]);
                }
                if (j < allChildrenItems.Count)
                {
                    currentSegmentId = allChildrenItems[j].Vehicle.SegmentId;
                }

                // Sort the current segment vehicles by market days supply.
                ReportItemCollection collection = new ReportItemCollection(temp, _reportCollection.BasisPeriod,
                                                                           ReportItemCollection.
                                                                               ReportCollectionStructure.OnlyChild);
                collection.Sort(Market.SortExpression.MarketDaysSupply, true, SortStructure.Self);

                // Gather the first ResultsPerPage (if there are that many).
                List<ReportItem> topInSegment = new List<ReportItem>();
                int highBound = (collection.Count > PageSize ? PageSize : collection.Count);
                for (int i = 0; i < highBound; i++)
                {
                    topInSegment.Add(collection[i]);
                }
                _topEachSegment.Add(topInSegment);
            }
        }

        private void CalculateFilteredReport()
        {
            IReportItemCollection collection = _reportCollection.Filter(_reportFilters);

            if (string.IsNullOrEmpty(_sortExpression))
            {
                _sortExpression = "SmallestMarketDaysSupply";
            }

            if (Enum.IsDefined(typeof(SortExpression), _sortExpression))
            {
                SortExpression sort =
                    (SortExpression)
                    Enum.Parse(typeof(SortExpression), _sortExpression);

                collection.Sort(sort, SortDirection == SortDirection.Ascending);
            }

            int lower = PageIndex*PageSize;
            int upper = (PageIndex*PageSize) + PageSize;

            if (upper > collection.Count)
            {
                upper = collection.Count;
            }

            for (int i = lower; i < upper; i++)
            {
                _filteredReportItems.Add((ReportItem)collection[i]);
            }

        }


        /// <summary>
        /// Create a pdf from a report. Will first create the html representation of the report, and then create the 
        /// pdf byte stream from this html using Prince XML.
        /// </summary>
        /// <returns>A pdf as a byte stream.</returns>
        private StringBuilder BuildReportHead(StringBuilder html)
        {            
            // Head.
            html.Append(@"<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Strict//EN"" ");
            html.Append(@"""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"">");
            html.Append(@"<html xmlns=""http://www.w3.org/1999/xhtml"">");
            html.Append(@"<head>");
            if (ConfigurationManager.AppSettings["prince_xml_assests_host"] != null)
            {
                html.Append(@"<base href=""");
                html.Append(ConfigurationManager.AppSettings["prince_xml_assests_host"]);
                html.Append(@""" />");
            }
            foreach (string s in _stylesheets)
            {
                html.Append(@"<link rel=""stylesheet"" type=""text/css"" href=""");
                html.Append(s);
                html.Append(@""" />");
            }
            html.Append(@"<meta http-equiv=""Content-type"" content=""text/html; charset=utf-8"" />");
            html.Append(@"<title>Market Stocking Velocity Report - Report</title>");
            html.Append(@"</head>");

            // Body.
            html.Append(@"<body id=""mvsg"">");
            html.Append(@"<div id=""page_wrapper"">");

            // Page title + dealer name.
            html.Append(@"<h1 id=""branding"">Market Velocity Stocking Guide</h1>");
            html.Append(@"<div id=""footer"">");
            html.Append(@"<span class=""date"">");
            html.Append(string.Format("{0:M/d/yyyy}", DateTime.Today));
            html.Append(@"</span>");
            html.Append(@"<img src=""pricing/Public/Images/PrintImages/Logo.jpg"" />");
            html.Append(@"</div>");
            html.Append(@"<h2 class=""dealer_name"">");
            html.Append(_owner.Name);
            html.Append(@"</h2>");

            // Report data table header.
            html.Append(@"<div class=""market_report_table"">");

            return html; 
        }

        private static StringBuilder BuildReportEnd(StringBuilder html)
        {
            return html.Append(@"</div></div></body></html>");
        }

        public byte[] BuildTopVehiclesReport()
        {
            CalculateTopInSegments();

            StringBuilder html = new StringBuilder();

            html = BuildReportHead(html);

            html.Append(@"<h3>Top ");
            html.Append(PageSize);
            html.Append(@" performing vehicles</h3>");
            html = RenderReportParameters(html);
            html = RenderTable(html, _topAllSegments);

            html = BuildReportEnd(html);

            return PdfHelper.GeneratePdf(html.ToString(), _stylesheets);
        }

        public byte[] BuildSegmentsReport()
        {
            CalculateTopInSegments();

            StringBuilder html = new StringBuilder();

            BuildReportHead(html);

            foreach (List<ReportItem> segment in _topEachSegment)
            {
                html.Append(@"<h3>");
                html.Append(segment[0].Vehicle.Segment);
                if (segment[0].Vehicle.Segment.EndsWith(@"s"))
                {
                    html.Append(@"es");
                }
                else
                {
                    html.Append(@"s");
                }
                html.Append(@"</h3>");
                html = RenderReportParameters(html);
                RenderTable(html, segment);
            }

            BuildReportEnd(html);

            return PdfHelper.GeneratePdf(html.ToString(), _stylesheets);
        }

        public byte[] BuildFilteredReport()
        {
            CalculateFilteredReport();

            StringBuilder html = new StringBuilder();

            BuildReportHead(html);

            html.Append(@"<h3>Filtered Report</h3>");
            html = RenderFilterSummary(html, _filteredReportItems);
            html = RenderReportParameters(html);
            RenderTable(html, _filteredReportItems);

            BuildReportEnd(html);

            return PdfHelper.GeneratePdf(html.ToString(), _stylesheets);
        }

        private StringBuilder RenderFilterSummary(StringBuilder html, IList<ReportItem> reportItems)
        {
            html.Append(@"<p class=""vehicle_info_summary"">");

            if (_reportFilters.ModelYears == Range.Unlimited)
            {
                html.Append(@"All Years, ");
            }
            else
            {
                html.Append(@"Years ");
                html.Append(_reportFilters.ModelYears.Lower);
                html.Append(@" to ");
                html.Append(_reportFilters.ModelYears.Upper);
                html.Append(@", ");
            }

            if (!_reportFilters.MakeId.HasValue)
            {
                html.Append(@"All Makes, ");
            }
            else
            {
                html.Append(@"Make: ");
                html.Append(reportItems[0].Vehicle.Make);
                html.Append(@" ");
            }

            if (!_reportFilters.ModelFamilyId.HasValue)
            {
                html.Append(@"All Models ");
            }
            else
            {
                html.Append(@"Model: ");
                html.Append(reportItems[0].Vehicle.ModelFamily);
            }

            html.Append(@"</p>");

            html.Append(@"<p class=""price_summary"">");

            if (_reportFilters.Prices != Range.Unlimited)
            {
                html.Append(@"Avg Price Between ");
                html.Append(@"<br />");
                html.AppendFormat("${0:#,#0}",_reportFilters.Prices.Lower);
                html.Append(@" and ");
                html.AppendFormat("${0:#,#0}",_reportFilters.Prices.Upper);
            }

            html.Append(@"<p class=""mileage_summary"">");

            if (_reportFilters.Mileages != Range.Unlimited)
            {
                html.Append(@"Avg Mileage Between ");
                html.Append(@"<br />");
                html.AppendFormat("{0:#,#0}", _reportFilters.Prices.Lower);
                html.Append(@" and ");
                html.AppendFormat("{0:#,#0}", _reportFilters.Prices.Upper);
            }

            html.Append(@"</p>");

            return html;
        }

        private StringBuilder RenderReportParameters(StringBuilder html)
        {
            html.Append(@"<p class=""report_days_summary"">");
            html.Append(@"Sales within last ");
            html.Append(_reportParameters.BasisPeriod.Name);
            html.Append(@" days");
            html.Append(@"<br />");
            html.Append(@"Within radius of");
            html.Append(_reportParameters.SearchRadius.Name);
            html.Append(@" days");
            html.Append(@"</p>");

            return html;
        }

        private StringBuilder RenderTable(StringBuilder html, IEnumerable<ReportItem> reportItems)
        {
            html.Append(@"<table class=""grid market_report_table"">");
            html.Append(@"<thead><tr class=""headings"">");
            html.Append(@"<th class=""mds"">Market Days Suppy</th>");
            html.Append(@"<th class=""vehicle_info"">Vehicle Information</th>");
            if (_showUnitsSold)
            {
                html.Append(@"<th class=""units_sold"">Units Sold</th>");
            }
            html.Append(@"<th class=""number_of_listings"">Number Of Listings</th>");
            html.Append(@"<th class=""avg_market_price"">Avg Internet Price</th>");
            html.Append(@"<th class=""avg_mileage"">Avg Mileage</th>");
            html.Append(@"<th class=""units_in_stock"">Units in Stock</th>");
            html.Append(@"</tr>");
           
            html.Append(@"</thead>");
            html.Append(@"<tbody>");

            // Parent report items - these have no configuration details.
            bool isOdd = true;
            foreach (ReportItem reportItem in reportItems)
            {
                string className = isOdd ? "odd" : string.Empty;

                RenderTableRow(html, className, reportItem);

                isOdd = !isOdd;
            }

            html.Append(@"</tbody></table>");

            return html;
        }

        private void RenderTableRow(StringBuilder html, string className, ReportItem reportItem)
        {
            RenderTableRow(html, className, reportItem, 0);
        }

        private void RenderTableRow(StringBuilder html, string className, ReportItem reportItem, int depth)
        {
            bool isTopLevel = depth == 0;

            bool hasChildren = reportItem.ReportItems.Count > 0;

            if (!hasChildren || isTopLevel)
            {
                html.Append(@"<tr");
                if (depth > 0)
                {
                    className += string.Format(" child_level_{0}", depth);
                }
                if (!string.IsNullOrEmpty(className))
                {
                    html.Append(string.Format(" class='{0}'", className));
                }
                html.Append(@">");

                // Market days supply.
                html.Append(@"<td class=""mds"">");
                if (reportItem.MarketInformation.MarketDaysSupply.HasValue)
                {
                    if (reportItem.MarketInformation.MarketDaysSupply.Value < 180)
                    {
                        html.Append(string.Format("{0:#,0;#,0;--}", reportItem.MarketInformation.MarketDaysSupply));
                    }
                    else
                    {
                        html.Append("180+");
                    }
                }
                else
                {
                    html.Append("--");
                }
                html.Append(@"</td>");

                // Year.
                html.Append(@"<td class=""vehicle_info"">");

                if (isTopLevel)
                {
                    html.Append(@"<span class=""year"">");
                    html.Append(reportItem.Vehicle.ModelYear);
                    html.Append(@"</span> ");

                    // Make.
                    html.Append(@"<span class=""make"">");
                    html.Append(reportItem.Vehicle.Make);
                    html.Append(@"</span> ");

                    // Model.    
                    html.Append(@"<span class=""model"">");
                    html.Append(reportItem.Vehicle.ModelFamily);
                    html.Append(@"</span> ");

                    // Segment.
                    html.Append(@"<span class=""segment"">");
                    html.Append(reportItem.Vehicle.Segment);
                    html.Append(@"</span>");
                }

                VehicleConfiguration vc = reportItem.Vehicle as VehicleConfiguration;
                if (vc != null)
                {
                    html.Append(@"<ul>");

                    // Series.
                    if (!string.IsNullOrEmpty(vc.Series))
                    {
                        html.Append(@"<li class=""series"">");
                        html.Append(vc.Series);
                        html.Append(@"</li>");
                    }

                    // Drive train.
                    if (!string.IsNullOrEmpty(vc.DriveTrain))
                    {
                        html.Append(@"<li class=""drive_train"">");
                        html.Append(vc.DriveTrain);
                        html.Append(@"</li>");
                    }

                    // Engine.
                    if (!string.IsNullOrEmpty(vc.Engine))
                    {
                        html.Append(@"<li class=""engine"">");
                        html.Append(vc.Engine);
                        html.Append(@"</li>");
                    }

                    // Fuel type.
                    if (!string.IsNullOrEmpty(vc.FuelType))
                    {
                        html.Append(@"<li class=""fuel_type"">");
                        html.Append(vc.FuelType);
                        html.Append(@"</li>");
                    }

                    // Transmission.
                    if (!string.IsNullOrEmpty(vc.Transmission))
                    {
                        html.Append(@"<li class=""transmission"">");
                        html.Append(vc.Transmission);
                        html.Append(@"</li>");
                    }

                    html.Append(@"</ul>");
                }
                html.Append(@"</td>");

                if (_showUnitsSold)
                {
                    // Units sold.
                    html.Append(@"<td class=""units_sold"">");
                    html.Append(reportItem.MarketInformation.UnitsSold);
                    html.Append(@"</td>");
                }

                // Number of listings.
                html.Append(@"<td class=""number_of_listings"">");
                html.Append(reportItem.MarketInformation.NumberOfListings);
                html.Append(@"</td>");

                // Average market price.
                html.Append(@"<td class=""avg_market_price"">$");
                html.AppendFormat(@"{0:###,###,###}", reportItem.MarketInformation.AverageInternetPrice);
                html.Append(@"</td>");

                // Average market mileage.
                html.Append(@"<td class=""avg_mileage"">");
                html.AppendFormat(@"{0:###,###,###}", reportItem.MarketInformation.AverageInternetMileage);
                html.Append(@"</td>");

                // Units in stock.
                html.Append(@"<td class=""units_in_stock"">");
                html.Append(reportItem.MarketInformation.UnitsInStock);
                html.Append(@"</td>");

                html.Append(@"</tr>"); 
            }

            bool showChildern = isTopLevel || _expandedReportItems.Contains(((IReportItem)reportItem).Id);

            if (showChildern)
            {
                foreach (ReportItem childItem in reportItem.ReportItems)
                {
                    RenderTableRow(html, className, childItem, depth + 1);
                } 
            }
        }
    }
}