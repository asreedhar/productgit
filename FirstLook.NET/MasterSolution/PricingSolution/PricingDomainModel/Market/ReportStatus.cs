using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Market
{
    /// <summary>
    /// A command object that takes an owner handle and report handle to see if the report for those parameters has
    /// been generated.
    /// </summary>
    /// <remarks>
    /// See procedure: <c>Reporting.Report#Status</c>
    /// </remarks>
    [Serializable]
    public class ReportStatus : CommandBase
    {
        private readonly string _ownerHandle;  // Owner handle for identifying the report.
        private readonly string _reportHandle; // Report handle for identifying the report.
        private bool            _ready;        // Has the report finished generation?

        /// <summary>
        /// Create a new report status object with the values necessary to query the database for status info.
        /// </summary>
        /// <param name="ownerHandle">Owner GUID.</param>
        /// <param name="reportHandle">Report GUID.</param>
        private ReportStatus(string ownerHandle, string reportHandle)
        {
            _ownerHandle = ownerHandle;
            _reportHandle = reportHandle;
        }

        /// <summary>
        /// Execute the request for the report status. Will hand execution off to the data portal, and return a bool
        /// with whether or not the report is ready.
        /// </summary>
        /// <param name="ownerHandle">Owner GUID.</param>
        /// <param name="reportHandle">Report GUID.</param>
        /// <returns>True if the report is ready to load, false otherwise.</returns>
        public static bool Execute(string ownerHandle, string reportHandle)
        {
            ReportStatus command = new ReportStatus(ownerHandle, reportHandle);
            DataPortal.Execute(command);
            return command._ready;
        }                

        /// <summary>
        /// Execute this request for report status. This method will be called by 
        /// <see cref="DataPortal.Execute(CommandBase)"/>, and will set the report status on completion of execution.
        /// </summary>
        protected override void DataPortal_Execute()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandText = "Reporting.Report#Status";
                    command.CommandType = CommandType.StoredProcedure;
                    command.AddParameterWithValue("OwnerHandle", DbType.String, false, _ownerHandle);
                    command.AddParameterWithValue("ReportHandle", DbType.String, false, _reportHandle);                    

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            _ready = reader.GetBoolean(reader.GetOrdinal("Ready"));
                        }                        
                    }
                }
            }
        }
    }
}
