namespace FirstLook.Pricing.DomainModel.Market
{
    /// <summary>
    /// Enum for identifying a sorting criteria.
    /// </summary>
    public enum SortExpression
    {
        MarketDaysSupply,
        SmallestMarketDaysSupply,
        ModelYear,
        Make,
        ModelFamily,
        UnitsSold,
        NumberOfListings,
        AverageInternetPrice,
        AverageInternetMileage,
        UnitsInStock,
        Quantity,
        Segment
    }
}