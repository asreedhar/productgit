using System.Collections.Generic;

namespace FirstLook.Pricing.DomainModel.Market
{
    /// <summary>
    /// Interface for the filtering and sorting of a collection of report items.
    /// </summary>
    public interface IReportItemCollection : IList<IReportItem>
    {
        /// <summary>
        /// Filter a report based on the provided arguments.
        /// </summary>
        /// <param name="filterArguments">Filters to apply to the report.</param>
        /// <returns>Collection of report items.</returns>
        IReportItemCollection Filter(ReportFilterArguments filterArguments);

        /// <summary>
        /// Sort a report based on the provided arguments.
        /// </summary>
        /// <param name="expression">Expression to sort by.</param>
        /// <param name="ascending">Should the expression be used ascending or descending?</param>
        void Sort(SortExpression expression, bool ascending);

        IReportItemCollection Page(int pageIndex, int pageSize);
    }
}