using System;
using System.Collections.Generic;

namespace FirstLook.Pricing.DomainModel.Market
{
    /// <summary>
    /// Arguments to be used in filtering a report.
    /// </summary>
    public class ReportFilterArguments
    {
        private Range _modelYears = Range.Unlimited;   // Model year filter.
        private int?  _makeId;                         // Make filter.
        private int?  _modelFamilyId;                  // Model family filter.
        private Range _prices = Range.Unlimited;       // Price filter.
        private Range _mileages = Range.Unlimited;     // Mileage filter.
        private Range _unitsInStock = Range.Unlimited; // Units in stock filter.
        private readonly IList<int> _segments = new List<int>(); // Segment filter.

        public Range ModelYears
        {
            get { return _modelYears; }
            set
            {
                AssertNotNull(value);

                _modelYears = value;
            }
        }

        public int? MakeId
        {
            get { return _makeId; }
            set { _makeId = value; }
        }

        public int? ModelFamilyId
        {
            get { return _modelFamilyId; }
            set { _modelFamilyId = value; }
        }

        public IList<int> Segments
        {
            get { return _segments; }
        }

        public Range Prices
        {
            get { return _prices; }
            set
            {
                AssertNotNull(value);

                _prices = value;
            }
        }

        public Range Mileages
        {
            get { return _mileages; }
            set
            {
                AssertNotNull(value);

                _mileages = value;
            }
        }

        public Range UnitsInStock
        {
            get { return _unitsInStock; }
            set
            {
                AssertNotNull(value);

                _unitsInStock = value;
            }
        }

        /// <summary>
        /// Ensure that a given range is not null.
        /// </summary>
        /// <param name="value">Range value to check for nullness.</param>
        private static void AssertNotNull(Range value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }
        }

        /// <summary>
        /// Match the given report item against the current range filters.
        /// </summary>
        /// <param name="item">Report item to check against these filters.</param>
        /// <returns>True if the given item matches all filters, false otherwise.</returns>
        public bool Match(ReportItem item)
        {
            // Model.
            if (!_modelYears.Contains(item.Vehicle.ModelYear))
            {
                return false;
            }

            // Mileage.
            int? mileage = item.MarketInformation.AverageInternetMileage;

            if (!(_mileages == Range.Unlimited || (mileage.HasValue && _mileages.Contains(mileage.Value))))
            {
                return false;
            }

            // Price.
            int? price = item.MarketInformation.AverageInternetPrice;

            if (!(_prices == Range.Unlimited || (price.HasValue && _prices.Contains(price.Value))))
            {
                return false;
            }

            // Units in stock.
            int unitsInStock = item.MarketInformation.UnitsInStock.GetValueOrDefault();

            if (!(_unitsInStock == Range.Unlimited || _unitsInStock.Contains(unitsInStock)))
            {
                return false;
            }

            // Make.
            if (MakeId.HasValue && item.Vehicle.MakeId != MakeId.Value)
            {
                return false;
            }
            
            // Model family.
            if (ModelFamilyId.HasValue && item.Vehicle.ModelFamilyId != ModelFamilyId)
            {
                return false;
            }

            // Segments.
            if (_segments.Count > 0 && !_segments.Contains(item.Vehicle.SegmentId))
            {
                return false;
            }

            return true;
        }
    }
}