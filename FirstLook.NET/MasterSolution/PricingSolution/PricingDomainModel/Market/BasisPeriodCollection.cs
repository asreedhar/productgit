using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Market
{
    /// <summary>
    /// Is a read-only collection of basis periods fetched from the database.  A basis period is a simple read-only 
    /// key / value pair used to define a report's criteria.
    /// </summary>
    /// <remarks>
    /// See procedure: <c>Reporting.BasisPeriodCollection#Fetch</c>
    /// </remarks>
    [Serializable]
    public class BasisPeriodCollection : ReadOnlyListBase<BasisPeriodCollection, BasisPeriod>
    {
        #region Factory Methods

        /// <summary>
        /// Fetch a collection of basis periods from the database. Hands execution off to the data portal, who will
        /// manage the connection to the db server.
        /// </summary>
        /// <returns>A collection of basis period objects.</returns>
        public static BasisPeriodCollection GetBasisPeriods()
        {
            return DataPortal.Fetch<BasisPeriodCollection>(new Criteria());
        }

        /// <summary>        
        /// Data source class for handling the fetching of a basis period collection from the database.
        /// </summary>
        [Serializable]
        public class DataSource
        {
            /// <summary>
            /// Fetch a collection of basis periods from the database. Hands execution off to the data portal, who will
            /// manage the connection to the db server.
            /// </summary>
            /// <returns>A collection of basis period objects.</returns>
            public BasisPeriodCollection Select()
            {
                return GetBasisPeriods();
            }
        }

        #endregion

        #region Data Access

        /// <summary>
        /// Criteria by which to associate this object to its corresponding database values. Because this is a 
        /// collection object that wants all basis periods from the database, there is no criteria to limit our query
        /// by.
        /// </summary>
        [Serializable]
        protected class Criteria
        {            
        }

        /// <summary>
        /// Go to the database to retrieve the basis period. This method is automatically called by 
        /// <see cref="DataPortal.Fetch(object)"/> once it has established itself on the application server.
        /// </summary>
        /// <param name="criteria">Criteria by which to find basis periods. Because we want all basis periods from the
        /// database, the criteria class is empty.</param>
        protected virtual void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.ReportDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand((connection.CreateCommand())))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Reporting.BasisPeriodCollection#Fetch";                    

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        Fetch(reader);                        
                    }
                }
            }
        }

        /// <summary>
        /// Populate this basis period collection object with values from the database. Will create a new 
        /// <see cref="BasisPeriod"/> object for each row returned.
        /// </summary>
        /// <param name="reader">Database rows that each have basis period values.</param>
        protected void Fetch(IDataReader reader)
        {
            IsReadOnly = false;

            while (reader.Read())
            {
                Add(BasisPeriod.GetBasisPeriod(reader, string.Empty));
            }

            IsReadOnly = true;
        }

        #endregion        
    }
}
