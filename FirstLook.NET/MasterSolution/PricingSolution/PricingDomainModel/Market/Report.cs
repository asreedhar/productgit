using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Market
{
    /// <summary>
    /// <para>
    /// Is a read-only class that contains the collection of data points for a report. It is identified by a handle 
    /// property and exposes a read-only collection of <see cref="ReportItem"/>s. Instances are fetched from the
    /// database using an owner-handle and report-handle. The database returns rows with columns that are the union of
    /// vehicle configuration, market information and list information. The report class relies on the 
    /// <see cref="ReportItemCollection"/> class to generate vehicle granularity parent rows from the vehicle 
    /// configuration data points (hence the existence of the sum properties on market information.
    /// </para>
    /// <para>
    /// Report instances will be cached (sliding window) as they are read-only objects. Due to the complexity of the 
    /// filtering of the data expressed in any given report object the filter operation is to happen in the middle tier
    /// and leverage the caching (otherwise filtering in the middle tier would suffer from extremely poor performance). 
    /// The cache duration will have to be carefully tuned due to the large size of the object.
    /// </para>
    /// </summary>
    /// <remarks>
    /// See procedure: <c>Reporting.Report#Fetch</c>
    /// </remarks>
    [Serializable]
    public class Report : ReadOnlyBase<Report>
    {
        #region Business Methods

        private string _reportHandle; // Report handle.
        private string _ownerHandle;  // Owner handle.
        private ReportItemCollection _reportItems; // Collection of report items.

        public string Handle
        {
            get { return _reportHandle; }
        }

        public string OwnerHandle
        {
            get { return _ownerHandle; }
        }

        public ReportItemCollection ReportItems
        {
            get { return _reportItems; }
        }

        protected override object GetIdValue()
        {
            return _reportHandle;
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Get the report defined by the given owner and report handle.
        /// </summary>
        /// <param name="ownerHandle">Identifier of the owner whose report we should retrieve.</param>
        /// <param name="reportHandle">Identifier of the report to retrieve.</param>
        /// <returns>A report object.</returns>
        public static Report GetReport(string ownerHandle, string reportHandle)
        {
            return DataPortal.Fetch<Report>(new Criteria(ownerHandle, reportHandle));
        }

        /// <summary>
        /// Data source for retrieving a report from the database.
        /// </summary>
        public class DataSource
        {
            public ReportItemCollection Select(string ownerHandle, string reportHandle)
            {
                Report report = GetReport(ownerHandle, reportHandle);

                return report.ReportItems;
            }
        }

        #endregion

        #region Data Access

        /// <summary>
        /// Criteria by which to associate this object to its corresponding database values. Specifically, this is the
        /// owner handle and report handle used to define a report.
        /// </summary>
        protected class Criteria
        {
            private readonly string _ownerHandle; // Owner GUID.
            private readonly string _reportHandle; // Report GUID.

            public Criteria(string ownerHandle, string reportHandle)
            {
                _ownerHandle = ownerHandle;
                _reportHandle = reportHandle;
            }

            public string OwnerHandle
            {
                get { return _ownerHandle; }
            }

            public string ReportHandle
            {
                get { return _reportHandle; }
            }
        }

        /// <summary>
        /// Go to the database to retrieve the values of this report object. This method is automatically called by 
        /// <see cref="DataPortal.Fetch(object)"/> once it has established itself on the application server.
        /// </summary>
        /// <param name="criteria">Criteria by which to find our report (i.e. the owner and report handles).</param>
        protected virtual void DataPortal_Fetch(Criteria criteria)
        {
            _reportHandle = criteria.ReportHandle;
            _ownerHandle = criteria.OwnerHandle;            

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.ReportDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand((connection.CreateCommand())))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Reporting.Report#Fetch";
                    command.AddParameterWithValue("OwnerHandle", DbType.String, false, criteria.OwnerHandle);
                    command.AddParameterWithValue("ReportHandle", DbType.String, false, criteria.ReportHandle);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        // Populate the report.
                        Fetch(reader);

                        // Due to how everything is designed, we need to get the quantities of the report items that
                        // are parents to other report items seperately. Do that here.
                        if (reader.NextResult())
                        {
                            _reportItems.UpdateReportItemQuantities(reader);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Populate this report object with values retrieved from the database.
        /// </summary>
        /// <param name="reader">Reader for database values.</param>
        protected void Fetch(IDataReader reader)
        {
            _reportItems = ReportItemCollection.GetReportItemCollection(reader);
        }

        #endregion
    }
}