using System;
using System.Data;

namespace FirstLook.Pricing.DomainModel.Market
{
    /// <summary>
    /// Specifies the quantity and user-entered notes for a vehicle.
    /// </summary>    
    [Serializable]
    public class ListInformation
    {
        #region Business Methods
        
        private int?   _quantity; // Quantity of the vehicle on the purchase list.
        private string _notes;    // User-entered notes for the vehicle.        

        public int? Quantity
        {
            get { return _quantity; }
            set
            {
                if (value > 99)
                {
                    _quantity = 99;
                }
                else if (value < 0)
                {
                    _quantity = 0;
                }
                else
                {
                    _quantity = value;   
                }                
            }
        }

        public string Notes
        {
            get { return _notes; }
            set
            {
                if (value.Length > 2000)
                {
                    _notes = value.Substring(0, 2000);
                }
                else
                {
                    _notes = value;   
                }                
            }
        }

        #endregion

        #region Factory Methods
        
        /// <summary>
        /// Create a new, empty list information object.
        /// </summary>
        /// <returns>A list information object.</returns>
        internal static ListInformation NewListInformation()
        {
            return new ListInformation();
        }

        /// <summary>
        /// Create a new list information object with values from the database.
        /// </summary>
        /// <param name="record">List information values retrieved from that database.</param>
        /// <returns>A new list information object.</returns>
        internal static ListInformation GetListInformation(IDataRecord record)
        {
            return new ListInformation(record);
        }

        /// <summary>
        /// Create a new list information object. Private to force creation to go through 
        /// <see cref="GetListInformation(IDataRecord)"/>.
        /// </summary>
        /// <param name="record">List information values retrieved from that database.</param>
        private ListInformation(IDataRecord record)
        {
            Fetch(record);
        }

        /// <summary>
        /// Empty constructor for creating a list information object with no values. Private to force construction to
        /// go through factory methods.
        /// </summary>
        private ListInformation()
        {            
        }

        #endregion

        #region Data Access

        /// <summary>
        /// Populate this list information object with values retrieved from the database.
        /// </summary>
        /// <param name="record">List information values retrieved from the database.</param>
        private void Fetch(IDataRecord record)
        {
            if (!record.IsDBNull(record.GetOrdinal("Quantity")))
            {
                _quantity = record.GetInt32(record.GetOrdinal("Quantity")); 
            }
            if (!record.IsDBNull(record.GetOrdinal("Notes")))
            {
                _notes = record.GetString(record.GetOrdinal("Notes"));
            }
        }

        #endregion
    }
}
