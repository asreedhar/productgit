using System;
using System.Data;

using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.AdvertisementGenerator
{
    [Serializable]
    public class AdvertisementEquipmentProvider : ReadOnlyBase<AdvertisementEquipmentProvider>
    {
        #region Business Methods

        private int id;
        private string name;

        public int ID
        {
            get
            {
                CanReadProperty("ID", true);
                return id;
            }
        }
        public string Name
        {
            get
            {
                CanReadProperty("Name", true);
                return name;
            }
        }

        protected override object GetIdValue()
        {
            return id;
        }

        #endregion Business Methods

        #region Factory Methods

        private AdvertisementEquipmentProvider()
        {
            //force factory methods
        }

        internal static AdvertisementEquipmentProvider GetAdvertisementEquipmentProvider(IDataRecord record)
        {
            return new AdvertisementEquipmentProvider(record);
        }

        private AdvertisementEquipmentProvider(IDataRecord record)
        {
            Fetch(record);
        }

        #endregion

        #region Data Access

        protected void Fetch(IDataRecord reader)
        {
            id = reader.GetInt32(reader.GetOrdinal("AdvertisementEquipmentProviderID"));
            name = reader.GetString(reader.GetOrdinal("AdvertisementEquipmentProviderName"));
        }

        #endregion
    }
}