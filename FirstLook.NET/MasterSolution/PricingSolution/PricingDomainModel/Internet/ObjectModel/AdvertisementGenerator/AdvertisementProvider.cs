using System;
using System.Data;

using Csla;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.AdvertisementGenerator
{
    [Serializable]
    public class AdvertisementProvider : ReadOnlyBase<AdvertisementProvider>
    {
        #region Business Methods

        private int id;
        private string name;
        private int rank;
        private bool selected;

        public int ID
        {
            get
            {
                CanReadProperty("ID", true);
                return id;
            }
        }
        public string Name
        {
            get
            {
                CanReadProperty("Name", true);
                return name;
            }
        }
        public int Rank
        {
            get
            {
                CanReadProperty("Rank", true);
                return rank;
            }
        }
        public bool Selected
        {
            get
            {
                CanReadProperty("Selected", true);
                return selected;
            }
        }

        protected override object GetIdValue()
        {
            return id;
        }

        #endregion Business Methods

        #region Factory Methods

        private AdvertisementProvider()
        {
            //force factory methods
        }

        internal static AdvertisementProvider GetAdvertisementProvider(IDataRecord record)
        {
            return new AdvertisementProvider(record);
        }

        private AdvertisementProvider(IDataRecord record)
        {
            Fetch(record);
        }

        #endregion

        #region Data Access

        protected void Fetch(IDataRecord reader)
        {
            id = reader.GetInt32(reader.GetOrdinal("AdvertisementProviderID"));
            name = reader.GetString(reader.GetOrdinal("AdvertisementProviderName"));
            rank = reader.GetInt32(reader.GetOrdinal("AdvertisementProviderRank"));
            selected = reader.GetBoolean(reader.GetOrdinal("AdvertisementProviderSelected"));
        }

        #endregion
    }
}