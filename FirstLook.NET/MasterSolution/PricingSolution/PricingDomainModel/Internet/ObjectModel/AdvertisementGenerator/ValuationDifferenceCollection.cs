using System;
using System.Data;

using Csla;

using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.AdvertisementGenerator
{
    [Serializable]
    public class ValuationDifferenceCollection : ReadOnlyListBase<ValuationDifferenceCollection, ValuationDifference>
    {
        #region Business Methods

        public ValuationDifference GetItem(int valuationDifferenceId)
        {
            foreach (ValuationDifference vd in this)
                if (vd.ID.Equals(valuationDifferenceId))
                    return vd;
            return null;
        }

        #endregion

        #region Factory Methods

        public static ValuationDifferenceCollection GetValuationDifferenceCollection()
        {
            return DataPortal.Fetch<ValuationDifferenceCollection>();
        }

        public class DataSource
        {
            public ValuationDifferenceCollection Select()
            {
                return GetValuationDifferenceCollection();
            }
        }

        
        #endregion

        #region Data Access

        [Transactional(TransactionalTypes.Manual)]
        private void DataPortal_Fetch()
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Merchandising.ValuationDifferenceCollection#Fetch";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        IsReadOnly = false;

                        while (reader.Read())
                        {
                            ValuationDifference ap = ValuationDifference.GetValuationDifference(reader);
                            Add(ap);
                        }

                        IsReadOnly = true;
                    }
                }
            }

            RaiseListChangedEvents = true;
        }

        #endregion
    }
}