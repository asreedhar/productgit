using System.Data;

using Csla;

using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.AdvertisementGenerator
{
    public class AdvertisementFragmentCollection : ReadOnlyListBase<AdvertisementFragmentCollection, AdvertisementFragment>
    {
        #region Factory Methods

        public static AdvertisementFragmentCollection GetAdvertisementFragmentCollection()
        {
            return DataPortal.Fetch<AdvertisementFragmentCollection>();
        }

        #endregion

        #region Data Access

        [Transactional(TransactionalTypes.Manual)]
        private void DataPortal_Fetch()
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Merchandising.AdvertisementFragmentCollection#Fetch";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        IsReadOnly = false;

                        while (reader.Read())
                        {
                            AdvertisementFragment ap = AdvertisementFragment.GetAdvertisementFragment(reader);
                            Add(ap);
                        }

                        IsReadOnly = true;
                    }
                }
            }

            RaiseListChangedEvents = true;
        }

        #endregion
    }
}