using System;
using System.Data;

using Csla;

using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.AdvertisementGenerator
{
    [Serializable]
    public class ValuationSelectorCollection : ReadOnlyListBase<ValuationSelectorCollection, ValuationSelector>
    {
        #region Business Methods

        public ValuationSelector GetItem(int valuationSelectorId)
        {
            foreach (ValuationSelector vs in this)
                if (vs.ID.Equals(valuationSelectorId))
                    return vs;
            return null;
        }

        #endregion

        #region Factory Methods

        public static ValuationSelectorCollection GetValuationSelectorCollection()
        {
            return DataPortal.Fetch<ValuationSelectorCollection>();
        }

        public class DataSource
        {
            public ValuationSelectorCollection Select()
            {
                return GetValuationSelectorCollection();
            }
        }
        
        #endregion

        #region Data Access

        [Transactional(TransactionalTypes.Manual)]
        private void DataPortal_Fetch()
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Merchandising.ValuationSelectorCollection#Fetch";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        IsReadOnly = false;

                        while (reader.Read())
                        {
                            ValuationSelector ap = ValuationSelector.GetValuationSelector(reader);
                            Add(ap);
                        }

                        IsReadOnly = true;
                    }
                }
            }

            RaiseListChangedEvents = true;
        }

        #endregion
    }
}