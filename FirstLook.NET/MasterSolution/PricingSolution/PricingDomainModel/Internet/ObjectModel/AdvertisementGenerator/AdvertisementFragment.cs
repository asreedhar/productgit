using System;
using System.Data;

using Csla;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.AdvertisementGenerator
{
    [Serializable]
    public class AdvertisementFragment : ReadOnlyBase<AdvertisementFragment>
    {
        #region Business Methods

        private int id;
        private string name;
        private int rank;
        private bool selected;

        public int ID
        {
            get
            {
                CanReadProperty("ID", true);
                return id;
            }
        }
        public string Name
        {
            get
            {
                CanReadProperty("Name", true);
                return name;
            }
        }
        public int Rank
        {
            get
            {
                CanReadProperty("Rank", true);
                return rank;
            }
        }
        public bool Selected
        {
            get
            {
                CanReadProperty("Selected", true);
                return selected;
            }
        }

        protected override object GetIdValue()
        {
            return ID;
        }

        #endregion Business Methods
        
        #region Factory Methods

        private AdvertisementFragment()
        {
            //force factory methods
        }

        internal static AdvertisementFragment GetAdvertisementFragment(IDataRecord record)
        {
            return new AdvertisementFragment(record);
        }

        private AdvertisementFragment(IDataRecord record)
        {
            Fetch(record);
        }

        #endregion

        #region Data Access

        protected void Fetch(IDataRecord reader)
        {
            id = reader.GetInt32(reader.GetOrdinal("AdvertisementFragmentID"));
            name = reader.GetString(reader.GetOrdinal("AdvertisementFragmentName"));
            rank = reader.GetInt32(reader.GetOrdinal("AdvertisementFragmentRank"));
            selected = reader.GetBoolean(reader.GetOrdinal("AdvertisementFragmentSelected"));
        }

        #endregion
    }
}