using System.Data;

using Csla;

using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.AdvertisementGenerator
{
    public class AdvertisementProviderCollection : ReadOnlyListBase<AdvertisementProviderCollection, AdvertisementProvider>
    {
        #region Factory Methods

        public static AdvertisementProviderCollection GetAdvertisementProviderCollection()
        {
            return DataPortal.Fetch<AdvertisementProviderCollection>();
        }
        
        #endregion

        #region Data Access

        [Transactional(TransactionalTypes.Manual)]
        private void DataPortal_Fetch()
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Merchandising.AdvertisementProviderCollection#Fetch";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        IsReadOnly = false;

                        while (reader.Read())
                        {
                            AdvertisementProvider ap = AdvertisementProvider.GetAdvertisementProvider(reader);
                            Add(ap);
                        }

                        IsReadOnly = true;
                    }
                }
            }

            RaiseListChangedEvents = true;
        }

        #endregion
    }
}