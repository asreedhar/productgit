using System;
using System.Data;

using Csla;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.AdvertisementGenerator
{
    [Serializable]
    public class ValuationDifference : ReadOnlyBase<ValuationDifference>
    {
        #region Business Methods

        private int id;
        private int amount;
        private int rank;
        private bool isDefault;

        public int ID
        {
            get
            {
                CanReadProperty("ID", true);
                return id;
            }
        }
        public int Amount
        {
            get
            {
                CanReadProperty("Amount", true);
                return amount;
            }
        }
        public int Rank
        {
            get
            {
                CanReadProperty("Rank", true);
                return rank;
            }
        }
        public bool IsDefault
        {
            get
            {
                CanReadProperty("IsDefault", true);
                return isDefault;
            }
        }

        protected override object GetIdValue()
        {
            return id;
        }

        #endregion Business Methods

        #region Factory Methods

        private ValuationDifference()
        {
            //force factory methods
        }

        internal static ValuationDifference GetValuationDifference(IDataRecord record)
        {
            return new ValuationDifference(record);
        }

        private ValuationDifference(IDataRecord record)
        {
            Fetch(record);
        }

        #endregion

        #region Data Access

        protected void Fetch(IDataRecord reader)
        {
            id = reader.GetInt32(reader.GetOrdinal("ValuationDifferenceID"));
            amount = reader.GetInt32(reader.GetOrdinal("Amount"));
            rank = reader.GetInt32(reader.GetOrdinal("Rank"));
            isDefault = reader.GetBoolean(reader.GetOrdinal("IsDefault"));
        }

        #endregion
    }
}