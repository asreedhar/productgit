using System;
using System.Data;

using Csla;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.AdvertisementGenerator
{
    [Serializable]
    public class ValuationSelector : ReadOnlyBase<ValuationSelector>
    {
        #region Business Methods

        private int id;
        private string name;
        private int rank;
        private bool isDefault;

        public int ID
        {
            get
            {
                CanReadProperty("ID", true);
                return id;
            }
        }
        public string Name
        {
            get
            {
                CanReadProperty("Name", true);
                return name;
            }
        }
        public int Rank
        {
            get
            {
                CanReadProperty("Rank", true);
                return rank;
            }
        }
        public bool IsDefault
        {
            get
            {
                CanReadProperty("IsDefault", true);
                return isDefault;
            }
        }

        protected override object GetIdValue()
        {
            return id;
        }

        #endregion Business Methods

        #region Factory Methods

        private ValuationSelector()
        {
            //force factory methods
        }

        internal static ValuationSelector GetValuationSelector(IDataRecord record)
        {
            return new ValuationSelector(record);
        }

        private ValuationSelector(IDataRecord record)
        {
            Fetch(record);
        }

        #endregion

        #region Data Access

        protected void Fetch(IDataRecord reader)
        {
            id = reader.GetInt32(reader.GetOrdinal("ValuationSelectorID"));
            name = reader.GetString(reader.GetOrdinal("Name"));
            rank = reader.GetInt32(reader.GetOrdinal("Rank"));
            isDefault = reader.GetBoolean(reader.GetOrdinal("IsDefault"));
        }

        #endregion
    }
}