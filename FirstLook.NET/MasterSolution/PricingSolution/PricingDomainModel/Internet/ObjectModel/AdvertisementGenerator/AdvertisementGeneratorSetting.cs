using System;
using System.Data;
using System.Threading;
using Csla;
using FirstLook.Common.Data;
using Csla.Validation;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.AdvertisementGenerator
{
    [Serializable]
    public class AdvertisementGeneratorSetting : BusinessBase<AdvertisementGeneratorSetting>
    {
        #region Business Methods

        private int dealerId;
        private bool enableGeneration;
        private bool enableImport;
        private AdvertisementFragmentSettingCollection advertisementFragmentSettings;
        private AdvertisementProviderSettingCollection advertisementProviderSettings;
        private ValuationProviderSettingCollection valuationProviderSettings;
        private ValuationDifference valuationDifference;
        private ValuationSelector valuationSelector;

        public int DealerId
        {
            get
            {
                CanReadProperty("DealerID", true);

                return dealerId;
            }
            set
            {
                CanWriteProperty("DealerID", true);

                if (!Equals(DealerId, value))
                {
                    dealerId = value;

                    PropertyHasChanged("DealerID");
                }
            }
        }
        public bool EnableGeneration
        {
            get
            {
                CanReadProperty("EnableGeneration", true);

                return enableGeneration;
            }
            set
            {
                CanWriteProperty("EnableGeneration", true);

                if (!Equals(EnableGeneration, value))
                {
                    enableGeneration = value;

                    PropertyHasChanged("EnableGeneration");
                }
            }
        }
        public bool EnableImport
        {
            get
            {
                CanReadProperty("EnableImport", true);

                return enableImport;
            }
            set
            {
                CanWriteProperty("EnableImport", true);

                if (!Equals(EnableImport, value))
                {
                    enableImport = value;

                    PropertyHasChanged("EnableImport");
                }
            }
        }
        public AdvertisementFragmentSettingCollection AdvertisementFragmentSettings
        {
            get
            {
                CanReadProperty("AdvertisementFragmentSettings", true);

                return advertisementFragmentSettings;
            }
            set
            {
                CanWriteProperty("AdvertisementFragmentSettings", true);

                if (!Equals(AdvertisementFragmentSettings, value))
                {
                    advertisementFragmentSettings = value;

                    PropertyHasChanged("AdvertisementFragmentSettings");
                }
            }
        }
        public AdvertisementProviderSettingCollection AdvertisementProviderSettings
        {
            get
            {
                CanReadProperty("AdvertisementProviderSettings", true);

                return advertisementProviderSettings;
            }
            set
            {
                CanWriteProperty("AdvertisementProviderSettings", true);

                if (!Equals(AdvertisementProviderSettings, value))
                {
                    advertisementProviderSettings = value;

                    PropertyHasChanged("AdvertisementProviderSettings");
                }
            }
        }
        public ValuationProviderSettingCollection ValuationProviderSettings
        {
            get
            {
                CanReadProperty("ValuationProviderSettings", true);

                return valuationProviderSettings;
            }
            set
            {
                CanWriteProperty("ValuationProviderSettings", true);

                if (!Equals(ValuationProviderSettings, value))
                {
                    valuationProviderSettings = value;

                    PropertyHasChanged("ValuationProviderSettings");
                }
            }
        }
        public ValuationDifference ValuationDifference
        {
            get
            {
                CanReadProperty("ValuationDifference", true);

                return valuationDifference;
            }
            set
            {
                CanWriteProperty("ValuationDifference", true);

                if (!Equals(ValuationDifference, value))
                {
                    valuationDifference = value;

                    PropertyHasChanged("ValuationDifference");
                }
            }
        }
        public ValuationSelector ValuationSelector
        {
            get
            {
                CanReadProperty("ValuationSelector", true);

                return valuationSelector;
            }
            set
            {
                CanWriteProperty("ValuationSelector", true);

                if (!Equals(ValuationSelector, value))
                {
                    valuationSelector = value;

                    PropertyHasChanged("ValuationSelector");
                }
            }
        }

        protected override object GetIdValue()
        {
            return dealerId;
        }

        #endregion Business Methods

        #region Validation Rules

        public override bool IsValid
        {
            get
            {
                return
                (
                    base.IsValid
                    && AdvertisementFragmentSettings.IsValid
                    && AdvertisementProviderSettings.IsValid
                    && ValuationProviderSettings.IsValid
                    && ValuationDifference != null
                    && ValuationSelector != null
                );
            }
        }

        public override bool IsDirty
        {
            get
            {
                return
                (
                    base.IsDirty
                    || AdvertisementFragmentSettings.IsDirty
                    || AdvertisementProviderSettings.IsDirty
                    || ValuationProviderSettings.IsDirty
                );
            }
        }

        protected override void AddBusinessRules()
        {
            ValidationRules.AddRule<AdvertisementGeneratorSetting>(ValuationDifferenceRequired,
                "ValuationDifference");

            ValidationRules.AddRule<AdvertisementGeneratorSetting>(ValuationSelectorRequired,
                "ValuationSelector");
        }

        private static bool ValuationDifferenceRequired<T>(T target, RuleArgs e) where T : AdvertisementGeneratorSetting
        {
            if (target.ValuationDifference == null)
            {
                e.Description = "You must choose a valuation difference";
                return false;
            }
            else
            {
                return true;
            }
        }

        private static bool ValuationSelectorRequired<T>(T target, RuleArgs e) where T : AdvertisementGeneratorSetting
        {
            if (target.ValuationSelector == null)
            {
                e.Description = "You must choose a valuation selector";
                return false;
            }
            else
            {
                return true;
            }
        }


        #endregion

        #region Factory Methods

        public static AdvertisementGeneratorSetting GetAdvertisementGeneratorSetting(int dealerID, bool createIfNotExists)
        {
            ExistsCommand command = new ExistsCommand(new Criteria(dealerID));
            DataPortal.Execute(command);

            if (command.Exists)
            {
                return DataPortal.Fetch<AdvertisementGeneratorSetting>(new Criteria(dealerID));
            }
            else if (createIfNotExists)
            {
                return DataPortal.Create<AdvertisementGeneratorSetting>(new Criteria(dealerID));
            }
            else
            {
                return null;
            }
        }

        public static AdvertisementGeneratorSetting GetAdvertisementGeneratorSetting(int dealerID)
        {
            return GetAdvertisementGeneratorSetting(dealerID, false);
        }

        public static bool Exists(int dealerID)
        {
            ExistsCommand command = new ExistsCommand(new Criteria(dealerID));

            DataPortal.Execute(command);

            return command.Exists;
        }

        public static AdvertisementGeneratorSetting NewAdvertisementGeneratorSetting(int dealerID)
        {
            return DataPortal.Create<AdvertisementGeneratorSetting>(new Criteria(dealerID));
        }

        private AdvertisementGeneratorSetting()
        {
			/* force use of factory methods */
        }

        #endregion

        #region Data Access

        [Serializable]
        private class Criteria
        {
            private readonly int dealerId;

            public int DealerId
            {
                get { return dealerId; }
            }

            public Criteria(int dealerId)
            {
                this.dealerId = dealerId;
            }

        }

        [Serializable]
        class ExistsCommand : CommandBase
        {
            private readonly Criteria criteria;
            private bool exists;

            public ExistsCommand(Criteria criteria)
            {
                this.criteria = criteria;
            }

            public bool Exists
            {
                get { return exists; }
            }

            protected override void DataPortal_Execute()
            {
                using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.CommandText = "Merchandising.AdvertisementGeneratorSetting#Exists";
                        command.CommandType = CommandType.StoredProcedure;
                        command.AddParameterWithValue("DealerId", DbType.String, false, criteria.DealerId);

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                exists = reader.GetBoolean(reader.GetOrdinal("Exists"));
                            }
                        }
                    }
                }
            }
        }

        [Transactional(TransactionalTypes.Manual)]
        private void DataPortal_Create(Criteria criteria)
        {
            DoFetchOrCreate(criteria, "Merchandising.AdvertisementGeneratorSetting#Create", true);

            MarkNew();
        }

        [Transactional(TransactionalTypes.Manual)]
        private void DataPortal_Fetch(Criteria criteria)
        {
            DoFetchOrCreate(criteria, "Merchandising.AdvertisementGeneratorSetting#Fetch", false);

            MarkOld();
        }

        private void DoFetchOrCreate(Criteria criteria, string commandText, bool isNew)
        {
            dealerId = criteria.DealerId;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandText = commandText;
                    command.CommandType = CommandType.StoredProcedure;
                    command.AddParameterWithValue("DealerId", DbType.String, false, criteria.DealerId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        Fetch(reader, isNew);
                    }
                }
            }
        }

		private static void Read(IDataReader reader, string recordSet)
		{
			if (!reader.Read())
			{
				throw new DataException(string.Format("Expected 1 row in record-set {0}", recordSet));
			}
		}

		private static void NextResult(IDataReader reader, string recordSet)
		{
			if (!reader.NextResult())
			{
				throw new DataException(string.Format("Expected record-set {0}", recordSet));
			}
		}

		private static void NextResultAndRead(IDataReader reader, string recordSet)
		{
			NextResult(reader, recordSet);

			Read(reader, recordSet);
		}

    	private void Fetch(IDataReader reader, bool isNew)
        {
        	Read(reader, "Generator");

			dealerId = reader.GetInt32(reader.GetOrdinal("DealerID"));

            enableImport = reader.GetBoolean(reader.GetOrdinal("EnableImport"));

			enableGeneration = reader.GetBoolean(reader.GetOrdinal("EnableGeneration"));
			
			NextResultAndRead(reader, "ValuationSelector");

            ValuationSelector = ValuationSelector.GetValuationSelector(reader);

			NextResultAndRead(reader, "ValuationDifference");

            ValuationDifference = ValuationDifference.GetValuationDifference(reader);

			NextResult(reader, "ValuationProviders");

            ValuationProviderSettings = ValuationProviderSettingCollection.GetValuationProviderSettingCollection(reader, isNew);

			NextResult(reader, "AdvertisementProviders");

            AdvertisementProviderSettings = AdvertisementProviderSettingCollection.GetAdvertisementProviderSettingCollection(reader, isNew);

			NextResult(reader, "AdvertisementFragments");

            AdvertisementFragmentSettings = AdvertisementFragmentSettingCollection.GetAdvertisementFragmentSettingCollection(reader, isNew);
        }

        [Transactional(TransactionalTypes.Manual)]
        protected override void DataPortal_Insert()
        {
            DoInsertUpdate("Merchandising.AdvertisementGeneratorSetting#Insert");

            MarkOld();
        }

        [Transactional(TransactionalTypes.Manual)]
        protected override void DataPortal_Update()
        {
            DoInsertUpdate("Merchandising.AdvertisementGeneratorSetting#Update");

            MarkOld();
        }

        private void DoInsertUpdate(string commandName)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.CommandText = commandName;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Transaction = transaction;

                        command.AddParameterWithValue("DealerId", DbType.Int32, false, dealerId);
                        command.AddParameterWithValue("EnableGeneration", DbType.Boolean, false, enableGeneration);
                        command.AddParameterWithValue("EnableImport", DbType.Boolean, false, enableImport);
                        command.AddParameterWithValue("ValuationDifferenceID", DbType.Int32, false, valuationDifference.ID);
                        command.AddParameterWithValue("ValuationSelectorID", DbType.Int32, false, valuationSelector.ID);
                        command.AddParameterWithValue("Login", DbType.String, false, Thread.CurrentPrincipal.Identity.Name);

                        command.ExecuteNonQuery();
                    }

                    AdvertisementFragmentSettings.Update(connection, transaction, dealerId);
                    AdvertisementProviderSettings.Update(connection, transaction, dealerId);
                    ValuationProviderSettings.Update(connection, transaction, dealerId);

                    transaction.Commit();
                }
            }
        }

        #endregion
    }
}