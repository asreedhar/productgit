using System.Data;

using Csla;

using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.AdvertisementGenerator
{
    public class ValuationProviderCollection : ReadOnlyListBase<ValuationProviderCollection, ValuationProvider>
    {
        #region Factory Methods

        public static ValuationProviderCollection GetValuationProviderCollection()
        {
            return DataPortal.Fetch<ValuationProviderCollection>();
        }
        
        #endregion

        #region Data Access

        [Transactional(TransactionalTypes.Manual)]
        private void DataPortal_Fetch()
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Merchandising.ValuationProviderCollection#Fetch";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        IsReadOnly = false;

                        while (reader.Read())
                        {
                            ValuationProvider ap = ValuationProvider.GetValuationProvider(reader);
                            Add(ap);
                        }

                        IsReadOnly = true;
                    }
                }
            }

            RaiseListChangedEvents = true;
        }

        #endregion
    }
}