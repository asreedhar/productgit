using System;
using System.Data;

using Csla;

using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.AdvertisementGenerator
{
    [Serializable]
    public class AdvertisementProviderSettingCollection : BusinessListBase<AdvertisementProviderSettingCollection, AdvertisementProviderSetting>
    {
        #region Business Methods

        public void Swap(int index, int index2)
        {
            AdvertisementProviderSetting item = this[index];
            AdvertisementProviderSetting item2 = this[index2];

            this[index2] = item;
            this[index] = item2;

            item.PleaseMarkDirty();
            item2.PleaseMarkDirty();
        }

        #endregion Business Methods

        #region Factory Methods

        internal static AdvertisementProviderSettingCollection GetAdvertisementProviderSettingCollection(IDataReader reader, bool isNew)
        {
            return new AdvertisementProviderSettingCollection(reader, isNew);
        }

        private AdvertisementProviderSettingCollection(IDataReader reader, bool isNew)
        {
            MarkAsChild();
            Fetch(reader, isNew);
        }

        private AdvertisementProviderSettingCollection()
        {
            MarkAsChild();
            //force factory
        }


        #endregion

        #region Data Access

        private void Fetch(IDataReader reader, bool isNew)
        {
            while (reader.Read())
            {
                AdvertisementProviderSetting aps = AdvertisementProviderSetting.GetAdvertisementProviderSetting(reader, isNew);
                Add(aps);
            }
        }


        internal void Update(IDataConnection connection, IDbTransaction transaction, int dealerId)
        {
            RaiseListChangedEvents = false;

            // add/update any current child objects

            for (int i = 0; i < this.Count; i++)
            {
                if (this[i].IsNew)
                {
                    this[i].Insert(connection, transaction, i, dealerId);
                }
                else
                {
                    this[i].Update(connection, transaction, i, dealerId);
                }
            }

            RaiseListChangedEvents = true;
        }
        
        #endregion
    }
}