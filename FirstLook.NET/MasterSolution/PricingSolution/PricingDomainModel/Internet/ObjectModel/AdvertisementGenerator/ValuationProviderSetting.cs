using System;
using System.Data;
using System.Threading;

using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.AdvertisementGenerator
{
    [Serializable]
    public class ValuationProviderSetting : BusinessBase<ValuationProviderSetting>
    {
        #region Business Methods

        private ValuationProvider valuationProvider;
        private bool selected;

        public ValuationProvider ValuationProvider
        {
            get
            {
                CanReadProperty("ValuationProvider", true);

                return valuationProvider;
            }
            set
            {
                CanWriteProperty("ValuationProvider", true);

                if (!Equals(ValuationProvider, value))
                {
                    valuationProvider = value;

                    PropertyHasChanged("ValuationProvider");
                }
            }
        }
        public bool Selected
        {
            get
            {
                CanReadProperty("Selected", true);
                return selected;
            }
            set
            {
                CanWriteProperty("Selected", true);

                if (!Equals(Selected, value))
                {
                    selected = value;

                    PropertyHasChanged("Selected");
                }
            }
        }

        protected override object GetIdValue()
        {
            return valuationProvider.ID;
        }

        internal void PleaseMarkDirty()
        {
            this.MarkDirty();
        }

        #endregion Business Methods

        #region Factory Methods

        private ValuationProviderSetting()
        {
            MarkAsChild();
            //force factory methods
        }

        internal static ValuationProviderSetting GetValuationProviderSetting(IDataRecord record, bool isNew)
        {
            return new ValuationProviderSetting(record, isNew);
        }

        private ValuationProviderSetting(IDataRecord record, bool isNew)
        {
            MarkAsChild();
            Fetch(record);

            if (isNew)
            {
                MarkNew();
            }
            else
            {
                MarkOld();
            }
        }

        #endregion

        #region Data Access

        protected void Fetch(IDataRecord record)
        {
            valuationProvider = ValuationProvider.GetValuationProvider(record);
            selected = record.GetBoolean(record.GetOrdinal("Selected"));
        }

        internal void Insert(IDataConnection connection, IDbTransaction transaction, int rank, int dealerId)
        {
            if (!IsDirty) return;

            DoInsertUpdate(connection, transaction, rank, dealerId, "Merchandising.ValuationProviderSetting#Insert");

            MarkOld();
        }

        internal void Update(IDataConnection connection, IDbTransaction transaction, int rank, int dealerId)
        {
            if (!IsDirty) return;

            if (IsNew) return;

            DoInsertUpdate(connection, transaction, rank, dealerId, "Merchandising.ValuationProviderSetting#Update");

            MarkOld();
        }

        private void DoInsertUpdate(IDataConnection connection, IDbTransaction transaction, int rank, int dealerId, string commandText)
        {
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = commandText;
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;

                command.AddParameterWithValue("DealerID", DbType.Int32, false, dealerId);
                command.AddParameterWithValue("ValuationProviderID", DbType.Int32, false, valuationProvider.ID);
                command.AddParameterWithValue("Rank", DbType.Int32, false, rank);
                command.AddParameterWithValue("Selected", DbType.Boolean, false, selected);
                command.AddParameterWithValue("Login", DbType.String, false, Thread.CurrentPrincipal.Identity.Name);


                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}