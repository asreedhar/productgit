using System;
using System.Data;
using System.Threading;

using Csla;

using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.AdvertisementGenerator
{
    [Serializable]
    public class AdvertisementProviderSetting : BusinessBase<AdvertisementProviderSetting>
    {
        #region Business Methods

        private AdvertisementProvider advertisementProvider;
        private bool selected;

        public AdvertisementProvider AdvertisementProvider
        {
            get
            {
                CanReadProperty("AdvertisementProvider", true);

                return advertisementProvider;
            }
        }
        public bool Selected
        {
            get
            {
                CanReadProperty("Selected", true);
                return selected;
            }
            set
            {
                CanWriteProperty("Selected", true);

                if (!Equals(Selected, value))
                {
                    selected = value;

                    PropertyHasChanged("Selected");
                }
            }
        }

        protected override object GetIdValue()
        {
            return advertisementProvider.ID;
        }

        internal void PleaseMarkDirty()
        {
            this.MarkDirty();
        }

        #endregion Business Methods

        #region Factory Methods

        private AdvertisementProviderSetting()
        {
            //force factory methods
            MarkAsChild();
        }

        internal static AdvertisementProviderSetting GetAdvertisementProviderSetting(IDataRecord record, bool isNew)
        {
            return new AdvertisementProviderSetting(record, isNew);
        }

        private AdvertisementProviderSetting(IDataRecord record, bool isNew)
        {
            Fetch(record);
            MarkAsChild();

            if (isNew)
            {
                MarkNew();
            }
            else
            {
                MarkOld();
            }
        }

        #endregion

        #region Data Access

        protected void Fetch(IDataRecord record)
        {
            advertisementProvider = AdvertisementProvider.GetAdvertisementProvider(record);
            selected = record.GetBoolean(record.GetOrdinal("Selected"));
        }

        internal void Insert(IDataConnection connection, IDbTransaction transaction, int rank, int dealerId)
        {
            if (!IsDirty) return;

            DoInsertUpdate(connection, transaction, rank, dealerId, "Merchandising.AdvertisementProviderSetting#Insert");

            MarkOld();
        }

        internal void Update(IDataConnection connection, IDbTransaction transaction, int rank, int dealerId)
        {
            if (!IsDirty) return;

            if (IsNew) return;

            DoInsertUpdate(connection, transaction, rank, dealerId, "Merchandising.AdvertisementProviderSetting#Update");

            MarkOld();
        }

        private void DoInsertUpdate(IDataConnection connection, IDbTransaction transaction, int rank, int dealerId, string commandText)
        {
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = commandText;
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;

                command.AddParameterWithValue("DealerID", DbType.Int32, false, dealerId);
                command.AddParameterWithValue("AdvertisementProviderID", DbType.Int32, false, advertisementProvider.ID);
                command.AddParameterWithValue("Rank", DbType.Int32, false, rank);
                command.AddParameterWithValue("Selected", DbType.Boolean, false, selected);
                command.AddParameterWithValue("Login", DbType.String, false, Thread.CurrentPrincipal.Identity.Name);


                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}