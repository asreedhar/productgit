using System;
using System.Data;
using System.Text;
using Csla;
using Csla.Validation;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.AdvertisementGenerator
{
    [Serializable]
    public class AdvertisementEquipmentSetting : BusinessBase<AdvertisementEquipmentSetting>
    {
        #region Business Methods

        private int dealerId;
        private string highValueEquipmentPrefix;
        private int highValueEquipmentThreshold = 150;
        private string spacerText;
        private string standardEquipmentPrefix;
        private int equipmentProviderID;

        public int DealerId
        {
            get
            {
                CanReadProperty("DealerId", true);

                return dealerId;
            }
            set
            {
                CanWriteProperty("DealerId", true);

                if (!Equals(DealerId, value))
                {
                    dealerId = value;

                    PropertyHasChanged("DealerId");
                }
            }
        }

        public string HighValueEquipmentPrefix
        {
            get
            {
                CanReadProperty("HighValueEquipmentPrefix", true);

                return highValueEquipmentPrefix;
            }
            set
            {
                CanWriteProperty("HighValueEquipmentPrefix", true);

                if (!Equals(HighValueEquipmentPrefix, value))
                {
                    highValueEquipmentPrefix = value;

                    PropertyHasChanged("HighValueEquipmentPrefix");
                }
            }
        }

        public int HighValueEquipmentThreshold
        {
            get
            {
                CanReadProperty("HighValueEquipmentThreshold", true);

                return highValueEquipmentThreshold;
            }
            set
            {
                CanWriteProperty("HighValueEquipmentThreshold", true);

                if (!Equals(HighValueEquipmentThreshold, value))
                {
                    highValueEquipmentThreshold = value;

                    PropertyHasChanged("HighValueEquipmentThreshold");
                }
            }
        }

        public string SpacerText
        {
            get
            {
                CanReadProperty("SpacerText", true);

                return spacerText;
            }
            set
            {
                CanWriteProperty("SpacerText", true);

                if (!Equals(SpacerText, value))
                {
                    spacerText = value;

                    PropertyHasChanged("SpacerText");
                }
            }
        }

        public string StandardEquipmentPrefix
        {
            get
            {
                CanReadProperty("StandardEquipmentPrefix", true);

                return standardEquipmentPrefix;
            }
            set
            {
                CanWriteProperty("StandardEquipmentPrefix", true);

                if (!Equals(StandardEquipmentPrefix, value))
                {
                    standardEquipmentPrefix = value;

                    PropertyHasChanged("StandardEquipmentPrefix");
                }
            }
        }

        public int EquipmentProviderID
        {
            get
            {
                CanReadProperty("EquipmentProviderID", true);

                return equipmentProviderID;
            }
            set
            {
                CanWriteProperty("EquipmentProviderID", true);

                if (!Equals(EquipmentProviderID, value))
                {
                    equipmentProviderID = value;

                    PropertyHasChanged("EquipmentProviderID");
                }
            }
        }

        protected override object GetIdValue()
        {
            return dealerId;
        }

        #endregion Business Methods

		#region Validation Rules

		protected override void AddBusinessRules()
		{
			ValidationRules.AddRule(CommonRules.StringMaxLength, new CommonRules.MaxLengthRuleArgs("HighValueEquipmentPrefix", 150));
			ValidationRules.AddRule(CommonRules.StringMaxLength, new CommonRules.MaxLengthRuleArgs("StandardEquipmentPrefix", 150));
			ValidationRules.AddRule(CommonRules.StringMaxLength, new CommonRules.MaxLengthRuleArgs("SpacerText", 5));
			ValidationRules.AddRule(CommonRules.IntegerMinValue, new CommonRules.IntegerMinValueRuleArgs("HighValueEquipmentThreshold", 0));
			ValidationRules.AddRule(CommonRules.IntegerMaxValue, new CommonRules.IntegerMaxValueRuleArgs("HighValueEquipmentThreshold", 32767));
		}

		#endregion

		#region Factory Methods

		public static AdvertisementEquipmentSetting GetAdvertisementEquipmentSetting(int dealerId)
        {
            return DataPortal.Fetch<AdvertisementEquipmentSetting>(new FetchCriteria(dealerId));
        }

        public static bool Exists(int dealerId)
        {
            return DataPortal.Fetch<AdvertisementEquipmentSetting>(new FetchCriteria(dealerId)).DealerId != 0;
        }


        public static AdvertisementEquipmentSetting NewAdvertisementEquipmentSetting(int dealerId)
        {
            return new AdvertisementEquipmentSetting(dealerId);
        }

        private AdvertisementEquipmentSetting(int dealerId)
        {
            this.dealerId = dealerId;
        }

        private AdvertisementEquipmentSetting()
        {
			/* force use of factory method */
        }

        #endregion

        #region Data Access

        [Serializable]
        private class FetchCriteria
        {
            private readonly int dealerId;

            public int DealerId
            {
                get { return dealerId; }
            }

            public FetchCriteria(int dealerId)
            {
                this.dealerId = dealerId;
            }

        }

        [Transactional(TransactionalTypes.Manual)]
        private void DataPortal_Fetch(FetchCriteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Merchandising.AdvertisementEquipmentSetting#Fetch";

                    command.AddParameterWithValue("DealerId", DbType.Int32, false, criteria.DealerId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            dealerId = criteria.DealerId;

                            if (!reader.IsDBNull(reader.GetOrdinal("HighValueEquipmentPrefix")))
                            {
                                highValueEquipmentPrefix = reader.GetString(reader.GetOrdinal("HighValueEquipmentPrefix"));
                            }

                            highValueEquipmentThreshold = reader.GetInt16(reader.GetOrdinal("HighValueEquipmentThreshold"));

                            if (!reader.IsDBNull(reader.GetOrdinal("SpacerText")))
                            {
                                spacerText = reader.GetString(reader.GetOrdinal("SpacerText"));
                            }
                            if (!reader.IsDBNull(reader.GetOrdinal("StandardEquipmentPrefix")))
                            {
                                standardEquipmentPrefix = reader.GetString(reader.GetOrdinal("StandardEquipmentPrefix"));
                            }

                            equipmentProviderID = reader.GetInt32(reader.GetOrdinal("EquipmentProviderID"));

                        }

                        MarkOld();
                    }
                }
            }
        }


        [Transactional(TransactionalTypes.TransactionScope)]
        protected override void DataPortal_Insert()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandText = "Merchandising.AdvertisementEquipmentSetting#Insert";

                    DoInsertUpdate(command);
                }
            }

            MarkOld();
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        protected override void DataPortal_Update()
        {
            if (IsDirty)
            {
                using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
                {
                    connection.Open();

                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.CommandText = "Merchandising.AdvertisementEquipmentSetting#Update";

                        DoInsertUpdate(command);
                    }
                }

                MarkOld();
            }
        }

        private void DoInsertUpdate(IDataCommand command)
        {
            command.CommandType = CommandType.StoredProcedure;

            command.AddParameterWithValue("dealerId", DbType.Int32, false, dealerId);
            command.AddParameterWithValue("HighValueEquipmentPrefix", DbType.String, true, string.IsNullOrEmpty(highValueEquipmentPrefix) ? null : highValueEquipmentPrefix);
            command.AddParameterWithValue("HighValueEquipmentThreshold", DbType.Int16, false, highValueEquipmentThreshold);
            command.AddParameterWithValue("SpacerText", DbType.String, true, string.IsNullOrEmpty(spacerText) ? null : spacerText);
            command.AddParameterWithValue("StandardEquipmentPrefix", DbType.String, true, string.IsNullOrEmpty(standardEquipmentPrefix) ? null : standardEquipmentPrefix);

            command.AddParameterWithValue("EquipmentProviderID", DbType.Int32, true, equipmentProviderID);

            command.ExecuteNonQuery();
        }


        #endregion

        #region Data Source

        public class DataSource
        {
            public AdvertisementEquipmentSetting Select(int dealerId)
            {
                if (dealerId == -1 || !Exists(dealerId))
                {
                    return null;
                }

                return GetAdvertisementEquipmentSetting(dealerId);
            }

            public int Insert(int dealerId, string highValueEquipmentPrefix, int highValueEquipmentThreshold, string spacerText, string standardEquipmentPrefix, int equipmentProviderID)
            {
                if (dealerId == -1) return 0;

                AdvertisementEquipmentSetting aes = NewAdvertisementEquipmentSetting(dealerId);
                aes.HighValueEquipmentPrefix = highValueEquipmentPrefix;
                aes.HighValueEquipmentThreshold = highValueEquipmentThreshold;
                aes.SpacerText = spacerText;
                aes.StandardEquipmentPrefix = standardEquipmentPrefix;
                aes.EquipmentProviderID = equipmentProviderID;

                if (aes.IsValid)
                {
                    aes.Save();
                    return 1;
                }
                else
                {
                    throw new DataException(InvalidAdvertisementEquipmentSetting(aes));
                }
            }

            public int Update(int dealerId, string highValueEquipmentPrefix, int highValueEquipmentThreshold, string spacerText, string standardEquipmentPrefix, int equipmentProviderID)
            {
                if (dealerId == -1) return 0;

                AdvertisementEquipmentSetting aes = GetAdvertisementEquipmentSetting(dealerId);
                aes.HighValueEquipmentPrefix = highValueEquipmentPrefix;
                aes.HighValueEquipmentThreshold = highValueEquipmentThreshold;
                aes.SpacerText = spacerText;
                aes.StandardEquipmentPrefix = standardEquipmentPrefix;
                aes.EquipmentProviderID = equipmentProviderID;

                if (aes.IsValid)
                {
                    aes.Save();
                    return 1;
                }
                else
                {
                    throw new DataException(InvalidAdvertisementEquipmentSetting(aes));
                }
            }

            private static string InvalidAdvertisementEquipmentSetting(AdvertisementEquipmentSetting aes)
            {
                StringBuilder sb = new StringBuilder();

                foreach (BrokenRule rule in aes.BrokenRulesCollection)
                {
                    if (sb.Length > 0) sb.Append("; ");

                    sb.Append(rule.Description);
                }

                return sb.ToString();
            }
        }

        #endregion Data Source
    }
}