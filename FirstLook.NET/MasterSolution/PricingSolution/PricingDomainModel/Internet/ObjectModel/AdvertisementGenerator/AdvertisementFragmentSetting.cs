using System;
using System.Data;
using System.Threading;

using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.AdvertisementGenerator
{
    [Serializable]
    public class AdvertisementFragmentSetting : BusinessBase<AdvertisementFragmentSetting>
    {
        #region Business Methods

        private AdvertisementFragment advertisementFragment;
        private bool selected;

        public AdvertisementFragment AdvertisementFragment
        {
            get
            {
                CanReadProperty("AdvertisementFragment", true);

                return advertisementFragment;
            }
        }
        public bool Selected
        {
            get
            {
                CanReadProperty("Selected", true);
                return selected;
            }
            set
            {
                CanWriteProperty("Selected", true);

                if (!Equals(Selected, value))
                {
                    selected = value;

                    PropertyHasChanged("Selected");
                }
            }
        }

        protected override object GetIdValue()
        {
            return advertisementFragment.ID;
        }

        internal void PleaseMarkDirty()
        {
            this.MarkDirty();
        }

        #endregion Business Methods

        #region Factory Methods

        private AdvertisementFragmentSetting()
        {
            //force factory methods
            MarkAsChild();
        }

        internal static AdvertisementFragmentSetting GetAdvertisementFragmentSetting(IDataRecord record, bool isNew)
        {
            return new AdvertisementFragmentSetting(record, isNew);
        }


        private AdvertisementFragmentSetting(IDataRecord record, bool isNew)
        {
            MarkAsChild();
            Fetch(record);

            if (isNew)
            {
                MarkNew();
            }
            else
            {
                MarkOld();
            }
        }

        #endregion

        #region Data Access

        protected void Fetch(IDataRecord record)
        {
            advertisementFragment = AdvertisementFragment.GetAdvertisementFragment(record);
            selected = record.GetBoolean(record.GetOrdinal("Selected"));
        }

        internal void Insert(IDataConnection connection, IDbTransaction transaction, int rank, int dealerId)
        {
            if (!IsDirty) return;

            DoInsertUpdate(connection, transaction, rank, dealerId, "Merchandising.AdvertisementFragmentSetting#Insert");

            MarkOld();
        }

        internal void Update(IDataConnection connection, IDbTransaction transaction, int rank, int dealerId)
        {
            if (!IsDirty) return;

            if (IsNew) return;

            DoInsertUpdate(connection, transaction, rank, dealerId, "Merchandising.AdvertisementFragmentSetting#Update");

            MarkOld();
        }

        private void DoInsertUpdate(IDataConnection connection, IDbTransaction transaction, int rank, int dealerId, string commandText)
        {
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = commandText;
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;

                command.AddParameterWithValue("DealerID", DbType.Int32, false, dealerId);
                command.AddParameterWithValue("AdvertisementFragmentID", DbType.Int32, false, advertisementFragment.ID);
                command.AddParameterWithValue("Rank", DbType.Int32, false, rank);
                command.AddParameterWithValue("Selected", DbType.Boolean, false, selected);
                command.AddParameterWithValue("Login", DbType.String, false, Thread.CurrentPrincipal.Identity.Name);


                command.ExecuteNonQuery();
            }
        }

        #endregion
    }
}