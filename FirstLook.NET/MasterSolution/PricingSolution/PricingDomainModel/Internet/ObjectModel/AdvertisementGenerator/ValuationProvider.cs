using System;
using System.Data;

using Csla;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.AdvertisementGenerator
{
    [Serializable]
    public class ValuationProvider : ReadOnlyBase<ValuationProvider>
    {
        #region Business Methods

        private int id;
        private string name;
        private int rank;

        public int ID
        {
            get
            {
                CanReadProperty("ID", true);
                return id;
            }
        }
        public string Name
        {
            get
            {
                CanReadProperty("Name", true);
                return name;
            }
        }
        public int Rank
        {
            get
            {
                CanReadProperty("Rank", true);
                return rank;
            }
        }

        protected override object GetIdValue()
        {
            return id;
        }

        #endregion Business Methods

        #region Factory Methods

        private ValuationProvider()
        {
            //force factory methods
        }

        internal static ValuationProvider GetValuationProvider(IDataRecord record)
        {
            return new ValuationProvider(record);
        }

        private ValuationProvider(IDataRecord record)
        {
            Fetch(record);
        }

        #endregion

        #region Data Access

        protected void Fetch(IDataRecord reader)
        {
            id = reader.GetInt32(reader.GetOrdinal("ValuationProviderID"));
            name = reader.GetString(reader.GetOrdinal("ValuationProviderName"));
            rank = reader.GetInt32(reader.GetOrdinal("ValuationProviderRank"));
        }

        #endregion
    }
}