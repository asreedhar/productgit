using System;
using System.Data;

using Csla;

using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.AdvertisementGenerator
{
    public class AdvertisementEquipmentProviderCollection : ReadOnlyListBase<AdvertisementEquipmentProviderCollection, AdvertisementEquipmentProvider>
    {
        #region Business Methods

        public AdvertisementEquipmentProvider GetItem(int equipmentProviderId)
        {
            foreach (AdvertisementEquipmentProvider a in this)
                if (a.ID.Equals(equipmentProviderId))
                    return a;
            return null;
        }

        #endregion

        #region Factory Methods

        public static AdvertisementEquipmentProviderCollection GetAdvertisementEquipmentProviderCollection(int dealerID)
        {
            return DataPortal.Fetch<AdvertisementEquipmentProviderCollection>(new FetchCriteria(dealerID));
        }

        #endregion

        #region Data Access

        [Serializable]
        private class FetchCriteria
        {
            private readonly int dealerId;

            public int DealerId
            {
                get { return dealerId; }
            }

            public FetchCriteria(int dealerId)
            {
                this.dealerId = dealerId;
            }

        }


        [Transactional(TransactionalTypes.Manual)]
        private void DataPortal_Fetch(FetchCriteria criteria)
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "Merchandising.AdvertisementEquipmentProviderCollection#Fetch";
                    command.AddParameterWithValue("DealerID", DbType.Int32, false, criteria.DealerId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        IsReadOnly = false;

                        while (reader.Read())
                        {
                            AdvertisementEquipmentProvider ap = AdvertisementEquipmentProvider.GetAdvertisementEquipmentProvider(reader);
                            Add(ap);
                        }

                        IsReadOnly = true;
                    }
                }
            }

            RaiseListChangedEvents = true;
        }

        #endregion

        public class DataSource
        {
            public AdvertisementEquipmentProviderCollection Select(int dealerID)
            {
                return GetAdvertisementEquipmentProviderCollection(dealerID);
            }
        }

    }
}