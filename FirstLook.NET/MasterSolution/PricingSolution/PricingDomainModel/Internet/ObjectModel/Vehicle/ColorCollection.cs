using System;
using System.ComponentModel;
using Csla;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle
{
    [Serializable]
    public class ColorCollection : ReadOnlyListBase<ColorCollection, Color>
    {
        private readonly static ColorCollection standardColors;

        private static readonly Color unknownColor;

        static ColorCollection()
        {
            unknownColor = Color.NewColor("UNKNOWN");

            standardColors = new ColorCollection();
            standardColors.IsReadOnly = false;
            standardColors.Add(unknownColor);
            standardColors.Add(Color.NewColor("BEIGE"));
            standardColors.Add(Color.NewColor("BLACK"));
            standardColors.Add(Color.NewColor("BLUE"));
            standardColors.Add(Color.NewColor("BROWN"));
            standardColors.Add(Color.NewColor("GOLD"));
            standardColors.Add(Color.NewColor("GRAY"));
            standardColors.Add(Color.NewColor("GREEN"));
            standardColors.Add(Color.NewColor("MAROON"));
            standardColors.Add(Color.NewColor("ORANGE"));
            standardColors.Add(Color.NewColor("PEWTER"));
            standardColors.Add(Color.NewColor("RED"));
            standardColors.Add(Color.NewColor("SILVER"));
            standardColors.Add(Color.NewColor("TAN"));
            standardColors.Add(Color.NewColor("WHITE"));
            standardColors.Add(Color.NewColor("YELLOW"));
            standardColors.IsReadOnly = true;
        }

        public Color Find(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return unknownColor;
            }

            name = name.Trim();

            foreach (Color color in this)
            {
                if (string.Equals(color.Name, name, StringComparison.InvariantCultureIgnoreCase))
                {
                    return color;
                }
            }

            return unknownColor;
        }

        [DataObject]
        public class DataSource
        {
            public ColorCollection Select()
            {
                return standardColors;
            }
        }

        private ColorCollection()
        {
			
        }

        public static ColorCollection GetColors()
        {
            return standardColors;
        }
    }
}