using System;
using Csla;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle
{
    [Serializable]
    public class VehicleTypeCommand : CommandBase
    {
        public static VehicleType Execute(string vehicleHandle)
        {
            VehicleTypeCommand command = new VehicleTypeCommand(vehicleHandle);

            DataPortal.Execute(command);

            return command.VehicleType;
        }

        private readonly string vehicleHandle = string.Empty;

        private VehicleType vehicleType = VehicleType.Unknown;

        private VehicleTypeCommand(string vehicleHandle)
        {
            this.vehicleHandle = vehicleHandle;
        }

        public string VehicleHandle
        {
            get { return vehicleHandle; }
        }

        public VehicleType VehicleType
        {
            get { return vehicleType; }
        }

        protected override void DataPortal_Execute()
        {
            char c = vehicleHandle[0];

            switch (c)
            {
                case '1':
                    vehicleType = VehicleType.Inventory;
                    break;
                case '2':
                    vehicleType = VehicleType.Appraisal;
                    break;
                case '3':
                    vehicleType = VehicleType.OnlineAuctionListing;
                    break;
                case '4':
                    vehicleType = VehicleType.DealerGroupListing;
                    break;
                case '5':
                    vehicleType = VehicleType.InternetListing;
                    break;
            }
        }
    }
}