using System;
using Csla;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle
{
    [Serializable]
    public class InteriorType : ReadOnlyBase<InteriorType>
    {
        private readonly string name;

        public string Name
        {
            get { return name; }
        }

        protected override object GetIdValue()
        {
            return Name;
        }

        private InteriorType(string name)
        {
            this.name = name;
        }

        internal static InteriorType NewInteriorType(string name)
        {
            return new InteriorType(name);
        }
    }
}