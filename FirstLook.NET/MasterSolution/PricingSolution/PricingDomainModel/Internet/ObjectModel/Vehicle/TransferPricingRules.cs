using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle
{
    [Serializable]
    public class TransferPricingRules : ReadOnlyBase<TransferPricingRules>
    {
        #region Business Methods
		
        private VehicleType vehicleEntityType;
        private int vehicleEntityId;
        private bool hasTransferPricing;
        private bool canSpecifyRetailOnly;
        private int? maximumTransferPrice;

        public VehicleType VehicleEntityType
        {
            get { return vehicleEntityType; }
        }

        public int VehicleEntityId
        {
            get { return vehicleEntityId; }
        }

        public bool HasTransferPricing
        {
            get { return hasTransferPricing; }
        }

        public bool CanSpecifyRetailOnly
        {
            get { return canSpecifyRetailOnly; }
        }

        public int? MaximumTransferPrice
        {
            get { return maximumTransferPrice; }
        }

        protected override object GetIdValue()
        {
            return vehicleEntityId;
        }

        #endregion

        #region Factory Methods

        public static TransferPricingRules GetTransferPricingRules(string ownerHandle, string vehicleHandle)
        {
            return DataPortal.Fetch<TransferPricingRules>(new Criteria(ownerHandle, vehicleHandle));
        }

        private TransferPricingRules()
        {
            /* force use of factory methods */
        }

        #endregion

        #region Data Access

        [Serializable]
        protected class Criteria
        {
            private readonly string ownerHandle;
            private readonly string vehicleHandle;

            public Criteria(string ownerHandle, string vehicleHandle)
            {
                this.ownerHandle = ownerHandle;
                this.vehicleHandle = vehicleHandle;
            }


            public string OwnerHandle
            {
                get { return ownerHandle; }
            }

            public string VehicleHandle
            {
                get { return vehicleHandle; }
            }
        }

        protected void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandText = "Pricing.TransferPricingRules#Fetch";
                    command.CommandType = CommandType.StoredProcedure;
                    command.AddParameterWithValue("OwnerHandle", DbType.String, false, criteria.OwnerHandle);
                    command.AddParameterWithValue("VehicleHandle", DbType.String, false, criteria.VehicleHandle);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Fetch(reader);
                        }
                        else
                        {
                            throw new DataException("Expected TransferPricingRules result set");
                        }
                    }
                }
            }
        }

        private void Fetch(IDataRecord record)
        {
            vehicleEntityType = (VehicleType) Enum.ToObject(typeof (VehicleType), record.GetInt32(record.GetOrdinal("VehicleEntityTypeId")));

            vehicleEntityId = record.GetInt32(record.GetOrdinal("VehicleEntityId"));

            hasTransferPricing = record.GetBoolean(record.GetOrdinal("HasTransferPricing"));

            canSpecifyRetailOnly = record.GetBoolean(record.GetOrdinal("CanSpecifyRetailOnly"));

            if (!record.IsDBNull(record.GetOrdinal("MaximumTransferPrice")))
            {
                maximumTransferPrice = record.GetInt32(record.GetOrdinal("MaximumTransferPrice"));
            }
        }

        #endregion
    }
}