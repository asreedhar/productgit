using System;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle
{
    [Serializable]
    public enum VehicleType
    {
        Unknown = 0,
        Inventory = 1,
        Appraisal = 2,
        OnlineAuctionListing = 3,
        DealerGroupListing = 4,
        InternetListing = 5
    }
}