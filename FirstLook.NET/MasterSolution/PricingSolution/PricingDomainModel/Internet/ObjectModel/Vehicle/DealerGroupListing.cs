using System;
using System.ComponentModel;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle
{
    [Serializable]
    public class DealerGroupListing : Vehicle<DealerGroupListing>, IInventory
    {
        #region Business Methods
        private DealType _dealType;
        private Light _risk;
        private int _age;
        private bool _certified;
        private string _stockNumber;
        private int? _listPrice;
        private Color _interiorColor;
        private InteriorType _interiorType;

        public int Age
        {
            get { return _age; }
        }

        public bool Certified
        {
            get { return _certified; }
        }

        public DealType DealType
        {
            get { return _dealType; }
        }

        public Color InteriorColor
        {
            get { return _interiorColor; }
        }

        public InteriorType InteriorType
        {
            get { return _interiorType; }
        }

        public int? ListPrice
        {
            get
            {
                CanReadProperty("ListPrice", true);

                return _listPrice;
            }
        }

        public Light Risk
        {
            get { return _risk; }
        }

        public string StockNumber
        {
            get { return _stockNumber; }
        }

        #endregion

        #region Factory Methods
        
        [DataObject]
        public class DataSource
        {
            [DataObjectMethod(DataObjectMethodType.Select)]
            public DealerGroupListing Select(string ownerHandle, string vehicleHandle)
            {
                return GetDealerGroupListing(ownerHandle, vehicleHandle);
            }
        }

        public static DealerGroupListing GetDealerGroupListing(string ownerHandle, string vehicleHandle)
        {
            return DataPortal.Fetch<DealerGroupListing>(new Criteria(ownerHandle, vehicleHandle));
        }

        private DealerGroupListing()
        {
            /* force use of factory methods */
        }

        #endregion

        #region Business Methods

        protected void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandText = "Pricing.VehicleInformation#DealerGroupListing#Fetch";
                    command.CommandType = CommandType.StoredProcedure;

                    criteria.AddToCommand(command);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Fetch(reader);
                        }
                    }
                }
            }
        }

        protected override void Fetch(IDataRecord record)
        {
            base.Fetch(record);

            _dealType = (DealType)record.GetInt32(record.GetOrdinal("DealType"));

            _risk = (Light)record.GetInt32(record.GetOrdinal("RiskLight"));

            _age = record.GetInt32(record.GetOrdinal("Age"));

            _certified = record.GetBoolean(record.GetOrdinal("Certified"));

            _stockNumber = record.GetString(record.GetOrdinal("StockNumber"));

            if (!record.IsDBNull(record.GetOrdinal("InteriorColor")))
            {
                _interiorColor = ColorCollection.GetColors().Find(record.GetString(record.GetOrdinal("InteriorColor")));
            }
            else
            {
                _interiorColor = ColorCollection.GetColors().Find(string.Empty);
            }

            if (!record.IsDBNull(record.GetOrdinal("InteriorType")))
            {
                _interiorType = InteriorTypeCollection.GetInteriorTypes().Find(record.GetString(record.GetOrdinal("InteriorColor")));
            }
            else
            {
                _interiorType = InteriorTypeCollection.GetInteriorTypes().Find(string.Empty);
            }

            if (!record.IsDBNull(record.GetOrdinal("ListPrice")))
            {
                _listPrice = record.GetInt32(record.GetOrdinal("ListPrice"));
            }
        }

        #endregion
    }
}