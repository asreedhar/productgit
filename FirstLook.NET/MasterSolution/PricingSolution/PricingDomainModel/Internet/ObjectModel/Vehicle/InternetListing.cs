using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle
{
    [Serializable]
    public class InternetListing : Vehicle<InternetListing>, IInventory
    {
        #region Business Methods
        
        private int _age;
        private bool _certified;
        private int? _listPrice;
        private string _stockNumber;

        public int Age
        {
            get { return _age; }
        }

        public bool Certified
        {
            get { return _certified; }
        }

        public int? ListPrice
        {
            get { return _listPrice; }
        }

        public string StockNumber
        {
            get { return _stockNumber; }
        }

        #endregion

        #region Factory Methods
        
        public class DataSource
        {
            public InternetListing Select(string ownerHandle, string vehicleHandle)
            {
                return GetInternetListing(ownerHandle, vehicleHandle);
            }

            public void Update(string ownerHandle, string vehicleHandle, int id)
            {
                // do nothing: this is the sales tool! there is no updating ...
            }
        }

        public static InternetListing GetInternetListing(string ownerHandle, string vehicleHandle)
        {
            return DataPortal.Fetch<InternetListing>(new Criteria(ownerHandle, vehicleHandle));
        }

        private InternetListing()
        {
            /* force use of factory methods */
        }

        #endregion

        #region Data Access

        protected void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandText = "Pricing.VehicleInformation#InternetListing#Fetch";
                    command.CommandType = CommandType.StoredProcedure;

                    criteria.AddToCommand(command);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Fetch(reader);
                        }
                    }
                }
            }
        }

        protected override void Fetch(IDataRecord record)
        {
            base.Fetch(record);

            _age = record.GetInt32(record.GetOrdinal("Age"));

            _certified = record.GetBoolean(record.GetOrdinal("Certified"));

            if (!record.IsDBNull(record.GetOrdinal("ListPrice")))
            {
                _listPrice = record.GetInt32(record.GetOrdinal("ListPrice"));
            }

            _stockNumber = record.GetString(record.GetOrdinal("StockNumber"));
        }

        #endregion
    }
}