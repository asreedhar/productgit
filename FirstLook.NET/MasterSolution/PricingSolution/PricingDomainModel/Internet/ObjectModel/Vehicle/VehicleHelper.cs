namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle
{
    public static class VehicleHelper
    {
        public static string GetVin(string ownerHandle, string vehicleHandle)
        {
            IVehicle vehicle = GetVehicle(ownerHandle, vehicleHandle);

            return vehicle != null ? vehicle.Vin : string.Empty;
        }

        public static IVehicle GetVehicle(string ownerHandle, string vehicleHandle)
        {
            switch (VehicleTypeCommand.Execute(vehicleHandle))
            {
                case VehicleType.Appraisal:
                    return Appraisal.GetAppraisal(ownerHandle, vehicleHandle);
                case VehicleType.DealerGroupListing:
                    return DealerGroupListing.GetDealerGroupListing(ownerHandle, vehicleHandle);
                case VehicleType.InternetListing:
                    return InternetListing.GetInternetListing(ownerHandle, vehicleHandle);
                case VehicleType.Inventory:
                    return Inventory.GetInventory(ownerHandle, vehicleHandle);
                case VehicleType.OnlineAuctionListing:
                    return OnlineAuctionListing.GetOnlineAuctionListing(ownerHandle, vehicleHandle);
            }

            return null;
        }
    }
}