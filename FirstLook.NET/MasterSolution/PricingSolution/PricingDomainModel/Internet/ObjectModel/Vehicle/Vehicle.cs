using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle
{
    [Serializable]
    public abstract class Vehicle<T> : BusinessBase<T>, IVehicle where T : BusinessBase<T>
    {
        #region Business Methods

        private VehicleClassification _categorizationClassification;
        private VehicleClassification _decodingClassification;
        private int _id;
        private string _description;
        private string _vin;
        private Color _exteriorColor;
        private int? _mileage;

        public VehicleClassification DecodingClassification
        {
            get { return _decodingClassification; }
        }

        public VehicleClassification CategorizationClassification
        {
            get { return _categorizationClassification; }
        }

        public int Id
        {
            get { return _id; }
        }

        public string Description
        {
            get { return _description; }
        }

        public string Vin
        {
            get { return _vin; }
        }

        public Color ExteriorColor
        {
            get
            {
                CanReadProperty("ExteriorColor", true);

                return _exteriorColor;
            }
            set
            {
                CanWriteProperty("ExteriorColor", true);

                if (!Equals(ExteriorColor, value))
                {
                    _exteriorColor = value;

                    PropertyHasChanged("ExteriorColor");
                }
            }
        }

        public int? Mileage
        {
            get
            {
                CanReadProperty("Mileage", true);

                return _mileage;
            }
            set
            {
                CanWriteProperty("Mileage", true);


                if (!Nullable.Equals(Mileage, value))
                {
                    _mileage = value;

                    PropertyHasChanged("Mileage");
                }
            }
        }

        protected override object GetIdValue()
        {
            return Id;
        }

        #endregion

        #region Validation

        public override bool IsDirty
        {
            get
            {
                return base.IsDirty || DecodingClassification.IsDirty;
            }
        }

        public override bool IsValid
        {
            get
            {
                return base.IsValid && DecodingClassification.IsValid;
            }
        } 

        #endregion

        #region Factory Methods

        #endregion

        #region Data Access

        [Serializable]
        protected class Criteria
        {
            private readonly string _ownerHandle;
            private readonly string _vehicleHandle;

            public Criteria(string ownerHandle, string vehicleHandle)
            {
                _ownerHandle = ownerHandle;
                _vehicleHandle = vehicleHandle;
            }

            public string OwnerHandle
            {
                get { return _ownerHandle; }
            }

            public string VehicleHandle
            {
                get { return _vehicleHandle; }
            }

            public void AddToCommand(IDataCommand command)
            {
                command.AddParameterWithValue("OwnerHandle", DbType.String, false, OwnerHandle);
                command.AddParameterWithValue("VehicleHandle", DbType.String, false, VehicleHandle);
            }
        }

        protected virtual void Fetch(IDataRecord record)
        {
            _decodingClassification = new VehicleClassification(VehicleClassificationType.Decoding, record);
            
            _categorizationClassification = new VehicleClassification(VehicleClassificationType.Categorization, record);

            _id = record.GetInt32(record.GetOrdinal("Id"));

            _description = record.GetString(record.GetOrdinal("Description"));

            _vin = record.GetString(record.GetOrdinal("VIN"));

            if (!record.IsDBNull(record.GetOrdinal("ExteriorColor")))
            {
                _exteriorColor = ColorCollection.GetColors().Find(record.GetString(record.GetOrdinal("ExteriorColor")));
            }
            else
            {
                _exteriorColor = ColorCollection.GetColors().Find(string.Empty);
            }

            if (!record.IsDBNull(record.GetOrdinal("Mileage")))
            {
                _mileage = record.GetInt32(record.GetOrdinal("Mileage"));
            }
        }

        #endregion
    }
}