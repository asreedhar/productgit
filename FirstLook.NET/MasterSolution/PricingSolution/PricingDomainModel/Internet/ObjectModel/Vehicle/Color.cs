using System;
using System.Collections.Generic;
using System.Text;
using Csla;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle
{
    [Serializable]
    public class Color : ReadOnlyBase<Color>
    {
        private readonly string name;

        public string Name
        {
            get { return name; }
        }

        protected override object GetIdValue()
        {
            return Name;
        }

        private Color(string name)
        {
            this.name = name;
        }

        static internal Color NewColor(string name)
        {
            return new Color(name);
        }
    }
}