using System;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle
{
    [Serializable]
    public enum DealType
    {
        Undefined,
        Purchase,
        Trade,
        Unknown
    }
}