using System;
using System.Data;
using Csla;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle
{
    [Serializable]
    public class VehicleClassification : BusinessBase<VehicleClassification>
    {
        #region Business Methods

        private readonly VehicleClassificationType _type;
        private int _modelYear;
        private int _makeId;
        private int _lineId;
        private int _modelId; // note: when categorization is model family id
        private int _id;

        public VehicleClassificationType Type
        {
            get { return _type; }
        }

        public int ModelYear
        {
            get { return _modelYear; }
        }

        public int MakeId
        {
            get { return _makeId; }
        }

        public int LineId
        {
            get { return _lineId; }
        }

        public int ModelId
        {
            get { return _modelId; }
        }

        public int Id
        {
            get
            {
                CanReadProperty("Id", true);

                return _id;
            }
            internal set
            {
                CanWriteProperty("Id", true);

                if (Id != value)
                {
                    _id = value;

                    PropertyHasChanged("Id");
                }
            }
        }

        protected override object GetIdValue()
        {
            return _id;
        }

        #endregion

        #region Factory Methods
		
        internal VehicleClassification(VehicleClassificationType type, IDataRecord record)
        {
            _type = type;

            Fetch(record);
        }

        #endregion

        #region Data Access
		
        private void Fetch(IDataRecord record)
        {
            _modelYear = record.GetInt32(record.GetOrdinal("ModelYear"));

            string prefix = _type == VehicleClassificationType.Categorization
                ? "Categorization"
                : "Decoding";

            _makeId = record.GetInt32(record.GetOrdinal(prefix + "MakeId"));

            _lineId = record.GetInt32(record.GetOrdinal(prefix + "LineId"));

            string id = _type == VehicleClassificationType.Categorization
                ? "ModelConfigurationId"
                : "VehicleCatalogId";

            _id = record.GetInt32(record.GetOrdinal(id));

            id = _type == VehicleClassificationType.Categorization
                ? "CategorizationModelFamilyId"
                : "DecodingModelId";

            if (!record.IsDBNull(record.GetOrdinal(id)))
            {
                _modelId = record.GetInt32(record.GetOrdinal(id));
            }
        }

        #endregion
    }
}