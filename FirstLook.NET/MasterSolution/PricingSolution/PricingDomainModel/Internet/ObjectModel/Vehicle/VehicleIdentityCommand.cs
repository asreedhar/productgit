using System;
using Csla;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle
{
    [Serializable]
    public class VehicleIdentityCommand : CommandBase
    {
        public static int Execute(string vehicleHandle)
        {
            VehicleIdentityCommand command = new VehicleIdentityCommand(vehicleHandle);

            DataPortal.Execute(command);

            return command.identity;
        }

        private readonly string vehicleHandle = string.Empty;

        private int identity;

        private VehicleIdentityCommand (string vehicleHandle)
        {
            this.vehicleHandle = vehicleHandle;
        }

        protected override void DataPortal_Execute()
        {
            identity = int.Parse(vehicleHandle.Substring(1));
        }
    }
}