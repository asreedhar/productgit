﻿using System.Data;
using System.Data.SqlClient;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle
{
    public class GetInventoryManufacturerMakeInfo : CachedCommand
    {
        public int InventoryId { get; private set; }
        public InventoryManufacturerMakeTO Result { get; private set; }

        public GetInventoryManufacturerMakeInfo(int inventoryId)
        {
            InventoryId = inventoryId;
        }

        protected override void Run()
        {
            Result = new InventoryManufacturerMakeTO();

            using (var connection =  SimpleQuery.ConfigurationManagerConnection(Database.Merchandising))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "builder.GetInventoryManufacturerMake";
                    command.CommandType = CommandType.StoredProcedure;
                    CommonMethods.AddWithValue(command, "InventoryId", InventoryId, DbType.Int32);

                    var makeIdParam = new SqlParameter("MakeId", SqlDbType.Int) { Direction = ParameterDirection.Output };

                    var makeNameParam = new SqlParameter("MakeName", SqlDbType.VarChar) { Direction = ParameterDirection.Output, Size = 50 };

                    var manufacturerIdParam = new SqlParameter("ManufacturerId", SqlDbType.Int) { Direction = ParameterDirection.Output };

                    var manufacturerNameParam = new SqlParameter("ManufacturerName", SqlDbType.VarChar)
                    {
                        Direction = ParameterDirection.Output,
                        Size = 50
                    };

                    command.Parameters.Add(makeIdParam);
                    command.Parameters.Add(makeNameParam);
                    command.Parameters.Add(manufacturerIdParam);
                    command.Parameters.Add(manufacturerNameParam);

                    command.ExecuteNonQuery();

                    var makeid = makeIdParam.Value as int?;
                    var makename = makeNameParam.Value.ToString();
                    var mfrid = manufacturerIdParam.Value as int?;
                    var mfrname = manufacturerNameParam.Value.ToString();

                    Result = new InventoryManufacturerMakeTO(makeid, makename, mfrid, mfrname);

                }
            }
        }

        public class InventoryManufacturerMakeTO
        {
            public InventoryManufacturerMakeTO()
            {
            }

            public InventoryManufacturerMakeTO(int? makeId, string makeName, int? manufacturerId, string manufacturerName)
            {
                MakeId = makeId;
                MakeName = makeName;
                ManufacturerId = manufacturerId;
                ManufacturerName = manufacturerName;
            }

            public int? MakeId { get; private set; }
            public string MakeName { get; private set; }
            public int? ManufacturerId { get; private set; }
            public string ManufacturerName { get; private set; }
        }
    }
}
