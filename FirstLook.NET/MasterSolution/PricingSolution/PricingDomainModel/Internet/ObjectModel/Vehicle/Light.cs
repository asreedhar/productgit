using System;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle
{
    [Serializable]
    public enum Light
    {
        Undefined,
        Red,
        Yellow,
        Green
    }
}