using System;
using System.ComponentModel;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle
{
    [Serializable]
    public class Appraisal : Vehicle<Appraisal>
    {
        private DealType _dealType;
        private Light _risk;
        private int _age;
        private Color _interiorColor;
        private InteriorType _interiorType;

        public int Age
        {
            get { return _age; }
        }

        public DealType DealType
        {
            get { return _dealType; }
        }
		
        public Color InteriorColor
        {
            get
            {
                CanReadProperty("InteriorColor", true);

                return _interiorColor;
            }
            set
            {
                CanWriteProperty("InteriorColor", true);

                if (!Equals(InteriorColor, value))
                {
                    _interiorColor = value;

                    PropertyHasChanged("InteriorColor");
                }
            }
        }

        public InteriorType InteriorType
        {
            get
            {
                CanReadProperty("InteriorType", true);

                return _interiorType;
            }
            set
            {
                CanWriteProperty("InteriorType", true);

                if (!Equals(InteriorType, value))
                {
                    _interiorType = value;

                    PropertyHasChanged("InteriorType");
                }
            }
        }

        public Light Risk
        {
            get { return _risk; }
        }

        [DataObject]
        public class DataSource
        {
            [DataObjectMethod(DataObjectMethodType.Select)]
            public Appraisal Select(string ownerHandle, string vehicleHandle)
            {
                return GetAppraisal(ownerHandle, vehicleHandle);
            }

            [DataObjectMethod(DataObjectMethodType.Update)]
            public void Update(string ownerHandle, string vehicleHandle, int id, int vehicleCatalogId, int? mileage, string interiorColor, string interiorType, string exteriorColor)
            {
                Appraisal appraisal = GetAppraisal(ownerHandle, vehicleHandle);
                appraisal.DecodingClassification.Id = vehicleCatalogId;
                appraisal.Mileage = mileage;
                appraisal.InteriorColor = ColorCollection.GetColors().Find(interiorColor);
                appraisal.InteriorType = InteriorTypeCollection.GetInteriorTypes().Find(interiorType);
                appraisal.ExteriorColor = ColorCollection.GetColors().Find(exteriorColor);
                appraisal.Save();
            }
        }

        public static Appraisal GetAppraisal(string ownerHandle, string vehicleHandle)
        {
            return DataPortal.Fetch<Appraisal>(new Criteria(ownerHandle, vehicleHandle));
        }

        protected void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandText = "Pricing.VehicleInformation#Appraisal#Fetch";
                    command.CommandType = CommandType.StoredProcedure;

                    criteria.AddToCommand(command);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Fetch(reader);
                        }
                    }
                }
            }
        }

        protected override void DataPortal_Update()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.CommandText = "Pricing.VehicleInformation#Appraisal#Update";
                        command.CommandType = CommandType.StoredProcedure;
                        command.Transaction = transaction;

                        command.AddParameterWithValue("Id", DbType.Int32, false, Id);
                        command.AddParameterWithValue("VehicleCatalogId", DbType.Int32, false, DecodingClassification.Id);
                        command.AddParameterWithValue("Mileage", DbType.Int32, false, Mileage);

                        if (InteriorColor == null || string.Equals(InteriorColor.Name, "UNKNOWN"))
                        {
                            command.AddParameterWithValue("InteriorColor", DbType.String, true, DBNull.Value);
                        }
                        else
                        {
                            command.AddParameterWithValue("InteriorColor", DbType.String, true, InteriorColor.Name);
                        }

                        if (InteriorType == null || string.Equals(InteriorType.Name, "UNKNOWN"))
                        {
                            command.AddParameterWithValue("InteriorType", DbType.String, true, DBNull.Value);
                        }
                        else
                        {
                            command.AddParameterWithValue("InteriorType", DbType.String, true, InteriorType.Name);
                        }

                        command.AddParameterWithValue("ExteriorColor", DbType.String, false, ExteriorColor.Name);

                        command.ExecuteNonQuery();

                        transaction.Commit();
                    }
                }
            }
        }

        protected override void Fetch(IDataRecord record)
        {
            base.Fetch(record);

            _dealType = (DealType) record.GetInt32(record.GetOrdinal("DealType"));

            _risk = (Light) record.GetInt32(record.GetOrdinal("RiskLight"));

            _age = record.GetInt32(record.GetOrdinal("Age"));

            if (!record.IsDBNull(record.GetOrdinal("InteriorColor")))
            {
                _interiorColor = ColorCollection.GetColors().Find(record.GetString(record.GetOrdinal("InteriorColor")));
            }
            else
            {
                _interiorColor = ColorCollection.GetColors().Find(string.Empty);
            }

            if (!record.IsDBNull(record.GetOrdinal("InteriorType")))
            {
                _interiorType = InteriorTypeCollection.GetInteriorTypes().Find(record.GetString(record.GetOrdinal("InteriorColor")));
            }
            else
            {
                _interiorType = InteriorTypeCollection.GetInteriorTypes().Find(string.Empty);
            }
        }
    }
}