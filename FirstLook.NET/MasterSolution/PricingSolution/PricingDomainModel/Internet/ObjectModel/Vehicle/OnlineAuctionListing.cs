using System;
using System.ComponentModel;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle
{
    [Serializable]
    public class OnlineAuctionListing : Vehicle<OnlineAuctionListing>
    {
        private Light risk;
        private string auctionNotes;
        private string auctionLocation;
        private int? auctionDistance;
        private int? auctionBidPrice;

        public string AuctionNotes
        {
            get { return auctionNotes; }
        }

        public string AuctionLocation
        {
            get { return auctionLocation; }
        }

        public int? AuctionDistance
        {
            get { return auctionDistance; }
        }

        public int? AuctionBidPrice
        {
            get { return auctionBidPrice; }
        }

        public Light Risk
        {
            get { return risk; }
        }

        [DataObject]
        public class DataSource
        {
            [DataObjectMethod(DataObjectMethodType.Select)]
            public OnlineAuctionListing Select(string ownerHandle, string vehicleHandle)
            {
                return GetOnlineAuctionListing(ownerHandle, vehicleHandle);
            }
        }

        public static OnlineAuctionListing GetOnlineAuctionListing(string ownerHandle, string vehicleHandle)
        {
            return DataPortal.Fetch<OnlineAuctionListing>(new Criteria(ownerHandle, vehicleHandle));
        }

        protected void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandText = "Pricing.VehicleInformation#OnlineAuctionListing#Fetch";
                    command.CommandType = CommandType.StoredProcedure;

                    criteria.AddToCommand(command);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Fetch(reader);
                        }
                    }
                }
            }
        }

        protected override void Fetch(IDataRecord record)
        {
            base.Fetch(record);

            risk = (Light)record.GetInt32(record.GetOrdinal("RiskLight"));

            auctionNotes = record.GetString(record.GetOrdinal("AuctionNotes"));

            auctionLocation = record.GetString(record.GetOrdinal("AuctionLocation"));

            if (!record.IsDBNull(record.GetOrdinal("AuctionBidPrice")))
            {
                auctionBidPrice = record.GetInt32(record.GetOrdinal("AuctionBidPrice"));
            }

            int ordinal = record.GetOrdinal("AuctionDistance");
            if (!record.IsDBNull(ordinal))
            {
                //auctionDistance is null when searching unlimited mileage
                auctionDistance = record.GetInt32(ordinal);
            }
        }
    }
}