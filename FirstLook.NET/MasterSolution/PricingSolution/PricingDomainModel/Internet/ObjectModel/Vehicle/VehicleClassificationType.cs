using System;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle
{
    [Serializable]
    public enum VehicleClassificationType
    {
        Categorization,
        Decoding
    }
}