﻿
namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle
{
    public class VehicleUpdateCertificationResult
    {
        public bool ManufacturerCertified { get; set; }
    }
}
