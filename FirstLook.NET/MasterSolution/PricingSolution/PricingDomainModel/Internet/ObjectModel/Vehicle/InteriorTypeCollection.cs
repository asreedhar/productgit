using System;
using System.ComponentModel;
using Csla;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle
{
    [Serializable]
    public class InteriorTypeCollection : ReadOnlyListBase<InteriorTypeCollection, InteriorType>
    {
        private static readonly InteriorTypeCollection standardInteriors;

        private static readonly InteriorType unknownInterior;

        static InteriorTypeCollection()
        {
            unknownInterior = InteriorType.NewInteriorType("UNKNOWN");

            standardInteriors = new InteriorTypeCollection();
            standardInteriors.IsReadOnly = false;
            standardInteriors.Add(unknownInterior);
            standardInteriors.Add(InteriorType.NewInteriorType("LEATHER"));
            standardInteriors.Add(InteriorType.NewInteriorType("LEATHERETTE"));
            standardInteriors.Add(InteriorType.NewInteriorType("VINYL"));
            standardInteriors.Add(InteriorType.NewInteriorType("CLOTH"));
            standardInteriors.IsReadOnly = true;
        }

        public InteriorType Find(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return unknownInterior;
            }

            name = name.Trim();

            foreach (InteriorType interiorType in this)
            {
                if (string.Equals(interiorType.Name, name, StringComparison.InvariantCultureIgnoreCase))
                {
                    return interiorType;
                }
            }

            return unknownInterior;
        }

        [DataObject]
        public class DataSource
        {
            public InteriorTypeCollection Select()
            {
                return standardInteriors;
            }
        }

        private InteriorTypeCollection()
        {
			
        }

        public static InteriorTypeCollection GetInteriorTypes()
        {
            return standardInteriors;
        }
    }
}