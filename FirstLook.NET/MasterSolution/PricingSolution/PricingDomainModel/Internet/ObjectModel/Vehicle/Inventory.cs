using System;
using System.ComponentModel;
using System.Data;
using System.Runtime.ConstrainedExecution;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using Csla;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Data;
using IDataConnection = FirstLook.Common.Data.IDataConnection;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle
{
    [Serializable]
    public class Inventory : Vehicle<Inventory>, IInventory
    {
        #region Business Methods

        private DealType _dealType;
        private Light _risk;
        private int _age;
        private bool _certified;
        private string _stockNumber;
        private Color _interiorColor;
        private InteriorType _interiorType;
        private int _unitCost;
        private bool _hasSpecialFinance;
        private string _lotLocation;
        private int? _listPrice;
        private int? _transferPrice;
        private bool _transferPriceForRetailOnly;
	    private int? _certifiedProgramId;
        private string _certificationNumber { get; set; }

	    public int Year { get; private set; }
        public string Make { get; private set; }
        public string Line { get; private set; }
        public string Series { get; private set; }
        public int BusinessUnitId { get; private set; }


        private VehicleCertification.Manufacturer _manufacturer;
        public VehicleCertification.Manufacturer Manufacturer
        {
            get
            {
                //var o = ViewState["ManufacturerID"];
                //if (o == null)
                //{
                    if (_manufacturer.Id == 0)
                    {
                        try
                        {
                            _manufacturer = VehicleCertification.GetManufacturer(Id);
                        }
                        catch (VehicleCertification.FailedToGetManufacturer) { }
                    }
                   // ViewState["ManufacturerID"] = _manufacturer;
                    return _manufacturer;
                }
            //    return (VehicleCertification.Manufacturer)o;
            //}
        }
        
        public int Age
        {
            get { return _age; }
        }

        public bool Certified
        {
            get
            {
                CanReadProperty("Certified", true);

                return _certified;
            }
            set
            {
                CanWriteProperty("Certified", true);

                if (Certified != value)
                {
                    _certified = value;

                    PropertyHasChanged("Certified");
                }
            }
        }

		public int? CertifiedProgramId
		{
			get
			{
				CanReadProperty("CertifiedProgramId", true);

				return _certifiedProgramId;
			}
            set
            {
                CanWriteProperty("CertifiedProgramId", true);

                if (CertifiedProgramId != value)
                {
                    _certifiedProgramId = value;

                    PropertyHasChanged("CertifiedProgramId");
                }
            }
		}

        public string CertificationNumber
        {
            get { return _certificationNumber; }
            set { _certificationNumber = value; }
        }
        

        public DealType DealType
        {
            get { return _dealType; }
        }

        public bool HasSpecialFinance
        {
            get
            {
                CanReadProperty("HasSpecialFinance", true);

                return _hasSpecialFinance;
            }
            set
            {
                CanWriteProperty("HasSpecialFinance", true);

                if (HasSpecialFinance != value)
                {
                    _hasSpecialFinance = value;

                    PropertyHasChanged("HasSpecialFinance");
                }
            }
        }

        public Color InteriorColor
        {
            get
            {
                CanReadProperty("InteriorColor", true);

                return _interiorColor;
            }
            set
            {
                CanWriteProperty("InteriorColor", true);

                if (!Equals(InteriorColor, value))
                {
                    _interiorColor = value;

                    PropertyHasChanged("InteriorColor");
                }
            }
        }

        public InteriorType InteriorType
        {
            get
            {
                CanReadProperty("InteriorType", true);

                return _interiorType;
            }
            set
            {
                CanWriteProperty("InteriorType", true);

                if (!Equals(InteriorType, value))
                {
                    _interiorType = value;

                    PropertyHasChanged("InteriorType");
                }
            }
        }

        public int? ListPrice
        {
            get
            {
                CanReadProperty("ListPrice", true);

                return _listPrice;
            }
        }

        public string LotLocation
        {
            get
            {
                CanReadProperty("LotLocation", true);

                return _lotLocation;
            }
            set
            {
                CanWriteProperty("LotLocation", true);

                if (!Equals(LotLocation, value))
                {
                    _lotLocation = value;

                    PropertyHasChanged("LotLocation");
                }
            }
        }
        public Light Risk
        {
            get { return _risk; }
        }

        public string StockNumber
        {
            get { return _stockNumber; }
        }

        public int? TransferPrice
        {
            get
            {
                CanReadProperty("TransferPrice", true);

                return _transferPrice;
            }
            set
            {
                CanWriteProperty("TransferPrice", true);

                if (TransferPrice != value)
                {
                    _transferPrice = value;

                    PropertyHasChanged("TransferPrice");
                }
            }
        }

        public bool TransferPriceForRetailOnly
        {
            get
            {
                CanReadProperty("TransferPriceForRetailOnly", true);

                return _transferPriceForRetailOnly;
            }
            set
            {
                CanWriteProperty("TransferPriceForRetailOnly", true);

                if (TransferPriceForRetailOnly != value)
                {
                    _transferPriceForRetailOnly = value;

                    PropertyHasChanged("TransferPriceForRetailOnly");
                }
            }
        }

        public int UnitCost
        {
            get { return _unitCost; }
        }




        #endregion    

        #region Factory Methods
        
        [DataObject]
        public class DataSource
        {
            [DataObjectMethod(DataObjectMethodType.Select)]
            public Inventory Select(string ownerHandle, string vehicleHandle)
            {
                return GetInventory(ownerHandle, vehicleHandle);
            }

            [DataObjectMethod(DataObjectMethodType.Update)]
            public void Update(string ownerHandle, string vehicleHandle, int id, int vehicleCatalogId, int? mileage, string interiorColor, string interiorType, string exteriorColor, int certifiedProgramId, bool hasSpecialFinance, string lotLocation, int? transferPrice, bool transferPriceForRetailOnly, bool certified, string certificationNumber)
            {
                Inventory inventory = GetInventory(ownerHandle, vehicleHandle);
                inventory.DecodingClassification.Id = vehicleCatalogId;
                inventory.Mileage = mileage;
                inventory.InteriorColor = ColorCollection.GetColors().Find(interiorColor);
                inventory.InteriorType = InteriorTypeCollection.GetInteriorTypes().Find(interiorType);
                inventory.ExteriorColor = ColorCollection.GetColors().Find(exteriorColor);
                inventory.CertifiedProgramId = certifiedProgramId;
                inventory.Certified = certified;
                inventory.HasSpecialFinance = hasSpecialFinance;
                inventory.LotLocation = lotLocation;
                inventory.TransferPrice = transferPrice;
                inventory.TransferPriceForRetailOnly = transferPriceForRetailOnly;
                inventory.CertificationNumber = certificationNumber;
                inventory.Save();
            }
        }

        public static Inventory GetInventory(string ownerHandle, string vehicleHandle)
        {
            return DataPortal.Fetch<Inventory>(new Criteria(ownerHandle, vehicleHandle));
        }

        private Inventory()
        {
            /* force use of factory methods */
        }

        #endregion

        #region Data Access

        protected void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandText = "Pricing.VehicleInformation#Inventory#Fetch";
                    command.CommandType = CommandType.StoredProcedure;

                    criteria.AddToCommand(command);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Fetch(reader);
                        }
                    }
                }
            }
        }


        /// <remarks>
        /// IMT sucks ass, you can not put transactions around anything, Damit - Simon
        /// </remarks>
        [Transactional(TransactionalTypes.Manual)]
        protected override void DataPortal_Update()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                //using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.CommandText = "Pricing.VehicleInformation#Inventory#Update";
                        command.CommandType = CommandType.StoredProcedure;
                        //command.Transaction = transaction;

                        command.AddParameterWithValue("Id", DbType.Int32, false, Id);
                        command.AddParameterWithValue("VehicleCatalogId", DbType.Int32, false, DecodingClassification.Id);
                        command.AddParameterWithValue("Mileage", DbType.Int32, false, Mileage);

                        if (InteriorColor == null || string.Equals(InteriorColor.Name, "UNKNOWN"))
                        {
                            command.AddParameterWithValue("InteriorColor", DbType.String, true, DBNull.Value);
                        }
                        else
                        {
                            command.AddParameterWithValue("InteriorColor", DbType.String, true, InteriorColor.Name);
                        }

                        if (InteriorType == null || string.Equals(InteriorType.Name, "UNKNOWN"))
                        {
                            command.AddParameterWithValue("InteriorType", DbType.String, true, DBNull.Value);
                        }
                        else
                        {
                            command.AddParameterWithValue("InteriorType", DbType.String, true, InteriorType.Name);
                        }

                        command.AddParameterWithValue("ExteriorColor", DbType.String, false, ExteriorColor.Name);
                        command.AddParameterWithValue("HasSpecialFinance", DbType.Boolean, false, HasSpecialFinance);

                        if (string.IsNullOrEmpty(LotLocation.Trim()))
                        {
                            command.AddParameterWithValue("VehicleLocation", DbType.String, true, DBNull.Value);
                        }
                        else
                        {
                            command.AddParameterWithValue("VehicleLocation", DbType.String, true, LotLocation);
                        }

                        if (TransferPrice.HasValue)
                        {
                            command.AddParameterWithValue("TransferPrice", DbType.Int32, true, TransferPrice.Value);
                        }
                        else
                        {
                            command.AddParameterWithValue("TransferPrice", DbType.Int32, true, DBNull.Value);
                        }

                        command.AddParameterWithValue("TransferPriceForRetailOnly", DbType.Boolean, false, TransferPriceForRetailOnly);

                        command.AddParameterWithValue("UserName", DbType.String, false, Thread.CurrentPrincipal.Identity.Name);

                        command.ExecuteNonQuery();

                        //transaction.Commit();
                    }
                }
            }
           
            var certifiedId = string.Empty;  // we don't have any certifiedIds coming in on feeds (yet).
           // VehicleCertification.ValidateCertifiedId(CertificationNumber, Manufacturer.Name)
            if (CertificationNumber != null)
            {
               
                certifiedId = CertificationNumber;
            }
            VehicleCertification.CreateOrUpdate(this.BusinessUnitId, Id, Certified, Convert.ToInt32(CertifiedProgramId), certifiedId,
                Thread.CurrentPrincipal.Identity.Name);
        }

        protected override void Fetch(IDataRecord record)
        {
            base.Fetch(record);

            BusinessUnitId = record.GetInt32(record.GetOrdinal("BusinessUnitId"));
            Year = record.GetInt32(record.GetOrdinal("ModelYear"));
            Make = record.GetString(record.GetOrdinal("Make"));
            Line = record.GetString(record.GetOrdinal("Line"));
            Series = record.GetString(record.GetOrdinal("Series"));

            _dealType = (DealType)record.GetInt32(record.GetOrdinal("DealType"));

            _risk = (Light)record.GetInt32(record.GetOrdinal("RiskLight"));

            _age = record.GetInt32(record.GetOrdinal("Age"));

            _certified = record.GetBoolean(record.GetOrdinal("Certified"));
	        _certifiedProgramId = record.GetNullableInt32("CertifiedProgramId");

            _stockNumber = record.GetString(record.GetOrdinal("StockNumber"));

            _unitCost = record.GetInt32(record.GetOrdinal("UnitCost"));

            if (!record.IsDBNull(record.GetOrdinal("HasSpecialFinance")))
            {
                _hasSpecialFinance = record.GetBoolean(record.GetOrdinal("HasSpecialFinance"));
            }

            if (!record.IsDBNull(record.GetOrdinal("VehicleLocation")))
            {
                _lotLocation = record.GetString(record.GetOrdinal("VehicleLocation")).Trim();
            }
            else
            {
                _lotLocation = string.Empty;
            }

            if (!record.IsDBNull(record.GetOrdinal("InteriorColor")))
            {
                _interiorColor = ColorCollection.GetColors().Find(record.GetString(record.GetOrdinal("InteriorColor")));
            }
            else
            {
                _interiorColor = ColorCollection.GetColors().Find(string.Empty);
            }

            if (!record.IsDBNull(record.GetOrdinal("InteriorType")))
            {
                _interiorType = InteriorTypeCollection.GetInteriorTypes().Find(record.GetString(record.GetOrdinal("InteriorType")));
            }
            else
            {
                _interiorType = InteriorTypeCollection.GetInteriorTypes().Find(string.Empty);
            }

            if (!record.IsDBNull(record.GetOrdinal("ListPrice")))
            {
                _listPrice = record.GetInt32(record.GetOrdinal("ListPrice"));
            }

            if (!record.IsDBNull(record.GetOrdinal("TransferPrice")))
            {
                _transferPrice = record.GetInt32(record.GetOrdinal("TransferPrice"));
            }
            else
            {
                _transferPrice = null;
            }

            _transferPriceForRetailOnly = record.GetBoolean(record.GetOrdinal("TransferPriceForRetailOnly"));

            if (!record.IsDBNull(record.GetOrdinal("CertificationNumber")))
            {
                _certificationNumber = record.GetString(record.GetOrdinal("CertificationNumber")).Trim();
            }
        }

        #endregion
    }
}