namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle
{
    public interface IVehicle
    {
        VehicleClassification DecodingClassification { get; }
        VehicleClassification CategorizationClassification { get; }
        string Description { get; }
        string Vin { get; }
        int? Mileage { get; }
        Color ExteriorColor { get; }
    }

    public interface IInventory : IVehicle
    {
        int? ListPrice { get; }
        bool Certified { get; }
    }
}