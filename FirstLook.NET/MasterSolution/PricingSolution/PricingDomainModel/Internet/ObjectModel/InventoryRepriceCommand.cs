using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel
{
    [Serializable]
    public class InventoryRepriceCommand : CommandBase
    {
        public static void Execute(string userName, int inventoryId, int originalListPrice, int newListPrice, bool confirmed)
        {
            Execute(userName, inventoryId, originalListPrice, newListPrice, confirmed, 3); // 3 is pingII
        }

        public static void Execute(string userName, int inventoryId, int originalListPrice, int newListPrice, bool confirmed, short source)
        {
            InventoryRepriceCommand command = new InventoryRepriceCommand(userName, inventoryId, originalListPrice, newListPrice, confirmed, source);
            DataPortal.Execute(command);
        }

        private readonly string userName;
        private readonly int inventoryId;
        private readonly int oldListPrice;
        private readonly int newListPrice;
        private readonly bool confirmed;
        private readonly short source;

        private InventoryRepriceCommand(string userName, int inventoryId, int oldListPrice, int newListPrice, bool confirmed, short source)
        {
            this.userName = userName;
            this.inventoryId = inventoryId;
            this.oldListPrice = oldListPrice;
            this.newListPrice = newListPrice;
            this.confirmed = confirmed;
            this.source = source;
        }

        protected override void DataPortal_Execute()
        {
            DateTime now = DateTime.Now;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.InventoryDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "dbo.InsertRepriceEvent";
                    command.AddParameterWithValue("inventoryID", DbType.Int32, false, inventoryId);
                    command.AddParameterWithValue("beginDate", DbType.DateTime, false, now);
                    command.AddParameterWithValue("endDate", DbType.DateTime, false, now);
                    command.AddParameterWithValue("value", DbType.Int32, false, newListPrice);
                    command.AddParameterWithValue("createdBy", DbType.String, false, userName);
                    command.AddParameterWithValue("originalListPrice", DbType.Int32, false, oldListPrice);
                    command.AddParameterWithValue("lastModified", DbType.DateTime, false, now);
                    command.AddParameterWithValue("Confirmed", DbType.Boolean, false, confirmed);
                    command.AddParameterWithValue("source", DbType.Int16, false, source);
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}