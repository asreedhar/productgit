using System;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search
{
    /// <summary>
    /// Enumeration of the different types of search supported by the pricing application.
    /// </summary>
    [Serializable]
    public enum SearchType
    {
        /// <summary>
        /// The search type is undefined.  This means the code was not properly initialized
        /// and the use of this value should raise an exception.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// A basic year/make/model search.
        /// </summary>
        YearMakeModel = 1,

        /// <summary>
        /// A precision search (by default your vin patterns series equivalence class within the
        /// model year and line).
        /// </summary>
        Precision = 4
    }
}