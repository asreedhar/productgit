using System;
using System.Data;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search
{
    /// <summary>
    /// The primary key of search related classes.
    /// </summary>
    [Serializable]
    public class SearchKey
    {
        private readonly string ownerHandle;
        private readonly string vehicleHandle;
        private readonly string searchHandle;

        public SearchKey(string ownerHandle, string vehicleHandle, string searchHandle)
        {
            this.ownerHandle = ownerHandle;
            this.vehicleHandle = vehicleHandle;
            this.searchHandle = searchHandle;
        }

        internal SearchKey(SearchKey key)
        {
            ownerHandle = key.ownerHandle;
            vehicleHandle = key.vehicleHandle;
            searchHandle = key.searchHandle;
        }

        public string OwnerHandle
        {
            get { return ownerHandle; }
        }

        public string VehicleHandle
        {
            get { return vehicleHandle; }
        }

        public string SearchHandle
        {
            get { return searchHandle; }
        }

        internal void AddToCommand(IDataCommand command)
        {
            command.AddParameterWithValue("OwnerHandle", DbType.String, false, OwnerHandle);
            command.AddParameterWithValue("VehicleHandle", DbType.String, false, VehicleHandle);
            command.AddParameterWithValue("SearchHandle", DbType.String, false, SearchHandle);
        }
    }
}