using System;
using System.Collections.Generic;
using FirstLook.DomainModel.Lexicon.Categorization;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search
{
    internal interface ISearchSummaryInfo
    {
        Boolean IsPrecisionTrimSearch { get;}
        String YearMakeModel { get; }
        Catalog Catalog { get; }
        List<INameable> ModelFamilies { get; }
        List<INameable> BodyTypes { get; }
        List<INameable> Series { get; }
        List<INameable> Engines { get; }
        List<INameable> Transmissions { get; }
        List<INameable> DriveTrains { get; }
        List<INameable> FuelTypes { get; }
        List<INameable> PassengerDoors { get; }
        List<INameable> Segments { get; }
    }
}