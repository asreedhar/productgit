using System;
using System.Data;
using Csla;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search
{
    [Serializable]
    public class SearchSummaryDetail : ReadOnlyBase<SearchSummaryDetail>
    {
        #region Business Methods

        private string description;
        private int numberOfListings;
        private int marketDaySupply;
        private int averageListPrice;
        private int averageVehicleMileage;
        private bool isReference;
        private bool isSearch;

        public string Description
        {
            get { return description; }
        }

        public int NumberOfListings
        {
            get { return numberOfListings; }
        }

        public int MarketDaySupply
        {
            get { return marketDaySupply; }
        }

        public int AverageListPrice
        {
            get { return averageListPrice; }
        }

        public int AverageVehicleMileage
        {
            get { return averageVehicleMileage; }
        }

        public bool IsReference
        {
            get { return isReference; }
        }

        public bool IsSearch
        {
            get { return isSearch; }
        }

        protected override object GetIdValue()
        {
            return description;
        }

        #endregion

        #region Factory Methods

        internal static SearchSummaryDetail GetSearchSummaryDetail(IDataRecord record)
        {
            return new SearchSummaryDetail(record);
        }

        private SearchSummaryDetail(IDataRecord record)
        {
            Fetch(record);
        }

        #endregion

        #region Data Access

        private void Fetch(IDataRecord record)
        {
            description = record.GetString(record.GetOrdinal("Description"));
            numberOfListings = record.GetInt32(record.GetOrdinal("NumberOfListings"));
            if (!record.IsDBNull(record.GetOrdinal("MarketDaySupply")))
                marketDaySupply = record.GetInt32(record.GetOrdinal("MarketDaySupply"));
            if (!record.IsDBNull(record.GetOrdinal("AvgListPrice")))
                averageListPrice = record.GetInt32(record.GetOrdinal("AvgListPrice"));
            if (!record.IsDBNull(record.GetOrdinal("AvgVehicleMileage")))
                averageVehicleMileage = record.GetInt32(record.GetOrdinal("AvgVehicleMileage"));
            isReference = record.GetBoolean(record.GetOrdinal("IsReference"));
            isSearch = record.GetBoolean(record.GetOrdinal("IsSearch"));
        }

        #endregion
    }
}