using System;
using System.Data;
using Csla;
using FirstLook.Common.Core;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search
{
    [Serializable]
    public class SearchSummary : ReadOnlyBase<SearchSummary>
    {
        #region Business Methods

        private SearchType searchType;
        private string description;
        private int units;
        private int comparableUnits;
        private bool isPrimarySearch;
        private bool isDefaultSearch;
        private SampleSummary<int> listPrice;
        private SampleSummary<int> mileage;
        private bool isCertificationSpecified;
        private bool isColorSpecified;
        private bool isDoorsSpecified;
        private bool isDriveTrainSpecified;
        private bool isEngineSpecified;
        private bool isFuelTypeSpecified;
        private bool isSegmentSpecified;
        private bool isBodyTypeSpecified;
        private bool isTransmissionSpecified;
        private bool isTrimSpecified;

        public string Description
        {
            get
            {
                CanReadProperty("Description", true);

                return description;
            }
        }

        public SearchType SearchType
        {
            get
            {
                CanReadProperty("SearchType", true);

                return searchType;
            }
        }

        public int ComparableUnits
        {
            get
            {
                CanReadProperty("ComparableUnits", true);

                return comparableUnits;
            }
        }

        public int Units
        {
            get
            {
                CanReadProperty("Units", true);

                return units;
            }
        }

        public bool IsPrimarySearch
        {
            get
            {
                CanReadProperty("IsPrimarySearch", true);

                return isPrimarySearch;
            }
        }

        public bool IsDefaultSearch
        {
            get
            {
                CanReadProperty("IsDefaultSearch", true);

                return isDefaultSearch;
            }
        }

        public SampleSummary<int> ListPrice
        {
            get
            {
                CanReadProperty("ListPrice", true);

                return listPrice;
            }
        }

        public SampleSummary<int> Mileage
        {
            get
            {
                CanReadProperty("Mileage", true);

                return mileage;
            }
        }

        public bool IsCertificationSpecified
        {
            get { return isCertificationSpecified; }
        }

        public bool IsColorSpecified
        {
            get { return isColorSpecified; }
        }

        public bool IsDoorsSpecified
        {
            get { return isDoorsSpecified; }
        }

        public bool IsDriveTrainSpecified
        {
            get { return isDriveTrainSpecified; }
        }

        public bool IsEngineSpecified
        {
            get { return isEngineSpecified; }
        }

        public bool IsFuelTypeSpecified
        {
            get { return isFuelTypeSpecified; }
        }

        public bool IsSegmentSpecified
        {
            get { return isSegmentSpecified; }
        }

        public bool IsBodyTypeSpecified
        {
            get { return isBodyTypeSpecified; }
        }

        public bool IsTransmissionSpecified
        {
            get { return isTransmissionSpecified; }
        }

        public bool IsTrimSpecified
        {
            get { return isTrimSpecified; }
        }

        protected override object GetIdValue()
        {
            return searchType;
        }

        #endregion

        #region Factory Methods

        private SearchSummary(IDataRecord record)
        {
            Fetch(record);
        }

        internal static SearchSummary GetSearchSummary(IDataRecord record)
        {
            return new SearchSummary(record);
        }

        #endregion

        #region Data Access

        private void Fetch(IDataRecord record)
        {
            searchType = (SearchType)Enum.ToObject(typeof(SearchType), record.GetInt32(record.GetOrdinal("SearchTypeID")));
            description = record.GetString(record.GetOrdinal("Description"));
            units = record.GetInt32(record.GetOrdinal("Units"));
            comparableUnits = record.GetInt32(record.GetOrdinal("ComparableUnits"));
            isPrimarySearch = record.GetBoolean(record.GetOrdinal("IsPrimarySearch"));
            isDefaultSearch = record.GetBoolean(record.GetOrdinal("IsDefaultSearch"));
            
            int? minListPrice, avgListPrice, maxListPrice;

            minListPrice = GetInt(record, "MinListPrice");
            avgListPrice = GetInt(record, "AvgListPrice");
            maxListPrice = GetInt(record, "MaxListPrice");

            listPrice = new SampleSummary<int>(minListPrice, avgListPrice, maxListPrice, comparableUnits);

            int? minMileage, avgMileage, maxMileage;

            minMileage = GetInt(record, "MinMileage");
            avgMileage = GetInt(record, "AvgMileage");
            maxMileage = GetInt(record, "MaxMileage");

            mileage = new SampleSummary<int>(minMileage, avgMileage, maxMileage, units);

            isCertificationSpecified = record.GetBoolean(record.GetOrdinal("IsCertificationSpecified"));
            isColorSpecified = record.GetBoolean(record.GetOrdinal("IsColorSpecified"));
            isDoorsSpecified = record.GetBoolean(record.GetOrdinal("IsDoorsSpecified"));
            isDriveTrainSpecified = record.GetBoolean(record.GetOrdinal("IsDriveTrainSpecified"));
            isEngineSpecified = record.GetBoolean(record.GetOrdinal("IsEngineSpecified"));
            isFuelTypeSpecified = record.GetBoolean(record.GetOrdinal("IsFuelTypeSpecified"));
            isSegmentSpecified = record.GetBoolean(record.GetOrdinal("IsSegmentSpecified"));
            isBodyTypeSpecified = record.GetBoolean(record.GetOrdinal("IsBodyTypeSpecified"));
            isTransmissionSpecified = record.GetBoolean(record.GetOrdinal("IsTransmissionSpecified"));
            isTrimSpecified = record.GetBoolean(record.GetOrdinal("IsTrimSpecified"));
        }

        private static int? GetInt(IDataRecord record, string columnName)
        {
            int ordinal = record.GetOrdinal(columnName);

            if (record.IsDBNull(ordinal))
                return null;

            return record.GetInt32(ordinal);
        }

        #endregion
    }
}