﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;
using FirstLook.DomainModel.Lexicon.Categorization;
using FirstLook.DomainModel.Lexicon.Categorization.Query;


namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search
{
    class SearchSummaryInfoDataAccess : ISearchSummaryInfoDataAccess
    {

        private FacetNode _facetNode = null;

        private Boolean _isPrecisionTrimSearch = false;
        public Boolean IsPrecisionTrimSearch
        {
            get { return _isPrecisionTrimSearch; }
            set { _isPrecisionTrimSearch = value; }
        }

        private String _yearMakeModel = String.Empty;
        public String YearMakeModel
        {
            get { return _yearMakeModel; }
            set { _yearMakeModel = value; }
        }

        private Catalog _catalog = null;
        public Catalog Catalog
        {
            get { return _catalog; }
            set { _catalog = value; }
        }


        public List<INameable> ModelFamilies
        {
            get
            {
                return _facetNode[typeof(ModelFamily)];
            }
        }


        public List<INameable> BodyTypes
        {
            get
            {
                return _facetNode[typeof(BodyType)];
            }
        }

        public List<INameable> Series
        {
            get
            {
                return _facetNode[typeof(Series)];
            }
        }

        public List<INameable> Engines
        {
            get
            {
                return _facetNode[typeof(Engine)];
            }
        }

        public List<INameable> Transmissions
        {
            get
            {
                return _facetNode[typeof(Transmission)];
            }
        }

        public List<INameable> DriveTrains
        {
            get
            {
                return _facetNode[typeof(DriveTrain)];
            }
        }

        public List<INameable> FuelTypes
        {
            get
            {
                return _facetNode[typeof(FuelType)];
            }
        }

        public List<INameable> PassengerDoors
        {
            get
            {
                return _facetNode[typeof(PassengerDoor)];
            }
        }

        public List<INameable> Segments
        {
            get
            {
                return _facetNode[typeof(Segment)];
            }
        }
        
        #region Implementation of ISearchSummaryDataAccess

        public SearchSummaryInfoTO Fetch(SearchSummaryInfo.Criteria criteria)
        {
            SearchSummaryInfoTO searchSummaryInfoTO = new SearchSummaryInfoTO();

            //First we get the Catalog
            searchSummaryInfoTO.Catalog = Catalog.GetCatalog(criteria.ModelYear, criteria.MakeId, criteria.LineId);

            List<Int32> modelConfigurationIdsInSearch = new List<int>(criteria.ModelConfigurations);

            List<ModelConfiguration> modelConfigurationsInSearch = new List<ModelConfiguration>();

            foreach (ModelConfiguration modelConfiguration in searchSummaryInfoTO.Catalog.ModelConfigurations)
            {
                if (modelConfigurationIdsInSearch.Contains(modelConfiguration.Id))
                {
                    modelConfigurationsInSearch.Add(modelConfiguration);
                }
            }

            _facetNode = new FacetNode(modelConfigurationsInSearch);
            
            //BUILD THE OBJECT'S FIELDS
            searchSummaryInfoTO.YearMakeModel = 
                String.Format("{0} {1} {2}", searchSummaryInfoTO.Catalog.ModelYear.Name, searchSummaryInfoTO.Catalog.Make.Name, searchSummaryInfoTO.Catalog.Line.Name);

            
            searchSummaryInfoTO.BodyTypes = BodyTypes;
            searchSummaryInfoTO.DriveTrains = DriveTrains;
            searchSummaryInfoTO.Engines = Engines;
            searchSummaryInfoTO.FuelTypes = FuelTypes;
            searchSummaryInfoTO.ModelFamilies = ModelFamilies;
            searchSummaryInfoTO.PassengerDoors = PassengerDoors;
            searchSummaryInfoTO.Segments = Segments;
            searchSummaryInfoTO.Series = Series;
            searchSummaryInfoTO.Transmissions = Transmissions;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();
                using (IDataCommand command = new DataCommand((connection.CreateCommand())))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Pricing.IsPrecisionTrim";
                    command.AddParameterWithValue("OwnerHandle", DbType.String, false, criteria.OwnerHandle);
                    command.AddParameterWithValue("SearchHandle", DbType.String, false, criteria.SearchHandle);

                    //Add the out param
                    IDataParameter isPrecisionTrimSearch = command.AddOutParameter("IsPrecisionTrimSearch", DbType.Boolean);

                    //Execute
                    command.ExecuteNonQuery();
                    searchSummaryInfoTO.IsPrecisionTrimSearch = (Boolean)isPrecisionTrimSearch.Value;
                }
            }
            return searchSummaryInfoTO;
        }
        #endregion Implementation of ISearchSummaryDataAccess
    }
}
