﻿using System;
using System.Collections.Generic;
using FirstLook.DomainModel.Lexicon.Categorization;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search
{
    [Serializable]
    public class SearchSummaryInfoTO : ISearchSummaryInfo
    {
        #region Implementation of ISearchSummaryInfo

        private bool _isPrecisionTrimSearch = false;
        private String _yearMakeModel = String.Empty;
        private Catalog _catalog = null;
        private List<INameable> _modelFamilies = null;
        private List<INameable> _bodyTypes = null;
        private List<INameable> _series = null;
        private List<INameable> _engines = null;
        private List<INameable> _transmissions = null;
        private List<INameable> _driveTrains = null;
        private List<INameable> _fuelTypes = null;
        private List<INameable> _passengerDoors = null;
        private List<INameable> _segments = null;

        public bool IsPrecisionTrimSearch
        {
            get { return _isPrecisionTrimSearch; }
            internal set { _isPrecisionTrimSearch = value; }
        }

        public string YearMakeModel
        {
            get { return _yearMakeModel; }
            internal set { _yearMakeModel = value; }
        }

        public Catalog Catalog
        {
            get { return _catalog; }
            internal set { _catalog = value; }
        }

        public List<INameable> ModelFamilies
        {
            get { return _modelFamilies; }
            internal set { _modelFamilies = value; }
        }

        public List<INameable> BodyTypes
        {
            get { return _bodyTypes; }
            internal set { _bodyTypes = value; }
        }

        public List<INameable> Series
        {
            get { return _series; }
            internal set { _series = value; }
        }

        public List<INameable> Engines
        {
            get { return _engines;}
            internal set { _engines = value; }
        }

        public List<INameable> Transmissions
        {
            get { return _transmissions; }
            internal set { _transmissions = value; }
        }

        public List<INameable> DriveTrains
        {
            get { return _driveTrains; }
            internal set { _driveTrains = value; }
        }

        public List<INameable> FuelTypes
        {
            get { return _fuelTypes; }
            internal set { _fuelTypes = value; }
        }

        public List<INameable> PassengerDoors
        {
            get { return _passengerDoors; }
            internal set { _passengerDoors = value; }
        }

        public List<INameable> Segments
        {
            get { return _segments; }
            internal set { _segments = value; }
        }

        #endregion
    }
}
