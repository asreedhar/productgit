using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search
{
    [Serializable]
    public class SearchSummaryDetailCollection : ReadOnlyListBase<SearchSummaryDetailCollection, SearchSummaryDetail>
    {
        #region Factory Methods

        public class DataSource
        {
            public SearchSummaryDetailCollection GetSearchSummaryDetails(string ownerHandle, string vehicleHandle, string searchHandle, int searchTypeId)
            {
                return GetSearchSummaryDetailCollection(new Criteria(ownerHandle, vehicleHandle, searchHandle, searchTypeId));
            }
        }

        internal static SearchSummaryDetailCollection GetSearchSummaryDetailCollection(Criteria key)
        {
            return DataPortal.Fetch<SearchSummaryDetailCollection>(key);
        }

        private SearchSummaryDetailCollection()
        {
            /* force use of factory methods */
        }

        #endregion

        #region Data Access

        [Serializable]
        internal class Criteria : SearchKey
        {
            private readonly int searchTypeId;

            public Criteria(string ownerHandle, string vehicleHandle, string searchHandle, int searchTypeId) : base(ownerHandle, vehicleHandle, searchHandle)
            {
                this.searchTypeId = searchTypeId;
            }

            public int SearchTypeId
            {
                get { return searchTypeId; }
            }
        }

        private void DataPortal_Fetch(Criteria key)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Pricing.SearchSummaryDetailCollection#Fetch";

                    key.AddToCommand(command);

                    command.AddParameterWithValue("SearchTypeID", DbType.Int32, false, key.SearchTypeId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        IsReadOnly = false;

                        while (reader.Read())
                        {
                            Add(SearchSummaryDetail.GetSearchSummaryDetail(reader));
                        }

                        IsReadOnly = true;
                    }
                }
            }
        }

        #endregion
    }
}