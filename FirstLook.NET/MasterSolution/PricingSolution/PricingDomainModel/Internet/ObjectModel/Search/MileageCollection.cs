using System;
using System.Data;
using System.Xml;
using System.Xml.Serialization;
using Csla;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search
{
    [Serializable]
    public class MileageCollection : BusinessListBase<MileageCollection, Mileage>, IXmlSerializable
    {
        #region Business Methods
        
        public Mileage GetItem(int mileage)
        {
            foreach (Mileage item in this)
                if (item.Value == mileage)
                    return item;
            return null;
        }

        #endregion

        #region Factory Methods
        
        internal static MileageCollection GetMileageCollection(IDataReader reader)
        {
            return new MileageCollection(reader);
        }

        internal static MileageCollection GetMileageCollection(XmlReader reader)
        {
            return new MileageCollection(reader);
        }

        private MileageCollection(IDataReader reader)
        {
            MarkAsChild();
            Fetch(reader);
        }

        private MileageCollection(XmlReader reader)
        {
            MarkAsChild();
            ReadXml(reader);
        }

        #endregion

        #region Data Access
        
        private void Fetch(IDataReader reader)
        {
            RaiseListChangedEvents = false;
            while (reader.Read())
                Add(Mileage.GetMileage(reader));
            RaiseListChangedEvents = true;
        } 
        
        #endregion

        #region IXmlSerializable Members

        internal const string elementName = "MileageCollection";

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            RaiseListChangedEvents = false;
            if (reader.MoveToContent() == XmlNodeType.Element && reader.Name == elementName)
            {
                bool isEmptyElement = reader.IsEmptyElement;
                reader.ReadStartElement(elementName);
                while (Mileage.CanReadXml(reader))
                    Add(Mileage.GetMileage(reader));
                if (!isEmptyElement)
                    reader.ReadEndElement();
            }
            RaiseListChangedEvents = true;
        }

        public void WriteXml(XmlWriter writer)
        {
            if (Count > 0)
            {
                writer.WriteStartElement(elementName);
                foreach (Mileage item in this)
                    item.WriteXml(writer);
                writer.WriteEndElement();
            }
        }

        #endregion
    }
}