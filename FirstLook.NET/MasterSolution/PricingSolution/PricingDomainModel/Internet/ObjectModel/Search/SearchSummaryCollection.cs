using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search
{
    [Serializable]
    public class SearchSummaryCollection : ReadOnlyListBase<SearchSummaryCollection, SearchSummary>
    {
        #region Business Methods

        public SearchSummary YearMakeModelSearchSummary
        {
            get
            {
                foreach (SearchSummary item in this)
                    if (item.SearchType == SearchType.YearMakeModel)
                        return item;
                return null;
            }
        }

        public SearchSummary PrecisionSearchSummary
        {
            get
            {
                foreach (SearchSummary item in this)
                    if (item.SearchType == SearchType.Precision)
                        return item;
                return null;
            }
        }

        public SearchSummary ActiveSearchSummary
        {
            get
            {
                foreach (SearchSummary item in this)
                    if (item.IsPrimarySearch)
                        return item;
                return null;
            }
        }

        #endregion

        #region Factory Methods

        public static SearchSummaryCollection GetSearchSummaryCollection(SearchKey key)
        {
            return DataPortal.Fetch<SearchSummaryCollection>(key);
        }

        private SearchSummaryCollection()
        {
            /* force use of factory methods */
        }

        #endregion

        #region Data Access

        private void DataPortal_Fetch(SearchKey key)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Pricing.SearchSummaryCollection#Fetch";

                    key.AddToCommand(command);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        IsReadOnly = false;

                        while (reader.Read())
                        {
                            Add(SearchSummary.GetSearchSummary(reader));
                        }

                        IsReadOnly = true;
                    }
                }
            }
        }

        #endregion
    }
}
