using System;
using System.Data;
using System.Globalization;
using System.Xml;
using System.Xml.Serialization;
using Csla;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search
{
    [Serializable]
    public class Distance : BusinessBase<Distance>, IXmlSerializable
    {
        #region Business Methods
        
        private int id;
        private int value;

        public int Id
        {
            get { return id; }
        }

        public int Value
        {
            get { return value; }
        }

        protected override object GetIdValue()
        {
            return id;
        }

        #endregion

        #region Factory Methods

        internal static Distance GetDistance(IDataRecord record)
        {
            return new Distance(record);
        }

        internal static Distance GetDistance(XmlReader reader)
        {
            return new Distance(reader);
        }

        private Distance(IDataRecord record)
        {
            MarkAsChild();
            Fetch(record);
        }

        private Distance(XmlReader reader)
        {
            MarkAsChild();
            ReadXml(reader);
        }

        #endregion

        #region Data Access
        
        private void Fetch(IDataRecord record)
        {
            id = record.GetByte(record.GetOrdinal("DistanceBucketId"));
            value = record.GetInt32(record.GetOrdinal("Distance"));
        }

        #endregion

        #region IXmlSerializable Members

        private const string elementName = "Distance";

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public static bool CanReadXml(XmlReader reader)
        {
            return (reader.MoveToContent() == XmlNodeType.Element && reader.Name == elementName);
        }

        public void ReadXml(XmlReader reader)
        {
            if (CanReadXml(reader))
            {
                if (reader.HasAttributes && reader.MoveToAttribute("Id"))
                {
                    id = Convert.ToInt32(reader.Value, CultureInfo.InvariantCulture);
                    reader.MoveToElement();
                }
                reader.ReadStartElement(elementName);
                value = Convert.ToInt32(reader.ReadString(), CultureInfo.InvariantCulture);
                reader.ReadEndElement();
            }
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(elementName);
            writer.WriteAttributeString("Id", Convert.ToString(Id, CultureInfo.InvariantCulture));
            writer.WriteString(Convert.ToString(Value, CultureInfo.InvariantCulture));
            writer.WriteEndElement();
        }

        #endregion
    }
}