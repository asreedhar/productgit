using System;
using System.Data;
using System.Xml;
using System.Xml.Serialization;
using Csla;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search
{
    [Serializable]
    public class DistanceCollection : BusinessListBase<DistanceCollection, Distance>, IXmlSerializable
    {
        #region Business Methods

        public Distance GetItem(int distanceId)
        {
            foreach (Distance item in this)
                if (item.Id == distanceId)
                    return item;
            return null;
        }

        #endregion

        #region Factory Methods

        internal static DistanceCollection GetDistanceCollection(IDataReader reader)
        {
            return new DistanceCollection(reader);
        }

        internal static DistanceCollection GetDistanceCollection(XmlReader reader)
        {
            return new DistanceCollection(reader);
        }

        private DistanceCollection(IDataReader reader)
        {
            MarkAsChild();
            Fetch(reader);
        }

        private DistanceCollection(XmlReader reader)
        {
            MarkAsChild();
            ReadXml(reader);
        }

        #endregion

        #region Data Access
        
        private void Fetch(IDataReader reader)
        {
            RaiseListChangedEvents = false;
            while (reader.Read())
                Add(Distance.GetDistance(reader));
            RaiseListChangedEvents = true;
        } 
        
        #endregion

        #region IXmlSerializable Members

        internal const string elementName = "DistanceCollection";

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            RaiseListChangedEvents = false;
            if (reader.MoveToContent() == XmlNodeType.Element && reader.Name == elementName)
            {
                bool isEmptyElement = reader.IsEmptyElement;
                reader.ReadStartElement(elementName);
                while (Distance.CanReadXml(reader))
                    Add(Distance.GetDistance(reader));
                if (!isEmptyElement)
                    reader.ReadEndElement();
            }
            RaiseListChangedEvents = true;
        }

        public void WriteXml(XmlWriter writer)
        {
            if (Count > 0)
            {
                writer.WriteStartElement(elementName);
                foreach (Distance item in this)
                    item.WriteXml(writer);
                writer.WriteEndElement();
            }
        }

        #endregion
    }
}