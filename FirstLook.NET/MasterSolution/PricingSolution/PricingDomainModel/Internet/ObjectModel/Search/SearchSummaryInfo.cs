using System;
using System.Collections.Generic;
using Csla;
using FirstLook.Common.Core.Utilities;
using FirstLook.DomainModel.Lexicon.Categorization;
using FirstLook.DomainModel.Lexicon;


namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search
{
    [Serializable]
    public class SearchSummaryInfo : InjectableReadOnlyBase<SearchSummaryInfo>, ISearchSummaryInfo
    {
        private SearchSummaryInfoTO _searchSummaryInfoTo;

        #region Injected dependencies

        public ISearchSummaryInfoDataAccess InfoDataAccess { get; set;}

        #endregion

        #region Business Methods
        // TODO: add your own fields, properties and methods
        private Criteria _id;

        public Criteria Id
        {
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
            get
            {
                CanReadProperty("Id", true);
                return _id;
            }
        }

        protected override object GetIdValue()
        {
            return _id;
        }
        

        public Boolean IsPrecisionTrimSearch
        {
            get { return _searchSummaryInfoTo.IsPrecisionTrimSearch; }
        }

        

        public String YearMakeModel
        {
            get { return _searchSummaryInfoTo.YearMakeModel; }
        }
        
        public Catalog Catalog
        {
            get { return _searchSummaryInfoTo.Catalog; }
        }


        public List<INameable> ModelFamilies
        {
            get{return _searchSummaryInfoTo.ModelFamilies;}
        }


        public List<INameable> BodyTypes
        {
            get{return _searchSummaryInfoTo.BodyTypes;}
        }


        public List<INameable> Series
        {
            get{return _searchSummaryInfoTo.Series;}
        }


        public List<INameable> Engines
        {
            get{return _searchSummaryInfoTo.Engines;}
        }


        public List<INameable> Transmissions
        {
            get{return _searchSummaryInfoTo.Transmissions;}
        }



        public List<INameable> DriveTrains
        {
            get{return _searchSummaryInfoTo.DriveTrains;}
        }

        public List<INameable> FuelTypes
        {
            get{return _searchSummaryInfoTo.FuelTypes;}
        }



        public List<INameable> PassengerDoors
        {
            get{return _searchSummaryInfoTo.PassengerDoors;}
        }


        public List<INameable> Segments
        {
            get{return _searchSummaryInfoTo.Segments;}
        }


        //private FacetNode _facetNode = null;
        #endregion

        #region Authorization Rules

        protected override void AddAuthorizationRules()
        {
            // TODO: add authorization rules
            //AuthorizationRules.AllowRead("", "");
        }

        public static bool CanGetObject()
        {
            // TODO: customize to check user role
            //return ApplicationContext.User.IsInRole("");
            return true;
        }

        #endregion

        #region Factory Methods

        public static SearchSummaryInfo GetSearchSummaryInfo(String ownerHandle, String vehicleHandle, String searchHandle, Search search)
        {
            List<Int32> catalogEntries = new List<Int32>();
            foreach (CatalogEntry catalogEntry in search.CatalogEntries)
            {
                catalogEntries.Add(catalogEntry.Id);
            }

            return DataPortal.Fetch<SearchSummaryInfo>(new Criteria(search.ModelYear, search.MakeId, search.LineId, ownerHandle, searchHandle,catalogEntries.ToArray()));
        }

        private SearchSummaryInfo()
        { /* require use of factory methods */ }

        #endregion

        #region Data Access

        [Serializable]
        public class Criteria : IEquatable<Criteria>, IExplicitlySerializable
        {
            private readonly int _modelYear;
            private readonly int _makeId;
            private readonly int _lineId;
            private readonly String _ownerHandle = String.Empty;
            private readonly String _searchHandle = String.Empty;
            private readonly Int32[] _modelConfigurations;

            public Criteria(int modelYear, int makeId, int lineId, String ownerHandle, String searchHandle, Int32[] modelConfigurations)
            {
                _modelYear = modelYear;
                _makeId = makeId;
                _lineId = lineId;
                _ownerHandle = ownerHandle;
                _searchHandle = searchHandle;
                _modelConfigurations = modelConfigurations;
            }

            public int ModelYear
            {
                get { return _modelYear; }
            }

            public int MakeId
            {
                get { return _makeId; }
            }

            public int LineId
            {
                get { return _lineId; }
            }

            public string OwnerHandle
            {
                get { return _ownerHandle; }
            }

            public string SearchHandle
            {
                get { return _searchHandle; }
            }
            
            public Int32[] ModelConfigurations
            {
                get { return _modelConfigurations; }
            }

            public bool Equals(Criteria criteria)
            {
                if (criteria == null) return false;
                if (_modelYear != criteria._modelYear) return false;
                if (_makeId != criteria._makeId) return false;
                if (_lineId != criteria._lineId) return false;
                if (_ownerHandle != criteria.OwnerHandle) return false;
                if (_searchHandle != criteria.SearchHandle) return false;
                if (_modelConfigurations != criteria.ModelConfigurations) return false;

                return true;
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(this, obj)) return true;
                return Equals(obj as Criteria);
            }

            public override int GetHashCode()
            {
                int result = _modelYear;
                result = 29 * result + _makeId;
                result = 29 * result + _lineId;
                result = 29 * result + _ownerHandle.GetHashCode();
                result = 29 * result + _searchHandle.GetHashCode();
                return result;
            }

            #region IExplicitlySerializable Members

            public void GetObjectData(SerializationData data)
            {
                data.AddValue("ModelYear", _modelYear);
                data.AddValue("MakeId", _makeId);
                data.AddValue("LineId", _lineId);
                data.AddValue("OwnerHandle", _ownerHandle);
                data.AddValue("SearchHandle", _searchHandle);
                List<String> modelConfigurations = new List<string>();
                foreach (Int32 i in _modelConfigurations)
                {
                    modelConfigurations.Add(i.ToString());
                }
                data.AddValue("ModelConfiguration", String.Join(",", modelConfigurations.ToArray()));

            }

            internal Criteria(SerializationData data)
            {
                _modelYear = data.GetInt32("ModelYear");
                _makeId = data.GetInt32("MakeId");
                _lineId = data.GetInt32("LineId");
                _ownerHandle = data.GetString("OwnerHandle");
                _searchHandle = data.GetString("SearchHandle");
                string[] modelConfigurations = data.GetString("ModelConfiguration").Split(',');
                List<Int32> list = new List<int>();
                foreach (String s in modelConfigurations)
                {
                    list.Add(Int32Helper.ToInt32(s));
                }
                _modelConfigurations = list.ToArray();
            }

            #endregion
        }

        private void DataPortal_Fetch(Criteria criteria)
        {
            //Identify
            _id = criteria;
            _searchSummaryInfoTo = InfoDataAccess.Fetch(criteria);
        }

        #endregion
    }
}