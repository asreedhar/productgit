using System;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search
{
    /// <summary>
    /// Event argument used to specify a search type (typically the current search type).
    /// </summary>
    [Serializable]
    public class SearchTypeEventArgs : EventArgs
    {
        public static readonly SearchTypeEventArgs YearMakeModel = new SearchTypeEventArgs(SearchType.YearMakeModel);
        public static readonly SearchTypeEventArgs Precision = new SearchTypeEventArgs(SearchType.Precision);

        private readonly SearchType _searchType;

        public SearchTypeEventArgs(SearchType searchType)
        {
            _searchType = searchType;
        }

        public SearchType SearchType
        {
            get { return _searchType; }
        }
    }
}