namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search
{
    public interface ISearchSummaryInfoDataAccess
    {
        SearchSummaryInfoTO Fetch(SearchSummaryInfo.Criteria criteria);
    }
}
