using System;
using System.Data;
using Csla;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search
{
    [Serializable]
    public class CatalogEntryCollection : BusinessListBase<CatalogEntryCollection, CatalogEntry>
    {
        #region Business Methods

        public CatalogEntry GetItem(int catalogEntryId)
        {
            foreach (CatalogEntry connection in this)
                if (connection.Id == catalogEntryId)
                    return connection;
            return null;
        }

        public void Assign(int catalogEntryId)
        {
            if (!Contains(catalogEntryId))
            {
                Add(CatalogEntry.NewCatalogEntry(catalogEntryId));
            }
            else
            {
                throw new InvalidOperationException("CatalogEntry already assigned to collection");
            }
        }

        public void Remove(int catalogEntryId)
        {
            foreach (CatalogEntry catalogEntry in this)
            {
                if (catalogEntry.Id == catalogEntryId)
                {
                    Remove(catalogEntry);
                    break;
                }
            }
        }

        public bool Contains(int catalogEntryId)
        {
            foreach (CatalogEntry catalogEntry in this)
                if (catalogEntry.Id == catalogEntryId)
                    return true;
            return false;
        }

        public bool ContainsDeleted(int catalogEntryId)
        {
            foreach (CatalogEntry catalogEntry in DeletedList)
                if (catalogEntry.Id == catalogEntryId)
                    return true;
            return false;
        }

        #endregion

        #region Factory Methods

        internal static CatalogEntryCollection NewCatalogEntryCollection()
        {
            return new CatalogEntryCollection();
        }

        internal static CatalogEntryCollection GetCatalogEntryCollection(IDataReader reader)
        {
            return new CatalogEntryCollection(reader);
        }

        private CatalogEntryCollection()
        {
            MarkAsChild();
        }

        private CatalogEntryCollection(IDataReader reader) : this()
        {
            Fetch(reader);
        }

        #endregion

        #region Data Access

        private void Fetch(IDataReader reader)
        {
            RaiseListChangedEvents = false;
            while (reader.Read())
            {
                Add(CatalogEntry.GetCatalogEntry(reader));
            }
            RaiseListChangedEvents = true;
        }

        internal void Update(IDbConnection connection, IDbTransaction transaction, Search search)
        {
            RaiseListChangedEvents = false;
            // update (thus deleting) any deleted child objects
            foreach (CatalogEntry entry in DeletedList)
                entry.DeleteSelf(connection, transaction, search);
            // now that they are deleted, remove them from memory too
            DeletedList.Clear();
            // add/update any current child objects
            foreach (CatalogEntry entry in this)
            {
                if (entry.IsNew)
                {
                    entry.Insert(connection, transaction, search);
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion 
    }
}