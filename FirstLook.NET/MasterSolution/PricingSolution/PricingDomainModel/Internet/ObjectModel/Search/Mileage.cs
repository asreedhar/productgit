using System;
using System.Data;
using System.Globalization;
using System.Xml;
using System.Xml.Serialization;
using Csla;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search
{
    [Serializable]
    public class Mileage : BusinessBase<Mileage>, IXmlSerializable
    {
        #region Business Methods

        private int value;

        public int Value
        {
            get { return value; }
        }

        protected override object GetIdValue()
        {
            return value;
        }

        #endregion

        #region Factory Methods

        internal static Mileage GetMileage(IDataRecord record)
        {
            return new Mileage(record);
        }

        internal static Mileage GetMileage(XmlReader reader)
        {
            return new Mileage(reader);
        }

        public static Mileage NewMileage(int value)
        {
            Mileage mileage = DataPortal.Create<Mileage>();
            mileage.value = value;
            return mileage;
        }

        private Mileage(IDataRecord record)
        {
            MarkAsChild();
            Fetch(record);
        }

        private Mileage(XmlReader reader)
        {
            MarkAsChild();
            ReadXml(reader);
        }

        private Mileage()
        {
            /* Force use of factory methods */
        }

        #endregion

        #region Data Access

        [RunLocal]
        protected override void DataPortal_Create()
        {
            /* no fields to initialize */
        }

        private void Fetch(IDataRecord record)
        {
            value = record.GetInt32(record.GetOrdinal("Mileage"));
        }

        #endregion

        #region IXmlSerializable Members

        private const string elementName = "Mileage";

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public static bool CanReadXml(XmlReader reader)
        {
            return (reader.MoveToContent() == XmlNodeType.Element && reader.Name == elementName);
        }

        public void ReadXml(XmlReader reader)
        {
            if (CanReadXml(reader))
            {
                reader.ReadStartElement(elementName);
                value = Convert.ToInt32(reader.ReadString(), CultureInfo.InvariantCulture);
                reader.ReadEndElement();
            }
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(elementName);
            writer.WriteString(Convert.ToString(Value, CultureInfo.InvariantCulture));
            writer.WriteEndElement();
        }

        #endregion
    }
}