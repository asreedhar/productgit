using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search
{
    [Serializable]
    public class CatalogEntry : BusinessBase<CatalogEntry>
    {
        private int _id;

        public int Id
        {
            get
            {
                CanReadProperty("Id", true);

                return _id;
            }
        }

        protected override object GetIdValue()
        {
            return _id;
        }

        public static CatalogEntry GetCatalogEntry(IDataRecord record)
        {
            return new CatalogEntry(record);
        }

        public static CatalogEntry NewCatalogEntry(int id)
        {
            return new CatalogEntry(id);
        }

        private CatalogEntry()
        {
            MarkAsChild();
        }

        private CatalogEntry(IDataRecord record) : this()
        {
            Fetch(record);
        }

        private CatalogEntry(int id)
            : this()
        {
            Fetch(id);
        }

        private void Fetch(int id)
        {
            MarkNew();

            _id = id;
        }

        private void Fetch(IDataRecord record)
        {
            MarkOld();

            _id = record.GetInt32(record.GetOrdinal("Id"));
        }

        internal void Insert(IDbConnection connection, IDbTransaction transaction, Search search)
        {
            if (!IsDirty) return;

            DoAssignmentCommand(connection, "Pricing.CatalogEntry#AddAssignment", transaction, search);

            MarkOld();
        }

        internal void DeleteSelf(IDbConnection connection, IDbTransaction transaction, Search search)
        {
            if (!IsDirty) return;

            if (IsNew) return;

            DoAssignmentCommand(connection, "Pricing.CatalogEntry#DeleteAssignment", transaction, search);

            MarkNew();
        }

        private void DoAssignmentCommand(IDbConnection connection, string commandText, IDbTransaction transaction, Search search)
        {
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = commandText;
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;
                search.Id.AddToCommand(command);
                command.AddParameterWithValue("ModelConfigurationId", DbType.Int32, false, Id);
                command.ExecuteNonQuery();
            }
        }
    }
}