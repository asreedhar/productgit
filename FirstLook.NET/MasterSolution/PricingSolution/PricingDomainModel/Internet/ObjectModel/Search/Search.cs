using System;
using System.Collections.ObjectModel;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search
{
    [Serializable]
    public class Search : BusinessBase<Search>
    {
        #region Business Methods

        private SearchKey _id;

        private int _modelYear, _makeId, _lineId, _modelConfigurationId;
        private string _vinPattern;
        private Distance _distance;
        private Mileage _lowMileage;
        private Mileage _highMileage;
        private bool _matchColor;
        private bool _matchCertified;
        private string _updateUser;
        private DateTime _updateDate;
        //private bool? _previousYear;
        //private bool? _nextYear;

        private CatalogEntryCollection _catalogEntries;
        private ReadOnlyCollection<Distance> _distances;
        private ReadOnlyCollection<Mileage> _mileages;

        internal SearchKey Id
        {
            get { return _id; }
        }

        public int ModelYear
        {
            get { return _modelYear; }
        }

        public int MakeId
        {
            get { return _makeId; }
        }

        public int LineId
        {
            get { return _lineId; }
        }

        public int ModelConfigurationId
        {
            get { return _modelConfigurationId; }
        }

        public string VinPattern
        {
            get { return _vinPattern; }
        }

        public Distance Distance
        {
            get
            {
                return _distance;
            }
            set
            {
                if (!Equals(_distance, value))
                {
                    _distance = value;
                    PropertyHasChanged("Distance");
                }
            }
        }

        public Mileage LowMileage
        {
            get
            {
                return _lowMileage;
            }
            set
            {
                if (!Equals(_lowMileage, value))
                {
                    _lowMileage = value;

                    PropertyHasChanged("LowMileage");
                }
            }
        }

        public Mileage HighMileage
        {
            get
            {
                return _highMileage;
            }
            set
            {
                if (!Equals(_highMileage, value))
                {
                    _highMileage = value;

                    PropertyHasChanged("HighMileage");
                }
            }
        }

        public bool MatchColor
        {
            get
            {
                return _matchColor;
            }
            set
            {
                if (!Equals(_matchColor, value))
                {
                    _matchColor = value;

                    PropertyHasChanged("MatchColor");
                }
            }
        }

        public bool MatchCertified
        {
            get
            {
                return _matchCertified;
            }
            set
            {
                if (!Equals(_matchCertified, value))
                {
                    _matchCertified = value;

                    PropertyHasChanged("MatchCertified");
                }
            }
        }

        public string UpdateUser
        {
            get { return _updateUser; }
            set { _updateUser = value; } // does not mark the class dirty; only a change to search parameters will do that
        }

        public DateTime UpdateDate
        {
            get { return _updateDate; }
        }

        //public bool? PreviousYear
        //{
        //    get { return _previousYear; }
        //    set {
        //              if (!Equals(_previousYear, value))
        //               {
        //               _previousYear = value;

        //               PropertyHasChanged("PreviousYearConfigurationId");
        //               }
        //        }
        //}

        //public bool? NextYear
        //{
        //    get { return _nextYear; }
        //    set { 

        //            if (!Equals(_nextYear, value))
        //            {
        //                _nextYear = value;

        //                PropertyHasChanged("NextYearConfigurationId");
        //            }
        //        }
        //}

        public CatalogEntryCollection CatalogEntries
        {
            get { return _catalogEntries; }
        }

        public ReadOnlyCollection<Distance> Distances
        {
            get { return _distances; }
        }

        public ReadOnlyCollection<Mileage> Mileages
        {
            get { return _mileages; }
        }

        protected override object GetIdValue()
        {
            return _id;
        }

        #endregion

        #region Validation Rules

        public override bool IsDirty
        {
            get
            {
                return base.IsDirty || CatalogEntries.IsDirty;
            }
        }

        #endregion

        #region Factory Methods

        public static Search GetSearch(SearchKey key)
        {
            return DataPortal.Fetch<Search>(key);
        }

        public class DataSource
        {
            public Search Select(string ownerHandle, string vehicleHandle, string searchHandle)
            {
                SearchKey key = new SearchKey(ownerHandle, vehicleHandle, searchHandle);

                return GetSearch(key);
            }
        }

        #endregion

        #region Data Access

        private void DataPortal_Fetch(SearchKey key)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandText = "Pricing.Search#Fetch";
                    command.CommandType = CommandType.StoredProcedure;

                    key.AddToCommand(command);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        Fetch(key, reader);
                    }
                }
            }
        }

        private void Fetch(SearchKey key, IDataReader reader)
        {
            if (reader.Read())
            {
                _id = key;

                _distance = Distance.GetDistance(reader);

                _lowMileage = Mileage.NewMileage(reader.GetInt32(reader.GetOrdinal("LowMileage")));

                if (!reader.IsDBNull(reader.GetOrdinal("HighMileage")))
                {
                    _highMileage = Mileage.NewMileage(reader.GetInt32(reader.GetOrdinal("HighMileage")));
                }

                _matchCertified = reader.GetBoolean(reader.GetOrdinal("MatchCertified"));

                _matchColor = reader.GetBoolean(reader.GetOrdinal("MatchColor"));

                _modelYear = reader.GetInt32(reader.GetOrdinal("ModelYear"));

                _makeId = reader.GetInt32(reader.GetOrdinal("MakeId"));

                _lineId = reader.GetInt32(reader.GetOrdinal("LineId"));

                _modelConfigurationId = reader.GetInt32(reader.GetOrdinal("ModelConfigurationId"));

                _vinPattern = reader.GetString(reader.GetOrdinal("VinPattern"));

                if (!reader.IsDBNull(reader.GetOrdinal("UpdateUser")))
                    _updateUser = reader.GetString(reader.GetOrdinal("UpdateUser"));

                if (!reader.IsDBNull(reader.GetOrdinal("UpdateDate")))
                    _updateDate = reader.GetDateTime(reader.GetOrdinal("UpdateDate"));

                    //_previousYear = Convert.ToBoolean(reader.GetInt32(reader.GetOrdinal("PreviousYearConfigurationId")));

                    //_nextYear =Convert.ToBoolean( reader.GetInt32(reader.GetOrdinal("NextYearConfigurationID")));

            }
            else
            {
                throw new DataException("Missing first result set");
            }

            if (reader.NextResult())
            {
                _catalogEntries = CatalogEntryCollection.GetCatalogEntryCollection(reader);
            }
            else
            {
                throw new DataException("Missing second result set");
            }

            if (reader.NextResult())
            {
                _distances = new ReadOnlyCollection<Distance>(DistanceCollection.GetDistanceCollection(reader));
            }
            else
            {
                throw new DataException("Missing second result set");
            }

            if (reader.NextResult())
            {
                _mileages = new ReadOnlyCollection<Mileage>(MileageCollection.GetMileageCollection(reader));
            }
            else
            {
                throw new DataException("Missing third result set");
            }
        }

        [Transactional(TransactionalTypes.Manual)]
        protected override void DataPortal_Update()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                        {
                            command.CommandText = "Pricing.Search#Update";
                            command.CommandType = CommandType.StoredProcedure;
                            command.Transaction = transaction;

                            _id.AddToCommand(command);

                            command.AddParameterWithValue("DistanceBucketId", DbType.Byte, false, Distance.Id);

                            command.AddParameterWithValue("LowMileage", DbType.Int32, false, LowMileage.Value);

                            if (HighMileage == null)
                            {
                                command.AddParameterWithValue("HighMileage", DbType.Int32, false, DBNull.Value);
                              
                            }
                            else
                            {
                                command.AddParameterWithValue("HighMileage", DbType.Int32, false, HighMileage.Value);
                            }

                            command.AddParameterWithValue("MatchCertified", DbType.Boolean, false, MatchCertified);

                            command.AddParameterWithValue("MatchColor", DbType.Boolean, false, MatchColor);

                            command.AddParameterWithValue("ModifiedCatalogEntries", DbType.Boolean, false, _catalogEntries.IsDirty);

                            command.AddParameterWithValue("UpdateUser", DbType.String, false, UpdateUser);

                            //command.AddParameterWithValue("PreviousYearFlag", DbType.Boolean, false, PreviousYear);

                            //command.AddParameterWithValue("NextYearFlag", DbType.Boolean, false, NextYear);

                            command.ExecuteNonQuery();
                        }

                        _catalogEntries.Update(connection, transaction, this);

                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();

                        throw;
                    }

                    DataPortal.Execute(new RunSearchCommand(Id));
                }
            }
        }

        //public string FindPrevNxtYearsConfigIds(string currentYearConfigIds, string searchhandle)             //31079
        //{
        //    string configIds = null;
        //    using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
        //    {
        //        if (connection.State == ConnectionState.Closed)
        //            connection.Open();

        //        using (IDataCommand command = new DataCommand(connection.CreateCommand()))
        //        {
        //            command.CommandText = "Pricing.PrevNextYearModelConfigIds#Fetch";
        //            command.CommandType = CommandType.StoredProcedure;

        //            command.AddParameterWithValue("CurrentConfigIds", DbType.String, false, currentYearConfigIds);
        //            command.AddParameterWithValue("SearchHandle", DbType.String, false, searchhandle);

        //            using (IDataReader reader = command.ExecuteReader())
        //            {
        //                configIds = Fetch(reader);
        //            }
        //            if (configIds != null)
        //            {
        //                configIds=configIds.TrimEnd(',');
        //            }
        //        }
        //    }
        //    return configIds;
        //}

        //private string Fetch(IDataReader reader)
        //{
        //    string configurationIds = null;
        //    while(reader.Read())
        //    {
        //        configurationIds = configurationIds + reader.GetInt32(reader.GetOrdinal("Id")) + ",";
        //    }
        //    return configurationIds;
        //}


        #endregion

        #region Search Commands

        [Serializable]
        private abstract class ModifySearchCommand : CommandBase
        {
            private readonly SearchKey key;

            public ModifySearchCommand(SearchKey key)
            {
                this.key = key;
            }

            public ModifySearchCommand(string ownerHandle, string searchHandle, string vehicleHandle)
            {
                this.key = new SearchKey(ownerHandle, vehicleHandle, searchHandle);
            }

            protected override void DataPortal_Execute()
            {
                using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.CommandText = CommandText;
                        command.CommandType = CommandType.StoredProcedure;
                        SetCommandParameters(command);
                        command.ExecuteNonQuery();
                    }
                }
            }

            protected abstract string CommandText { get; }

            protected virtual void SetCommandParameters(IDataCommand command)
            {
                key.AddToCommand(command);
            }
        }

        [Serializable]
        private class RestorePrecisionSearchCommand : ModifySearchCommand
        {
            public RestorePrecisionSearchCommand(SearchKey key)
                : base(key)
            {
            }

            protected override string CommandText
            {
                get { return "Pricing.Search#RestorePrecisionSearch"; }
            }
        }

        [Serializable]
        private class RunSearchCommand : ModifySearchCommand
        {
            public RunSearchCommand(SearchKey key)
                : base(key)
            {
            }

            protected override string CommandText
            {
                get { return "Pricing.Search#Execute"; }
            }
        }

        [Serializable]
        private class SetSearchTypeCommand : ModifySearchCommand
        {
            private readonly int searchTypeId;

            public SetSearchTypeCommand(SearchKey key, int searchTypeId)
                : base(key)
            {
                this.searchTypeId = searchTypeId;
            }

            public SetSearchTypeCommand(string ownerHandle, string searchHandle, string vehicleHandle, int searchTypeId)
                : base(ownerHandle, searchHandle, vehicleHandle)
            {
                this.searchTypeId = searchTypeId;
            }

            protected override string CommandText
            {
                get { return "Pricing.Search#SetSearchTypeID"; }
            }

            protected override void SetCommandParameters(IDataCommand command)
            {
                base.SetCommandParameters(command);

                command.AddParameterWithValue("SearchTypeID", DbType.Int32, false, searchTypeId);
            }
        }

        public static void SetSearchTypeId(string oh, string sh, string vh, int searchTypeId)
        {
            DataPortal.Execute(new SetSearchTypeCommand(oh, sh, vh, searchTypeId));
        }

        public static void RestorePrecisionSearch(SearchKey key)
        {
            DataPortal.Execute(new RestorePrecisionSearchCommand(key));
        }

        public static void SetSearchTypeId(SearchKey key, int searchTypeId)
        {
            DataPortal.Execute(new SetSearchTypeCommand(key, searchTypeId));
        }

        #endregion
    }
}