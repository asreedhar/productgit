using System;
using System.Data;
using System.Web;
using FirstLook.Common.Core.Utilities;
using FirstLook.DomainModel.Oltp;
using FirstLook.Pricing.DomainModel.Internet.DataSource;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel
{
    public static class Identity
    {
        #region Identity Checkers

        /// <summary>
        /// Does a given owner handle describe a dealer?
        /// </summary>
        /// <param name="ownerHandle">The owner handle to check.</param>
        /// <returns>True if the handle describes a dealer, false otherwise.</returns>
        public static bool IsDealer(string ownerHandle)
        {
            bool result = false;

            if (!string.IsNullOrEmpty(ownerHandle))
            {
                DataTable table = GetOwnerTable(ownerHandle);
                foreach (DataRow row in table.Rows)
                {
                    result = Int32Helper.ToInt32(row["OwnerType"]) == 1;
                }
            }
            return result;
        }

        /// <summary>
        /// Is the current identity that of a user?
        /// </summary>
        /// <returns>True if user.</returns>
        public static bool IsUser()
        {
            Member member = HttpContext.Current.Items[MemberFacade.HttpContextKey] as Member;
            if (member != null)
            {
                return member.MemberType == MemberType.User;
            }
            return false;
        }

        #endregion

        #region Misc. Getters

        /// <summary>
        /// Get the dealer id for a given owner handle.
        /// </summary>
        /// <param name="ownerHandle">Handle to the owner to find the dealer id for.</param>
        /// <returns>Dealer id if the owner handle describes a dealer, -1 otherwise.</returns>
        public static int GetDealerId(string ownerHandle)
        {
            if (!string.IsNullOrEmpty(ownerHandle))
            {
                DataTable table = GetOwnerTable(ownerHandle);
                foreach (DataRow row in table.Rows)
                {
                    return Int32Helper.ToInt32(row["OwnerKey"]);
                }
            }
            return -1;
        }

        /// <summary>
        /// Get owner data for a handle.
        /// </summary>
        /// <param name="ownerHandle">Handle to the owner to return data for.</param>
        /// <returns>Owner data.</returns>
        private static DataTable GetOwnerTable(string ownerHandle)
        {
            OwnerNameDataSource source = new OwnerNameDataSource();
            if(HttpContext.Current != null)
                source.SetCache(HttpContext.Current.Items);
            return source.FindByOwnerHandle(ownerHandle);
        }

        #endregion

        #region Upgrade Checkers

        /// <summary>
        /// Does a given owner have the EStockCard upgrade?
        /// </summary>
        /// <param name="ownerHandle">Handle to the owner to check for.</param>
        /// <returns>True if the owner does have the EStockCard upgrade, false otherwise.</returns>
        public static bool HasEStockCard(string ownerHandle)
        {
            BusinessUnit dealer = HttpContext.Current.Items[BusinessUnit.HttpContextKey] as BusinessUnit;
            if (dealer != null)
            {
                return dealer.HasDealerUpgrade(Upgrade.AgingInventoryPlan);
            }
            return false;
        }

        /// <summary>
        /// Dies a given owner have the EdmundsTmv upgrade?
        /// </summary>
        /// <param name="login">Login to check for.</param>
        /// <param name="handle">Handle to check for.</param>
        /// <returns></returns>
        public static bool HasEdmundsTmvUpgrade(string login, string handle)
        {
            BusinessUnit dealer = HttpContext.Current.Items[BusinessUnit.HttpContextKey] as BusinessUnit;

            if (dealer != null)
            {
                if (dealer.HasDealerUpgrade(Upgrade.EdmundsTrueMarketValue))
                {
                    return true;
                }
            }

            if (dealer == null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Does a given owner have the JDPower upgrade?
        /// </summary>
        /// <param name="identityName">Identity to check against.</param>
        /// <param name="ownerHandle">GUID for the owner to check.</param>
        /// <returns>True if they have the upgrade, false otherwise.</returns>
        public static bool HasJdPowerUpgrade(string identityName, string ownerHandle)
        {
            BusinessUnit dealer = HttpContext.Current.Items[BusinessUnit.HttpContextKey] as BusinessUnit;

            if (dealer != null)
            {
                if (dealer.HasDealerUpgrade(Upgrade.JDPowerUsedCarMarketData) &&
                    LicenseFacade.HadAcceptedLicense(License.JDPowerLicenseName, identityName))
                {
                    return true;
                }
            }
            else
            {
                foreach (DataRow row in GetOwnerTable(ownerHandle).Rows)
                {
                    return row["PowerRegionID"] != DBNull.Value;
                }
            }

            return false;
        }

        /// <summary>
        /// Does a given owner have the Market Stocking upgrade?
        /// </summary>
        /// <param name="ownerHandle">Handle of the owner to check.</param>
        /// <returns>True if the owner has the upgrade, false otherwise.</returns>
        public static bool HasMarketStockingUpgrade(string ownerHandle)
        {
            BusinessUnit dealer = HttpContext.Current.Items[BusinessUnit.HttpContextKey] as BusinessUnit;
            if (dealer != null)
            {
                return dealer.HasDealerUpgrade(Upgrade.MarketStocking);
            }
            return false;
        }

        #endregion
    }
}
