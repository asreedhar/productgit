using System;
using System.Collections.Generic;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Document
{
	[Serializable]
	public class AdvertisementProperties : BusinessBase<AdvertisementProperties>
	{
		#region Business Methods

		private string author;
		private DateTime created;
		private DateTime accessed;
		private DateTime modified;
		private TimeSpan editDuration;
		private int revisionNumber;
		private bool hasEditedBody;
		private bool hasEditedFooter;
		private IList<string> warnings = new List<string>();

		protected override object GetIdValue()
		{
			return string.Empty; // better id please
		}

		public string Author
		{
			get
			{
				return author;
			}
			set
			{
				if (author != value)
				{
					author = value;

					PropertyHasChanged("Author");
				}
			}
		}

		public DateTime Created
		{
			get { return created; }
		}

		public DateTime Accessed
		{
			get { return accessed; }
		}

		public DateTime Modified
		{
			get { return modified; }
		}

		public TimeSpan EditDuration
		{
			get
			{
				return editDuration;
			}
			set
			{
				if (editDuration != value)
				{
					if (editDuration > value)
					{
						throw new ArgumentOutOfRangeException("value", value, "Cannot reduce edit duration");
					}

					editDuration = value;

					PropertyHasChanged("EditDuration");
				}
			}
		}

		public bool HasEditedBody
		{
			get
			{
				return hasEditedBody;
			}
			set
			{
				if (hasEditedBody != value)
				{
					hasEditedBody = value;

					PropertyHasChanged("HasEditedBody");
				}
			}
		}

		public bool HasEditedFooter
		{
			get
			{
				return hasEditedFooter;
			}
			set
			{
				if (hasEditedFooter != value)
				{
					hasEditedFooter = value;

					PropertyHasChanged("HasEditedFooter");
				}
			}
		}

		public int RevisionNumber
		{
			get { return revisionNumber; }
		}

		public IList<string> Warnings
		{
			get { return warnings; }
		}

		internal void Updated()
		{
			MarkOld();
		}

		#endregion

		#region Factory Methods

		internal static AdvertisementProperties NewDocumentProperties()
		{
			return new AdvertisementProperties();
		}

		internal static AdvertisementProperties GetProperties(IDataReader reader)
		{
			return new AdvertisementProperties(reader);
		}

		private AdvertisementProperties()
		{
			MarkAsChild();
		}

		private AdvertisementProperties(IDataReader reader) : this()
		{
			Fetch(reader);
		}

		#endregion

		#region Data Access

		private void Fetch(IDataReader reader)
		{
			if (reader.Read())
			{
				author = reader.GetString(reader.GetOrdinal("Author"));
				created = reader.GetDateTime(reader.GetOrdinal("Created"));
				accessed = reader.GetDateTime(reader.GetOrdinal("Accessed"));
				modified = reader.GetDateTime(reader.GetOrdinal("Modified"));
				editDuration = TimeSpan.FromMinutes(reader.GetInt32(reader.GetOrdinal("EditDuration")));
				revisionNumber = reader.GetInt32(reader.GetOrdinal("RevisionNumber"));
				hasEditedBody = reader.GetBoolean(reader.GetOrdinal("HasEditedBody"));
				hasEditedFooter = reader.GetBoolean(reader.GetOrdinal("HasEditedFooter"));

				if (reader.NextResult())
				{
					warnings = new List<string>();

					while (reader.Read())
					{
						warnings.Add(reader.GetString(reader.GetOrdinal("Warning")));
					}
				}
				else
				{
					throw new DataException("Missing Warnings result set");
				}
			}
			else
			{
				throw new DataException("Missing TextDocumentProperties result set");
			}

			MarkOld();
		}

		internal void Insert(IDbConnection connection, IDbTransaction transaction, int id)
		{
			int editDurationInMinutes = Convert.ToInt32(EditDuration.TotalMinutes) < 1 ? 1 : Convert.ToInt32(EditDuration.TotalMinutes);

			using (IDataCommand command = new DataCommand(connection.CreateCommand()))
			{
				command.CommandText = "Merchandising.Advertisement_Properties#Insert";
				command.CommandType = CommandType.StoredProcedure;
				command.Transaction = transaction;

				command.AddParameterWithValue("AdvertisementId", DbType.Int32, false, id);
				command.AddParameterWithValue("Author", DbType.String, false, author);
				command.AddParameterWithValue("Created", DbType.DateTime, false, created);
				command.AddParameterWithValue("Accessed", DbType.DateTime, false, accessed);
				command.AddParameterWithValue("Modified", DbType.DateTime, false, modified);
				command.AddParameterWithValue("EditDuration", DbType.Int32, false, editDurationInMinutes);
				command.AddParameterWithValue("RevisionNumber", DbType.Int32, false, ++revisionNumber);
				command.AddParameterWithValue("HasEditedBody", DbType.Boolean, false, hasEditedBody);
				command.AddParameterWithValue("HasEditedFooter", DbType.Boolean, false, hasEditedFooter);

				command.ExecuteNonQuery();
			}

			MarkOld();
		}

		#endregion
	}
}
