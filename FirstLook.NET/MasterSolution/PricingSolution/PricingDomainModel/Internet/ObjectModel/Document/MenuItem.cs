using System.Web.UI;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Document
{
	public abstract class MenuItem : IHierarchyData
	{
		private readonly IHierarchyData parent;

		private string name;

		private string text = string.Empty;

		private bool enabled;

		protected MenuItem(IHierarchyData parent)
		{
			this.parent = parent;
		}

		protected MenuItem(IHierarchyData parent, string name, bool enabled)
		{
			this.parent = parent;
			this.name = name;
			this.enabled = enabled;
		}

		public string Name
		{
			get { return name; }
			set { name = value; }
		}

		public string Text
		{
			get { return text; }
			set { text = value; }
		}

		public bool Enabled
		{
			get { return enabled; }
			set { enabled = value; }
		}

		protected abstract IHierarchicalEnumerable GetChildren();

		protected virtual IHierarchyData GetParent()
		{
			return parent;
		}

		public abstract bool HasChildren { get; }

		protected virtual object Item { get { return this; } }

		protected virtual string Path
		{
			get
			{
				if (parent == null) return Name;

				string parentPath = GetParent().Path;

				if (parentPath.EndsWith("/"))
				{
					return parentPath + Name;
				}
				else
				{
					return parentPath + "/" + Name;
				}
			}
		}

		protected abstract string Type { get; }

		protected class EmptyHierarchicalEnumerable : IHierarchicalEnumerable
		{
			private static readonly object[] emptyList = new object[0];

			#region IHierarchicalEnumerable Members

			IHierarchyData IHierarchicalEnumerable.GetHierarchyData(object enumeratedItem)
			{
				return null;
			}

			#endregion

			#region IEnumerable Members

			System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
			{
				return emptyList.GetEnumerator();
			}

			#endregion
		}

		#region IHierarchyData Members

		IHierarchicalEnumerable IHierarchyData.GetChildren()
		{
			return GetChildren();
		}

		IHierarchyData IHierarchyData.GetParent()
		{
			return GetParent();
		}

		bool IHierarchyData.HasChildren
		{
			get { return HasChildren; }
		}

		object IHierarchyData.Item
		{
			get { return Item; }
		}

		string IHierarchyData.Path
		{
			get { return Path; }
		}

		string IHierarchyData.Type
		{
			get { return Type; }
		}

		#endregion
	}
}
