using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Csla;
using Csla.Validation;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Document
{
	[Serializable]
	public class Advertisement : BusinessBase<Advertisement>
	{
		#region Business Methods

		private DocumentCriteria _id;
		private string _body;
		private string _footer;
		private AdvertisementProperties _properties;
		private AdvertisementExtendedProperties _extendedProperties;
		private int _maxLength;

		public string OwnerHandle
		{
			get { return _id.OwnerHandle; }
		}

		public string VehicleHandle
		{
			get { return _id.VehicleHandle; }
		}

		public string Body
		{
			get
			{
				return _body;
			}
			set
			{
				if (value == null) value = string.Empty;

				if (!string.Equals(Body, value))
				{
					_body = value;

					PropertyHasChanged("Body");
				}
			}
		}

		public string Footer
		{
			get
			{
				return _footer;
			}
			set
			{
				if (value == null) value = string.Empty;

				if (!string.Equals(Body, value))
				{
					_footer = value;

					PropertyHasChanged("Footer");
				}
			}
		}

		protected int Length
		{
			get
			{
				return _body.Length + _footer.Length + 1;
			}
		}

		public AdvertisementExtendedProperties ExtendedProperties
		{
			get { return _extendedProperties; }
		}

		public AdvertisementProperties Properties
		{
			get { return _properties; }
		}

		public int MaxLength
		{
			get { return _maxLength; }
		}

		protected override object GetIdValue()
		{
			return _id;
		}

		#endregion

		#region Validation Rules

		/// <remarks>
		/// DO NOT ADD BACK IN THE REGEX LOGIC!! EVER!! IF YOU FEEL THE NEED TO
		/// DO SO RAISE A FOGBUGZ CASE AGAINST SIMON!!
		/// </remarks>
		protected override void AddBusinessRules()
		{
			ValidationRules.AddRule(
				CommonRules.IntegerMaxValue,
				new CommonRules.IntegerMaxValueRuleArgs("Length", MaxLength));
		}

		#endregion

		#region Factory Methods

		private Advertisement()
		{
			/* force use of factory methods */
		}

		public class DataSource
		{
			public Advertisement Select(string ownerHandle, string vehicleHandle)
			{
				ExistsCommand command = new ExistsCommand(new DocumentCriteria(ownerHandle, vehicleHandle));

				DataPortal.Execute(command);

				if (command.Exists)
				{
					return GetAdvertisement(ownerHandle, vehicleHandle);
				}
				else
				{
					return CreateAdvertisement(ownerHandle, vehicleHandle);
				}
			}

			public void Update(string ownerHandle, string vehicleHandle, string body, string footer, IDictionary properties, IDictionary toolBarUsage)
			{
				Advertisement advertisement = Select(ownerHandle, vehicleHandle);
				advertisement.Body = body;
				advertisement.Footer = footer;

				advertisement.Properties.Author = (string) properties["Author"];
				advertisement.Properties.EditDuration += (TimeSpan) properties["EditDuration"];
				advertisement.Properties.HasEditedBody = (bool) properties["HasEditedBody"];
				advertisement.Properties.HasEditedFooter = (bool) properties["HasEditedFooter"];

				foreach (DictionaryEntry entry in toolBarUsage)
				{
					string name = (string) entry.Key;

					if (string.IsNullOrEmpty(name)) continue;

					string value = (string) entry.Value;

					bool included = value.Equals("Included");

					bool available = included || value.Equals("Available");

					if (name.StartsWith("Carfax$", StringComparison.Ordinal))
					{
						advertisement.ExtendedProperties.AvailableCarfax = available;
						advertisement.ExtendedProperties.IncludedCarfax = included;
					}
					else if (string.Equals(name, "Certified", StringComparison.Ordinal))
					{
						advertisement.ExtendedProperties.AvailableCertified = available;
						advertisement.ExtendedProperties.IncludedCertified = included;
					}
					else if (string.Equals(name, "EdmundsTMV", StringComparison.Ordinal))
					{
						advertisement.ExtendedProperties.AvailableEdmundsTrueMarketValue = available;
						advertisement.ExtendedProperties.IncludedEdmundsTrueMarketValue = included;
					}
					else if (string.Equals(name, "NADA", StringComparison.Ordinal))
					{
						advertisement.ExtendedProperties.AvailableNada = available;
						advertisement.ExtendedProperties.IncludedNada = included;
					}
					else if (string.Equals(name, "KelleyBlueBook", StringComparison.Ordinal))
					{
						advertisement.ExtendedProperties.AvailableKelleyBlueBook = available;
						advertisement.ExtendedProperties.IncludedKelleyBlueBook = included;
					}
					else if (string.Equals(name, "HighValueEquipment", StringComparison.Ordinal))
					{
						advertisement.ExtendedProperties.AvailableHighValueEquipment = available;
						advertisement.ExtendedProperties.IncludedHighValueEquipment = included;
					}
					else if (string.Equals(name, "StandardEquipment", StringComparison.Ordinal))
					{
						advertisement.ExtendedProperties.AvailableStandardEquipment = available;
						advertisement.ExtendedProperties.IncludedStandardEquipment = included;
					}
				}

				advertisement.Save();
			}

			public void Delete(string ownerHandle, string vehicleHandle)
			{
				DataPortal.Delete(new DeleteCriteria(ownerHandle, vehicleHandle));
			}
		}

		class DeleteCriteria : DocumentCriteria
		{
			public DeleteCriteria(string ownerHandle, string vehicleHandle)
				: base(ownerHandle, vehicleHandle)
			{
			}
		}

		public static Advertisement GetAdvertisement(string ownerHandle, string vehicleHandle)
		{
			return DataPortal.Fetch<Advertisement>(new DocumentCriteria(ownerHandle, vehicleHandle));
		}

		public static Advertisement CreateAdvertisement(string ownerHandle, string vehicleHandle)
		{
			return DataPortal.Create<Advertisement>(new DocumentCriteria(ownerHandle, vehicleHandle));
		}

		#endregion

		#region Data Access

		[Serializable]
		class ExistsCommand : CommandBase
		{
			private readonly DocumentCriteria criteria;
			private bool exists;

			public ExistsCommand(DocumentCriteria criteria)
			{
				this.criteria = criteria;
			}

			public bool Exists
			{
				get { return exists; }
			}

			protected override void DataPortal_Execute()
			{
				using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
				{
					if (connection.State == ConnectionState.Closed)
						connection.Open();

					using (IDataCommand command = new DataCommand(connection.CreateCommand()))
					{
						command.CommandText = "Merchandising.Advertisement#Exists";
						command.CommandType = CommandType.StoredProcedure;
						command.AddParameterWithValue("OwnerHandle", DbType.String, false, criteria.OwnerHandle);
						command.AddParameterWithValue("VehicleHandle", DbType.String, false, criteria.VehicleHandle);

						using (IDataReader reader = command.ExecuteReader())
						{
							if (reader.Read())
							{
								exists = reader.GetBoolean(reader.GetOrdinal("Exists"));
							}
						}
					}
				}
			}
		}

		[Transactional(TransactionalTypes.Manual)]
		private void DataPortal_Create(DocumentCriteria criteria)
		{
			DoFetchOrCreate(criteria, "Merchandising.Advertisement#Create");

			MarkNew();
		}

		[Transactional(TransactionalTypes.Manual)]
		private void DataPortal_Fetch(DocumentCriteria criteria)
		{
			DoFetchOrCreate(criteria, "Merchandising.Advertisement#Fetch");
			
			MarkOld();
		}

		private void DoFetchOrCreate(DocumentCriteria criteria, string commandText)
		{
			_id = criteria;

			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
			{
				if (connection.State == ConnectionState.Closed)
					connection.Open();

				using (IDataCommand command = new DataCommand(connection.CreateCommand()))
				{
					command.CommandText = commandText;
					command.CommandType = CommandType.StoredProcedure;
					command.AddParameterWithValue("OwnerHandle", DbType.String, false, criteria.OwnerHandle);
					command.AddParameterWithValue("VehicleHandle", DbType.String, false, criteria.VehicleHandle);

					using (IDataReader reader = command.ExecuteReader())
					{
						Fetch(reader);
					}
				}
			}
		}

		[Transactional(TransactionalTypes.Manual)]
		protected override void DataPortal_Insert()
		{
			DoInsertUpdate();

			MarkOld();
		}

		[Transactional(TransactionalTypes.Manual)]
		protected override void DataPortal_Update()
		{
			DoInsertUpdate();

			MarkOld();
		}

		private void DoInsertUpdate()
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
			{
				if (connection.State == ConnectionState.Closed)
					connection.Open();

				using (IDbTransaction transaction = connection.BeginTransaction())
				{
					DoInsertUpdateBody(connection, transaction);

					_properties.Updated();

					transaction.Commit();
				}
			}
		}

		private void DoInsertUpdateBody(IDbConnection connection, IDbTransaction transaction)
		{
			int advertisementId;

			using (IDataCommand command = new DataCommand(connection.CreateCommand()))
			{
				command.CommandText = "Merchandising.Advertisement#Insert";
				command.CommandType = CommandType.StoredProcedure;
				command.Transaction = transaction;

				command.AddParameterWithValue("Body", DbType.String, false, Body);
				command.AddParameterWithValue("Footer", DbType.String, false, Footer);

				IDbDataParameter parameter = command.CreateParameter();
				parameter.DbType = DbType.Int32;
				parameter.Direction = ParameterDirection.Output;
				parameter.ParameterName = "AdvertisementId";

				SqlParameter sqlParameter = parameter as SqlParameter;

				if (sqlParameter != null)
				{
					sqlParameter.IsNullable = false;
				}

				command.Parameters.Add(parameter);

				command.ExecuteNonQuery();

				advertisementId = (int)parameter.Value;
			}

			_properties.Insert(connection, transaction, advertisementId);

			_extendedProperties.Insert(connection, transaction, advertisementId);

			using (IDataCommand command = new DataCommand(connection.CreateCommand()))
			{
				command.CommandText = "Merchandising.VehicleAdvertisement#Assign";
				command.CommandType = CommandType.StoredProcedure;
				command.Transaction = transaction;
				command.AddParameterWithValue("OwnerHandle", DbType.String, false, OwnerHandle);
				command.AddParameterWithValue("VehicleHandle", DbType.String, false, VehicleHandle);
				command.AddParameterWithValue("AdvertisementId", DbType.Int32, false, advertisementId);
				command.ExecuteNonQuery();
			}
		}

		[Transactional(TransactionalTypes.Manual)]
		private void DataPortal_Delete(DeleteCriteria criteria)
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
			{
				if (connection.State == ConnectionState.Closed)
					connection.Open();

				using (IDbTransaction transaction = connection.BeginTransaction())
				{
					using (IDataCommand command = new DataCommand(connection.CreateCommand()))
					{
						command.CommandText = "Merchandising.Advertisement#Delete";
						command.CommandType = CommandType.StoredProcedure;
						command.Transaction = transaction;
						command.AddParameterWithValue("OwnerHandle", DbType.String, false, criteria.OwnerHandle);
						command.AddParameterWithValue("VehicleHandle", DbType.String, false, criteria.VehicleHandle);
						command.ExecuteNonQuery();
					}

					transaction.Commit();
				}
			}
		}

		private void Fetch(IDataReader reader)
		{
			if (reader.Read())
			{
				_body = reader.GetString(reader.GetOrdinal("Body"));
				_footer = reader.GetString(reader.GetOrdinal("Footer"));
				_maxLength = reader.GetInt32(reader.GetOrdinal("MaxLength"));

				if (reader.NextResult())
				{
					_properties = AdvertisementProperties.GetProperties(reader);
					
					if (reader.NextResult())
					{
						if (reader.Read())
						{
							_extendedProperties = AdvertisementExtendedProperties.GetExtendedProperties(reader);
						}
						else
						{
							_extendedProperties = AdvertisementExtendedProperties.NewExtendedProperties();
						}
					}
					else
					{
						throw new DataException("Expected ExtendedProperties result set");
					}
				}
				else
				{
					throw new DataException("Expected Properties result set");
				}
			}
			else
			{
				throw new DataException("Expected ITextDocument result set");
			}
		}

		#endregion
	}
}