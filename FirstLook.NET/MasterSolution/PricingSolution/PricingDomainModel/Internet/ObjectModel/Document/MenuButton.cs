using System.Web.UI;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Document
{
	public class MenuButton : MenuItem
	{
		private string bodyText = string.Empty;
		private string footerText = string.Empty;

		public MenuButton(IHierarchyData parent) : base(parent)
		{
		}

		public MenuButton(IHierarchyData parent, string name, bool enabled) : base(parent, name, enabled)
		{
		}

		public MenuButton(IHierarchyData parent, string name, bool enabled, string bodyText)
			: base(parent, name, enabled)
		{
			this.bodyText = bodyText;
		}

		public MenuButton(IHierarchyData parent, string name, bool enabled, string bodyText, string footerText) : base(parent, name, enabled)
		{
			this.bodyText = bodyText;
			this.footerText = footerText;
		}

		public string BodyText
		{
			get { return bodyText; }
			set { bodyText = value; }
		}

		public string FooterText
		{
			get { return footerText; }
			set { footerText = value; }
		}

		protected override IHierarchicalEnumerable GetChildren()
		{
			return new EmptyHierarchicalEnumerable();
		}

		public override bool HasChildren
		{
			get { return false; }
		}

		protected override string Type
		{
			get { return "MenuButton"; }
		}
	}
}
