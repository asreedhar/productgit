using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Document
{
	[Serializable]
	public class AdvertisementExtendedProperties : BusinessBase<AdvertisementExtendedProperties>
	{
		#region Business Methods

		private bool availableCarfax;
		private bool availableCertified;
		private bool availableEdmundsTrueMarketValue;
		private bool availableHighValueEquipment;
		private bool availableKelleyBlueBook;
		private bool availableNada;
		private bool availableStandardEquipment;
		private bool availableTagline;

		private bool includedCarfax;
		private bool includedCertified;
		private bool includedEdmundsTrueMarketValue;
		private bool includedHighValueEquipment;
		private bool includedKelleyBlueBook;
		private bool includedNada;
		private bool includedStandardEquipment;
		private bool includedTagline;

		public bool AvailableCarfax
		{
			get
			{
				CanReadProperty("AvailableCarfax", true);

				return availableCarfax;
			}
			set
			{
				CanWriteProperty("AvailableCarfax", true);

				if (AvailableCarfax != value)
				{
					availableCarfax = value;

					PropertyHasChanged("AvailableCarfax");
				}
			}
		}

		public bool AvailableCertified
		{
			get
			{
				CanReadProperty("AvailableCertified", true);

				return availableCertified;
			}
			set
			{
				CanWriteProperty("AvailableCertified", true);

				if (AvailableCertified != value)
				{
					availableCertified = value;

					PropertyHasChanged("AvailableCertified");
				}
			}
		}

		public bool AvailableEdmundsTrueMarketValue
		{
			get
			{
				CanReadProperty("AvailableEdmundsTrueMarketValue", true);

				return availableEdmundsTrueMarketValue;
			}
			set
			{
				CanWriteProperty("AvailableEdmundsTrueMarketValue", true);

				if (AvailableEdmundsTrueMarketValue != value)
				{
					availableEdmundsTrueMarketValue = value;

					PropertyHasChanged("AvailableEdmundsTrueMarketValue");
				}
			}
		}

		public bool AvailableKelleyBlueBook
		{
			get
			{
				CanReadProperty("AvailableKelleyBlueBook", true);

				return availableKelleyBlueBook;
			}
			set
			{
				CanWriteProperty("AvailableKelleyBlueBook", true);

				if (AvailableKelleyBlueBook != value)
				{
					availableKelleyBlueBook = value;

					PropertyHasChanged("AvailableKelleyBlueBook");
				}
			}
		}

		public bool AvailableNada
		{
			get
			{
				CanReadProperty("AvailableNada", true);

				return availableNada;
			}
			set
			{
				CanWriteProperty("AvailableNada", true);

				if (AvailableNada != value)
				{
					availableNada = value;

					PropertyHasChanged("AvailableNada");
				}
			}
		}

		public bool AvailableTagline
		{
			get
			{
				CanReadProperty("AvailableTagline", true);

				return availableTagline;
			}
			set
			{
				CanWriteProperty("AvailableTagline", true);

				if (AvailableTagline != value)
				{
					availableTagline = value;

					PropertyHasChanged("AvailableTagline");
				}
			}
		}

		public bool AvailableHighValueEquipment
		{
			get
			{
				CanReadProperty("AvailableHighValueEquipment", true);

				return availableHighValueEquipment;
			}
			set
			{
				CanWriteProperty("AvailableHighValueEquipment", true);

				if (AvailableHighValueEquipment != value)
				{
					availableHighValueEquipment = value;

					PropertyHasChanged("AvailableHighValueEquipment");
				}
			}
		}

		public bool AvailableStandardEquipment
		{
			get
			{
				CanReadProperty("AvailableStandardEquipment", true);

				return availableStandardEquipment;
			}
			set
			{
				CanWriteProperty("AvailableStandardEquipment", true);

				if (AvailableStandardEquipment != value)
				{
					availableStandardEquipment = value;

					PropertyHasChanged("AvailableStandardEquipment");
				}
			}
		}

		public bool IncludedCarfax
		{
			get
			{
				CanReadProperty("IncludedCarfax", true);

				return includedCarfax;
			}
			set
			{
				CanWriteProperty("IncludedCarfax", true);

				if (IncludedCarfax != value)
				{
					includedCarfax = value;

					PropertyHasChanged("IncludedCarfax");
				}
			}
		}

		public bool IncludedCertified
		{
			get
			{
				CanReadProperty("IncludedCertified", true);

				return includedCertified;
			}
			set
			{
				CanWriteProperty("IncludedCertified", true);

				if (IncludedCertified != value)
				{
					includedCertified = value;

					PropertyHasChanged("IncludedCertified");
				}
			}
		}

		public bool IncludedEdmundsTrueMarketValue
		{
			get
			{
				CanReadProperty("IncludedEdmundsTrueMarketValue", true);

				return includedEdmundsTrueMarketValue;
			}
			set
			{
				CanWriteProperty("IncludedEdmundsTrueMarketValue", true);

				if (IncludedEdmundsTrueMarketValue != value)
				{
					includedEdmundsTrueMarketValue = value;

					PropertyHasChanged("IncludedEdmundsTrueMarketValue");
				}
			}
		}

		public bool IncludedKelleyBlueBook
		{
			get
			{
				CanReadProperty("IncludedKelleyBlueBook", true);

				return includedKelleyBlueBook;
			}
			set
			{
				CanWriteProperty("IncludedKelleyBlueBook", true);

				if (IncludedKelleyBlueBook != value)
				{
					includedKelleyBlueBook = value;

					PropertyHasChanged("IncludedKelleyBlueBook");
				}
			}
		}

		public bool IncludedNada
		{
			get
			{
				CanReadProperty("IncludedNada", true);

				return includedNada;
			}
			set
			{
				CanWriteProperty("IncludedNada", true);

				if (IncludedNada != value)
				{
					includedNada = value;

					PropertyHasChanged("IncludedNada");
				}
			}
		}

		public bool IncludedTagline
		{
			get
			{
				CanReadProperty("IncludedTagline", true);

				return includedTagline;
			}
			set
			{
				CanWriteProperty("IncludedTagline", true);

				if (IncludedTagline != value)
				{
					includedTagline = value;

					PropertyHasChanged("IncludedTagline");
				}
			}
		}

		public bool IncludedHighValueEquipment
		{
			get
			{
				CanReadProperty("IncludedHighValueEquipment", true);

				return includedHighValueEquipment;
			}
			set
			{
				CanWriteProperty("IncludedHighValueEquipment", true);

				if (IncludedHighValueEquipment != value)
				{
					includedHighValueEquipment = value;

					PropertyHasChanged("IncludedHighValueEquipment");
				}
			}
		}

		public bool IncludedStandardEquipment
		{
			get
			{
				CanReadProperty("IncludedStandardEquipment", true);

				return includedStandardEquipment;
			}
			set
			{
				CanWriteProperty("IncludedStandardEquipment", true);

				if (IncludedStandardEquipment != value)
				{
					includedStandardEquipment = value;

					PropertyHasChanged("IncludedStandardEquipment");
				}
			}
		}

		protected override object GetIdValue()
		{
			return null;
		}

		#endregion

		#region Factory Methods

		internal static AdvertisementExtendedProperties NewExtendedProperties()
		{
			return new AdvertisementExtendedProperties();
		}

		internal static AdvertisementExtendedProperties GetExtendedProperties(IDataRecord record)
		{
			return new AdvertisementExtendedProperties(record);
		}

		private AdvertisementExtendedProperties()
		{
			MarkAsChild();
		}

		private AdvertisementExtendedProperties(IDataRecord record) : this()
		{
			Fetch(record);
		}

		
		#endregion

		#region Data Access

		private void Fetch(IDataRecord record)
		{
			availableCarfax = record.GetBoolean(record.GetOrdinal("AvailableCarfax"));
			availableCertified = record.GetBoolean(record.GetOrdinal("AvailableCertified"));
			availableEdmundsTrueMarketValue = record.GetBoolean(record.GetOrdinal("AvailableEdmundsTrueMarketValue"));
			availableHighValueEquipment = record.GetBoolean(record.GetOrdinal("AvailableHighValueEquipment"));
			availableKelleyBlueBook = record.GetBoolean(record.GetOrdinal("AvailableKelleyBlueBook"));
			availableNada = record.GetBoolean(record.GetOrdinal("AvailableNada"));
			availableStandardEquipment = record.GetBoolean(record.GetOrdinal("AvailableStandardEquipment"));
			availableTagline = record.GetBoolean(record.GetOrdinal("AvailableTagline"));

			includedCarfax = record.GetBoolean(record.GetOrdinal("IncludedCarfax"));
			includedCertified = record.GetBoolean(record.GetOrdinal("IncludedCertified"));
			includedEdmundsTrueMarketValue = record.GetBoolean(record.GetOrdinal("IncludedEdmundsTrueMarketValue"));
			includedHighValueEquipment = record.GetBoolean(record.GetOrdinal("IncludedHighValueEquipment"));
			includedKelleyBlueBook = record.GetBoolean(record.GetOrdinal("IncludedKelleyBlueBook"));
			includedNada = record.GetBoolean(record.GetOrdinal("IncludedNada"));
			includedStandardEquipment = record.GetBoolean(record.GetOrdinal("IncludedStandardEquipment"));
			includedTagline = record.GetBoolean(record.GetOrdinal("IncludedTagline"));

			MarkOld();
		}

		internal void Insert(IDbConnection connection, IDbTransaction transaction, int id)
		{
			using (IDataCommand command = new DataCommand(connection.CreateCommand()))
			{
				command.CommandText = "Merchandising.Advertisement_ExtendedProperties#Insert";
				command.CommandType = CommandType.StoredProcedure;
				command.Transaction = transaction;

				command.AddParameterWithValue("AdvertisementId", DbType.Int32, false, id);

				command.AddParameterWithValue("AvailableCarfax", DbType.Boolean, false, AvailableCarfax);
				command.AddParameterWithValue("AvailableCertified", DbType.Boolean, false, AvailableCertified);
				command.AddParameterWithValue("AvailableEdmundsTrueMarketValue", DbType.Boolean, false, AvailableEdmundsTrueMarketValue);
				command.AddParameterWithValue("AvailableHighValueEquipment", DbType.Boolean, false, AvailableHighValueEquipment);
				command.AddParameterWithValue("AvailableKelleyBlueBook", DbType.Boolean, false, AvailableKelleyBlueBook);
				command.AddParameterWithValue("AvailableNada", DbType.Boolean, false, AvailableNada);
				command.AddParameterWithValue("AvailableStandardEquipment", DbType.Boolean, false, AvailableStandardEquipment);
				command.AddParameterWithValue("AvailableTagline", DbType.Boolean, false, AvailableTagline);

				command.AddParameterWithValue("IncludedCarfax", DbType.Boolean, false, IncludedCarfax);
				command.AddParameterWithValue("IncludedCertified", DbType.Boolean, false, IncludedCertified);
				command.AddParameterWithValue("IncludedEdmundsTrueMarketValue", DbType.Boolean, false, IncludedEdmundsTrueMarketValue);
				command.AddParameterWithValue("IncludedHighValueEquipment", DbType.Boolean, false, IncludedHighValueEquipment);
				command.AddParameterWithValue("IncludedKelleyBlueBook", DbType.Boolean, false, IncludedKelleyBlueBook);
				command.AddParameterWithValue("IncludedNada", DbType.Boolean, false, IncludedNada);
				command.AddParameterWithValue("IncludedStandardEquipment", DbType.Boolean, false, IncludedStandardEquipment);
				command.AddParameterWithValue("IncludedTagline", DbType.Boolean, false, IncludedTagline);

				command.ExecuteNonQuery();
			}

			MarkOld();
		}

		#endregion
	}
}