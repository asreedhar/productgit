using System;
using System.Data;
using System.Web.UI;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Document
{
	[Serializable]
	public class NotesToolBarProvider : ReadOnlyBase<NotesToolBarProvider>
	{
		#region Business Methods
		
		private DocumentCriteria _id;

		private readonly Menu _menu;

		public string OwnerHandle
		{
			get { return _id.OwnerHandle; }
		}

		public string VehicleHandle
		{
			get { return _id.VehicleHandle; }
		}

		public Menu Menu
		{
			get { return _menu; }
		}

		protected override object GetIdValue()
		{
			return _id;
		}

		#endregion

		
		#region Factory Methods

		private NotesToolBarProvider()
		{
			_menu = new Menu();
		}

		public static NotesToolBarProvider GetProvider(string ownerHandle, string vehicleHandle)
		{
			return DataPortal.Fetch<NotesToolBarProvider>(new DocumentCriteria(ownerHandle, vehicleHandle));
		}

		public class DataSource
		{
			public IHierarchicalEnumerable Select(string ownerHandle, string vehicleHandle)
			{
				return GetProvider(ownerHandle, vehicleHandle).Menu;
			}
		}

		#endregion

		#region Data Access

		private void DataPortal_Fetch(DocumentCriteria criteria)
		{
			_id = criteria;

			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
			{
				if (connection.State == ConnectionState.Closed)
					connection.Open();

				using (IDataCommand command = new DataCommand(connection.CreateCommand()))
				{
					command.CommandText = "Pricing.NotesToolBarProvider#Fetch";
					command.CommandType = CommandType.StoredProcedure;
					command.AddParameterWithValue("OwnerHandle", DbType.String, false, criteria.OwnerHandle);
					command.AddParameterWithValue("VehicleHandle", DbType.String, false, criteria.VehicleHandle);

					using (IDataReader reader = command.ExecuteReader())
					{
						Fetch(reader);
					}
				}
			}
		}

		private void Fetch(IDataReader reader)
		{
			while (reader.Read())
			{
				Fetch(reader, _menu);
			}
		}

		private static void Fetch(IDataRecord record, Menu menu)
		{
			MenuButton button = new MenuButton(menu);

			button.Enabled = record.GetBoolean(record.GetOrdinal("Enabled"));
			button.Name = record.GetString(record.GetOrdinal("Name"));
			button.Text = record.GetString(record.GetOrdinal("Text"));

			if (!record.IsDBNull(record.GetOrdinal("BodyText")))
			{
				button.BodyText = record.GetString(record.GetOrdinal("BodyText"));
			}

			if (!record.IsDBNull(record.GetOrdinal("FooterText")))
			{
				button.FooterText = record.GetString(record.GetOrdinal("FooterText"));
			}
			
			menu.Children.Add(button);
		}

		#endregion
	}
}
