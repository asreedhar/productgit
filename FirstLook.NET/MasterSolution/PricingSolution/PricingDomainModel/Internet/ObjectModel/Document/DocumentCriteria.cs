using System;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Document
{
	[Serializable]
	internal class DocumentCriteria : IEquatable<DocumentCriteria>
	{
		private readonly string ownerHandle;
		private readonly string vehicleHandle;

		public DocumentCriteria(string ownerHandle, string vehicleHandle)
		{
			this.ownerHandle = ownerHandle;
			this.vehicleHandle = vehicleHandle;
		}

		public string OwnerHandle
		{
			get { return ownerHandle; }
		}

		public string VehicleHandle
		{
			get { return vehicleHandle; }
		}

		public static bool operator !=(DocumentCriteria documentCriteria1, DocumentCriteria documentCriteria2)
		{
			return !Equals(documentCriteria1, documentCriteria2);
		}

		public static bool operator ==(DocumentCriteria documentCriteria1, DocumentCriteria documentCriteria2)
		{
			return Equals(documentCriteria1, documentCriteria2);
		}

		public bool Equals(DocumentCriteria documentCriteria)
		{
			if (documentCriteria == null) return false;
			return Equals(ownerHandle, documentCriteria.ownerHandle) && Equals(vehicleHandle, documentCriteria.vehicleHandle);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(this, obj)) return true;
			return Equals(obj as DocumentCriteria);
		}

		public override int GetHashCode()
		{
			return ownerHandle.GetHashCode() + 29*vehicleHandle.GetHashCode();
		}
	}
}
