using System;
using System.Data;
using System.Threading;
using System.Web.UI;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Document
{
	[Serializable]
	public class AdvertisementToolBarProvider : ReadOnlyBase<AdvertisementToolBarProvider>
	{
		#region Business Methods

		private DocumentCriteria _id;

		private readonly Menu _menu;

		private readonly MenuDropDown _historyDD;
		private readonly MenuDropDown _informationDD;
		private readonly MenuDropDown _pricingDD;
		private readonly MenuDropDown _textFragmentDD;

		public string OwnerHandle
		{
			get { return _id.OwnerHandle; }
		}

		public string VehicleHandle
		{
			get { return _id.VehicleHandle; }
		}

		public Menu Menu
		{
			get { return _menu; }
		}

		protected override object GetIdValue()
		{
			return _id;
		}

		#endregion

		#region Factory Methods

		private AdvertisementToolBarProvider()
		{
			_menu = new Menu();

			_historyDD = new MenuDropDown(_menu, "VehicleHistory", true);
			_menu.Children.Add(_historyDD);

			_informationDD = new MenuDropDown(_menu, "VehicleInformation", true);
			_menu.Children.Add(_informationDD);

			_pricingDD = new MenuDropDown(_menu, "Pricing", true);
			_menu.Children.Add(_pricingDD);

			_textFragmentDD = new MenuDropDown(_menu, "Tagline", true);
			_menu.Children.Add(_textFragmentDD);
		}

		public static AdvertisementToolBarProvider GetProvider(string ownerHandle, string vehicleHandle)
		{
			return DataPortal.Fetch<AdvertisementToolBarProvider>(new DocumentCriteria(ownerHandle, vehicleHandle));
		}

		public class DataSource
		{
			public IHierarchicalEnumerable Select(string ownerHandle, string vehicleHandle)
			{
				return GetProvider(ownerHandle, vehicleHandle).Menu;
			}
		}

		#endregion

		#region Data Access

		private void DataPortal_Fetch(DocumentCriteria criteria)
		{
			_id = criteria;

			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
			{
				if (connection.State == ConnectionState.Closed)
					connection.Open();

				using (IDataCommand command = new DataCommand(connection.CreateCommand()))
				{
					command.CommandText = "Merchandising.AdvertisementToolBarProvider#Fetch";
					command.CommandType = CommandType.StoredProcedure;
					command.AddParameterWithValue("OwnerHandle", DbType.String, false, criteria.OwnerHandle);
					command.AddParameterWithValue("VehicleHandle", DbType.String, false, criteria.VehicleHandle);
					command.AddParameterWithValue("UserName", DbType.String, true, Thread.CurrentPrincipal.Identity.Name);

					using (IDataReader reader = command.ExecuteReader())
					{
						Fetch(reader);
					}
				}
			}
		}

		private void Fetch(IDataReader reader)
		{
			while (reader.Read())
			{
				Fetch(reader, _historyDD);
			}

			if (reader.NextResult())
			{
				while (reader.Read())
				{
					Fetch(reader, _historyDD);
				}

				if (reader.NextResult())
				{
					while (reader.Read())
					{
						Fetch(reader, _informationDD);
					}

					if (reader.NextResult())
					{
						while (reader.Read())
						{
							Fetch(reader, _pricingDD);
						}

						if (reader.NextResult())
						{
							while (reader.Read())
							{
								Fetch(reader, _textFragmentDD);
							}
						}
					}
				}
			}
		}

		private static void Fetch(IDataRecord record, MenuDropDown list)
		{
			MenuButton button = new MenuButton(list);

			button.Enabled = record.GetBoolean(record.GetOrdinal("Enabled"));
			button.Name = record.GetString(record.GetOrdinal("Name"));
			button.Text = record.GetString(record.GetOrdinal("Text"));

			if (!record.IsDBNull(record.GetOrdinal("BodyText")))
			{
				button.BodyText = record.GetString(record.GetOrdinal("BodyText"));
			}

			if (!record.IsDBNull(record.GetOrdinal("FooterText")))
			{
				button.FooterText = record.GetString(record.GetOrdinal("FooterText"));
			}

			list.Children.Add(button);
		}

		#endregion
	}
}
