using System;
using System.Collections;
using System.Data;
using Csla;
using Csla.Validation;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Document
{
	[Serializable]
	public class Notes : BusinessBase<Notes>
	{
		#region Business Methods

		private DocumentCriteria _id;
		private string _body;

		public string OwnerHandle
		{
			get { return _id.OwnerHandle; }
		}

		public string VehicleHandle
		{
			get { return _id.VehicleHandle; }
		}

		public string Body
		{
			get
			{
				return _body;
			}
			set
			{
				if (value == null) value = string.Empty;

				if (!string.Equals(Body, value))
				{
					_body = value;

					PropertyHasChanged("Body");
				}
			}
		}

		public int MaxLength
		{
			get { return 500; }
		}

		protected override object GetIdValue()
		{
			return _id;
		}

		#endregion

		#region Factory Methods

		private Notes()
		{
			/* force use of factory methods */
		}

		public class DataSource
		{
			public Notes Select(string ownerHandle, string vehicleHandle)
			{
				ExistsCommand command = new ExistsCommand(new DocumentCriteria(ownerHandle, vehicleHandle));

				DataPortal.Execute(command);

				if (command.Exists)
				{
					return GetPricingNotes(ownerHandle, vehicleHandle);
				}
				else
				{
					return CreatePricingNotes(ownerHandle, vehicleHandle);
				}
			}

			public void Update(string ownerHandle, string vehicleHandle, string body, string footer, IDictionary properties, IDictionary toolBarUsage)
			{
				Notes notes = Select(ownerHandle, vehicleHandle);
				notes.Body = body;
				notes.Save();
			}

			public void Delete(string ownerHandle, string vehicleHandle)
			{
				DataPortal.Delete(new DeleteCriteria(ownerHandle, vehicleHandle));
			}
		}

		class DeleteCriteria : DocumentCriteria
		{
			public DeleteCriteria(string ownerHandle, string vehicleHandle) : base(ownerHandle, vehicleHandle)
			{
			}
		}

		public static Notes GetPricingNotes(string ownerHandle, string vehicleHandle)
		{
			return DataPortal.Fetch<Notes>(new DocumentCriteria(ownerHandle, vehicleHandle));
		}

		public static Notes CreatePricingNotes(string ownerHandle, string vehicleHandle)
		{
			return DataPortal.Create<Notes>(new DocumentCriteria(ownerHandle, vehicleHandle));
		}

		#endregion

		#region Validation Rules

		protected override void AddBusinessRules()
		{
			ValidationRules.AddRule(
				CommonRules.StringMaxLength,
				new CommonRules.MaxLengthRuleArgs("Body", MaxLength));
		}

		#endregion

		#region Data Access

		[Serializable]
		class ExistsCommand : CommandBase
		{
			private readonly DocumentCriteria criteria;
			private bool exists;

			public ExistsCommand(DocumentCriteria criteria)
			{
				this.criteria = criteria;
			}

			public bool Exists
			{
				get { return exists; }
			}

			protected override void DataPortal_Execute()
			{
				using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
				{
					if (connection.State == ConnectionState.Closed)
						connection.Open();

					using (IDataCommand command = new DataCommand(connection.CreateCommand()))
					{
						command.CommandText = "Pricing.Notes#Exists";
						command.CommandType = CommandType.StoredProcedure;
						command.AddParameterWithValue("OwnerHandle", DbType.String, false, criteria.OwnerHandle);
						command.AddParameterWithValue("VehicleHandle", DbType.String, false, criteria.VehicleHandle);

						using (IDataReader reader = command.ExecuteReader())
						{
							if (reader.Read())
							{
								exists = reader.GetBoolean(reader.GetOrdinal("Exists"));
							}
						}
					}
				}
			}
		}

		[Transactional(TransactionalTypes.Manual)]
		private void DataPortal_Create(DocumentCriteria criteria)
		{
			DoFetchOrCreate(criteria, "Pricing.Notes#Create");

			MarkNew();
		}

		[Transactional(TransactionalTypes.Manual)]
		private void DataPortal_Fetch(DocumentCriteria criteria)
		{
			DoFetchOrCreate(criteria, "Pricing.Notes#Fetch");

			MarkOld();
		}

		private void DoFetchOrCreate(DocumentCriteria criteria, string commandText)
		{
			_id = criteria;

			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
			{
				if (connection.State == ConnectionState.Closed)
					connection.Open();

				using (IDataCommand command = new DataCommand(connection.CreateCommand()))
				{
					command.CommandText = commandText;
					command.CommandType = CommandType.StoredProcedure;
					command.AddParameterWithValue("OwnerHandle", DbType.String, false, criteria.OwnerHandle);
					command.AddParameterWithValue("VehicleHandle", DbType.String, false, criteria.VehicleHandle);

					using (IDataReader reader = command.ExecuteReader())
					{
						Fetch(reader);
					}
				}
			}
		}

		private void Fetch(IDataReader reader)
		{
			if (reader.Read())
			{
				_body = reader.GetString(reader.GetOrdinal("Body"));
			}
			else
			{
				throw new DataException("Expected ITextDocument result set");
			}
		}

		[Transactional(TransactionalTypes.Manual)]
		protected override void DataPortal_Insert()
		{
			DoInsertOrUpdate("Pricing.Notes#Insert");
		}

		[Transactional(TransactionalTypes.Manual)]
		protected override void DataPortal_Update()
		{
			DoInsertOrUpdate("Pricing.Notes#Update");
		}

		private void DoInsertOrUpdate(string commandText)
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
			{
				if (connection.State == ConnectionState.Closed)
					connection.Open();

				using (IDbTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						using (IDataCommand command = new DataCommand(connection.CreateCommand()))
						{
							command.CommandText = commandText;
							command.CommandType = CommandType.StoredProcedure;
							command.Transaction = transaction;
							command.AddParameterWithValue("OwnerHandle", DbType.String, false, OwnerHandle);
							command.AddParameterWithValue("VehicleHandle", DbType.String, false, VehicleHandle);
							command.AddParameterWithValue("Body", DbType.String, false, Body);

							command.ExecuteNonQuery();
						}

						transaction.Commit();
					}
					catch
					{
						transaction.Rollback();

						throw;
					}
				}
			}

			MarkOld();
		}

		[Transactional(TransactionalTypes.Manual)]
		private void DataPortal_Delete(DeleteCriteria criteria)
		{
			using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
			{
				if (connection.State == ConnectionState.Closed)
					connection.Open();

				using (IDbTransaction transaction = connection.BeginTransaction())
				{
					using (IDataCommand command = new DataCommand(connection.CreateCommand()))
					{
						command.CommandText = "Pricing.Notes#Delete";
						command.CommandType = CommandType.StoredProcedure;
						command.Transaction = transaction;
						command.AddParameterWithValue("OwnerHandle", DbType.String, false, criteria.OwnerHandle);
						command.AddParameterWithValue("VehicleHandle", DbType.String, false, criteria.VehicleHandle);
						command.ExecuteNonQuery();
					}

					transaction.Commit();

				}
			}
		}

		#endregion
	}
}
