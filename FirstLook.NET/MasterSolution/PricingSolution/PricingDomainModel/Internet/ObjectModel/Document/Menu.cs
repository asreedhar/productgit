using System.Collections;
using System.Collections.Generic;
using System.Web.UI;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Document
{
	public class Menu : IHierarchyData, IHierarchicalEnumerable
	{
		private readonly List<MenuItem> children;

		public Menu()
		{
			children = new List<MenuItem>();
		}

		public List<MenuItem> Children
		{
			get { return children; }
		}

		#region IHierarchyData Members

		IHierarchicalEnumerable IHierarchyData.GetChildren()
		{
			return new ChildHierarchicalEnumerable(children);
		}

		IHierarchyData IHierarchyData.GetParent()
		{
			return null;
		}

		bool IHierarchyData.HasChildren
		{
			get { return children.Count > 0; }
		}

		object IHierarchyData.Item
		{
			get { return this; }
		}

		string IHierarchyData.Path
		{
			get { return "/"; }
		}

		string IHierarchyData.Type
		{
			get { return "Menu"; }
		}

		protected class ChildHierarchicalEnumerable : IHierarchicalEnumerable
		{
			private readonly List<MenuItem> children;

			public ChildHierarchicalEnumerable(List<MenuItem> children)
			{
				this.children = children;
			}

			#region IHierarchicalEnumerable Members

			IHierarchyData IHierarchicalEnumerable.GetHierarchyData(object enumeratedItem)
			{
				return enumeratedItem as MenuItem;
			}

			#endregion

			#region IEnumerable Members

			IEnumerator IEnumerable.GetEnumerator()
			{
				return children.GetEnumerator();
			}

			#endregion
		}

		#endregion

		#region IHierarchicalEnumerable Members

		IHierarchyData IHierarchicalEnumerable.GetHierarchyData(object enumeratedItem)
		{
			return enumeratedItem as IHierarchyData;
		}

		#endregion

		#region IEnumerable Members

		IEnumerator IEnumerable.GetEnumerator()
		{
			return children.GetEnumerator();
		}

		#endregion
	}
}
