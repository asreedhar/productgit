using System.Collections;
using System.Collections.Generic;
using System.Web.UI;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Document
{
	public class MenuDropDown : MenuItem
	{
		private readonly List<MenuItem> children = new List<MenuItem>();

		public MenuDropDown(IHierarchyData parent) : base(parent)
		{
		}

		public MenuDropDown(IHierarchyData parent, string name, bool enabled) : base(parent, name, enabled)
		{
		}

		public List<MenuItem> Children
		{
			get { return children; }
		}

		protected override IHierarchicalEnumerable GetChildren()
		{
			return new ChildHierarchicalEnumerable(children);
		}

		public override bool HasChildren
		{
			get { return children.Count > 0; }
		}

		protected override string Type
		{
			get { return "MenuDropDown"; }
		}

		protected class ChildHierarchicalEnumerable : IHierarchicalEnumerable
		{
			private readonly List<MenuItem> children;

			public ChildHierarchicalEnumerable(List<MenuItem> children)
			{
				this.children = children;
			}

			#region IHierarchicalEnumerable Members

			IHierarchyData IHierarchicalEnumerable.GetHierarchyData(object enumeratedItem)
			{
				return enumeratedItem as MenuItem;
			}

			#endregion

			#region IEnumerable Members

			IEnumerator IEnumerable.GetEnumerator()
			{
				return children.GetEnumerator();
			}

			#endregion
		}
	}
}
