using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel
{
    [Serializable]
    public class OwnerPreferences : ReadOnlyBase<OwnerPreferences>
    {
        #region Business Methods
        private string _ownerHandle;
        private bool _suppressSellerName;
        private bool _suppressTrimMatchStatus;
        private decimal _excludeLowPriceOutliersMultiplier;
        private decimal _excludeHighPriceOutliersMultiplier;
        private bool _excludeNoPriceFromCalc;

        public decimal ExcludeLowPriceOutliersMultiplier
        {
            get { return _excludeLowPriceOutliersMultiplier; }
        }

        public decimal ExcludeHighPriceOutliersMultiplier
        {
            get { return _excludeHighPriceOutliersMultiplier; }
        }

        public bool ExcludeNoPriceFromCalc
        {
            get { return _excludeNoPriceFromCalc; }
        }

        public string OwnerHandle
        {
            get { return _ownerHandle; }
        }

        public bool SuppressSellerName
        {
            get { return _suppressSellerName; }
        }

        public bool SuppressTrimMatchStatus
        {
            get { return _suppressTrimMatchStatus; }
        }

        protected override object GetIdValue()
        {
            return _ownerHandle;
        } 

        #endregion

        #region Factory Methods

        public static OwnerPreferences GetOwnerPreferences(string ownerHandle)
        {
            return DataSource.Select(ownerHandle);
        }


        public class DataSource
        {
            public static OwnerPreferences Select(string ownerHandle)
            {
                return DataPortal.Fetch<OwnerPreferences>(new Criteria(ownerHandle));
            }
        }


        #endregion

        #region Data Access

        [Serializable]
        protected class Criteria
        {
            private readonly string _ownerHandle;

            public Criteria(string ownerHandle)
            {
                _ownerHandle = ownerHandle;
            }

            public string OwnerHandle
            {
                get { return _ownerHandle; }
            }
        }

        protected virtual void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (IDataCommand command = new DataCommand((connection.CreateCommand())))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Pricing.OwnerPreferences#Fetch";
                    command.AddParameterWithValue("OwnerHandle", DbType.String, false, criteria.OwnerHandle);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        Fetch(reader);
                    }
                }
            }
        }

        protected void Fetch(IDataReader reader)
        {
            if (reader.Read())
            {
                _ownerHandle = reader.GetString(reader.GetOrdinal("OwnerHandle"));
                _suppressSellerName = reader.GetBoolean(reader.GetOrdinal("SuppressSellerName"));
                _suppressTrimMatchStatus = reader.GetBoolean(reader.GetOrdinal("SuppressTrimMatchStatus"));
                _excludeHighPriceOutliersMultiplier = reader.GetDecimal(reader.GetOrdinal("ExcludeHighPriceOutliersMultiplier"));
                _excludeLowPriceOutliersMultiplier = reader.GetDecimal(reader.GetOrdinal("ExcludeLowPriceOutliersMultiplier"));
                _excludeNoPriceFromCalc = reader.GetBoolean(reader.GetOrdinal("ExcludeNoPriceFromCalc")); 
            }
        }

        #endregion
    }
}
