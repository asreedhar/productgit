using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel
{
    [Serializable]
    public class PackageDetails : ReadOnlyBase<PackageDetails>
    {
        #region Business Methods

        private int id;
        private string pageBrand;
        private string notesBrand;
        private string merchandisingBrand;
        private bool hasPricingTile;
        private bool hasMysteryShoppingTile;
        private bool hasNotes;
        private bool hasInternetAdvertisement;
        private bool hasOneClick;
        private bool hasMileageAdjustedSearch;

        public int Id
        {
            get { return id; }
        }

        public string PageBrand
        {
            get { return pageBrand; }
        }

        public string NotesBrand
        {
            get { return notesBrand; }
        }

        public string MerchandisingBrand
        {
            get { return merchandisingBrand; }
        }

        public bool HasPricingTile
        {
            get { return hasPricingTile; }
        }

        public bool HasMysteryShoppingTile
        {
            get { return hasMysteryShoppingTile; }
        }

        public bool HasNotes
        {
            get { return hasNotes; }
        }

        public bool HasInternetAdvertisement
        {
            get { return hasInternetAdvertisement; }
        }

        public bool HasOneClick
        {
            get { return hasOneClick; }
        }

        public bool HasMileageAdjustedSearch
        {
            get { return hasMileageAdjustedSearch;  }
        }

        protected override object GetIdValue()
        {
            return id;
        }

        #endregion

        #region Factory Methods

        private static readonly PackageDetails[] Packages = new PackageDetails[8];

        static PackageDetails()
        {
            Packages[0] = new PackageDetails(
                1,
                "PING III Market Pricing Analyzer",
                "Selling Notes",
                "Merchandising",
                false, false, false, false, false, false);
            Packages[1] = new PackageDetails(
                2,
                "PING III Market Pricing Analyzer",
                "Selling Notes",
                "Merchandising",
                false, true, false, false, false, false);
            Packages[2] = new PackageDetails(
                3,
                "PING III Market Pricing Analyzer",
                "Selling Notes",
                "Merchandising",
                true, false, true, false, false, false);
            Packages[3] = new PackageDetails(
                4,
                "PING III Market Pricing Analyzer",
                "Selling Notes",
                "Merchandising",
                true, true, true, false, false, false);
            Packages[4] = new PackageDetails(
                5,
                "PING III Market Pricing Analyzer",
                "Selling Notes",
                "Internet Advertising Accelerator",
                true, true, true, true, false, false);
            Packages[5] = new PackageDetails(
                6,
                "PING III Market Pricing Analyzer",
                "Selling Notes",
                "Internet Advertising Accelerator",
                true, true, true, true, true, false);
            Packages[6] = new PackageDetails(
                7,
                "Ping III with Market Pricing Power",
                "Notes",
                "Internet Advertising Accelerator",
                true, true, true, true, false, true);
            Packages[7] = new PackageDetails(
                8,
                "Ping III with Market Pricing Power",
                "Selling Notes",
                "Internet Advertising Accelerator",
                true, true, true, true, true, true);
        }

        private PackageDetails()
        {
        }

        internal PackageDetails(int id, string pageBrand, string notesBrand, string merchandisingBrand, bool hasPricingTile, bool hasMysteryShoppingTile, bool hasNotes, bool hasAdBuilder, bool hasOneClick, bool hasMileageAdjustedSearch)
        {
            this.id = id;
            this.pageBrand = pageBrand;
            this.notesBrand = notesBrand;
            this.merchandisingBrand = merchandisingBrand;
            this.hasPricingTile = hasPricingTile;
            this.hasMysteryShoppingTile = hasMysteryShoppingTile;
            this.hasNotes = hasNotes;
            this.hasInternetAdvertisement = hasAdBuilder;
            this.hasOneClick = hasOneClick;
            this.hasMileageAdjustedSearch = hasMileageAdjustedSearch;
        }

        public static PackageDetails GetDealerPricingPackageDetails(string ownerHandle, string searchHandle, string vehicleHandle)
        {
            return DataPortal.Fetch<PackageDetails>(new SearchKey(ownerHandle, vehicleHandle, searchHandle));
        }

        public static PackageDetails GetDealerPricingPackageDetails(SearchKey searchKey)
        {
            return DataPortal.Fetch<PackageDetails>(searchKey);
        }

        #endregion

        #region Data Access

        private void DataPortal_Fetch(SearchKey key)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandText = "Pricing.PackageDetails#Fetch";
                    command.CommandType = CommandType.StoredProcedure;

                    key.AddToCommand(command);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        int packageDetailsId = 1;

                        if (reader.Read())
                        {
                            packageDetailsId = reader.GetInt32(reader.GetOrdinal("Id"));
                        }

                        Fetch(Packages[packageDetailsId-1]);
                    }
                }
            }
        }

        private void Fetch(PackageDetails packageDetails)
        {
            id = packageDetails.id;
            pageBrand = packageDetails.pageBrand;
            notesBrand = packageDetails.notesBrand;
            merchandisingBrand = packageDetails.merchandisingBrand;
            hasPricingTile = packageDetails.hasPricingTile;
            hasMysteryShoppingTile = packageDetails.hasMysteryShoppingTile;
            hasNotes = packageDetails.hasNotes;
            hasInternetAdvertisement = packageDetails.hasInternetAdvertisement;
            hasOneClick = packageDetails.hasOneClick;
            hasMileageAdjustedSearch = packageDetails.hasMileageAdjustedSearch;
        }

        #endregion
    }
}
