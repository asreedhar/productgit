using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel
{
    public class IsSellerNameSuppressed : CommandBase
    {
        public static bool Execute(string ownerHandle)
        {
            IsSellerNameSuppressed command = new IsSellerNameSuppressed(ownerHandle);

            DataPortal.Execute(command);

            return command.Result;
        }

        private string ownerHandle;
        private bool result;

        private IsSellerNameSuppressed(string ownerHandle)
        {
            OwnerHandle = ownerHandle;
            Result = true;
        }

        protected override void DataPortal_Execute()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = @"
                        DECLARE	@err INT, @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT, @Result TINYINT
                        BEGIN TRY
                            EXEC @err = [Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
                        END TRY
                        BEGIN CATCH
                            SELECT @err = @@ERROR
                        END CATCH
                        IF @err <> 0 BEGIN
                            SET @Result = 1
                        END
                        ELSE BEGIN
                            IF @OwnerEntityTypeID = 1 BEGIN
                                SELECT @Result = COALESCE(PingII_SupressSellerName,1) FROM IMT.dbo.DealerPreference_Pricing WHERE BusinessUnitID = @OwnerEntityID
                            END
                            ELSE BEGIN
                                SET @Result = 0
                            END
                        END
                        SELECT CAST(@Result AS BIT) SuppressSellerName, @err ErrorCode
                    ";
                    command.AddParameterWithValue("OwnerHandle", DbType.String, false, OwnerHandle);
                    using (IDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int ordinal = reader.GetOrdinal("SuppressSellerName");
                            if (reader.IsDBNull(ordinal))
                                result = true;
                            else
                                result = reader.GetBoolean(ordinal);
                        }
                    }
                }
            }
        }

        public bool Result
        {
            get { return result; }
            protected set { result = value; }
        }

        public string OwnerHandle
        {
            get { return ownerHandle; }
            protected set { ownerHandle = value; }
        }
    }
}