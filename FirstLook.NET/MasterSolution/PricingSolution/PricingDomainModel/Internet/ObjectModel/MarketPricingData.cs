using System;
using System.Collections.Specialized;
using System.Data;
using System.Text;
using FirstLook.Common.Core.Utilities;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel
{
    public class MarketPricingData
    {
        #region Fields
        
        private readonly int  _vehicleEntityTypeId;
        private readonly int  _vehicleEntityId;
        private readonly int? _inventoryListPrice;
        private readonly int? _minMarketPrice;
        private readonly int? _avgMarketPrice;
        private readonly int? _maxMarketPrice;
        private readonly int? _minPctMarketAvg;
        private readonly int? _maxPctMarketAvg;
        private readonly int? _minPctMarketValue;
        private readonly int? _maxPctMarketValue;
        private readonly int? _pctAvgMarketPrice;
        private readonly int? _minComparableMarketPrice;
        private readonly int? _maxComparableMarketPrice;
        private readonly int  _numListings;
        private readonly int  _numComparableListings;
        private readonly int? _appraisalValue;
        private readonly int? _estimatedReconditioningCost;
        private readonly int? _targetGrossProfit;

        private readonly int? _newPctAvgMarketPrice;
        private readonly int? _newPrice;

        #endregion

        #region Constructors
        
        /// <summary>
        /// Extract the market pricing property values from a DataRow.
        /// </summary>
        /// <param name="row"></param>
        public MarketPricingData(DataRowView row)
        {
            if (row == null)
                throw new ArgumentNullException("row", "DataRow cannot be null");

            _vehicleEntityTypeId = Int32Helper.ToInt32(row["VehicleEntityTypeID"]);
            _vehicleEntityId = Int32Helper.ToInt32(row["VehicleEntityID"]);
            _inventoryListPrice = Int32Helper.ToNullableInt32(row["InventoryListPrice"]);
            _minMarketPrice = Int32Helper.ToNullableInt32(row["MinMarketPrice"]);
            _avgMarketPrice = Int32Helper.ToNullableInt32(row["AvgMarketPrice"]);
            _maxMarketPrice = Int32Helper.ToNullableInt32(row["MaxMarketPrice"]);
            _minPctMarketAvg = Int32Helper.ToNullableInt32(row["MinPctMarketAvg"]);
            _maxPctMarketAvg = Int32Helper.ToNullableInt32(row["MaxPctMarketAvg"]);
            _minPctMarketValue = Int32Helper.ToNullableInt32(row["MinPctMarketValue"]);
            _maxPctMarketValue = Int32Helper.ToNullableInt32(row["MaxPctMarketValue"]);
            _pctAvgMarketPrice = Int32Helper.ToNullableInt32(row["PctAvgMarketPrice"]);
            _minComparableMarketPrice = Int32Helper.ToNullableInt32(row["MinComparableMarketPrice"]);
            _maxComparableMarketPrice = Int32Helper.ToNullableInt32(row["MaxComparableMarketPrice"]);
            _numListings = Int32Helper.ToInt32(row["NumListings"]);
            _numComparableListings = Int32Helper.ToInt32(row["NumComparableListings"]);
            _appraisalValue = Int32Helper.ToNullableInt32(row["AppraisalValue"]);
            _estimatedReconditioningCost = Int32Helper.ToNullableInt32(row["EstimatedReconditioningCost"]);
            _targetGrossProfit = Int32Helper.ToNullableInt32(row["TargetGrossProfit"]);
        }

        /// <summary>
        /// Extract the market pricing property values from a QueryString.
        /// </summary>
        /// <param name="parameters"></param>
        public MarketPricingData(NameValueCollection parameters)
        {
            if (parameters == null)
                throw new ArgumentNullException("parameters", "NameValueCollection cannot be null");

            _vehicleEntityTypeId = Int32Helper.ToInt32(parameters, "VehicleEntityTypeID");
            _vehicleEntityId = Int32Helper.ToInt32(parameters, "VehicleEntityID");
            _inventoryListPrice = Int32Helper.ToNullableInt32(parameters, "InventoryListPrice");
            _minMarketPrice = Int32Helper.ToNullableInt32(parameters, "MinMarketPrice");
            _avgMarketPrice = Int32Helper.ToNullableInt32(parameters, "AvgMarketPrice");
            _maxMarketPrice = Int32Helper.ToNullableInt32(parameters, "MaxMarketPrice");
            _minPctMarketAvg = Int32Helper.ToNullableInt32(parameters, "MinPctMarketAvg");
            _maxPctMarketAvg = Int32Helper.ToNullableInt32(parameters, "MaxPctMarketAvg");
            _minPctMarketValue = Int32Helper.ToNullableInt32(parameters, "MinPctMarketValue");
            _maxPctMarketValue = Int32Helper.ToNullableInt32(parameters, "MaxPctMarketValue");
            _pctAvgMarketPrice = Int32Helper.ToNullableInt32(parameters, "PctAvgMarketPrice");
            _minComparableMarketPrice = Int32Helper.ToNullableInt32(parameters, "MinComparableMarketPrice");
            _maxComparableMarketPrice = Int32Helper.ToNullableInt32(parameters, "MaxComparableMarketPrice");
            _numListings = Int32Helper.ToInt32(parameters, "NumListings");
            _numComparableListings = Int32Helper.ToInt32(parameters, "NumComparableListings");
            _appraisalValue = Int32Helper.ToNullableInt32(parameters, "AppraisalValue");
            _estimatedReconditioningCost = Int32Helper.ToNullableInt32(parameters, "EstimatedReconditioningCost");
            _targetGrossProfit = Int32Helper.ToNullableInt32(parameters, "TargetGrossProfit");
            _newPctAvgMarketPrice = Int32Helper.ToNullableInt32(parameters, "NewPctAvgMarketPrice");
            _newPrice = Int32Helper.ToNullableInt32(parameters, "NewPrice");
        }

        #endregion

        #region Business Methods

        /// <summary>
        /// Build a query string fragment from the object properties.
        /// </summary>
        /// <returns></returns>
        public string QueryString(int? newPctAvgMarketPrice, int? newPrice)
        {
            StringBuilder sb = new StringBuilder();

            AppendInt(sb, "VehicleEntityTypeID", _vehicleEntityTypeId);
            AppendInt(sb, "VehicleEntityID", _vehicleEntityId);
            AppendInt(sb, "InventoryListPrice", _inventoryListPrice);
            AppendInt(sb, "MinMarketPrice", _minMarketPrice);
            AppendInt(sb, "AvgMarketPrice", _avgMarketPrice);
            AppendInt(sb, "MaxMarketPrice", _maxMarketPrice);
            AppendInt(sb, "MinPctMarketAvg", _minPctMarketAvg);
            AppendInt(sb, "MaxPctMarketAvg", _maxPctMarketAvg);
            AppendInt(sb, "MinPctMarketValue", _minPctMarketValue);
            AppendInt(sb, "MaxPctMarketValue", _maxPctMarketValue);
            AppendInt(sb, "PctAvgMarketPrice", _pctAvgMarketPrice);
            AppendInt(sb, "MinComparableMarketPrice", _minComparableMarketPrice);
            AppendInt(sb, "MaxComparableMarketPrice", _maxComparableMarketPrice);
            AppendInt(sb, "NumListings", _numListings);
            AppendInt(sb, "NumComparableListings", _numComparableListings);
            AppendInt(sb, "AppraisalValue", AppraisalValue);
            AppendInt(sb, "EstimatedReconditioningCost", EstimatedReconditioningCost);
            AppendInt(sb, "TargetGrossProfit", TargetGrossProfit);
            AppendInt(sb, "NewPctAvgMarketPrice", newPctAvgMarketPrice);
            AppendInt(sb, "NewPrice", newPrice);

            return sb.ToString();
        }

        private static void AppendInt(StringBuilder sb, string key, int? value)
        {
            if (value.HasValue)
            {
                if (sb.Length > 0)
                    sb.Append("&");
                sb.Append(key).Append("=").Append(value.Value);
            }
        }

        public bool NotAnalyzed
        {
            get
            {
                return (NumComparableListings < 5 || PriceOutOfRange);
            }
        }

        public bool PriceOutOfRange
        {
            get
            {
                bool outOfRange = false;
                if (VehicleEntityTypeId == 1)
                {
                    int value = _newPrice.HasValue ? _newPrice.Value :
                        InventoryListPrice.HasValue ? InventoryListPrice.Value : 0;

                    outOfRange |= value < MinComparableMarketPrice.Value;
                    outOfRange |= value > MaxComparableMarketPrice.Value;
                }
                return outOfRange;
            }
        }

        #endregion

        #region Properties
        
        public int VehicleEntityTypeId
        {
            get { return _vehicleEntityTypeId; }
        }

        public int VehicleEntityId
        {
            get { return _vehicleEntityId; }
        }

        public int? InventoryListPrice
        {
            get { return _inventoryListPrice; }
        }

        public int? MinMarketPrice
        {
            get { return _minMarketPrice; }
        }

        public int? AvgMarketPrice
        {
            get { return _avgMarketPrice; }
        }

        public int? MaxMarketPrice
        {
            get { return _maxMarketPrice; }
        }

        public int? MinPctMarketAvg
        {
            get { return _minPctMarketAvg; }
        }

        public int? MaxPctMarketAvg
        {
            get { return _maxPctMarketAvg; }
        }

        public int? MinPctMarketValue
        {
            get { return _minPctMarketValue; }
        }

        public int? MaxPctMarketValue
        {
            get { return _maxPctMarketValue; }
        }

        public int? PctAvgMarketPrice
        {
            get { return _pctAvgMarketPrice; }
        }

        public int? MinComparableMarketPrice
        {
            get { return _minComparableMarketPrice; }
        }

        public int? MaxComparableMarketPrice
        {
            get { return _maxComparableMarketPrice; }
        }

        public int NumListings
        {
            get { return _numListings; }
        }

        public int NumComparableListings
        {
            get { return _numComparableListings; }
        }

        public int? AppraisalValue
        {
            get { return _appraisalValue; }
        }

        public int? EstimatedReconditioningCost
        {
            get { return _estimatedReconditioningCost; }
        }

        public int? TargetGrossProfit
        {
            get { return _targetGrossProfit; }
        }

        public int? NewPctAvgMarketPrice
        {
            get { return _newPctAvgMarketPrice; }
        }

        public int? NewPrice
        {
            get { return _newPrice; }
        }

        #endregion
    }
}