using System.Data;
using System.Text;
using System.Threading;
using Csla;
using Csla.Core;
using Csla.Validation;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.TextFragment
{
    public class TextFragment : BusinessBase<TextFragment>
    {
        #region Business Methods

        private int textFragmentID;
        private string name;
        private string text;
        private bool isDefault;

        public int TextFragmentID
        {
            get
            {
                CanReadProperty("TextFragmentID", true);
                return textFragmentID;
            }
            set
            {
                CanWriteProperty("TextFragmentID", true);
                if (textFragmentID != value)
                {
                    textFragmentID = value;
                    PropertyHasChanged("TextFragmentID");
                }
            }
        }
        public string Name
        {
            get
            {
                CanReadProperty("Name", true);
                return name;
            }
            set
            {
                CanWriteProperty("Name", true);
                if (name != value)
                {
                    name = value;
                    PropertyHasChanged("Name");
                }
            }
        }
        public string Text
        {
            get
            {
                CanReadProperty("Text", true);
                return text;
            }
            set
            {
                CanWriteProperty("Text", true);
                if (text != value)
                {
                    text = value;
                    PropertyHasChanged("Text");
                }
            }
        }
        public bool IsDefault
        {
            get
            {
                CanReadProperty("IsDefault", true);
                return isDefault;
            }
            set
            {
                CanWriteProperty("IsDefault", true);
                if (isDefault != value)
                {
                    isDefault = value;
                    PropertyHasChanged("IsDefault");
                }
            }
        }

        protected override object GetIdValue()
        {
            return textFragmentID;
        }

        #endregion Business Methods
        
        #region Validation Rules

        protected override void AddBusinessRules()
        {
            ValidationRules.AddRule(CommonRules.StringRequired, "Name");
            ValidationRules.AddRule(CommonRules.StringMaxLength, new CommonRules.MaxLengthRuleArgs("Name", 20));

            ValidationRules.AddRule(CommonRules.StringRequired, "Text");
            ValidationRules.AddRule(CommonRules.StringMaxLength, new CommonRules.MaxLengthRuleArgs("Text", 200));

            ValidationRules.AddRule<TextFragment>(NameNotEmpty, "Name");
            ValidationRules.AddRule<TextFragment>(TextNotEmpty, "Text");

        }

        private static bool NameNotEmpty<T>(T target, RuleArgs e) where T : TextFragment
        {
            if (string.IsNullOrEmpty(target.Name.Trim()))
            {
                e.Description = "The snippet name cannot be blank or empty.";
                return false;
            }
            else
            {
                return true;
            }
        }
        private static bool TextNotEmpty<T>(T target, RuleArgs e) where T : TextFragment
        {
            if (string.IsNullOrEmpty(target.Text.Trim()))
            {
                e.Description = "The snippet text cannot be blank or empty.";
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion

        #region Factory Methods

        public static TextFragment GetTextFragment(string ownerHandle, int textFragmentId)
        {
            return DataPortal.Fetch<TextFragment>(new Criteria(ownerHandle, textFragmentId));
        }

        public static TextFragment NewTextFragment()
        {
            return new TextFragment();
        }

        private TextFragment()
        {
            MarkAsChild();
        }

        public static TextFragment GetTextFragment(IDataRecord record)
        {
            return new TextFragment(record);
        }

        private TextFragment(IDataRecord record) : this()
        {
            Fetch(record);
            
            ValidationRules.CheckRules();
        }

        public class DataSource
        {
            public TextFragment Select(string ownerHandle, int textFragmentId)
            {
                if (string.IsNullOrEmpty(ownerHandle)) return null;

                if (textFragmentId == -1) return null;

                return GetTextFragment(ownerHandle, textFragmentId);
            }

            public int Insert(string ownerHandle, string name, string text)
            {
                if (string.IsNullOrEmpty(ownerHandle)) return 0;

                TextFragment fragment = NewTextFragment();
                fragment.Name = name;
                fragment.Text = text;

                if (fragment.IsValid)
                {
                    TextFragmentCollection collection = TextFragmentCollection.GetTextFragmentCollection(ownerHandle);

                    if (collection.Count == 0)
                    {
                        fragment.IsDefault = true;
                    }

                    if (collection.CanAddTextFragment)
                    {
                        collection.Assign(fragment);

                        collection.Save();

                        return 1;
                    }
                    else
                    {
                        throw new DataException("You can only have " + collection.MaxFragments + " text fragments");
                    }
                }
                else
                {
                    throw new DataException(InvalidFragmentText(fragment));
                }
            }

            public int Update(string ownerHandle, int textFragmentId, string name, string text)
            {
                if (string.IsNullOrEmpty(ownerHandle)) return 0;

                if (textFragmentId == -1) return 0;

                TextFragmentCollection collection = TextFragmentCollection.GetTextFragmentCollection(ownerHandle);

                TextFragment fragment = collection.GetItem(textFragmentId);
                fragment.Name = name;
                fragment.Text = text;

                if (fragment.IsValid)
                {
                    collection.Save();

                    return 1;
                }
                else
                {
                    throw new DataException(InvalidFragmentText(fragment));
                }
            }

            private static string InvalidFragmentText(BusinessBase fragment)
            {
                StringBuilder sb = new StringBuilder();

                foreach (BrokenRule rule in fragment.BrokenRulesCollection)
                {
                    if (sb.Length > 0) sb.Append("; ");

                    sb.Append(rule.Description);
                }

                return sb.ToString();
            }
        }

        #endregion

        #region Data Access

        protected class Criteria
        {
            private readonly string ownerHandle;
            private readonly int textFragmentId;

            public Criteria(string ownerHandle, int textFragmentId)
            {
                this.ownerHandle = ownerHandle;
                this.textFragmentId = textFragmentId;
            }

            public string OwnerHandle
            {
                get { return ownerHandle; }
            }

            public int TextFragmentId
            {
                get { return textFragmentId; }
            }
        }

        private static string CurrentLogon()
        {
            return Thread.CurrentPrincipal.Identity.Name;
        }

        protected void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Merchandising.TextFragment#Fetch";

                    command.AddParameterWithValue("OwnerHandle", DbType.String, false, criteria.OwnerHandle);
                    command.AddParameterWithValue("TextFragmentID", DbType.Int32, false, criteria.TextFragmentId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Fetch(reader);
                        }
                    }
                }
            }
        }

        protected void Fetch(IDataRecord reader)
        {
            textFragmentID = reader.GetInt32(reader.GetOrdinal("TextFragmentID"));
            name = reader.GetString(reader.GetOrdinal("Name"));
            text = reader.GetString(reader.GetOrdinal("Text"));
            isDefault = reader.GetBoolean(reader.GetOrdinal("IsDefault"));
            MarkOld();
        }

        internal void Insert(IDataConnection connection, IDbTransaction transaction, string ownerHandle)
        {
            if (!IsDirty) return;

            DoInsertUpdate(connection, transaction, false, "Merchandising.TextFragment#Insert", ownerHandle);

            MarkOld();
        }

        internal void Update(IDataConnection connection, IDbTransaction transaction, string ownerHandle)
        {
            if (!IsDirty) return;

            if (IsNew) return;

            DoInsertUpdate(connection, transaction, true, "Merchandising.TextFragment#Update", ownerHandle);

            MarkOld();
        }

        internal void DeleteSelf(IDataConnection connection, IDbTransaction transaction, string ownerHandle)
        {
            if (!IsDirty) return;

            if (IsNew) return;

            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = "Merchandising.TextFragment#Delete";
                command.CommandType = CommandType.StoredProcedure;

                command.Transaction = transaction;

                command.AddParameterWithValue("OwnerHandle", DbType.String, false, ownerHandle);
                command.AddParameterWithValue("TextFragmentID", DbType.Int32, false, TextFragmentID);
                command.AddParameterWithValue("Login", DbType.String, false, CurrentLogon());
                command.ExecuteNonQuery();
            }

            MarkNew();
        }

        private void DoInsertUpdate(IDbConnection connection, IDbTransaction transaction, bool update, string commandText, string ownerHandle)
        {
            using (IDataCommand command = new DataCommand(connection.CreateCommand()))
            {
                command.CommandText = commandText;
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;

                command.AddParameterWithValue("OwnerHandle", DbType.String, false, ownerHandle);
                command.AddParameterWithValue("Name", DbType.String, false, Name);
                command.AddParameterWithValue("Text", DbType.String, false, Text);
                command.AddParameterWithValue("IsDefault", DbType.String, false, IsDefault);
                command.AddParameterWithValue("Login", DbType.String, false, CurrentLogon());

                if (update)
                {
                    command.AddParameterWithValue("TextFragmentID", DbType.Int32, false, textFragmentID);
                    command.ExecuteNonQuery();
                }
                else
                {
                    IDataParameter outParam = command.AddOutParameter("TextFragmentID", DbType.Int32);
                    command.ExecuteNonQuery();
                    textFragmentID = int.Parse(outParam.Value.ToString());
                }
            }
        }

        #endregion
    }
}