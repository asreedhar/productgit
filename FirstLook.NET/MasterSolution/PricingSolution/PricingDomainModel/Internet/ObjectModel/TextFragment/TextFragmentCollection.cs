using System;
using System.Collections.Generic;
using System.Data;
using Csla;
using Csla.Validation;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.TextFragment
{
    public class TextFragmentCollection : BusinessListBase<TextFragmentCollection, FirstLook.Pricing.DomainModel.Internet.ObjectModel.TextFragment.TextFragment>
    {
        #region Business Methods

        private string ownerHandle;

        public string OwnerHandle
        {
            get { return ownerHandle; }
        }

        public int MaxFragments
        {
            get
            {
                return 8;
            }
        }

        public bool CanAddTextFragment
        {
            get
            {
                return Count < MaxFragments;
            }
        }

        public void IncreaseRank(int textFragmentID)
        {
            FirstLook.Pricing.DomainModel.Internet.ObjectModel.TextFragment.TextFragment item = GetItem(textFragmentID);

            int index = IndexOf(item);

            if (index > 0)
            {
                FirstLook.Pricing.DomainModel.Internet.ObjectModel.TextFragment.TextFragment temp = Items[index - 1];

                DataPortal.Execute(new SwapRankCommand(OwnerHandle, item.TextFragmentID, temp.TextFragmentID));

                Items[index - 1] = item;
                Items[index] = temp;
            }
        }

        public void DecreaseRank(int textFragmentID)
        {
            FirstLook.Pricing.DomainModel.Internet.ObjectModel.TextFragment.TextFragment item = GetItem(textFragmentID);

            int index = IndexOf(item);

            if (index + 1 < Count)
            {
                FirstLook.Pricing.DomainModel.Internet.ObjectModel.TextFragment.TextFragment temp = Items[index + 1];

                DataPortal.Execute(new SwapRankCommand(OwnerHandle, item.TextFragmentID, temp.TextFragmentID));

                Items[index + 1] = item;
                Items[index] = temp;
            }
        }

        public void SetDefault(int textFragmentID)
        {
            foreach (FirstLook.Pricing.DomainModel.Internet.ObjectModel.TextFragment.TextFragment textFragment in this)
            {
                if (textFragment.TextFragmentID.Equals(textFragmentID))
                {
                    textFragment.IsDefault = true;
                }
                else
                {
                    textFragment.IsDefault = false;
                }
            }
        }

        public FirstLook.Pricing.DomainModel.Internet.ObjectModel.TextFragment.TextFragment GetItem(int textFragmentId)
        {
            foreach (FirstLook.Pricing.DomainModel.Internet.ObjectModel.TextFragment.TextFragment textFragment in this)
                if (textFragment.TextFragmentID.Equals(textFragmentId))
                    return textFragment;
            return null;
        }

        public void Assign(FirstLook.Pricing.DomainModel.Internet.ObjectModel.TextFragment.TextFragment textFragment)
        {
            if (!Contains(textFragment.TextFragmentID))
            {
                if (!ContainsDeleted(textFragment.TextFragmentID))
                {
                    bool found = false;

                    foreach (FirstLook.Pricing.DomainModel.Internet.ObjectModel.TextFragment.TextFragment fragment in this)
                    {
                        if(fragment.Name.ToLower() == textFragment.Name.ToLower())
                        {
                            throw new InvalidOperationException("A tagline with the same name already exists.");
                        }
                        found |= fragment.IsDefault;
                    }

                    if (found && textFragment.IsDefault)
                    {
                        throw new InvalidOperationException("You can't have two default taglines.");
                    }

                    Add(textFragment);
                }
                else
                {
                    FirstLook.Pricing.DomainModel.Internet.ObjectModel.TextFragment.TextFragment deleted = GetDeletedItem(textFragment.TextFragmentID);
                    Add(deleted);
                    DeletedList.Remove(deleted);
                }
            }
            else
            {
                throw new InvalidOperationException("Text Fragment already assigned to Dealer");
            }
        }

        public void Remove(int textFragmentId)
        {
            foreach (FirstLook.Pricing.DomainModel.Internet.ObjectModel.TextFragment.TextFragment textFragment in this)
            {
                if (textFragment.TextFragmentID.Equals(textFragmentId))
                {
                    Remove(textFragment);
                    if(textFragment.IsDefault && this.Count > 0)
                    {
                        this[0].IsDefault = true;
                    }
                    break;
                }
            }
        }

        public bool Contains(int textFragmentId)
        {
            foreach (FirstLook.Pricing.DomainModel.Internet.ObjectModel.TextFragment.TextFragment textFragment in this)
                if (textFragment.TextFragmentID.Equals(textFragmentId))
                    return true;
            return false;
        }

        public bool ContainsDeleted(int textFragmentId)
        {
            foreach (FirstLook.Pricing.DomainModel.Internet.ObjectModel.TextFragment.TextFragment textFragment in DeletedList)
                if (textFragment.TextFragmentID.Equals(textFragmentId))
                    return true;
            return false;
        }

        private FirstLook.Pricing.DomainModel.Internet.ObjectModel.TextFragment.TextFragment GetDeletedItem(int textFragmentId)
        {
            foreach (FirstLook.Pricing.DomainModel.Internet.ObjectModel.TextFragment.TextFragment textFragment in DeletedList)
                if (textFragment.TextFragmentID.Equals(textFragmentId))
                    return textFragment;
            return null;
        }

        [Serializable]
        protected class SwapRankCommand : CommandBase
        {
            private readonly string ownerHandle;
            private readonly int fragmentOneId;
            private readonly int fragmentTwoId;

            public SwapRankCommand(string ownerHandle, int fragmentOneId, int fragmentTwoId)
            {
                this.ownerHandle = ownerHandle;
                this.fragmentOneId = fragmentOneId;
                this.fragmentTwoId = fragmentTwoId;
            }

            protected override void DataPortal_Execute()
            {
                using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
                {
                    connection.Open();

                    using (IDbTransaction transaction = connection.BeginTransaction())
                    {
                        using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.CommandText = "Merchandising.TextFragment#SwapRanks";
                            command.Transaction = transaction;
                            command.AddParameterWithValue("OwnerHandle", DbType.String, false, ownerHandle);
                            command.AddParameterWithValue("TextFragmentOneID", DbType.Int32, false, fragmentOneId);
                            command.AddParameterWithValue("TextFragmentTwoID", DbType.Int32, false, fragmentTwoId);
                            command.ExecuteNonQuery();
                        }

                        transaction.Commit();
                    }
                }
            }
        }

        #endregion

        #region Validation Rules

        public override bool IsValid
        {
            get
            {
                return
                    (
                        base.IsValid
                        && (Count <= MaxFragments)
                        && AllUniqueNames()
                        && IsOneDefault()
                    );
            }
        }

        protected bool AllUniqueNames()
        {
            List<string> names = new List<string>();
            foreach (FirstLook.Pricing.DomainModel.Internet.ObjectModel.TextFragment.TextFragment fragment in this)
            {
                if(names.Contains(fragment.Name))
                {
                    return false;
                }
                else 
                {
                    names.Add(fragment.Name);
                }
            }
            return true;
        }

        protected bool IsOneDefault()
        {
            if (Count == 0)
            {
                return true;
            }

            bool found = false;
            foreach (FirstLook.Pricing.DomainModel.Internet.ObjectModel.TextFragment.TextFragment fragment in this)
            {
                if (fragment.IsDefault)
                {
                    found = true;
                    break;
                }
            }

            return found;
        }

        #endregion

        #region Factory Methods

        public static TextFragmentCollection GetTextFragmentCollection(string ownerHandle)
        {
            return DataPortal.Fetch<TextFragmentCollection>(new FetchCriteria(ownerHandle));
        }

        public static TextFragmentCollection DeleteTextFragment(string ownerHandle, int textFragmentID)
        {
            return DataPortal.Fetch<TextFragmentCollection>(new FetchCriteria(ownerHandle));
        }
        
        public class DataSource
        {
            public TextFragmentCollection Select(string ownerHandle)
            {
                if (string.IsNullOrEmpty(ownerHandle)) return null;

                return GetTextFragmentCollection(ownerHandle);
            }

            public int Delete(string ownerHandle, int textFragmentId)
            {
                if (string.IsNullOrEmpty(ownerHandle)) return 0;

                if (textFragmentId == -1) return 0;

                TextFragmentCollection collection = GetTextFragmentCollection(ownerHandle);

                collection.Remove(textFragmentId);

                collection.Save();

                return 1;
            }
        }

        #endregion

        #region Data Access

        [Serializable]
        private class FetchCriteria
        {
            private readonly string ownerHandle;

            public FetchCriteria(string ownerHandle)
            {
                this.ownerHandle = ownerHandle;
            }

            public string OwnerHandle
            {
                get { return ownerHandle; }
            }
        }

        [Transactional(TransactionalTypes.Manual)]
        private void DataPortal_Fetch(FetchCriteria criteria)
        {
            RaiseListChangedEvents = false;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Merchandising.TextFragmentCollection#Fetch";

                    command.AddParameterWithValue("OwnerHandle", DbType.String, false, criteria.OwnerHandle);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        ownerHandle = criteria.OwnerHandle;

                        while (reader.Read())
                        {
                            FirstLook.Pricing.DomainModel.Internet.ObjectModel.TextFragment.TextFragment tf = FirstLook.Pricing.DomainModel.Internet.ObjectModel.TextFragment.TextFragment.GetTextFragment(reader);
                            Add(tf);
                        }
                    }
                }
            }

            RaiseListChangedEvents = true;
        }


        [Transactional(TransactionalTypes.Manual)]
        protected override void DataPortal_Update()
        {
            if (!IsDirty) return;

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                {
                    RaiseListChangedEvents = false;

                    foreach (FirstLook.Pricing.DomainModel.Internet.ObjectModel.TextFragment.TextFragment item in DeletedList)
                    {
                        item.DeleteSelf(connection, transaction, ownerHandle);
                    }
                    DeletedList.Clear();

                    // add/update any current child objects

                    foreach (FirstLook.Pricing.DomainModel.Internet.ObjectModel.TextFragment.TextFragment item in this)
                    {
                        if (item.IsNew)
                        {
                            item.Insert(connection, transaction, ownerHandle);
                        }
                        else
                        {
                            item.Update(connection, transaction, ownerHandle);
                        }
                    }

                    RaiseListChangedEvents = true;

                    transaction.Commit();
                }
            }
        }

        #endregion
    }
}