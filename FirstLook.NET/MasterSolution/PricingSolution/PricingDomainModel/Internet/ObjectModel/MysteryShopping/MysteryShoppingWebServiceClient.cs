﻿using FirstLook.Pricing.DomainModel.Commands.Impl;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.MysteryShopping
{
    public class MysteryShoppingWebServiceClient : IMysteryShoppingWebServiceClient
    {

        public string GetCarSoupUrl(string ownerHandle, string searchHandle, string vehicleHandle, int searchRadius, int modelYear)
        {
            return MysteryShoppingUrlBuilder.CarSoupUrl(ownerHandle, searchHandle, vehicleHandle, modelYear, searchRadius);
        }

        public string GetCarsDotComUrl(string ownerHandle, string searchHandle, string vehicleHandle, int searchRadius)
        {
            return MysteryShoppingUrlBuilder.CarsUrl(ownerHandle, searchHandle, vehicleHandle, searchRadius);
        }

        public string GetAutoTraderUrl(string ownerHandle, string searchHandle, string vehicleHandle, int searchRadius, int modelYear)
        {
            return MysteryShoppingUrlBuilder.AutoTraderUrl(ownerHandle, searchHandle, vehicleHandle, modelYear, searchRadius);
        }
    }

    
}
