﻿
namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.MysteryShopping
{
    public interface IMysteryShoppingWebServiceClient
    {
        string GetAutoTraderUrl(string ownerHandle, string searchHandle, string vehicleHandle, int searchRadius, int modelYear);

        string GetCarsDotComUrl(string ownerHandle, string searchHandle, string vehicleHandle, int searchRadius);

        string GetCarSoupUrl(string ownerHandle, string searchHandle, string vehicleHandle, int searchRadius, int modelYear);
    }
}
