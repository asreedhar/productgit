﻿using System.Configuration;


namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.MysteryShopping
{
    public static class MysteryShoppingConfiguration
    {
        public static string CarsDotComUrl() {
            string text = string.Empty;

            try {
                string value = ConfigurationManager.AppSettings["cars_dot_com_url"];
                if (!string.IsNullOrEmpty(value)) text = value; 

            } catch {
                // ignore error!
            }

            return text;
        }

        public static string CarSoupUrl()
        {
            string text = string.Empty;

            try
            {
                string value = ConfigurationManager.AppSettings["car_soup_url"];
                if (!string.IsNullOrEmpty(value)) text = value;

            }
            catch
            {
                // ignore error!
            }

            return text;
        }

        public static string AutoTraderUrl()
        {
            string text = string.Empty;

            try
            {
                string value = ConfigurationManager.AppSettings["auto_trader_url"];
                if (!string.IsNullOrEmpty(value)) text = value;

            }
            catch
            {
                // ignore error!
            }

            return text;
        }
    }
}
