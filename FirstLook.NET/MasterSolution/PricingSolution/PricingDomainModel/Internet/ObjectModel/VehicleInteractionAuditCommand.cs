using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel
{
    [Serializable]
    public class VehicleInteractionAuditCommand : CommandBase
    {
        enum VehicleInteractionType
        {
            Review = 3,
            Reprice = 4
        }

        public static void Review(string ownerHandle, string vehicleHandle, string searchHandle, string userName)
        {
            VehicleInteractionAuditCommand command = new VehicleInteractionAuditCommand(ownerHandle, vehicleHandle, searchHandle, userName);

            DataPortal.Execute(command);
        }

        public static void Reprice(string ownerHandle, string vehicleHandle, string searchHandle, string userName, int oldListPrice, int newListPrice)
        {
            VehicleInteractionAuditCommand command = new VehicleInteractionAuditCommand(ownerHandle, vehicleHandle, searchHandle, userName, oldListPrice, newListPrice);

            DataPortal.Execute(command);
        }

        private readonly VehicleInteractionType type;
        private readonly string ownerHandle;
        private readonly string vehicleHandle;
        private readonly string searchHandle;
        private readonly string userName;
        private readonly Nullable<int> oldListPrice;
        private readonly Nullable<int> newListPrice;

        private VehicleInteractionAuditCommand(string ownerHandle, string vehicleHandle, string searchHandle, string userName)
        {
            type = VehicleInteractionType.Review;
            this.ownerHandle = ownerHandle;
            this.vehicleHandle = vehicleHandle;
            this.searchHandle = searchHandle;
            this.userName = userName;
        }

        private VehicleInteractionAuditCommand(string ownerHandle, string vehicleHandle, string searchHandle, string userName, Nullable<int> oldListPrice, Nullable<int> newListPrice)
        {
            type = VehicleInteractionType.Reprice;
            this.ownerHandle = ownerHandle;
            this.vehicleHandle = vehicleHandle;
            this.searchHandle = searchHandle;
            this.userName = userName;
            this.oldListPrice = oldListPrice;
            this.newListPrice = newListPrice;
        }

        protected override void DataPortal_Execute()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Pricing.VehicleMarketHistory#Create";
                    command.AddParameterWithValue("OwnerHandle", DbType.String, false, ownerHandle);
                    command.AddParameterWithValue("VehicleHandle", DbType.String, false, vehicleHandle);
                    command.AddParameterWithValue("SearchHandle", DbType.String, false, searchHandle);
                    command.AddParameterWithValue("Login", DbType.String, false, userName);
                    command.AddParameterWithValue("VehicleMarketHistoryEntryTypeID", DbType.Int32, false, type);

                    if (oldListPrice.HasValue)
                        command.AddParameterWithValue("OldListPrice", DbType.Int32, true, oldListPrice.Value);
                    else
                        command.AddParameterWithValue("OldListPrice", DbType.Int32, true, DBNull.Value);

                    if (newListPrice.HasValue)
                        command.AddParameterWithValue("NewListPrice", DbType.Int32, true, newListPrice.Value);
                    else
                        command.AddParameterWithValue("NewListPrice", DbType.Int32, true, DBNull.Value);

                    command.ExecuteNonQuery();
                }
            }
        }
    }
}