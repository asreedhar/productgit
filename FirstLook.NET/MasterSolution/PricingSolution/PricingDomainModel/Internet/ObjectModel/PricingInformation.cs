using System;
using System.Data;
using Csla;
using FirstLook.Common.Data;
using FirstLook.DomainModel.Oltp;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.KBB;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel
{
    [Serializable]
    public class PricingInformation : ReadOnlyBase<PricingInformation>
    {
        #region Business Methods

        private Criteria _id;

        protected override object GetIdValue()
        {
            return _id;
        }

        private bool? _hasKbbAsBook;
        private bool? _hasKbbConsumerTool;

        private VehicleType _vehicleType;
        private int? _mileage;
        private int? _dealerId;

        public string StockNumber { get; private set; }

        public string Vin { get; private set; }

        public int? UnitCost { get; private set; }

        public int? ListPrice { get; private set; }

        public int ModelYear { get; private set; }

        public int VehicleCatalogId { get; private set; }

        public int GroupingDescriptionId { get; private set; }

        public bool HasMarketPrice { get; private set; }

        public int? MinMarketPrice { get; private set; }

        public int? AvgMarketPrice { get; private set; }

        public int? MaxMarketPrice { get; private set; }

        public int? SearchRadius { get; private set; }

        public bool HasKbbAsBook
        {
            get
            {
                if (!_hasKbbAsBook.HasValue && IsValidDealer())
                {
                    _hasKbbAsBook = KelleyBlueBookClient.HasKbbAsBook(DealerId);
                }

                return _hasKbbAsBook.GetValueOrDefault();
            }
        }

        public bool HasKbbConsumerTool
        {
            get
            {
                if (!_hasKbbConsumerTool.HasValue && IsValidDealer())
                {
                    _hasKbbConsumerTool = KelleyBlueBookClient.HasKbbConsumerTool(DealerId);
                }

                return _hasKbbConsumerTool.GetValueOrDefault();
            }
        }

        public VehicleType VehicleType
        {
            get
            {
                if (_vehicleType == VehicleType.Unknown)
                {
                    _vehicleType = VehicleTypeCommand.Execute(_id.VehicleHandle);
                }

                return _vehicleType;   
            }
        }

        public int Mileage
        {
            get
            {
                if (!_mileage.HasValue)
                {
                    switch (VehicleType)
                    {
                        case VehicleType.Inventory:
                            _mileage = Inventory.GetInventory(_id.OwnerHandle, _id.VehicleHandle).Mileage.GetValueOrDefault();
                            break;
                        case VehicleType.Appraisal:
                            _mileage = Appraisal.GetAppraisal(_id.OwnerHandle, _id.VehicleHandle).Mileage.GetValueOrDefault();
                            break;
                    }
                }

                return _mileage.GetValueOrDefault();
            }
        }

        public bool HasKbbPrice { get; private set; }

        public bool HasNadaPrice { get; private set; }

        public bool HasEdmundsPrice { get; private set; }

        public bool HasJdPowerPrice { get; private set; }

        public bool HasStorePrice { get; private set; }

        public int? KbbPrice { get; private set; }

        public bool? KbbPriceAccurate { get; private set; }

        public string KbbPublicationDate { get; private set; }

        public int? NadaPrice { get; private set; }

        public bool? NadaPriceAccurate { get; private set; }

        public string NadaPublicationDate { get; private set; }

        public int? EdmundsPrice { get; private set; }

        public int? JdPowerPrice { get; private set; }

        public int? JdPowerUnits { get; private set; }

        public DateTime? JdPowerPeriodBeginDate { get; private set; }

        public DateTime? JdPowerPeriodEndDate { get; private set; }

        public int? StorePrice { get; private set; }

        public int? StoreProfit { get; private set; }

        public int? StoreUnits { get; private set; }

        public DateTime? StorePeriodBeginDate { get; private set; }

        public DateTime? StorePeriodEndDate { get; private set; }

        public int DealerId
        {
            get
            {
                if (!_dealerId.HasValue)
                {
                    //Need to use outside of ASP.NET. Changed from using Identity.GetDealerId
                    Owner owner = Owner.GetOwner(_id.OwnerHandle);
                    _dealerId = owner.OwnerEntityId;
                }

                return _dealerId.GetValueOrDefault();
            }
        }

        private bool IsValidDealer()
        {
            //Need to use outside of ASP.NET. Changed from using Identity.IsDealer
            Owner owner = Owner.GetOwner(_id.OwnerHandle);
            return owner.OwnerEntityType == OwnerEntityType.Dealer;
        }

        #endregion

        #region Factory Methods

        public class DataSource
        {
            public PricingInformation Select(string ownerHandle, string vehicleHandle, string searchHandle, int searchTypeId)
            {
                return DataPortal.Fetch<PricingInformation>(new Criteria(ownerHandle, vehicleHandle, searchHandle, searchTypeId));
            }
        }

        [Serializable]
        public class Criteria
        {
            public Criteria(string ownerHandle, string vehicleHandle, string searchHandle, int searchTypeId)
            {
                OwnerHandle = ownerHandle;
                VehicleHandle = vehicleHandle;
                SearchHandle = searchHandle;
                SearchTypeId = searchTypeId;
            }

            public string OwnerHandle { get; private set; }

            public string VehicleHandle { get; private set; }

            public string SearchHandle { get; private set; }

            public int SearchTypeId { get; private set; }
        }

        #endregion

        #region Data Access

        protected void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandText = "Pricing.PricingInformation";
                    command.CommandType = CommandType.StoredProcedure;

                    command.AddParameterWithValue("OwnerHandle", DbType.String, false, criteria.OwnerHandle);
                    command.AddParameterWithValue("VehicleHandle", DbType.String, false, criteria.VehicleHandle);
                    command.AddParameterWithValue("SearchHandle", DbType.String, false, criteria.SearchHandle);
                    command.AddParameterWithValue("SearchTypeID", DbType.Int32, false, criteria.SearchTypeId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            _id = criteria;

                            Fetch(reader);
                        }
                    }
                }
            }
        }

        private void Fetch(IDataRecord record)
        {
            StockNumber = GetString(record,"StockNumber");
            Vin = GetString(record, "VIN");
            ListPrice = GetNullableInt32(record, "ListPrice");
            UnitCost = GetNullableInt32(record, "UnitCost");
            ModelYear = GetNullableInt32(record, "ModelYear").GetValueOrDefault();
            VehicleCatalogId = GetNullableInt32(record, "VehicleCatalogID").GetValueOrDefault();
            GroupingDescriptionId = GetNullableInt32(record, "GroupingDescriptionID").GetValueOrDefault();

            HasMarketPrice = GetNullableBoolean(record, "HasMarketPrice").GetValueOrDefault();
            if (HasMarketPrice)
            {
                MinMarketPrice = GetNullableInt32(record, "MinMarketPrice");
                AvgMarketPrice = GetNullableInt32(record, "AvgMarketPrice");
                MaxMarketPrice = GetNullableInt32(record, "MaxMarketPrice");
                SearchRadius = GetNullableInt32(record, "SearchRadius");
            }

            HasKbbPrice = GetNullableBoolean(record, "HasKbbPrice").GetValueOrDefault();
            if (HasKbbPrice)
            {
                KbbPrice = GetNullableInt32(record, "KbbPrice");
                KbbPriceAccurate = GetNullableBoolean(record, "HasAccurateKbbPrice");
                KbbPublicationDate = GetString(record, "KbbPublicationDate");
            }
            
            HasNadaPrice = GetNullableBoolean(record, "HasNadaPrice").GetValueOrDefault();
            if (HasNadaPrice)
            {
                NadaPrice = GetNullableInt32(record, "NadaPrice");
                NadaPriceAccurate = GetNullableBoolean(record, "HasAccurateNadaPrice");
                NadaPublicationDate = GetString(record, "NadaPublicationDate");
            }

            HasEdmundsPrice = GetNullableBoolean(record, "HasEdmundsPrice").GetValueOrDefault();
            if (HasEdmundsPrice)
            {
                EdmundsPrice = GetNullableInt32(record, "EdmundsPrice");
            }

            HasJdPowerPrice = GetNullableBoolean(record, "HasJDPowerPrice").GetValueOrDefault();
            if (HasJdPowerPrice)
            {
                JdPowerPrice = GetNullableInt32(record, "JDPowerAvgSellingPrice");
                JdPowerUnits = GetNullableInt32(record, "JDPowerUnits");
                JdPowerPeriodBeginDate = GetNullableDateTime(record, "JDPowerPeriodBeginDate");
                JdPowerPeriodEndDate = GetNullableDateTime(record, "JDPowerPeriodEndDate");
            }
            
            HasStorePrice = GetNullableBoolean(record, "HasStorePrice").GetValueOrDefault();
            if (HasStorePrice)
            {
                StorePrice = GetNullableInt32(record, "StoreAvgSellingPrice");
                StoreUnits = GetNullableInt32(record, "StoreUnits");
                StoreProfit = GetNullableInt32(record, "StoreAvgGrossProfit");
                StorePeriodBeginDate = GetNullableDateTime(record, "StorePeriodBeginDate");
                StorePeriodEndDate = GetNullableDateTime(record, "StorePeriodEndDate");
            }

            if (!HasKbbPrice)
            {
                KelleyBlueBookReport kbbReport = KelleyBlueBookReportCommand.TryFetch(_id.OwnerHandle, _id.VehicleHandle);
                if (kbbReport != null && kbbReport.Valuations.Count > 0)
                {
                    HasKbbPrice = true;
                    KbbPrice =
                        kbbReport.Valuations.GetValue(KelleyBlueBookCategory.Retail,
                                                      KelleyBlueBookCondition.NotApplicable).Value;
                }
            }
        }

        #region IDataRecord Helper Methods

        private static string GetString(IDataRecord record, string columnName)
        {
            string value = string.Empty;

            int ordinal = record.GetOrdinal(columnName);

            if (!record.IsDBNull(ordinal))
            {
                value = record.GetString(ordinal);
            }

            return value;
        }

        private static int? GetNullableInt32(IDataRecord record, string columnName)
        {
            int? value = null;

            int ordinal = record.GetOrdinal(columnName);

            if (!record.IsDBNull(ordinal))
            {
                value = record.GetInt32(ordinal);
            }

            return value;
        }

        private static bool? GetNullableBoolean(IDataRecord record, string columnName)
        {
            bool? value = null;

            int ordinal = record.GetOrdinal(columnName);

            if (!record.IsDBNull(ordinal))
            {
                value = record.GetBoolean(ordinal);
            }

            return value;
        }

        private static DateTime? GetNullableDateTime(IDataRecord record, string columnName)
        {
            DateTime? value = null;

            int ordinal = record.GetOrdinal(columnName);

            if (!record.IsDBNull(ordinal))
            {
                value = record.GetDateTime(ordinal);
            }

            return value;
        }

        #endregion        

        #endregion
    }
}
