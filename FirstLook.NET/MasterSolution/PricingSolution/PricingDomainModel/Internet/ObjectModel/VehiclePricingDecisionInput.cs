using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel
{
    public class VehiclePricingDecisionInput : BusinessBase<VehiclePricingDecisionInput>
    {
        private const string ObjectNotFoundMessage = "Object not found!";

        #region Business Methods

        private string ownerHandle;
        private string vehicleHandle;

        private Nullable<DateTime> categorizationOverrideExpiryDate;
        private Nullable<int> appraisalValue;
        private Nullable<int> targetGrossProfit;
        private Nullable<int> estimatedAdditionalCosts;

        public string OwnerHandle
        {
            get { return ownerHandle; }
        }

        public string VehicleHandle
        {
            get { return vehicleHandle; }
        }

        public Nullable<int> AppraisalValue
        {
            get { return appraisalValue; }
            set
            {
                if (Nullable.Compare(appraisalValue, value) != 0)
                {
                    appraisalValue = value;
                    PropertyHasChanged("AppraisalValue");
                }
            }
        }

        public Nullable<int> TargetGrossProfit
        {
            get { return targetGrossProfit; }
            set
            {
                if (Nullable.Compare(targetGrossProfit, value) != 0)
                {
                    targetGrossProfit = value;
                    PropertyHasChanged("TargetGrossProfit");
                }
            }
        }

        public Nullable<int> EstimatedAdditionalCosts
        {
            get { return estimatedAdditionalCosts; }
            set
            {
                if (Nullable.Compare(estimatedAdditionalCosts, value) != 0)
                {
                    estimatedAdditionalCosts = value;
                    PropertyHasChanged("EstimatedAdditionalCosts");
                }
            }
        }

        public Nullable<DateTime> CategorizationOverrideExpiryDate
        {
            get { return categorizationOverrideExpiryDate; }
            protected set
            {
                if (Nullable.Compare(categorizationOverrideExpiryDate, value) != 0)
                {
                    categorizationOverrideExpiryDate = value;
                    PropertyHasChanged("CategorizationOverrideExpiryDate");
                }
            }
        }

        public bool IsCategorizationOverridden
        {
            get { return categorizationOverrideExpiryDate.HasValue; }
        }

        public void OverrideCategorization(DateTime expiryDate)
        {
            CategorizationOverrideExpiryDate = expiryDate;
        }

        public void ClearCategorizationOverride()
        {
            CategorizationOverrideExpiryDate = null;
        }

        protected override object GetIdValue()
        {
            return new Criteria(OwnerHandle, VehicleHandle);
        }

        #endregion

        #region Data Access
        
        [Serializable]
        private class Criteria : IEquatable<Criteria>
        {
            private readonly string ownerHandle;
            private readonly string vehicleHandle;

            public Criteria(string ownerHandle, string vehicleHandle)
            {
                this.ownerHandle = ownerHandle;
                this.vehicleHandle = vehicleHandle;
            }

            public string OwnerHandle
            {
                get { return ownerHandle; }
            }

            public string VehicleHandle
            {
                get { return vehicleHandle; }
            }

            public bool Equals(Criteria criteria)
            {
                if (criteria == null) return false;
                return Equals(ownerHandle, criteria.ownerHandle) && Equals(vehicleHandle, criteria.vehicleHandle);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(this, obj)) return true;
                return Equals(obj as Criteria);
            }

            public override int GetHashCode()
            {
                return ownerHandle.GetHashCode() + 29 * vehicleHandle.GetHashCode();
            }
        }

        [RunLocal]
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Create(Criteria criteria)
        {
            ownerHandle = criteria.OwnerHandle;
            vehicleHandle = criteria.VehicleHandle;
            categorizationOverrideExpiryDate = null;
            appraisalValue = null;
            targetGrossProfit = null;
            estimatedAdditionalCosts = null;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Pricing.VehiclePricingDecisionInput#Fetch";
                    command.AddParameterWithValue("OwnerHandle", DbType.String, false, criteria.OwnerHandle);
                    command.AddParameterWithValue("VehicleHandle", DbType.String, false, criteria.VehicleHandle);

                    ownerHandle = criteria.OwnerHandle;
                    vehicleHandle = criteria.VehicleHandle;

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            int ordinal = reader.GetOrdinal("CategorizationOverrideExpiryDate");
                            if (reader.IsDBNull(ordinal))
                                categorizationOverrideExpiryDate = null;
                            else
                                categorizationOverrideExpiryDate = reader.GetDateTime(ordinal);

                            ordinal = reader.GetOrdinal("AppraisalValue");
                            if (reader.IsDBNull(ordinal))
                                appraisalValue = null;
                            else
                                appraisalValue = reader.GetInt32(ordinal);

                            ordinal = reader.GetOrdinal("TargetGrossProfit");
                            if (reader.IsDBNull(ordinal))
                                targetGrossProfit = null;
                            else
                                targetGrossProfit = reader.GetInt32(ordinal);

                            ordinal = reader.GetOrdinal("EstimatedAdditionalCosts");
                            if (reader.IsDBNull(ordinal))
                                estimatedAdditionalCosts = null;
                            else
                                estimatedAdditionalCosts = reader.GetInt32(ordinal);
                        }
                        else
                        {
                            throw new DataException(ObjectNotFoundMessage);
                        }
                    }
                }
            }

            MarkOld();
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        protected override void DataPortal_Insert()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandText = "Pricing.VehiclePricingDecisionInput#Insert";

                    DoInsertUpdate(command);
                }
            }

            MarkOld();
        }

        [Transactional(TransactionalTypes.TransactionScope)]
        protected override void DataPortal_Update()
        {
            if (IsDirty)
            {
                using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
                {
                    connection.Open();

                    using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                    {
                        command.CommandText = "Pricing.VehiclePricingDecisionInput#Update";

                        DoInsertUpdate(command);
                    }
                }

                MarkOld();
            }
        }

        private void DoInsertUpdate(IDataCommand command)
        {
            command.CommandType = CommandType.StoredProcedure;

            command.AddParameterWithValue("OwnerHandle", DbType.String, false, ownerHandle);
            command.AddParameterWithValue("VehicleHandle", DbType.String, false, vehicleHandle);
            if (CategorizationOverrideExpiryDate.HasValue)
            {
                command.AddParameterWithValue("CategorizationOverrideExpiryDate", DbType.DateTime, true, CategorizationOverrideExpiryDate);
            }
            else
            {
                command.AddParameterWithValue("CategorizationOverrideExpiryDate", DbType.DateTime, true, DBNull.Value);
            }
            if (AppraisalValue.HasValue)
            {
                command.AddParameterWithValue("AppraisalValue", DbType.Int32, true, AppraisalValue);
            }
            else
            {
                command.AddParameterWithValue("AppraisalValue", DbType.Int32, true, DBNull.Value);
            }
            if (TargetGrossProfit.HasValue)
            {
                command.AddParameterWithValue("TargetGrossProfit", DbType.Int32, true, TargetGrossProfit);
            }
            else
            {
                command.AddParameterWithValue("TargetGrossProfit", DbType.Int32, true, DBNull.Value);
            }
            if (EstimatedAdditionalCosts.HasValue)
            {
                command.AddParameterWithValue("EstimatedAdditionalCosts", DbType.Int32, true, EstimatedAdditionalCosts);
            }
            else
            {
                command.AddParameterWithValue("EstimatedAdditionalCosts", DbType.Int32, true, DBNull.Value);
            }

            command.ExecuteNonQuery();
        }

        #endregion

        #region Factory Methods

        public static VehiclePricingDecisionInput GetVehiclePricingDecisionInput(string ownerHandle, string vehicleHandle)
        {
            Criteria criteria = new Criteria(ownerHandle, vehicleHandle);
            try
            {
                return DataPortal.Fetch<VehiclePricingDecisionInput>(criteria);
            }
            catch (DataPortalException e)
            {
                if (e.BusinessException.Message.Equals(ObjectNotFoundMessage))
                {
                    return DataPortal.Create<VehiclePricingDecisionInput>(criteria);
                }
                else
                {
                    throw;
                }
            }
        }

        private VehiclePricingDecisionInput()
        {
            /* force use of factory methods */
        }

        #endregion
    }
}