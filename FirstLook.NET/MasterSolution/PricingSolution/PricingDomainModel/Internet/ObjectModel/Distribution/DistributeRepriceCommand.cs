﻿using System;
using System.Collections.Generic;
using FirstLook.Distribution.WebService.Client.Distributor;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Distribution
{
    [Serializable]
    public class DistributeRepriceCommand
    {
        public static void Execute(int dealerId, int inventoryId, int newListPrice)
        {
            List<BodyPart> bodyParts = new List<BodyPart>();

            Price newPrice = new Price {PriceType = PriceType.Internet, Value = newListPrice};

            bodyParts.Add(newPrice);

            DistributeCommand.Execute(bodyParts, dealerId, inventoryId);
        }

        public static void Execute(string ownerHandle, string vehicleHandle, int newListPrice)
        {
            int dealerId = Identity.GetDealerId(ownerHandle);
            int vehicleId = VehicleIdentityCommand.Execute(vehicleHandle);
            Execute(dealerId, vehicleId, newListPrice);
        }
    }
}
