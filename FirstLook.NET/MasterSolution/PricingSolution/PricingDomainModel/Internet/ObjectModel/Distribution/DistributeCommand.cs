﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Csla;
//using Distribution.WebService.Client.Distributor;
using FirstLook.Distribution.WebService.Client.Distributor;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Distribution
{
    [Serializable]
    public class DistributeCommand : CommandBase
    {
        private static Distributor GetService()
        {
            var userIdentity = new UserIdentity {UserName = Thread.CurrentPrincipal.Identity.Name};

            return new Distributor { UserIdentityValue = userIdentity };
        }

        public static void Execute(IEnumerable<BodyPart> bodyParts, int dealerId, int inventoryId)
        {
            DistributeCommand command = new DistributeCommand(bodyParts, dealerId, inventoryId);

            DataPortal.Execute(command);
        }

        private readonly IEnumerable<BodyPart> _bodyParts;
        private readonly int _dealerId;
        private readonly int _inventoryId;

        private DistributeCommand(IEnumerable<BodyPart> bodyParts, int dealerID, int inventoryID)
        {
            _bodyParts = bodyParts;
            _dealerId = dealerID;
            _inventoryId = inventoryID;
        }


        protected override void DataPortal_Execute()
        {
            var service = GetService();

            var message = new Message
                                  {
                                      From = new Dealer {Id = _dealerId},
                                      Subject = new global::FirstLook.Distribution.WebService.Client.Distributor.Vehicle
                                                    {
                                                        VehicleType =
                                                            VehicleType.
                                                            Inventory,
                                                        Id = _inventoryId
                                                    },
                                      Body = new MessageBody {Parts = _bodyParts.ToArray()}
                                  };

            try
            {
                service.Distribute(message);
            }
            catch (Exception exception)
            {
                bool isNetworkError = (exception is SocketException || exception is WebException);

                if (!isNetworkError)
                {
                    throw;
                }
            }
        }
    }
}
