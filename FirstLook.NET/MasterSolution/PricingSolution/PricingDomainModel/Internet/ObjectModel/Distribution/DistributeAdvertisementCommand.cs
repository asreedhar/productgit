﻿using System.Collections.Generic;
using FirstLook.Distribution.WebService.Client.Distributor;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.Distribution
{
    public class DistributeAdvertisementCommand
    {
        public static void Execute(int dealerId, int inventoryId, string adText, int listPrice)
        {
            List<BodyPart> bodyParts = new List<BodyPart>();

            TextContent textContent = new TextContent {Text = adText};

            Advertisement advertisement = new Advertisement {Contents = new Content[] {textContent}};

            Price newPrice = new Price { PriceType = PriceType.Internet, Value = listPrice };

            bodyParts.Add(advertisement);

            bodyParts.Add(newPrice);

            DistributeCommand.Execute(bodyParts, dealerId, inventoryId);
        }

        public static void Execute(string ownerHandle, string vehicleHandle, string adText, int listPrice)
        {
            int dealerId = Identity.GetDealerId(ownerHandle);
            int vehicleId = VehicleIdentityCommand.Execute(vehicleHandle);
            Execute(dealerId, vehicleId, adText, listPrice);
        }
    }
}
