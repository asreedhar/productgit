using System;


namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.KBB
{
    public class KelleyBlueBookPublication
    {
        private readonly string region;
        private readonly DateTime validFrom;
        private readonly DateTime validUntil;

        public string Region
        {
            get { return region; }
        }
        public DateTime ValidFrom
        {
            get { return validFrom; }
        }
        public DateTime ValidUntil
        {
            get { return validUntil; }
        }


        public KelleyBlueBookPublication(string region, DateTime validFrom, DateTime validUntil)
        {
            this.region = region;
            this.validUntil = validUntil;
            this.validFrom = validFrom;
        }
    }
}