namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.KBB
{
    public class KelleyBlueBookValuation
    {
        private readonly KelleyBlueBookCondition condition;
        private readonly KelleyBlueBookCategory category;
        private readonly int value;


        public KelleyBlueBookCondition Condition
        {
            get { return condition; }
        }
        public KelleyBlueBookCategory Category
        {
            get { return category; }
        }
        public int Value
        {
            get { return value; }
        }


        public KelleyBlueBookValuation(KelleyBlueBookCondition condition, KelleyBlueBookCategory category, int value)
        {
            this.condition = condition;
            this.value = value;
            this.category = category;
        }
    }
}