namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.KBB
{
    public enum KelleyBlueBookCategory
    {
        Unknown,
        TradeIn,
        Retail,
        Wholesale
    }
}