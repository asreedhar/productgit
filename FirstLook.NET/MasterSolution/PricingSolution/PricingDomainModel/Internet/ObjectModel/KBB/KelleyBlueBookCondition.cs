namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.KBB
{
    public enum KelleyBlueBookCondition
    {
        Unknown,
        NotApplicable,
        Excellent,
        Good,
        Fair
    }
}