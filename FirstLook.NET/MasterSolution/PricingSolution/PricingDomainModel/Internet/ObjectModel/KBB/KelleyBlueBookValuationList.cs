using System.Collections.Generic;


namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.KBB
{
    public class KelleyBlueBookValuationList : List<KelleyBlueBookValuation>
    {
        public KelleyBlueBookValuation GetValue(KelleyBlueBookCategory category, KelleyBlueBookCondition condition)
        {
            return Find(delegate(KelleyBlueBookValuation v) { return v.Category == category && v.Condition == condition; });
        }
    }
}