namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.KBB
{
    public class KelleyBlueBookReport
    {
        private readonly int mileage;
        private readonly string vin;
        private readonly KelleyBlueBookCondition condition;
        private readonly KelleyBlueBookValuationList valuations;
        private readonly KelleyBlueBookPublication publication;



        public int Mileage
        {
            get { return mileage; }
        }
        public string Vin
        {
            get { return vin; }
        }
        public KelleyBlueBookCondition Condition
        {
            get { return condition; }
        }
        public KelleyBlueBookValuationList Valuations
        {
            get { return valuations; }
        }
        public KelleyBlueBookPublication Publication
        {
            get { return publication; }
        }


        public KelleyBlueBookReport(int mileage, string vin, KelleyBlueBookCondition condition, KelleyBlueBookValuationList valuations, KelleyBlueBookPublication publication)
        {
            this.mileage = mileage;
            this.condition = condition;
            this.publication = publication;
            this.valuations = valuations;
            this.vin = vin;
        }
    }
}