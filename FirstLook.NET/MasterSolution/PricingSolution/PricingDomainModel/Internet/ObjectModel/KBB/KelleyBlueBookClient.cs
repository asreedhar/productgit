using System;
using System.Data;
using System.IO;
using System.Net;
using System.Xml;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.KBB
{
    public class KelleyBlueBookClient
    {
        private readonly string url;

        public KelleyBlueBookClient(string url)
        {
            this.url = url;
        }


        public KelleyBlueBookReport GetReport(int dealerId, int mileage, string vin)
        {
            if (!HasUsedKbbConsumerTool(dealerId, vin) || (!HasKbbConsumerTool(dealerId)))
            {
                return null;
            }
            else
            {
                using (StreamReader responseStream = MakeWebRequest(dealerId, mileage, vin))
                {
                    return ProcessReport(responseStream);
                }
            }
        }

        public static bool HasUsedKbbConsumerTool(int dealerId, string vin)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandText = "Pricing.HasUsedKbbConsumerTool";
                    command.CommandType = CommandType.StoredProcedure;
                    command.AddParameterWithValue("BusinessUnitId", DbType.Int32, false, dealerId);
                    command.AddParameterWithValue("Vin", DbType.String, false, vin);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return reader.GetBoolean(reader.GetOrdinal("HasUsedKbbConsumerTool"));
                        }
                        return false;
                    }
                }
            }
        }

        public static bool HasKbbConsumerTool(int dealerId)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandText = "Pricing.HasKbbConsumerTool";
                    command.CommandType = CommandType.StoredProcedure;
                    command.AddParameterWithValue("BusinessUnitId", DbType.Int32, false, dealerId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return reader.GetBoolean(reader.GetOrdinal("HasKbbConsumerTool"));
                        }
                        return false;
                    }
                }
            }
        }

        public static bool HasKbbAsBook(int dealerId)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandText = "Pricing.HasKbbAsBook";
                    command.CommandType = CommandType.StoredProcedure;
                    command.AddParameterWithValue("BusinessUnitId", DbType.Int32, false, dealerId);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return reader.GetBoolean(reader.GetOrdinal("HasKbbAsBook"));
                        }
                        return false;
                    }
                }
            }
        }

        private static KelleyBlueBookReport ProcessReport(TextReader response)
        {
            //<kelley-blue-book>
            //  <vehicle condition="EXCELLENT">
            //    <vin>1GYFK66827R308842</vin>
            //    <mileage>36456</mileage>
            //  </vehicle>
            //  <values publication-region="IL" valid-from="2009-02-20" valid-until="2009-02-26">
            //    <value condition="EXCELLENT" category="TRADE-IN">10500</value>
            //    <value condition="GOOD" category="TRADE-IN">9500</value>
            //    <value condition="Fair" category="TRADE-IN">8500</value>
            //    <value condition="EXCELLENT" category="RETAIL">15830</value>
            //  </values>
            //</kelley-blue-book>

            XmlDocument doc = new XmlDocument();
            doc.Load(response);

            if (doc.SelectSingleNode("/kelley-blue-book/error") != null)
            {
                return null;
            }

            XmlAttribute ConditionAttribute = doc.SelectSingleNode("/kelley-blue-book/vehicle").Attributes["condition"];
            XmlNode VinNode = doc.SelectSingleNode("/kelley-blue-book/vehicle/vin");
            XmlNode MileageNode = doc.SelectSingleNode("/kelley-blue-book/vehicle/mileage");
            XmlAttribute PublicationRegionAttribute = doc.SelectSingleNode("/kelley-blue-book/values").Attributes["publication-region"];
            XmlAttribute PublicationValidFromAttribute = doc.SelectSingleNode("/kelley-blue-book/values").Attributes["valid-from"];
            XmlAttribute PublicationValidUntilAttribute = doc.SelectSingleNode("/kelley-blue-book/values").Attributes["valid-until"];
            XmlNodeList ValuationNodes = doc.SelectNodes("/kelley-blue-book/values/value");

            KelleyBlueBookCondition condition = GetCondition(ConditionAttribute.InnerText.ToUpper());
            int mileage = int.Parse(MileageNode.InnerText);
            string vin = VinNode.InnerText;

            KelleyBlueBookValuationList vals = new KelleyBlueBookValuationList();
            foreach (XmlNode node in ValuationNodes)
            {
                KelleyBlueBookCondition cond = GetCondition(node.Attributes["condition"].InnerText.ToUpper());
                KelleyBlueBookCategory cat = GetCategory(node.Attributes["category"].InnerText.ToUpper());
                int val = int.Parse(node.InnerText);
                vals.Add(new KelleyBlueBookValuation(cond, cat, val));
            }

            string publicationRegionNode = PublicationRegionAttribute.InnerText;
            DateTime validFrom = DateTime.Parse(PublicationValidFromAttribute.InnerText);
            DateTime validUntil = DateTime.Parse(PublicationValidUntilAttribute.InnerText);
            KelleyBlueBookPublication pub = new KelleyBlueBookPublication(publicationRegionNode, validFrom, validUntil);

            KelleyBlueBookReport report = new KelleyBlueBookReport(mileage, vin, condition, vals, pub);

            return report;
        }

        private static KelleyBlueBookCategory GetCategory(string cat)
        {
            switch (cat)
            {
                case "TRADEIN":
                    return KelleyBlueBookCategory.TradeIn;
                case "WHOLESALE":
                    return KelleyBlueBookCategory.Wholesale;
                case "RETAIL":
                    return KelleyBlueBookCategory.Retail;
                default:
                    return KelleyBlueBookCategory.Unknown;
            }
        }

        private static KelleyBlueBookCondition GetCondition(string cond)
        {
            switch (cond)
            {
                case "N_A":
                    return KelleyBlueBookCondition.NotApplicable;
                case "EXCELLENT":
                    return KelleyBlueBookCondition.Excellent;
                case "FAIR":
                    return KelleyBlueBookCondition.Fair;
                case "GOOD":
                    return KelleyBlueBookCondition.Good;
                default:
                    return KelleyBlueBookCondition.Unknown;
            }
        }

        private StreamReader MakeWebRequest(int dealerId, int mileage, string vin)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url + "?mileage=" + mileage + "&vin=" + vin + "&dealerId=" + dealerId);
            request.Method = "GET";
            return new StreamReader(request.GetResponse().GetResponseStream());
        }
    }
}