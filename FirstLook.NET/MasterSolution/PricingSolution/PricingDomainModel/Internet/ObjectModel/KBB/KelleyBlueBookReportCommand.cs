﻿using System;
using System.Configuration;
using Csla;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;

namespace FirstLook.Pricing.DomainModel.Internet.ObjectModel.KBB
{
    public class KelleyBlueBookReportCommand : CommandBase
    {
        private KelleyBlueBookReport _kelleyBlueBookReport;
        private VehicleType _vehicleType;
        private readonly string _ownerHandle;
        private readonly string _vehicleHandle;
        private int? _dealerId;
        
        public KelleyBlueBookReport KelleyBlueBookReport
        {
            get { return _kelleyBlueBookReport; }
        }

        private VehicleType VehicleType
        {
            get
            {
                if (_vehicleType == VehicleType.Unknown)
                {
                    _vehicleType = VehicleTypeCommand.Execute(_vehicleHandle);
                }

                return _vehicleType;
            }
        }

        private int DealerId
        {
            get
            {
                if (!_dealerId.HasValue)
                {
                    _dealerId = Identity.GetDealerId(_ownerHandle);
                }

                return _dealerId.Value;
            }
        }

        public static KelleyBlueBookReport TryFetch(string oh, string vh)
        {
            KelleyBlueBookReportCommand kelleyBlueBookReportCommand = new KelleyBlueBookReportCommand(oh, vh);
            DataPortal.Execute(kelleyBlueBookReportCommand);

            return kelleyBlueBookReportCommand.KelleyBlueBookReport;
        }

        private KelleyBlueBookReportCommand(string ownerHandle, string vehicleHandle)
        {
            _ownerHandle = ownerHandle;
            _vehicleHandle = vehicleHandle;
        }

        protected override void DataPortal_Execute()
        {
            try
            {
                if (Identity.IsDealer(_ownerHandle))
                {
                    if (VehicleType == VehicleType.Inventory || VehicleType == VehicleType.Appraisal)
                    {
                        string vin = "";
                        int mileage = -1;

                        if (VehicleType == VehicleType.Inventory)
                        {
                            Inventory inv = Inventory.GetInventory(_ownerHandle, _vehicleHandle);
                            vin = inv.Vin;
                            mileage = inv.Mileage.HasValue ? inv.Mileage.Value : 0;
                        }
                        else if (VehicleType == VehicleType.Appraisal)
                        {
                            Appraisal app = Appraisal.GetAppraisal(_ownerHandle, _vehicleHandle);
                            vin = app.Vin;
                            mileage = app.Mileage.HasValue ? app.Mileage.Value : 0;
                        }

                        if (!String.IsNullOrEmpty(vin) && mileage != -1)
                        {
                            KelleyBlueBookClient client =
                                new KelleyBlueBookClient(ConfigurationManager.AppSettings["KbbUrl"]);
                            _kelleyBlueBookReport = client.GetReport(DealerId, mileage, vin);
                        }
                    }
                }
            }
            catch
            {
                // ignore all and any exceptions we do not care
            }
        }
    }
}
