using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using FirstLook.Common.Data;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.Pricing.DomainModel.Internet.DataSource
{
    public class MarketListingDataSource
    {
        private const string ReportId = "D9D37F98-4380-4279-8033-6E40D7AF73AA";

        private const string ReportDataSet_Table = "MarketListing";

        private const string ReportDataSet_Count = "MarketListingCount";

        private const string ReportDataSet_Vehicle = "MarketListingVehicle";

        private const string ReportDataSet_TableRaw = "MarketListingRaw";

        private readonly DataTableCallback NullCallback = new DataTableCallback();

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public DataTable FindByOwnerHandleAndParameters(string ownerHandle, int modelYear, int makeId, int lineId, string sortColumns, int distanceBucketId)
        {
            if (string.IsNullOrEmpty(ownerHandle))
            {
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");
            }

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;
            parameterValues["ModelYear"] = modelYear;
            parameterValues["MakeID"] = makeId;
            parameterValues["LineID"] = lineId;
            parameterValues["DistanceBucketID"] = distanceBucketId;
            // parameterValues["SortColumns"] = null;

            return ReportHelper.LoadReportDataTable(ReportId, ReportDataSet_TableRaw, parameterValues, NullCallback);
        }

        public DataTable FindByOwnerHandleAndSearchHandle(string ownerHandle, string searchHandle, int searchTypeId)
        {
            return FindByOwnerHandleAndSearchHandle(
                ownerHandle,
                searchHandle,
                searchTypeId,
                "DistanceFromDealer ASC",
                9999,
                0);
        }

        public DataTable FindByOwnerHandleAndSearchHandle(string ownerHandle, string searchHandle, int searchTypeId, string sortColumns)
        {
            return FindByOwnerHandleAndSearchHandle(
                ownerHandle,
                searchHandle,
                searchTypeId,
                sortColumns,
                9999,
                0);
        }

        public DataTable FindByOwnerHandleAndSearchHandle(string ownerHandle, string searchHandle, int searchTypeId, string sortColumns, int maximumRows, int startRowIndex)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            if (string.IsNullOrEmpty(searchHandle))
                throw new ArgumentNullException("searchHandle", "Cannot be null or empty");

            if (searchTypeId != 1 && searchTypeId != 4)
                throw new ArgumentNullException("searchTypeId", "Valid values are 1 and 4.");

            if (string.IsNullOrEmpty(sortColumns))
            {
                sortColumns = "DistanceFromDealer ASC";
            }

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;
            parameterValues["SearchHandle"] = searchHandle;
            parameterValues["SearchTypeID"] = searchTypeId;
            parameterValues["SortColumns"] = sortColumns;
            parameterValues["MaximumRows"] = maximumRows.ToString(CultureInfo.InvariantCulture);
            parameterValues["StartRowIndex"] = startRowIndex.ToString(CultureInfo.InvariantCulture);

            DataTable dataTable = null;
            try
            {
                dataTable = PricingAnalyticsClient.GetReportDataAndCacheKeys<DataTable>(PricingAnalyticsClient.GetParameterList(parameterValues), ownerHandle);
            }
            catch (Exception ex)
            {
                Log.Error("An error occured while implementing Memcached Client : " + ex.Message);
            }

            if (dataTable == null)
            {
                dataTable = ReportHelper.LoadReportDataTable(ReportId, ReportDataSet_Table, parameterValues, NullCallback);
                try
                {
                    PricingAnalyticsClient.SetReportDataAndCacheKeys<DataTable>(PricingAnalyticsClient.GetParameterList(parameterValues), dataTable, ownerHandle);
                }
                catch (Exception ex)
                {
                    Log.Error("An error occured while implementing Memcached Client : " + ex.Message);
                }
            }

            return dataTable;
        }

        public int CountByOwnerHandleAndSearchHandle(string ownerHandle, string searchHandle, int searchTypeId)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            if (string.IsNullOrEmpty(searchHandle))
                throw new ArgumentNullException("searchHandle", "Cannot be null or empty");

            if (searchTypeId != 1 && searchTypeId != 4)
                throw new ArgumentNullException("searchTypeId", "Valid values are 1 and 4.");

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;
            parameterValues["SearchHandle"] = searchHandle;
            parameterValues["SearchTypeID"] = searchTypeId;

            DataTable table = ReportHelper.LoadReportDataTable(ReportId, ReportDataSet_Count, parameterValues, NullCallback);

            foreach (DataRow row in table.Rows)
            {
                return Convert.ToInt32(row["NumberOfRows"], CultureInfo.InvariantCulture);
            }

            throw new DataException("Expected one row but got zero!");
        }

        public int FindVehicleByOwnerHandleAndSearchHandleAndSortColumns(string ownerHandle, string searchHandle, int searchTypeId, string sortColumns, int maximumRows, int startRowIndex)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            if (string.IsNullOrEmpty(searchHandle))
                throw new ArgumentNullException("searchHandle", "Cannot be null or empty");

            if (searchTypeId != 1 && searchTypeId != 4)
                throw new ArgumentNullException("searchTypeId", "Valid values are 1 and 4.");

            if (string.IsNullOrEmpty(sortColumns))
            {
                sortColumns = "DistanceFromDealer ASC";
            }

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;
            parameterValues["SearchHandle"] = searchHandle;
            parameterValues["SearchTypeID"] = searchTypeId;
            parameterValues["SortColumns"] = sortColumns;

            DataTable table = ReportHelper.LoadReportDataTable(ReportId, ReportDataSet_Vehicle, parameterValues, NullCallback);

            foreach (DataRow row in table.Rows)
            {
                return Convert.ToInt32(row["RowIndex"], CultureInfo.InvariantCulture);
            }

            throw new DataException("Expected one row but got zero!");
        }

        public int FindVehicleByOwnerHandleAndSearchHandleAndSortColumns(string ownerHandle, string searchHandle, int searchTypeId)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            if (string.IsNullOrEmpty(searchHandle))
                throw new ArgumentNullException("searchHandle", "Cannot be null or empty");

            if (searchTypeId != 1 && searchTypeId != 4)
                throw new ArgumentNullException("searchTypeId", "Valid values are 1 and 4.");

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;
            parameterValues["SearchHandle"] = searchHandle;
            parameterValues["SearchTypeID"] = searchTypeId;
            parameterValues["SortColumns"] = "DistanceFromDealer ASC";

            DataTable table = ReportHelper.LoadReportDataTable(ReportId, ReportDataSet_Vehicle, parameterValues, NullCallback);

            foreach (DataRow row in table.Rows)
            {
                return Convert.ToInt32(row["RowIndex"], CultureInfo.InvariantCulture);
            }

            throw new DataException("Expected one row but got zero!");
        }
    }
}
