using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.Pricing.DomainModel.Internet.DataSource
{
    public class VehicleInformationDataSource
    {
        private const string ReportId = "B58F95F1-C318-4b07-B3A2-8AADC4580613";

        private const string ReportDataSet_Information = "VehicleInformation";

        private const string ReportDataSet_Equipment = "VehicleEquipment";

        private const string ReportDataSet_Providers = "VehicleEquipmentProviders";

        private readonly DataTableCallback NullCallback = new DataTableCallback();

        public DataTable FindByOwnerHandleAndVehicleHandle(String ownerHandle, String vehicleHandle)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            if (string.IsNullOrEmpty(vehicleHandle))
                throw new ArgumentNullException("vehicleHandle", "Cannot be null or empty");

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;
            parameterValues["VehicleHandle"] = vehicleHandle;

            return ReportHelper.LoadReportDataTable(ReportId, ReportDataSet_Information, parameterValues, NullCallback);
        }

        public DataTable FindProvidersByOwnerHandleAndVehicleHandle(String ownerHandle)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;

            return ReportHelper.LoadReportDataTable(ReportId, ReportDataSet_Providers, parameterValues, NullCallback);
        }

        public DataTable FindEquipmentByOwnerHandleAndVehicleHandle(String ownerHandle, String vehicleHandle, int providerId)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            if (string.IsNullOrEmpty(vehicleHandle))
                throw new ArgumentNullException("vehicleHandle", "Cannot be null or empty");

            if (providerId < 1 || providerId > 6)
                throw new ArgumentOutOfRangeException("providerId", providerId, "Must be between 1 and 6");

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;
            parameterValues["VehicleHandle"] = vehicleHandle;
            parameterValues["ProviderId"] = providerId;

            return ReportHelper.LoadReportDataTable(ReportId, ReportDataSet_Equipment, parameterValues, NullCallback);
        }
    }
}
