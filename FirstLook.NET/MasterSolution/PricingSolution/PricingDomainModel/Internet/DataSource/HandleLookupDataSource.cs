using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using FirstLook.Common.Data;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.Pricing.DomainModel.Internet.DataSource
{
    public class HandleLookupDataSource
    {
        private const string ReportId_Vehicle = "BEDDB3C7-5DD1-4639-B2BE-AEE839704398";

        private const string ReportDataSet_Vehicle = "HandleLookup_Vehicle";

        private const string ReportId_StockNumber = "B9D63E31-4A9F-4ec6-8C41-BA99963A3574";

        private const string ReportDataSet_StockNumber = "HandleLookup_StockNumber";

        private const string ReportId_Search = "E161BC16-CC2A-4e04-9F84-2FF5A55ADE8D";

        private const string ReportDataSet_Search = "HandleLookup_Search";
        private const string ReportDataSet_SearchID = "HandleLookup_SearchID";

        private const string ReportDataSet_LoadSearchResultsData = "HandleLookup_LoadSearchResults";

        private readonly DataTableCallback NullCallback = new DataTableCallback();

        [SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "3#")]
        [SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "4#")]
        public void FindByOwnerHandleAndVehicle(String ownerHandle, int vehicleEntityTypeId, int vehicleEntityId, string insertUser, out string vehicleHandle, out string searchHandle)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

			if (string.IsNullOrEmpty(insertUser))
				throw new ArgumentNullException("insertUser", "Cannot be null or empty");

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;
            parameterValues["VehicleEntityTypeID"] = vehicleEntityTypeId.ToString(CultureInfo.InvariantCulture);
            parameterValues["VehicleEntityID"] = vehicleEntityId.ToString(CultureInfo.InvariantCulture);
			parameterValues["InsertUser"] = insertUser;

            DataTable table = ReportHelper.LoadReportDataTable(ReportId_Vehicle, ReportDataSet_Vehicle, parameterValues, NullCallback);

            foreach (DataRow row in table.Rows)
            {
                searchHandle = Convert.ToString(row["SearchHandle"], CultureInfo.InvariantCulture);

                vehicleHandle = Convert.ToString(row["VehicleHandle"], CultureInfo.InvariantCulture);

                return;
            }

            throw new DataException("Expected one row of handle information but got none");
        }

        [SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "2#")]
        [SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "3#")]
        public void FindByOwnerHandleAndStockNumber(String ownerHandle, string stockNumber, string insertUser, out string vehicleHandle, out string searchHandle)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            if (string.IsNullOrEmpty(stockNumber))
                throw new ArgumentNullException("stockNumber", "Cannot be null or empty");

			if (string.IsNullOrEmpty(insertUser))
				throw new ArgumentNullException("insertUser", "Cannot be null or empty");

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;
            parameterValues["StockNumber"] = stockNumber;
			parameterValues["InsertUser"] = insertUser;

            DataTable table = ReportHelper.LoadReportDataTable(ReportId_StockNumber, ReportDataSet_StockNumber, parameterValues, NullCallback);

            foreach (DataRow row in table.Rows)
            {
                searchHandle = Convert.ToString(row["SearchHandle"], CultureInfo.InvariantCulture);

                vehicleHandle = Convert.ToString(row["VehicleHandle"], CultureInfo.InvariantCulture);

                return;
            }

            throw new DataException("Expected one row of handle information but got none");
        }

        public string FindByOwnerHandleAndVehicleHandle(string ownerHandle, string vehicleHandle, string insertUser)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            if (string.IsNullOrEmpty(vehicleHandle))
                throw new ArgumentNullException("vehicleHandle", "Cannot be null or empty");

			if (string.IsNullOrEmpty(insertUser))
				throw new ArgumentNullException("insertUser", "Cannot be null or empty");

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;
            parameterValues["VehicleHandle"] = vehicleHandle;
			parameterValues["InsertUser"] = insertUser;

            DataTable table = ReportHelper.LoadReportDataTable(ReportId_Search, ReportDataSet_Search, parameterValues, NullCallback);

            foreach (DataRow row in table.Rows)
            {
                return Convert.ToString(row["SearchHandle"], CultureInfo.InvariantCulture);
            }

            throw new DataException("Expected one row of handle information but got none");
        }
        [SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "3#")]
        [SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "4#")]
        public void FindByOwnerHandleAndSearchId(String ownerHandle, int vehicleEntityTypeId, int vehicleEntityId, out int searchId)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");



            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;
            parameterValues["VehicleEntityTypeID"] = vehicleEntityTypeId.ToString(CultureInfo.InvariantCulture);
            parameterValues["VehicleEntityID"] = vehicleEntityId.ToString(CultureInfo.InvariantCulture);


            DataTable table = ReportHelper.LoadReportDataTable(ReportId_Vehicle, ReportDataSet_SearchID, parameterValues, NullCallback);

            foreach (DataRow row in table.Rows)
            {
                searchId = Convert.ToInt32(row["SearchID"], CultureInfo.InvariantCulture);
                return;
            }

            throw new DataException("Expected one row of handle information but got none");
        }
        [SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "3#")]
        [SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "4#")]
        public void FindByOwnerHandleAndLoadSearchResults(String ownerHandle, int vehicleEntityTypeId, int vehicleEntityId, out string vehicleHandle, out string searchHandle)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");


            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;
            parameterValues["VehicleEntityTypeID"] = vehicleEntityTypeId.ToString(CultureInfo.InvariantCulture);
            parameterValues["VehicleEntityID"] = vehicleEntityId.ToString(CultureInfo.InvariantCulture);


            DataTable table = ReportHelper.LoadReportDataTable(ReportId_Vehicle, ReportDataSet_LoadSearchResultsData, parameterValues, NullCallback);

            foreach (DataRow row in table.Rows)
            {
                searchHandle = Convert.ToString(row["SearchHandle"], CultureInfo.InvariantCulture);

                vehicleHandle = Convert.ToString(row["VehicleHandle"], CultureInfo.InvariantCulture);

                return;
            }

            throw new DataException("Expected one row of handle information but got none");
        }
    }
}
