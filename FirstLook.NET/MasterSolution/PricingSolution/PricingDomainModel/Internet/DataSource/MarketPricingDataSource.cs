using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.Pricing.DomainModel.Internet.DataSource
{
    public class MarketPricingDataSource
    {
        private const string ReportId = "DC117AFB-C67C-4b11-B99D-B90F340F5724";

        private const string ReportDataSet = "MarketPricing";

        private const string ReportDataSet_List = "MarketPricingList";

        private readonly DataTableCallback NullCallback = new DataTableCallback();

        private IDictionary cache;

        public void SetCache(IDictionary dictionary)
        {
            cache = dictionary;
        }

        public IDictionary Cache
        {
            get { return cache; }
        }

        public DataTable FindByOwnerHandleAndVehicleHandleAndSearchHandle(string ownerHandle, string vehicleHandle, string searchHandle, int searchTypeId)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            if (string.IsNullOrEmpty(searchHandle))
                throw new ArgumentNullException("searchHandle", "Cannot be null or empty");

            if (string.IsNullOrEmpty(vehicleHandle))
                throw new ArgumentNullException("vehicleHandle", "Cannot be null or empty");

			if (searchTypeId != 1 && searchTypeId != 4)
				throw new ArgumentNullException("searchTypeId", "Valid values are 1 and 4.");

            CacheKey cacheKey = new CacheKey(ownerHandle, vehicleHandle, searchHandle);

            DataTable table = QueryCache(ReportId, ReportDataSet, cacheKey);

            if (table == null)
            {
                Dictionary<string, object> parameterValues = new Dictionary<string, object>();
                parameterValues["OwnerHandle"] = ownerHandle;
                parameterValues["VehicleHandle"] = vehicleHandle;
                parameterValues["SearchHandle"] = searchHandle;
                parameterValues["SearchTypeID"] = searchTypeId;

                table = ReportHelper.LoadReportDataTable(ReportId, ReportDataSet, parameterValues, NullCallback);

                PopulateCache(ReportId, ReportDataSet, cacheKey, table);
            }

            return table;
        }

        public DataTable FindListByOwnerHandleAndSearchHandle(string ownerHandle, string searchHandle, int searchTypeId)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            if (string.IsNullOrEmpty(searchHandle))
                throw new ArgumentNullException("searchHandle", "Cannot be null or empty");

			if (searchTypeId != 1 && searchTypeId != 4)
				throw new ArgumentNullException("searchTypeId", "Valid values are 1 and 4.");

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;
            parameterValues["SearchHandle"] = searchHandle;
            parameterValues["SearchTypeID"] = searchTypeId;

            return ReportHelper.LoadReportDataTable(ReportId, ReportDataSet_List, parameterValues, NullCallback);
        }

        private DataTable QueryCache(string key, string subKey, object parameterKey)
        {
            if (Cache == null)
                return null;
            return FindOrCreate(FindOrCreate(Cache, key), subKey)[parameterKey] as DataTable;
        }

        private void PopulateCache(string key, string subKey, object parameterKey, DataTable data)
        {
            if (Cache == null)
                return;
            AddKey(FindOrCreate(FindOrCreate(Cache, key), subKey), parameterKey, data);
        }

        private static IDictionary FindOrCreate(IDictionary dictionary, object key)
        {
            if (!dictionary.Contains(key))
                dictionary[key] = new Hashtable();
            return dictionary[key] as IDictionary;
        }

        private static void AddKey(IDictionary dictionary, object key, object value)
        {
            if (dictionary.Contains(key))
                dictionary.Remove(key);
            dictionary[key] = value;
        }

        private sealed class CacheKey : IEquatable<CacheKey>
        {
            private readonly string ownerHandle, vehicleHandle, searchHandle;

            public CacheKey(string ownerHandle, string vehicleHandle, string searchHandle)
            {
                this.ownerHandle = ownerHandle;
                this.vehicleHandle = vehicleHandle;
                this.searchHandle = searchHandle;
            }

            public bool Equals(CacheKey cacheEntry)
            {
                if (cacheEntry == null) return false;
                if (!Equals(ownerHandle, cacheEntry.ownerHandle)) return false;
                if (!Equals(vehicleHandle, cacheEntry.vehicleHandle)) return false;
                if (!Equals(searchHandle, cacheEntry.searchHandle)) return false;
                return true;
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(this, obj)) return true;
                return Equals(obj as CacheKey);
            }

            public override int GetHashCode()
            {
                int result = ownerHandle != null ? ownerHandle.GetHashCode() : 0;
                result = 29*result + (vehicleHandle != null ? vehicleHandle.GetHashCode() : 0);
                result = 29*result + (searchHandle != null ? searchHandle.GetHashCode() : 0);
                return result;
            }
        }
    }
}
