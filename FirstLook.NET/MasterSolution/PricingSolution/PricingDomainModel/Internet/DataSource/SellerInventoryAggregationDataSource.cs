using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.Pricing.DomainModel.Internet.DataSource
{
    public class SellerInventoryAggregationDataSource
    {
        private const string ReportId = "BAEE3BB3-4A80-44e9-9975-25C62497D69A";

        private const string ReportDataSet = "SellerInventoryAggregation";

        private readonly DataTableCallback NullCallback = new DataTableCallback();

        public DataTable FindBySellerIdAndJDPowerRegionId(string sellerId, string jdPowerRegionId)
        {
            if (string.IsNullOrEmpty(sellerId))
                throw new ArgumentNullException("sellerId", "Cannot be null or empty");

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["SellerID"] = sellerId;
            parameterValues["JDPowerRegionID"] = jdPowerRegionId;

            return ReportHelper.LoadReportDataTable(ReportId, ReportDataSet, parameterValues, NullCallback);
        }
    }
}
