using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.Pricing.DomainModel.Internet.DataSource
{
    public class SearchHandleNameDataSource
    {
        private const string ReportId = "DB74DD14-CDB2-4356-AA28-E15922D4FC54";

        private const string ReportDataSet = "SearchHandleName";

        private readonly DataTableCallback NullCallback = new DataTableCallback();

        public DataTable FindBySearchHandle(String searchHandle)
        {
            if (string.IsNullOrEmpty(searchHandle))
                throw new ArgumentNullException("searchHandle", "Cannot be null or empty");

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["SearchHandle"] = searchHandle;

            return ReportHelper.LoadReportDataTable(ReportId, ReportDataSet, parameterValues, NullCallback);
        }

        public DataTable FindBySearchHandle(String searchHandle, int maximumRows, int startRowIndex)
        {
            if (string.IsNullOrEmpty(searchHandle))
                throw new ArgumentNullException("searchHandle", "Cannot be null or empty");

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["SearchHandle"] = searchHandle;

            return ReportHelper.LoadReportDataTable(ReportId, ReportDataSet, parameterValues, NullCallback);
        }
    }
}
