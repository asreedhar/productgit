using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.Pricing.DomainModel.Internet.DataSource
{
    public class OwnerNameDataSource
    {
        private const string ReportId = "C9E2D3E4-5719-4263-95EB-71ADFA227D1E";

        private const string ReportDataSet = "OwnerName";

        private readonly DataTableCallback NullCallback = new DataTableCallback();
        
        private IDictionary cache;

        public void SetCache(IDictionary dictionary)
        {
            cache = dictionary;
        }

        public IDictionary Cache
        {
            get { return cache; }
        }

        public DataTable FindByOwnerHandle(string ownerHandle)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            CacheKey cacheKey = new CacheKey(ownerHandle);

            DataTable table = QueryCache(ReportId, ReportDataSet, cacheKey);

            if (table == null)
            {
                Dictionary<string, object> parameterValues = new Dictionary<string, object>();
                parameterValues["OwnerHandle"] = ownerHandle;
                table = ReportHelper.LoadReportDataTable(ReportId, ReportDataSet, parameterValues, NullCallback);
                PopulateCache(ReportId, ReportDataSet, cacheKey, table);
            }

            return table;
        }

        private DataTable QueryCache(string key, string subKey, object parameterKey)
        {
            if (Cache == null)
                return null;
            return FindOrCreate(FindOrCreate(Cache, key), subKey)[parameterKey] as DataTable;
        }

        private void PopulateCache(string key, string subKey, object parameterKey, DataTable data)
        {
            if (Cache == null)
                return;
            AddKey(FindOrCreate(FindOrCreate(Cache, key), subKey), parameterKey, data);
        }

        private static IDictionary FindOrCreate(IDictionary dictionary, object key)
        {
            if (!dictionary.Contains(key))
                dictionary[key] = new Hashtable();
            return dictionary[key] as IDictionary;
        }

        private static void AddKey(IDictionary dictionary, object key, object value)
        {
            if (dictionary.Contains(key))
                dictionary.Remove(key);
            dictionary[key] = value;
        }

        private sealed class CacheKey : IEquatable<CacheKey>
        {
            private readonly string ownerHandle;

            public CacheKey(string ownerHandle)
            {
                this.ownerHandle = ownerHandle;
            }

            public static bool operator !=(CacheKey cacheKey1, CacheKey cacheKey2)
            {
                return !Equals(cacheKey1, cacheKey2);
            }

            public static bool operator ==(CacheKey cacheKey1, CacheKey cacheKey2)
            {
                return Equals(cacheKey1, cacheKey2);
            }

            public bool Equals(CacheKey cacheKey)
            {
                if (cacheKey == null) return false;
                return Equals(ownerHandle, cacheKey.ownerHandle);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(this, obj)) return true;
                return Equals(obj as CacheKey);
            }

            public override int GetHashCode()
            {
                return ownerHandle.GetHashCode();
            }
        }
    }
}
