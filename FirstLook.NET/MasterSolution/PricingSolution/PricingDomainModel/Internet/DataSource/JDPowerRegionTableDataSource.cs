using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.Pricing.DomainModel.Internet.DataSource
{
    public class JDPowerRegionTableDataSource
    {
        private const string ReportId = "C825AAC1-5A4A-4671-86AF-0B4AB3D30AE3";

        private const string ReportDataSet = "JDPowerRegionTable";

        private readonly DataTableCallback NullCallback = new DataTableCallback();

        public DataTable SelectJDPowerRegion()
        {
            return ReportHelper.LoadReportDataTable(ReportId, ReportDataSet, null, NullCallback);
        }
    }

}
