using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using FirstLook.Common.Data;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.Pricing.DomainModel.Internet.DataSource
{
    public class SellerTableDataSource
    {
        private const string ReportId = "CEBB3325-C28D-480f-9A4E-47E13211B4F1";

        private const string ReportDataSet = "SellerTable";

        private const string ReportDataSet_Count = "SellerTableCount";

        private readonly DataTableCallback NullCallback = new DataTableCallback();

        public DataTable FindByZipCodeAndDealershipNameAndFilters(string zipCode, String dealershipName, int jdPowerRegionId, int searchRadius, int columnIndex, string sortColumns, int maximumRows, int startRowIndex)
        {
            if (string.IsNullOrEmpty(sortColumns))
            {
                sortColumns = "SellerName ASC";
            }

            ValidateZipCode(zipCode);

            ValidateColumnIndex(columnIndex);

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["ZipCode"] = zipCode;
            parameterValues["DealershipName"] = dealershipName;
            parameterValues["JDPowerRegionID"] = jdPowerRegionId;
            parameterValues["SearchRadius"] = searchRadius;
            parameterValues["ColumnIndex"] = columnIndex;
            parameterValues["SortColumns"] = sortColumns;
            parameterValues["MaximumRows"] = maximumRows.ToString(CultureInfo.InvariantCulture);
            parameterValues["StartRowIndex"] = startRowIndex.ToString(CultureInfo.InvariantCulture);

            return ReportHelper.LoadReportDataTable(ReportId, ReportDataSet, parameterValues, NullCallback);
        }

        public int CountByZipCodeAndDealershipNameAndFilters(string zipCode, String dealershipName, int jdPowerRegionId, int searchRadius, int columnIndex)
        {
            ValidateZipCode(zipCode);

            ValidateColumnIndex(columnIndex);

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["ZipCode"] = zipCode;
            parameterValues["DealershipName"] = dealershipName;
            parameterValues["JDPowerRegionID"] = jdPowerRegionId;
            parameterValues["SearchRadius"] = searchRadius;
            parameterValues["ColumnIndex"] = columnIndex;

            DataTable table = ReportHelper.LoadReportDataTable(ReportId, ReportDataSet_Count, parameterValues, NullCallback);

            foreach (DataRow row in table.Rows)
            {
                return Convert.ToInt32(row["NumberOfRows"], CultureInfo.InvariantCulture);
            }

            throw new DataException("Expected one row but got zero!");
        }

        private static void ValidateZipCode(string zipCode)
        {
            int parsedZipCode;

            if (!string.IsNullOrEmpty(zipCode) && !int.TryParse(zipCode, out parsedZipCode))
                throw new ArgumentOutOfRangeException("zipCode", "Not a valid Integer");

            if (!string.IsNullOrEmpty(zipCode) && (zipCode.Length < 3 || zipCode.Length > 5))
                throw new ArgumentOutOfRangeException("zipCode", "Valid values are between 3 and 5");
        }

        private static void ValidateColumnIndex(int columnIndex)
        {
            if (columnIndex < 0 || columnIndex > 5)
                throw new ArgumentOutOfRangeException("columnIndex", "Valid values are between 0 and 5");
        }
    }
}
