using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using FirstLook.Common.Data;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.Pricing.DomainModel.Internet.DataSource
{
    public class PricingGraphDataSource
    {
        private const string ReportId = "FFA7E5E8-2138-4c5a-8986-E121C2CAF199";

        private const string ReportDataSet = "PricingGraph";

        private const string ReportId_Name = "D26AED0D-F30C-4706-BC23-492A2D106009";

        private const string ReportDataSet_Name = "PricingGraphColumnName";

        private readonly DataTableCallback NullCallback = new DataTableCallback();

        public DataTable FindByOwnerHandleAndModeAndSaleStrategy(string ownerHandle, string mode, string saleStrategy)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            if (string.IsNullOrEmpty(mode) || (!mode.Equals("A") && !mode.Equals("R") && !mode.Equals("N")))
                throw new ArgumentNullException("mode", "Valid values are 'A' and 'R' and 'N'");

            if (string.IsNullOrEmpty(saleStrategy) || (!saleStrategy.Equals("A") && !saleStrategy.Equals("R") && !saleStrategy.Equals("N")))
                throw new ArgumentNullException("saleStrategy", "Valid values are 'A' and 'R' and 'N'");

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;
            parameterValues["Mode"] = mode;
            parameterValues["SaleStrategy"] = saleStrategy;

            return ReportHelper.LoadReportDataTable(ReportId, ReportDataSet, parameterValues, NullCallback);
        }

        public string FindNameByOwnerHandleAndModeAndColumnIndex(string ownerHandle, string mode, int columnIndex)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            if (string.IsNullOrEmpty(mode) || (!mode.Equals("A") && !mode.Equals("R")))
                throw new ArgumentNullException("mode", "Valid values are 'A' and 'R'");

            if (columnIndex < 0 || columnIndex > 6)
                throw new ArgumentOutOfRangeException("columnIndex", "Valid values are between 0 and 6");

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;
            parameterValues["Mode"] = mode;
            parameterValues["ColumnIndex"] = columnIndex.ToString(CultureInfo.InvariantCulture);

            DataTable table = ReportHelper.LoadReportDataTable(ReportId_Name, ReportDataSet_Name, parameterValues, NullCallback);

            foreach (DataRow row in table.Rows)
            {
                return Convert.ToString(row["Name"], CultureInfo.InvariantCulture);
            }

            throw new DataException("Expected one row of but got none");
        }
    }
}
