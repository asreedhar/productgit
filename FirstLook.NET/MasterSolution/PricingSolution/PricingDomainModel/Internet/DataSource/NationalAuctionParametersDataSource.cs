using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;
using FirstLook.DomainModel.Olap.WebControls;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.Pricing.DomainModel.Internet.DataSource
{
    public class NationalAuctionParametersDataSource
    {
        private const string ReportId = "BD9E8387-943D-47d5-9E75-5F9EC66F583F";

        private const string ReportDataSet = "NationalAuctionParameters";

        private readonly DataTableCallback NullCallback = new DataTableCallback();

        public NationalAuctionPanelArgs FindByOwnerHandleAndVehicleHandle(String ownerHandle, String vehicleHandle)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            if (string.IsNullOrEmpty(vehicleHandle))
                throw new ArgumentNullException("vehicleHandle", "Cannot be null or empty");

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;
            parameterValues["VehicleHandle"] = vehicleHandle;

            DataTable table = ReportHelper.LoadReportDataTable(ReportId, ReportDataSet, parameterValues, NullCallback);

            foreach (DataRow row in table.Rows)
            {
                object vin = row["VIN"];
                object vic = row["VIC"];
                object areaId = row["AreaID"];
                object periodId = row["PeriodID"];
                object mileage = row["Mileage"];

                if (IsNotNull(vin))
                {
                    NationalAuctionPanelArgs args = new NationalAuctionPanelArgs((string)vin);

                    if (IsNotNull(vic))
                        args.Vic = (string) vic;

                    if (IsNotNull(areaId))
                        args.AreaId = (int) areaId;

                    if (IsNotNull(periodId))
                        args.PeriodId = (int) periodId;

                    if (IsNotNull(mileage))
                        args.Mileage = (int) mileage;

                    return args;
                }
            }

            throw new ApplicationException("No Parameters!");
        }

        private static bool IsNotNull(object value)
        {
            return (value != null && value != DBNull.Value);
        }
    }
}
