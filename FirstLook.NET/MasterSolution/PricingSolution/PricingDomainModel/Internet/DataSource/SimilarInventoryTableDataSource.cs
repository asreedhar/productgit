using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using FirstLook.Common.Data;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.Pricing.DomainModel.Internet.DataSource
{
    public class SimilarInventoryTableDataSource
    {
        private const string ReportId = "B8B428E9-10C9-4bd7-95D1-F93D01F79280";

        private const string ReportDataSet_Description = "SimilarInventoryTableDescription";

        private const string ReportDataSet_Table = "SimilarInventoryTable";

        private const string ReportDataSet_Count = "SimilarInventoryTableCount";

        private readonly DataTableCallback NullCallback = new DataTableCallback();

        public DataTable FindDescriptionByOwnerHandleAndVehicleHandle(string ownerHandle, string vehicleHandle, bool isAllYears)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            if (string.IsNullOrEmpty(vehicleHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;
            parameterValues["VehicleHandle"] = vehicleHandle;
            parameterValues["IsAllYears"] = isAllYears;

            return ReportHelper.LoadReportDataTable(ReportId, ReportDataSet_Description, parameterValues, NullCallback);
        }

        public DataTable FindByOwnerHandleAndVehicleHandle(string ownerHandle, string vehicleHandle, bool isAllYears, string sortColumns, int maximumRows, int startRowIndex)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            if (string.IsNullOrEmpty(vehicleHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            if (string.IsNullOrEmpty(sortColumns))
                sortColumns = "Age DESC";

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;
            parameterValues["VehicleHandle"] = vehicleHandle;
            parameterValues["IsAllYears"] = isAllYears;
            parameterValues["SortColumns"] = sortColumns;
            parameterValues["MaximumRows"] = maximumRows.ToString(CultureInfo.InvariantCulture);
            parameterValues["StartRowIndex"] = startRowIndex.ToString(CultureInfo.InvariantCulture);

            return ReportHelper.LoadReportDataTable(ReportId, ReportDataSet_Table, parameterValues, NullCallback);
        }

        public int CountByOwnerHandleAndVehicleHandle(string ownerHandle, string vehicleHandle, bool isAllYears)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            if (string.IsNullOrEmpty(vehicleHandle))
                throw new ArgumentNullException("vehicleHandle", "Cannot be null or empty");

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;
            parameterValues["VehicleHandle"] = vehicleHandle;
            parameterValues["IsAllYears"] = isAllYears;
            
            DataTable table = ReportHelper.LoadReportDataTable(ReportId, ReportDataSet_Count, parameterValues, NullCallback);

            foreach (DataRow row in table.Rows)
            {
                return Convert.ToInt32(row["NumberOfRows"], CultureInfo.InvariantCulture);
            }

            throw new DataException("Expected one row but got zero!");
        }
    }
}
