using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using FirstLook.Common.Data;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.Pricing.DomainModel.Internet.DataSource
{
    public class InventoryTableSummaryDataSource
    {
        private const string ReportId = "A9CF211B-2330-4a2b-A630-E481CD967326";

        private const string ReportDataSet = "InventoryTableSummary";

        private readonly DataTableCallback NullCallback = new DataTableCallback();

        public IList<InventoryTableFilterOption> FindByOwnerHandleAndModeAndSaleStrategyAndFilters(string ownerHandle, string mode, string saleStrategy, string filterMode, int filterColumnIndex)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            if (string.IsNullOrEmpty(mode) || (!mode.Equals("A") && !mode.Equals("R") && !mode.Equals("N")))
                throw new ArgumentNullException("mode", "Valid values are 'A' and 'R' and 'N'");

            if (string.IsNullOrEmpty(saleStrategy) || (!saleStrategy.Equals("A") && !saleStrategy.Equals("R")))
                throw new ArgumentNullException("saleStrategy", "Valid values are 'A' and 'R'");

            if (string.IsNullOrEmpty(filterMode) || (!filterMode.Equals("A") && !filterMode.Equals("R") && !filterMode.Equals("N")))
                throw new ArgumentNullException("filterMode", "Valid values are 'A' and 'R' and 'N'");

            if (filterMode.Equals(mode) && !filterMode.Equals("N"))
                throw new ArgumentException(string.Format(CultureInfo.CurrentUICulture, "filterMode ({0}) and mode ({1}) cannot be the same", mode, filterMode), "filterMode");

            if (filterColumnIndex < -1 || filterColumnIndex > 6)
                throw new ArgumentOutOfRangeException("filterColumnIndex", "Valid values are between -1 and 6");

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;
            parameterValues["Mode"] = mode;
            parameterValues["SaleStrategy"] = saleStrategy;
            parameterValues["FilterMode"] = filterMode;
            parameterValues["FilterColumnIndex"] = filterColumnIndex.ToString(CultureInfo.InvariantCulture);

            List<InventoryTableFilterOption> list = new List<InventoryTableFilterOption>();
            
			DataTable table = ReportHelper.LoadReportDataTable(ReportId, ReportDataSet, parameterValues, NullCallback);

            // get total number of units for display

            DataRow[] allInventoryRows = table.Select("Name = 'All'");

            string labelSuffix;

            if (allInventoryRows.Length > 0)
                labelSuffix = string.Format(CultureInfo.InvariantCulture, " of {0} Units)", allInventoryRows[0]["Units"]);
            else
                labelSuffix = " Units)";

            foreach (DataRow row in table.Rows)
            {
                if (String.Equals(row["Name"].ToString(), "All", StringComparison.OrdinalIgnoreCase))
                {
                    list.Add(new InventoryTableFilterOption(string.Format(CultureInfo.InvariantCulture, "{0} ({1} units)", "All", row["Units"]), string.Format(CultureInfo.InvariantCulture, "{0}", row["ColumnIndex"])));
                }
                else                                
                {
                    list.Add(new InventoryTableFilterOption(string.Format(CultureInfo.InvariantCulture, "{0} ({1} {2}", row["Name"], row["Units"], labelSuffix), string.Format(CultureInfo.InvariantCulture, "{0}", row["ColumnIndex"])));
                }
            }

            return list;
        }
    }
}
