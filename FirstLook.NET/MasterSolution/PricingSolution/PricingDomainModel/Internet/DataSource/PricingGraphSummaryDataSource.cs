using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.Pricing.DomainModel.Internet.DataSource
{
    public class PricingGraphSummaryDataSource
    {
        private const string ReportId = "BE0753B9-0D96-4502-8918-306A89883B7E";

        private const string ReportDataSet = "PricingGraphSummary";

        private readonly DataTableCallback NullCallback = new DataTableCallback();

        public DataTable FindByOwnerHandle(string ownerHandle, string saleStrategy)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            if (string.IsNullOrEmpty(saleStrategy) || (!saleStrategy.Equals("A") && !saleStrategy.Equals("R")))
                throw new ArgumentNullException("saleStrategy", "Valid values are 'A' and 'R'");

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;
            parameterValues["SaleStrategy"] = saleStrategy;

            return ReportHelper.LoadReportDataTable(ReportId, ReportDataSet, parameterValues, NullCallback);
        }
    }
}
