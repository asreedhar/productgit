using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.Pricing.DomainModel.Internet.DataSource
{
    public class SellerInventoryTableDataSource
    {
        private const string ReportId = "DBA4D32E-6EF7-4d49-AF43-081DCD21C0A8";

        private const string ReportDataSet = "SellerInventoryTable";

        private readonly DataTableCallback NullCallback = new DataTableCallback();

        public DataTable FindBySellerId(string ownerHandle)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;

            return ReportHelper.LoadReportDataTable(ReportId, ReportDataSet, parameterValues, NullCallback);
        }
    }
}
