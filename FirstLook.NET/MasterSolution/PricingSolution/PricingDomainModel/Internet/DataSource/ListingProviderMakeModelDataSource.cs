using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using FirstLook.Common.Data;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.Pricing.DomainModel.Internet.DataSource
{
    public class ListingProviderMakeModelDataSource
    {
        private const string ReportId = "FF886EAC-903C-4b12-A5C6-EBFD94877927";

        private const string ReportDataSet = "ListingProviderMakeModel";

        private readonly DataTableCallback NullCallback = new DataTableCallback();

        public DataTable FindByOwnerHandleAndVehicleHandleAndProvider(String ownerHandle, String vehicleHandle, int providerId)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            if (string.IsNullOrEmpty(vehicleHandle))
                throw new ArgumentNullException("vehicleHandle", "Cannot be null or empty");

            //cars.com id =2, autotrader id = 4, carsoup id = 5
            if (providerId != 2 && providerId != 4 && providerId != 5)
                throw new ArgumentOutOfRangeException("providerId", "Not a valid Listing Provider ID");

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;
            parameterValues["VehicleHandle"] = vehicleHandle;
            parameterValues["ProviderID"] = providerId.ToString(CultureInfo.InvariantCulture);

            return ReportHelper.LoadReportDataTable(ReportId, ReportDataSet, parameterValues, NullCallback);
        }
		
        //[Obsolete("Bluewhale does not contain this function...only here for legacy purposes")]
        public DataTable FindByOwnerHandleAndVehicleHandleAndProvider(String ownerHandle, String searchHandle, String vehicleHandle, int providerId)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            if (string.IsNullOrEmpty(searchHandle))
                throw new ArgumentNullException("searchHandle", "Cannot be null or empty");

            if (string.IsNullOrEmpty(vehicleHandle))
                throw new ArgumentNullException("vehicleHandle", "Cannot be null or empty");
        
            //cars.com id =2, autotrader id = 4, carsoup id = 5
            if(providerId != 2 && providerId != 4 && providerId != 5)
                throw new ArgumentOutOfRangeException("providerId", "Not a valid Listing Provider ID");

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;
            parameterValues["SearchHandle"] = searchHandle;
            parameterValues["VehicleHandle"] = vehicleHandle;
            parameterValues["ProviderID"] = providerId.ToString(CultureInfo.InvariantCulture);

            return ReportHelper.LoadReportDataTable(ReportId, ReportDataSet, parameterValues, NullCallback);
        }

    }
}
