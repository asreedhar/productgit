using System.Collections.ObjectModel;
using System.Data;
using FirstLook.Common.Data;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Search;

namespace FirstLook.Pricing.DomainModel.Internet.DataSource
{
    public class ReferenceLookupDataSource
    {
        private ReadOnlyCollection<Distance> _distances;

        public ReadOnlyCollection<Distance> GetDistanceCollection()
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = @"
                        SELECT DistanceBucketID, Distance
                        FROM   [Pricing].DistanceBucket
                    ";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        _distances = new ReadOnlyCollection<Distance>(DistanceCollection.GetDistanceCollection(reader));
                    }
                }
            }

            return _distances;
        }
    }
}
