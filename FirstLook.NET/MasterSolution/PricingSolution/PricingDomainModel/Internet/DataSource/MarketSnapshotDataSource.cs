using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.Pricing.DomainModel.Internet.DataSource
{
    public class MarketSnapshotDataSource
    {
        private const string ReportId = "C1F596F1-74B8-44d6-830D-43034C154865";

        private const string ReportDataSet = "MarketSnapshot";

        private readonly DataTableCallback NullCallback = new DataTableCallback();

        public DataTable FindByOwnerHandleAndSearchHandle(String ownerHandle, String searchHandle)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            if (string.IsNullOrEmpty(searchHandle))
                throw new ArgumentNullException("searchHandle", "Cannot be null or empty");

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;
            parameterValues["SearchHandle"] = searchHandle;

            return ReportHelper.LoadReportDataTable(ReportId, ReportDataSet, parameterValues, NullCallback);
        }
    }
}
