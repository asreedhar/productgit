using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using FirstLook.Common.Data;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.Pricing.DomainModel.Internet.DataSource
{
    public class InventoryTableDataSource
    {
        private const string ReportId = "B9F3240B-DD61-4cd1-84FD-950EC70C196B";
      
        private const string ReportDataSet_Table = "InventoryTable";

        private const string ReportDataSet_Count = "InventoryTableCount";

        private readonly DataTableCallback NullCallback = new DataTableCallback();

        public static string Id
        {
            get { return ReportId; }
        }

        public static string TableDataSet
        {
            get { return ReportDataSet_Table; }
        }

        public static string CountDataSet
        {
            get { return ReportDataSet_Count; }
        }

        public DataTable FindByOwnerHandleAndModeAndColumnIndexAndFilters(string ownerHandle, string mode, string saleStrategy, int columnIndex, string filterMode, int filterColumnIndex, string sortColumns, int maximumRows, int startRowIndex, string inventoryFilter)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            if (string.IsNullOrEmpty(mode) || (!mode.Equals("A") && !mode.Equals("R") && !mode.Equals("N")))
                throw new ArgumentNullException("mode", "Valid values are 'A' and 'R' and 'N'");

            if (string.IsNullOrEmpty(saleStrategy) || (!saleStrategy.Equals("A") && !saleStrategy.Equals("R")))
                throw new ArgumentNullException("saleStrategy", "Valid values are 'A' and 'R'");

            if (columnIndex < -1 || columnIndex > 6)
                throw new ArgumentOutOfRangeException("columnIndex", "Valid values are between -1 and 6");

            if (string.IsNullOrEmpty(filterMode) || (!filterMode.Equals("A") && !filterMode.Equals("R") && !filterMode.Equals("N")))
                throw new ArgumentNullException("filterMode", "Valid values are 'A' and 'R' and 'N'");

            if (filterMode.Equals(mode) && !filterMode.Equals("N"))
                throw new ArgumentException(string.Format(CultureInfo.CurrentUICulture, "filterMode ({0}) and mode ({1}) cannot be the same", mode, filterMode), "filterMode");

            if (filterColumnIndex < 0 || filterColumnIndex > 6)
                throw new ArgumentOutOfRangeException("filterColumnIndex", "Valid values are between 1 and 6");

            if (string.IsNullOrEmpty(sortColumns))
            {
                sortColumns = mode.Equals("A") ? "Age DESC" : "PctAvgMarketPrice DESC";
            }

            if (!sortColumns.Contains("StockNumber"))
            {
                sortColumns += ", StockNumber DESC"; /// Always sort end a sort by a unique column to ensure reliable results, PM
            }

            if (!string.IsNullOrEmpty(inventoryFilter))
                inventoryFilter = inventoryFilter.Trim();

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;
            parameterValues["Mode"] = mode;
            parameterValues["SaleStrategy"] = saleStrategy;
            parameterValues["ColumnIndex"] = columnIndex.ToString(CultureInfo.InvariantCulture);
            parameterValues["FilterMode"] = filterMode;
            parameterValues["FilterColumnIndex"] = filterColumnIndex.ToString(CultureInfo.InvariantCulture);
            parameterValues["SortColumns"] = sortColumns;
            parameterValues["MaximumRows"] = maximumRows.ToString(CultureInfo.InvariantCulture);
            parameterValues["StartRowIndex"] = startRowIndex.ToString(CultureInfo.InvariantCulture);
            parameterValues["InventoryFilter"] = inventoryFilter;

            return ReportHelper.LoadReportDataTable(ReportId, ReportDataSet_Table, parameterValues, NullCallback);
        }

        public int CountByOwnerHandleAndModeAndColumnIndexAndFilters(string ownerHandle, string mode, string saleStrategy, int columnIndex, string filterMode, int filterColumnIndex, string inventoryFilter)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            if (string.IsNullOrEmpty(mode) || (!mode.Equals("A") && !mode.Equals("R") && !mode.Equals("N")))
                throw new ArgumentNullException("mode", "Valid values are 'A' and 'R' and 'N'");

            if (string.IsNullOrEmpty(saleStrategy) || (!saleStrategy.Equals("A") && !saleStrategy.Equals("R")))
                throw new ArgumentNullException("saleStrategy", "Valid values are 'A' and 'R'");

            if (columnIndex < -1 || columnIndex > 6)
                throw new ArgumentOutOfRangeException("columnIndex", "Valid values are between -1 and 6");

            if (string.IsNullOrEmpty(filterMode) || (!filterMode.Equals("A") && !filterMode.Equals("R") && !filterMode.Equals("N")))
                throw new ArgumentNullException("filterMode", "Valid values are 'A' and 'R' and 'N'");

            if (filterMode.Equals(mode) && !filterMode.Equals("N"))
                throw new ArgumentException(string.Format(CultureInfo.CurrentUICulture, "filterMode ({0}) and mode ({1}) cannot be the same", mode, filterMode), "filterMode");

            if (filterColumnIndex < 0 || filterColumnIndex > 6)
                throw new ArgumentOutOfRangeException("filterColumnIndex", "Valid values are between 1 and 6");

            if (!string.IsNullOrEmpty(inventoryFilter))
                inventoryFilter = inventoryFilter.Trim();

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;
            parameterValues["Mode"] = mode;
            parameterValues["SaleStrategy"] = saleStrategy;
            parameterValues["ColumnIndex"] = columnIndex.ToString(CultureInfo.InvariantCulture);
            parameterValues["FilterMode"] = filterMode;
            parameterValues["FilterColumnIndex"] = filterColumnIndex.ToString(CultureInfo.InvariantCulture);
            parameterValues["InventoryFilter"] = inventoryFilter;

            DataTable table = ReportHelper.LoadReportDataTable(ReportId, ReportDataSet_Count, parameterValues, NullCallback);

            foreach (DataRow row in table.Rows)
            {
                return Convert.ToInt32(row["NumberOfRows"], CultureInfo.InvariantCulture);
            }

            throw new DataException("Expected one row but got zero!");
        }
    }
}
