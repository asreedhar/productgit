﻿using System.Reflection;
using System.Runtime.CompilerServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("FirstLook.Pricing.DomainModel")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyProduct("FirstLook.Pricing.DomainModel")]

// Make internals visible to test project
[assembly: InternalsVisibleTo("PricingDomainModel_Test" )]

[assembly: InternalsVisibleTo("PricingDomainModel_IntegrationTests")]
