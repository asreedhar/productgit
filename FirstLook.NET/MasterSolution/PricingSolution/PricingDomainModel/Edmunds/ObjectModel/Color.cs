using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using Csla;

namespace FirstLook.Pricing.DomainModel.Edmunds.ObjectModel
{
    [Serializable]
    public class Color : BusinessBase<Color>
    {
        #region BusinessMethods

        private int id;
        private string value;

        public int Id
        {
            get { return id; }
        }

        public string Value
        {
            get { return value; }
        }

        protected override object GetIdValue()
        {
            return id;
        }

        #endregion

        #region Factory Methods

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal static Color GetColor(IDataRecord record)
        {
            return GetColor(record, true);
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal static Color GetColor(IDataRecord record, bool hasDescription)
        {
            return new Color(record, hasDescription);
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Color(IDataRecord record, bool hasDescription)
        {
            MarkAsChild();
            Fetch(record, hasDescription);
        }

        #endregion

        #region Data Access

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Fetch(IDataRecord record, bool hasDescription)
        {
            id = record.GetInt32(record.GetOrdinal("ColorId"));
            if (hasDescription)
                value = record.GetString(record.GetOrdinal("ColorDescription"));
            else
                value = string.Empty;
        }

        #endregion
    }
}