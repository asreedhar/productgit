using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using Csla;

namespace FirstLook.Pricing.DomainModel.Edmunds.ObjectModel
{
    [Serializable]
    public class ColorCollection : BusinessListBase<ColorCollection, Color>
    {
        #region Business Methods

        public Color GetItem(int colorId)
        {
            foreach (Color item in this)
                if (item.Id == colorId) return item;

            return null;
        }

        #endregion

        #region Factory Methods

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA invokes via reflection")]
        internal static ColorCollection GetColorCollection(IDataReader reader)
        {
            return new ColorCollection(reader);
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA invokes via reflection")]
        private ColorCollection(IDataReader reader)
        {
            MarkAsChild();
            Fetch(reader);
        }

        #endregion

        #region Data Access

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA invokes via reflection")]
        private void Fetch(IDataReader reader)
        {
            RaiseListChangedEvents = false;
            while (reader.Read())
                Add(Color.GetColor(reader));
            RaiseListChangedEvents = true;
        }

        #endregion
    }
}