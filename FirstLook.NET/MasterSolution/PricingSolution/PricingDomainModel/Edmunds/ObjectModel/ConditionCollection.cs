using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using Csla;

namespace FirstLook.Pricing.DomainModel.Edmunds.ObjectModel
{
    [Serializable]
    public class ConditionCollection : BusinessListBase<ConditionCollection, Condition>
    {
        #region Business Methods

        public Condition GetItem(int conditionId)
        {
            foreach (Condition item in this)
                if (item.Id == conditionId) return item;
            return null;
        }

        #endregion

        #region Factory Methods

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA invokes via reflection")]
        internal static ConditionCollection GetConditionCollection(IDataReader reader)
        {
            return new ConditionCollection(reader);
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA invokes via reflection")]
        private ConditionCollection(IDataReader reader)
        {
            MarkAsChild();
            Fetch(reader);
        }

        #endregion

        #region Data Access

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA invokes via reflection")]
        private void Fetch(IDataReader reader)
        {
            RaiseListChangedEvents = false;
            while (reader.Read())
                Add(Condition.GetCondition(reader));
            RaiseListChangedEvents = true;
        }

        #endregion
    }
}