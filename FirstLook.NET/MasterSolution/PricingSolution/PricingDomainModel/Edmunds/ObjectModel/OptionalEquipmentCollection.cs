using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Csla;

namespace FirstLook.Pricing.DomainModel.Edmunds.ObjectModel
{
    [Serializable]
    public class OptionalEquipmentCollection : BusinessListBase<OptionalEquipmentCollection, OptionalEquipment>, IXmlSerializable
    {
        #region Business Methods

        public OptionalEquipment GetItem(long optionId)
        {
            foreach (OptionalEquipment item in this)
                if (item.Id == optionId) return item;
            return null;
        }

        #endregion

        #region Factory Methods

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal static OptionalEquipmentCollection GetOptionCollection(IDataReader reader)
        {
            return new OptionalEquipmentCollection(reader);
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal static OptionalEquipmentCollection GetOptionCollection(XmlReader reader)
        {
            return new OptionalEquipmentCollection(reader);
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal OptionalEquipmentCollection()
        {
            MarkAsChild();
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private OptionalEquipmentCollection(IDataReader reader)
        {
            MarkAsChild();
            Fetch(reader);
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private OptionalEquipmentCollection(XmlReader reader)
        {
            MarkAsChild();
            ReadXml(reader);
        }

        #endregion

        #region Data Access

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Fetch(IDataReader reader)
        {
            RaiseListChangedEvents = false;
            while (reader.Read())
                Add(OptionalEquipment.GetOption(reader));
            RaiseListChangedEvents = true;
        }

        #endregion

        internal const string elementName = "EdmundsOptions";

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            RaiseListChangedEvents = false;
            if (reader.MoveToContent() == XmlNodeType.Element && reader.Name == elementName)
            {
                bool isEmptyElement = reader.IsEmptyElement;
                reader.ReadStartElement(elementName);
                while (OptionalEquipment.CanReadXml(reader))
                    Add(OptionalEquipment.GetOption(reader));
                if (!isEmptyElement)
                    reader.ReadEndElement();
            }
            RaiseListChangedEvents = true;
        }

        public void WriteXml(XmlWriter writer)
        {
            if (Count > 0)
            {
                writer.WriteStartElement(elementName);
                foreach (OptionalEquipment item in this)
                    item.WriteXml(writer);
                writer.WriteEndElement();
            }
        }
    }
}