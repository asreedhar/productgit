using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Edmunds.ObjectModel
{
    [Serializable]
    [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Tmv", Justification = "Well known acronym")]
    public class EdmundsTmvPanel : ReadOnlyBase<EdmundsTmvPanel>
    {
        private readonly Guid guid;

        private string vehicleDescription;
        private int mileage;
        private string color;
        private ReadOnlyCollection<Trim> trims;
        private ReadOnlyCollection<Color> colors;
        private ReadOnlyCollection<Condition> conditions;
        private bool resultsAvailable = false;

        #region Business Methods

        protected override object GetIdValue()
        {
            return guid;
        }

        public string VehicleDescription
        {
            get { return vehicleDescription; }
        }

        public int Mileage
        {
            get { return mileage; }
        }

        public string Color
        {
            get { return color; }
        }

        public bool ResultsAvailable
        {
            get { return resultsAvailable;  }
        }

        public ReadOnlyCollection<Trim> Trims
        {
            get { return trims; }
        }

        public ReadOnlyCollection<Color> Colors
        {
            get { return colors; }
        }

        public ReadOnlyCollection<Condition> Conditions
        {
            get { return conditions; }
        }

        #endregion

        #region Data Access

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA: Method invoked via reflection in base class")]
        private void DataPortal_Fetch(EdmundsTmvInputRecord criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Pricing.TmvPanel#Fetch";
                    command.AddParameterWithValue("VehicleHandle", DbType.String, false, criteria.VehicleHandle);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                        vehicleDescription = reader.GetString(reader.GetOrdinal("VehicleDescription"));
                        mileage = reader.GetInt32(reader.GetOrdinal("Mileage"));
                        color = reader.GetString(reader.GetOrdinal("Color"));
                        reader.NextResult();
                        trims = new ReadOnlyCollection<Trim>(TrimCollection.GetTrimCollection(reader));
                        reader.NextResult();
                        colors = new ReadOnlyCollection<Color>(ColorCollection.GetColorCollection(reader));
                        reader.NextResult();
                            conditions =
                                new ReadOnlyCollection<Condition>(ConditionCollection.GetConditionCollection(reader));
                            resultsAvailable = true;
                        } 
                    }
                }
            }
        }

        #endregion

        #region Factory Methods

        public static EdmundsTmvPanel GetSearchPanel(EdmundsTmvInputRecord input)
        {
            return DataPortal.Fetch<EdmundsTmvPanel>(input);
        }

        private EdmundsTmvPanel()
        {
            guid = Guid.NewGuid();
        }

        #endregion
    }
}