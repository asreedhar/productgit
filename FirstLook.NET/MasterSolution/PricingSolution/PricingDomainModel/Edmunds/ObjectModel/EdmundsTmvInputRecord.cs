using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Text;
using System.Xml;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Edmunds.ObjectModel
{
    [Serializable]
    [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Tmv", Justification = "Well known acronym")]
    public class EdmundsTmvInputRecord : BusinessBase<EdmundsTmvInputRecord>
    {
        #region Business Methods

        private string ownerHandle;
        private string vehicleHandle;
        private Trim trim;
        private Color color;
        private Condition condition;
        private OptionalEquipmentCollection options;

        private bool objectNotFound; // defaults to false

        protected override object GetIdValue()
        {
            return new Criteria(ownerHandle, vehicleHandle);
        }

        public string OwnerHandle
        {
            get { return ownerHandle; }
        }

        public string VehicleHandle
        {
            get { return vehicleHandle; }
        }

        public Trim Trim
        {
            get { return trim; }
            set
            {
                if (Trim != value)
                {
                    trim = value;
                    PropertyHasChanged("Trim");
                }
            }
        }

        public Color Color
        {
            get { return color; }
            set
            {
                if (color != value)
                {
                    color = value;
                    PropertyHasChanged("Color");
                }
            }
        }

        public Condition Condition
        {
            get { return condition; }
            set
            {
                if (condition != value)
                {
                    condition = value;
                    PropertyHasChanged("Condition");
                }
            }
        }

        public OptionalEquipmentCollection Options
        {
            get { return options; }
        }

        #endregion

        #region Data Access

        [Serializable]
        private class Criteria : IEquatable<Criteria>
        {
            private readonly string ownerHandle;
            private readonly string vehicleHandle;

            public Criteria(string ownerHandle, string vehicleHandle)
            {
                this.ownerHandle = ownerHandle;
                this.vehicleHandle = vehicleHandle;
            }

            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA: Is called via reflected method invokation")]
            public string OwnerHandle
            {
                get { return ownerHandle; }
            }

            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA: Is called via reflected method invokation")]
            public string VehicleHandle
            {
                get { return vehicleHandle; }
            }

            public bool Equals(Criteria criteria)
            {
                if (criteria == null) return false;
                return Equals(ownerHandle, criteria.ownerHandle) && Equals(vehicleHandle, criteria.vehicleHandle);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(this, obj)) return true;
                return Equals(obj as Criteria);
            }

            public override int GetHashCode()
            {
                return ownerHandle.GetHashCode() + 29*vehicleHandle.GetHashCode();
            }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA: Method invoked via reflection in base class")]
        private void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand((connection.CreateCommand())))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Pricing.TMVInputRecord#Fetch";
                    command.AddParameterWithValue("OwnerHandle", DbType.String, false, criteria.OwnerHandle);
                    command.AddParameterWithValue("VehicleHandle", DbType.String, false, criteria.VehicleHandle);

                    ownerHandle = criteria.OwnerHandle;
                    vehicleHandle = criteria.VehicleHandle;

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            color = Color.GetColor(reader, false);
                            condition = Condition.GetCondition(reader, false);
                            trim = Trim.GetTrim(reader, false);

                            SqlXml optionsXML = ((SqlDataReader) reader).GetSqlXml(reader.GetOrdinal("OptionXML"));
                            if (!optionsXML.IsNull && !String.IsNullOrEmpty(optionsXML.ToString()))
                            {
                                ReadOptionXml(optionsXML);
                            }
                            else
                            {
                                options = new OptionalEquipmentCollection();
                            }

                            Options.ListChanged += delegate { PropertyHasChanged("Options"); };

                            MarkOld();
                        }
                        else
                        {
                            objectNotFound = true;
                        }
                    }
                }
            }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA: Is called via reflected method invokation")]
        private void ReadOptionXml(SqlXml optionsXML)
        {
            using (XmlReader optionsReader = optionsXML.CreateReader())
            {
                options = OptionalEquipmentCollection.GetOptionCollection(optionsReader);
            }
        }

        protected override void DataPortal_Insert()
        {
            if (!IsNew)
            {
                return; 
            }
            // build options xml
            StringBuilder optionsStringBuffer = GetOptionsXmlStringBuffer();

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand((connection.CreateCommand())))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Pricing.TMVInputRecord#Insert";
                    command.AddParameterWithValue("OwnerHandle", DbType.String, false, ownerHandle);
                    command.AddParameterWithValue("VehicleHandle", DbType.String, false, vehicleHandle);
                    command.AddParameterWithValue("EdmundsStyleId", DbType.Int32, false, Trim.Id);
                    command.AddParameterWithValue("ColorID", DbType.Int32, false, Color.Id);
                    command.AddParameterWithValue("ConditionID", DbType.Int32, false, Condition.Id);

                    if (!String.IsNullOrEmpty(optionsStringBuffer.ToString()))
                    {
                        command.AddParameterWithValue("OptionXml", DbType.Xml, false,
                                                      new SqlXml(
                                                          XmlReader.Create(
                                                              new StringReader(optionsStringBuffer.ToString()))));
                    }
                    else
                    {
                        command.AddParameterWithValue("OptionXml", DbType.Xml, false, new SqlXml());
                    }
                    command.ExecuteNonQuery();

                    MarkOld();
                }
            }
        }

        protected override void DataPortal_Update()
        {
            if (!IsDirty)
            {
                return;
            }

            // build options xml
            StringBuilder optionsStringBuffer = GetOptionsXmlStringBuffer();

            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand((connection.CreateCommand())))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Pricing.TMVInputRecord#Update";
                    command.AddParameterWithValue("OwnerHandle", DbType.String, false, ownerHandle);
                    command.AddParameterWithValue("VehicleHandle", DbType.String, false, vehicleHandle);
                    command.AddParameterWithValue("EdmundsStyleID", DbType.String, false, trim.Id);
                    command.AddParameterWithValue("ColorID", DbType.String, false, color.Id);
                    command.AddParameterWithValue("ConditionID", DbType.Int32, false, condition.Id);

                    if (!String.IsNullOrEmpty(optionsStringBuffer.ToString()))
                    {
                        command.AddParameterWithValue("OptionXml", DbType.Xml, false,
                                                      new SqlXml(
                                                          XmlReader.Create(
                                                              new StringReader(optionsStringBuffer.ToString()))));
                    }
                    else
                    {
                        command.AddParameterWithValue("OptionXml", DbType.Xml, false, new SqlXml());
                    }


                    command.ExecuteNonQuery();

                    MarkOld();
                }
            }
        }

        protected override void DataPortal_Create()
        {
            options = new OptionalEquipmentCollection();
        }

        private StringBuilder GetOptionsXmlStringBuffer()
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = Encoding.ASCII;
            settings.Indent = true;
            settings.IndentChars = "  ";

            StringBuilder sb = new StringBuilder();
            XmlWriter writer = XmlWriter.Create(sb, settings);
            Options.WriteXml(writer);
            writer.Flush();
            return sb;
        }

        #endregion

        #region Factory Methods

        public static EdmundsTmvInputRecord GetInputRecord(string ownerHandle, string vehicleHandle)
        {
            EdmundsTmvInputRecord tmv = DataPortal.Fetch<EdmundsTmvInputRecord>(new Criteria(ownerHandle, vehicleHandle));
            if (tmv.objectNotFound)
            {
                tmv = DataPortal.Create<EdmundsTmvInputRecord>();
                tmv.ownerHandle = ownerHandle;
                tmv.vehicleHandle = vehicleHandle;
            }
            return tmv;
        }

        private EdmundsTmvInputRecord()
        {
        }

        #endregion
    }
}