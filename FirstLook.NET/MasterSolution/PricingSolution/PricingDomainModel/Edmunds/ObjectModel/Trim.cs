using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using Csla;

namespace FirstLook.Pricing.DomainModel.Edmunds.ObjectModel
{
    [Serializable]
    public class Trim : BusinessBase<Trim>
    {
        #region Business Methods

        private int id;
        private string value;

        public int Id
        {
            get { return id; }
        }

        public string Value
        {
            get { return value; }
        }

        protected override object GetIdValue()
        {
            return id;
        }

        #endregion

        #region Factory Methods

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal static Trim GetTrim(IDataRecord record)
        {
            return GetTrim(record, true);
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal static Trim GetTrim(IDataRecord record, bool hasDescription)
        {
            return new Trim(record, hasDescription);
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private Trim(IDataRecord record, bool hasDescription)
        {
            MarkAsChild();
            Fetch(record, hasDescription);
        }

        #endregion

        #region Data Access

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Fetch(IDataRecord record, bool hasDescription)
        {
            id = record.GetInt32(record.GetOrdinal("EdmundsStyleId"));
            if (hasDescription)
                value = record.GetString(record.GetOrdinal("TrimDescription"));
            else
                value = string.Empty;
        }

        #endregion
    }
}