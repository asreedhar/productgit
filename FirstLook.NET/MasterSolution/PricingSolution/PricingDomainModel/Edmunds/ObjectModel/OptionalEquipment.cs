using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Csla;

namespace FirstLook.Pricing.DomainModel.Edmunds.ObjectModel
{
    [Serializable]
    public class OptionalEquipment : BusinessBase<OptionalEquipment>, IXmlSerializable
    {
        #region Business Methods

        private long id;
        private int edmundsStyleId;
        private string description;
        private bool selected;

        public long Id
        {
            get { return id; }
        }

        public int EdmundsStyleId
        {
            get { return edmundsStyleId; }
        }

        public string Description
        {
            get { return description; }
        }

        public bool Selected
        {
            get { return selected; }
            set { selected = value; }
        }

        protected override object GetIdValue()
        {
            return id;
        }

        #endregion

        #region Factory Methods

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal static OptionalEquipment GetOption(IDataRecord record)
        {
            return new OptionalEquipment(record);
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal static OptionalEquipment GetOption(XmlReader record)
        {
            return new OptionalEquipment(record);
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private OptionalEquipment(IDataRecord record)
        {
            MarkAsChild();
            Fetch(record);
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private OptionalEquipment(XmlReader reader)
        {
            MarkAsChild();
            ReadXml(reader);
        }

        #endregion

        #region Data Access

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Fetch(IDataRecord record)
        {
            id = record.GetInt64(record.GetOrdinal("OptionId")); // record.GetValue()
            edmundsStyleId = record.GetInt32(record.GetOrdinal("EdmundsStyleId"));
            description = record.GetString(record.GetOrdinal("OptionDescription"));
            selected = Convert.ToBoolean(record.GetValue(record.GetOrdinal("IsSelected")), CultureInfo.InvariantCulture);
        }

        #endregion

        public XmlSchema GetSchema()
        {
            return null;
        }

        private const string elementName = "EdmundsOption";

        public static bool CanReadXml(XmlReader reader)
        {
            return (reader.MoveToContent() == XmlNodeType.Element && reader.Name == elementName);
        }

        public void ReadXml(XmlReader reader)
        {
            if (CanReadXml(reader))
            {
                if (reader.HasAttributes && reader.MoveToAttribute("Id"))
                {
                    id = Convert.ToInt64(reader.Value, CultureInfo.InvariantCulture);
                    reader.MoveToElement();
                }
                reader.ReadStartElement(elementName);
                reader.ReadStartElement("OptionDescription");
                description = reader.ReadContentAsString();
                reader.ReadEndElement();
                reader.ReadStartElement("IsSelected");
                selected = reader.ReadContentAsBoolean();
                reader.ReadEndElement();
                reader.ReadEndElement();
            }
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(elementName);
            writer.WriteAttributeString("Id", Convert.ToString(Id, CultureInfo.InvariantCulture));
            writer.WriteStartElement("OptionDescription");
            writer.WriteString(description);
            writer.WriteEndElement();
            writer.WriteStartElement("IsSelected");
            writer.WriteValue(selected);
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
    }
}