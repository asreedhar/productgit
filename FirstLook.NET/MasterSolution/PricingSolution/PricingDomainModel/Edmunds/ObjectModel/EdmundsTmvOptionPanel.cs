using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Edmunds.ObjectModel
{
    [Serializable]
    [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Tmv", Justification = "Well known acronym")]
    public class EdmundsTmvOptionPanel : ReadOnlyBase<EdmundsTmvOptionPanel>
    {
        #region Business Methods

        private Guid guid;
        private int edmundsStyleId;
        private ReadOnlyCollection<OptionalEquipment> options;

        public ReadOnlyCollection<OptionalEquipment> Options
        {
            get { return options; }
        }

        protected override object GetIdValue()
        {
            return guid;
        }

        public int EdmundsStyleId
        {
            get { return edmundsStyleId; }
        }

        #endregion

        #region Data Access

        [Serializable]
        private class Criteria
        {
            private readonly int edmundsStyleId;

            public Criteria(Int32 id)
            {
                edmundsStyleId = id;
            }

            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
            public int EdmundsStyleId
            {
                get { return edmundsStyleId; }
            }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA: Invoked through reflection")]
        private void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Pricing.TmvOptionsPanel#Fetch";
                    command.AddParameterWithValue("Ed_Style_Id", DbType.Int32, false, criteria.EdmundsStyleId);

                    edmundsStyleId = criteria.EdmundsStyleId;

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        options = new ReadOnlyCollection<OptionalEquipment>(OptionalEquipmentCollection.GetOptionCollection(reader));
                    }
                }
            }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA: Invoked through reflection")]
        private void DataPortal_Create()
        {
            guid = new Guid();
            options = new ReadOnlyCollection<OptionalEquipment>(new OptionalEquipmentCollection());
        }

        #endregion

        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Tmv")]
        public static EdmundsTmvOptionPanel GetEdmundsTmvOptionPanel(Int32 edmundsStyleId)
        {
            return DataPortal.Fetch<EdmundsTmvOptionPanel>(new Criteria(edmundsStyleId));
        }

        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Tmv")]
        public static EdmundsTmvOptionPanel EmptyEdmundsTmvOptionPanel()
        {
            return DataPortal.Create<EdmundsTmvOptionPanel>();
        }

        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Tmv")]
        private EdmundsTmvOptionPanel()
        {
        }
    }
}