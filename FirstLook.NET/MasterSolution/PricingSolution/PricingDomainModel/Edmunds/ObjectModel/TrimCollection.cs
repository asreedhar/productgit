using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using Csla;

namespace FirstLook.Pricing.DomainModel.Edmunds.ObjectModel
{
    [Serializable]
    public class TrimCollection : BusinessListBase<TrimCollection, Trim>
    {
        #region Business Methods

        public Trim GetItem(int trimId)
        {
            foreach (Trim item in this)
                if (item.Id == trimId)
                    return item;
            return null;
        }

        #endregion

        #region Factory Methods

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal static TrimCollection GetTrimCollection(IDataReader reader)
        {
            return new TrimCollection(reader);
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private TrimCollection(IDataReader reader)
        {
            MarkAsChild();
            Fetch(reader);
        }

        #endregion

        #region Data Access

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Fetch(IDataReader reader)
        {
            RaiseListChangedEvents = false;
            while (reader.Read())
                Add(Trim.GetTrim(reader));
            RaiseListChangedEvents = true;
        }

        #endregion
    }
}