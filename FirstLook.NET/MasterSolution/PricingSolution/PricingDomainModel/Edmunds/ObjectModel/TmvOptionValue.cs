using System;
using System.Diagnostics.CodeAnalysis;

namespace FirstLook.Pricing.DomainModel.Edmunds.ObjectModel
{
    /// <summary>
    /// Simple bean for storing TMV Option Value Results from GetEdmundsTmv procedure.
    /// </summary>
    [Serializable]
    [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Tmv", Justification = "Well known acronym")]
    public class TmvOptionValue
    {
        private readonly string label;
        private readonly long id; // edmundsFeatureId
        private readonly int tradeInValue;
        private readonly int retailValue;

        public TmvOptionValue(string label, long id, int tradeInValue, int retailValue)
        {
            this.label = label;
            this.id = id;
            this.tradeInValue = tradeInValue;
            this.retailValue = retailValue;
        }

        public string Label
        {
            get { return label; }
        }

        public long Id
        {
            get { return id; }
        }

        public int TradeInValue
        {
            get { return tradeInValue; }
        }

        public int RetailValue
        {
            get { return retailValue; }
        }
    }
}