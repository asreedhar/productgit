using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using Csla;
using FirstLook.Common.Data;

namespace FirstLook.Pricing.DomainModel.Edmunds.ObjectModel
{
    [Serializable]
    [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Tmv", Justification = "Well known acronym")]
    public class EdmundsTmvValuesPanel : ReadOnlyBase<EdmundsTmvValuesPanel>
    {
        #region Business Methods

        private readonly Guid guid;

        private TmvOptionValue baseValue;
        private TmvOptionValue equipmentTotal;
        private TmvOptionValue colorTotal;
        private TmvOptionValue regionalTotal;
        private TmvOptionValue mileageTotal;
        private TmvOptionValue conditionTotal;
        private TmvOptionValue total;

        private readonly List<TmvOptionValue> equipmentValues = new List<TmvOptionValue>();

        private int certifiedTotal;

        private bool isCertifiedVehicle;

        public int CertifiedTotal
        {
            get { return certifiedTotal; }
        }

        public bool IsCertifiedVehicle
        {
            get { return isCertifiedVehicle; }
        }

        public TmvOptionValue Total
        {
            get { return total; }
        }

        public TmvOptionValue ConditionTotal
        {
            get { return conditionTotal; }
        }

        public TmvOptionValue MileageTotal
        {
            get { return mileageTotal; }
        }

        public TmvOptionValue RegionalTotal
        {
            get { return regionalTotal; }
        }

        public TmvOptionValue ColorTotal
        {
            get { return colorTotal; }
        }

        public TmvOptionValue EquipmentTotal
        {
            get { return equipmentTotal; }
        }

        public TmvOptionValue BaseValue
        {
            get { return baseValue; }
        }

        protected override object GetIdValue()
        {
            return guid;
        }

        public IList<TmvOptionValue> EquipmentValues
        {
            get { return equipmentValues; }
        }

        #endregion

        #region Data Access

        [Serializable]
        private class Criteria
        {
            private readonly string ownerHandle;
            private readonly string vehicleHandle;
            private readonly string optionsIds; // needed as comma delimited string
            private readonly int edmundsStyleId;
            private readonly int colorId;
            private readonly int conditionId;
            private readonly int mileage;

            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
            public string OwnerHandle
            {
                get { return ownerHandle; }
            }

            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
            public string VehicleHandle
            {
                get { return vehicleHandle; }
            }

            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
            public string OptionsIds
            {
                get { return optionsIds; }
            }

            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
            public int EdmundsStyleId
            {
                get { return edmundsStyleId; }
            }

            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
            public int ColorId
            {
                get { return colorId; }
            }

            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
            public int ConditionId
            {
                get { return conditionId; }
            }

            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
            public int Mileage
            {
                get { return mileage; }
            }

            public Criteria(string ownerHandle, string vehicleHandle, string optionIds, int edmundsStyleId, int colorId, int conditionId,
                            int mileage)
            {
                this.ownerHandle = ownerHandle;
                this.vehicleHandle = vehicleHandle;
                optionsIds = optionIds;
                this.edmundsStyleId = edmundsStyleId;
                this.colorId = colorId;
                this.conditionId = conditionId;
                this.mileage = mileage;
            }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA: Method invoked via reflection in base class")]
        private void DataPortal_Fetch(Criteria criteria)
        {
            using (IDataConnection connection = SimpleQuery.ConfigurationManagerConnection(Database.PricingDatabase))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                using (IDataCommand command = new DataCommand(connection.CreateCommand()))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "Pricing.GetEdmundsTMV";
                    command.AddParameterWithValue("OwnerHandle", DbType.String, false, criteria.OwnerHandle);
                    command.AddParameterWithValue("VehicleHandle", DbType.String, false, criteria.VehicleHandle);
                    command.AddParameterWithValue("Ed_Style_ID", DbType.Int32, false, criteria.EdmundsStyleId);
                    command.AddParameterWithValue("TMV_Color_ID", DbType.Int32, false, criteria.ColorId);
                    command.AddParameterWithValue("TMV_Condition_ID", DbType.Int32, false, criteria.ConditionId);
                    command.AddParameterWithValue("Ed_Equipment_IDs", DbType.String, false, criteria.OptionsIds);
                    command.AddParameterWithValue("Mileage", DbType.Int32, false, criteria.Mileage);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        reader.Read();
                        baseValue = new TmvOptionValue("Base Value", 0,
                            reader.GetInt32(reader.GetOrdinal("TradeIn")),
                            reader.GetInt32(reader.GetOrdinal("DealerRetail")));

                        reader.Read();
                        equipmentTotal = new TmvOptionValue("Equipment Total", 0,
                            reader.GetInt32(reader.GetOrdinal("TradeIn")),
                            reader.GetInt32(reader.GetOrdinal("DealerRetail")));

                        reader.Read();
                        colorTotal = new TmvOptionValue("Color Total", 0,
                            reader.GetInt32(reader.GetOrdinal("TradeIn")),
                            reader.GetInt32(reader.GetOrdinal("DealerRetail")));

                        reader.Read();
                        regionalTotal = new TmvOptionValue("Regional Total", 0,
                            reader.GetInt32(reader.GetOrdinal("TradeIn")),
                            reader.GetInt32(reader.GetOrdinal("DealerRetail")));

                        reader.Read();
                        mileageTotal = new TmvOptionValue("Mileage Total", 0,
                            reader.GetInt32(reader.GetOrdinal("TradeIn")),
                            reader.GetInt32(reader.GetOrdinal("DealerRetail")));

                        reader.Read();
                        conditionTotal = new TmvOptionValue("Condition Total", 0,
                            reader.GetInt32(reader.GetOrdinal("TradeIn")),
                            reader.GetInt32(reader.GetOrdinal("DealerRetail")));

                        reader.Read();
                        total = new TmvOptionValue("Total", 0,
                            reader.GetInt32(reader.GetOrdinal("TradeIn")),
                            reader.GetInt32(reader.GetOrdinal("DealerRetail")));

                        reader.NextResult();
                        while (reader.Read())
                        {
                            TmvOptionValue value = new TmvOptionValue(
                                reader.GetString(reader.GetOrdinal("Label")),
                                reader.GetInt64(reader.GetOrdinal("FeatureId")),
                                reader.GetInt32(reader.GetOrdinal("TradeIn")),
                                reader.GetInt32(reader.GetOrdinal("DealerRetail")));
                            equipmentValues.Add(value);
                        }

                        reader.NextResult();
                        reader.Read();
                        certifiedTotal = reader.GetInt32(reader.GetOrdinal("CertifiedValue"));

                        reader.NextResult();
                        reader.Read();
                        isCertifiedVehicle = reader.GetBoolean(reader.GetOrdinal("isCertified"));
                    }
                }
            }
        }

        #endregion

        #region Factory Methods

        public static EdmundsTmvValuesPanel GetEdmundsValuePanel(string ownerHandle,
                                                                 string selectedOptionsCommaDelimited,
                                                                 int edmundsStyleId, int colorId, int conditionId,
                                                                 int mileage, string vehicleHandle)
        {
            Criteria criteria = new Criteria(ownerHandle, vehicleHandle, selectedOptionsCommaDelimited, edmundsStyleId, colorId, conditionId, mileage);
            return DataPortal.Fetch<EdmundsTmvValuesPanel>(criteria);
        }

        private EdmundsTmvValuesPanel()
        {
            guid = new Guid();
        }

        #endregion
    }
}