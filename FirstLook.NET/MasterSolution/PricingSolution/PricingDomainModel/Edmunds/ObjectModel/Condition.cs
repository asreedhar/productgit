using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using Csla;

namespace FirstLook.Pricing.DomainModel.Edmunds.ObjectModel
{
    [Serializable]
    public class Condition : BusinessBase<Condition>
    {
        #region Business Methods

        private int id;
        private string value;

        public int Id
        {
            get { return id; }
        }

        public string Value
        {
            get { return value; }
        }

        protected override object GetIdValue()
        {
            return id;
        }

        #endregion

        #region Factory Methods

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA invokes via reflection")]
        internal static Condition GetCondition(IDataRecord record)
        {
            return GetCondition(record, true);
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA invokes via reflection")]
        internal static Condition GetCondition(IDataRecord record, bool hasDescription)
        {
            return new Condition(record, hasDescription);
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA invokes via reflection")]
        private Condition(IDataRecord record, bool hasDescription)
        {
            MarkAsChild();
            Fetch(record, hasDescription);
        }

        #endregion

        #region Data Access

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "CSLA invokes via reflection")]
        private void Fetch(IDataRecord record, bool hasDescription)
        {
            id = record.GetInt32(record.GetOrdinal("ConditionId"));
            if (hasDescription)
                value = record.GetString(record.GetOrdinal("ConditionDescription"));
            else
                value = string.Empty;
        }

        #endregion
    }
}