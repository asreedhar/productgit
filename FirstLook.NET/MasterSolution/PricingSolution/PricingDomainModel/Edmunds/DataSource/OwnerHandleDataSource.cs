using System.Collections.Generic;
using System.Data;
using System.Globalization;
using FirstLook.Common.Data;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.Pricing.DomainModel.Edmunds.DataSource
{
    public class OwnerHandleDataSource
    {
        private const string ReportId = "B39FFBE6-16DC-443b-B09B-9F25B491F7AC";

        private const string ReportDataSet = "OwnerHandle";

        private readonly DataTableCallback NullCallback = new DataTableCallback();

        public string FindByDealerId(int dealerId)
        {
            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["DealerID"] = dealerId;

            DataTable table = ReportHelper.LoadReportDataTable(ReportId, ReportDataSet, parameterValues, NullCallback);

            foreach (DataRow row in table.Rows)
            {
                return (string)row["OwnerHandle"];
            }

            throw new DataException(string.Format(CultureInfo.CurrentUICulture, "No OwnerHandle for Dealer {0}", dealerId));
        }
    }
}