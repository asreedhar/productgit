using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Data;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.Pricing.DomainModel.Edmunds.DataSource
{
    public class OwnerNameDataSource
    {
        private const string ReportId = "C9E2D3E4-5719-4263-95EB-71ADFA227D1E";

        private const string ReportDataSet = "OwnerName";

        private readonly DataTableCallback NullCallback = new DataTableCallback();

        public DataTable FindByOwnerHandle(string ownerHandle)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;

            return ReportHelper.LoadReportDataTable(ReportId, ReportDataSet, parameterValues, NullCallback);
        }
    }
}
