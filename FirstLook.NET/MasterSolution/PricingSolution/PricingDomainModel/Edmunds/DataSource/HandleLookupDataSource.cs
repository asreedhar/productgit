using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using FirstLook.Common.Data;
using FirstLook.Reports.App.ReportDefinitionLibrary;

namespace FirstLook.Pricing.DomainModel.Edmunds.DataSource
{
    public class HandleLookupDataSource
    {
        private const string ReportId_Vehicle = "BEDDB3C7-5DD1-4639-B2BE-AEE839704398";

        private const string ReportDataSet_Vehicle = "HandleLookup_Vehicle";

        private const string ReportId_StockNumber = "B9D63E31-4A9F-4ec6-8C41-BA99963A3574";

        private const string ReportDataSet_StockNumber = "HandleLookup_StockNumber";

        private readonly DataTableCallback NullCallback = new DataTableCallback();

        public string FindByOwnerHandleAndVehicle(String ownerHandle, int vehicleEntityTypeId, int vehicleEntityId)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;
            parameterValues["VehicleEntityTypeID"] = vehicleEntityTypeId.ToString(CultureInfo.InvariantCulture);
            parameterValues["VehicleEntityID"] = vehicleEntityId.ToString(CultureInfo.InvariantCulture);

            DataTable table = ReportHelper.LoadReportDataTable(ReportId_Vehicle, ReportDataSet_Vehicle, parameterValues, NullCallback);

            foreach (DataRow row in table.Rows)
            {
                return Convert.ToString(row["VehicleHandle"], CultureInfo.InvariantCulture);
            }

            throw new DataException("Expected one row of handle information but got none");
        }

        public string FindByOwnerHandleAndStockNumber(String ownerHandle, string stockNumber)
        {
            if (string.IsNullOrEmpty(ownerHandle))
                throw new ArgumentNullException("ownerHandle", "Cannot be null or empty");

            if (string.IsNullOrEmpty(stockNumber))
                throw new ArgumentNullException("stockNumber", "Cannot be null or empty");

            Dictionary<string, object> parameterValues = new Dictionary<string, object>();
            parameterValues["OwnerHandle"] = ownerHandle;
            parameterValues["StockNumber"] = stockNumber;

            DataTable table = ReportHelper.LoadReportDataTable(ReportId_StockNumber, ReportDataSet_StockNumber, parameterValues, NullCallback);

            foreach (DataRow row in table.Rows)
            {
                return Convert.ToString(row["VehicleHandle"], CultureInfo.InvariantCulture);
            }

            throw new DataException("Expected one row of handle information but got none");
        }
    }
}