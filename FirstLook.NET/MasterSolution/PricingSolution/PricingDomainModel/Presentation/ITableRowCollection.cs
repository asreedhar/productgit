using System.Collections.Generic;

namespace FirstLook.Pricing.DomainModel.Presentation
{
    public interface ITableRowCollection : IList<ITableRow>
    {
        ITableRow NewItem();

        int IndexOf(int id);

        ITableRow Find(int id);
    }
}