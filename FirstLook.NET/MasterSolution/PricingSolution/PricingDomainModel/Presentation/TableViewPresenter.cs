namespace FirstLook.Pricing.DomainModel.Presentation
{
    public class TableViewPresenter
    {
        private readonly ITableView _view;

        public TableViewPresenter(ITableView view)
        {
            _view = view;
        }

        public ITableView View
        {
            get { return _view; }
        }

        public bool Exists(int id)
        {
            return View.Items.IndexOf(id) != -1;
        }

        public bool MoveUp(int id)
        {
            int index = View.Items.IndexOf(id);

            if (index == -1 || index == 0)
            {
                return false;
            }

            ITableRow row = View.Items[index - 1];

            View.Items.RemoveAt(index - 1);

            View.Items.Insert(index, row);

            return true;
        }

        public bool MoveDown(int id)
        {
            int index = View.Items.IndexOf(id);

            if (index == -1 || index == View.Items.Count - 1)
            {
                return false;
            }

            ITableRow row = View.Items[index];

            View.Items.RemoveAt(index);

            View.Items.Insert(index + 1, row);

            return true;
        }

        public bool SetSelected(int id, bool value)
        {
            int index = View.Items.IndexOf(id);

            if (index == -1)
            {
                return false;
            }

            ITableRow item = View.Items[index];

            item.Selected = value;

            return item.HasSelectedChanged;
        }

        public bool SetRank(int id, int rank)
        {
            int index = View.Items.IndexOf(id);

            if (index == -1)
            {
                return false;
            }

            if (rank != index)
            {
                ITableRow tmp = View.Items[index];

                View.Items.RemoveAt(index);

                View.Items.Insert(rank, tmp);

                return true;
            }

            return false;
        }
    }
}