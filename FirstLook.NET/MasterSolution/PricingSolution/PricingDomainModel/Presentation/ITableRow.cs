namespace FirstLook.Pricing.DomainModel.Presentation
{
    public interface ITableRow
    {
        int Id { get; set; }
        string Name { get; set; }
        bool Enabled { get; set; }
        bool Selected { get; set; }
        bool HasSelectedChanged { get; }
    }
}