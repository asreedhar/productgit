namespace FirstLook.Pricing.DomainModel.Presentation
{
    public interface ITableView
    {
        ITableRowCollection Items { get; }
    }
}