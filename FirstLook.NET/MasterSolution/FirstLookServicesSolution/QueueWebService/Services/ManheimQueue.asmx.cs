﻿using System;
using System.Web.Services;
using System.Web.Services.Protocols;
using FirstLook.Common.Core.Registry;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model;

namespace FirstLook.QueueWebService.Services
{
    /// <summary>
    /// Summary description for ManheimQueue
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ManheimQueue : System.Web.Services.WebService
    {
        #region Properties
        /// <summary>
        /// Identity of the user calling the web service.
        /// </summary>
        public UserIdentity UserIdentity { get; set; }

        private IManheimQueue _manheimQueue;

        #endregion

        [WebMethod]
        [SoapHeader("UserIdentity", Direction = SoapHeaderDirection.In)]        
        public void PushPeriodicDataToQueue(int days)
        {
            try
            {
                Utility.SetIdentity(UserIdentity);
                _manheimQueue = GetResolver().Resolve<IManheimQueue>();
                _manheimQueue.PushPeriodicDataToQueue(days);
            }
            catch (Exception exception)
            {
                SoapException soapException = Utility.HandleException(exception, Context.Request);
                throw soapException;
            }  
        }

        [WebMethod]
        [SoapHeader("UserIdentity", Direction = SoapHeaderDirection.In)]        
        public void UpdateMMRAveragePriceFromQueue(int recordCount)
        {
            try
            {
                Utility.SetIdentity(UserIdentity);
                _manheimQueue = GetResolver().Resolve<IManheimQueue>();
                _manheimQueue.UpdateMMRAveragePriceFromQueue(recordCount);
            }
            catch (Exception exception)
            {
                SoapException soapException = Utility.HandleException(exception, Context.Request);
                throw soapException;
            }
            
        }

        [WebMethod]
        [SoapHeader("UserIdentity", Direction = SoapHeaderDirection.In)]
        public void PurgeSuccessQueue(int days)
        {
            try
            {
                Utility.SetIdentity(UserIdentity);
                _manheimQueue = GetResolver().Resolve<IManheimQueue>();
                _manheimQueue.PurgeSuccessQueue(days);
            }
            catch (Exception exception)
            {
                SoapException soapException = Utility.HandleException(exception, Context.Request);
                throw soapException;
            }
        }

        [WebMethod]
        [SoapHeader("UserIdentity", Direction = SoapHeaderDirection.In)]
        public void PurgeFailureQueue(int days)
        {
            try
            {
                Utility.SetIdentity(UserIdentity);
                _manheimQueue = GetResolver().Resolve<IManheimQueue>();
                _manheimQueue.PurgeFailureQueue(days);
            }
            catch (Exception exception)
            {
                SoapException soapException = Utility.HandleException(exception, Context.Request);
                throw soapException;
            }
        }

        [WebMethod]
        [SoapHeader("UserIdentity", Direction = SoapHeaderDirection.In)]
        public MMRResult UpdateAveragePrice(MMRInventoryRequest request)
        {
            try
            {
                Utility.SetIdentity(UserIdentity);
                _manheimQueue = GetResolver().Resolve<IManheimQueue>();
                return _manheimQueue.UpdateAveragePrice(request);
            }
            catch (Exception exception)
            {
                SoapException soapException = Utility.HandleException(exception, Context.Request);
                throw soapException;
            }
        }

        [WebMethod]
        [SoapHeader("UserIdentity", Direction = SoapHeaderDirection.In)]
        public void DecodeVIN(string vin)
        {
            try
            {
                Utility.SetIdentity(UserIdentity);
                _manheimQueue = GetResolver().Resolve<IManheimQueue>();
                _manheimQueue.DecodeVIN(vin);
            }
            catch (Exception exception)
            {
                SoapException soapException = Utility.HandleException(exception, Context.Request);
                throw soapException;
            }
        }

        protected static IResolver GetResolver()
        {
            IResolver resolver = RegistryFactory.GetResolver();

            return resolver;
        }
    }
}
