﻿using System;
using System.IO;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Serialization;
using FirstLook.Client.DomainModel.Clients.Model.Authorities.Dealers;
using FirstLook.Client.DomainModel.Clients.Repositories.Gateways;
using FirstLook.Common.Core.Registry;
using FirstLook.QueueProcessor.DomainModel.Exceptions;



namespace FirstLook.QueueWebService.Services
{
    /// <summary>
    /// Utility methods common to all distribution web services.
    /// </summary>
    internal static class Utility
    {
        #region Identity Helpers

        /// <summary>
        /// Set the identity of the calling user. Will check if the identity is valid.
        /// </summary>
        /// <exception cref="UserIdentityException">Thrown when the provided user identity is invalid.</exception>
        internal static void SetIdentity(UserIdentity userIdentity)
        {
            string userName = userIdentity == null ? string.Empty : userIdentity.UserName;

            MemberGateway memberGateway= new MemberGateway();
            Member member=memberGateway.Fetch(userName);

            if (member==null)
            {
                throw new UserIdentityException();
            }

            Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity(userName, "SOAP"), new string[0]);
        }

        #endregion

        #region Error Handlers

        /// <summary>
        /// Handle an application exception. Will log it and create a soap exception to be returned to the caller of 
        /// this web service.
        /// </summary>
        /// <param name="exception">Application exception.</param>        
        /// <param name="request">Http request object.</param>
        /// <returns>Soap exception.</returns>
        internal static SoapException HandleException(Exception exception, HttpRequest request)
        {
            return HandleException(exception, request, null);
        }

        /// <summary>
        /// Handle an application exception. Will log it and create a soap exception to be returned to the caller of 
        /// this web service. Also logs the message that caused the exception.
        /// </summary>
        /// <param name="exception">Application exception.</param>        
        /// <param name="request">Http request object.</param>
        /// <param name="message">Incoming message that caused exception. May be null.</param>
        /// <returns>Soap exception.</returns>
        internal static SoapException HandleException(Exception exception, HttpRequest request, Message message)
        {
            IExceptionLogger logger = RegistryFactory.GetResolver().Resolve<IExceptionLogger>();

            // Log the exception - including the incoming message if it's not null.
            if (message != null)
            {
                // Serialize the message into a string.            
                StringBuilder stringBuilder = new StringBuilder();
                StringWriter writer = new StringWriter(stringBuilder);
                XmlSerializer serializer = new XmlSerializer(typeof (Message));
                serializer.Serialize(writer, message);
                         
                logger.Record(exception, writer.ToString());
            }
            else
            {
                logger.Record(exception);
            }

            // Generate the xml exception details.
            XmlDocument document = new XmlDocument();
            XmlNode detail = document.CreateNode(XmlNodeType.Element,
                                                 SoapException.DetailElementName.Name,
                                                 SoapException.DetailElementName.Namespace);
            XmlQualifiedName faultCode;
            XmlNode serviceException;            
            
            // User input exceptions.
            if (exception as UserInputException != null)
            {
                serviceException = document.CreateNode(XmlNodeType.Element,
                                                       "UserInputException",
                                                       "http://services.firstlook.biz/QueueService/");
                serviceException.AppendChild(document.CreateTextNode("Error with user input. Please check message values."));
                detail.AppendChild(serviceException);

                faultCode = SoapException.ClientFaultCode;                                
            }            
            // All other exceptions are presumed to be server faults.
            else
            {
                serviceException = document.CreateNode(XmlNodeType.Element,
                                                       "ServiceException",
                                                       "http://services.firstlook.biz/QueueService/");
                serviceException.AppendChild(document.CreateTextNode("Unexpected error. Please contact FirstLook support."));
                detail.AppendChild(serviceException);

                faultCode = SoapException.ServerFaultCode;                
            }

            return new SoapException(exception.Message, faultCode, request.Url.AbsoluteUri, detail);
        }

        #endregion
    }
}
