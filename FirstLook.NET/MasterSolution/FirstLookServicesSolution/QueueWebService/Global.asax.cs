﻿using System;
using System.Web;
using System.Web.Caching;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.QueueProcessor.DomainModel.Exceptions;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue;

namespace QueueWebService
{
    public class Global : System.Web.HttpApplication
    {

        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup      
            IRegistry registry = RegistryFactory.GetRegistry();
            registry.Register<IExceptionLogger, ExceptionLogger>(ImplementationScope.Shared);
            registry.Register<ICache, WebCache>(ImplementationScope.Shared);
            registry = RegistryFactory.GetRegistry();
            registry.Register<FirstLook.QueueProcessor.DomainModel.Module>();
            registry.Register<FirstLook.QueueProcessor.DomainModel.BookQueue.Module>();
            registry.Register<FirstLook.QueueProcessor.DomainModel.ManheimQueue.Module>();
            registry.Register<FirstLook.Client.DomainModel.Module>();
            registry.Register<IDataSessionManager, DataSessionManager>(ImplementationScope.Shared);
            registry.Register<IManheimQueue, ProcessManheimQueue>(ImplementationScope.Shared);

        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

        }

        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started

        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }

        private class WebCache : ICache
        {
            private readonly Cache _cache;

            public WebCache()
            {
                _cache = HttpContext.Current.Cache;
            }

            public object Add(string key, object value, DateTime absoluteExpiration, TimeSpan slidingExpiration)
            {
                return _cache.Add(
                    key,
                    value,
                    null,
                    absoluteExpiration,
                    slidingExpiration,
                    CacheItemPriority.Normal,
                    null);
            }

            public object Get(string key)
            {
                return _cache.Get(key);
            }

            public object Remove(string key)
            {
                return _cache.Remove(key);
            }
        }
    }
}
