﻿using System;
using System.Web.Services.Protocols;

namespace FirstLook.QueueWebService.Services
{
    [Serializable]
    public class UserIdentity : SoapHeader
    {
        /// <summary>
        /// Username of the caller.
        /// </summary>
        public string UserName { get; set; }
    }
}