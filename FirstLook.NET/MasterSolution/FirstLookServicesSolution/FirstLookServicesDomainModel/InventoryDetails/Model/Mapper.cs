﻿
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.InventoryDetails.Model
{
    public static class Mapper
    {
        public static InventoryDataDto Map(InventoryInfo objs)
        {
            return new InventoryDataDto()
                   {
                       AcquisitionPrice = objs.AcquisitionPrice,
                       AdReviewStatus = objs.AdReviewStatus,
                       BaseColor = objs.BaseColor,
                       BusinessUnitId = objs.BusinessUnitId,
                       CertificationName = objs.BaseColor,
                       Certified = objs.Certified,
                       CertifiedId = objs.CertifiedId,
                       CertifiedProgramId = objs.CertifiedProgramId,
                       ChromeStyleId = objs.ChromeStyleId,
                       DatePosted = objs.DatePosted,
                       Description = objs.Description,
                       DesiredPhotoCount = objs.DesiredPhotoCount,
                       DueForReprice = objs.DueForReprice,
                       ExtColor1 = objs.ExtColor1,
                       ExtColor2 = objs.ExtColor2,
                       ExteriorColorCode = objs.ExteriorColorCode,
                       ExteriorColorCode2 = objs.ExteriorColorCode2,
                       ExteriorStatus = objs.ExteriorStatus,
                       HasCarfax = objs.HasCarfax,
                       HasCarfaxAdmin = objs.HasCarfaxAdmin,
                       HasCurrentBookValue = objs.HasCurrentBookValue,
                       HasKeyInformation = objs.HasKeyInformation,
                       InteriorColor = objs.InteriorColor,
                       InteriorColorCode = objs.InteriorColorCode,
                       InteriorStatus = objs.InteriorStatus,
                       InventoryId = objs.InventoryId,
                       InventoryReceivedDate = objs.InventoryReceivedDate,
                       InventoryStatusCD = objs.InventoryStatusCD,
                       InventoryType = objs.InventoryType,
                       LastUpdateListPrice = objs.LastUpdateListPrice,
                       ListPrice = objs.ListPrice,
                       LotPrice = objs.LotPrice,
                       LowActivityFlag = objs.LowActivityFlag,
                       Make = objs.Make,
                       MakeId = objs.MakeId,
                       Mileage = objs.Mileage,
                       Model = objs.Model,
                       ModelId = objs.ModelId,
                       Msrp = objs.Msrp,
                       OnlineFlag = objs.OnlineFlag,
                       PhotoStatus = objs.PhotoStatus,
                       PostingStatus = objs.PostingStatus,
                       PricingStatus = objs.PricingStatus,
                       SpecialPrice = objs.SpecialPrice,
                       StatusBucket = objs.StatusBucket,
                       StockNumber = objs.StockNumber,
                       TradePurchase = objs.TradePurchase,
                       Trim = objs.Trim,
                       UnitCost = objs.UnitCost,
                       VehicleLocation = objs.VehicleLocation,
                       Vin = objs.Vin,
                       Year = objs.Year,
                       CarsDotComMakeID=objs.CarsDotComMakeID,
                       CarsDotComModelId=objs.CarsDotComModelId,
                       AutoTraderMake=objs.AutoTraderMake,
                       AutoTraderModel=objs.AutoTraderModel
                   };
        }



    }
}
