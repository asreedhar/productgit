﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.InventoryDetails.Model
{
    public class TrimInfo
    {
        public int ChromeStyleId { get; set; }
        public string Trim { get; set; }
    }
}
