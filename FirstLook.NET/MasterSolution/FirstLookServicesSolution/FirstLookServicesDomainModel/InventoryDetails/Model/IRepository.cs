﻿using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Commands.TransferObjects.Envelopes;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.InventoryDetails.Model
{
    interface IRepository
    {
        InventoryInfo Invinfo(int inventoryid);

        InventoryInfo Invinfo(FetchInventoryArgumentsDto parameters);

        IList<TrimInfo> GetTrimList(FetchTrimListArgumentsDto args);
    }
}
