﻿
namespace FirstLook.FirstLookServices.DomainModel.InventoryDetails.Repositories
{
    public class InventoryInfoInput
    {
        public int InventoryId { get; set; }
        public int DealerId { get; set; }
    }
}
