﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Model;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Repositories.DataStores;

namespace FirstLook.FirstLookServices.DomainModel.InventoryDetails.Repositories.Gateways
{
    public class InventoryInfoGateway : GatewayBase
    {
        public InventoryInfo Get(InventoryInfoInput inventoryinfo)
        {
            string key = CreateCacheKey("" + inventoryinfo.InventoryId ,inventoryinfo.DealerId );
            //InventoryInfo invinfo = null;
            InventoryInfo invinfo = Cache.Get(key) as InventoryInfo;

            if (invinfo == null)
            {
                var serializer = Resolve<ISerializer<InventoryInfo>>();

                var datastore = Resolve<IInventoryInfoDataStore>();

                using (IDataReader tbl = datastore.InventoryInfo(inventoryinfo))
                {
                    IList<InventoryInfo> items = serializer.Deserialize(tbl);

                     invinfo = items.FirstOrDefault();

                    Remember(key, invinfo);
                }

            
        }
            return invinfo;
        }

    }
}
