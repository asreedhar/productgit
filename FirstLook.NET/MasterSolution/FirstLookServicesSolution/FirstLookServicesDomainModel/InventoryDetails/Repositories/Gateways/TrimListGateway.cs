﻿using System.Collections.Generic;
using FirstLook.Common.Core;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Repositories.DataStores;
using System.Data;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Repositories.Serializers;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Model;

namespace FirstLook.FirstLookServices.DomainModel.InventoryDetails.Repositories.Gateways
{
    public class TrimListGateway : GatewayBase
    {
        public IList<TrimInfo> GetTrims(string vin)
        {

            string key = CreateCacheKey("" + vin);

            IList<TrimInfo> trimList = Cache.Get(key) as List<TrimInfo>;

            if (trimList == null)
            {
                trimList = new List<TrimInfo>();
               
                var serializer = new TrimSerializer();

                var datastore = Resolve<IInventoryInfoDataStore>();
                 using (IDataReader tbl = datastore.ChromeStyleNamesSelect(vin))
                {
                    if (tbl != null)
                    {
                      trimList = serializer.Deserialize(tbl);
                      Remember(key, trimList);
                    }
                    
                 }

            }
            return trimList;
        }
    }
}
