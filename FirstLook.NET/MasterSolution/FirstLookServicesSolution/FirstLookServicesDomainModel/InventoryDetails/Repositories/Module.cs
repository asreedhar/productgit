﻿using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Model;

namespace FirstLook.FirstLookServices.DomainModel.InventoryDetails.Repositories
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<DataStores.Module>();

            registry.Register<Serializers.Module>();

            registry.Register<IRepository, InventoryRepository>(ImplementationScope.Shared);

        }
    }
}
