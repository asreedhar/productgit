﻿using FirstLook.Client.DomainModel.Common.Repositories;
using System.Data;
using FirstLook.Common.Core.Data;
using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Model;
using System;

namespace FirstLook.FirstLookServices.DomainModel.InventoryDetails.Repositories.Serializers
{
    public class TrimSerializer : Serializer<TrimInfo>
    {
        public override TrimInfo Deserialize(IDataRecord record)
        {

            int chromeStyleId = 0;
            string trim = string.Empty;
            string styleNameWoTrim = string.Empty;


            try
            {
                chromeStyleId = DataRecord.GetInt32(record, "styleID", 0);
                trim = DataRecord.GetString(record, "Trim");
                styleNameWoTrim = DataRecord.GetString(record, "styleNameWOTrim");
            }
            catch(Exception ex)
            {
                throw ex;
            }


            return new TrimInfo() { 
            ChromeStyleId=chromeStyleId,
            Trim=trim+" - "+styleNameWoTrim
            };
        }
    }
}
