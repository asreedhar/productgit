﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Model;

namespace FirstLook.FirstLookServices.DomainModel.InventoryDetails.Repositories.Serializers
{
    public class InventoryInfoSerializer : Serializer<InventoryInfo>
    {

        public override InventoryInfo Deserialize(IDataRecord record)
        {
           InventoryInfo obj = new InventoryInfo()
            {
                AcquisitionPrice = record.GetDecimal("AcquisitionPrice",0),
                AdReviewStatus = Convert.ToInt32(record.GetNullableInt32("adReviewStatus")),
                BaseColor = record.GetString("BaseColor"),
                BusinessUnitId = Convert.ToInt32(record.GetNullableInt32("BusinessUnitID")),
                CertificationName = record.GetString("Name"),
                Certified = record.GetTinyInt("Certified",0),
                CertifiedId = record.GetString("CertifiedId"),
                CertifiedProgramId = Convert.ToInt32( record.GetNullableInt32("CertifiedProgramId")),
                ChromeStyleId = Convert.ToInt32(record.GetNullableInt32("ChromeStyleId")),
                DatePosted = record.GetDateTime("DatePosted"),
                Description = record.GetString("DescriptionSample"),
                DesiredPhotoCount = Convert.ToInt32(record.GetNullableInt32("DesiredPhotoCount")),
                DueForReprice = record.GetTinyInt("DueForRepricing",0),
                ExtColor1 = record.GetString("ExtColor1"),
                ExtColor2 = record.GetString("ExtColor2"),
                ExteriorColorCode = record.GetString("ExteriorColorCode"),
                ExteriorColorCode2 = record.GetString("ExteriorColorCode2"),
                ExteriorStatus = Convert.ToInt32(record.GetNullableInt32("exteriorStatus")),
                HasCarfax = record.GetTinyInt("HasCarfax", 0),
                HasCarfaxAdmin = record.GetTinyInt("HasCarfaxAsAdmin", 0),
                HasCurrentBookValue = record.GetTinyInt("HasCurrentBookValue", 0),
                HasKeyInformation = record.GetTinyInt("HasKeyInformation", 0),
                InteriorColor = record.GetString("IntColor"),
                InteriorColorCode = record.GetString("InteriorColorCode"),
                InteriorStatus = Convert.ToInt32(record.GetTinyInt("interiorStatus", 0)),
                InventoryId = Convert.ToInt32(record.GetNullableInt32("InventoryID")),
                InventoryReceivedDate = record.GetDateTime("InventoryReceivedDate"),
                InventoryStatusCD = record.GetTinyInt("InventoryStatusCD", 0),
                InventoryType = record.GetTinyInt("InventoryType", 0),
                LastUpdateListPrice = record.GetDecimal("LastUpdateListPrice",0),
                ListPrice = record.GetDecimal("ListPrice", 0),
                LotPrice = record.GetDecimal("LotPrice", 0),
                LowActivityFlag = record.GetTinyInt("lowActivityFlag", 0),
                Make = record.GetString("Make"),
                MakeId = Convert.ToInt32(record.GetNullableInt32("MakeID")),
                Mileage = record.GetNullableInt32("MileageReceived"),
                Model = record.GetString("Model"),
                ModelId = Convert.ToInt32(record.GetNullableInt32("ModelId")),
                Msrp = record.GetDecimal("MSRP", 0),
                OnlineFlag = record.GetTinyInt("onlineFlag", 0),
                PhotoStatus = record.GetTinyInt("photoStatus", 0),
                PostingStatus = record.GetTinyInt("postingStatus", 0),
                PricingStatus = record.GetTinyInt("pricingStatus", 0),
                SpecialPrice = record.GetDecimal("SpecialPrice",0),
                StatusBucket = Convert.ToInt32(record.GetNullableInt32("StatusBucket")),
                StockNumber = record.GetString("StockNumber"),
                TradePurchase = record.GetTinyInt("TradeOrPurchase", 0),
                Trim = record.GetString("Trim"),
                UnitCost = record.GetDecimal("UnitCost", 0),
                VehicleLocation = record.GetString("VehicleLocation"),
                Vin = record.GetString("Vin"),
                Year = record.GetInt32("VehicleYear", 0),
                CarsDotComMakeID = record.GetNullableInt32("CarsDotComMakeID"),
                CarsDotComModelId = record.GetNullableInt32("CarsDotComModelId"),
                AutoTraderMake=record.GetString("AutoTraderMake"),
                AutoTraderModel=record.GetString("AutoTraderModel")

            };
           return obj;
        }


    }
    
}
