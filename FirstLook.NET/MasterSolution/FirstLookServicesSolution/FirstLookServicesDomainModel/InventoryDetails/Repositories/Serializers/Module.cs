﻿using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Model;

namespace FirstLook.FirstLookServices.DomainModel.InventoryDetails.Repositories.Serializers
{
    public class Module: IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ISerializer<InventoryInfo>, InventoryInfoSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<TrimInfo>, TrimSerializer>(ImplementationScope.Shared);
        }
    }
}
