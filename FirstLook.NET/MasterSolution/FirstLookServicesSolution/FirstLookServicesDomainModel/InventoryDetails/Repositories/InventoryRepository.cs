﻿using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Model;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Repositories.Gateways;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Commands.TransferObjects.Envelopes;
using System.Collections.Generic;


namespace FirstLook.FirstLookServices.DomainModel.InventoryDetails.Repositories
{
    public class InventoryRepository : RepositoryBase, IRepository
    {
        #region IRepository Members

        public InventoryInfo Invinfo(int inventoryid)
        {
            InventoryInfoInput invinfoinput = new InventoryInfoInput();
            invinfoinput.InventoryId = inventoryid;
            return new InventoryInfoGateway().Get(invinfoinput);
        }

        #endregion

        protected override string DatabaseName
        {
            get { return "IMT"; }
        }

        protected override void OnBeginTransaction(IDataSession session)
        {
            // do nothing -- we do not audited creation of reference/client items
        }



        public InventoryInfo Invinfo(FetchInventoryArgumentsDto parameters)
        {
            InventoryInfoInput invinfoinput = new InventoryInfoInput();
            invinfoinput.InventoryId = parameters.InventoryId;
            invinfoinput.DealerId = parameters.DealerId;
            return new InventoryInfoGateway().Get(invinfoinput);
        }

        #region IRepository Members


        public IList<TrimInfo> GetTrimList(FetchTrimListArgumentsDto args)
        {
            return new TrimListGateway().GetTrims(args.Vin);
        }

        #endregion
    }
}
