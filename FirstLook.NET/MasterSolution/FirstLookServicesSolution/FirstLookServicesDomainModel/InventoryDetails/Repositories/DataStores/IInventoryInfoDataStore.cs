﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


namespace FirstLook.FirstLookServices.DomainModel.InventoryDetails.Repositories.DataStores
{
    interface IInventoryInfoDataStore
    {
        IDataReader InventoryInfo(InventoryInfoInput InventoryInfo);

        IDataReader ChromeStyleNamesSelect(string vin);
    }
}
