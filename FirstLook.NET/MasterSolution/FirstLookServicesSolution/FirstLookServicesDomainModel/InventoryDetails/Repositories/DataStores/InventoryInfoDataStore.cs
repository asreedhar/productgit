﻿using System.Data;
using FirstLook.Common.Core.Data;
using System.Data.SqlClient;
using FirstLook.Merchandising.DomainModel.Vehicles;
using System.Collections.Generic;


namespace FirstLook.FirstLookServices.DomainModel.InventoryDetails.Repositories.DataStores
{
    public class InventoryInfoDataStore : IInventoryInfoDataStore
    {

        #region Class Members

        private string DatabaseName = "Merchandising";
        //private string VehicleCatalogDatabaseName = "VehicleCatalog";
        private string ChromeDatabaseName="Merchandising";

        #endregion

        #region ISalesPeopleDataStore Members


        public IDataReader InventoryInfo(InventoryInfoInput inventoryinfo)
        {
            try
            {
                using (IDbConnection connection = Database.Connection(DatabaseName))
                {
                    using (IDbCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.CommandText = "builder.InventoryInfoFetch";

                        Database.AddWithValue(command, "DealerId", inventoryinfo.DealerId, DbType.Int32);
                        Database.AddWithValue(command, "InventoryId", inventoryinfo.InventoryId, DbType.Int32);

                        var set = new DataSet();

                        IDataReader reader = command.ExecuteReader();

                        using (reader)
                        {
                            var table = new DataTable("InventoryDetails");

                            table.Load(reader);
                            set.Tables.Add(table);
                        }

                        return set.CreateDataReader();
                    }

                }
            }
            catch (SqlException se)
            {
                System.Console.WriteLine(se);
                throw se;
            }

        }

        #region IInventoryInfoDataStore Members


        public IDataReader ChromeStyleNamesSelect(string vin)
        {

            
            int vinPatternID = ChromeStyle.GetVinPatternIdForVin(vin);
            if (vinPatternID < 0)
            {
                return null;
            }

            using (IDbConnection con = Database.Connection(ChromeDatabaseName))
            {
                

                using (IDbCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Chrome.GetChromeStyles";
                    cmd.CommandType = CommandType.StoredProcedure;

                    Database.AddWithValue(cmd, "VinPatternID", vinPatternID, DbType.Int32);

                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        var set = new DataSet();
                        using (reader)
                        {
                            var table = new DataTable("TrimList");

                            table.Load(reader);
                            set.Tables.Add(table);
                        }

                        return set.CreateDataReader();

                    }
                }
                

                
            }

        }

        #endregion
    }


        #endregion
}














