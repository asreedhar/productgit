﻿using System;
using FirstLook.Common.Core.Registry;

namespace FirstLook.FirstLookServices.DomainModel.InventoryDetails.Repositories.DataStores
{
    [Serializable]
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IInventoryInfoDataStore, InventoryInfoDataStore>(ImplementationScope.Shared);
        }

    }
}
