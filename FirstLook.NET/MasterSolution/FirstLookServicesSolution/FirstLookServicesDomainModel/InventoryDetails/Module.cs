﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.FirstLookServices.DomainModel.InventoryDetails
{
    public class Module : IModule
    {
        #region IModule Members

        public void Configure(IRegistry registry)
        {
            registry.Register<Commands.Module>();

            registry.Register<Repositories.Module>();
        }

        #endregion

    }
}
