﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Commands.TransferObjects.Envelopes;


namespace FirstLook.FirstLookServices.DomainModel.InventoryDetails.Commands
{
    public interface ICommandFactory
    {
        ICommand<FetchInventoryDetailsResultsDto, IdentityContextDto<FetchInventoryArgumentsDto>> CreateFetchInventoryDetailsCommand();

        ICommand<FetchTrimListResultsDto, IdentityContextDto<FetchTrimListArgumentsDto>> CreateTrimListCommand();
    }
}
