﻿
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;


namespace FirstLook.FirstLookServices.DomainModel.InventoryDetails.Commands.TransferObjects.Envelopes
{
    public class FetchInventoryDetailsResultsDto
    {
        public PrincipalDto Principal { get; set; }

        public FetchInventoryDetailsResultsDto Arguments { get; set; }

        public InventoryDataDto InventoryData { get; set; }

    }
}
