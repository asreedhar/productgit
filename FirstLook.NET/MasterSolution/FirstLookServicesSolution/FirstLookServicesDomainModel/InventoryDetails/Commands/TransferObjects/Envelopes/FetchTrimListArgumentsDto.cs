﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.InventoryDetails.Commands.TransferObjects.Envelopes
{
    public class FetchTrimListArgumentsDto
    {
        public string Vin { get; set; }
    }
}
