﻿
namespace FirstLook.FirstLookServices.DomainModel.InventoryDetails.Commands.TransferObjects.Envelopes
{
     public class FetchInventoryDetailsDto
    {
        public int InventoryId { get; set; }
        public string Vin { get; set; }
        public int Year { get; set; }
        public string MakeModel { get; set; }
        public string Trim { get; set; }
        public int AgeInDays { get; set; }
        public string Color { get; set; }
        public int Mileage { get; set; }
        public decimal ListPrice { get; set; }
        public string RiskLight { get; set; }
        public decimal UnitCost { get; set; }
        public string StockNumber { get; set; }
        public int Certified { get; set; }
        public string CertificationName { get; set; }
        public int MakeId { get; set; }
        public int ModelId { get; set; }

        
    }
}
