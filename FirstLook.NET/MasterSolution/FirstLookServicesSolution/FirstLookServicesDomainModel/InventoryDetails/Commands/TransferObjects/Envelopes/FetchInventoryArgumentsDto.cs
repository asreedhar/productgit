﻿
namespace FirstLook.FirstLookServices.DomainModel.InventoryDetails.Commands.TransferObjects.Envelopes
{
    public class FetchInventoryArgumentsDto
    {
        public int InventoryId{get;set;}
        public int DealerId { get; set; }
    }
}
