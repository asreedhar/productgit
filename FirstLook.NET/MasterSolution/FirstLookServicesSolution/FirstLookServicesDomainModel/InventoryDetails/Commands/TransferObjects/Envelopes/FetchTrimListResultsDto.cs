﻿using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Model;

namespace FirstLook.FirstLookServices.DomainModel.InventoryDetails.Commands.TransferObjects.Envelopes
{
    public class FetchTrimListResultsDto
    {
        public IList<TrimInfo> TrimList { get; set; }
    }
}
