﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.InventoryDetails.Commands.Impl
{
    public class CommandFactory : ICommandFactory
    {
        #region ICommandFactory Members

        public ICommand<FetchInventoryDetailsResultsDto, IdentityContextDto<FetchInventoryArgumentsDto>> CreateFetchInventoryDetailsCommand()
         {
             return new FetchInventoryCommand();
         }
        #endregion




        #region ICommandFactory Members


        public ICommand<FetchTrimListResultsDto, IdentityContextDto<FetchTrimListArgumentsDto>> CreateTrimListCommand()
        {
            return new FetchTrimListCommand();
        }

        #endregion
    }
}
