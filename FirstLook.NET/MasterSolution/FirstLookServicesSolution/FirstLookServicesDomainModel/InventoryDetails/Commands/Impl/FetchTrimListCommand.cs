﻿using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Model;
using FirstLook.Common.Core.Registry;

namespace FirstLook.FirstLookServices.DomainModel.InventoryDetails.Commands.Impl
{
    public class FetchTrimListCommand:ICommand<FetchTrimListResultsDto, IdentityContextDto<FetchTrimListArgumentsDto>>
    {            
        public FetchTrimListResultsDto Execute(IdentityContextDto<FetchTrimListArgumentsDto> parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();
            var inventoryRepository = resolver.Resolve<IRepository>();
            FetchTrimListArgumentsDto args= new FetchTrimListArgumentsDto()
            {
                Vin=parameters.Arguments.Vin
            };
           
            IList<TrimInfo> trims=inventoryRepository.GetTrimList(args);

            return new FetchTrimListResultsDto()
            {
                TrimList=trims
            };
        }
    }
}
