﻿using System;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Model;
using MAX.Market;

namespace FirstLook.FirstLookServices.DomainModel.InventoryDetails.Commands.Impl
{
    public class FetchInventoryCommand : ICommand<FetchInventoryDetailsResultsDto, IdentityContextDto<FetchInventoryArgumentsDto>>
    {
        #region ICommand<FetchInventoryDetailsResultsDto,IdentityContextDto<FetchStorePerformanceArgumentsDto>> Members

        public FetchInventoryDetailsResultsDto Execute(IdentityContextDto<FetchInventoryArgumentsDto> parameters)
        {

            IResolver resolver = RegistryFactory.GetResolver();
            var inventoryRepository = resolver.Resolve<IRepository>();
            FetchInventoryArgumentsDto fetch = new FetchInventoryArgumentsDto()
            {
                InventoryId = parameters.Arguments.InventoryId,
                DealerId = parameters.Arguments.DealerId

            };

            InventoryInfo detail = inventoryRepository.Invinfo(fetch);

            InventoryDataDto invdatadto = Mapper.Map(detail);

            return new FetchInventoryDetailsResultsDto()
            {
                InventoryData = invdatadto
            };

        }
        #endregion
    }
    }
    