﻿using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.TransferObjects
{
    public class InventoryTable
    {
       public List<InventoryDto> Inventory { get; set; } 
    }
}
