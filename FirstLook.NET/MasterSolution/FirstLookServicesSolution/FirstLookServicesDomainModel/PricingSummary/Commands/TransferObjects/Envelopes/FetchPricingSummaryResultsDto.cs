﻿using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.TransferObjects.Envelopes
{
    public class FetchPricingSummaryResultsDto
    {
        public PrincipalDto Principal { get; set; }

        public FetchPricingSummaryArgumentsDto Arguments { get; set; }

        public string OwnerName { get; set; }

        public Graph PricingGraph { get; set; }

        public GraphSummary PringGraphSummary { get; set; }

        public InventoryTable PricingInventoryTable { get; set; }

        public string BucketName { get; set; }
    }
}
