﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Model;

namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.TransferObjects.Envelopes
{
    public class FetchDealerBucketsResultsDto
    {
        public IList<DealerBucketDetail> Buckets { get; set; }
    }
}
