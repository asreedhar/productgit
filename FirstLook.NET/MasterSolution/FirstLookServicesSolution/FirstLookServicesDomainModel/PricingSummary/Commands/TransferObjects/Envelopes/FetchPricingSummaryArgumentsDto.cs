﻿
namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.TransferObjects.Envelopes
{
    public class FetchPricingSummaryArgumentsDto
    {
        public int DealerId { get; set; }

        public string Mode { get; set; }

        public string SalesStrategy { get; set; }

        public int ColumnIndex { get; set; }        

        public string SortColumns { get; set; }

        public int MaximumRows { get; set; }

        public int StartPageIndex { get; set; }

        public string InventoryFilter { get; set; }
        
    }
}
