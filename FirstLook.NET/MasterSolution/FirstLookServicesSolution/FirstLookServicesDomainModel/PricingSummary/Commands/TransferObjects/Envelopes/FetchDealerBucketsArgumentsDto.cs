﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.TransferObjects.Envelopes
{
    public class FetchDealerBucketsArgumentsDto
    {
        public int DealerId { get; set; }

        public string Mode { get; set; }
    }
}
