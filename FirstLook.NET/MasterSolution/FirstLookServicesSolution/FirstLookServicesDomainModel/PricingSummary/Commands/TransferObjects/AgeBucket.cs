﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.TransferObjects
{
    public class AgeBucket
    {
        public int AgeBucketId { get; set; }
        public int Low { get; set; }
        public int? High { get; set; }
    }
}
