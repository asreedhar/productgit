﻿
namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.TransferObjects
{
    public class BucketDto
    {
        public int BucketId { get; set; }

        public string BucketName { get; set; }

        public int Bucketunits { get; set; }

        public decimal BucketPercentage { get; set; }
    }
}
