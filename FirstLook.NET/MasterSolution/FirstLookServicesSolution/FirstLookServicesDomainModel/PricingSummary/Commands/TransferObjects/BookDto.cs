﻿
namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.TransferObjects
{
    public class BookDto
    {
        public string BookName { get; set; }

        public int BookValue { get; set; }
    }
}
