﻿
namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.TransferObjects
{
    public class GraphSummary
    {
        public int UnitsInStock { get; set; }

        public int AvgInternetPrice { get; set; }
        
        public int PctMarketAverage { get; set; }

        public int MarketDaysSupply { get; set; }

        public int DealerFavourableThreshold { get; set; }
    }
}
