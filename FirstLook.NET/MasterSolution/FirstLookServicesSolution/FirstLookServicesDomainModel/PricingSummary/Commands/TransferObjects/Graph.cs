﻿using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.TransferObjects
{
    public class Graph
    {
        public List<BucketDto> Buckets { get; set; }
    }
}
