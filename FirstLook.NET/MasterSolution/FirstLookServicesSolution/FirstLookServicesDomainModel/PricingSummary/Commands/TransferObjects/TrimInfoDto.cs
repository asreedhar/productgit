﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.TransferObjects
{
    [DataContract]
    public class TrimInfoDto
    {
        [DataMember(Name = "chromeStyleId")]
        public int ChromeStyleId { get; set; }

        [DataMember(Name = "trim")]
        public string Trim { get; set; }
    }
}
