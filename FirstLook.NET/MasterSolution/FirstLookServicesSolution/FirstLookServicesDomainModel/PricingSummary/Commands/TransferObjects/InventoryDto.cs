﻿
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.TransferObjects
{
    public class InventoryDto
    {
        public int Age { get; set; }

        public string VehicleDescription { get; set; }

        public string Make { get; set; }

        public string VehicleColor { get; set; }

        public int VehicleMileage { get; set; }

        public int UnitCost { get; set; }

        public int InternetPrice { get; set; }

        public string StockNumber { get; set; }

        public List<TrimInfoDto> TrimList { get; set; }

        public int InventoryId { get; set; }

        public decimal? PctMarketAverage { get; set; }

        public int MarketDaysSupply { get; set; }

        public List<BookDto> PriceComparisons { get; set; }

        public int ComparisonColor { get; set; }

        public int AgeBucket { get; set; }
    }
}
