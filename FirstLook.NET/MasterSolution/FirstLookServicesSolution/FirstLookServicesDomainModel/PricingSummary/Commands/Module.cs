﻿using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.Impl;

namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ICommandFactory, CommandFactory>(ImplementationScope.Shared);
        }
    }
}
