﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Model;
using FirstLook.FirstLookServices.DomainModel.DealerInfo.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.DealerInfo.Model;
using FirstLook.FirstLookServices.DomainModel.PriceComparisons.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.PriceComparisons.Model;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands
{
    public interface ICommandFactory
    {
        ICommand<FetchPricingSummaryResultsDto, IdentityContextDto<FetchPricingSummaryArgumentsDto>> CreateFetchPricingSummaryCommand();

        ICommand<List<ListingsAndCounts>, IdentityContextDto<List<FetchMarketListingsArgumentsDto>>> CreateFetchMarketListingsCommand();

        ICommand<PriceComparisonsResultsDto, IdentityContextDto<PriceComparisonsArgumentsDto>> CreateFetchPriceComparisonsCommand();

        ICommand<FetchDealerBucketsResultsDto, IdentityContextDto<FetchDealerBucketsArgumentsDto>> CreateFetchDealerBucketsCommand();

        ICommand<DealerInfoResultsDto, IdentityContextDto<DealerInfoArgumentsDto>> CreateDealerInfoCommand();

        ICommand<FetchTrimListResultsDto, IdentityContextDto<FetchTrimListArgumentsDto>> CreateTrimListCommand();

    }
}
