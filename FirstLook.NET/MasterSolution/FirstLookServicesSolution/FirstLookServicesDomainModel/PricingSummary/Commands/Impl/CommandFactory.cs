﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Commands.Impl;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Model;
using FirstLook.FirstLookServices.DomainModel.PriceComparisons.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.PriceComparisons.Model;
using FirstLook.FirstLookServices.DomainModel.PriceComparisons.Commands.Impl;
using FirstLook.FirstLookServices.DomainModel.DealerInfo.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.DealerInfo.Model;
using FirstLook.FirstLookServices.DomainModel.DealerInfo.Commands.Impl;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Model;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Commands.Impl;


namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.Impl
{
    public class CommandFactory : ICommandFactory
    {
        #region IcommandFactory Members

        public ICommand<FetchPricingSummaryResultsDto, IdentityContextDto<FetchPricingSummaryArgumentsDto>> CreateFetchPricingSummaryCommand()
        {
            return new FetchPricingSummaryCommand();
        }

        public ICommand<List<ListingsAndCounts>, IdentityContextDto<List<FetchMarketListingsArgumentsDto>>> CreateFetchMarketListingsCommand()
        {
            return new FetchMarketListingsCommand();
        }

        public ICommand<PriceComparisonsResultsDto, IdentityContextDto<PriceComparisonsArgumentsDto>> CreateFetchPriceComparisonsCommand()
        {
            return new FetchPriceComparisonsCommand();
        }

        public ICommand<FetchDealerBucketsResultsDto, IdentityContextDto<FetchDealerBucketsArgumentsDto>> CreateFetchDealerBucketsCommand()
        {
            return new FetchDealerBucketsCommand();
        }

        public ICommand<DealerInfoResultsDto, IdentityContextDto<DealerInfoArgumentsDto>> CreateDealerInfoCommand()
        {
            return new FetchDealerInfoCommand();
        }

        public ICommand<FetchTrimListResultsDto, IdentityContextDto<FetchTrimListArgumentsDto>> CreateTrimListCommand()
        {
            return new FetchTrimListCommand();
        }

        #endregion





      
    }
}
