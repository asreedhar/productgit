﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.Impl;
using FirstLook.FirstLookServices.DomainModel.Caching;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Model;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Model;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.Validation;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.FirstLookServices.DomainModel.PriceComparisons.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.DealerInfo.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Commands.TransferObjects.Envelopes;
using FirstLook.Merchandising.DomainModel.Vehicles;


namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.Impl
{
    public class FetchPricingSummaryCommand : ICommand<FetchPricingSummaryResultsDto, IdentityContextDto<FetchPricingSummaryArgumentsDto>>
    {
        public FetchPricingSummaryResultsDto Execute(IdentityContextDto<FetchPricingSummaryArgumentsDto> parameters)
        {

            #region set arguments
            //set default page as 1
            if (parameters.Arguments.StartPageIndex == 0)
            {
                parameters.Arguments.StartPageIndex = 1;
            }
            //set default no of rows as 25
            if (parameters.Arguments.MaximumRows == 0)
            {
                parameters.Arguments.MaximumRows = 25;
            }
            //set default to Age
            if (parameters.Arguments.SortColumns == null)
            {
                parameters.Arguments.SortColumns = "Age";
            }

            #endregion


            #region Call to Load#Inventory
            IResolver resolver = RegistryFactory.GetResolver();
            var loadinventoryRepository = resolver.Resolve<IRepository>();

            var inventoryInput = new LoadInventoryInput
            {
                DealerId = parameters.Arguments.DealerId,
                SaleStrategy = parameters.Arguments.SalesStrategy,
                InventoryFilter = parameters.Arguments.InventoryFilter
            };

            IList<LoadInventoryDetail> inventoryList = loadinventoryRepository.Loadinventory(inventoryInput).OrderBy(il => parameters.Arguments.SortColumns).ToList(); // Load#Inventory Table
            #endregion

            #region BucketName

            string bucketDecription;
            if (parameters.Arguments.Mode != "N")
            {
                var bucketNameInput = new BucketNameInput
                {
                    DealerId = parameters.Arguments.DealerId,
                    Mode = parameters.Arguments.Mode,
                    ColumnIndex = parameters.Arguments.ColumnIndex
                };

                IList<DealerBucketNameDetail> bucketName = loadinventoryRepository.FetchBucketName(bucketNameInput);
                bucketDecription = bucketName.Select(il => il.BucketName).First();
            }
            else
            {
                bucketDecription = "All Inventory";
            }


            #endregion

            #region Call to AgeBuckets

            IList<AgeBucket> list = loadinventoryRepository.FetchAgeBucket(parameters.Arguments.DealerId);

            foreach (var inv in inventoryList)
            {
                var bucket = list.ToList().Where(li => li.Low <= inv.Age && (inv.Age <= li.High || li.High == null));
                if (bucket != null)
                    inv.AgeBucket = bucket.FirstOrDefault().AgeBucketId;
            }

            #endregion

            #region Call to search criteria API

            #endregion


            #region Call to Max API MarketListing for market average

            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            var marketListingcommand = factory.CreateFetchMarketListingsCommand();

            var aggregaions = new List<string> { "price_stats" };
            var identityContextDto = new IdentityContextDto<List<FetchMarketListingsArgumentsDto>>();
            List<FetchMarketListingsArgumentsDto> arguments = inventoryList.Select(loadInventoryDetail => new FetchMarketListingsArgumentsDto
            {
                InventoryId = loadInventoryDetail.InventoryId,
                Make = loadInventoryDetail.Make,
                Year = loadInventoryDetail.VehicleYear,
                Aggregations = aggregaions,
                Size = 0,
                IsRequiredAggregations = false
            }).ToList();

            identityContextDto.Arguments = arguments;

            List<ListingsAndCounts> results = marketListingcommand.Execute(identityContextDto);

            #endregion


            #region Map % of mktavg , mktavg in price comparison and MDS

            for (int i = 0; i < inventoryList.Count(); i++)
            {
                for (int j = 0; j < results.Count(); j++)
                {
                    if (inventoryList[i].InventoryId == results[j].InventoryId)
                    {
                        if ((results[j].PriceStats.Avg == 0) || (results[j].PriceStats.Avg == null) || (inventoryList[i].InternetPrice == 0))
                        {
                            inventoryList[i].PctMarketAverage = 0;
                        }
                        else
                        {
                            inventoryList[i].PctMarketAverage = ((decimal)inventoryList[i].InternetPrice / results[j].PriceStats.Avg) * 100;
                        }
                        inventoryList[i].MarketAvg = Convert.ToInt32(results[j].PriceStats.Avg);
                        break;
                    }
                }
            }

            #endregion


            #region Call to Dealer setting FL Service

            var dealerInfoCommand = factory.CreateDealerInfoCommand();
            var identityContextDtoDi = new IdentityContextDto<DealerInfoArgumentsDto>();

            DealerInfoArgumentsDto argumentsDi = new DealerInfoArgumentsDto()
            {
                BusinessUnitId = parameters.Arguments.DealerId
            };

            identityContextDtoDi.Arguments = argumentsDi;

            DealerInfoResultsDto resultsDi = dealerInfoCommand.Execute(identityContextDtoDi);
            //Dealer Settings
            int pingIiRedRange1Value1 = resultsDi.DealerInfo.PingIIRedRange1Value1;
            int pingIiRedRange1Value2 = resultsDi.DealerInfo.PingIIRedRange1Value2;
            int pingIiYellowRange1Value1 = resultsDi.DealerInfo.PingIIYellowRange1Value1;
            int pingIiYellowRange1Value2 = resultsDi.DealerInfo.PingIIYellowRange1Value2;
            int pingIiYellowRange2Value1 = resultsDi.DealerInfo.PingIIYellowRange2Value1;
            int pingIiYellowRange2Value2 = resultsDi.DealerInfo.PingIIYellowRange2Value2;
            int pingIiGreenRange1Value1 = resultsDi.DealerInfo.PingIIGreenRange1Value1;
            int pingIiGreenRange1Value2 = resultsDi.DealerInfo.PingIIGreenRange1Value2;
            int favourableThreshold = resultsDi.DealerInfo.FavourableThreshold;
            int originalMsrp = resultsDi.DealerInfo.OriginalMSRP;
            int marketAvgInternetPrice = resultsDi.DealerInfo.MarketAvgInternetPrice;
            int edmundsTrueMarketValue = resultsDi.DealerInfo.EdmundsTrueMarketValue;
            int nadaRetailValue = resultsDi.DealerInfo.NADARetailValue;
            int kbbRetailValue = resultsDi.DealerInfo.KBBRetailValue;
            string ownerName = resultsDi.DealerInfo.OwnerName;

            #endregion


            #region Map risk index based on % of market avg and dealer specific settings.

            foreach (var loadInventoryDetail in inventoryList)
            {
                if ((loadInventoryDetail.InternetPrice == 0) || (loadInventoryDetail.PctMarketAverage == 0))
                {
                    loadInventoryDetail.RiskIndex = 1;
                    loadInventoryDetail.RiskDescription = "No Price";
                    continue;
                }
                else if (loadInventoryDetail.PctMarketAverage < pingIiGreenRange1Value1)
                {
                    loadInventoryDetail.RiskIndex = 4;
                    loadInventoryDetail.RiskDescription = "UnderPriced Risk";
                    continue;
                }
                else if (loadInventoryDetail.PctMarketAverage > pingIiGreenRange1Value2)
                {
                    loadInventoryDetail.RiskIndex = 2;
                    loadInventoryDetail.RiskDescription = "OverPriced Risk";
                    continue;
                }
                else if (loadInventoryDetail.PctMarketAverage >= pingIiGreenRange1Value1 && loadInventoryDetail.PctMarketAverage <= pingIiGreenRange1Value2)     //--- red
                {
                    loadInventoryDetail.RiskIndex = 3;
                    loadInventoryDetail.RiskDescription = "Priced at Market Risk ";
                }
            }
            //To test
            //var invalid = inventoryList.Where(il => il.RiskIndex == 0).ToList();
            //var noprice = inventoryList.Where(il => il.RiskIndex == 1).ToList();
            //var overprice = inventoryList.Where(il => il.RiskIndex == 2).ToList();
            //var pricedatmarket = inventoryList.Where(il => il.RiskIndex == 3).ToList();
            //var underprice = inventoryList.Where(il => il.RiskIndex == 4).ToList();
            //var vehiclesWitoutPriceComaprison = inventoryList.Where(il => il.RiskIndex == 5).ToList();

            //var dueforplanning = inventoryList.Where(il => il.AgeIndex == 0).ToList();
            //var first = inventoryList.Where(il => il.AgeIndex == 1).ToList();
            //var second = inventoryList.Where(il => il.AgeIndex == 2).ToList();
            //var third = inventoryList.Where(il => il.AgeIndex == 3).ToList();
            //var fourth = inventoryList.Where(il => il.AgeIndex == 4).ToList();
            //var fifth = inventoryList.Where(il => il.AgeIndex == 5).ToList();


            #endregion


            #region Call to Price Comparisons FL Service

            PriceComparisonsArgumentsDto arg = new PriceComparisonsArgumentsDto();

            List<int> inventoryIds = new List<int>();
            foreach (var inventory in inventoryList)
            {
                int inv = inventory.InventoryId;
                inventoryIds.Add(inv);
            }

            arg.DealerId = parameters.Arguments.DealerId;
            arg.InventoryIds = inventoryIds;

            var identityContextDtoPc = new IdentityContextDto<PriceComparisonsArgumentsDto>();
            identityContextDtoPc.Arguments = arg;

            var priceComparisonCommand = factory.CreateFetchPriceComparisonsCommand();
            PriceComparisonsResultsDto resultsPc = priceComparisonCommand.Execute(identityContextDtoPc);

            for (int i = 0; i < inventoryList.Count(); i++)
            {
                for (int j = 0; j < resultsPc.PriceComparisons.Count(); j++)
                {
                    if (inventoryList[i].InventoryId == resultsPc.PriceComparisons[j].InventoryId)
                    {
                        for (int k = 0; k < resultsPc.PriceComparisons[j].PriceComparisonsDetails.Count(); k++)
                        {
                            inventoryList[i].ComparisonColor = resultsPc.PriceComparisons[j].PriceComparisonsDetails[k].ComparisonColor;

                            if (resultsPc.PriceComparisons[j].PriceComparisonsDetails[k].BookName == "KBB")
                            {
                                inventoryList[i].Kbb = resultsPc.PriceComparisons[j].PriceComparisonsDetails[k].BookValue;
                            }
                            if (resultsPc.PriceComparisons[j].PriceComparisonsDetails[k].BookName == "Nada")
                            {
                                inventoryList[i].Nada = resultsPc.PriceComparisons[j].PriceComparisonsDetails[k].BookValue;
                            }
                            if (resultsPc.PriceComparisons[j].PriceComparisonsDetails[k].BookName == "MSRP")
                            {
                                inventoryList[i].Msrp = resultsPc.PriceComparisons[j].PriceComparisonsDetails[k].BookValue;
                            }
                            if (resultsPc.PriceComparisons[j].PriceComparisonsDetails[k].BookName == "Edmunds")
                            {
                                inventoryList[i].Edmunds = resultsPc.PriceComparisons[j].PriceComparisonsDetails[k].BookValue;
                            }
                        }
                        break;
                    }
                }
            }


            #endregion


            #region Calculate Price Comparisons and Vehicles without price comparison Flag and Create BookDto for each Inventory
            //const int kbbThr = 1;
            //const int nadaThr = 1;
            //const int msrp = 1;
            //const int edmunds = 1;
            //const int marketAvg = 1;
            //const int dealerFavThr = 2;


            foreach (var loadInventoryDetail in inventoryList)
            {
                List<BookDto> books = new List<BookDto>();
                loadInventoryDetail.VehiclesWithoutPriceComparison = false;


                //KBB
                if ((loadInventoryDetail.InternetPrice < loadInventoryDetail.Kbb) &&
                    ((loadInventoryDetail.Kbb - loadInventoryDetail.InternetPrice) >= kbbRetailValue))
                {


                    BookDto book = new BookDto();
                    book.BookName = "KBB";
                    book.BookValue = loadInventoryDetail.Kbb - loadInventoryDetail.InternetPrice;
                    books.Add(book);
                }
                //NADA
                if ((loadInventoryDetail.InternetPrice < loadInventoryDetail.Nada) &&
                    ((loadInventoryDetail.Nada - loadInventoryDetail.InternetPrice) >= nadaRetailValue))
                {


                    BookDto book = new BookDto();
                    book.BookName = "NADA";
                    book.BookValue = loadInventoryDetail.Nada - loadInventoryDetail.InternetPrice;
                    books.Add(book);
                }
                //MSRP
                if ((loadInventoryDetail.InternetPrice < loadInventoryDetail.Msrp) &&
                    ((loadInventoryDetail.Msrp - loadInventoryDetail.InternetPrice) >= originalMsrp))
                {


                    BookDto book = new BookDto();
                    book.BookName = "MSRP";
                    book.BookValue = loadInventoryDetail.Msrp - loadInventoryDetail.InternetPrice;
                    books.Add(book);
                }
                //EDMUNDS
                if ((loadInventoryDetail.InternetPrice < loadInventoryDetail.Edmunds) &&
                    ((loadInventoryDetail.Edmunds - loadInventoryDetail.InternetPrice) >= edmundsTrueMarketValue))
                {


                    BookDto book = new BookDto();
                    book.BookName = "Edmunds";
                    book.BookValue = loadInventoryDetail.Edmunds - loadInventoryDetail.InternetPrice;
                    books.Add(book);
                }
                //MarketAveage
                if (((decimal)loadInventoryDetail.InternetPrice < loadInventoryDetail.MarketAvg) &&
                    ((loadInventoryDetail.MarketAvg - (decimal)loadInventoryDetail.InternetPrice) >= marketAvgInternetPrice))
                {


                    BookDto book = new BookDto();
                    book.BookName = "Market Avg.Internet Price";
                    book.BookValue = Convert.ToInt32(loadInventoryDetail.MarketAvg - (decimal)loadInventoryDetail.InternetPrice);
                    books.Add(book);
                }


                if (favourableThreshold == 0)
                {
                    if ((loadInventoryDetail.ComparisonColor == 0) && (loadInventoryDetail.StatusBucket != 8))
                    {
                        loadInventoryDetail.VehiclesWithoutPriceComparison = true;
                    }
                }
                else if (favourableThreshold == 1)
                {
                    if ((loadInventoryDetail.ComparisonColor == 0 || loadInventoryDetail.ComparisonColor == 2) &&
                        (loadInventoryDetail.StatusBucket != 8))
                    {
                        loadInventoryDetail.VehiclesWithoutPriceComparison = true;
                    }
                }

                //Adding price comparison object to each inventory
                loadInventoryDetail.PriceComparisons = books;

            }

            #endregion


            #region Aggregate the data(Graph,Summary,table)
            //Order matters : Graph has to be created after graph table.

            #region GraphSummary

            var graphSummary = new GraphSummary
            {
                UnitsInStock = inventoryList.Count(),
                PctMarketAverage =
                    inventoryList.Where(il => il.PctMarketAverage != 0).ToList().Any()
                        ? Convert.ToInt32(
                            inventoryList.Where(il => il.PctMarketAverage != 0)
                    .ToList()
                    .Average(inventory => inventory.PctMarketAverage))
                        : 0,
                AvgInternetPrice =
                    inventoryList.Where(il => il.InternetPrice != 0).ToList().Any()
                        ? Convert.ToInt32(
                            inventoryList.Where(il => il.InternetPrice != 0)
                    .ToList()
                    .Average(inventory => inventory.InternetPrice))
                        : 0,
                MarketDaysSupply =
                    inventoryList.Where(il => il.MarketDaysSupply != 0).ToList().Any()
                        ? Convert.ToInt32(
                            inventoryList.Where(il => il.MarketDaysSupply != 0)
                    .ToList()
                    .Average(inventory => inventory.MarketDaysSupply))
                        : 0,
                DealerFavourableThreshold = resultsDi.DealerInfo.FavourableThreshold
            };


            #endregion


            #region GraphTable

            var inventories = new List<LoadInventoryDetail>();
            if (parameters.Arguments.Mode == "N")
            {
                //All inventory
                inventories = inventoryList.ToList();//.Skip(parameters.Arguments.MaximumRows * parameters.Arguments.StartPageIndex).Take(parameters.Arguments.MaximumRows).ToList();
                GetTrimList(inventories);
            }
            else if (parameters.Arguments.Mode == "A")
            {
                if (parameters.Arguments.ColumnIndex == 0)
                {
                    //Due for planning inventories
                    inventories = inventoryList.Where(il => il.DueForPlanning == true).ToList();//.Skip(parameters.Arguments.MaximumRows * parameters.Arguments.StartPageIndex).Take(parameters.Arguments.MaximumRows).ToList();
                    GetTrimList(inventories);
                }
                else
                {
                    //Age Index will depend on dealer specific settings.
                    inventories = inventoryList.Where(il => il.AgeIndex == parameters.Arguments.ColumnIndex).ToList();//.Skip(parameters.Arguments.MaximumRows * parameters.Arguments.StartPageIndex).Take(parameters.Arguments.MaximumRows).ToList();
                    GetTrimList(inventories);
                }
            }
            else if (parameters.Arguments.Mode == "R")
            {
                if (parameters.Arguments.ColumnIndex == 5)
                {
                    //Vehicles without price comparison
                    inventories = inventoryList.Where(il => il.VehiclesWithoutPriceComparison == true).ToList();//.Skip(parameters.Arguments.MaximumRows * parameters.Arguments.StartPageIndex).Take(parameters.Arguments.MaximumRows).ToList();
                    GetTrimList(inventories);
                }
                else
                {
                    //Risk Index should be between between 1 to 4 
                    inventories = inventoryList.Where(il => il.RiskIndex == parameters.Arguments.ColumnIndex).ToList();//.Skip(parameters.Arguments.MaximumRows * parameters.Arguments.StartPageIndex).Take(parameters.Arguments.MaximumRows).ToList();
                    GetTrimList(inventories);
                }
            }

            var inventoryTable = inventories.Select(loadInventoryDetail => new InventoryDto
            {
                Age = loadInventoryDetail.Age,
                VehicleDescription = loadInventoryDetail.VehicleDescription,
                Make = loadInventoryDetail.Make,
                VehicleColor = loadInventoryDetail.VehicleColor,
                VehicleMileage = loadInventoryDetail.VehicleMileage,
                UnitCost = loadInventoryDetail.UnitCost,
                InternetPrice = loadInventoryDetail.InternetPrice,
                StockNumber = loadInventoryDetail.StockNumber,
                TrimList = loadInventoryDetail.TrimList,
                InventoryId = loadInventoryDetail.InventoryId,
                PctMarketAverage = loadInventoryDetail.PctMarketAverage,
                PriceComparisons = loadInventoryDetail.PriceComparisons.OrderByDescending(x => x.BookValue).ToList(),
                MarketDaysSupply = loadInventoryDetail.MarketDaysSupply,
                ComparisonColor = loadInventoryDetail.ComparisonColor,
                AgeBucket = loadInventoryDetail.AgeBucket
            }).ToList();// Final table

            var table = new InventoryTable
            {
                Inventory = inventoryTable.OrderBy(il => il.Age).ToList()
            };


            #endregion


            #region Graph

            var dealerInput = new FetchDealerBucketsArgumentsDto
            {
                DealerId = parameters.Arguments.DealerId,
                Mode = parameters.Arguments.Mode
            };

            var identityContextDtoDb = new IdentityContextDto<FetchDealerBucketsArgumentsDto> { Arguments = dealerInput };

            var commandDb = factory.CreateFetchDealerBucketsCommand();
            FetchDealerBucketsResultsDto resultsDb = commandDb.Execute(identityContextDtoDb);

            var buckets = new List<BucketDto>();
            for (int i = 0; i < resultsDb.Buckets.Count(); i++)
            {
                var bucket = new BucketDto
                {
                    BucketId = resultsDb.Buckets[i].BucketId,
                    BucketName = resultsDb.Buckets[i].BucketName
                };

                buckets.Add(bucket);
            }



            var graph = new Graph { Buckets = buckets };
            int totalCount = inventoryList.Count();
            if (parameters.Arguments.Mode == "A")
            {
                //Add ageIndex Inventories as 0(Dueforplanning)
                inventoryList.Where(il => il.DueForPlanning == true).ToList().ForEach(fe => inventoryList.Add(new LoadInventoryDetail()
                {
                    Age = fe.Age,
                    AgeIndex = 0,//For Due for planning 
                    AgeDescription = fe.AgeDescription,
                    RiskIndex = fe.RiskIndex,
                    RiskDescription = fe.RiskDescription,
                    VehicleDescription = fe.VehicleDescription,
                    Make = fe.Make,
                    VehicleColor = fe.VehicleColor,
                    VehicleMileage = fe.VehicleMileage,
                    UnitCost = fe.UnitCost,
                    InternetPrice = fe.InternetPrice,
                    PctMarketAverage = fe.PctMarketAverage,
                    MarketDaysSupply = fe.MarketDaysSupply,
                    StockNumber = fe.StockNumber,
                    TrimList = fe.TrimList,
                    InventoryId = fe.InventoryId,
                    DueForPlanning = fe.DueForPlanning,
                    VehiclesWithoutPriceComparison = fe.VehiclesWithoutPriceComparison,
                    PriceComparisons = fe.PriceComparisons,
                    ComparisonColor = fe.ComparisonColor,
                    AgeBucket = fe.AgeBucket

                }
                ));
                //Group buckets based on age index
                var tempBucket = inventoryList.GroupBy(il => il.AgeIndex).Select(cl => new BucketDto { BucketId = cl.FirstOrDefault().AgeIndex, BucketName = cl.FirstOrDefault().AgeDescription, Bucketunits = cl.Count(), BucketPercentage = (cl.Count() * 100) / totalCount }).OrderBy(li => li.BucketId).ToList();
                foreach (var b in tempBucket)
                {
                    for (int i = 0; i < graph.Buckets.Count(); i++)
                    {
                        if (graph.Buckets[i].BucketId == b.BucketId)
                        {
                            graph.Buckets[i].Bucketunits = b.Bucketunits;
                            graph.Buckets[i].BucketPercentage = b.BucketPercentage;
                        }
                    }
                }
                graph.Buckets.OrderBy(il => il.BucketId);
            }
            else if (parameters.Arguments.Mode == "R")
            {

                //Add risk index inventories as 5 (Vehicles without price comparison)
                inventoryList.Where(il => il.VehiclesWithoutPriceComparison == true).ToList().ForEach(fe => inventoryList.Add(new LoadInventoryDetail()
                {
                    Age = fe.Age,
                    AgeIndex = fe.AgeIndex,
                    AgeDescription = fe.AgeDescription,
                    RiskIndex = 5,//For Vehicles without price comparison
                    RiskDescription = fe.RiskDescription,
                    VehicleDescription = fe.VehicleDescription,
                    Make = fe.Make,
                    VehicleColor = fe.VehicleColor,
                    VehicleMileage = fe.VehicleMileage,
                    UnitCost = fe.UnitCost,
                    InternetPrice = fe.InternetPrice,
                    PctMarketAverage = fe.PctMarketAverage,
                    MarketDaysSupply = fe.MarketDaysSupply,
                    StockNumber = fe.StockNumber,
                    TrimList = fe.TrimList,
                    InventoryId = fe.InventoryId,
                    DueForPlanning = fe.DueForPlanning,
                    VehiclesWithoutPriceComparison = fe.VehiclesWithoutPriceComparison,
                    PriceComparisons = fe.PriceComparisons,
                    ComparisonColor = fe.ComparisonColor,
                    AgeBucket = fe.AgeBucket
                }
                ));
                //Group buckets based on Risk index
                var tempBucket = inventoryList.GroupBy(il => il.RiskIndex).Select(cl => new BucketDto { BucketId = cl.FirstOrDefault().RiskIndex, BucketName = cl.FirstOrDefault().RiskDescription, Bucketunits = cl.Count(), BucketPercentage = (cl.Count() * 100) / totalCount }).OrderBy(li => li.BucketId).ToList();
                foreach (var b in tempBucket)
                {
                    for (int i = 0; i < graph.Buckets.Count(); i++)
                    {
                        if (graph.Buckets[i].BucketId == b.BucketId)
                        {
                            graph.Buckets[i].Bucketunits = b.Bucketunits;
                            graph.Buckets[i].BucketPercentage = b.BucketPercentage;
                        }
                    }
                }
                graph.Buckets.OrderBy(il => il.BucketId);
            }


            #endregion


            #endregion


            #region Response
            var finalResult = new FetchPricingSummaryResultsDto
            {
                OwnerName = ownerName,
                PringGraphSummary = graphSummary,
                PricingGraph = graph,
                PricingInventoryTable = table,
                BucketName = bucketDecription
            };
            return finalResult;

            #endregion


        }

        #region Call TrimList Command if trm is not selected

        public List<LoadInventoryDetail> GetTrimList(List<LoadInventoryDetail> inv)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            var trimListCommand = factory.CreateTrimListCommand();
            var identityContextDtoTl = new IdentityContextDto<FetchTrimListArgumentsDto>();

            var inventories = inv.Where(i => i.TrimList.Any(tl => tl.ChromeStyleId == 0 || tl.ChromeStyleId == -1)).ToList();

            foreach (var i in inventories)
            {
                var arg = new FetchTrimListArgumentsDto()
                {
                    Vin = i.Vin
                };
                identityContextDtoTl.Arguments = arg;
                FetchTrimListResultsDto result = trimListCommand.Execute(identityContextDtoTl);

                var trims = result.TrimList.Select(r => new TrimInfoDto()
                {
                    ChromeStyleId = r.ChromeStyleId,
                    Trim = r.Trim
                }).ToList();
                i.TrimList = trims;
            }

            foreach (var i in inventories)
            {
                for (int j = 0; j < inv.Count(); j++)
                {
                    if (i.InventoryId == inv[j].InventoryId)
                    {
                        inv[j].TrimList = i.TrimList;
                        break;
                    }

                }
            }

            return inv;
        }

        #endregion

    }
}
