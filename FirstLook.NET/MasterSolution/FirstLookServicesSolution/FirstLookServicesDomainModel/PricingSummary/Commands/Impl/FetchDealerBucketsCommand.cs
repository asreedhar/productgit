﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Model;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.Impl
{
    public class FetchDealerBucketsCommand : ICommand<FetchDealerBucketsResultsDto, IdentityContextDto<FetchDealerBucketsArgumentsDto>>
    {
        public FetchDealerBucketsResultsDto Execute(IdentityContextDto<FetchDealerBucketsArgumentsDto> parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();
            var loadDealerBucket = resolver.Resolve<IRepository>();

            var dealerBucketInput = new DealerBucketInput
            {
                DealerId = parameters.Arguments.DealerId,
                Mode = parameters.Arguments.Mode
            };

            IList<DealerBucketDetail> buckets = loadDealerBucket.FetchBuckets(dealerBucketInput);

            return new FetchDealerBucketsResultsDto
            {
                Buckets = buckets
            };

        }
    }
}
