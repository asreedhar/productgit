﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Model;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Repositories.Serializers
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ISerializer<LoadInventoryDetail>, LoadInventorySerializer>(ImplementationScope.Shared);

            registry.Register<ISerializer<DealerBucketDetail>, DealerBucketSerializer>(ImplementationScope.Shared);

            registry.Register<ISerializer<DealerBucketNameDetail>, DealerBucketNameSerializer>(ImplementationScope.Shared);

            registry.Register<ISerializer<AgeBucket>, AgeBucketSerializer>(ImplementationScope.Shared);
        }
    }
}
