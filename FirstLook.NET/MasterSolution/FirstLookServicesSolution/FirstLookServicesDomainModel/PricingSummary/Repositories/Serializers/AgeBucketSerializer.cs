﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using System.Text;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Model;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Repositories.Serializers
{
    public class AgeBucketSerializer : Serializer<AgeBucket>
    {
        public override AgeBucket Deserialize(IDataRecord record)
        {
            int ageBucketId;
            int low;
            int? high;


            ageBucketId = DataRecord.GetTinyInt(record, "RangeId", 0);
            low = record.GetInt32("Low", 0);
            high = DataRecord.GetNullableInt32(record, "High");

            return new AgeBucket
            {
                AgeBucketId = ageBucketId,
                Low = low,
                High = high

            };
        }
    }
}
