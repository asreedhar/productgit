﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Model;
namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Repositories.Serializers
{
    public class DealerBucketSerializer : Serializer<DealerBucketDetail>
    {
        public override DealerBucketDetail Deserialize(IDataRecord record)
        {
            int bucketId;
            string bucketName = string.Empty;
           
            try
            {
                bucketId = DataRecord.GetTinyInt(record, "ColumnIndex", 0);
                bucketName = DataRecord.GetString(record, "Description");
                
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return new DealerBucketDetail
            {
               BucketId = bucketId,
               BucketName= bucketName
            };
        }
    }
}
