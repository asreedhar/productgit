﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Model;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.TransferObjects;


namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Repositories.Serializers
{
    public class LoadInventorySerializer : Serializer<LoadInventoryDetail>
    {
        public override LoadInventoryDetail Deserialize(IDataRecord record)
        {
            int age;
            int ageIndex;
            string ageDescription;
            string vehicleDescription;
            string vehicleColor;
            string baseColor;
            string interiorColor;
            string interiorColorCode;
            string extColor1;
            string exteriorColorCode;
            string extColor2;
            string exteriorColorCode2;
            int vehicleMileage;
            int unitCost;
            int internetPrice;
            int marketDaysSupply;
            string stockNumber;
            int chromeStyleId;
            string trim;
            int inventoryId;
            int statusBucket;
            string vin;
            bool dueForPlanning;
            int vehicleYear;
            string make; 
            string model;

            try
            {
                age = record.GetInt32("Age", 0);
                ageIndex = record.GetTinyInt("AgeIndex", 0);
                ageDescription = record.GetString("AgeDescription");
                vehicleDescription = record.GetString("VehicleDescription");
                vehicleColor = record.GetString("VehicleColor");
                baseColor = record.GetString("BaseColor");
                extColor1 = record.GetString("ExtColor1");
                exteriorColorCode = record.GetString("ExteriorColorCode");
                extColor2 = record.GetString("ExtColor2");
                exteriorColorCode2 = record.GetString("ExteriorColorCode2");
                interiorColor = record.GetString("InteriorColor");
                interiorColorCode = record.GetString("InteriorColorCode");
                vehicleMileage = record.GetInt32("VehicleMileage", 0);
                unitCost = record.GetInt32("UnitCost", 0);
                internetPrice = record.GetInt32("ListPrice", 0);
                marketDaysSupply = record.GetInt32("MarketDaysSupply", 0);
                stockNumber = record.GetString("StokNumber");
                chromeStyleId = record.GetInt32("ChromeStyleId", 0);
                trim = record.GetString("Trim");
                inventoryId = record.GetInt32("InventoryID", 0);
                statusBucket = record.GetInt32("StatusBucket", 0);
                vin = record.GetString("VIN");
                dueForPlanning = record.GetBoolean("DueForPlanning", false);
                vehicleYear = record.GetInt32("vehicleYear", 0); 
                make = record.GetString("make");
                model = record.GetString("model"); 
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return new LoadInventoryDetail
            {
                AgeIndex = ageIndex,
                Age = age,
                AgeDescription = ageDescription,
                InventoryId = inventoryId,
                StatusBucket=statusBucket,
                Vin=vin,
                VehicleDescription = vehicleDescription,
                VehicleColor=vehicleColor,
                StockNumber = stockNumber,
                TrimList = new List<TrimInfoDto>()
                {
                    new TrimInfoDto()
                    {
                    ChromeStyleId = chromeStyleId,
                    Trim= trim
                    }
                },                
                BaseColor = baseColor,
                ExtColor1 = extColor1,
                ExteriorColorCode = exteriorColorCode,
                ExtColor2 = extColor2,
                ExteriorColorCode2 = exteriorColorCode2,
                InteriorColor = interiorColor,
                InteriorColorCode = interiorColorCode,
                VehicleMileage = vehicleMileage,
                UnitCost = unitCost,
                InternetPrice = internetPrice,
                MarketDaysSupply = marketDaysSupply,
                DueForPlanning = dueForPlanning,
                VehicleYear = vehicleYear, 
                Make = make,               
                Model = model,               
                AgeBucket=0
               
            };
        }
    }
}
