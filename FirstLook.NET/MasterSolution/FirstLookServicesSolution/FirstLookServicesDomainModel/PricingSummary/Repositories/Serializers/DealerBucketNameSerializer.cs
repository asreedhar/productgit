﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Model;


namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Repositories.Serializers
{
    public class DealerBucketNameSerializer : Serializer<DealerBucketNameDetail>
    {
        public override DealerBucketNameDetail Deserialize(IDataRecord record)
        {
            
            string bucketName = string.Empty;

            try
            {
                bucketName = DataRecord.GetString(record, "Description");

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return new DealerBucketNameDetail
            {                
                BucketName = bucketName
            };
        }
    }
}
