﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Model;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Repositories.Datastores;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Repositories.Gateways
{
    public class LoadInventoryGateway : GatewayBase
    {
        public IList<LoadInventoryDetail> Fetch(LoadInventoryInput inventoryInput)
        {
            string key = CreateCacheKey("" + inventoryInput.DealerId);

            IList<LoadInventoryDetail> value = Cache.Get(key) as List<LoadInventoryDetail>;

            if (value == null)
            {
                var serializer = Resolve<ISerializer<LoadInventoryDetail>>();

                var datastore = Resolve<ILoadInventoryDataStore>();

                using (IDataReader reader = datastore.LoadInventory(inventoryInput))
                {
                    IList<LoadInventoryDetail> items = serializer.Deserialize(reader);

                    value = items;

                    Remember(key, value);
                }
            }

            return value;
        }

        public IList<DealerBucketDetail> Fetch(DealerBucketInput dealerBucketInput)
        {
            string key = CreateCacheKey("" + dealerBucketInput.DealerId);

            IList<DealerBucketDetail> value = Cache.Get(key) as List<DealerBucketDetail>;

            if (value == null)
            {
                var serializer = Resolve<ISerializer<DealerBucketDetail>>();

                var datastore = Resolve<ILoadInventoryDataStore>();

                using (IDataReader reader = datastore.DealerBuckets(dealerBucketInput))
                {
                    IList<DealerBucketDetail> items = serializer.Deserialize(reader);

                    value = items;

                    Remember(key, value);
                }
            }

            return value;
        }

        public IList<DealerBucketNameDetail> Fetch(BucketNameInput dealerBucketInput)
        {
            string key = CreateCacheKey("" + dealerBucketInput.DealerId);

            IList<DealerBucketNameDetail> value = Cache.Get(key) as List<DealerBucketNameDetail>;

            if (value == null)
            {
                var serializer = Resolve<ISerializer<DealerBucketNameDetail>>();

                var datastore = Resolve<ILoadInventoryDataStore>();

                using (IDataReader reader = datastore.BucketName(dealerBucketInput))
                {
                    IList<DealerBucketNameDetail> items = serializer.Deserialize(reader);

                    value = items;

                    Remember(key, value);
                }
            }

            return value;
        }


        public IList<AgeBucket> Fetch(int businessUnitId)
        {
            string key = CreateCacheKey("" + businessUnitId);

            IList<AgeBucket> value = Cache.Get(key) as List<AgeBucket>;

            if (value == null)
            {
                var serializer = Resolve<ISerializer<AgeBucket>>();

                var datastore = Resolve<ILoadInventoryDataStore>();

                using (IDataReader reader = datastore.ReadBucketRanges(businessUnitId))
                {
                    IList<AgeBucket> items = serializer.Deserialize(reader);

                    value = items;

                    Remember(key, value);
                }
            }

            return value;
        }

    }
}
