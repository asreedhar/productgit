﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Model;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Repositories.Gateways;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Repositories
{
    public class PricingSummaryRepository : IRepository
    {
        public IList<LoadInventoryDetail> Loadinventory(LoadInventoryInput loadInventoryInput)
        {
            return new LoadInventoryGateway().Fetch(loadInventoryInput);
        }


        public IList<DealerBucketDetail> FetchBuckets(DealerBucketInput dealerBucketInput)
        {
            return new LoadInventoryGateway().Fetch(dealerBucketInput);
        }


        public IList<DealerBucketNameDetail> FetchBucketName(BucketNameInput bucketNameInput)
        {
            return new LoadInventoryGateway().Fetch(bucketNameInput);
        }

        public IList<AgeBucket> FetchAgeBucket(int businessUnitId)
        {
            return new LoadInventoryGateway().Fetch(businessUnitId);
        }
    }
}
