﻿using FirstLook.FirstLookServices.DomainModel.PricingSummary.Model;
using FirstLook.Common.Core.Registry;

namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Repositories
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<Datastores.Module>();

            registry.Register<Serializers.Module>();

            registry.Register<IRepository, PricingSummaryRepository>(ImplementationScope.Shared);
        }
    }
}
