﻿using System.Data;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Repositories.Datastores
{
    public interface ILoadInventoryDataStore
    {
        IDataReader LoadInventory(LoadInventoryInput inventoryInput);

        IDataReader DealerBuckets(DealerBucketInput dealerBucketInput);

        IDataReader BucketName(BucketNameInput bucketNameInput);

        IDataReader ReadBucketRanges(int businessUnitId);
    }
}
