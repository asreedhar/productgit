﻿using System.Data;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Repositories.Entities;
using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Repositories.Datastores
{
    public class LoadInventoryDataStore : ILoadInventoryDataStore
    {
        #region Class Members

        private const string Database_IMT = "IMT";
        private const string Database_Merchandising = "Merchandising";


        #endregion

        public IDataReader LoadInventory(LoadInventoryInput inventoryInput)
        {
            using (IDbConnection connection = Database.Connection(Database_Merchandising))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "builder.Load#Inventory";

                    Database.AddWithValue(command, "DealerId", inventoryInput.DealerId, DbType.String);
                    Database.AddWithValue(command, "SaleStrategy", inventoryInput.SaleStrategy, DbType.String);
                    Database.AddWithValue(command, "InventoryFilter", inventoryInput.InventoryFilter, DbType.String);

                    var set = new DataSet();

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        var table = new DataTable("LoadInventory");

                        table.Load(reader);

                        set.Tables.Add(table);
                    }

                    return set.CreateDataReader();
                }
            }
        }


        public IDataReader DealerBuckets(DealerBucketInput dealerBucketInput)
        {
            using (IDbConnection connection = Database.Connection(Database_IMT))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "dbo.Dealerbuckets#Fetch";

                    Database.AddWithValue(command, "DealerId", dealerBucketInput.DealerId, DbType.String);
                    Database.AddWithValue(command, "Mode", dealerBucketInput.Mode, DbType.String);

                    var set = new DataSet();

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        var table = new DataTable("DealerBuckets");

                        table.Load(reader);

                        set.Tables.Add(table);
                    }

                    return set.CreateDataReader();
                }
            }
        }


        public IDataReader BucketName(BucketNameInput bucketNameInput)
        {

            using (IDbConnection connection = Database.Connection(Database_IMT))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "dbo.DealerbucketName#Fetch";

                    Database.AddWithValue(command, "DealerId", bucketNameInput.DealerId, DbType.String);
                    Database.AddWithValue(command, "Mode", bucketNameInput.Mode, DbType.String);
                    Database.AddWithValue(command, "ColumnIndex", bucketNameInput.ColumnIndex, DbType.String);

                    var set = new DataSet();

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        var table = new DataTable("DealerBucketName");

                        table.Load(reader);

                        set.Tables.Add(table);
                    }

                    return set.CreateDataReader();
                }
            }
        }

        public IDataReader ReadBucketRanges(int businessUnitId)
        {
            using (IDbConnection connection = Database.Connection(Database_Merchandising))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "Merchandising.workflow.GetInventoryBucketRanges";


                    Database.AddWithValue(command, "@BusinessUnitId", businessUnitId, DbType.Int32);

                    var set = new DataSet();
                    using (IDataReader reader = command.ExecuteReader())
                    {
                        var table = new DataTable("AgeBucketName");

                        table.Load(reader);

                        set.Tables.Add(table);
                    }

                    return set.CreateDataReader();

                }

            }
        }
    }
}
