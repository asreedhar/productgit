﻿
namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Repositories.Entities
{
    public class LoadInventoryInput
    {
        public int DealerId { get; set; }

        public string SaleStrategy { get; set; }

        public string InventoryFilter { get; set; }
    }
}
