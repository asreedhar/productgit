﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Repositories.Entities
{
    public class DealerBucketInput
    {
        public int DealerId { get; set; }

        public string Mode { get; set; }
    }
}
