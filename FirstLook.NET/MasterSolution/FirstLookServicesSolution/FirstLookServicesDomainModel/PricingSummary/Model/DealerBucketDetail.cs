﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Model
{
    public class DealerBucketDetail
    {
        public int BucketId { get; set; }

        public string BucketName { get; set; }
    }
}
