﻿using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Model
{
    public class LoadInventoryDetail
    {
        public int Age { get; set; }

        public int AgeIndex { get; set; }

        public string AgeDescription { get; set; }

        public int RiskIndex { get; set; }

        public string RiskDescription { get; set; }

        public string VehicleDescription { get; set; }

        public string VehicleColor { get; set; }

        public string BaseColor { get; set; }

        public string InteriorColor { get; set; }

        public string InteriorColorCode { get; set; }

        public string ExtColor1 { get; set; }

        public string ExteriorColorCode { get; set; }

        public string ExtColor2 { get; set; }

        public string ExteriorColorCode2 { get; set; }

        public int VehicleMileage { get; set; }

        public int UnitCost { get; set; }

        public int InternetPrice { get; set; }

        public string Vin { get; set; }

        public decimal? PctMarketAverage { get; set; }

        public int MarketDaysSupply { get; set; }

        public List<BookDto> PriceComparisons { get; set; }

        public string StockNumber { get; set; }

        public List<TrimInfoDto> TrimList { get; set; }

        public int InventoryId { get; set; }

        public bool DueForPlanning { get; set; }

        public bool VehiclesWithoutPriceComparison { get; set; }

        public int Kbb { get; set; }

        public int Nada { get; set; }

        public int Msrp { get; set; }

        public int Edmunds { get; set; }

        public decimal? MarketAvg { get; set; }

        public int VehicleYear { get; set; } 
        public string Make { get; set; }
        public string Model { get; set; }

        public int StatusBucket { get; set; }

        public int ComparisonColor { get; set; }

        public int AgeBucket { get; set; }
    }
}
