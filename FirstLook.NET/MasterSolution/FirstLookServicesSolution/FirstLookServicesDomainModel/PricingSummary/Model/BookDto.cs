﻿
namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Model
{
    public class BookDto
    {
        public string BookName { get; set; }

        public int BookValue { get; set; }
    }
}
