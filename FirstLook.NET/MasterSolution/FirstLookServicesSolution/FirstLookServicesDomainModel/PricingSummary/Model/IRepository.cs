﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.PricingSummary.Model
{
    public interface IRepository
    {
        IList<LoadInventoryDetail> Loadinventory(LoadInventoryInput loadInventoryInput);

        IList<DealerBucketDetail> FetchBuckets (DealerBucketInput dealerBucketInput);

        IList<DealerBucketNameDetail> FetchBucketName(BucketNameInput bucketNameInput);

        IList<AgeBucket> FetchAgeBucket(int businessUnitId);
    }
}
