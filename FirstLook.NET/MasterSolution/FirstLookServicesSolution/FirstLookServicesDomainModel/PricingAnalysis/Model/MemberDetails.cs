﻿
namespace FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Model
{
    public class MemberDetails
    {
        public int MemberId { get; set; }
        public string FirstName {get;set;}
        public string LastName  {get;set;}
        public string Login     {get;set;}
        public int MemberType   { get; set; }
                        
    }
}
