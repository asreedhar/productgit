﻿using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Model
{
    public interface IRepository
    {
        IList<MemberDetails> Member(MemberInput memberInput);
    }
}
