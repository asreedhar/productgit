﻿using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Commands.Impl
{
    public class CommandFactory:ICommandFactory
    {
        public ICommand<ChangeInternetPriceResultsDto, IdentityContextDto<ChangeInternetPriceArgumentsDto>> CreateChangeInternetPriceCommand()
        {
            return new ChangeInternetPriceCommand();
        }
    }
}
