﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Model;

namespace FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Commands.Impl
{
    public static class Mapper
    {
        public static IList<MemberDetailsDto> Map(IList<MemberDetails> objs)
        {
            IList<MemberDetailsDto> memberDetailsDtos = objs.Select(obj => new MemberDetailsDto { MemberId=obj.MemberId,FirstName=obj.FirstName,LastName=obj.LastName,Login=obj.Login,MemberType=obj.MemberType }).ToList();
            return memberDetailsDtos;

        }
    }
}
