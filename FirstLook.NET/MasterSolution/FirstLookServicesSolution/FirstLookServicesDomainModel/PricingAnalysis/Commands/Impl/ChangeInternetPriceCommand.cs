﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Model;
using FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Repositories.Entities;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Distribution;

namespace FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Commands.Impl
{
    public class ChangeInternetPriceCommand:ICommand<ChangeInternetPriceResultsDto,IdentityContextDto<ChangeInternetPriceArgumentsDto>>
    {
        private decimal MinPrice = 0;
        private decimal MaxPrice = 999999;

        private bool IsPriceValid(decimal? newPrice)
        {
            if(newPrice.HasValue)
                return newPrice >= MinPrice && newPrice <= MaxPrice;
            return false;

        }


        public ChangeInternetPriceResultsDto Execute(IdentityContextDto<ChangeInternetPriceArgumentsDto> parameters)
        {
            if (IsPriceValid(parameters.Arguments.NewPrice))
            {

                IResolver resolver = RegistryFactory.GetResolver();
                var pricingAnalysisRepository = resolver.Resolve<IRepository>();

                MemberInput memberInput = new MemberInput() { username = (parameters.Arguments.Username != null) ? parameters.Arguments.Username : parameters.Identity.Name };

                IList<MemberDetailsDto> memberDetails = Mapper.Map(pricingAnalysisRepository.Member(memberInput));

                if (memberDetails[0].MemberType == 2)
                {
                    VehicleInteractionAuditCommand.Reprice(parameters.Arguments.OwnerHandle,
                        parameters.Arguments.VehicleHandle,
                        parameters.Arguments.SearchHandle,
                        (parameters.Arguments.Username != null) ? parameters.Arguments.Username : parameters.Identity.Name,
                        Decimal.ToInt32(parameters.Arguments.Oldprice),
                        Decimal.ToInt32(parameters.Arguments.NewPrice));
                }
                InventoryRepriceCommand.Execute(
                    (parameters.Arguments.Username != null) ? parameters.Arguments.Username : parameters.Identity.Name,
                    parameters.Arguments.InventoryId,
                    Decimal.ToInt32(parameters.Arguments.Oldprice),
                    Decimal.ToInt32(parameters.Arguments.NewPrice),
                    false);

                DistributeRepriceCommand.Execute(
                    parameters.Arguments.DealerId,
                    parameters.Arguments.InventoryId,
                    Decimal.ToInt32(parameters.Arguments.NewPrice));

            }



            return new ChangeInternetPriceResultsDto() { OldInternetPrice=parameters.Arguments.Oldprice,NewinternetPrice=parameters.Arguments.NewPrice };
        }
    }
}
