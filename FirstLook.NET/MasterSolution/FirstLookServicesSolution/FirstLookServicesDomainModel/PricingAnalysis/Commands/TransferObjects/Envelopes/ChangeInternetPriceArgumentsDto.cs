﻿
namespace FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Commands.TransferObjects.Envelopes
{
    public class ChangeInternetPriceArgumentsDto
    {
        public string OwnerHandle { get; set; }
        public string VehicleHandle { get; set; }
        public string SearchHandle { get; set; }
        public decimal Oldprice { get; set; }
        public decimal NewPrice { get; set; }
        public int InventoryId { get; set; }
        public int DealerId { get; set; }
        public string Username { get; set; }
    }
}
