﻿
namespace FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Commands.TransferObjects.Envelopes
{
    public class ChangeInternetPriceResultsDto
    {
        public decimal OldInternetPrice { get; set; }
        public decimal NewinternetPrice { get; set; }
    }
}
