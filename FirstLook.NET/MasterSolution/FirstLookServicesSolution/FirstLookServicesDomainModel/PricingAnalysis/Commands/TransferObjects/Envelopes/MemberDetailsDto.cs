﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Commands.TransferObjects.Envelopes
{
    public class MemberDetailsDto
    {
        public int MemberId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Login { get; set; }
        public int MemberType { get; set; }
    }
}
