﻿using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Commands
{
    public interface ICommandFactory
    {
        ICommand<ChangeInternetPriceResultsDto, IdentityContextDto<ChangeInternetPriceArgumentsDto>> CreateChangeInternetPriceCommand();
    }
}
