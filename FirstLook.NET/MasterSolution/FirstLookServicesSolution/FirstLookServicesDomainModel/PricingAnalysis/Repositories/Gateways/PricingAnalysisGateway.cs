﻿using System.Collections.Generic;
using FirstLook.Common.Core;
using FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Model;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Repositories.DataStores;
using System.Data;

namespace FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Repositories.Gateways
{
    public class PricingAnalysisGateway:GatewayBase
    {
        public IList<MemberDetails> FetchMemberDetails(MemberInput memberInput)
        {

            string key = CreateCacheKey("" + memberInput.username);

            IList<MemberDetails> value = Cache.Get(key) as IList<MemberDetails>;

            if (value == null)
            {
                var serializer = Resolve<ISerializer<MemberDetails>>();

                var datastore = Resolve<IPricingAnalysisDataStore>();
                using (IDataReader reader = datastore.FetchMemberDetails(memberInput))
                {
                    IList<MemberDetails> items = serializer.Deserialize(reader);

                    value = items;

                    Remember(key, value);
                }
            }
            return value;
        }

    }

}
