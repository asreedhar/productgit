﻿using System.Data;
using FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Repositories.DataStores
{
    public interface IPricingAnalysisDataStore
    {
        IDataReader FetchMemberDetails(MemberInput memberInput);
    }
}
