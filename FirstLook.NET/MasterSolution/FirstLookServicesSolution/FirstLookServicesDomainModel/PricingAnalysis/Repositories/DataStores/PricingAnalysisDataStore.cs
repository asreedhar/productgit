﻿using System.Data;
using FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Repositories.Entities;
using FirstLook.Common.Core.Data;

namespace FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Repositories.DataStores
{
    class PricingAnalysisDataStore:IPricingAnalysisDataStore
    {

        private const string DatabaseName = "IMT";

        public IDataReader FetchMemberDetails(MemberInput memberInput)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "dbo.Member#Fetch";

                    Database.AddWithValue(command, "UserName", memberInput.username, DbType.String);
                    

                    var set = new DataSet();

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        var table = new DataTable("Member");

                        table.Load(reader);

                        set.Tables.Add(table);
                    }

                    return set.CreateDataReader();
                }
            }
        }
    }
}
