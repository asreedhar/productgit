﻿using System;
using FirstLook.Common.Core.Registry;

namespace FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Repositories.DataStores
{
    [Serializable]
    class Module:IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IPricingAnalysisDataStore, PricingAnalysisDataStore>(ImplementationScope.Shared);
        }
    }
}
