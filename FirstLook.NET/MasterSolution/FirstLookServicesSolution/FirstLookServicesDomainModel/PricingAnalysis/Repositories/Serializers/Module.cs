﻿using System;
using FirstLook.Common.Core.Registry;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Model;

namespace FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Repositories.Serializers
{
    [Serializable]
    class Module:IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ISerializer<MemberDetails>, MemberDetailsSerializers>(ImplementationScope.Shared);
        }
    }
}
