﻿using System;
using FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Model;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using System.Data;

namespace FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Repositories.Serializers
{
    public class MemberDetailsSerializers:Serializer<MemberDetails>
    {
        public override MemberDetails Deserialize(IDataRecord record)
        {
            int id = 0;
            string fname = string.Empty;
            string lname = string.Empty;
            string login = string.Empty;
            int mtype = 0;
            try
            {
                id=    DataRecord.GetInt32(record, "Id", 0);
                fname= DataRecord.GetString(record, "FirstName");
                lname = DataRecord.GetString(record, "LastName");
                login = DataRecord.GetString(record, "UserName");
                mtype = DataRecord.GetInt32(record, "MemberType", 0);
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return new MemberDetails()
            {
                MemberId = id,
                FirstName = fname,
                LastName = lname,
                Login = login,
                MemberType = mtype
            };
        }
    }
}
