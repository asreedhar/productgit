﻿using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Model;
using FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Repositories.Gateways;

namespace FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Repositories
{
    public class PricingAnalysisRepository:IRepository
    {
        public IList<MemberDetails> Member(MemberInput memberInput)
        {
            return new PricingAnalysisGateway().FetchMemberDetails(memberInput);
        }
    }
}
