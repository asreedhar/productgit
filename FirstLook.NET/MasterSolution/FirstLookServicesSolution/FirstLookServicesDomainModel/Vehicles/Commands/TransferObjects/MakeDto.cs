﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.TransferObjects
{
    [Serializable]
    public class MakeDto
    {
        public int Id { get; set; }

        public string Desc { get; set; }
    }
}
