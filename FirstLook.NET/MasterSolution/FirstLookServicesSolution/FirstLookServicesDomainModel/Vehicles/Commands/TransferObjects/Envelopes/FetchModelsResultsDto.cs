﻿using System;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class FetchModelsResultsDto
    {
        public List<ModelDto> Models { get; set; }
    }
}
