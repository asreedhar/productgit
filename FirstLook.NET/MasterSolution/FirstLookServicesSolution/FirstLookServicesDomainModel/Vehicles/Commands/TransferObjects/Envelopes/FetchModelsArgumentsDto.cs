﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class FetchModelsArgumentsDto
    {
        public int Year { get; set; }

        public int MakeId { get; set; }
    }
}
