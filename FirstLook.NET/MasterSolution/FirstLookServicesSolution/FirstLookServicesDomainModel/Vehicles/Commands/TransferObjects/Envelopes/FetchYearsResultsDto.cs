﻿using System;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class FetchYearsResultsDto
    {
        public List<int> Years { get; set; }
    }
}
