﻿using System;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    [Serializable]

    public class FetchVehicleHierarchyResultsDto
    {
        public List<VehicleHierarchyDto> VehicleHierarchies { get; set; }
    }
}
