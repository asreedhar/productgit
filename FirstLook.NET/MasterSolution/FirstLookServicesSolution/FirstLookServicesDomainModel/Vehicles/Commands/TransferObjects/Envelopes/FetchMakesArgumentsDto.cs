﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class FetchMakesArgumentsDto
    {
        public int Year { get; set; }
    }
}
