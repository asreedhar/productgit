﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class FetchStylesArgumentsDto
    {
        public int Year { get; set; }

        public int MakeId { get; set; }

        public int ModelId { get; set; }
    }
}
