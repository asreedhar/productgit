﻿using System;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class FetchMakesResultsDto
    {
        public List<MakeDto> Makes { get; set; }
    }
}
