﻿using System;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class FetchStylesResultsDto
    {
        public List<StyleDto> Styles { get; set; }
    }
}
