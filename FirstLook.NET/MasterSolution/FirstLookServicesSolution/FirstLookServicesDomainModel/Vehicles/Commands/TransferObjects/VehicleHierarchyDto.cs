﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.TransferObjects
{
    [Serializable]

    public class VehicleHierarchyDto
    {
        public int Year { get; set; }

        public string Make { get; set; }

        public string Model { get; set; }

        public string Style { get; set; }

        public int StyleId { get; set; }
    }
}
