﻿using System;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.Impl
{
    class CommandFactory : ICommandFactory
    {
        public ICommand<FetchVehicleHierarchyResultsDto, FetchVehicleHierarchyArgumentsDto> CreateFetchVehicleHierarchyCommand()
        {
            return new FetchVehicleHierarchyCommand();
        }

        public ICommand<FetchYearsResultsDto, FetchYearsArgumentsDto> CreateFetchYearsCommand()
        {
            return new FetchYearsCommand();
        }

        public ICommand<FetchMakesResultsDto, FetchMakesArgumentsDto> CreateFetchMakesCommand()
        {
            return new FetchMakesCommand();
        }

        public ICommand<FetchModelsResultsDto, FetchModelsArgumentsDto> CreateFetchModelsCommand()
        {
            return new FetchModelsCommand();
        }

        public ICommand<FetchStylesResultsDto, FetchStylesArgumentsDto> CreateFetchStylesCommand()
        {
            return new FetchStylesCommand();
        }

    }
}
