﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Model;

namespace FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.Impl
{
    public static class Mapper
    {
        public static VehicleHierarchyDto Map (IVehicleHierarchy obj)
        {
            //TODO: follow up... can u have a null style or year? ... should this not be allowed in returned data?
            return new VehicleHierarchyDto
            {
                Make = obj.Make,
                Model = obj.Model,
                Style = obj.Style,
                StyleId = obj.StyleId.GetValueOrDefault(),
                Year = obj.Year.GetValueOrDefault()
            };
        }


        public static IList<VehicleHierarchyDto> Map (IList<IVehicleHierarchy> objs)
        {
            IList<VehicleHierarchyDto> dtos = objs.Select(Map).ToList();

            return dtos;
        }

        public static IList<MakeDto> Map(IList<Make> objs)
        {
            IList<MakeDto> dtos = objs.Select(Map).ToList();

            return dtos;
        }

        public static MakeDto Map(Make obj)
        {
            if(obj == null)
            {
                return null;
            }

            return new MakeDto
                       {
                           Desc = obj.Desc,
                           Id = obj.Id
                       };
        }

        public static IList<ModelDto> Map(IList<Model.Model> objs)
        {
            IList<ModelDto> dtos = objs.Select(Map).ToList();

            return dtos;
        }

        public static ModelDto Map(Model.Model obj)
        {
            if (obj == null)
            {
                return null;
            }

            return new ModelDto
                       {
                Desc = obj.Desc,
                Id = obj.Id
            };
        }

        public static IList<StyleDto> Map(IList<Style> objs)
        {
            IList<StyleDto> dtos = objs.Select(Map).ToList();

            return dtos;
        }

        public static StyleDto Map(Style obj)
        {
            if (obj == null)
            {
                return null;
            }

            return new StyleDto
            {
                Desc = obj.Desc,
                Id = obj.Id
            };
        }
    }
}
