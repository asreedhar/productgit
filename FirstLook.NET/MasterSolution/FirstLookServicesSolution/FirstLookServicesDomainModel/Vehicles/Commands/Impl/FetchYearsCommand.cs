﻿using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Model;

namespace FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.Impl
{
    public class FetchYearsCommand : ICommand<FetchYearsResultsDto, FetchYearsArgumentsDto>
    {
        public FetchYearsResultsDto Execute(FetchYearsArgumentsDto parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            var vehiclesRepository = resolver.Resolve<IRepository>();

            IList<int> years = vehiclesRepository.Years();

            if (years.Count == 0)
            {
                throw new NoDataFoundException("Dev: No years found.");
            }

            return new FetchYearsResultsDto
            {
                Years = (List<int>) years
            };
        }
    }
}
