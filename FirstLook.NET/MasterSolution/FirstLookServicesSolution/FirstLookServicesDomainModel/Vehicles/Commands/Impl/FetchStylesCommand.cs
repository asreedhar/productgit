﻿using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Model;

namespace FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.Impl
{
    public class FetchStylesCommand : ICommand<FetchStylesResultsDto, FetchStylesArgumentsDto>
    {
        public FetchStylesResultsDto Execute(FetchStylesArgumentsDto parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            var vehiclesRepository = resolver.Resolve<IRepository>();

            IList<StyleDto> styles = Mapper.Map(vehiclesRepository.Styles(parameters.Year, parameters.MakeId, parameters.ModelId));

            if (styles.Count == 0)
            {
                throw new NoDataFoundException("Dev: No styles found for given year/make/model combination.");
            }

            return new FetchStylesResultsDto
            {
                Styles = (List<StyleDto>)styles
            };
        }
    }
}
