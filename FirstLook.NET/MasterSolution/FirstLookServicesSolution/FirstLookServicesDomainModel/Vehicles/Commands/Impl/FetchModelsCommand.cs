﻿using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Model;

namespace FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.Impl
{
    public class FetchModelsCommand : ICommand<FetchModelsResultsDto, FetchModelsArgumentsDto>
    {
        public FetchModelsResultsDto Execute(FetchModelsArgumentsDto parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            var vehiclesRepository = resolver.Resolve<IRepository>();

            IList<ModelDto> models = Mapper.Map(vehiclesRepository.Models(parameters.Year, parameters.MakeId));

            if (models.Count == 0)
            {
                throw new NoDataFoundException("Dev: No models found for given year/make combination.");
            }

            return new FetchModelsResultsDto
            {
                Models = (List<ModelDto>) models
            };
        }
    }
}
