﻿using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Model;

namespace FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.Impl
{
    public class FetchVehicleHierarchyCommand : ICommand<FetchVehicleHierarchyResultsDto, FetchVehicleHierarchyArgumentsDto>
    {
        public FetchVehicleHierarchyResultsDto Execute(FetchVehicleHierarchyArgumentsDto parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            var vehiclesRepository = resolver.Resolve<IRepository>();

            IList<VehicleHierarchyDto> vehicleHierarchies = Mapper.Map(vehiclesRepository.VehicleHierarchies());

            if (vehicleHierarchies.Count == 0)
            {
                throw new NoDataFoundException("Dev: No hierarchies found.");
            }

            return new FetchVehicleHierarchyResultsDto
            {
                VehicleHierarchies = (List<VehicleHierarchyDto>) Mapper.Map(vehiclesRepository.VehicleHierarchies())
            };
        }
    }
}
