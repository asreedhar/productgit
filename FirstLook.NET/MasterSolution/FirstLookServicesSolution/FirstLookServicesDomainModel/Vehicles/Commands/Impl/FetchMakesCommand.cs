﻿using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Model;

namespace FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.Impl
{
    public class FetchMakesCommand : ICommand<FetchMakesResultsDto, FetchMakesArgumentsDto>
    {
        public FetchMakesResultsDto Execute(FetchMakesArgumentsDto parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            var vehiclesRepository = resolver.Resolve<IRepository>();

            IList<MakeDto> makes = Mapper.Map(vehiclesRepository.Makes(parameters.Year));

            if (makes.Count == 0)
            {
                throw new NoDataFoundException("Dev: No makes found for given year.");
            }

            return new FetchMakesResultsDto
            {
                Makes = (List<MakeDto>) makes
            };
        }
    }
}
