﻿using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.Vehicles.Commands
{
    public interface ICommandFactory
    {
        ICommand<FetchVehicleHierarchyResultsDto, FetchVehicleHierarchyArgumentsDto> CreateFetchVehicleHierarchyCommand();

        ICommand<FetchYearsResultsDto, FetchYearsArgumentsDto> CreateFetchYearsCommand();

        ICommand<FetchMakesResultsDto, FetchMakesArgumentsDto> CreateFetchMakesCommand();

        ICommand<FetchModelsResultsDto, FetchModelsArgumentsDto> CreateFetchModelsCommand();

        ICommand<FetchStylesResultsDto, FetchStylesArgumentsDto> CreateFetchStylesCommand();

    }
}
