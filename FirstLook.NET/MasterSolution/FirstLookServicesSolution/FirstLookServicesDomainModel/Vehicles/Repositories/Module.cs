﻿using System;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Model;

namespace FirstLook.FirstLookServices.DomainModel.Vehicles.Repositories
{
    [Serializable]
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<Datastores.Module>();

            registry.Register<Serializers.Module>();

            registry.Register<IRepository, VehicleRepository>(ImplementationScope.Shared);

        }
    }
}
