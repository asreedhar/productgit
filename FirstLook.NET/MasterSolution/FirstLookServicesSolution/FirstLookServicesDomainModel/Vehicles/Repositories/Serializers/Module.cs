﻿using System;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Model;

namespace FirstLook.FirstLookServices.DomainModel.Vehicles.Repositories.Serializers
{
    [Serializable]
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ISerializer<IVehicleHierarchy>, VehicleHierarchySerializer>(ImplementationScope.Shared);
        }
    }
}
