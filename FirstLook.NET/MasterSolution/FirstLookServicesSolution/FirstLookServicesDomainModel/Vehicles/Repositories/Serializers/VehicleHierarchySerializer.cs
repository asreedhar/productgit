﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Model;

namespace FirstLook.FirstLookServices.DomainModel.Vehicles.Repositories.Serializers
{
    public class VehicleHierarchySerializer : Serializer<IVehicleHierarchy>
    {
        public override IVehicleHierarchy Deserialize(IDataRecord record)
        {
            int? year = record.GetNullableInt32("Year");

            string make = record.GetString("DivisionName");

            string model = record.GetString("CFModelName");

            string style = record.GetString("CFStyleName");

            int? styleId = record.GetNullableInt32("ChromeStyleID");

            return new VehicleHierarchy
                       {
                           Make = make,
                           Model = model,
                           Style = style,
                           StyleId = styleId,
                           Year = year
                       };
        }
    }
}
