﻿using System.Collections.Generic;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Model;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Repositories.Gateways;

namespace FirstLook.FirstLookServices.DomainModel.Vehicles.Repositories
{
    public class VehicleRepository : RepositoryBase, IRepository
    {
        #region IRepository Members

        public IList<IVehicleHierarchy> VehicleHierarchies()
        {
            return new VehicleGateway().VehicleHierarchyFetch();
        }

        public IList<int> Years()
        {
            return new VehicleGateway().YearsFetch();
        }

        public IList<Make> Makes(int year)
        {
            return new VehicleGateway().MakesFetch(year);
        }

        public IList<Model.Model> Models(int year, int makeId)
        {
            return new VehicleGateway().ModelsFetch(year, makeId);
        }

        public IList<Style> Styles(int year, int makeId, int modelId)
        {
            return new VehicleGateway().StylesFetch(year, makeId, modelId);
        }

        #endregion

        protected override string DatabaseName
        {
            get { return "VehicleCatalog"; }
        }

        protected override void OnBeginTransaction(IDataSession session)
        {
            // do nothing
        }
    }
}
