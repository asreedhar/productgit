﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Model;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Repositories.Datastores;

namespace FirstLook.FirstLookServices.DomainModel.Vehicles.Repositories.Gateways
{
    public class VehicleGateway : GatewayBase
    {
        public IList<IVehicleHierarchy> VehicleHierarchyFetch()
        {
            string key = CreateCacheKey("vehicleHierarchies");

            IList<IVehicleHierarchy> value = Cache.Get(key) as List<IVehicleHierarchy>;

            if (value == null)
            {
                var serializer = Resolve<ISerializer<IVehicleHierarchy>>();

                var datastore = Resolve<IVehicleDatastore>();

                using (IDataReader reader = datastore.VehicleHierarchies())
                {
                    IList<IVehicleHierarchy> items = serializer.Deserialize(reader);

                    value = items;

                    Remember(key, value);
                }
            }

            return value;
        }

        public IList<int> YearsFetch()
        {
            var datastore = Resolve<IVehicleDatastore>();

            IList<int> years = new List<int>();

            using (IDataReader reader = datastore.Years())
            {
                while (reader.Read())
                {
                    years.Add(reader.GetInt32(reader.GetOrdinal("Year")));
                }

            }

            return years;
        }

        public IList<Make> MakesFetch(int year)
        {
            var datastore = Resolve<IVehicleDatastore>();

            IList<Make> makes = new List<Make>();

            using (IDataReader reader = datastore.Makes(year))
            {
                while (reader.Read())
                {
                    makes.Add( new Make
                                   {
                                       Desc = reader.GetString(reader.GetOrdinal("DivisionName")),
                                       Id = reader.GetInt32(reader.GetOrdinal("DivisionID"))
                                   });
                }

            }

            return makes;
        }

        public IList<Model.Model> ModelsFetch(int year, int makeId)
        {
            var datastore = Resolve<IVehicleDatastore>();

            IList<Model.Model> models = new List<Model.Model>();

            using (IDataReader reader = datastore.Models(year, makeId))
            {
                while (reader.Read())
                {
                    models.Add( new Model.Model
                                   {
                                       Desc = reader.GetString(reader.GetOrdinal("CFModelName")),
                                       Id = reader.GetInt32(reader.GetOrdinal("ModelID"))
                                   });
                }

            }

            return models;
        }

        public IList<Style> StylesFetch(int year, int makeId, int modelId)
        {
            var datastore = Resolve<IVehicleDatastore>();

            IList<Style> styles = new List<Style>();

            using (IDataReader reader = datastore.Styles(year, makeId, modelId))
            {
                while (reader.Read())
                {
                    styles.Add(new Style
                    {
                        Desc = reader.GetString(reader.GetOrdinal("CFStyleName")),
                        Id = reader.GetInt32(reader.GetOrdinal("ChromeStyleID"))
                    });
                }

            }

            return styles;
        }

    }
}
