﻿using System.Data;
using System.Reflection;
using FirstLook.Common.Core.Data;

namespace FirstLook.FirstLookServices.DomainModel.Vehicles.Repositories.Datastores
{
    public class VehicleDatastore : SimpleDataStore, IVehicleDatastore
    {
        public IDataReader VehicleHierarchies()
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "Chrome.GetYearsMakesModelsStyles";

                    var set = new DataSet();

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        var table = new DataTable("VehicleHierarchies");

                        table.Load(reader);

                        set.Tables.Add(table);
                    }

                    return set.CreateDataReader();
                }
            }
        }

        public IDataReader Years()
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "Chrome.GetYears";

                    var set = new DataSet();

                    using (IDataReader reader = command.ExecuteReader())
                    {                        
                            var table = new DataTable("Years");

                            table.Load(reader);

                            set.Tables.Add(table);
                    }

                    return set.CreateDataReader();
                }
            }
        }

        public IDataReader Makes(int year)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "Chrome.GetMakesByYear";

                    Database.AddWithValue(command, "Year", year, DbType.Int32);

                    var set = new DataSet();

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        var table = new DataTable("Makes");

                        table.Load(reader);

                        set.Tables.Add(table);
                    }

                    return set.CreateDataReader();
                }
            }
        }

        public IDataReader Models(int year, int makeId)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "Chrome.GetModelsByYearMake";

                    Database.AddWithValue(command, "Year", year, DbType.Int32);

                    Database.AddWithValue(command, "DivisionID", makeId, DbType.Int32);

                    var set = new DataSet();

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        var table = new DataTable("Models");

                        table.Load(reader);

                        set.Tables.Add(table);
                    }

                    return set.CreateDataReader();
                }
            }
        }

        public IDataReader Styles(int year, int makeId, int modelId)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "Chrome.GetStylesByYearMakeModel";

                    Database.AddWithValue(command, "Year", year, DbType.Int32);

                    Database.AddWithValue(command, "DivisionID", makeId, DbType.Int32);

                    Database.AddWithValue(command, "ModelID", modelId, DbType.Int32);

                    var set = new DataSet();

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        var table = new DataTable("Styles");

                        table.Load(reader);

                        set.Tables.Add(table);
                    }

                    return set.CreateDataReader();
                }
            }
        }


        /// <summary>
        /// Get the type of the current assembly.
        /// </summary>
        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        protected override string DatabaseName
        {
            get { return "VehicleCatalog"; }
        }
    }
}