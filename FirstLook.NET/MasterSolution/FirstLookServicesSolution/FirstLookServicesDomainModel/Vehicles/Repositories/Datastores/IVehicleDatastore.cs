﻿using System.Data;

namespace FirstLook.FirstLookServices.DomainModel.Vehicles.Repositories.Datastores
{
    public interface IVehicleDatastore
    {
        IDataReader VehicleHierarchies();

        IDataReader Years();

        IDataReader Makes(int year);

        IDataReader Models(int year, int makeId);

        IDataReader Styles(int year, int makeId, int modelId);

    }
}
