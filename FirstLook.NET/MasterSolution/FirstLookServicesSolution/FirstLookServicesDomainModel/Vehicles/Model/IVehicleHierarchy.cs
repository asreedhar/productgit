﻿namespace FirstLook.FirstLookServices.DomainModel.Vehicles.Model
{
    public interface IVehicleHierarchy
    {
        int? Year { get; }

        string Make { get; }

        string Model { get; }

        string Style { get; }

        int? StyleId { get; }
    }
}
