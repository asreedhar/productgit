﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Vehicles.Model
{
    [Serializable]
    public class Model
    {
        private int _id;

        private string _desc;

        public int Id
        {
            get { return _id; }
            internal set { _id = value; }
        }

        public string Desc
        {
            get { return _desc; }
            internal set { _desc = value; }
        }
    }
}
