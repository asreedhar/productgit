﻿using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.Vehicles.Model
{
    public interface IRepository
    {
        IList<IVehicleHierarchy> VehicleHierarchies();

        IList<int> Years();

        IList<Make> Makes(int year);

        IList<Model> Models(int year, int makeId);

        IList<Style> Styles(int year, int makeId, int modelId);

    }
}
