﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Vehicles.Model
{
    [Serializable]
    public class VehicleHierarchy : IVehicleHierarchy
    {
        private int? _year;

        private string _make;

        private string _model;

        private string _style;

        private int? _styleId;

        public int? Year
        {
            get { return _year; }
            internal set { _year = value; }
        }

        public string Make
        {
            get { return _make; }
            internal set { _make = value; }
        }

        public string Model
        {
            get { return _model; }
            internal set { _model = value; }
        }

        public string Style
        {
            get { return _style; }
            internal set { _style = value; }
        }

        public int? StyleId
        {
            get { return _styleId; }
            internal set { _styleId = value; }
        }


    }
}
