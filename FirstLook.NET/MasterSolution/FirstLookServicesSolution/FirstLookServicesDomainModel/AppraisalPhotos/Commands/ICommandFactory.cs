﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.TransferObjects.Envelopes;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands
{
    public interface ICommandFactory
    {
        ICommand<PostAppraisalPhotosResultsDto, IdentityContextDto<PostAppraisalPhotosArgumentsDto>> CreatePostAppraisalPhotosCommand();

        ICommand<AppraisalCopyPhotosResultsDto, IdentityContextDto<AppraisalCopyPhotosArgumentsDto>>
            CreateCopyAppraisalPhotos();

        ICommand<GetAppraisalPhotosResultsDto, IdentityContextDto<GetAppraisalPhotosArgumentsDto>> CreateGetAppraisalPhotosCommand();

        ICommand<DeleteAppraisalPhotosResultsDto, IdentityContextDto<DeleteAppraisalPhotosArgumentsDto>> CreateDeleteAppraisalPhoto();

        ICommand<List<FirstlookPhotoDetailsDto>, FirstlookPhotoDetailsArgumentsDto> CreateGetKeyFromAmazon();
    }
}
