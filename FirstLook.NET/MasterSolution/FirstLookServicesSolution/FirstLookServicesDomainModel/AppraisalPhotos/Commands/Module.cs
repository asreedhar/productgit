﻿using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.Impl;

namespace FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands
{
    public class Module:IModule
    {
        #region IModule Members

        public void Configure(IRegistry registry)
        {
            registry.Register<ICommandFactory, CommandFactory>(ImplementationScope.Shared);
        }

        #endregion
    }
}
