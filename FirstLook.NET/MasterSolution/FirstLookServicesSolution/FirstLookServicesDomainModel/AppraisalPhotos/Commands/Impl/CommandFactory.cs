﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.Impl;
using FirstLook.Common.Core.Command;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.Impl
{
    public class CommandFactory:ICommandFactory
    {

        #region ICommandFactory Members

        public ICommand<PostAppraisalPhotosResultsDto, IdentityContextDto<PostAppraisalPhotosArgumentsDto>> CreatePostAppraisalPhotosCommand()
        {
            return new SaveAndPostAppraisalPhotos();
        }

      
        public ICommand<AppraisalCopyPhotosResultsDto, IdentityContextDto<AppraisalCopyPhotosArgumentsDto>> CreateCopyAppraisalPhotos()
        {
            return new CopyAppraisalPhotos();
        }


        public ICommand<GetAppraisalPhotosResultsDto, IdentityContextDto<GetAppraisalPhotosArgumentsDto>> CreateGetAppraisalPhotosCommand()
        {
            return new GetAppraisalPhotos();
        }

        public ICommand<DeleteAppraisalPhotosResultsDto, IdentityContextDto<DeleteAppraisalPhotosArgumentsDto>> CreateDeleteAppraisalPhoto()
        {
            return new DeleteAppraisalPhoto();
        }
        public ICommand<List<FirstlookPhotoDetailsDto>, FirstlookPhotoDetailsArgumentsDto> CreateGetKeyFromAmazon()
        {
            return new GetKeyFromAmazon();
        }
        #endregion


    }
}
