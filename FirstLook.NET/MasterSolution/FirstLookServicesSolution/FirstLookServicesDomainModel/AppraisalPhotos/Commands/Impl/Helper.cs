﻿using System;
using System.Configuration;
using System.IO;

namespace FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.Impl
{
    public static class Helper
    {
        public static string GetKeyForAppraisalPhoto(string uniqueID, string dealerId, string source, string sequenceId, string fileExtension)
        {
            return string.Format(Convert.ToString((!String.IsNullOrEmpty(fileExtension) ? ConfigurationManager.AppSettings["AppraisalPhotoFileNameFormat"] : ConfigurationManager.AppSettings["AppraisalPhotoFileNameFormat"].Replace(".", ""))), uniqueID, dealerId, source, sequenceId, fileExtension);
        }
        public static string GetPrefix(string uniqueID, string dealerId, string source, string sequenceId)
        {
            string prefix=string.Format(Convert.ToString(ConfigurationManager.AppSettings["AppraisalPhotoFileNameFormat"]).Replace(".",""), uniqueID, dealerId, source, sequenceId,"");
           return prefix = string.Join("_",prefix.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries));
           
        }
        public static string GetKeyForAppraisalThumbnail(string uniqueID, string dealerId, string source, string sequenceId, string fileExtension)
        {
            return string.Format(ConfigurationManager.AppSettings["AppraisalPhotoThumbnailFormat"], uniqueID, dealerId, source, sequenceId, fileExtension);
        }
        public static int GetSourceIndex(string key)
        {
            int index = GetIndex("{2}");
            string[] keyBreakdown = key.Split('_', '.');
            return Convert.ToInt32(keyBreakdown[index]);
            
        }
        public static int GetSequenceIndex(string key)
        {
            int index = GetIndex("{3}");
            string[] keyBreakdown = key.Split('_', '.');
            return Convert.ToInt32(keyBreakdown[index]);

        }
        private static int GetIndex(string position)
        {
            string[] keyFormat = ConfigurationManager.AppSettings["AppraisalPhotoFileNameFormat"].Split('_');
            int index = 0;
            for (int i = 0; i < keyFormat.Length; i++)
            {
                if (keyFormat[i].Contains(position))
                {
                    index = i;
                }
            }
            return index;
        }

        public static string GetKeyForAppraisalPhoto(string uniqueID, string dealerId, string source, string sequenceId)
        {
            return string.Format(ConfigurationManager.AppSettings["AppraisalPhotoFileNameFormat"].Replace(".", ""), uniqueID, dealerId, source, sequenceId,"");
        }
        public static string GetKeyForAppraisalThumbnail(string uniqueID, string dealerId, string source, string sequenceId)
        {
            return string.Format(ConfigurationManager.AppSettings["AppraisalPhotoThumbnailFormat"].Replace(".", ""), uniqueID, dealerId, source, sequenceId,"");
        }

        public static string GetNameWithoutExtension(string fileName)
        {
            return Path.GetFileNameWithoutExtension(fileName);
        }
    }
}
