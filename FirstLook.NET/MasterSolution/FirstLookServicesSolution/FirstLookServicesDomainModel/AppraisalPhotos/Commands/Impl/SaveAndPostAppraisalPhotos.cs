﻿using System;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.FirstLookServices.DomainModel.Validation;


namespace FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.Impl
{
    public class SaveAndPostAppraisalPhotos : ICommand<PostAppraisalPhotosResultsDto, IdentityContextDto<PostAppraisalPhotosArgumentsDto>>
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region ICommand<PostAppraisalPhotosResultsDto,IdentityContextDto<PostAppraisalPhotosArgumentsDto>> Members

        public PostAppraisalPhotosResultsDto Execute(IdentityContextDto<PostAppraisalPhotosArgumentsDto> parameters)
        {
            Image image = null;
            string awskey = null;
            string awsThumbkey = null;
            Validators.CheckDealerId(parameters.Arguments.DealerID, parameters);

            if (String.IsNullOrEmpty(parameters.Arguments.Photos))
            {
                throw new InvalidInputException("photo", parameters.Arguments.Photos, "No photo supplied");
            }

            if (parameters.Arguments.Source!=(int)AppraisalSourceDto.Mobile && parameters.Arguments.Source!=(int)AppraisalSourceDto.The_Edge)
            {
                throw new InvalidInputException("Source", parameters.Arguments.Source.ToString(), "Invalid source Id. Must be Mobile or The Edge");
            }

            if (string.IsNullOrEmpty(parameters.Arguments.UniqueID))
            {
                parameters.Arguments.UniqueID = Guid.NewGuid().ToString();
            }

            byte[] bytes = new byte[parameters.Arguments.Photos.Length];
            bytes = Convert.FromBase64String(parameters.Arguments.Photos);
            ImageFormat imgFormat = ImageFormat.Jpeg;

            string extension = Path.GetExtension(parameters.Arguments.FileName).Trim(new Char[] {'.'});
            if (!string.IsNullOrEmpty(extension))
            {
                switch (extension.ToLower())
                {
                    case @"bmp":
                        imgFormat = ImageFormat.Bmp;
                        break;
                    case @"gif":
                        imgFormat = ImageFormat.Gif;
                        break;
                    case @"ico":
                        imgFormat = ImageFormat.Icon;
                        break;
                    case @"jpg":
                    case @"jpeg":
                        imgFormat = ImageFormat.Jpeg;
                        break;
                    case @"png":
                        imgFormat = ImageFormat.Png;
                        break;
                    case @"tif":
                    case @"tiff":
                        imgFormat = ImageFormat.Tiff;
                        break;
                    case @"wmf":
                        imgFormat = ImageFormat.Wmf;
                        break;
                }
            }
            else
            {
                extension = "jpg";
            }
            awskey = Helper.GetKeyForAppraisalPhoto(parameters.Arguments.UniqueID, parameters.Arguments.DealerID.ToString(), parameters.Arguments.Source.ToString(), parameters.Arguments.SequenceID.ToString(), extension);

            string bucketName = ConfigurationManager.AppSettings["AppraisalPhotoBucket"];

            string photoUrl = "";
            string thumbnailUrl = "";
           
            double awsExpirationMins = Convert.ToDouble(ConfigurationManager.AppSettings["AWSPreAuthenticateExpirationTimeInMinutes"]);
            int imgHeight = 0;
            int imgWidth = 0;
            try
            {
                //Save Photo
                using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.USEast1))
                {
                    using (var streamBitmap = new MemoryStream(bytes))
                    {
                        using (image = Image.FromStream(streamBitmap))
                        {
                            var stream = new MemoryStream();
                            image.Save(stream, imgFormat);
                            stream.Position = 0;

                            PutObjectRequest request = new PutObjectRequest();
                            request.InputStream = stream;
                            request.BucketName = bucketName;
                            request.CannedACL = S3CannedACL.PublicRead;
                            request.Key = awskey;
                            PutObjectResponse response = s3Client.PutObject(request);
                            imgHeight = image.Height;
                            imgWidth = image.Width;
                        }
                    }
                    
                    GetPreSignedUrlRequest request1 = new GetPreSignedUrlRequest()
                    {
                        BucketName = bucketName,
                        Key = awskey,
                        Expires = DateTime.Now.AddMinutes(awsExpirationMins)
                    };

                    photoUrl = s3Client.GetPreSignedURL(request1);

                }

                //Save Thumbnail
                awsThumbkey = Helper.GetKeyForAppraisalThumbnail(parameters.Arguments.UniqueID, parameters.Arguments.DealerID.ToString(), parameters.Arguments.Source.ToString(), parameters.Arguments.SequenceID.ToString(), extension);
                    
                int width = Convert.ToInt32(ConfigurationManager.AppSettings["ThumbnailWidth"]);
                //Height is commented to maintain Image aspect ration with width.
                //int height = Convert.ToInt32(ConfigurationManager.AppSettings["ThumbnailHeight"]);


                using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.USEast1))
                {
                    using (var streamBitmap = new MemoryStream(bytes))
                    {
                        using (image = Image.FromStream(streamBitmap))
                        {
                            //Maintain Aspect ration.
                            int X = imgWidth;
                            int Y = imgHeight;
                            int height = (int)((width * Y) / X);

                            var stream = new MemoryStream();
                            Image thumb = image.GetThumbnailImage(width, height, () => false, IntPtr.Zero);
                            thumb.Save(stream, imgFormat);
                            stream.Position = 0;

                            PutObjectRequest request = new PutObjectRequest();
                            request.InputStream = stream;
                            request.BucketName = bucketName;
                            request.CannedACL = S3CannedACL.PublicRead;
                            request.Key = awsThumbkey;
                            PutObjectResponse response = s3Client.PutObject(request);
                        }
                    }
                  
                    GetPreSignedUrlRequest request1 = new GetPreSignedUrlRequest()
                    {
                        BucketName = bucketName,
                        Key = awsThumbkey,
                        Expires = DateTime.Now.AddMinutes(awsExpirationMins)
                    };

                    thumbnailUrl = s3Client.GetPreSignedURL(request1);
                }


            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") ||
                     amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    Log.Error("Error Writing Appraisal photo to S3 Bucket - " + bucketName + ":  Incorrect AWS Credentials");
                    throw new UserAccessException("AWS", "Forbidden", "Incorrect AWS Credentials.");
                }
                else
                {
                    Log.Error("Exception Writing Appraisal photo to S3 Bucket - " + bucketName, amazonS3Exception);
                    throw new FirstlookGeneralException("Errorcode " + amazonS3Exception.ErrorCode +
                                                        " when writing to S3 Bucket: " + bucketName +
                                                        " - Error message = " + amazonS3Exception.Message);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Exception Writing Appraisal photo to S3 Bucket - " + bucketName, ex);
                throw new FirstlookGeneralException("Errorcode " +
                                                    " when writing to S3 Bucket: " + bucketName +
                                                    " - Error message = " + ex.Message);
            }

            return new PostAppraisalPhotosResultsDto() { UniqueID = parameters.Arguments.UniqueID, ThumbnailUrl = thumbnailUrl, PhotoUrl = photoUrl, PhotoKey = awskey,ThumbnailKey = awsThumbkey};
        }

        #endregion
    }
}
