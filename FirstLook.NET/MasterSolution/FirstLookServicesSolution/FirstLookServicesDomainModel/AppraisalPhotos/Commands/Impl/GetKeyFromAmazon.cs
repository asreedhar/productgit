﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Exceptions;

namespace FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.Impl
{
    public class GetKeyFromAmazon : ICommand<List<FirstlookPhotoDetailsDto>, FirstlookPhotoDetailsArgumentsDto>
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<FirstlookPhotoDetailsDto> Execute(FirstlookPhotoDetailsArgumentsDto parameters)
        {
            List<FirstlookPhotoDetailsDto> results = new List<FirstlookPhotoDetailsDto>();
            List<string> keys = new List<string>();
           
            string bucketName = ConfigurationManager.AppSettings["AppraisalPhotoBucket"];
            int maxPhotoLength = Convert.ToInt32(ConfigurationManager.AppSettings["maxPhotoLength"]);
           
            try
            {
                using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.USEast1))
                {
                    ListObjectsRequest request = new ListObjectsRequest
                    {
                        BucketName = bucketName,
                        Prefix = parameters.prefix,
                        MaxKeys = maxPhotoLength
                    };

                    do
                    {
                        ListObjectsResponse response = s3Client.ListObjects(request);
                       
                        foreach (S3Object entry in response.S3Objects)
                        {
                            keys.Add(entry.Key);
                        }
                        if (response.IsTruncated)
                        {
                            request.Marker = response.NextMarker;
                        }
                        else
                        {
                            request = null;
                        }
                    } while (request != null);

                    //Sorting will position Thumbnail key after photo key
                    keys.Sort();

                    for (int i = 0; i < keys.Count; i += 2)
                    {
                        results.Add(new FirstlookPhotoDetailsDto() { photoKey = keys[i], thumbNailKey = keys[i + 1], sequenceNo =Helper.GetSequenceIndex(keys[i])});
                    }
                }


            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null && (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") || amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    Log.Error("Error Getting Appraisal photos from S3 Bucket - " + bucketName + ":  Incorrect AWS Credentials");
                    throw new UserAccessException("AWS", "Forbidden", "Incorrect AWS Credentials.");
                }
                else
                {
                    Log.Error("Exception Getting Appraisal photos from S3 Bucket - " + bucketName, amazonS3Exception);
                    throw new FirstlookGeneralException("Errorcode " + amazonS3Exception.ErrorCode + " when writing to S3 Bucket: " + bucketName + " - Error message = " + amazonS3Exception.Message);
                }
            }
            return results;
        }
    }
}

