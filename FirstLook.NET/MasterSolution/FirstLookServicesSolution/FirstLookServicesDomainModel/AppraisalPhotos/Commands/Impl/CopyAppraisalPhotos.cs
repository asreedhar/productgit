﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Model;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.FirstLookServices.DomainModel.Validation;
using FirstLook.Common.Core.Registry;
using System.IO;

namespace FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.Impl
{
    public class CopyAppraisalPhotos : ICommand<AppraisalCopyPhotosResultsDto, IdentityContextDto<AppraisalCopyPhotosArgumentsDto>>
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #region ICommand<CopyAppraisalPhotosResultsDto,IdentityContextDto<CopyAppraisalPhotosArgumentsDto>> Members

        public AppraisalCopyPhotosResultsDto Execute(IdentityContextDto<AppraisalCopyPhotosArgumentsDto> parameters)
        {
            Validators.CheckDealerId(parameters.Arguments.DealerID, parameters);

            if (String.IsNullOrEmpty(parameters.Arguments.NewUniqueID))
            {
                throw new InvalidInputException("New Unique ID", parameters.Arguments.NewUniqueID, "New unique ID supplied");
            }

            if (String.IsNullOrEmpty(parameters.Arguments.UniqueID))
            {
                throw new InvalidInputException("Old Unique ID", parameters.Arguments.UniqueID, "Old unique ID supplied");
            }

            if (parameters.Arguments.Source != (int)AppraisalSourceDto.Mobile && parameters.Arguments.Source != (int)AppraisalSourceDto.The_Edge)
            {
                throw new InvalidInputException("Source", parameters.Arguments.Source.ToString(), "Invalid source Id. Must be Mobile or The Edge");
            }

            string bucketName = ConfigurationManager.AppSettings["AppraisalPhotoBucket"];
            int maxPhotoLength = Convert.ToInt32(ConfigurationManager.AppSettings["maxPhotoLength"]);

            List<string> appraisalPhotokeyList = new List<string>();
            List<AppraisalPhoto> appraisalPhotos = new List<AppraisalPhoto>();
            try
            {
                var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
                var command = factory.CreateGetKeyFromAmazon();

                string prefix = Helper.GetPrefix(parameters.Arguments.UniqueID, parameters.Arguments.DealerID.ToString(), "", "");
                FirstlookPhotoDetailsArgumentsDto firstlookPhotoDetailsArgumentsDto = new FirstlookPhotoDetailsArgumentsDto() { prefix = prefix };
                List<FirstlookPhotoDetailsDto> firstLooksPhotoDetails = command.Execute(firstlookPhotoDetailsArgumentsDto);

               

                using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.USEast1))
                {

                    foreach (FirstlookPhotoDetailsDto firstlookPhotoDetailsDto in firstLooksPhotoDetails)
                    {
                        string destinationkey = "";
                        destinationkey = Helper.GetKeyForAppraisalPhoto(parameters.Arguments.NewUniqueID, parameters.Arguments.DealerID.ToString(), Helper.GetSourceIndex(firstlookPhotoDetailsDto.photoKey).ToString(), firstlookPhotoDetailsDto.sequenceNo.ToString(), Path.GetExtension(firstlookPhotoDetailsDto.photoKey).Trim(new Char[] { '.' }));

                        //Copy Photo
                        CopyObjectRequest copyRequest = new CopyObjectRequest
                        {
                            SourceBucket = bucketName,
                            SourceKey = firstlookPhotoDetailsDto.photoKey,
                            DestinationBucket = bucketName,
                            DestinationKey = destinationkey
                        };
                        CopyObjectResponse copyResponse = s3Client.CopyObject(copyRequest);
                        appraisalPhotokeyList.Add(destinationkey);

                        //Copy Thumbnail
                        destinationkey = Helper.GetKeyForAppraisalThumbnail(parameters.Arguments.NewUniqueID, parameters.Arguments.DealerID.ToString(), Helper.GetSourceIndex(firstlookPhotoDetailsDto.photoKey).ToString(), firstlookPhotoDetailsDto.sequenceNo.ToString(), Path.GetExtension(firstlookPhotoDetailsDto.photoKey).Trim(new Char[] { '.' }));

                        copyRequest = new CopyObjectRequest
                        {
                            SourceBucket = bucketName,
                            SourceKey = firstlookPhotoDetailsDto.thumbNailKey,
                            DestinationBucket = bucketName,
                            DestinationKey = destinationkey
                        };
                        copyResponse = s3Client.CopyObject(copyRequest);
                        appraisalPhotokeyList.Add(destinationkey);

                        appraisalPhotokeyList.Sort();
                        


                        for (int i = 0; i < appraisalPhotokeyList.Count; i += 2)
                        {
                            String[] keybreakdown = appraisalPhotokeyList[i].Split('_', '.');
                            appraisalPhotos.Add(new AppraisalPhoto() { photoURl = appraisalPhotokeyList[i], thumbUrl = appraisalPhotokeyList[i + 1], sequenceNo = firstlookPhotoDetailsDto.sequenceNo });
                        }
                        appraisalPhotokeyList.Clear();
                    }
                }
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null && (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") || amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    Log.Error("Error copying Appraisal photos from S3 Bucket - " + bucketName + ":  Incorrect AWS Credentials");
                    throw new UserAccessException("AWS", "Forbidden", "Incorrect AWS Credentials.");
                }
                else
                {
                    Log.Error("Exception copying Appraisal photos from S3 Bucket - " + bucketName, amazonS3Exception);
                    throw new FirstlookGeneralException("Errorcode " + amazonS3Exception.ErrorCode + " when writing to S3 Bucket: " + bucketName + " - Error message = " + amazonS3Exception.Message);
                }
            }
            return new AppraisalCopyPhotosResultsDto() { AppraisalPhotos = appraisalPhotos };

        }

        #endregion
    }
}
