﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Model;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.FirstLookServices.DomainModel.Validation;

namespace FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.Impl
{
    public class DeleteAppraisalPhoto : ICommand<DeleteAppraisalPhotosResultsDto, IdentityContextDto<DeleteAppraisalPhotosArgumentsDto>>
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public DeleteAppraisalPhotosResultsDto Execute(IdentityContextDto<DeleteAppraisalPhotosArgumentsDto> parameters)
        {
            
            string bucketName = ConfigurationManager.AppSettings["AppraisalPhotoBucket"];
            
            if (String.IsNullOrEmpty(parameters.Arguments.UniqueID))
            {
                throw new InvalidInputException("Unique ID", parameters.Arguments.UniqueID, "New unique ID supplied");
            }

            if (parameters.Arguments.Source != (int)AppraisalSourceDto.Mobile && parameters.Arguments.Source != (int)AppraisalSourceDto.The_Edge)
            {
                throw new InvalidInputException("Source", parameters.Arguments.Source.ToString(), "Invalid source Id. Must be Mobile or The Edge");
            }

            Validators.CheckDealerId(parameters.Arguments.DealerID, parameters);
            string prefix = Helper.GetPrefix(parameters.Arguments.UniqueID, parameters.Arguments.DealerID.ToString(), parameters.Arguments.Source.ToString(), parameters.Arguments.SequenceId.ToString());
           
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            var command = factory.CreateGetKeyFromAmazon();
            FirstlookPhotoDetailsArgumentsDto firstlookPhotoDetailsArgumentsDto = new FirstlookPhotoDetailsArgumentsDto() { prefix = prefix };
            List<FirstlookPhotoDetailsDto> firstLooksPhotoDetails = command.Execute(firstlookPhotoDetailsArgumentsDto);
            
            using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.USEast1))
            {
                if (firstLooksPhotoDetails != null && firstLooksPhotoDetails.Count > 0)
                {
                    DeleteObjectsRequest multiObjectDeleteRequest = new DeleteObjectsRequest();
                    multiObjectDeleteRequest.BucketName = bucketName;
    
                    foreach (FirstlookPhotoDetailsDto firstlookPhotoDetailsDto in firstLooksPhotoDetails)
                    {
                        if (firstlookPhotoDetailsDto.photoKey != null && firstlookPhotoDetailsDto.photoKey.Trim() != "" && Helper.GetNameWithoutExtension(firstlookPhotoDetailsDto.photoKey) == Helper.GetKeyForAppraisalPhoto(parameters.Arguments.UniqueID, parameters.Arguments.DealerID.ToString(), parameters.Arguments.Source.ToString(), parameters.Arguments.SequenceId.ToString()))
                            multiObjectDeleteRequest.AddKey(firstlookPhotoDetailsDto.photoKey);

                        if (firstlookPhotoDetailsDto.thumbNailKey != null && firstlookPhotoDetailsDto.thumbNailKey.Trim() != "" && Helper.GetNameWithoutExtension(firstlookPhotoDetailsDto.thumbNailKey) == Helper.GetKeyForAppraisalThumbnail(parameters.Arguments.UniqueID, parameters.Arguments.DealerID.ToString(), parameters.Arguments.Source.ToString(), parameters.Arguments.SequenceId.ToString()))
                            multiObjectDeleteRequest.AddKey(firstlookPhotoDetailsDto.thumbNailKey);
                    }

                    try
                    {
                        DeleteObjectsResponse response = s3Client.DeleteObjects(multiObjectDeleteRequest);
                    }
                    catch (DeleteObjectsException deleteObjectException)
                    {
                        Log.Error("Exception deleting Appraisal photos from S3 Bucket - " + bucketName, deleteObjectException);
                        throw new FirstlookGeneralException("Errorcode " + deleteObjectException.ErrorCode + " when writing to S3 Bucket: " + bucketName + " - Error message = " + deleteObjectException.Message);
                    }
                    catch (AmazonS3Exception amazonS3Exception)
                    {
                        if (amazonS3Exception.ErrorCode != null && (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") || amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                        {
                            Log.Error("Error deleting Appraisal photos from S3 Bucket - " + bucketName + ":  Incorrect AWS Credentials");
                            throw new UserAccessException("AWS", "Forbidden", "Incorrect AWS Credentials.");
                        }
                        else
                        {
                            Log.Error("Exception deleting Appraisal photos from S3 Bucket - " + bucketName, amazonS3Exception);
                            throw new FirstlookGeneralException("Errorcode " + amazonS3Exception.ErrorCode + " when writing to S3 Bucket: " + bucketName + " - Error message = " + amazonS3Exception.Message);
                        }
                    }
                }
                else
                {
                    throw new InvalidInputException("Invalid input: File not found");
                }

            }
            List<AppraisalPhoto> deletedPhotos = new List<AppraisalPhoto>();
            
            firstLooksPhotoDetails.ForEach(flp=> deletedPhotos.Add(new AppraisalPhoto(){ 
                photoURl=flp.photoKey,
                thumbUrl=flp.thumbNailKey,
                sequenceNo=flp.sequenceNo
            }));

            return new DeleteAppraisalPhotosResultsDto() { DeletedPhotos = deletedPhotos, DeletedCount = deletedPhotos.Count};
        }
    }
}
