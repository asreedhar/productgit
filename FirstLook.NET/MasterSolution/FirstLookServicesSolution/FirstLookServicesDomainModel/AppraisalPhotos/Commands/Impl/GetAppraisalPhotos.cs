﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using FirstLook.Common.Core.Registry;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Model;
using FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.TransferObjects;
using System.Linq;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.FirstLookServices.DomainModel.Validation;

namespace FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.Impl
{
    public class GetAppraisalPhotos : ICommand<GetAppraisalPhotosResultsDto, IdentityContextDto<GetAppraisalPhotosArgumentsDto>>
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public GetAppraisalPhotosResultsDto Execute(IdentityContextDto<GetAppraisalPhotosArgumentsDto> parameters)
        {
            // string photoUrl = null;

            string bucketName = ConfigurationManager.AppSettings["AppraisalPhotoBucket"];
            int maxPhotoLength = Convert.ToInt32(ConfigurationManager.AppSettings["maxPhotoLength"]);
            double awsExpirationMins = Convert.ToDouble(ConfigurationManager.AppSettings["AWSPreAuthenticateExpirationTimeInMinutes"]);

            if (String.IsNullOrEmpty(parameters.Arguments.UniqueID))
            {
                throw new InvalidInputException("Unique ID", parameters.Arguments.UniqueID, "New unique ID supplied");
            }

            if (parameters.Arguments.Source != (int)AppraisalSourceDto.Mobile && parameters.Arguments.Source != (int)AppraisalSourceDto.The_Edge)
            {
                throw new InvalidInputException("Source", parameters.Arguments.Source.ToString(), "Invalid source Id. Must be Mobile or The Edge");
            }

            Validators.CheckDealerId(parameters.Arguments.DealerID, parameters);

            Dictionary<string, string> appraisalPhotokeyList = new Dictionary<string, string>();
            List<AppraisalPhoto> appraisalPhotos = new List<AppraisalPhoto>();
            
            //If source is mobile then only show photos which are uploaded from mobile. This is determined by sourceId.
            //The objects are returned by prefix, for mobile we are passsing source ID
            string prefix = parameters.Arguments.Source == (int)AppraisalSourceDto.Mobile ? Helper.GetPrefix(parameters.Arguments.UniqueID, parameters.Arguments.DealerID.ToString(), ((int)AppraisalSourceDto.Mobile).ToString(),"") : Helper.GetPrefix(parameters.Arguments.UniqueID, parameters.Arguments.DealerID.ToString(),"","");
            FirstlookPhotoDetailsArgumentsDto firstlookPhotoDetailsArgumentsDto = new FirstlookPhotoDetailsArgumentsDto() { prefix = prefix };
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            var command = factory.CreateGetKeyFromAmazon();
            List<FirstlookPhotoDetailsDto> firstLooksPhotoDetails = command.Execute(firstlookPhotoDetailsArgumentsDto);
            
            try
            {
                using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.USEast1))
                {

                    foreach (FirstlookPhotoDetailsDto firstlookPhotoDetailsDto in firstLooksPhotoDetails)
                    {
                        List<string> urls = new List<string>();
                        GetPreSignedUrlRequest request1 = new GetPreSignedUrlRequest()
                        {
                            BucketName = bucketName,
                            Key = firstlookPhotoDetailsDto.photoKey,
                            Expires = DateTime.Now.AddMinutes(awsExpirationMins)
                        };
                        urls.Add(s3Client.GetPreSignedURL(request1));

                        request1 = new GetPreSignedUrlRequest()
                        {
                            BucketName = bucketName,
                            Key = firstlookPhotoDetailsDto.thumbNailKey,
                            Expires = DateTime.Now.AddMinutes(awsExpirationMins)
                        };
                        urls.Add(s3Client.GetPreSignedURL(request1));

                        appraisalPhotos.Add(new AppraisalPhoto() { photoURl = urls[0], thumbUrl = urls[1], sequenceNo = firstlookPhotoDetailsDto.sequenceNo, source = Helper.GetSourceIndex(firstlookPhotoDetailsDto.photoKey)});
                    }
                }
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null && (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") || amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    Log.Error("Error Getting Appraisal photos from S3 Bucket - " + bucketName + ":  Incorrect AWS Credentials");
                    throw new UserAccessException("AWS", "Forbidden", "Incorrect AWS Credentials.");
                }
                else
                {
                    Log.Error("Exception Getting Appraisal photos from S3 Bucket - " + bucketName, amazonS3Exception);
                    throw new FirstlookGeneralException("Errorcode " + amazonS3Exception.ErrorCode + " when writing to S3 Bucket: " + bucketName + " - Error message = " + amazonS3Exception.Message);
                }
            }
            return new GetAppraisalPhotosResultsDto() { AppraisalPhotos = appraisalPhotos, UniqueID = parameters.Arguments.UniqueID };
        }
    }


}

