﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.TransferObjects
{
    [Serializable]
    public enum AppraisalSourceDto
    {
        Undefined = 0,
        The_Edge = 1,      
        Wavis=2,
        AutoBase=3,
        HigherGear=4,
        DealerSocket=5,
        Reynolds=6,
        Mobile=7
    }
}
