﻿
namespace FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.TransferObjects.Envelopes
{
    public class AppraisalCopyPhotosArgumentsDto : PhotoArgumentDto
    {        
        public string NewUniqueID { get; set; }        
    }
}
