﻿using FirstLook.FirstLookServices.DomainModel.Appraisals.Model;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.TransferObjects.Envelopes
{
    public class DeleteAppraisalPhotosResultsDto
    {
        public List<AppraisalPhoto> DeletedPhotos { get; set; }
        public int DeletedCount { get; set; }
    }
}
