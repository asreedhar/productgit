﻿
namespace FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.TransferObjects.Envelopes
{
    public class PostAppraisalPhotosResultsDto
    {
        public string PhotoUrl { get; set; }
        public string ThumbnailUrl { get; set; }
        public string UniqueID { get; set; }
        public string PhotoKey { get; set; }
        public string ThumbnailKey { get; set; }
    }
}
