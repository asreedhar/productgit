﻿
namespace FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.TransferObjects.Envelopes
{
    public class PostAppraisalPhotosArgumentsDto : PhotoArgumentDto
    {
        public string Photos { get; set; }
        public int SequenceID { get; set; }
        public string FileName { get; set; }
    }
}
