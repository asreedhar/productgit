﻿using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Model;

namespace FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.TransferObjects.Envelopes
{
    public class GetAppraisalPhotosResultsDto
    {
        public List<AppraisalPhoto> AppraisalPhotos { get; set; }
        public string UniqueID { get; set; }
    }
}
