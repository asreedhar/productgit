﻿
namespace FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.TransferObjects.Envelopes
{
    public class PhotoArgumentDto
    {
        public int DealerID { get; set; }
        public string UniqueID { get; set; }        
        public int Source { get; set; }
    }
}
