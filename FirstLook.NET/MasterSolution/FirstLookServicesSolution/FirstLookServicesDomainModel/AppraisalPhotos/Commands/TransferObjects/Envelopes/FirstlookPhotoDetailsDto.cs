﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.TransferObjects.Envelopes
{
    public class FirstlookPhotoDetailsDto
    {
        public string photoKey { get; set; }
        public string thumbNailKey { get; set; }
        public int sequenceNo { get; set; }
    }
}
