﻿
using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Model;

namespace FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.TransferObjects.Envelopes
{
    public class AppraisalCopyPhotosResultsDto
    {
        public List<AppraisalPhoto> AppraisalPhotos { get; set; }
    }
}
