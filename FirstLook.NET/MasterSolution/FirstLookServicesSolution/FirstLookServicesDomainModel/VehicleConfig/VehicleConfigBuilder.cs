﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.FirstLookServices.DomainModel.BookMapping;
using FirstLook.FirstLookServices.DomainModel.BookMapping.Internal;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.FirstLookServices.DomainModel.Utility;
using FirstLook.FirstLookServices.DomainModel.VehicleConfig.Internal;
using FirstLook.FirstLookServices.DomainModel.VehicleConfig.Internal.BookBuilders;

namespace FirstLook.FirstLookServices.DomainModel.VehicleConfig
{
    public class VehicleConfigException : ApplicationException
    {
        public VehicleConfigException(string message) : base(message) { }
    }

    public class VehicleConfigBuilder
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly KbbBuilder _kbbBuilder;
        private readonly NadaBuilder _nadaBuilder;
        private readonly GalvesBuilder _galvesBuilder;
        private readonly BlackBookBuilder _blackBookBuilder;

        public VehicleConfigBuilder(int dealerId)
        {
            // Initialize and store builders.
            _kbbBuilder = new KbbBuilder();
            _nadaBuilder = new NadaBuilder(dealerId);
            _galvesBuilder = new GalvesBuilder();
            _blackBookBuilder = new BlackBookBuilder();
        }

        private IVehicleConfigBuilder GetBuilder(IBook book)
        {
            switch (book.Id)
            {
                case BookId.Kbb:
                    return _kbbBuilder;
                case BookId.Nada:
                    return _nadaBuilder;
                case BookId.Galves:
                    return _galvesBuilder;
                case BookId.BlackBook:
                    return _blackBookBuilder;
            }
            string message = string.Format("The specified book {0} is not supported.", book.Desc);
            Log.Error(message);
            throw new VehicleConfigException(message);
        }

        /// <summary>
        /// Build the vehicle config by the specified vin and book.
        /// </summary>
        /// <param name="vin">A VIN</param>
        /// <param name="book">One of the books defined in the BookMapping.BookNames class.</param>
        /// <returns></returns>
        public IEnumerable<IVehicleConfig> Build(string vin, IBook book)
        {
            // validate the vin
            if (String.IsNullOrWhiteSpace(vin) || (vin.Length != 9 && vin.Length != 17))
            {
                Exception ex = new InvalidInputException("vin", vin,"The specified vin has bad length.");
                throw ex;
            }
            // use the book mapper to identify the book trims for which we should build configs.
            var records = GetMappedBookRecords(vin, book);

            // build the configs
            return BuildFromBookRecords(records, book);
        }

        /// <summary>
        /// Build the vehicle config by the specified style and book.
        /// </summary>
        /// <param name="style">A chrome style id.</param>
        /// <param name="book">One of the books defined in the BookMapping.BookNames class.</param>
        /// <returns></returns>
        public IEnumerable<IVehicleConfig> Build(int style, IBook book)
        {
            // use the book mapper to identify the book trims for which we should build configs.
            List<BookRecord> records = GetMappedBookRecords(style, book);
            

            // build the configs.
            return BuildFromBookRecords(records, book);
        }

        #region VINSpecificOptions
        
        public IEnumerable<IVehicleConfig> Build(string vin, IBook book,int dealerId)
        {
            // validate the vin
            if (String.IsNullOrWhiteSpace(vin) || (vin.Length != 9 && vin.Length != 17))
            {
                Exception ex = new InvalidInputException("vin", vin, "The specified vin has bad length.");
                throw ex;
            }
            // use the book mapper to identify the book trims for which we should build configs.
            var records = GetMappedBookRecords(vin, book);

            // build the configs
            return BuildFromBookRecords(records, book,dealerId);
        }


        /// <summary>
        /// Build the vehicle config by the specified style and book.
        /// </summary>
        /// <param name="style">A chrome style id.</param>
        /// <param name="book">One of the books defined in the BookMapping.BookNames class.</param>
        /// <param name="dealerId"></param>
        /// <returns></returns>
        public IEnumerable<IVehicleConfig> Build(int style, IBook book,int dealerId)
        {
            // use the book mapper to identify the book trims for which we should build configs.
            List<BookRecord> records = GetMappedBookRecords(style, book);


            // build the configs.
            return BuildFromBookRecords(records, book);
        }

        internal IEnumerable<Internal.VehicleConfig> BuildFromBookRecords(IList<BookRecord> bookRecords, IBook book, int dealerId)
        {
            IVehicleConfigBuilder builder = GetBuilder(book);
            var configs = new List<Internal.VehicleConfig>();
            bool isOneRecord = bookRecords.Count == 1 ? true : false;

            foreach (var bookRecord in bookRecords)
            {
                var vehicleConfigs=builder.CreateConfigs(bookRecord, book).ToList();
                
                // Create the configs based on the book.


                // If we couldn't create a config, stop.
                if (vehicleConfigs.Count == 0)
                {
                    string message =
                        String.Format("Could not create a config for trim {0} and book {1}.",
                                        bookRecord.TrimId, book.Desc);
                    Log.Error(message);
                    throw new VehicleConfigException(message);
                }

                //Originally just the logic in parens right of the or, but needed to account for byTrim searches with only one record
                bool selected = vehicleConfigs.Count == 1 && (isOneRecord || bookRecord.IsDefault);

                // The book may have returned multiple configs.
                foreach (var vehicleConfig in vehicleConfigs)
                {
                    vehicleConfig.Selected = selected;

                    IEnumerable<Equipment> equipments = null;

                    if (builder.GetType() == typeof (KbbBuilder))
                    {
                        equipments = builder.GetEquipment(bookRecord.TrimId, dealerId, bookRecord.Vin);
                    }
                    else
                    {
                        equipments = builder.GetEquipment(bookRecord.TrimId, bookRecord.Vin);
                    }

                    builder.SaveEquipment(vehicleConfig, equipments);

                    // Load the rules, partition them, save them to the config.
                    var rules = builder.GetRules(bookRecord.TrimId);
                    var partitionedRules = GroupEquipmentRules(rules);
                    builder.SaveRules(vehicleConfig, partitionedRules);

                    // add the config to our list
                    configs.Add(vehicleConfig);
                }

            }

            return configs;
        }
        
        #endregion

        internal IEnumerable<Internal.VehicleConfig> BuildFromBookRecords(IList<BookRecord> bookRecords, IBook book)
        {
            var builder = GetBuilder(book);
            var configs = new List<Internal.VehicleConfig>();
            bool isOneRecord = bookRecords.Count == 1 ? true : false;

            foreach (var bookRecord in bookRecords)
            {
                // Create the configs based on the book.
                var vehicleConfigs = builder.CreateConfigs(bookRecord, book).ToList();

                // If we couldn't create a config, stop.
                if (vehicleConfigs.Count == 0)
                {
                    string message =
                        String.Format("Could not create a config for trim {0} and book {1}.",
                                        bookRecord.TrimId, book.Desc);
                    Log.Error(message);
                    throw new VehicleConfigException(message);
                }

                //Originally just the logic in parens right of the or, but needed to account for byTrim searches with only one record
                bool selected = vehicleConfigs.Count == 1 && ( isOneRecord || bookRecord.IsDefault );

                // The book may have returned multiple configs.
                foreach (var vehicleConfig in vehicleConfigs)
                {
                    vehicleConfig.Selected = selected;                        
                   
                    var equipments = builder.GetEquipment(bookRecord.TrimId,bookRecord.Vin);
                    builder.SaveEquipment(vehicleConfig, equipments);
                    
                    // Load the rules, partition them, save them to the config.
                    var rules = builder.GetRules(bookRecord.TrimId);
                    var partitionedRules = GroupEquipmentRules(rules);
                    builder.SaveRules(vehicleConfig, partitionedRules);

                    // add the config to our list
                    configs.Add(vehicleConfig);
                }

            }

            return configs;
        }

       

        #region grouping rules

        private static IEnumerable<EquipmentRule> GroupIncludesRules(IEnumerable<PairwiseEquipmentRule> rules)
        {
            var partitionedRules = new List<EquipmentRule>();
            var groups = rules.Where(r => r.RuleType.Equals(RuleTypes.INCLUDES)).GroupBy(r => r.EquipmentId1);
            foreach (var group in groups)
            {
                // capture the parent option id
                var optionId = @group.First().EquipmentId1;
                var includes = new List<dynamic> { optionId };
                foreach (var rule in @group)
                {
                    includes.Add(rule.EquipmentId2);
                }

                // add the rule
                partitionedRules.Add(new EquipmentRule
                {
                    Equipment = includes.ToArray(),
                    Rule = RuleTypes.INCLUDES
                });
            }

            return partitionedRules;
        }

        private static IEnumerable<EquipmentRule> GroupExcludesRules(IEnumerable<PairwiseEquipmentRule> rules)
        {
            var exGraph = new UndirectedGraph();
            var exRules = rules.Where(r => r.RuleType.Equals(RuleTypes.EXCLUDES));
            foreach (var exRule in exRules)
            {
                exGraph.AddEdge(exRule.EquipmentId1, exRule.EquipmentId2);
            }
            var exSets = exGraph.DisjointSets();
            var partitionedRules = new List<EquipmentRule>();
            foreach (var exSet in exSets)
            {
                partitionedRules.Add(
                    new EquipmentRule { Equipment = exSet.Value.ToArray(), Rule = RuleTypes.EXCLUDES });
            }

            return partitionedRules;
        }

        private static IEnumerable<EquipmentRule> GroupMustPickOneRules(IEnumerable<PairwiseEquipmentRule> rules)
        {
            var graph = new UndirectedGraph();
            var mpoRules = rules.Where(r => r.RuleType.Equals(RuleTypes.MUST_PICK_EXACTLY_ONE));
            foreach (var mpoRule in mpoRules)
            {
                graph.AddEdge(mpoRule.EquipmentId1, mpoRule.EquipmentId2);
            }

            var partitionedRules = new List<EquipmentRule>();
            var disjointSets = graph.DisjointSets();
            foreach (var set in disjointSets)
            {
                partitionedRules.Add(
                    new EquipmentRule
                    {
                        Equipment = set.Value.ToArray(),
                        Rule = RuleTypes.MUST_PICK_EXACTLY_ONE
                    });
            }

            return partitionedRules;
        }

        /// <summary>
        /// Partitions rules into groups.
        /// </summary>
        /// <param name="rules"></param>
        private IEnumerable<EquipmentRule> GroupEquipmentRules(IList<PairwiseEquipmentRule> rules )
        {
            // partition all of the "must pick one" rules.
            var mpoRules = GroupMustPickOneRules(rules);

            // partition all of the "excludes" rules.
            var exRules = GroupExcludesRules(rules);

            // Group the "includes" rules on the parent option id: e.g. 1->2, 1->3 == 1->2,3
            var incRules = GroupIncludesRules(rules);

            var groupedRules = new List<EquipmentRule>();
            groupedRules.AddRange(mpoRules);
            groupedRules.AddRange(exRules);
            groupedRules.AddRange(incRules);

            return groupedRules;

        }

        #endregion

        #region book mapping

        private List<BookRecord> GetMappedBookRecords(int style, IBook book)
        {
            var map = new BookMapper();
            switch (book.Id)
            {
                case BookId.Kbb:
                    return map.KbbTrims(style).ToList();
                case BookId.Nada:
                    return map.NadaTrims(style).ToList();
                case BookId.Galves:
                    return map.GalvesTrims(style).ToList();
                case BookId.BlackBook:
                    return map.BlackBookTrims(style).ToList();
            }
            throw new VehicleConfigException("Invalid book specified.");
        }

        private List<BookRecord> GetMappedBookRecords(string vin, IBook book)
        {
            var map = new BookMapper();
            switch (book.Id)
            {
                case BookId.Kbb:
                    return map.KbbTrims(vin).ToList();
                case BookId.Nada:
                    return map.NadaTrims(vin).ToList();
                case BookId.Galves:
                    return map.GalvesTrims(vin).ToList();
                case BookId.BlackBook:
                    return map.BlackBookTrims(vin).ToList();
            }
            throw new VehicleConfigException("Invalid book specified.");
        }
        

        #endregion
    }
}
