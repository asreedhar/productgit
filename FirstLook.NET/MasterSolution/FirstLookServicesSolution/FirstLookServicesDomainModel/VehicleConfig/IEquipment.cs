namespace FirstLook.FirstLookServices.DomainModel.VehicleConfig
{
    public interface IEquipment
    {
        dynamic Id { get; }
        string Desc { get; }
        bool Default { get; }
        int SortOrder { get; }
    }
}