namespace FirstLook.FirstLookServices.DomainModel.VehicleConfig.Internal
{
    public interface IEquipmentRule
    {
        string Rule { get;}
        dynamic[] Equipment { get;}
    }
}