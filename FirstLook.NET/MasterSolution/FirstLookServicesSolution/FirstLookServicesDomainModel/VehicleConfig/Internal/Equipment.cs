using System;

namespace FirstLook.FirstLookServices.DomainModel.VehicleConfig.Internal
{
    internal class Equipment : IEquipment, IComparable
    {
        public dynamic Id { get; set; }
        public string Desc { get; set; }
        public bool Default { get; set; }
        public int SortOrder { get; set; }
        public dynamic CategoryId { get; set; }
        public bool Status { get; set; }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            Equipment e2 = obj as Equipment;
            if (e2 == null) return 1;

            if (Id < e2.Id) return -1;
            if (Id == e2.Id) return 0;
            if (Id > e2.Id) return 1;

            throw new Exception("Invalid equipment comparison");
        }
    }
}