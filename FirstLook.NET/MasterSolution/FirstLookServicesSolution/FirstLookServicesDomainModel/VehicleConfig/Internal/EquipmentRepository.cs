using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using FirstLook.FirstLookServices.DomainModel.Utility;

namespace FirstLook.FirstLookServices.DomainModel.VehicleConfig.Internal
{
    internal class EquipmentRepository
    {
        internal readonly string CONNECTION;
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public EquipmentRepository()
        {
            CONNECTION = ConfigurationManager.ConnectionStrings["VehicleCatalog"].ConnectionString;
        }

        internal IEnumerable<PairwiseEquipmentRule> GetKbbEquipmentRules(int vehicleId)
        {
            const int mustPick = 15;
            const int excludes = 9;
            const int includes = 11;

            const string proc = "Firstlook.GetKbbEquipmentRulesFromVehicleId";
            var rules = new List<PairwiseEquipmentRule>();

            using (var connection = ProfiledDbConnectionFactory.GetConnection(CONNECTION) )
            using (var command = connection.CreateCommand() )
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = proc;
                command.Parameters.AddWithValue(command,"vehicleId", vehicleId);

                try
                {
                    connection.Open();
                    var reader = command.ExecuteReader();

                    // Get the equipment
                    while (reader.Read())
                    {
                        int optionId = reader.GetInt32(reader.GetOrdinal("OptionId"));
                        int relationshipTypeId = reader.GetInt32(reader.GetOrdinal("RelationshipTypeId"));
                        int contextValueId = reader.GetInt32(reader.GetOrdinal("ContextValueId"));

                        var r = new PairwiseEquipmentRule
                            {EquipmentId1 = optionId, EquipmentId2 = contextValueId, RuleTypeId = relationshipTypeId};

                        switch (relationshipTypeId)
                        {
                            case mustPick:
                                r.RuleType = RuleTypes.MUST_PICK_EXACTLY_ONE;
                                break;
                            case excludes:
                                r.RuleType = RuleTypes.EXCLUDES;
                                break;
                            case includes:
                                r.RuleType = RuleTypes.INCLUDES;
                                break;                            
                        }

                        rules.Add(r);

                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    throw;
                }
            }

            return rules;
        }  // we may not need this.

        internal IEnumerable<Equipment> GetKbbEquipment(int vehicleId) 
        {
            const string proc = "Firstlook.GetKbbEquipmentFromVehicleId";
            var equipment = new List<Equipment>();

            using (var connection = ProfiledDbConnectionFactory.GetConnection(CONNECTION) )
            using (var command = connection.CreateCommand() )
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = proc;
                command.Parameters.AddWithValue(command, "vehicleId", vehicleId);

                try
                {
                    connection.Open();
                    var reader = command.ExecuteReader();

                    // Get the equipment
                    while (reader.Read())
                    {
                        int categoryId = reader.GetInt32( reader.GetOrdinal("CategoryId"));
                        int id = reader.GetInt32(reader.GetOrdinal("Id"));
                        bool isDefaultConfig = reader.GetBoolean(reader.GetOrdinal("IsDefaultConfiguration"));
                        string name = reader.GetString(reader.GetOrdinal("name"));
                        int sortOrder;
                        
                        if( reader.IsDBNull(reader.GetOrdinal("SortOrder")))
                        {
                            sortOrder = 10000;
                        }
                        else
                        {
                            sortOrder = reader.GetInt32(reader.GetOrdinal("SortOrder"));                            
                        }
                        
                        
                        var e = new Equipment
                            {
                                CategoryId = categoryId,
                                Id = id,
                                Default = isDefaultConfig,
                                Desc = name,
                                SortOrder = sortOrder
                            };

                        equipment.Add(e);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    throw;
                }
            }

            return equipment;
        }  // confirmed
    }
}