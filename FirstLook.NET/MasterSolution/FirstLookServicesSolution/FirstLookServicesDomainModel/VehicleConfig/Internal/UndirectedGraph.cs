﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.FirstLookServices.DomainModel.Utility;
using QuickGraph;
using QuickGraph.Algorithms;

namespace FirstLook.FirstLookServices.DomainModel.VehicleConfig.Internal
{
    public class UndirectedGraph
    {
        private readonly Dictionary<dynamic, List<dynamic>> _adjacencyList;

        public UndirectedGraph()
        {
            _adjacencyList = new Dictionary<dynamic, List<dynamic>>();
        }

        public IDictionary<dynamic, List<dynamic>> DisjointSets()
        {
            var qGraph = ToUndirectedGraph();
            var disjointSet = qGraph.ComputeDisjointSet();

            // we will track the sets of items in a dictionary of lists.
            var sets = new Dictionary<dynamic, List<dynamic>>();

            // get all the pairs of vertices, determine which set they are in.
            var combinations = Vertices().GetPairwisePermutations<dynamic>();

            /*
            //          var list = Vertices().ToList();
                        var combinations = from item1 in list
                                           from item2 in list
                                           where item1 < item2
                                           select Tuple.Create(item1, item2);
            */

            foreach (var pair in combinations)
            {
                var item1 = pair.Item1;
                var item2 = pair.Item2;

                // are they in the same set?
                var item1Set = disjointSet.FindSet(item1);
                var item2Set = disjointSet.FindSet(item2);

                if (!sets.ContainsKey(item1Set)) sets.Add(item1Set, new List<dynamic>());
                if (!sets.ContainsKey(item2Set)) sets.Add(item2Set, new List<dynamic>());

                if (!sets[item1Set].Contains(item1)) sets[item1Set].Add(item1);
                if (!sets[item2Set].Contains(item2)) sets[item2Set].Add(item2);
            }
            
            return sets;
        }

        public IEnumerable<Tuple<dynamic, dynamic>> Edges()
        {
            var qEdges = ToUndirectedGraph().Edges;
            return qEdges.Select(qEdge => new Tuple<dynamic, dynamic>(qEdge.Source, qEdge.Target)).ToList();
        }

        public IEnumerable<dynamic> Vertices()
        {
            return ToUndirectedGraph().Vertices;
        }

        private UndirectedGraph<dynamic, Edge<dynamic>> ToUndirectedGraph()
        {
            var qGraph = new UndirectedGraph<dynamic, Edge<dynamic>>();

            foreach (var item in _adjacencyList)
            {
                var itemEdges = item.Value;
                foreach (var edge in itemEdges)
                {
                    qGraph.AddVerticesAndEdge(new Edge<dynamic>(item.Key, edge));
                }
            }
            return qGraph;
        }

        public void AddEdge(dynamic a, dynamic b)
        {
            // since this is an undirected graph, we don'dynamic need to store a->b and b->a. 
            if ((_adjacencyList.ContainsKey(a) && _adjacencyList[a].Contains(b)) ||
                (_adjacencyList.ContainsKey(b) && _adjacencyList[b].Contains(a)))
            {
                // already in the list in either a->b or b->a format. don'dynamic need both.
                return;
            }

            if (!_adjacencyList.ContainsKey(a))
            {
                _adjacencyList.Add(a, new List<dynamic>());
            }

            _adjacencyList[a].Add(b);
        }
    }
}