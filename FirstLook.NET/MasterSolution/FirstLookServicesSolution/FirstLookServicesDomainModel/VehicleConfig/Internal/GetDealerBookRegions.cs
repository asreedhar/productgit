﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using FirstLook.FirstLookServices.DomainModel.Utility;

namespace FirstLook.FirstLookServices.DomainModel.VehicleConfig.Internal
{
    internal class GetDealerBookRegions
    {
        private int _dealerId;

        internal readonly string IMT;
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        internal GetDealerBookRegions(int dealerId)
        {
            _dealerId = dealerId;
            IMT = ConfigurationManager.ConnectionStrings["IMT"].ConnectionString;
        }

        internal VehicleValuationGuide.DomainModel.Nada.Model.Region NadaRegion()
        {
            // look up dealer's state. cross-reference with States.
            const string proc = "dbo.GetNadaRegionCode";
            List<int> regionCodes = new List<int>();

            using (var connection = ProfiledDbConnectionFactory.GetConnection(IMT))
            using (var command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = proc;
                command.Parameters.AddWithValue(command, "businessUnitId", _dealerId);

                try
                {
                    connection.Open();
                    var reader = command.ExecuteReader();

                    // Get the equipment
                    while (reader.Read())
                    {
                        int code = reader.GetInt32(reader.GetOrdinal("NADARegionCode"));
                        regionCodes.Add(code);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    throw;
                }
            }

            if( regionCodes.Any() )
            {
                return (VehicleValuationGuide.DomainModel.Nada.Model.Region) regionCodes.First();
            }

            return VehicleValuationGuide.DomainModel.Nada.Model.Region.Undefined;
        }

/*
        internal VehicleValuationGuide.DomainModel.Galves.Model.Region GalvesRegion()
        {
            throw new NotImplementedException();
        }

        internal VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Region KbbRegion()
        {
            throw new NotImplementedException();            
        }
*/

    }
}
