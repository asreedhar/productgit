﻿using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.BookMapping.Internal;
using Microsoft.SqlServer.Server;

namespace FirstLook.FirstLookServices.DomainModel.VehicleConfig.Internal.BookBuilders
{
    interface IVehicleConfigBuilder
    {
        IEnumerable<VehicleConfig> CreateConfigs(BookRecord bookRecord, IBook book);
        
        IEnumerable<Equipment> GetEquipment(dynamic trimId, string vin="");

        void SaveEquipment(VehicleConfig config, IEnumerable<Equipment> equipment);

        IEnumerable<PairwiseEquipmentRule> GetRules(dynamic trimId);

        void SaveRules(VehicleConfig config, IEnumerable<EquipmentRule> rules);

        IEnumerable<Equipment> GetEquipment(dynamic trimId, int dealerId,string vin = "");

    }

}
