using System.Collections.Generic;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.BookMapping.Internal;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Galves.Model.Api;

namespace FirstLook.FirstLookServices.DomainModel.VehicleConfig.Internal.BookBuilders
{
    internal class GalvesBuilder : IVehicleConfigBuilder
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IResolver _resolver;        
        private readonly IGalvesService _galvesService;

        public GalvesBuilder()
        {
            _resolver = RegistryFactory.GetResolver();            
            _galvesService = _resolver.Resolve<IGalvesService>();
        }

        public IEnumerable<VehicleConfig> CreateConfigs(BookRecord bookRecord, IBook book)
        {
            if (book.Id != BookId.Galves)
            {
                string message = string.Format("The Galves builder cannot build {0} configurations.", book.Desc);
                Log.Error(message);
                throw new VehicleConfigException(message);
            }

            // create a new vehicle config
            var config = new VehicleConfig {Book = book, TrimId = bookRecord.TrimId, Make=bookRecord.Make, 
                Model=bookRecord.Model, Year=bookRecord.Year};
            var galvesVehicle = _galvesService.Vehicle(bookRecord.TrimId);
            config.Trim = galvesVehicle.Style.Value;
            return new List<VehicleConfig> {config};
        }

        public IEnumerable<Equipment> GetEquipment(dynamic trimId, string vin="")
        {
            int vehicleId = (int) trimId;
            var adjustments = _galvesService.Adjustments(vehicleId);

            var equipment = new List<Equipment>();
            foreach (var adj in adjustments)
            {
                var eq = new Equipment();
                eq.Default = adj.AdjustmentCode == AdjustmentCode.Nul;
                eq.Desc = adj.Name.Replace("DED:", string.Empty).Replace("ADD:", string.Empty);
                eq.Id = adj.Name;
                eq.SortOrder = 0;

                equipment.Add(eq);
            }

            return equipment;
        }

        public void SaveEquipment(VehicleConfig config, IEnumerable<Equipment> equipment)
        {
            foreach (var e in equipment)
            {
                config.AddEquipment(e);
            }
        }

        public IEnumerable<PairwiseEquipmentRule> GetRules(dynamic trimId)
        {
            int vehicleId = (int)trimId;
            var adjExc = _galvesService.AdjustmentExceptions(vehicleId);

            // Galves only has mutually exclusive rules.
            var rules = new List<PairwiseEquipmentRule>();
            foreach(var exc in adjExc)
            {
                foreach(var adjustmentName in exc.Exceptions)
                {
                    PairwiseEquipmentRule r = new PairwiseEquipmentRule();
                    r.EquipmentId1 = exc.Name;
                    r.EquipmentId2 = adjustmentName;
                    r.RuleType = RuleTypes.EXCLUDES;
                    rules.Add(r);
                }
            }
            return rules;
        }

        public void SaveRules(VehicleConfig config, IEnumerable<EquipmentRule> rules)
        {
            foreach (var rule in rules)
            {
                config.AddEquipmentRule(rule);
            }
        }
        
        #region IVehicleConfigBuilder Members


        public IEnumerable<Equipment> GetEquipment(dynamic trimId, int dealerId, string vin = "")
        {
            throw new System.NotImplementedException();
        }

        #endregion
    }
}