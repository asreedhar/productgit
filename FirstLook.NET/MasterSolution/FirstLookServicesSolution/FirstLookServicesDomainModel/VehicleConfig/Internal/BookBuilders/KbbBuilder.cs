﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.BookMapping.Internal;
using FirstLook.FirstLookServices.DomainModel.Utility;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Api;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model.Sql;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model;
using FirstLook.Reports.App.ReportDefinitionLibrary;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


namespace FirstLook.FirstLookServices.DomainModel.VehicleConfig.Internal.BookBuilders
{
    internal class KbbBuilder : IVehicleConfigBuilder
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IResolver _resolver;
        private readonly IKbbService _kbbService;
        private readonly EquipmentRepository _equipmentRepository;
        
        


        const int MustPick = 15;
        const int Excludes = 9;
        const int Includes = 11;


        public KbbBuilder()
        {
            _resolver = RegistryFactory.GetResolver();            
            _kbbService = _resolver.Resolve<IKbbService>();
            _equipmentRepository = new EquipmentRepository();
         
        }
        //public IList<VehicleOption> Options
        //{
        //    get;
        //    set;
        //}

        public IEnumerable<VehicleConfig> CreateConfigs(BookRecord bookRecord, IBook book)
        {
            if (book.Id != BookId.Kbb)
            {
                string message = string.Format("The kbb builder cannot build {0} configurations.", book.Desc);
                Log.Error(message);
                throw new VehicleConfigException(message);
            }

            // create a new vehicle config
            var config = new VehicleConfig { Book = book, TrimId = bookRecord.TrimId, Make=bookRecord.Make, 
                Model=bookRecord.Model, Year=bookRecord.Year };

          
          // var kbbVehicle = _kbbService.Vehicle(bookRecord.TrimId);

            MemoryStream sdata = null;
            BinaryFormatter binaryFormatter = null;
            Vehicle kbbVehicle = new Vehicle();

            try
            {

                sdata =
                    ReportAnalyticsClient.GetReportData<MemoryStream>(new List<string>() { Convert.ToString(bookRecord.TrimId) });

                if (sdata != null)
                {
                    sdata.Position = 0;
                    binaryFormatter = new BinaryFormatter();
                    kbbVehicle = (Vehicle)binaryFormatter.Deserialize(sdata);
                    sdata.Close();
                }
            }
            catch (Exception e)
            {
                Log.Error("An error occured while implementing Memcached Client", e);
            }


            if (sdata == null)
            {
                kbbVehicle = _kbbService.Vehicle(bookRecord.TrimId);
                try
                {
                    using (MemoryStream stream = new MemoryStream())
                    {
                        BinaryFormatter binaryFormatter2 = new BinaryFormatter();
                        binaryFormatter2.Serialize(stream, kbbVehicle);
                        ReportAnalyticsClient.SetReportData(new List<string>() { Convert.ToString(bookRecord.TrimId) },
                                                            stream);
                    }
                }
                catch (Exception e)
                {
                    Log.Error("An error occured while implementing Memcached Client", e);

                }
            }
           

            config.Trim = kbbVehicle.BlueBookName; // or KBB.Trims.DetailName?  Should be consistent.
            return new List<VehicleConfig>{config};
        }

        public IEnumerable<VehicleConfig> CreateConfigs(BookRecord bookRecord, IBook book, string vin)
        {
            if (book.Id != BookId.Kbb)
            {
                string message = string.Format("The kbb builder cannot build {0} configurations.", book.Desc);
                Log.Error(message);
                throw new VehicleConfigException(message);
            }

            // create a new vehicle config
            var config = new VehicleConfig
            {
                Book = book,
                TrimId = bookRecord.TrimId,
                Make = bookRecord.Make,
                Model = bookRecord.Model,
                Year = bookRecord.Year
            };

            var kbbVehicle = _kbbService.Vehicle(bookRecord.TrimId);
            config.Trim = kbbVehicle.BlueBookName; // or KBB.Trims.DetailName?  Should be consistent.
            return new List<VehicleConfig> { config };
        }

        public IEnumerable<Equipment> GetEquipment(dynamic trimId, string vin = "")
        {
            int vehicleId = (int) trimId;
//            return GetDatabaseEquipment(vehicleId);
            return GetVvgEquipment(vehicleId, vin);
        }

        public void SaveEquipment(VehicleConfig config, IEnumerable<Equipment> equipment)
        {
            foreach (var e in equipment)
            {
                config.AddEquipment(e);
            }
        }

        public IEnumerable<PairwiseEquipmentRule> GetRules(dynamic trimId)
        {
            int vehicleId = (int)trimId;

            var rules = new List<PairwiseEquipmentRule>();

//            rules.AddRange(GetDatabaseRules(vehicleId));  // rules from DB sprocs
            rules.AddRange( GetVvgRules(vehicleId) );       // rules from VVG

            // get the implied rules about engines, transmissions, and drive trains.
            var implied = GetImpliedRules(vehicleId);

            // only add them if they aren't already present
            foreach(var impliedRule in implied)
            {
                if(rules.Count(r => r.EquipmentId1 == impliedRule.EquipmentId1 && 
                                    r.EquipmentId2 == impliedRule.EquipmentId2 &&
                                    r.RuleType == impliedRule.RuleType &&
                                    r.RuleTypeId == impliedRule.RuleTypeId) == 0)
                {
                    rules.Add(impliedRule);
                }
            }

            return rules;
        }

        public void SaveRules(VehicleConfig config, IEnumerable<EquipmentRule> rules)
        {
            foreach (var rule in rules)
            {
                config.AddEquipmentRule(rule);
            }
        }

        private IEnumerable<Equipment> GetDatabaseEquipment(int vehicleId)
        {
            return _equipmentRepository.GetKbbEquipment(vehicleId);
        }

/*
        // use this if you want to get equipment rules from our sprocs instead of VVG
        private IEnumerable<PairwiseEquipmentRule> GetDatabaseRules(int vehicleId)
        {
            return _equipmentRepository.GetKbbEquipmentRules(vehicleId);
        }
*/

        private IEnumerable<PairwiseEquipmentRule> GetImpliedRules(int vehicleId)
        {
            var rules = new List<PairwiseEquipmentRule>();

            // get the equipment from the database.
            var equipment = GetDatabaseEquipment(vehicleId).ToList();

            // Add rules for the engine, transmission, and drivetrain, since they aren't supposed / guaranteed to be
            // explicitly listed in the kbb rules in our db, except sometimes they are.

            // Engines
            var engines = equipment.Where(e => e.CategoryId == 2655).ToList();
            IEnumerable<Tuple<Equipment, Equipment>> enginePairs;

            if( engines.Count == 1 )
            {
                // If there is only one engine, you must pick it.
                enginePairs = new List<Tuple<Equipment, Equipment>>
                    {
                        new Tuple<Equipment, Equipment>(engines.First(), engines.First())
                    };
            }
            else
            {
                // Add all the permutations.
                enginePairs = engines.GetPairwisePermutations();
            }
            foreach (var enginePair in enginePairs)
            {
                var pwEngRule = new PairwiseEquipmentRule();
                pwEngRule.EquipmentId1 = enginePair.Item1.Id;
                pwEngRule.EquipmentId2 = enginePair.Item2.Id;
                pwEngRule.RuleType = RuleTypes.MUST_PICK_EXACTLY_ONE;
                rules.Add(pwEngRule);
            }                

            // Transmissions
            var transmissions = equipment.Where(e => e.CategoryId == 2670).ToList();
            IEnumerable<Tuple<Equipment, Equipment>> transmissionPairs;

            if(transmissions.Count == 1)
            {
                // If there is only one transmission, you must pick it.
                transmissionPairs = new List<Tuple<Equipment, Equipment>>
                    {
                        new Tuple<Equipment, Equipment>(transmissions.First(), transmissions.First())
                    };
            }
            else
            {
                transmissionPairs = transmissions.GetPairwisePermutations();
            }
            foreach (var transmissionPair in transmissionPairs)
            {
                var transRule = new PairwiseEquipmentRule();
                transRule.EquipmentId1 = transmissionPair.Item1.Id;
                transRule.EquipmentId2 = transmissionPair.Item2.Id;
                transRule.RuleType = RuleTypes.MUST_PICK_EXACTLY_ONE;
                rules.Add(transRule);
            }                

            // Drivetrains
            var driveTrains = equipment.Where(e => e.CategoryId == 2654).ToList();
            IEnumerable<Tuple<Equipment, Equipment>> driveTrainPairs;

            if(driveTrains.Count == 1)
            {
                driveTrainPairs = new List<Tuple<Equipment, Equipment>>
                    {
                        new Tuple<Equipment, Equipment>(driveTrains.First(), driveTrains.First())
                    };
            }
            else
            {
                driveTrainPairs = driveTrains.GetPairwisePermutations();
            }
            foreach (var driveTrainCombination in driveTrainPairs)
            {
                var dtRule = new PairwiseEquipmentRule();
                dtRule.EquipmentId1 = driveTrainCombination.Item1.Id;
                dtRule.EquipmentId2 = driveTrainCombination.Item2.Id;
                dtRule.RuleType = RuleTypes.MUST_PICK_EXACTLY_ONE;
                rules.Add(dtRule);
            }                

            return rules;
        }

        #region VVG lookups

        private IEnumerable<Equipment> GetVvgEquipment(int vehicleId)
        {
            var kbbVehicle = _kbbService.Vehicle(vehicleId);
            var options = kbbVehicle.Options;

            var equipment = new List<Equipment>();
            foreach(var option in options)
            {
                var eq = new Equipment();
                eq.CategoryId = option.Category;
                eq.Default = option.IsDefaultConfiguration;
                eq.Desc = option.Name;
                eq.Id = option.Id;
                eq.SortOrder = option.SortOrder;

                equipment.Add(eq);
            }

            return equipment;
        }

        private IEnumerable<Equipment> GetVvgEquipment(int vehicleId,string vin)
        {
            var kbbVehicle = _kbbService.Vehicle(vehicleId);
            var options = kbbVehicle.Options;

            var equipment = new List<Equipment>();
            foreach (var option in options)
            {
                var eq = new Equipment();
                eq.CategoryId = option.Category;
                eq.Default = option.IsDefaultConfiguration;
                eq.Desc = option.Name;
                eq.Id = option.Id;
                eq.SortOrder = option.SortOrder;

                equipment.Add(eq);
            }

            return equipment;
        }

        private IEnumerable<PairwiseEquipmentRule> GetVvgRules(int vehicleId)
        {
            // get the equipment rules from VVG
         // var kbbVehicle = _kbbService.Vehicle(vehicleId);


            MemoryStream sdata = null;
            BinaryFormatter binaryFormatter = null;
            Vehicle kbbVehicle = new Vehicle();

            try
            {
               
                sdata =
                    ReportAnalyticsClient.GetReportData<MemoryStream>(new List<string>() { Convert.ToString(vehicleId) });
               
                if (sdata != null)
                {

                    sdata.Position = 0;
                    binaryFormatter = new BinaryFormatter();
                    kbbVehicle = (Vehicle)binaryFormatter.Deserialize(sdata);
                    sdata.Close();

                }
            }
            catch (Exception e)
            {
                Log.Error("An error occured while implementing Memcached Client", e);
            }


            if (sdata == null)
            {
                kbbVehicle = _kbbService.Vehicle(vehicleId);
                try
                {
                    using (MemoryStream stream = new MemoryStream())
                    {
                        BinaryFormatter binaryFormatter2 = new BinaryFormatter();
                        binaryFormatter2.Serialize(stream, kbbVehicle);
                        ReportAnalyticsClient.SetReportData(new List<string>() { Convert.ToString(vehicleId) },
                                                            stream);
                    }
                }
                catch (Exception e)
                {
                    Log.Error("An error occured while implementing Memcached Client", e);

                }
            }
           

            var options = kbbVehicle.Options;

            var rules = new List<PairwiseEquipmentRule>();
            foreach (var option in options)
            {
                var relationships = option.Relationships;
                foreach (var relationship in relationships)
                {
                    foreach (var relatedOption in relationship.RelatedOptions)
                    {
                        var rule = new PairwiseEquipmentRule();

                        // Note: the order matters due to "includes" rules!!!
                        rule.EquipmentId2 = option.Id;
                        rule.EquipmentId1 = relatedOption.Id;
                        rule.RuleTypeId = (int) relationship.RelationshipType;

                        switch ((int) rule.RuleTypeId)
                        {
                            case MustPick:
                                rule.RuleType = RuleTypes.MUST_PICK_EXACTLY_ONE;
                                break;
                            case Excludes:
                                rule.RuleType = RuleTypes.EXCLUDES;
                                break;
                            case Includes:
                                rule.RuleType = RuleTypes.INCLUDES;
                                break;
                        }

                        rules.Add(rule);
                    }
                }
            }

            return rules;
            
        }

        #endregion

        #region VINSpecificOptions


        public IEnumerable<Equipment> GetEquipment(dynamic trimId, int dealerId, string vin = "")
        {
            int vehicleId = (int)trimId;
            IEnumerable<Equipment> equipments = GetVvgEquipment(vehicleId, dealerId,vin);

            return equipments;
        }

        private IEnumerable<Equipment> GetVvgEquipment(int vehicleId, int dealerId,string vin)
        {
            var kbbVehicleOptions = _kbbService.GetOptions(vehicleId, vin, dealerId);
            var equipment = new List<Equipment>();
            foreach (var kbbVehicleOption in kbbVehicleOptions)
            {
                var eq = new Equipment();
                eq.CategoryId = kbbVehicleOption.CategoryId;
                eq.Default = kbbVehicleOption.Status;
                eq.Desc = kbbVehicleOption.Description;
                eq.Id = kbbVehicleOption.VehicleOpionId;
                eq.SortOrder = kbbVehicleOption.SortOrder;
                /*eq.Status = kbbVehicleOption.Status;*/


                equipment.Add(eq);
            }

            return equipment;
        }

        #endregion
    }
}
