using System.Collections.Generic;
using System.Collections.Specialized;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.BookMapping.Internal;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model;
using FirstLook.FirstLookServices.DomainModel.Caching;
namespace FirstLook.FirstLookServices.DomainModel.VehicleConfig.Internal.BookBuilders
{
    internal class BlackBookBuilder : IVehicleConfigBuilder
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private IResolver _resolver;
        private BlackBookServiceLookup _lookup;
       
        public BlackBookBuilder()
        {
            _resolver = RegistryFactory.GetResolver();
            _lookup = new BlackBookServiceLookup();
        }

        public IEnumerable<VehicleConfig> CreateConfigs(BookRecord bookRecord, IBook book)
        {

            if (book.Id != BookId.BlackBook)
            {
                string message = string.Format("The blackbook builder cannot build {0} configurations.", book.Desc);
                Log.Error(message);
                throw new VehicleConfigException(message);
            }            
            
            List<CarInfo> cars = LookupAndCache(bookRecord.TrimId, bookRecord.Vin);

            List<VehicleConfig> configs = new List<VehicleConfig>();

            foreach(var car in cars)
            {

                // create a new vehicle config
                var config = new VehicleConfig
                {
                    Book = book,
                    TrimId = bookRecord.TrimId,
                    Make = bookRecord.Make,
                    Model = bookRecord.Model,
                    Year = bookRecord.Year
                };

                // Try to get the "trim" name.
                config.Trim = car.Series + " " + car.BodyStyle;               
                configs.Add(config);
            }

            return configs;
        }

        private List<CarInfo> LookupAndCache(string uvc, string vin=null)
        {
            List<CarInfo> ret = this.GetFromCarCache(uvc, vin);
            if (null == ret)
            {
                ret = new List<CarInfo>();
                // if we don't have it, go get it.
                var lockedList = _lookup.GetCarsByUvc(uvc);
                //Need to be able to break Cars objects into pieces but not sure if changing Cars would cause ish elsewhere - Erik
                foreach (var carInf in lockedList.Values)
                {
                    this.AddCar(carInf);
                    ret.Add(carInf);
                }
            }
            return ret;
        }

        public IEnumerable<Equipment> GetEquipment(dynamic trimId, string vin="")
        {
            List<Equipment> equipment = new List<Equipment>();

            var cars = LookupAndCache(trimId, vin);
            foreach(var carInfo in cars)
            {
                // Note: There is some rather nasty casting going on inside VVG. Your CarInfo is actually a Car.
                // Note 2: I actually found I had to go back to CarInfo lists for caching purposes. Cars (class) has too many dependencies to muck with
                // but we needed a way to cache by UVC since everything is looped that way - Erik
                Car car = carInfo as Car;
                if( car == null )
                {
                    throw new VehicleConfigException("unable to cast to type Car.");
                }

                var options = car.Options;
                foreach(var option in options)
                {
                    var eq = new Equipment();
                    eq.Default = false; //used to be option.Flag but we need to line up with valuations and desktop functionality that ignores defaults
                    eq.Desc = option.Description;
                    eq.Id = option.UniversalOptionCode;
                    eq.SortOrder = 0;
                    equipment.Add(eq);
                }

            }

            return equipment;
        }

        public void SaveEquipment(VehicleConfig config, IEnumerable<Equipment> equipment)
        {
            foreach (var e in equipment)
            {
                config.AddEquipment(e);
            }
        }

        public IEnumerable<PairwiseEquipmentRule> GetRules(dynamic trimId)
        {
            // BlackBook has no rules
            return new List<PairwiseEquipmentRule>();
        }

        public void SaveRules(VehicleConfig config, IEnumerable<EquipmentRule> rules)
        {
            foreach (var rule in rules)
            {
                config.AddEquipmentRule(rule);
            }
        }
        

        #region IVehicleConfigBuilder Members


        public IEnumerable<Equipment> GetEquipment(dynamic trimId, int dealerId, string vin = "")
        {
            throw new System.NotImplementedException();
        }

        #endregion
    }
}