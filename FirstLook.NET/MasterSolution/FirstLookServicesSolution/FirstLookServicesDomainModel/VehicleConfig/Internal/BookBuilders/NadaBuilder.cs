﻿using System.Collections.Generic;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.BookMapping.Internal;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Model.Api;

namespace FirstLook.FirstLookServices.DomainModel.VehicleConfig.Internal.BookBuilders
{
    internal class NadaBuilder : IVehicleConfigBuilder
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IResolver _resolver;
        private readonly INadaService _nadaService;
        private readonly Region _region;

        public NadaBuilder(int dealerId)
        {
            _resolver = RegistryFactory.GetResolver();
            _nadaService = _resolver.Resolve<INadaService>();

            // This may be unnecessary. Can we use a default region for the purposes of getting the equipment and rules?
            _region = new GetDealerBookRegions(dealerId).NadaRegion();
        }

        public IEnumerable<VehicleConfig> CreateConfigs(BookRecord bookRecord, IBook book)
        {
            if (book.Id != BookId.Nada)
            {
                string message = string.Format("The NADA builder cannot build {0} configurations.", book.Desc);
                Log.Error(message);
                throw new VehicleConfigException(message);
            }

            // create a new vehicle config
            var config = new VehicleConfig {Book = book, TrimId = bookRecord.TrimId, Make=bookRecord.Make, 
                Model=bookRecord.Model, Year=bookRecord.Year};

            var nadaVehicle = _nadaService.Information(bookRecord.TrimId);
            config.Trim = nadaVehicle.Body.Name;
            return new List<VehicleConfig> {config};
        }

        public IEnumerable<Equipment> GetEquipment(dynamic trimId, string vin="")
        {
            var nadaAccessories = _nadaService.Accessories(trimId, _region);
            var equipment = new List<Equipment>();
            foreach (var accessory in nadaAccessories)
            {
                equipment.Add(
                    new Equipment
                    {
                        Default = accessory.IsAdded || accessory.IsIncluded,
                        Desc = accessory.Description,
                        Id = accessory.Code,
                        SortOrder = 0
                    }
                );
            }
            return equipment;
        }

        public void SaveEquipment(VehicleConfig config, IEnumerable<Equipment> equipment)
        {
            foreach(var eq in equipment)
            {
                config.AddEquipment(eq);
            }
        }

        public IEnumerable<PairwiseEquipmentRule> GetRules(dynamic trimId)
        {
            // Get the equipment relationships
            var relationships = _nadaService.AccessoryRelationships(trimId);

            var rules = new List<PairwiseEquipmentRule>();

            foreach (AccessoryRelationship relationship in relationships)
            {
                // create all combintaions of this accessory with its related accessories.
                foreach (var relatedAccessory in relationship.RelatedAccessories)
                {
                    var r = new PairwiseEquipmentRule();
                    r.EquipmentId1 = relationship.AccessoryCode;
                    r.EquipmentId2 = relatedAccessory.Code;

                    switch (relationship.RelationshipType)
                    {
                        case AccessoryRelationshipType.Excluded:
                            r.RuleType = RuleTypes.EXCLUDES;
                            break;
                        case AccessoryRelationshipType.Included:
                            r.RuleType = RuleTypes.INCLUDES;
                            break;
                        case AccessoryRelationshipType.Undefined:
                            // TODO: what should we do with these? ignore them?
                            var message =
                                string.Format(
                                    "Ignoring the relationship between accessories {0} and {1} since the relationship type is undefined.",
                                    relationship.AccessoryCode, relatedAccessory.Code);
                            Log.Warn(message);
                            continue;
                    }

                    rules.Add(r);
                }
            }

            return rules;
        }

        public void SaveRules(VehicleConfig config, IEnumerable<EquipmentRule> rules)
        {
            foreach(var rule in rules)
            {
                config.AddEquipmentRule(rule);
            }
        }

        #region IVehicleConfigBuilder Members
        
        public IEnumerable<Equipment> GetEquipment(dynamic trimId, int dealerId, string vin = "")
        {
            throw new System.NotImplementedException();
        }

        #endregion
        
    }
}
