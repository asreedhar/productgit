namespace FirstLook.FirstLookServices.DomainModel.VehicleConfig.Internal
{
    /// <summary>
    /// A rule which applies to a list of equipment.
    /// </summary>
    internal class EquipmentRule : IEquipmentRule
    {
        public string Rule { get; set; }
        public dynamic[] Equipment { get; set; }
    }

    /// <summary>
    /// A rule between two pieces of equipment.
    /// </summary>
    internal class PairwiseEquipmentRule
    {
        public dynamic EquipmentId1 { get; set; }
        public dynamic EquipmentId2 { get; set; }
        public string RuleType { get; set; }
        public dynamic RuleTypeId { get; set; }
    }

}