﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FirstLook.FirstLookServices.DomainModel.VehicleConfig.Internal
{
    internal class VehicleConfig : IVehicleConfig
    {
        private readonly List<EquipmentRule> _rules;
        private readonly List<Equipment> _equipment;
 
        public VehicleConfig()
        {
            _rules = new List<EquipmentRule>();
            _equipment = new List<Equipment>();
        }

        #region IVehicleConfig

        public IBook Book { get; set; }
        public string Trim { get; set; }
        public dynamic TrimId { get; set; }
        public bool Selected { get; set; }
        IEquipmentRule[] IVehicleConfig.EquipmentRules
        {
            get { return EquipmentRules().ToArray(); }
        }
        IEquipment[] IVehicleConfig.Equipment
        {
            get { return Equipment().ToArray(); }
        }

        public string Make { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }

        #endregion

        internal IList<EquipmentRule> EquipmentRules()
        {
            return _rules;
        }
        internal IList<Equipment> Equipment()
        {
            return _equipment;
        }
        internal void AddEquipmentRule(EquipmentRule rule)
        {
            // make sure we don't have this rule already
            if (_rules.Any(r => r.Rule == rule.Rule && 
                r.Equipment.Intersect(rule.Equipment).Count() == r.Equipment.Count() &&
                r.Equipment.Intersect(rule.Equipment).Count() == rule.Equipment.Count() ))
            {
                // we already have this rule
                return;
            }

            _rules.Add(rule);
        }
        internal void AddEquipment(Equipment e)
        {
            // make sure we don't have it already
            if (_equipment.All(eq => eq.Id != e.Id))
            {
                _equipment.Add(e);                
            }

/*
            if( _equipment.Contains(e))
            {
                throw new ApplicationException("added equipment twice");
//                return;
            }
            _equipment.Add(e);
*/
        }

    }
}
