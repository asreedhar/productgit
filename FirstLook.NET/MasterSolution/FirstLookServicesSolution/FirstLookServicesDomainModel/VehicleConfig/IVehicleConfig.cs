namespace FirstLook.FirstLookServices.DomainModel.VehicleConfig.Internal
{
    public interface IVehicleConfig
    {
        IBook Book { get; }
        string Trim { get; }
        dynamic TrimId { get; }
        bool Selected { get; }
        IEquipmentRule[] EquipmentRules { get; }
        IEquipment[] Equipment { get; }

        string Make { get; set; }
        string Model { get; set; }
        int Year { get; set; }
    }
}