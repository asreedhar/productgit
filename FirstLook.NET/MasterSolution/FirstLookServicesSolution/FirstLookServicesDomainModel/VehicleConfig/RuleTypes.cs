namespace FirstLook.FirstLookServices.DomainModel.VehicleConfig
{
    internal static class RuleTypes
    {
        internal const string MUST_PICK_EXACTLY_ONE = "MustPickExactlyOne";
        internal const string INCLUDES = "Includes";
        internal const string EXCLUDES = "Excludes";        
    }
}