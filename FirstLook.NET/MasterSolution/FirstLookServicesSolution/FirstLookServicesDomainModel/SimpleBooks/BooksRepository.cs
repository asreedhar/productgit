﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using FirstLook.FirstLookServices.DomainModel.Utility;

namespace FirstLook.FirstLookServices.DomainModel.SimpleBooks
{
    public class BooksRepository
    {
        internal readonly string CONNECTION;
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public BooksRepository()
        {
            CONNECTION = ConfigurationManager.ConnectionStrings["IMT"].ConnectionString;
        }

        /// <summary>
        /// What books does the system support?
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IBook> GetBooks()
        {
            return BookFactory.Instance.GetBooks();
        }

        /// <summary>
        /// What are the dealer's preferred books (two are supported) in the correct order?
        /// </summary>
        /// <param name="dealer"></param>
        /// <returns></returns>
        public IEnumerable<IBook> GetBooks(int dealer)
        {
            const string proc = "dbo.GetDealerBooks";
            var scoredBooks = new List<Tuple<int,IBook>>();

            using (var connection = ProfiledDbConnectionFactory.GetConnection(CONNECTION))
            using (var command = connection.CreateCommand() )
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = proc;
                command.Parameters.AddWithValue(command,"businessUnitId", dealer);

                try
                {
                    connection.Open();
                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        var score = reader.GetInt32(reader.GetOrdinal("Score"));
                        var thirdPartyId = reader.GetByte(reader.GetOrdinal("ThirdPartyId")); // it's a tinyint in sql
                        
                        // use the book factory to add the book along with its score: 1=primary, 2=secondary
                        var id = (BookId) Convert.ToInt32(thirdPartyId);
                        scoredBooks.Add(new Tuple<int, IBook>(score, BookFactory.Instance.Get(id)));
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    throw;
                }
            }

            // sort the books by score, return the IBooks
            return scoredBooks.OrderBy(t => t.Item1).Select(t => t.Item2);
        }
    }
}
