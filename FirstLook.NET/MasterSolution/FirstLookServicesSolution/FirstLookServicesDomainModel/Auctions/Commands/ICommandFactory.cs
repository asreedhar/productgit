﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Mmr;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Naaa.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Sales.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands
{
    public interface ICommandFactory
    {
        ICommand<FetchNaaaReferenceResultsDto, FetchNaaaReferenceArgumentsDto> CreateFetchNaaaReferenceCommand();

        ICommand<FetchNaaaTransactionSummaryResultsDto, IdentityContextDto<FetchNaaaTransactionSummaryArgumentsDto>> CreateFetchNaaaTransactionSummaryCommand();

        ICommand<MmrReferenceResultsDto, string> CreateMmrReferenceCommand();// Empty string argument needed to fit command factory.

        ICommand<MmrReferenceResultsDto, string> CreateFetchMmrReferenceCommand();

        ICommand<FetchMMRAuctionSummaryResultsDto, FetchMMRAuctionSummaryArgumentsDto> CreateFetchMmrAuctionSummaryCommand();

        ICommand<FetchMMRAuctionTransactionsResultsDto, FetchMMRAuctionTransactionsArgumentsDto> CreateFetchMmrAuctionTransactionsCommand();
		
		ICommand<FetchSalesResultsDto, FetchSalesArgumentsDto> CreateFetchSalesDetailsCommand();

        ICommand<FetchVehiclesResultsDto, FetchVehiclesArgumentsDto> CreateFetchAuctionVehiclesCommand();

        ICommand<FetchLanesResultsDto, FetchLanesArgumentsDto> CreateFetchAuctionLanesCommand();
    }
}
