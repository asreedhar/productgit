﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Mmr
{
    [Serializable]
    public class MMRVehicleDto
    {
        public string Mid { get; set; }
        public int Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Trim { get; set; }
        public string Region { get; set; }
        public MileagesDto Mileages { get; set; }
        public PricesDto Prices { get; set; }
        public System.Nullable<System.DateTime> EarliestSaleDate { get; set; }
        public System.Nullable<System.DateTime> LatestSaleDate { get; set; }
    }
}
