﻿using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Mmr
{
    public class FetchMMRAuctionSummaryResultsDto
    {
        public List<MMRVehicleDto> Vehicles { get; set; }
    }
}
