﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Mmr
{
    [Serializable]
    public class MmrTransactionDto
    {
        public string Date { get; set; }
        public string Auction { get; set; }
        public string SalesType { get; set; }
        public int Price { get; set; }
        public int Mileage { get; set; }
        public string Condition { get; set; }
        public string Color { get; set; }
        public string Engine { get; set; }
        public string Transmission { get; set; }
        public string InSample { get; set; }
    }
}
