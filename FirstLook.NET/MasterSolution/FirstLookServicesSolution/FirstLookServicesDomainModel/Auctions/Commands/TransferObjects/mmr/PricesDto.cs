﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Mmr
{
    [Serializable]
    public class PricesDto
    {
        public int Average { get; set; }
        public int Min { get; set; }
        public int Max { get; set; }
        public int SampleSize { get; set; }
    }
}
