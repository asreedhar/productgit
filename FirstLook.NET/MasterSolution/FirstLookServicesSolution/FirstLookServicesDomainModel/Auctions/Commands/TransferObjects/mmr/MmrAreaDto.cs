﻿namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Mmr
{
    public class MmrAreaDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
