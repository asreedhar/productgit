﻿using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Mmr
{
    public class FetchMMRAuctionTransactionsResultsDto
    {
        public string Mid { get; set; }
        public int Year { get; set; }
        public string Area { get; set; }
        public List<MmrTransactionDto> Transactions { get; set; }
    }
}
