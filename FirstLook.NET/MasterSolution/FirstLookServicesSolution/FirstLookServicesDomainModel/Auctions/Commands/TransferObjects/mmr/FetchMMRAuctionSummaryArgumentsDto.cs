﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Mmr
{
    [Serializable]
    public class FetchMMRAuctionSummaryArgumentsDto
    {
        public string Vin { get; set; }
        public string Area { get; set; }
    }
}
