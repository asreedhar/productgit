﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Mmr
{
    [Serializable]
    public class FetchMMRAuctionTransactionsArgumentsDto
    {
        public string Mid { get; set; }
        public int Year { get; set; }
        public string Area { get; set; }
    }
}
