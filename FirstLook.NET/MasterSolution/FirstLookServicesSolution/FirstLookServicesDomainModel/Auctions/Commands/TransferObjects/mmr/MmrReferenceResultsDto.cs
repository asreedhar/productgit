﻿
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Mmr
{
    public class MmrReferenceResultsDto
    {
        public List<MmrAreaDto> Areas { get; set; }
    }
}
