﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Sales.Envelopes
{
    [Serializable]
    public class FetchVehiclesArgumentsDto
    {
        public int DealerId { get; set; }
        public int LocationId { get; set; }
        public string Date { get; set; }
        public string Lane { get; set; }
    }
}
