﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model.Sale;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Sales.Envelopes
{
    public class FetchLanesResultsDto
    {
        public PrincipalDto Principal { get; set; }

        public FetchVehiclesArgumentsDto Arguments { get; set; }

        public IList<LanesDto> Lanes { get; set; }
    }
}
