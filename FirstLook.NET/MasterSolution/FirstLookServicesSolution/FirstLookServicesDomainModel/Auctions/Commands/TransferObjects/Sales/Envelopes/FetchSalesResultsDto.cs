﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Sales.Envelopes
{
    [Serializable]
    public class FetchSalesResultsDto
    {
        public PrincipalDto Principal { get; set; }

        public FetchSalesArgumentsDto Arguments { get; set; }

        public IList<SalesDto> NearSales { get; set; }

        public IList<SalesDto> AllSales { get; set; }
    }
}
