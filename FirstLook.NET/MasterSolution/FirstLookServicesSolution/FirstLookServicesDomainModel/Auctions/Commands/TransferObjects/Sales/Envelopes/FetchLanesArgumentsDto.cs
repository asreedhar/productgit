﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Repositories.Gateways;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Sales.Envelopes
{
    [Serializable]
    public class FetchLanesArgumentsDto
    {
        public int LocationId { get; set; }
        public  string Date { get; set; }
    }
}
