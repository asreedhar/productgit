﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Sales.Envelopes
{
    [Serializable]
    public class FetchSalesArgumentsDto
    {
        public double Lat { get; set; }
        public double Long { get; set; }
    }
}
