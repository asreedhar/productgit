﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Sales
{
    [Serializable]
    public class LocationDto
    {
        public int LocationId { get; set; }
        public string LocationDescription { get; set; }
        public List<string> SalesDate { get; set; }
    }
}
