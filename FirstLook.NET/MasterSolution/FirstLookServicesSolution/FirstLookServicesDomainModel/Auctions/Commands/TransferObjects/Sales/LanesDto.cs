﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Sales
{
    [Serializable]
    public class LanesDto
    {
        public int LocationId { get; set; }
        public string SaleDate { get; set; }
        public string Lane { get; set; }
    }
}
