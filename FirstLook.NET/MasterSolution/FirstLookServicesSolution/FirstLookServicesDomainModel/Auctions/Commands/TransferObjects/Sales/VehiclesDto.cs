﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Sales
{
    [Serializable]
    public class VehiclesDto
    {
        //public string Lane { get; set; }
        public string RunNumber { get; set; }
        public int Year { get; set; }
        public string MakeModelTrimStyle { get; set; }
        public string Engine { get; set; }
        public string Color { get; set; }
        public int Mileage { get; set; }
        public string Vin { get; set; }
    }
}
