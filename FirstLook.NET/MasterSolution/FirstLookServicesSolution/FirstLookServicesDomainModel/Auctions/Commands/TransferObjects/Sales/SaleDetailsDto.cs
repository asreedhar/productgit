﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Sales
{
    [Serializable]
    public class SaleDetailsDto
    {
        public IList<SalesDto> NearSales { get; set; }

        public IList<SalesDto> AllSales { get; set; }
    }
}
