﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Sales.Envelopes
{
    [Serializable]
    public class FetchVehiclesResultsDto
    {
        public PrincipalDto Principal { get; set; }

        public FetchVehiclesArgumentsDto Arguments { get; set; }

        public IList<VehiclesDto> Vehicles { get; set; }
        
    }
}
