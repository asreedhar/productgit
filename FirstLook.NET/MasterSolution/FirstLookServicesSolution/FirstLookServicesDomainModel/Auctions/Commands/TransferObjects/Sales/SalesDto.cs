﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Sales.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Sales
{
    [Serializable]
    public class SalesDto
    {
        public string State { get; set; }
        public List<LocationDto> Location { get; set; }
        //public string SalesDate { get; set; } 
        //public string Lane { get; set; }
    }
}
