﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Naaa
{
    [Serializable]

    public class AreaDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
