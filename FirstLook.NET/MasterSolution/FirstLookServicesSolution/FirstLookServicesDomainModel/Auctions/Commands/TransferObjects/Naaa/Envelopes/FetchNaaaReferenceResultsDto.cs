﻿using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Naaa.Envelopes
{
    public class FetchNaaaReferenceResultsDto
    {
        public List<AreaDto> Areas { get; set; }

        public List<PeriodDto> Periods { get; set; }
    }
}
