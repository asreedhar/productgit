﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Naaa
{
    [Serializable]
    public class NaaaTransactionSummaryDto
    {
        public int LowMileage { get; set; }
        public int HighMileage { get; set; }
        public string Vin { get; set; }
        public decimal MaxSalePrice { get; set; }
        public decimal AvgSalePrice { get; set; }
        public decimal MinSalePrice { get; set; }
        public int SalePriceSampleSize { get; set; }
        public decimal MaxSalePriceSimilarMileage { get; set; }
        public decimal AvgSalePriceSimilarMileage { get; set; }
        public decimal MinSalePriceSimilarMileage { get; set; }
        public int SalePriceSimilarMileageSampleSize { get; set; }
        public int MaxMileage { get; set; }
        public int AvgMileage { get; set; }
        public int MinMileage { get; set; }
        public int MileageSampleSize { get; set; }
    }
}
