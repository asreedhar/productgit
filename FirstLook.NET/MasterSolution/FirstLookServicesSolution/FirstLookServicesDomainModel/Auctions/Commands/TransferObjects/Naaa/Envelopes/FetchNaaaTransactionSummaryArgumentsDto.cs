﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Naaa.Envelopes
{
    [Serializable]
    public class FetchNaaaTransactionSummaryArgumentsDto
    {
        private string _vin;

        public string Vin
        {
            get { return _vin; } 

            set
            {
                _vin = value;
                if( _vinPattern == "" || _vinPattern == null )
                {
                    _vinPattern = _vin.Substring(0, 8) + "_" + _vin.Substring(9, 1);
                }
            }
        }

        public string _vinPattern;

        public string VinPattern 
        {
            get { return _vinPattern;  }
            set { _vinPattern = value;  }
        }

        public int Uid { get; set; }
        public int Area { get; set; }
        public int Period { get; set; }
        public int Mileage { get; set; }
    }
}
