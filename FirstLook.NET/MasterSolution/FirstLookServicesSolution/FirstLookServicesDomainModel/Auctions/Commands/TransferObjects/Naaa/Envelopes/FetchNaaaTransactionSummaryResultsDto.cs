﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Naaa.Envelopes
{
    [Serializable]
    public class FetchNaaaTransactionSummaryResultsDto
    {
        public NaaaTransactionSummaryDto NaaaTransactionSummary { get; set; }
    }
}
