﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Sales;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Sales.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model.Sale;
using FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.Exceptions;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.Impl.Sales
{
    class FetchAuctionVehiclesCommand : ICommand<FetchVehiclesResultsDto,FetchVehiclesArgumentsDto>
    {
        public FetchVehiclesResultsDto Execute(FetchVehiclesArgumentsDto parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();
            var vehiclesRepository = resolver.Resolve<IVehiclesRepository>();

            VehicleDetailsInput vehicleDetailsInput = new VehicleDetailsInput()
            {
                DealerId = parameters.DealerId,
                LocationId = parameters.LocationId,
                Date = parameters.Date,
                Lane = parameters.Lane
            };

            IList<VehiclesDto> vehicleDetails = Mapper.Map(vehiclesRepository.VehicleDetails(vehicleDetailsInput));
            if (vehicleDetails == null || vehicleDetails.Count == 0)
            {
                throw new NoDataFoundException("Auction Vehicles");
            }
            return new FetchVehiclesResultsDto()
            {
               Vehicles = vehicleDetails
            };
        }
    }
}
