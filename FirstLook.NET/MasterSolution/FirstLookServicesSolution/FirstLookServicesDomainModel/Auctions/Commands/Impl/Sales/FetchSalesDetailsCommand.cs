﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Sales;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Sales.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model.Sale;
using FirstLook.FirstLookServices.DomainModel.Auctions.Repositories;
using FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.Validation;
using FirstLook.FirstLookServices.DomainModel.Exceptions;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.Impl.Sales
{
    class FetchSalesDetailsCommand : ICommand<FetchSalesResultsDto, FetchSalesArgumentsDto>
    {
        public FetchSalesResultsDto Execute(FetchSalesArgumentsDto parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();
            var saleDetailsRepository = resolver.Resolve<ISaleDetailsRepository>();
        
            SaleDetailsInput saleDetailsInput = new SaleDetailsInput()
            {
                Lat =  parameters.Lat,
                Long = parameters.Long
            };

            SaleDetailsDto salesDetails = Mapper.Map(saleDetailsRepository.SalesDetails(saleDetailsInput));
            if (salesDetails == null || (salesDetails.AllSales == null && salesDetails.NearSales == null)||(salesDetails.AllSales.Count == 0 && salesDetails.NearSales.Count == 0))
            {
                throw new NoDataFoundException("Auction Sales");
            }
             return new FetchSalesResultsDto()
                    {
                        NearSales = salesDetails.NearSales,
                        AllSales = salesDetails.AllSales
                    };
        }
    }
}
