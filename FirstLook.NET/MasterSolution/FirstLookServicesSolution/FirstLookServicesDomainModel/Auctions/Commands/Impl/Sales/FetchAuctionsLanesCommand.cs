﻿using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Sales;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Sales.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model.Sale;
using FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.Exceptions;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.Impl.Sales
{
    class FetchAuctionsLanesCommand : ICommand<FetchLanesResultsDto, FetchLanesArgumentsDto>
    {
        public FetchLanesResultsDto Execute(FetchLanesArgumentsDto parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();
            var laneDetailsRepository = resolver.Resolve<ILaneRepository>();

            LaneDetailsInput laneDetailsInput = new LaneDetailsInput()
            {
                LocationId    = parameters.LocationId,
                Date = parameters.Date
            };

           IList<LanesDto> laneDetails = Mapper.Map(laneDetailsRepository.LaneDetails(laneDetailsInput));
           if (laneDetails == null || laneDetails.Count == 0)
           {
               throw new NoDataFoundException("Auction Lanes");
           }
            return new FetchLanesResultsDto()
            {
                Lanes = laneDetails
            };
        }
    }
}
