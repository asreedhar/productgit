﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Sales;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.Impl.Sales
{
    public static class Mapper
    {
        public static SaleDetailsDto Map(Model.Sale.SalesDetails dto)
        {
            SaleDetailsDto saleDetailsDto =
                new SaleDetailsDto()
                {
                    //NearSales = dto.NearSales.Select(d => new SalesDto {State = d.State, Location = new LocationDto() {LocationId = d.Location.LocationId,LocationDescription = d.Location.LocationDescription}, SalesDate = d.SalesDate}).ToList(),
                    //AllSales = dto.AllSales.Select(d => new SalesDto { State = d.State, Location = new LocationDto() { LocationId = d.Location.LocationId, LocationDescription = d.Location.LocationDescription }, SalesDate = d.SalesDate }).ToList()
                    NearSales = dto.NearSales.Select(d => new SalesDto { State = d.State, Location =  d.Location.Select(x => new LocationDto() {LocationId = x.LocationId , LocationDescription = x.LocationDescription , SalesDate = x.SalesDate}).ToList() }).ToList(),
                    AllSales = dto.AllSales.Select(d => new SalesDto { State = d.State, Location = d.Location.Select(x => new LocationDto() { LocationId = x.LocationId, LocationDescription = x.LocationDescription, SalesDate = x.SalesDate }).ToList() }).ToList()
                };
                //dto.Select(d => new SalesDto {State = d.State, Location = new LocationDto() {LocationId = d.Location.LocationId,LocationDescription = d.Location.LocationDescription}, SalesDate = d.SalesDate}).ToList();

            return saleDetailsDto;
        }
        public static IList<VehiclesDto> Map(IList<Model.Sale.Vehicle> dto)
        {
            IList<VehiclesDto> vehicleDetailsDto = dto.Select(obj =>  
                new VehiclesDto()
                {
                    Color = obj.Color,
                    Engine = obj.Engine,
                    //Lane = obj.Lane,
                    MakeModelTrimStyle = obj.MakeModelTrimStyle,
                    Mileage = obj.Mileage,
                    RunNumber = obj.RunNumber,
                    Vin = obj.Vin,
                    Year = obj.Year
                }).ToList();
            //dto.Select(d => new SalesDto {State = d.State, Location = new LocationDto() {LocationId = d.Location.LocationId,LocationDescription = d.Location.LocationDescription}, SalesDate = d.SalesDate}).ToList();

            return vehicleDetailsDto;
        }
        public static IList<LanesDto> Map(IList<Model.Sale.Lanes> dto)
        {
            IList<LanesDto> laneDetailsDto = dto.Select(obj =>
                new LanesDto()
                {
                    Lane = obj.Lane,
                    LocationId = obj.LocationId,
                    SaleDate = obj.SaleDate
                }).ToList();

            return laneDetailsDto;
        }
    }
}
