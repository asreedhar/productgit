﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.ServiceModel.Description;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Mmr;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.FirstLookServices.DomainModel.MMRTransactionService;
using FirstLook.FirstLookServices.DomainModel.MMRVehicleDecoderService;
using VehicleType = FirstLook.FirstLookServices.DomainModel.MMRTransactionService.VehicleType;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.Impl.Mmr
{
    class FetchMmrAuctionSummaryCommand : ICommand<FetchMMRAuctionSummaryResultsDto, FetchMMRAuctionSummaryArgumentsDto>
    {
        public FetchMMRAuctionSummaryResultsDto Execute(FetchMMRAuctionSummaryArgumentsDto parameters)
        {
            string region = "NA";

            if (String.IsNullOrEmpty(parameters.Area) == false)
            {
                region = parameters.Area;
            }

            string mmrUsername = ConfigurationManager.AppSettings["MmrUserName"];
            string mmrPassword = ConfigurationManager.AppSettings["MmrPassword"];
            string mmrTimeout = ConfigurationManager.AppSettings["MmrServiceTimeout"];
            string mmrServiceUrl = ConfigurationManager.AppSettings["MmrVehicleDecoderServiceUrl"]; // BUZID:27747
            if (mmrTimeout.Length == 0)
            {
                throw new FirstlookGeneralException("Service Requires mmrTimeout");
            }
            int nTimeoutMs = System.Convert.ToInt32(mmrTimeout);
            if (nTimeoutMs == 0)
            {
                throw new FirstlookGeneralException("Service Requires mmrTimeout");
            }

            // Call the mmr service to decode the vin.
            VehicleDecoderServiceImplService decoderClient = new VehicleDecoderServiceImplService();
            decoderClient.Url = mmrServiceUrl;
            NetworkCredential cred = new NetworkCredential(mmrUsername, mmrPassword);
            decoderClient.Credentials = cred;

            Vehicle[] mmrVehicles = null;

            try
            {
                mmrVehicles = decoderClient.decodeVin(parameters.Vin, "", "", "MMR");
            }
            catch (WebException we)
            {
                throw new FirstlookGeneralException("We were unable to connect to Manheim.  Please try again.", we.Message);                
            }
            catch (Exception e)
            {
                throw new FirstlookGeneralException("Manheim was unable to find this vehicle.", e.Message);
            }
            
            if (mmrVehicles.Length < 1)
            {
                throw new FirstlookGeneralException("Manheim was unable to find this vehicle.");
            }


            List<MMRVehicleDto> vehicles = new List<MMRVehicleDto>();

            // Go through each vehicle returned from the mmr vin decoder service and call the getVehicleSummary service, compiling the
            // final return object.
            for (int i = 0; i < mmrVehicles.Length; i++)
            {
                Vehicle mmrVeh = mmrVehicles[i];

                MmrTransactionsServiceUSService transClient = new MmrTransactionsServiceUSService();

                transClient.Credentials = cred;
                transClient.Timeout = nTimeoutMs;//BUGZID: 27990
                MMRTVehicleSummary vehSummary = null;

                var mileagesDto = new MileagesDto();
                var pricesDto = new PricesDto();
                DateTime? earliestSalesDate = null;
                DateTime? latestSalesDate = null;

                try
                {
                    vehSummary = transClient.getVehicleSummary(new VehicleType {mid = mmrVeh.type.mid, year = mmrVeh.type.year}, region);

                    mileagesDto = new MileagesDto
                    {
                        Average = vehSummary.averageOdometer,
                        Max = vehSummary.highestOdometer,
                        Min = vehSummary.lowestOdometer,
                        SampleSize = vehSummary.numberOfVehicles
                    };

                    pricesDto = new PricesDto
                    {
                        Average = vehSummary.averagePrice,
                        Max = vehSummary.highestPrice,
                        Min = vehSummary.lowestPrice,
                        SampleSize = vehSummary.numberOfVehicles
                    };

                    earliestSalesDate = vehSummary.earliestSaleDate;
                    latestSalesDate = vehSummary.latestSaleDate;
                }
                catch
                {
                    // It's expected that there may be no mmr summary data and that Manheim will throw an exception.  Do nothing.
                }

                // Use body instead of trim.
                string trim = mmrVeh.type.body.name;

                if ((trim == null) || (trim == ""))
                {
                    trim = "Base";
                }

                vehicles.Add(new MMRVehicleDto
                {
                    Make = mmrVeh.type.make.name,
                    Mid = mmrVeh.type.mid,
                    Mileages = mileagesDto,
                    Model = mmrVeh.type.model.name,
                    Region = region,
                    Prices = pricesDto,
                    Trim = trim,
                    Year = mmrVeh.type.year,
                    EarliestSaleDate = earliestSalesDate,
                    LatestSaleDate = latestSalesDate
                });
            }

            return new FetchMMRAuctionSummaryResultsDto
            {
                Vehicles = vehicles
            };
        }
    }
}
