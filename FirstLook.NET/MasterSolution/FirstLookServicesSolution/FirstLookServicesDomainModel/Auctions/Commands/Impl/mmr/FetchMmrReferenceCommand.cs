﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Mmr;
using FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Model;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.Impl.Mmr
{
    class FetchMmrReferenceCommand : ICommand<MmrReferenceResultsDto, string>
    {
        public MmrReferenceResultsDto Execute(string unusedParams)
        {
            MmrRegions region = new MmrRegions();

            List<MmrAreaDto> areas = new List<MmrAreaDto>();

            foreach (var key in region.regions)
            {
                areas.Add(new MmrAreaDto
                 {
                     Id = key.Key,
                     Name = key.Value
                 });
            }

             return new MmrReferenceResultsDto
            {
                Areas = areas
            };
        }
       }

 
           
    }
