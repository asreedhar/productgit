﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Mmr;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.FirstLookServices.DomainModel.MMRTransactionService;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.Impl.Mmr
{
    class FetchMmrAuctionTransactionsCommand : ICommand<FetchMMRAuctionTransactionsResultsDto, FetchMMRAuctionTransactionsArgumentsDto>
    {
        public FetchMMRAuctionTransactionsResultsDto Execute(FetchMMRAuctionTransactionsArgumentsDto parameters)
        {
            string region = "NA";

            if (String.IsNullOrEmpty(parameters.Area) == false)
            {
                region = parameters.Area;
            }

            string mmrUsername = ConfigurationManager.AppSettings["MmrUserName"];
            string mmrPassword = ConfigurationManager.AppSettings["MmrPassword"];
            string mmrTimeout = ConfigurationManager.AppSettings["MmrServiceTimeout"];
            string mmrServiceUrl = ConfigurationManager.AppSettings["MmrTransactionsServiceUrl"]; // BUZID:27747
            int nTimeoutMs = System.Convert.ToInt32(mmrTimeout);

            MmrTransactionsServiceUSService transClient = new MmrTransactionsServiceUSService();
            transClient.Url = mmrServiceUrl;

            NetworkCredential cred = new NetworkCredential(mmrUsername, mmrPassword);
            transClient.Credentials = cred;
            transClient.Timeout = nTimeoutMs;//BUGZID: 27990

            MMRTPurchasedVehicle[] vehicles = null;

            try
            {
                vehicles = transClient.getVehicleTransactions(new VehicleType { mid = parameters.Mid, year = parameters.Year }, parameters.Area);
            }
            catch (WebException we)
            {
                throw new FirstlookGeneralException("We were unable to connect to Manheim.  Please try again.", we.Message);
            }
            catch (Exception e)
            {
                throw new FirstlookGeneralException(" Manheim does not have any transactions for this vehicle.", e.Message);
            }

            List<MmrTransactionDto> transactions = new List<MmrTransactionDto>();

            for (int i = 0; i < vehicles.Length; i++)
            {
                MMRTPurchasedVehicle v = vehicles[i];
                transactions.Add(new MmrTransactionDto
                {
                    Auction = v.auction.name,
                    Color = v.vehicle.detail.exteriorColor.value,
                    Condition = v.statisticalIndicators.bookCondition,
                    Date = v.purchaseDate,
                    Engine = v.vehicle.detail.engine.value,
                    InSample = v.statisticalIndicators.inSampleFlag,
                    Mileage = int.Parse(v.vehicle.detail.odometer),
                    Price = (int)v.purchasePrice,
                    SalesType = v.saleTypeRFL,
                    Transmission = v.vehicle.detail.transmission.value
                });
            }

            return new FetchMMRAuctionTransactionsResultsDto
            {
                Area = region,
                Mid = parameters.Mid,
                Year = parameters.Year,
                Transactions = transactions
            };
        }
    }
}
