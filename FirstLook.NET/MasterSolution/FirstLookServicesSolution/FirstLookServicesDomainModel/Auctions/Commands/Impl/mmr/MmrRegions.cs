﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.Impl.Mmr
{
    public class MmrRegions
    {
       public Dictionary<string, string> regions = new Dictionary<string, string>()
        {
            {"NA","National"},
            {"SE", "Southeast"},
            {"NE", "Northeast"},
            {"MW", "Midwest"},
            {"SW", "Southwest"},
            {"WC", "West Coast"},
        };

    }
}
