﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Mmr;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.FirstLookServices.DomainModel.MMRTransactionService;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.Impl.Mmr
{
    class MmrReferenceCommand : ICommand<MmrReferenceResultsDto, string>
    {
        public MmrReferenceResultsDto Execute(string unusedParams)
        {
            string mmrUsername = ConfigurationManager.AppSettings["MmrUserName"];
            string mmrPassword = ConfigurationManager.AppSettings["MmrPassword"];
            string mmrTimeout = ConfigurationManager.AppSettings["MmrServiceTimeout"];
            string mmrServiceUrl = ConfigurationManager.AppSettings["MmrTransactionsServiceUrl"]; // BUZID:27747
            int nTimeoutMs = System.Convert.ToInt32(mmrTimeout);

            MmrTransactionsServiceUSService transClient = new MmrTransactionsServiceUSService();
            transClient.Url = mmrServiceUrl;
            NetworkCredential cred = new NetworkCredential(mmrUsername, mmrPassword);
            transClient.Credentials = cred;
            transClient.Timeout = nTimeoutMs;/// Default to 5 second timeout for MMR
            Region[] regions = null;

            try
            {
                regions = transClient.getRegionList();
            }
            catch (Exception e)
            {
                throw new FirstlookGeneralException("We were unable to connect to Manheim.  Please try again.", e.Message);
            }

            List<MmrAreaDto> areas = new List<MmrAreaDto>();

            for (int i = 0; i < regions.Length; i++ )
            {
                Region r = regions[i];
                areas.Add(new MmrAreaDto
                {
                    Id = r.regionCode,
                    Name = r.regionDescription
                });
            }

            return new MmrReferenceResultsDto
            {
                Areas = areas
            };
        }
    }
}
