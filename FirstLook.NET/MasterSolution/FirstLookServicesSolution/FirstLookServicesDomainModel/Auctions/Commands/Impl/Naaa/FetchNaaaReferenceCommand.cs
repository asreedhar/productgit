﻿using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Naaa;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Naaa.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.Naaa.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.Impl.Naaa
{
    public class FetchNaaaReferenceCommand : ICommand<FetchNaaaReferenceResultsDto, FetchNaaaReferenceArgumentsDto>
    {
        public FetchNaaaReferenceResultsDto Execute(FetchNaaaReferenceArgumentsDto parameters)
        {
            var factory = RegistryFactory.GetResolver().Resolve<VehicleValuationGuide.DomainModel.Naaa.Commands.ICommandFactory>();

            var command = factory.CreateFormCommand();

            var argumentsDto = new FormArgumentsDto();

            FormResultsDto results = command.Execute(argumentsDto);

            return new FetchNaaaReferenceResultsDto
                       {
                           Areas = (List<AreaDto>) Mapper.Map(results.Areas),
                           Periods = (List<PeriodDto>) Mapper.Map(results.Periods)
                       };
        }
    }
}
