﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Naaa;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Naaa.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.Impl.Naaa
{
    public static class Mapper
    {
        public static AreaDto Map(VehicleValuationGuide.DomainModel.Naaa.Commands.TransferObjects.AreaDto dto)
        {
            if (dto == null)
            {
                return null;
            }

            return new AreaDto
                       {
                           Id = dto.Id,
                           Name = dto.Name
                       };
        }

        public static IList<AreaDto> Map(IList<VehicleValuationGuide.DomainModel.Naaa.Commands.TransferObjects.AreaDto> objs)
        {
            IList<AreaDto> areaDtos = objs.Select(obj => new AreaDto { Id = obj.Id, Name = obj.Name}).ToList();

            return areaDtos;
        }

        public static PeriodDto Map(VehicleValuationGuide.DomainModel.Naaa.Commands.TransferObjects.PeriodDto dto)
        {
            if (dto == null)
            {
                return null;
            }

            return new PeriodDto
                       {
                Id = dto.Id,
                Name = dto.Name
            };
        }

        public static IList<PeriodDto> Map(IList<VehicleValuationGuide.DomainModel.Naaa.Commands.TransferObjects.PeriodDto> objs)
        {
            IList<PeriodDto> periodDtos = objs.Select(obj => new PeriodDto { Id = obj.Id, Name = obj.Name }).ToList();

            return periodDtos;
        }

        public static NaaaTransactionSummaryDto Map(Model.INaaaTransactionSummary naaaTransactionSummary)
        {
            return new NaaaTransactionSummaryDto
            {
                //Vin = naaaTransactionSummary.Vin,
                LowMileage = naaaTransactionSummary.LowMileage,
                HighMileage = naaaTransactionSummary.HighMileage,
                MinSalePrice = naaaTransactionSummary.MinSalePrice,
                AvgSalePrice = naaaTransactionSummary.AvgSalePrice,
                MaxSalePrice = naaaTransactionSummary.MaxSalePrice,
                SalePriceSampleSize = naaaTransactionSummary.SalePriceSampleSize,
                MinSalePriceSimilarMileage = naaaTransactionSummary.MinSalePriceSimilarMileage,
                AvgSalePriceSimilarMileage = naaaTransactionSummary.AvgSalePriceSimilarMileage,
                MaxSalePriceSimilarMileage = naaaTransactionSummary.MaxSalePriceSimilarMileage,
                SalePriceSimilarMileageSampleSize = naaaTransactionSummary.SalePriceSimilarMileageSampleSize,
                MinMileage = naaaTransactionSummary.MinMileage,
                AvgMileage = naaaTransactionSummary.AvgMileage,
                MaxMileage = naaaTransactionSummary.MaxMileage,
                MileageSampleSize = naaaTransactionSummary.MileageSampleSize
            };
        }
    }
}
