﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Naaa.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.Impl.Naaa
{
    public class FetchNaaaTransactionSummaryCommand : ICommand<FetchNaaaTransactionSummaryResultsDto, IdentityContextDto<FetchNaaaTransactionSummaryArgumentsDto>>
    {
        public FetchNaaaTransactionSummaryResultsDto Execute(IdentityContextDto<FetchNaaaTransactionSummaryArgumentsDto> parameters)
        {
            var naaaTransactionSummaryRepository = RegistryFactory.GetResolver().Resolve<IRepository>();

            var naaaTransactionSummary = naaaTransactionSummaryRepository.FetchNaaaTransactionSummary(parameters.Arguments.Uid, parameters.Arguments.Area, parameters.Arguments.Period, parameters.Arguments.Mileage);

            return new FetchNaaaTransactionSummaryResultsDto
            {
                NaaaTransactionSummary = Mapper.Map(naaaTransactionSummary)
            };
        }
    }
}
