﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.Impl.Mmr;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.Impl.Naaa;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.Impl.Sales;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Mmr;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Naaa.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Sales.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Commands.Impl
{
    public class CommandFactory : ICommandFactory
    {
        public ICommand<FetchNaaaReferenceResultsDto, FetchNaaaReferenceArgumentsDto> CreateFetchNaaaReferenceCommand()
        {
            return new FetchNaaaReferenceCommand();
        }

        public ICommand<FetchNaaaTransactionSummaryResultsDto, IdentityContextDto<FetchNaaaTransactionSummaryArgumentsDto>> CreateFetchNaaaTransactionSummaryCommand()
        {
            return new FetchNaaaTransactionSummaryCommand();
        }

        public ICommand<MmrReferenceResultsDto, string> CreateMmrReferenceCommand()
        {
            return new MmrReferenceCommand();
        }

        public ICommand<MmrReferenceResultsDto, string> CreateFetchMmrReferenceCommand()
        {
            return new FetchMmrReferenceCommand();
        }
       
        public ICommand<FetchMMRAuctionSummaryResultsDto, FetchMMRAuctionSummaryArgumentsDto> CreateFetchMmrAuctionSummaryCommand()
        {
            return new FetchMmrAuctionSummaryCommand();
        }

       


        public ICommand<FetchMMRAuctionTransactionsResultsDto, FetchMMRAuctionTransactionsArgumentsDto> CreateFetchMmrAuctionTransactionsCommand()
        {
            return new FetchMmrAuctionTransactionsCommand();
        }
		
		public ICommand<FetchSalesResultsDto, FetchSalesArgumentsDto> CreateFetchSalesDetailsCommand()
        {
            return new FetchSalesDetailsCommand();
        }

        public ICommand<FetchVehiclesResultsDto, FetchVehiclesArgumentsDto> CreateFetchAuctionVehiclesCommand()
        {
            return new FetchAuctionVehiclesCommand();
        }

        public ICommand<FetchLanesResultsDto, FetchLanesArgumentsDto> CreateFetchAuctionLanesCommand()
        {
            return new FetchAuctionsLanesCommand();
        }
    }
}
