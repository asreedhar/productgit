﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Naaa;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Model
{
    public interface IRepository
    {
        INaaaTransactionSummary FetchNaaaTransactionSummary(int uid, int area, int period, int? mileage);
    }
}
