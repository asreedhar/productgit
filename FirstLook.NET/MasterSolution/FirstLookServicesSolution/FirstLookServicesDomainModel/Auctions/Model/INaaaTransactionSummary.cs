﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Model
{
    public interface INaaaTransactionSummary
    {
        string Vin { get; }
        int LowMileage { get; }
        int HighMileage { get; }
        decimal MinSalePrice { get; }
        decimal AvgSalePrice { get; }
        decimal MaxSalePrice { get; }
        int SalePriceSampleSize { get; set; }
        decimal MinSalePriceSimilarMileage { get; }
        decimal AvgSalePriceSimilarMileage { get; }
        decimal MaxSalePriceSimilarMileage { get; }
        int SalePriceSimilarMileageSampleSize { get; set; }
        int MinMileage { get; }
        int AvgMileage { get; }
        int MaxMileage { get; }
        int MileageSampleSize { get; set; }
    }
}
