﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Model.Sale
{
    [DataContract]
    public class LaneDetails
    {
        [DataMember(Name = "locationId")]
        public int LocationId { get; set; }
        [DataMember(Name = "saleDate")]
        public string SaleDate { get; set; }
        [DataMember(Name = "lanes")]
        public IList<string> Lane { get; set; }
    }
}
