﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Model.Sale
{
    [DataContract]
    public class Vehicle
    {
        //public string Lane { get; set; }
        [DataMember(Name = "runNumber")]
        public string RunNumber { get; set; }
        [DataMember(Name = "year")]
        public int Year { get; set; }
        [DataMember(Name = "makeModelTrimStyle")]
        public string MakeModelTrimStyle { get; set; }
        [DataMember(Name = "engine")]
        public string Engine { get; set; }
        [DataMember(Name = "color")]
        public string Color { get; set; }
        [DataMember(Name = "mileage")]
        public int Mileage { get; set; }
        [DataMember(Name = "vin")]
        public string Vin { get; set; }
    }
}
