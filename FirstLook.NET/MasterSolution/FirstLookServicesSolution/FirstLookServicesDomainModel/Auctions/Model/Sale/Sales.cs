﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Model.Sale
{
    [DataContract]
    public class Sales
    {
        [DataMember(Name = "state")]
        public string State { get; set; }
        [DataMember(Name = "location")]
        public List<Location> Location { get; set; }

        //public string Lane { get; set; }
    }
    
}
