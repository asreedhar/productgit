﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Model.Sale
{
    public interface ILaneRepository
    {
        IList<Lanes> LaneDetails(LaneDetailsInput laneDetails);
    }
}
