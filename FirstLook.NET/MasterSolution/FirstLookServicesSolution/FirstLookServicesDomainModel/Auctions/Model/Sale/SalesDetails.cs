﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Model.Sale
{
    public class SalesDetails
    {
        public IList<Sales> NearSales { get; set; }

        public IList<Sales> AllSales { get; set; }
    }
}
