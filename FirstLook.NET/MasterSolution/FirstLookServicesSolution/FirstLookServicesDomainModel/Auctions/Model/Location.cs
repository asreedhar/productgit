﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Model
{
    [DataContract]
    public class Location
    {
        [DataMember(Name = "id")]
        public int LocationId { get; set; }
        [DataMember(Name = "description")]
        public string LocationDescription { get; set; }
        [DataMember(Name = "saleDate")]
        public List<string> SalesDate { get; set; }
    }
}
