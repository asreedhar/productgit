﻿using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model.Sale;
using FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model.Api;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Repositories
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<Datastores.Module>();

            registry.Register<Serializers.Module>();

            registry.Register<INaaaService, NaaaService>(ImplementationScope.Shared);

            registry.Register<IRepository, NaaaTransactionSummaryRepository>(ImplementationScope.Shared);
			
			registry.Register<ISaleDetailsRepository, SaleDetailsRepository>(ImplementationScope.Shared);

            registry.Register<IVehiclesRepository, VehicleDetailsRepository>(ImplementationScope.Shared);

            registry.Register<ILaneRepository, LaneRepository>(ImplementationScope.Shared);
        }
    }
}
