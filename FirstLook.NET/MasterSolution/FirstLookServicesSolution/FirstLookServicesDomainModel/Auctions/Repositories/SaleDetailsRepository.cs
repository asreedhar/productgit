﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model.Sale;
using FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Gateways;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Repositories
{
    public class SaleDetailsRepository : ISaleDetailsRepository
    {
        public Model.Sale.SalesDetails SalesDetails(Entities.SaleDetailsInput saleDetails)
        {
            return new SaleDetailsGateway().Fetch(saleDetails);
        }
    }
}
