﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model;
using FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Datastores;
using FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model;
using FirstLook.VehicleValuationGuide.DomainModel.Naaa.Model.Api;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Gateways
{
    public class NaaaTransactionSummaryGateway : GatewayBase
    {
        public INaaaTransactionSummary FetchTransactionSummary(int uid, int area, int period, int? mileage)
        {
            //string key = CreateCacheKey("" + vinPattern + area + period);
            string key = CreateCacheKey("" + uid + area + period);

            INaaaTransactionSummary value = Cache.Get(key) as INaaaTransactionSummary;

            if (value == null)
            {
                INaaaService service = Resolve<INaaaService>();

                string vic = service.Vic(uid).Value;

                Report report = service.Report(vic, area, period, mileage);

                return new NaaaTransactionSummary()
                {
                    LowMileage = report.LowMileage,
                    HighMileage = report.HighMileage,
                    MaxSalePrice = (report.SalePriceReport.Maximum.HasValue ? report.SalePriceReport.Maximum.Value : (decimal)0.0),
                    AvgSalePrice = (report.SalePriceReport.Average.HasValue ? report.SalePriceReport.Average.Value : (decimal)0.0),
                    MinSalePrice = (report.SalePriceReport.Minimum.HasValue ? report.SalePriceReport.Minimum.Value : (decimal)0.0),
                    SalePriceSampleSize = report.SalePriceReport.SampleSize,
                    MaxSalePriceSimilarMileage = (report.SalePriceSimilarMileageReport.Maximum.HasValue ? report.SalePriceSimilarMileageReport.Maximum.Value : (decimal)0.0),
                    AvgSalePriceSimilarMileage = (report.SalePriceSimilarMileageReport.Average.HasValue ? report.SalePriceSimilarMileageReport.Average.Value : (decimal)0.0),
                    MinSalePriceSimilarMileage = (report.SalePriceSimilarMileageReport.Minimum.HasValue ? report.SalePriceSimilarMileageReport.Minimum.Value : (decimal)0.0),
                    SalePriceSimilarMileageSampleSize = report.SalePriceSimilarMileageReport.SampleSize,
                    MaxMileage = (report.MileageReport.Maximum.HasValue ? report.MileageReport.Maximum.Value : 0),
                    AvgMileage = (report.MileageReport.Average.HasValue ? report.MileageReport.Average.Value : 0),
                    MinMileage = (report.MileageReport.Minimum.HasValue ? report.MileageReport.Minimum.Value : 0),
                    MileageSampleSize = report.MileageReport.SampleSize
                };
            }

            return value;
        }
    }
}
