﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model.Sale;
using FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Datastores;
using FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Gateways
{
    public class LaneGateway : GatewayBase
    {
        public IList<Lanes> Fetch(LaneDetailsInput laneDetailsInput)
        {
            string key = CreateCacheKey("" + laneDetailsInput.LocationId + laneDetailsInput.Date);

            IList<Lanes> value = Cache.Get(key) as List<Lanes>;

            if (value == null)
            {
                var serializer = Resolve<ISerializer<Lanes>>();

                var datastore = Resolve<ILaneDetailsDataStore>();

                using (IDataReader reader = datastore.GetAuctionLanes(laneDetailsInput))
                {
                    IList<Lanes> items = serializer.Deserialize(reader);

                    value = items;

                    Remember(key, value);
                }
            }

            return value;
        }
    }
}
