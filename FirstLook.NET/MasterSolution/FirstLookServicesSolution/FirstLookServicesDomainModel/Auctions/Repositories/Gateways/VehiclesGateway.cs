﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model.Sale;
using FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Datastores;
using FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Gateways
{
    public class VehiclesGateway :GatewayBase
    {
        public IList<Vehicle> Fetch(VehicleDetailsInput vehicleDetailsInput)
        {
            string key = CreateCacheKey("" + vehicleDetailsInput.DealerId + vehicleDetailsInput.LocationId + vehicleDetailsInput.Date);

            IList<Vehicle> value = Cache.Get(key) as List<Vehicle>;

            if (value == null)
            {
                var serializer = Resolve<ISerializer<Vehicle>>();

                var datastore = Resolve<IVehicleDetailsDataStore>();

                using (IDataReader reader = datastore.GetAuctionVehicles(vehicleDetailsInput))
                {
                    IList<Vehicle> items = serializer.Deserialize(reader);

                    value = items;

                    Remember(key, value);
                }
            }

            return value;
        }
    }
}
