﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model.Sale;
using FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Datastores;
using FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Serializers;


namespace FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Gateways
{
    public class SaleDetailsGateway : GatewayBase
    {
        public SalesDetails Fetch(SaleDetailsInput saleDetails)
        {
            string key = CreateCacheKey("" + saleDetails.Lat + saleDetails.Long);

            SalesDetails value = Cache.Get(key) as SalesDetails;

            if (value == null)
            {
                value = new SalesDetails();

                var serializer = Resolve<ISerializer<Sales>>();

                var datastore = Resolve<ISaleDetailsDataStore>();

                IList<Sales> nearbySales = datastore.NearSales(saleDetails);
                value.NearSales = nearbySales;
                IList<Sales> allSales = datastore.AllSales();
                value.AllSales = allSales;
                //using (IDataReader reader = datastore.NearSales(saleDetails))
                //{
                //    IList<Sales> items = serializer.Deserialize(reader);

                //    value.NearSales = items;

                //    //Remember(key, value);
                //}
                //using (IDataReader reader = datastore.AllSales())
                //{
                //    IList<Sales> items = serializer.Deserialize(reader);

                //    value.AllSales = items;
                //}

                Remember(key,value);
            }

            return value;
        }
    }
}
