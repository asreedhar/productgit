﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Entities
{
    public class SaleDetailsInput
    {
        public double Lat { get; set; }
        public double Long { get; set; }
    }
}
