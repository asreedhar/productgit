﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Entities
{
    public class LaneDetailsInput
    {
        public int LocationId { get; set; }
        public string Date { get; set; }
    }
}
