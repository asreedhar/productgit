﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model.Sale;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Serializers
{
    class LaneSerializer : Serializer<Model.Sale.Lanes>
    {
        public override Model.Sale.Lanes Deserialize(System.Data.IDataRecord record)
        {
            return new Lanes()
                   {
                       LocationId = (record.GetInt16(record.GetOrdinal("LocationId"))),
                       Lane = (record.GetString(record.GetOrdinal("Lane"))),
                       SaleDate = (record.GetString(record.GetOrdinal("SaleDate")))
                   };
        }
    }
}
