﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Serializers
{
    class NaaaTransactionSummarySerializer : Serializer<NaaaTransactionSummary>
    {
        public override NaaaTransactionSummary Deserialize(IDataRecord record)
        {
            return new NaaaTransactionSummary()
            {
                Vin = "novin",
                MaxSalePrice = (record.GetDecimal(record.GetOrdinal("MaxSalePrice"))),
                AvgSalePrice = (record.GetDecimal(record.GetOrdinal("AvgSalePrice"))),
                MinSalePrice = (record.GetDecimal(record.GetOrdinal("MinSalePrice"))),
                MaxMileage = Decimal.ToInt32(record.GetDecimal(record.GetOrdinal("MaxMileage"))),
                AvgMileage = Decimal.ToInt32(record.GetDecimal(record.GetOrdinal("AvgMileage"))),
                MinMileage = Decimal.ToInt32(record.GetDecimal(record.GetOrdinal("MinMileage")))
            };
        }
    }
}
