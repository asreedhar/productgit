﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model.Sale;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Serializers
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ISerializer<NaaaTransactionSummary>, NaaaTransactionSummarySerializer>(ImplementationScope.Shared);
			registry.Register<ISerializer<Sales>, SalesSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<Vehicle>, VehiclesSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<Lanes>, LaneSerializer>(ImplementationScope.Shared);
        }
    }
}
