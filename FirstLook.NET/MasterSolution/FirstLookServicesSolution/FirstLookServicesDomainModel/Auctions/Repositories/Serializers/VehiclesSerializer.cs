﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model.Sale;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Serializers
{
    class VehiclesSerializer : Serializer<Model.Sale.Vehicle>
    {
        public override Model.Sale.Vehicle Deserialize(System.Data.IDataRecord record)
        {
            return new Model.Sale.Vehicle()
                   {
                       Color = (record.GetString(record.GetOrdinal("Color"))),
                       Engine = (record.GetString(record.GetOrdinal("Engine"))),
                       //Lane = (record.GetString(record.GetOrdinal("Lane"))),
                       MakeModelTrimStyle = (record.GetString(record.GetOrdinal("MakeModelTrimStyle"))),
                       Mileage = (record.GetInt32(record.GetOrdinal("Mileage"))),
                       RunNumber = (record.GetString(record.GetOrdinal("RunNumber"))),
                       Vin = (record.GetString(record.GetOrdinal("Vin"))),
                       Year = (record.GetInt16(record.GetOrdinal("Year")))
                   };
        }
    }
}
