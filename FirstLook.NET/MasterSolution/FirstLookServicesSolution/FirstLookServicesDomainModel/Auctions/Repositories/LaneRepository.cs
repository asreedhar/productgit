﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model.Sale;
using FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Gateways;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Repositories
{
    public class LaneRepository : ILaneRepository
    {
        public IList<Lanes> LaneDetails(Entities.LaneDetailsInput laneDetails)
        {
            return new LaneGateway().Fetch(laneDetails);
        }
    }
}
