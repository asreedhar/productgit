﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using FirstLook.Common.Core.Data;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Datastores
{
    class LaneDetailsDataStore : ILaneDetailsDataStore
    {       
        #region Class Members

        private const string DatabaseName = "IMT";

        #endregion

        #region ILaneDetailsDataStore Members
        public System.Data.IDataReader GetAuctionLanes(Entities.LaneDetailsInput laneDetails)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "dbo.GetPurchasingCenterAuctionsLanesForMobile";

                    Database.AddWithValue(command, "LocationId", laneDetails.LocationId, DbType.Int32);
                    Database.AddWithValue(command, "Date", laneDetails.Date, DbType.String);

                    var set = new DataSet();

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        var table = new DataTable("Lanes");

                        table.Load(reader);

                        set.Tables.Add(table);
                    }

                    return set.CreateDataReader();
                }
            }

        }
        #endregion
        
    }
}
