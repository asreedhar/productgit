﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using FirstLook.Common.Core.Data;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Datastores
{
    class NaaaTransactionSummaryDatastore : SimpleDataStore, INaaaTransactionSummaryDatastore
    {
        internal const string Prefix = "FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Resources";

        public IDataReader FetchNaaaTransactionSummary(string vinPattern, int area, int period)
        {
            const string queryName = Prefix + ".NaaaTransactionSummary_Report2.txt";

            return Query(new[] { "AuctionTransaction_A3" }, queryName, new[] { new Parameter("VinPatternIn", vinPattern, DbType.String), new Parameter("AreaIn", area, DbType.Int32), new Parameter("PeriodIn", period, DbType.Int32) });
        }

        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        protected override string DatabaseName
        {
            get { return "AuctionNet"; }
        }
    }
}
