﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Datastores
{
    public interface INaaaTransactionSummaryDatastore
    {
        IDataReader FetchNaaaTransactionSummary(string vinPattern, int area, int period);
    }
}
