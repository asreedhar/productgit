﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Datastores
{
    interface IVehicleDetailsDataStore
    {
        IDataReader GetAuctionVehicles(VehicleDetailsInput vehicleDetails);
    }
}
