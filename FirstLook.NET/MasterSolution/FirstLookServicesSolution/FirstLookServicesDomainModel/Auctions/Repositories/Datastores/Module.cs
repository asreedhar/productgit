﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.Registry;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Datastores
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<INaaaTransactionSummaryDatastore, NaaaTransactionSummaryDatastore>(ImplementationScope.Shared);
			registry.Register<ISaleDetailsDataStore, SaleDetailsDataStore>(ImplementationScope.Shared);
            registry.Register<IVehicleDetailsDataStore, VehicleDetailsDataStore>(ImplementationScope.Shared);
            registry.Register<ILaneDetailsDataStore, LaneDetailsDataStore>(ImplementationScope.Shared);
        } 
    }
}
