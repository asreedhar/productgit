﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model.Sale;
using FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Datastores
{
    interface ISaleDetailsDataStore
    {
        //IDataReader NearSales(SaleDetailsInput saleDetails);
        //IDataReader AllSales();
        List<Sales> NearSales(SaleDetailsInput saleDetails);
        List<Sales> AllSales();
    }
}
