﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model.Sale;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Vehicle;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Datastores
{
    class SaleDetailsDataStore : ISaleDetailsDataStore
    {
        #region Class Members

        private const string DatabaseName = "IMT";

        #endregion

        #region ISalesPeopleDataStore Members
        //public System.Data.IDataReader NearSales(Entities.SaleDetailsInput saleDetails)
        public List<Sales> NearSales(Entities.SaleDetailsInput saleDetails)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "dbo.GetPurchasingCenterAuctionsforMobile";

                    Database.AddWithValue(command, "Latitude", saleDetails.Lat, DbType.Double);

                    Database.AddWithValue(command, "Longitude", saleDetails.Long, DbType.Double);

                    var set = new DataSet();

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        var table = new DataTable("NearSales");

                        table.Load(reader);

                        set.Tables.Add(table);
                    }
                    List<Sales> sales = new List<Sales>();
                    foreach (DataRow drv in set.Tables[0].DefaultView.ToTable(true, "State").Rows)
                    {
                        var distinctStateRows = set.Tables[0].Select("State='" + drv["State"].ToString() + "'");

                        Sales sale = new Sales();
                        sale.State = drv["State"].ToString();

                        List<Location> locations = new List<Location>();
                        foreach (DataRow drStateRow in distinctStateRows)
                        {
                            if (locations.Any(obj => obj.LocationId.Equals(Convert.ToInt32(drStateRow["LocationID"])))) continue;

                            DataRow[] locationRows = set.Tables[0].Select("State='" + drv["State"].ToString() + "' And LocationID=" + drStateRow["LocationID"]);

                            Location location = new Location();
                            location.LocationId = Convert.ToInt32(locationRows[0]["LocationID"]);
                            location.LocationDescription = locationRows[0]["LocationDesc"].ToString();

                            List<string> dates = new List<string>();
                            foreach (DataRow drLocationRow in locationRows)
                            {
                                dates.Add(drLocationRow["SaleDate"].ToString());
                            }
                            location.SalesDate = dates;

                            locations.Add(location);
                        }

                        sale.Location = locations;

                        sales.Add(sale);
                    }

                    return sales;
                }
            }
        }

        //public System.Data.IDataReader AllSales()
        public List<Sales> AllSales()
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "dbo.GetPurchasingCenterAuctionsforMobilewithoutCoords";

                    var set = new DataSet();

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        var table = new DataTable("AllSales");

                        table.Load(reader);

                        set.Tables.Add(table);
                    }

                    List<Sales> sales = new List<Sales>();
                    foreach (DataRow drv in set.Tables[0].DefaultView.ToTable(true, "State").Rows)
                    {
                        var distinctStateRows = set.Tables[0].Select("State='" + drv["State"].ToString() + "'");

                        Sales sale = new Sales();
                        sale.State = drv["State"].ToString();

                        List<Location> locations = new List<Location>();
                        foreach (DataRow drStateRow in distinctStateRows)
                        {
                            if (locations.Any(obj => obj.LocationId.Equals(Convert.ToInt32(drStateRow["LocationID"])))) continue;

                            DataRow[] locationRows = set.Tables[0].Select("State='" + drv["State"].ToString() + "' And LocationID=" + drStateRow["LocationID"]);
                            
                            Location location = new Location();
                            location.LocationId = Convert.ToInt32(locationRows[0]["LocationID"]);
                            location.LocationDescription = locationRows[0]["LocationDesc"].ToString();

                            List<string> dates = new List<string>();
                            foreach (DataRow drLocationRow in locationRows)
                            {
                                dates.Add(drLocationRow["SaleDate"].ToString());
                            }
                            location.SalesDate = dates;

                            locations.Add(location);
                        }

                        sale.Location = locations;

                        sales.Add(sale);
                    }

                    return sales;
                    //return set.CreateDataReader();
                }
            }
        }
        #endregion
    }
}
