﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using FirstLook.Common.Core.Data;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Datastores
{
    class VehicleDetailsDataStore :IVehicleDetailsDataStore
    {
        #region Class Members

        private const string DatabaseName = "IMT";

        #endregion

        #region ISalesPeopleDataStore Members
        public System.Data.IDataReader GetAuctionVehicles(Entities.VehicleDetailsInput vehicleDetails)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "dbo.GetPurchasingCenterAuctionsVehicleForMobile";

                    Database.AddWithValue(command, "BusinessUnitId", vehicleDetails.DealerId, DbType.Int32);
                    Database.AddWithValue(command, "LocationId", vehicleDetails.LocationId, DbType.Int32);
                    Database.AddWithValue(command, "Date", vehicleDetails.Date, DbType.String);
                    Database.AddWithValue(command, "Lane", vehicleDetails.Lane, DbType.String);

                    var set = new DataSet();

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        var table = new DataTable("Vehicles");

                        table.Load(reader);

                        set.Tables.Add(table);
                    }

                    return set.CreateDataReader();
                }
            }
        }
        #endregion
    }
}
