﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Datastores
{
    interface ILaneDetailsDataStore
    {
        IDataReader GetAuctionLanes(LaneDetailsInput laneDetails);
    }
}
