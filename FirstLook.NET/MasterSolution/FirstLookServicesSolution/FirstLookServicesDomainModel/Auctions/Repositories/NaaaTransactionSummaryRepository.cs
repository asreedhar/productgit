﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Naaa;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model;
using FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Gateways;

namespace FirstLook.FirstLookServices.DomainModel.Auctions.Repositories
{
    class NaaaTransactionSummaryRepository : RepositoryBase, IRepository
    {
        #region IRepository Members

        public INaaaTransactionSummary FetchNaaaTransactionSummary(int uid, int area, int period, int? mileage)
        {
            var summary =  new NaaaTransactionSummaryGateway().FetchTransactionSummary(uid, area, period, mileage);

            return summary;
        }

        #endregion

        protected override string DatabaseName
        {
            get { return "AuctionNet"; }
        }

        protected override void OnBeginTransaction(IDataSession session)
        {
            // do nothing -- we do not audited creation of reference/client items
        }
    }
}
