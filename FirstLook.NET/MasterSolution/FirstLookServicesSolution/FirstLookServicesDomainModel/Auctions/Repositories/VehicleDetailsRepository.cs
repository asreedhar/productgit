﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model.Sale;
using FirstLook.FirstLookServices.DomainModel.Auctions.Repositories.Gateways;


namespace FirstLook.FirstLookServices.DomainModel.Auctions.Repositories
{
    public class VehicleDetailsRepository : IVehiclesRepository
    {

        public IList<Vehicle> VehicleDetails(Entities.VehicleDetailsInput vehicleDetail)
        {
            return new VehiclesGateway().Fetch(vehicleDetail);
        }
    }
}
