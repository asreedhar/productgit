using System;
using System.Runtime.Serialization;
using FirstLook.FirstLookServices.DomainModel.Utility;

namespace FirstLook.FirstLookServices.DomainModel.Exceptions
{
    [Serializable]
    public class InvalidInputException : Exception
    {
        public string InputField { get; private set; }
        public string InputValue { get; private set; }

        public InvalidInputException()
        {
            this.Data["code"] = ErrorCodeEnumeration.InvalidInput;
        }

        public InvalidInputException(string message)
            : base(message)
        {
            this.Data["code"] = ErrorCodeEnumeration.InvalidInput;
        }

        public InvalidInputException(string inputField, string inputValue, string message)
            : base(message)
        {
            this.Data["code"] = ErrorCodeEnumeration.InvalidInput;
            this.Data["badValue"] = inputValue;
            this.InputField = inputField;
            this.InputValue = inputValue;
        }
    }
}