﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.Exceptions
{
    public static class ExceptionExtensions
    {
        public static void SetCode( this Exception ex, int codeKey)
        {
            ex.Data["code"] = codeKey;
        }

        /// <summary>
        ///  returns 0 if there is NO code
        /// </summary>
        /// <returns>The code found or 0 if none. </returns>
        public static int Code(this Exception ex)
        {
            return ex.Data.Contains("code") ?  (int) ex.Data["code"] : 0;
        }

        public static void SetDevMsg( this Exception ex, string devMsg)
        {
            ex.Data["devMsg"] = devMsg;
        }

        public static string DevMsg(this Exception ex)
        {
            return ex.Data.Contains("devMsg") ? (string) ex.Data["devMsg"] : "";
        }
    }
}
