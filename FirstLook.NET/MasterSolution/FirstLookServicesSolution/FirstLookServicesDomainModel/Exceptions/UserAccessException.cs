using System;
using System.Runtime.Serialization;
using FirstLook.FirstLookServices.DomainModel.Utility;

namespace FirstLook.FirstLookServices.DomainModel.Exceptions
{
    [Serializable]
    public class UserAccessException : Exception
    {
        public string Resource { get; private set; }
        public string Reason { get; private set; }

        public UserAccessException()
        {
            this.Data["code"] = ErrorCodeEnumeration.UserAccessDeniedToResource;
        }

        public UserAccessException(string message)
            : base(message)
        {
            this.Data["code"] = ErrorCodeEnumeration.UserAccessDeniedToResource;
        }

        public UserAccessException(string resource, string reason, string message)
            : base(message)
        {
            this.Data["code"] = ErrorCodeEnumeration.UserAccessDeniedToResource;
            this.Resource = resource;
            this.Reason = reason;
        }

        protected UserAccessException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }
    }
}