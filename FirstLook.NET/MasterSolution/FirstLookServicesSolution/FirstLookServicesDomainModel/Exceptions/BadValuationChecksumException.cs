﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.FirstLookServices.DomainModel.Utility;

namespace FirstLook.FirstLookServices.DomainModel.Exceptions
{
    class BadValuationChecksumException : Exception
    {
       
        public BadValuationChecksumException(string checkSum)
            : base("Bad checksum:" + checkSum)
        {
            this.SetCode((int) ErrorCodeEnumeration.BookValuesChecksumDontMatch);
            this.Data["checkSum"] = checkSum;
            this.Data["badValue"] = checkSum;

        }
    }
}
