using System;
using System.Runtime.Serialization;
using FirstLook.FirstLookServices.DomainModel.Utility;

namespace FirstLook.FirstLookServices.DomainModel.Exceptions
{
    [Serializable]
    public class MissingInputException : Exception
    {
        public string InputField { get; private set; }

        public MissingInputException()
        {
            this.Data["code"] = ErrorCodeEnumeration.MissingInput;
        }

        public MissingInputException(string message)
            : base(message)
        {
            this.Data["code"] = ErrorCodeEnumeration.MissingInput;
        }

        public MissingInputException(string inputField, string message)
            : base(message)
        {
            this.Data["code"] = ErrorCodeEnumeration.MissingInput;
            this.InputField = inputField;
        }

        public MissingInputException(string message, Exception inner)
            : base(message, inner)
        {
            this.Data["code"] = ErrorCodeEnumeration.MissingInput;
        }

        protected MissingInputException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}