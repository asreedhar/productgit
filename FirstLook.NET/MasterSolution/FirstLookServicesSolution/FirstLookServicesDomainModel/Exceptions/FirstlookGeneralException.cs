﻿using System;
using FirstLook.FirstLookServices.DomainModel.Utility;

namespace FirstLook.FirstLookServices.DomainModel.Exceptions
{
    public class FirstlookGeneralException : Exception
    {
        public FirstlookGeneralException(string message)
            : base(message)
        {
            this.SetCode((int)ErrorCodeEnumeration.GeneralFirstlookError);
        }

        public FirstlookGeneralException(string message, string devMsg)
            : base(message)
        {
            this.SetCode((int)ErrorCodeEnumeration.GeneralFirstlookError);
            this.SetDevMsg(devMsg);
        }
    }
}
