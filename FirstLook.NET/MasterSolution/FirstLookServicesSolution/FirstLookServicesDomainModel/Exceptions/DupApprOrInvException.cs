﻿using System;
using FirstLook.FirstLookServices.DomainModel.Utility;

namespace FirstLook.FirstLookServices.DomainModel.Exceptions
{
    public class DupApprOrInvException : Exception
    {
        public DupApprOrInvException(string message)
            : base(message)
        {
            this.SetCode((int)ErrorCodeEnumeration.AppraisalOrInventoryExists);
        }
    }
}
