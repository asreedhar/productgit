﻿using System;
using FirstLook.FirstLookServices.DomainModel.Utility;

namespace FirstLook.FirstLookServices.DomainModel.Exceptions
{
   [Serializable]
   public class NoMobileBookDataFoundException:Exception
    {
        public NoMobileBookDataFoundException(string message): base(message)
        {
            this.SetCode((int)ErrorCodeEnumeration.NoMobileBookFound);
        }
    }
}
