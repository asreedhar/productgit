﻿using System;
using FirstLook.FirstLookServices.DomainModel.Utility;

namespace FirstLook.FirstLookServices.DomainModel.Exceptions
{
    [Serializable]
    public class NoDataFoundException : Exception //also extended by
    {
        public NoDataFoundException(string message) : base(message)
        {
            this.SetCode((int)ErrorCodeEnumeration.NoDataFound);
        }
    }
}