﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.FirstLookServices.DomainModel.DealerInfo.Model;
using FirstLook.FirstLookServices.DomainModel.DealerInfo.Repositories.DataStores;
using FirstLook.FirstLookServices.DomainModel.DealerInfo.Repositories.Entities;



namespace FirstLook.FirstLookServices.DomainModel.DealerInfo.Repositories.Gateways
{
    public class DealerInfoGateway : GatewayBase
    {
        public DealerDataInfo Fetch(DealerInfoInput dealerInfo)
        {
            string key = CreateCacheKey("" + dealerInfo.BusinessUnitId );
            
            DealerDataInfo dealerinfo = Cache.Get(key) as DealerDataInfo;

            if (dealerinfo == null)
            {
                
                var serializer = Resolve<ISerializer<DealerDataInfo>>();
                var distSerializer = Resolve<ISerializer<int>>();
                IList<int> distanceList = null;
                var datastore = Resolve<IDealerInfoDataStore>();

                using (IDataReader dataReader = datastore.DealerInfo(dealerInfo))
                {

                    IList<DealerDataInfo> items = serializer.Deserialize(dataReader);
                     dealerinfo = items.FirstOrDefault();

                }
                using (IDataReader dataReader = datastore.DistanceInfo(dealerInfo))
                {
                    distanceList = distSerializer.Deserialize(dataReader);

                    dealerinfo.DistanceList = new List<int>();

                    dealerinfo.DistanceList = distanceList;

                  
                    Remember(key, dealerinfo);
                }
                
            
            }
            return dealerinfo;
        }

      

    }
}
