﻿using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.DealerInfo.Model;

namespace FirstLook.FirstLookServices.DomainModel.DealerInfo.Repositories
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<DataStores.Module>();

            registry.Register<Serializers.Module>();

            registry.Register<IRepository, DealerInfoRepository>(ImplementationScope.Shared);

        }
    }
}
