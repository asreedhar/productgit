﻿using System.Data;
using FirstLook.FirstLookServices.DomainModel.DealerInfo.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.DealerInfo.Repositories.DataStores
{
    public interface IDealerInfoDataStore
    {
        IDataReader DealerInfo(DealerInfoInput DealerInfo);
        IDataReader DistanceInfo(DealerInfoInput dealerinfo);
       
    }
}
