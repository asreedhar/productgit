﻿using System.Data;
using System.Data.SqlClient;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.DealerInfo.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.DealerInfo.Repositories.DataStores
{
    public class DealerInfoDataStore : IDealerInfoDataStore
    {
        #region Class Members

        private const string DatabaseName = "IMT";

        #endregion

        public IDataReader DealerInfo(DealerInfoInput dealerinfo)
        {
            try
            {
                using (IDbConnection connection = Database.Connection(DatabaseName))
                {
                    using (IDbCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.CommandText = "dbo.DealerInfoFetch";

                        Database.AddWithValue(command, "BusinessUnitID", dealerinfo.BusinessUnitId, DbType.Int32);

                        var set = new DataSet();

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            var table = new DataTable("DealerInfo");

                            table.Load(reader);

                            set.Tables.Add(table);
                        }

                        return set.CreateDataReader();

                        
                    }
                }
            }
            catch (SqlException e)
            {
                throw e;
            }

        }
        public IDataReader DistanceInfo(DealerInfoInput dealerinfo)
        {
            try
            {
                using (IDbConnection connection = Database.Connection(DatabaseName))
                {
                    using (IDbCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.CommandText = "dbo.DealerInfoFetch";

                        Database.AddWithValue(command, "BusinessUnitID", dealerinfo.BusinessUnitId, DbType.Int32);

                        var set = new DataSet();

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            reader.NextResult();
                            var table = new DataTable("Distance");

                            table.Load(reader);

                            set.Tables.Add(table);
                        }

                        return set.CreateDataReader();


                    }
                }
            }
            catch (SqlException e)
            {
                throw e;
            }

        }
       
    }
}
