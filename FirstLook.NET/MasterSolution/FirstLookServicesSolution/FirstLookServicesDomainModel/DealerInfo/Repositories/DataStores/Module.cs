﻿using System;
using FirstLook.Common.Core.Registry;

namespace FirstLook.FirstLookServices.DomainModel.DealerInfo.Repositories.DataStores
{
    [Serializable]
    public class Module:IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IDealerInfoDataStore, DealerInfoDataStore>(ImplementationScope.Shared);
        }
    }
}
