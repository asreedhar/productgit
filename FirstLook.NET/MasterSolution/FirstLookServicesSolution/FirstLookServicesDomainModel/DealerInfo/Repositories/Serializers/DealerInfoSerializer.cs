﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.DealerInfo.Model;

namespace FirstLook.FirstLookServices.DomainModel.DealerInfo.Repositories.Serializers
{
    public class DealerInfoSerializer :Serializer<DealerDataInfo>
    {
        public override DealerDataInfo Deserialize(IDataRecord record)
        {
           DealerDataInfo obj = new DealerDataInfo()
            {
                DealerID = DataRecord.GetInt32(record, "BusinessUnitID",0),
                PingIIDefaultSearchRadius = DataRecord.GetInt32(record, "PingII_DefaultSearchRadius", 0),
                PingIIMaxSearchRadius = DataRecord.GetInt32(record, "PingII_MaxSearchRadius",0),
                PingIIGuideBookId = DataRecord.GetInt32(record, "PingII_GuideBookId",0),
                IsNewPing = DataRecord.GetInt32(record, "IsNewPing", 0),
                PingIIMarketListing = DataRecord.GetString(record, "PingII_MarketListing"),
                PingIIRedRange1Value1 = DataRecord.GetInt32(record, "PingII_RedRange1Value1", 0),
                PingIIRedRange1Value2 = DataRecord.GetInt32(record, "PingII_RedRange1Value2", 0),
                PingIIYellowRange1Value1 = DataRecord.GetInt32(record, "PingII_YellowRange1Value1", 0),
                PingIIYellowRange1Value2 = DataRecord.GetInt32(record, "PingII_YellowRange1Value2", 0),
                PingIIYellowRange2Value1 = DataRecord.GetInt32(record, "PingII_YellowRange2Value1", 0),
                PingIIYellowRange2Value2 = DataRecord.GetInt32(record, "PingII_YellowRange2Value2",0),
                PingIIGreenRange1Value1 = DataRecord.GetInt32(record, "PingII_GreenRange1Value1", 0),
                PingIIGreenRange1Value2 = DataRecord.GetInt32(record, "PingII_GreenRange1Value2",0),
                ZipCode = DataRecord.GetString(record, "ZipCode"),
                OwnerName = DataRecord.GetString(record, "OwnerName"),
                Latitude = DataRecord.GetDecimal(record, "Latitude",0),
                Longitude = DataRecord.GetDecimal(record, "Longitude",0),
                UnitCostThresholdLower = DataRecord.GetInt32(record, "UnitCostThresholdLower",0),
                UnitCostThresholdUpper = DataRecord.GetInt32(record, "UnitCostThresholdUpper",0),
                GuideBookID = DataRecord.GetInt32(record, "GuideBookID",0),
                BookOut = DataRecord.GetInt32(record, "BookOut",0),
                BookOutPreferenceId = DataRecord.GetInt32(record, "BookOutPreferenceId",0),
                UnitCostThreshold = DataRecord.GetDecimal(record, "UnitCostThreshold",0),
                GuideBook2Id = DataRecord.GetInt32(record,"GuideBook2Id",0),
                FavourableThreshold = DataRecord.GetInt32(record,"FavourableThreshold",0),
                OriginalMSRP = DataRecord.GetInt32(record,"OriginalMSRP",0),
                MarketAvgInternetPrice = DataRecord.GetInt32(record,"MarketAverageInternetPrice",0),
                EdmundsTrueMarketValue = DataRecord.GetInt32(record,"EdmundsTrueMarketValue",0),
                NADARetailValue = DataRecord.GetInt32(record,"NADARetailValue",0),
                KBBRetailValue = DataRecord.GetInt32(record,"KelleyBlueBookRetailValue",0),


            };
           return obj;
        }

           
        }

        
    }

