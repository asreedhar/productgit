﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;

namespace FirstLook.FirstLookServices.DomainModel.DealerInfo.Repositories.Serializers
{
     public class DistanceSerializer:Serializer<int>
    {
         public override int Deserialize(IDataRecord record)
         {
             return DataRecord.GetInt32(record, "Distance", 0);
         }

    }
}
