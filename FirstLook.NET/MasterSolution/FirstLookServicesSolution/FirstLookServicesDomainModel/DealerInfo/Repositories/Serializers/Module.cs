﻿using System;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.DealerInfo.Model;

namespace FirstLook.FirstLookServices.DomainModel.DealerInfo.Repositories.Serializers
{
    [Serializable]
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ISerializer<DealerDataInfo>, DealerInfoSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<int>, DistanceSerializer>(ImplementationScope.Shared);
        }
    }
}
