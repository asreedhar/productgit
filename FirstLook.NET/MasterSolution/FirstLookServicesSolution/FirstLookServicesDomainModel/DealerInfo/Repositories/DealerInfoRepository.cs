﻿using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.DealerInfo.Model;
using FirstLook.FirstLookServices.DomainModel.DealerInfo.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.DealerInfo.Repositories.Gateways;

namespace FirstLook.FirstLookServices.DomainModel.DealerInfo.Repositories
{
    public class DealerInfoRepository : RepositoryBase, IRepository
    {
        #region IRepository Members

        public DealerDataInfo FetchDealerDetails(int dealerId)
        {
            DealerInfoInput dealerinfoinput = new DealerInfoInput();
            dealerinfoinput.BusinessUnitId = dealerId;
            return new DealerInfoGateway().Fetch(dealerinfoinput);
        }

        #endregion

        protected override string DatabaseName
        {
            get { return "IMT"; }
        }

        protected override void OnBeginTransaction(IDataSession session)
        {
            // do nothing -- we do not audited creation of reference/client items
        }


    }
}
