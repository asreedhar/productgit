﻿using FirstLook.FirstLookServices.DomainModel.DealerInfo.Commands.TransferObjects;
using System.Linq;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.DealerInfo.Model
{
    public class Mapper
    {
        public static DealerInfoDto Map(DealerDataInfo objs)
        {
            DealerInfoDto dealerinfodto = new DealerInfoDto();
            dealerinfodto.DealerID = objs.DealerID;
            dealerinfodto.PingIIDefaultSearchRadius = objs.PingIIDefaultSearchRadius;
            dealerinfodto.PingIIMaxSearchRadius = objs.PingIIMaxSearchRadius;
            dealerinfodto.PingIIGuideBookId = objs.PingIIGuideBookId;
            dealerinfodto.IsNewPing = objs.IsNewPing;
            dealerinfodto.PingIIMarketListing = objs.PingIIMarketListing;
            dealerinfodto.PingIIRedRange1Value1 = objs.PingIIRedRange1Value1;
            dealerinfodto.PingIIRedRange1Value2 = objs.PingIIRedRange1Value2;
            dealerinfodto.PingIIYellowRange1Value1 = objs.PingIIYellowRange1Value1;
            dealerinfodto.PingIIYellowRange1Value2 = objs.PingIIYellowRange1Value2;
            dealerinfodto.PingIIYellowRange2Value1 = objs.PingIIYellowRange2Value1;
            dealerinfodto.PingIIYellowRange2Value2 = objs.PingIIYellowRange2Value2;
            dealerinfodto.PingIIGreenRange1Value1 = objs.PingIIGreenRange1Value1;
            dealerinfodto.PingIIGreenRange1Value2 = objs.PingIIGreenRange1Value2;
            dealerinfodto.ZipCode = objs.ZipCode;
            dealerinfodto.OwnerName = objs.OwnerName;
            dealerinfodto.Latitude = objs.Latitude;
            dealerinfodto.Longitude = objs.Longitude;
            dealerinfodto.UnitCostThresholdLower = objs.UnitCostThresholdLower;
            dealerinfodto.UnitCostThresholdUpper = objs.UnitCostThresholdUpper;
            dealerinfodto.GuideBookID = objs.GuideBookID;
            dealerinfodto.BookOut = objs.BookOut;
            dealerinfodto.UnitCostThreshold = objs.UnitCostThreshold;
            dealerinfodto.GuideBook2Id = objs.GuideBook2Id;
            dealerinfodto.DistanceList = new List<int>();
            objs.DistanceList.ToList().ForEach(obj => dealerinfodto.DistanceList.Add(obj));

            dealerinfodto.FavourableThreshold = objs.FavourableThreshold;
            dealerinfodto.OriginalMSRP = objs.OriginalMSRP;
            dealerinfodto.MarketAvgInternetPrice = objs.MarketAvgInternetPrice;
            dealerinfodto.EdmundsTrueMarketValue = objs.EdmundsTrueMarketValue;
            dealerinfodto.NADARetailValue = objs.NADARetailValue;
            dealerinfodto.KBBRetailValue = objs.KBBRetailValue;

            return dealerinfodto;

        }
    }
}
