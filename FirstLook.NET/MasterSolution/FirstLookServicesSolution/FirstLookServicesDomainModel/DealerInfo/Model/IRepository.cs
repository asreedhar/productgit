﻿
namespace FirstLook.FirstLookServices.DomainModel.DealerInfo.Model
{
    public interface IRepository
    {
        DealerDataInfo FetchDealerDetails(int dealerId);
   
    }
}
