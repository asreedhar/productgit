﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.DealerInfo.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.DealerInfo.Commands
{
    public interface ICommandFactory
    {
        ICommand<DealerInfoResultsDto, IdentityContextDto<DealerInfoArgumentsDto>> CreateDealerInfoCommand();
    }
}
