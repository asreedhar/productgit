﻿using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.DealerInfo.Commands.TransferObjects.Envelopes
{
    public class DealerInfoResultsDto
    {
        public PrincipalDto Principal { get; set; }

        public DealerInfoArgumentsDto Arguments { get; set; }

        public DealerInfoDto DealerInfo { get; set; }

    }
}
