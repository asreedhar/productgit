﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.DealerInfo.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.DealerInfo.Commands.Impl
{
    public class CommandFactory:ICommandFactory
    {
        public ICommand<DealerInfoResultsDto, IdentityContextDto<DealerInfoArgumentsDto>> CreateDealerInfoCommand()
        {
            return new FetchDealerInfoCommand();
        }
    }
}
