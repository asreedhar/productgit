﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.DealerInfo.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.DealerInfo.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.DealerInfo.Model;
using IRepository = FirstLook.FirstLookServices.DomainModel.DealerInfo.Model.IRepository;

namespace FirstLook.FirstLookServices.DomainModel.DealerInfo.Commands.Impl
{
    public class FetchDealerInfoCommand : ICommand<DealerInfoResultsDto, IdentityContextDto<DealerInfoArgumentsDto>>
    {
        #region ICommand<DealerInfoResultsDto,IdentityContextDto<DealerInfoArgumentsDto>> Members


        public DealerInfoResultsDto Execute(IdentityContextDto<DealerInfoArgumentsDto> parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();
            var dealerRepository = resolver.Resolve<IRepository>();

            DealerInfoArgumentsDto fetch = new DealerInfoArgumentsDto()
                                          {
                                              BusinessUnitId = parameters.Arguments.BusinessUnitId,
                                             
                                          };
            DealerDataInfo detail = dealerRepository.FetchDealerDetails(parameters.Arguments.BusinessUnitId);

            DealerInfoDto dealerinfodto = Mapper.Map(detail);
            
            return new DealerInfoResultsDto()
            {
                DealerInfo = dealerinfodto 
            };


        }
        #endregion
    }
    }
    


        