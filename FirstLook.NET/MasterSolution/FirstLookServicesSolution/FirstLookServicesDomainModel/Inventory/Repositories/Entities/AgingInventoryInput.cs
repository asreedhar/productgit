﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.Inventory.Repositories.Entities
{
   public class AgingInventoryInput
    {
        public int BusinessUnitId { get; set; }
        public string StockNumber { get; set; }
    }
}
