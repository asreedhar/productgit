﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.FirstLookServices.DomainModel.Inventory.Model;
using FirstLook.Common.Core.Registry;
namespace FirstLook.FirstLookServices.DomainModel.Inventory.Repositories
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<Datastores.Module>();

            registry.Register<Serializers.Module>();

            registry.Register<IRepository, AgingInventoryRepository>(ImplementationScope.Shared);
        }
    }
}
