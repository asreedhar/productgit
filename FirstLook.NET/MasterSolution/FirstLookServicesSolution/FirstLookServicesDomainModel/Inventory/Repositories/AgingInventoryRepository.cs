﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.FirstLookServices.DomainModel.Inventory.Model;
using FirstLook.FirstLookServices.DomainModel.Inventory.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.Inventory.Repositories.Gateways;

namespace FirstLook.FirstLookServices.DomainModel.Inventory.Repositories
{
    public class AgingInventoryRepository : IRepository
    {
        public IList<AgingInventoryDetail> AgingInventory(AgingInventoryInput agingInventory)
        {
            return new AgingInventoryGateway().Fetch(agingInventory);
        }
    }
}
