﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using FirstLook.FirstLookServices.DomainModel.Inventory.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.Inventory.Repositories.Datastores
{
    public interface IAgingInventoryDataStore
    {
        IDataReader AgingInventory(AgingInventoryInput agingInventory);
    }
}
