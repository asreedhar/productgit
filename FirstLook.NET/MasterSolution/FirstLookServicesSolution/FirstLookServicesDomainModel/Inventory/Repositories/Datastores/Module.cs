﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.Registry;

namespace FirstLook.FirstLookServices.DomainModel.Inventory.Repositories.Datastores
{
    [Serializable]
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IAgingInventoryDataStore, AgingInventoryDataStore>(ImplementationScope.Shared);
        }
    }
}
