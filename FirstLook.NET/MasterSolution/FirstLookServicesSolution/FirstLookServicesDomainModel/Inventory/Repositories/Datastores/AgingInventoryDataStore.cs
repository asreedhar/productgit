﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Data;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.Inventory.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.Exceptions;

namespace FirstLook.FirstLookServices.DomainModel.Inventory.Repositories.Datastores
{
    public class AgingInventoryDataStore : IAgingInventoryDataStore
    {
        #region Class Members

        private const string DatabaseName = "FLDW";

        #endregion

        #region ISalesPeopleDataStore Members


        public IDataReader AgingInventory(AgingInventoryInput agingInventory)
        {
             using (IDbConnection connection = Database.Connection(DatabaseName))
                {
                    using (IDbCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.CommandText = "dbo.Fetch#AgingInventoryData";

                        Database.AddWithValue(command, "BusinessUnitID", agingInventory.BusinessUnitId, DbType.Int32);
                        Database.AddWithValue(command, "StockNumber", agingInventory.StockNumber, DbType.String);

                        var set = new DataSet();

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            var table = new DataTable("AgingInventory");

                            table.Load(reader);

                            set.Tables.Add(table);
                        }

                        return set.CreateDataReader();
                    }
                }
            
           
        }
        #endregion

    }
}
