﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.FirstLookServices.DomainModel.Inventory.Model;
using FirstLook.FirstLookServices.DomainModel.Inventory.Repositories.Datastores;
using FirstLook.FirstLookServices.DomainModel.Inventory.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.Inventory.Repositories.Gateways
{
    public class AgingInventoryGateway : GatewayBase
    {
        public IList<AgingInventoryDetail> Fetch(AgingInventoryInput agingInventory)
        {
            string key = CreateCacheKey("" + agingInventory.BusinessUnitId);

            IList<AgingInventoryDetail> value = Cache.Get(key) as List<AgingInventoryDetail>;

            if (value == null)
            {
                var serializer = Resolve<ISerializer<AgingInventoryDetail>>();

                var datastore = Resolve<IAgingInventoryDataStore>();

                using (IDataReader reader = datastore.AgingInventory(agingInventory))
                {
                    IList<AgingInventoryDetail> items = serializer.Deserialize(reader);

                    value = items;

                    Remember(key, value);
                }
            }

            return value;
        }
    }
}
