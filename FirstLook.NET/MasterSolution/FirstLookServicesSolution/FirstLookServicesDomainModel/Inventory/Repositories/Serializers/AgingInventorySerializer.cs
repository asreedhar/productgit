﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.Inventory.Model;
using FirstLook.FirstLookServices.DomainModel.MMRTransactionService;

namespace FirstLook.FirstLookServices.DomainModel.Inventory.Repositories.Serializers
{
    public class AgingInventorySerializer : Serializer<AgingInventoryDetail>
    {
        public override AgingInventoryDetail Deserialize(IDataRecord record)
        {

            string stockNumber = string.Empty;
            string name = string.Empty;
            string vin = string.Empty;
            int year = 0;
            string makeModel = string.Empty;
            string trim = string.Empty;
            int ageInDays = 0;
            string color = string.Empty;
            int mileage = 0;
            decimal listPrice = 0;
            InventoryRiskLight light = InventoryRiskLight.Undefined;
            string riskLight = string.Empty;
            decimal unitCost = 0;
            int inventoryId = 0;
           
            


            try
            {
                stockNumber = DataRecord.GetString(record, "StockNumber");
                inventoryId = DataRecord.GetInt32(record, "InventoryId",0);
                name = DataRecord.GetString(record, "Description");
                vin = DataRecord.GetString(record, "VIN");
                year = DataRecord.GetInt32(record, "Year",0);
                makeModel = DataRecord.GetString(record, "MakeModel");
                trim = DataRecord.GetString(record, "Trim");
                ageInDays = DataRecord.GetInt32(record, "AgeInDays",0);
                color = DataRecord.GetString(record, "Color");
                mileage = DataRecord.GetInt32(record, "MileageReceived", 0);
                int ordinal = record.GetOrdinal("ListPrice");

                if (record.IsDBNull(ordinal))
                {
                    listPrice = 0;
                }
                else
                    listPrice = Convert.ToDecimal(record.GetValue(ordinal));

                int ordinal1 = record.GetOrdinal("RiskLight");

                if (record.IsDBNull(ordinal1))
                {
                    light = 0;
                }
                else
                    light = (InventoryRiskLight)Convert.ToInt16(record.GetValue(ordinal1));
                riskLight = light.ToString().ToLower();
                int ordinal2 = record.GetOrdinal("UnitCost");

                if (record.IsDBNull(ordinal2))
                {
                    unitCost = 0;
                }
                else
                    unitCost = Convert.ToDecimal(record.GetValue(ordinal2));



            }
            catch (Exception ex)
            {
                throw ex;
            }

            return new AgingInventoryDetail
            {
                BucketName = name,
                Vin =vin,
                Year = year,
                MakeModel = makeModel,
                Trim=trim,
                AgeInDays = ageInDays,
                Color = color,
                Mileage = mileage,
                ListPrice = listPrice,
                RiskLight=riskLight,
                UnitCost = unitCost,
                InventoryId=inventoryId,
                StockNumber = stockNumber
            };
        }
    }
}
