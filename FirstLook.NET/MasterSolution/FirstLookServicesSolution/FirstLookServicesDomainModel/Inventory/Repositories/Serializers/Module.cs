﻿using System;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Inventory.Model;

namespace FirstLook.FirstLookServices.DomainModel.Inventory.Repositories.Serializers
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ISerializer<AgingInventoryDetail>, AgingInventorySerializer>(ImplementationScope.Shared);
        }
    }
}
