﻿

namespace FirstLook.FirstLookServices.DomainModel.Inventory.Model
{
 
    public enum InventoryRiskLight
    {
        Undefined = 0,
        Red = 1,
        Yellow = 2,
        Green = 3
    }
}
