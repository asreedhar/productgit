﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.FirstLookServices.DomainModel.Inventory.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.Inventory.Model
{
    public interface IRepository
    {
        IList<AgingInventoryDetail> AgingInventory(AgingInventoryInput agingInventory);
    }
}
