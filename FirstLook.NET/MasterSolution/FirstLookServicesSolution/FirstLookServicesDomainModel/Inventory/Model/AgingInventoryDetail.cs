﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.Inventory.Model
{
  public  class AgingInventoryDetail
    {
        public string BucketName { get; set; }
        public string Vin { get; set; }
        public int Year { get; set; }
        public string MakeModel { get; set; }
        public string Trim { get; set; }
        public int AgeInDays { get; set; }
        public string Color { get; set; }
        public int Mileage { get; set; }
        public decimal ListPrice { get; set; }
        public string RiskLight { get; set; }
        public decimal UnitCost { get; set; }
        public int InventoryId { get; set; }
        public string StockNumber { get; set; }

    }
}
