﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.Inventory.Commands.TransferObjects.Envelopes
{
    public class FetchAgingInventoryArgumentsDto
    {
        public int BusinessUnitId { get; set; }
        public string StockNumber { get; set; }
    }
}
