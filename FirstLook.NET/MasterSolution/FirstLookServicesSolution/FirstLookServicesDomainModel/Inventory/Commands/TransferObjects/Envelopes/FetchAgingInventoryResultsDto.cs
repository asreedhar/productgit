﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.Inventory.Commands.TransferObjects.Envelopes
{
   public class FetchAgingInventoryResultsDto
    {
        public PrincipalDto Principal { get; set; }

        public FetchAgingInventoryArgumentsDto Arguments { get; set; }

        public IList<AgingInventoryDto> AgingInventory { get; set; }
    }
}
