﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Amazon.S3.Model;
using FirstLook.FirstLookServices.DomainModel.Inventory.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Inventory.Model;

namespace FirstLook.FirstLookServices.DomainModel.Inventory.Commands.Impl
{
    public static class Mapper
    {
        public static IList<AgingInventoryDto> Map(IList<AgingInventoryDetail> objs)
        {
            IList<AgingInventoryDto> agingInventoryDto = objs.Select(obj => new AgingInventoryDto
            {
                BucketName = obj.BucketName,
                Vin = obj.Vin,
                Year = obj.Year,
                MakeModel = obj.MakeModel,
                Trim = obj.Trim,
                AgeInDays = obj.AgeInDays,
                Color = obj.Color,
                Mileage = obj.Mileage,
                ListPrice = obj.ListPrice,
                RiskLight=obj.RiskLight,
                UnitCost = obj.UnitCost,
                InventoryId =obj.InventoryId,
                StockNumber = obj.StockNumber
                
            }).ToList();
            return agingInventoryDto;

        }
        //method to separatebucketwise



      


    }
}
