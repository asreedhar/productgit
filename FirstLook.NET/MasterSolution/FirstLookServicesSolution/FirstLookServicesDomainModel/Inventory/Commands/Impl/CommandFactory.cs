﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.Inventory.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.Inventory.Commands.Impl
{
    public class CommandFactory : ICommandFactory
    {
        #region IcommandFactory Members
        public ICommand<FetchAgingInventoryResultsDto, IdentityContextDto<FetchAgingInventoryArgumentsDto>> CreateFetchAgingInventoryCommand()
        {
            return new FetchAgingInventoryCommand();
        }
        #endregion
    }
}
