﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Inventory.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Inventory.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Inventory.Model;
using FirstLook.FirstLookServices.DomainModel.Inventory.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.Validation;
using FirstLook.FirstLookServices.DomainModel.Exceptions;


namespace FirstLook.FirstLookServices.DomainModel.Inventory.Commands.Impl
{

    public class FetchAgingInventoryCommand : ICommand<FetchAgingInventoryResultsDto, IdentityContextDto<FetchAgingInventoryArgumentsDto>>
    {
        #region ICommand<FetchAgingInventoryResultsDto,IdentityContextDto<FetchAgingInventoryArgumentsDto>> Members

        public FetchAgingInventoryResultsDto Execute(IdentityContextDto<FetchAgingInventoryArgumentsDto> parameters)
        {

            IResolver resolver = RegistryFactory.GetResolver();
            var agingInventoryRepository = resolver.Resolve<IRepository>();
            var paramId = parameters.Arguments.BusinessUnitId;
            Validators.CheckDealerId(paramId, parameters);
            AgingInventoryInput agingInventoryInput = new AgingInventoryInput()
            {
                BusinessUnitId = parameters.Arguments.BusinessUnitId,
                StockNumber = parameters.Arguments.StockNumber
            };
            IList<AgingInventoryDetail> agingList = agingInventoryRepository.AgingInventory(agingInventoryInput);


            if (agingList.Count == 0)
                throw new InvalidInputException("Stock Number", parameters.Arguments.StockNumber,"");
          
                IList<AgingInventoryDto> agingInventory = Mapper.Map(agingList);
                return new FetchAgingInventoryResultsDto()
                {
                    AgingInventory = agingInventory
                };
        }
        
        #endregion
    }
}
