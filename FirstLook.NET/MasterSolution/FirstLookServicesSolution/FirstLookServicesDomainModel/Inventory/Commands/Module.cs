﻿using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Inventory.Commands.Impl;

namespace FirstLook.FirstLookServices.DomainModel.Inventory.Commands
{
    public class Module : IModule
    {
        #region IModule Members

        public void Configure(IRegistry registry)
        {
            registry.Register<ICommandFactory, CommandFactory>(ImplementationScope.Shared);
        }

        #endregion
    }
}
