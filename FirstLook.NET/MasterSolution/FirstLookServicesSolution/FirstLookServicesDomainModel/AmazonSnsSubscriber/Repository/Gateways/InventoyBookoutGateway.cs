﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Model;
using FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Repository.Datastores;

namespace FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Repository.Gateways
{
    public class InventoyBookoutGateway:GatewayBase
    {
        public InventoryBookout UpdateInventoryBookout(int inventoryId)
        {
            var serializer = Resolve<ISerializer<InventoryBookout>>();

            var datastore = Resolve<IInventoryBookoutDatastore>();

            using (IDataReader reader = datastore.UpdateInventoryBookout(inventoryId))
            {
                if (reader.Read())
                {
                    return serializer.Deserialize((IDataRecord)reader);
                }
            }

            return null;
        }
    }
}
