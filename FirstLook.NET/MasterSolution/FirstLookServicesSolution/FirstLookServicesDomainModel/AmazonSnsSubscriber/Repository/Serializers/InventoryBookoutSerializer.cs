﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Model;

namespace FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Repository.Serializers
{
    public class InventoryBookoutSerializer : Serializer<InventoryBookout>
    {
        public override InventoryBookout Deserialize(IDataRecord record)
        {
            int? bookoutRequired = record.GetNullableInt32("BookoutRequired");

            return new InventoryBookout(){BookoutRequired =  bookoutRequired==null?0:(int)bookoutRequired };
        }
    }
}
