﻿using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Model;
using FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Repository.Gateways;

namespace FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Repository
{
    public class InventoryBookoutRepository : RepositoryBase, IInventoryBookoutRepository
    {
        #region IInventoryBookoutRepository Members

        public InventoryBookout UpdateInventoryBookout(int inventoryId)
        {
            return DoInTransaction(() => new InventoyBookoutGateway().UpdateInventoryBookout(inventoryId));
        }

        #endregion

        protected override string DatabaseName
        {
            get { return "IMT"; }
        }

        protected override void OnBeginTransaction(IDataSession session)
        {
           
        }
    }
}
