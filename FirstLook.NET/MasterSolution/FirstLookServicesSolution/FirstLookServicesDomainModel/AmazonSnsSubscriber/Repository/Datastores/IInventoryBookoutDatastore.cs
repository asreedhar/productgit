﻿using System.Data;

namespace FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Repository.Datastores
{
    public interface IInventoryBookoutDatastore
    {
        #region InventoryBookout

        IDataReader UpdateInventoryBookout(int inventoryId);

        #endregion

    }
}
