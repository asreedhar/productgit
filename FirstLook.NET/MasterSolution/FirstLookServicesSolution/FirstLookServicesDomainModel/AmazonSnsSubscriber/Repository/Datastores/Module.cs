﻿using System;
using FirstLook.Common.Core.Registry;

namespace FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Repository.Datastores
{
    [Serializable]
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IInventoryBookoutDatastore, InventoryBookoutDatastore>(ImplementationScope.Shared);
        }
    }
}
