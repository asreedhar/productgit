﻿using System.Data;
using System.Reflection;
using FirstLook.Common.Core.Data;

namespace FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Repository.Datastores
{
    public class InventoryBookoutDatastore : SimpleDataStore, IInventoryBookoutDatastore
    {
        /// <summary>
        /// Get the type of the current assembly.
        /// </summary>
        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }        
        protected override string DatabaseName
        {
            get { return "IMT"; }
        }
        
        #region IInventoryBookoutDatastore Members

        public IDataReader UpdateInventoryBookout(int inventoryId)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "dbo.InventoryBookout#Update";

                    Database.AddWithValue(command, "InventoryId", inventoryId, DbType.Int32);

                    var set = new DataSet();

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        var table = new DataTable();

                        table.Load(reader);

                        set.Tables.Add(table);
                    }

                    return set.CreateDataReader();
                }
            }
        }

        #endregion
    }
}
