﻿using FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Model;

namespace FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Repository
{
    public interface IInventoryBookoutRepository
    {
        InventoryBookout UpdateInventoryBookout(int inventoryId);
    }
}
