﻿using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Model;
using FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Repository.Serializers;

namespace FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Repository
{
    public class Module:IModule
    {
        #region IModule Members

        public void Configure(IRegistry registry)
        {
            registry.Register<IInventoryBookoutRepository, InventoryBookoutRepository>(ImplementationScope.Shared);
            registry.Register<ISerializer<InventoryBookout>,InventoryBookoutSerializer>(ImplementationScope.Shared);
            registry.Register<Datastores.Module>();
        }

        #endregion
    }
}
