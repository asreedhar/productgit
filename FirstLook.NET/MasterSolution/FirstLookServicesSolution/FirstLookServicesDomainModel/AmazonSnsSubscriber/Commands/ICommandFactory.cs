﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Commands
{
    public interface ICommandFactory
    {
        ICommand<SubscriberCommandResultDto, IdentityContextDto<SubscriberCommandArgumentDto>> CreateSubscribeCommand();

        ICommand<InventoryBookoutCommandResultDto, IdentityContextDto<InventoryBookoutCommandArgumentDto>> CreateInventoryBookoutCommand();
    }
}
