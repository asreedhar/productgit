﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Commands.Impl;
using FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Commands
{
    public class CommandFactory:ICommandFactory
    {

        #region ICommandFactory Members

        ICommand<SubscriberCommandResultDto, IdentityContextDto<SubscriberCommandArgumentDto>> ICommandFactory.CreateSubscribeCommand()
        {
            return new SubscribeCommand();
        }

        public ICommand<InventoryBookoutCommandResultDto, IdentityContextDto<InventoryBookoutCommandArgumentDto>> CreateInventoryBookoutCommand()
        {
            return new InventoryBookoutCommand();
        }

        #endregion
    }
}
