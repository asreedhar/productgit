﻿
namespace FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Commands.TransferObjects
{
    public class SubscriberCommandArgumentDto
    {
        public string Url { get; set; }
    }
}
