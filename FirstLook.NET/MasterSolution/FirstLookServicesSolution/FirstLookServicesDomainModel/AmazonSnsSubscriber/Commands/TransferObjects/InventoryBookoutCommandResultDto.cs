﻿
namespace FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Commands.TransferObjects
{
    public class InventoryBookoutCommandResultDto
    {
        public int BookoutRequired { get; set; }
    }
}
