﻿using System.Net;

namespace FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Commands.TransferObjects
{
    public class SubscriberCommandResultDto
    {
        public HttpStatusCode HttpStatusCode { get; set; }
        public string  Result { get; set; }
    }
}
