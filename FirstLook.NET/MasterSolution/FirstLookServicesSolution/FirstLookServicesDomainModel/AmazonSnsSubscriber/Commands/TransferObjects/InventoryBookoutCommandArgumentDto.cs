﻿
namespace FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Commands.TransferObjects
{
    public class InventoryBookoutCommandArgumentDto
    {
        public int InventoryId { get; set; }
        public int BusinessUnitId { get; set; }
        public string UserName { get; set; }     
    }
}
