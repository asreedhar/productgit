﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Model;
using FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Repository;

namespace FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Commands.Impl
{
    public class InventoryBookoutCommand : ICommand<InventoryBookoutCommandResultDto, IdentityContextDto<InventoryBookoutCommandArgumentDto>>
    {

        #region ICommand<InventoryBookoutCommandResultDto,InventoryBookoutCommandArgumentDto> Members

        public InventoryBookoutCommandResultDto Execute(IdentityContextDto<InventoryBookoutCommandArgumentDto> parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            var inventoryBookoutRepositoryRepository = resolver.Resolve<IInventoryBookoutRepository>();

            InventoryBookout inventoryBookout= inventoryBookoutRepositoryRepository.UpdateInventoryBookout(parameters.Arguments.InventoryId);

            return new InventoryBookoutCommandResultDto() { BookoutRequired = inventoryBookout.BookoutRequired};

        }

        #endregion
    }
}
