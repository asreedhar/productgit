﻿using System;
using System.IO;
using System.Net;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Commands.Impl
{
    public class SubscribeCommand : ICommand<SubscriberCommandResultDto, IdentityContextDto<SubscriberCommandArgumentDto>>
    {
        #region ICommand<SubscriberCommandResultDto,SubscriberCommandArgumentDto> Members

        public SubscriberCommandResultDto Execute(IdentityContextDto<SubscriberCommandArgumentDto> parameters)
        {
            SubscriberCommandResultDto result=new SubscriberCommandResultDto();
            try
            {
                HttpWebRequest request = (HttpWebRequest) WebRequest.Create(parameters.Arguments.Url);
                request.Method = "GET";
                HttpWebResponse response=(HttpWebResponse) request.GetResponse();
                result.HttpStatusCode = response.StatusCode;
                Stream stream = response.GetResponseStream();

                if(stream!=null)
                    result.Result = new StreamReader(stream).ReadToEnd();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;

        }

        #endregion
    }
}
