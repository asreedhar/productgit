﻿using System.Data;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.ApproveAd.Repositories.Entities;
using System.Data.SqlClient;

namespace FirstLook.FirstLookServices.DomainModel.ApproveAd.Repositories.DataStores
{

    public class ApproveAdDataStore : IApproveAdDataStore
    {
        #region Class Members

        private const string DatabaseName = "Merchandising";

        #endregion

        #region IApproveAdDataStore Members

        public bool StoreListPrice(ApproveAdInput approveAd)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "postings.SscheduleListPrice#Update";
                 
                    Database.AddWithValue(command, "BusinessUnitID", approveAd.Businessunitid, DbType.Int32);
                    Database.AddWithValue(command, "InventoryId", approveAd.InventoryId, DbType.Int32);
                    Database.AddWithValue(command, "ApproverLogin", approveAd.UserName, DbType.String);
                    Database.AddWithValue(command, "ListPrice", approveAd.FlListPrice, DbType.Decimal);
                    Database.AddWithValue(command, "SpecialPrice", approveAd.SpecialPrice, DbType.Decimal);

                    SqlParameter outputIdParam = new SqlParameter("@RecordsAffected", SqlDbType.Int)
                    {
                        Direction = ParameterDirection.Output
                    };

                    command.Parameters.Add(outputIdParam);


                    int AdsAffected = 0;

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        AdsAffected = int.Parse(outputIdParam.Value.ToString());
                    }

                    return (AdsAffected>=1)? true:false;
                }
            }
        }
        #endregion

    }
}
