﻿using System.Data;
using FirstLook.FirstLookServices.DomainModel.ApproveAd.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.ApproveAd.Repositories.DataStores
{
    public interface IApproveAdDataStore
    {
        bool StoreListPrice(ApproveAdInput approveAd);
    }
}
