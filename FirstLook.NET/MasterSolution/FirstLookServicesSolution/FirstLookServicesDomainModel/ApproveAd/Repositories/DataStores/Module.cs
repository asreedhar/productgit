﻿using System;
using FirstLook.Common.Core.Registry;


namespace FirstLook.FirstLookServices.DomainModel.ApproveAd.Repositories.DataStores
{
    [Serializable]
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IApproveAdDataStore, ApproveAdDataStore>(ImplementationScope.Shared);
        }
    }
}
