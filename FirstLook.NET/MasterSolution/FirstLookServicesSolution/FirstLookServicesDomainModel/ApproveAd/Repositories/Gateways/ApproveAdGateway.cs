﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.FirstLookServices.DomainModel.ApproveAd.Model;
using FirstLook.FirstLookServices.DomainModel.ApproveAd.Repositories.DataStores;
using FirstLook.FirstLookServices.DomainModel.ApproveAd.Repositories.Entities;


namespace FirstLook.FirstLookServices.DomainModel.ApproveAd.Repositories.Gateways
{
   public class ApproveAdGateway:GatewayBase
    {
       public bool Save(ApproveAdInput approveAd)
       {
           
            var datastore = Resolve<IApproveAdDataStore>();

            bool status = false;

            status = datastore.StoreListPrice(approveAd);

            return status;
       }
    }
}
