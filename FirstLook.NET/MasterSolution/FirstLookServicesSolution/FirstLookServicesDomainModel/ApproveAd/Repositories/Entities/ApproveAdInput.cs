﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.ApproveAd.Repositories.Entities
{
   public class ApproveAdInput
    {
        public int Businessunitid { get; set; }
        public int InventoryId { get; set; }
        public string UserName { get; set; }
        public decimal SpecialPrice { get; set; }
        public decimal FlListPrice { get; set; }
    }
}
