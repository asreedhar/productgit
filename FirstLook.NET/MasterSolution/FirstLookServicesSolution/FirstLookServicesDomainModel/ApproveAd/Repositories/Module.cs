﻿using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.ApproveAd.Model;


namespace FirstLook.FirstLookServices.DomainModel.ApproveAd.Repositories
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<DataStores.Module>();

            registry.Register<IRepository, ApproveAdRepository>(ImplementationScope.Shared);
        }
    }
}
