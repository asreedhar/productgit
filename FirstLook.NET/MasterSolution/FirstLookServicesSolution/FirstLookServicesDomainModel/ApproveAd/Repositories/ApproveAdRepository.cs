﻿using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.ApproveAd.Model;
using FirstLook.FirstLookServices.DomainModel.ApproveAd.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.ApproveAd.Repositories.Gateways;


namespace FirstLook.FirstLookServices.DomainModel.ApproveAd.Repositories
{
  public  class ApproveAdRepository:IRepository
    {
      public bool ApproveAd(ApproveAdInput approveAd)
        {
            return new ApproveAdGateway().Save(approveAd);
        }
    }
}
