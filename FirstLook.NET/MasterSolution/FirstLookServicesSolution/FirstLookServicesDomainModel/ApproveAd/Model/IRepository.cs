﻿using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.ApproveAd.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.ApproveAd.Model
{
   public interface IRepository
   {
       bool ApproveAd(ApproveAdInput ApproveAdinput);

   }
}
