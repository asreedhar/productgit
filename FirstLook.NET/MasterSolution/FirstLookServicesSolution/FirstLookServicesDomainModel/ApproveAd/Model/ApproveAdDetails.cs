﻿
namespace FirstLook.FirstLookServices.DomainModel.ApproveAd.Model
{
    public class ApproveAdDetails
    {
        public int Businessunitid { get; set; }
        public int InventoryId { get; set; }
        public string UserName { get; set; }
    }
}
