﻿using FirstLook.Common.Core.Registry;
//using FirstLook.Merchandising.DomainModel;
using FirstLook.Merchandising.DomainModel;
using Autofac;
using Merchandising.Messages;
using FirstLook.Common.Core.IOC;
using System.Web;

namespace FirstLook.FirstLookServices.DomainModel.ApproveAd
{


    public class Module : IModule
    {
        #region IModule Members

        /// <summary>
        /// Add pertinent interfaces and impelementing classes to the registry.
        /// </summary>
        public void Configure(IRegistry registry)
        {
            registry.Register<Commands.Module>();
            registry.Register<Repositories.Module>();
        }

        #endregion
    }
}
