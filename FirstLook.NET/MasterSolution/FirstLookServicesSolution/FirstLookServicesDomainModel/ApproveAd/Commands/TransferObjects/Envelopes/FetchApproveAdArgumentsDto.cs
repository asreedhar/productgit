﻿using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;
using System;

namespace FirstLook.FirstLookServices.DomainModel.ApproveAd.Commands.TransferObjects.Envelopes
{
    public class FetchApproveAdArgumentsDto
    {
        public PrincipalDto Principal { get; set; }
        public int Businessunitid { get; set; }
        public int InventoryId { get; set; }
        public decimal SpecialPrice { get; set; }
        public decimal FlListPrice { get; set; }
        public string Username { get; set; }
    }
}
