﻿
namespace FirstLook.FirstLookServices.DomainModel.ApproveAd.Commands.TransferObjects.Envelopes
{
    public class FetchApproveAdResultsDto
    {
        public bool Result { get; set; }
    }
}
