﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.ApproveAd.Commands.TransferObjects
{
    public class RegenerateAdDto
    {
        public string ApprovalStatus { get; set; }
    }
}
