﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.ApproveAd.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.ApproveAd.Commands
{
   public interface ICommandFactory
   {
       ICommand<UpdateListPriceResultsDto, IdentityContextDto<UpdateListPriceArgumentsDto>> CreateUpdateListPriceCommand
           ();
       ICommand<FetchApproveAdResultsDto, IdentityContextDto<FetchApproveAdArgumentsDto>> CreateApprovalAdCommand();
   }
}
