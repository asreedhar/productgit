﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.ApproveAd.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Commands.Impl;
using FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.ApproveAd.Commands.Impl
{
   public class CommandFactory:ICommandFactory
   {
       #region IcommandFactory Members

       public ICommand<UpdateListPriceResultsDto, IdentityContextDto<UpdateListPriceArgumentsDto>> CreateUpdateListPriceCommand()
       {
           return new UpdateListPriceCommand();
       }

       public ICommand<FetchApproveAdResultsDto, IdentityContextDto<FetchApproveAdArgumentsDto>> CreateApprovalAdCommand()
       {
           return new CreateApprovalAdCommand();
       }

       #endregion

   }
}
