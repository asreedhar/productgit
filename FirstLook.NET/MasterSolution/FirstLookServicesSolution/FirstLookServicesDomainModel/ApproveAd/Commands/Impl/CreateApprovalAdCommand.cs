﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.FirstLookServices.DomainModel.ApproveAd.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.ApproveAd.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Validation;
using System.Collections.Generic;
using System.Web;
using System;
using FirstLook.Merchandising.DomainModel.Vehicles.Inventory;
using log4net;
using Merchandising.Messages.AutoApprove;
using FirstLook.Merchandising.DomainModel;
using FirstLook.FirstLookServices.DomainModel.ApproveAd.Model;
using FirstLook.FirstLookServices.DomainModel.ApproveAd.Repositories.Entities;
using FirstLook.Merchandising.DomainModel.Postings;
using FirstLook.Common.Core.IOC;
using Autofac;
using Merchandising.Messages;
using System.Data;
using System.Reflection;
using FirstLook.Merchandising.DomainModel.Vehicles;
using FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Commands.Impl;

namespace FirstLook.FirstLookServices.DomainModel.ApproveAd.Commands.Impl
{
    public class CreateApprovalAdCommand : ICommand<FetchApproveAdResultsDto, IdentityContextDto<FetchApproveAdArgumentsDto>>
    {

        private static readonly ILog Log = LogManager.GetLogger(typeof(CreateApprovalAdCommand).FullName);

        #region ICommand<FetchRegenerateAdResultsDto,IdentityContextDto<FetchApproveAdArgumentsDto>> Members
        public FetchApproveAdResultsDto Execute(IdentityContextDto<FetchApproveAdArgumentsDto> parameters)
        {

            FetchApproveAdResultsDto Result = new FetchApproveAdResultsDto();
            Result.Result = false;
            try
            {
                // Mark Ad Review as completed
                var statuses = StepStatusCollection.Fetch(parameters.Arguments.Businessunitid, parameters.Arguments.InventoryId);
                statuses.SetStatus(StepStatusTypes.AdReview, ActivityStatusCodes.Complete);
                statuses.Save((parameters.Arguments.Username != null) ? parameters.Arguments.Username : parameters.Identity.Name);


                // Approve it!
                Log.InfoFormat("Ad approved for release. BusinessUnitId:{0}, InventoryId:{1}", parameters.Arguments.Businessunitid, parameters.Arguments.InventoryId);
                ////call the stored proc for saving the list price postings.VehicleAdScheduling

                StepStatusCollection.Fetch(parameters.Arguments.Businessunitid, parameters.Arguments.InventoryId)
                  .SetPostingStatus(AdvertisementPostingStatus.NoActionRequired)
                  .Save((parameters.Arguments.Username != null) ? parameters.Arguments.Username : parameters.Identity.Name);
                
                IResolver resolver = RegistryFactory.GetResolver();
                var ApproveAdRepository = resolver.Resolve<IRepository>();
                Result.Result = ApproveAdRepository.ApproveAd(new ApproveAdInput()
                {
                    Businessunitid = parameters.Arguments.Businessunitid,
                    InventoryId = parameters.Arguments.InventoryId,
                    UserName = (parameters.Arguments.Username != null) ? parameters.Arguments.Username : parameters.Identity.Name,
                    SpecialPrice = parameters.Arguments.SpecialPrice,
                    FlListPrice = parameters.Arguments.FlListPrice
                });

                IPricingMessager MessageSender = Registry.Resolve<IPricingMessager>();
                MessageSender.SendInventoryChangedMessage(parameters.Arguments.Businessunitid, parameters.Arguments.InventoryId);

            }
            catch (Exception ex)
            {
                Log.InfoFormat("Ad approval Failed for release. Exception:{1}", ex.Message);
            }
            return Result;
        }

        #endregion

    }
}
