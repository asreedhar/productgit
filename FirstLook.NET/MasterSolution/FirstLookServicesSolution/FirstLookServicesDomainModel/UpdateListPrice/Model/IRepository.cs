﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Model
{
    public interface IRepository
    {
        InventoryDto FetchInventoryDetails(InventoryDto inventory);
        void InsertMarketHistory(InventoryDto inventory);
        void InsertRepriceEvent(InventoryDto inventory);
        string GetMemberType(string userName);
    }
}
