﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Commands.Impl
{
    class CommandFactory:ICommandFactory
    {
        public ICommand<UpdateListPriceResultsDto, IdentityContextDto<UpdateListPriceArgumentsDto>> CreateUpdateListPriceCommand()
        {
            return new UpdateListPriceCommand();
        }
    }
}
