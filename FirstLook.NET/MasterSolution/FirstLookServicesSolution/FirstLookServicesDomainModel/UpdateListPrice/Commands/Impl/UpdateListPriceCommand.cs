﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using FirstLook.Client.DomainModel.Clients.Model.Authorities.Dealers;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.Distribution.WebService.Client.Distributor;
using FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Repositories.Entities;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Distribution;
using IRepository = FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Model.IRepository;
using FirstLook.Distribution.WebService.Client;

namespace FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Commands.Impl
{
    public class UpdateListPriceCommand : ICommand<UpdateListPriceResultsDto, IdentityContextDto<UpdateListPriceArgumentsDto>>
    {
        public UpdateListPriceResultsDto Execute(IdentityContextDto<UpdateListPriceArgumentsDto> parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();
            var inventoryListPriceRepository = resolver.Resolve<IRepository>();

            InventoryDto inventoryInput = new InventoryDto()
                                          {
                                              BusinessUnitId = parameters.Arguments.BusinessUnitId,
                                              InventoryId = parameters.Arguments.InventoryId,
                                              UserName = parameters.Arguments.UserName,
                                              NewListPrice = parameters.Arguments.NewListPrice
                                          };

            //------------------------------Updating all the changes while repricing the inventory(same as PING functionality)
            inventoryInput = inventoryListPriceRepository.FetchInventoryDetails(inventoryInput);
                // Return HandleDetails and validate Inventory

            if (inventoryListPriceRepository.GetMemberType(inventoryInput.UserName) == MemberType.User.ToString())
            {
                inventoryListPriceRepository.InsertMarketHistory(inventoryInput);
                    // Call "Pricing.VehicleMarketHistory#Create"
            }

            inventoryListPriceRepository.InsertRepriceEvent(inventoryInput); // Call "dbo.InsertRepriceEvent"
            //-------------------------------------------------------------------------------------------------------------
            

            //-------------------------------Distribute---------------------------------------------------------------------

            DistributeRepriceCommand.Execute(inventoryInput.BusinessUnitId, inventoryInput.InventoryId, Convert.ToInt32(inventoryInput.NewListPrice));

            //-----------------------------------------------------------------------------------------------------------------

            
            InventoryListPrice listprice = new InventoryListPrice()
                                         {
                                             InventoryId = inventoryInput.InventoryId,
                                             OldListPrice = inventoryInput.OldListPrice,
                                             NewListPrice = inventoryInput.NewListPrice
                                         };

            return new UpdateListPriceResultsDto()
                   {
                       ListPriceUpdate = listprice
                   };
        }
    }
}
