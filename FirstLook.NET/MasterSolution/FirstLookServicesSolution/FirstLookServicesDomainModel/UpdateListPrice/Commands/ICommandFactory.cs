﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Commands
{
    public interface ICommandFactory
    {
        ICommand<UpdateListPriceResultsDto, IdentityContextDto<UpdateListPriceArgumentsDto>> CreateUpdateListPriceCommand();
    }
}
