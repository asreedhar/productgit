﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Commands.TransferObjects
{
    public class InventoryListPrice
    {
        public int InventoryId { get; set; }

        public decimal OldListPrice { get; set; }

        public decimal NewListPrice { get; set; }
    }
}
