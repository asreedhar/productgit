﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Commands.TransferObjects.Envelopes
{
    public class UpdateListPriceArgumentsDto
    {
        public string UserName { get; set; }
        
        public int BusinessUnitId { get; set; }

        public int InventoryId { get; set; }

        public decimal NewListPrice { get; set; }
    }
}
