﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Commands.TransferObjects.Envelopes
{
    public class UpdateListPriceResultsDto
    {
        public PrincipalDto Principal { get; set; }

        public UpdateListPriceArgumentsDto Arguments { get; set; }

        public InventoryListPrice ListPriceUpdate { get; set; }

    }
}
