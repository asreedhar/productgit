﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.FirstLookServices.DomainModel.UpdateListPrice
{
    public class Module:IModule
    {
        #region IModule Members

        public void Configure(IRegistry registry)
        {
            registry.Register<Commands.Module>();

            registry.Register<Repositories.Module>();
        }
        #endregion
    }
}
