﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Repositories.DataStores;
using FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.UserRole.Model;
using FirstLook.FirstLookServices.DomainModel.UserRole.Repositories.Datastores;
using FirstLook.FirstLookServices.DomainModel.UserRole.Repositories.Entities;
//using MemberType=FirstLook.DomainModel.Oltp.MemberType;


namespace FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Repositories.Gateways
{
    public class InventoryGateway:GatewayBase
    {
        public InventoryDto Fetch(InventoryDto inventory)
        {
            var serializer = Resolve<ISerializer<HandleDetails>>();

            var datastore = Resolve<IInventoryDataStore>();

            using (IDataReader reader = datastore.Details(inventory))
            {
                HandleDetails items = serializer.Deserialize(reader).FirstOrDefault();
                inventory.HandleDetails = items;
                inventory.OldListPrice = items.OldListPrice;
            }

            return inventory;

        }

        public void InsertInMarketHistory(InventoryDto inventory)
        {
            var datastore = Resolve<IInventoryDataStore>();
            datastore.History(inventory);
        }

        public void InsertInRepriceEvent(InventoryDto inventory)
        {
            var datastore = Resolve<IInventoryDataStore>();
            datastore.Reprice(inventory);
        }

        public string GetMember(string username)
        {
            var serializer = Resolve<ISerializer<UserRoleDetails>>();
            var datastore = Resolve<IUserRoleDatastore>();
            UserRoleInput userRoleInput = new UserRoleInput()
                                            {

                                                UserName = username
                                            };
            UserRoleDetails items;
            using (IDataReader reader = datastore.UserRole(userRoleInput))
            {
                 items = serializer.Deserialize(reader).FirstOrDefault();
            }
            if (items.MemberType == null)
            {
                return "";
            }
            return items.MemberType;
        }

    }
}
