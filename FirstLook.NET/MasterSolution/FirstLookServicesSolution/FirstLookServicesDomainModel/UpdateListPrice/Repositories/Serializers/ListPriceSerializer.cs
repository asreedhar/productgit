﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Repositories.Serializers
{
    public class ListPriceSerializer :Serializer<HandleDetails>
    {
        public override HandleDetails Deserialize(IDataRecord record)
        {
            string ownerHandle = string.Empty;
            string vehicleHandle = string.Empty;
            string searchHandle = string.Empty;
            decimal oldListPrice = decimal.Zero;

            try
            {
                oldListPrice = DataRecord.GetDecimal(record, "ListPrice", decimal.Zero);
                ownerHandle = DataRecord.GetGuid(record, "OwnerHandle",Guid.Empty).ToString();
                vehicleHandle = DataRecord.GetString(record, "VehicleHandle");
                searchHandle = DataRecord.GetGuid(record, "SearchHandle",Guid.Empty).ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return new HandleDetails()
            {
                OldListPrice = oldListPrice,
                OwnerHandle = ownerHandle,
                VehicleHandle = vehicleHandle,
                SearchHandle=searchHandle
            };
        }

        
    }
}
