﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Repositories.Serializers
{
    [Serializable]
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ISerializer<HandleDetails>, ListPriceSerializer>(ImplementationScope.Shared);
        }
    }
}
