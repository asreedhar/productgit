﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Vehicles.Repositories.Gateways;
using FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Model;
using FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Repositories.Entities;
using InventoryGateway = FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Repositories.Gateways.InventoryGateway;

namespace FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Repositories
{
    public class InventoryRepository:IRepository
    {
        public InventoryDto FetchInventoryDetails(InventoryDto inventory)
        {
            return new InventoryGateway().Fetch(inventory);
        }

        public void InsertMarketHistory(InventoryDto inventory)
        {
            new InventoryGateway().InsertInMarketHistory(inventory);
        }

        public void InsertRepriceEvent(InventoryDto inventory)
        {
            new InventoryGateway().InsertInRepriceEvent(inventory);
        }

        public string GetMemberType(string username)
        {
           return new InventoryGateway().GetMember(username);
        }
    }
}
