﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Repositories.Entities
{
    public class HandleDetails
    {
        public string OwnerHandle { get; set; }

        public string VehicleHandle { get; set; }

        public string SearchHandle { get; set; }

        public decimal OldListPrice { get; set; }

    }
}
