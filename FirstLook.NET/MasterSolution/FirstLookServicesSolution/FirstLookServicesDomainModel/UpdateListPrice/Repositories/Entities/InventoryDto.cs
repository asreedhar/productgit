﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Repositories.Entities
{
    public class InventoryDto
    {
        public int BusinessUnitId { get; set; }

        public int InventoryId { get; set; }

        public decimal OldListPrice { get; set; }

        public decimal NewListPrice { get; set; }

        public string UserName { get; set; }

        public HandleDetails HandleDetails { get; set; }
    }
}
