﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Repositories.Entities
{
    public enum UserRoles
    {
            Dummy = 0,
            Administrator = 1,
            User = 2,
            AccountRepresentative = 3,
            SalesPerson = 4
    }
}
