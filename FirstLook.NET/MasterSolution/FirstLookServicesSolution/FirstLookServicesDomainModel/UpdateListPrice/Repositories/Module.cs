﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Model;

namespace FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Repositories
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<DataStores.Module>();

            registry.Register<Serializers.Module>();

            registry.Register<IRepository, InventoryRepository>(ImplementationScope.Shared);

        }
    }
}
