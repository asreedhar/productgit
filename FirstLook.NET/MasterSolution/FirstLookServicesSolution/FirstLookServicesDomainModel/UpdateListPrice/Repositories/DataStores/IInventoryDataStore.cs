﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Repositories.DataStores
{
    public interface IInventoryDataStore
    {
        IDataReader Details(InventoryDto inventory);
        void History(InventoryDto inventory);
        void Reprice(InventoryDto inventory);
    }
}
