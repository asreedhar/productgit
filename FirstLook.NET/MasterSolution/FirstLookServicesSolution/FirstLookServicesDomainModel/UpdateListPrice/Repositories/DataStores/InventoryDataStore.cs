﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Repositories.DataStores
{
    public class InventoryDataStore : IInventoryDataStore
    {
        #region Class Members

        private const string DatabaseName = "IMT";
        private const string DatabaseName1 = "Market";

        public enum VehicleInteractionType
        {
            Review = 3,
            Reprice = 4
        }

        #endregion

        public IDataReader Details(InventoryDto inventory)
        {
            try
            {
                using (IDbConnection connection = Database.Connection(DatabaseName))
                {
                    using (IDbCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.CommandText = "dbo.Fetch#InventoryDetails";

                        Database.AddWithValue(command, "BusinessUnitID", inventory.BusinessUnitId, DbType.Int32);
                        Database.AddWithValue(command, "InventoryID", inventory.InventoryId, DbType.Int32);

                        var set = new DataSet();

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            var table = new DataTable("Inventory");

                            table.Load(reader);

                            set.Tables.Add(table);
                        }

                        return set.CreateDataReader();
                    }
                }
            }
            catch (SqlException e)
            {
                throw e;
            }

        }

        public void History(InventoryDto inventory)
        {
            try
            {
                using (IDbConnection connection = Database.Connection(DatabaseName1))
                {
                    using (IDbCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.CommandText = "Pricing.VehicleMarketHistory#Create";

                        Database.AddWithValue(command, "OwnerHandle", inventory.HandleDetails.OwnerHandle, DbType.String);
                        Database.AddWithValue(command, "VehicleHandle", inventory.HandleDetails.VehicleHandle,DbType.String);
                        Database.AddWithValue(command, "SearchHandle", inventory.HandleDetails.SearchHandle,DbType.String);
                        Database.AddWithValue(command, "Login", inventory.UserName, DbType.String);
                        Database.AddWithValue(command, "VehicleMarketHistoryEntryTypeID", VehicleInteractionType.Reprice, DbType.Int32);
                        Database.AddWithValue(command, "OldListPrice", inventory.OldListPrice, DbType.Int32);
                        Database.AddWithValue(command, "NewListPrice", inventory.NewListPrice, DbType.Int32);

                        command.ExecuteReader();
                    }

                }
            }
            catch (SqlException e)
            {
                throw e;
            }

        }
        public void Reprice(InventoryDto inventory)
        {
            DateTime now = DateTime.Now;

            try
            {
                using (IDbConnection connection = Database.Connection(DatabaseName))
                {
                    using (IDbCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.CommandText = "dbo.InsertRepriceEvent";

                        Database.AddWithValue(command, "InventoryID", inventory.InventoryId, DbType.Int32);
                        Database.AddWithValue(command, "BeginDate", now, DbType.DateTime);
                        Database.AddWithValue(command, "EndDate", now, DbType.DateTime);
                        Database.AddWithValue(command, "Value", inventory.NewListPrice, DbType.Int32);
                        Database.AddWithValue(command, "CreatedBy", inventory.UserName, DbType.String);
                        Database.AddWithValue(command, "OriginalListPrice", inventory.OldListPrice, DbType.Int32);
                        Database.AddWithValue(command, "LastModified", now, DbType.DateTime);
                        Database.AddWithValue(command, "Confirmed", false, DbType.Boolean);
                        Database.AddWithValue(command, "Source", 3, DbType.Int16);

                        command.ExecuteReader();
                    }

                }
            }
            catch (SqlException e)
            {
                throw e;
            }

        }
    }
}
