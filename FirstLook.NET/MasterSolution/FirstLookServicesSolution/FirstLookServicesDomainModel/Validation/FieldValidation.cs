﻿namespace FirstLook.FirstLookServices.DomainModel.Validation
{
    public static class FieldValidation
    {
        public static bool ValidVin(this object callee, string v)
        {
            var vin = v.ToUpper();// just in case
            return ! (vin.Length != 17 || vin.Contains("I") || vin.Contains("O") || vin.Contains("Q") || vin[9] =='Z' || vin[9]=='U');
        }

    }
}
