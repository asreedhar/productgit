﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Dealers.Commands.Impl;
using FirstLook.FirstLookServices.DomainModel.Dealers.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.FirstLookServices.DomainModel.Dealers.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.Validation
{
    public class Validators
    {
        public static void CheckDealerId(int dealerId, IIdentityContextDto commandParams)
        {
            
            var dealers = new FetchDealersCommand().Execute(new IdentityContextDto<FetchDealersArgumentsDto>(){ Identity = commandParams.Identity } ).Dealers;

            bool isLegitDealer = false;

            //must be more of a shorthand way of doing this sort of thing
            foreach ( DealerDto dealer in dealers)
            {
                if (dealer.Id == dealerId)
                {
                    isLegitDealer = true;
                    break;
                }
            }

            if (!isLegitDealer)
            {
                throw new InvalidInputException("DealerId", dealerId.ToString(), "");
            }
        }
    }
}