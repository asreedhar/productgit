﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model;
using ServiceStack.CacheAccess;
using ServiceStack.CacheAccess.Providers;

namespace FirstLook.FirstLookServices.DomainModel.Caching
{
    public static class CarCaching
    {
        private static ICacheClient cacheClient = new MemoryCacheClient();

        //for sanity, we need to be able to just add CarInfo objects
        public static void AddCar(this object callee, CarInfo carInf)
        {
            string key = carInf.Uvc + (carInf.Vin ?? "");
            var carList = cacheClient.Get<List<CarInfo>>(key);
            if (carList == null)
            {
                carList = new List<CarInfo>(){ carInf };
                cacheClient.Add(key, carList);
            }
        }

        public static void AddToCarCache(this object callee, List<CarInfo> cars, string uvc, string vin = null)
        {
            string key = uvc + (vin ?? "");
            cacheClient.Add<List<CarInfo>>(key, cars);
        }
        public static List<CarInfo> GetFromCarCache(this object callee, string uvc, string vin = null)
        {
            string key = uvc + (vin ?? "");
            return cacheClient.Get<List<CarInfo>>(key);
        }
    }
}
