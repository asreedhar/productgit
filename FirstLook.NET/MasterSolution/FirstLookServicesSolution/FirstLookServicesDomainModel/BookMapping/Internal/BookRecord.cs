namespace FirstLook.FirstLookServices.DomainModel.BookMapping.Internal
{
    /// <summary>
    /// These are internal classes used within the domain.
    /// </summary>
    internal class BookRecord //: IBookRecordDto
    {
        internal IBook Book { get; set; }
        internal virtual dynamic TrimId { get; set; }
        internal string Trim { get; set; }

        internal string Make { get; set; }
        internal string Model { get; set; }
        internal int Year { get; set; }

        internal string VinPattern { get; set; }
        internal int? ChromeStyleId { get; set; }

        // pattern based matching
        internal bool VinPatternMatch { get; set; }
        internal int VinPatternScore { get; set; }


        /// <summary>
        /// True if this trim/chromestyleid are associated thru an explicit map, such as chrome's used valuation guide data. 
        /// </summary>
        public bool ExplicitChromeStyleTrimMatch { get; set; }

        /// <summary>
        /// True if this book record was matched to a chrome style.
        /// </summary>
        public bool ChromeStyleMatch { get; set; }

        /// <summary>
        /// Should this record be selected as default? 
        /// </summary>
        public bool IsDefault { get; set; }

        public string Vin { get; set; }
    }

    internal class KbbBookRecord : BookRecord
    {
        internal override dynamic TrimId
        {
            get { return VehicleId; }
        }

        internal int VehicleId { get; set; }
    }

    internal class NadaBookRecord : BookRecord
    {
        internal override dynamic TrimId
        {
            get { return Uid; }
        }
        
        internal int VehicleId { get; set; }
        internal int Uid { get; set; }
        internal string Vic { get; set; }
    }

    internal class GalvesBookRecord : BookRecord
    {
        internal override dynamic TrimId
        {
            get { return PhGuid; }
        }

        internal int PhGuid { get; set; }
    }

    internal class BlackBookRecord : BookRecord
    {
        internal override dynamic TrimId
        {
            get { return Uvc; }
        }
 
        internal string Uvc { get; set; }
    }
}