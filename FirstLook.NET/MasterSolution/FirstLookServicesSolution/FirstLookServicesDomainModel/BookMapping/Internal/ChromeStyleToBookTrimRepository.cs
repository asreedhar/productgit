﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace FirstLook.FirstLookServices.DomainModel.BookMapping.Internal
{
    /// <summary>
    /// Note: most books treat their internal id as an integer. BlackBook doesn't (UVC is a string).
    /// It's simpler for us to treat them all as strings.
    /// </summary>
    internal interface IChromeStyleToBookTrimRepository
    {
        IEnumerable<string> BlackBookUvcFromStyle(int style);
        IEnumerable<string> KbbVehicleIdFromStyle(int style);
        IEnumerable<string> NadaUvcFromStyle(int style);
        IEnumerable<string> GalvesPhGuidFromStyle(int style);
    }

    /// <summary>
    /// Provides chrome style to book trim mappings. Only works for books for which we have purchased this data
    /// from Chrome, via their "used valuation mapping" product - currently just Blackbook.
    /// http://www.chromedata.com/products/data-products/used-valuation-mapping/?
    /// </summary>
    internal class ChromeStyleToBookTrimRepository : IChromeStyleToBookTrimRepository
    {
        internal readonly string CONNECTION;
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        internal ChromeStyleToBookTrimRepository()
        {
            CONNECTION = ConfigurationManager.ConnectionStrings["VehicleCatalog"].ConnectionString;
        }

        public IEnumerable<string> BlackBookUvcFromStyle(int style)
        {
            // TODO: work on UvcFromStyle so that it returns all the potential matches?

            const string proc = "FirstLook.GetBlackBookUvcFromStyle";
            IList<string> uvcRecords = new List<string>();

            using (var connection = new SqlConnection(CONNECTION))
            using (var command = new SqlCommand(proc, connection) { CommandType = CommandType.StoredProcedure })
            {
                command.Parameters.AddWithValue("style", style);

                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        string uvc = reader.GetString(reader.GetOrdinal("Uvc"));

                        // if we already have this uvc, we can ignore this one.
                        if (uvcRecords.All(r => r != uvc))
                        {
                            uvcRecords.Add(uvc);
                        }
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    throw;
                }
            }
            return uvcRecords;
        }

        /// <summary>
        /// When we purchase this data, likely from Chrome's Used Valuation Mapping product, this method 
        /// can be implemented to so something useful.
        /// </summary>
        /// <param name="style"></param>
        /// <returns></returns>
        public IEnumerable<string> KbbVehicleIdFromStyle(int style)
        {
            // when we purchase this data, we can implement this method.
            return new List<string>();
        }

        /// <summary>
        /// When we purchase this data, likely from Chrome's Used Valuation Mapping product, this method 
        /// can be implemented to so something useful.
        /// </summary>
        /// <param name="style"></param>
        /// <returns></returns>
        public IEnumerable<string> NadaUvcFromStyle(int style)
        {
            // when we purchase this data, we can implement this method.
            return new List<string>();
        }

        /// <summary>
        /// When we purchase this data, likely from Chrome's Used Valuation Mapping product, this method 
        /// can be implemented to so something useful.
        /// </summary>
        /// <param name="style"></param>
        /// <returns></returns>
        public IEnumerable<string> GalvesPhGuidFromStyle(int style)
        {
            // when we purchase this data, we can implement this method.
            return new List<string>();
        }
    }
}
