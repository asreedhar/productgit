using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using Amazon.EC2.Model;
using FirstLook.FirstLookServices.DomainModel.Utility;

namespace FirstLook.FirstLookServices.DomainModel.BookMapping.Internal
{
    internal interface IBookMappingRepository
    {
        IEnumerable<KbbBookRecord> KbbDataFromVinPatternAndYear(string vinPattern, int year);
        IEnumerable<NadaBookRecord> NadaDataFromVinPatternAndYear(string vinPattern, int year);
        IEnumerable<GalvesBookRecord> GalvesDataFromVinPatternAndYear(string vinPattern, int year);

        IEnumerable<KbbBookRecord> KbbDataFromStyleAndCountry(int style, Countries country);
        IEnumerable<NadaBookRecord> NadaDataFromStyleAndCountry(int style, Countries country);
        IEnumerable<GalvesBookRecord> GalvesDataFromStyleAndCountry(int style, Countries country);

        IEnumerable<int> ChromeYearFromVinPattern(string vinPattern);
        string CanonicalVinPatternFromVin(string vin);

        IEnumerable<KbbBookRecord> KbbDataFromVinPatternAndYear(string vinPattern, int year, string vin);
    }

    internal class BookMappingRepository : IBookMappingRepository
    {
        internal readonly string CONNECTION;
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        internal BookMappingRepository()
        {
            CONNECTION = ConfigurationManager.ConnectionStrings["VehicleCatalog"].ConnectionString;
        }

        #region Book-specific mappings


        public IEnumerable<KbbBookRecord> KbbDataFromVinPatternAndYear(string vinPattern, int year)
        {
            const string proc = "FirstLook.GetKbbDataFromVinPatternAndYear";
            IList<KbbBookRecord> bookRecords = new List<KbbBookRecord>();

            using (var connection = ProfiledDbConnectionFactory.GetConnection(CONNECTION))
            using (var command = connection.CreateCommand() )
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = proc;

                command.Parameters.AddWithValue(command, "vinPattern", vinPattern);
                command.Parameters.AddWithValue(command, "year", year);

                try
                {
                    connection.Open();
                    IDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        string trim = reader.GetString(reader.GetOrdinal("Trim"));
                        int vehicleId = reader.GetInt32(reader.GetOrdinal("VehicleId"));
                        string make = reader.GetString(reader.GetOrdinal("MakeName"));
                        string model = reader.GetString(reader.GetOrdinal("ModelName"));

                        bookRecords.Add(
                            new KbbBookRecord
                            {
                                Book = BookFactory.Instance.Get(BookId.Kbb),
                                Trim = trim,
                                VehicleId = vehicleId,
                                Year=year,
                                Make=make,
                                Model=model
                            }
                        );
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    throw;
                }
            }

            return bookRecords;
        } // confirmed needed

        public IEnumerable<NadaBookRecord> NadaDataFromVinPatternAndYear(string vinPattern, int year)
        {
            const string proc = "FirstLook.GetNadaDataFromVinPatternAndYear";
            IList<NadaBookRecord> bookRecords = new List<NadaBookRecord>();

            using (var connection = ProfiledDbConnectionFactory.GetConnection(CONNECTION))
            using (var command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = proc;
                command.Parameters.AddWithValue(command,"vinPattern", vinPattern);
                command.Parameters.AddWithValue(command,"year", year);

                try
                {
                    connection.Open();
                    IDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        string trim = reader.GetString(reader.GetOrdinal("Trim"));
                        int uid = reader.GetInt32(reader.GetOrdinal("Uid"));
                        string make = reader.GetString(reader.GetOrdinal("MakeName"));
                        string series = reader.GetString(reader.GetOrdinal("SeriesName"));

                        bookRecords.Add(
                            new NadaBookRecord
                            {
                                Book = BookFactory.Instance.Get(BookId.Nada),
                                Trim = trim,
                                Uid = uid,
                                Make = make,
                                Model=series,
                                Year=year
                            }
                        );
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    throw;
                }
            }

            return bookRecords;
        } // confirmed needed

        public IEnumerable<GalvesBookRecord> GalvesDataFromVinPatternAndYear(string vinPattern, int year)
        {
            const string proc = "FirstLook.GetGalvesDataFromVinPatternAndYear";
            IList<GalvesBookRecord> bookRecords = new List<GalvesBookRecord>();

            using (var connection = ProfiledDbConnectionFactory.GetConnection(CONNECTION))
            using (var command = connection.CreateCommand() )
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = proc;
                command.Parameters.AddWithValue(command, "vinPattern", vinPattern);
                command.Parameters.AddWithValue(command, "year", year);

                try
                {
                    connection.Open();
                    IDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        string trim = reader.GetString(reader.GetOrdinal("Trim"));
                        int phGuid = reader.GetInt32(reader.GetOrdinal("PhGuid"));
                        string make = reader.GetString(reader.GetOrdinal("MakeName"));
                        string model = reader.GetString(reader.GetOrdinal("ModelName"));

                        bookRecords.Add(
                            new GalvesBookRecord
                            {
                                Book = BookFactory.Instance.Get(BookId.Galves),
                                Trim = trim,
                                PhGuid = phGuid,
                                Make=make,
                                Model=model,
                                Year=year
                            }
                        );

                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    throw;
                }
            }

            return bookRecords;
        } // confirmed needed

        #endregion

        #region Style-based lookups

        public IEnumerable<KbbBookRecord> KbbDataFromStyleAndCountry(int style, Countries country)
        {
            const string proc = "FirstLook.GetKbbRollupDataFromStyle";
            IList<KbbBookRecord> bookRecords = new List<KbbBookRecord>();

            using (var connection = ProfiledDbConnectionFactory.GetConnection(CONNECTION))
            using (var command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = proc;

                command.Parameters.AddWithValue(command, "style", style);
                command.Parameters.AddWithValue(command, "countryCode", (int)country);

                try
                {
                    connection.Open();
                    IDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        int vehicleId = reader.GetInt32(reader.GetOrdinal("VehicleId"));
                        bool chromeStyleMatch = reader.GetBoolean(reader.GetOrdinal("ChromeStyleMatch"));
                        string trim = reader.GetString(reader.GetOrdinal("Trim"));
                        string model = reader.GetString(reader.GetOrdinal("ModelName"));
                        string make = reader.GetString(reader.GetOrdinal("MakeName"));
                        int year = reader.GetInt32(reader.GetOrdinal("YearId"));

                        bookRecords.Add(
                            new KbbBookRecord
                            {
                                Book = BookFactory.Instance.Get(BookId.Kbb),
                                Trim = trim,
                                VehicleId = vehicleId,
                                ChromeStyleMatch = chromeStyleMatch,
                                Model = model,
                                Make = make,
                                Year = year
                            }
                        );
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    throw;
                }
            }

            return bookRecords;
        } // confirmed needed

        public IEnumerable<NadaBookRecord> NadaDataFromStyleAndCountry(int style, Countries country)
        {
            const string proc = "FirstLook.GetNadaRollupDataFromStyle";
            IList<NadaBookRecord> bookRecords = new List<NadaBookRecord>();

            using (var connection = ProfiledDbConnectionFactory.GetConnection(CONNECTION))
            using (var command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = proc;

                command.Parameters.AddWithValue(command, "style", style);
                command.Parameters.AddWithValue(command, "countryCode", (int)country);

                try
                {
                    connection.Open();
                    IDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        int uid = reader.GetInt32(reader.GetOrdinal("uid"));
                        bool chromeStyleMatch = reader.GetBoolean(reader.GetOrdinal("ChromeStyleMatch"));
                        string trim = reader.GetString(reader.GetOrdinal("Trim"));
                        string make = reader.GetString(reader.GetOrdinal("MakeName"));
                        string series = reader.GetString(reader.GetOrdinal("SeriesName"));
                        int year = reader.GetInt32(reader.GetOrdinal("VehicleYear"));

                        bookRecords.Add(
                            new NadaBookRecord
                            {
                                Book = BookFactory.Instance.Get(BookId.Nada),
                                Trim = trim,
                                Uid=uid,
                                ChromeStyleMatch = chromeStyleMatch,
                                Make=make,
                                Model=series,
                                Year=year
                            }
                        );
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    throw;
                }
            }

            return bookRecords;
        } // confirmed needed

        public IEnumerable<GalvesBookRecord> GalvesDataFromStyleAndCountry(int style, Countries country)
        {
            const string proc = "FirstLook.GetGalvesRollupDataFromStyle";
            IList<GalvesBookRecord> bookRecords = new List<GalvesBookRecord>();

            using (var connection = ProfiledDbConnectionFactory.GetConnection(CONNECTION))
            using (var command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = proc;

                command.Parameters.AddWithValue(command, "style", style);
                command.Parameters.AddWithValue(command, "countryCode", (int)country);

                try
                {
                    connection.Open();
                    IDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        int phguid = reader.GetInt32(reader.GetOrdinal("PhGuid"));
                        bool chromeStyleMatch = reader.GetBoolean(reader.GetOrdinal("ChromeStyleMatch"));
                        string trim = reader.GetString(reader.GetOrdinal("Trim"));
                        string make = reader.GetString(reader.GetOrdinal("ph_manuf"));
                        string model = reader.GetString(reader.GetOrdinal("ph_model"));
                        int year = reader.GetInt16(reader.GetOrdinal("ph_year"));

                        bookRecords.Add(
                            new GalvesBookRecord
                            {
                                Book = BookFactory.Instance.Get(BookId.Galves),
                                Trim = trim,
                                PhGuid = phguid,
                                ChromeStyleMatch = chromeStyleMatch,
                                Make=make,
                                Model=model,
                                Year=year
                            }
                        );
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    throw;
                }
            }

            return bookRecords;
        } // confirmed needed

        #endregion

        #region Obtain vin pattern

        /// <summary>
        /// Given a vin, return the vin pattern & year.
        /// </summary>
        /// <param name="vin"></param>
        /// <returns></returns>
        public string CanonicalVinPatternFromVin(string vin)
        {
            // TODO: should we verify that this pattern is defined in chrome?
            if (string.IsNullOrWhiteSpace(vin) || vin.Length > 17)
            {
                const string message = "Invalid vin.";
                Log.Error(message);
                throw new BookMappingException(message);
            }

            return vin.Substring(0, 8) + vin.Substring(9, 1);
        }


/*
        public IEnumerable<Tuple<string, int, bool>> ChromeVinPatternFromStyle(int style)
        {
            return ChromeVinPatternFromStyleAndCountry(style, Countries.Usa);
        }
*/

/*        /// <summary>
        /// Gets the vin pattern and year for the specified style and country.
        /// </summary>
        /// <param name="style"></param>
        /// <param name="country"></param>
        /// <returns>Tuples of vin pattern & year</returns>
        public IEnumerable<Tuple<string,int,bool>> ChromeVinPatternFromStyleAndCountry(int style, Countries country)
        {
            const string proc = "FirstLook.GetVinPatternFromStyle";
            var patternYearMatchTuples = new List<Tuple<string,int, bool>>();

            using (var connection = ProfiledDbConnectionFactory.GetConnection(CONNECTION))
            using (var command = connection.CreateCommand() )
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = proc;
                command.Parameters.AddWithValue(command,"style", style);
                command.Parameters.AddWithValue(command,"countryCode", (int)country);

                try
                {
                    connection.Open();
                    IDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        int year = reader.GetInt32(reader.GetOrdinal("ModelYear"));
                        string pattern = reader.GetString(reader.GetOrdinal("VINPattern_Prefix"));
                        bool styleMatch = reader.GetBoolean(reader.GetOrdinal("StyleMatch")); 

                        var pair = new Tuple<string, int, bool>(pattern,year, styleMatch);
                        patternYearMatchTuples.Add(pair);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    throw;
                }
            }

            return patternYearMatchTuples;
        }*/

        #endregion


        /// <summary>
        /// Given a vin pattern, return the year.
        /// </summary>
        /// <param name="vinPattern"></param>
        /// <returns></returns>
        public IEnumerable<int> ChromeYearFromVinPattern(string vinPattern)
        {
            const string proc = "FirstLook.GetYearFromVinPattern";
            IList<int> years = new List<int>();

            using (var connection = ProfiledDbConnectionFactory.GetConnection(CONNECTION))
            using (var command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = proc;
                command.Parameters.AddWithValue(command, "vinPattern", vinPattern);

                try
                {
                    connection.Open();
                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        int year = reader.GetInt32(reader.GetOrdinal("Year"));
                        years.Add(year);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    throw;
                }
            }

            return years;
        }  // confirmed needed


        #region VINSpecificOptions
        public IEnumerable<KbbBookRecord> KbbDataFromVinPatternAndYear(string vinPattern, int year,string vin)
        {
            const string proc = "FirstLook.GetKbbDataFromVinPatternAndYear";
            IList<KbbBookRecord> bookRecords = new List<KbbBookRecord>();

            using (var connection = ProfiledDbConnectionFactory.GetConnection(CONNECTION))
            using (var command = connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = proc;

                command.Parameters.AddWithValue(command, "vinPattern", vinPattern);
                command.Parameters.AddWithValue(command, "year", year);

                try
                {
                    connection.Open();
                    IDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        string trim = reader.GetString(reader.GetOrdinal("Trim"));
                        int vehicleId = reader.GetInt32(reader.GetOrdinal("VehicleId"));
                        string make = reader.GetString(reader.GetOrdinal("MakeName"));
                        string model = reader.GetString(reader.GetOrdinal("ModelName"));

                        bookRecords.Add(
                            new KbbBookRecord
                            {
                                Book = BookFactory.Instance.Get(BookId.Kbb),
                                Trim = trim,
                                VehicleId = vehicleId,
                                Year = year,
                                Make = make,
                                Model = model,
                                Vin = vin
                            }
                        );
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    throw;
                }
            }

            return bookRecords;
        }
        #endregion
    }
}