﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.BookMapping.Internal
{
    internal class BookMappingException : ApplicationException
    {
        internal BookMappingException(string message) : base(message)
        {}
    }
}
