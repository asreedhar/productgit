﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.FirstLookServices.DomainModel.Utility; //for ErrorCode enum
using FirstLook.FirstLookServices.DomainModel.Exceptions; //for exception extension method

namespace FirstLook.FirstLookServices.DomainModel.BookMapping.Internal
{
    internal class VinBasedMapping
    {

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IBookMappingRepository _bookMappingRepository;
        private readonly IBlackBookFacade _blackBookFacade;
        private readonly IChromeStyleToBookTrimRepository _chromeStyleToBookTrimRepository;


        internal VinBasedMapping()
        {
            _bookMappingRepository = new BookMappingRepository();
            _chromeStyleToBookTrimRepository = new ChromeStyleToBookTrimRepository();
            _blackBookFacade = new BlackBookFacade(_chromeStyleToBookTrimRepository);
        }

        /// <summary>
        /// Alternative constructor provided so CommonMappings can be unit tested absent a db.
        /// </summary>
        /// <param name="bookMappingRepository"></param>
        /// <param name="chromeStyleToBookTrimRepository"> </param>
        /// <param name="blackBookFacade"> </param>
        internal VinBasedMapping(IBookMappingRepository bookMappingRepository, 
                                IChromeStyleToBookTrimRepository chromeStyleToBookTrimRepository,
                                IBlackBookFacade blackBookFacade)
        {
            _bookMappingRepository = bookMappingRepository;
            _chromeStyleToBookTrimRepository = chromeStyleToBookTrimRepository;
            _blackBookFacade = blackBookFacade;
        }

        /// <summary>
        /// Given a vin, year and book, 
        ///     if vin maps to a single book specific trim (vin pattern match), return only it
        ///     if vin maps to multiple book specific trims on vin pattern, return all vin pattern matches
        ///     if the vin does not map to any book specific trim on vin pattern, return none
        /// </summary>
        /// <param name="year">the year</param>
        /// <param name="vin">A 17 character vin</param>
        /// <param name="book">The book</param>
        /// <returns></returns>
        internal IEnumerable<BookRecord> BookDataFromVin(string vin, IBook book, int? year = null)
        {
            string vinPattern = VinOrPatternToPatternWithValidation(vin);

            IEnumerable<BookRecord> records;

            // get the year if we don't have one already.
            int theYear = !year.HasValue ? YearFromPatternWithValidation(vinPattern) : year.Value;

            switch (book.Id)
            {
                case BookId.Kbb:
                    records = _bookMappingRepository.KbbDataFromVinPatternAndYear(vinPattern, theYear, vin);
                    break;
                case BookId.Nada:
                    records = _bookMappingRepository.NadaDataFromVinPatternAndYear(vinPattern, theYear);
                    break;
                case BookId.Galves:
                    records = _bookMappingRepository.GalvesDataFromVinPatternAndYear(vinPattern, theYear);
                    break;
                case BookId.BlackBook:
                    records = _blackBookFacade.BlackBookDataFromVin(vin);
                    break;
                default:
                    string message = String.Format("We don't currently support the book {0}", book.Desc);
                    Log.Error(message);
                    throw new BookMappingException(message);
            }

            IList<BookRecord> recordsList = records.ToList();

            // if we only found one record, it's the default.
            if (recordsList.Count() == 1)
            {
                recordsList.First().IsDefault = true;
            }

            return recordsList;

        }

        internal static void ValidateVin(string vinOrPattern)
        {
            // a vin is 17 characters, a vin pattern (as far as we're concerned) is 9 or 10 characters, depending on whether
            // you include the check digit
            var length = vinOrPattern.Length;
            if( length != 17 && length != 9 && length != 10 )
            {
                string message = String.Format("The specified vin or pattern is invalid: {0}", vinOrPattern);
                Log.Error(message);
                throw new BookMappingException(message);
            }            
        }

        internal string VinOrPatternToPatternWithValidation(string vinOrPattern)
        {
            ValidateVin(vinOrPattern);

            // If the "vinOrPattern" is 17 characters long, assume it's a vin.  Otherwise, assume it's a pattern.
            if (vinOrPattern.Length == 17)
            {
                // get the pattern
                return _bookMappingRepository.CanonicalVinPatternFromVin(vinOrPattern);
            }

            return vinOrPattern;
        }

        internal int YearFromPatternWithValidation(string vinPattern)
        {
            var years = _bookMappingRepository.ChromeYearFromVinPattern(vinPattern).ToList();

            if (years.Count() != 1)
            {
                throw new NoDataFoundException("Dev: No year found for vin pattern.");
            }

            return years.First();
        }
    }
}
