﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.Extensions;
using FirstLook.FirstLookServices.DomainModel.VehicleConfig;

namespace FirstLook.FirstLookServices.DomainModel.BookMapping.Internal
{
    /*
        Style – non blackbook (as we currently only have chrome used valuation mapping for blackbook)
        -          _chrome rollup_
        -          rollup to everything with same YMM
        -          if a single book trim in the set of trims is a vin pattern match, it should be the default
 
        Style – blackbook (we have chrome used valuation mapping for blackbook)
        -          if the selected style maps to a single bb trim, return only it
        -          if the selected style maps to multiple bb trims, return all
        -          if the selected style maps to no bb trims, return none

    */

    internal class StyleBasedMapping
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private IChromeStyleToBookTrimRepository _chromeStyleToBookTrimRepository;
        private readonly IBlackBookFacade _blackBookFacade;
        private readonly IBookMappingRepository _bookMappingRepository;

        internal StyleBasedMapping()
        {
            _chromeStyleToBookTrimRepository = new ChromeStyleToBookTrimRepository();
            _bookMappingRepository = new BookMappingRepository();
            _blackBookFacade = new BlackBookFacade(_chromeStyleToBookTrimRepository);
        }

        /// <summary>
        /// Alternative constructor provided for unit tested absent a db.
        /// </summary>
        /// <param name="bookMappingRepository"></param>
        /// <param name="chromeStyleToBookTrimRepository"> </param>
        /// <param name="blackBookFacade"> </param>
        internal StyleBasedMapping( IBookMappingRepository bookMappingRepository, 
                                    IChromeStyleToBookTrimRepository chromeStyleToBookTrimRepository,
                                    IBlackBookFacade blackBookFacade)
        {
            _bookMappingRepository = bookMappingRepository;
            _chromeStyleToBookTrimRepository = chromeStyleToBookTrimRepository;
            _blackBookFacade = blackBookFacade;
        }

       
        /// <summary>
        /// Return book data for the specified style and book. We assume this is US data.
        /// </summary>
        /// <param name="style"></param>
        /// <param name="country"></param>
        /// <param name="book"> </param>
        /// <returns></returns>
        internal IEnumerable<BookRecord> BookDataFromStyleAndCountry(int style, Countries country, IBook book)
        {
            // Blackbook works differently, since we don't have their data in a local db.
            if(book.Id == BookId.BlackBook)
            {
                return RepoBookDataFromStyleAndCountry(style, country, book).ToList();
            }

            // look for explicit style -> trim map data.
            var explicitChrometyleToTrimMap = FindBookTrimsFromExplicitChromeStyleMap(style, book).ToList();

            // get the book rollup by style and country.
            var bookRollup = RepoBookDataFromStyleAndCountry(style, country, book).ToList();

            foreach (var record in bookRollup.Where(record => explicitChrometyleToTrimMap.Contains(record.TrimId)))
            {
                record.ExplicitChromeStyleTrimMatch = true;
            }

            // which record should be the default?
            var explicitMatches = bookRollup.Where(r => r.ExplicitChromeStyleTrimMatch).ToList();
            var chromeMatches = bookRollup.Where(r => r.ChromeStyleMatch).ToList();

            // a single explicit match takes precendence, followed by a single chrome match.
            if (explicitMatches.Count() == 1)
            {
                explicitMatches.First().IsDefault = true;
            }
            else if( chromeMatches.Count() == 1)
            {
                chromeMatches.First().IsDefault = true;
            }
            else
            {
                foreach(var record in bookRollup)
                {
                    record.IsDefault = false;
                }
            }

            // if we only have a single record, it's the default
            if( bookRollup.Count == 1 )
            {
                bookRollup.First().IsDefault = true;
            }

            return bookRollup;

        }

        internal IEnumerable<BookRecord> RepoBookDataFromStyleAndCountry(int style, Countries country, IBook book)
        {
            switch (book.Id)
            {
                case BookId.Kbb:
                    return _bookMappingRepository.KbbDataFromStyleAndCountry(style, country).ToList();

                case BookId.Nada:
                    return _bookMappingRepository.NadaDataFromStyleAndCountry(style, country).ToList();

                case BookId.Galves:
                    return _bookMappingRepository.GalvesDataFromStyleAndCountry(style, country).ToList();

                case BookId.BlackBook:
                    return _blackBookFacade.BlackBookDataFromStyleAndCountry(style, country).ToList();
            }

            string message = String.Format("We don't currently support the book {0}", book.Desc);
            Log.Error(message);
            throw new BookMappingException(message);
        }

       /* private Tuple<bool,IEnumerable<BookRecord>>  BookDataFromStyleAndCountry_VerifyConditions(List<object> explicitChrometyleToTrimMap, 
                                                                  List<Tuple<string, int, bool>> chromeVinPatterns)
        {
            // if we don't have either, we can't continue.

            // check for no data
            if (!chromeVinPatterns.Any() && !explicitChrometyleToTrimMap.Any())
            {
                var message = String.Format("No chrome vin patterns or explicit mapping data were found");
                Log.Warn(message);
                return new Tuple<bool, IEnumerable<BookRecord>>(false, new List<BookRecord>());
            }

            // use explicit map if that's all we have.
            if (!chromeVinPatterns.Any() && explicitChrometyleToTrimMap.Any())
            {
                // we have an explicit map, but no chrome rollup data.  Use the explicit map.
                var trimId = explicitChrometyleToTrimMap.First();
                // TODO: Get the trim name.
                var trim = string.Empty;
                var record = new BookRecord { Book = book, ChromeStyleId = trimId, Trim = trim };
                return new List<BookRecord> { record };
            }

            if (chromeVinPatterns.Count() > 1)
            {
                var message = String.Format("Multiple chrome vin pattern found for style {0} and countryCode {1}", style, country);
                Log.Debug(message);
            }

        }*/

        /// <summary>
        /// Evaluate the current bookRecord against a list of distinct book records. Determine which should be the "default" record, if any.
        /// </summary>
        /// <param name="explicitChromeStyleMatch"></param>
        /// <param name="bookRecord"></param>
        /// <param name="distinctBookRecords"></param>
        internal static void DetermineDefaultRecord(bool explicitChromeStyleMatch, BookRecord bookRecord, List<BookRecord> distinctBookRecords)
        {

            // if this is the only book record, then it's the default.
            if (distinctBookRecords.Count == 0)
            {
                bookRecord.IsDefault = true;
                return;
            }

            // Do we already have a "default" book record? We cannot have two. we either have to pick one or not specify a default.
            // If no default record exists, this one should be used. 
            var defaultRecords = distinctBookRecords.Where(r => r.IsDefault).ToList();
            if (defaultRecords.Count == 0)
            {
                bookRecord.IsDefault = true;
                return;
            }

            // It shouldn't be possible to have more than one default.
            if (defaultRecords.Count() > 1)
            {
                // we shouldn't be in this state.
                string message = "Multiple default book records detected.";
                Log.Error(message);
                throw new VehicleConfigException(message);
            }

            // one default record already exists. compare it to this one and decide what to do.
            if (defaultRecords.Count() == 1)
            {
                var previousDefault = defaultRecords.First();

                // Look at the explicit maps. This data trumps the other data points.
                if (previousDefault.ExplicitChromeStyleTrimMatch && explicitChromeStyleMatch)
                {
                    // both cannot be the default, so neither will be.
                    previousDefault.IsDefault = false;
                    bookRecord.IsDefault = false;
                    return;
                }

                if (previousDefault.ExplicitChromeStyleTrimMatch && !explicitChromeStyleMatch)
                {
                    // the previous is still the default.
                    previousDefault.IsDefault = true;
                    bookRecord.IsDefault = false;
                    return;
                }

                if (!previousDefault.ExplicitChromeStyleTrimMatch && explicitChromeStyleMatch)
                {
                    previousDefault.IsDefault = false;
                    bookRecord.IsDefault = true;
                    return;
                }

                // compare the implied matches.
                if (previousDefault.ChromeStyleMatch && bookRecord.ChromeStyleMatch)
                {
                    // both are implied matches, so neither can be the default.
                    previousDefault.IsDefault = false;
                    bookRecord.IsDefault = false;
                    return;
                }
                if (previousDefault.ChromeStyleMatch && !bookRecord.ChromeStyleMatch)
                {
                    // previous remains the default.
                    previousDefault.IsDefault = true;
                    bookRecord.IsDefault = false;
                    return;
                }
                if (!previousDefault.ChromeStyleMatch && bookRecord.ChromeStyleMatch)
                {
                    // this one is the default.
                    previousDefault.IsDefault = false;
                    bookRecord.IsDefault = true;
                    return;
                }
            }
        }

        /// <summary>
        /// Look for direct mappings between the Chrome style and the book, provide by Chrome.
        /// </summary>
        /// <param name="style"></param>
        /// <param name="book"></param>
        /// <returns>Book trims explicitly mapped to the specified style.</returns>
        private IEnumerable<dynamic> FindBookTrimsFromExplicitChromeStyleMap(int style, IBook book)
        {
            IEnumerable<string> chromeMatches;

            // Let's see if we have a direct mapping between the style and the book.
            switch (book.Id)
            {
                case BookId.BlackBook:
                    chromeMatches = _chromeStyleToBookTrimRepository.BlackBookUvcFromStyle(style);
                    break;
                case BookId.Kbb:
                    chromeMatches = _chromeStyleToBookTrimRepository.KbbVehicleIdFromStyle(style);
                    break;
                case BookId.Galves:
                    chromeMatches = _chromeStyleToBookTrimRepository.GalvesPhGuidFromStyle(style);
                    break;
                case BookId.Nada:
                    chromeMatches = _chromeStyleToBookTrimRepository.NadaUvcFromStyle(style);
                    break;
                default:
                    string message = String.Format("Invalid book {0} specified.", book.Desc);
                    Log.Error(message);
                    throw new BookMappingException(message);
            }
            return chromeMatches;
        }

        /// <summary>
        /// Return book data for the specified style, assuming the country is US.
        /// </summary>
        /// <param name="style"></param>
        /// <param name="book"> </param>
        /// <returns></returns>
        internal IEnumerable<BookRecord> BookDataFromStyle(int style, IBook book)
        {
            return BookDataFromStyleAndCountry(style, Countries.Usa, book);
        }


        /*
        private IEnumerable<BookRecord> FindRecordsByExplicitChromeStyleMap(IEnumerable<string> explicitlyMappedBookTrims, IBook book)
        {
            var bookRecords = new List<BookRecord>();

            foreach(var trim in explicitlyMappedBookTrims)
            {
                switch (book.Id)
                {
                    case BookId.Kbb:
                        var vehicleId = Int32.Parse(trim);
                        bookRecords.AddRange(_bookMappingRepository.KbbDataFromVehicleId(vehicleId));
                        break;
                    case BookId.Nada:
                        throw new NotImplementedException();
                    case BookId.BlackBook:
                        var uvc = trim;
                        bookRecords.AddRange(_blackBookFacade.BlackBookDataFromUvc(uvc));
                        break;
                    case BookId.Galves:
                        throw new NotImplementedException();
                    default:
                        string message = String.Format("Invalid book {0} specified.", book.Desc);
                        Log.Error(message);
                        throw new BookMappingException(message);

                }
            }
            return bookRecords;
        }
*/
    }
}
