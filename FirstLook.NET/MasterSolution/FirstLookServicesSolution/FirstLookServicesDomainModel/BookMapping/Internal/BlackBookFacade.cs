﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model;
using FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Model.Xml;
using ServiceStack.MiniProfiler;
using FirstLook.FirstLookServices.DomainModel.Caching;
namespace FirstLook.FirstLookServices.DomainModel.BookMapping.Internal
{
    internal interface IBlackBookFacade
    {
        IEnumerable<string> UvcFromStyle(int style);
        IEnumerable<string> UvcFromVin(string vin);

        IEnumerable<BlackBookRecord> BlackBookDataFromVin(string vin);
        IEnumerable<BlackBookRecord> BlackBookDataFromStyleAndCountry(int style, Countries country);
    }

    internal class BlackBookFacade : IBlackBookFacade
    {
//        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IChromeStyleToBookTrimRepository _repository;
        private BlackBookServiceLookup _lookup;

        internal BlackBookFacade()
        {
            _repository = new ChromeStyleToBookTrimRepository();
            _lookup = new BlackBookServiceLookup();
        }

        /// <summary>
        /// Alternate constructor provided for unit testing without db or live service.
        /// </summary>
        /// <param name="repository"></param>
        internal BlackBookFacade(IChromeStyleToBookTrimRepository repository)
        {
            _repository = repository;
            _lookup = new BlackBookServiceLookup();
        }

        public IEnumerable<BlackBookRecord> BlackBookDataFromVin(string vin)
        {
            var cars = _lookup.GetCarsByVin(vin);
            var bookRecords = new List<BlackBookRecord>();
            foreach(var car in cars.Values)
            {
                var uvc = "";
                BlackBookRecord r = new BlackBookRecord();
                r.Book = BookFactory.Instance.Get(BookId.BlackBook);
                r.Trim = car.Series;
                r.Uvc = car.Uvc;
                if (uvc=="" )
                    uvc = car.Uvc ?? "";
                r.Make = car.Make;
                r.Model = car.Model;
                r.Year = car.Year;
                r.Vin = car.Vin;
                bookRecords.Add(r);

                if (uvc != "")
                    this.AddCar(car);
            }
            return bookRecords;
        }

        public IEnumerable<BlackBookRecord> BlackBookDataFromStyleAndCountry(int style, Countries country)
        {
            var bookRecords = new List<BlackBookRecord>();

            // TODO: get all the styles in the chrome rollup. Do the following for all of them.

            // get the uvcs
            var uvcs = UvcFromStyle(style).ToList();
            var book = BookFactory.Instance.Get(BookId.BlackBook);

            // pair it down to a unique list.
            

            // for each of these uvcs, call the service
            
            foreach(var uvc in uvcs)
            {
                var cars = this.GetFromCarCache(uvc);
                if (cars == null)
                {
                    cars = (List<CarInfo>)_lookup.GetCarsByUvc(uvc).Values;
                }

                // capture the YMM, then place another request for YMM, thereby getting all of them.
               foreach(var car in cars)
                {
                    var r = new BlackBookRecord();

                    r.Year = car.Year;
                    r.Make = car.Make;
                    r.Model = car.Model;
                    r.Trim = car.Series;
                    r.Uvc = uvc;
                    r.Book = book;

                    bookRecords.Add(r);
                   
                } 
            }
            

            // which is the default?

            // if we got back multiple uvc matches for the style, we have no default.
            if( uvcs.Count > 1 )
            {
                foreach(var record in bookRecords)
                {
                    record.IsDefault = false;
                }
            }
            else if(uvcs.Count == 1 )
            {
                // if we only have one match, it's the default.
                if( bookRecords.Count(b => b.ExplicitChromeStyleTrimMatch) == 1)
                {
                    bookRecords.First(b => b.ExplicitChromeStyleTrimMatch).IsDefault = true;
                }
            }

            return bookRecords;
        }


        public IEnumerable<string> UvcFromStyle(int style)
        {
            return _repository.BlackBookUvcFromStyle(style);
        }

        

        /// <summary>
        /// Look up the mapped uvcs from the vin. Note, this method makes remote service calls to the BlackBook web service.
        /// These service calls are restricted by IP address. ITO added an http proxy at the colo. If you are running this code
        /// in debug mode, it will use the proxy.
        /// </summary>
        /// <param name="vin"></param>
        /// <returns></returns>
        public IEnumerable<string> UvcFromVin(string vin)
        {
            return _lookup.GetCarsByVin(vin).Values.Select(c => c.Uvc);
        }

        
    }

    internal class BlackBookServiceLookup
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        internal Cars GetCarsByVin(string vin)
        {
            var profiler = Profiler.Current;

            XmlReader data;

            using (profiler.Step(string.Format("Calling black book service with vin:{0}", vin)))
            {
                try
                {
                    // TODO: Set reasonable timout. Don't accep the .NET default of 100s.
                    // Also, we should cache the results.
                    var blackBookService = new XmlService();
                    data = blackBookService.Vin(vin);
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    throw;
                }
            }

            // Deserialize the data.
            using (profiler.Step(string.Format("Deserializing black book data for vin:{0}", vin)))
            {
                var serializer = new CarsSerializer();
                var cars = serializer.Deserialize(data);

                return cars;
            }
        }

        internal Cars GetCarsByUvc(string uvc)
        {
            var profiler = Profiler.Current;

                XmlReader data;

                using (profiler.Step(string.Format("Calling black book service with uvc:{0}", uvc)))
                {
                    try
                    {
                        // TODO: Set reasonable timout. Don't accep the .NET default of 100s.
                        // Also, we should cache the results.
                        var blackBookService = new XmlService();
                        data = blackBookService.Uvc(uvc);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex);
                        throw;
                    }
                }


                // Deserialize the data.
                using (profiler.Step(string.Format("Deserializing black book data for uvc:{0}", uvc)))
                {
                    var serializer = new CarsSerializer();
                    var cars = serializer.Deserialize(data);
                    this.AddToCarCache((List<CarInfo>)cars.Values, uvc);
                    return cars;
                }
        }
        
        /* leaving commented for now but we haven't needed since 5-13-2013
        internal Cars GetByYearMakeModelSeries(int year, string make, string model, string series)
        {
            var profiler = Profiler.Current;

            XmlReader data;

            using (profiler.Step(string.Format("Calling black book service with year:{0}, make:{1}, model:{2}, series:{3}", year,make,model,series)))
            {
                try
                {
                    // TODO: Set reasonable timout. Don't accep the .NET default of 100s.
                    // Also, we should cache the results.
                    var blackBookService = new XmlService();
                    data = blackBookService.Query(year, make, model, series);
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    throw;
                }
            }

            // Deserialize the data.
            using (profiler.Step(string.Format("Deserializing black book data with year:{0}, make:{1}, model:{2}, series:{3}", year,make,model,series)))
            {
                var serializer = new CarsSerializer();
                var cars = serializer.Deserialize(data);

                return cars;
            }
        }
        */
    }
}
