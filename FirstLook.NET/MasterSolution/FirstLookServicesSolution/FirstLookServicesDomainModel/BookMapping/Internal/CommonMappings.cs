﻿using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.BookMapping.Internal
{
    internal enum Countries
    {
        Usa = 1,
        Canada = 2
    }


    internal class CommonMappings
    {
        /*
            Style – non blackbook (as we currently only have chrome used valuation mapping for blackbook)
            -          _chrome rollup_
            -          rollup to everything with same YMM
            -          if a single book trim in the set of trims is a vin pattern match, it should be the default
 
            Style – blackbook (we have chrome used valuation mapping for blackbook)
            -          if the selected style maps to a single bb trim, return only it
            -          if the selected style maps to multiple bb trims, return all
            -          if the selected style maps to no bb trims, return none

            Vins
            -          if vin maps to a single book specific trim (vin pattern match), return only it
            -          if vin maps to multiple book specific trims on vin pattern, return all vin pattern matches
            -          if the vin does not map to any book specific trim on vin pattern, return none

        */

        private readonly IBookMappingRepository _bookMappingRepository;
        private readonly IBlackBookFacade _blackBookFacade;
        private readonly IChromeStyleToBookTrimRepository _chromeStyleToBookTrimRepository;

        private readonly StyleBasedMapping _styleBasedMapping;
        private readonly VinBasedMapping _vinBasedMapping;

        internal CommonMappings()
        {
            _bookMappingRepository = new BookMappingRepository();
            _chromeStyleToBookTrimRepository = new ChromeStyleToBookTrimRepository();
            _blackBookFacade = new BlackBookFacade(_chromeStyleToBookTrimRepository);

            _styleBasedMapping = new StyleBasedMapping(_bookMappingRepository, _chromeStyleToBookTrimRepository, _blackBookFacade);
            _vinBasedMapping = new VinBasedMapping(_bookMappingRepository, _chromeStyleToBookTrimRepository, _blackBookFacade);
        }

        /// <summary>
        /// Alternative constructor provided so CommonMappings can be unit tested absent a db.
        /// </summary>
        /// <param name="bookMappingRepository"></param>
        /// <param name="chromeStyleToBookTrimRepository"> </param>
        /// <param name="blackBookFacade"> </param>
        internal CommonMappings(IBookMappingRepository bookMappingRepository, 
                                IChromeStyleToBookTrimRepository chromeStyleToBookTrimRepository,
                                IBlackBookFacade blackBookFacade)
        {
            _bookMappingRepository = bookMappingRepository;
            _chromeStyleToBookTrimRepository = chromeStyleToBookTrimRepository;
            _blackBookFacade = blackBookFacade;
            _styleBasedMapping = new StyleBasedMapping(_bookMappingRepository, _chromeStyleToBookTrimRepository, _blackBookFacade);
            _vinBasedMapping = new VinBasedMapping(_bookMappingRepository, _chromeStyleToBookTrimRepository, _blackBookFacade);
        }


        /// <summary>
        /// Given a vin, year, and book, 
        ///     if vin maps to a single book specific trim (vin pattern match), return only it
        ///     if vin maps to multiple book specific trims on vin pattern, return all vin pattern matches
        ///     if the vin does not map to any book specific trim on vin pattern, return none
        /// </summary>
        /// <param name="year"></param>
        /// <param name="vin"> </param>
        /// <param name="book"></param>
        /// <returns></returns>
        internal IEnumerable<BookRecord> BookDataFromVin(string vin, IBook book, int? year = null)
        {
            return _vinBasedMapping.BookDataFromVin(vin, book, year);
        }

        /// <summary>
        ///    Style – non blackbook (as we currently only have chrome used valuation mapping for blackbook)
        ///    -          _chrome rollup_
        ///    -          rollup to everything with same YMM
        ///    -          if a single book trim in the set of trims is a vin pattern match, it should be the default
        /// 
        ///    Style – blackbook (we have chrome used valuation mapping for blackbook)
        ///    -          if the selected style maps to a single bb trim, return only it
        ///    -          if the selected style maps to multiple bb trims, return all
        ///    -          if the selected style maps to no bb trims, return none
        ///
        /// </summary>
        /// <param name="style"></param>
        /// <param name="book"></param>
        /// <returns></returns>
        public IEnumerable<BookRecord> BookDataFromStyle(int style, IBook book)
        {
            return _styleBasedMapping.BookDataFromStyle(style, book);
        }
    }
}
