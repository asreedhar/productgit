﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.BookMapping
{
    public class BookMappingException : ApplicationException
    {
        internal BookMappingException(string message) : base(message)
        {
        }
    }
}
