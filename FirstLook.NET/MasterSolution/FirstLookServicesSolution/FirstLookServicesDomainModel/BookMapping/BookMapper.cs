using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.FirstLookServices.DomainModel.BookMapping.Internal;

namespace FirstLook.FirstLookServices.DomainModel.BookMapping
{
    /// <summary>
    /// This is the public interface against which clients should depend. It maps styles and vins into the books.
    /// </summary>
    public interface IBookMapper
    {
        IEnumerable<Tuple<int, bool>> KbbTrimIds(int style);
        IEnumerable<Tuple<int, bool>> KbbTrimIds(string vin);

        IEnumerable<Tuple<int, bool>> NadaTrimIds(int style);
        IEnumerable<Tuple<int, bool>> NadaTrimIds(string vin);

        IEnumerable<Tuple<int, bool>> GalvesTrimIds(int style);
        IEnumerable<Tuple<int, bool>> GalvesTrimIds(string vin);

        IEnumerable<Tuple<string, bool>> BlackBookTrimIds(int style);
        IEnumerable<Tuple<string, bool>> BlackBookTrimIds(string vin);
    }

    /// <summary>
    /// This is the class that implements the public-facing interface IBookMapper. 
    /// It also implements an internal interface which is used within the domain.
    /// </summary>
    internal class BookMapper : IBookMapper
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /*
            Style � non blackbook
            -          chrome rollup
            -          rollup to everything with same YMM
            -          if a single book trim in the set of trims is a vin pattern match, it should be the default
 
            Style � blackbook
            -          if the selected style maps to a single bb trim, return only it
            -          if the selected style maps to multiple bb trims, return all
            -          if the selected style maps to no bb trims, return none
         
            Vins
            -          if vin maps to a single book specific trim (vin pattern match), return only it
            -          if vin maps to multiple book specific trims on vin pattern, return all vin pattern matches
            -          if the vin does not map to any book specific trim on vin pattern, return none

        */


        private readonly CommonMappings _commonMappings;

        private readonly List<BookRecord> _kbbTrims;
        private readonly List<BookRecord> _nadaTrims;
        private readonly List<BookRecord> _galvesTrims;
        private readonly List<BookRecord> _blackbookTrims; 

        public BookMapper()
        {
            _commonMappings = new CommonMappings();

            _kbbTrims = new List<BookRecord>();
            _nadaTrims = new List<BookRecord>();
            _galvesTrims = new List<BookRecord>();
            _blackbookTrims = new List<BookRecord>();
        }

        #region kbb
        public IEnumerable<Tuple<int, bool>> KbbTrimIds(int style)
        {
            return GetTuples<int>(KbbTrims(style));
        }

        public IEnumerable<Tuple<int, bool>> KbbTrimIds(string vin)
        {
            return GetTuples<int>(KbbTrims(vin));
        }

        #endregion

        #region nada
        public IEnumerable<Tuple<int, bool>> NadaTrimIds(int style)
        {
            return GetTuples<int>(NadaTrims(style));
        }

        public IEnumerable<Tuple<int, bool>> NadaTrimIds(string vin)
        {
            return GetTuples<int>(NadaTrims(vin));
        }
        #endregion

        #region galves
        public IEnumerable<Tuple<int, bool>> GalvesTrimIds(int style)
        {
            return GetTuples<int>(GalvesTrims(style));
        }

        public IEnumerable<Tuple<int, bool>> GalvesTrimIds(string vin)
        {
            return GetTuples<int>(GalvesTrims(vin));
        }
        #endregion

        #region blackbook
        public IEnumerable<Tuple<string, bool>> BlackBookTrimIds(int style)
        {
            return GetTuples<string>(BlackBookTrims(style));
        }

        public IEnumerable<Tuple<string, bool>> BlackBookTrimIds(string vin)
        {
            return GetTuples<string>(BlackBookTrims(vin));
        }
        #endregion

        #region Internal

        /// <summary>
        /// Load the bookRecords into the specified list. Return tuples of ID / IsDefault from the list.
        /// </summary>
        /// <param name="bookRecords"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        private void LoadList(IEnumerable<BookRecord> bookRecords, List<BookRecord> list)
        {
            list.Clear();
            foreach (var record in bookRecords.Where(record => !list.Contains(record)))
            {
                list.Add(record);
            }
        }

        private IEnumerable<Tuple<T, bool>> GetTuples<T>(IEnumerable<BookRecord> list)
        {
            // note that we must be able to cast trimId to T            
            return list.Select(t => new Tuple<T, bool>((T)t.TrimId, t.IsDefault));
        }
        private IEnumerable<BookRecord> LoadTrims(IEnumerable<BookRecord> bookRecords, List<BookRecord> list)
        {
            LoadList(bookRecords, list);
            return list;
        }

        #region kbb
        internal IEnumerable<BookRecord> KbbTrims(int style)
        {
            try
            {
                var kbb = _commonMappings.BookDataFromStyle(style, BookFactory.Instance.Get(BookId.Kbb));
                return LoadTrims(kbb, _kbbTrims);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        internal IEnumerable<BookRecord> KbbTrims(string vin)
        {
            try
            {
                VinBasedMapping.ValidateVin(vin);
                var kbb = _commonMappings.BookDataFromVin(vin, BookFactory.Instance.Get(BookId.Kbb));
                return LoadTrims(kbb, _kbbTrims);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }
        #endregion

        internal IEnumerable<BookRecord> NadaTrims(int style)
        {
            try
            {
                var nada = _commonMappings.BookDataFromStyle(style, BookFactory.Instance.Get(BookId.Nada));
                return LoadTrims(nada, _nadaTrims);
            }
            catch (Exception ex)
            {
                Log.Error(ex);                
                throw;
            }
        }

        internal IEnumerable<BookRecord> NadaTrims(string vin)
        {
            try
            {
                VinBasedMapping.ValidateVin(vin);
                var nada = _commonMappings.BookDataFromVin(vin, BookFactory.Instance.Get(BookId.Nada));
                return LoadTrims(nada, _nadaTrims);
            }
            catch (Exception ex)
            {
                Log.Error(ex);                
                throw;
            }
        }

        internal IEnumerable<BookRecord> GalvesTrims(int style)
        {
            try
            {
                var galves = _commonMappings.BookDataFromStyle(style, BookFactory.Instance.Get(BookId.Galves));
                return LoadTrims(galves, _galvesTrims);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }
        internal IEnumerable<BookRecord> GalvesTrims(string vin)
        {
            try
            {
                VinBasedMapping.ValidateVin(vin);
                var galves = _commonMappings.BookDataFromVin(vin, BookFactory.Instance.Get(BookId.Galves));
                return LoadTrims(galves, _galvesTrims);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        internal IEnumerable<BookRecord> BlackBookTrims(int style)
        {
            try
            {
                var bb = _commonMappings.BookDataFromStyle(style, BookFactory.Instance.Get(BookId.BlackBook));
                return LoadTrims(bb, _blackbookTrims);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        internal IEnumerable<BookRecord> BlackBookTrims(string vin)
        {
            try
            {
                VinBasedMapping.ValidateVin(vin);
                var bb = _commonMappings.BookDataFromVin(vin, BookFactory.Instance.Get(BookId.BlackBook));
                return LoadTrims(bb, _blackbookTrims);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw;
            }
        }

        #endregion
    }
}