﻿using Autofac;
using FirstLook.Common.Core.IOC;
using FirstLook.Merchandising.DomainModel;
using MAX.Caching;
using Merchandising.Messages;

namespace FirstLook.FirstLookServices.DomainModel
{
    public class FirstlookServiceDomainRegister:IRegistryModule
    {

        #region IRegistryModule Members

        public void Register(ContainerBuilder builder)
        {
            builder.RegisterModule(new MerchandisingMessagesModule());
            builder.RegisterType<CacheKeyBuilder>().As<ICacheKeyBuilder>();
            builder.RegisterType<MemcachedClientWrapper>().As<ICacheWrapper>();
            new MerchandisingDomainRegistry().Register(builder);
            //builder.RegisterType<IPricingMessager>().As<AdMessageSender>();
        }

        #endregion
    }
}
