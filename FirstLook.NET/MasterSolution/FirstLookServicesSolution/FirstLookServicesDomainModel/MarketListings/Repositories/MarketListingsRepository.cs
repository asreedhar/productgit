﻿using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories.Gateways;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Model;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings
{
    public class MarketListingsRepository:IRepository
    {
        public List<ListingsAndCounts> FetchMarketListings(List<MarketListingsInput> input)
        {
            return new MarketListingsGateway().Fetch(input);
        }
        public IList<FacetEquipment> FetchTierOneEquipment(TierOneInput input)
        {
            return new TierOneEquipmentsGateway().Fetch(input);
        }
    }
}
