﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Model;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories.DataStore;
using Newtonsoft.Json;
using System.Linq;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories.Gateways
{
    public class MarketListingsGateway : GatewayBase
    {
        public List<ListingsAndCounts> Fetch(List<MarketListingsInput> input)
        {
            var datastore = Resolve<IMarketListingsDataStore>();

            //If recent and active required do not filter on active records.
            foreach (var inputData in input)
            {
                if (inputData.RecentAndActive == true)
                {
                    inputData.ForSaleIn14Days = null;
                    inputData.ForSaleNow = null;
                }
            }
            //Set sorting fields
            #region SortingRegions
            foreach (var inputData in input)
            {
                if (inputData.Sorts != null)
                {
                    if (inputData.Sorts[0].Field == "Age")
                    {
                        if (inputData.Sorts[0].Order == "asc")
                        {
                            inputData.Sorts[0].Order = "desc";
                        }
                        else
                        {
                            inputData.Sorts[0].Order = "asc";
                        }
                    }
                    inputData.Sorts.ForEach(st => st.Field = GetSortingField(st.Field));
                }
            }

            #endregion
            string str = datastore.GetMarketListings(input);
            FinalResult listingResult = JsonConvert.DeserializeObject<FinalResult>(str);

            List<ListingsAndCounts> result = GetResult(listingResult, input);

            //Get Listing Aggregate count
            if (input.Any(i => i.AggregateListings == true))
            {
                int ctr = 0;
                #region Get Active count
                input.ForEach(i => i.ForSaleNow = true);
                FinalResult ActiveCounts = JsonConvert.DeserializeObject<FinalResult>(datastore.GetMarketListings(input));
                foreach (var activeCount in ActiveCounts.CompleteResult)
                {
                    if (result[ctr] != null)
                    {
                        result[ctr].ListingTerms.ActiveCounts = Convert.ToInt32(activeCount.AllHits.Total);
                    }
                    else
                        break;
                    ctr++;
                }
                #endregion
            }

            if (input.Any(i => i.AggregateListings == true) || input.Any(i => i.RecentAndActive == true))
            {
                result = GetRecentActiveDetails(result, input);
            }
            return result;

        }

        private List<ListingsAndCounts> GetResult(FinalResult result, List<MarketListingsInput> input)
        {
            List<ListingsAndCounts> list = new List<ListingsAndCounts>();

            ListingsAndCounts listingsAndCounts = null;
            List<MarketListingsDetail> items = null;
            Facet facet = null;
            PriceStat priceStat = null;
            OdometerStat odometerStat = null;
            List<EquipmentCount> equipmentCount = null;

            List<EquipmentCount> facetCount = null;
            var datastore = Resolve<IMarketListingsDataStore>();

            try
            {

                int i = 0;
                if (result.CompleteResult != null)
                {
                    foreach (var res in result.CompleteResult)
                    {
                        FinalResult rankResult = new FinalResult();
                        List<Result> rankListings = new List<Result>();
                        listingsAndCounts = new ListingsAndCounts();
                        facet = new Facet();
                        priceStat = new PriceStat();
                        odometerStat = new OdometerStat();
                        facetCount = new List<EquipmentCount>();
                        items = new List<MarketListingsDetail>();
                        equipmentCount = new List<EquipmentCount>();
                        listingsAndCounts.CertifiedTerms = new CertifiedTerm();
                        listingsAndCounts.CertifiedTerms.Buckets = new List<EquipmentCount>();
                        listingsAndCounts.DriveTrainTerms = new DriveTrainTerm();
                        listingsAndCounts.DriveTrainTerms.Buckets = new List<EquipmentCount>();
                        listingsAndCounts.EngineTerms = new EngineTerm();
                        listingsAndCounts.EngineTerms.Buckets = new List<EquipmentCount>();
                        listingsAndCounts.FuelTypeTerms = new FuelTypeTerm();
                        listingsAndCounts.FuelTypeTerms.Buckets = new List<EquipmentCount>();
                        listingsAndCounts.TransmissionTerms = new TransmissionTerm();
                        listingsAndCounts.TransmissionTerms.Buckets = new List<EquipmentCount>();
                        listingsAndCounts.TrimTerms = new TrimTerm();
                        listingsAndCounts.TrimTerms.Buckets = new List<EquipmentCount>();
                        listingsAndCounts.ModelTerms = new ModelTerm();
                        listingsAndCounts.ModelTerms.Buckets = new List<EquipmentCount>();
                        listingsAndCounts.PriceHistogramTerm = new HistogramTerm();
                        listingsAndCounts.PriceHistogramTerm.Buckets = new List<HistogramCount>();
                        listingsAndCounts.MarketDaysSupplyInfo = new List<MarketDaysSupply>();
                        listingsAndCounts.ListingTerms = new ListingTerms();
                        if (Convert.ToBoolean((input[i].GetRank)) || (input[i].RecentAndActive == true && input[i].Size > 0))
                        {
                            MarketListingsInput rankInput = input[i];
                            listingsAndCounts.MarketPrices = new List<int>();
                            List<MarketListingsInput> rankInputs = new List<MarketListingsInput>();
                            rankInput.Size = res.AllHits.Total;
                            rankInput.From = 0;
                            rankInput.Comment = "Get Ranks";
                            rankInputs.Add(rankInput);
                            string rankData = datastore.GetMarketListings(rankInputs);
                            rankResult = JsonConvert.DeserializeObject<FinalResult>(rankData);
                            rankListings = rankResult.CompleteResult;
                            rankListings.ForEach(cr => cr.AllHits.Hit.ForEach(ah => listingsAndCounts.MarketPrices.Add(Convert.ToInt32(ah.Source.Advertisement.Price))));
                            
                            if (input[i].RecentAndActive == true && input[i].Size > 0)
                            {
                                //Get Recent Active listings for grid
                                rankResult.CompleteResult.FirstOrDefault().AllHits.Hit.Where(h=>h.Source.Advertisement.IsForSale==true ||h.Source.TimeLine.WasActive==true).Skip(Convert.ToInt32(input[i].From) == 0 ? 0 : Convert.ToInt32(input[i].From) - 1).Take(Convert.ToInt32(input[i].Size)).ToList().ForEach(hit => items.Add(new MarketListingsDetail()
                                {
                                    Age = Convert.ToInt32((DateTime.Now - hit.Source.Advertisement.FromDate).TotalDays),
                                    Certified = hit.Source.Advertisement.IsCertified,
                                    Color = hit.Source.ProductDetails.GenericColor,
                                    Year = hit.Source.ProductDetails.Year,
                                    Make = hit.Source.ProductDetails.Make,
                                    Model = hit.Source.ProductDetails.Model,
                                    Trim = hit.Source.ProductDetails.Trim,
                                    InternetPrice = hit.Source.Advertisement.Price,
                                    Mileage = hit.Source.Advertisement.Mileage,
                                    Vin = hit.Source.Vin,
                                    Seller = hit.Source.Advertisement.AdvertisementAdvertiser.Name,
                                    Location = hit.Source.Advertisement.AdvertisementAdvertiser.AdvertiserAddress.Location,
                                    AdText = hit.Source.AdText,
                                    StockNumber = hit.Source.Advertisement.StockNumber
                                }));
                            }
                            else
                            {
                                rankResult.CompleteResult.FirstOrDefault().AllHits.Hit.Skip(Convert.ToInt32(input[i].From) == 0 ? 0 : Convert.ToInt32(input[i].From) - 1).Take(Convert.ToInt32(input[i].Size)).ToList().ForEach(hit => items.Add(new MarketListingsDetail()
                                {
                                    Age = Convert.ToInt32((DateTime.Now - hit.Source.Advertisement.FromDate).TotalDays),
                                    Certified = hit.Source.Advertisement.IsCertified,
                                    Color = hit.Source.ProductDetails.GenericColor,
                                    Year = hit.Source.ProductDetails.Year,
                                    Make = hit.Source.ProductDetails.Make,
                                    Model = hit.Source.ProductDetails.Model,
                                    Trim = hit.Source.ProductDetails.Trim,
                                    InternetPrice = hit.Source.Advertisement.Price,
                                    Mileage = hit.Source.Advertisement.Mileage,
                                    Vin = hit.Source.Vin,
                                    Seller = hit.Source.Advertisement.AdvertisementAdvertiser.Name,
                                    Location = hit.Source.Advertisement.AdvertisementAdvertiser.AdvertiserAddress.Location,
                                    AdText = hit.Source.AdText,
                                    StockNumber = hit.Source.Advertisement.StockNumber
                                }));
                            }
                        }
                        else
                        {
                            res.AllHits.Hit.ForEach(hit => items.Add(new MarketListingsDetail()
                            {
                                Age = Convert.ToInt32((DateTime.Now - hit.Source.Advertisement.FromDate).TotalDays),
                                Certified = hit.Source.Advertisement.IsCertified,
                                Color = hit.Source.ProductDetails.GenericColor,
                                Year = hit.Source.ProductDetails.Year,
                                Make = hit.Source.ProductDetails.Make,
                                Model = hit.Source.ProductDetails.Model,
                                Trim = hit.Source.ProductDetails.Trim,
                                InternetPrice = hit.Source.Advertisement.Price,
                                Mileage = hit.Source.Advertisement.Mileage,
                                Vin = hit.Source.Vin,
                                Seller = hit.Source.Advertisement.AdvertisementAdvertiser.Name,
                                Location = hit.Source.Advertisement.AdvertisementAdvertiser.AdvertiserAddress.Location,
                                AdText = hit.Source.AdText,
                                StockNumber = hit.Source.Advertisement.StockNumber
                            }));
                        }
                        
                        
                      

                        if (res.Aggregations.EquipmentTerms != null && res.Aggregations.EquipmentTerms.Buckets != null)
                        {
                            foreach (var bucket in res.Aggregations.EquipmentTerms.Buckets)
                            {
                                equipmentCount.Add(bucket);
                            }

                            foreach (var count in equipmentCount)
                            {
                                facetCount.Add(count);
                            }
                        }

                        if (res.Aggregations.CertifiedTerms != null && res.Aggregations.CertifiedTerms.Buckets != null)
                        {
                            foreach (var certifiedCount in res.Aggregations.CertifiedTerms.Buckets)
                            {
                                listingsAndCounts.CertifiedTerms.Buckets.Add(certifiedCount);
                            }
                        }

                        if (res.Aggregations.DriveTrainTerms != null && res.Aggregations.DriveTrainTerms.Buckets != null)
                        {
                            foreach (var driveTrainCount in res.Aggregations.DriveTrainTerms.Buckets)
                            {
                                listingsAndCounts.DriveTrainTerms.Buckets.Add(driveTrainCount);
                            }
                        }

                        if (res.Aggregations.EngineTerms != null && res.Aggregations.EngineTerms.Buckets != null)
                        {
                            foreach (var engineCount in res.Aggregations.EngineTerms.Buckets)
                            {
                                listingsAndCounts.EngineTerms.Buckets.Add(engineCount);
                            }
                        }

                        if (res.Aggregations.FuelTypeTerms != null && res.Aggregations.FuelTypeTerms.Buckets != null)
                        {
                            foreach (var fuelCount in res.Aggregations.FuelTypeTerms.Buckets)
                            {
                                listingsAndCounts.FuelTypeTerms.Buckets.Add(fuelCount);
                            }
                        }

                        if (res.Aggregations.TransmissionTerms != null && res.Aggregations.TransmissionTerms.Buckets != null)
                        {
                            foreach (var transmissionCount in res.Aggregations.TransmissionTerms.Buckets)
                            {
                                listingsAndCounts.TransmissionTerms.Buckets.Add(transmissionCount);
                            }
                        }

                        if (res.Aggregations.TrimTerms != null && res.Aggregations.TrimTerms.Buckets != null)
                        {
                            foreach (var trimCount in res.Aggregations.TrimTerms.Buckets)
                            {
                                listingsAndCounts.TrimTerms.Buckets.Add(trimCount);
                            }
                        }

                        if (res.Aggregations.ModelTerms != null && res.Aggregations.ModelTerms.Buckets != null)
                        {
                            foreach (var modelCount in res.Aggregations.ModelTerms.Buckets)
                            {
                                listingsAndCounts.ModelTerms.Buckets.Add(modelCount);
                            }
                        }

                        if (res.Aggregations.PriceHistogramTerms != null && res.Aggregations.PriceHistogramTerms.Buckets != null)
                        {
                            foreach (var histogramCount in res.Aggregations.PriceHistogramTerms.Buckets)
                            {
                                listingsAndCounts.PriceHistogramTerm.Buckets.Add(histogramCount);
                            }
                        }
                        facet.FacetCounts = facetCount;
                        priceStat = res.Aggregations.PriceStats;
                        odometerStat = res.Aggregations.OdometerStats;
                        if (input[i].InventoryId != null)
                            listingsAndCounts.InventoryId = input[i].InventoryId;

                        //==================MarketDaysSupply===============
                        IDataReader reader = datastore.GetMarketDaysSupply(input[i]);

                        var serializer = Resolve<ISerializer<MarketDaysSupply>>();
                        IList<MarketDaysSupply> mkt = serializer.Deserialize(reader);
                        mkt.ToList().ForEach(m => listingsAndCounts.MarketDaysSupplyInfo.Add(m));

                        i++;
                        listingsAndCounts.Listings = items;
                        listingsAndCounts.Facets = facet;
                        listingsAndCounts.PriceStats = priceStat;
                        listingsAndCounts.OdometerStats = odometerStat;
                        listingsAndCounts.RecordCount = Convert.ToInt32(res.AllHits.Total);
                        list.Add(listingsAndCounts);
                    }
                }

            }

            catch (Exception e)
            {
                throw new Exception("Exception: " + e.Message);
            }

            return list;
        }

        private List<ListingsAndCounts> MergeResults(List<ListingsAndCounts> result1, List<ListingsAndCounts> result2)
        {
            int i = 0;
            foreach (var result in result2)
            {
                //Add Data
                result1[i].Listings.AddRange(result.Listings);

                //Update Aggregations
                result1[i].CertifiedTerms.Buckets.ForEach(r1b => r1b.DocCount = result.CertifiedTerms.Buckets.FirstOrDefault(rb => r1b.Key == rb.Key) == null ? 0 : r1b.DocCount + result.CertifiedTerms.Buckets.FirstOrDefault(rb => r1b.Key == rb.Key).DocCount);
                result1[i].CertifiedTerms.OtherDocCount += result.CertifiedTerms.OtherDocCount;
                result1[i].CertifiedTerms.UpperBound += result.CertifiedTerms.UpperBound;

                result1[i].DriveTrainTerms.Buckets.ForEach(r1b => r1b.DocCount = result.DriveTrainTerms.Buckets.FirstOrDefault(rb => r1b.Key == rb.Key) == null ? 0 : r1b.DocCount + result.DriveTrainTerms.Buckets.FirstOrDefault(rb => r1b.Key == rb.Key).DocCount);
                result1[i].DriveTrainTerms.OtherDocCount += result.DriveTrainTerms.OtherDocCount;
                result1[i].DriveTrainTerms.UpperBound += result.DriveTrainTerms.UpperBound;

                result1[i].EngineTerms.Buckets.ForEach(r1b => r1b.DocCount = result.EngineTerms.Buckets.FirstOrDefault(rb => r1b.Key == rb.Key) == null ? 0 : r1b.DocCount + result.EngineTerms.Buckets.FirstOrDefault(rb => r1b.Key == rb.Key).DocCount);
                result1[i].EngineTerms.OtherDocCount += result.EngineTerms.OtherDocCount;
                result1[i].EngineTerms.UpperBound += result.EngineTerms.UpperBound;

                result1[i].FuelTypeTerms.Buckets.ForEach(r1b => r1b.DocCount = result.FuelTypeTerms.Buckets.FirstOrDefault(rb => r1b.Key == rb.Key) == null ? 0 : r1b.DocCount + result.FuelTypeTerms.Buckets.FirstOrDefault(rb => r1b.Key == rb.Key).DocCount);
                result1[i].FuelTypeTerms.OtherDocCount += result.FuelTypeTerms.OtherDocCount;
                result1[i].FuelTypeTerms.UpperBound += result.FuelTypeTerms.UpperBound;

                result1[i].ModelTerms.Buckets.ForEach(r1b => r1b.DocCount = result.ModelTerms.Buckets.FirstOrDefault(rb => r1b.Key == rb.Key) == null ? 0 : r1b.DocCount + result.ModelTerms.Buckets.FirstOrDefault(rb => r1b.Key == rb.Key).DocCount);
                result1[i].ModelTerms.OtherDocCount += result.ModelTerms.OtherDocCount;
                result1[i].ModelTerms.UpperBound += result.ModelTerms.UpperBound;

                result1[i].TransmissionTerms.Buckets.ForEach(r1b => r1b.DocCount = result.TransmissionTerms.Buckets.FirstOrDefault(rb => r1b.Key == rb.Key) == null ? 0 : r1b.DocCount + result.TransmissionTerms.Buckets.FirstOrDefault(rb => r1b.Key == rb.Key).DocCount);
                result1[i].TransmissionTerms.OtherDocCount += result.TransmissionTerms.OtherDocCount;
                result1[i].TransmissionTerms.UpperBound += result.TransmissionTerms.UpperBound;

                result1[i].TrimTerms.Buckets.ForEach(r1b => r1b.DocCount = result.TrimTerms.Buckets.FirstOrDefault(rb => r1b.Key == rb.Key) == null ? 0 : r1b.DocCount + result.TrimTerms.Buckets.FirstOrDefault(rb => r1b.Key == rb.Key).DocCount);
                result1[i].TrimTerms.OtherDocCount += result.TrimTerms.OtherDocCount;
                result1[i].TrimTerms.UpperBound += result.TrimTerms.UpperBound;

                result1[i].Facets.FacetCounts.ForEach(r1b => r1b.DocCount = result.Facets.FacetCounts.FirstOrDefault(rb => r1b.Key == rb.Key) == null ? 0 : r1b.DocCount + result.Facets.FacetCounts.FirstOrDefault(rb => r1b.Key == rb.Key).DocCount);

                result1[i].OdometerStats.Sum += result.OdometerStats.Sum;
                result1[i].OdometerStats.Count += result.OdometerStats.Count;
                result1[i].OdometerStats.Avg = result1[i].OdometerStats.Count == 0 ? 0 : result1[i].OdometerStats.Sum / result.OdometerStats.Count;
                result1[i].OdometerStats.AvgAsString = Convert.ToString(result1[i].OdometerStats.Avg);
                result1[i].OdometerStats.SumAsString = Convert.ToString(result1[i].OdometerStats.Sum);
                result1[i].OdometerStats.Max = result.OdometerStats.Max > result1[i].OdometerStats.Max ? result.OdometerStats.Max : result1[i].OdometerStats.Max;
                result1[i].OdometerStats.Min = result.OdometerStats.Min < result1[i].OdometerStats.Min ? result.OdometerStats.Min : result1[i].OdometerStats.Min;
                result1[i].OdometerStats.MinAsString = Convert.ToString(result1[i].OdometerStats.Min);
                result1[i].OdometerStats.MaxAsString = Convert.ToString(result1[i].OdometerStats.Max);

                result1[i].PriceHistogramTerm.Buckets.ForEach(r1b => r1b.DocCount = result.PriceHistogramTerm.Buckets.FirstOrDefault(rb => r1b.Key == rb.Key) == null ? 0 : r1b.DocCount + result.PriceHistogramTerm.Buckets.FirstOrDefault(rb => r1b.Key == rb.Key).DocCount);

                result1[i].PriceStats.Sum += result.PriceStats.Sum;
                result1[i].PriceStats.Count += result.PriceStats.Count;
                result1[i].PriceStats.Avg = result1[i].PriceStats.Count == 0 ? 0 : result1[i].PriceStats.Sum / result.PriceStats.Count;
                result1[i].PriceStats.AvgAsString = Convert.ToString(result1[i].PriceStats.Avg);
                result1[i].PriceStats.SumAsString = Convert.ToString(result1[i].PriceStats.Sum);
                result1[i].PriceStats.Max = result.PriceStats.Max > result1[i].PriceStats.Max ? result.PriceStats.Max : result1[i].PriceStats.Max;
                result1[i].PriceStats.Min = result.PriceStats.Min < result1[i].PriceStats.Min ? result.PriceStats.Min : result1[i].PriceStats.Min;
                result1[i].PriceStats.MinAsString = Convert.ToString(result1[i].PriceStats.Min);
                result1[i].PriceStats.MaxAsString = Convert.ToString(result1[i].PriceStats.Max);

                result1[i].RecordCount += result.RecordCount;
                result1[i].ListingTerms.ActiveCounts = result1[i].RecordCount;
                result1[i].ListingTerms.RecentActiveCounts = result1[i].RecordCount + result.RecordCount;

                i++;
            }

            return result1;
        }

        private AggregateTerms MergeTerms(AggregateTerms source1, AggregateTerms source2)
        {
            if (source1 == null)
                return source2;
            if (source2 == null)
                return source1;

            List<EquipmentCount> newBuckets = new List<EquipmentCount>();
            foreach (var bucket in source1.Buckets)
            {
                var data = source2.Buckets.Where(b => b.Key == bucket.Key);
                if (data != null && data.Count()>0)
                {
                    bucket.DocCount += data.FirstOrDefault().DocCount;
                    source2.Buckets.Remove(data.FirstOrDefault());
                }
            }
            source1.OtherDocCount += source2.OtherDocCount;
            source1.UpperBound += source2.UpperBound;

            //Add buckets which are not common
            newBuckets.AddRange(source2.Buckets);

            source1.Buckets.AddRange(newBuckets);
            return source1;
        }

        private AggregateStats MergeStats(AggregateStats source1, AggregateStats source2)
        {
            if (source1 == null)
                return source2;
            if (source2 == null)
                return source1;

            source1.Sum = (Convert.ToDecimal(source1.Sum) + Convert.ToDecimal(source2.Sum));
            source1.Count = (Convert.ToInt32(source1.Count) + Convert.ToInt32(source2.Count));
            source1.Avg = source1.Count == 0 ? 0 : source1.Sum / source1.Count;
            source1.AvgAsString = Convert.ToString(source1.Avg);
            source1.Max = Convert.ToDecimal(source1.Max) > Convert.ToDecimal(source2.Max) ? Convert.ToDecimal(source1.Max) : Convert.ToDecimal(source2.Max);
            source1.Min = Convert.ToDecimal(source1.Min) < Convert.ToDecimal(source2.Min) ? Convert.ToDecimal(source1.Min) :Convert.ToDecimal( source2.Min);
            source1.MaxAsString = Convert.ToString(source1.Max);
            source1.MinAsString = Convert.ToString(source1.Min);
            source1.SumAsString = Convert.ToString(source1.Sum);
            return source1;
        }

        private List<ListingsAndCounts> GetRecentActiveDetails(List<ListingsAndCounts> result, List<MarketListingsInput> input)   
        {
            var datastore = Resolve<IMarketListingsDataStore>();
             #region Get Recent and active count
            
                input.Where(i=>i.RecentAndActive==true||i.AggregateListings==true).ToList().ForEach(i => { i.ForSaleIn14Days = true; i.ForSaleNow = null; });

                FinalResult recentListings = JsonConvert.DeserializeObject<FinalResult>(datastore.GetMarketListings(input));
                int ctr = 0;
                foreach (var activeCount in recentListings.CompleteResult)
                {
                    if (result[ctr] != null)
                    {
                        result[ctr].ListingTerms.RecentActiveCounts = Convert.ToInt32(activeCount.AllHits.Total);
                        //Get Recent listing first.
                        if (input[ctr].RecentAndActive == true)
                        {
                            result[ctr].CertifiedTerms = activeCount.Aggregations.CertifiedTerms;
                            result[ctr].DriveTrainTerms = activeCount.Aggregations.DriveTrainTerms;
                            result[ctr].EngineTerms = activeCount.Aggregations.EngineTerms;
                            result[ctr].FuelTypeTerms = activeCount.Aggregations.FuelTypeTerms;
                            result[ctr].ModelTerms = activeCount.Aggregations.ModelTerms;
                            result[ctr].OdometerStats = activeCount.Aggregations.OdometerStats;
                            result[ctr].PriceHistogramTerm = activeCount.Aggregations.PriceHistogramTerms;
                            result[ctr].PriceStats = activeCount.Aggregations.PriceStats;
                            result[ctr].TransmissionTerms = activeCount.Aggregations.TransmissionTerms;
                            result[ctr].TrimTerms = activeCount.Aggregations.TrimTerms;

                            result[ctr].RecordCount = Convert.ToInt32(activeCount.AllHits.Total);
                        }
                    }
                    else
                        break;
                    ctr++;
                }

                input.Where(i=>i.RecentAndActive==true||i.AggregateListings==true).ToList().ForEach(i => { i.ForSaleNow = true; i.ForSaleIn14Days = false; });
                FinalResult nonRecentActiveListings = JsonConvert.DeserializeObject<FinalResult>(datastore.GetMarketListings(input));
                ctr = 0;
                foreach (var activeCount in nonRecentActiveListings.CompleteResult)
                {
                    if (result[ctr] != null)
                    {
                        result[ctr].ListingTerms.RecentActiveCounts += Convert.ToInt32(activeCount.AllHits.Total);

                        //Merge non recent Active listings
                        if (input[ctr].RecentAndActive == true)
                        {
                            result[ctr].CertifiedTerms = MergeTerms(result[ctr].CertifiedTerms, activeCount.Aggregations.CertifiedTerms) as CertifiedTerm;
                            result[ctr].DriveTrainTerms = MergeTerms(result[ctr].DriveTrainTerms, activeCount.Aggregations.DriveTrainTerms) as DriveTrainTerm;
                            result[ctr].EngineTerms = MergeTerms(result[ctr].EngineTerms, activeCount.Aggregations.EngineTerms) as EngineTerm;
                            result[ctr].FuelTypeTerms = MergeTerms(result[ctr].FuelTypeTerms, activeCount.Aggregations.FuelTypeTerms) as FuelTypeTerm;
                            result[ctr].ModelTerms = MergeTerms(result[ctr].ModelTerms, activeCount.Aggregations.ModelTerms) as ModelTerm;
                            result[ctr].OdometerStats = MergeStats(result[ctr].OdometerStats, activeCount.Aggregations.OdometerStats) as OdometerStat;
                            result[ctr].PriceHistogramTerm = activeCount.Aggregations.PriceHistogramTerms;
                            result[ctr].PriceStats = MergeStats(result[ctr].PriceStats, activeCount.Aggregations.PriceStats) as PriceStat;
                            result[ctr].TransmissionTerms = MergeTerms(result[ctr].TransmissionTerms, activeCount.Aggregations.TransmissionTerms) as TransmissionTerm;
                            result[ctr].TrimTerms = MergeTerms(result[ctr].TrimTerms, activeCount.Aggregations.TrimTerms) as TrimTerm;
                            result[ctr].RecordCount += Convert.ToInt32(activeCount.AllHits.Total);
                        }
                    }
                    else
                        break;
                    ctr++;
                }
                #endregion

            
            return result;
        }


        private string GetSortingField(string columnName)
        {
            string columnNameField = columnName.ToLower();
            string fieldName = "";
            switch (columnNameField)
            {
                case "age": fieldName = "advertisement.from";
                    break;
                case "seller": fieldName = "advertisement.advertiser.name";
                    break;
                case "iscertified": fieldName = "advertisement.is_certified";
                    break;
                case "color": fieldName = "product_details.exterior_color_name_generic";
                    break;
                case "mileage": fieldName = "advertisement.odometer";
                    break;
                case "internetprice": fieldName = "advertisement.price";
                    break;
                case "pmarketaverage": fieldName = "advertisement.price";
                    break;
                case "vin": fieldName = "vin";
                    break;
                case "year": fieldName = "product_details.year";
                    break;
                case "make": fieldName = "product_details.make";
                    break;
                case "model": fieldName = "product_details.model";
                    break;
                case "trim": fieldName = "product_details.trim";
                    break;
                case "distance": fieldName = "geo_distance";
                    break;
                case "IsCertified": fieldName = "advertisement.is_certified";
                    break;
                default: fieldName = "vin";
                    break;
            }

            return fieldName;
        }
    }
}
