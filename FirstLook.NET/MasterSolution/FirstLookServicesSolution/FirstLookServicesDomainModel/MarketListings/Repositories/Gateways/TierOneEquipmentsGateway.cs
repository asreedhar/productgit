﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories.DataStore;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Model;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories.Gateways
{
    class TierOneEquipmentsGateway:GatewayBase
    {
        public IList<FacetEquipment> Fetch(TierOneInput input)
        {
            string key = CreateCacheKey("" + input.InventoryId);
            IList<FacetEquipment> value = Cache.Get(key) as List<FacetEquipment>;

            if (value == null)
            {
                var datastore = Resolve<ITierOneDatastore>();
                IDataReader reader = datastore.GetTierOneEquipment(input);

                var serializer = Resolve<ISerializer<FacetEquipment>>();
                value = serializer.Deserialize(reader);
                
                Remember(key, value);
            }

            return value;
            
        }
    }
}
