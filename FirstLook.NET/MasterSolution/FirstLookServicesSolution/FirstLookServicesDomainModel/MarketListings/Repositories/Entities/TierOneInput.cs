﻿namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories.Entities
{
    public class TierOneInput
    {
        public int BusinessUnitId { get; set; }
        public int InventoryId { get; set; }
    }
}
