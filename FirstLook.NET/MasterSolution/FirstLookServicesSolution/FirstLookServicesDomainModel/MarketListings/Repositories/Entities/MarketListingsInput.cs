﻿using System.Collections.Generic;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.ComponentModel;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Model;
namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories.Entities
{
    [DataContract]
    public class MarketListingsInput
    {
        [DefaultValue(null)]
        [JsonProperty(PropertyName = "year", NullValueHandling = NullValueHandling.Ignore)]
        public int? Year { get; set; }

        [DefaultValue(null)]
        [JsonProperty(PropertyName = "make", NullValueHandling = NullValueHandling.Ignore)]
        public string Make { get; set; }

        [DefaultValue(null)]
        [JsonProperty(PropertyName = "model", NullValueHandling = NullValueHandling.Ignore)]
        public string Model { get; set; }

        [DefaultValue(null)]
        [JsonProperty(PropertyName = "trims", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> Trims { get; set; }

        [DefaultValue(null)]
        [JsonProperty(PropertyName = "odometer_min", NullValueHandling = NullValueHandling.Ignore)]
        public int? MinMileage { get; set; }

        [DefaultValue(null)]
        [JsonProperty(PropertyName = "odometer_max", NullValueHandling = NullValueHandling.Ignore)]
        public int? MaxMileage { get; set; }

        [DefaultValue(null)]
        [JsonProperty(PropertyName = "price_min", NullValueHandling = NullValueHandling.Ignore)]
        public int? MinPrice { get; set; }

        [DefaultValue(null)]
        [JsonProperty(PropertyName = "price_max", NullValueHandling = NullValueHandling.Ignore)]
        public int? MaxPrice { get; set; }

        [JsonProperty(PropertyName = "lat", NullValueHandling = NullValueHandling.Ignore)]
        [DefaultValue(null)]
        public string Latitude { get; set; }

        [DefaultValue(null)]
        [JsonProperty(PropertyName = "lon", NullValueHandling = NullValueHandling.Ignore)]
        public string Longitude { get; set; }

        [DefaultValue(null)]
        [JsonProperty(PropertyName = "radius_miles", NullValueHandling = NullValueHandling.Ignore)]
        public int? Distance { get; set; }

        [DefaultValue(10)]
        [JsonProperty(PropertyName = "size", NullValueHandling = NullValueHandling.Ignore)]
        public int? Size { get; set; }

        [DefaultValue(0)]
        [JsonProperty(PropertyName = "from", NullValueHandling = NullValueHandling.Ignore)]
        public int? From { get; set; }

        [DefaultValue(false)]
        [JsonProperty(PropertyName = "oem_certified", NullValueHandling = NullValueHandling.Ignore)]
        public bool? OemCertified { get; set; }

        [DefaultValue(false)]
        [JsonProperty(PropertyName = "for_sale_now", NullValueHandling = NullValueHandling.Ignore)]
        public bool? ForSaleNow { get; set; }

        [DefaultValue(false)]
        [JsonProperty(PropertyName = "for_sale_in_past_14d", NullValueHandling = NullValueHandling.Ignore)]
        public bool? ForSaleIn14Days { get; set; }

        [DefaultValue(null)]
        [JsonProperty(PropertyName = "transmissions", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> Transmissions { get; set; }

        [DefaultValue(null)]
        [JsonProperty(PropertyName = "drive_trains", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> DriveTrains { get; set; }

        [DefaultValue(null)]
        [JsonProperty(PropertyName = "engines", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> Engines { get; set; }

        [DefaultValue(null)]

        [JsonProperty(PropertyName = "fuel_types", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> FuelTypes { get; set; }

        [DefaultValue(null)]
        [JsonProperty(PropertyName = "aggregations", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> Aggregations { get; set; }

        [DefaultValue(null)]
        [JsonProperty(PropertyName = "equipment", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> Equipment { get; set; }

        [DefaultValue(null)]
        [JsonProperty(PropertyName = "sorts", NullValueHandling = NullValueHandling.Ignore)]        
        public List<Sorts> Sorts { get; set; }

        [DefaultValue(false)]
        [JsonIgnore]
        public bool? IncludeZeroPrice { get; set; }

        [DefaultValue(null)]
        [JsonIgnore]
        public int? InventoryId { get; set; }

        [DefaultValue(null)]
        [JsonIgnore]
        public int? BusinessUnitId { get; set; }

        [DefaultValue(false)]
        [JsonIgnore]
        public bool? GetRank { get; set; }

        [DefaultValue(false)]
        [JsonIgnore]
        public bool? RecentAndActive { get; set; }

        [DefaultValue(false)]
        [JsonIgnore]
        public bool? ActiveOnly { get; set; }

        [DefaultValue(false)]
        [JsonIgnore]
        public bool? AggregateListings { get; set; }

        [DefaultValue(false)]
        [JsonIgnore]
        public string Comment { get; set; }
    }
}
