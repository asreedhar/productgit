﻿using System;
using FirstLook.Common.Core.Registry;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories.DataStore
{
    [Serializable]
    class Module:IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IMarketListingsDataStore, MarketListingsDataStore>(ImplementationScope.Shared);
            registry.Register<ITierOneDatastore, TierOneDatastore>(ImplementationScope.Shared);
        }
    }
}
