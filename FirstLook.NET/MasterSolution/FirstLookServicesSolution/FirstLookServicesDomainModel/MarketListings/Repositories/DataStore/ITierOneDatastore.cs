﻿using System.Data;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories.DataStore
{
   public interface ITierOneDatastore
    {
         IDataReader GetTierOneEquipment(TierOneInput input);
    }
}
