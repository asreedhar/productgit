﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Linq;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories.Entities;
using Newtonsoft.Json;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core.Data;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories.DataStore
{
    public class MarketListingsDataStore : IMarketListingsDataStore
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const string DatabaseName = "IMT";
        
        public string GetMarketListings(List<MarketListingsInput> inputs)
        {
            try
            {
                string requestString = ConfigurationManager.AppSettings["MarketListingsUrl"];
                foreach (var input in inputs)
                {
                    if (input.IncludeZeroPrice == null || input.IncludeZeroPrice == false)
                        input.MinPrice = 1;
                }

                var json = JsonConvert.SerializeObject(inputs);

                //Log Json with comment. Comment will help in identofying request source
                var jsonRequest = inputs != null ? inputs.FirstOrDefault() : null;
                Log.Debug(jsonRequest!=null?jsonRequest.Comment+ ":" + json:json);

                ASCIIEncoding encoding = new ASCIIEncoding();
                byte[] byteArray = encoding.GetBytes(json);

                string authInfo = string.Format("{0}:{1}", ConfigurationManager.AppSettings["MarketListingsUsername"], ConfigurationManager.AppSettings["MarketListingsPassword"]);
                authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));

                var request = (HttpWebRequest)WebRequest.Create(requestString);
                request.Headers.Add("Authorization", "Basic " + authInfo);
                request.Method = "post";
                request.ContentType = "application/json";
                request.ContentLength = byteArray.Length;

                Stream newStream = request.GetRequestStream();
                newStream.Write(byteArray, 0, byteArray.Length);

                var response = (HttpWebResponse)request.GetResponse();
                Stream stream = response.GetResponseStream();
                return new StreamReader(stream).ReadToEnd();
            }
            catch (Exception e)
            {
                throw new NoDataFoundException("No Market Listings Found: " + e.Message);
            }
        }


        //================MarketDaysSupply=========================

        public IDataReader GetMarketDaysSupply(MarketListingsInput input)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "dbo.GetMarketDaysSupply";

                    Database.AddWithValue(command, "BusinessUnitID", input.BusinessUnitId, DbType.Int32);
                    Database.AddWithValue(command, "InventoryId", input.InventoryId, DbType.Int32);

                    var set = new DataSet();

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        var table = new DataTable("MarketDaysSupply");

                        table.Load(reader);

                        set.Tables.Add(table);
                    }

                    return set.CreateDataReader();
                }
            }
        }


    }
}


