﻿using System.Data;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories.DataStore
{
    public class TierOneDatastore:ITierOneDatastore
    {

        private const string DatabaseName = "Merchandising";
        public IDataReader GetTierOneEquipment(TierOneInput input)
        { 
        using (IDbConnection connection = Database.Connection(DatabaseName))
                {
                    using (IDbCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.CommandText = "builder.FetchTierOneEquipments";

                        Database.AddWithValue(command, "BusinessUnitID", input.BusinessUnitId, DbType.Int32);
                        Database.AddWithValue(command, "InventoryId", input.InventoryId, DbType.Int32);

                        var set = new DataSet();

                        using (IDataReader reader = command.ExecuteReader())
                        {
                            var table = new DataTable("Equipments");

                            table.Load(reader);

                            set.Tables.Add(table);
                        }

                        return set.CreateDataReader();
                    }
                }
        }
    }
}
