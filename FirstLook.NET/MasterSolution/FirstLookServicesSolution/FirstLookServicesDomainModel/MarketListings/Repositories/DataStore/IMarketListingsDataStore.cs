﻿using System.IO;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories.Entities;
using System.Collections.Generic;
using System.Data;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories.DataStore
{
    public interface IMarketListingsDataStore
    {
        string GetMarketListings(List<MarketListingsInput> input);

        IDataReader GetMarketDaysSupply(MarketListingsInput input);

    }
}
