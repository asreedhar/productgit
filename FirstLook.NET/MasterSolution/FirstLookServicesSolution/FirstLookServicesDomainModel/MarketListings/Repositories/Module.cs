﻿using FirstLook.Common.Core.Registry;


namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories
{
    public class Module:IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories.DataStore.Module>();

            registry.Register<IRepository, MarketListingsRepository>(ImplementationScope.Shared);
        }
    }
}
