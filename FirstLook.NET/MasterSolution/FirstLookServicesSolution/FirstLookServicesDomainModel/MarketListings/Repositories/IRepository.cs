﻿using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Model;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories
{
    public interface IRepository
    {
        List<ListingsAndCounts> FetchMarketListings(List<MarketListingsInput> marketListingInput);

        IList<FacetEquipment> FetchTierOneEquipment(TierOneInput input);
    }
}
