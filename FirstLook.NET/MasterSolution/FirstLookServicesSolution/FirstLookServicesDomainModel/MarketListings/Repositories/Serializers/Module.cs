﻿using System;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Model;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories.Serializers;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories.Serializers
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ISerializer<FacetEquipment>, TierOneSerializer>(ImplementationScope.Shared);

            registry.Register<ISerializer<MarketDaysSupply>, MktDaysSupplySerializer>(ImplementationScope.Shared);
        }
    }
}