﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Model;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories.Serializers
{
    public class TierOneSerializer:Serializer<FacetEquipment>
    {
        public override FacetEquipment Deserialize(IDataRecord record)        {
            var equipment = record.GetString("userFriendlyName");
            var categoryName = record.GetString("CategoryName");
            var tierNumber = record.GetInt32("tierNumber", 0);

            return new FacetEquipment { Equipment = equipment, Level = tierNumber, CategoryName = categoryName };                }
    }
}
