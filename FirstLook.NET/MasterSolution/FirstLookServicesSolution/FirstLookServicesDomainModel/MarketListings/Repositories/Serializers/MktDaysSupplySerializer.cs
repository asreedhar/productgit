﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Model;
using FirstLook.Client.DomainModel.Common.Repositories;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories.Serializers
{
    public class MktDaysSupplySerializer:Serializer<MarketDaysSupply>
    {
        public override MarketDaysSupply Deserialize(IDataRecord record)
        {
            MarketDaysSupply mktDaysSupply = null;
            try
            {
                mktDaysSupply = new MarketDaysSupply();
                mktDaysSupply.SearchTypeId = DataRecord.GetTinyInt(record, "SearchTypeId", 0);
                mktDaysSupply.MarketDaysSupplyCount = DataRecord.GetTinyInt(record, "MarketDaysSupply", 0);
            }
            catch (Exception e)
            {
                throw e;
            }
            return mktDaysSupply;

        }

    }
}
