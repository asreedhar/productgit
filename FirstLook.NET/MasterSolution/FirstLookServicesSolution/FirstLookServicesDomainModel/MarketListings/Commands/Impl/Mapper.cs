﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Model;
namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Commands.Impl
{
   public static class Mapper
    {
       public static IList<MarketListingsDto> Map(IList<MarketListingsDetail> objs)
       {
           IList<MarketListingsDto> marketListingsDto = objs.Select(obj => new MarketListingsDto
           {
               Age = obj.Age,
               Certified = obj.Certified,
               Color = obj.Color,
               Location = obj.Location,
               InternetPrice = obj.InternetPrice,
               Mileage = obj.Mileage,
               Seller = obj.Seller,
               Vin = obj.Vin,
               Year=obj.Year,
               Make=obj.Make,
               Model=obj.Model,
               Trim=obj.Trim

           }).ToList();
           return marketListingsDto;

       }
    }
}
