﻿using FirstLook.FirstLookServices.DomainModel.MarketListings.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Model;
using FirstLook.Common.Core.Command;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Commands.Impl
{
   public class CommandFactory:ICommandFactory
    {
        #region IcommandFactory Members
       public ICommand<List<ListingsAndCounts>, IdentityContextDto<List<FetchMarketListingsArgumentsDto>>> CreateFetchMarketListingsCommand()
        {
            return new FetchMarketListingsCommand();
        }

        public ICommand<FetchTierOneEquipmentResultsDto, IdentityContextDto<FetchTierOneEquipmentArgumentsDto>> CreateFetchTierOneEquipmentCommand()
        {
            return new FetchTierOneEquipmentCommand();
        }
        #endregion
    }
}
