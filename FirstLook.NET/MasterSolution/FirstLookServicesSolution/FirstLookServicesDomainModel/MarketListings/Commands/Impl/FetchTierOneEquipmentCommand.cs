﻿using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Model;


namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Commands.Impl
{
    public class FetchTierOneEquipmentCommand:ICommand<FetchTierOneEquipmentResultsDto, IdentityContextDto<FetchTierOneEquipmentArgumentsDto>>
    {

        public FetchTierOneEquipmentResultsDto Execute(IdentityContextDto<FetchTierOneEquipmentArgumentsDto> parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();
            var tierOneEquipmentRepository = resolver.Resolve<IRepository>();
            TierOneInput input = new TierOneInput()
            {
                BusinessUnitId = parameters.Arguments.BusinessUnitId,
                InventoryId = parameters.Arguments.InventoryId
            };

            List<FacetEquipment> result = (List<FacetEquipment>)tierOneEquipmentRepository.FetchTierOneEquipment(input);
            if (result == null)
                result = new List<FacetEquipment>();
            
            return new FetchTierOneEquipmentResultsDto() { TierOneEquipment=result };
        }
    }
}
