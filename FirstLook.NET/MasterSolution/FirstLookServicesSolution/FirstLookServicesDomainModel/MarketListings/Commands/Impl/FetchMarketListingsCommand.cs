﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Model;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Commands.Impl
{
    public class FetchMarketListingsCommand : ICommand<List<ListingsAndCounts>, IdentityContextDto<List<FetchMarketListingsArgumentsDto>>>
    {
        #region ICommand<FetchAgingInventoryResultsDto,IdentityContextDto<FetchMarketListingsArgumentsDto>> Members

        public List<ListingsAndCounts> Execute(IdentityContextDto<List<FetchMarketListingsArgumentsDto>> param)
        {

            var resolver = RegistryFactory.GetResolver();
            var inputList = new List<MarketListingsInput>();
            var marketListingsRepository = resolver.Resolve<IRepository>();

            var tierOneResult = new List<FetchTierOneEquipmentResultsDto>();

            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            var cmd = factory.CreateFetchTierOneEquipmentCommand();

            foreach (var parameters in param.Arguments)
            {

                var marketListingsInput = new MarketListingsInput
                {
                    Year = parameters.Year,
                    Make = parameters.Make,
                    Model = parameters.Model,
                    Trims = parameters.Trims,
                    MinMileage = parameters.MinMileage,
                    MaxMileage = parameters.MaxMileage,
                    MinPrice = parameters.MinPrice,
                    MaxPrice = parameters.MaxPrice,
                    Latitude = parameters.Latitude,
                    Longitude = parameters.Longitude,
                    Distance = parameters.Distance,
                    Size = parameters.Size,
                    From = parameters.From,
                    OemCertified = parameters.OemCertified,
                    ForSaleNow=parameters.ActiveOnly,                    
                    Transmissions = parameters.Transmissions,
                    DriveTrains = parameters.DriveTrains,
                    Engines = parameters.Engines,
                    FuelTypes = parameters.FuelTypes,
                    Aggregations = parameters.Aggregations,
                    IncludeZeroPrice = parameters.IncludeZeroPrice,
                    Equipment = parameters.Equipment,
                    InventoryId = parameters.InventoryId,
                    BusinessUnitId = parameters.BusinessUnitId,
                    GetRank = parameters.GetRanks,
                    Sorts = parameters.Sorts,
                    RecentAndActive=parameters.RecentActive,
                    ActiveOnly = parameters.ActiveOnly,
                    AggregateListings=parameters.AggregateListings,
                    Comment=parameters.Comment
                };
                

                inputList.Add(marketListingsInput);


                if (!parameters.IsRequiredAggregations) continue;

                var args = new FetchTierOneEquipmentArgumentsDto
                {
                    BusinessUnitId = Convert.ToInt32(parameters.BusinessUnitId),
                    InventoryId = Convert.ToInt32(parameters.InventoryId)
                };

                tierOneResult.Add(cmd.Execute(new IdentityContextDto<FetchTierOneEquipmentArgumentsDto>
                {
                    Arguments = args,
                    Identity = new Client.DomainModel.Common.Commands.TransferObjects.IdentityDto { AuthorityName = param.Identity.AuthorityName, Name = param.Identity.Name }
                }));
            }

            List<ListingsAndCounts> marketListingList = marketListingsRepository.FetchMarketListings(inputList);

            int iCtr = 0;
            if (tierOneResult.Count > 0)
            {

                foreach (var results in marketListingList)
                {
                    results.TierOneEquipments = new List<FacetEquipments>();
                    if (tierOneResult[iCtr] != null)
                    {
                        Debug.WriteLine("--- Matching Max Equipment to Market Listing Equipment ---");
                        foreach (var tier in tierOneResult[iCtr].TierOneEquipment)
                        {
                            var tier1 = tier;
                            var facet = results.Facets.FacetCounts.Where(fc => fc.Key.Equals(tier1.CategoryName, StringComparison.InvariantCultureIgnoreCase));
                            var equipmentCounts = facet as IList<EquipmentCount> ?? facet.ToList();
                            if (equipmentCounts.Any())
                            {
                                // ReSharper disable once RedundantStringFormatCall
                                Debug.WriteLine(string.Format("Equipment MATCH: Processing max equipment '{0}', with CategoryName '{1}' ", tier.Equipment, tier.CategoryName ));
                                var firstOrDefault = equipmentCounts.FirstOrDefault();
                                if (firstOrDefault != null)
                                    results.TierOneEquipments.Add(new FacetEquipments
                                    {
                                        Key = tier.Equipment,
                                        DocCount = firstOrDefault.DocCount,
                                        Level = tier.Level
                                    });
                            }
                            else
                            {
                                // ReSharper disable once RedundantStringFormatCall
                                Debug.WriteLine(string.Format("Equipment Failed to Match: Processing max equipment '{0}', with CategoryName '{1}' ", tier.Equipment, tier.CategoryName));
                                results.TierOneEquipments.Add(new FacetEquipments
                                {
                                    Key = tier.Equipment,
                                    DocCount = 0,
                                    Level = tier.Level
                                });
                            }
                        }
                    }

                    iCtr++;
                }
            }

            if (marketListingList == null)
                throw new NoDataFoundException("Empty Listings");

            return marketListingList;
        }

        #endregion
    }
}
