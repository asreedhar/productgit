﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Model;
using System.Collections.Generic;
using FirstLook.Common.Core.Command;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Commands
{
   public interface ICommandFactory
    {
        ICommand<List<ListingsAndCounts>, IdentityContextDto<List<FetchMarketListingsArgumentsDto>>> CreateFetchMarketListingsCommand();

        ICommand<FetchTierOneEquipmentResultsDto, IdentityContextDto<FetchTierOneEquipmentArgumentsDto>> CreateFetchTierOneEquipmentCommand();
    }
}
