﻿using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Commands.TransferObjects.Envelopes
{
    public class FetchMarketListingsResultsDto
    {
        public PrincipalDto Principal { get; set; }

        public FetchMarketListingsArgumentsDto Arguments { get; set; }

        public IList<MarketListingsDto> MarketListings { get; set; }

    }
}
