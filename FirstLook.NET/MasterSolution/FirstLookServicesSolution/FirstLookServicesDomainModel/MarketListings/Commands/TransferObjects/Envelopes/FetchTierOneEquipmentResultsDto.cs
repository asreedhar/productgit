﻿using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;
using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Model;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Commands.TransferObjects.Envelopes
{
   public class FetchTierOneEquipmentResultsDto
    {
        public PrincipalDto Principal { get; set; }

        public List<FacetEquipment> TierOneEquipment { get; set; }
    }
}
