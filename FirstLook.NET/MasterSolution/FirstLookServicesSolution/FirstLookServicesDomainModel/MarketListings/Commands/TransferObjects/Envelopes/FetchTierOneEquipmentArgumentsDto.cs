﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Commands.TransferObjects.Envelopes
{
   public class FetchTierOneEquipmentArgumentsDto
    {
        public int InventoryId { get; set; }

        public int BusinessUnitId { get; set; }

    }
}
