﻿namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Commands.TransferObjects
{
   public class MarketListingsDto
    {
        public int? Age { get; set; }
        public string Seller { get; set; }
        public bool Certified { get; set; }
        public string Color { get; set; }
        public int? Mileage { get; set; }
        public decimal? InternetPrice { get; set; }
        public string Location { get; set; }
        public string Vin { get; set; }
        public int? Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Trim { get; set; }
    }
}
