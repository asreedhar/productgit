﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Model
{
    [DataContract]
    public class ListingTerms
    {
        [DataMember(Name = "ActiveCounts", EmitDefaultValue = false)]
        public int ActiveCounts { get; set; }

        [DataMember(Name = "RecentActiveCounts", EmitDefaultValue = false)]
        public int RecentActiveCounts { get; set; }
    }
}
