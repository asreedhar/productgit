﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Model
{
    [DataContract]
    class FinalResult
    {
        [DataMember(Name="results", EmitDefaultValue=false)]
        public List<Result> CompleteResult { get; set; }   
    }
}
