﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Model
{
    [DataContract]
  public   class Result
    {
        [DataMember(Name = "hits", EmitDefaultValue = false)]
        public Hits AllHits { get; set; }

        [DataMember(Name = "_shards", EmitDefaultValue = false)]
        public Shards HitShards { get; set; }

        [DataMember(Name = "took", EmitDefaultValue = false)]
        public int Took { get; set; }

        [DataMember(Name = "timed_out", EmitDefaultValue = false)]
        public bool TimeOut { get; set; }

        [DataMember(Name = "aggregations", EmitDefaultValue = false)]
        public Aggregation Aggregations { get; set; }
    }
}
