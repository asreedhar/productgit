﻿using System.Runtime.Serialization;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Model
{
    [DataContract]
    public class Aggregation
    {
        [DataMember(Name = "equipment_terms", EmitDefaultValue = false)]
        public EquipmentTerm EquipmentTerms { get; set; }

        [DataMember(Name = "price_stats", EmitDefaultValue = false)]
        public PriceStat PriceStats { get; set; }

        [DataMember(Name = "is_certified_terms", EmitDefaultValue = false)]
        public CertifiedTerm CertifiedTerms { get; set; }

        [DataMember(Name = "drive_train_terms", EmitDefaultValue = false)]
        public DriveTrainTerm DriveTrainTerms { get; set; }

        [DataMember(Name = "engine_terms", EmitDefaultValue = false)]
        public EngineTerm EngineTerms { get; set; }

        [DataMember(Name = "fuel_type_terms", EmitDefaultValue = false)]
        public FuelTypeTerm FuelTypeTerms { get; set; }

        [DataMember(Name = "trim_terms", EmitDefaultValue = false)]
        public TrimTerm TrimTerms { get; set; }

        [DataMember(Name = "transmission_terms", EmitDefaultValue = false)]
        public TransmissionTerm TransmissionTerms { get; set; }

        [DataMember(Name = "price_histogram", EmitDefaultValue = false)]
        public HistogramTerm PriceHistogramTerms { get; set; }

        [DataMember(Name = "odometer_stats", EmitDefaultValue = false)]
        public OdometerStat OdometerStats { get; set; }

        [DataMember(Name = "model_terms", EmitDefaultValue = false)]
        public ModelTerm ModelTerms { get; set; }

    }
}