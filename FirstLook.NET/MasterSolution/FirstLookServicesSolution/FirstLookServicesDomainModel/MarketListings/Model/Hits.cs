﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Model
{
    [DataContract]
    public class Hits
    {
        [DataMember(Name = "hits", EmitDefaultValue = false)]
        public List<Hit> Hit { get; set; }

        [DataMember(Name = "total", EmitDefaultValue = false)]
        public int? Total { get; set; }

        [DataMember(Name = "max_score", EmitDefaultValue = false)]
        public float? MaxScore { get; set; }
    }
}