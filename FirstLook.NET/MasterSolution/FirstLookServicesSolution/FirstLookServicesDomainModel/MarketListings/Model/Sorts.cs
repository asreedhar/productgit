﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Newtonsoft.Json;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Model
{
    public class Sorts
    {
        [DefaultValue(null)]
        [JsonProperty(PropertyName = "field", NullValueHandling = NullValueHandling.Ignore)]
        public string Field { get; set; }

        [DefaultValue("asc")]
        [JsonProperty(PropertyName = "order", NullValueHandling = NullValueHandling.Ignore)]
        public string Order { get; set; } 
    }
}
