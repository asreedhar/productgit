﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Model
{
    [DataContract]
    public class Shards
    {
        [DataMember(Name = "successful", EmitDefaultValue = false)]
        public int Successful { get; set; }

        [DataMember(Name = "failed", EmitDefaultValue = false)]
        public int Failed { get; set; }

        [DataMember(Name = "total", EmitDefaultValue = false)]
        public int Total { get; set; }
    }
}