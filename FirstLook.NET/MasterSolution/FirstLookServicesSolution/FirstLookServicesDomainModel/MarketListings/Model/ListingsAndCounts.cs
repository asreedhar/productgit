﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Model
{
    [DataContract]
    public class ListingsAndCounts
    {
        [DataMember(Name = "listings", EmitDefaultValue = false)]
        public List<MarketListingsDetail> Listings { get; set; }

        [DataMember(Name = "facets", EmitDefaultValue = false)]
        public Facet Facets { get; set; }

        [DataMember(Name = "priceStats", EmitDefaultValue = false)]
        public PriceStat PriceStats { get; set; }

        [DataMember(Name = "certifiedTerms", EmitDefaultValue = false)]
        public CertifiedTerm CertifiedTerms { get; set; }

        [DataMember(Name = "driveTrainTerms", EmitDefaultValue = false)]
        public DriveTrainTerm DriveTrainTerms { get; set; }

        [DataMember(Name = "transmissionTerms", EmitDefaultValue = false)]
        public TransmissionTerm TransmissionTerms { get; set; }

        [DataMember(Name = "trimTerms", EmitDefaultValue = false)]
        public TrimTerm TrimTerms { get; set; }

        [DataMember(Name = "engineTerms", EmitDefaultValue = false)]
        public EngineTerm EngineTerms { get; set; }

        [DataMember(Name = "fuelTypeTerms", EmitDefaultValue = false)]
        public FuelTypeTerm FuelTypeTerms { get; set; }

        [DataMember(Name = "listingTerms", EmitDefaultValue = false)]
        public ListingTerms ListingTerms { get; set; }

        [DataMember(Name = "inventoryId", EmitDefaultValue = false)]
        public int? InventoryId { get; set; }

        [DataMember(Name = "price_histogram", EmitDefaultValue = false)]
        public HistogramTerm PriceHistogramTerm { get; set; }

        [DataMember(Name = "odometer_stats", EmitDefaultValue = false)]
        public OdometerStat OdometerStats { get; set; }

        [DataMember(Name = "model_terms", EmitDefaultValue = false)]
        public ModelTerm ModelTerms { get; set; }

        //==========MarketDaysSupply Attributes=======================
        [DataMember(Name = "marketDaysSupply", EmitDefaultValue = false)]
        public List<MarketDaysSupply> MarketDaysSupplyInfo { get; set; }

        [DataMember(Name = "marketPrices", EmitDefaultValue = false)]
        public List<int> MarketPrices { get; set; }

        [DataMember(Name = "RecordCount", EmitDefaultValue = false)]
        public int RecordCount { get; set; }

        public List<FacetEquipments> TierOneEquipments { get; set; }

    }
}
