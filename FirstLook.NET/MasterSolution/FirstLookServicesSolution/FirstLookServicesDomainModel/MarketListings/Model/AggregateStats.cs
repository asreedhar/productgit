﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Model
{
    [DataContract]
    public class AggregateStats
    {
        [DataMember(Name = "count", EmitDefaultValue = false)]
        public int? Count { get; set; }

        [DataMember(Name = "sum", EmitDefaultValue = false)]
        public decimal? Sum { get; set; }

        [DataMember(Name = "avg", EmitDefaultValue = false)]
        public decimal? Avg { get; set; }

        [DataMember(Name = "max", EmitDefaultValue = false)]
        public decimal? Max { get; set; }

        [DataMember(Name = "min", EmitDefaultValue = false)]
        public decimal? Min { get; set; }

        [DataMember(Name = "sum_as_string", EmitDefaultValue = false)]
        public string SumAsString { get; set; }

        [DataMember(Name = "avg_as_string", EmitDefaultValue = false)]
        public string AvgAsString { get; set; }

        [DataMember(Name = "max_as_string", EmitDefaultValue = false)]
        public string MaxAsString { get; set; }

        [DataMember(Name = "min_as_string", EmitDefaultValue = false)]
        public string MinAsString { get; set; }

    }
}
