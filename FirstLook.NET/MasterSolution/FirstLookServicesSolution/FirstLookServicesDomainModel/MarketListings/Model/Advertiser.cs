﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Model
{
    [DataContract]
    public class Advertiser
    {
        [DataMember(Name = "price", EmitDefaultValue = false)]
        public int PhoneNo { get; set; }

        [DataMember(Name = "address", EmitDefaultValue = false)]
        public Address AdvertiserAddress { get; set; }

        [DataMember(Name = "email", EmitDefaultValue = false)]
        public string Email { get; set; }

        [DataMember(Name = "name", EmitDefaultValue = false)]
        public string Name { get; set; }


    }
}