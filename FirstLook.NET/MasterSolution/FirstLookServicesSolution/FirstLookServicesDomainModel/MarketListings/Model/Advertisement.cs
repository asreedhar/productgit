﻿using System;
using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Model
{   
    [DataContract]
    public class Advertisement
    {

        [DataMember(Name = "advertiser", EmitDefaultValue = false)]
        public Advertiser AdvertisementAdvertiser { get; set; }
        
        [DataMember(Name = "price", EmitDefaultValue = false)]
        public decimal? Price { get; set; }

        [DataMember(Name = "odometer", EmitDefaultValue = false)]
        public int? Mileage { get; set; }

        [DataMember(Name = "from", EmitDefaultValue = false)]
        public DateTime FromDate { get; set; }

        [DataMember(Name = "transactions", EmitDefaultValue = false)]
        public string StockType { get; set; }

        [DataMember(Name = "is_for_sale", EmitDefaultValue = false)]
        public bool IsForSale { get; set; }

        [DataMember(Name = "stock_number", EmitDefaultValue = false)]
        public string StockNumber { get; set; }

        [DataMember(Name = "is_certified", EmitDefaultValue = false)]
        public bool IsCertified { get; set; }

    }
}