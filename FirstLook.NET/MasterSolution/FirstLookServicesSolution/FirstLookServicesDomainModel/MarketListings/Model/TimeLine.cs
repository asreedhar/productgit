﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Model
{
    [DataContract]
    public class TimeLine
    {
        [DataMember(Name = "was_active_in_past_14_days", EmitDefaultValue = false)]
        public bool WasActive { get; set; }

        [DataMember(Name = "retail_txn_count_in_past_90_days", EmitDefaultValue = false)]
        public bool RetailTransaction { get; set; }

        [DataMember(Name = "transactions", EmitDefaultValue = false)]
        public List<Transaction> Transaction { get; set; }
    }
}