﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Model
{
    public class MarketDaysSupply
    {

        public int MarketDaysSupplyCount { get; set; }

        public int SearchTypeId { get; set; }
    }
}
