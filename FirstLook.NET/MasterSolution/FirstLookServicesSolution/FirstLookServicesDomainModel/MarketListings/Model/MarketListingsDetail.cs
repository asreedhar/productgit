﻿using System.Collections.Generic;
using System.Runtime.Serialization;
namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Model
{
    [DataContract]
    public class MarketListingsDetail
    {
        [DataMember(Name = "age", EmitDefaultValue = false)]
        public int? Age { get; set; }
        [DataMember(Name = "seller", EmitDefaultValue = false)]
        public string Seller { get; set; }
        [DataMember(Name = "certified", EmitDefaultValue = false)]
        public bool Certified { get; set; }
        [DataMember(Name = "color", EmitDefaultValue = false)]
        public string Color { get; set; }
        [DataMember(Name = "mileage", EmitDefaultValue = false)]
        public int? Mileage { get; set; }
        [DataMember(Name = "internetPrice", EmitDefaultValue = false)]
        public decimal? InternetPrice { get; set; }
        [DataMember(Name = "vin", EmitDefaultValue = false)]
        public string Vin { get; set; }
        [DataMember(Name = "year", EmitDefaultValue = false)]
        public int? Year { get; set; }
        [DataMember(Name = "make", EmitDefaultValue = false)]
        public string Make { get; set; }
        [DataMember(Name = "model", EmitDefaultValue = false)]
        public string Model { get; set; }
        [DataMember(Name = "trim", EmitDefaultValue = false)]
        public string Trim { get; set; }
        [DataMember(Name = "Location", EmitDefaultValue = false)]
        public string Location { get; set; }
        [DataMember(Name = "AdText", EmitDefaultValue = false)]
        public string AdText{get;set;}
        [DataMember(Name = "StockNumber", EmitDefaultValue = false)]
        public string StockNumber { get; set; }
    }
}
