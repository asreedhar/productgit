﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Model
{
    public class FacetEquipment
    {
        public string Equipment { get; set; }
        public int Level { get; set; }
        
        /// <summary>
        /// CategoryName should be used when matching equipment items found from the MarketListings API.
        /// </summary>
        public string CategoryName { get; set; }
    }
}
