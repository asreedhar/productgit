﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Model
{
    [DataContract]
    public class AggregateTerms
    {
        [DataMember(Name = "buckets", EmitDefaultValue = false)]
        public List<EquipmentCount> Buckets { get; set; }

        [DataMember(Name = "sum_other_doc_count", EmitDefaultValue = false)]
        public int OtherDocCount { get; set; }

        [DataMember(Name = "doc_count_error_upper_bound", EmitDefaultValue = false)]
        public int UpperBound { get; set; }
    }
}
