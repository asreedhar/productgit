﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Model
{
    [DataContract]
    public class HistogramCount
    {
        [DataMember(Name = "key", EmitDefaultValue = false)]
        public int Key { get; set; }

        [DataMember(Name = "doc_count", EmitDefaultValue = false)]
        public int DocCount { get; set; }
    }
}
