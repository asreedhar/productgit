﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Model
{
    [DataContract]
    public class HitSource
    {
        [DataMember(Name = "ad_text", EmitDefaultValue = false)]
        public string AdText { get; set; }

        [DataMember(Name = "time_line", EmitDefaultValue = false)]
        public TimeLine TimeLine { get; set; }

        [DataMember(Name = "vin", EmitDefaultValue = false)]
        public string Vin { get; set; }

        [DataMember(Name = "product_details", EmitDefaultValue = false)]
        public ProductDetails ProductDetails { get; set; }

        [DataMember(Name = "equipment", EmitDefaultValue = false)]
        public List<string> Equipment { get; set; }

        [DataMember(Name = "advertisement", EmitDefaultValue = false)]
        public Advertisement Advertisement { get; set; }

    }
}