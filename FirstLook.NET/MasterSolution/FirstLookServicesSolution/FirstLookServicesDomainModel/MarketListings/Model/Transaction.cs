﻿using System;
using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Model
{
    [DataContract]
    public class Transaction
    {
        [DataMember(Name = "timestamp", EmitDefaultValue = false)]
        public DateTime TimeStamp { get; set; }

        [DataMember(Name = "price", EmitDefaultValue = false)]
        public decimal Price { get; set; }

        [DataMember(Name = "txn_type", EmitDefaultValue = false)]
        public string TransactionType { get; set; }

    }
}