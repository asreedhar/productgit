﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Model
{
    [DataContract]
    public class Hit
    {
           [DataMember(Name = "_score", EmitDefaultValue = false)]
            public decimal? Score { get; set; }

            [DataMember(Name = "_type", EmitDefaultValue = false)]
            public string Type { get; set; }

            [DataMember(Name = "_id", EmitDefaultValue = false)]
            public string Id { get; set; }

            [DataMember(Name = "_source", EmitDefaultValue = false)]
            public HitSource Source { get; set; }

            [DataMember(Name = "_index", EmitDefaultValue = false)]
            public string Index { get; set; }
    }
}