﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Model
{
    [DataContract]
    public class ProductDetails
    {
        [DataMember(Name = "engine", EmitDefaultValue = false)]
        public string Engine { get; set; }
    
        [DataMember(Name = "trim", EmitDefaultValue = false)]
        public string Trim { get; set; }
    
        [DataMember(Name = "style", EmitDefaultValue = false)]
        public string Style { get; set; }
    
        [DataMember(Name = "chrome_styles", EmitDefaultValue = false)]
        public List<int> ChromeStyle { get; set; }
    
        [DataMember(Name = "transmission", EmitDefaultValue = false)]
        public string Transmission { get; set; }

        [DataMember(Name = "fuel_type", EmitDefaultValue = false)]
        public List<string> FuelType { get; set; }

        [DataMember(Name = "exterior_color_name_oem", EmitDefaultValue = false)]
        public string OemColor { get; set; }

        [DataMember(Name = "exterior_color_name_generic", EmitDefaultValue = false)]
        public string GenericColor { get; set; }

        [DataMember(Name = "year", EmitDefaultValue = false)]
        public int Year { get; set; }

        [DataMember(Name = "drive_train", EmitDefaultValue = false)]
        public string DriveTrain { get; set; }

        [DataMember(Name = "model", EmitDefaultValue = false)]
        public string Model { get; set; }

        [DataMember(Name = "make", EmitDefaultValue = false)]
        public string Make { get; set; }

        [DataMember(Name = "exterior_color_rgb_hex", EmitDefaultValue = false)]
        public string RgbHexColor { get; set; }

    }
}