﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.DomainModel.MarketListings.Model
{
    [DataContract]
    public class Facet
    {
        [DataMember(Name = "facetCounts", EmitDefaultValue = false)]
        public List<EquipmentCount> FacetCounts { get; set; }
    }
}
