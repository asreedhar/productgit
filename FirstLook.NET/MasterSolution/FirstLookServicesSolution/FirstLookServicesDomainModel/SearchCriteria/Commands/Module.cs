﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.SearchCriteria.Commands.Impl;

namespace FirstLook.FirstLookServices.DomainModel.SearchCriteria.Commands
{
   public  class Module:IModule
    {
        #region IModule Members

        public void Configure(IRegistry registry)
        {
            registry.Register<ICommandFactory, CommandFactory>(ImplementationScope.Shared);
        }

        #endregion
    }
}
