﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.SearchCriteria.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.SearchCriteria.Commands.Impl
{
    class CommandFactory: ICommandFactory
    {
            
        public ICommand<FetchSearchCriteriaResultsDto, IdentityContextDto<FetchSearchCriteriaArgumentsDto>> CreateSearchCriteriaCommand()
        {
            return new FetchSearchCriteriaCommand();
        }

        public ICommand<FetchCriteriaResultsDto, IdentityContextDto<FetchCriteriaArgumentsDto>> GetCriteriaFromAmazomCommand()
        {
            return new GetCriteriaFromAmazon();
        }
    }
}
