﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.Command;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.SearchCriteria.Commands.TransferObjects;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon;
using FirstLook.Common.Core.Registry;
using System.Configuration;
using FirstLook.FirstLookServices.DomainModel.Validation;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using System.IO;
using Newtonsoft.Json;

namespace FirstLook.FirstLookServices.DomainModel.SearchCriteria.Commands.Impl
{
    public class GetCriteriaFromAmazon : ICommand<FetchCriteriaResultsDto, IdentityContextDto<FetchCriteriaArgumentsDto>>
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public FetchCriteriaResultsDto Execute(IdentityContextDto<FetchCriteriaArgumentsDto> parameters)
        {

            FetchCriteriaResultsDto result = new FetchCriteriaResultsDto();

            string bucketName = ConfigurationManager.AppSettings["ProfitSearchBucket"];
            

            Validators.CheckDealerId(parameters.Arguments.DealerId, parameters);

            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            var command = factory.GetCriteriaFromAmazomCommand();

            try
            {
                if (Exists(parameters.Arguments.DealerId + "_" + parameters.Arguments.InventoryId, bucketName))
                {
                    using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.USEast1))
                    {
                        //Create a request to Amazon
                        GetObjectRequest request = new GetObjectRequest
                                                   {
                                                       BucketName = bucketName,
                                                       Key =
                                                           parameters.Arguments.DealerId + "_" +
                                                           parameters.Arguments.InventoryId
                                                   };

                        //Fetch the object based on the request sent from Amazon
                        using (GetObjectResponse response = s3Client.GetObject(request))
                        {
                            Stream stream = response.ResponseStream;
                            result =
                                JsonConvert.DeserializeObject<FetchCriteriaResultsDto>(
                                    new StreamReader(stream).ReadToEnd());

                        }


                    }
                }
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null && (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") || amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    Log.Error("Error Getting Appraisal photos from S3 Bucket - " + bucketName + ":  Incorrect AWS Credentials");
                    throw new UserAccessException("AWS", "Forbidden", "Incorrect AWS Credentials.");
                }
                else
                {
                    Log.Error("Exception Getting Appraisal photos from S3 Bucket - " + bucketName, amazonS3Exception);
                    throw new FirstlookGeneralException("Errorcode " + amazonS3Exception.ErrorCode + " when writing to S3 Bucket: " + bucketName + " - Error message = " + amazonS3Exception.Message);
                }
            }

            return result;


        }
        public bool Exists(string fileKey, string bucketName)
        {
            IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.USEast1);
            try
            {
                var response = s3Client.GetObjectMetadata(new GetObjectMetadataRequest() { BucketName=bucketName,Key=fileKey});

                return true;
            }

            catch (Amazon.S3.AmazonS3Exception ex)
            {
                if (ex.StatusCode == System.Net.HttpStatusCode.NotFound)
                    return false;

                //status wasn't not found, so throw the exception
                throw;
            }
        }
    }
}
