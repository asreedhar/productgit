﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core.Extensions;
using FirstLook.FirstLookServices.DomainModel.SearchCriteria.Commands.TransferObjects;
using System.Linq;
using System.Text;


namespace FirstLook.FirstLookServices.DomainModel.SearchCriteria.Commands.Impl
{
    public class Mapper
    {

        public static string ToJson(FetchSearchCriteriaArgumentsDto arguments, FetchSearchCriteriaResultsDto resultKey)
        {
            var criteriaRequest = new Dictionary<string, object>();

            var criteriaResultDocument = new Dictionary<string, object>();

            criteriaRequest.Add("avgMileage", arguments.AvgMileage);

            criteriaRequest.Add("avgPrice", arguments.AvgPrice);

            criteriaRequest.Add("businessUnitId", arguments.DealerId);

            criteriaRequest.Add("certified", arguments.Certified);

            criteriaRequest.Add("notCertified", arguments.NotCertified);

            criteriaRequest.Add("distance", arguments.Distance);

            criteriaRequest.Add("driveTrain", arguments.DriveTrian);

            criteriaRequest.Add("engine", arguments.Engine);

            criteriaRequest.Add("fuel", arguments.Fuel);

            criteriaRequest.Add("facet", arguments.Facet);

            criteriaRequest.Add("inventoryId", arguments.InventoryId);

            criteriaRequest.Add("listings", arguments.Listings);

            criteriaRequest.Add("maxMileage", arguments.MaxMileage);

            criteriaRequest.Add("maxPrice", arguments.MaxPrice);

            criteriaRequest.Add("minMileage", arguments.MinMileage);

            criteriaRequest.Add("minPrice", arguments.MinPrice);

            criteriaRequest.Add("overallCount", arguments.OverallCount);

            criteriaRequest.Add("precisionCount", arguments.PrecisionCount);

            criteriaRequest.Add("transmission", arguments.Transmission);

            criteriaRequest.Add("trim", arguments.Trim);

            criteriaRequest.Add("vin", arguments.Vin);

            criteriaResultDocument.Add("resultKey", resultKey.Key);

            criteriaResultDocument.Add("searchCriteriaRequest", criteriaRequest);

            return criteriaResultDocument.ToJson();


        }
    }
}
