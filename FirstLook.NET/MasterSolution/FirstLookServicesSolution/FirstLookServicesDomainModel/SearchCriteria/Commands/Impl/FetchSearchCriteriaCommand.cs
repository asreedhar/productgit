﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.SQS;
using Amazon.SQS.Model;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.SearchCriteria.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.FirstLookServices.DomainModel.SearchCriteria.Model;
using FirstLook.FirstLookServices.DomainModel.Utility;
using IResolver = FirstLook.Common.Core.Registry.IResolver;
using FirstLook.Common.Core.Command;

namespace FirstLook.FirstLookServices.DomainModel.SearchCriteria.Commands.Impl
{
    public class FetchSearchCriteriaCommand : ICommand<FetchSearchCriteriaResultsDto, IdentityContextDto<FetchSearchCriteriaArgumentsDto>>
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public FetchSearchCriteriaResultsDto Execute(IdentityContextDto<FetchSearchCriteriaArgumentsDto> parameters)
        {
            FetchSearchCriteriaResultsDto result = new FetchSearchCriteriaResultsDto();
            result.Key = parameters.Arguments.DealerId + "_" + parameters.Arguments.InventoryId;
            result.Criteria = new SearchCriteriaDto()
                             {
                                 AvgMileage = parameters.Arguments.AvgMileage,
                                 AvgPrice = parameters.Arguments.AvgPrice,
                                 Certified = parameters.Arguments.Certified,
                                 DealerId = parameters.Arguments.DealerId,
                                 Distance = parameters.Arguments.Distance,
                                 DriveTrian = parameters.Arguments.DriveTrian,
                                 Engine = parameters.Arguments.Engine,
                                 Fuel = parameters.Arguments.Fuel,
                                 Facet = parameters.Arguments.Facet,
                                 InventoryId = parameters.Arguments.InventoryId,
                                 Listings = parameters.Arguments.Listings,
                                 Trim = parameters.Arguments.Trim,
                                 MaxMileage = parameters.Arguments.MaxMileage,
                                 MaxPrice = parameters.Arguments.MaxPrice,
                                 MinMileage = parameters.Arguments.MinMileage,
                                 MinPrice = parameters.Arguments.MinPrice,
                                 NotCertified = parameters.Arguments.NotCertified,
                                 OverallCount = parameters.Arguments.OverallCount,
                                 PrecisionCount = parameters.Arguments.PrecisionCount,
                                 Transmission = parameters.Arguments.Transmission,
                                 Vin = parameters.Arguments.Vin
                             };
            //Generate criteria document to ship to S3
            var criteriaDocument = Mapper.ToJson(parameters.Arguments, result);

            // *************Save the message to the S3 Bucket and S3 queue**********************

            string bucketName = ConfigurationManager.AppSettings["ProfitSearchBucket"];

            // Get the S3 client for our region.
            IAmazonS3 s3Client = Amazon.AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.USEast1);

            try
            {
                if (Exists(result.Key, bucketName))
                {
                    Delete(result.Key, bucketName);
                }

                PutObjectRequest s3Request = new PutObjectRequest() { ContentBody = criteriaDocument, BucketName = bucketName, Key = result.Key };


                PutObjectResponse response = s3Client.PutObject(s3Request);
                Log.Info("Successfully wrote appraisal id = " + result.Key + " to S3 Bucket = " + bucketName);


            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null && (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") || amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    Log.Error("Error Writing Appraisal to S3 Bucket - " + bucketName + ":  Incorrect AWS Credentials");
                    throw new UserAccessException("AWS", "Forbidden", "Incorrect AWS Credentials.");
                }
                else
                {
                    Log.Error("Exception Writing Appraisal to S3 Bucket - " + bucketName, amazonS3Exception);
                    throw new FirstlookGeneralException("Errorcode " + amazonS3Exception.ErrorCode + " when writing to S3 Bucket: " + bucketName + " - Error message = " + amazonS3Exception.Message);
                }
            }

            return result;
        }

        public bool Exists(string fileKey, string bucketName)
        {
            IAmazonS3 s3Client = Amazon.AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.USEast1);
            try
            {
                var response = s3Client.GetObjectMetadata(new GetObjectMetadataRequest() { BucketName = bucketName, Key = fileKey });

                return true;
            }

            catch (Amazon.S3.AmazonS3Exception ex)
            {
                if (ex.StatusCode == System.Net.HttpStatusCode.NotFound)
                    return false;

                //status wasn't not found, so throw the exception
                throw;
            }
        }

        public void Delete(string fileKey, string bucketName)
        {
            IAmazonS3 s3Client = Amazon.AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.USEast1);
            DeleteObjectRequest request = new DeleteObjectRequest();
            request.BucketName = bucketName;
            request.Key = fileKey;
            s3Client.DeleteObject(request);
        }
    }
}
