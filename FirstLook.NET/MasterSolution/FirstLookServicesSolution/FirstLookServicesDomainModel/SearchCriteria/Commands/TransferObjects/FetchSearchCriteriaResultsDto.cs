﻿
using System.Runtime.Serialization;
using FirstLook.FirstLookServices.DomainModel.SearchCriteria.Model;

namespace FirstLook.FirstLookServices.DomainModel.SearchCriteria.Commands.TransferObjects
{
   public  class FetchSearchCriteriaResultsDto
    {
        [DataMember(Name = "resultKey")]
        public string Key { get; set; }

        [DataMember(Name = "searchCriteriaRequest")]
        public SearchCriteriaDto Criteria { get; set; }
    }
}
