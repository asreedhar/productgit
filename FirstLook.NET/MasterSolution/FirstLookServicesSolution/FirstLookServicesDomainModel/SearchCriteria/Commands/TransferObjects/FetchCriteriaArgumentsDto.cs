﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.SearchCriteria.Commands.TransferObjects
{
   public  class FetchCriteriaArgumentsDto
    {
       public int InventoryId { get; set; }

       public int DealerId { get; set; }

       public string Vin { get; set; }
    }
}
