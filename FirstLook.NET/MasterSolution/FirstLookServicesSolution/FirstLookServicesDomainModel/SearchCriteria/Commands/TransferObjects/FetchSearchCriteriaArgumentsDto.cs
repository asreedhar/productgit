﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.SearchCriteria.Commands.TransferObjects
{
    public class FetchSearchCriteriaArgumentsDto
    {

        public string Vin { get; set; }

        public int InventoryId { get; set; }

        public int DealerId { get; set; }

        public int OverallCount { get; set; }

        public int PrecisionCount { get; set; }

        public int MaxMileage { get; set; }

        public int MinMileage { get; set; }

        public List<string> Listings { get; set; }

        public int Distance { get; set; }

        public bool Certified { get; set; }

        public bool NotCertified { get; set; }

        public List<string> Trim { get; set; }

        public List<string> DriveTrian { get; set; }

        public List<string> Transmission { get; set; }

        public List<string> Engine { get; set; }

        public List<string> Fuel { get; set; }

        public List<string> Facet { get; set; }

        public int AvgMileage { get; set; }

        public int MinPrice { get; set; }

        public int MaxPrice { get; set; }

        public int AvgPrice { get; set; }
    }
}
