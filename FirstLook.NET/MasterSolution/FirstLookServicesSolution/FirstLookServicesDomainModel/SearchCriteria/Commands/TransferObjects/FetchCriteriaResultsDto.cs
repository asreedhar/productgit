﻿using System.Runtime.Serialization;
using FirstLook.FirstLookServices.DomainModel.SearchCriteria.Model;

namespace FirstLook.FirstLookServices.DomainModel.SearchCriteria.Commands.TransferObjects
{
    [DataContract]
    public class FetchCriteriaResultsDto
    {
        [DataMember(Name="resultKey")]
        public string Key { get; set; }

        [DataMember(Name = "searchCriteriaRequest")]
        public SearchCriteriaDto Criteria{get; set;}

    }
}
