﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.Command;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.SearchCriteria.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.SearchCriteria.Commands
{
    public interface ICommandFactory
    {
        ICommand<FetchSearchCriteriaResultsDto, IdentityContextDto<FetchSearchCriteriaArgumentsDto>> CreateSearchCriteriaCommand();

        ICommand<FetchCriteriaResultsDto, IdentityContextDto<FetchCriteriaArgumentsDto>> GetCriteriaFromAmazomCommand();
    }
}
