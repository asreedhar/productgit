﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.DomainModel.SearchCriteria.Model
{
    [DataContract]
    public class SearchCriteriaDto
    {
        [DataMember(Name = "vin")]
        public string Vin { get; set; }

        [DataMember(Name = "inventoryId")]
        public int InventoryId { get; set; }

        [DataMember(Name = "dealerId")]
        public int DealerId { get; set; }

        [DataMember(Name = "overallCount")]
        public int OverallCount { get; set; }

        [DataMember(Name = "precisionCount")]
        public int PrecisionCount { get; set; }

        [DataMember(Name = "maxMileage")]
        public int MaxMileage { get; set; }

        [DataMember(Name = "minMileage")]
        public int MinMileage { get; set; }

        [DataMember(Name = "listings")]
        public List<string> Listings { get; set; }

        [DataMember(Name = "distance")]
        public int Distance { get; set; }

        [DataMember(Name = "certified")]
        public bool Certified { get; set; }

        [DataMember(Name = "notCertified")]
        public bool NotCertified { get; set; }

        [DataMember(Name = "trim")]
        public List<string> Trim { get; set; }

        [DataMember(Name = "driveTrain")]
        public List<string> DriveTrian { get; set; }

        [DataMember(Name = "transmission")]
        public List<string> Transmission { get; set; }

        [DataMember(Name = "engine")]
        public List<string> Engine { get; set; }

        [DataMember(Name = "fuel")]
        public List<string> Fuel { get; set; }

        [DataMember(Name = "facet")]
        public List<string> Facet { get; set; }

        [DataMember(Name = "avgMileage")]
        public int AvgMileage { get; set; }

        [DataMember(Name = "minPrice")]
        public int MinPrice { get; set; }

        [DataMember(Name = "maxPrice")]
        public int MaxPrice { get; set; }

        [DataMember(Name = "avgPrice")]
        public int AvgPrice { get; set; }
    }
}
