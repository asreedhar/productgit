﻿using System;
using FirstLook.Common.Core.Registry;

namespace FirstLook.FirstLookServices.DomainModel.Books.Repositories.Datastores
{
    [Serializable]
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IBookDatastore, BookDatastore>(ImplementationScope.Shared);
        }
    }
}
