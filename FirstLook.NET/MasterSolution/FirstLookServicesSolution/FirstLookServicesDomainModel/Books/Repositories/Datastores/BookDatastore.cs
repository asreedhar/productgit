﻿using System;
using System.Data;
using System.Reflection;
using FirstLook.Common.Core.Data;

namespace FirstLook.FirstLookServices.DomainModel.Books.Repositories.Datastores
{
    public class BookDatastore : SimpleDataStore, IBookDatastore
    {
        internal const string Prefix = "FirstLook.FirstLookServices.DomainModel.Books.Repositories.Resources";

        public IDataReader DealerBooks(int dealerId)
        {
            const string queryName = Prefix + ".BooksDatastore_Dealer_Fetch.txt";

            return Query(new[] { "DealerPreference", "ThirdParties" }, queryName, new Parameter("DealerID", dealerId, DbType.Int32));
        }

        public IDataReader Books()
        {
            const string queryName = Prefix + ".BooksDatastore_Books_Fetch.txt";

            return Query(new[] { "ThirdParties" }, queryName);
        }

        /// <summary>
        /// Get the type of the current assembly.
        /// </summary>
        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        protected override string DatabaseName
        {
            get { return "IMT"; }
        }
    }
}
