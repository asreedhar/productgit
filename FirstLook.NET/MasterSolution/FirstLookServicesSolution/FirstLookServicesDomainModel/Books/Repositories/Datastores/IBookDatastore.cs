﻿using System.Data;

namespace FirstLook.FirstLookServices.DomainModel.Books.Repositories.Datastores
{
    public interface IBookDatastore
    {
        IDataReader DealerBooks(int dealerId);

        IDataReader Books();
    }
}
