﻿using System;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Books.Model;

namespace FirstLook.FirstLookServices.DomainModel.Books.Repositories
{
    [Serializable]
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<Datastores.Module>();

            registry.Register<Serializers.Module>();

            registry.Register<IRepository, BookRepository>(ImplementationScope.Shared);
        }
    }
}
