﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.Books.Model;

namespace FirstLook.FirstLookServices.DomainModel.Books.Repositories.Serializers
{
    public class BookSerializer : Serializer<Book>
    {
        public override Book Deserialize(IDataRecord record)
        {
            return new Book
            {
                Name = DataRecord.GetString(record, "Name"),
                Description = DataRecord.GetString(record, "Description")
            };
        }
    }
}
