﻿using System;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Books.Model;

namespace FirstLook.FirstLookServices.DomainModel.Books.Repositories.Serializers
{
    [Serializable]
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ISerializer<Book>, BookSerializer>(ImplementationScope.Shared);
        }
    }
}
