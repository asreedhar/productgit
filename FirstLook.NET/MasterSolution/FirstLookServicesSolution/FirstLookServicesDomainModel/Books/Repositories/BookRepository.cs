﻿using System;
using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.Books.Model;
using FirstLook.FirstLookServices.DomainModel.Books.Repositories.Gateways;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core;

namespace FirstLook.FirstLookServices.DomainModel.Books.Repositories
{
    public class BookRepository : RepositoryBase, IRepository
    {

        #region IRepository Members

        public IList<Book> DealerBooks(int dealerId)
        {
            return new BooksGateway().Fetch(dealerId);
        }

        public IList<Book> Books()
        {
            return new BooksGateway().Fetch();
        }

        #endregion

        protected override string DatabaseName
        {
            get { return "IMT"; }
        }

        protected override void OnBeginTransaction(IDataSession session)
        {
            // do nothing -- we do not audited creation of reference/client items
        }
    }
}
