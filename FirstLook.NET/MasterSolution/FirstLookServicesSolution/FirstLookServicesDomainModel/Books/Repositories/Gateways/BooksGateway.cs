﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Common.Core;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.FirstLookServices.DomainModel.Books.Model;
using FirstLook.FirstLookServices.DomainModel.Books.Repositories.Datastores;

namespace FirstLook.FirstLookServices.DomainModel.Books.Repositories.Gateways
{
    public class BooksGateway : GatewayBase
    {
        public IList<Book> Fetch(int dealerId)
        {
            string key = CreateCacheKey("" + dealerId);

            IList<Book> value = Cache.Get(key) as List<Book>;

            if (value == null)
            {
                var serializer = Resolve<ISerializer<Book>>();

                var datastore = Resolve<IBookDatastore>();

                using (IDataReader reader = datastore.DealerBooks(dealerId))
                {
                    IList<Book> items = serializer.Deserialize(reader);

                    value = items;

                    Remember(key, value);
                }
            }

            return value;
        }

        public IList<Book> Fetch()
        {
            string key = CreateCacheKey("books");

            IList<Book> value = Cache.Get(key) as List<Book>;

            if (value == null)
            {
                var serializer = Resolve<ISerializer<Book>>();

                var datastore = Resolve<IBookDatastore>();

                using (IDataReader reader = datastore.Books())
                {
                    IList<Book> items = serializer.Deserialize(reader);

                    value = items;

                    Remember(key, value);
                }
            }

            return value;
        }
    }
}
