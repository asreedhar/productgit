﻿using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.Books.Model
{
    public interface IRepository
    {
        IList<Book> DealerBooks(int dealerId);

        IList<Book> Books();
    }
}
