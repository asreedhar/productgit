﻿namespace FirstLook.FirstLookServices.DomainModel.Books.Model
{
    public interface IBook
    {
        string Name { get; }

        string Description { get; }
    }
}
