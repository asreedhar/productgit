﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Books.Model
{
    [Serializable]
    public class Book : IBook
    {
        private string _name;
        private string _description;

        public string Name
        {
            get { return _name; }
            internal set { _name = value; }
        }

        public string Description
        {
            get { return _description; }
            internal set { _description = value; }
        }
    }
}
