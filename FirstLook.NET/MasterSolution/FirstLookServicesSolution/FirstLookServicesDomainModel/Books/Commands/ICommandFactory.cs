﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.Books.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.Books.Commands
{
    public interface ICommandFactory
    {
        ICommand<FetchDealerBooksResultsDto, IdentityContextDto<FetchDealerBooksArgumentsDto>> CreateFetchDealerBooksCommand();

        ICommand<FetchBooksResultsDto, FetchBooksArgumentsDto> CreateFetchBooksCommand();

    }
}
