﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.Books.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.Books.Commands.Impl
{
    class CommandFactory : ICommandFactory
    {
        public ICommand<FetchDealerBooksResultsDto, IdentityContextDto<FetchDealerBooksArgumentsDto>> CreateFetchDealerBooksCommand()
        {
            return new FetchDealerBooksCommand();
        }

        public ICommand<FetchBooksResultsDto, FetchBooksArgumentsDto> CreateFetchBooksCommand()
        {
            return new FetchBooksCommand();
        }
    }
}
