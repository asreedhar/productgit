﻿using System.Collections.Generic;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Books.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Books.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Books.Model;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.FirstLookServices.DomainModel.Validation;

namespace FirstLook.FirstLookServices.DomainModel.Books.Commands.Impl
{
    public class FetchDealerBooksCommand : ICommand<FetchDealerBooksResultsDto, IdentityContextDto<FetchDealerBooksArgumentsDto>>
    {
        public FetchDealerBooksResultsDto Execute(IdentityContextDto<FetchDealerBooksArgumentsDto> parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            var bookRepository = resolver.Resolve<IRepository>();

            var paramId = parameters.Arguments.DealerId;

            Validators.CheckDealerId(paramId, parameters);

            IList<BookDto> books = Mapper.Map(bookRepository.DealerBooks(paramId));
            //Below code checks the count of books
            //Added below code on 1/02/2014
            if (books.Count == 0)
            {
                throw new NoMobileBookDataFoundException("You do not have access to any books on your mobile device");
            }
            //End of the code
            return new FetchDealerBooksResultsDto
                       {
                           Books = (List<BookDto>) books
                       };
        }
    }
}
