﻿using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.Books.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Books.Model;
using System.Linq;

namespace FirstLook.FirstLookServices.DomainModel.Books.Commands.Impl
{
    public static class Mapper
    {
        public static IList<BookDto> Map (IList<Book> objs)
        {
            IList<BookDto> bookDtos = objs.Select(obj => new BookDto {Description = obj.Description, Name = obj.Name}).ToList();

            return bookDtos;
        }
    }
}
