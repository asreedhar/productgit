﻿using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Books.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Books.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Books.Model;

namespace FirstLook.FirstLookServices.DomainModel.Books.Commands.Impl
{
    public class FetchBooksCommand : ICommand<FetchBooksResultsDto, FetchBooksArgumentsDto>
    {
        public FetchBooksResultsDto Execute(FetchBooksArgumentsDto parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            var bookRepository = resolver.Resolve<IRepository>();

            IList<BookDto> books = Mapper.Map(bookRepository.Books());

            return new FetchBooksResultsDto
                       {
                           Books = (List<BookDto>) books
                       };
        }
    }
}
