﻿namespace FirstLook.FirstLookServices.DomainModel.Books.Commands.TransferObjects
{
    public class BookDto
    {
        public string Description { get; set; }

        public string Name { get; set; }
    }
}
