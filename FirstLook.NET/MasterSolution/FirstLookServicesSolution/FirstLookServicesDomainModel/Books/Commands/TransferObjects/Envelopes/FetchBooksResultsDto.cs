﻿using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.Books.Commands.TransferObjects.Envelopes
{
    public class FetchBooksResultsDto
    {
        public FetchBooksArgumentsDto Arguments { get; set; }

        public List<BookDto> Books { get; set; }
    }
}
