﻿using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.Books.Commands.TransferObjects.Envelopes
{
    public class FetchDealerBooksResultsDto
    {
        public PrincipalDto Principal { get; set; }

        public FetchDealerBooksArgumentsDto Arguments { get; set; }

        public List<BookDto> Books { get; set; }
    }
}
