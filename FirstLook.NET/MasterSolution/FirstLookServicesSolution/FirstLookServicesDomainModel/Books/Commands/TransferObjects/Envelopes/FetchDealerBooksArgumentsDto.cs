﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Books.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public partial class FetchDealerBooksArgumentsDto
    {
        public int DealerId { get; set; }
    }
}
