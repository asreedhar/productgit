﻿
namespace FirstLook.FirstLookServices.DomainModel.SalesPeople.Model
{
   public class SalesPeopleDetails
    {
        public int PersonId { get; set; }
        public string Name { get; set; }
    }
}
