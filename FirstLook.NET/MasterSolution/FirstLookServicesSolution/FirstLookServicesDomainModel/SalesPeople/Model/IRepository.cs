﻿using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.SalesPeople.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.SalesPeople.Model
{
   public interface IRepository
   {
       IList<SalesPeopleDetails> SalesPeople(SalesPeopleInput salesPeople);

   }
}
