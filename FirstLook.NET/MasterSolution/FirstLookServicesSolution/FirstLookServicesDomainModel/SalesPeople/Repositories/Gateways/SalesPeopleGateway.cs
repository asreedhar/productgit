﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.FirstLookServices.DomainModel.SalesPeople.Model;
using FirstLook.FirstLookServices.DomainModel.SalesPeople.Repositories.DataStores;
using FirstLook.FirstLookServices.DomainModel.SalesPeople.Repositories.Entities;


namespace FirstLook.FirstLookServices.DomainModel.SalesPeople.Repositories.Gateways
{
   public class SalesPeopleGateway:GatewayBase
    {
       public IList<SalesPeopleDetails> Fetch(SalesPeopleInput salesPeople)
       {
           string key = CreateCacheKey("" + salesPeople.BusinessUnitId);

           IList<SalesPeopleDetails> value = Cache.Get(key) as List<SalesPeopleDetails>;

           if (value == null)
           {
               var serializer = Resolve<ISerializer<SalesPeopleDetails>>();

               var datastore = Resolve<ISalesPeopleDataStore>();

               using (IDataReader reader = datastore.StorePerformance(salesPeople))
               {
                   IList<SalesPeopleDetails> items = serializer.Deserialize(reader);

                   value = items;

                   Remember(key, value);
               }
           }

           return value;
       }
    }
}
