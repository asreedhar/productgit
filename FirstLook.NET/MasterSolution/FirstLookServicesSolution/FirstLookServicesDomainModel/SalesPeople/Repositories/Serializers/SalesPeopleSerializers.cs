﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.SalesPeople.Model;

namespace FirstLook.FirstLookServices.DomainModel.SalesPeople.Repositories.Serializers
{
   public class SalesPeopleSerializers:Serializer<SalesPeopleDetails>
   {
       public override SalesPeopleDetails Deserialize(IDataRecord record)
       {

           int Id  = 0;
           string name = string.Empty;

           try
           {
               Id  = DataRecord.GetInt32(record, "PersonId", 0);
               name = DataRecord.GetString(record, "Name");
           }
           catch (Exception ex)
           {
               throw ex;
           }

           return new SalesPeopleDetails
           {
               PersonId = Id,
               Name = name.ToString()
           };
       }
    }
}
