﻿using System;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.SalesPeople.Model;

namespace FirstLook.FirstLookServices.DomainModel.SalesPeople.Repositories.Serializers
{
    [Serializable]
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ISerializer<SalesPeopleDetails>, SalesPeopleSerializers>(ImplementationScope.Shared);
        }
    }
}
