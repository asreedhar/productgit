﻿using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.SalesPeople.Model;
using FirstLook.FirstLookServices.DomainModel.SalesPeople.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.SalesPeople.Repositories.Gateways;


namespace FirstLook.FirstLookServices.DomainModel.SalesPeople.Repositories
{
  public  class SalesPeopleRepository:IRepository
    {
        public IList<SalesPeopleDetails>  SalesPeople(SalesPeopleInput salesPeople)
        {
            return new SalesPeopleGateway().Fetch(salesPeople);
        }
    }
}
