﻿using System.Data;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.SalesPeople.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.SalesPeople.Repositories.DataStores
{

    public class SalesPeopleDataStore:ISalesPeopleDataStore
    {
        #region Class Members

        private const string DatabaseName = "IMT";

        #endregion

        #region ISalesPeopleDataStore Members

       

        public IDataReader StorePerformance(SalesPeopleInput salesPeople)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "dbo.Fetch#SalesPeopleData";
                 
                    Database.AddWithValue(command, "BusinessUnitID", salesPeople.BusinessUnitId, DbType.Int32);

                    var set = new DataSet();

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        var table = new DataTable("SalesPeople");

                        table.Load(reader);

                        set.Tables.Add(table);
                    }

                    return set.CreateDataReader();
                }
            }
        }
        #endregion

    }
}
