﻿using System.Data;
using FirstLook.FirstLookServices.DomainModel.SalesPeople.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.SalesPeople.Repositories.DataStores
{
   public  interface ISalesPeopleDataStore
    {

       IDataReader StorePerformance(SalesPeopleInput salesPeople);
    }
}
