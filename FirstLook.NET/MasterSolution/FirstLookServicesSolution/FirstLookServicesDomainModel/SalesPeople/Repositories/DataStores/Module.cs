﻿using System;
using FirstLook.Common.Core.Registry;


namespace FirstLook.FirstLookServices.DomainModel.SalesPeople.Repositories.DataStores
{
    [Serializable]
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ISalesPeopleDataStore, SalesPeopleDataStore>(ImplementationScope.Shared);
        }
    }
}
