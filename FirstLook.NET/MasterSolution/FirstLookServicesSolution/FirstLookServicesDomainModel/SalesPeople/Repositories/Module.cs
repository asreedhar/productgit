﻿using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.SalesPeople.Model;


namespace FirstLook.FirstLookServices.DomainModel.SalesPeople.Repositories
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<DataStores.Module>();

            registry.Register<Serializers.Module>();

            registry.Register<IRepository, SalesPeopleRepository>(ImplementationScope.Shared);
        }
    }
}
