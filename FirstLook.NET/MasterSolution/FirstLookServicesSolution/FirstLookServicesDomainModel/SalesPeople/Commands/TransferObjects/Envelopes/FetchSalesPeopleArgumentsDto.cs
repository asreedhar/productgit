﻿namespace FirstLook.FirstLookServices.DomainModel.SalesPeople.Commands.TransferObjects.Envelopes
{
   public class FetchSalesPeopleArgumentsDto
    {
        public int BusinessUnitId { get; set; }
    }
}
