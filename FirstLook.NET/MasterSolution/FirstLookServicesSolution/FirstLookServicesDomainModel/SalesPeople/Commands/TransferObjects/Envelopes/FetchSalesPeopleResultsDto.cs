﻿using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.SalesPeople.Commands.TransferObjects.Envelopes
{
    public class FetchSalesPeopleResultsDto
    {
        public PrincipalDto Principal { get; set; }

        public FetchSalesPeopleArgumentsDto Arguments { get; set; }

        public IList<SalesPeopleDto> SalesPeople { get; set; }

    }
}
