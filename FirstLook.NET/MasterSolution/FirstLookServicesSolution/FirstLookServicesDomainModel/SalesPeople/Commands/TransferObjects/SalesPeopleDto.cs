﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.SalesPeople.Commands.TransferObjects
{
    public class SalesPeopleDto
    {
        public int PersonId { get; set; }
        public string Name { get; set; }
    }
}
