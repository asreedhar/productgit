﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.SalesPeople.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.SalesPeople.Commands.Impl
{
   public class CommandFactory:ICommandFactory
   {
       #region IcommandFactory Members
       public ICommand<FetchSalesPeopleResultsDto,IdentityContextDto<FetchSalesPeopleArgumentsDto>> CreateFetchSalesPeopleCommand()
       {
           return new FetchSalesPeopleCommand();
       }
       #endregion

   }
}
