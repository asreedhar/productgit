﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.FirstLookServices.DomainModel.SalesPeople.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.SalesPeople.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.SalesPeople.Model;
using FirstLook.FirstLookServices.DomainModel.SalesPeople.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.Validation;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.SalesPeople.Commands.Impl
{
  public  class FetchSalesPeopleCommand : ICommand<FetchSalesPeopleResultsDto, IdentityContextDto<FetchSalesPeopleArgumentsDto>>
  {
      #region ICommand<FetchStorePerformanceResultsDto,IdentityContextDto<FetchStorePerformanceArgumentsDto>> Members
      public FetchSalesPeopleResultsDto Execute(IdentityContextDto<FetchSalesPeopleArgumentsDto> parameters)
      {
          IResolver resolver = RegistryFactory.GetResolver();
          var salesPeopleRepository = resolver.Resolve<IRepository>();
          var paramId = parameters.Arguments.BusinessUnitId;
          Validators.CheckDealerId(paramId, parameters);
          SalesPeopleInput salesPeopleInput = new SalesPeopleInput()
                                                  {
                                                      BusinessUnitId = parameters.Arguments.BusinessUnitId
                                                  };

          IList<SalesPeopleDto> salesPeople = Mapper.Map(salesPeopleRepository.SalesPeople(salesPeopleInput));
			
		  if (salesPeople == null || salesPeople.Count == 0)
          {
              throw new NoDataFoundException("Sales People");
          }
		  
          return new FetchSalesPeopleResultsDto()
                     {
                         SalesPeople = salesPeople
                     };
      }

      #endregion
  }
}
