﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.FirstLookServices.DomainModel.SalesPeople.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.SalesPeople.Model;

namespace FirstLook.FirstLookServices.DomainModel.SalesPeople.Commands.Impl
{
   public static class Mapper
    {
       public static IList<SalesPeopleDto> Map(IList<SalesPeopleDetails> objs)
       {
           IList<SalesPeopleDto> salesPeopleDtos = objs.Select(obj => new SalesPeopleDto {PersonId = obj.PersonId, Name = obj.Name}).ToList();
           return salesPeopleDtos;

       }
    }
}
