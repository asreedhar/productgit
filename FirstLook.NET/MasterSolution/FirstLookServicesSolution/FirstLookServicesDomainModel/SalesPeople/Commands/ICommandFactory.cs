﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.SalesPeople.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.SalesPeople.Commands
{
   public interface ICommandFactory
   {
        ICommand<FetchSalesPeopleResultsDto, IdentityContextDto<FetchSalesPeopleArgumentsDto>> CreateFetchSalesPeopleCommand();
   }
}
