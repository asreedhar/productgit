﻿using System;
using FirstLook.Common.Core.Registry;

namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Repositories.Datastores
{
    [Serializable]
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IAppraisalDatastore, AppraisalDatastore>(ImplementationScope.Shared);
        }
    }
}
