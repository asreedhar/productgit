﻿using System;
using System.Data;
using System.Reflection;
using FirstLook.Common.Core.Data;

namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Repositories.Datastores
{
    public class AppraisalDatastore : SimpleDataStore, IAppraisalDatastore
    {
        internal const string Prefix = "FirstLook.FirstLookServices.DomainModel.Appraisals.Repositories.Resources";

        /// <summary>
        /// Get the type of the current assembly.
        /// </summary>
        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }        
        protected override string DatabaseName
        {
            get { return "Vehicle"; }
        }

        #region appraisal

        public IDataReader InsertAppraisal(int clientVehicleId, DateTime created, string user, byte appraisalTypeId, int? appraisalAmount, int? totalGrossProfit, int? estimatedRecon, int? targetSellingPrice,string reconNotes)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "Client.Appraisal#Insert";

                    Database.AddWithValue(command, "ClientVehicleID", clientVehicleId, DbType.Int32);
                    Database.AddWithValue(command, "Created", created, DbType.DateTime);
                    Database.AddWithValue(command, "User", user, DbType.AnsiString);
                    Database.AddWithValue(command, "AppraisalTypeID", appraisalTypeId, DbType.Byte);
                    Database.AddWithValue(command, "AppraisalAmount", appraisalAmount, DbType.Int32);
                    Database.AddWithValue(command, "TargetGrossProfit", totalGrossProfit, DbType.Int32);
                    Database.AddWithValue(command, "EstimatedRecon", estimatedRecon, DbType.Int32);
                    Database.AddWithValue(command, "TargetSellingPrice", targetSellingPrice, DbType.Int32);
                    Database.AddWithValue(command, "ReconNotes", reconNotes, DbType.String);

                    var set = new DataSet();

                    using (IDataReader reader = command.ExecuteReader())
                    {
                       
                        var table = new DataTable("Appraisal");

                        table.Load(reader);

                        set.Tables.Add(table);
                    }

                    return set.CreateDataReader();
                }
            }
        }

        public IDataReader FetchAppraisals(int dealerId, int? days)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "Client.Appraisal#Fetch";

                    Database.AddWithValue(command, "DealerID", dealerId, DbType.Int32);
                    Database.AddWithValue(command, "Days", days, DbType.Int32);

                    var set = new DataSet();

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        var table = new DataTable("Appraisals");

                        table.Load(reader);

                        set.Tables.Add(table);
                    }

                    return set.CreateDataReader();
                }
            }
        }

      
        /// <summary>
        /// For case id 28593-performance improvement.clubbing two sp and one inline query
        /// </summary>
        /// <param name="dealerId"></param>
        /// <param name="days"></param>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        public IDataReader FetchAppraisals(int dealerId, int? days,int brokerId)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                   
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "Client.AppraisalsNonInv#Fetch";

                    Database.AddWithValue(command, "DealerID", dealerId, DbType.Int32);
                    Database.AddWithValue(command, "Days", days, DbType.Int32);
                    Database.AddWithValue(command, "ClientID", brokerId, DbType.Int32);
                   
                    var set = new DataSet();

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        var table = new DataTable("Appraisals");

                        table.Load(reader);

                        set.Tables.Add(table);
                    }

                    return set.CreateDataReader();
                }
            }
        }

        #endregion

        #region vehicle

        public IDataReader UpsertVehicle(int clientVehicleId, string color, int? mileage)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "Client.VehicleAttributes#Upsert";

                    Database.AddWithValue(command, "ClientVehicleID", clientVehicleId, DbType.Int32);
                    Database.AddWithValue(command, "Color", color, DbType.AnsiString);
                    Database.AddWithValue(command, "Mileage", mileage, DbType.Int32);

                    var set = new DataSet();

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        var table = new DataTable("VehicleAttributes");

                        table.Load(reader);

                        set.Tables.Add(table);
                    }

                    return set.CreateDataReader();
                }
            }
        }

        public IDataReader FetchVehicle(int clientVehicleId)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "Client.VehicleAttributes#Fetch";

                    Database.AddWithValue(command, "ClientVehicleID", clientVehicleId, DbType.Int32);

                    var set = new DataSet();

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        var table = new DataTable("VehicleAttributes");

                        table.Load(reader);

                        set.Tables.Add(table);
                    }

                    return set.CreateDataReader();
                }
            }
        }

        #endregion
    }
}
