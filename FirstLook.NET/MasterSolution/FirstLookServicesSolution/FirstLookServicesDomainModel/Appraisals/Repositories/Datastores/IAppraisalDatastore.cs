﻿using System;
using System.Data;

namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Repositories.Datastores
{
    public interface IAppraisalDatastore
    {
        #region appraisal

        IDataReader InsertAppraisal(int clientVehicleId, DateTime created, string user, byte appraisalTypeId, int? appraisalAmount, int? totalGrossProfit, int? estimatedRecon, int? targetSellingPrice, string reconNotes);

        IDataReader FetchAppraisals(int dealerId, int? days);

        
        /// <summary>
        /// For case id 28593-performance improvement.clubbing two sp and one inline query
        /// </summary>
        /// <param name="dealerId"></param>
        /// <param name="days"></param>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        IDataReader FetchAppraisals(int dealerId, int? days,int brokerId);

        #endregion

        #region vehicle

        IDataReader UpsertVehicle(int clientVehicleId, string color, int? mileage);

        IDataReader FetchVehicle(int clientVehicleId);

        #endregion
    }
}
