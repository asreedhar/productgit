﻿using FirstLook.Common.Core.Data;
using FirstLook.Common.Core;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Model;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Repositories.Gateways;

namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Repositories
{
    public class VehicleRepository : RepositoryBase, IVehicleRepository
    {
        #region IRepository Members

       
        #endregion

        protected override string DatabaseName
        {
            get { return "Vehicle"; }
        }

        protected override void OnBeginTransaction(IDataSession session)
        {
            // do nothing
        }

        public Vehicle Upsert(int clientVehicleId, Vehicle vehicle)
        {
            return DoInTransaction(() => new VehicleAttributesGateway().Upsert(clientVehicleId, vehicle));
        }

        public Vehicle Fetch(int clientVehicleId)
        {
            return DoInSession(() => new VehicleAttributesGateway().Fetch(clientVehicleId));
        }

    }
}
