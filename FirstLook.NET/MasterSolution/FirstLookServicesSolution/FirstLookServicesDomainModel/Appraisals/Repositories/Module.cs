﻿using System;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Model;

namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Repositories
{
    [Serializable]
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<Datastores.Module>();

            registry.Register<Serializers.Module>();

            registry.Register<IAppraisalRepository, AppraisalRepository>(ImplementationScope.Shared);

            registry.Register<IVehicleRepository, VehicleRepository>(ImplementationScope.Shared);
        }
    }
}
