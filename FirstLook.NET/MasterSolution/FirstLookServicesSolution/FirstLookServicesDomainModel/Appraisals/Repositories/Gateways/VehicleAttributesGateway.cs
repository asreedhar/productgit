﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Model;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Repositories.Datastores;

namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Repositories.Gateways
{
    public class VehicleAttributesGateway : GatewayBase
    {
        public Vehicle Upsert(int clientVehicleId, Vehicle vehicle)
        {
            var serializer = Resolve<ISerializer<Vehicle>>();

            var datastore = Resolve<IAppraisalDatastore>();

            using (IDataReader reader = datastore.UpsertVehicle(clientVehicleId, vehicle.Color, vehicle.Mileage))
            {
                if (reader.Read())
                {
                    return serializer.Deserialize((IDataRecord)reader);
                }                
            }

            return null;
        }

        public Vehicle Fetch(int clientVehicleId)
        {
            var serializer = Resolve<ISerializer<Vehicle>>();

            var datastore = Resolve<IAppraisalDatastore>();

            using (IDataReader reader = datastore.FetchVehicle(clientVehicleId))
            {
                if (reader.Read())
                {
                    return serializer.Deserialize((IDataRecord)reader);
                }
            }

            return null;
        }
    }
}
