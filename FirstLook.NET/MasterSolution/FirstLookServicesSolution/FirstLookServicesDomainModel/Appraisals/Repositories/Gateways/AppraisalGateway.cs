﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Model;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Repositories.Datastores;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Repositories.Gateways
{
    public class AppraisalGateway : GatewayBase
    {
        public Appraisal Insert(int clientVehicleId, Appraisal appraisal)
        {
            var serializer = Resolve<ISerializer<Appraisal>>();

            var datastore = Resolve<IAppraisalDatastore>();

            using (IDataReader reader = datastore.InsertAppraisal(clientVehicleId, DateTime.Now, appraisal.User, (byte)(appraisal.AppraisalType == AppraisalType.Trade ? 1 : 2), appraisal.AppraisalAmount, appraisal.TargetGrossProfit, appraisal.EstimatedRecon, appraisal.TargetSellingPrice, appraisal.ReconNotes))
            {
                if (reader.Read())
                {
                    return serializer.Deserialize((IDataRecord)reader);
                }
            }

            return null;
        }

        public IList<Appraisal> Fetch(int dealerId, int? days)
        {
            var serializer = Resolve<ISerializer<Appraisal>>();

            var store = Resolve<IAppraisalDatastore>();

            using (IDataReader reader = store.FetchAppraisals(dealerId, days))
            {
                IList<Appraisal> appraisals = serializer.Deserialize(reader);

                return appraisals;
            }
        }
       

        /// <summary>
        /// For case id 28593-performance improvement.clubbing two sp and one inline query
        /// </summary>
        /// <param name="dealerId"></param>
        /// <param name="days"></param>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        public IList<FetchAppraisalQueryResultDto> Fetch(int dealerId, int? days, int brokerId)
        {
            var serializer = Resolve<ISerializer<FetchAppraisalQueryResultDto>>();

            var store = Resolve<IAppraisalDatastore>();

            using (IDataReader reader = store.FetchAppraisals(dealerId, days,brokerId))
            {
                IList<FetchAppraisalQueryResultDto> appraisals = serializer.Deserialize(reader);

                return appraisals;
            }
        }

        
    }
}
