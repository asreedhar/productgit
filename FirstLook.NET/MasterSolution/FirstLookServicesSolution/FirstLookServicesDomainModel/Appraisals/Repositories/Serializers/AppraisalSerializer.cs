﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Model;

namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Repositories.Serializers
{
    public class AppraisalSerializer : Serializer<Appraisal>
    {
        public override Appraisal Deserialize(IDataRecord record)
        {
            int appraisalId = record.GetInt32("AppraisalID", 0);

            int clientVehicleId = record.GetInt32("ClientVehicleID", 0);

            int? targetGrossProfit = record.GetNullableInt32("TargetGrossProfit");

            int? appraisalAmount = record.GetNullableInt32("AppraisalAmount");

            int? estimatedRecon = record.GetNullableInt32("EstimatedRecon");

            string reconNotes = record.GetString("ReconNotes");

            int? targetSellingPrice = record.GetNullableInt32("TargetSellingPrice");

            DateTime created = record.GetDateTime(record.GetOrdinal("Created"));

            string user = record.GetString("User");

            byte appraisalTypeId = record.GetByte(record.GetOrdinal("AppraisalTypeID"));

            return new Appraisal
                       {
                           Id = appraisalId,
                           AppraisalAmount = appraisalAmount,
                           TargetGrossProfit = targetGrossProfit,
                           EstimatedRecon = estimatedRecon,
                           ReconNotes = reconNotes,
                           TargetSellingPrice = targetSellingPrice,
                           DateCreated = created,
                           User = user,
                           AppraisalType = appraisalTypeId == 1 ? AppraisalType.Trade : AppraisalType.Purchase,
                           ClientVehicleId = clientVehicleId
                       };
        }
    }
}
