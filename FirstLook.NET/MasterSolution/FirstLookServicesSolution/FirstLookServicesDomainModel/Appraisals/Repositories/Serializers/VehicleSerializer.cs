﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Model;

namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Repositories.Serializers
{
    public class VehicleSerializer : Serializer<Vehicle>
    {
        public override Vehicle Deserialize(IDataRecord record)
        {
            int? mileage = record.GetNullableInt32("Mileage");

            string color = record.GetString("Color");

            int clientVehicleId = record.GetInt32("ClientVehicleID", 0);

            string vin = record.GetString("VIN");

            return new Vehicle
                       {
                           Color = color,
                           Mileage = mileage,
                           Id = clientVehicleId,
                           Vin = vin
                       };
        }
    }
}
