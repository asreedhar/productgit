﻿using System;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Model;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Repositories.Serializers
{
    [Serializable]
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ISerializer<Appraisal>, AppraisalSerializer>(ImplementationScope.Shared);

            registry.Register<ISerializer<Vehicle>, VehicleSerializer>(ImplementationScope.Shared);

            // For case id 28593-performance improvement.clubbing two sp and one inline query.Register serializer
            registry.Register<ISerializer<FetchAppraisalQueryResultDto>, AppraisalSummaryResultSerializer>(ImplementationScope.Shared);

        }
    }
}
