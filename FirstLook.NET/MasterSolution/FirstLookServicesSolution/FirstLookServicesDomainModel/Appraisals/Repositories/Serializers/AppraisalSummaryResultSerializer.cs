﻿using System;
using System.Data;
using FirstLook.Common.Core.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Model;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects;


namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Repositories.Serializers
{
    public class AppraisalSummaryResultSerializer : Serializer<FetchAppraisalQueryResultDto>
    {
        /// <summary>
        /// For case id 28593-performance improvement.clubbing two sp and one inline query
        /// Converts db data to FetchAppraisalQueryResultDto object.
        /// </summary>
        /// <param name="record"></param>
        /// <returns></returns>
        public override FetchAppraisalQueryResultDto Deserialize(IDataRecord record)
        {
            int appraisalId = record.GetInt32("AppraisalID", 0);
            DateTime created = record.GetDateTime(record.GetOrdinal("Created"));
            string vin = record.GetString("VIN");
            string color = record.GetString("Color"); 
            byte appraisalTypeId = record.GetByte(record.GetOrdinal("AppraisalTypeID"));
            int? appraisalAmount = record.GetNullableInt32("AppraisalAmount");

            int modelYear = record.GetInt32(record.GetOrdinal("ModelYear"));
            string makeName = record.GetString("MakeName"); 
            string modelFamilyName = record.GetString("ModelFamilyName");
            string seriesName = record.GetString("SeriesName");

            VehicleDescriptionDto vehicleDescriptionDto=new VehicleDescriptionDto(){
                Vin=vin,
                Color=color,

                ModelYear=modelYear,
                MakeName=makeName,
                ModelFamilyName=modelFamilyName,
                SeriesName=seriesName
            };

            FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.VehicleDto vehicleDto=new FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.VehicleDto(){
            Description=vehicleDescriptionDto
            };
            

            return new FetchAppraisalQueryResultDto
                       {
                           Id = appraisalId,
                           DateCreated = created,
                           AppraisalType = appraisalTypeId == 1 ? AppraisalTypeDto.Trade : AppraisalTypeDto.Purchase,
                           AppraisalAmount = appraisalAmount,
                           Href=null,
                           Vehicle=vehicleDto                          
                       };


        }

    }
}
