﻿using System.Collections.Generic;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Model;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Repositories.Gateways;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Repositories
{
    public class AppraisalRepository : RepositoryBase, IAppraisalRepository
    {
        protected override string DatabaseName
        {
            get { return "Vehicle"; }
        }

        protected override void OnBeginTransaction(IDataSession session)
        {
            // do nothing
        }

        public Appraisal Insert(int clientVehicleId, Appraisal appraisal)
        {
            return DoInTransaction(() => new AppraisalGateway().Insert(clientVehicleId, appraisal));
        }

        public IList<Appraisal> FetchList(int dealerId, int? days)
        {
            return DoInSession(() => new AppraisalGateway().Fetch(dealerId, days));
        }
      
        /// <summary>
        /// For case id 28593-performance improvement.clubbing two sp and one inline query
        /// </summary>
        /// <param name="dealerId"></param>
        /// <param name="days"></param>
        /// <param name="brokerId"></param>
        /// <returns></returns>

        public IList<FetchAppraisalQueryResultDto> FetchList(int dealerId, int? days, int brokerId)
        {
            return DoInSession(() => new AppraisalGateway().Fetch(dealerId, days,brokerId));
        }


    }
}
