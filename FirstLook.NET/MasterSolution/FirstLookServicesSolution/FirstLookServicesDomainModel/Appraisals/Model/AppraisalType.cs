﻿namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Model
{
    public enum AppraisalType
    {
        Undefined = 0,
        Trade = 1,
        Purchase = 2
    }
}
