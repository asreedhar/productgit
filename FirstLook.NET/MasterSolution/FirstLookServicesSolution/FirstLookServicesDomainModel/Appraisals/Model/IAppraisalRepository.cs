﻿using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Model
{
    public interface IAppraisalRepository
    {
        Appraisal Insert(int clientVehicleId, Appraisal appraisal);

        IList<Appraisal> FetchList(int dealerId, int? days);

        /// <summary>
        /// For case id 28593-performance improvement.clubbing two sp and one inline query
        /// </summary>
        /// <param name="dealerid"></param>
        /// <param name="days"></param>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        IList<FetchAppraisalQueryResultDto> FetchList(int dealerid, int? days, int brokerId);
    }
}
