﻿
using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Model
{
    [DataContract]
    public class AppraisalPhoto
    {
        [DataMember(Name = "photoUrl")]
        public string photoURl { get; set; }

        [DataMember(Name = "wide127Url")]
        public string thumbUrl { get; set; }

        [DataMember(Name = "sequenceNo")]
        public int sequenceNo { get; set; }

        [DataMember(Name = "source")]
        public int source { get; set; }
    }
}
