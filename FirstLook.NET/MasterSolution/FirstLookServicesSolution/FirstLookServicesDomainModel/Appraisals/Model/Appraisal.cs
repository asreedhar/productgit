﻿
namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Model
{
    public class Appraisal : AppraisalSummary
    {
        public int Id { get; set; }

        public int? TargetGrossProfit { get; set; }

        public int? EstimatedRecon { get; set; }

        public string ReconNotes { get; set; }

        public int? TargetSellingPrice { get; set; }

        public string User { get; set; }

        public int ClientVehicleId { get; set; }

        public int SalespersonId { get; set; }
        
    }
}
