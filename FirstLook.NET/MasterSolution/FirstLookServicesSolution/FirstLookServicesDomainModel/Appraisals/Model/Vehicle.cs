﻿namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Model
{
    public class Vehicle
    {
        public int Id { get; set; }

        public string Color { get; set; }

        public int? Mileage { get; set; }

        public string Description { get; set; }

        public string Vin { get; set; }
    }
}
