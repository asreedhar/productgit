﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Model
{
    public class AppraisalSummary
    {
        public DateTime DateCreated { get; set; }

        public Vehicle Vehicle { get; set; }

        public AppraisalType AppraisalType { get; set; }

        public int? AppraisalAmount { get; set; }
    }
}
