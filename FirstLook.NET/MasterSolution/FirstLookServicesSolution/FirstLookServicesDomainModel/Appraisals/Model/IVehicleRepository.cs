﻿namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Model
{
    public interface IVehicleRepository
    {
        Vehicle Upsert(int clientVehicleId, Vehicle vehicle);

        Vehicle Fetch(int clientVehicleId);
    }
}
