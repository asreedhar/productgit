﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Model;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.FirstLookServices.DomainModel.Utility;
using FirstLook.FirstLookServices.DomainModel.Validation;
using FirstLook.FirstLookServices.DomainModel.Valuations.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Valuations.Commands.TransferObjects.Envelopes;
using System.Linq;

#region client
using VehicleDto = FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.VehicleDto;
#endregion

#region bb
using BlackBook_PublicationSaveArgumentsDto = FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes.Publications.PublicationSaveArgumentsDto;
#endregion

#region kbb
using KBB_PublicationSaveArgumentsDto = FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes.Publications.PublicationSaveArgumentsDto;
#endregion

#region galves
using Galves_PublicationSaveArgumentsDto = FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes.Publications.PublicationSaveArgumentsDto;
#endregion

#region nada
using Nada_PublicationSaveArgumentsDto = FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes.Publications.PublicationSaveArgumentsDto;
#endregion


namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.Impl
{
    public class SaveAppraisalCommand : ICommand<SaveAppraisalResultsDto, IdentityContextDto<SaveAppraisalArgumentsDto>>
    {
        public SaveAppraisalResultsDto Execute(IdentityContextDto<SaveAppraisalArgumentsDto> parameters)
        {
            #region initialization

            IResolver resolver = RegistryFactory.GetResolver();

            var clientVehicleFactory = RegistryFactory.GetResolver().Resolve<Client.DomainModel.Vehicles.Commands.ICommandFactory>();
            
            var clientClientFactory = RegistryFactory.GetResolver().Resolve<Client.DomainModel.Clients.Commands.ICommandFactory>();

            var kbbFactory = RegistryFactory.GetResolver().Resolve<VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.ICommandFactory>();

            var bbFactory = RegistryFactory.GetResolver().Resolve<VehicleValuationGuide.DomainModel.BlackBook.Commands.ICommandFactory>();

            var nadaFactory = RegistryFactory.GetResolver().Resolve<VehicleValuationGuide.DomainModel.Nada.Commands.ICommandFactory>();

            var galvesFactory = RegistryFactory.GetResolver().Resolve<VehicleValuationGuide.DomainModel.Galves.Commands.ICommandFactory>();

            var bookValFactory = RegistryFactory.GetResolver().Resolve<Valuations.Commands.ICommandFactory>();

            var vehiclesRepository = resolver.Resolve<IVehicleRepository>();

            var appraisalRepository = resolver.Resolve<IAppraisalRepository>();

            IdentityDto identityDto = parameters.Identity;

            #endregion

            #region get broker

            /*
             * Get the broker information from the dealerId. The broker (which is a guid representing the dealer),
             * is needed when calling some existing commands (ie the publicationsave commands in VVG)
             */

            var clientIdentityContext = new IdentityContextDto<BrokerForClientArgumentsDto>
            {
                Identity = identityDto,
                Arguments = new BrokerForClientArgumentsDto
                {
                    AuthorityName = identityDto.AuthorityName,
                    Client = new ClientDto
                    {
                        Id = parameters.Arguments.Appraisal.DealerId
                    }
                }
            };

            ICommand<BrokerForClientResultsDto, IdentityContextDto<BrokerForClientArgumentsDto>> brokerForClientCommand = clientClientFactory.CreateBrokerForClientCommand();

            BrokerForClientResultsDto brokerForClientResultsDto = brokerForClientCommand.Execute(clientIdentityContext);

            if ((brokerForClientResultsDto == null) || (brokerForClientResultsDto.Broker == null))
            {
                throw new InvalidInputException("dealer", "Invalid dealer ID", "null broker for dealer id = " + parameters.Arguments.Appraisal.DealerId);
            }

            BrokerDto brokerDto = brokerForClientResultsDto.Broker;

            #endregion

            #region get client vehicle id

            /*
             * All this code is doing is getting the clientVehicleId for the vehicle.  That is how the vehicle
             * is identified to the client in the Vehicle DB. 
             */

            ICommand<IdentifyVehicleResultsDto, IdentityContextDto<IdentifyVehicleArgumentsDto>> identityVehicleCommand = clientVehicleFactory.CreateIdentifyVehicleCommand();

            if (!this.ValidVin(parameters.Arguments.Appraisal.Vehicle.Vin))
                throw new InvalidInputException("Vin", parameters.Arguments.Appraisal.Vehicle.Vin, "Bad Vin: " + parameters.Arguments.Appraisal.Vehicle.Vin);
            var identifyVehicleArgumentsDto = new IdentifyVehicleArgumentsDto
            {
                Broker = brokerDto.Handle,
                Vin = parameters.Arguments.Appraisal.Vehicle.Vin
            };

            var vehicleIdentifyContext = new IdentityContextDto
                <IdentifyVehicleArgumentsDto>
            {
                Arguments = identifyVehicleArgumentsDto,
                Identity = identityDto
            };

            var identifyVehicleResultsDto = identityVehicleCommand.Execute(vehicleIdentifyContext);

            ICommand<QueryResultsDto, IdentityContextDto<QueryArgumentsDto>> queryCommand = clientVehicleFactory.CreateQueryCommand();

            var queryArgumentsDto = new QueryArgumentsDto
                                        {
                                            Broker = brokerDto.Handle,
                                            Example = new VehicleDescriptionDto
                                                          {
                                                              Vin = parameters.Arguments.Appraisal.Vehicle.Vin,
                                                              BodyTypeName = null,
                                                              DriveTrainName = null,
                                                              EngineName = null,
                                                              FuelTypeName = null,
                                                              MakeName = null,
                                                              ModelFamilyName = null,
                                                              PassengerDoorName = null,
                                                              SegmentName = null,
                                                              SeriesName = null,
                                                              TransmissionName = null
                                                          },
                                            SortColumns = new List<SortColumnDto>
                                                              {
                                                                  new SortColumnDto
                                                                      {
                                                                          Ascending = true,
                                                                          ColumnName = "VIN"
                                                                      }
                                                              },
                                            Actuality = VehicleActualityDto.NotInventory
                                            //TODO: Not sure what to use here.
                                        };


            var queryIdentityContext = new IdentityContextDto<QueryArgumentsDto>
            {
                Arguments = queryArgumentsDto,
                Identity = identityDto
            };

            QueryResultsDto queryResultsDto = queryCommand.Execute(queryIdentityContext);

            VehicleDto vehicleDto = queryResultsDto.Vehicles[0];

            int clientVehicleId = vehicleDto.Id;

            #endregion

            #region verify dealer books
            //todo: we should verify the books specified on the bookvaluationrequests correspond to the dealers allowed books            
            #endregion

            #region verify checksums

            /*
             * Pull out the book valuation requests from the BookEvaluationRequests. Send the list of bookvaluation requests
             * to the FetchBookValuationCommand.  This will return a list of current valuations based on the requests.  Upon
             * returning we'll need to verify that the checksums (hash of valuations returned), match what we expect for
             * each bookvaluationrequest. If they don't match then the appraisal was made based on some outdated valuations 
             * and the appraisal should not be saved.
             */

            var bookValCommand = bookValFactory.CreateFetchBookValuationsCommand();

            IList<BookValuationRequestDto> bookValuationRequestDtos = 
                parameters.Arguments.BookEvaluationRequests
                .Select(evalRequest => evalRequest.BookValuationRequest)
                .ToList();

            var bookValIdentityContext = new IdentityContextDto<FetchBookValuationsArgumentsDto>
                                             {
                                                 Arguments = new FetchBookValuationsArgumentsDto
                                                                 {
                                                                     BookValuationRequests = (List<BookValuationRequestDto>) bookValuationRequestDtos,
                                                                     DealerId = parameters.Arguments.Appraisal.DealerId
                                                                     
                                                                 },
                                                 Identity = identityDto
                                             };

            var bookValResultsDto = bookValCommand.Execute(bookValIdentityContext);

            if (bookValuationRequestDtos.Count != bookValResultsDto.BookValuations.Count)
            {
                //todo: throw more suitable error
                 throw new InvalidOperationException(); 
            }

            foreach (BookValuationDto valuationDto in bookValResultsDto.BookValuations)
            {
                BookEvaluationRequestDto bookEvaluationRequestDto =
                    ((List<BookEvaluationRequestDto>)parameters.Arguments.BookEvaluationRequests).Find(eval => eval.BookValuationRequest.ReturnId == valuationDto.ReturnId);

                if (bookEvaluationRequestDto.CheckSum.CompareTo(valuationDto.CheckSum) != 0)
                {
                    throw new BadValuationChecksumException(bookEvaluationRequestDto.CheckSum);
                }
            }

            #endregion

            #region save vehicle attributes

            /*
             * Fetch the vehicle from the db if we have it.  If not create a new one, merge on attributes
             * from the vehicle that was passed in, and update or insert the vehicle into the db.
             */

            Vehicle vehicle = vehiclesRepository.Fetch(clientVehicleId) ?? new Vehicle();

            Mapper.Merge(vehicle, parameters.Arguments.Appraisal.Vehicle);

            vehicle.Description = identifyVehicleResultsDto.Description.ModelYear + " " +
                                  identifyVehicleResultsDto.Description.MakeName + " " +
                                  identifyVehicleResultsDto.Description.ModelFamilyName + " " +
                                  identifyVehicleResultsDto.Description.SeriesName;

            vehiclesRepository.Upsert(clientVehicleId, vehicle);

            #endregion

            #region save appraisal

            /*
             * Create a new appraisal for the vehicle regardless of one was already there.  This is destructive.
             * If an appraisal was there it will replace it with this one.
             */
            Appraisal appraisal = appraisalRepository.Insert(clientVehicleId, Mapper.ToObj(parameters.Arguments.Appraisal));
            appraisal.SalespersonId = parameters.Arguments.Appraisal.SalespersonId;
            appraisal.Vehicle = vehicle;
            
            #endregion

            //todo: it may make sense to pull this out into a seperate publication validate & save command at some point
            #region save publications

            /*
             * Now that we have passed the validation/verification stages and saved the appraisal and vehicle information
             * we can save the publications.  For each vehicleconfiguration returned from calling the bookvaluation validation
             * we will call a publication save for the respective book... returning the publicationIds generated after 
             * saving to the caller.
             */

            //IList<int> publicationIds = new List<int>();

            foreach (BookValuationDto valuationDto in bookValResultsDto.BookValuations)
            {
                BookEvaluationRequestDto bookEvaluationRequestDto =
                    ((List<BookEvaluationRequestDto>)parameters.Arguments.BookEvaluationRequests)
                    .Find(eval => eval.BookValuationRequest.ReturnId == valuationDto.ReturnId);

                switch (bookEvaluationRequestDto.BookValuationRequest.Book)
                {
                    case "kbb":
                    {
                            var bookValuationRequestDto=parameters.Arguments.BookEvaluationRequests.FirstOrDefault(br => br.BookValuationRequest.Book == "kbb");
                            var kbbPublicationSaveArgumentsDto = new KBB_PublicationSaveArgumentsDto
                                                                     {
                                                                         Broker = brokerDto.Handle,
                                                                         Vehicle = identifyVehicleResultsDto.Vehicle,
                                                                         VehicleConfiguration = valuationDto.KbbVehicleConfiguration,
                                                                         EquipmentSelected = bookValuationRequestDto.BookValuationRequest.EquipmentSelected
                                                                     };

                            var kbbPublicationIdentityContext = new IdentityContextDto<KBB_PublicationSaveArgumentsDto>
                                                                    {
                                                                        Arguments = kbbPublicationSaveArgumentsDto,
                                                                        Identity = identityDto,
                                                                        
                                                                    };

                            var kbbPublicationSaveCommand = kbbFactory.CreatePublicationSaveCommand();

                            //var kbbPublicationSaveResultsDto = kbbPublicationSaveCommand.Execute(kbbPublicationIdentityContext);

                           // publicationIds.Add(kbbPublicationSaveResultsDto.Publication.Id);

                            break;
                        }
                    //todo: spec says bb, but api returns blackbook as code
                    case "blackbook":
                        {
                            var bbPublicationSaveArgumentsDto = new BlackBook_PublicationSaveArgumentsDto
                                                                    {
                                                                        Broker = brokerDto.Handle,
                                                                        Vehicle = identifyVehicleResultsDto.Vehicle,
                                                                        VehicleConfiguration = valuationDto.BlackBookVehicleConfiguration
                                                                    };

                            var bbPublicationIdentityContext = new IdentityContextDto<BlackBook_PublicationSaveArgumentsDto>
                            {
                                Arguments = bbPublicationSaveArgumentsDto,
                                Identity = identityDto
                            };

                            var bbPublicationSaveCommand = bbFactory.CreatePublicationSaveCommand();

                            var bbPublicationSaveResultsDto = bbPublicationSaveCommand.Execute(bbPublicationIdentityContext);

                            //publicationIds.Add(bbPublicationSaveResultsDto.Publication.Id);

                            break;
                        }
                    case "nada":
                        {
                            var nadaPublicationSaveArgumentsDto = new Nada_PublicationSaveArgumentsDto
                                                                      {
                                                                          Broker = brokerDto.Handle,
                                                                          Vehicle = identifyVehicleResultsDto.Vehicle,
                                                                          VehicleConfiguration = valuationDto.NadaVehicleConfiguration
                                                                      };

                            var nadaPublicationIdentityContext = new IdentityContextDto<Nada_PublicationSaveArgumentsDto>
                                                                     {
                                                                         Arguments = nadaPublicationSaveArgumentsDto,
                                                                         Identity = identityDto
                                                                     };

                            var nadaPublicationSaveCommand = nadaFactory.CreatePublicationSaveCommand();

                            var nadaPublicationSaveResultsDto = nadaPublicationSaveCommand.Execute(nadaPublicationIdentityContext);

                            //publicationIds.Add(nadaPublicationSaveResultsDto.Publication.Id);

                            break;
                        }
                    case "galves":
                        {
                            var galvesPublicationSaveArgumentsDto = new Galves_PublicationSaveArgumentsDto
                                                                        {
                                                                            Broker = brokerDto.Handle,
                                                                            Vehicle = identifyVehicleResultsDto.Vehicle,
                                                                            VehicleConfiguration = valuationDto.GalvesVehicleConfiguration
                                                                        };

                            var galvesPublicationIdentityContext = new IdentityContextDto<Galves_PublicationSaveArgumentsDto>
                                                                       {
                                                                           Arguments = galvesPublicationSaveArgumentsDto,
                                                                           Identity = identityDto
                                                                       };

                            var galvesPublicationSaveCommand = galvesFactory.CreatePublicationSaveCommand();

                            var galvesPublicationSaveResultsDto = galvesPublicationSaveCommand.Execute(galvesPublicationIdentityContext);

                           // publicationIds.Add(galvesPublicationSaveResultsDto.Publication.Id);

                            break;
                        }                    
                }
            }

            #endregion

            var resultsDto = new SaveAppraisalResultsDto
                                 {
                                     AppraisalSummary = Mapper.ToDto(appraisal),
                                     BookValuations = bookValResultsDto.BookValuations,
                                     //PublicationIds = (List<int>) publicationIds
                                 };

            //return saved appraisal summary, verified book valuations, and list of publicationIds generated when publication was saved.)
            return resultsDto;
        }
    }
}
