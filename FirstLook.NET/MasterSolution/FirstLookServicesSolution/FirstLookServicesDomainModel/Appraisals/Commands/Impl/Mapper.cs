﻿using System.Collections.Generic;
using System.Configuration;
using FirstLook.Common.Core.Extensions;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Model;
using FirstLook.FirstLookServices.DomainModel.Valuations.Commands.TransferObjects;
using System;

namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.Impl
{
    public static class Mapper
    {
        #region appraisal

        public static Appraisal ToObj(AppraisalDto dto)
        {
            if (dto == null)
            {
                return null;
            }

            return new Appraisal
            {
                AppraisalType = ToObj(dto.AppraisalType),
                User = dto.User,
                AppraisalAmount = dto.AppraisalAmount,
                EstimatedRecon = dto.EstimatedRecon,
                ReconNotes = dto.ReconNotes,
                TargetGrossProfit = dto.TargetGrossProfit,
                TargetSellingPrice = dto.TargetSellingPrice,
                Vehicle = ToObj(dto.Vehicle)
            };
        }

        public static AppraisalSummaryDto ToDto(Appraisal obj)
        {
            if (obj == null)
            {
                return null;
            }

            VehicleDto vehicleDto = null;

            if (obj.Vehicle != null)
            {
                vehicleDto = new VehicleDto
                {
                    Color = obj.Vehicle.Color,
                    Mileage = obj.Vehicle.Mileage,
                    Description = obj.Vehicle.Description,
                    Vin = obj.Vehicle.Vin
                };
            }

            return new AppraisalSummaryDto
            {
                AppraisalAmount = obj.AppraisalAmount,
                AppraisalType = ToDto(obj.AppraisalType),
                Id = obj.Id,
                DateCreated = obj.DateCreated,
                User = obj.User,
                Vehicle = vehicleDto,
                SalespersonId = obj.SalespersonId,
                ReconNotes = obj.ReconNotes
            };

        }

        public static AppraisalType ToObj(AppraisalTypeDto dto)
        {
            switch (dto)
            {
                case AppraisalTypeDto.Purchase:
                    {
                        return AppraisalType.Purchase;
                    }
                case AppraisalTypeDto.Trade:
                    {
                        return AppraisalType.Trade;
                    }
                default:
                    {
                        return AppraisalType.Undefined;
                    }
            }
        }

        public static AppraisalTypeDto ToDto(AppraisalType obj)
        {
            switch (obj)
            {
                case AppraisalType.Purchase:
                    {
                        return AppraisalTypeDto.Purchase;
                    }
                case AppraisalType.Trade:
                    {
                        return AppraisalTypeDto.Trade;
                    }
                default:
                    return AppraisalTypeDto.Undefined;
            }
        }

        public static string ToJson(SaveAppraisalArgumentsDto args, SaveAppraisalResultsDto results)
        {            
            var appraisalDocument = new Dictionary<string, object>();

            var appraisalRequest = new Dictionary<string, object>();

            var bookValuationRequests = new List<Dictionary<string, object>>();

            var bookValuations = new List<Dictionary<string, object>>();

            var photoUrlList = new List<Dictionary<string, string>>();

            appraisalDocument.Add("appraisalId", results.AppraisalSummary.Id);

            appraisalDocument.Add("publicationIds", results.PublicationIds);

            appraisalDocument.Add("href", results.AppraisalSummary.Href);

            appraisalDocument.Add("dateCreated", results.AppraisalSummary.DateCreated.ToUniversalTime().ToString("yyyyMMdd'T'HHmmss'Z'"));

            appraisalDocument.Add("user", results.AppraisalSummary.User);

            appraisalRequest.Add("vin", args.Appraisal.Vehicle.Vin);

            appraisalRequest.Add("dealer", args.Appraisal.DealerId);

            appraisalRequest.Add("appraisalAmount", args.Appraisal.AppraisalAmount);

            appraisalRequest.Add("appraisalType", args.Appraisal.AppraisalType == AppraisalTypeDto.Trade ? "T" : "P");

            appraisalRequest.Add("color", args.Appraisal.Vehicle.Color);

            appraisalRequest.Add("targetGrossProfit", args.Appraisal.TargetGrossProfit);

            appraisalRequest.Add("estimatedRecon", args.Appraisal.EstimatedRecon);

            if( null != args.Appraisal.MmrVehicle)//BUGZID: 27826
                appraisalRequest.Add("mmrVehicle", args.Appraisal.MmrVehicle);//BUGZID: 27826

            appraisalRequest.Add("targetSellingPrice", args.Appraisal.TargetSellingPrice);

            appraisalRequest.Add("salespersonId", args.Appraisal.SalespersonId);

            appraisalRequest.Add("reconNotes", args.Appraisal.ReconNotes);

            if (results.PhotoUrl != null)
            {
                foreach (var photoUrl in results.PhotoUrl)
                {
                    Dictionary<string, string> photos = new Dictionary<string, string>();
                    photos.Add("photoUrl", photoUrl.photoURl);
                    photos.Add("wide127Url", photoUrl.thumbUrl);
                    photos.Add("sequenceNo", Convert.ToString(photoUrl.sequenceNo));
                    photoUrlList.Add(photos);
                }
                if (photoUrlList.Count > 0)
                {
                    appraisalRequest.Add("photoUrl", photoUrlList);
                    appraisalRequest.Add("uniqueId", args.Appraisal.UniqueId);
                }
            }
            

            foreach (BookEvaluationRequestDto bookEvaluationRequestDto in args.BookEvaluationRequests)
            {
                var bookEvalRequest = new Dictionary<string, object>();

                var request = new Dictionary<string, object>();

                request.Add("book", bookEvaluationRequestDto.BookValuationRequest.Book);

                request.Add("mileage", bookEvaluationRequestDto.BookValuationRequest.Mileage);

                request.Add("trimId", bookEvaluationRequestDto.BookValuationRequest.TrimId);

                request.Add("returnId", bookEvaluationRequestDto.BookValuationRequest.ReturnId);

                request.Add("equipmentSelected", bookEvaluationRequestDto.BookValuationRequest.EquipmentSelected);

                //Yeah, we need to refactor the action forcing this - Erik - 5/14/2013
                var equipList = new List<Dictionary<string, string>>();
                foreach (var action in bookEvaluationRequestDto.BookValuationRequest.EquipmentUserActions)
                {
                    string actionName;
                    switch (action.Action)
                    {
                        case EquipmentActionTypeDto.Add:
                            actionName = "A";
                            break;
                        case EquipmentActionTypeDto.Remove:
                            actionName = "R";
                            break;
                        default:
                            actionName = action.Action.ToString();
                            break;
                    }
                    equipList.Add(
                        new Dictionary<string, string>()
                        {
                            { "action", actionName },
                            { "equipment", action.Equipment } 
                        }
                    );

                }
                
                request.Add("equipmentExplicitActions", equipList);

                bookEvalRequest.Add("bookValuationRequest", request);

                bookEvalRequest.Add("checkSum", bookEvaluationRequestDto.CheckSum);

                bookValuationRequests.Add(bookEvalRequest);
            }

            appraisalRequest.Add("bookValuationRequests", bookValuationRequests);

            appraisalDocument.Add("appraisalRequest", appraisalRequest);

            foreach (BookValuationDto bookValuationDto in results.BookValuations)
            {
                var valuation = new Dictionary<string, object>();

                var valuationData = new List<Dictionary<string, object>>();

                valuation.Add("returnId", bookValuationDto.ReturnId);

                valuation.Add("checkSum", bookValuationDto.CheckSum);

                foreach (ValuationDataDto valuationDataDto in bookValuationDto.ValuationData)
                {
                    var marketData = new Dictionary<string, object>();

                    var valuations = new List<Dictionary<string, object>>();

                    marketData.Add("market", valuationDataDto.Market);

                    marketData.Add("condition", valuationDataDto.Condition);

                    foreach (ValuationDto valuationDto in valuationDataDto.Valuations)
                    {
                        var marketVal = new Dictionary<string, object>();

                        marketVal.Add("desc", valuationDto.Description);
                        
                        marketVal.Add("value", valuationDto.Value);

                        valuations.Add(marketVal);
                    }

                    marketData.Add("valuations", valuations);
  
                    valuationData.Add(marketData);
                }

                valuation.Add("valuationData", valuationData);

                bookValuations.Add(valuation);
            }

            appraisalDocument.Add("valuations", bookValuations);

            return appraisalDocument.ToJson();
        }

        //todo:   i'm not happy with having this here... revisit?
        /// <summary>
        /// construct the amazon s3 href.
        /// </summary>
        /// <param name="appraisalId"></param>
        /// <returns></returns>
        public static string ConstructS3Href(int appraisalId)
        {
            string bucket = ConfigurationManager.AppSettings["Bucket"];

            return "http://s3.amazonaws.com/" + bucket + "/" + appraisalId;
        }

        #endregion

        #region vehicle

        public static Vehicle ToObj(VehicleDto dto)
        {
            if (dto == null)
            {
                return null;
            }

            return new Vehicle
            {
                Color = dto.Color,
                Mileage = dto.Mileage,
                Description = dto.Description,
                Vin = dto.Vin
            };
        }

        public static Vehicle Merge(Vehicle merged, VehicleDto toPersist)
        {
            if (toPersist == null)
            {
                return merged;
            }

            if (toPersist.Color != null)
            {
                merged.Color = toPersist.Color;
            }

            if (toPersist.Mileage != null)
            {
                merged.Mileage = toPersist.Mileage;
            }

            if (toPersist.Vin != null)
            {
                merged.Vin = toPersist.Vin;
            }

            return merged;
        }

        #endregion

    }
}
