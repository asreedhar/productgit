﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Model;
using VehicleDto = FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.VehicleDto;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.FirstLookServices.DomainModel.Validation;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using System.Linq;



namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.Impl
{
    public class FetchAppraisalsCommand : ICommand<FetchAppraisalsResultsDto, IdentityContextDto<FetchAppraisalsArgumentsDto>>
    {
        public FetchAppraisalsResultsDto Execute(IdentityContextDto<FetchAppraisalsArgumentsDto> parameters)
        {

            Validators.CheckDealerId(parameters.Arguments.DealerId, parameters);

            #region initialization

            IResolver resolver = RegistryFactory.GetResolver();

            var clientVehicleFactory = RegistryFactory.GetResolver().Resolve<Client.DomainModel.Vehicles.Commands.ICommandFactory>();

            var clientClientFactory = RegistryFactory.GetResolver().Resolve<Client.DomainModel.Clients.Commands.ICommandFactory>();

            var vehiclesRepository = resolver.Resolve<IVehicleRepository>();

            var appraisalRepository = resolver.Resolve<IAppraisalRepository>();

            IdentityDto identityDto = parameters.Identity;

            #endregion

            #region get broker

            /*
             * Get the broker information from the dealerId. The broker (which is a guid representing the dealer),
             * is needed when calling some existing commands (ie the publicationsave commands in VVG)
             */

            var clientIdentityContext = new IdentityContextDto<BrokerForClientArgumentsDto>
            {
                Identity = identityDto,
                Arguments = new BrokerForClientArgumentsDto
                {
                    AuthorityName = identityDto.AuthorityName,
                    Client = new ClientDto
                    {
                        Id = parameters.Arguments.DealerId
                    }
                }
            };

            ICommand<BrokerForClientResultsDto, IdentityContextDto<BrokerForClientArgumentsDto>> brokerForClientCommand = clientClientFactory.CreateBrokerForClientCommand();

            BrokerForClientResultsDto brokerForClientResultsDto = brokerForClientCommand.Execute(clientIdentityContext);

            if ((brokerForClientResultsDto == null) || (brokerForClientResultsDto.Broker == null))
            {
                throw new InvalidInputException("dealer", "Invalid dealer ID", "null broker for dealer id = " + parameters.Arguments.DealerId);
            }

            BrokerDto brokerDto = brokerForClientResultsDto.Broker;

            #endregion           

            IBroker broker = Broker.Get(identityDto.Name, identityDto.AuthorityName, brokerDto.Handle);
            
            IList<FetchAppraisalQueryResultDto> appraisals = appraisalRepository.FetchList(parameters.Arguments.DealerId,
                                                                  parameters.Arguments.Days, broker.Id);
           
            if (appraisals.Count == 0)
            {
                throw new NoDataFoundException("Appraisal list count was 0");
            }

            IList<AppraisalSummaryDto> appraisalSummaryDtos = new List<AppraisalSummaryDto>();
            // To create the response object
            foreach (FetchAppraisalQueryResultDto appraisal in appraisals)
            {
                AppraisalSummaryDto appraisalSummaryDto = new AppraisalSummaryDto();
                appraisalSummaryDto.Vehicle = new TransferObjects.VehicleDto();
                appraisalSummaryDto.Vehicle.Description = appraisal.Vehicle.Description.ModelYear + " " +
                                                   appraisal.Vehicle.Description.MakeName + " " +
                                                   appraisal.Vehicle.Description.ModelFamilyName + " " +
                                                   appraisal.Vehicle.Description.SeriesName;


                appraisalSummaryDto.Href = Mapper.ConstructS3Href(appraisal.Id);

                appraisalSummaryDto.Vehicle.Vin = appraisal.Vehicle.Description.Vin;

                appraisalSummaryDto.DateCreated = appraisal.DateCreated;

                appraisalSummaryDto.Vehicle.Color = appraisal.Vehicle.Description.Color;

                appraisalSummaryDto.AppraisalType = appraisal.AppraisalType;

                appraisalSummaryDto.AppraisalAmount = appraisal.AppraisalAmount;


                appraisalSummaryDtos.Add(appraisalSummaryDto);

               
            }           

            return new FetchAppraisalsResultsDto
            {
                AppraisalSummaries = appraisalSummaryDtos.ToList().OrderBy(asd => asd.Vehicle.Vin).ToList()
            };         

         
          
        }
    }
}
