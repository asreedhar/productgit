﻿using System;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.Impl
{
    class CommandFactory : ICommandFactory
    {
        public ICommand<SaveAppraisalResultsDto, IdentityContextDto<SaveAppraisalArgumentsDto>> CreateSaveAppraisalCommand()
        {
            return new SaveAppraisalCommand();
        }

        public ICommand<SaveAppraisalResultsDto, IdentityContextDto<SaveAppraisalArgumentsDto>> CreateSaveAndPostAppraisalCommand()
        {
            return new SaveAndPostAppraisalCommand();
        }

        public ICommand<FetchAppraisalsResultsDto, IdentityContextDto<FetchAppraisalsArgumentsDto>> CreateFetchAppraisalsCommand()
        {
            return new FetchAppraisalsCommand();
        }

        public ICommand<FetchAppraisalTypesResultsDto, FetchAppraisalTypesArgumentsDto> CreateFetchAppraisalTypesCommand()
        {
            return new FetchAppraisalTypesCommand();
        }
    }
}
