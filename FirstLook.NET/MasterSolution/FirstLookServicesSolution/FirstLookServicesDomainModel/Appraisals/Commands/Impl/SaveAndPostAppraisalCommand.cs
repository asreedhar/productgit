﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.SQS;
using Amazon.SQS.Model;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Model;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.FirstLookServices.DomainModel.Utility;
using IResolver = FirstLook.Common.Core.Registry.IResolver;


namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.Impl
{
    public class SaveAndPostAppraisalCommand : ICommand<SaveAppraisalResultsDto, IdentityContextDto<SaveAppraisalArgumentsDto>>
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public SaveAppraisalResultsDto Execute(IdentityContextDto<SaveAppraisalArgumentsDto> parameters)
        {
            if (parameters.Arguments.Appraisal.AppraisalType == AppraisalTypeDto.Trade || parameters.Arguments.Appraisal.AppraisalType == AppraisalTypeDto.Purchase)
            {
                HasAppraisalOrInventory hasAppOrInv =
                    CheckIfAppraisalOrInventoryExists(parameters.Arguments.Appraisal.Vehicle.Vin,
                                                      parameters.Arguments.Appraisal.DealerId);
                if (hasAppOrInv.HasAppraisal)
                {
                    throw new DupApprOrInvException(
                        "FirstLook has an existing appraisal for this vehicle from your dealership.  Please view the existing appraisal in the desktop.");
                }

                if (hasAppOrInv.HasInventory)
                {
                    throw new DupApprOrInvException(
                        "This vehicle is already in your inventory.  Please view the appraisal in the desktop version of FirstLook");
                }
            }

            if (parameters.Arguments.BookEvaluationRequests.Count < 1)
            {
                throw new FirstlookGeneralException("Error: This Appraisal request has no Book data.");
            }


            IResolver resolver = RegistryFactory.GetResolver();

            //Save appraisal
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            var saveAppraisalCommand = factory.CreateSaveAppraisalCommand();

            SaveAppraisalResultsDto saveResults = saveAppraisalCommand.Execute(parameters);
            if (!String.IsNullOrEmpty(parameters.Arguments.Appraisal.UniqueId))
                saveResults.PhotoUrl = GetAppraisalPhotos(parameters.Arguments.Appraisal.UniqueId, parameters.Arguments.Appraisal.DealerId);

            if (saveResults.PhotoUrl != null && saveResults.PhotoUrl.Count > 0)
                saveResults.UniqueId = parameters.Arguments.Appraisal.UniqueId;

            //Save off href to json object in S3
            saveResults.AppraisalSummary.Href = Mapper.ConstructS3Href(saveResults.AppraisalSummary.Id);

            //Generate appraisal document to ship to S3
            var appraisalDocument = Mapper.ToJson(parameters.Arguments, saveResults);





            // *************Save the message to the S3 Bucket and S3 queue**********************

            string bucketName = ConfigurationManager.AppSettings["Bucket"];

            // Get the S3 client for our region.
            IAmazonS3 s3Client = Amazon.AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.USEast1);

            try
            {
                // simple object put
                PutObjectRequest s3Request = new PutObjectRequest() { ContentBody=appraisalDocument,BucketName=bucketName,Key=saveResults.AppraisalSummary.Id.ToString() };


                PutObjectResponse response = s3Client.PutObject(s3Request);
                Log.Info("Successfully wrote appraisal id = " + saveResults.AppraisalSummary.Id + " to S3 Bucket = " + bucketName);
               
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null && (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") || amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    Log.Error("Error Writing Appraisal to S3 Bucket - " + bucketName + ":  Incorrect AWS Credentials");
                    throw new UserAccessException("AWS", "Forbidden", "Incorrect AWS Credentials.");
                }
                else
                {
                    Log.Error("Exception Writing Appraisal to S3 Bucket - " + bucketName, amazonS3Exception);
                    throw new FirstlookGeneralException("Errorcode " + amazonS3Exception.ErrorCode + " when writing to S3 Bucket: " + bucketName + " - Error message = " + amazonS3Exception.Message);
                }
            }


            if (parameters.Arguments.Appraisal.AppraisalType == AppraisalTypeDto.Trade || parameters.Arguments.Appraisal.AppraisalType == AppraisalTypeDto.Purchase)
            {
                string queueName = ConfigurationManager.AppSettings["QueueName"];

                // Get the queue corresponding to the queue name
                try
                {


                    // Get the Amazon SQS client for our region.
                    IAmazonSQS sqsClient = AWSClientFactory.CreateAmazonSQSClient(RegionEndpoint.USEast1);

                    GetQueueUrlRequest queueRequest = new GetQueueUrlRequest();
                    queueRequest.QueueName = queueName;
                    GetQueueUrlResponse getQueuesResponse = sqsClient.GetQueueUrl(queueRequest);
                    string queueUrl = getQueuesResponse.QueueUrl;

                    // Send the message to the queue.
                    SendMessageRequest sendMessageRequest = new SendMessageRequest();
                    sendMessageRequest.QueueUrl = queueUrl; //URL from initial queue creation
                    sendMessageRequest.MessageBody = appraisalDocument;
                    sqsClient.SendMessage(sendMessageRequest);
                    Log.Info("Successfully wrote appraisal id = " + saveResults.AppraisalSummary.Id + " to SQS Queue = " + queueName);
                }
                catch (Exception ex)
                {
                    Log.Error("Exception Writing Appraisal to SQS Queue - " + queueName, ex);
                    throw new FirstlookGeneralException("Error writing to S3 Queue: " + queueName +
                                                        " - Error message is " + ex.Message);
                }
            }

            //Return saved appraisal info);)
            return saveResults;
        }

        /**
         * returns a boolean array - [0] = true if an appraisal for this vin and buid exists; [1] = true if an inventory item exists.
         */
        private static HasAppraisalOrInventory CheckIfAppraisalOrInventoryExists(string vin, int buid)
        {
            var parameters = new Dictionary<string, object>() { { "vin", vin }, { "buid", buid } };

            var hasApp = false;
            var hasInv = false;

            var dpc = new DbProcCaller();

            try
            {
                dpc.CallProc("IMT", "dbo.AppraisalOrInventory#Exists", parameters);
                while (dpc.Reader.Read())
                {
                    if (dpc.Reader.GetBoolean(dpc.Reader.GetOrdinal("appraisalExists")))
                    {
                        hasApp = true;
                    }

                    if (dpc.Reader.GetBoolean(dpc.Reader.GetOrdinal("inventoryExists")))
                    {
                        hasInv = true;
                    }
                }
            }
            finally
            {
                dpc.Close();
            }

            return new HasAppraisalOrInventory { HasAppraisal = hasApp, HasInventory = hasInv };
        }

        private List<AppraisalPhoto> GetAppraisalPhotos(string uniqueID,int dealerID)
        {
            string bucketName = ConfigurationManager.AppSettings["AppraisalPhotoBucket"];
            int maxPhotoLength = Convert.ToInt32(ConfigurationManager.AppSettings["maxPhotoLength"]);

            List<string> appraisalPhotokeyList = new List<string>();
            List<AppraisalPhoto> appraisalPhotos = new List<AppraisalPhoto>();

            using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.USEast1))
            {
                ListObjectsRequest request = new ListObjectsRequest
                {
                    BucketName = bucketName,
                    Prefix =  string.Format("{0}_{1}", uniqueID,dealerID),
                    MaxKeys = maxPhotoLength
                };

                do
                {
                    ListObjectsResponse response = s3Client.ListObjects(request);

                    foreach (S3Object entry in response.S3Objects)
                    {
                        appraisalPhotokeyList.Add(entry.Key);
                    }

                    // If response is truncated, set the marker to get the next 
                    // set of keys.
                    if (response.IsTruncated)
                    {
                        request.Marker = response.NextMarker;
                    }
                    else
                    {
                        request = null;
                    }
                } while (request != null);

                appraisalPhotokeyList.Sort();

                for (int i = 0; i < appraisalPhotokeyList.Count; i += 2)
                {
                    int sequenceNo =
                        AppraisalPhotos.Commands.Impl.Helper.GetSequenceIndex(
                            appraisalPhotokeyList[i]);
                    appraisalPhotos.Add(new AppraisalPhoto() { photoURl = appraisalPhotokeyList[i], thumbUrl = appraisalPhotokeyList[i + 1], sequenceNo = sequenceNo });
                }
            }
            return appraisalPhotos;
        }

    }

    class HasAppraisalOrInventory
    {
        public bool HasAppraisal { get; set; }
        public bool HasInventory { get; set; }
    }
}
