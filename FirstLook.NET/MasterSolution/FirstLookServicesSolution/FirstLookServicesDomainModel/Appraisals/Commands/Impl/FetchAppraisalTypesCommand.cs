﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects.Envelopes;


namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.Impl
{
    public class FetchAppraisalTypesCommand : ICommand<FetchAppraisalTypesResultsDto, FetchAppraisalTypesArgumentsDto>
    {
        public FetchAppraisalTypesResultsDto Execute(FetchAppraisalTypesArgumentsDto parameters)
        {
            #region initialization

            IList<string> types = new List<string>(Enum.GetNames(typeof(AppraisalTypeDto)));

            //the appraisal types supported is based on the appraisaltypeEnum... however we don't want to broadcast we support an undefined appraisal type
            //... so i'm filtering that element from the list before returning
            types = ((List<string>) types).FindAll(s => !s.Equals("Undefined"));

            #endregion

            return new FetchAppraisalTypesResultsDto
                       {
                           AppraisalTypes = (List<string>) types
                       };
        }
    }
}
