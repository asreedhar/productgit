﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Commands
{
    public interface ICommandFactory
    {
        ICommand<SaveAppraisalResultsDto, IdentityContextDto<SaveAppraisalArgumentsDto>> CreateSaveAppraisalCommand();

        ICommand<SaveAppraisalResultsDto, IdentityContextDto<SaveAppraisalArgumentsDto>> CreateSaveAndPostAppraisalCommand();

        ICommand<FetchAppraisalsResultsDto, IdentityContextDto<FetchAppraisalsArgumentsDto>> CreateFetchAppraisalsCommand();

        ICommand<FetchAppraisalTypesResultsDto, FetchAppraisalTypesArgumentsDto> CreateFetchAppraisalTypesCommand();

    }
}
