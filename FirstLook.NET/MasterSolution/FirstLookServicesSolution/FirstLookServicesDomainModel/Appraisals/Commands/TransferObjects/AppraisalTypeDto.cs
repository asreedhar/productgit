﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects
{
    [Serializable]
    public enum AppraisalTypeDto
    {
        Undefined = 0,
        Trade = 1,
        Purchase = 2
    }
}
