﻿using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects.Envelopes
{
    public class FetchAppraisalTypesResultsDto
    {
        public List<string> AppraisalTypes { get; set; }
    }
}
