﻿using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects.Envelopes
{
    public class FetchAppraisalsResultsDto
    {
        public IList<AppraisalSummaryDto> AppraisalSummaries { get; set; }
    }
}
