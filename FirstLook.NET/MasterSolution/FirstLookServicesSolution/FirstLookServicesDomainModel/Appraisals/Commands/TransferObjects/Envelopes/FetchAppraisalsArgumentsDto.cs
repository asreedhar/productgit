﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class FetchAppraisalsArgumentsDto
    {
        public int DealerId { get; set; }

        public int? Days { get; set; }
    }
}
