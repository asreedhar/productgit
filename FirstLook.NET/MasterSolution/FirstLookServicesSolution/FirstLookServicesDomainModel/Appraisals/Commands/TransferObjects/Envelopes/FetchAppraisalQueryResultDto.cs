﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class FetchAppraisalQueryResultDto
    {
        
        /// <summary>
        /// For case id 28593-performance improvement.clubbing two sp and one inline query
        /// </summary>
        public FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.VehicleDto Vehicle { get; set; }

        public int Id { get; set; }

        public DateTime DateCreated { get; set; }

        public AppraisalTypeDto AppraisalType { get; set; }

        public int? AppraisalAmount { get; set; }       
        
        public string Href { get; set; }


    }
}
