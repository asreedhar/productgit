﻿using System;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class SaveAppraisalArgumentsDto
    {
        public AppraisalDto Appraisal { get; set; }

        public IList<BookEvaluationRequestDto> BookEvaluationRequests { get; set; }
    }
}
