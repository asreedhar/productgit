﻿using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Model;
using FirstLook.FirstLookServices.DomainModel.Valuations.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects.Envelopes
{
    public class SaveAppraisalResultsDto
    {
        public AppraisalSummaryDto AppraisalSummary { get; set; }

        public List<BookValuationDto> BookValuations { get; set; }

        public List<int> PublicationIds { get; set; }

        public List<AppraisalPhoto> PhotoUrl { get; set; }

        public string UniqueId { get; set; }
    }
}
