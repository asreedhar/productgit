﻿using System;
using FirstLook.FirstLookServices.DomainModel.Valuations.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects
{
    [Serializable]
    public class BookEvaluationRequestDto
    {
        public BookValuationRequestDto BookValuationRequest { get; set;}

        public string CheckSum { get; set; } 
    }
}
