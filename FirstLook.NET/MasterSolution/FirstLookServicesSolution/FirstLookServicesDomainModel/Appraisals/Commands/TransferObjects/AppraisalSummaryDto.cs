﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects
{
    [Serializable]
    public class AppraisalSummaryDto
    {
        public int Id { get; set; }

        public DateTime DateCreated { get; set; }

        public VehicleDto Vehicle { get; set; }

        public AppraisalTypeDto AppraisalType { get; set; }

        public int? AppraisalAmount { get; set; }

        public string User { get; set; }

        public int SalespersonId { get; set; }

        public string ReconNotes { get; set; }

        //Only used if SaveAndPostAppraisalCommand is executed
        public string Href { get; set; }
    }
}
