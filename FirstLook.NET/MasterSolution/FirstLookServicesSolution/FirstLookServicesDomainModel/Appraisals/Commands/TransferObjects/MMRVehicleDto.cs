using System;

namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects
{
    [Serializable]
    public class MMRVehicleDto
    {
        public string area { get; set; } 
        public string trimId { get; set; }
    }
}
