﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects
{
    [Serializable]
    public class VehicleDto
    {
        public string Color { get; set; }

        public int? Mileage { get; set; }

        public string Description { get; set; }

        public string Vin { get; set; }
    }
}
