﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects
{
    [Serializable]
    public class AppraisalDto
    {
        public int AppraisalAmount { get; set; }

        public int TargetGrossProfit { get; set; } 
            
        public int EstimatedRecon { get; set; }

        public int TargetSellingPrice { get; set; }

        public AppraisalTypeDto AppraisalType { get; set; }

        public VehicleDto Vehicle { get; set; }

        public string User { get; set; }

        public int DealerId { get; set; }

        public MMRVehicleDto MmrVehicle { get; set; }

        public int SalespersonId { get; set; }

        public string UniqueId { get; set; }

        public string ReconNotes { get; set; }
    }
}
