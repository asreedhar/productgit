using System;
using System.Linq;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel
{

    /// <summary>
    /// A singleton book factory which exposes all known books.
    /// </summary>
    public sealed class BookFactory
    {
        private static BookInstance _kbb;
        private static BookInstance _nada;
        private static BookInstance _galves;
        private static BookInstance _blackBook;
        private static IList<IBook> _books; 

        // setup the singleton
        private static readonly Lazy<BookFactory> Factory = new Lazy<BookFactory>( () => new BookFactory() );

        /// <summary>
        /// Singleton instance of Books
        /// </summary>
        public static BookFactory Instance { get { return Factory.Value; } }

        // singleton constructor
        private BookFactory()
        {
            _blackBook = new BookInstance(BookId.BlackBook, "blackbook", "BlackBook");
            _nada = new BookInstance(BookId.Nada, "nada", "NADA");
            _kbb = new BookInstance(BookId.Kbb, "kbb", "Kelley Blue Book");
            _galves = new BookInstance(BookId.Galves, "galves", "GALVES");

            _books = new List<IBook> { _blackBook, _nada, _kbb, _galves };
        }

        /// <summary>
        /// Return known books.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IBook> GetBooks()
        {
            return _books;
        }

        /// <summary>
        /// Get a book by BookId.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IBook Get(BookId id)
        {
            return _books.FirstOrDefault(b => b.Id == id);
        }

        private sealed class BookInstance : IBook
        {
            internal BookInstance(BookId id, string desc, string name)
            {
                Id = id;
                Desc = desc;
                Name = name;
            }

            public BookId Id { get; private set; }
            public string Desc { get; private set; }
            public string Name { get; private set; }
        }

    }

    /// <summary>
    /// These are the known datapoints about a book.
    /// </summary>
    public interface IBook
    {
        BookId Id { get; }
        string Desc { get; }
        string Name { get; }
    }

    /// <summary>
    /// These values should agree with IMT.dbo.ThirdParties.ThirdPartyId
    /// </summary>
    public enum BookId
    {
        BlackBook = 1,
        Nada = 2,
        Kbb = 3,
        Galves = 4
    }

    

}