﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.Reports.Commands.Impl.AutoCheck;
using FirstLook.FirstLookServices.DomainModel.Reports.Commands.Impl.Carfax;
using FirstLook.FirstLookServices.DomainModel.Reports.Commands.TransferObjects.Envelopes.AutoCheck;
using FirstLook.FirstLookServices.DomainModel.Reports.Commands.TransferObjects.Envelopes.Carfax;

namespace FirstLook.FirstLookServices.DomainModel.Reports.Commands.Impl
{
    class CommandFactory : ICommandFactory
    {
        public ICommand<FetchAutoCheckReportResultsDto, IdentityContextDto<FetchAutoCheckReportArgumentsDto>> CreateFetchAutoCheckReportCommand()
        {
            return new FetchAutoCheckReportCommand();
        }

        public ICommand<FetchCarfaxReportResultsDto, IdentityContextDto<FetchCarfaxReportArgumentsDto>> CreateFetchCarfaxReportCommand()
        {
            return new FetchCarfaxReportCommand();
        }

        public ICommand<PurchaseAutoCheckReportResultsDto, IdentityContextDto<PurchaseAutoCheckReportArgumentsDto>> CreatePurchaseAutoCheckReportCommand()
        {
            return new PurchaseAutoCheckReportCommand();
        }

        public ICommand<PurchaseCarfaxReportResultsDto, IdentityContextDto<PurchaseCarfaxReportArgumentsDto>> CreatePurchaseCarfaxReportCommand()
        {
            return new PurchaseCarfaxReportCommand();
        }
    }
}
