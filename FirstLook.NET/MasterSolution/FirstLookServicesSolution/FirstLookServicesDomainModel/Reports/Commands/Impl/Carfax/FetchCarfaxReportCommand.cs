﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Reports.Commands.TransferObjects.Carfax;
using FirstLook.FirstLookServices.DomainModel.Reports.Commands.TransferObjects.Envelopes.Carfax;
using FirstLook.FirstLookServices.DomainModel.Utility;
using FirstLook.VehicleHistoryReport.DomainModel;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.Reports.Commands.Impl.Carfax
{
    public class FetchCarfaxReportCommand : ICommand<FetchCarfaxReportResultsDto, IdentityContextDto<FetchCarfaxReportArgumentsDto>>
    {
        public FetchCarfaxReportResultsDto Execute(IdentityContextDto<FetchCarfaxReportArgumentsDto> parameters)
        {
            var vhrfactory = RegistryFactory.GetResolver().Resolve<VehicleHistoryReport.DomainModel.Carfax.Commands.ICommandFactory>();

            var queryReportCommand = vhrfactory.CreateReportQueryCommand();

            var clientDto = new ClientDto
            {
                ClientType = ClientTypeDto.Dealer,
                Id = parameters.Arguments.DealerId
            };


            //reach into carfax reportquery command (client / vin)
            var reportQueryArguments = new ReportQueryArgumentsDto
            {
                Client = clientDto,
                UserName = parameters.Identity.Name,
                Vin = parameters.Arguments.Vin
            };


            ReportQueryResultsDto reportQueryResults = queryReportCommand.Execute(reportQueryArguments);

            ReportDto reportDto = Mapper.Map(reportQueryResults.Report);

            if (reportDto == null)
            {
                var reportPreferenceCommand = vhrfactory.CreateReportPreferenceCommand();

                var reportPreferenceArguments = new ReportPreferenceArgumentsDto
                {
                    Client = clientDto,
                    UserName = parameters.Identity.Name,
                    //TODO: Inventory or Appraisal? ... Undefined not supported even though Undefined Enum exists.
                    VehicleEntityType = VehicleEntityType.Appraisal
                };

                ReportPreferenceResultsDto reportPreferenceResultsDto = reportPreferenceCommand.Execute(reportPreferenceArguments);

                if (reportPreferenceResultsDto.Preference.PurchaseReport)
                {
                    var purchaseCarfaxReportCommand = new PurchaseCarfaxReportCommand();

                    var purchaseCarfaxReportArgumentsDto = new PurchaseCarfaxReportArgumentsDto
                                                               {
                                                                   DealerId = parameters.Arguments.DealerId,
                                                                   Vin = parameters.Arguments.Vin
                                                               };

                    PurchaseCarfaxReportResultsDto purchaseCarfaxReportResultsDto =
                        purchaseCarfaxReportCommand.Execute(Helper.WrapInContext(purchaseCarfaxReportArgumentsDto,
                                                                                 parameters.Identity.Name));

                    reportDto = purchaseCarfaxReportResultsDto.Report;
                }

            }

            return reportDto == null
                       ? new FetchCarfaxReportResultsDto { Report = null }
                       : new FetchCarfaxReportResultsDto { Report = reportDto };
            
        }
    }
}
