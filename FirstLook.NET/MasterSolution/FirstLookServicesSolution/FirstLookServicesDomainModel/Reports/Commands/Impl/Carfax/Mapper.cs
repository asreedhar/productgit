﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.FirstLookServices.DomainModel.Reports.Commands.TransferObjects.Carfax;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.Reports.Commands.Impl.Carfax
{
    public static class Mapper
    {
        public static ReportDto Map(CarfaxReportTO dto)
        {
            if (dto == null)
            {
                return null;
            }

            return new ReportDto
                       {
                           ExpirationDate = dto.ExpirationDate,
                           Inspections = (List<InspectionDto>) Map(dto.Inspections),
                           OwnerCount = dto.OwnerCount,
                           UserName = dto.UserName,
                           HasProblems = dto.HasProblem,
                           ReportType = Enum.GetName(typeof(CarfaxReportType), dto.ReportType),
                           Vin = dto.Vin
                       };
        }

        public static InspectionDto Map(CarfaxReportInspectionTO dto)
        {
            if (dto == null)
            {
                return null;
            }

            return new InspectionDto
                       {
                           Name = dto.Name,
                           Selected = dto.Selected
                       };
        }

        public static IList<InspectionDto> Map(IList<CarfaxReportInspectionTO> dtos)
        {
            return dtos.Select(Map).ToList();
        }
    }
}
