﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.FirstLookServices.DomainModel.Reports.Commands.TransferObjects.AutoCheck;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.Reports.Commands.Impl.AutoCheck
{
    public static class Mapper
    {
        public static ReportDto Map(AutoCheckReportTO dto)
        {
            if (dto == null)
            {
                return null;
            }

            return new ReportDto
                       {
                           CompareScoreRangeHigh = dto.CompareScoreRangeHigh,
                           CompareScoreRangeLow = dto.CompareScoreRangeLow,
                           ExpirationDate = dto.ExpirationDate,
                           Inspections = (List<InspectionDto>) Map(dto.Inspections),
                           OwnerCount = dto.OwnerCount,
                           Score = dto.Score,
                           UserName = dto.UserName,
                           Vin = dto.Vin
                       };
        }

        public static InspectionDto Map(AutoCheckReportInspectionTO dto)
        {
            if (dto == null)
            {
                return null;
            }

            return new InspectionDto
                       {
                           Name = dto.Name,
                           Selected = dto.Selected
                       };
        }

        public static IList<InspectionDto> Map(IList<AutoCheckReportInspectionTO> dtos)
        {
            return dtos.Select(Map).ToList();
        }
    }
}
