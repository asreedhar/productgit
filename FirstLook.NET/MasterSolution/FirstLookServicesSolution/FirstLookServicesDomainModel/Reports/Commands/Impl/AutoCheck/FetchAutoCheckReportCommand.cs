﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Reports.Commands.TransferObjects.Envelopes.AutoCheck;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.Reports.Commands.Impl.AutoCheck
{
    public class FetchAutoCheckReportCommand : ICommand<FetchAutoCheckReportResultsDto, IdentityContextDto<FetchAutoCheckReportArgumentsDto>>
    {
        public FetchAutoCheckReportResultsDto Execute(IdentityContextDto<FetchAutoCheckReportArgumentsDto> parameters)
        {
            var vhrfactory = RegistryFactory.GetResolver().Resolve<VehicleHistoryReport.DomainModel.AutoCheck.Commands.ICommandFactory>();

            var queryReportCommand = vhrfactory.CreateReportQueryCommand();

            //reach into autocheck reportquery command (client / vin)
            FetchAutoCheckReportArgumentsDto fetchAutoCheckReportArgumentsDto = parameters.Arguments;

            var argumentsDto = new ReportQueryArgumentsDto
            {
                Client =
                    new ClientDto
                    {
                        ClientType = ClientTypeDto.Dealer,
                        Id = fetchAutoCheckReportArgumentsDto.DealerId
                    },
                UserName = parameters.Identity.Name,
                Vin = fetchAutoCheckReportArgumentsDto.Vin
            };

            ReportQueryResultsDto reportQueryResults = queryReportCommand.Execute(argumentsDto);

            return reportQueryResults == null
                       ? new FetchAutoCheckReportResultsDto {Report = null}
                       : new FetchAutoCheckReportResultsDto {Report = Mapper.Map(reportQueryResults.Report)};

        }
    }
}
