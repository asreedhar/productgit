﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Reports.Commands.TransferObjects.Envelopes.Carfax
{
    [Serializable]
    public class PurchaseCarfaxReportArgumentsDto
    {
        public int DealerId { get; set; }

        public string Vin { get; set; }
    }
}
