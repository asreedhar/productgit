﻿using FirstLook.FirstLookServices.DomainModel.Reports.Commands.TransferObjects.Carfax;

namespace FirstLook.FirstLookServices.DomainModel.Reports.Commands.TransferObjects.Envelopes.Carfax
{
    public class FetchCarfaxReportResultsDto
    {
        public ReportDto Report { get; set; }
    }
}
