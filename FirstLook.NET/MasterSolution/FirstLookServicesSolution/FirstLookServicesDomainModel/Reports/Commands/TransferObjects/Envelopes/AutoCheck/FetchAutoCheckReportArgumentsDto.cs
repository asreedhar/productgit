﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Reports.Commands.TransferObjects.Envelopes.AutoCheck
{
    [Serializable]
    public class FetchAutoCheckReportArgumentsDto
    {
        public int DealerId { get; set; }

        public string Vin { get; set; }

    }
}
