﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Reports.Commands.TransferObjects.Envelopes.Carfax
{
    [Serializable]
    public class FetchCarfaxReportArgumentsDto
    {
        public int DealerId { get; set; }

        public string Vin { get; set; }

    }
}
