﻿using FirstLook.FirstLookServices.DomainModel.Reports.Commands.TransferObjects.AutoCheck;

namespace FirstLook.FirstLookServices.DomainModel.Reports.Commands.TransferObjects.Envelopes.AutoCheck
{
    public class FetchAutoCheckReportResultsDto
    {
        public ReportDto Report { get; set; }
    }
}
