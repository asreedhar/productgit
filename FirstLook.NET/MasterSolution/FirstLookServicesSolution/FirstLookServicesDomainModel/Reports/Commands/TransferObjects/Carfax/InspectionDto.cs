﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Reports.Commands.TransferObjects.Carfax
{
    [Serializable]

    public class InspectionDto
    {
        public string Name { get; set; }

        public bool Selected { get; set; }
    }
}
