﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Reports.Commands.TransferObjects.AutoCheck
{
    [Serializable]

    public class InspectionDto
    {
        public string Name { get; set; }

        public bool Selected { get; set; }
    }
}
