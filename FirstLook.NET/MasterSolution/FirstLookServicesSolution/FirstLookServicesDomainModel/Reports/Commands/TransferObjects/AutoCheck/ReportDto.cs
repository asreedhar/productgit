﻿using System;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.Reports.Commands.TransferObjects.AutoCheck
{
    [Serializable]

    public class ReportDto
    {
        public DateTime ExpirationDate { get; set; }

        public string UserName { get; set; }

        public int OwnerCount { get; set; }

        public int Score { get; set; }

        public int CompareScoreRangeLow { get; set; }

        public int CompareScoreRangeHigh { get; set; }

        public string Href { get; set; }

        public List<InspectionDto> Inspections { get; set; }

        public string Vin { get; set; }
    }
}
