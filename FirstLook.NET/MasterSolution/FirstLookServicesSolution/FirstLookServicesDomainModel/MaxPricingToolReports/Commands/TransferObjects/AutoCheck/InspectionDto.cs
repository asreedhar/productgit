﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.MaxPricingToolReports.Commands.TransferObjects.AutoCheck
{
    [Serializable]

    public class InspectionDto
    {
        public string Name { get; set; }

        public bool Selected { get; set; }
    }
}
