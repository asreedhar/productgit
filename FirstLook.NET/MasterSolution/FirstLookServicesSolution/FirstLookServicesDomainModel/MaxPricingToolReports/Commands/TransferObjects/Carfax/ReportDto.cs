﻿using System;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.MaxPricingToolReports.Commands.TransferObjects.Carfax
{
    [Serializable]

    public class ReportDto
    {
        public DateTime ExpirationDate { get; set; }

        public string UserName { get; set; }

        public int OwnerCount { get; set; }

        public bool HasProblems { get; set; }

        public string ReportType { get; set; }

        public string Href { get; set; }

        public List<InspectionDto> Inspections { get; set; }

        public string Vin { get; set; }
    }
}
