﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.MaxPricingToolReports.Commands.TransferObjects.Carfax
{
    [Serializable]

    public class InspectionDto
    {
        public string Name { get; set; }

        public bool Selected { get; set; }
    }
}
