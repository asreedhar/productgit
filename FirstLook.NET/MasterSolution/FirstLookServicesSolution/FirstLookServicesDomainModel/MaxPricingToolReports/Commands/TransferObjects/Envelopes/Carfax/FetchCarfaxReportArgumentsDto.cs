﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.MaxPricingToolReports.Commands.TransferObjects.Envelopes.Carfax
{
    [Serializable]
    public class FetchCarfaxReportArgumentsDto
    {
        public int DealerId { get; set; }

        public string Vin { get; set; }

        public string Username { get; set; }
    }
}
