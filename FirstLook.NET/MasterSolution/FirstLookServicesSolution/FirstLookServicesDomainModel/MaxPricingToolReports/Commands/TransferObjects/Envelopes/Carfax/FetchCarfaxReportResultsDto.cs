﻿using FirstLook.FirstLookServices.DomainModel.MaxPricingToolReports.Commands.TransferObjects.Carfax;

namespace FirstLook.FirstLookServices.DomainModel.MaxPricingToolReports.Commands.TransferObjects.Envelopes.Carfax
{
    public class FetchCarfaxReportResultsDto
    {
        public ReportDto Report { get; set; }
        public bool ReportAvailable { get; set; }
    }
}
