﻿using FirstLook.FirstLookServices.DomainModel.MaxPricingToolReports.Commands.TransferObjects.Carfax;

namespace FirstLook.FirstLookServices.DomainModel.MaxPricingToolReports.Commands.TransferObjects.Envelopes.Carfax
{
    public class PurchaseCarfaxReportResultsDto
    {
        public ReportDto Report { get; set; }
    }
}
