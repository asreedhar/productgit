﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.MaxPricingToolReports.Commands.TransferObjects.Envelopes.AutoCheck
{
    [Serializable]
    public class FetchAutoCheckReportArgumentsDto
    {
        public int DealerId { get; set; }

        public string Vin { get; set; }

        public string Username { get; set; }

    }
}
