﻿using FirstLook.FirstLookServices.DomainModel.MaxPricingToolReports.Commands.TransferObjects.AutoCheck;

namespace FirstLook.FirstLookServices.DomainModel.MaxPricingToolReports.Commands.TransferObjects.Envelopes.AutoCheck
{
    public class PurchaseAutoCheckReportResultsDto
    {
        public ReportDto Report { get; set; }
    }
}
