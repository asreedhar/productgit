﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.MaxPricingToolReports.Commands.TransferObjects.Envelopes.Carfax;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Utility;

namespace FirstLook.FirstLookServices.DomainModel.MaxPricingToolReports.Commands.Impl.Carfax
{
    public class PurchaseCarfaxReportCommand : ICommand<PurchaseCarfaxReportResultsDto, IdentityContextDto<PurchaseCarfaxReportArgumentsDto>>
    {
        public PurchaseCarfaxReportResultsDto Execute(IdentityContextDto<PurchaseCarfaxReportArgumentsDto> parameters)
        {
            var clientDto = new ClientDto
            {
                ClientType = ClientTypeDto.Dealer,
                Id = parameters.Arguments.DealerId
            };

            var vhrfactory = RegistryFactory.GetResolver().Resolve<VehicleHistoryReport.DomainModel.Carfax.Commands.ICommandFactory>();

            var purchaseAuthorityCommand = vhrfactory.CreatePurchaseAuthorityCommand();

            PurchaseAuthorityResultsDto purchaseAuthorityResults =
                purchaseAuthorityCommand.Execute(new PurchaseAuthorityArgumentsDto
                {
                    Client = clientDto,
                    UserName = parameters.Arguments.Username
                });

            PurchaseReportResultsDto purchaseReportResultsDto = null;

            if (purchaseAuthorityResults.Authority)
            {
                var purchaseReportCommand = vhrfactory.CreatePurchaseReportCommand();

                //reach into carfax reportquery command (client / vin)
                var purchaseReportArgumentsDto = new PurchaseReportArgumentsDto
                    {
                        Client = clientDto,
                        UserName = parameters.Arguments.Username,
                        Vin = parameters.Arguments.Vin,
                        //TODO: ReportType ... Display in HotList ... Tracking code?
                        ReportType = CarfaxReportType.VHR,
                        DisplayInHotList = false,
                        TrackingCode = CarfaxTrackingCode.FLN
                    };

                purchaseReportResultsDto = purchaseReportCommand.Execute(purchaseReportArgumentsDto);
            }
            else
            {
                throw new CarfaxException(-1, CarfaxResponseCode.Failure_AccountStatus);
            }

            return purchaseReportResultsDto == null
                      ? new PurchaseCarfaxReportResultsDto { Report = null }
                      : new PurchaseCarfaxReportResultsDto { Report = Mapper.Map(purchaseReportResultsDto.Report) };
        }
    }
}
