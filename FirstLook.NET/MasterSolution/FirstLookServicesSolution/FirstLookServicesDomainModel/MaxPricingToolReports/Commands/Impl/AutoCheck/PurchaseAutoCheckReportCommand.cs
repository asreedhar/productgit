﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.MaxPricingToolReports.Commands.TransferObjects.Envelopes.AutoCheck;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleHistoryReport.DomainModel.AutoCheck;
using FirstLook.FirstLookServices.DomainModel.Utility;


namespace FirstLook.FirstLookServices.DomainModel.MaxPricingToolReports.Commands.Impl.AutoCheck
{
    public class PurchaseAutoCheckReportCommand : ICommand<PurchaseAutoCheckReportResultsDto, IdentityContextDto<PurchaseAutoCheckReportArgumentsDto>>
    {
        public PurchaseAutoCheckReportResultsDto Execute(IdentityContextDto<PurchaseAutoCheckReportArgumentsDto> parameters)
        {
            var vhrfactory = RegistryFactory.GetResolver().Resolve<VehicleHistoryReport.DomainModel.AutoCheck.Commands.ICommandFactory>();

            var purchaseAuthorityCommand = vhrfactory.CreatePurchaseAuthorityCommand();

            var clientDto = new ClientDto
            {
                ClientType = ClientTypeDto.Dealer,
                Id = parameters.Arguments.DealerId
            };

            PurchaseAuthorityResultsDto purchaseAuthorityResults =
                purchaseAuthorityCommand.Execute(new PurchaseAuthorityArgumentsDto
                                                     {
                                                         Client = clientDto,
                                                         UserName = parameters.Arguments.Username
                                                     });

            PurchaseReportResultsDto purchaseReportResultsDto = null;

            //Authority to purchase report?
            if (purchaseAuthorityResults.Authority)
            {
                var purchaseReportCommand = vhrfactory.CreatePurchaseReportCommand();

                //purchase report
                var purchaseReportArguments = new PurchaseReportArgumentsDto
                    {
                        Client = clientDto,
                        UserName = parameters.Arguments.Username,
                        Vin = parameters.Arguments.Vin
                    };


                purchaseReportResultsDto = purchaseReportCommand.Execute(purchaseReportArguments);
            }
            else
            {
                throw new AutoCheckException(AutoCheckResponseCode.Forbidden);
                
            }

            return purchaseReportResultsDto == null
                       ? new PurchaseAutoCheckReportResultsDto {Report = null}
                       : new PurchaseAutoCheckReportResultsDto {Report = Mapper.Map(purchaseReportResultsDto.Report)};

        }
    }
}
