﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.MaxPricingToolReports.Commands.TransferObjects.Envelopes.AutoCheck;
using FirstLook.FirstLookServices.DomainModel.MaxPricingToolReports.Commands.TransferObjects.Envelopes.Carfax;

namespace FirstLook.FirstLookServices.DomainModel.MaxPricingToolReports.Commands
{
    public interface ICommandFactory
    {
        ICommand<FetchAutoCheckReportResultsDto, IdentityContextDto<FetchAutoCheckReportArgumentsDto>> CreateFetchAutoCheckReportCommand();

        ICommand<FetchCarfaxReportResultsDto, IdentityContextDto<FetchCarfaxReportArgumentsDto>> CreateFetchCarfaxReportCommand();

        ICommand<PurchaseAutoCheckReportResultsDto, IdentityContextDto<PurchaseAutoCheckReportArgumentsDto>> CreatePurchaseAutoCheckReportCommand();

        ICommand<PurchaseCarfaxReportResultsDto, IdentityContextDto<PurchaseCarfaxReportArgumentsDto>> CreatePurchaseCarfaxReportCommand();
    }
}
