﻿
namespace FirstLook.FirstLookServices.DomainModel.StorePerformance.Commands.TransferObjects.Envelopes
{
    public class FetchStorePerformanceArgumentsDto
    {
        public string VIN { get; set; }
        public int BusinessUnitID { get; set; }
        public int Mileage { get; set; }
    }
}
