﻿using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.StorePerformance.Commands.TransferObjects.Envelopes
{
    public class FetchStorePerformanceResultsDto
    {
        public PrincipalDto Principal { get; set; }

        public FetchStorePerformanceArgumentsDto Arguments { get; set; }

        public IList<StorePerformanceDto> StorePerformance { get; set; }
    }
}
