﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.StorePerformance.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.StorePerformance.Commands
{
    public interface ICommandFactory
    {
        ICommand<FetchStorePerformanceResultsDto, IdentityContextDto<FetchStorePerformanceArgumentsDto>> CreateFetchStorePerformanceCommand();
    }
}
