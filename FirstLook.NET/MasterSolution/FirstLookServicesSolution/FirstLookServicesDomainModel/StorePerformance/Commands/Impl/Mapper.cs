﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.FirstLookServices.DomainModel.StorePerformance.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.StorePerformance.Model;

namespace FirstLook.FirstLookServices.DomainModel.StorePerformance.Commands.Impl
{
    public static class Mapper
    {
        public static IList<StorePerformanceDto> Map(IList<StorePerformanceDetails> objs)
        {
            IList<StorePerformanceDto> storePerformanceDtos = objs.Select(obj => new StorePerformanceDto { RiskLight = obj.RiskLight}).ToList();

            return storePerformanceDtos;
        }
    }
}
