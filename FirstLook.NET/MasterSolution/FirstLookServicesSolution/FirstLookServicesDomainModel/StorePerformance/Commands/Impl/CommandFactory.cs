﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.StorePerformance.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.StorePerformance.Commands.Impl
{
    public class CommandFactory:ICommandFactory
    {
        #region ICommandFactory Members

        public ICommand<FetchStorePerformanceResultsDto, IdentityContextDto<FetchStorePerformanceArgumentsDto>> CreateFetchStorePerformanceCommand()
        {
            return new FetchStorePerformanceCommand();
        }

        #endregion
    }
}
