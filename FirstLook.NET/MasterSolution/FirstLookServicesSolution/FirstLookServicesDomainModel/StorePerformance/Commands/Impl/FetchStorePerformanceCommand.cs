﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.StorePerformance.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.StorePerformance.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.StorePerformance.Model;
using FirstLook.FirstLookServices.DomainModel.StorePerformance.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.Validation;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.StorePerformance.Commands.Impl
{
    public class FetchStorePerformanceCommand : ICommand<FetchStorePerformanceResultsDto, IdentityContextDto<FetchStorePerformanceArgumentsDto>>
    {

        #region ICommand<FetchStorePerformanceResultsDto,IdentityContextDto<FetchStorePerformanceArgumentsDto>> Members

        public FetchStorePerformanceResultsDto Execute(IdentityContextDto<FetchStorePerformanceArgumentsDto> parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            var storePerformanceRepository = resolver.Resolve<IRepository>();

            var paramId = parameters.Arguments.BusinessUnitID;

            Validators.CheckDealerId(paramId, parameters);

            StorePerformanceInput storePerformanceInput = new StorePerformanceInput()
                                                          {
                                                              BusinessUnitId =
                                                                  parameters.Arguments
                                                                  .BusinessUnitID,
                                                              VIN = parameters.Arguments.VIN,
                                                              Mileage =
                                                                  parameters.Arguments
                                                                  .Mileage
                                                          };

            IList<StorePerformanceDto> storePerformance = Mapper.Map(storePerformanceRepository.StorePerformance(storePerformanceInput));
           
            //End of the code
            return new FetchStorePerformanceResultsDto()
            {
                StorePerformance= storePerformance
            };
        }

        #endregion
    }
}
