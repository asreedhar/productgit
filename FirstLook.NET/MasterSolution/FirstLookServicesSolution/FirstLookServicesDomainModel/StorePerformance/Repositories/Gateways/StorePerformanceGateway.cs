﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.FirstLookServices.DomainModel.StorePerformance.Model;
using FirstLook.FirstLookServices.DomainModel.StorePerformance.Repositories.Datastores;
using FirstLook.FirstLookServices.DomainModel.StorePerformance.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.StorePerformance.Repositories.Gateways
{
    public class StorePerformanceGateway : GatewayBase
    {
        public IList<StorePerformanceDetails> Fetch(StorePerformanceInput storePerformance)
        {
            string key = CreateCacheKey("" + storePerformance.BusinessUnitId);

            IList<StorePerformanceDetails> value = Cache.Get(key) as List<StorePerformanceDetails>;

            if (value == null)
            {
                var serializer = Resolve<ISerializer<StorePerformanceDetails>>();

                var datastore = Resolve<IStorePerformanceDatastore>();

                using (IDataReader reader = datastore.StorePerformance(storePerformance))
                {
                    IList<StorePerformanceDetails> items = serializer.Deserialize(reader);

                    value = items;

                    Remember(key, value);
                }
            }

            return value;
        }
    }
}
