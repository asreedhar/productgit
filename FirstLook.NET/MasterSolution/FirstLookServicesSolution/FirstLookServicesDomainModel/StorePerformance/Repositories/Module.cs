﻿using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.StorePerformance.Model;

namespace FirstLook.FirstLookServices.DomainModel.StorePerformance.Repositories
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<Datastores.Module>();

            registry.Register<Serializers.Module>();

            registry.Register<IRepository, StorePerformanceRepository>(ImplementationScope.Shared);
        }
    }
}
