﻿
namespace FirstLook.FirstLookServices.DomainModel.StorePerformance.Repositories.Entities
{
    public class StorePerformanceInput
    {
        public string VIN { get; set; }
        public int BusinessUnitId { get; set; }
        public int Mileage { get; set; }
    }
}
