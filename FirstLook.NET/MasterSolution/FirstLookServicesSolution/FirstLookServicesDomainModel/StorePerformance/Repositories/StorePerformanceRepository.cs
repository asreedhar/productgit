﻿using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.StorePerformance.Model;
using FirstLook.FirstLookServices.DomainModel.StorePerformance.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.StorePerformance.Repositories.Gateways;

namespace FirstLook.FirstLookServices.DomainModel.StorePerformance.Repositories
{
    public class StorePerformanceRepository:IRepository
    {
        #region IRepository Members

        public IList<StorePerformanceDetails> StorePerformance(StorePerformanceInput storePerformance)
        {
            return new StorePerformanceGateway().Fetch(storePerformance);
        }

        #endregion
    }
}
