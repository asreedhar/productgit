﻿using System;
using FirstLook.Common.Core.Registry;

namespace FirstLook.FirstLookServices.DomainModel.StorePerformance.Repositories.Datastores
{
    [Serializable]
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IStorePerformanceDatastore, StorePerformanceDatastore>(ImplementationScope.Shared);
        }
    }
}
