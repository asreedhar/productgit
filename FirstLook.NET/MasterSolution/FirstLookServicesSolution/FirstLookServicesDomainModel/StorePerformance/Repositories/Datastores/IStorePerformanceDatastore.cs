﻿using System.Data;
using FirstLook.FirstLookServices.DomainModel.StorePerformance.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.StorePerformance.Repositories.Datastores
{
    public interface IStorePerformanceDatastore
    {
        IDataReader StorePerformance(StorePerformanceInput storePerformance);
    }
}
