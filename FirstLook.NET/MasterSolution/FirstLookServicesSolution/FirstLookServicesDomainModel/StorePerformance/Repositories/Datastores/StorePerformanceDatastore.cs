﻿using System.Data;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.StorePerformance.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.StorePerformance.Repositories.Datastores
{
    public class StorePerformanceDatastore:IStorePerformanceDatastore
    {
        #region Class Members

            private const string DatabaseName = "IMT";

        #endregion
        
        #region IStorePerformanceDatastore Members

        public IDataReader StorePerformance(StorePerformanceInput storePerformance)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "dbo.Fetch#StorePerformanceData";

                    Database.AddWithValue(command, "VIN", storePerformance.VIN, DbType.String);
                    Database.AddWithValue(command, "BusinessUnitID", storePerformance.BusinessUnitId, DbType.Int32);
                    Database.AddWithValue(command, "VehicleMileage", storePerformance.Mileage, DbType.Int32);

                    var set = new DataSet();

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        var table = new DataTable("StorePerformance");

                        table.Load(reader);

                        set.Tables.Add(table);
                    }

                    return set.CreateDataReader();
                }
            }
        }

        #endregion
    }
}
