﻿using System;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.StorePerformance.Model;

namespace FirstLook.FirstLookServices.DomainModel.StorePerformance.Repositories.Serializers
{
    [Serializable]
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ISerializer<StorePerformanceDetails>, StorePerformanceSerializer>(ImplementationScope.Shared);
        }
    }
}
