﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.StorePerformance.Model;

namespace FirstLook.FirstLookServices.DomainModel.StorePerformance.Repositories.Serializers
{
    public class StorePerformanceSerializer : Serializer<StorePerformanceDetails>
    {
        public override StorePerformanceDetails Deserialize(IDataRecord record)
        {
            InventoryLightType light =InventoryLightType.Undefined;
            try
            {
               light = (InventoryLightType) DataRecord.GetInt32(record, "VehicleLight",0);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
            return new StorePerformanceDetails
            {
                RiskLight = light.ToString().ToLower()
            };
        }
    }
}
