﻿
namespace FirstLook.FirstLookServices.DomainModel.StorePerformance.Model
{
    public enum InventoryLightType
    {
        Undefined=0,
        Red = 1,
        Yellow = 2,
        Green = 3
    }
}
