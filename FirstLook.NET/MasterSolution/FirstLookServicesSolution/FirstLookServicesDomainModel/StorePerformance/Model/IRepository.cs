﻿using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.StorePerformance.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.StorePerformance.Model
{
    public interface IRepository
    {
        IList<StorePerformanceDetails> StorePerformance(StorePerformanceInput storePerformance);
    }
}
