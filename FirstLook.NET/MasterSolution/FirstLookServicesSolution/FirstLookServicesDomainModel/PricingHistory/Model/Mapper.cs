﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.FirstLookServices.DomainModel.PricingHistory.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.PricingHistory.Model
{
    public class Mapper
    {
        public static IList<PricingHistoryDataDto> Map(IList<PricingHistoryInfo> objs)
        {
            var pricinghistorydatadto=new List<PricingHistoryDataDto>();
            foreach (var obj in objs)
            {
                PricingHistoryDataDto temp = new PricingHistoryDataDto();
                temp.InventoryId = obj.InventoryId;
                temp.Login = obj.Login;
                temp.UpdatedDate = obj.UpdatedDate;
                temp.ListPrice = obj.ListPrice;
                pricinghistorydatadto.Add(temp);
            }
            //pricinghistorydatadto.Select(objs => new PricingHistoryDataDto()
            //{
            //    InventoryId = objs.InventoryId,
            //    Login = objs.Login,
            //    UpdatedDate = objs.UpdatedDate,
            //    ListPrice = objs.ListPrice
            //}).ToList();
            return pricinghistorydatadto;
        }

    }
}
