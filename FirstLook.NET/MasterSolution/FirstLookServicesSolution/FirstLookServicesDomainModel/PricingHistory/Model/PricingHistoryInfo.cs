﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.PricingHistory.Model
{
    public class PricingHistoryInfo
    {
        public int InventoryId { get; set; }
        public int ListPrice { get; set; }
        public string Login { get; set; }
        public string UpdatedDate { get; set; }
    }
}
