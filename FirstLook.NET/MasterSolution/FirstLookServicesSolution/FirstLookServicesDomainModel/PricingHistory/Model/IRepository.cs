﻿using FirstLook.FirstLookServices.DomainModel.PricingHistory.Commands.TransferObjects.Envelopes;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.PricingHistory.Model
{
    public interface IRepository
    {
         IList<PricingHistoryInfo> PricingHistoryInfo(FetchPricingHistoryArgumentsDto parameters);
    }
}
