﻿using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.PricingHistory.Model;

namespace FirstLook.FirstLookServices.DomainModel.PricingHistory.Repositories
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<DataStores.Module>();

            registry.Register<Serializers.Module>();

            registry.Register<IRepository, PricingHistoryRepository>(ImplementationScope.Shared);

        }
    }
}
