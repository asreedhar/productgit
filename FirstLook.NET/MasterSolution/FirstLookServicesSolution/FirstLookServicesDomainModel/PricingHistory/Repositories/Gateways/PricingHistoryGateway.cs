﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.FirstLookServices.DomainModel.PricingHistory.Model;
using FirstLook.FirstLookServices.DomainModel.PricingHistory.Repositories.DataStores;
using FirstLook.FirstLookServices.DomainModel.PricingHistory.Repositories.Entities;
using System;

namespace FirstLook.FirstLookServices.DomainModel.PricingHistory.Repositories.Gateways
{
    public class PricingHistoryGateway : GatewayBase
    {
        public IList<PricingHistoryInfo> Get(PricingHistoryInput pricinghistoryinput)
        {
            string key = CreateCacheKey("" + pricinghistoryinput.InventoryId ,pricinghistoryinput.DealerId );
            //InventoryInfo invinfo = null;
            IList<PricingHistoryInfo> pricinghistory = Cache.Get(key) as IList<PricingHistoryInfo>;
            try
            {

                if (pricinghistory == null)
                {
                    pricinghistory = new List<PricingHistoryInfo>();

                    var serializer = Resolve<ISerializer<PricingHistoryInfo>>();

                    var datastore = Resolve<IPricingHistoryDataStore>();

                    using (IDataReader tbl = datastore.PricingHistoryInfo(pricinghistoryinput))
                    {
                       
                       var items = serializer.Deserialize(tbl);
                        
                        Remember(key, items);

                        return items;
                        //if(pricinghistory == null)
                        //throw new Exception("No records found for this vehicle");

                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }


             return pricinghistory;
        }


    }
}
