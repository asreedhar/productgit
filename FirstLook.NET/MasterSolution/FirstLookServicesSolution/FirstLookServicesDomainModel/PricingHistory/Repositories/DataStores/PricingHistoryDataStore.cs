﻿using System.Data;
using System.Data.SqlClient;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.PricingHistory.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.PricingHistory.Repositories.DataStores
{
    public class PricingHistoryDataStore : IPricingHistoryDataStore
    {

        #region Class Members

        private string DatabaseName = "Market";

        #endregion

        #region IPricingHistoryDataStore Members

        public IDataReader PricingHistoryInfo(PricingHistoryInput pricinghistory)
          {
            try
            {
                 using (IDbConnection connection = Database.Connection(DatabaseName))
                {
                    using (IDbCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.CommandText = "Pricing.FetchPricingHistory";

                        Database.AddWithValue(command, "BusinessUnitId", pricinghistory.DealerId, DbType.Int32);
                        Database.AddWithValue(command, "InventoryId", pricinghistory.InventoryId, DbType.Int32);

                        var set = new DataSet();
                         IDataReader reader = command.ExecuteReader();

                         using (reader)
                         {
                             var table = new DataTable("PricingHistory");

                             table.Load(reader);
                             set.Tables.Add(table);
                         }

                         return set.CreateDataReader();
                    }

                }
            }


            catch (SqlException se)
            {
                System.Console.WriteLine(se);
                throw se;
            }

          }

        
    }
    #endregion

    }

