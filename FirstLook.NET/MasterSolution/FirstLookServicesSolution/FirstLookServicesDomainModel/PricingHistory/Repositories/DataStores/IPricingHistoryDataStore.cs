﻿using System.Data;
using FirstLook.FirstLookServices.DomainModel.PricingHistory.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.PricingHistory.Repositories.DataStores
{
    public interface IPricingHistoryDataStore
    {
        IDataReader PricingHistoryInfo(PricingHistoryInput pricingHistory);
    }
}
