﻿using System;
using FirstLook.Common.Core.Registry;

namespace FirstLook.FirstLookServices.DomainModel.PricingHistory.Repositories.DataStores
{
    [Serializable]
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IPricingHistoryDataStore, PricingHistoryDataStore>(ImplementationScope.Shared);
        }

    }
}
