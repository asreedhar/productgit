﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.PricingHistory.Repositories.Entities
{
     public class PricingHistoryInput
     {
         public int InventoryId { get; set; }
         public int DealerId { get; set; }
     }
}
