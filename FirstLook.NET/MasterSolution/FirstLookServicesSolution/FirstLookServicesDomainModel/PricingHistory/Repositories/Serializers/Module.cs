﻿using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.PricingHistory.Model;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.PricingHistory.Repositories.Serializers
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ISerializer<PricingHistoryInfo>, PricingHistorySerializer>(ImplementationScope.Shared);
        }
    }
}
