﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.PricingHistory.Model;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.PricingHistory.Repositories.Serializers
{
    public class PricingHistorySerializer : Serializer<PricingHistoryInfo>
    {
        public override PricingHistoryInfo Deserialize(IDataRecord record)
        {
            return new PricingHistoryInfo()
                {
                    InventoryId = DataRecord.GetInt32(record, "InventoryID", 0),
                    Login = DataRecord.GetString(record, "Login"),
                    UpdatedDate = DataRecord.GetString(record, "UpdatedDate"),
                    ListPrice = DataRecord.GetInt32(record, "ListPrice", 0)
                };          
            
           
        }
    }
}
