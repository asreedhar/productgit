﻿using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.PricingHistory.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.PricingHistory.Model;
using FirstLook.FirstLookServices.DomainModel.PricingHistory.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.PricingHistory.Repositories.Gateways;
using System.Collections.Generic;


namespace FirstLook.FirstLookServices.DomainModel.PricingHistory.Repositories
{
    public class PricingHistoryRepository  : RepositoryBase, IRepository
    {
        #region IRepository Members

        public IList<PricingHistoryInfo> PricingHistoryInfo(FetchPricingHistoryArgumentsDto parameters)
        {
            PricingHistoryInput pricinghistoryinput = new PricingHistoryInput();
            pricinghistoryinput.InventoryId = parameters.InventoryId;
            pricinghistoryinput.DealerId = parameters.DealerId;
            return new PricingHistoryGateway().Get(pricinghistoryinput);

        }
        #endregion

        protected override void OnBeginTransaction(IDataSession session)
        {
            // do nothing -- we do not audited creation of reference/client items
        }


        protected override string DatabaseName
        {
            get { return "Market"; }
        }
        
    }

    }
