﻿using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.PricingHistory.Commands.Impl;

namespace FirstLook.FirstLookServices.DomainModel.PricingHistory.Commands
{
    public class Module : IModule
    {
        #region IModule Members

        public void Configure(IRegistry registry)
        {
            registry.Register<ICommandFactory, CommandFactory>(ImplementationScope.Shared);
        }
        #endregion
    }
}
