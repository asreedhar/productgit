﻿
namespace FirstLook.FirstLookServices.DomainModel.PricingHistory.Commands.TransferObjects.Envelopes
{
    public class FetchPricingHistoryDetailsDto
    {
        public int InventoryId { get; set; }
        public int ListPrice { get; set; }
        public string Login { get; set; }

    }
}
