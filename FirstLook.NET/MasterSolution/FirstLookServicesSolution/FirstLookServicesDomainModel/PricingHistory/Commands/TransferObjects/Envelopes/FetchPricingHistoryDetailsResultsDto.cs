﻿
using System.Collections.Generic;
namespace FirstLook.FirstLookServices.DomainModel.PricingHistory.Commands.TransferObjects.Envelopes
{
    public class FetchPricingHistoryDetailsResultsDto
    {
        public FetchPricingHistoryDetailsResultsDto Arguments { get; set; }

        public IList<PricingHistoryDataDto> PricingHistoryData { get; set; }
    }
}
