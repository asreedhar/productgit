﻿
namespace FirstLook.FirstLookServices.DomainModel.PricingHistory.Commands.TransferObjects.Envelopes
{
    public class FetchPricingHistoryArgumentsDto
    {
        public int InventoryId { get; set; }
        public int DealerId { get; set; }
    }
}
