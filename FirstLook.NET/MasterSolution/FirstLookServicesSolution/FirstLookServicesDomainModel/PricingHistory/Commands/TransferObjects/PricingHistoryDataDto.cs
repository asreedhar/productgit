﻿
namespace FirstLook.FirstLookServices.DomainModel.PricingHistory.Commands.TransferObjects
{
    public class PricingHistoryDataDto
    {
        public int InventoryId { get; set; }
        public string UpdatedDate  { get; set; }
        public string Login { get; set; }
        public int ListPrice { get; set; }
    }
}
