﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.PricingHistory.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.PricingHistory.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.PricingHistory.Model;
using System.Collections.Generic;


namespace FirstLook.FirstLookServices.DomainModel.PricingHistory.Commands.Impl
{
    public class FetchPricingHistoryCommand : ICommand<FetchPricingHistoryDetailsResultsDto, IdentityContextDto<FetchPricingHistoryArgumentsDto>>
    {
        #region ICommand<FetchPricingHistoryResultsDto,IdentityContextDto<FetchPricingHistoryArgumentsDto>> Members

        public FetchPricingHistoryDetailsResultsDto Execute(IdentityContextDto<FetchPricingHistoryArgumentsDto> parameters)
        {

            IResolver resolver = RegistryFactory.GetResolver();
            var pricingRepository = resolver.Resolve<IRepository>();

            FetchPricingHistoryArgumentsDto input = new FetchPricingHistoryArgumentsDto()
             {
                 InventoryId = parameters.Arguments.InventoryId,
                 DealerId = parameters.Arguments.DealerId

             };

            IList<PricingHistoryInfo> detail = pricingRepository.PricingHistoryInfo(input);

            IList<PricingHistoryDataDto> pricinghistorydatadto = Mapper.Map(detail);

             return new FetchPricingHistoryDetailsResultsDto()
            {
                PricingHistoryData = pricinghistorydatadto
            };

        }
        #endregion

    }
}
