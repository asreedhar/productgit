﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.PricingHistory.Commands.TransferObjects.Envelopes;



namespace FirstLook.FirstLookServices.DomainModel.PricingHistory.Commands.Impl
{
    public class CommandFactory : ICommandFactory
    {
        #region ICommandFactory Members

        public ICommand<FetchPricingHistoryDetailsResultsDto, IdentityContextDto<FetchPricingHistoryArgumentsDto>> CreateFetchPricingHistoryCommand()
        {
            return new FetchPricingHistoryCommand();
        }
        #endregion


        
    }
}
