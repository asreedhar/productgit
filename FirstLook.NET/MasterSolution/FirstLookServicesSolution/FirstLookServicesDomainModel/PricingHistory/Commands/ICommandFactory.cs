﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.PricingHistory.Commands.TransferObjects.Envelopes;


namespace FirstLook.FirstLookServices.DomainModel.PricingHistory.Commands
{
    public interface ICommandFactory
    {
        ICommand<FetchPricingHistoryDetailsResultsDto, IdentityContextDto<FetchPricingHistoryArgumentsDto>> CreateFetchPricingHistoryCommand();
    }
}
