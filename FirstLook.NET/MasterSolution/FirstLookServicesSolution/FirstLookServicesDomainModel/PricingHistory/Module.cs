﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.Registry;


namespace FirstLook.FirstLookServices.DomainModel.PricingHistory
{
    public class Module : IModule
    {
        #region IModule Members

        public void Configure(IRegistry registry)
        {
            registry.Register<Commands.Module>();

            registry.Register<Repositories.Module>();
        }

        #endregion
    }
}
