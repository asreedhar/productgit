﻿
namespace FirstLook.FirstLookServices.DomainModel.PriceComparisons.Model
{
    public class PriceComparisonDetails
    {
        public int InventoryId { get; set; }

        public int BookId { get; set; }

        public string BookName { get; set; }

        public int BookValue { get; set; }

        public bool IsValid { get; set; }

        public bool IsAccurate { get; set; }

        public int ComparisonColor { get; set; }
                        
    }
}
