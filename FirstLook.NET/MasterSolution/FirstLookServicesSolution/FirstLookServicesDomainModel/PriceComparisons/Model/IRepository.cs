﻿using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.PriceComparisons.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.PriceComparisons.Model
{
    public interface IRepository
    {
        IList<PriceComparisonDetails> PriceComparison(PriceComparisonInput memberInput);
    }
}
