﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.PriceComparisons.Model
{
    public class PriceComparisonsListModel
    {
        public int InventoryId { get; set; }
        public List<PriceComparisonDetails> PriceComparisonsDetails { get; set; }
    }
}
