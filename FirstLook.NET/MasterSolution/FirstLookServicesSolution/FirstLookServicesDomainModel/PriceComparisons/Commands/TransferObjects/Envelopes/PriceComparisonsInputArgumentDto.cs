﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.PriceComparisons.Commands.TransferObjects.Envelopes
{
    public class PriceComparisonsInputArgumentDto
    {
        public int InventoryId { get; set; }
    }
}
