﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.PriceComparisons.Commands.TransferObjects.Envelopes
{
    public class PriceComparisonsResult
    {
        public int InventoryId { get; set; }
        public List<PriceComparisonDetailDto> PriceComparisonsDetails { get; set; }
    }
}
