﻿
using System.Collections.Generic;
namespace FirstLook.FirstLookServices.DomainModel.PriceComparisons.Commands.TransferObjects.Envelopes
{
    public class PriceComparisonsResultsDto
    {
        public IList<PriceComparisonsResult> PriceComparisons { get; set; } 
    }
}
