﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.PriceComparisons.Commands.TransferObjects
{
    public class PriceComparisonDetailDto
    {
        public int BookId { get; set; }
        public string BookName { get; set; }
        public int BookValue { get; set; }
        public bool IsValid { get; set; }
        public bool IsAccurate { get; set; }
        public int ComparisonColor { get; set; }
    }
}
