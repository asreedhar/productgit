﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.PriceComparisons.Commands.TransferObjects
{
    public class PriceComparisonsListDto
    {
        public int InventoryId { get; set; }
        public List<PriceComparisonDetailDto> PriceComparisonDetailDto { get; set; }
    }
}
