﻿using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.PriceComparisons.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.PriceComparisons.Commands
{
    public interface ICommandFactory
    {
        ICommand<PriceComparisonsResultsDto, IdentityContextDto<PriceComparisonsArgumentsDto>> CreateFetchPriceComparisonsCommand();
    }
}
