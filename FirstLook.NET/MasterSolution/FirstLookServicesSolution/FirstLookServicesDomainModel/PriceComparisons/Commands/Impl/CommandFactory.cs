﻿using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.PriceComparisons.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.PriceComparisons.Commands.Impl
{
    public class CommandFactory:ICommandFactory
    {
        public ICommand<PriceComparisonsResultsDto, IdentityContextDto<PriceComparisonsArgumentsDto>> CreateFetchPriceComparisonsCommand()
        {
            return new FetchPriceComparisonsCommand();
        }

      
    }
}
