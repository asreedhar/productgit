﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.PriceComparisons.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.PriceComparisons.Model;
using FirstLook.FirstLookServices.DomainModel.PriceComparisons.Repositories.Entities;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Distribution;
using FirstLook.FirstLookServices.DomainModel.PriceComparisons.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using System.Linq;

namespace FirstLook.FirstLookServices.DomainModel.PriceComparisons.Commands.Impl
{
    class FetchPriceComparisonsCommand:ICommand<PriceComparisonsResultsDto,IdentityContextDto<PriceComparisonsArgumentsDto>>
    {
      


        public PriceComparisonsResultsDto Execute(IdentityContextDto<PriceComparisonsArgumentsDto> parameters)
        {

                int dealerId = parameters.Arguments.DealerId;
                IResolver resolver = RegistryFactory.GetResolver();
                var pricingAnalysisRepository = resolver.Resolve<IRepository>();
                IList<PriceComparisonsResult> PriceComparisons = new List<PriceComparisonsResult>();

                //IList<IList<PriceComparisonDetailDto>> AllPriceComparisons=new List<IList<PriceComparisonDetailDto>>();
                List<string> InventoryIds = new List<string>();
               
               


                PriceComparisonInput PriceComparisonInput = new PriceComparisonInput() { DealerId = dealerId, InventoryId = InventoryIds };
                IList<PriceComparisonsListDto> priceComparisonsInventory = Mapper.Map(pricingAnalysisRepository.PriceComparison(PriceComparisonInput));

                if (priceComparisonsInventory == null || priceComparisonsInventory.Count == 0)
                {
                    throw new NoDataFoundException("Price Comparisons");
                }
                else
                {
                    foreach (var v in parameters.Arguments.InventoryIds)
                    {
                        PriceComparisonsListDto x = priceComparisonsInventory.Where(ob => ob.InventoryId == v).FirstOrDefault();
                        if (x != null)
                        {
                            List<PriceComparisonDetailDto> priceComparisonDetailDto = new List<PriceComparisonDetailDto>();
                            priceComparisonDetailDto = priceComparisonsInventory.Where(obj => obj.InventoryId == v).Select(obj => obj.PriceComparisonDetailDto).FirstOrDefault();
                            PriceComparisons.Add(new PriceComparisonsResult() { InventoryId = v, PriceComparisonsDetails = priceComparisonDetailDto });
                        }
                        else
                        {
                            List<PriceComparisonDetailDto> priceComparisonDetailDto = new List<PriceComparisonDetailDto>();
                            priceComparisonDetailDto = null;
                            PriceComparisons.Add(new PriceComparisonsResult() { InventoryId = v, PriceComparisonsDetails = priceComparisonDetailDto });
                        }


                    }
                }


                return new PriceComparisonsResultsDto()
                {
                    PriceComparisons = PriceComparisons
                };
		  
        }
    }
}
