﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.FirstLookServices.DomainModel.PriceComparisons.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.PriceComparisons.Model;
using FirstLook.FirstLookServices.DomainModel.PriceComparisons.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.PriceComparisons.Commands.Impl
{
    public static class Mapper
    {
        public static IList<PriceComparisonsListDto> Map(IList<PriceComparisonDetails> objs)
        {
            IList<PriceComparisonsListDto> priceComparisonsListDto = new List<PriceComparisonsListDto>();
            

            //List<int> inventoryids=new List<int>();

            var inventoryIds = objs.Select(obj=>obj.InventoryId).Distinct();

            foreach(var inventoryId in inventoryIds){
                List<PriceComparisonDetailDto> priceComparisonDetailDto = new List<PriceComparisonDetailDto>();
                objs.Where(ob=>ob.InventoryId==inventoryId).ToList().ForEach(data=> priceComparisonDetailDto.Add(new PriceComparisonDetailDto(){
                        BookId=data.BookId,
                        BookName=data.BookName,
                        BookValue=data.BookValue,
                        IsAccurate=data.IsAccurate,
                        IsValid=data.IsValid,
                        ComparisonColor=data.ComparisonColor
                }   ));
                priceComparisonsListDto.Add(new PriceComparisonsListDto(){InventoryId=inventoryId,PriceComparisonDetailDto=priceComparisonDetailDto} );
            }

            return priceComparisonsListDto;

            

        }
    }
}
