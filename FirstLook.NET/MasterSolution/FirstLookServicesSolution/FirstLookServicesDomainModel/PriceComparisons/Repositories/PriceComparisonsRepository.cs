﻿using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.PriceComparisons.Model;
using FirstLook.FirstLookServices.DomainModel.PriceComparisons.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.PriceComparisons.Repositories.Gateways;

namespace FirstLook.FirstLookServices.DomainModel.PriceComparisons.Repositories
{
    public class PriceComparisonsRepository:IRepository
    {
        public IList<PriceComparisonDetails> PriceComparison(PriceComparisonInput priceComparisonInput)
        {
            return new PriceComparisonsGateway().FetchPriceComparisons(priceComparisonInput);
        }
    }
}
