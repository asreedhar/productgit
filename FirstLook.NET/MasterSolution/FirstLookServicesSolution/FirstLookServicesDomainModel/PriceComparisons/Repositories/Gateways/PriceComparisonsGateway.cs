﻿using System.Collections.Generic;
using FirstLook.Common.Core;
using FirstLook.FirstLookServices.DomainModel.PriceComparisons.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.PriceComparisons.Model;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.FirstLookServices.DomainModel.PriceComparisons.Repositories.DataStores;
using System.Data;

namespace FirstLook.FirstLookServices.DomainModel.PriceComparisons.Repositories.Gateways
{
    public class PriceComparisonsGateway:GatewayBase
    {
        public IList<PriceComparisonDetails> FetchPriceComparisons(PriceComparisonInput priceComparisonInput)
        {

            string key = CreateCacheKey("" + priceComparisonInput.InventoryId);

            IList<PriceComparisonDetails> value = Cache.Get(key) as IList<PriceComparisonDetails>;

            if (value == null)
            {
                var serializer = Resolve<ISerializer<PriceComparisonDetails>>();

                var datastore = Resolve<IPriceComparisonsDataStore>();
                using (IDataReader reader = datastore.FetchPriceComparisons(priceComparisonInput))
                {
                    IList<PriceComparisonDetails> items = serializer.Deserialize(reader);

                    value = items;

                    Remember(key, value);
                }
            }
            return value;
        }

    }

}
