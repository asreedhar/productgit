﻿using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.PriceComparisons.Model;
namespace FirstLook.FirstLookServices.DomainModel.PriceComparisons.Repositories
{
    public class Module:IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<Commands.Module>();


            registry.Register<DataStores.Module>();

            registry.Register<Serializers.Module>();

            registry.Register<IRepository, PriceComparisonsRepository>(ImplementationScope.Shared);
        }
    }
}
