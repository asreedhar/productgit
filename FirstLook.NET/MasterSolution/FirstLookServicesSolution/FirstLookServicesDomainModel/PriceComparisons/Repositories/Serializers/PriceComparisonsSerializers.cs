﻿using System;
using FirstLook.FirstLookServices.DomainModel.PriceComparisons.Model;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using System.Data;

namespace FirstLook.FirstLookServices.DomainModel.PriceComparisons.Repositories.Serializers
{
    public class PriceComparisonsSerializers:Serializer<PriceComparisonDetails>
    {
        public override PriceComparisonDetails Deserialize(IDataRecord record)
        {
            int inventoryId = 0;
            int bookId =0;

         string bookName=string.Empty;

         int bookValue =0;

         bool isValid=false;

         bool isAccurate = false;

         int comparisonColor = 0;
            try
            {
                inventoryId = DataRecord.GetInt32(record, "InventoryId", 0);
                bookId = DataRecord.GetInt32(record, "BookId", 0);
                bookName = DataRecord.GetString(record, "BookName");
                bookValue = DataRecord.GetInt32(record, "BookValue", 0);
                isValid = DataRecord.GetBoolean(record, "IsValid");
                isAccurate = DataRecord.GetBoolean(record, "IsAccurate");
                comparisonColor = DataRecord.GetInt32(record, "ComparisonColor", 0);
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return new PriceComparisonDetails()
            {
                InventoryId=inventoryId,
                BookId=bookId,
                BookName=bookName,
                BookValue=bookValue,
                IsValid=isValid,
                IsAccurate=isAccurate,
                ComparisonColor=comparisonColor
            };
        }

    }
}
