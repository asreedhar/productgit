﻿using System;
using FirstLook.Common.Core.Registry;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.FirstLookServices.DomainModel.PriceComparisons.Model;

namespace FirstLook.FirstLookServices.DomainModel.PriceComparisons.Repositories.Serializers
{
    [Serializable]
    class Module:IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ISerializer<PriceComparisonDetails>, PriceComparisonsSerializers>(ImplementationScope.Shared);
        }
    }
}
