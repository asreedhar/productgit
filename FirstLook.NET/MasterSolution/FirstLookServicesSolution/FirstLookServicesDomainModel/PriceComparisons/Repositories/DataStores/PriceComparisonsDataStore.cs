﻿using System.Data;
using FirstLook.FirstLookServices.DomainModel.PriceComparisons.Repositories.Entities;
using FirstLook.Common.Core.Data;

namespace FirstLook.FirstLookServices.DomainModel.PriceComparisons.Repositories.DataStores
{
    class PriceComparisonsDataStore:IPriceComparisonsDataStore
    {

        private const string DatabaseName = "IMT";

        public IDataReader FetchPriceComparisons(PriceComparisonInput priceComparisonInput)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "dbo.Fetch#PriceComparisons";

                    Database.AddWithValue(command, "BusinessUnitID", priceComparisonInput.DealerId, DbType.Int64);
                   
                    

                    var set = new DataSet();

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        var table = new DataTable("PriceComparisons");

                        table.Load(reader);

                        set.Tables.Add(table);
                    }

                    return set.CreateDataReader();
                }
            }
        }
    }
}
