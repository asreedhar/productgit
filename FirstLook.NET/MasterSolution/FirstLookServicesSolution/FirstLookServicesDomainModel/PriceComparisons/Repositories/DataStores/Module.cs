﻿using System;
using FirstLook.Common.Core.Registry;

namespace FirstLook.FirstLookServices.DomainModel.PriceComparisons.Repositories.DataStores
{
    [Serializable]
    class Module:IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IPriceComparisonsDataStore, PriceComparisonsDataStore>(ImplementationScope.Shared);
        }
    }
}
