﻿using System.Data;
using FirstLook.FirstLookServices.DomainModel.PriceComparisons.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.PriceComparisons.Repositories.DataStores
{
    public interface IPriceComparisonsDataStore
    {
        IDataReader FetchPriceComparisons(PriceComparisonInput priceComparisonInput);
    }
}
