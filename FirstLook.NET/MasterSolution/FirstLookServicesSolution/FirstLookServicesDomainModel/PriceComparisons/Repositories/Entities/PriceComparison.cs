﻿
using System.Collections.Generic;
namespace FirstLook.FirstLookServices.DomainModel.PriceComparisons.Repositories.Entities
{
    public class PriceComparisonInput
    {
        public int DealerId { get; set; }
        public List<string> InventoryId { get; set; }
    }
}
