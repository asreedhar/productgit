﻿using System;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.Utility
{
    public class Helper
    {
        public static IdentityContextDto<T> WrapInContext<T>(T arguments, string userName)
        {
            return new IdentityContextDto<T>
            {
                Arguments = arguments,
                Identity = new IdentityDto
                {
                    AuthorityName = "CAS",
                    Name = userName
                }
            };
        }

        //todo: can i make a web request to construct the tiny url as i'm doin here?
        protected string ToTinyUrls(string txt)
        {
            var regx = new Regex("http://([\\w+?\\.\\w+])+([a-zA-Z0-9\\~\\!\\@\\#\\$\\%\\^\\&amp;\\*\\(\\)_\\-\\=\\+\\\\\\/\\?\\.\\:\\;\\'\\,]*)?", RegexOptions.IgnoreCase);

            MatchCollection mactches = regx.Matches(txt);

            foreach (Match match in mactches)
            {
                string tUrl = MakeTinyUrl(match.Value);
                
                txt = txt.Replace(match.Value, tUrl);
            }

            return txt;
        }

        public static string MakeTinyUrl(string url)
        {
            try
            {
                if (url.Length <= 12)
                {
                    return url;
                }

                if (!url.ToLower().StartsWith("http") && !url.ToLower().StartsWith("ftp"))
                {
                    url = "http://" + url;
                }

                var request = WebRequest.Create("http://tinyurl.com/api-create.php?url=" + url);
                
                var res = request.GetResponse();
                
                string text;
                
                using (var reader = new StreamReader(res.GetResponseStream()))
                {
                    text = reader.ReadToEnd();
                }

                return text;
            }
            catch (Exception)
            {
                return url;
            }
        }

        public static int GetHashValue(string arg)
        {
            //Create a new instance of the UnicodeEncoding class to 
            //convert the string into an array of Unicode bytes.
            var ue = new UnicodeEncoding();

            //Convert the string into an array of bytes.
            byte[] messageBytes = ue.GetBytes(arg);

            //Create a new instance of the SHA1Managed class to create 
            //the hash value.
            var sHhash = new SHA1Managed();

            //Create the hash value from the array of bytes.
            byte[] hash = sHhash.ComputeHash(messageBytes);

            return BitConverter.ToInt32(hash, 0);

        }


    }
}