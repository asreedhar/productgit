﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FirstLook.FirstLookServices.DomainModel.Utility
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<Tuple<T,T>> GetPairwisePermutations<T>(this IEnumerable<T> enumerable )
        {
            var list = enumerable.ToList();
            var combinations = from item1 in list
                               from item2 in list
                               where !item1.Equals(item2)
                               select Tuple.Create(item1, item2);

            return combinations;
        }
    }
}
