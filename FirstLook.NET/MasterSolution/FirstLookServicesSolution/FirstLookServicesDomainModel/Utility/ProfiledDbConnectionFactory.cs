﻿using System.Data;
using System.Data.SqlClient;
using ServiceStack.MiniProfiler;
using ServiceStack.MiniProfiler.Data;

namespace FirstLook.FirstLookServices.DomainModel.Utility
{
    internal static class ProfiledDbConnectionFactory
    {
        public static IDbConnection GetConnection(string connectionString)
        {
            var connection = new SqlConnection(connectionString);
            return new ProfiledDbConnection( connection, Profiler.Current );
        }
    }
}
