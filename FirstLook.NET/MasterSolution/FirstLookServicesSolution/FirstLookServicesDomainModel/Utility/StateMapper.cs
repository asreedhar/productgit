﻿using System;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.Utility
{
    [Serializable]
    public class StateMapper
    {
        public string Name { get; internal set; }

        public string Code { get; internal set; }

        private static readonly ICollection<StateMapper> Lookup;

        static StateMapper()
        {
            Lookup = new List<StateMapper>
                                     {
                                         new StateMapper {Code = "AL", Name = "Alabama"},
                                         new StateMapper {Code = "AK", Name = "Alaska"},
                                         new StateMapper {Code = "AZ", Name = "Arizona"},
                                         new StateMapper {Code = "AR", Name = "Arkansas"},
                                         new StateMapper {Code = "CA", Name = "California"},
                                         new StateMapper {Code = "CO", Name = "Colorado"},
                                         new StateMapper {Code = "CT", Name = "Connecticut"},
                                         new StateMapper {Code = "DE", Name = "Delaware"},
                                         new StateMapper {Code = "FL", Name = "Florida"},
                                         new StateMapper {Code = "GA", Name = "Georgia"},
                                         new StateMapper {Code = "HI", Name = "Hawaii"},
                                         new StateMapper {Code = "ID", Name = "Idaho"},
                                         new StateMapper {Code = "IL", Name = "Illinois"},
                                         new StateMapper {Code = "IN", Name = "Indiana"},
                                         new StateMapper {Code = "IA", Name = "Iowa"},
                                         new StateMapper {Code = "KS", Name = "Kansas"},
                                         new StateMapper {Code = "KY", Name = "Kentucky"},
                                         new StateMapper {Code = "LA", Name = "Louisiana"},
                                         new StateMapper {Code = "ME", Name = "Maine"},
                                         new StateMapper {Code = "MD", Name = "Maryland"},
                                         new StateMapper {Code = "MA", Name = "Massachusetts"},
                                         new StateMapper {Code = "MI", Name = "Michigan"},
                                         new StateMapper {Code = "MN", Name = "Minnesota"},
                                         new StateMapper {Code = "MS", Name = "Mississippi"},
                                         new StateMapper {Code = "MO", Name = "Missouri"},
                                         new StateMapper {Code = "MT", Name = "Montana"},
                                         new StateMapper {Code = "NE", Name = "Nebraska"},
                                         new StateMapper {Code = "NV", Name = "Nevada"},
                                         new StateMapper {Code = "NH", Name = "New Hampshire"},
                                         new StateMapper {Code = "NJ", Name = "New Jersey"},
                                         new StateMapper {Code = "NM", Name = "New Mexico"},
                                         new StateMapper {Code = "NY", Name = "New York"},
                                         new StateMapper {Code = "NC", Name = "North Carolina"},
                                         new StateMapper {Code = "ND", Name = "North Dakota"},
                                         new StateMapper {Code = "OH", Name = "Ohio"},
                                         new StateMapper {Code = "OK", Name = "Oklahoma"},
                                         new StateMapper {Code = "OR", Name = "Oregon"},
                                         new StateMapper {Code = "PA", Name = "Pennsylvania"},
                                         new StateMapper {Code = "RI", Name = "Rhode Island"},
                                         new StateMapper {Code = "SC", Name = "South Carolina"},
                                         new StateMapper {Code = "SD", Name = "South Dakota"},
                                         new StateMapper {Code = "TN", Name = "Tennessee"},
                                         new StateMapper {Code = "TX", Name = "Texas"},
                                         new StateMapper {Code = "UT", Name = "Utah"},
                                         new StateMapper {Code = "VT", Name = "Vermont"},
                                         new StateMapper {Code = "VA", Name = "Virginia"},
                                         new StateMapper {Code = "WA", Name = "Washington"},
                                         new StateMapper {Code = "WV", Name = "West Virginia"},
                                         new StateMapper {Code = "WI", Name = "Wisconsin"},
                                         new StateMapper {Code = "WY", Name = "Wyoming"},
                                         new StateMapper {Code = "PR", Name = "Puerto Rico"},
                                         new StateMapper {Code = "DC", Name = "District of Columbia"}
                                     }.AsReadOnly();
        }

        public static ICollection<StateMapper> Values
        {
            get
            {
                return Lookup;
            }
        }

        public static StateMapper Identify(string code)
        {
            foreach (StateMapper state in Lookup)
            {
                if (state.Code == code)
                {
                    return state;
                }
            }
            return null;
        }

    }
}
