﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.FirstLookServices.DomainModel.Dealers.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.Dealers.Commands.Impl
{
    public static class Mapper
    {
        public static DealerDto Map(Client.DomainModel.Clients.Commands.TransferObjects.ClientDto dto)
        {
            if (dto == null)
            {
                return null;
            }

            return new DealerDto
                       {
                           Id = dto.Id,
                           Description = dto.Name
                       };
        }

        public static IList<DealerDto> Map(IList<Client.DomainModel.Clients.Commands.TransferObjects.ClientDto> objs)
        {
            IList<DealerDto> dealerDtos = objs.Select(Map).ToList();

            return dealerDtos;
        }
    }
}
