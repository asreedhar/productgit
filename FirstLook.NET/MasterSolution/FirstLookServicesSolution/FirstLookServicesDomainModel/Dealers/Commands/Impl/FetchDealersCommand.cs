﻿using System.Collections.Generic;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Dealers.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Dealers.Commands.TransferObjects.Envelopes;


namespace FirstLook.FirstLookServices.DomainModel.Dealers.Commands.Impl
{
    public class FetchDealersCommand : ICommand<FetchDealersResultsDto, IdentityContextDto<FetchDealersArgumentsDto>>
    {
        public FetchDealersResultsDto Execute(IdentityContextDto<FetchDealersArgumentsDto> parameters)
        {
            var factory = RegistryFactory.GetResolver().Resolve<Client.DomainModel.Clients.Commands.ICommandFactory>();

            var command = factory.CreateQueryClientCommand();

            var argumentsDto = new QueryClientArgumentsDto
                                   {
                                       SortColumns =
                                           new List<SortColumnDto>
                                               {
                                                   new SortColumnDto
                                                       {
                                                           Ascending = true,
                                                           ColumnName = "Name"
                                                       }
                                               },
                                       MaximumRows = int.MaxValue,
                                       StartRowIndex = 0
                                   };

            var identityContextDto = new IdentityContextDto<QueryClientArgumentsDto>
                                                        {
                                                            Identity = parameters.Identity,
                                                            Arguments = argumentsDto
                                                        };


            QueryClientResultsDto results = command.Execute(identityContextDto);

            return new FetchDealersResultsDto
                       {
                           Dealers = (List<DealerDto>) Mapper.Map(results.Clients)
                       };

        }
    }
}
