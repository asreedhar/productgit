﻿using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Dealers.Commands.Impl;

namespace FirstLook.FirstLookServices.DomainModel.Dealers.Commands
{
    public class Module : IModule
    {
        #region IModule Members

        /// <summary>
        /// Add pertinent interfaces and impelementing classes to the registry.
        /// </summary>
        public void Configure(IRegistry registry)
        {
            registry.Register<ICommandFactory, CommandFactory>(ImplementationScope.Shared);
        }

        #endregion

    }
}
