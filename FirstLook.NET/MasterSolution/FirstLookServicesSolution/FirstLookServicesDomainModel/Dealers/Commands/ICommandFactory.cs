﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.Dealers.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.Dealers.Commands
{
    public interface ICommandFactory
    {
        ICommand<FetchDealersResultsDto, IdentityContextDto<FetchDealersArgumentsDto>> CreateFetchDealersCommand();
    }
}
