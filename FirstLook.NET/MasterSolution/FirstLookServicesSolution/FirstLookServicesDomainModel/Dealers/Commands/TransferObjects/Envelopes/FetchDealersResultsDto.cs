﻿using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.Dealers.Commands.TransferObjects.Envelopes
{
    public class FetchDealersResultsDto
    {
        public List<DealerDto> Dealers { get; set; }
    }
}
