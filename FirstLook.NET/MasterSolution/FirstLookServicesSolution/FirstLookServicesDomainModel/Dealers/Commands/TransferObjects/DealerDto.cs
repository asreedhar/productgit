﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Dealers.Commands.TransferObjects
{
    [Serializable]

    public class DealerDto
    {
        public int Id { get; set; }

        public string Description { get; set; }
    }
}
