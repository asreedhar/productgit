﻿
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.UserRole.Commands.TransferObjects.Envelopes
{
   public class FetchUserRoleResultsDto
    {
       public PrincipalDto Principal { get; set; }

       public FetchUserRoleArgumentsDto Arguments { get; set; }

       public IList<UserRoleDto> UserRole { get; set; }

    }
}
