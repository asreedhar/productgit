﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.UserRole.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.UserRole.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.UserRole.Model;
using FirstLook.FirstLookServices.DomainModel.UserRole.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.Validation;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.UserRole.Commands.Impl
{
   public class FetchUserRoleCommand:ICommand<FetchUserRoleResultsDto,IdentityContextDto<FetchUserRoleArgumentsDto>> 
   {

       #region ICommand<FetchStorePerformanceResultsDto,IdentityContextDto<FetchStorePerformanceArgumentsDto>> Members

       public FetchUserRoleResultsDto Execute(IdentityContextDto<FetchUserRoleArgumentsDto> parameters)
       {
           IResolver resolver = RegistryFactory.GetResolver();

           var userRoleRepository = resolver.Resolve<IRepository>();

           var paramId = parameters.Arguments.UserName;

          // Validators.CheckDealerId(paramId, parameters);

           UserRoleInput userRoleInput = new UserRoleInput()
           {
               UserName=parameters.Arguments.UserName
           };

           IList<UserRoleDto> userRole = Mapper.Map(userRoleRepository.UserRole(userRoleInput));

           //End of the code
           return new FetchUserRoleResultsDto
           {
               UserRole = userRole
           };
       }

       #endregion
    }
}
