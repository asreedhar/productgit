﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.UserRole.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.UserRole.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.UserRole.Commands.Impl
{
    public class CommandFactory : ICommandFactory
    {
        #region ICommandFactory Members

        public ICommand<FetchUserRoleResultsDto,IdentityContextDto<FetchUserRoleArgumentsDto>> CreateFetchUserRoleCommand()
        {
            return new FetchUserRoleCommand();
        }

        #endregion
    }
}
