﻿using System.Collections.Generic;
using System.Linq;
using FirstLook.FirstLookServices.DomainModel.UserRole.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.UserRole.Model;


namespace FirstLook.FirstLookServices.DomainModel.UserRole.Commands.Impl
{
    public static class Mapper
    {
        public static IList<UserRoleDto> Map(IList<UserRoleDetails> objs)
        {
            IList<UserRoleDto> userRoleDtos = objs.Select(obj => new UserRoleDto { MemberType = obj.MemberType }).ToList();

            return userRoleDtos;
        }
    }
}
