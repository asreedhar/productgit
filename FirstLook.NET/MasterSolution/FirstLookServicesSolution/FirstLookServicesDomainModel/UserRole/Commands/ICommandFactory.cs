﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.UserRole.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.UserRole.Commands
{
   public interface ICommandFactory
   {
       ICommand<FetchUserRoleResultsDto, IdentityContextDto<FetchUserRoleArgumentsDto>> CreateFetchUserRoleCommand();
   }
}
