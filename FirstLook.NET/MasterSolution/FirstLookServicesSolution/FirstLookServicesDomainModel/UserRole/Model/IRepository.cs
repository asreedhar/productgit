﻿using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.UserRole.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.UserRole.Model
{
   public interface IRepository
    {
       IList<UserRoleDetails> UserRole(UserRoleInput userRole);
    }
}
