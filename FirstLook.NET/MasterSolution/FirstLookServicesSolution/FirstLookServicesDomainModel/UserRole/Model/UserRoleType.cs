﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.UserRole.Model
{
    public enum UserRoleType
    {
        None=0,
        Admin=1,
        User=2,
        AccountRepresentative=3,
        SalesPerson=4
    }
}
