﻿using System;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.UserRole.Model;

namespace FirstLook.FirstLookServices.DomainModel.UserRole.Repositories.Serializers
{
    [Serializable]
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ISerializer<UserRoleDetails>, UserRoleSerializer>(ImplementationScope.Shared);
        }
    }
}
