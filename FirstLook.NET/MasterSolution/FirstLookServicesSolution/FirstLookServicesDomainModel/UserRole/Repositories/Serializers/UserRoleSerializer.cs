﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.UserRole.Model;
using FirstLook.DomainModel.Oltp;


namespace FirstLook.FirstLookServices.DomainModel.UserRole.Repositories.Serializers
{
    public class UserRoleSerializer : Serializer<UserRoleDetails>
    {
        public override UserRoleDetails Deserialize(IDataRecord record)
        {
            
            MemberType roletype = MemberType.Dummy;

            try
            {
                roletype = (MemberType)DataRecord.GetInt32(record, "MemberType", 0);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return new UserRoleDetails
            {
               MemberType  = roletype.ToString()
            };
        }
    }

}
