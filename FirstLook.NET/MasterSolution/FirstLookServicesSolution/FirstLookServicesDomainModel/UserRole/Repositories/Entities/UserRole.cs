﻿

namespace FirstLook.FirstLookServices.DomainModel.UserRole.Repositories.Entities
{
    public class UserRoleInput
    {
        public string UserName { get; set; }
    }
}
