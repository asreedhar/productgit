﻿using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.UserRole.Model;

namespace FirstLook.FirstLookServices.DomainModel.UserRole.Repositories
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<Datastores.Module>();

            registry.Register<Serializers.Module>();

            registry.Register<IRepository, UserRoleRepository>(ImplementationScope.Shared);
        }
    }
}
