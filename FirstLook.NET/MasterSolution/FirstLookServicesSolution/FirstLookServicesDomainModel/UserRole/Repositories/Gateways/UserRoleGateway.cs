﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core;
using FirstLook.FirstLookServices.DomainModel.UserRole.Model;
using FirstLook.FirstLookServices.DomainModel.UserRole.Repositories.Datastores;
using FirstLook.FirstLookServices.DomainModel.UserRole.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.UserRole.Repositories.Gateways
{
    public class UserRoleGateway:GatewayBase
    {
        public IList<UserRoleDetails> Fetch(UserRoleInput userRoleInput)
        {
            string key = CreateCacheKey("" + userRoleInput.UserName);

            IList<UserRoleDetails> value = Cache.Get(key) as List<UserRoleDetails>;

            if (value == null)
            {
                var serializer = Resolve<ISerializer<UserRoleDetails>>();

                var datastore = Resolve<IUserRoleDatastore>();

                using (IDataReader reader = datastore.UserRole(userRoleInput))
                {
                    IList<UserRoleDetails> items = serializer.Deserialize(reader);

                    value = items;

                    Remember(key, value);
                }
            }

            return value;
        }
    }
}
