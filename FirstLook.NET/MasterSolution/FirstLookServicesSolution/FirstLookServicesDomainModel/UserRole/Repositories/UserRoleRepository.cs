﻿using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.UserRole.Model;
using FirstLook.FirstLookServices.DomainModel.UserRole.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.UserRole.Repositories.Gateways;

namespace FirstLook.FirstLookServices.DomainModel.UserRole.Repositories
{
    public class UserRoleRepository:IRepository
    {
        #region IRepository Members

        public IList<UserRoleDetails> UserRole(UserRoleInput userRole)
        {
            return new UserRoleGateway().Fetch(userRole);
        }

        #endregion
    }
}
