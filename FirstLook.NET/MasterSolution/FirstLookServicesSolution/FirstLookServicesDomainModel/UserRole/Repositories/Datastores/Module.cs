﻿using System;
using FirstLook.Common.Core.Registry;

namespace FirstLook.FirstLookServices.DomainModel.UserRole.Repositories.Datastores
{
    [Serializable]
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<IUserRoleDatastore, UserDataStore>(ImplementationScope.Shared);
        }
    }
}
