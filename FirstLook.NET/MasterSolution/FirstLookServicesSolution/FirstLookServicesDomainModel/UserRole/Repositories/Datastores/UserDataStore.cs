﻿using System.Data;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.UserRole.Repositories.Entities;



namespace FirstLook.FirstLookServices.DomainModel.UserRole.Repositories.Datastores
{
   public class UserDataStore:IUserRoleDatastore
    {
        #region Class Members

        private const string DatabaseName = "IMT";

        #endregion

        #region IStorePerformanceDatastore Members

        public IDataReader UserRole(UserRoleInput userRole)
        {
            using (IDbConnection connection = Database.Connection(DatabaseName))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "dbo.Fetch#UserRole";

                    Database.AddWithValue(command, "UserName", userRole.UserName, DbType.String);

                    var set = new DataSet();

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        var table = new DataTable("UserRole");

                        table.Load(reader);

                        set.Tables.Add(table);
                    }

                    return set.CreateDataReader();
                }
            }
        }

        #endregion
    }
}
