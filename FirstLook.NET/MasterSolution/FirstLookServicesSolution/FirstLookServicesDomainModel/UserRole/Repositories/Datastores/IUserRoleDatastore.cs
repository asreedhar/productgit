﻿using System.Data;
using FirstLook.FirstLookServices.DomainModel.UserRole.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.UserRole.Repositories.Datastores
{
    public interface IUserRoleDatastore
    {
        IDataReader UserRole(UserRoleInput userRole);
    }
}
