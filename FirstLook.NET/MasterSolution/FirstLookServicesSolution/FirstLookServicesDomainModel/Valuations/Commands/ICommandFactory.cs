﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.Valuations.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.Valuations.Commands
{
    public interface ICommandFactory
    {
        ICommand<FetchEquipmentActionsResultsDto, FetchEquipmentActionsArgumentsDto> CreateFetchEquipmentActionsCommand();

        ICommand<FetchBookValuationsResultsDto, IdentityContextDto<FetchBookValuationsArgumentsDto>> CreateFetchBookValuationsCommand();

    }
}
