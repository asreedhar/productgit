﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Valuations.Commands.TransferObjects
{
    [Serializable]
    public class EquipmentUserActionDto
    {
        public EquipmentActionTypeDto Action { get; set; }

        public string Equipment { get; set; }
    }
}
