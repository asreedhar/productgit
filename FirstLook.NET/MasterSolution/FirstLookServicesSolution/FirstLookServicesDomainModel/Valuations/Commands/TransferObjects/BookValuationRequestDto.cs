﻿using System;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.Valuations.Commands.TransferObjects
{
    [Serializable]
    public class BookValuationRequestDto
    {
        public string Book { get; set; }

        public int? Mileage { get; set; }

        public string TrimId { get; set; }

        public string ReturnId { get; set; }

        public IList<string> EquipmentSelected { get; set; }

        public IList<EquipmentUserActionDto> EquipmentUserActions { get; set; }
    }
}
