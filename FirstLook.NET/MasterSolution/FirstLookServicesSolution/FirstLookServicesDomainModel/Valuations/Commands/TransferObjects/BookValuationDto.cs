﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.FirstLookServices.DomainModel.Utility;


namespace FirstLook.FirstLookServices.DomainModel.Valuations.Commands.TransferObjects
{
    [Serializable]
    public class BookValuationDto
    {
        public string Book { get; set; }

        public string ReturnId { get; set; }

        public string CheckSum { get; set; }

        public List<ValuationDataDto> ValuationData { get; set; }

        public VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.VehicleConfigurationDto BlackBookVehicleConfiguration { get; set; }

        public VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.VehicleConfigurationDto KbbVehicleConfiguration { get; set; }

        public VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.VehicleConfigurationDto GalvesVehicleConfiguration { get; set; }

        public VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.VehicleConfigurationDto NadaVehicleConfiguration { get; set; }

        public int GetHashValue()
        {
            unchecked
            {
                int result = ValuationData.Aggregate(19, (current, valuationDataDto) => (current*397) ^ valuationDataDto.GetHashValue());

                result = (result * 397) ^ Helper.GetHashValue(Book);

                return result;
            }
        }

    }
}
