﻿using System;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.Valuations.Commands.TransferObjects.Envelopes
{
    [Serializable]

    public class FetchEquipmentActionsResultsDto
    {
        public List<EquipmentActionDto> EquipmentActions { get; set; }
    }
}
