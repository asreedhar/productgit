﻿using System;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.Valuations.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class FetchBookValuationsArgumentsDto
    {
        public List<BookValuationRequestDto> BookValuationRequests { get; set; }

        public int DealerId { get; set; }
    }
}
