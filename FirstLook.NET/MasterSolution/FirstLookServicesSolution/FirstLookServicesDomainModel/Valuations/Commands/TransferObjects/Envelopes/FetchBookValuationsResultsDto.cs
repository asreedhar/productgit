﻿using System;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.Valuations.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class FetchBookValuationsResultsDto
    {
        public List<BookValuationDto> BookValuations { get; set; }
    }
}
