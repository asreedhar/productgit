﻿namespace FirstLook.FirstLookServices.DomainModel.Valuations.Commands.TransferObjects
{
    public enum EquipmentActionTypeDto
    {
        Add,
        Remove,
    }
}
