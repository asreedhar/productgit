﻿using System;
using FirstLook.FirstLookServices.DomainModel.Utility;

namespace FirstLook.FirstLookServices.DomainModel.Valuations.Commands.TransferObjects
{
    [Serializable]
    public class ValuationDto
    {
        public string Description { get; set; }

        public int Value { get; set; }

        public int GetHashValue()
        {
            unchecked
            {
                int result = 19;

                result = (result * 397) ^ Helper.GetHashValue(Description);

                result = (result * 397) ^ Value;

                return result;
            }
        }
    }
}
