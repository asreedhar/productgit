﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.FirstLookServices.DomainModel.Utility;

namespace FirstLook.FirstLookServices.DomainModel.Valuations.Commands.TransferObjects
{
    [Serializable]
    public class ValuationDataDto
    {
        public string Market { get; set; }

        public string Condition { get; set; }

        public List<ValuationDto> Valuations { get; set; }

        public int GetHashValue()
        {
            unchecked
            {
                int result = 19;

                result = (result * 397) ^ Helper.GetHashValue(Market);

                result = (result * 397) ^ Helper.GetHashValue(Condition);

                return Valuations.Aggregate(result, (current, valuation) => (current*397) ^ valuation.GetHashValue());
            }
        }
    }
}
