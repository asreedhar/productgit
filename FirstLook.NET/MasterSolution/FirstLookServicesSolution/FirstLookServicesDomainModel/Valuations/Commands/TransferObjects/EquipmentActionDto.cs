﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Valuations.Commands.TransferObjects
{
    [Serializable]
    public class EquipmentActionDto
    {
        public string Action { get; set; }

        public string Description { get; set; }
    }
}
