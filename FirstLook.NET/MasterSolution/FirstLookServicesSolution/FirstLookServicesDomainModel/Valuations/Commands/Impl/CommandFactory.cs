﻿using System;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.Valuations.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.Valuations.Commands.Impl
{
    class CommandFactory : ICommandFactory
    {
        public ICommand<FetchEquipmentActionsResultsDto, FetchEquipmentActionsArgumentsDto> CreateFetchEquipmentActionsCommand()
        {
            return new FetchEquipmentActionsCommand();
        }

        public ICommand<FetchBookValuationsResultsDto, IdentityContextDto<FetchBookValuationsArgumentsDto>> CreateFetchBookValuationsCommand()
        {
            return new FetchBookValuationsCommand();
        }
    }
}
