﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.Valuations.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Valuations.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.Valuations.Commands.Impl
{
    public class FetchEquipmentActionsCommand : ICommand<FetchEquipmentActionsResultsDto, FetchEquipmentActionsArgumentsDto>
    {
        public FetchEquipmentActionsResultsDto Execute(FetchEquipmentActionsArgumentsDto parameters)
        {
            IList<string> actions = Enum.GetNames(typeof(EquipmentActionTypeDto));

            IList<EquipmentActionDto> equipmentActionDtos = actions.Select(action => new EquipmentActionDto
                                                                                         {
                                                                                             //This is cheap... I know.
                                                                                             Action = action.Substring(0, 1), 
                                                                                             Description = action
                                                                                         }).ToList();

            return new FetchEquipmentActionsResultsDto
            {
                EquipmentActions = (List<EquipmentActionDto>) equipmentActionDtos
            };
        }
    }
}
