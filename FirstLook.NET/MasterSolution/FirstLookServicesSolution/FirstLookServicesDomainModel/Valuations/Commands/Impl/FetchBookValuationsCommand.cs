﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.SimpleBooks;
using FirstLook.FirstLookServices.DomainModel.Books.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.FirstLookServices.DomainModel.Utility;
using FirstLook.FirstLookServices.DomainModel.Valuations.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Valuations.Commands.TransferObjects.Envelopes;
using System.Linq;

#region bb

using BlackBook_OptionActionTypeDto = FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.OptionActionTypeDto;
using BlackBook_OptionActionDto = FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.OptionActionDto;
using BlackBook_VehicleConfigurationDto = FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.VehicleConfigurationDto;
using BlackBook_InitialValuationArgumentsDto = FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes.InitialValuationArgumentsDto;
using BlackBook_InitialValuationResultsDto = FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes.InitialValuationResultsDto;
using BlackBook_AdjustedValuationArgumentsDto = FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes.AdjustedValuationArgumentsDto;
using BlackBook_AdjustedValuationResultsDto = FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.Envelopes.AdjustedValuationResultsDto;
#endregion

#region kbb
using KBB_OptionActionTypeDto = FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.OptionActionTypeDto;
using KBB_OptionActionDto = FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.OptionActionDto;
using KBB_VehicleConfigurationDto = FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.VehicleConfigurationDto;
using KBB_InitialValuationArgumentsDto = FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes.InitialValuationArgumentsDto;
using KBB_InitialValuationResultsDto = FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes.InitialValuationResultsDto;
using KBB_AdjustedValuationArgumentsDto = FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes.AdjustedValuationArgumentsDto;
using KBB_AdjustedValuationResultsDto = FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.Envelopes.AdjustedValuationResultsDto;
#endregion

#region galves
using Galves_OptionActionTypeDto = FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.AdjustmentActionTypeDto;
using Galves_OptionActionDto = FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.AdjustmentActionDto;
using Galves_VehicleConfigurationDto = FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.VehicleConfigurationDto;
using Galves_InitialValuationArgumentsDto = FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes.InitialValuationArgumentsDto;
using Galves_InitialValuationResultsDto = FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes.InitialValuationResultsDto;
using Galves_AdjustedValuationArgumentsDto = FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes.AdjustedValuationArgumentsDto;
using Galves_AdjustedValuationResultsDto = FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes.AdjustedValuationResultsDto;
using Galves_StatesArgumentsDto = FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes.StatesArgumentsDto;
using Galves_StatesResultsDto = FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.Envelopes.StatesResultsDto;
#endregion

#region Nada
using Nada_OptionActionTypeDto = FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.AccessoryActionTypeDto;
using Nada_OptionActionDto = FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.AccessoryActionDto;
using Nada_VehicleConfigurationDto = FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.VehicleConfigurationDto;
using Nada_InitialValuationArgumentsDto = FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes.InitialValuationArgumentsDto;
using Nada_InitialValuationResultsDto = FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes.InitialValuationResultsDto;
using Nada_AdjustedValuationArgumentsDto = FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes.AdjustedValuationArgumentsDto;
using Nada_AdjustedValuationResultsDto = FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes.AdjustedValuationResultsDto;
#endregion

namespace FirstLook.FirstLookServices.DomainModel.Valuations.Commands.Impl
{
    public class FetchBookValuationsCommand : ICommand<FetchBookValuationsResultsDto, IdentityContextDto<FetchBookValuationsArgumentsDto>>
    {
        public FetchBookValuationsResultsDto Execute(IdentityContextDto<FetchBookValuationsArgumentsDto> parameters)
        {
            #region initialization

            /*
             * This is just initialization code.  Get access to the required command factories for execution. We will be interfacing
             * with the ClientDomainModel and the VehicleValuationGuideDomainModel.
             */

            IdentityDto identityDto = parameters.Identity;

            var clientClientFactory = RegistryFactory.GetResolver().Resolve<Client.DomainModel.Clients.Commands.ICommandFactory>();

            var kbbFactory = RegistryFactory.GetResolver().Resolve<VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.ICommandFactory>();

            var bbFactory = RegistryFactory.GetResolver().Resolve<VehicleValuationGuide.DomainModel.BlackBook.Commands.ICommandFactory>();

            var nadaFactory = RegistryFactory.GetResolver().Resolve<VehicleValuationGuide.DomainModel.Nada.Commands.ICommandFactory>();

            var galvesFactory = RegistryFactory.GetResolver().Resolve<VehicleValuationGuide.DomainModel.Galves.Commands.ICommandFactory>();

            #endregion

            #region get broker

            /*
             * We have a dealerId from the parameters passed in, but we're going to need more information about the dealer
             * like the addres of the dealer and a broker handle (this is just a guid representing the dealer).  In order to 
             * get more information about the dealer we need to reach into the ClientDomainModel, specically the BrokerForClient
             * Command, which (given a dealerId), we can retrieve client information.
             */


            var clientIdentityContext = new IdentityContextDto<BrokerForClientArgumentsDto>
            {
                Identity = identityDto,
                Arguments = new BrokerForClientArgumentsDto
                {
                    AuthorityName = identityDto.AuthorityName,
                    Client = new ClientDto
                    {
                        Id = parameters.Arguments.DealerId
                    }
                }
            };

            ICommand<BrokerForClientResultsDto, IdentityContextDto<BrokerForClientArgumentsDto>> brokerForClientCommand = clientClientFactory.CreateBrokerForClientCommand();

            BrokerForClientResultsDto brokerForClientResultsDto = brokerForClientCommand.Execute(clientIdentityContext);

            if ((brokerForClientResultsDto == null) || (brokerForClientResultsDto.Broker == null))
            {
                throw new InvalidInputException("dealer", "Invalid dealer ID", "null broker for dealer id = " + parameters.Arguments.DealerId);
            }

            BrokerDto brokerDto = brokerForClientResultsDto.Broker;


            //get the dealer books

            var b = new BooksRepository();
            var dealerBookResults = b.GetBooks(parameters.Arguments.DealerId);

            #endregion

            /*
             * At this point we are ready to start performing the valuations for the bookvaluationrequests passed in on the
             * call.  For each book valuation request passed in, determine which book we are evaluating using the switch stmt
             * below.  For each book process the valuation.  Establish an empty list initially to return all of the resultant
             * book valuations in.
             */
            IList<BookValuationDto> bookValuationDtos = new List<BookValuationDto>();

            foreach (BookValuationRequestDto bookValuationRequestDto in parameters.Arguments.BookValuationRequests)
            {
                //is this a valid book for the dealer?
                if (!dealerBookResults.Any(i => i.Desc.ToLower().Equals(bookValuationRequestDto.Book.ToLower())))
                {
                    throw new UserAccessException(bookValuationRequestDto.Book, "Forbidden",
                                                  "User does not have access to book: " + bookValuationRequestDto.Book);
                }

                int? mileage = bookValuationRequestDto.Mileage;

                AddressDto addressDto = brokerDto.Client.Address;

                switch (bookValuationRequestDto.Book)
                {
                    /*
                     * Basically the logic is the same for all of the books.  So I will write some general documentation
                     * here that was applied to each book in the switch statement.
                     * 
                     * First we need an initial valuation for the vehicle (the vehicle is identified by the passed in trimId 
                     * on each bookvaluationrequest).  It will return to us the default selected options
                     * among some other vehicle information presented in the form of a VehicleConfigurationDto.  After 
                     * we get this initial valuation we can replay the list of Actions performed by the user in the order that 
                     * they occurred. This list of actions is passed in on the parameters via the EquipmentUserActions list.
                     * 
                     * For each action performed by the user we call the AdjustedValuationCommand.  This will take a single
                     * action (ie user added/removed option x, identified by some optionId), and apply it to the passed in 
                     * VehicleConfigurationDto.  On each return from the AdjustedValuation command it will send back a 
                     * new VehicleConfigurationDto, updated with a new price table based on the action that was just 
                     * processed.  This VehicleConfigurationDto will then be passed in with the next action to process
                     * and so on until all actions are replayed.
                     * 
                     * At the end of replaying the actions the final VehicleConfiguration returned should have a list of
                     * all selected equipment and a final price table that we will send back to the user.  This price
                     * table contains the valuations the caller is interested in.  For example one element might be the "Final" 
                     * price for a "Trade-In" with a condition of "Good"... these are book specific categorizations.
                     * 
                     * We need to check to make sure the final list of selected equipment returned agrees with the final list of
                     * equipment selected specified by the user via the EquipmentSelected value passed in on the call to this
                     * command.
                     * 
                     * If the lists agree, then we calculate a hash (see the GetHash method used in the code below) of the
                     * valuation data, and send that back as a "checksum".  Follow up calls can use that hash to determine
                     * if valuations have changed over a period... (ie before saving an appraisal you'd want to call this
                     * to make sure the appraisal was created based on currently accurate book valuations).
                     * 
                     * At the end of all this return back the set of book valuations and the hash.
                     */

                    #region bb
                    //todo: bb or blackbook
                    case "blackbook":
                        {
                            var initialValuationCommand = bbFactory.CreateInitialValuationCommand();

                            var initialValuationArguments = new BlackBook_InitialValuationArgumentsDto
                                                                {
                                                                    HasMileage = mileage.HasValue,
                                                                    Mileage = mileage.GetValueOrDefault(),
                                                                    Uvc = bookValuationRequestDto.TrimId,
                                                                    State = addressDto.State
                                                                };

                            BlackBook_InitialValuationResultsDto initialValuationResultsDto;

                            // If the vehicleID is not valid, a null pointer exception is thrown (Time kept me from fixing the called code).
                            try
                            {
                                initialValuationResultsDto = initialValuationCommand.Execute(initialValuationArguments);
                            }
                            catch (ArgumentNullException ex)
                            {
                                throw ExceptionFilters.InvalidInputTrimId(ex, bookValuationRequestDto.TrimId);
                            }

                            BlackBook_VehicleConfigurationDto vehicleConfiguration = initialValuationResultsDto.VehicleConfiguration;

                            BlackBook_AdjustedValuationResultsDto adjustedValuationResults = null;

                            foreach (EquipmentUserActionDto equipmentUserActionDto in bookValuationRequestDto.EquipmentUserActions)
                            {
                                var adjustValuationCommand = bbFactory.CreateAdjustedValuationCommand();

                                BlackBook_OptionActionTypeDto optionActionTypeDto;

                                switch (equipmentUserActionDto.Action)
                                {
                                    case EquipmentActionTypeDto.Add:
                                        {
                                            optionActionTypeDto = BlackBook_OptionActionTypeDto.Add;
                                            break;
                                        }
                                    case EquipmentActionTypeDto.Remove:
                                        {
                                            optionActionTypeDto = BlackBook_OptionActionTypeDto.Remove;
                                            break;
                                        }
                                    default:
                                        {
                                            optionActionTypeDto = BlackBook_OptionActionTypeDto.Undefined;
                                            break;
                                        }
                                }

                                var adjustedValuationArgs = new BlackBook_AdjustedValuationArgumentsDto
                                                                {
                                                                    HasMileage = mileage.HasValue,
                                                                    Mileage = mileage.GetValueOrDefault(),
                                                                    State = addressDto.State,
                                                                    VehicleConfiguration = vehicleConfiguration,
                                                                    OptionAction = new BlackBook_OptionActionDto
                                                                            {
                                                                                ActionType = optionActionTypeDto,
                                                                                Uoc = equipmentUserActionDto.Equipment
                                                                            }
                                                                };

                                try
                                {
                                    adjustedValuationResults = adjustValuationCommand.Execute(adjustedValuationArgs);
                                }
                                catch (ArgumentNullException ex)
                                {
                                    throw ExceptionFilters.InvalidInputEquipmentOptions(ex, equipmentUserActionDto.Equipment);
                                }

                                vehicleConfiguration = adjustedValuationResults.VehicleConfiguration;

                            }

                            BlackBook_VehicleConfigurationDto vehicleConfigurationDto;

                            try
                            {
                                //adjustments were made
                                if (adjustedValuationResults != null)
                                {
                                    IList<string> selectedOptions =
                                        (adjustedValuationResults.VehicleConfiguration.OptionStates.FindAll(option => option.Selected))
                                        .Select(option => option.Uoc)
                                        .ToList();

                                    CompareOptions(selectedOptions, bookValuationRequestDto.EquipmentSelected);

                                    vehicleConfigurationDto = adjustedValuationResults.VehicleConfiguration;
                                }
                                else
                                {
                                    IList<string> selectedOptions =
                                        (initialValuationResultsDto.VehicleConfiguration.OptionStates.FindAll(option => option.Selected))
                                        .Select(option => option.Uoc)
                                        .ToList();

                                    CompareOptions(selectedOptions, bookValuationRequestDto.EquipmentSelected);

                                    vehicleConfigurationDto = initialValuationResultsDto.VehicleConfiguration;
                                }
                            }
                            catch (ArgumentNullException ex)
                            {
                                throw ExceptionFilters.InvalidInputEquipmentOptions(ex, string.Join(",", bookValuationRequestDto.EquipmentSelected.ToArray()));
                            }

                            //if we get here we passed the equipment selection test

                            var valuationDto = new BookValuationDto
                                                   {
                                                       BlackBookVehicleConfiguration = vehicleConfigurationDto,
                                                       Book = bookValuationRequestDto.Book,
                                                       ReturnId = bookValuationRequestDto.ReturnId,
                                                       ValuationData = (List<ValuationDataDto>)(adjustedValuationResults != null
                                                                ? Mapper.ToValuationData(adjustedValuationResults.Tables)
                                                                : Mapper.ToValuationData(initialValuationResultsDto.Tables))
                                                   };

                            valuationDto.CheckSum = valuationDto.GetHashValue().ToString();

                            bookValuationDtos.Add(valuationDto);

                            break;
                        }

                    #endregion

                    #region kbb

                    case "kbb":
                        {
                            var initialValuationCommand = kbbFactory.CreateInitialValuationCommand();

                            var initialValuationArguments = new KBB_InitialValuationArgumentsDto
                                                                {
                                                                    HasMileage = mileage.HasValue,
                                                                    Mileage = mileage.GetValueOrDefault(),
                                                                    VehicleId = Convert.ToInt32(bookValuationRequestDto.TrimId),
                                                                    ZipCode = addressDto.ZipCode,
                                                                    DealerId = parameters.Arguments.DealerId,
                                                                    EquipmentSelected = bookValuationRequestDto.EquipmentSelected
                                                                };

                            KBB_InitialValuationResultsDto initialValuationResultsDto;

                            // If the vehicleID is not valid, a null pointer exception is thrown (Time kept me from fixing the called code).
                            try
                            {
                                initialValuationResultsDto = initialValuationCommand.Execute(initialValuationArguments);
                            }
                            catch (ArgumentNullException ex)
                            {
                                throw ExceptionFilters.InvalidInputTrimId(ex, bookValuationRequestDto.TrimId);
                            }

                            KBB_VehicleConfigurationDto vehicleConfiguration = initialValuationResultsDto.VehicleConfiguration;

                            KBB_AdjustedValuationResultsDto adjustedValuationResults=null;

                            if (bookValuationRequestDto.EquipmentUserActions.Count > 0)
                            {
                                adjustedValuationResults = new KBB_AdjustedValuationResultsDto();
                                adjustedValuationResults.EquipmentSelected = bookValuationRequestDto.EquipmentSelected;
                            }

                            foreach (EquipmentUserActionDto equipmentUserActionDto in bookValuationRequestDto.EquipmentUserActions)
                            {
                                var adjustValuationCommand = kbbFactory.CreateAdjustedValuationCommand();

                                KBB_OptionActionTypeDto optionActionTypeDto;

                                switch (equipmentUserActionDto.Action)
                                {
                                    case EquipmentActionTypeDto.Add:
                                        {
                                            optionActionTypeDto = KBB_OptionActionTypeDto.Add;
                                            break;
                                        }
                                    case EquipmentActionTypeDto.Remove:
                                        {
                                            optionActionTypeDto = KBB_OptionActionTypeDto.Remove;
                                            break;
                                        }
                                    default:
                                        {
                                            optionActionTypeDto = KBB_OptionActionTypeDto.Undefined;
                                            break;
                                        }
                                }

                                var adjustedValuationArgs = new KBB_AdjustedValuationArgumentsDto
                                                                {
                                                                    HasMileage = mileage.HasValue,
                                                                    Mileage = mileage.GetValueOrDefault(),
                                                                    ZipCode = addressDto.ZipCode,
                                                                    VehicleConfiguration = vehicleConfiguration,
                                                                    DealerId = parameters.Arguments.DealerId,
                                                                    OptionAction =
                                                                        new KBB_OptionActionDto
                                                                            {
                                                                                ActionType = optionActionTypeDto,
                                                                                OptionId = Convert.ToInt32(equipmentUserActionDto.Equipment)
                                                                            },
                                                                    EquipmentSelected = adjustedValuationResults.EquipmentSelected
                                                                };

                                try
                                {
                                    adjustedValuationResults = adjustValuationCommand.Execute(adjustedValuationArgs);
                                }
                                catch (ArgumentNullException ex)
                                {
                                    throw ExceptionFilters.InvalidInputEquipmentOptions(ex, equipmentUserActionDto.Equipment);
                                }

                                vehicleConfiguration = adjustedValuationResults.VehicleConfiguration;
                            }

                            KBB_VehicleConfigurationDto vehicleConfigurationDto;

                            try
                            {
                                //adjustments were made
                                if (adjustedValuationResults != null)
                                {
                                    IList<string> selectedOptions =
                                        (adjustedValuationResults.VehicleConfiguration.OptionStates.FindAll(option => option.Selected))
                                        .Select(option => option.OptionId.ToString())
                                        .ToList();
                                    //CompareOptions(selectedOptions, bookValuationRequestDto.EquipmentSelected);    

                                    vehicleConfigurationDto = adjustedValuationResults.VehicleConfiguration;
                                }
                                else
                                {
                                    IList<string> selectedOptions =
                                        (initialValuationResultsDto.VehicleConfiguration.OptionStates.FindAll(option => option.Selected))
                                        .Select(option => option.OptionId.ToString())
                                        .ToList();

                                    //CompareOptions(selectedOptions, bookValuationRequestDto.EquipmentSelected);

                                    vehicleConfigurationDto = initialValuationResultsDto.VehicleConfiguration;
                                }
                            }
                            catch (ArgumentNullException ex)
                            {
                                throw ExceptionFilters.InvalidInputEquipmentOptions(ex, string.Join(",", bookValuationRequestDto.EquipmentSelected.ToArray()));
                            }

                            //if drivetrain, engine, or transmission is missing we have an invalid configuration, so throw an exception
                            if (vehicleConfigurationDto.IsMissingDriveTrain || vehicleConfigurationDto.IsMissingEngine || vehicleConfigurationDto.IsMissingTransmission)
                            {
                                if (vehicleConfigurationDto.IsMissingDriveTrain)
                                {
                                    throw new InvalidInputException("equipment", "Invalid Value(s)", "Missing drivetrain equipment selection.");
                                }

                                if (vehicleConfigurationDto.IsMissingEngine)
                                {
                                    throw new InvalidInputException("equipment", "Invalid Value(s)", "Missing engine equipment selection.");
                                }

                                if (vehicleConfigurationDto.IsMissingTransmission)
                                {
                                    throw new InvalidInputException("equipment", "Invalid Value(s)", "Missing transmission equipment selection.");
                                }
                            }

                            //if we get here we passed the equipment selection test
                            var valuationDto = new BookValuationDto
                                                   {
                                                       KbbVehicleConfiguration = vehicleConfigurationDto,
                                                       Book = bookValuationRequestDto.Book,
                                                       ReturnId = bookValuationRequestDto.ReturnId,
                                                       ValuationData = (List<ValuationDataDto>)
                                                           (adjustedValuationResults != null
                                                                ? Mapper.ToValuationData(adjustedValuationResults.Tables)
                                                                : Mapper.ToValuationData(initialValuationResultsDto.Tables))
                                                   };

                            valuationDto.CheckSum = valuationDto.GetHashValue().ToString();

                            bookValuationDtos.Add(valuationDto);

                            break;
                        }

                    #endregion

                    #region galves

                    case "galves":
                        {
                            var initialValuationCommand = galvesFactory.CreateInitialValuationCommand();

                            var statesCommand = galvesFactory.CreateStatesCommand();

                            var states = (statesCommand.Execute(new Galves_StatesArgumentsDto())).States;

                            var state = StateMapper.Identify(addressDto.State.ToUpper());

                            string fullState = states.Find(x => Equals(x.Name.ToLower(), state.Name.ToLower())).Name;

                            var initialValuationArguments = new Galves_InitialValuationArgumentsDto
                                                                {
                                                                    HasMileage = mileage.HasValue,
                                                                    Mileage = mileage.GetValueOrDefault(),
                                                                    VehicleId = Convert.ToInt32(bookValuationRequestDto.TrimId),
                                                                    State = fullState
                                                                };

                            Galves_InitialValuationResultsDto initialValuationResultsDto;

                            // If the vehicleID is not valid, a null pointer exception is thrown (Time kept me from fixing the called code).
                            try
                            {
                                initialValuationResultsDto = initialValuationCommand.Execute(initialValuationArguments);
                            }
                            catch (ArgumentNullException ex)
                            {
                                throw ExceptionFilters.InvalidInputTrimId(ex, bookValuationRequestDto.TrimId);
                            }

                            Galves_VehicleConfigurationDto vehicleConfiguration = initialValuationResultsDto.VehicleConfiguration;

                            Galves_AdjustedValuationResultsDto adjustedValuationResults = null;

                            foreach (EquipmentUserActionDto equipmentUserActionDto in bookValuationRequestDto.EquipmentUserActions)
                            {
                                var adjustValuationCommand = galvesFactory.CreateAdjustedValuationCommand();

                                Galves_OptionActionTypeDto optionActionTypeDto;

                                switch (equipmentUserActionDto.Action)
                                {
                                    case EquipmentActionTypeDto.Add:
                                        {
                                            optionActionTypeDto = Galves_OptionActionTypeDto.Add;
                                            break;
                                        }
                                    case EquipmentActionTypeDto.Remove:
                                        {
                                            optionActionTypeDto = Galves_OptionActionTypeDto.Remove;
                                            break;
                                        }
                                    default:
                                        {
                                            optionActionTypeDto = Galves_OptionActionTypeDto.Undefined;
                                            break;
                                        }
                                }

                                var adjustedValuationArgs = new Galves_AdjustedValuationArgumentsDto
                                                                {
                                                                    HasMileage = mileage.HasValue,
                                                                    Mileage = mileage.GetValueOrDefault(),
                                                                    State = fullState,
                                                                    VehicleConfiguration = vehicleConfiguration,
                                                                    AdjustmentAction = new Galves_OptionActionDto
                                                                            {
                                                                                ActionType = optionActionTypeDto,
                                                                                AdjustmentName = equipmentUserActionDto.Equipment
                                                                            }
                                                                };

                                try
                                {
                                    adjustedValuationResults = adjustValuationCommand.Execute(adjustedValuationArgs);
                                }
                                catch (ArgumentNullException ex)
                                {
                                    throw ExceptionFilters.InvalidInputEquipmentOptions(ex, equipmentUserActionDto.Equipment);
                                }

                                vehicleConfiguration = adjustedValuationResults.VehicleConfiguration;
                            }

                            Galves_VehicleConfigurationDto vehicleConfigurationDto;

                            try
                            {
                                //adjustments were made
                                if (adjustedValuationResults != null)
                                {
                                    IList<string> selectedOptions =
                                        (adjustedValuationResults.VehicleConfiguration.AdjustmentStates.FindAll(option => option.Selected))
                                        .Select(option => option.AdjustmentName)
                                        .ToList();

                                    CompareOptions(selectedOptions, bookValuationRequestDto.EquipmentSelected);

                                    vehicleConfigurationDto = adjustedValuationResults.VehicleConfiguration;
                                }
                                else
                                {
                                    IList<string> selectedOptions =
                                        (initialValuationResultsDto.VehicleConfiguration.AdjustmentStates.FindAll(option => option.Selected))
                                        .Select(option => option.AdjustmentName)
                                        .ToList();

                                    CompareOptions(selectedOptions, bookValuationRequestDto.EquipmentSelected);

                                    vehicleConfigurationDto = initialValuationResultsDto.VehicleConfiguration;
                                }
                            }
                            catch (ArgumentNullException ex)
                            {
                                throw ExceptionFilters.InvalidInputEquipmentOptions(ex, string.Join(",", bookValuationRequestDto.EquipmentSelected.ToArray()));
                            }

                            //if we get here we passed the equipment selection test

                            var valuationDto = new BookValuationDto
                                                                {
                                                                    GalvesVehicleConfiguration = vehicleConfigurationDto,
                                                                    Book = bookValuationRequestDto.Book,
                                                                    ReturnId = bookValuationRequestDto.ReturnId,
                                                                    ValuationData = (List<ValuationDataDto>)
                                                                        (adjustedValuationResults != null
                                                                             ? Mapper.ToValuationData(adjustedValuationResults.Table)
                                                                             : Mapper.ToValuationData(initialValuationResultsDto.Table))
                                                                };

                            valuationDto.CheckSum = valuationDto.GetHashValue().ToString();

                            bookValuationDtos.Add(valuationDto);

                            break;
                        }

                    #endregion

                    #region nada

                    case "nada":
                        {

                            var initialValuationCommand = nadaFactory.CreateInitialValuationCommand();

                            var initialValuationArguments = new Nada_InitialValuationArgumentsDto
                                                                {
                                                                    HasMileage = mileage.HasValue,
                                                                    Mileage = mileage.GetValueOrDefault(),
                                                                    Uid = Convert.ToInt32(bookValuationRequestDto.TrimId),
                                                                    State = addressDto.State
                                                                };

                            Nada_InitialValuationResultsDto initialValuationResultsDto;

                            // If the vehicleID is not valid, a null pointer exception is thrown (Time kept me from fixing the called code).
                            try
                            {
                                initialValuationResultsDto = initialValuationCommand.Execute(initialValuationArguments);
                            }
                            catch (ArgumentNullException ex)
                            {
                                throw ExceptionFilters.InvalidInputTrimId(ex, bookValuationRequestDto.TrimId);
                            }

                            Nada_VehicleConfigurationDto vehicleConfiguration = initialValuationResultsDto.VehicleConfiguration;

                            Nada_AdjustedValuationResultsDto adjustedValuationResults = null;

                            foreach (EquipmentUserActionDto equipmentUserActionDto in bookValuationRequestDto.EquipmentUserActions)
                            {
                                var adjustValuationCommand = nadaFactory.CreateAdjustedValuationCommand();

                                Nada_OptionActionTypeDto optionActionTypeDto;

                                switch (equipmentUserActionDto.Action)
                                {
                                    case EquipmentActionTypeDto.Add:
                                        {
                                            optionActionTypeDto = Nada_OptionActionTypeDto.Add;
                                            break;
                                        }
                                    case EquipmentActionTypeDto.Remove:
                                        {
                                            optionActionTypeDto = Nada_OptionActionTypeDto.Remove;
                                            break;
                                        }
                                    default:
                                        {
                                            optionActionTypeDto = Nada_OptionActionTypeDto.Undefined;
                                            break;
                                        }
                                }

                                var adjustedValuationArgs = new Nada_AdjustedValuationArgumentsDto
                                                                {
                                                                    HasMileage = mileage.HasValue,
                                                                    Mileage = mileage.GetValueOrDefault(),
                                                                    State = addressDto.State,
                                                                    VehicleConfiguration = vehicleConfiguration,
                                                                    AccessoryAction =
                                                                        new Nada_OptionActionDto
                                                                            {
                                                                                ActionType = optionActionTypeDto,
                                                                                AccessoryCode = equipmentUserActionDto.Equipment
                                                                            }
                                                                };

                                try
                                {
                                    adjustedValuationResults = adjustValuationCommand.Execute(adjustedValuationArgs);
                                }
                                catch (ArgumentNullException ex)
                                {
                                    throw ExceptionFilters.InvalidInputEquipmentOptions(ex, equipmentUserActionDto.Equipment);
                                }

                                vehicleConfiguration = adjustedValuationResults.VehicleConfiguration;
                            }

                            Nada_VehicleConfigurationDto vehicleConfigurationDto;

                            try
                            {
                                //adjustments were made
                                if (adjustedValuationResults != null)
                                {
                                    IList<string> selectedOptions =
                                        (adjustedValuationResults.VehicleConfiguration.AccessoryStates.FindAll(option => option.Selected))
                                        .Select(option => option.AccessoryCode)
                                        .ToList();

                                    CompareOptions(selectedOptions, bookValuationRequestDto.EquipmentSelected);

                                    vehicleConfigurationDto = adjustedValuationResults.VehicleConfiguration;
                                }
                                else
                                {
                                    IList<string> selectedOptions =
                                        (initialValuationResultsDto.VehicleConfiguration.AccessoryStates.FindAll(option => option.Selected))
                                        .Select(option => option.AccessoryCode)
                                        .ToList();

                                    CompareOptions(selectedOptions, bookValuationRequestDto.EquipmentSelected);

                                    vehicleConfigurationDto = initialValuationResultsDto.VehicleConfiguration;
                                }
                            }
                            catch (ArgumentNullException ex)
                            {
                                throw ExceptionFilters.InvalidInputEquipmentOptions(ex, string.Join(",", bookValuationRequestDto.EquipmentSelected.ToArray()));
                            }

                            //if we get here we passed the equipment selection test

                            var valuationDto = new BookValuationDto
                                                   {
                                                       NadaVehicleConfiguration = vehicleConfigurationDto,
                                                       Book = bookValuationRequestDto.Book,
                                                       ReturnId = bookValuationRequestDto.ReturnId,
                                                       ValuationData = (List<ValuationDataDto>)
                                                           (adjustedValuationResults != null
                                                                ? Mapper.ToValuationData(adjustedValuationResults.Table)
                                                                : Mapper.ToValuationData(initialValuationResultsDto.Table))
                                                   };

                            valuationDto.CheckSum = valuationDto.GetHashValue().ToString();

                            bookValuationDtos.Add(valuationDto);

                            break;
                        }

                    #endregion
                }
            }

            return new FetchBookValuationsResultsDto
                       {
                           BookValuations = (List<BookValuationDto>)bookValuationDtos
                       };
        }

        private static void CompareOptions(IEnumerable<string> l1, IEnumerable<string> l2)
        {
            var result = l1.Intersect(l2, StringComparer.InvariantCultureIgnoreCase);

            if (result.Count() != l2.Count())
            {
                throw new InvalidInputException("Equipment", "Invalid Value(s)", "Could not find option for Equipment ID(s) = " + String.Join(", ", l2.Except(l1).ToList()));
            }
        }
    }
}
