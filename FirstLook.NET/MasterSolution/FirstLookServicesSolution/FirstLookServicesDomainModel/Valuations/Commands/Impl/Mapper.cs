﻿using System;
using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.Valuations.Commands.TransferObjects; 

#region bb
using BlackBook_PriceTablesDto = FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.PriceTablesDto;
using BlackBook_PriceTableRowDto = FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.PriceTableRowDto;
using BlackBook_PriceTableRowTypeDto = FirstLook.VehicleValuationGuide.DomainModel.BlackBook.Commands.TransferObjects.PriceTableRowTypeDto;
#endregion

#region kbb
using KBB_PriceTablesDto = FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.PriceTablesDto;
using KBB_PriceTableRowDto = FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.PriceTableRowDto;
using KBB_PriceTableRowTypeDto = FirstLook.VehicleValuationGuide.DomainModel.KelleyBlueBook.Commands.TransferObjects.PriceTableRowTypeDto;
#endregion

#region nada
using Nada_PriceTablesDto = FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.PriceTableDto;
using Nada_PriceTableRowDto = FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.PriceTableRowDto;
using Nada_PriceTableRowTypeDto = FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.PriceTableRowTypeDto;
#endregion

#region galves
using Galves_PriceTablesDto = FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.PriceTableDto;
using Galves_PriceTableRowDto = FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.PriceTableRowDto;
using Galves_PriceTableRowTypeDto = FirstLook.VehicleValuationGuide.DomainModel.Galves.Commands.TransferObjects.PriceTableRowTypeDto;
#endregion

namespace FirstLook.FirstLookServices.DomainModel.Valuations.Commands.Impl
{
    //todo: this class needs to be refactored.
    public static class Mapper
    {
        #region bb

        public static IList<ValuationDataDto> ToValuationData(BlackBook_PriceTablesDto priceTables)
        {
            IList<ValuationDataDto> valuationDataDtos = new List<ValuationDataDto>();

            //Retail
            if (priceTables.Retail != null)
            {
                var retailAvg = new ValuationDataDto
                                    {
                                        Valuations = new List<ValuationDto>(),
                                        Market = "Retail",
                                        Condition = "Average"
                                    };

                foreach (BlackBook_PriceTableRowDto priceTableRowDto in priceTables.Retail.Rows)
                {
                    if (priceTableRowDto.Prices.HasAverage)
                    {
                        var valuationDto = new ValuationDto
                                                {
                                                    Description =
                                                        Enum.GetName(
                                                            typeof(BlackBook_PriceTableRowTypeDto),
                                                            priceTableRowDto.RowType),
                                                    Value = priceTableRowDto.Prices.Average
                                                };

                        retailAvg.Valuations.Add(valuationDto);
                    }
                }

                if (retailAvg.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(retailAvg);
                }

                var retailClean = new ValuationDataDto
                                        {
                                            Valuations = new List<ValuationDto>(),
                                            Market = "Retail",
                                            Condition = "Clean"
                                        };

                foreach (BlackBook_PriceTableRowDto priceTableRowDto in priceTables.Retail.Rows)
                {
                    if (priceTableRowDto.Prices.HasClean)
                    {
                        var valuationDto = new ValuationDto
                                                {
                                                    Description =
                                                        Enum.GetName(
                                                            typeof(BlackBook_PriceTableRowTypeDto),
                                                            priceTableRowDto.RowType),
                                                    Value = priceTableRowDto.Prices.Clean
                                                };

                        retailClean.Valuations.Add(valuationDto);
                    }
                }

                if (retailClean.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(retailClean);
                }
                
                var retailExtraClean = new ValuationDataDto
                                            {
                                                Valuations = new List<ValuationDto>(),
                                                Market = "Retail",
                                                Condition = "Extra Clean"
                                            };

                foreach (BlackBook_PriceTableRowDto priceTableRowDto in priceTables.Retail.Rows)
                {
                    if (priceTableRowDto.Prices.HasExtraClean)
                    {
                        var valuationDto = new ValuationDto
                                                {
                                                    Description =
                                                        Enum.GetName(
                                                            typeof(BlackBook_PriceTableRowTypeDto),
                                                            priceTableRowDto.RowType),
                                                    Value = priceTableRowDto.Prices.ExtraClean
                                                };

                        retailExtraClean.Valuations.Add(valuationDto);
                    }
                }

                if (retailExtraClean.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(retailExtraClean);
                }
                
                var retailRough = new ValuationDataDto
                                        {
                                            Valuations = new List<ValuationDto>(),
                                            Market = "Retail",
                                            Condition = "Rough"
                                        };

                foreach (BlackBook_PriceTableRowDto priceTableRowDto in priceTables.Retail.Rows)
                {
                    if (priceTableRowDto.Prices.HasRough)
                    {
                        var valuationDto = new ValuationDto
                                                {
                                                    Description =
                                                        Enum.GetName(
                                                            typeof(BlackBook_PriceTableRowTypeDto),
                                                            priceTableRowDto.RowType),
                                                    Value = priceTableRowDto.Prices.Rough
                                                };

                        retailRough.Valuations.Add(valuationDto);
                    }
                }

                if (retailRough.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(retailRough);
                }
                
            }

            //Wholesale
            if (priceTables.Wholesale != null)
            {
                var wholesaleAvg = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Wholesale",
                    Condition = "Average"
                };

                foreach (BlackBook_PriceTableRowDto priceTableRowDto in priceTables.Wholesale.Rows)
                {
                    if (priceTableRowDto.Prices.HasAverage)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(BlackBook_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = priceTableRowDto.Prices.Average
                        };

                        wholesaleAvg.Valuations.Add(valuationDto);
                    }
                }

                if (wholesaleAvg.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(wholesaleAvg);
                }

                var wholesaleClean = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Wholesale",
                    Condition = "Clean"
                };

                foreach (BlackBook_PriceTableRowDto priceTableRowDto in priceTables.Wholesale.Rows)
                {
                    if (priceTableRowDto.Prices.HasClean)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(BlackBook_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = priceTableRowDto.Prices.Clean
                        };

                        wholesaleClean.Valuations.Add(valuationDto);
                    }
                }

                if (wholesaleClean.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(wholesaleClean);
                }
                
                var wholesaleExtraClean = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Wholesale",
                    Condition = "Extra Clean"
                };

                foreach (BlackBook_PriceTableRowDto priceTableRowDto in priceTables.Wholesale.Rows)
                {
                    if (priceTableRowDto.Prices.HasExtraClean)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(BlackBook_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = priceTableRowDto.Prices.ExtraClean
                        };

                        wholesaleExtraClean.Valuations.Add(valuationDto);
                    }
                }

                if (wholesaleExtraClean.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(wholesaleExtraClean);
                }

                var wholesaleRough = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Wholesale",
                    Condition = "Rough"
                };

                foreach (BlackBook_PriceTableRowDto priceTableRowDto in priceTables.Wholesale.Rows)
                {
                    if (priceTableRowDto.Prices.HasRough)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(BlackBook_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = priceTableRowDto.Prices.Rough
                        };

                        wholesaleRough.Valuations.Add(valuationDto);
                    }
                }

                if (wholesaleRough.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(wholesaleRough);
                }
            }

            //Trade-In
            if (priceTables.TradeIn != null)
            {
                var tradeInAvg = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Trade-In",
                    Condition = "Average"
                };

                foreach (BlackBook_PriceTableRowDto priceTableRowDto in priceTables.TradeIn.Rows)
                {
                    if (priceTableRowDto.Prices.HasAverage)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(BlackBook_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = priceTableRowDto.Prices.Average
                        };

                        tradeInAvg.Valuations.Add(valuationDto);
                    }
                }

                if (tradeInAvg.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(tradeInAvg);
                }

                var tradeInClean = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Trade-In",
                    Condition = "Clean"
                };

                foreach (BlackBook_PriceTableRowDto priceTableRowDto in priceTables.TradeIn.Rows)
                {
                    if (priceTableRowDto.Prices.HasClean)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(BlackBook_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = priceTableRowDto.Prices.Clean
                        };

                        tradeInClean.Valuations.Add(valuationDto);
                    }
                }

                if (tradeInClean.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(tradeInClean);
                }

                var tradeInExtraClean = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Trade-In",
                    Condition = "Extra Clean"
                };

                foreach (BlackBook_PriceTableRowDto priceTableRowDto in priceTables.TradeIn.Rows)
                {
                    if (priceTableRowDto.Prices.HasExtraClean)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(BlackBook_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = priceTableRowDto.Prices.ExtraClean
                        };

                        tradeInExtraClean.Valuations.Add(valuationDto);
                    }
                }

                if (tradeInExtraClean.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(tradeInExtraClean);
                }
                
                var tradeInRough = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Trade-In",
                    Condition = "Rough"
                };

                foreach (BlackBook_PriceTableRowDto priceTableRowDto in priceTables.TradeIn.Rows)
                {
                    if (priceTableRowDto.Prices.HasRough)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(BlackBook_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = priceTableRowDto.Prices.Rough
                        };

                        tradeInRough.Valuations.Add(valuationDto);
                    }
                }

                if (tradeInRough.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(tradeInRough);
                }
            }

            //Finance-Advance
            if (priceTables.FinanceAdvance != null)
            {
                var financeAdvAvg = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Finance Advance",
                    Condition = "Average"
                };

                foreach (BlackBook_PriceTableRowDto priceTableRowDto in priceTables.FinanceAdvance.Rows)
                {
                    if (priceTableRowDto.Prices.HasAverage)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(BlackBook_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = priceTableRowDto.Prices.Average
                        };

                        financeAdvAvg.Valuations.Add(valuationDto);
                    }
                }

                if (financeAdvAvg.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(financeAdvAvg);
                }

                var financeAdvClean = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Finance Advance",
                    Condition = "Clean"
                };

                foreach (BlackBook_PriceTableRowDto priceTableRowDto in priceTables.FinanceAdvance.Rows)
                {
                    if (priceTableRowDto.Prices.HasClean)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(BlackBook_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = priceTableRowDto.Prices.Clean
                        };

                        financeAdvClean.Valuations.Add(valuationDto);
                    }
                }

                if (financeAdvClean.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(financeAdvClean);
                }

                var financeAdvExtraClean = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Finance Advance",
                    Condition = "Extra Clean"
                };

                foreach (BlackBook_PriceTableRowDto priceTableRowDto in priceTables.FinanceAdvance.Rows)
                {
                    if (priceTableRowDto.Prices.HasExtraClean)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(BlackBook_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = priceTableRowDto.Prices.ExtraClean
                        };

                        financeAdvExtraClean.Valuations.Add(valuationDto);
                    }
                }

                if (financeAdvExtraClean.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(financeAdvExtraClean);
                }

                var financeAdvRough = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Finance Advance",
                    Condition = "Rough"
                };

                foreach (BlackBook_PriceTableRowDto priceTableRowDto in priceTables.FinanceAdvance.Rows)
                {
                    if (priceTableRowDto.Prices.HasRough)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(BlackBook_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = priceTableRowDto.Prices.Rough
                        };

                        financeAdvRough.Valuations.Add(valuationDto);
                    }
                }

                if (financeAdvRough.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(financeAdvRough);
                }
            }

            return valuationDataDtos;
        }

        #endregion

        #region kbb

        public static IList<ValuationDataDto> ToValuationData(KBB_PriceTablesDto priceTables)
        {
            IList<ValuationDataDto> valuationDataDtos = new List<ValuationDataDto>();

            //Retail
            if (priceTables.SuggestedRetail != null)
            {
                var retailGood = new ValuationDataDto
                                        {
                                            Valuations = new List<ValuationDto>(),
                                            Market = "Retail",
                                            Condition = "Good"
                                        };

                foreach (KBB_PriceTableRowDto priceTableRowDto in priceTables.SuggestedRetail.Rows)
                {
                    if (priceTableRowDto.Prices.HasGood)
                    {
                        var valuationDto = new ValuationDto
                                                {
                                                    Description =
                                                        Enum.GetName(
                                                            typeof(KBB_PriceTableRowTypeDto),
                                                            priceTableRowDto.RowType),
                                                    Value = (int)priceTableRowDto.Prices.Good
                                                };

                        retailGood.Valuations.Add(valuationDto);
                    }
                }

                if (retailGood.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(retailGood);
                }

                var retailFair = new ValuationDataDto
                                        {
                                            Valuations = new List<ValuationDto>(),
                                            Market = "Retail",
                                            Condition = "Fair"
                                        };

                foreach (KBB_PriceTableRowDto priceTableRowDto in priceTables.SuggestedRetail.Rows)
                {
                    if (priceTableRowDto.Prices.HasFair)
                    {
                        var valuationDto = new ValuationDto
                                                {
                                                    Description =
                                                        Enum.GetName(
                                                            typeof(KBB_PriceTableRowTypeDto),
                                                            priceTableRowDto.RowType),
                                                    Value = (int)priceTableRowDto.Prices.Fair
                                                };

                        retailFair.Valuations.Add(valuationDto);
                    }
                }

                if (retailFair.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(retailFair);
                }

                var retailVeryGood = new ValuationDataDto
                                            {
                                                Valuations = new List<ValuationDto>(),
                                                Market = "Retail",
                                                Condition = "Very Good"
                                            };

                foreach (KBB_PriceTableRowDto priceTableRowDto in priceTables.SuggestedRetail.Rows)
                {
                    if (priceTableRowDto.Prices.HasVeryGood)
                    {
                        var valuationDto = new ValuationDto
                                                {
                                                    Description =
                                                        Enum.GetName(
                                                            typeof(KBB_PriceTableRowTypeDto),
                                                            priceTableRowDto.RowType),
                                                    Value = (int)priceTableRowDto.Prices.VeryGood
                                                };

                        retailVeryGood.Valuations.Add(valuationDto);
                    }
                }

                if (retailVeryGood.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(retailVeryGood);
                }

                var retailExcellent = new ValuationDataDto
                                            {
                                                Valuations = new List<ValuationDto>(),
                                                Market = "Retail",
                                                Condition = "Excellent"
                                            };

                foreach (KBB_PriceTableRowDto priceTableRowDto in priceTables.SuggestedRetail.Rows)
                {
                    if (priceTableRowDto.Prices.HasExcellent)
                    {
                        var valuationDto = new ValuationDto
                                                {
                                                    Description =
                                                        Enum.GetName(
                                                            typeof(KBB_PriceTableRowTypeDto),
                                                            priceTableRowDto.RowType),
                                                    Value = (int)priceTableRowDto.Prices.Excellent
                                                };

                        retailExcellent.Valuations.Add(valuationDto);
                    }
                }

                if (retailExcellent.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(retailExcellent);
                }
            }

            //Wholesale
            if (priceTables.WholesaleLending != null)
            {
                var wholesaleGood = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Wholesale",
                    Condition = "Good"
                };

                foreach (KBB_PriceTableRowDto priceTableRowDto in priceTables.WholesaleLending.Rows)
                {
                    if (priceTableRowDto.Prices.HasGood)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(KBB_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = (int)priceTableRowDto.Prices.Good
                        };

                        wholesaleGood.Valuations.Add(valuationDto);
                    }
                }

                if (wholesaleGood.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(wholesaleGood);
                }
                
                var wholesaleFair = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Wholesale",
                    Condition = "Fair"
                };

                foreach (KBB_PriceTableRowDto priceTableRowDto in priceTables.WholesaleLending.Rows)
                {
                    if (priceTableRowDto.Prices.HasFair)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(KBB_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = (int)priceTableRowDto.Prices.Fair
                        };

                        wholesaleFair.Valuations.Add(valuationDto);
                    }
                }

                if (wholesaleFair.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(wholesaleFair);
                }

                var wholesaleVeryGood = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Wholesale",
                    Condition = "Very Good"
                };

                foreach (KBB_PriceTableRowDto priceTableRowDto in priceTables.WholesaleLending.Rows)
                {
                    if (priceTableRowDto.Prices.HasVeryGood)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(KBB_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = (int)priceTableRowDto.Prices.VeryGood
                        };

                        wholesaleVeryGood.Valuations.Add(valuationDto);
                    }
                }

                if (wholesaleVeryGood.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(wholesaleVeryGood);
                }

                var wholesaleExcellent = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Wholesale",
                    Condition = "Excellent"
                };

                foreach (KBB_PriceTableRowDto priceTableRowDto in priceTables.WholesaleLending.Rows)
                {
                    if (priceTableRowDto.Prices.HasExcellent)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(KBB_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = (int)priceTableRowDto.Prices.Excellent
                        };

                        wholesaleExcellent.Valuations.Add(valuationDto);
                    }
                }

                if (wholesaleExcellent.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(wholesaleExcellent);
                }
            }

            //Trade-In
            if (priceTables.TradeIn != null)
            {
                var tradeInGood = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Trade-In",
                    Condition = "Good"
                };

                foreach (KBB_PriceTableRowDto priceTableRowDto in priceTables.TradeIn.Rows)
                {
                    if (priceTableRowDto.Prices.HasGood)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(KBB_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                                    Value = (int)priceTableRowDto.Prices.Good
                        };

                        tradeInGood.Valuations.Add(valuationDto);
                    }
                }

                if (tradeInGood.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(tradeInGood);
                }
                
                var tradeInFair = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Trade-In",
                    Condition = "Fair"
                };

                foreach (KBB_PriceTableRowDto priceTableRowDto in priceTables.TradeIn.Rows)
                {
                    if (priceTableRowDto.Prices.HasFair)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(KBB_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = (int)priceTableRowDto.Prices.Fair
                        };

                        tradeInFair.Valuations.Add(valuationDto);
                    }
                }

                if (tradeInFair.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(tradeInFair);
                }

                var tradeInVeryGood = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Trade-In",
                    Condition = "Very Good"
                };

                foreach (KBB_PriceTableRowDto priceTableRowDto in priceTables.TradeIn.Rows)
                {
                    if (priceTableRowDto.Prices.HasVeryGood)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(KBB_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = (int)priceTableRowDto.Prices.VeryGood
                        };

                        tradeInVeryGood.Valuations.Add(valuationDto);
                    }
                }

                if (tradeInVeryGood.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(tradeInVeryGood);
                }

                var tradeInExcellent = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Trade-In",
                    Condition = "Excellent"
                };

                foreach (KBB_PriceTableRowDto priceTableRowDto in priceTables.TradeIn.Rows)
                {
                    if (priceTableRowDto.Prices.HasExcellent)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(KBB_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = (int)priceTableRowDto.Prices.Excellent
                        };

                        tradeInExcellent.Valuations.Add(valuationDto);
                    }
                }

                if (tradeInExcellent.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(tradeInExcellent);
                }               
            }


            #region TradeInHigh
            //Trade-In High
            if (priceTables.TradeInHigh != null)
            {
                var tradeInGood = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Trade-In + RangeHigh",
                    Condition = "Good"
                };

                foreach (KBB_PriceTableRowDto priceTableRowDto in priceTables.TradeInHigh.Rows)
                {
                    if (priceTableRowDto.Prices.HasGood)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(KBB_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = (int)priceTableRowDto.Prices.Good
                        };

                        tradeInGood.Valuations.Add(valuationDto);
                    }
                }

                if (tradeInGood.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(tradeInGood);
                }

                var tradeInFair = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Trade-In + RangeHigh",
                    Condition = "Fair"
                };

                foreach (KBB_PriceTableRowDto priceTableRowDto in priceTables.TradeInHigh.Rows)
                {
                    if (priceTableRowDto.Prices.HasFair)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(KBB_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = (int)priceTableRowDto.Prices.Fair
                        };

                        tradeInFair.Valuations.Add(valuationDto);
                    }
                }

                if (tradeInFair.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(tradeInFair);
                }

                var tradeInVeryGood = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Trade-In + RangeHigh",
                    Condition = "Very Good"
                };

                foreach (KBB_PriceTableRowDto priceTableRowDto in priceTables.TradeInHigh.Rows)
                {
                    if (priceTableRowDto.Prices.HasVeryGood)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(KBB_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = (int)priceTableRowDto.Prices.VeryGood
                        };

                        tradeInVeryGood.Valuations.Add(valuationDto);
                    }
                }

                if (tradeInVeryGood.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(tradeInVeryGood);
                }

                var tradeInExcellent = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Trade-In + RangeHigh",
                    Condition = "Excellent"
                };

                foreach (KBB_PriceTableRowDto priceTableRowDto in priceTables.TradeInHigh.Rows)
                {
                    if (priceTableRowDto.Prices.HasExcellent)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(KBB_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = (int)priceTableRowDto.Prices.Excellent
                        };

                        tradeInExcellent.Valuations.Add(valuationDto);
                    }
                }

                if (tradeInExcellent.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(tradeInExcellent);
                }
            }
            #endregion

            #region TradeInLow
            //Trade-In Low
            if (priceTables.TradeInLow != null)
            {
                var tradeInGood = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Trade-In + RangeLow",
                    Condition = "Good"
                };

                foreach (KBB_PriceTableRowDto priceTableRowDto in priceTables.TradeInLow.Rows)
                {
                    if (priceTableRowDto.Prices.HasGood)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(KBB_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = (int)priceTableRowDto.Prices.Good
                        };

                        tradeInGood.Valuations.Add(valuationDto);
                    }
                }

                if (tradeInGood.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(tradeInGood);
                }

                var tradeInFair = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Trade-In + RangeLow",
                    Condition = "Fair"
                };

                foreach (KBB_PriceTableRowDto priceTableRowDto in priceTables.TradeInLow.Rows)
                {
                    if (priceTableRowDto.Prices.HasFair)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(KBB_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = (int)priceTableRowDto.Prices.Fair
                        };

                        tradeInFair.Valuations.Add(valuationDto);
                    }
                }

                if (tradeInFair.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(tradeInFair);
                }

                var tradeInVeryGood = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Trade-In + RangeLow",
                    Condition = "Very Good"
                };

                foreach (KBB_PriceTableRowDto priceTableRowDto in priceTables.TradeInLow.Rows)
                {
                    if (priceTableRowDto.Prices.HasVeryGood)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(KBB_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = (int)priceTableRowDto.Prices.VeryGood
                        };

                        tradeInVeryGood.Valuations.Add(valuationDto);
                    }
                }

                if (tradeInVeryGood.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(tradeInVeryGood);
                }

                var tradeInExcellent = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Trade-In + RangeLow",
                    Condition = "Excellent"
                };

                foreach (KBB_PriceTableRowDto priceTableRowDto in priceTables.TradeInLow.Rows)
                {
                    if (priceTableRowDto.Prices.HasExcellent)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(KBB_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = (int)priceTableRowDto.Prices.Excellent
                        };

                        tradeInExcellent.Valuations.Add(valuationDto);
                    }
                }

                if (tradeInExcellent.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(tradeInExcellent);
                }
            }
            #endregion




            //Private Party
            if (priceTables.PrivateParty != null)
            {
                var privatePartyGood = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Private Party",
                    Condition = "Good"
                };

                foreach (KBB_PriceTableRowDto priceTableRowDto in priceTables.PrivateParty.Rows)
                {
                    if (priceTableRowDto.Prices.HasGood)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(KBB_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = (int)priceTableRowDto.Prices.Good
                        };

                        privatePartyGood.Valuations.Add(valuationDto);
                    }
                }

                if (privatePartyGood.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(privatePartyGood);
                }
                
                var privatePartyFair = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Private Party",
                    Condition = "Fair"
                };

                foreach (KBB_PriceTableRowDto priceTableRowDto in priceTables.PrivateParty.Rows)
                {
                    if (priceTableRowDto.Prices.HasFair)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(KBB_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = (int)priceTableRowDto.Prices.Fair
                        };

                        privatePartyFair.Valuations.Add(valuationDto);
                    }
                }

                if (privatePartyFair.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(privatePartyFair);
                }
                
                var privatePartyVeryGood = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Private Party",
                    Condition = "Very Good"
                };

                foreach (KBB_PriceTableRowDto priceTableRowDto in priceTables.PrivateParty.Rows)
                {
                    if (priceTableRowDto.Prices.HasVeryGood)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(KBB_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = (int)priceTableRowDto.Prices.VeryGood
                        };

                        privatePartyVeryGood.Valuations.Add(valuationDto);
                    }
                }

                if (privatePartyVeryGood.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(privatePartyVeryGood);
                }
                
                var privatePartyExcellent = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Private Party",
                    Condition = "Excellent"
                };

                foreach (KBB_PriceTableRowDto priceTableRowDto in priceTables.PrivateParty.Rows)
                {
                    if (priceTableRowDto.Prices.HasExcellent)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(KBB_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = (int)priceTableRowDto.Prices.Excellent
                        };

                        privatePartyExcellent.Valuations.Add(valuationDto);
                    }
                }

                if (privatePartyExcellent.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(privatePartyExcellent);
                }              
            }

            //Auctions
            if (priceTables.Auction != null)
            {
                var auctionGood = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Auction",
                    Condition = "Good"
                };

                foreach (KBB_PriceTableRowDto priceTableRowDto in priceTables.Auction.Rows)
                {
                    if (priceTableRowDto.Prices.HasGood)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(KBB_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = (int)priceTableRowDto.Prices.Good
                        };

                        auctionGood.Valuations.Add(valuationDto);
                    }
                }

                if (auctionGood.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(auctionGood);
                }     
                
                var auctionFair = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Auction",
                    Condition = "Fair"
                };

                foreach (KBB_PriceTableRowDto priceTableRowDto in priceTables.Auction.Rows)
                {
                    if (priceTableRowDto.Prices.HasFair)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(KBB_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = (int)priceTableRowDto.Prices.Fair
                        };

                        auctionFair.Valuations.Add(valuationDto);
                    }
                }

                if (auctionFair.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(auctionFair);
                }    
                
                var auctionVeryGood = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Auction",
                    Condition = "Very Good"
                };

                foreach (KBB_PriceTableRowDto priceTableRowDto in priceTables.Auction.Rows)
                {
                    if (priceTableRowDto.Prices.HasVeryGood)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(KBB_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = (int)priceTableRowDto.Prices.VeryGood
                        };

                        auctionVeryGood.Valuations.Add(valuationDto);
                    }
                }

                if (auctionVeryGood.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(auctionVeryGood);
                }    
                
                var auctionExcellent = new ValuationDataDto
                {
                    Valuations = new List<ValuationDto>(),
                    Market = "Auction",
                    Condition = "Excellent"
                };

                foreach (KBB_PriceTableRowDto priceTableRowDto in priceTables.Auction.Rows)
                {
                    if (priceTableRowDto.Prices.HasExcellent)
                    {
                        var valuationDto = new ValuationDto
                        {
                            Description =
                                Enum.GetName(
                                    typeof(KBB_PriceTableRowTypeDto),
                                    priceTableRowDto.RowType),
                            Value = (int)priceTableRowDto.Prices.Excellent
                        };

                        auctionExcellent.Valuations.Add(valuationDto);
                    }
                }

                if (auctionExcellent.Valuations.Count > 0)
                {
                    valuationDataDtos.Add(auctionExcellent);
                }    
                
            }

            return valuationDataDtos;
        }

        #endregion

        #region nada

        public static IList<ValuationDataDto> ToValuationData(Nada_PriceTablesDto priceTable)
        {
            IList<ValuationDataDto> valuationDataDtos = new List<ValuationDataDto>();

            var tradeInAvg = new ValuationDataDto
                                    {
                                        Valuations = new List<ValuationDto>(),
                                        Market = "Trade-In",
                                        Condition = "Average"
                                    };

            foreach (Nada_PriceTableRowDto priceTableRowDto in priceTable.Rows)
            {
                if (priceTableRowDto.Prices.HasTradeInAverage)
                {
                    var valuationDto = new ValuationDto
                    {
                        Description =
                            Enum.GetName(
                                typeof(Nada_PriceTableRowTypeDto),
                                priceTableRowDto.RowType),
                        Value = (int)priceTableRowDto.Prices.TradeInAverage
                    };

                    tradeInAvg.Valuations.Add(valuationDto);
                }
            }

            if (tradeInAvg.Valuations.Count > 0)
            {
                valuationDataDtos.Add(tradeInAvg);
            }    
            
            var tradeInClean = new ValuationDataDto
            {
                Valuations = new List<ValuationDto>(),
                Market = "Trade-In",
                Condition = "Clean"
            };

            foreach (Nada_PriceTableRowDto priceTableRowDto in priceTable.Rows)
            {
                if (priceTableRowDto.Prices.HasTradeInClean)
                {
                    var valuationDto = new ValuationDto
                    {
                        Description =
                            Enum.GetName(
                                typeof(Nada_PriceTableRowTypeDto),
                                priceTableRowDto.RowType),
                        Value = (int)priceTableRowDto.Prices.TradeInClean
                    };

                    tradeInClean.Valuations.Add(valuationDto);
                }
            }

            if (tradeInClean.Valuations.Count > 0)
            {
                valuationDataDtos.Add(tradeInClean);
            }    

            var tradeInRough = new ValuationDataDto
            {
                Valuations = new List<ValuationDto>(),
                Market = "Trade-In",
                Condition = "Rough"
            };

            foreach (Nada_PriceTableRowDto priceTableRowDto in priceTable.Rows)
            {
                if (priceTableRowDto.Prices.HasTradeInRough)
                {
                    var valuationDto = new ValuationDto
                    {
                        Description =
                            Enum.GetName(
                                typeof(Nada_PriceTableRowTypeDto),
                                priceTableRowDto.RowType),
                        Value = (int)priceTableRowDto.Prices.TradeInRough
                    };

                    tradeInRough.Valuations.Add(valuationDto);
                }
            }

            if (tradeInRough.Valuations.Count > 0)
            {
                valuationDataDtos.Add(tradeInRough);
            }    
            
            var tradeInLoan = new ValuationDataDto
            {
                Valuations = new List<ValuationDto>(),
                Market = "Trade-In",
                Condition = "Loan"
            };

            foreach (Nada_PriceTableRowDto priceTableRowDto in priceTable.Rows)
            {
                if (priceTableRowDto.Prices.HasLoan)
                {
                    var valuationDto = new ValuationDto
                    {
                        Description =
                            Enum.GetName(
                                typeof(Nada_PriceTableRowTypeDto),
                                priceTableRowDto.RowType),
                        Value = (int)priceTableRowDto.Prices.Loan
                    };

                    tradeInLoan.Valuations.Add(valuationDto);
                }
            }

            if (tradeInLoan.Valuations.Count > 0)
            {
                valuationDataDtos.Add(tradeInLoan);
            }     

            var retail = new ValuationDataDto
            {
                Valuations = new List<ValuationDto>(),
                Market = "Retail",
                Condition = "Retail"
            };

            foreach (Nada_PriceTableRowDto priceTableRowDto in priceTable.Rows)
            {
                if (priceTableRowDto.Prices.HasRetail)
                {
                    var valuationDto = new ValuationDto
                    {
                        Description =
                            Enum.GetName(
                                typeof(Nada_PriceTableRowTypeDto),
                                priceTableRowDto.RowType),
                        Value = (int)priceTableRowDto.Prices.Retail
                    };

                    retail.Valuations.Add(valuationDto);
                }
            }

            if (retail.Valuations.Count > 0)
            {
                valuationDataDtos.Add(retail);
            }    

            return valuationDataDtos;
        }

        #endregion

        #region galves

        public static IList<ValuationDataDto> ToValuationData(Galves_PriceTablesDto priceTable)
        {
            IList<ValuationDataDto> valuationDataDtos = new List<ValuationDataDto>();

            var tradeIn = new ValuationDataDto
            {
                Valuations = new List<ValuationDto>(),
                Market = "Trade-In",
                Condition = "Trade-In"
            };

            foreach (Galves_PriceTableRowDto priceTableRowDto in priceTable.Rows)
            {
                if (priceTableRowDto.Prices.HasTradeIn)
                {
                    var valuationDto = new ValuationDto
                    {
                        Description =
                            Enum.GetName(
                                typeof(Galves_PriceTableRowTypeDto),
                                priceTableRowDto.RowType),
                        Value = (int)priceTableRowDto.Prices.TradeIn
                    };

                    tradeIn.Valuations.Add(valuationDto);
                }
            }

            if (tradeIn.Valuations.Count > 0)
            {
                valuationDataDtos.Add(tradeIn);
            }    

            var marketReady = new ValuationDataDto
            {
                Valuations = new List<ValuationDto>(),
                Market = "Market Ready",
                Condition = "Market Ready"
            };

            foreach (Galves_PriceTableRowDto priceTableRowDto in priceTable.Rows)
            {
                if (priceTableRowDto.Prices.HasRetail)
                {
                    var valuationDto = new ValuationDto
                    {
                        Description =
                            Enum.GetName(
                                typeof(Galves_PriceTableRowTypeDto),
                                priceTableRowDto.RowType),
                        Value = (int)priceTableRowDto.Prices.Retail
                    };

                    marketReady.Valuations.Add(valuationDto);
                }
            }

            if (marketReady.Valuations.Count > 0)
            {
                valuationDataDtos.Add(marketReady);
            }    

            return valuationDataDtos;
        }

        #endregion

    }
}
