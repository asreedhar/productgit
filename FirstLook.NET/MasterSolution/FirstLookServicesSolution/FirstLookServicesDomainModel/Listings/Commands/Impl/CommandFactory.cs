﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.Listings.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.Listings.Commands.Impl
{
    class CommandFactory : ICommandFactory
    {
        public ICommand<FetchMarketListingsReferenceResultsDto, FetchMarketListingsReferenceArgumentsDto> CreateFetchMarketListingsReferenceCommand()
        {
            return new FetchMarketListingsReferenceCommand();
        }

        public ICommand<FetchMarketListingsResultsDto, IdentityContextDto<FetchMarketListingsArgumentsDto>> CreateFetchMarketListingsCommand()
        {
            return new FetchMarketListingsCommand();
        }
        public ICommand<FetchInventoryMarketListingsResultsDto, IdentityContextDto<FetchInventoryMarketListingsArgumentsDto>> CreateFetchInventoryMarketListingsCommand()
        {
            return new FetchInventoryMarketListingsCommand();
        }
    }
}
