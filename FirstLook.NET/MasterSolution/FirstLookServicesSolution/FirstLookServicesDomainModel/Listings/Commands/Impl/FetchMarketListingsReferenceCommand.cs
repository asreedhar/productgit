﻿using System;
using System.Collections.Generic;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Listings.Commands.TransferObjects.Envelopes;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes;
using System.Linq;

namespace FirstLook.FirstLookServices.DomainModel.Listings.Commands.Impl
{
    public class FetchMarketListingsReferenceCommand : ICommand<FetchMarketListingsReferenceResultsDto, FetchMarketListingsReferenceArgumentsDto>
    {
        public FetchMarketListingsReferenceResultsDto Execute(FetchMarketListingsReferenceArgumentsDto parameters)
        {
            var factory = RegistryFactory.GetResolver().Resolve<Pricing.DomainModel.Commands.ICommandFactory>();

            var command = factory.CreateListingsReferenceCommand();

            var argumentsDto = new ReferenceLookupArgumentsDto();

            ReferenceLookupResultsDto results = command.Execute(argumentsDto);

            var mileages = new List<int?>{0, 1000, 2000};

            for (var i = 5000; i < 126000; i+=5000)
            {
                mileages.Add(i);
            }

            return new FetchMarketListingsReferenceResultsDto
            {
                Distances = results.Distances,
                SearchTypes = results.SearchType.ToList(),
                Mileages = mileages,
            };
        }
    }
}
