﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.FirstLookServices.DomainModel.Listings.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Utility;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes;
using FirstLook.Pricing.DomainModel.Internet.DataSource;
using MarketListingDto = FirstLook.FirstLookServices.DomainModel.Listings.Commands.TransferObjects.MarketListingDto;
using SearchResultsDto = FirstLook.Pricing.DomainModel.Commands.TransferObjects.Envelopes.SearchResultsDto;
using SortColumnDto = FirstLook.Client.DomainModel.Common.Commands.TransferObjects.SortColumnDto;


namespace FirstLook.FirstLookServices.DomainModel.Listings.Commands.Impl
{
    public class FetchMarketListingsCommand : ICommand<FetchMarketListingsResultsDto, IdentityContextDto<FetchMarketListingsArgumentsDto>>
    {
        public const int Vehentitytypeidmobile = 7;

        public FetchMarketListingsResultsDto Execute(IdentityContextDto<FetchMarketListingsArgumentsDto> parameters)
        {
            FetchMarketListingsArgumentsDto fetchMarketListingsArgumentsDto = parameters.Arguments;

            IdentityDto identityDto = parameters.Identity;

            #region factories

            var clientClientFactory = RegistryFactory.GetResolver().Resolve<Client.DomainModel.Clients.Commands.ICommandFactory>();

            var clientVehicleFactory = RegistryFactory.GetResolver().Resolve<Client.DomainModel.Vehicles.Commands.ICommandFactory>();

            var pricingFactory = RegistryFactory.GetResolver().Resolve<Pricing.DomainModel.Commands.ICommandFactory>();

            var flServicesfactory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            #endregion

            #region validation

            if (String.IsNullOrEmpty(fetchMarketListingsArgumentsDto.Vin))
            {
                throw new MissingInputException("vin", "Error: Vin was not provided.");
            }

            if (fetchMarketListingsArgumentsDto.DealerId <= 0)
            {
                throw new MissingInputException("dealer", "Error: Dealer ID was not provided.");
            }

            DistanceDto distanceDto = null;
            int distanceFromDealer = fetchMarketListingsArgumentsDto.Distance;

            if (distanceFromDealer <= 0)
            {
                // Updated this file on 01/06/14 -- Now this distancefromDealer will no longer used apart from validation conditions written below.
                // In the validation conditions there is no check for value '0'. Therefore, value 100 will be used to validate and the distance value will
                // be returned from the database(latest distance saved.)
                distanceFromDealer = 100;
            }

            // Null defaults to the maximum value.
            MileageDto mileageMax = null;

            // Make sure the search type, distance, and mileages fit into the predefined buckets.
            var command = flServicesfactory.CreateFetchMarketListingsReferenceCommand();

            FetchMarketListingsReferenceResultsDto fmlRefArgsDto = command.Execute(new FetchMarketListingsReferenceArgumentsDto());

            if (String.IsNullOrEmpty(fetchMarketListingsArgumentsDto.SearchType) == false)
            {
                var searchType = fmlRefArgsDto.SearchTypes.Find(stype => stype.ToString().ToLower() == fetchMarketListingsArgumentsDto.SearchType) ?? null;

                if (searchType == null)
                {
                    throw new InvalidInputException("searchtype", fetchMarketListingsArgumentsDto.SearchType, "Search type set to an invalid value - " + fetchMarketListingsArgumentsDto.SearchType);
                }
            }

            distanceDto = fmlRefArgsDto.Distances.Find(distance => distance.Value == distanceFromDealer) ?? null;

            if (distanceDto == null)
            {
                throw new InvalidInputException("distance", distanceFromDealer.ToString(), "Distance is set to an invalid value" + fetchMarketListingsArgumentsDto.Distance);
            }

            if (fetchMarketListingsArgumentsDto.MileageMin != null && fmlRefArgsDto.Mileages.Contains(fetchMarketListingsArgumentsDto.MileageMin) == false)
            {
                throw new InvalidInputException("mileagemin", fetchMarketListingsArgumentsDto.MileageMin.ToString(), "Mileagemin is set to an invalid value" + fetchMarketListingsArgumentsDto.MileageMin);
            }

            if (fetchMarketListingsArgumentsDto.MileageMax != null && fetchMarketListingsArgumentsDto.MileageMax != "unlimited" && fetchMarketListingsArgumentsDto.MileageMax != "0")
            {
                if (fmlRefArgsDto.Mileages.Contains(Convert.ToInt32(fetchMarketListingsArgumentsDto.MileageMax)) == false)
                {
                    throw new InvalidInputException("mileagemax", fetchMarketListingsArgumentsDto.MileageMax.ToString(), "Mileagemax is set to an invalid value" + fetchMarketListingsArgumentsDto.MileageMax);
                }

                if (Convert.ToInt32(fetchMarketListingsArgumentsDto.MileageMax) <= fetchMarketListingsArgumentsDto.MileageMin)
                {
                    throw new InvalidInputException("mileagemax", fetchMarketListingsArgumentsDto.MileageMax.ToString(), "Mileagemax is not set larger than mileagemin" + fetchMarketListingsArgumentsDto.MileageMax);
                }

                // The desktop subtracts 1 from the max mileage, so we will do the same here.  Monkey see, Monkey do.
                mileageMax = new MileageDto() { Value = Convert.ToInt32(fetchMarketListingsArgumentsDto.MileageMax) - 1 };
            }

            #endregion

            #region broker

            //Get Broker For Client

            var clientIdentityContext = new IdentityContextDto<BrokerForClientArgumentsDto>
            {
                Identity = identityDto,
                Arguments = new BrokerForClientArgumentsDto
                {
                    AuthorityName = identityDto.AuthorityName,
                    Client = new ClientDto
                    {
                        Id = fetchMarketListingsArgumentsDto.DealerId
                    }
                }
            };

            ICommand<BrokerForClientResultsDto, IdentityContextDto<BrokerForClientArgumentsDto>> brokerForClientCommand = clientClientFactory.CreateBrokerForClientCommand();

            BrokerForClientResultsDto brokerForClientResultsDto = brokerForClientCommand.Execute(clientIdentityContext);

            if ((brokerForClientResultsDto == null) || (brokerForClientResultsDto.Broker == null))
            {
                throw new InvalidInputException("dealer", "Invalid dealer ID", "null broker for dealer id = " + fetchMarketListingsArgumentsDto.DealerId);
            }

            BrokerDto brokerDto = brokerForClientResultsDto.Broker;

            #endregion

            #region vehicle identification

            ICommand<IdentifyVehicleResultsDto, IdentityContextDto<IdentifyVehicleArgumentsDto>> identityVehicleCommand = clientVehicleFactory.CreateIdentifyVehicleCommand();

            var identifyVehicleArgumentsDto = new IdentifyVehicleArgumentsDto
            {
                Broker = brokerDto.Handle,
                Vin = fetchMarketListingsArgumentsDto.Vin
            };

            var vehicleIdentifyContext = new IdentityContextDto
                <IdentifyVehicleArgumentsDto>
            {
                Arguments = identifyVehicleArgumentsDto,
                Identity = identityDto
            };

            try
            {
                var identifyVehicleResultsDto = identityVehicleCommand.Execute(vehicleIdentifyContext);
            }
            catch (Exception)
            {
                throw new InvalidInputException("vin", "Invalid Vin", "Exception attempting to identify a vehicle for vin = " + fetchMarketListingsArgumentsDto.Vin);
            }

            ICommand<QueryResultsDto, IdentityContextDto<QueryArgumentsDto>> queryCommand = clientVehicleFactory.CreateQueryCommand();

            var queryArgumentsDto = new QueryArgumentsDto
                                        {
                                            Broker = brokerDto.Handle,
                                            Example = new VehicleDescriptionDto
                                                          {
                                                              Vin = fetchMarketListingsArgumentsDto.Vin,
                                                              BodyTypeName = null,
                                                              DriveTrainName = null,
                                                              EngineName = null,
                                                              FuelTypeName = null,
                                                              MakeName = null,
                                                              ModelFamilyName = null,
                                                              PassengerDoorName = null,
                                                              SegmentName = null,
                                                              SeriesName = null,
                                                              TransmissionName = null
                                                          },
                                            SortColumns = new List<SortColumnDto>
                                                              {
                                                                  new SortColumnDto
                                                                      {
                                                                          Ascending = true,
                                                                          ColumnName = "VIN"
                                                                      }
                                                              },
                                            Actuality = VehicleActualityDto.NotInventory
                                        };
           

            var queryIdentityContext = new IdentityContextDto<QueryArgumentsDto>
            {
                Arguments = queryArgumentsDto,
                Identity = identityDto
            };

            QueryResultsDto queryResultsDto = queryCommand.Execute(queryIdentityContext);

            if (queryResultsDto.Vehicles.Count < 1)
            {
                throw new InvalidInputException("vin", "Invalid dealer ID", "null broker for dealer id = " + fetchMarketListingsArgumentsDto.DealerId);
            }

            VehicleDto vehicleDto = queryResultsDto.Vehicles[0];

            #endregion

            #region pricing search

            SearchTypeDto searchTypeDto;

            switch (fetchMarketListingsArgumentsDto.SearchType)
            {
                case "overall":
                    {
                        searchTypeDto = SearchTypeDto.Overall;
                        break;
                    }
                case "precision":
                    {
                        searchTypeDto = SearchTypeDto.Precision;
                        break;
                    }
                default:
                    {
                        searchTypeDto = SearchTypeDto.Precision;
                        break;
                    }
            }

            // Here's the deal.  Ping normally runs a standard search (CreateSearchCommand) when the desktop GUI first brings the ping page up.  Then, when the user changes options,
            // the desktop GUI will run modified searches (CreateSearchUpdateCommand).  The mobile app could call this with parameters the first time it wants listings.  So, to handle
            // this, the following things needs to be done:
            // 1.  Call CreateSearchCommand.  This will just get the search as it was last run.
            // 2.  Modify the searchDto in the result of the CreateSearchCommand,
            // 3.  Call CreateSearchUpdateCommand with the new parameters.  This needs to be done because there is no way to know if a previous search has been run.

            ICommand<SearchResultsDto, SearchArgumentsDto> searchCommand = pricingFactory.CreateSearchCommand();

            var searchArgumentsDto = new SearchArgumentsDto
            {
                InsertUser =identityDto.Name,
                OwnerEntityId = fetchMarketListingsArgumentsDto.DealerId,
                OwnerEntityTypeId = 1,      // hardcode to Dealer - not used
                VehicleEntityId = vehicleDto.Id,
                VehicleEntityTypeId = Vehentitytypeidmobile
            };

            SearchResultsDto searchResultsDto = searchCommand.Execute(searchArgumentsDto);

            // Get ready to call the searchUpdateCommand for the specific search that matches the caller's parameters.
            SearchDto searchDto = searchResultsDto.Search;

            //All the three If conditions are added to remove inventory ping api isuue(i.e. listings on desktop were different than the api)

            if (fetchMarketListingsArgumentsDto.Distance != 0)
            {
                searchDto.Distance = distanceDto;    
            }
            if (fetchMarketListingsArgumentsDto.MileageMin != null)
            {
                 searchDto.LowMileage.Value = Convert.ToInt32(fetchMarketListingsArgumentsDto.MileageMin);
            }
            if (fetchMarketListingsArgumentsDto.MileageMax != null)
            {
                searchDto.HighMileage = fetchMarketListingsArgumentsDto.MileageMax == "0" ? null : mileageMax;
                //searchDto.HighMileage = fetchMarketListingsArgumentsDto.MileageMax == "unlimited" ? null : mileageMax;
                //searchDto.HighMileage = mileageMax;
            }

            searchDto.UpdateUser =identityDto.Name;

            ICommand<SearchResultsDto, SearchUpdateArgumentsDto> searchUpdCommand = pricingFactory.CreateSearchUpdateCommand();

            var searchUpdateArgumentsDto = new SearchUpdateArgumentsDto
            {
                InsertUser = identityDto.Name,
                OwnerEntityId = fetchMarketListingsArgumentsDto.DealerId,
                OwnerEntityTypeId = 1, // hardcode to Dealer - not used
                VehicleEntityId = vehicleDto.Id,
                VehicleEntityTypeId = Vehentitytypeidmobile,
                Search = searchDto,
                SearchType = searchTypeDto
            };

            searchResultsDto = searchUpdCommand.Execute(searchUpdateArgumentsDto);

            // Handle the case where if the search type is not specified (in which its defaulted to Precision), and there is not
            // enough precision results, switch the search type to overall.
            SearchTypeDto modifiedSearchTypeDto = searchTypeDto;

            if (fetchMarketListingsArgumentsDto.SearchType == null)
            {
                if (searchResultsDto.Precision.Pricing.NumComparableListings <= 5)
                {
                    modifiedSearchTypeDto = SearchTypeDto.Overall;
                }
            }

            #endregion

            #region market listings

            var marketListingsFetchArgumentsDto = new MarketListingsFetchArgumentsDto
            {
                InsertUser = identityDto.Name,
                MaximumRows = 100000,         // This is what the desktop sets it to.
                SearchType = modifiedSearchTypeDto,
                VehicleEntityTypeId = Vehentitytypeidmobile,
                VehicleEntityId = vehicleDto.Id,
                OwnerEntityTypeId = 1,      // hardcode to Dealer - not used
                OwnerEntityId = fetchMarketListingsArgumentsDto.DealerId
            };

            ICommand<MarketListingsFetchResultsDto, MarketListingsFetchArgumentsDto> listingsFetchCommand = pricingFactory.CreateMarketListingsFetchCommand();

            MarketListingsFetchResultsDto listingsFetchResultsDto = listingsFetchCommand.Execute(marketListingsFetchArgumentsDto);

            #endregion

            return new FetchMarketListingsResultsDto
            {
                MarketListings = (List<MarketListingDto>) Mapper.Map(listingsFetchResultsDto.MarketListings),
                SearchResult = Mapper.Map(searchResultsDto, modifiedSearchTypeDto)
            };
        }
    }
}