﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects;
using MarketPricingDto = FirstLook.FirstLookServices.DomainModel.Listings.Commands.TransferObjects.MarketPricingDto;
using SearchDetailDto = FirstLook.FirstLookServices.DomainModel.Listings.Commands.TransferObjects.SearchDetailDto;
using SearchResultsDto = FirstLook.FirstLookServices.DomainModel.Listings.Commands.TransferObjects.SearchResultsDto;
using SearchSummaryInfoDto = FirstLook.FirstLookServices.DomainModel.Listings.Commands.TransferObjects.SearchSummaryInfoDto;
using MarketListingDto = FirstLook.FirstLookServices.DomainModel.Listings.Commands.TransferObjects.MarketListingDto;

namespace FirstLook.FirstLookServices.DomainModel.Listings.Commands.Impl
{
    public static class Mapper
    {
        public static SearchResultsDto Map(Pricing.DomainModel.Commands.TransferObjects.Envelopes.SearchResultsDto searchResultsDto, SearchTypeDto searchTypeDto)
        {
            if (searchResultsDto == null)
            {
                return null;
            }

            var dto = new SearchResultsDto();
            
            if (searchTypeDto == SearchTypeDto.Overall)
            {
                dto.SearchDetail = Map(searchResultsDto.Overall);
            }
            else
            {
                dto.SearchDetail = Map(searchResultsDto.Precision);
            }

            dto.SearchDetail.SearchType = Enum.GetName(typeof(SearchTypeDto), searchTypeDto);

            dto.SearchSummaryInfo = Map(searchResultsDto.SearchSummaryInfo);

            dto.SearchDetail.MarketPricing.MinMileageSearched = searchResultsDto.Search.LowMileage.Value;
            if (searchResultsDto.Search.HighMileage != null)
            {
                dto.SearchDetail.MarketPricing.MaxMileageSearched = searchResultsDto.Search.HighMileage.Value + 1;
            }
            else
            {
                dto.SearchDetail.MarketPricing.MaxMileageSearched = null;
            }
           

            return dto;
        }

        public static SearchDetailDto Map(Pricing.DomainModel.Commands.TransferObjects.SearchDetailDto searchDetailDto)
        {
            if (searchDetailDto == null)
            {
                return null;
            }

            var dto = new SearchDetailDto {MarketPricing = Map(searchDetailDto.Pricing)};

            return dto;

        }

        public static MarketPricingDto Map(Pricing.DomainModel.Commands.TransferObjects.MarketPricingDto marketPricingDto)
        {
            if(marketPricingDto == null)
            {
                return null;
            }

            var dto = new MarketPricingDto();

            if (marketPricingDto.HasMarketDaySupply)
            {
                dto.DaySupply = marketPricingDto.MarketDaySupply;
            }

            if (marketPricingDto.HasSearchRadius)
            {
                dto.Distance = marketPricingDto.SearchRadius;
            }

            dto.MarketPrice = Map(marketPricingDto.MarketPrice);

            dto.Mileage = Map(marketPricingDto.VehicleMileage);
       
            return dto;
        }

        public static TransferObjects.SampleSummaryDto<int> Map(SampleSummaryDto<int> sampleSummaryDto)
        {

            if (sampleSummaryDto == null)
            {
                return null;
            }

            var dto = new TransferObjects.SampleSummaryDto<int>();

            if (sampleSummaryDto.HasAverage)
            {
                dto.HasAverage = true;
                dto.Average = sampleSummaryDto.Average;
            }

            if (sampleSummaryDto.HasMaximum)
            {
                dto.HasMaximum = true;
                dto.Maximum = sampleSummaryDto.Maximum;
            }

            if(sampleSummaryDto.HasMinimum)
            {
                dto.HasMinimum = true;
                dto.Minimum = sampleSummaryDto.Minimum;
            }

            if(sampleSummaryDto.HasMaximum)
            {
                dto.HasMaximum = true;
                dto.Maximum = sampleSummaryDto.Maximum;
            }

            return dto;
        }

        public static SearchSummaryInfoDto Map(Pricing.DomainModel.Commands.TransferObjects.SearchSummaryInfoDto searchSummaryInfoDto)
        {
            if (searchSummaryInfoDto == null)
            {
                return null;
            }

            return new SearchSummaryInfoDto
                       {
                           BodyType = searchSummaryInfoDto.BodyType,
                           Doors = searchSummaryInfoDto.Doors,
                           DriveTrain = searchSummaryInfoDto.DriveTrain,
                           Engine = searchSummaryInfoDto.Engine,
                           FuelType = searchSummaryInfoDto.FuelType,
                           Segment = searchSummaryInfoDto.Segment,
                           Transmission = searchSummaryInfoDto.Transmission,
                           Trim = searchSummaryInfoDto.Trim,
                           YearMakeModel = searchSummaryInfoDto.YearMakeModel
                       };
        }

        public static MarketListingDto Map(Pricing.DomainModel.Commands.TransferObjects.MarketListingDto marketListingDto)
        {
            if (marketListingDto == null)
            {
                return null;
            }

            var dto = new MarketListingDto();

            if (marketListingDto.HasAge)
            {
                dto.HasAge = true;
                dto.Age = marketListingDto.Age;
            }

            if (marketListingDto.HasVehicleMileage)
            {
                dto.HasVehicleMileage = true;
                dto.VehicleMileage = marketListingDto.VehicleMileage;
            }

            if (marketListingDto.HasListPrice)
            {
                dto.HasListPrice = true;
                dto.ListPrice = marketListingDto.ListPrice;
            }

            if (marketListingDto.HasPercentMarketAverage)
            {
                dto.HasPercentMarketAverage = true;
                dto.PercentMarketAverage = marketListingDto.PercentMarketAverage;
            }

            if (marketListingDto.HasIsCertified)
            {
                dto.HasIsCertified = true;
                dto.IsCertified = marketListingDto.IsCertified;
            }

            dto.Desc = marketListingDto.VehicleDescription;

            dto.Color = marketListingDto.VehicleColor;

            dto.Distance = marketListingDto.DistanceFromDealer;

            dto.Seller = marketListingDto.Seller;

            return dto;
        }

        public static IList<MarketListingDto> Map(IList<Pricing.DomainModel.Commands.TransferObjects.MarketListingDto> marketListingDtos)
        {
            return marketListingDtos.Select(Map).ToList();
        }
    }
}
