﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.Listings.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.Listings.Commands
{
    public interface ICommandFactory
    {
        ICommand<FetchMarketListingsReferenceResultsDto, FetchMarketListingsReferenceArgumentsDto> CreateFetchMarketListingsReferenceCommand();

        ICommand<FetchMarketListingsResultsDto, IdentityContextDto<FetchMarketListingsArgumentsDto>> CreateFetchMarketListingsCommand();

        ICommand<FetchInventoryMarketListingsResultsDto, IdentityContextDto<FetchInventoryMarketListingsArgumentsDto>> CreateFetchInventoryMarketListingsCommand();


    }
}
