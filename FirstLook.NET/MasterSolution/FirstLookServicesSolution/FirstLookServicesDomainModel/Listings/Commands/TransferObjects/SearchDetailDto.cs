﻿using FirstLook.Pricing.DomainModel.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.Listings.Commands.TransferObjects
{
    public class SearchDetailDto
    {
        public MarketPricingDto MarketPricing { get; set; }
        public string SearchType { get; set; }
    }
}
