﻿namespace FirstLook.FirstLookServices.DomainModel.Listings.Commands.TransferObjects
{
    public class SearchResultsDto
    {
        public SearchDetailDto SearchDetail { get; set; }

        public SearchSummaryInfoDto SearchSummaryInfo { get;set; }
    }
}
