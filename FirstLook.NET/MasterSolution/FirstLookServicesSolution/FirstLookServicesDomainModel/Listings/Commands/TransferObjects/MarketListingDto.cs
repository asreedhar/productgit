﻿namespace FirstLook.FirstLookServices.DomainModel.Listings.Commands.TransferObjects
{
    public class MarketListingDto
    {
        public bool HasAge { get; set; }

        public int Age { get; set; }

        public string Desc { get; set; }

        public string Color { get; set; }

        public bool HasVehicleMileage { get; set; }

        public int VehicleMileage { get; set; }

        public bool HasListPrice { get; set; }

        public int ListPrice { get; set; }

        public bool HasPercentMarketAverage { get; set; }

        public int PercentMarketAverage { get; set; }

        public int Distance { get; set; }

        public bool HasIsCertified { get; set; }

        public bool IsCertified { get; set; }

        public string Seller { get; set; }
    }
}
