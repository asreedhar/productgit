﻿namespace FirstLook.FirstLookServices.DomainModel.Listings.Commands.TransferObjects
{
    public class MarketPricingDto
    {
        public SampleSummaryDto<int> MarketPrice { get; set; }

        public SampleSummaryDto<int> Mileage { get; set; }

        public int DaySupply { get; set; }

        public int Distance { get; set; }

        public int MinMileageSearched { get; set; }

        public int? MaxMileageSearched { get; set; }

    }
}
