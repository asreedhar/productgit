﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.Listings.Commands.TransferObjects.Envelopes
{
    public class FetchInventoryMarketListingsResultsDto
    {
        public List<MarketListingDto> MarketListings { get; set; }

        public SearchResultsDto SearchResult { get; set; }
    }
}
