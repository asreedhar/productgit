﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.FirstLookServices.DomainModel.Listings.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class FetchInventoryMarketListingsArgumentsDto
    {

        public int DealerId { get; set; }

        public int InventoryId { get; set; }

        public string SearchType { get; set; }

        public int Distance { get; set; }

        public int? MileageMin { get; set; }

        public string MileageMax { get; set; }

        public bool? PreviousYear { get; set; }

        public bool? NextYear { get; set; }

    }
}
