﻿using System;

namespace FirstLook.FirstLookServices.DomainModel.Listings.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class FetchMarketListingsArgumentsDto
    {
        public int DealerId { get; set; }

        public string Vin { get; set; }

        public string SearchType { get; set; }

        public int Distance { get; set; }

        public int? MileageMin { get; set; }

        public string MileageMax { get; set; }
    }
}
