﻿using System.Collections.Generic;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.Listings.Commands.TransferObjects.Envelopes
{
    public class FetchMarketListingsReferenceResultsDto
    {
        public List<DistanceDto> Distances { get; set; }

        public List<string> SearchTypes { get; set; }

        public List<int?> Mileages { get; set; }
    }
}
