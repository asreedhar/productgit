﻿using System.Collections.Generic;

namespace FirstLook.FirstLookServices.DomainModel.Listings.Commands.TransferObjects.Envelopes
{
    public class FetchMarketListingsResultsDto
    {
        public List<MarketListingDto> MarketListings { get; set; }

        public SearchResultsDto SearchResult { get; set; }
    }
}
