﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.FirstLookServices.DomainModel.Listings
{
    public class Module : IModule
    {
        #region IModule Members

        /// <summary>
        /// Add pertinent interfaces and impelementing classes to the registry.
        /// </summary>
        public void Configure(IRegistry registry)
        {
            registry.Register<Commands.Module>();
        }

        #endregion

    }
}
