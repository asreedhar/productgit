﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.FirstLookServices.DomainModel
{
    public class Module : IModule
    {

        #region IModule Members

        /// <summary>
        /// Add pertinent interfaces and impelementing classes to the registry.
        /// </summary>
        public void Configure(IRegistry registry)
        {
            registry.Register<Auctions.Module>();

            registry.Register<Books.Module>();

            registry.Register<BookMapping.Module>();

            registry.Register<Dealers.Module>();

            registry.Register<Vehicles.Module>();

            registry.Register<Reports.Module>();

            registry.Register<Listings.Module>();

            registry.Register<Valuations.Module>();

            registry.Register<Appraisals.Module>();

            registry.Register<StorePerformance.Module>();

            registry.Register<AppraisalPhotos.Module>();

            registry.Register<InventoryDetails.Module>();

            registry.Register<PricingHistory.Module>();

            registry.Register<SalesPeople.Module>();

            registry.Register<Inventory.Module>();

            registry.Register<UserRole.Module>();

            registry.Register<UpdateListPrice.Module>();

            registry.Register<AmazonSnsSubscriber.Module>();

            registry.Register<DealerInfo.Module>();
            registry.Register<MarketListings.Module>();

            registry.Register<PricingAnalysis.Module>();

            registry.Register<ApproveAd.Module>();

            registry.Register<PriceComparisons.Module>();

            registry.Register<Trim.Module>();

            registry.Register<MaxPricingToolReports.Module>();

            registry.Register<PricingSummary.Module>();

            registry.Register<SearchCriteria.Module>();
        }

        #endregion
    }
}
