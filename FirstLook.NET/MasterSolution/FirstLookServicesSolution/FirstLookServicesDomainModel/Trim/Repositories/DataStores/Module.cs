﻿using System;
using FirstLook.Common.Core.Registry;

namespace FirstLook.FirstLookServices.DomainModel.Trim.Repositories.DataStores
{
    [Serializable]
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ITrimInfoDataStore, TrimInfoDataStore>(ImplementationScope.Shared);
        }
    }
}
