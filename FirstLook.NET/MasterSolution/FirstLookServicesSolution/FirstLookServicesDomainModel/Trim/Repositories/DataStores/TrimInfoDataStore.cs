﻿using System.Data;
using System.Data.SqlClient;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.Trim.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.Trim.Repositories.DataStores
{
    class TrimInfoDataStore : ITrimInfoDataStore
    {

        #region Class Members

        private string DatabaseName = "Merchandising";

        #endregion

        #region ITrimDataStore Members

        public IDataReader TrimInfo(TrimInfoInput triminfo)
        {
            try
            {
                using (IDbConnection connection = Database.Connection(DatabaseName))
                {
                    using (IDbCommand command = connection.CreateCommand())
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.CommandText = "builder.updateVehicleStyleID";

                        Database.AddWithValue(command, "BusinessUnitID", triminfo.DealerId, DbType.Int32);
                        Database.AddWithValue(command, "InventoryId", triminfo.InventoryId, DbType.Int32);
                        Database.AddWithValue(command, "ChromeStyleID", triminfo.ChromeStyleId, DbType.Int32);

                        IDataReader reader = command.ExecuteReader();
                        return reader;

                    }
                }
            }

            catch (SqlException se)
            {
                throw se;
            }

        }
    }


        #endregion
}
    




