﻿using System.Data;
using FirstLook.FirstLookServices.DomainModel.Trim.Repositories.Entities;

namespace FirstLook.FirstLookServices.DomainModel.Trim.Repositories.DataStores
{
    public interface ITrimInfoDataStore
    {
        IDataReader TrimInfo(TrimInfoInput TrimInfo);
    }
}
