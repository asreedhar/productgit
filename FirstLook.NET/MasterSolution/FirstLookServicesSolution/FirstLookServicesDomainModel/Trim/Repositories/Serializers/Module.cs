﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Trim.Model;

namespace FirstLook.FirstLookServices.DomainModel.Trim.Repositories.Serializers
{
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ISerializer<TrimInfo>, TrimInfoSerializer>(ImplementationScope.Shared);
        }
    }
}
