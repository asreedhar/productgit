﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;
using FirstLook.FirstLookServices.DomainModel.Trim.Model;

namespace FirstLook.FirstLookServices.DomainModel.Trim.Repositories.Serializers
{
    public class TrimInfoSerializer : Serializer<TrimInfo>
    {

        public override TrimInfo Deserialize(IDataRecord record)
        {
            TrimInfo obj = new TrimInfo()
            {
                ChromeStyleID = DataRecord.GetInt32(record, "ChromeStyleID", 0),
               
            };
            return obj;
        }
    }
}
