﻿using System;
using FirstLook.FirstLookServices.DomainModel.Trim.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Trim.Model;
using FirstLook.FirstLookServices.DomainModel.Trim.Repositories.Entities;
using FirstLook.FirstLookServices.DomainModel.Trim.Repositories.Gateways;

namespace FirstLook.FirstLookServices.DomainModel.Trim.Repositories
{
    public class TrimRepository : IRepository
    {
        #region IRepository Members

        public bool TrimInfo(InsertTrimArgumentsDto parameters)
        {
            TrimInfoInput trimInfoInput = new TrimInfoInput();
            trimInfoInput.InventoryId = parameters.InventoryId;
            trimInfoInput.DealerId = parameters.DealerId;
            trimInfoInput.ChromeStyleId = parameters.ChromeStyleID;
            return new TrimInfoGateway().Get(trimInfoInput);
        }

         #endregion

        
    }
}
