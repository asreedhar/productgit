﻿
namespace FirstLook.FirstLookServices.DomainModel.Trim.Repositories.Entities
{
    public class TrimInfoInput
    {
        public int InventoryId { get; set; }
        public int DealerId { get; set; }
        public int ChromeStyleId { get; set; }

    }
}
