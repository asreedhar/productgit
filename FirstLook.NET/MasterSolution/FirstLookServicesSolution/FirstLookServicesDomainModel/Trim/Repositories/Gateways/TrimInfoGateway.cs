﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.FirstLookServices.DomainModel.Trim.Model;
using FirstLook.FirstLookServices.DomainModel.Trim.Repositories.DataStores;
using FirstLook.FirstLookServices.DomainModel.Trim.Repositories.Entities;
using FirstLook.Common.Core;

namespace FirstLook.FirstLookServices.DomainModel.Trim.Repositories.Gateways
{
    public class TrimInfoGateway : GatewayBase
    {
        public bool Get(TrimInfoInput triminfo)
        {
            string key = CreateCacheKey("" + triminfo.InventoryId ,triminfo.DealerId,triminfo.ChromeStyleId );
            TrimInfo trmInfo = Cache.Get(key) as TrimInfo;
            bool flag = false;
             if (trmInfo == null)
            {
                var serializer = Resolve<ISerializer<TrimInfo>>();

                var datastore = Resolve<ITrimInfoDataStore>();



                IDataReader reader = datastore.TrimInfo(triminfo);

                if (reader == null)
                {
                     flag = false;
                }
                else { flag = true; }
                return flag;

             }
             return flag;
        }

    }
}

    