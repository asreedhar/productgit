﻿using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Trim.Model;

namespace FirstLook.FirstLookServices.DomainModel.Trim.Repositories
{
    class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<DataStores.Module>();

            registry.Register<Serializers.Module>();

            registry.Register<IRepository, TrimRepository>(ImplementationScope.Shared);

        }
    }
}
