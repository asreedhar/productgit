﻿using FirstLook.FirstLookServices.DomainModel.Trim.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.Trim.Model
{
    public interface IRepository
    {
        
        bool TrimInfo(InsertTrimArgumentsDto parameters);
    }
}
