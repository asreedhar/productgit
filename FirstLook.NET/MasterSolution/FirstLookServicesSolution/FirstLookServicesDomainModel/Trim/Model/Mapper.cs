﻿using FirstLook.FirstLookServices.DomainModel.Trim.Commands.TransferObjects;

namespace FirstLook.FirstLookServices.DomainModel.Trim.Model
{
    public class Mapper
    {
        public static TrimDataDto Map(TrimInfo objs)
        {
            TrimDataDto trimDataDto = new TrimDataDto();
            trimDataDto.ChromeStyleId = objs.ChromeStyleID;
            trimDataDto.Flag = objs.Flag;

            return trimDataDto;
        }

    }
}
