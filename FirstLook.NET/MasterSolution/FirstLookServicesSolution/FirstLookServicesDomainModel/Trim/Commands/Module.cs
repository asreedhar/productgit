﻿using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Trim.Commands.Impl;

namespace FirstLook.FirstLookServices.DomainModel.Trim.Commands
{
    class Module : IModule
    {
        #region IModule Members

        public void Configure(IRegistry registry)
        {
            registry.Register<ICommandFactory, CommandFactory>(ImplementationScope.Shared);
        }

        #endregion
    }
}
