﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.Trim.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.Trim.Commands
{
    public interface ICommandFactory
    {
      ICommand<InsertTrimDetailsResultsDto, IdentityContextDto<InsertTrimArgumentsDto>> CreateInsertTrimDetailsCommand();
    }
    
}
