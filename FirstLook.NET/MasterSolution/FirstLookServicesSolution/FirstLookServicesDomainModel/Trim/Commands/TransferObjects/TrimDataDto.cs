﻿
namespace FirstLook.FirstLookServices.DomainModel.Trim.Commands.TransferObjects
{
    public class TrimDataDto
    {
        public int ChromeStyleId { get; set; }
        public bool Flag { get; set; }
    }
}
