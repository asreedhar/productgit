﻿
namespace FirstLook.FirstLookServices.DomainModel.Trim.Commands.TransferObjects.Envelopes
{
    public class InsertTrimDetailsResultsDto
    {
        public InsertTrimDetailsResultsDto Arguments { get; set; }

        public TrimDataDto TrimData { get; set; }
    }
}
