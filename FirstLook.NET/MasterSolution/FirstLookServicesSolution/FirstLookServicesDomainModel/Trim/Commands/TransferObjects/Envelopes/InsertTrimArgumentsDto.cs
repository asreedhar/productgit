﻿
namespace FirstLook.FirstLookServices.DomainModel.Trim.Commands.TransferObjects.Envelopes
{
    public class InsertTrimArgumentsDto
    {
        public int InventoryId { get; set; }
        public int DealerId { get; set; }
        public int ChromeStyleID { get; set; }
    }
}
