﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Trim.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Trim.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Trim.Model;

namespace FirstLook.FirstLookServices.DomainModel.Trim.Commands.Impl
{
    class InsertTrimCommand : ICommand<InsertTrimDetailsResultsDto, IdentityContextDto<InsertTrimArgumentsDto>>
    {
        #region ICommand<InsertTrimDetailsResultsDto,IdentityContextDto<FetchStorePerformanceArgumentsDto>> Members

        public InsertTrimDetailsResultsDto Execute(IdentityContextDto<InsertTrimArgumentsDto> parameters)
        {
            IResolver resolver = RegistryFactory.GetResolver();
            var trimRepository = resolver.Resolve<IRepository>();
            InsertTrimArgumentsDto put = new InsertTrimArgumentsDto()
            {
                InventoryId = parameters.Arguments.InventoryId,
                DealerId = parameters.Arguments.DealerId,
                ChromeStyleID = parameters.Arguments.ChromeStyleID
            };

           bool flagTrimmer =  trimRepository.TrimInfo(put);


            return new InsertTrimDetailsResultsDto()
            {
                TrimData = new TrimDataDto()
                {
                    ChromeStyleId = parameters.Arguments.ChromeStyleID,
                    Flag = flagTrimmer
                }
            };
        }
        #endregion

    }
}
