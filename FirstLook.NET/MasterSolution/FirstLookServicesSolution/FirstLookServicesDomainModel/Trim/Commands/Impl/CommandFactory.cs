﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.Trim.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.DomainModel.Trim.Commands.Impl
{
    class CommandFactory : ICommandFactory
    {
        #region ICommandFactory Members

        public ICommand<InsertTrimDetailsResultsDto, IdentityContextDto<InsertTrimArgumentsDto>> CreateInsertTrimDetailsCommand()
        {
            return new InsertTrimCommand();
        }
        #endregion



      
    }
}
