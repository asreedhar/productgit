﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ServiceStack.ServiceClient.Web;
using FirstLook.FirstLookServices.WebServiceStack.Services.Dealers;

namespace FirstLookServicesWebServiceStack_Test.Dealers
{
    [TestClass]
    public class Dealers
    {
        [TestMethod]
        public void Get()
        {
            var client = new JsonServiceClient("http://localhost:51133/")
            {
                UserName = "dreddy",
                Password = "N@d@123",
            };

            var request = new Dealers();

            var response = client.Send<DealersResponse>(request);    

            Assert.IsTrue(response.Dealers.Count > 0);
        }
    }
}
