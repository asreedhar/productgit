﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Inventory.Commands.TransferObjects.Envelopes;
using FirstLookServicesDomainModel_Test.Common;
using NUnit.Framework;

namespace FirstLookServicesDomainModel_Test.Commands.AgingInventory
{
    public class FetchAgingInventoryCommandTest : TestBase
    {
        [Test]
        public void TestAgingInventoryCommand()
        {
            var factory = RegistryFactory.GetResolver().Resolve<FirstLook.FirstLookServices.DomainModel.Inventory.Commands.ICommandFactory>();
            var broker = GetBroker(TestUserName, TestAuthorityName, GetClient(TestUserName, TestAuthorityName));
            var argumentsDto = new FetchAgingInventoryArgumentsDto
            {
                BusinessUnitId = 105239
            };

            ICommand<FetchAgingInventoryResultsDto, IdentityContextDto<FetchAgingInventoryArgumentsDto>> command = factory.CreateFetchAgingInventoryCommand();

            FetchAgingInventoryResultsDto resultsDto =
                 command.Execute(WrapInContext(argumentsDto, TestAuthorityName, TestUserName));

            Assert.NotNull(resultsDto);
        }
    }
}
