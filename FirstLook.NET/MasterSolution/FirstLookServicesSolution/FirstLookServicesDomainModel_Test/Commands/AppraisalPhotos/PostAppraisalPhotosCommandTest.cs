﻿using System;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.TransferObjects.Envelopes;
using FirstLookServicesDomainModel_Test.Common;
using NUnit.Framework;


namespace FirstLookServicesDomainModel_Test.Commands.AppraisalPhotos
{
     [TestFixture]
    public class PostAppraisalPhotosCommandTest : TestBase
    {
         [Test]
         public void TestAppraisalPhotosCommand()
         {
             var factory = RegistryFactory.GetResolver().Resolve<FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.ICommandFactory>();

             var broker = GetBroker(TestUserName, TestAuthorityName, GetClient(TestUserName, TestAuthorityName));

             var argumentsDto = new PostAppraisalPhotosArgumentsDto()
             {
                 DealerID = 101620,
                 UniqueID = Guid.NewGuid().ToString(),
                 Photos = "/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMQEhUSEhQVFhISFhQYFxYWFRQUFRQYFRYXFhYUFBQYHCggGBolHBUUITEhJSkrLi4uFx8zOD8sNygtLi0BCgoKDg0OGhAQGywkHyQsLCwsLCwsLCwsLCwsLCssLCwsLCwsLCwsLDcsODc4NzcrNywsLCwrKysrKywsKysrK//AABEIAJQAXAMBIgACEQEDEQH/xAAbAAACAgMBAAAAAAAAAAAAAAAEBQMGAAECB//EADQQAAEDAgQEBAUDBQEBAAAAAAEAAhEDBAUSITFBUWFxBhMikTKBobHRUsHwI0Ji4fEWFf/EABkBAAMBAQEAAAAAAAAAAAAAAAIDBAEABf/EACMRAAICAwABBAMBAAAAAAAAAAABAhEDITESIjJBURNhkQT/2gAMAwEAAhEDEQA/APVoXLmcljagWzUCiHkXlBRvZyOyFxHFG0h6tDBOvLiqZf8Aip7m5WemZLnTJ7BcoN8Ockh5jb4qa6SBHVLGV2kwCCVVbzFH1PjcZGsnUgckPaXxbpJaXEGefGD0RP8AzmrMXZZKTUcWkTEnjB0Rlneh8zoRwKRLFJdGqaYe1y6zqNhW0oMklZKiK5zLqNLtbua4S3YoXErxlFpcTHzAntJ1SvAcXblyu9J56RHZUfxRjXn1nEH0AkN7SrVC3RG5EviLGnV3AcGkweYMQNOWvuq66qRvoo6l0RshKlclURjQtuwrzOu6h82Sosy0UdGBNO5LTomNC+MgzBG3z3+yVUm9lNTqiem3suo1F3w288xs8RoRyRqqeH3fl5XToIzDp/2FbbeHtDhxAIXnZsfgyzFPyRpRVasGJCndSKrWP3ZZUAH6R9yl41bDk6Q9NxS8pzmNIIaYJIknkOese6oF3XJJJTy6uyxmQiJInh/1V643XpQVEMiElahaJUlJpKMHpwto5lg5wmETZYM57tdELmg1Bizy9FzBV7tsCZlAhEf+XYRsg/KH+MoVKsdQrj4ZvzlyvOg2lIsXwryKkcCjMLqimQdD0XZIqcGZF+Mi5MeDsqt4ooE1QQP7G/dybUrmi7/A/MfbRSvtc2oqAjgTDvqoI+hlctop9/eeY1ocACOI0n5DsEluXKw3mEObasuDs55bEcIkOnjqOXuq3U1XpxIWZSbJTmzpgRollo3VPbBmuqCf0Mx6G1oOiZ06UcFBaVKY0JCcUXtI0SWh6YNTcRzR9OsRwQ772mzRx2XTMWoO0DhK1ICTEni+nmbmHBVCjX9QXomIU21GOAMgg+/BeZOnP2n6T+EzH8oXP4Y4fcFnxtI5nce/BdG5aNyB30PsVDfsc2m4Fj2yAebemhEjvmPZNqVxRIH9QgwJBYT9QQlyijVNl08YWTP/AJzmCP6Ya4DsdSPcrxqqF7d40vGttn0zq57CABwA/udy2XilcJmLgqRuhIEhEN6vK7oUQWtHNFPwotGaC5p5brWxkVoELy0yHSm+E4g5zg0boSmzTI1hgxM9OaaYDZRUB6rJU0MimjjG5YdZkxol9k9hPqiepP4hXbxHgpqBr2gZo4quW9hUa7WkCecj8IY1RslYbavaYyaOBHEe2mmqqGJsy1qg4Z3ezpI07FejW+GANzFrQeQAHvzVK8R2Z84kcpPYD/S6D2BkToXuv6uR1MVHFjhBaSSI5AHb5LtuIwIyNMcZeJ7wd+qDqDdbDe6bSE7L9iNc1nEu1Lv5HYKkYnQyPLRsNuyt7TmdvoP2SXxCwTI3P7T+R7JEMi8qMggK2iG9grTh9w0NhUujVgDojad4WtWzjZVjqixV6zXuytiSmGD24a8SdlVadX06fFvPFbtr+qwyZ9lzTDTR67UpipTEQqziVXyamV431BCW4biNaqA1uYDmPyj8Ya00y1xE9SJlDs7SGttdtc3hsqljtk6rUc2nGYtG+wGYyfso8JunZXa/DKPpVwCSfigA/dDHUjZcE9t4aa3Wo+f8W6D3O/sj20KTdBTZHUA/UrqvcpdUutU2xfiFt9DJ4u0CEo1qYqMdVAdTa71DQ5mkEECeOv0S258Sh8DIWkCAM2k9dEiuaznmXO/19eqDFgldyJb2FX1drqtQsaGMLiQ0GQAeEqa2hwI57JSxuuh4Iq3rx8k9xGwl9hps4cDwPA6J/huHMedS9o11BnlGvul1s/OI5ou2tqrfgIhLf7HxSLhY4PRyic73Q4aujXTLx7oHFPDrDVdV2HBjdh8+PH3UGGUrgnUiOnZObp0MglL5wKhDQohjT/P5sk7MRzVHs4cD1G/3+ikxzFg0ZW7lVincEOzcjP5TMcLTbFZMiTSRZK1dAvfqu31EO56AYJKkHU7qBz109aqsjurWQpWZbn1Kchc29KNSiGsS29jox0btro0z0Tyzx2N0kfSW6NKUDSYxWi12PiOD8UDkt3OKPuPSJDOJ4noEpw7Dsx2Vgo2oaEqTS4NUW+iHGLbMWtbuGuIH6gIkDrr81X3FWPxBTOdhBgjMfbKh7zD/AD6fm02w8aPaOJ/V3Kfi9ovJjvaIrNpcwE6Aad0XTGmiUYc86jhxTG3dp80qcdk7m2JLZkkn9I+vD+dERUoS89NFNh9A5J09Tvpw+6NuKHrPGRJ7/wAhUTdIfjxekXBi7axFeStmjop3IYokWTRSWwgqWgxSNtyDCBsNRHOHnKO6OLtEtswRuiLmrASatjaoSY9Vl4jgHH7Kexc0DP8AE0iHyYA6/JKsUqnPm5SPytVb0tovDdWugDpJ1MfT5q/GqiKtK2HXtKmZdRc1w3dlMxO5I/dD0NBuAlFk8h7SNzofnoZToUgi8EyLJTdpEdi0ZWH/ACH7JrXpgO06fUf6C0sWZOF8Pb/DPJErvygsWKRmg7GCUx8sRK2sQSCRum1RXCxYuj02XCtOOpPGCo30RlJjcD7grFi9BCGtA2WDp3T8NlbWIkSZOn//2Q==",
                 SequenceID = 1,
                 Source = 7,
                 FileName = "Chrysanthemum - Copy.jpg"
             };

             ICommand<PostAppraisalPhotosResultsDto, IdentityContextDto<PostAppraisalPhotosArgumentsDto>> command = factory.CreatePostAppraisalPhotosCommand();

             PostAppraisalPhotosResultsDto resultsDto =
                command.Execute(WrapInContext(argumentsDto, TestAuthorityName, TestUserName));

             Assert.NotNull(resultsDto);
         }
    }
}
