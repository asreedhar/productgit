﻿using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Books.Commands.TransferObjects.Envelopes;
using FirstLookServicesDomainModel_Test.Common;
using NUnit.Framework;
using ICommandFactory = FirstLook.FirstLookServices.DomainModel.Books.Commands.ICommandFactory;

namespace FirstLookServicesDomainModel_Test.Commands.Books
{
    [TestFixture]
    public class FetchDealerBooksCommand : TestBase
    {        
        [Test]
        public void FetchDealersCommandTest()
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            BrokerDto broker = GetBroker(TestUserName, TestAuthorityName, GetClient(TestUserName, TestAuthorityName));

            FetchDealerBooksArgumentsDto argumentsDto = new FetchDealerBooksArgumentsDto
                                                            {
                                                                DealerId = 101590
                                                            };

            ICommand<FetchDealerBooksResultsDto, IdentityContextDto<FetchDealerBooksArgumentsDto>> command = factory.CreateFetchDealerBooksCommand();

            FetchDealerBooksResultsDto resultsDto =
                command.Execute(WrapInContext(argumentsDto, TestAuthorityName, TestUserName));

            Assert.NotNull(resultsDto);
        }
        
    }
}
