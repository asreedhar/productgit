﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.StorePerformance.Commands.TransferObjects.Envelopes;
using FirstLookServicesDomainModel_Test.Common;
using NUnit.Framework;

namespace FirstLookServicesDomainModel_Test.Commands.StorePerformance
{
    [TestFixture]
    public class FetchStorePerformanceCommandTest : TestBase
    {
        
        [Test]
        public void TestStorePerformanceCommand()
        {
            var factory = RegistryFactory.GetResolver().Resolve<FirstLook.FirstLookServices.DomainModel.StorePerformance.Commands.ICommandFactory>();

            var broker = GetBroker(TestUserName, TestAuthorityName, GetClient(TestUserName, TestAuthorityName));

            var argumentsDto = new FetchStorePerformanceArgumentsDto
                                                            {
                                                                VIN = "ZARDA1360H1311699",
                                                                BusinessUnitID = 100148,
                                                                Mileage = 10000
                                                            };

            ICommand<FetchStorePerformanceResultsDto, IdentityContextDto<FetchStorePerformanceArgumentsDto>> command = factory.CreateFetchStorePerformanceCommand();

            FetchStorePerformanceResultsDto resultsDto =
                command.Execute(WrapInContext(argumentsDto, TestAuthorityName, TestUserName));

            Assert.NotNull(resultsDto);
        }
    }
}
