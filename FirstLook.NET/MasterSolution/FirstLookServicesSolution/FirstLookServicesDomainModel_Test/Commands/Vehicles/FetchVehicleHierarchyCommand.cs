﻿using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;
using FirstLookServicesDomainModel_Test.Common;
using NUnit.Framework;
using ICommandFactory = FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.ICommandFactory;

namespace FirstLookServicesDomainModel_Test.Commands.Vehicles
{
    [TestFixture]
    public class FetchVehicleHierarchyCommand : TestBase
    {        
        [Test]
        public void FetchVehicleHierarchyCommandTest()
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            var argumentsDto = new FetchVehicleHierarchyArgumentsDto();

            ICommand<FetchVehicleHierarchyResultsDto, FetchVehicleHierarchyArgumentsDto> command = factory.CreateFetchVehicleHierarchyCommand();

            FetchVehicleHierarchyResultsDto resultsDto = command.Execute(argumentsDto);

            Assert.NotNull(resultsDto);
        }
        
    }
}
