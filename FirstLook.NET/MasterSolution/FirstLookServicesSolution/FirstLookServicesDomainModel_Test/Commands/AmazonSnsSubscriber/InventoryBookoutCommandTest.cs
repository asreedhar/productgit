﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Commands.TransferObjects;
using FirstLookServicesDomainModel_Test.Common;
using NUnit.Framework;

namespace FirstLookServicesDomainModel_Test.Commands.AmazonSnsSubscriber
{
    public class InventoryBookoutCommandTest:TestBase
    {

        [Test]
        public void TestInventoryBookoutCommand()
        {
            var factory = RegistryFactory.GetResolver().Resolve<FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Commands.ICommandFactory>();
            var broker = GetBroker(TestUserName, TestAuthorityName, GetClient(TestUserName, TestAuthorityName));
            var argumentsDto = new InventoryBookoutCommandArgumentDto
            {
                InventoryId = 39388109
            };

            ICommand<InventoryBookoutCommandResultDto, IdentityContextDto<InventoryBookoutCommandArgumentDto>> command = factory.CreateInventoryBookoutCommand();

            InventoryBookoutCommandResultDto resultsDto =
                 command.Execute(WrapInContext(argumentsDto, TestAuthorityName, TestUserName));

            Assert.NotNull(resultsDto);
        }
    }
}
