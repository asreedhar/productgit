﻿using System.Diagnostics;
using System.Linq;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Model;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Repositories.Entities;
using FirstLookServicesDomainModel_Test.Common;
using NUnit.Framework;
using ICommandFactory = FirstLook.FirstLookServices.DomainModel.MarketListings.Commands.ICommandFactory;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Model;
using System.Collections.Generic;

namespace FirstLookServicesDomainModel_Test.Commands.MarketListings
{
    [TestFixture]
    public class FetchMarketListingCommand:TestBase
    {
        /// <summary>
        /// This test is useful for debugging equipment matching issues.  You can run this (in debug mode) and it will output
        /// some information about what equipment was obtained from the market listings api call, what equipment was obtained 
        /// from the database, and what equipment in the DB matched equipment in the Market Listings response.
        /// Note: Right now you either need to manually set the Model or leave it out (Model = null).  The Model from the database is not 
        /// the same in all cases as those found in the market listings data.  There is a fix for this that could be implemented:
        /// Call the mapping data to obtain the correct Model for a given ChromeStyleId, but I just didn't implement it here.
        /// </summary>
        [Test]
        public void MarketListingEquipmentMatchingTest()
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<List<ListingsAndCounts>, IdentityContextDto<List<FetchMarketListingsArgumentsDto>>> command = factory.CreateFetchMarketListingsCommand();

            var inventoryInput = new LoadInventoryInput
            {
                DealerId = 100148,
                SaleStrategy = "A",
                InventoryFilter = string.Empty
            };

            var loadinventoryRepository = RegistryFactory.GetResolver().Resolve<IRepository>();
            IList<LoadInventoryDetail> inventoryList = loadinventoryRepository.Loadinventory(inventoryInput).ToList(); // Load#Inventory Table

            if (inventoryList.All(x => x.InventoryId != 39668921))
                Debug.WriteLine("inventory item not found.");

            var marketListingArgs = inventoryList.Where(i => i.InventoryId == 39668921).Select(
                x => new FetchMarketListingsArgumentsDto
                {
                    BusinessUnitId = inventoryInput.DealerId,
                    InventoryId = x.InventoryId,
                    Year = x.VehicleYear,
                    Make = x.Make,
                    Model = "7 series",
                    Aggregations = new List<string> { "equipment_terms", "model_terms" },
                    Size = 3,
                    IsRequiredAggregations = true
                }).ToList()[0];

            /*
                        var marketListingArgs = new FetchMarketListingsArgumentsDto
                        {

                            Year = 2012,
                            Make = "Honda",
                            Model = null,
                            Trims = null,
                            MinMileage = 1,
                            MaxMileage = 99999,
                            MinPrice = 200,
                            MaxPrice = 999999,
                            Latitude = null,
                            Longitude = null,
                            Distance = 2000,
                            Size = 10,
                            From = 10,
                            OemCertified = false,
                            Transmissions = null,
                            DriveTrains =null,
                            Engines = null,
                            FuelTypes = null,
                            Aggregations = new List<string> {"price_stats"},
                            IsRequiredAggregations = true
                        };

             */
            Debug.WriteLine("Inventory info:");
            Debug.WriteLine(string.Format("Inventory Id: {0}", marketListingArgs.InventoryId));
            Debug.WriteLine(string.Format("Inventory Year: {0}", marketListingArgs.Year));
            Debug.WriteLine(string.Format("Inventory Make: {0}", marketListingArgs.Make));
            Debug.WriteLine(string.Format("Inventory Model: {0}", marketListingArgs.Model));

            var contextDto = new IdentityContextDto<List<FetchMarketListingsArgumentsDto>>();

            if (contextDto.Arguments == null)
                contextDto.Arguments = new List<FetchMarketListingsArgumentsDto>();  // this should probably live in the constructor

            contextDto.Arguments.Add(marketListingArgs);
            contextDto.Identity = new IdentityDto
            {
                AuthorityName = TestAuthorityName,
                Name = TestUserName
            };

            var resultsDto = command.Execute(contextDto);

            

            Assert.NotNull(resultsDto);

            foreach (var lc in resultsDto)
            {
                Debug.WriteLine(string.Empty);
                Debug.WriteLine("--- Market Listing Equipment Facets ----");
                foreach (var fc in lc.Facets.FacetCounts)
                {
                    Debug.WriteLine(string.Format("facetCount  Key: {0}, DocCount: {1}", fc.Key, fc.DocCount));
                }
            }

        }
        
    }
}
