﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.UserRole.Commands.TransferObjects.Envelopes;
using FirstLookServicesDomainModel_Test.Common;
using NUnit.Framework;

namespace FirstLookServicesDomainModel_Test.Commands.UserRole
{
    [TestFixture]
   public class FetchUserRoleCommandTest:TestBase
    {

        [Test]
        public void TestStorePerformanceCommand()
        {
            var factory = RegistryFactory.GetResolver().Resolve<FirstLook.FirstLookServices.DomainModel.UserRole.Commands.ICommandFactory>();

            var broker = GetBroker(TestUserName, TestAuthorityName, GetClient(TestUserName, TestAuthorityName));

            var argumentsDto = new FetchUserRoleArgumentsDto
            {
               UserName = "rgade"
            };

            ICommand<FetchUserRoleResultsDto, IdentityContextDto<FetchUserRoleArgumentsDto>> command = factory.CreateFetchUserRoleCommand();

            FetchUserRoleResultsDto resultsDto =
                command.Execute(WrapInContext(argumentsDto, TestAuthorityName, TestUserName));

            Assert.NotNull(resultsDto);
        }
    }
}
