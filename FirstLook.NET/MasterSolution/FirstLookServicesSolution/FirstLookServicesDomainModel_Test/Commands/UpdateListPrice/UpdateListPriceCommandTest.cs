﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Clients.Model.Authorities.Dealers;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Repositories.Entities;
using FirstLook.Pricing.DomainModel.Internet.ObjectModel.Distribution;
using FirstLookServicesDomainModel_Test.Common;
using NUnit.Framework;
using IRepository = FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Model.IRepository;

namespace FirstLookServicesDomainModel_Test.Commands.UpdateListPrice
{
    [TestFixture]
    class UpdateListPriceCommandTest : TestBase
    {
        [Test]
        public void TestUpdateListPriceCommand()
        {
            IResolver resolver = RegistryFactory.GetResolver();
            var inventoryListPriceRepository = resolver.Resolve<IRepository>();

            InventoryDto inventoryInput = new InventoryDto()
                                          {
                                              BusinessUnitId = 101620,
                                              InventoryId = 37006185,
                                              UserName = "mharkhani",//parameters.Arguments.UserName
                                              NewListPrice = 10000
                                          };

            //------------------------------Updating all the changes while repricing the inventory(same as PING functionality)
            inventoryInput = inventoryListPriceRepository.FetchInventoryDetails(inventoryInput);
            // Return HandleDetails and validate Inventory

            if (inventoryListPriceRepository.GetMemberType(inventoryInput.UserName) == MemberType.User.ToString())
            {
                inventoryListPriceRepository.InsertMarketHistory(inventoryInput);
                // Call "Pricing.VehicleMarketHistory#Create"
            }

            inventoryListPriceRepository.InsertRepriceEvent(inventoryInput); // Call "dbo.InsertRepriceEvent"
            //-------------------------------------------------------------------------------------------------------------


            //-------------------------------Distribute---------------------------------------------------------------------

            DistributeRepriceCommand.Execute(inventoryInput.BusinessUnitId, inventoryInput.InventoryId, Convert.ToInt32(inventoryInput.NewListPrice));

            //-----------------------------------------------------------------------------------------------------------------


            InventoryListPrice listprice = new InventoryListPrice()
                                         {
                                             InventoryId = inventoryInput.InventoryId,
                                             OldListPrice = inventoryInput.OldListPrice,
                                             NewListPrice = inventoryInput.NewListPrice
                                         };

            Assert.NotNull(listprice);
        }
    }
}


