﻿
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Sales.Envelopes;
using FirstLookServicesDomainModel_Test.Common;
using NUnit.Framework;


namespace FirstLookServicesDomainModel_Test.Commands.Sales
{
    [TestFixture]
    class FetchSalesCommandTest:TestBase
    {
        [Test]
        public void TestSalesDetailCommand()
        {
            var factory = RegistryFactory.GetResolver().Resolve<FirstLook.FirstLookServices.DomainModel.Auctions.Commands.ICommandFactory>();

            //var broker = GetBroker(TestUserName, TestAuthorityName, GetClient(TestUserName, TestAuthorityName));

            var argumentsDto = new FetchSalesArgumentsDto()
            {
                Lat = 18.4739,
                Long = -66.7295
            };

            ICommand<FetchSalesResultsDto, FetchSalesArgumentsDto> command = factory.CreateFetchSalesDetailsCommand();

            FetchSalesResultsDto resultsDto =
                command.Execute(argumentsDto);

            Assert.NotNull(resultsDto);
        }
    }
}
