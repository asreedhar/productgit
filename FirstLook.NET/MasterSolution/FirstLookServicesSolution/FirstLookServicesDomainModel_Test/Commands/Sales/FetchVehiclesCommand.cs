﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Common.Core.Command;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Sales.Envelopes;
using FirstLookServicesDomainModel_Test.Common;
using NUnit.Framework;

namespace FirstLookServicesDomainModel_Test.Commands.Sales
{
    [TestFixture]
    class FetchVehiclesCommand :TestBase
    {
        [Test]
        public void TestVehiclesDetailCommand()
        {
            var factory = FirstLook.Common.Core.Registry.RegistryFactory.GetResolver().Resolve<FirstLook.FirstLookServices.DomainModel.Auctions.Commands.ICommandFactory>();

            //var broker = GetBroker(TestUserName, TestAuthorityName, GetClient(TestUserName, TestAuthorityName));

            var argumentsDto = new FetchVehiclesArgumentsDto()
            {
                DealerId = 105858,
                LocationId = 355,
                Date = "2014-09-03"
            };

            ICommand<FetchVehiclesResultsDto, FetchVehiclesArgumentsDto> command = factory.CreateFetchAuctionVehiclesCommand();

            FetchVehiclesResultsDto resultsDto =
                command.Execute(argumentsDto);

            Assert.NotNull(resultsDto);
        }
    }
}
