﻿using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects.Envelopes;
using FirstLookServicesDomainModel_Test.Common;
using NUnit.Framework;
using ICommandFactory = FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.ICommandFactory;

namespace FirstLookServicesDomainModel_Test.Commands.Appraisals
{
    [TestFixture]
    public class FetchAppraisalTypesCommand : TestBase
    {        
        [Test]
        public void FetchAppraisalTypesCommandTest()
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<FetchAppraisalTypesResultsDto, FetchAppraisalTypesArgumentsDto> command = factory.CreateFetchAppraisalTypesCommand();

            var argumentsDto = new FetchAppraisalTypesArgumentsDto();

            FetchAppraisalTypesResultsDto results = command.Execute(argumentsDto);

            Assert.NotNull(results);
        }
        
    }
}
