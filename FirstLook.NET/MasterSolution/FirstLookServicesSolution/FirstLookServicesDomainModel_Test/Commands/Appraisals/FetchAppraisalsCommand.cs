﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects.Envelopes;
using FirstLookServicesDomainModel_Test.Common;
using NUnit.Framework;
using ICommandFactory = FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.ICommandFactory;

namespace FirstLookServicesDomainModel_Test.Commands.Appraisals
{
    [TestFixture]
    public class FetchAppraisalsCommand : TestBase
    {        
        [Test]
        public void FetchAppraisalCommandTest()
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<FetchAppraisalsResultsDto, IdentityContextDto<FetchAppraisalsArgumentsDto>> command = factory.CreateFetchAppraisalsCommand();

            var argumentsDto = new FetchAppraisalsArgumentsDto
                                   {
                                       Days = null,
                                       DealerId = 101590
                                   };

            FetchAppraisalsResultsDto resultsDto = command.Execute(WrapInContext(argumentsDto, TestAuthorityName, TestUserName));

            Assert.NotNull(resultsDto);
        }
        
    }
}
