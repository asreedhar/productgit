﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.SalesPeople.Commands.TransferObjects.Envelopes;
using FirstLookServicesDomainModel_Test.Common;
using NUnit.Framework;

namespace FirstLookServicesDomainModel_Test.Commands.SalesPeople
{
   public class FetchSalesPeopleCommandTest:TestBase
    {
       [Test]
       public void TestSalesPeopleCommand()
       {
           var factory = RegistryFactory.GetResolver().Resolve<FirstLook.FirstLookServices.DomainModel.SalesPeople.Commands.ICommandFactory>();
           var broker = GetBroker(TestUserName, TestAuthorityName, GetClient(TestUserName, TestAuthorityName));
           var argumentsDto = new FetchSalesPeopleArgumentsDto
                                  {
                                      BusinessUnitId = 105239
                                  };

           ICommand<FetchSalesPeopleResultsDto, IdentityContextDto<FetchSalesPeopleArgumentsDto>> command = factory.CreateFetchSalesPeopleCommand();

           FetchSalesPeopleResultsDto resultsDto =
                command.Execute(WrapInContext(argumentsDto, TestAuthorityName, TestUserName));

           Assert.NotNull(resultsDto);
       }
    }
}
