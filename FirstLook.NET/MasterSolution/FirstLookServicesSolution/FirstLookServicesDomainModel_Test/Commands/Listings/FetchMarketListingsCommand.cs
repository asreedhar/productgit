﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Listings.Commands.TransferObjects.Envelopes;
using FirstLookServicesDomainModel_Test.Common;
using NUnit.Framework;
using ICommandFactory = FirstLook.FirstLookServices.DomainModel.Listings.Commands.ICommandFactory;

namespace FirstLookServicesDomainModel_Test.Commands.Listings
{
    [TestFixture]
    public class FetchMarketListingsCommand : TestBase
    {        
        [Test]
        public void FetchMarketListingsCommandTest()
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<FetchMarketListingsResultsDto, IdentityContextDto<FetchMarketListingsArgumentsDto>> command = factory.CreateFetchMarketListingsCommand();

            var argumentsDto = new FetchMarketListingsArgumentsDto
            {
                DealerId = 105239,
                Distance = 100,
                SearchType = "Precision",
                Vin = "YS3EB49EX23051831"
            };

            FetchMarketListingsResultsDto resultsDto = command.Execute(WrapInContext(argumentsDto, TestAuthorityName, TestUserName));

            Assert.NotNull(resultsDto);
        }
        
    }
}
