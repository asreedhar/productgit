namespace FirstLookServicesDomainModel_Test.BookMapping
{
    internal static class VolvoS70TestData
    {
        public const string VIN_PATTERN = "YV1LW524W";
        public const int YEAR = 1998;
        public const int COUNTRY = 1;

        public const string KBB_BOOK = "KBB";
        public const int KBB_VEHICLE_ID = 7158;
        public const string KBB_VIN_PATTERN = "YV____2%";
        public const string KBB_TRIM = "V70 R AWD Wagon 4D";
        public const int KBB_FIRST_STYLE = 105916;
        public const int KBB_FIRST_COUNTRY = 1;

        public const int KBB_STYLE_W_MULTIPLE_TRIMS = 105913;
        public const int KBB_MISSING_STYLE = 105912;
        public const int KBB_MISSING_COUNTRY = 1;
    }
}