using FirstLook.FirstLookServices.DomainModel.BookMapping.Internal;

namespace FirstLookServicesDomainModel_Test.BookMapping
{
    internal static class CommonTestData
    {
        public const string VIN = "1GNDU23Ea2aaaaaaa";
        public const int STYLE = 1;
        public const Countries COUNTRY_CODE = Countries.Usa;
        public const string LONG_VIN = "1GNDU23Ea2aaaaaaa1";
        public const string VIN_PATTERN = "1GNDU23E2";
        public const int YEAR = 2002;

        public const string KBB_BOOK = "KBB";
        public const int KBB_VEHICLE_ID = 3684;
        public const string KBB_TRIM = "Venture Passenger Miniva n 4D";
        public const string KBB_VIN_PATTERN = "1GN_U%";

        public const string NADA_BOOK = "NADA";
        public const int NADA_VEHICLE_ID = 15709;
        public const string NADA_TRIM = "Venture-V6";
        public const int NADA_UID = 1121984;
        public const int NADA_VIC = 2120026841;
        public const string NADA_VIN_PATTERN = "1GNDU23E02";

        public const int GALVES_PHGUID = 301198;
        public const string GALVES_VIN_PATTERN = "1GNDU23E-2";
        public const string GALVES_TRIM = "VENTURE 4 DOOR WAGON 3.4L V6";

        public const string CANADIAN_VIN = "JTDKT4K37B5341853";

        public const string F150_VIN_PATTERN = "1FTMF1CM9D"; // a 2013 XL
        public const int F150_STYLE = 354135; // a 2013

    }
}