using System;
using System.Linq;
using FirstLook.Common.Core.Extensions;
using FirstLook.FirstLookServices.DomainModel.BookMapping.Internal;
using NUnit.Framework;

namespace FirstLookServicesDomainModel_Test.BookMapping
{
    [TestFixture]
    public class BlackBookFacaadeTests
    {
        [Test]
        public void BlackBookGetUvcFromStyle()
        {
            var repo = new BlackBookFacade();
            var uvcs = repo.UvcFromStyle(107099).ToList();
            Assert.AreEqual(1, uvcs.Count());
            Assert.AreEqual("1999640042", uvcs.First());

            Console.WriteLine( uvcs.First().ToJson());
        }

        [Test]
        public void BlackBookDataFromStyleAndCountry_Style_310046()
        {
            var facade = new BlackBookFacade();
            var data = facade.BlackBookDataFromStyleAndCountry(310046, Countries.Usa);

            foreach(var record in data)
            {
                Console.WriteLine(record.ToJson());
            }
        }

        [Test]
        public void BlackBookGetUvcFromVin()
        {
            var bb = new BlackBookFacade();
            var uvcs = bb.UvcFromVin("JTMDF4DV3AD456713").ToList();

            Console.WriteLine(uvcs.ToDelimitedString(","));

            Assert.IsNotNull(uvcs);
            Assert.AreEqual(1, uvcs.Count());
            Assert.AreEqual("2010900327", uvcs.First());
        }

        [Test]
        public void BlackBookFacadeFromF150Vin()
        {
            var bb = new BlackBookFacade();
            var data = bb.UvcFromVin("1FTFW1ET7DFA22663");
            Assert.IsNotNull(data);
        }

    }
}