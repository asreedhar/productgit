﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.BookMapping;
using NUnit.Framework;

namespace FirstLookServicesDomainModel_Test.BookMapping
{
    /*
        Style – non blackbook
        -          chrome rollup
        -          rollup to everything with same YMM
        -          if a single book trim in the set of trims is a vin pattern match, it should be the default
 
        Style – blackbook
        -          if the selected style maps to a single bb trim, return only it
        -          if the selected style maps to multiple bb trims, return all
        -          if the selected style maps to no bb trims, return none

        Vins
        -          if vin maps to a single book specific trim (vin pattern match), return only it
        -          if vin maps to multiple book specific trims on vin pattern, return all vin pattern matches
        -          if the vin does not map to any book specific trim on vin pattern, return none

    */
    [TestFixture]
    public class ExampleDrivenBookMappingTests_Style_320055
    {
        private IEnumerable<Tuple<int, bool>> _galves;
        private IEnumerable<Tuple<int, bool>> _kbb;
        private IEnumerable<Tuple<int, bool>> _nada;
        private IEnumerable<Tuple<string, bool>> _bb;
        private const int STYLE = 320055;

        [SetUp]
        public void SetUp()
        {
            IRegistry registry = RegistryFactory.GetRegistry();
            registry.Register<FirstLook.VehicleValuationGuide.DomainModel.Module>();

            BookMapper mapper = new BookMapper();
            _galves = mapper.GalvesTrimIds(STYLE);
            _kbb = mapper.KbbTrimIds(STYLE);
            _nada = mapper.NadaTrimIds(STYLE);
            _bb = mapper.BlackBookTrimIds(STYLE);

        }

        [Test]
        public void MapByStyle_320055_Kbb()
        {
            Assert.AreEqual(1, _kbb.Count());
            var match = new Tuple<int, bool>(256091, true);
            Assert.IsTrue(_kbb.Contains(match));
        }

        [Test]
        public void MapByStyle_320055_Nada()
        {
            Assert.AreEqual(4, _nada.Count());
            Assert.IsTrue(_nada.Contains( new Tuple<int, bool>(1164131,true)));
            Assert.IsTrue(_nada.Contains( new Tuple<int, bool>(1164132,false)));
            Assert.IsTrue(_nada.Contains( new Tuple<int, bool>(1164391,false)));
            Assert.IsTrue(_nada.Contains(new Tuple<int, bool>(1164392, false)));
        }

        [Test]
        public void MapByStyle_320055_Galves()
        {
            Assert.AreEqual(2, _galves.Count());
            Assert.IsTrue(_galves.Contains(new Tuple<int, bool>(306321, false)));
            Assert.IsTrue(_galves.Contains(new Tuple<int, bool>(306862, true)));
        }

        [Test]
        public void MapByStyle_32055_BlackBook()
        {
            foreach(var bb in _bb)
            {
                Console.WriteLine("trim: {0}, default {1}", bb.Item1, bb.Item2);
            }

            Assert.AreEqual(2, _bb.Count());
            Assert.IsTrue(_bb.Contains(new Tuple<string, bool>("2010020050", true)));
            Assert.IsTrue(_bb.Contains(new Tuple<string, bool>("2010020065", false)));
        }

    }

    [TestFixture]
    public class ExampleDrivenBookMappingTests_Style_310046
    {
        private const int STYLE = 310046;
        private IEnumerable<Tuple<int, bool>> _galves;
        private IEnumerable<Tuple<int, bool>> _kbb;
        private IEnumerable<Tuple<int, bool>> _nada;
        private IEnumerable<Tuple<string, bool>> _bb;

        [SetUp]
        public void SetUp()
        {
            IRegistry registry = RegistryFactory.GetRegistry();
            registry.Register<FirstLook.VehicleValuationGuide.DomainModel.Module>();

            BookMapper mapper = new BookMapper();
            _galves = mapper.GalvesTrimIds(STYLE);
            _kbb = mapper.KbbTrimIds(STYLE);
            _nada = mapper.NadaTrimIds(STYLE);
            _bb = mapper.BlackBookTrimIds(STYLE);
        }

        [Test]
        public void MapByStyle_310046_Kbb()
        {
            Assert.AreEqual(2, _kbb.Count());
            var match1 = new Tuple<int, bool>(252337, false);
            var match2 = new Tuple<int, bool>(349715, true);

            Assert.IsTrue(_kbb.Contains(match1));
            Assert.IsTrue(_kbb.Contains(match2));
        }

        [Test]
        public void MapByStyle_310046_Nada()
        {
            Assert.AreEqual(4, _nada.Count());
            Assert.IsTrue(_nada.Contains(new Tuple<int, bool>(1163247, false)));
            Assert.IsTrue(_nada.Contains(new Tuple<int, bool>(1163248, false)));
            Assert.IsTrue(_nada.Contains(new Tuple<int, bool>(1163249, false)));
            Assert.IsTrue(_nada.Contains(new Tuple<int, bool>(1163250, true)));
        }

        [Test]
        public void MapByStyle_310046_Galves()
        {
            Assert.AreEqual(2, _galves.Count());

            Assert.IsTrue(_galves.Contains(new Tuple<int, bool>(306335, false)));
            Assert.IsTrue(_galves.Contains(new Tuple<int, bool>(306336, true)));
        }

        [Test]
        public void MapByStyle_310046_BlackBook()
        {            
            Assert.AreEqual(2, _bb.Count());

            Assert.IsTrue(_bb.Contains(new Tuple<string, bool>("2010120108", true)));
            Assert.IsTrue(_bb.Contains(new Tuple<string, bool>("2010120107", false)));
        }

    }


    [TestFixture]
    public class ExampleDrivenBookMappingTests_VIN_JTMDF4DV3AD456713
    {

        private const string VIN = "JTMDF4DV3AD456713";
        private IEnumerable<Tuple<int, bool>> _galves;
        private IEnumerable<Tuple<int, bool>> _kbb;
        private IEnumerable<Tuple<int, bool>> _nada;
        private IEnumerable<Tuple<string, bool>> _bb;

        [SetUp]
        public void SetUp()
        {
            IRegistry registry = RegistryFactory.GetRegistry();
            registry.Register<FirstLook.VehicleValuationGuide.DomainModel.Module>();
            BookMapper mapper = new BookMapper();
            _galves = mapper.GalvesTrimIds(VIN);
            _kbb = mapper.KbbTrimIds(VIN);
            _nada = mapper.NadaTrimIds(VIN);
            _bb = mapper.BlackBookTrimIds(VIN);

        }

        [Test]
        public void MapByVin_Kbb()
        {
            Assert.AreEqual(1, _kbb.Count());
            var match1 = new Tuple<int, bool>(260992, true);

            Assert.IsTrue(_kbb.Contains(match1));
        }

        [Test]
        public void MapByVin_Nada()
        {
            Assert.AreEqual(1, _nada.Count());
            Assert.IsTrue(_nada.Contains(new Tuple<int, bool>(1164921, true)));
        }

        [Test]
        public void MapByVin_Galves()
        {
            Assert.AreEqual(1, _galves.Count());
            Assert.IsTrue(_galves.Contains(new Tuple<int, bool>(306813, true)));
        }

        [Test]
        public void MapByVin_BlackBook()
        {
            Assert.AreEqual(1, _bb.Count());
            Assert.IsTrue(_bb.Contains(new Tuple<string, bool>("2010900327", true)));
        }
    }

    public class ExampleDrivenBookMappingTests_VIN_1GCJK39U44E229585
    {
        private const string VIN = "1GCJK39U44E229585";
        private IEnumerable<Tuple<int, bool>> _galves;
        private IEnumerable<Tuple<int, bool>> _kbb;
        private IEnumerable<Tuple<int, bool>> _nada;
        private IEnumerable<Tuple<string, bool>> _bb;


        [SetUp]
        public void SetUp()
        {
            IRegistry registry = RegistryFactory.GetRegistry();
            registry.Register<FirstLook.VehicleValuationGuide.DomainModel.Module>();

            BookMapper mapper = new BookMapper();
            _galves = mapper.GalvesTrimIds(VIN);
            _kbb = mapper.KbbTrimIds(VIN);
            _nada = mapper.NadaTrimIds(VIN);
            _bb = mapper.BlackBookTrimIds(VIN);

        }

        [Test]
        public void MapByVin_Kbb()
        {
            Assert.AreEqual(4, _kbb.Count());

            var match1 = new Tuple<int, bool>(347436, false);
            var match2 = new Tuple<int, bool>(348543, false);
            var match3 = new Tuple<int, bool>(348776, false);
            var match4 = new Tuple<int, bool>(349378, false);

            Assert.IsTrue(_kbb.Contains(match1));
            Assert.IsTrue(_kbb.Contains(match2));
            Assert.IsTrue(_kbb.Contains(match3));
            Assert.IsTrue(_kbb.Contains(match4));
        }

        [Test]
        public void MapByVin_Nada()
        {
            Assert.AreEqual(3, _nada.Count());
            Assert.IsTrue(_nada.Contains(new Tuple<int, bool>(1141483, false)));
            Assert.IsTrue(_nada.Contains(new Tuple<int, bool>(1141491, false)));
            Assert.IsTrue(_nada.Contains(new Tuple<int, bool>(1141500, false)));
        }

        [Test]
        public void MapByVin_Galves()
        {
            Assert.AreEqual(4, _galves.Count());
            Assert.IsTrue(_galves.Contains(new Tuple<int, bool>(301407, false)));
            Assert.IsTrue(_galves.Contains(new Tuple<int, bool>(301408, false)));
            Assert.IsTrue(_galves.Contains(new Tuple<int, bool>(301409, false)));
            Assert.IsTrue(_galves.Contains(new Tuple<int, bool>(301410, false)));
        }

        [Test]
        public void MapByVin_BlackBook()
        {
            Assert.AreEqual(4, _bb.Count());
            Assert.IsTrue(_bb.Contains(new Tuple<string, bool>("2004160790", false)));
            Assert.IsTrue(_bb.Contains(new Tuple<string, bool>("2004160786", false)));
            Assert.IsTrue(_bb.Contains(new Tuple<string, bool>("2004160864", false)));
            Assert.IsTrue(_bb.Contains(new Tuple<string, bool>("2004160780", false)));
        }
    }

}
