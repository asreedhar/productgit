﻿using System;
using System.Linq;
using FirstLook.FirstLookServices.DomainModel.BookMapping;
using FirstLook.FirstLookServices.DomainModel.BookMapping.Internal;
using NUnit.Framework;
using FirstLook.Common.Core.Extensions;

namespace FirstLookServicesDomainModel_Test.BookMapping
{
    [TestFixture]
    public class BookMapperTests
    {
        [Test]
        public void BookMapperCanadianVin()
        {
            var mapper = new BookMapper();

            var galves = mapper.GalvesTrimIds(CommonTestData.CANADIAN_VIN);
            var kbb = mapper.KbbTrimIds(CommonTestData.CANADIAN_VIN);
            var nada = mapper.NadaTrimIds(CommonTestData.CANADIAN_VIN);
            var bb = mapper.BlackBookTrimIds(CommonTestData.CANADIAN_VIN);

            Assert.IsNotNull(galves);
            Assert.IsNotNull(kbb);
            Assert.IsNotNull(nada);
            Assert.IsNotNull(bb);

            Assert.AreEqual(1, kbb.Count());
            Assert.AreEqual(1, galves.Count());
            Assert.AreEqual(1, nada.Count());
            Assert.AreEqual(1, bb.Count());

            Console.WriteLine("KBB Trims: {0}", kbb.ToDelimitedString(","));
            Console.WriteLine("NADA Trims: {0}", nada.ToDelimitedString(","));
            Console.WriteLine("Galves Trims: {0}", galves.ToDelimitedString(","));
            Console.WriteLine("BB Trims: {0}", bb.ToDelimitedString(","));
        }

        [Test]
        public void BookMapperMapByVinThrowsWhenGivenNullsAndEmptyStrings()
        {
            var mapper = new BookMapper();

            Assert.Throws<NullReferenceException>(() => mapper.GalvesTrimIds(null));
            Assert.Throws<BookMappingException>(() => mapper.GalvesTrimIds(string.Empty));

            Assert.Throws<NullReferenceException>(() => mapper.KbbTrimIds(null));
            Assert.Throws<BookMappingException>(() => mapper.KbbTrimIds(string.Empty));

            Assert.Throws<NullReferenceException>(() => mapper.NadaTrimIds(null));
            Assert.Throws<BookMappingException>(() => mapper.NadaTrimIds(string.Empty));

            Assert.Throws<NullReferenceException>(() => mapper.BlackBookTrimIds(null));
            Assert.Throws<BookMappingException>(() => mapper.BlackBookTrimIds(string.Empty));
        }
    }
}
