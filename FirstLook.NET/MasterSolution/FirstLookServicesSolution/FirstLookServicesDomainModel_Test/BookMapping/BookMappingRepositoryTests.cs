using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.FirstLookServices.DomainModel.BookMapping;
using FirstLook.FirstLookServices.DomainModel.BookMapping.Internal;
using NUnit.Framework;

namespace FirstLookServicesDomainModel_Test.BookMapping
{
    [TestFixture]
    public class BookMappingRepositoryTests
    {
        private BookMappingRepository _repository;

        [SetUp]
        public void Setup()
        {
            _repository = new BookMappingRepository();
        }

        [Test]
        public void GetYearFromVinPattern()
        {
            var years = _repository.ChromeYearFromVinPattern(CommonTestData.VIN_PATTERN).ToList();
            Assert.IsNotNull(years);
            Assert.AreEqual(1, years.Count());
            Assert.AreEqual(CommonTestData.YEAR, years.First());
        }

        [Test]
        public void ValidVinMapsToExpectedVinPattern()
        {
            Assert.AreEqual(CommonTestData.VIN_PATTERN, _repository.CanonicalVinPatternFromVin(CommonTestData.VIN));
        }

        [Test]
        public void EmptyVinRaisesException()
        {
            Assert.Throws<BookMappingException>(() => _repository.CanonicalVinPatternFromVin(string.Empty));
        }

        [Test]
        public void NullVinRaisesException()
        {
            Assert.Throws<BookMappingException>(() => _repository.CanonicalVinPatternFromVin(null));
        }

        [Test]
        public void LongVinRaisesException()
        {
            Assert.Throws<BookMappingException>(() => _repository.CanonicalVinPatternFromVin(CommonTestData.LONG_VIN));
        }

/*
        [Test]
        public void GetVinPatternFromStyleAndCountryResolvesToSinglePattern()
        {
            var patterns = _repository.ChromeVinPatternFromStyleAndCountry(CommonTestData.STYLE, CommonTestData.COUNTRY_CODE).ToList();

            foreach(var pattern in patterns)
            {
                Console.WriteLine("Pattern {0},{1},{2}", pattern.Item1, pattern.Item2, pattern.Item3);
            }

            Assert.IsNotNull(patterns);
            Assert.AreEqual(8, patterns.Count());
            Assert.IsTrue(patterns.All(p => p.Item2 == 2002));
            Assert.AreEqual("1GNDU23E2", patterns.First(p => p.Item3).Item1);            
//            Assert.AreEqual(CommonTestData.VIN_PATTERN, patterns.First().Item1);
        }

*/
        
    }
}