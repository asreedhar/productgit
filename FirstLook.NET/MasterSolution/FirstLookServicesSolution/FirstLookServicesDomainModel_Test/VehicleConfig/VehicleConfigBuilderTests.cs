﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel;
using FirstLook.FirstLookServices.DomainModel.BookMapping.Internal;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.FirstLookServices.DomainModel.VehicleConfig;
using NUnit.Framework;
using ServiceStack.Text;

namespace FirstLookServicesDomainModel_Test.VehicleConfig
{
    [TestFixture]
    public class VehicleConfigBuilderTests
    {
        private VehicleConfigBuilder _builder;
        private int _dealerId;

        [SetUp]
        public void SetUp()
        {
            IRegistry registry = RegistryFactory.GetRegistry();
            registry.Register<FirstLook.VehicleValuationGuide.DomainModel.Module>();
            registry.Register<ICache, MemoryCache>(ImplementationScope.Shared);

            _dealerId = 101590;

            _builder = new VehicleConfigBuilder(_dealerId);
        }

        #region KBB
        [Test]
        public void KbbBuildByVinThrows()
        {
            Assert.Throws<InvalidInputException>(() => _builder.Build(string.Empty, BookFactory.Instance.Get(BookId.Kbb)));
            Assert.Throws<InvalidInputException>(() => _builder.Build(null, BookFactory.Instance.Get(BookId.Kbb)));

            // too short to be a vin pattern or vin
            Assert.Throws<InvalidInputException>(() => _builder.Build("a", BookFactory.Instance.Get(BookId.Kbb)));
            Assert.Throws<InvalidInputException>(() => _builder.Build("aaaaaaaa", BookFactory.Instance.Get(BookId.Kbb)));

            // too long to be a vin pattern, too short to be a vin
            Assert.Throws<InvalidInputException>(() => _builder.Build("aaaaaaaaaa", BookFactory.Instance.Get(BookId.Kbb)));

            // longer than a vin
            Assert.Throws<InvalidInputException>(() => _builder.Build("aaaaaaaaaaaaaaaaaa", BookFactory.Instance.Get(BookId.Kbb)));
        }

        [Test]
        public void KbbBuildFromBookRecordsVehicleWithComplexRules()
        {
            const int vehicleId = 5932;

            var r = new BookRecord { Book = BookFactory.Instance.Get(BookId.Kbb), TrimId = vehicleId };
            var configs = _builder.BuildFromBookRecords(new List<BookRecord> { r }, BookFactory.Instance.Get(BookId.Kbb)).ToList();

            Assert.IsNotNull( configs );
            Assert.AreEqual(1, configs.Count );

            var config = configs.First();

            Assert.AreEqual(vehicleId, config.TrimId);
            Assert.AreEqual("kbb", config.Book.Desc.ToLower());
        }

        [Test]
        public void BuildFromBookRecords_BlackBook_2010020050()
        {
            var r = new BookRecord { Book = BookFactory.Instance.Get(BookId.BlackBook), TrimId = "2010020050" };
            var configs = _builder.BuildFromBookRecords(new List<BookRecord> { r }, BookFactory.Instance.Get(BookId.BlackBook)).ToList();

            Assert.IsNotNull( configs );
            Assert.AreEqual(1, configs.Count);

            var config = configs.First();

            Console.WriteLine(configs.ToJson());
            foreach(var eq in config.Equipment())
            {
                Console.WriteLine(eq.ToJson());
            }
        }


        [Test]
        public void BuildFromBookRecords_BlackBook_2010160052()
        {
            var r = new BookRecord { Book = BookFactory.Instance.Get(BookId.BlackBook), TrimId = "2010160052" };
            var configs = _builder.BuildFromBookRecords(new List<BookRecord> { r }, BookFactory.Instance.Get(BookId.BlackBook)).ToList();

            Assert.IsNotNull(configs);
            Assert.AreEqual(1, configs.Count);

            var config = configs.First();

            Console.WriteLine(configs.ToJson());
            foreach (var eq in config.Equipment())
            {
                Console.WriteLine(eq.ToJson());
            }
        }

        #endregion

    }
}
