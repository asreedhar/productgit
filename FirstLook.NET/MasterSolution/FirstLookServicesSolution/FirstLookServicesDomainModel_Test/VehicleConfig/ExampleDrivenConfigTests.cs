﻿using System.Linq;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel;
using FirstLook.FirstLookServices.DomainModel.VehicleConfig;
using NUnit.Framework;

namespace FirstLookServicesDomainModel_Test.VehicleConfig
{
    [TestFixture]
    public class ExampleDrivenConfigTests
    {
        [SetUp]
        public void SetUp()
        {
            IRegistry registry = RegistryFactory.GetRegistry();
            registry.Register<FirstLook.VehicleValuationGuide.DomainModel.Module>();
            registry.Register<ICache, MemoryCache>(ImplementationScope.Shared);
        }

        /// <summary>
        /// This style maps to a single KBB vehicle id.
        /// </summary>
        [Test]
        public void Build_ByStyle_320055_Kbb()
        {
            VehicleConfigBuilder b = new VehicleConfigBuilder(1);
            var configs = b.Build(320055, BookFactory.Instance.Get(BookId.Kbb));
            Assert.AreEqual(1, configs.Count());

            var config = configs.First();

            Assert.IsTrue(config.Selected);
            Assert.AreEqual(BookId.Kbb,config.Book.Id);
            Assert.AreEqual(256091, config.TrimId);
        }

        [Test]
        public void Build_ByVin_4T1BD1FK7CU058966_Kbb()
        {
            VehicleConfigBuilder b = new VehicleConfigBuilder(1);
            var configs = b.Build("4T1BD1FK7CU058966", BookFactory.Instance.Get(BookId.Kbb));
            Assert.AreEqual(2, configs.Count());

            var config = configs.First();

            Assert.IsFalse(config.Selected);
            Assert.AreEqual(BookId.Kbb, config.Book.Id);

            // this vin has a single engine, transmission, and drivetrain. they must still render
            // mustpickone rules. each rule will contain a single piece of equipment.
            var mustPickOneRules = config.EquipmentRules.Where(r => r.Rule == "MustPickExactlyOne").ToList();

            Assert.AreEqual(3, mustPickOneRules.Count());
            foreach(var rule in mustPickOneRules)
            {
                Assert.AreEqual(1, rule. Equipment.Length);
            }
            
        }

    }
}
