using System.Linq;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.VehicleConfig.Internal;
using NUnit.Framework;

namespace FirstLookServicesDomainModel_Test.VehicleConfig
{
    [TestFixture]
    public class EquipmentRepositoryTests
    {
        [SetUp]
        public void SetUp()
        {
            IRegistry registry = RegistryFactory.GetRegistry();
            registry.Register<FirstLook.VehicleValuationGuide.DomainModel.Module>();
            registry.Register<ICache, MemoryCache>(ImplementationScope.Shared);
        }

        [Test]
        public void GetKbbEquipmentAndRules()
        {
            var repo = new EquipmentRepository();

            const int vehicleId = 5932;
            var eq = repo.GetKbbEquipment(vehicleId);
            var rules = repo.GetKbbEquipmentRules(vehicleId);

            Assert.IsNotNull(eq);
            Assert.AreEqual(51, eq.Count());

            Assert.IsNotNull(rules);
            Assert.AreEqual(50, rules.Count());
        }
    }
}