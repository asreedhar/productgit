using System;
using System.Collections.Generic;
using System.Linq;
using FirstLook.FirstLookServices.DomainModel.VehicleConfig.Internal;

namespace FirstLookServicesDomainModel_Test.VehicleConfig
{
    internal static class Printer
    {
        internal static void Print(Equipment eq)
        {
            Console.WriteLine("Equipment...");
            Console.WriteLine("\tId: " + eq.Id);
            Console.WriteLine("\tDesc: " + eq.Desc);
            Console.WriteLine("\tDefault: " + eq.Default);
            Console.WriteLine("\tSortOrder: " + eq.SortOrder);
/*
            Console.WriteLine("\tAvailability: " + eq.Availability);
            Console.WriteLine("\tCategoryId: " + eq.CategoryId);
            Console.WriteLine("\tCategory: " + eq.Category);
            Console.WriteLine("\tIsDefaultConfiguration: " + eq.IsDefaultConfiguration);
            Console.WriteLine("\tOptionTypeId: " + eq.OptionTypeId);
*/
        }

        internal static void Print(FirstLook.FirstLookServices.DomainModel.VehicleConfig.Internal.VehicleConfig config)
        {
            Console.WriteLine("VehicleConfig...");
            Console.WriteLine("\tBook: " + config.Book);
            Console.WriteLine("\tTrim: " + config.Trim);
            Console.WriteLine("\tTrimId: " + config.TrimId);
            Console.WriteLine("\tSelected: " + config.Selected);

            Console.WriteLine("\tEquipment Count: " + config.Equipment().Count);
            Console.WriteLine("\tRule Count: " + config.EquipmentRules().Count);

            Console.WriteLine("-----EQUIPMENT-----");
            foreach (var equipment in config.Equipment())
            {
                Print(equipment);
            }

            Console.WriteLine("-----RULES-----");
            foreach (var rule in config.EquipmentRules())
            {
                Print(rule, config.Equipment());
            }
        }

        internal static void Print(EquipmentRule ruleInternal, IEnumerable<Equipment> equipmentList )
        {
            Console.WriteLine("EquipmentRule...");
            Console.WriteLine("\tRule: " + ruleInternal.Rule);
            foreach (var eq in ruleInternal.Equipment)
            {
                // get the equipment.
                var equipment = equipmentList.FirstOrDefault(e => e.Id == eq);
//                Console.WriteLine("\tEquipment: " + eq);                
                Print(equipment);

            }
        }

    }
}