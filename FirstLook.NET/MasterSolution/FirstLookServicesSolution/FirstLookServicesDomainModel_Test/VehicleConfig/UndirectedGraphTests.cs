using System;
using System.Linq;
using FirstLook.FirstLookServices.DomainModel.VehicleConfig.Internal;
using NUnit.Framework;

namespace FirstLookServicesDomainModel_Test.VehicleConfig
{
    [TestFixture]
    public class UndirectedGraphTests
    {
        [Test]
        public void EmptyGraph()
        {
            var g = new UndirectedGraph();
            var v = g.Vertices().ToList();
            var e = g.Edges().ToList();
            var s = g.DisjointSets().ToList();

            Assert.IsEmpty( v );
            Assert.IsEmpty( e );
            Assert.IsEmpty( s );
        }

        [Test]
        public void OnePartition_OneEdge()
        {
            var g = new UndirectedGraph();
            g.AddEdge(1,2);

            var v = g.Vertices().ToList();
            var e = g.Edges().ToList();
            var s = g.DisjointSets();

            Assert.AreEqual(2, v.Count());
            Assert.IsTrue(v.Contains(1));
            Assert.IsTrue(v.Contains(2));

            Assert.AreEqual(1, e.Count());
            Assert.IsTrue(e.Contains(new Tuple<dynamic, dynamic>(1, 2)));

            Assert.AreEqual(1, s.Count);

            var set = s.First();
            Assert.AreEqual( 1, set.Key );
            Assert.AreEqual( 2, set.Value.Count);
            Assert.IsTrue(set.Value.Contains(1));
            Assert.IsTrue(set.Value.Contains(2));
        }

        [Test]
        public void OnePartition_OneEdge_Symmetric()
        {
            var g = new UndirectedGraph();
            g.AddEdge(1, 2);
            g.AddEdge(2, 1);

            var v = g.Vertices().ToList();
            var e = g.Edges().ToList();
            var s = g.DisjointSets();

            Assert.AreEqual(2, v.Count());
            Assert.IsTrue(v.Contains(1));
            Assert.IsTrue(v.Contains(2));

            Assert.AreEqual(1, e.Count());
            Assert.IsTrue(e.Contains(new Tuple<dynamic, dynamic>(1, 2)));

            Assert.AreEqual(1, s.Count);

            var set = s.First();
            Assert.AreEqual(1, set.Key);
            Assert.AreEqual(2, set.Value.Count);
            Assert.IsTrue(set.Value.Contains(1));
            Assert.IsTrue(set.Value.Contains(2));
        }

        [Test]
        public void OnePartitionIgnoreDuplicateEdges()
        {
            var g = new UndirectedGraph();
            g.AddEdge(1, 2);
            g.AddEdge(1, 2);
            g.AddEdge(2, 1);
            g.AddEdge(2, 1);

            var v = g.Vertices().ToList();
            var e = g.Edges().ToList();
            var s = g.DisjointSets();

            Assert.AreEqual(2, v.Count());
            Assert.IsTrue(v.Contains(1));
            Assert.IsTrue(v.Contains(2));

            Assert.AreEqual(1, e.Count());
            Assert.IsTrue(e.Contains(new Tuple<dynamic, dynamic>(1, 2)));

            Assert.AreEqual(1, s.Count);

            var set = s.First();
            Assert.AreEqual(1, set.Key);
            Assert.AreEqual(2, set.Value.Count);
            Assert.IsTrue(set.Value.Contains(1));
            Assert.IsTrue(set.Value.Contains(2));
        }

        [Test]
        public void OnePartitionTwoEdges()
        {
            var g = new UndirectedGraph();
            g.AddEdge(2, 3);
            g.AddEdge(2, 4);

            var v = g.Vertices().ToList();
            var e = g.Edges().ToList();
            var s = g.DisjointSets();

            Assert.AreEqual(3, v.Count());
            Assert.IsTrue(v.Contains(2));
            Assert.IsTrue(v.Contains(3));
            Assert.IsTrue(v.Contains(4));

            Assert.AreEqual(2, e.Count());
            Assert.IsTrue(e.Contains(new Tuple<dynamic, dynamic>(2, 3)));
            Assert.IsTrue(e.Contains(new Tuple<dynamic, dynamic>(2, 4)));

            Assert.AreEqual(1, s.Count);
            Assert.IsTrue( s.ContainsKey(2) );
            Assert.IsTrue(s[2].Contains(2));
            Assert.IsTrue(s[2].Contains(3));
            Assert.IsTrue(s[2].Contains(4));
        }

        [Test]
        public void OneParitionMultipleEdges()
        {
            var g = new UndirectedGraph();
            g.AddEdge(1, 2);
            g.AddEdge(2, 3);
            g.AddEdge(3, 4);

            var v = g.Vertices().ToList();
            var e = g.Edges().ToList();

            Assert.AreEqual(4, v.Count());
            Assert.IsTrue(v.Contains(1));
            Assert.IsTrue(v.Contains(2));
            Assert.IsTrue(v.Contains(3));
            Assert.IsTrue(v.Contains(4));

            Assert.AreEqual(3, e.Count());
            Assert.IsTrue(e.Contains(new Tuple<dynamic,dynamic>(1, 2)));
            Assert.IsTrue(e.Contains(new Tuple<dynamic,dynamic>(2, 3)));
            Assert.IsTrue(e.Contains(new Tuple<dynamic,dynamic>(3, 4)));
        }

        [Test]
        public void TwoPartitions()
        {
            var g = new UndirectedGraph();
            g.AddEdge(1, 2);
            g.AddEdge(3, 4);

            var v = g.Vertices().ToList();
            var e = g.Edges().ToList();
            var s = g.DisjointSets();

            Assert.AreEqual(4, v.Count());
            Assert.IsTrue(v.Contains(1));
            Assert.IsTrue(v.Contains(2));
            Assert.IsTrue(v.Contains(3));
            Assert.IsTrue(v.Contains(4));

            Assert.AreEqual(2, e.Count());
            Assert.IsTrue(e.Contains(new Tuple<dynamic,dynamic>(1, 2)));
            Assert.IsTrue(e.Contains(new Tuple<dynamic,dynamic>(3, 4)));

            Assert.AreEqual(2, s.Count);
            Assert.IsTrue( s.ContainsKey(1) );
            Assert.IsTrue( s.ContainsKey(3) );

            Assert.AreEqual(2, s[1].Count);
            Assert.AreEqual(2, s[3].Count);

            Assert.IsTrue(s[1].Contains(1));
            Assert.IsTrue(s[1].Contains(2));
            Assert.IsTrue(s[3].Contains(3));
            Assert.IsTrue(s[3].Contains(4));
        }

        [Test]
        public void OnePartitionThreeEdgesSymmetric()
        {
            var g = new UndirectedGraph();
            g.AddEdge(1, 2);
            g.AddEdge(2, 1);

            g.AddEdge(1, 3);
            g.AddEdge(3, 1);

            g.AddEdge(2, 3);
            g.AddEdge(3, 2);

            var v = g.Vertices().ToList();
            var e = g.Edges().ToList();
            var s = g.DisjointSets();

            Assert.AreEqual(3, v.Count);
            Assert.AreEqual(3, e.Count);
            Assert.AreEqual(1, s.Count);

        }
    }
}