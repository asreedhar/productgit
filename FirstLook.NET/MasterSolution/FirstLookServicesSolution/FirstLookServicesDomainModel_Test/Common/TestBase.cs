﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Commands.Impl;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.IOC;
using FirstLook.Pricing.DomainModel;
using NUnit.Framework;
using Autofac;
using Autofac.Integration.Web;
using FirstLook.Common.Core.Registry;
using Module = FirstLook.FirstLookServices.DomainModel.Module;

namespace FirstLookServicesDomainModel_Test.Common
{
    public class TestBase : IContainerProviderAccessor
    {
        private static IContainerProvider _containerProvider;

        protected const string TestUserName = "vrane";

        protected const string TestAuthorityName = "CAS";

        [TestFixtureSetUp]
        public void FixtureSetup()
        {

            var builder = new ContainerBuilder();

            //builder.RegisterModule(new CoreModule());
            
            //builder.RegisterModule(new OltpModule());
            
            new PricingDomainRegister().Register(builder);

            IContainer container = builder.Build();
            
            Registry.RegisterContainer(container);
            
            _containerProvider = new ContainerProvider(container);

            IRegistry registry = RegistryFactory.GetRegistry();

            registry.Register<Module>();

            registry.Register<FirstLook.Client.DomainModel.Module>();

            registry.Register<FirstLook.Pricing.DomainModel.Commands.Module>();

            registry.Register<FirstLook.VehicleValuationGuide.DomainModel.Module>();

            registry.Register<FirstLook.VehicleHistoryReport.DomainModel.Module>();

            registry.Register<FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Module>();
            
            registry.Register<IDataSessionManager, DataSessionManager>(ImplementationScope.Shared);

            registry.Register<ICache, NoCache>(ImplementationScope.Shared);
        }

        //Used for HTTP modules in Autofac.Integration.Web
        IContainerProvider IContainerProviderAccessor.ContainerProvider
        {
            get { return _containerProvider; }
        }

        protected static IdentityContextDto<T> WrapInContext<T>(T arguments, string authorityName, string userName)
        {
            return new IdentityContextDto<T>
            {
                Arguments = arguments,
                Identity = new IdentityDto
                {
                    AuthorityName = authorityName,
                    Name = userName
                }
            };
        }

        protected static BrokerDto GetBroker(string userName, string authorityName, ClientDto client)
        {
            var arguments = new BrokerForClientArgumentsDto
            {
                AuthorityName = authorityName,
                Client = client
            };

            var command = new BrokerForClientCommand();

            BrokerForClientResultsDto results = command.Execute(WrapInContext(arguments, authorityName, userName));

            return results.Broker;
        }

        protected static ClientDto GetClient(string userName, string authorityName)
        {
            QueryClientArgumentsDto arguments = new QueryClientArgumentsDto
            {
                MaximumRows = 25,
                SortColumns = new List<SortColumnDto>
                                  {
                                      new SortColumnDto
                                          {
                                              Ascending = true,
                                              ColumnName = "Name"
                                          }
                                  },
                StartRowIndex = 0
            };

            QueryClientCommand command = new QueryClientCommand();

            QueryClientResultsDto results = command.Execute(WrapInContext(arguments, authorityName, userName));

            if (results.TotalRowCount > 0)
            {
                return results.Clients[0];
            }

            throw new NotSupportedException("no clients");
        }


        protected class NoCache : ICache
        {
            public object Add(string key, object value, DateTime absoluteExpiration, TimeSpan slidingExpiration)
            {
                return null;
            }

            public object Get(string key)
            {
                return null;
            }

            public object Remove(string key)
            {
                return null;
            }
        }
    }

}
