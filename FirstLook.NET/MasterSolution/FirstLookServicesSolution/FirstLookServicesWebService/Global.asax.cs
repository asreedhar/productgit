﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Web;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.WebService.App_Start;
using FirstLook.Pricing.DomainModel;
using Module = FirstLook.FirstLookServices.DomainModel.Books.Commands.Module;

namespace FirstLook.FirstLookServices.WebService
{
    public class MvcApplication : System.Web.HttpApplication, IContainerProviderAccessor
    {
        private static IContainerProvider _containerProvider;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RouteConfig.RegisterRoutes(RouteTable.Routes);

            var builder = new ContainerBuilder();

            new PricingDomainRegister().Register(builder);

            IContainer container = builder.Build();

            Registry.RegisterContainer(container);

            _containerProvider = new ContainerProvider(container);

            IRegistry registry = RegistryFactory.GetRegistry();

            registry.Register<Module>();

            registry.Register<Client.DomainModel.Module>();

            registry.Register<DomainModel.Module>();

            registry.Register<Pricing.DomainModel.Commands.Module>();

            registry.Register<Client.DomainModel.Clients.Commands.Module>();

            registry.Register<Client.DomainModel.Vehicles.Commands.Module>();

            registry.Register<VehicleValuationGuide.DomainModel.Module>();

            registry.Register<VehicleHistoryReport.DomainModel.Module>();

            registry.Register<IDataSessionManager, DataSessionManager>(ImplementationScope.Shared);

            registry.Register<ICache, NoCache>(ImplementationScope.Shared);

        }

        //Used for HTTP modules in Autofac.Integration.Web
        IContainerProvider IContainerProviderAccessor.ContainerProvider
        {
            get { return _containerProvider; }
        }

        //TODO:  How should this be handled?
        protected class NoCache : ICache
        {
            public object Add(string key, object value, DateTime absoluteExpiration, TimeSpan slidingExpiration)
            {
                return null;
            }

            public object Get(string key)
            {
                return null;
            }

            public object Remove(string key)
            {
                return null;
            }
        }
    }
}