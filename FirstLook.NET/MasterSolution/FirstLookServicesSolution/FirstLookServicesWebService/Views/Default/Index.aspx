﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FirstLook Services Web Service</title>

    <script type="text/javascript" charset="utf-8" src="<%=ResolveUrl("~/Scripts/jquery-1.8.0.min.js") %>"></script>
    <script type="text/javascript" charset="utf-8" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.23.custom.min.js") %>"></script>

</head>
<body>
    <div>
        Vin:      <input type="text" name="vin" id="vin"/>
        Mileage:  <input type="text" name="mileage" id="mileage"/>
    </div>
    <br />
    <div>
        <strong>Dealer</strong>
        <br />
        Dealers:  <select name="dealers" id="dealers"></select>
        Dealers Books:  <select name="dealersbooks" id="dealersbooks"></select>
    </div>
    <br />
    <div>
        <strong>Reference APIs</strong>
        <ul>
            <li> <a href="#results" onclick="dealersGET();"> GET /dealers </a> </li>
            <li> <a href="#results" onclick="dealersBooksGET();"> GET /books/dealers/{dealer} </a> </li>
            <li> <a href="#results" onclick="searchtypesGET();"> GET /listings/searchTypes </a> </li>
            <li> <a href="#results" onclick="distancesGET();"> GET /listings/distances </a> </li>
            <li> <a href="#results" onclick="auctionreferenceGET();"> GET /auctions/naaa/reference </a> </li>
        </ul>
    </div>
    <div>
        <strong>NAAA Auction</strong>
        <br />
            Areas    <select name="areas" id="areas"></select>
            Periods  <select name="periods" id="periods"></select>
         <ul>
            <li> <a href="#results" onclick="naaaAuctionGET();">GET /auctions/naaa/vins/{vin}</a></li>
         </ul>   
    </div>
    <div>
        <strong>Reports</strong>
         <ul>
            <li> <a href="#results" onclick="autocheckreportGET();"> GET /reports/autocheck/{dealer}/{vin}</a></li>
            <li> <a href="#results" onclick="carfaxreportGET();"> GET /reports/carfax/{dealer}/{vin}</a></li>
            <li> <a href="#results" onclick="autocheckreportPOST();"> POST /reports/autocheck/{dealer}/{vin}</a></li>
            <li> <a href="#results" onclick="carfaxreportPOST();"> POST /reports/carfax/{dealer}/{vin}</a></li>
         </ul>   
    </div>
    <div>
        <strong>Listings</strong>
        <br />
            SearchTypes <select name="searchtypes" id="searchtypes"></select>
            Distances   <select name="distances" id="distances"></select>
        <ul>
            <li> <a href="#results" onclick="listingsGET();"> GET /listings/dealers/{dealer}/vins/{vin}</a></li>
        </ul>
    </div>
    <div>
        <strong>Vehicles</strong>
        <ul>
            <li> <a href="#results" onclick="hierarchyGET();"> GET /vehicles/hierarchy</a></li>
        </ul>
    </div>
    <div id="results">
        <br />
        <a name="results"> <strong>Results</strong></a>
        <br />
        <textarea name="comments" cols="100" rows="10" id="resultBox">
        </textarea>
    </div>


    <script type="text/javascript" charset="utf-8">

        var rootDir = "<%= @Url.Content("~/") %>";

        function dealersGET() {
            
            var url = rootDir + "dealers";

            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    $("#resultBox").val(JSON.stringify(data));

                    $('#dealers option').each(function (index, option) {
                        $(option).remove();
                    });

                    var dealers = $('#dealers');

                    $.each(data, function (index, value) {
                        dealers.append(
                            $('<option></option>').val(value.Id).html(value.Description)
                        );
                    });
                    
                },
                complete: function () {
                }
            });
        }

        function dealersBooksGET() {
            
           var url = rootDir + "books/dealers/";

           var dealerid = $("#dealers option:selected").val();

           if (!dealerid) {
               alert('select dealer');
               return;
           }

           url = url + dealerid;

            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    $("#resultBox").val(JSON.stringify(data));

                    $('#dealersbooks option').each(function (index, option) {
                            $(option).remove();
                        });

                        var dealersbooks = $('#dealersbooks');

                        $.each(data.books, function (index, value) {
                            dealersbooks.append(
                                $('<option></option>').val(value).html(value)
                            );
                        });


                },
                complete: function () {
                }
            });
        }

        function searchtypesGET() {

            var url = rootDir + "listings/searchTypes";

            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    $("#resultBox").val(JSON.stringify(data));

                    $('#searchtypes option').each(function (index, option) {
                        $(option).remove();
                    });

                    var searchtypes = $('#searchtypes');

                    $.each(data, function (index, value) {
                        searchtypes.append(
                            $('<option></option>').val(value).html(value)
                        );
                    });

                },
                complete: function () {
                }
            });
        }

        function auctionreferenceGET() {

            var url = rootDir + "auctions/naaa/reference";
        
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    $("#resultBox").val(JSON.stringify(data));

                    $('#areas option').each(function (index, option) {
                        $(option).remove();
                    });

                    $('#periods option').each(function (index, option) {
                        $(option).remove();
                    });

                    var areas = $('#areas');

                    var periods = $('#periods');

                    $.each(data.areas, function (key, value) {
                        areas.append(
                            $('<option></option>').val(value.Id).html(value.Name)
                        );
                    });

                    $.each(data.periods, function (key, value) {
                        periods.append(
                            $('<option></option>').val(value.Id).html(value.Name)
                        );
                    });

                },
                complete: function () {
                }
            });
        }

        function distancesGET() {

            var url = rootDir + "listings/distances";
            
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    $("#resultBox").val(JSON.stringify(data));

                    $('#distances option').each(function (index, option) {
                        $(option).remove();
                    });

                    var distances = $('#distances');

                    $.each(data, function (index, value) {
                        distances.append(
                            $('<option></option>').val(value).html(value)
                        );
                    });

                },
                complete: function () {
                }
            });
        }

        function naaaAuctionGET() {

            var vin = $("#vin").val();

            var mileage = $("#mileage").val();

            var area = $("#areas option:selected").val();

            var period = $("#periods option:selected").val();

            //This needs reworking
            var queryString = '?';

            if (area) {
                queryString = queryString + "area=" + area;
            }

            if (period) {
                queryString = queryString + "&period=" + period;
            }

            if (mileage) {
                queryString = queryString + "&mileage=" + mileage;
            }

            if (vin == '') {
                alert('specify vin');
                return;
            }

            var url = rootDir + "auctions/naaa/vins/" + vin + queryString;
            
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                  $("#resultBox").val(JSON.stringify(data));
                },
                complete: function () {
                }
            });
        }

       function autocheckreportPOST() {

           var vin = $("#vin").val();

           var dealerid = $("#dealers option:selected").val();

           if (vin == '') {
               alert('specify vin');
               return;
           }

           if (!dealerid) {
               alert('select dealer');
               return;
           }

           var url = rootDir + "reports/autocheck/" + dealerid + "/" + vin;
           
            $.ajax({
                type: 'POST',
                url: url,
                success: function (data) {
                  $("#resultBox").val(JSON.stringify(data));
                },
                complete: function () {
                }
            });
        }

        function carfaxreportPOST() {

            var vin = $("#vin").val();

            var dealerid = $("#dealers option:selected").val();

            if (vin == '') {
                alert('specify vin');
                return;
            }

            if (!dealerid) {
                alert('select dealer');
                return;
            }

            var url = rootDir + "reports/carfax/" + dealerid + "/" + vin;
            
            $.ajax({
                type: 'POST',
                url: url,
                success: function (data) {
                  $("#resultBox").val(JSON.stringify(data));
                },
                complete: function () {
                }
            });
        }

        function autocheckreportGET() {

            var vin = $("#vin").val();

            var dealerid = $("#dealers option:selected").val();

            if (vin == '') {
                alert('specify vin');
                return;
            }

            if (!dealerid) {
                alert('select dealer');
                return;
            }

            var url = rootDir + "reports/autocheck/" + dealerid +"/" + vin;

            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    $("#resultBox").val(JSON.stringify(data));
                },
                complete: function () {
                }
            });
        }

        function carfaxreportGET() {

            var vin = $("#vin").val();

            var dealerid = $("#dealers option:selected").val();

            if (vin == '') {
                alert('specify vin');
                return;
            }

            if (!dealerid) {
                alert('select dealer');
                return;
            }

            var url = rootDir + "reports/carfax/" + dealerid + "/" + vin;

            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    $("#resultBox").val(JSON.stringify(data));
                },
                complete: function () {
                }
            });
        }

        function listingsGET() {

            var vin = $("#vin").val();

            var dealerid = $("#dealers option:selected").val();

            if (vin == '') {
                alert('specify vin');
                return;
            }

            if (!dealerid) {
                alert('select dealer');
                return;
            }

            var url = rootDir + "listings/dealers/" + dealerid + "/vins/" + vin;

            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    $("#resultBox").val(JSON.stringify(data));
                },
                complete: function (data) {
                    $("#resultBox").val(JSON.stringify(data));
                },
                error: function (data) {
                    $("#resultBox").val(JSON.stringify(data));
                }
            });
        }

        function hierarchyGET() {

            var url = rootDir + "vehicles/hierarchy";

            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    $("#resultBox").val(JSON.stringify(data));
                },
                complete: function (data) {
                    $("#resultBox").val(JSON.stringify(data));
                },
                error: function (data) {
                    $("#resultBox").val(JSON.stringify(data));
                }
            });
        }
    </script>   

</body>
</html>
