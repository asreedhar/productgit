﻿using System.Web.Mvc;

namespace FirstLook.FirstLookServices.WebService.Areas.Listings
{
    public class ListingsAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Listings";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "Listings",
                url: "listings/dealers/{dealer}/vins/{vin}",
                defaults: new {controller = "Listings", action = "Index"}
                );

            context.MapRoute(
               name: "ListingsReference",
               url: "listings/{reference}",
               defaults: new { controller = "Reference", action = "Index" }
            );

        }
    }
}
