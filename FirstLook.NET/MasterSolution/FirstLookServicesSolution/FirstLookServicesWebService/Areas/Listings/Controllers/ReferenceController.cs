﻿using System.Web.Mvc;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Listings.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Listings.Commands;

namespace FirstLook.FirstLookServices.WebService.Areas.Listings.Controllers
{
    public class ReferenceController : Controller
    {
        public ActionResult Index(string reference)
        {
            switch (reference)
            {
                case "searchTypes":
                    {
                        return SearchTypes();
                    }
                case "distances":
                    {
                        return Distances();
                    }
                default:
                    {
                        return null;
                    }
            }

        }

        public ActionResult SearchTypes()
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            var command = factory.CreateFetchMarketListingsReferenceCommand();

            var argumentsDto = new FetchMarketListingsReferenceArgumentsDto();

            FetchMarketListingsReferenceResultsDto results = command.Execute(argumentsDto);

            return Json(results.SearchTypes, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Distances()
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            var command = factory.CreateFetchMarketListingsReferenceCommand();

            var argumentsDto = new FetchMarketListingsReferenceArgumentsDto();

            FetchMarketListingsReferenceResultsDto results = command.Execute(argumentsDto);

            return Json(results.Distances, JsonRequestBehavior.AllowGet);
        }
    }
}