﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Listings.Commands;
using FirstLook.FirstLookServices.DomainModel.Listings.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Utility;
using FirstLook.FirstLookServices.WebService.Areas.Listings.Models;
using FirstLook.Pricing.DomainModel.Commands.TransferObjects;
using MarketListingDto = FirstLook.FirstLookServices.DomainModel.Listings.Commands.TransferObjects.MarketListingDto;

namespace FirstLook.FirstLookServices.WebService.Areas.Listings.Controllers
{
    public class ListingsController : Controller
    {
        //TODO: CONCERN OVER USE OF FirstLook.Reports.App.ReportDefinitionLibrary.Xml.Serialization.ReportMetadata.Path AND
        //TODO:     FirstLook.Reports.App.ReportDefinitionLibrary.Xml.XmlReportDefinitionFactory.Report.Directory
        //TODO:     ... Would this force the service to have to run on the same box as Pricing? ... or are we copying a bunch of files?

        public ActionResult Index(int? dealer, string vin)
        {
            if (dealer == null || vin == null)
            {
                return new HttpStatusCodeResult(400);
            }

            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            var command = factory.CreateFetchMarketListingsCommand();

            var argumentsDto = new FetchMarketListingsArgumentsDto
                                   {
                                       DealerId = dealer.Value,
                                       Vin = vin,
                                       Distance = 100,           //TODO: Change
                                       MileageMax = 10000,       //TODO: Change
                                       MileageMin = 0,           //TODO: Change
                                       SearchType = "Precision"  //TODO: Change
                                   };

            FetchMarketListingsResultsDto fetchMarketListingsResultsDto = command.Execute(Helper.WrapInContext(argumentsDto, HttpContext.User.Identity.Name));

            List<MarketListingDto> mListingDtoList = fetchMarketListingsResultsDto.MarketListings;

            List<MarketListing> marketListings = mListingDtoList.Select(marketListingDto => new MarketListing()
            {
                Age = marketListingDto.Age, 
                Desc = marketListingDto.Desc, 
                Color = marketListingDto.Color,
                Mileage = marketListingDto.VehicleMileage, 
                Price = marketListingDto.ListPrice, 
                PercentOfMarketAverage = marketListingDto.PercentMarketAverage, 
                Distance = marketListingDto.Distance, 
                Certified = marketListingDto.IsCertified, 
                Seller = marketListingDto.Seller
            }).ToList();

            var respSearchSumDesc = fetchMarketListingsResultsDto.SearchResult.SearchSummaryInfo.BodyType + " | " +
                                    fetchMarketListingsResultsDto.SearchResult.SearchSummaryInfo.Trim + " | " +
                                    fetchMarketListingsResultsDto.SearchResult.SearchSummaryInfo.Doors + " | " +
                                    fetchMarketListingsResultsDto.SearchResult.SearchSummaryInfo.Engine + "  |" +
                                    fetchMarketListingsResultsDto.SearchResult.SearchSummaryInfo.DriveTrain + " | " +
                                    fetchMarketListingsResultsDto.SearchResult.SearchSummaryInfo.FuelType + " | " +
                                    fetchMarketListingsResultsDto.SearchResult.SearchSummaryInfo.Transmission + " | ";


            var searchResults = new SearchResults()
            {
                MarketAverage = fetchMarketListingsResultsDto.SearchResult.SearchDetail.MarketPricing.MarketPrice.Average,
                MarketMin = fetchMarketListingsResultsDto.SearchResult.SearchDetail.MarketPricing.MarketPrice.Minimum,
                MarketMax = fetchMarketListingsResultsDto.SearchResult.SearchDetail.MarketPricing.MarketPrice.Maximum,
                DaysSupply = fetchMarketListingsResultsDto.SearchResult.SearchDetail.MarketPricing.DaySupply,
                SearchType = Enum.GetName(typeof (SearchTypeDto), fetchMarketListingsResultsDto.SearchResult.SearchDetail.SearchType),
                Distance = fetchMarketListingsResultsDto.SearchResult.SearchDetail.MarketPricing.Distance,
                MileageMin = fetchMarketListingsResultsDto.SearchResult.SearchDetail.MarketPricing.Mileage.Minimum,
                MileageMax = fetchMarketListingsResultsDto.SearchResult.SearchDetail.MarketPricing.Mileage.Maximum,
                MileageAvg = fetchMarketListingsResultsDto.SearchResult.SearchDetail.MarketPricing.Mileage.Average,
                SearchSummary = respSearchSumDesc,
                Listings = marketListings
            };

            return Json(searchResults, JsonRequestBehavior.AllowGet);
        }
    }
}