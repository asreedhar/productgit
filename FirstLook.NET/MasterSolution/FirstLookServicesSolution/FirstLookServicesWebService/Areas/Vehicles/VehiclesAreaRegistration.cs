﻿using System.Web.Mvc;

namespace FirstLook.FirstLookServices.WebService.Areas.Vehicles
{
    public class VehiclesAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Vehicles";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Vehicles_Hierarchy",
                "vehicles/hierarchy",
                new { controller = "Vehicles", action = "Hierarchy" }
            );
        }
    }
}
