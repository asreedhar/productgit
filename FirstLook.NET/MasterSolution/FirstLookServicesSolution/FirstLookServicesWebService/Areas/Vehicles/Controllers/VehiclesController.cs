﻿using System.Web.Mvc;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Books.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Commands;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.WebService.Areas.Vehicles.Controllers
{
    public class VehiclesController : Controller
    {
        //
        // GET: /vehicles/hierarchy

        public ActionResult Hierarchy()
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            var command = factory.CreateFetchVehicleHierarchyCommand();

            var argumentsDto = new FetchVehicleHierarchyArgumentsDto
            {
            };

            FetchVehicleHierarchyResultsDto listings = command.Execute(argumentsDto);

            var colNames = new string[] {"year", "make", "model", "style", "styleId"};

            return Json(new
            {
                columns = colNames,
                data = listings
            }, JsonRequestBehavior.AllowGet);
        }
    }
}