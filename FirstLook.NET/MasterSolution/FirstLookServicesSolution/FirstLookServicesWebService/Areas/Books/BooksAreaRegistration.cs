﻿using System.Web.Mvc;

namespace FirstLook.FirstLookServices.WebService.Areas.Books
{
    public class BooksAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Books";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "Books",
                url: "books/dealers/{id}",
                defaults: new { controller = "Books", action = "Index" }
                );
        }
    }
}
