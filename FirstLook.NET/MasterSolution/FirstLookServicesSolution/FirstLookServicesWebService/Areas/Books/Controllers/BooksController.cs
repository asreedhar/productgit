﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Books.Commands;
using FirstLook.FirstLookServices.DomainModel.Books.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.WebService.Areas.Books.Models;
using FirstLook.FirstLookServices.WebService.Utility;

namespace FirstLook.FirstLookServices.WebService.Areas.Books.Controllers
{
    public class BooksController : Controller
    {
        //
        // GET: /Books/

        public ActionResult Index(string id)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            var command = factory.CreateFetchDealerBooksCommand();

            var argumentsDto = new FetchDealerBooksArgumentsDto
            {
                DealerId = int.Parse(id)
            };

            FetchDealerBooksResultsDto results = command.Execute(Helper.WrapInContext(argumentsDto, HttpContext.User.Identity.Name));

            List<string> books = results.Books.Select(book => 
                book.Description.ToLower()
            ).ToList();

            return Json(new
            {
                books = books
            }, JsonRequestBehavior.AllowGet);
        }
    }
}
