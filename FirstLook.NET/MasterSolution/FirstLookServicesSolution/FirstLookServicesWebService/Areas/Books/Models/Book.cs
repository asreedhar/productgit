﻿using System.Collections.Generic;
//using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebService.Areas.Books.Models
{
    [DataContract]
    public class Book
    {
        //private string _name;

        [DataMember(Name =  "book")]
        public string BookName { get; internal set; }

/*
        [JsonProperty(PropertyName = "desc")]
        public string Description { get; internal set; }
*/
    }

    [DataContract]
    public  class BookContainer
    {
/*
        [JsonProperty(PropertyName = "books")]
        public IList<Book> Books { get; internal set; }
*/

        [DataMember(Name = "books")]
        public IList<string> Books { get; internal set; }
    }
}