﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.WebService.Areas.Auctions.Models.Naaa;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Naaa.Envelopes;

namespace FirstLook.FirstLookServices.WebService.Areas.Auctions.Controllers
{
    public class ReferenceController : Controller
    {
        public ActionResult Index(string id)
        {
            switch (id)
            {
                case "naaa":
                    {
                        return NaaaIndex();
                    }
                default:
                    {
                        return null;
                    }
            }

        }

        public ActionResult NaaaIndex()
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            var command = factory.CreateFetchNaaaReferenceCommand();

            var argumentsDto = new FetchNaaaReferenceArgumentsDto();

            FetchNaaaReferenceResultsDto results = command.Execute(argumentsDto);

            IList<Area> areas = results.Areas.Select(area => new Area { Id = area.Id, Name = area.Name }).ToList();

            IList<Period> periods = results.Periods.Select(period => new Period { Id = period.Id, Name = period.Name }).ToList();

            return Json(new { periods = periods, areas = areas }, JsonRequestBehavior.AllowGet);

        }
    }
}