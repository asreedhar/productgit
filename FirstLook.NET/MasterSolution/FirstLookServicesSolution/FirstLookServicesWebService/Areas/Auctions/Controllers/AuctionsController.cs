﻿using System.Collections.Generic;
using System.Web.Mvc;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Naaa.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Utility;

namespace FirstLook.FirstLookServices.WebService.Areas.Auctions.Controllers
{
    public class AuctionsController : Controller
    {        
        public ActionResult Index(string id, string vin, int? area, int? period, int? mileage)
        {
            switch (id)
            {
                case "naaa":
                    {
                        return NaaaIndex(vin, area, period, mileage);
                    }
                default:
                    {
                        return null;
                    }
            }

        }

        public ActionResult NaaaIndex(string vin, int? area, int? period, int? mileage)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            var command = factory.CreateFetchNaaaTransactionSummaryCommand();

            var argumentsDto = new FetchNaaaTransactionSummaryArgumentsDto()
            {
                Vin = vin,
                Area = (area.HasValue? (int)area : 1),
                Period = (period.HasValue ? (int)period : 11)
            };

            FetchNaaaTransactionSummaryResultsDto results = command.Execute(Helper.WrapInContext(argumentsDto, HttpContext.User.Identity.Name));

            //List<string> summary = null;// results;

            return Json(new
                            {
                                salePrices = new
                                                 {
                                                     min = results.NaaaTransactionSummary.MinSalePrice,
                                                     avg = results.NaaaTransactionSummary.AvgSalePrice,
                                                     max = results.NaaaTransactionSummary.MaxSalePrice
                                                 }
                            } , JsonRequestBehavior.AllowGet);
        }
        
    }
    
}