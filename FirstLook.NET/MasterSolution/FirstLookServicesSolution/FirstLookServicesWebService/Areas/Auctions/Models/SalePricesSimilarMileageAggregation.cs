﻿namespace FirstLook.FirstLookServices.WebService.Areas.Auctions.Models
{
    public class SalePricesSimilarMileageAggregation
    {
        public decimal Min { get; internal set; }

        public decimal Average { get; internal set; }

        public decimal Max { get; internal set; }

        public int SampleSize { get; internal set; }
    }
}