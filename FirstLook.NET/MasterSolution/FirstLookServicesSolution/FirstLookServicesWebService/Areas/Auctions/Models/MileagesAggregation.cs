﻿namespace FirstLook.FirstLookServices.WebService.Areas.Auctions.Models
{
    public class MileagesAggregation
    {
        public int Min { get; internal set; }

        public int Average { get; internal set; }

        public int Max { get; internal set; }

        public int SampleSize { get; internal set; }
    }
}