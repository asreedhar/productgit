﻿namespace FirstLook.FirstLookServices.WebService.Areas.Auctions.Models.Naaa
{
    public class Area
    {
        public int Id { get; internal set; }

        public string Name { get; internal set; }
    }
}