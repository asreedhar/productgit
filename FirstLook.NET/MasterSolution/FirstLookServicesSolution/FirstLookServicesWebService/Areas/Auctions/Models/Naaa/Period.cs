﻿namespace FirstLook.FirstLookServices.WebService.Areas.Auctions.Models.Naaa
{
    public class Period
    {
        public int Id { get; internal set; }

        public string Name { get; internal set; }
    }
}