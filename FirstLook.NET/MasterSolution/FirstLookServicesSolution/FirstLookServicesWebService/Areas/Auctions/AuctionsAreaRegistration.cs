﻿using System.Web.Mvc;

namespace FirstLook.FirstLookServices.WebService.Areas.Auctions
{
    public class AuctionsAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Auctions";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "Auctions",
                url: "auctions/{id}/vins/{vin}",
                defaults: new {controller = "Auctions", action = "Index"}
                );

            context.MapRoute(
                name: "AuctionsReference",
                url: "auctions/{id}/reference",
                defaults: new {controller = "Reference", action = "Index"}
                );
        }
    }
}
