﻿using System.Web.Mvc;

namespace FirstLook.FirstLookServices.WebService.Areas.VehicleConfig
{
    public class VehicleConfigAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "VehicleConfig";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "VehicleConfig_default",
                "VehicleConfig/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
