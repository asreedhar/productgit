﻿using System.Web.Mvc;

namespace FirstLook.FirstLookServices.WebService.Areas.Valuations
{
    public class ValuationsAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Valuations";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Valuations_default",
                "Valuations/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
