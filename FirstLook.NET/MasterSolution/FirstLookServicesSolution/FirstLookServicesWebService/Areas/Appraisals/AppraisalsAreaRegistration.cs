﻿using System.Web.Mvc;

namespace FirstLook.FirstLookServices.WebService.Areas.Appraisals
{
    public class AppraisalsAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Appraisals";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Appraisals_default",
                "Appraisals/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
