﻿using System.Web.Mvc;

namespace FirstLook.FirstLookServices.WebService.Areas.Reports
{
    public class ReportsAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Reports";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "Reports",
                url: "reports/{id}/{dealer}/{vin}",
                defaults: new {controller = "Reports", action = "Detail"}
                );
        }
    }
}
