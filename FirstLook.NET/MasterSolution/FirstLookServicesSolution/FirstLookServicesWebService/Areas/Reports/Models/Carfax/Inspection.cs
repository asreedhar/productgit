﻿namespace FirstLook.FirstLookServices.WebService.Areas.Reports.Models.Carfax
{
    public class Inspection
    {
        public string Name { get; internal set; }

        public bool Selected { get; internal set; }
    }
}