﻿using System;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.WebService.Areas.Reports.Models.Carfax
{
    public class Report
    {
        public DateTime ExpirationDate { get; internal set; }

        public string UserName { get; internal set; }

        public int OwnerCount { get; internal set; }

        public bool HasProblems { get; internal set; }

        public string ReportType { get; internal set; }

        public string Href { get; internal set; }

        public List<Inspection> Inspections { get; internal set; }
    }
}