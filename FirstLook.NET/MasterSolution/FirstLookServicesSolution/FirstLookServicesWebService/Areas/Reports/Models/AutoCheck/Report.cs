﻿using System;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.WebService.Areas.Reports.Models.AutoCheck
{
    public class Report
    {
        public DateTime ExpirationDate { get; internal set; }

        public string UserName { get; internal set; }

        public int OwnerCount { get; internal set; }

        public int Score { get; internal set; }

        public int CompareScoreRangeLow { get; internal set; }

        public int CompareScoreRangeHigh { get; internal set; }

        public string Href { get; internal set; }

        public List<Inspection> Inspections { get; internal set; }
    }
}