﻿namespace FirstLook.FirstLookServices.WebService.Areas.Reports.Models.AutoCheck
{
    public class Inspection
    {
        public string Name { get; internal set; }

        public bool Selected { get; internal set; }
    }
}