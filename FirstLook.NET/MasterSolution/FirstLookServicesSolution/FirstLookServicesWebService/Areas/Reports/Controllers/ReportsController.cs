﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Reports.Commands.TransferObjects.AutoCheck;
using FirstLook.FirstLookServices.WebService.Areas.Reports.Models.AutoCheck;
using FirstLook.FirstLookServices.WebService.Utility;
using FirstLook.FirstLookServices.DomainModel.Reports.Commands;
using FirstLook.FirstLookServices.DomainModel.Reports.Commands.TransferObjects.Envelopes.AutoCheck;
using FirstLook.FirstLookServices.DomainModel.Reports.Commands.TransferObjects.Envelopes.Carfax;


//TODO:  I think all this functionality acrossed all controllers need to be pushed down into the FirstLookServicesDomainModel.
//TODO:  Do not call cross domain models here... because the seems to open the door to having business logic in the controller which is BAD.
namespace FirstLook.FirstLookServices.WebService.Areas.Reports.Controllers
{
    public class ReportsController : Controller
    {
        private const string AutoCheckReportUrlBase = "AutoCheckReportUrlBase";
        
        public ActionResult Detail(string id, int? dealer, string vin)
        {

            if (dealer == null || vin == null)
            {
                return new HttpStatusCodeResult(400);
            }

            switch (id)
            {
                case "autocheck":
                    {
                        if (Request.RequestType == "POST")
                        {
                            return AutoCheckPurchase(dealer.Value, vin);
                        }

                        return AutoCheckGet(dealer.Value, vin);
                    }
                case "carfax":
                    {
                        if (Request.RequestType == "POST")
                        {
                            return CarfaxPurchase(dealer.Value, vin);
                        }

                        return CarfaxGet(dealer.Value, vin);
                    }
                default:
                    {
                        return null;
                    }
            }

        }

        #region AutoCheck

        public ActionResult AutoCheckGet(int dealer, string vin)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            var fetchAutoCheckReportCommand = factory.CreateFetchAutoCheckReportCommand();

            //reach into autocheck reportquery command (client / vin)
            var argumentsDto = new FetchAutoCheckReportArgumentsDto
                                {
                                    DealerId = dealer,
                                    Vin = vin
                                };

            FetchAutoCheckReportResultsDto resultsDto = fetchAutoCheckReportCommand.Execute(Helper.WrapInContext(argumentsDto, HttpContext.User.Identity.Name));

            if (resultsDto.Report == null)
            {
                //TODO: There doesnt appear to be an autopurchase for autocheck, so just return the 404
                //return 404
                return new HttpStatusCodeResult(404);
            }

            Report report = BuildAutoCheckReport(resultsDto.Report, dealer);

            return Json(report, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AutoCheckPurchase(int dealer, string vin)
        {
            var vhrfactory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            var purchaseAutoCheckReportCommand = vhrfactory.CreatePurchaseAutoCheckReportCommand();

            var argumentsDto = new PurchaseAutoCheckReportArgumentsDto
                                   {
                                       DealerId = dealer, Vin = vin
                                   };

            PurchaseAutoCheckReportResultsDto resultsDto =
                purchaseAutoCheckReportCommand.Execute(Helper.WrapInContext(argumentsDto, HttpContext.User.Identity.Name));


            if (resultsDto.Report == null)
            {
                //return 401 unauthorized
                return new HttpStatusCodeResult(401);
            }

            Report report = BuildAutoCheckReport(resultsDto.Report, dealer);

            return Json(report, JsonRequestBehavior.AllowGet);

        }

        private static Report BuildAutoCheckReport(ReportDto autoCheckReport, int dealer)
        {
            if (autoCheckReport == null)
            {
                return null;
            }

            string autoCheckUrlBase = ConfigurationManager.AppSettings[AutoCheckReportUrlBase];

            IList<Inspection> inspections =
                autoCheckReport.Inspections.Select(autoCheckReportInspectionTo => new Inspection
                                                                                      {
                                                                                          Name = autoCheckReportInspectionTo.Name,
                                                                                          Selected = autoCheckReportInspectionTo.Selected
                                                                                      }).ToList();

            var report = new Report
            {
                CompareScoreRangeHigh = autoCheckReport.CompareScoreRangeHigh,
                CompareScoreRangeLow = autoCheckReport.CompareScoreRangeLow,
                ExpirationDate = autoCheckReport.ExpirationDate,
                OwnerCount = autoCheckReport.OwnerCount,
                Score = autoCheckReport.Score,
                UserName = autoCheckReport.UserName,
                Href = autoCheckUrlBase + "/support/Reports/AutoCheckReport.aspx?DealerId=" + dealer + "&Vin=" + autoCheckReport.Vin,
                Inspections = (List<Inspection>)inspections
            };

            return report;
        }

        #endregion

        #region Carfax

        //TODO: We need a better way to handle bad data on call to services in general ... for now just spitting back an error
        public ActionResult CarfaxGet(int dealer, string vin)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            var fetchCarfaxReportCommand = factory.CreateFetchCarfaxReportCommand();

            //reach into autocheck reportquery command (client / vin)
            var argumentsDto = new FetchCarfaxReportArgumentsDto
                                   {
                DealerId = dealer,
                Vin = vin
            };

            FetchCarfaxReportResultsDto resultsDto = fetchCarfaxReportCommand.Execute(Helper.WrapInContext(argumentsDto, HttpContext.User.Identity.Name));

            if (resultsDto.Report == null)
            {
                //TODO: There doesnt appear to be an autopurchase for autocheck, so just return the 404
                //return 404
                return new HttpStatusCodeResult(404);
            }

            Models.Carfax.Report report = BuildCarFaxReport(resultsDto.Report);

            return Json(report, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CarfaxPurchase(int dealer, string vin)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            var purchaseCarfaxReportCommand = factory.CreatePurchaseCarfaxReportCommand();

            //reach into autocheck reportquery command (client / vin)
            var argumentsDto = new PurchaseCarfaxReportArgumentsDto()
            {
                DealerId = dealer,
                Vin = vin
            };

            PurchaseCarfaxReportResultsDto purchaseCarfaxReportResultsDto =
                purchaseCarfaxReportCommand.Execute(Helper.WrapInContext(argumentsDto, HttpContext.User.Identity.Name));

            Models.Carfax.Report report = BuildCarFaxReport(purchaseCarfaxReportResultsDto.Report);

            return Json(report, JsonRequestBehavior.AllowGet);
        }

        private static Models.Carfax.Report BuildCarFaxReport(DomainModel.Reports.Commands.TransferObjects.Carfax.ReportDto carfaxReport)
        {
            if (carfaxReport == null)
            {
                return null;
            }

            IList<Models.Carfax.Inspection> inspections =
                carfaxReport.Inspections.Select(carfaxReportInspectionTo => new Models.Carfax.Inspection
                                                                                {
                                                                                    Name = carfaxReportInspectionTo.Name,
                                                                                    Selected =
                                                                                        carfaxReportInspectionTo.
                                                                                        Selected
                                                                                }).ToList();


            var report = new Models.Carfax.Report
            {
                ExpirationDate = carfaxReport.ExpirationDate,
                OwnerCount = carfaxReport.OwnerCount,
                HasProblems = carfaxReport.HasProblems,
                UserName = carfaxReport.UserName,
                Href = "http://www.carfaxonline.com/cfm/Display_Dealer_Report.cfm?partner=FLK_0&UID=" + carfaxReport.UserName + "&vin=" + carfaxReport.Vin,
                ReportType = carfaxReport.ReportType,
                Inspections = (List<Models.Carfax.Inspection>)inspections
            };

            return report;
        }

        #endregion
    }
}