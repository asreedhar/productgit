﻿using Newtonsoft.Json;

namespace FirstLook.FirstLookServices.WebService.Areas.Dealers.Models
{
    public class Dealer
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; internal set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; internal set; }
    }
}