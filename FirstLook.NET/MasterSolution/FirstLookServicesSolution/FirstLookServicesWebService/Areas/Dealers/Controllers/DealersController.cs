﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Dealers.Commands;
using FirstLook.FirstLookServices.DomainModel.Dealers.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.WebService.Areas.Dealers.Models;
using FirstLook.FirstLookServices.WebService.Utility;

namespace FirstLook.FirstLookServices.WebService.Areas.Dealers.Controllers
{
    public class DealersController : Controller
    {
        //Get the dealers associated to the signed on user.
        //TODO: Eventually perhaps we can use the Filter created by the MAX Team for CAS authentication ... at the moment everything in this 
        //TODO: ... service is CAS authenticated (Web.config)
        public ActionResult Index()
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            var command = factory.CreateFetchDealersCommand();

            var argumentsDto = new FetchDealersArgumentsDto();


            FetchDealersResultsDto results = command.Execute(Helper.WrapInContext(argumentsDto, HttpContext.User.Identity.Name));

            //Map to dealer
            IList<Dealer> dealers = results.Dealers.Select(dealer => new Dealer
                                                                         {
                                                                             Id = dealer.Id,
                                                                             Description = dealer.Description
                                                                         }).ToList();

            return Json(dealers, JsonRequestBehavior.AllowGet);
        }
    }
}
