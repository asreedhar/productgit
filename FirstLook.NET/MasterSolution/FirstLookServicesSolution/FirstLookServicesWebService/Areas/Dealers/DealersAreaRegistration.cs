﻿using System.Web.Mvc;

namespace FirstLook.FirstLookServices.WebService.Areas.Dealers
{
    public class DealersAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Dealers";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
               name: "Dealers",
               url: "dealers",
               defaults: new { controller = "Dealers", action = "Index" }
               );
        }
    }
}
