﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.WebService.Utility
{
    public class Helper
    {
        public static IdentityContextDto<T> WrapInContext<T>(T arguments, string userName)
        {
            return new IdentityContextDto<T>
            {
                Arguments = arguments,
                Identity = new IdentityDto
                {
                    AuthorityName = "CAS",
                    Name = userName
                }
            };
        }

    }
}