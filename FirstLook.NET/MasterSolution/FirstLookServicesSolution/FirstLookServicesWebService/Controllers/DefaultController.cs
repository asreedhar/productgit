﻿using System.Web.Mvc;

namespace FirstLook.FirstLookServices.WebService.Controllers
{
    public class DefaultController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
