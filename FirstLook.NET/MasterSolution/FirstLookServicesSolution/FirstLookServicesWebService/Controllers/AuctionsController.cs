﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.WebService.Models;
using FirstLook.FirstLookServices.WebService.Models.Auction;
using FirstLook.FirstLookServices.WebService.Models.Auction.Naaa;
using FirstLook.VehicleValuationGuide.DomainModel.Naaa.Commands;
using FirstLook.VehicleValuationGuide.DomainModel.Naaa.Commands.TransferObjects;
using FirstLook.VehicleValuationGuide.DomainModel.Naaa.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.Nada.Commands.TransferObjects.Envelopes;
using FirstLook.VehicleValuationGuide.DomainModel.Utility.Commands.TransferObjects;
using ValuationArgumentsDto = FirstLook.VehicleValuationGuide.DomainModel.Naaa.Commands.TransferObjects.Envelopes.ValuationArgumentsDto;
using ValuationResultsDto = FirstLook.VehicleValuationGuide.DomainModel.Naaa.Commands.TransferObjects.Envelopes.ValuationResultsDto;

namespace FirstLook.FirstLookServices.WebService.Controllers
{
    public class AuctionsController : Controller
    {
        public ActionResult Reference(string id)
        {
            switch(id)
            {
                case "naaa":
                    {
                        return NaaaReference();
                    }
                default:
                    {
                        return null;
                    }
            }

        }

        public ActionResult Data(string id, string vin, int? area, int? period, int? mileage)
        {
            switch (id)
            {
                case "naaa":
                    {
                        return NaaaData(vin, area, period, mileage);
                    }
                default:
                    {
                        return null;
                    }
            }

        }

        public ActionResult NaaaData(string vin, int? area, int? period, int? mileage)
        {
            
            var nadaFactory = RegistryFactory.GetResolver().Resolve<VehicleValuationGuide.DomainModel.Nada.Commands.ICommandFactory>();

            var traversalCommand = nadaFactory.CreateTraversalCommand();

            var traversalArguments = new TraversalArgumentsDto {Vin = RouteData.Values["vin"].ToString()};

            TraversalResultsDto traversalResults = traversalCommand.Execute(traversalArguments);

            if(!traversalResults.Path.CurrentNode.HasUid)
            {
                return Json("failed to obtain uid based on vin.");
            }

            int uid = traversalResults.Path.CurrentNode.Uid;

            var naaaFactory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            var valuationCommand = naaaFactory.CreateValuationCommand();

            //TODO: What are default values for area, period, mileage?
            ValuationArgumentsDto valuationArguments = new ValuationArgumentsDto
                                                           {
                                                               Area = area.HasValue ? area.Value : 1,
                                                               Period = period.HasValue ? period.Value : 1,
                                                               Mileage = mileage.HasValue ? mileage.Value : 500,
                                                               Uid = uid
                                                           };


            ValuationResultsDto valuationResults = valuationCommand.Execute(valuationArguments);

            AggregateDecimalDto aggregateDecimalDto = valuationResults.Report.SalePriceReport;

            //TODO: What happens when no max, min ... etc?
            SalePriceAggregation salePriceAggregation =
                new SalePriceAggregation
                    {
                        Average = aggregateDecimalDto.HasAverage ? aggregateDecimalDto.Average : 0,
                        Max = aggregateDecimalDto.HasMaximum ? aggregateDecimalDto.Maximum : 0,
                        Min = aggregateDecimalDto.HasMinimum ? aggregateDecimalDto.Minimum : 0,
                        SampleSize = aggregateDecimalDto.SampleSize
                    };

            AggregateDecimalDto salePriceSimilarMileageReport = valuationResults.Report.SalePriceSimilarMileageReport;

            SalePricesSimilarMileageAggregation salePricesSimilarMileageAggregation =
                new SalePricesSimilarMileageAggregation
                    {
                        Average = salePriceSimilarMileageReport.HasAverage
                                      ? salePriceSimilarMileageReport.Average
                                      : 0,
                        Max = salePriceSimilarMileageReport.HasMaximum
                                  ? salePriceSimilarMileageReport.Maximum
                                  : 0,
                        Min = salePriceSimilarMileageReport.HasMinimum
                                  ? salePriceSimilarMileageReport.Minimum
                                  : 0,
                        SampleSize = salePriceSimilarMileageReport.SampleSize
                    };

            AggregateIntegerDto aggregateIntegerDto = valuationResults.Report.MileageReport;

            MileagesAggregation mileagesAggregation =
                new MileagesAggregation
                    {
                        Average = aggregateIntegerDto.HasAverage
                                ? aggregateIntegerDto.Average
                                : 0,
                        Max = aggregateIntegerDto.HasMaximum
                                ? aggregateIntegerDto.Maximum
                                : 0,
                        Min = aggregateIntegerDto.HasMinimum
                                ? aggregateIntegerDto.Minimum
                                : 0,
                        SampleSize = aggregateIntegerDto.SampleSize
                    };

            return Json(new { period = period, area = area, salePrices = salePriceAggregation, salePricesSimilarMileage = salePricesSimilarMileageAggregation, mileages = mileagesAggregation }, JsonRequestBehavior.AllowGet);

            //return Json("TODO");
        }

        public ActionResult NaaaReference()
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            var command = factory.CreateFormCommand();

            var argumentsDto = new FormArgumentsDto();

            FormResultsDto results = command.Execute(argumentsDto);

            IList<Area> areas = results.Areas.Select(area => new Area {Id = area.Id, Name = area.Name}).ToList();

            IList<Period> periods = results.Periods.Select(period => new Period {Id = period.Id, Name = period.Name}).ToList();

            return Json(new{ periods = periods, areas = areas}, JsonRequestBehavior.AllowGet);
        }
     
    }
}