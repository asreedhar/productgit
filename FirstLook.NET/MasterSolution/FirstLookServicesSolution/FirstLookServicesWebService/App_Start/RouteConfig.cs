﻿using System.Web.Mvc;
using System.Web.Routing;

namespace FirstLook.FirstLookServices.WebService.App_Start
{
    internal static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            SetupRoutes(routes);
        }

        private static void SetupRoutes(RouteCollection routes)
        {            
            routes.MapRoute(
                "Default", 
                "{controller}/{action}/{id}", 
                new { controller = "Default", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );
        }

    }
}