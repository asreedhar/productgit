﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Caching;
using Autofac;
using Autofac.Integration.Web;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.IOC;
using FirstLook.Common.Core.Registry;
using FirstLook.Fault.DomainModel.Utilities;
using FirstLook.FirstLookServices.DomainModel;
using FirstLook.FirstLookServices.WebServiceStack.App_Start;
using FirstLook.Pricing.DomainModel;
using ServiceStack.MiniProfiler;
using ServiceStack.Text;
using ICache = FirstLook.Common.Core.Cache.ICache;
using Module = FirstLook.FirstLookServices.DomainModel.Module;

namespace FirstLook.FirstLookServices.WebServiceStack
{
    public class Global : HttpApplication, IContainerProviderAccessor
    {
        private static IContainerProvider _containerProvider;

        private readonly Dictionary<String, IDisposable> _callStackDisposables = new Dictionary<String, IDisposable>();
        const string HeaderText = "callStackId";

        void Application_Start(object sender, EventArgs e)
        {
            LogConfig.RegisterLogging();

            var builder = new ContainerBuilder();

            new PricingDomainRegister().Register(builder);
            new FirstlookServiceDomainRegister().Register(builder);
     
            
            IContainer container = builder.Build();

            Registry.RegisterContainer(container);

            _containerProvider = new ContainerProvider(container);

            // Code that runs on application startup      
            IRegistry registry = RegistryFactory.GetRegistry();

            registry.Register<Module>();

            registry.Register<Client.DomainModel.Module>();

            registry.Register<Pricing.DomainModel.Commands.Module>();

            registry.Register<Client.DomainModel.Vehicles.Commands.Module>();

            registry.Register<VehicleValuationGuide.DomainModel.Module>();

            registry.Register<VehicleHistoryReport.DomainModel.Module>();

            registry.Register<Fault.DomainModel.Module>();

            registry.Register<ICommandExecutor, ListeningCommandExecutor>(ImplementationScope.Isolated);

            registry.Register<IDataSessionManager, DataSessionManager>(ImplementationScope.Shared);

            registry.Register<ICache, NullCache>(ImplementationScope.Shared);

            new FirstLookServicesAppHost().Init();

            // Configure AutoMapper definitions
            ExplicitAutoMapperProjections.Init();
        }

        //Used for HTTP modules in Autofac.Integration.Web
        IContainerProvider IContainerProviderAccessor.ContainerProvider
        {
            get { return _containerProvider; }
        }

        private class ListeningCommandExecutor : SimpleCommandExecutor
        {

            public ListeningCommandExecutor()
            {
                Add(new FaultWebExceptionObserver());
            }

        }

        //Cache
        private class WebCache : ICache
        {
            private readonly Cache _cache;

            public WebCache()
            {
                _cache = HttpContext.Current.Cache;
            }

            public object Add(string key, object value, DateTime absoluteExpiration, TimeSpan slidingExpiration)
            {
                return _cache.Add(
                    key,
                    value,
                    null,
                    absoluteExpiration,
                    slidingExpiration,
                    CacheItemPriority.Normal,
                    null);
            }

            public object Get(string key)
            {
                return _cache.Get(key);
            }

            public object Remove(string key)
            {
                return _cache.Remove(key);
            }
        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

            // tear down AutoMapper definitions
            ExplicitAutoMapperProjections.Reset();
        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs
        }

        protected void Application_BeginRequest(object src, EventArgs e)
        {
//            MiniProfiler.Start();
            var serviceConf = (NameValueCollection)ConfigurationManager.GetSection("servicesConfig");
            if (serviceConf["Debug"] == "true")
            {
                //Note that we're not wrapping the stop method in a debug check
                //That struck me as a more robust way to go but worth looking at if anything's being weird around profiler - Erik Reppen 4-26-2013
                Profiler.Start();
            }
            try
            {
                var callStackId = Request.Headers.AllKeys.Contains(HeaderText) ? 
                    Request.Headers[HeaderText] : 
                    Guid.NewGuid().ToString();
                
                _callStackDisposables.Add(callStackId, log4net.ThreadContext.Stacks[HeaderText].Push(callStackId));
                Response.Headers.Add(HeaderText, callStackId);
                
                if (e == EventArgs.Empty)
                {
                    log4net.ThreadContext.Stacks["context"].Push("empty");
                }
                else
                {
                    var childMessageJson = e.ToJson();
                    log4net.ThreadContext.Stacks["context"].Push(childMessageJson);
                }
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch (Exception) { /* That didn't work. */ }
            // ReSharper restore EmptyGeneralCatchClause
            
        }

        protected void Application_EndRequest(object src, EventArgs e)
        {
//            if (Request.IsLocal)
//            {
//                MiniProfiler.Start();
            Profiler.Stop();
            try
            {
                if (!Response.Headers.AllKeys.Contains(HeaderText))
                {
                    return;
                }

                IDisposable requestDisposable;
                _callStackDisposables.TryGetValue(Response.Headers[HeaderText], out requestDisposable);
                if (requestDisposable != null)
                {
                    requestDisposable.Dispose();
                    _callStackDisposables.Remove(Response.Headers[HeaderText]);
                }
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch (Exception) { /* Don't crash the request over it. */ }
            // ReSharper restore EmptyGeneralCatchClause
            
//            }
        }


        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started

        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }
    }
}
