﻿/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FirstLook.FirstLookServices.DomainModel;
using FirstLook.FirstLookServices.WebServiceStack.Utility;
using ServiceStack.CacheAccess;
using ServiceStack.Common;
using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Filters
{
    public class CachedByRequestAttribute : Attribute, IHasRequestFilter
    {
        //This property will be resolved by the IoC container
        public ICacheClient Cache { get; set; }

        public void RequestFilter(IHttpRequest req, IHttpResponse res, object requestDto)
        {
            var cacheKey = UrnId.Create("VehicleConfigService", ETagHelper.CreateETagHash(req));



            return RequestContext.ToOptimizedResultUsingCache(base.Cache, cacheKey, () =>
            {
                // Delegate is executed if item doesn't exist in cache 
                // Any response DTO returned here will be cached automatically

                // hard-coded to KBB for now
                var config = _builder.Build(r.Style, BookFactory.Instance.Get(BookId.Kbb));
                return BuildResponse(config);
            });

            //This code is executed before the service
            string userAgent = req.UserAgent;
            StatisticManager.SaveUserAgent(userAgent);
            throw new NotImplementedException();
        }

        public IHasRequestFilter Copy()
        {
            throw new NotImplementedException();
        }

        public int Priority { get; private set; }
    }

}*/