using System;
using System.Net;
using System.Collections.Generic;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Utility
{
    public class VersionMapper<T>
    {
        readonly string _defaultVersion;

        public VersionMapper(string defaultVersion)
        {
            _defaultVersion = defaultVersion; 
        }
        private IDictionary<string, Func<T, object>> _functions = new Dictionary<string, Func<T, object>>();

        public void Add(string version, string httpMethod, Func<T, object> serviceFunction)
        {
            _functions.Add(version + "," + httpMethod, serviceFunction);
        }
        
        public Func<T, object> GetServiceFunction(string version, string httpMethod)
        {
            if (version == null)
            {
                Console.WriteLine("VersionMapper<T>:add No version specified def :=> " + _defaultVersion );
                version = _defaultVersion;
            }

            string key = (version + "," + httpMethod);

            return (_functions.ContainsKey(key)) ?  _functions[key] : Response404;
        }

        static object Response404(T t)
        {
            IHttpResult result = new HttpResult {StatusCode = HttpStatusCode.NotFound};
            return result;
        }
    }

}