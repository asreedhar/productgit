﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Collections.Specialized;

namespace FirstLook.FirstLookServices.WebServiceStack.Utility
{


    static public class StringKeys
    {
    readonly static public string CacheControlHourSecs = "cacheControlHourSecs";
    readonly static public string CacheControlDaySecs = "cacheControlHourSecs";
    readonly static public string UserMayOrderReport = "userMayOrderReport";
    readonly static public string DefaultArea = "DefaultArea";
    readonly static public string DefaultPeriod = "DefaultPeriod";
    readonly static public string CarfaxReportNotAvailable = "carfaxReportNotAvailable";
    }

    public class ServiceSettings
    {
        public static string DefaultSection = "servicesConfig";
       // public static StringKeys TheStringKeys = StringKeys;
    
        public string Setting(string key)
        {
            var value = ((NameValueCollection)ConfigurationManager.GetSection(DefaultSection))[key];
            return value;
        }
        public string GetString(string key)
        {
            var value = ((NameValueCollection)ConfigurationManager.GetSection("stringTable"))[key];
            return value;
        }
        public string GetString(string key, string sub1)
        {
            var value = ((NameValueCollection)ConfigurationManager.GetSection("stringTable"))[key];
            var a = String.Format(value, sub1);
            return a;
        }
        public string GetString(string key, string sub1, string sub2)
        {
            var value = ((NameValueCollection)ConfigurationManager.GetSection("stringTable"))[key];
            var a = String.Format(value, sub1, sub2);
            return a;
        }
        public string GetString(string key, string sub1, string sub2, string sub3)
        {
            var value = ((NameValueCollection)ConfigurationManager.GetSection("stringTable"))[key];
            var a = String.Format(value, sub1, sub2, sub3);
            return a;
        }

        public string GetErrorString(int errorNumber)
        {
            return GetString("err." + errorNumber);
        }
        public string GetErrorString(int errorNumber, string sub1)
        {
            return GetString("err." + errorNumber, sub1);

        }
        public string GetErrorString(int errorNumber, string sub1, string sub2)
        {
            return GetString("err." + errorNumber, sub1, sub2);
        }
        public string GetErrorString(int errorNumber, string sub1, string sub2, string sub3)
        {
            return GetString("err." + errorNumber, sub1, sub2, sub3);
        }
    }

    public static class StringTableMixin
    {
        static readonly ServiceSettings Settings = new ServiceSettings();

        public static string GetString(this object caller, string key)
        {
            return Settings.GetString(key);
        }
        public static string GetString(this object caller, string key, string substitution1)
        {
            return Settings.GetString(key, substitution1);
        }
        public static string GetString(this object caller, string key, string substitution1, string substitution2)
        {
            return Settings.GetString(key, substitution1, substitution2);
        }
        public static string GetString(this object caller, string key, string substitution1, string substitution2, string substitution3)
        {
            return Settings.GetString(key, substitution1, substitution2, substitution3);
        }
        public static string GetErrorString(this object caller, int errNumber)
        {
            return Settings.GetErrorString(errNumber);
        }
        public static string GetErrorString(this object caller, int errNumber, string substitution1)
        {
            return Settings.GetErrorString(errNumber, substitution1);
        }
        public static string GetErrorString(this object caller, int errNumber, string substitution1, string substitution2)
        {
            return Settings.GetErrorString(errNumber, substitution1, substitution2);
        }
        public static string GetErrorString(this object caller, int errNumber, string substitution1, string substitution2, string substitution3)
        {
            return Settings.GetErrorString(errNumber, substitution1, substitution2, substitution3);
        }
    }

    public static class ServiceSettingsMixin
    {
        static readonly ServiceSettings Settings = new ServiceSettings();

        public static string Setting(this object caller, String key)
        {
            return Settings.Setting(key);
        }

        public static bool SettingAsBool(this ServiceStack.ServiceInterface.Service service, String key)
        {
            string strB = Setting(service, key);

            strB = strB == null ? "" : strB.ToLower();

            return (strB.Equals("true") || strB.Equals("yes"));
        }

        public static int SettingAsInteger(this ServiceStack.ServiceInterface.Service service, string key)
        {
            string strI = Setting(service, key);
            return strI == null ? 0 : Convert.ToInt32(strI);
        }
        public static string GetString(this ServiceStack.ServiceInterface.Service service, string key)
        {
            return Settings.GetString(key);
        }

    }
}