﻿using System.Security.Cryptography;
using System.Text;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.Text;

namespace FirstLook.FirstLookServices.WebServiceStack.Utility
{
    public class ETagHelper
    {
        
        public static string AddETagHeader(IHttpResult res, object data)
        {
            string eTagHash = CreateETagHash(data);
            res.Headers.Add(HttpHeaders.ETag, eTagHash);
            return eTagHash;
        }

        internal static string CreateETagHash(object data)
        {
            return GetMd5Hash(data.ToJson());
        }

        internal static string CreateETagHash(string data)
        {
            return GetMd5Hash(data);            
        }
  
        private static string GetMd5Hash( string input)
        {
            // Convert the input string to a byte array and compute the hash. 
            byte[] data = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes 
            // and create a string.
            var sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data  
            // and format each one as a hexadecimal string. 
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string. 
            return sBuilder.ToString();
        }
    } 
}
