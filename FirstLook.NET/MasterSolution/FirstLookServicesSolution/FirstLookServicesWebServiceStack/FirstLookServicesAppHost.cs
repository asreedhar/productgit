﻿using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using FirstLook.FirstLookServices.WebServiceStack.Plugins.Authentication;
using Funq;
using ServiceStack.CacheAccess;
using ServiceStack.CacheAccess.Providers;
using ServiceStack.Common.Web;
using ServiceStack.ServiceInterface;
using ServiceStack.ServiceInterface.Auth;
using ServiceStack.Text;
using ServiceStack.WebHost.Endpoints;
using System.Net;
using ServiceStack.ServiceHost;
using System;
using FirstLook.FirstLookServices.WebServiceStack.Utility;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.WebServiceStack
{
    public class FirstLookServicesAppHost : AppHostBase
    {
        //Tell Service Stack the name of your application and where to find your web services
        public FirstLookServicesAppHost() : base("FirstLookServices", typeof(FirstLookServicesAppHost).Assembly) { }

        public override void Configure(Container container)
        {
            //Prevent __type attribute from being serialized on json response
            JsConfig.ExcludeTypeInfo = true;
            JsConfig.IncludeNullValues = true;

            JsConfig.ThrowOnDeserializationError = true;

            var headerConf = (NameValueCollection)ConfigurationManager.GetSection("servicesConfig");

            string url = headerConf.Get("WebHostUrl");

            SetConfig(new EndpointHostConfig
            {
                DefaultContentType = ContentType.Json,
                DebugMode = true,
                WebHostUrl = url
            });

            //Handle Unhandled Exceptions occurring outside of Services
            //Stop the output on 'Forbidden'
            this.ExceptionHandler = (req, res, operationName, ex) =>
            {
                if (ex.Message == "Invalid UserName or Password")
                {
                    res.Write("");
                    res.StatusCode = (int)HttpStatusCode.Unauthorized;
                }
                else
                {
                    IDictionary<string, object> error = this.ExceptionToError(ex, null);
                    IDictionary<string, List<IDictionary<string, object>>> responseObj = new Dictionary<string, List<IDictionary<string, object>>>();
                    List<IDictionary<string, object>> errorList = new List<IDictionary<string, object>>();
                    if (error != null && error.Values.Count > 0)
                    {
                        if (error.ContainsKey("status"))
                        {
                            res.StatusCode = (int)error["status"];
                            error.Remove("status");
                        }
                    }
                    responseObj["errors"] = errorList;
                    errorList.Add(error);

                    // Preserve old error handling, but offer the option to return JSON compliant errors.
                    var useJsonErrors = (req.Headers.AllKeys.Contains("X-Use-Json-Errors") && 
                        req.Headers["X-Use-Json-Errors"].ToUpperInvariant().Equals("TRUE"));
                    res.Write(useJsonErrors ? 
                    JsonSerializer.SerializeToString(responseObj) : 
                    responseObj.SerializeAndFormat());
                }
            };

            this.ServiceExceptionHandler = (req, ex) =>
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            };

            Plugins.Add(new AuthFeature(() => new AuthUserSession(), new IAuthProvider[]
                                                                         {
                                                                             new CasAuthProvider()
                                                                         }) { HtmlRedirect = null });

        }
    }

}