﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Reports
{
    [DataContract]
    public class CarfaxResponse : Models.Carfax.Report
    {
    }

    [DataContract]
    public class AutoCheckResponse : Models.AutoCheck.Report 
    {
    }
}