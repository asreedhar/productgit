﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Reports.Models.AutoCheck
{
    [DataContract]
    public class Report
    {
        [DataMember(Name = "expirationDate")]
        public string ExpirationDate { get; internal set; }

        [DataMember(Name = "userName")]
        public string UserName { get; internal set; }

        [DataMember(Name = "ownerCount")]
        public int OwnerCount { get; internal set; }

        [DataMember(Name = "score")]
        public int Score { get; internal set; }

        [DataMember(Name = "compareScoreRangeLow")]
        public int CompareScoreRangeLow { get; internal set; }

        [DataMember(Name = "compareScoreRangeHigh")]
        public int CompareScoreRangeHigh { get; internal set; }

        [DataMember(Name = "href")]
        public string Href { get; internal set; }

        [DataMember(Name = "inspections")]
        public IDictionary<string,bool> Inspections { get; internal set; }
    }
}