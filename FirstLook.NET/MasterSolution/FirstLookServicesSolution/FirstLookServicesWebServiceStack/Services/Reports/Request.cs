﻿using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Reports
{
    [Route("/reports/carfax/dealers/{dealer}/vins/{vin}")]
    [Route("/{ver}/reports/carfax/dealers/{dealer}/vins/{vin}")]
    public class Carfax
    {
        public int Dealer { get; set; }
        public string Ver { get; set; }
        public string Vin { get; set; }
    }

    [Route("/reports/autocheck/dealers/{dealer}/vins/{vin}")]
    [Route("/{ver}/reports/autocheck/dealers/{dealer}/vins/{vin}")]
    public class AutoCheck
    {
        public int Dealer { get; set; }
        public string Ver { get; set; }   
        public string Vin { get; set; }
    }

}