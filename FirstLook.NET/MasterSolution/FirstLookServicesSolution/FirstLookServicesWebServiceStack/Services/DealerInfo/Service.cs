﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.DealerInfo.Commands;
using FirstLook.FirstLookServices.DomainModel.DealerInfo.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.DealerInfo.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.WebServiceStack.Services.Dealerinfo;
using FirstLook.FirstLookServices.WebServiceStack.Services.DealerInfo.Models;
using FirstLook.FirstLookServices.WebServiceStack.Utility;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;


namespace FirstLook.FirstLookServices.WebServiceStack.Services.DealerInfo
{
    [Authenticate]
    public class Service : ServiceStack.ServiceInterface.Service
    {

        private readonly VersionMapper<Request.DealerInfo> _dealerInfoMapper = new VersionMapper<Request.DealerInfo>("v1");

        public Service()
        {
            _dealerInfoMapper.Add("v1", "get", Get_v1);

        }

        public object Get(Request.DealerInfo request)
        {
            try
            {
                return _dealerInfoMapper.GetServiceFunction(request.Ver, "get")(request);
            }
            catch (Exception ex)
            {
                {
                    var err = this.ExceptionToError(ex, null);
                    return (err != null) ? this.HandleError(err) : null;
                }
            }

        }


         public object Get_v1(Request.DealerInfo request)
        {

            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            string oldETag = RequestContext.GetHeader("If-None-Match");
            var command = factory.CreateDealerInfoCommand();
            var arguments = new DealerInfoArgumentsDto
            {
                BusinessUnitId = request.DealerId
            };

            DealerInfoResultsDto results = Helper.ExecuteCommand(command, Helper.WrapInContext(arguments, this.GetSession().UserAuthName)); 

            Response.DealerInf response = null;

            if (results.DealerInfo != null)
            {
                response = GetDealerInfo(results.DealerInfo);
            }

              IHttpResult result = Handle304(oldETag, response, true);


                if (result.StatusCode != HttpStatusCode.NotModified)
                {
                    // ETag does not match so we need to send to updated resource
                    result = Helper.SetStaticHeaders(result, new[] { "cacheControlHour" });
                }

                return result;
        }

         IHttpResult Handle304(string oldETag, object response, bool compress)
         {
             string newETag = ETagHelper.CreateETagHash(response);

             IHttpResult result = new HttpResult();

             if (oldETag != null && oldETag.Equals(newETag))
             {
                 //  ETags match so no need to zip and send the whole response - just headers
                 result.StatusCode = HttpStatusCode.NotModified;
             }
             else
             {
                 // ETag does not match so we need to send to updated resource
                 // 
                 if (compress)
                     result = Helper.Compress(this, response);
                 else
                     result = new HttpResult
                     {
                         Response = response
                     };

                 ETagHelper.AddETagHeader(result, response);

                 result.StatusCode = HttpStatusCode.OK;
             }
             return result;
         }

        private Response.DealerInf GetDealerInfo(DealerInfoDto objs)
         {
             DealerDetails dealerdet = new DealerDetails();
             dealerdet.DealerId = objs.DealerID;
             dealerdet.PingIIDefaultSearchRadius = objs.PingIIDefaultSearchRadius;
             dealerdet.PingIIMaxSearchRadius = objs.PingIIMaxSearchRadius;
             dealerdet.PingIIGuideBookId = objs.PingIIGuideBookId;
             dealerdet.IsNewPing = objs.IsNewPing;
             dealerdet.PingIIMarketListing = objs.PingIIMarketListing;
             dealerdet.PingIIRedRange1Value1 = objs.PingIIRedRange1Value1;
             dealerdet.PingIIRedRange1Value2 = objs.PingIIRedRange1Value2;
             dealerdet.PingIIYellowRange1Value1 = objs.PingIIYellowRange1Value1;
             dealerdet.PingIIYellowRange1Value2 = objs.PingIIYellowRange1Value2;
             dealerdet.PingIIYellowRange2Value1 = objs.PingIIYellowRange2Value1;
             dealerdet.PingIIYellowRange2Value2 = objs.PingIIYellowRange2Value2;
             dealerdet.PingIIGreenRange1Value1 = objs.PingIIGreenRange1Value1;
             dealerdet.PingIIGreenRange1Value2 = objs.PingIIGreenRange1Value2;
             dealerdet.ZipCode = objs.ZipCode;
             dealerdet.OwnerName = objs.OwnerName;
             dealerdet.Latitude = objs.Latitude;
             dealerdet.Longitude = objs.Longitude;
             dealerdet.UnitCostThresholdLower = objs.UnitCostThresholdLower;
             dealerdet.UnitCostThresholdUpper = objs.UnitCostThresholdUpper;
             dealerdet.GuideBookID = objs.GuideBookID;
             dealerdet.BookOut = objs.BookOut;
             dealerdet.UnitCostThreshold = objs.UnitCostThreshold;
             dealerdet.GuideBook2Id = objs.GuideBook2Id;
             dealerdet.DistanceList = new List<int>();
             objs.DistanceList.ToList().ForEach(obj => dealerdet.DistanceList.Add(obj));
             dealerdet.FavourableThreshold = objs.FavourableThreshold;
             dealerdet.OriginalMSRP = objs.OriginalMSRP;
             dealerdet.MarketAvgInternetPrice = objs.MarketAvgInternetPrice;
             dealerdet.EdmundsTrueMarketValue = objs.EdmundsTrueMarketValue;
             dealerdet.NADARetailValue = objs.NADARetailValue;
             dealerdet.KBBRetailValue = objs.KBBRetailValue;

             return new Response.DealerInf() { DealerInfo = dealerdet };

        }
    }
}




