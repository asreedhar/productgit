﻿using System.Runtime.Serialization;



namespace FirstLook.FirstLookServices.WebServiceStack.Services.DealerInfo
{
    public class Response
    {
        [DataContract]
        public class DealerInf
        {
            [DataMember(Name = "dealerInfo")]
            public DealerInfo.Models.DealerDetails DealerInfo { get; set; }

        }

    }
}