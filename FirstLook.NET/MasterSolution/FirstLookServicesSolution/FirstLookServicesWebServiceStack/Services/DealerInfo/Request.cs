﻿using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Dealerinfo
{
    public class Request
    {
        [Route("/{ver}/dealerInfo/dealerId/{dealerid}")]
        [Route("/dealerInfo/dealerId/{dealerid}")]

        public class DealerInfo
        {
            public int DealerId { get; set; }
            public string Ver { get; set; }
        }
    }
}