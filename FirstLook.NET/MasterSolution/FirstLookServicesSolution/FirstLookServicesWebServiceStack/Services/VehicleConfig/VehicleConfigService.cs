﻿using System;
using System.Collections.Generic;
using System.Net;
using FirstLook.Fault.DomainModel.Utilities;
using FirstLook.FirstLookServices.DomainModel;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.FirstLookServices.DomainModel.SimpleBooks;
using FirstLook.FirstLookServices.DomainModel.VehicleConfig;
using FirstLook.FirstLookServices.DomainModel.VehicleConfig.Internal;
using FirstLook.FirstLookServices.WebServiceStack.Utility;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using SSI = ServiceStack.ServiceInterface;


namespace FirstLook.FirstLookServices.WebServiceStack.Services.VehicleConfig
{
    [SSI.Authenticate]
    public class VehicleConfigService : SSI.Service, IService<VehicleConfigRequestByVin>,
                                        IService<VehicleConfigRequestByStyle>
    {
        public object Execute(VehicleConfigRequestByVin r)
        {
            var builder = new VehicleConfigBuilder(r.Dealer);
            return Execute(r.Dealer, r.Vin, builder.Build);
        }

        public object Execute(VehicleConfigRequestByStyle r)
        {
            var builder = new VehicleConfigBuilder(r.Dealer);
            return Execute(r.Dealer, r.Style, builder.Build);
        }

        private object Execute<T>(int dealerId, T id, Func<T, IBook,int, IEnumerable<IVehicleConfig>> builderFunction)
        {
            VehicleConfigResponse response = null;
            string oldETag = RequestContext.GetHeader("If-None-Match");

            try
            {
                //   int n = 0;
                var b = new BooksRepository();
                var books = b.GetBooks(dealerId);
                //if (n == 0) throw new Exception("Nothing really!");
                var configs = new List<IVehicleConfig>();
                foreach (var book in books)
                {
                    configs.AddRange(builderFunction(id, book, dealerId));
                }

                response = BuildResponse(configs);

            }
            catch(Exception ex)
            {
                FaultWebExceptionObserver.SaveException(ex);
                throw;
            }

            IHttpResult result = handle304(oldETag, response, true);
            result = Helper.SetStaticHeaders(result, new string[] { "cacheControlHour" });
            return result;
        }


        private static VehicleConfigResponse BuildResponse(IEnumerable<IVehicleConfig> configs)
        {
            var response = new VehicleConfigResponse();
            var configList = new List<VehicleConfig>();

            foreach (var cfg in configs)
            {
                var c = new VehicleConfig();
                var equipments = new List<Equipment>();
                var equipmentRules = new List<EquipmentRule>();

                c.Book = cfg.Book.Desc;
                c.Selected = cfg.Selected;
                c.Trim = cfg.Trim;
                c.TrimId = cfg.TrimId;
                c.Make = cfg.Make;
                c.Model = cfg.Model;
                c.Year = cfg.Year;

                foreach (var e in cfg.Equipment)
                {
                    equipments.Add(
                        new Equipment
                        {
                            Default = e.Default,
                            Desc = e.Desc,
                            Id = e.Id,
                            SortOrder = e.SortOrder
                        });
                }
                c.Equipment = equipments.ToArray();

                foreach (var rule in cfg.EquipmentRules)
                {
                    equipmentRules.Add(
                        new EquipmentRule
                        {
                            Rule = rule.Rule,
                            Equipment = rule.Equipment
                        });
                }
                c.EquipmentRules = equipmentRules.ToArray();

                configList.Add(c);
            }

            if (configList.Count == 0)
                throw new NoDataFoundException("No Configs found.");

            response.Configs = configList.ToArray();
            return response;

        }

        IHttpResult handle304(string oldETag, object response, bool compress)
        {
            string newETag = ETagHelper.CreateETagHash(response);
            IHttpResult result = new HttpResult();
            if (oldETag != null && oldETag.Equals(newETag))
            {
                //  ETags match so no need to zip and send the whole response - just headers
                result.StatusCode = HttpStatusCode.NotModified;
            }
            else
            {
                // ETag does not match so we need to send to updated resource
                if (compress)
                    result = Helper.Compress(this, response);
                else
                    result = new HttpResult
                    {
                        Response = response
                    };

                ETagHelper.AddETagHeader(result, response);
                result.StatusCode = HttpStatusCode.OK;
            }
            return result;
        }

    }
}