﻿using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.VehicleConfig
{

    [Route("/{ver}/vehicleConfig/dealers/{dealer}/styles/{style}")]
    [Route("/vehicleConfig/dealers/{dealer}/styles/{style}")]
    public class VehicleConfigRequestByStyle
    {
        public string Ver { get; set; }
        public int Dealer { get; set; }
        public int Style { get; set; }
    }

    [Route("/{ver}/vehicleConfig/dealers/{dealer}/vins/{vin}")]
    [Route("/vehicleConfig/dealers/{dealer}/vins/{vin}")]
    public class VehicleConfigRequestByVin
    {
        public string Ver { get; set; }
        public int Dealer { get; set; }
        public string Vin { get; set; }        
    }
}