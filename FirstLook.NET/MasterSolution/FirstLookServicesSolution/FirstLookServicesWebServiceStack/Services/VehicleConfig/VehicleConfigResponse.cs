﻿
using System.Runtime.Serialization;
namespace FirstLook.FirstLookServices.WebServiceStack.Services.VehicleConfig
{
    [DataContract]
    public class VehicleConfigResponse
    {
        [DataMember(Name = "configs")]
        public VehicleConfig[] Configs { get; set; }
    }

    [DataContract]
    public class VehicleConfig
    {
        [DataMember(Name = "book")]
        public string Book { get; set; }

        [DataMember(Name = "trim")]
        public string Trim { get; set; }

        [DataMember(Name = "trimId")]
        public dynamic TrimId { get; set; }

        [DataMember(Name = "selected")]
        public bool Selected { get; set; }

        [DataMember(Name = "equipmentRules")]
        public EquipmentRule[] EquipmentRules { get; set; }

        [DataMember(Name = "equipment")]
        public Equipment[] Equipment { get; set; }

        [DataMember(Name = "make")]
        public string Make { get; set; }

        [DataMember(Name = "model")]
        public string Model { get; set; }

        [DataMember(Name = "year")]
        public int Year { get; set; }
    }

    [DataContract]
    public class Equipment
    {
        [DataMember(Name = "id")]
        public dynamic Id { get; set; }

        [DataMember(Name = "desc")]
        public string Desc { get; set; }

        [DataMember(Name = "default")]
        public bool Default { get; set; }

        [DataMember(Name = "sortOrder")]
        public int SortOrder { get; set; }
    }

    [DataContract]
    public class EquipmentRule
    {
        [DataMember(Name = "rule")]
        public string Rule { get; set; }

        [DataMember(Name = "equipment")]
        public dynamic[] Equipment { get; set; }
    }
}