﻿using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.UserRole
{
    
    public class Request
    {

        [Route("/{ver}/userAccess")]
        [Route("/userAccess")]
        public class UserRole
        {
            public string Ver { get; set; }
            
        }

    }
}