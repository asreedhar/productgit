﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.UserRole
{
    public class Response
    {
        [DataContract]
        public class UserRoleResponse
        {
            [DataMember(Name = "memberType")]
            public string MemberType { get; set;}
        }
    }
}