﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using FirstLook.FirstLookServices.WebServiceStack.Services.PricingAnalysis.Models;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.PricingAnalysis
{
    public class Response
    {
        [DataContract]
        public class PricingAnalysisResponse
        {
            [DataMember(Name = "InternetPrices")]
            public InternetPrices  ChangedInternetPrice { get; set; }
        }
    }
}