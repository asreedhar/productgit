﻿using System;
using System.Net;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Commands;
using FirstLook.FirstLookServices.DomainModel.PricingAnalysis.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.WebServiceStack.Utility;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.PricingAnalysis
{
    [Authenticate]
    public class Service : ServiceStack.ServiceInterface.Service
    {

        private readonly VersionMapper<Request.InterpriceChange> _PricingAnalysis = new VersionMapper<Request.InterpriceChange>("v1");


        public object Post(Request.InterpriceChange request)
        {
            try
            {
                return _PricingAnalysis.GetServiceFunction(request.Ver, "post")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }



        public Service()
        {
            _PricingAnalysis.Add("v1", "post", post_v1);

        }

        public IHttpResult post_v1(Request.InterpriceChange internetPriceChange)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            var command = factory.CreateChangeInternetPriceCommand();

            var arguments = new ChangeInternetPriceArgumentsDto()
            {
                OwnerHandle=internetPriceChange.OwnerHandle,
                SearchHandle=internetPriceChange.SearchHandle,
                VehicleHandle=internetPriceChange.VehicleHandle,
                Oldprice=internetPriceChange.OldPrice,
                NewPrice= internetPriceChange.NewPrice,
                InventoryId=internetPriceChange.InventoryId,
                DealerId=internetPriceChange.DealerId
                
            };

            ChangeInternetPriceResultsDto results = Helper.ExecuteCommand(command, Helper.WrapInContext(arguments, this.GetSession().UserAuthName));

            Response.PricingAnalysisResponse response=new Response.PricingAnalysisResponse(){ChangedInternetPrice=new Models.InternetPrices()
                                                                                            {OldPrice=results.OldInternetPrice,
                                                                                             NewPrice=results.NewinternetPrice}};


            IHttpResult result = Helper.Compress(this, response);
            
            result.StatusCode=HttpStatusCode.OK;

            result = Helper.SetStaticHeaders(result, new[]{ "cacheControlNoCache" });

            return result;
                    

        }

    }
}