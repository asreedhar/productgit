﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.PricingAnalysis.Models
{
    [DataContract]
    public class InternetPrices
    {
        [DataMember(Name = "OldPrice")]
        public decimal OldPrice { get; set; }
        [DataMember(Name = "NewPrice")]
        public decimal NewPrice { get; set; }
    }
}