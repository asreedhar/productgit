﻿using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.PricingAnalysis
{
    
    public class Request
    {
        [Route("/{Ver}/PricingAnalysis/dealer/{DealerId}/inventoryId/{InventoryId}")]
        [Route("/PricingAnalysis/dealer/{DealerId}/inventoryId/{InventoryId}")]
        public class InterpriceChange
        {
            
            public string OwnerHandle { get; set; }
            public string SearchHandle{get;set;}
            public string VehicleHandle{get;set;}
            public decimal OldPrice { get; set; }
            public decimal NewPrice { get; set; }
            public string Ver { get; set; }
            public int DealerId { get; set; }
            public int InventoryId { get; set; }
        }
    }
}