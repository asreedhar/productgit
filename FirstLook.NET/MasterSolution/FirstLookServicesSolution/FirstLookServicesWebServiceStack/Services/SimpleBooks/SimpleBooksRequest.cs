﻿using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.SimpleBooks
{
    [Route("/{ver}/simplebooks/dealers/{dealer}")]
    [Route("/simplebooks/dealers/{dealer}")]
    public class SimpleDealerBooksRequest
    {
        public int Dealer { get; set; }
        public string Ver { get; set; }
    }

    [Route("/{ver}/simplebooks")]
    [Route("/simplebooks")]
    public class SimpleBooksRequest
    {
        public string Ver { get; set; }
    }
}