﻿namespace FirstLook.FirstLookServices.WebServiceStack.Services.SimpleBooks
{
    public class SimpleBooksResponse
    {
        public string[] Books { get; set; }
    }
}