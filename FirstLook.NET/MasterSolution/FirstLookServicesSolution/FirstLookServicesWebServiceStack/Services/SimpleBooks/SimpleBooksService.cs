﻿using System.Linq;
using FirstLook.FirstLookServices.DomainModel.SimpleBooks;
using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.SimpleBooks
{
    public class SimpleBooksService : IService<SimpleDealerBooksRequest>, IService<SimpleBooksRequest>
    {
        private readonly BooksRepository _booksRepository;

        public SimpleBooksService()
        {
            _booksRepository = new BooksRepository();            
        }

        public object Execute(SimpleBooksRequest request)
        {
            return new SimpleBooksResponse
                {
                    Books = _booksRepository.GetBooks()
                                            .Select(b => b.Desc)
                                            .ToArray()
                };
        }

        public object Execute(SimpleDealerBooksRequest request)
        {
            return new SimpleBooksResponse
                {
                    Books = _booksRepository.GetBooks(request.Dealer)
                                            .Select(b => b.Desc)
                                            .ToArray()
                };
        }
    }
}