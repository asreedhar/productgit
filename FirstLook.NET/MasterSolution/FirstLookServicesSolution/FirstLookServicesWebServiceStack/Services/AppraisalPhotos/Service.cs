﻿using System;
using System.Net;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands;
using FirstLook.FirstLookServices.DomainModel.AppraisalPhotos.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.WebServiceStack.Utility;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.AppraisalPhotos
{
    [Authenticate]
    public class Service : ServiceStack.ServiceInterface.Service
    {
        readonly VersionMapper<Request.AppraisalPhotos> _appraisalPhotosMapper = new VersionMapper<Request.AppraisalPhotos>("v1");
        readonly VersionMapper<Request.AppraisalPhotosCopy> _appraisalCopyPhotosMapper = new VersionMapper<Request.AppraisalPhotosCopy>("v1");
        readonly VersionMapper<Request.AppraisalPhotoList> _appraisalPhotoListMapper = new VersionMapper<Request.AppraisalPhotoList>("v1");
        readonly VersionMapper<Request.AppraisalPhotoDelete> _appraisalPhotoDeleteMapper = new VersionMapper<Request.AppraisalPhotoDelete>("v1");
        public Service()
        {
            _appraisalPhotosMapper.Add("v1", "post", Post_v1);
            _appraisalCopyPhotosMapper.Add("v1", "post", Post_v1);
            _appraisalPhotoListMapper.Add("v1", "get", Get_v1);
            _appraisalPhotoDeleteMapper.Add("v1", "post", Post_v1);
        }

        public object Post(Request.AppraisalPhotos request)
        {
            try
            {
                return _appraisalPhotosMapper.GetServiceFunction(request.Ver, "post")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public object Post(Request.AppraisalPhotosCopy request)
        {
            try
            {
                return _appraisalCopyPhotosMapper.GetServiceFunction(request.Ver, "post")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public object Get(Request.AppraisalPhotoList request)
        {
            try
            {
                return _appraisalPhotoListMapper.GetServiceFunction(request.Ver, "get")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public object Post(Request.AppraisalPhotoDelete request)
        {
            try
            {
                return _appraisalPhotoDeleteMapper.GetServiceFunction(request.Ver, "post")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public IHttpResult Post_v1(Request.AppraisalPhotos request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            var command = factory.CreatePostAppraisalPhotosCommand();

            var arguments = new PostAppraisalPhotosArgumentsDto
            {
                DealerID = request.Dealer,
                FileName = request.FileName,
                UniqueID = request.UniqueID,
                Photos = request.Photo,
                SequenceID = request.SequenceID,
                Source = request.Source
            };

            PostAppraisalPhotosResultsDto results = Helper.ExecuteCommand(command, Helper.WrapInContext(arguments, this.GetSession().UserAuthName));

            AppraisalPhotoResponse response = new AppraisalPhotoResponse()
                                            {
                                                PhotoUrl = results.PhotoUrl,
                                                ThumbnailUrl = results.ThumbnailUrl,
                                                UniqueId = results.UniqueID,
                                                PhotoKey = results.PhotoKey,
                                                ThumbnailKey = results.ThumbnailKey
                                            };

            IHttpResult result = Helper.Compress(this, response);

            result.StatusCode = HttpStatusCode.Created;

            result = Helper.SetStaticHeaders(result, new[] { "cacheControlNoCache" });

            return result;
        }

        public IHttpResult Post_v1(Request.AppraisalPhotosCopy request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            var command = factory.CreateCopyAppraisalPhotos();

            var arguments = new AppraisalCopyPhotosArgumentsDto()
            {
                DealerID = request.Dealer,
                NewUniqueID = request.NewUniqueID,
                UniqueID = request.UniqueID,
                Source = request.Source
            };

            AppraisalCopyPhotosResultsDto results = Helper.ExecuteCommand(command, Helper.WrapInContext(arguments, this.GetSession().UserAuthName));

            AppraisalPhotoCopyResponse response = new AppraisalPhotoCopyResponse()
            {
                AppraisalPhotos = results.AppraisalPhotos
            };

            IHttpResult result = Helper.Compress(this, response);

            result.StatusCode = HttpStatusCode.OK;

            result = Helper.SetStaticHeaders(result, new[] { "cacheControlNoCache" });

            return result;
        }

        public IHttpResult Get_v1(Request.AppraisalPhotoList request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            string oldETag = this.RequestContext.GetHeader("If-None-Match");
            var command = factory.CreateGetAppraisalPhotosCommand();

            var arguments = new GetAppraisalPhotosArgumentsDto()
            {
                DealerID = request.Dealer,
                Source = request.Source,
                UniqueID = request.UniqueID
            };

            GetAppraisalPhotosResultsDto results = Helper.ExecuteCommand(command, Helper.WrapInContext(arguments, this.GetSession().UserAuthName));

            AppraisalPhotoListResponse response = new AppraisalPhotoListResponse()
            {
                appraisalPhotos = results.AppraisalPhotos,
                uniqueId = results.UniqueID
            };


            IHttpResult result = Handle304(oldETag, response, true);

            result.StatusCode = HttpStatusCode.OK;

            result = Helper.SetStaticHeaders(result, new[] { "cacheControlNoCache" });

            return result;
        }

        public IHttpResult Post_v1(Request.AppraisalPhotoDelete request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            var command = factory.CreateDeleteAppraisalPhoto();

            var arguments = new DeleteAppraisalPhotosArgumentsDto()
            {
                DealerID = request.Dealer,
                UniqueID = request.UniqueID,
                Source = request.Source,
                SequenceId = request.SequenceID
            };

            DeleteAppraisalPhotosResultsDto results = Helper.ExecuteCommand(command, Helper.WrapInContext(arguments, this.GetSession().UserAuthName));

            AppraisalPhotoDeleteResponse response = new AppraisalPhotoDeleteResponse()
            {
                AppraisalPhoto = results.DeletedPhotos,
                DeletedCount=results.DeletedCount
            };

            IHttpResult result = Helper.Compress(this, response);

            result.StatusCode = HttpStatusCode.OK;

            result = Helper.SetStaticHeaders(result, new[] { "cacheControlNoCache" });

            return result;
        }

        #region mapper methods


        #endregion

        IHttpResult Handle304(string oldETag, object response, bool compress)
        {
            string newETag = ETagHelper.CreateETagHash(response);

            IHttpResult result = new HttpResult();

            if (oldETag != null && oldETag.Equals(newETag))
            {
                //  ETags match so no need to zip and send the whole response - just headers
                result.StatusCode = HttpStatusCode.NotModified;
            }
            else
            {
                // ETag does not match so we need to send to updated resource
                // 
                if (compress)
                    result = Helper.Compress(this, response);
                else
                    result = new HttpResult
                    {
                        Response = response                       
                    };

                ETagHelper.AddETagHeader(result, response);

                result.StatusCode = HttpStatusCode.OK;
            }
            return result;
        }

        public string UniqueID { get; set; }
    }
}