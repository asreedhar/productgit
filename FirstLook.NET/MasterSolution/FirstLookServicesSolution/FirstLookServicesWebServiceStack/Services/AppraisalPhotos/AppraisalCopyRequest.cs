﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.AppraisalPhotos
{
    [DataContract]
    public class AppraisalCopyRequest : AppraisalPhotoRequestBase
    {        
        [DataMember(Name = "NewUniqueID")]
        public string NewUniqueID { get; set; }
     
    }
}