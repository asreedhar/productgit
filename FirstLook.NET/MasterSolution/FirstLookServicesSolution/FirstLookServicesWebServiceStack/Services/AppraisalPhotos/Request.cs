﻿using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.AppraisalPhotos
{
    public class Request
    {
        [Route("/{ver}/appraisalPhotos/dealers/{dealer}")]
        [Route("/appraisalPhotos/dealers/{dealer}")]
        public class AppraisalPhotos : AppraisalPhotoRequest
        {
            public int Dealer { get; set; }
            public string Ver { get; set; }
        }

        [Route("/{ver}/appraisalCopyPhotos/dealers/{dealer}")]
        [Route("/appraisalCopyPhotos/dealers/{dealer}")]
        public class AppraisalPhotosCopy:AppraisalCopyRequest
        {
            public int Dealer { get; set; }
            public string Ver { get; set; }           
        }

        [Route("/{ver}/appraisalsPhotos/dealers/{dealer}/uniqueId/{uniqueId}/source/{source}")]
        public class AppraisalPhotoList : AppraisalPhotoRequestBase
        {
            public int Dealer { get; set; }
            public string Ver { get; set; }           
        }

        [Route("/{ver}/appraisalsPhotosDelete/dealers/{dealer}")]
        public class AppraisalPhotoDelete : AppraisalPhotoRequestBase
        {
            public int Dealer { get; set; }
            public string Ver { get; set; }
            public int SequenceID { get; set; }
        }
        
    }


}