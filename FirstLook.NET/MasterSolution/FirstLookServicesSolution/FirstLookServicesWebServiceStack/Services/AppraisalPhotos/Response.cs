﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Model;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.AppraisalPhotos
{
    [DataContract]
    public class AppraisalPhotoResponse
    {
        [DataMember(Name = "wide127Url")]
        public string ThumbnailUrl { get; set; }

        [DataMember(Name = "photo")]
        public string PhotoUrl { get; set; }

        [DataMember(Name = "uniqueId")]
        public string UniqueId { get; set; }

        [DataMember(Name = "photoKey")]
        public string PhotoKey { get; set; }

        [DataMember(Name = "wide127Key")]
        public string ThumbnailKey { get; set; }
    }

    [DataContract]
    public class AppraisalPhotoCopyResponse
    {
        [DataMember(Name = "appraisalPhotos")]
        public List<AppraisalPhoto> AppraisalPhotos { get; set; }
        
    }

    [DataContract]
    public class AppraisalPhotoListResponse
    {
        [DataMember(Name = "appraisalPhotos")]
        public List<AppraisalPhoto> appraisalPhotos { get; set; }
        [DataMember(Name = "uniqueId")]
        public string uniqueId { get; set; }

    }

    [DataContract]
    public class AppraisalPhotoDeleteResponse
    {
        [DataMember(Name = "deletedPhotos")]
        public List<AppraisalPhoto> AppraisalPhoto { get; set; }

        [DataMember(Name = "filesAffected")]
        public int DeletedCount { get; set; }

    }
}