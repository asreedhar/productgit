﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.AppraisalPhotos
{
    [DataContract]
    public class AppraisalPhotoRequest : AppraisalPhotoRequestBase
    {
        [DataMember(Name = "Photo")]
        public string Photo { get; set; }
        
        [DataMember(Name = "SequenceID")]
        public int SequenceID { get; set; }

        [DataMember(Name = "FileName")]
        public string FileName { get; set; }

    }
}