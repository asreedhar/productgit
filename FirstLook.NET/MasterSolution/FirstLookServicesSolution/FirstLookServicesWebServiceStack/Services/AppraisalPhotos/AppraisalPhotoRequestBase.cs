﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.AppraisalPhotos
{
    [DataContract]
    public class AppraisalPhotoRequestBase
    {
        [DataMember(Name = "UniqueID")]
        public string UniqueID { get; set; }

        [DataMember(Name = "Source")]
        public int Source { get; set; }

    }
}