﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Inventory.Commands.Impl;
using FirstLook.FirstLookServices.DomainModel.Inventory.Commands;
using FirstLook.FirstLookServices.DomainModel.Inventory.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Inventory.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Inventory.Model;
using FirstLook.FirstLookServices.WebServiceStack.Services.Inventory.Models;
using FirstLook.FirstLookServices.WebServiceStack.Utility;
using ServiceStack.Common.Extensions;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;


namespace FirstLook.FirstLookServices.WebServiceStack.Services.Inventory
{
    [Authenticate]
    public class Service : ServiceStack.ServiceInterface.Service
    {
        private readonly VersionMapper<Request.Inventory> _agingInventoryMapper = new VersionMapper<Request.Inventory>("v1");

        public Service()
        {
            _agingInventoryMapper.Add("v1", "get", Get_v1);

        }
        public object Get(Request.Inventory request)
        {
            try
            {
                return _agingInventoryMapper.GetServiceFunction(request.Ver, "get")(request);
            }
            catch (Exception ex)
            {
                {
                    var err = this.ExceptionToError(ex, null);
                    return (err != null) ? this.HandleError(err) : null;
                }
            }

        }

        public object Get_v1(Request.Inventory request)
        {

            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            string oldETag = RequestContext.GetHeader("If-None-Match");
            var command = factory.CreateFetchAgingInventoryCommand();
            var arguments = new FetchAgingInventoryArgumentsDto()
                                {                                                       

                                    BusinessUnitId = request.Dealer,
                                    StockNumber=base.Request.QueryString["StockNumber"]
                                };

            FetchAgingInventoryResultsDto results = Helper.ExecuteCommand(command, Helper.WrapInContext(arguments, this.GetSession().UserAuthName));
           
            Response.AgingInventoryResponse response = null;

            if (results.AgingInventory != null && results.AgingInventory.FirstOrDefault() != null)
            {
                response = GetInventoryByBucket(results.AgingInventory);
            }

            IHttpResult result = Handle304(oldETag, response, true);

            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                result = Helper.SetStaticHeaders(result, new[] { "cacheControlHour" });
            }

            return result;

        }
        IHttpResult Handle304(string oldETag, object response, bool compress)
        {
            string newETag = ETagHelper.CreateETagHash(response);

            IHttpResult result = new HttpResult();

            if (oldETag != null && oldETag.Equals(newETag))
            {
                //  ETags match so no need to zip and send the whole response - just headers
                result.StatusCode = HttpStatusCode.NotModified;
            }
            else
            {
                // ETag does not match so we need to send to updated resource
                // 
                if (compress)
                    result = Helper.Compress(this, response);
                else
                    result = new HttpResult
                    {
                        Response = response
                    };

                ETagHelper.AddETagHeader(result, response);

                result.StatusCode = HttpStatusCode.OK;
            }
            return result;
        }
        private IList<string> GetBucketLocationRequest(IList<AgingInventoryDto> objs)
        {
            IList<string> bucketsList =
                objs.Select(obj => obj.BucketName).Distinct().ToList();
            return bucketsList;
        }

        private  Response.AgingInventoryResponse GetInventoryByBucket(IList<AgingInventoryDto> objs)
        {
            List<Bucket> buckets= new List<Bucket>();
            IList<string> bucketNames = GetBucketLocationRequest(objs);
            foreach (string str in bucketNames)
            {
                IList<AgingInventoryDto> agingInventoryDto = objs.Where(obj => obj.BucketName.ToUpper() == str.ToUpper()).ToList();
                List<DealerAgingInventory> bucketInventories =
                    agingInventoryDto.Select(aid => new DealerAgingInventory()
                    {
                        AgeInDays = aid.AgeInDays,
                        Color = aid.Color,
                        RiskLight = aid.RiskLight,
                        ListPrice = aid.ListPrice,
                        MakeModel = aid.MakeModel,
                        Trim=aid.Trim,
                        Mileage = aid.Mileage,
                        Vin = aid.Vin,
                        Year = aid.Year,
                        UnitCost=aid.UnitCost,
                        InventoryId=aid.InventoryId,//newly added
                        StockNumber = aid.StockNumber
                    }).ToList();
                buckets.Add(new Bucket(){BucketName = str,DealerAgingInventory=bucketInventories});
            }
            return  new Response.AgingInventoryResponse(){ Buckets = buckets};

        }

    }
}