﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Inventory
{
    public class Request
    {
        [Route("/{ver}/inventory/dealers/{dealer}/{stockNumber}")]
        [Route("/inventory/dealers/{dealer}/{stockNumber}")]
        [Route("/{ver}/inventory/dealers/{dealer}")]
        [Route("/inventory/dealers/{dealer}")]
        public class Inventory
        {
            public int Dealer { get; set; }
            public string StockNumber { get; set; }
            public string Ver { get; set; }
        }
    }
}