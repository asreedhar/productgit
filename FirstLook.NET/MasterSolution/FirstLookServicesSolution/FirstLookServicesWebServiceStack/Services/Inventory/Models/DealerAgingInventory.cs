﻿using System.Runtime.Serialization;


namespace FirstLook.FirstLookServices.WebServiceStack.Services.Inventory.Models
{
    
        [DataContract]
    public class DealerAgingInventory
        {

            [DataMember(Name = "inventoryId")]
            public int InventoryId { get; set; }
            [DataMember(Name = "vin")]
            public string Vin { get; set; }
             [DataMember(Name = "year")]
            public int Year { get; set; }
             [DataMember(Name = "makeModel")]
             public string MakeModel { get; set; }
             [DataMember(Name = "trim")]
             public string Trim { get; set; }
             [DataMember(Name = "ageInDays")]
            public int AgeInDays { get; set; }
             [DataMember(Name = "color")]
            public string Color { get; set; }
             [DataMember(Name = "mileage")]
            public int Mileage { get; set; }
             [DataMember(Name = "listPrice")]
            public decimal ListPrice { get; set; }
             [DataMember(Name = "riskLight")]
            public string RiskLight { get; set; }
             [DataMember(Name = "unitCost")]
             public decimal UnitCost { get; set; }
             [DataMember(Name = "stockNumber")]
             public string StockNumber { get; set; }
        
        }
}