﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Inventory.Models
{
    [DataContract]
    public class Bucket
    {
        [DataMember(Name="bucketName")]
        public string BucketName  { get; set; }
        [DataMember(Name="bucketInventory")]
        public List<DealerAgingInventory> DealerAgingInventory { get; set; }
    }
}