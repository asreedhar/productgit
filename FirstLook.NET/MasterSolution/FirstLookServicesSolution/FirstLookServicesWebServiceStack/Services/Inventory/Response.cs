﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using FirstLook.FirstLookServices.WebServiceStack.Services.Inventory.Models;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Inventory
{
    public class Response
    {
        [DataContract]
        public class AgingInventoryResponse
        {
            [DataMember(Name = "inventory")]
            //public List<DealerAgingInventory> DealerAgingInventory { get; set; }
            public List<Bucket> Buckets { get; set; }
        }
    }
}