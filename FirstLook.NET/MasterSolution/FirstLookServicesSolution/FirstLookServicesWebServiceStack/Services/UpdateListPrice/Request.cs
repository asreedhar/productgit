﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.UpdateListPrice
{
    public class Request
    {
        [Route("/{ver}/vehicles/dealers/{dealer}/inventory/{inventoryId}")]
        [Route("/vehicles/dealers/{dealer}/inventory/{inventoryId}")]
        public class UpdateListPrice
        {
            public int Dealer { get; set; }
            public string Ver { get; set; }
            public int InventoryId { get; set; }
            public decimal NewListPrice { get; set; }
            public string UserName { get; set; }
        }
    }
}