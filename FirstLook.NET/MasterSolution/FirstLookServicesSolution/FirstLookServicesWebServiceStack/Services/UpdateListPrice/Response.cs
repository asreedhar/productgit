﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.AccessControl;
using System.Web;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.UpdateListPrice
{
    public class Response
    {
        [DataContract]
        public class InventoryResponse
        {
            [DataMember(Name = "inventoryId")]
            public int InventoryId { get; set; }

            [DataMember(Name = "oldListprice")]
            public decimal OldListPrice { get; set; }

            [DataMember(Name = "newListPrice")]
            public decimal NewListPrice { get; set; }
        }
    }
    
}