﻿using System;
using System.Net;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Commands;
using FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.WebServiceStack.Utility;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.UpdateListPrice
{
    [Authenticate]
    public class Service : ServiceStack.ServiceInterface.Service
    {
        private readonly VersionMapper<Request.UpdateListPrice> _inventoryMapper = new VersionMapper<Request.UpdateListPrice>("v1");

        public Service()
        {
            _inventoryMapper.Add("v1", "post", Post_v1);

        }
        public object Post(Request.UpdateListPrice request)
        {
            try
            {
                return _inventoryMapper.GetServiceFunction(request.Ver,"post")(request);
            }
            catch (Exception ex)
            {
                {
                    var err = this.ExceptionToError(ex, null);
                    return (err != null) ? this.HandleError(err) : null;
                }
            }

        }

        public object Post_v1(Request.UpdateListPrice request)
        {

            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            string oldETag = RequestContext.GetHeader("If-None-Match");
            var command = factory.CreateUpdateListPriceCommand();
            var arguments = new UpdateListPriceArgumentsDto()
            {
                UserName = this.GetSession().UserAuthName,
                BusinessUnitId = request.Dealer,
                NewListPrice = request.NewListPrice,
                InventoryId = request.InventoryId
            };

            if (!string.IsNullOrEmpty(request.UserName))
            {
                arguments.UserName = request.UserName;
            }

            UpdateListPriceResultsDto results = Helper.ExecuteCommand(command, Helper.WrapInContext
                                                                                    (arguments, this.GetSession().UserAuthName));
            
            Response.InventoryResponse response = null;

            if (results != null)
            {
                response = new Response.InventoryResponse()
                {
                    InventoryId = results.ListPriceUpdate.InventoryId,
                    OldListPrice = results.ListPriceUpdate.OldListPrice,
                    NewListPrice = results.ListPriceUpdate.NewListPrice
                };
            }
            
            IHttpResult result = Helper.Compress(this, response);

            result = Helper.SetStaticHeaders(result, new[] { "cacheControlNoCache" });

            return result;

        }
        IHttpResult Handle304(string oldETag, object response, bool compress)
        {
            string newETag = ETagHelper.CreateETagHash(response);

            IHttpResult result = new HttpResult();

            if (oldETag != null && oldETag.Equals(newETag))
            {
                //  ETags match so no need to zip and send the whole response - just headers
                result.StatusCode = HttpStatusCode.NotModified;
            }
            else
            {
                // ETag does not match so we need to send to updated resource
                // 
                if (compress)
                    result = Helper.Compress(this, response);
                else
                    result = new HttpResult
                    {
                        Response = response
                    };

                ETagHelper.AddETagHeader(result, response);

                result.StatusCode = HttpStatusCode.OK;
            }
            return result;
        }
    }

}