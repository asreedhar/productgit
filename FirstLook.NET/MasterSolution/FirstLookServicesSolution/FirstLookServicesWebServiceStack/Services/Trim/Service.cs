﻿using System;
using System.Net;
using FirstLook.Common.Core.Registry;


using FirstLook.FirstLookServices.DomainModel.Trim.Commands;
using FirstLook.FirstLookServices.DomainModel.Trim.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Trim.Commands.TransferObjects.Envelopes;

using FirstLook.FirstLookServices.WebServiceStack.Utility;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;




namespace FirstLook.FirstLookServices.WebServiceStack.Services.Trim
{
    [Authenticate]
    public class Service : ServiceStack.ServiceInterface.Service
    {
        private readonly VersionMapper<Request.Trim> _trimInfoMapper = new VersionMapper<Request.Trim>("v1");

        public Service()
        {
            _trimInfoMapper.Add("v1", "post", Post_v1);

        }

        public object Post(Request.Trim request)
        {
            try
            {
                return _trimInfoMapper.GetServiceFunction(request.Ver, "post")(request);
            }
            catch (Exception ex)
            {
                {
                    var err = this.ExceptionToError(ex, null);
                    return (err != null) ? this.HandleError(err) : null;
                }
            }

        }

        public object Post_v1(Request.Trim request)
        {

            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            string oldETag = RequestContext.GetHeader("If-None-Match");
            var command = factory.CreateInsertTrimDetailsCommand();
            var arguments = new InsertTrimArgumentsDto
            {
                InventoryId = request.Inventory,
                DealerId = request.Dealers,
                ChromeStyleID = request.ChromeStyleId
            };

            InsertTrimDetailsResultsDto results = Helper.ExecuteCommand(command, Helper.WrapInContext(arguments, this.GetSession().UserAuthName));

            Response.TrimInfo response = null;

            if (results.TrimData != null)
            {
                response = InsertTrimInfo(results.TrimData);
            }

            IHttpResult result = Helper.Compress(this, response);

            result = Helper.SetStaticHeaders(result, new[] { "cacheControlNoCache" });

            return result;
        }
    
        private Response.TrimInfo InsertTrimInfo(TrimDataDto objs)
        {
            Trim.Models.TrimDetails trimDet = new Trim.Models.TrimDetails();
            trimDet.ChromeStyleId = objs.ChromeStyleId;
            trimDet.Flag = objs.Flag;

            return new Response.TrimInfo() { ChromeStyleId = trimDet.ChromeStyleId,Flag = trimDet.Flag};

        }
    }
}