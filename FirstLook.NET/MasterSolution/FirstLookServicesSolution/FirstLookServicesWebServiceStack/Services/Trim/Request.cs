﻿using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Trim
{
    public class Request
    {
        [Route("/{ver}/trim/dealers/{dealers}/inventory/{inventory}")]
        [Route("/trim/{dealerId}/{inventoryId}")]
        public class Trim
        {
            public int Inventory { get; set; }
            public int Dealers { get; set; }
            public string Ver { get; set; }
            public int ChromeStyleId { get; set; }
        }
    }
}