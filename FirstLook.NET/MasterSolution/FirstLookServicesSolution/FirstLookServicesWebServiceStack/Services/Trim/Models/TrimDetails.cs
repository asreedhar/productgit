﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Trim.Models
{
    [DataContract]
    public class TrimDetails
    {
        [DataMember(Name = "ChromeStyleId")]
        public int ChromeStyleId { get; set; }
        [DataMember(Name = "flag")]
        public bool Flag { get; set; }

    }
}