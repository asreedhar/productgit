﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Trim
{
    
    public class Response
    {
        [DataContract]
        public class TrimInfo
        {
            [DataMember(Name = "ChromeStyleId")]
            public int ChromeStyleId { get; set; }
            [DataMember(Name = "flag")]
            public bool Flag { get; set; }
        }

    }
}