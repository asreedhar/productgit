﻿using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.ApproveAd
{
    
    public class Request
    {
        [Route("/{ver}/approveAd/dealers/{dealer}/inventory/{inventory}")]
        [Route("/approveAd/dealers/{dealer}/inventory/{inventory}")]
        public class ApproveAd
        {
            public decimal OldPrice { get; set; }
            public decimal NewPrice { get; set; }
            public string Ver { get; set; }
            public int Dealer { get; set; }
            public int Inventory { get; set; }
            public decimal? SpecialPrice { get; set; }
            public string UserName { get; set; }

        }
    }
}