﻿using System;
using System.Net;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.ApproveAd.Commands;
using FirstLook.FirstLookServices.DomainModel.ApproveAd.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.WebServiceStack.Utility;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.ApproveAd
{
    [Authenticate]
    public class Service : ServiceStack.ServiceInterface.Service
    {

        private readonly VersionMapper<Request.ApproveAd> _ApproveAd = new VersionMapper<Request.ApproveAd>("v1");

        public Service()
        {
            _ApproveAd.Add("v1", "post", post_v1);

        }

        public object Post(Request.ApproveAd request)
        {
            try
            {
                return _ApproveAd.GetServiceFunction(request.Ver, "post")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }
        public object Get(Request.ApproveAd request)
        {
            try
            {
                return _ApproveAd.GetServiceFunction(request.Ver, "post")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }
        public IHttpResult post_v1(Request.ApproveAd request)
        {
            
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            var internetPriceCommand = factory.CreateUpdateListPriceCommand();

            var changeInternetPriceArguments = new UpdateListPriceArgumentsDto()
            {
                UserName = this.GetSession().UserAuthName,
                BusinessUnitId = request.Dealer,
                NewListPrice = request.NewPrice,
                InventoryId = request.Inventory
            };

            if (!string.IsNullOrEmpty(request.UserName))
            {
                changeInternetPriceArguments.UserName = request.UserName;
            }

            UpdateListPriceResultsDto changeInternetPriceResults = Helper.ExecuteCommand(internetPriceCommand, Helper.WrapInContext(changeInternetPriceArguments, this.GetSession().UserAuthName));

            var createApprovalAdCommand = factory.CreateApprovalAdCommand();

            var approveAdArguments = new FetchApproveAdArgumentsDto()
            {
                Businessunitid = request.Dealer,
                FlListPrice= request.NewPrice,
                SpecialPrice= request.SpecialPrice.Value,
                InventoryId = request.Inventory
            };

            FetchApproveAdResultsDto approveAdresults = Helper.ExecuteCommand(createApprovalAdCommand, Helper.WrapInContext
                                                                                    (approveAdArguments, this.GetSession().UserAuthName));
            Response.ApproveAdResponse response = new Response.ApproveAdResponse();
            response.ChangeInternetPriceResults = changeInternetPriceResults;
            response.AdApproved = approveAdresults.Result;

            IHttpResult result = Helper.Compress(this, response);
            
            result.StatusCode=HttpStatusCode.Created;

            result = Helper.SetStaticHeaders(result, new[]{ "cacheControlNoCache" });

            return result;
                    

        }

    }
}