﻿using System.Runtime.Serialization;
using FirstLook.FirstLookServices.DomainModel.UpdateListPrice.Commands.TransferObjects.Envelopes;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.ApproveAd
{
    public class Response
    {
        [DataContract]
        public class ApproveAdResponse
        {
            [DataMember]
            public bool  AdApproved { get; set; }

            [DataMember]
            public UpdateListPriceResultsDto ChangeInternetPriceResults { get; set; }
        }
    }
}