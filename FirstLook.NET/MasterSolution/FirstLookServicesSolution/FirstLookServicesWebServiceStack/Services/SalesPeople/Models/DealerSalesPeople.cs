﻿using System.Runtime.Serialization;


namespace FirstLook.FirstLookServices.WebServiceStack.Services.SalesPeople.Models
{
    [DataContract]
    public class DealerSalesPeople
    {
        [DataMember(Name = "id")]
        public int PersonId { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

    }
}