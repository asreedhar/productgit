﻿using System;
using System.Linq;
using System.Net;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.SalesPeople.Commands;
using FirstLook.FirstLookServices.DomainModel.SalesPeople.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.WebServiceStack.Services.SalesPeople.Models;
using FirstLook.FirstLookServices.WebServiceStack.Utility;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.SalesPeople
{
    [Authenticate]
    public class Service:ServiceStack.ServiceInterface.Service
    {
        private readonly VersionMapper<Request.SalesPeople> _salesPeopleMapper = new VersionMapper<Request.SalesPeople>("v1");

        public Service()
        {
            _salesPeopleMapper.Add("v1","get",Get_v1);

        }
        public object Get(Request.SalesPeople request)
        {
            try
            {
                return _salesPeopleMapper.GetServiceFunction(request.Ver,"get")(request);
            }
            catch (Exception ex)
            {
                {
                    var err = this.ExceptionToError(ex, null);
                    return (err != null) ? this.HandleError(err) : null;
                }
            }

        }

        public object Get_v1(Request.SalesPeople request)
        {

            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            string oldETag = RequestContext.GetHeader("If-None-Match");
            var command = factory.CreateFetchSalesPeopleCommand();
            var arguments = new FetchSalesPeopleArgumentsDto()
                                {

                                    BusinessUnitId = request.Dealer
                                };

            FetchSalesPeopleResultsDto results = Helper.ExecuteCommand(command, Helper.WrapInContext
                                                                                    (arguments,
                                                                                    this.GetSession().UserAuthName));
            Response.SalesPeopleResponse response = null;

            if (results.SalesPeople != null && results.SalesPeople.FirstOrDefault() != null)
            {
                response = new Response.SalesPeopleResponse()
                                   {
                                       DealerSalesPeoples =
                                           results.SalesPeople.Select(salespeople => new DealerSalesPeople
                                                                                         {
                                                                                             PersonId =
                                                                                                 salespeople.PersonId,
                                                                                             Name = salespeople.Name
                                                                                         }

                                           ).ToList()

                                   };
            }

            IHttpResult result = Handle304(oldETag, response, true);

            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                result = Helper.SetStaticHeaders(result, new[] { "cacheControlHour" });
            }

            return result;

        }
        IHttpResult Handle304(string oldETag, object response, bool compress)
        {
            string newETag = ETagHelper.CreateETagHash(response);

            IHttpResult result = new HttpResult();

            if (oldETag != null && oldETag.Equals(newETag))
            {
                //  ETags match so no need to zip and send the whole response - just headers
                result.StatusCode = HttpStatusCode.NotModified;
            }
            else
            {
                // ETag does not match so we need to send to updated resource
                // 
                if (compress)
                    result = Helper.Compress(this, response);
                else
                    result = new HttpResult
                    {
                        Response = response
                    };

                ETagHelper.AddETagHeader(result, response);

                result.StatusCode = HttpStatusCode.OK;
            }
            return result;
        }
    }
}