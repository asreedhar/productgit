﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using FirstLook.FirstLookServices.WebServiceStack.Services.SalesPeople.Models;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.SalesPeople
{
    public class Response
    {
        [DataContract]
        public class SalesPeopleResponse
        {
             [DataMember(Name = "salespeople")]
            public List<DealerSalesPeople> DealerSalesPeoples { get; set; }
        }
    }
}