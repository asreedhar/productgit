﻿using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.SalesPeople
{
    public class Request
    {
        [Route("/{ver}/salespeople/dealers/{dealer}")]
        [Route("/salespeople/dealers/{dealer}")]
        public class SalesPeople
        {
            public int Dealer { get; set;}
            public string Ver { get; set; }
        }
    }
}