﻿
using FirstLook.FirstLookServices.DomainModel.SearchCriteria.Model;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.SearchCriteria
{
    public class Response
    {
        public string Key { get; set; }

        public SearchCriteriaDto Criteria { get; set; }
    }
}