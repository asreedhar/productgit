﻿using FirstLook.FirstLookServices.DomainModel.SearchCriteria.Model;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.SearchCriteria
{
    public class CriteriaResponse
    {
        public string Key { get; set; }

        public SearchCriteriaDto Criteria{get; set;}
    }
}