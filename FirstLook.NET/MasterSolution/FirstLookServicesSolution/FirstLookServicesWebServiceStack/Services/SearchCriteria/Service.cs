﻿using System;
using System.Net;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.SearchCriteria.Commands;
using FirstLook.FirstLookServices.DomainModel.SearchCriteria.Commands.TransferObjects;
using FirstLook.FirstLookServices.WebServiceStack.Utility;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.SearchCriteria
{
    [Authenticate]
    public class Service : ServiceStack.ServiceInterface.Service
    {
        readonly VersionMapper<Request.PostSearchCriteriaRequest> _searchCriteriaMapper = new VersionMapper<Request.PostSearchCriteriaRequest>("v1");
        readonly VersionMapper<Request.CriteriaRequest> _criteriaMapper = new VersionMapper<Request.CriteriaRequest>("v1");

        public Service()
        {
            _searchCriteriaMapper.Add("v1", "post", Post_v1);
            _criteriaMapper.Add("v1", "get", Get_v1);

        }
        public object Post(Request.PostSearchCriteriaRequest request)
        {
            try
            {

                return _searchCriteriaMapper.GetServiceFunction(request.Ver, "post")(request);

            }
            catch (Exception ex)
            {
                {
                    var err = this.ExceptionToError(ex, null);
                    return (err != null) ? this.HandleError(err) : null;
                }
            }
        }

        public object Get(Request.CriteriaRequest request)
        {
            try
            {

                return _criteriaMapper.GetServiceFunction(request.Ver, "get")(request);

            }
            catch (Exception ex)
            {
                {
                    var err = this.ExceptionToError(ex, null);
                    return (err != null) ? this.HandleError(err) : null;
                }
            }
        }
        public object Post_v1(Request.PostSearchCriteriaRequest req)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            var command = factory.CreateSearchCriteriaCommand();

            var arguments = new FetchSearchCriteriaArgumentsDto()
            {
                AvgMileage = req.AvgMileage,
                AvgPrice = req.AvgPrice,
                DealerId = req.DealerId,
                Certified = req.Certified,
                NotCertified=req.NotCertified,
                Distance = req.Distance,
                DriveTrian = req.DriveTrian,
                Engine = req.Engine,
                Fuel = req.Fuel,
                Facet = req.Facet,
                InventoryId = req.InventoryId,
                Listings = req.Listings,
                MaxMileage = req.MaxMileage,
                MaxPrice = req.MaxPrice,
                MinMileage = req.MinMileage,
                MinPrice = req.MinPrice,
                OverallCount = req.OverallCount,
                PrecisionCount = req.PrecisionCount,
                Transmission = req.Transmission,
                Trim = req.Trim,
                Vin = req.Vin

            };
            
            FetchSearchCriteriaResultsDto results = Helper.ExecuteCommand(command, Helper.WrapInContext(arguments, this.GetSession().UserAuthName));

            var response = results;

            IHttpResult result = Helper.Compress(this, response);

            result.StatusCode = HttpStatusCode.Created;

            result = Helper.SetStaticHeaders(result, new[] { "cacheControlNoCache" });

            return result;
        }

        public IHttpResult Get_v1(Request.CriteriaRequest request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            string oldETag = this.RequestContext.GetHeader("If-None-Match");

            var command = factory.GetCriteriaFromAmazomCommand();
            var arguments = new FetchCriteriaArgumentsDto()
            {
                DealerId=request.DealerId,
                InventoryId=request.InventoryId,
                Vin=request.Vin

            };
            var results = Helper.ExecuteCommand(command, Helper.WrapInContext(arguments, this.GetSession().UserAuthName));
            CriteriaResponse response = new CriteriaResponse()
            {
                Key = results.Key,
                Criteria = new DomainModel.SearchCriteria.Model.SearchCriteriaDto() { 
                    AvgMileage=results.Criteria.AvgMileage,
                    AvgPrice=results.Criteria.AvgPrice,
                    Certified=results.Criteria.Certified,
                    DealerId=results.Criteria.DealerId,
                    Distance=results.Criteria.Distance,
                    DriveTrian=results.Criteria.DriveTrian,
                    Engine=results.Criteria.Engine,
                    Facet=results.Criteria.Facet,
                    Fuel=results.Criteria.Fuel,
                    InventoryId=results.Criteria.InventoryId,
                    Listings=results.Criteria.Listings,
                    MaxMileage=results.Criteria.MaxMileage,
                    MaxPrice=results.Criteria.MaxPrice,
                    MinMileage=results.Criteria.MinMileage,
                    MinPrice=results.Criteria.MinPrice,
                    NotCertified=results.Criteria.NotCertified,
                    OverallCount=results.Criteria.OverallCount,
                    PrecisionCount=results.Criteria.PrecisionCount,
                    Transmission=results.Criteria.Transmission,
                    Trim=results.Criteria.Trim,
                    Vin=results.Criteria.Vin
                }
            };
            
            IHttpResult result = Handle304(oldETag, response, true);

            // ETag does not match so we need to send to updated resource
            Helper.SetStaticHeaders(result, new string[] { "cacheControlHour" });

            return result;
        }

        IHttpResult Handle304(string oldETag, object response, bool compress)
        {
            string newETag = ETagHelper.CreateETagHash(response);

            IHttpResult result = new HttpResult();

            if (oldETag != null && oldETag.Equals(newETag))
            {
                //  ETags match so no need to zip and send the whole response - just headers
                result.StatusCode = HttpStatusCode.NotModified;
            }
            else
            {
                // ETag does not match so we need to send to updated resource
                // 
                if (compress)
                    result = Helper.Compress(this, response);
                else
                    result = new HttpResult
                    {
                        Response = response
                    };

                ETagHelper.AddETagHeader(result, response);

                result.StatusCode = HttpStatusCode.OK;
            }
            return result;
        }
          
    }
}