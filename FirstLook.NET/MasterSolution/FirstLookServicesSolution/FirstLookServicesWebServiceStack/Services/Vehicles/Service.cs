﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Commands;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.WebServiceStack.Services.Vehicles.Models;
using FirstLook.FirstLookServices.WebServiceStack.Utility;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;
using ServiceStack.Common.Web;
using System;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Vehicles
{

    [Authenticate]
    public class Service : ServiceStack.ServiceInterface.Service
    {
        readonly VersionMapper<VehicleHierarchy> _vehicleHierarchyMapper = new VersionMapper<VehicleHierarchy>("v1");

        readonly VersionMapper<VehicleHierarchyYears> _vehicleHierarchyYearsMapper = new VersionMapper<VehicleHierarchyYears>("v1");

        readonly VersionMapper<VehicleHierarchyMakes> _vehicleHierarchyMakesMapper = new VersionMapper<VehicleHierarchyMakes>("v1");

        readonly VersionMapper<VehicleHierarchyModels> _vehicleHierarchyModelsMapper = new VersionMapper<VehicleHierarchyModels>("v1");

        readonly VersionMapper<VehicleHierarchyStyles> _vehicleHierarchyStylesMapper = new VersionMapper<VehicleHierarchyStyles>("v1");

        public Service()
        {
            _vehicleHierarchyMapper.Add("v1", "get", Get_v1);
            _vehicleHierarchyYearsMapper.Add("v1", "get", Get_v1);
            _vehicleHierarchyMakesMapper.Add("v1", "get", Get_v1);
            _vehicleHierarchyModelsMapper.Add("v1", "get", Get_v1);
            _vehicleHierarchyStylesMapper.Add("v1", "get", Get_v1);

        }

        #region vehicle hierarchy

        public object Get(VehicleHierarchy request)
        {
            try
            {
                return _vehicleHierarchyMapper.GetServiceFunction(request.Ver, "get")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public IHttpResult Get_v1(VehicleHierarchy request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            string oldETag = RequestContext.GetHeader("If-None-Match");

            var command = factory.CreateFetchVehicleHierarchyCommand();

            var arguments = new FetchVehicleHierarchyArgumentsDto();

            FetchVehicleHierarchyResultsDto results = Helper.ExecuteCommand(command, arguments);

            var response = new VehicleHierarchyResponse();

            IList<IList<string>> hierarchies =
                results.VehicleHierarchies.Select(hierarchy => new List<string>
                                                    {
                                                        hierarchy.Year.ToString(),
                                                        hierarchy.Make,
                                                        hierarchy.Model,
                                                        hierarchy.Style,
                                                        hierarchy.StyleId.ToString()
                                                    }).Cast<IList<string>>().ToList();

            response.Columns = new List<string> { "year", "make", "model", "styleDesc", "style" };

            response.Data = hierarchies;

            IHttpResult result = Handle304(oldETag, response, true);

            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                result = Helper.SetStaticHeaders(result, new[] { "referenceDataTrue", "cacheControlDay" });
            }

            return result;
        }

        #endregion

        #region years

        public object Get(VehicleHierarchyYears request)
        {
            try
            {
                return _vehicleHierarchyYearsMapper.GetServiceFunction(request.Ver, "get")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public IHttpResult Get_v1(VehicleHierarchyYears request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            string oldETag = RequestContext.GetHeader("If-None-Match");

            var command = factory.CreateFetchYearsCommand();

            var arguments = new FetchYearsArgumentsDto();

            FetchYearsResultsDto results = Helper.ExecuteCommand(command, arguments);

            var response = new VehicleHierarchyYearsResponse
                               {
                                   Years = results.Years
                               };

            IHttpResult result = Handle304(oldETag, response, true);

            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                result = Helper.SetStaticHeaders(result, new[] { "referenceDataTrue", "cacheControlDay" });
            }

            return result;
        }

#endregion

        #region makes

        public object Get(VehicleHierarchyMakes request)
        {
            try
            {
                return _vehicleHierarchyMakesMapper.GetServiceFunction(request.Ver, "get")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public IHttpResult Get_v1(VehicleHierarchyMakes request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            string oldETag = RequestContext.GetHeader("If-None-Match");

            var command = factory.CreateFetchMakesCommand();

            var arguments = new FetchMakesArgumentsDto
                                {
                                    Year = request.Year
                                };

            FetchMakesResultsDto results = Helper.ExecuteCommand(command, arguments);

            var response = new VehicleHierarchyMakesResponse
                               {
                                   Makes = results.Makes.Select(Map).ToList()
                               };

            IHttpResult result = Handle304(oldETag, response, true);

            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                result = Helper.SetStaticHeaders(result, new[] { "referenceDataTrue", "cacheControlDay" });
            }

            return result;
        }

        #endregion

        #region models

        public object Get(VehicleHierarchyModels request)
        {
            try
            {
                return _vehicleHierarchyModelsMapper.GetServiceFunction(request.Ver, "get")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public IHttpResult Get_v1(VehicleHierarchyModels request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            string oldETag = RequestContext.GetHeader("If-None-Match");

            var command = factory.CreateFetchModelsCommand();

            var arguments = new FetchModelsArgumentsDto
                                {
                                    Year = request.Year,
                                    MakeId = request.MakeId
                                };

            FetchModelsResultsDto results = Helper.ExecuteCommand(command, arguments);

            var response = new VehicleHierarchyModelsResponse
                               {
                                   Models = results.Models.Select(Map).ToList()
                               };

            IHttpResult result = Handle304(oldETag, response, true);

            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                result = Helper.SetStaticHeaders(result, new[] { "referenceDataTrue", "cacheControlDay" });
            }

            return result;
        }

        #endregion

        #region styles

        public object Get(VehicleHierarchyStyles request)
        {
            try
            {
                return _vehicleHierarchyStylesMapper.GetServiceFunction(request.Ver, "get")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public IHttpResult Get_v1(VehicleHierarchyStyles request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            string oldETag = RequestContext.GetHeader("If-None-Match");

            var command = factory.CreateFetchStylesCommand();

            var arguments = new FetchStylesArgumentsDto
                                {
                                    Year = request.Year,
                                    MakeId = request.MakeId,
                                    ModelId = request.ModelId
                                };

            FetchStylesResultsDto results = Helper.ExecuteCommand(command, arguments);

            var response = new VehicleHierarchyStylesResponse()
            {
                Styles = results.Styles.Select(Map).ToList()
            };

            IHttpResult result = Handle304(oldETag, response, true);

            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                result = Helper.SetStaticHeaders(result, new[] { "referenceDataTrue", "cacheControlDay" });
            }

            return result;
        }

        #endregion

        public static Make Map(MakeDto dto)
        {
            if (dto == null)
            {
                return null;
            }

            return new Make
                       {
                           Description = dto.Desc,
                           Id = dto.Id
                       };
        }

        public static Model Map(ModelDto dto)
        {
            if (dto == null)
            {
                return null;
            }

            return new Model
                       {
                           Description = dto.Desc,
                           Id = dto.Id
                       };
        }

        public static Style Map(StyleDto dto)
        {
            if (dto == null)
            {
                return null;
            }

            return new Style
                       {
                           Description = dto.Desc,
                           Id = dto.Id
                       };
        }

        IHttpResult Handle304(string oldETag, object response, bool compress)
        {
            string newETag = ETagHelper.CreateETagHash(response);

            IHttpResult result = new HttpResult();

            if (oldETag != null && oldETag.Equals(newETag))
            {
                //  ETags match so no need to zip and send the whole response - just headers
                result.StatusCode = HttpStatusCode.NotModified;
            }
            else
            {
                // ETag does not match so we need to send to updated resource
                if (compress)
                    result = Helper.Compress(this, response);
                else
                    result = new HttpResult
                    {
                        Response = response
                    };

                ETagHelper.AddETagHeader(result, response);

                result.StatusCode = HttpStatusCode.OK;
            }
            return result;
        }

    }
}