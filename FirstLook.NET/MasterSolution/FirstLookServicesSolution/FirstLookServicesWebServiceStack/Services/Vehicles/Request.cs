﻿using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Vehicles
{
    [Route("/{ver}/vehicles/hierarchy")]
    [Route("/vehicles/hierarchy")]
    public class VehicleHierarchy
    {
        public string Ver { get; set; }
    }

    [Route("/{ver}/vehicles/hierarchy/years")]
    [Route("/vehicles/hierarchy/years")]
    public class VehicleHierarchyYears
    {
        public string Ver { get; set; }
    }


    [Route("/{ver}/vehicles/hierarchy/makes/years/{year}")]
    [Route("/vehicles/hierarchy/makes/years/{year}")]
    public class VehicleHierarchyMakes
    {
        public string Ver { get; set; }

        public int Year { get; set; }

    }

    [Route("/{ver}/vehicles/hierarchy/models/years/{year}/makes/{makeId}")]
    [Route("/vehicles/hierarchy/models/years/{year}/makes/{makeId}")]
    public class VehicleHierarchyModels
    {
        public string Ver { get; set; }

        public int MakeId { get; set; }

        public int Year { get; set; }
    }

    [Route("/{ver}/vehicles/hierarchy/styles/years/{year}/makes/{makeId}/models/{modelId}")]
    [Route("/vehicles/hierarchy/styles/years/{year}/makes/{makeId}/models/{modelId}")]
    public class VehicleHierarchyStyles
    {
        public string Ver { get; set; }

        public int Year { get; set; }

        public int MakeId { get; set; }

        public int ModelId { get; set; }

    }
}