﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using FirstLook.FirstLookServices.WebServiceStack.Services.Vehicles.Models;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Vehicles
{
    [DataContract]
    public class VehicleHierarchyResponse
    {
        [DataMember(Name = "columns")]
        public IList<string> Columns { get; set; }
                                                                         
        [DataMember(Name = "data")]
        public IList<IList<string>> Data { get; set; }
    }

    [DataContract]
    public class VehicleHierarchyYearsResponse
    {
        [DataMember(Name = "years")]
        public IList<int> Years { get; set; }
    }

    [DataContract]
    public class VehicleHierarchyMakesResponse
    {
        [DataMember(Name = "makes")]
        public IList<Make> Makes { get; set; }
    }

    [DataContract]
    public class VehicleHierarchyModelsResponse
    {
        [DataMember(Name = "models")]
        public IList<Model> Models { get; set; }
    }

    [DataContract]
    public class VehicleHierarchyStylesResponse
    {
        [DataMember(Name = "styles")]
        public IList<Style> Styles { get; set; }
    }

}