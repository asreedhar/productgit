﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Vehicles.Models
{
    [DataContract]
    public class Make
    {
        [DataMember(Name = "make")]
        public int Id { get; internal set; }

        [DataMember(Name = "desc")]
        public string Description { get; internal set; }
    }
}