﻿using System.Runtime.Serialization;
using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Model;



namespace FirstLook.FirstLookServices.WebServiceStack.Services.InventoryDetails
{
    public class Response
    {
        [DataContract]
        public class InventoryInfo
        {
            [DataMember(Name = "inventory")]
            public Models.InventoryDetails Inventory { get; set; }

            [DataMember(Name = "trims")]
            public IList<TrimInfo> TrimList { get; set; }

        }

    }
}