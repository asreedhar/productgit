﻿using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.InventoryDetails
{
    public class Request
    {
        [Route("/{ver}/pinginventory/{dealerid}/{inventoryid}")]
        [Route("/pinginventory/{dealerid}/{inventoryid}")]
        
        public class Inventory
        {
            public int InventoryId { get; set; }
            public int DealerId { get; set; }
            public string Ver { get; set; }
        }
    }
}