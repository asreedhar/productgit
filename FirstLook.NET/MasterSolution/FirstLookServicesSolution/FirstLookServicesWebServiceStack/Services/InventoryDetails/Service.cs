﻿using System;
using System.Net;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Commands;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Commands.TransferObjects.Envelopes;
using System.Linq;
using FirstLook.FirstLookServices.WebServiceStack.Utility;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;
using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.InventoryDetails.Model;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.InventoryDetails
{
    [Authenticate]
    public class Service : ServiceStack.ServiceInterface.Service
    {
        private readonly VersionMapper<Request.Inventory> _inventoryInfoMapper = new VersionMapper<Request.Inventory>("v1");

        public Service()
        {
            _inventoryInfoMapper.Add("v1", "get", Get_v1);

        }

        public object Get(Request.Inventory request)
        {
            try
            {
                return _inventoryInfoMapper.GetServiceFunction(request.Ver, "get")(request);
            }
            catch (Exception ex)
            {
                {
                    var err = this.ExceptionToError(ex, null);
                    return (err != null) ? this.HandleError(err) : null;
                }
            }

        }



        public object Get_v1(Request.Inventory request)
        {

            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            string oldETag = RequestContext.GetHeader("If-None-Match");
            var command = factory.CreateFetchInventoryDetailsCommand();
            var cmd = factory.CreateTrimListCommand();
            var arguments = new FetchInventoryArgumentsDto
            {
                InventoryId = request.InventoryId,
                DealerId = request.DealerId

            };


            FetchInventoryDetailsResultsDto results = Helper.ExecuteCommand(command, Helper.WrapInContext(arguments, this.GetSession().UserAuthName));
            var args = new FetchTrimListArgumentsDto
            {
                Vin = results.InventoryData.Vin
            };
            FetchTrimListResultsDto trimResults = Helper.ExecuteCommand(cmd, Helper.WrapInContext(args, this.GetSession().UserAuthName));
         
            Response.InventoryInfo response = null;

            if (results.InventoryData != null)
            {
                response = GetInventoryInfo(results.InventoryData);
            }

            response.TrimList = new List<TrimInfo>();
            trimResults.TrimList.ToList().ForEach(trim => response.TrimList.Add(trim));

            
            IHttpResult result = Handle304(oldETag, response, true);


            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                result = Helper.SetStaticHeaders(result, new[] { "cacheControlHour" });
            }

            return result;
        }

        IHttpResult Handle304(string oldETag, object response, bool compress)
        {
            string newETag = ETagHelper.CreateETagHash(response);

            IHttpResult result = new HttpResult();

            if (oldETag != null && oldETag.Equals(newETag))
            {
                //  ETags match so no need to zip and send the whole response - just headers
                result.StatusCode = HttpStatusCode.NotModified;
            }
            else
            {
                // ETag does not match so we need to send to updated resource
                // 
                if (compress)
                    result = Helper.Compress(this, response);
                else
                    result = new HttpResult
                    {
                        Response = response
                    };

                ETagHelper.AddETagHeader(result, response);

                result.StatusCode = HttpStatusCode.OK;
            }
            return result;
        }

        private Response.InventoryInfo GetInventoryInfo(InventoryDataDto objs)
        {
            Models.InventoryDetails invdet = new Models.InventoryDetails()
                                                              {
                       AcquisitionPrice = objs.AcquisitionPrice,
                       AdReviewStatus = objs.AdReviewStatus,
                       BaseColor = objs.BaseColor,
                       BusinessUnitId = objs.BusinessUnitId,
                       CertificationName = objs.BaseColor,
                       Certified = objs.Certified,
                       CertifiedId = objs.CertifiedId,
                       CertifiedProgramId = objs.CertifiedProgramId,
                       ChromeStyleId = objs.ChromeStyleId,
                       DatePosted = objs.DatePosted,
                       Description = objs.Description,
                       DesiredPhotoCount = objs.DesiredPhotoCount,
                       DueForReprice = objs.DueForReprice,
                       ExtColor1 = objs.ExtColor1,
                       ExtColor2 = objs.ExtColor2,
                       ExteriorColorCode = objs.ExteriorColorCode,
                       ExteriorColorCode2 = objs.ExteriorColorCode2,
                       ExteriorStatus = objs.ExteriorStatus,
                       HasCarfax = objs.HasCarfax,
                       HasCarfaxAdmin = objs.HasCarfaxAdmin,
                       HasCurrentBookValue = objs.HasCurrentBookValue,
                       HasKeyInformation = objs.HasKeyInformation,
                       InteriorColor = objs.InteriorColor,
                       InteriorColorCode = objs.InteriorColorCode,
                       InteriorStatus = objs.InteriorStatus,
                       InventoryId = objs.InventoryId,
                       InventoryReceivedDate = objs.InventoryReceivedDate,
                       InventoryStatusCD = objs.InventoryStatusCD,
                       InventoryType = objs.InventoryType,
                       LastUpdateListPrice = objs.LastUpdateListPrice,
                       ListPrice = objs.ListPrice,
                       LotPrice = objs.LotPrice,
                       LowActivityFlag = objs.LowActivityFlag,
                       Make = objs.Make,
                       MakeId = objs.MakeId,
                       Mileage = objs.Mileage,
                       Model = objs.Model,
                       ModelId = objs.ModelId,
                       Msrp = objs.Msrp,
                       OnlineFlag = objs.OnlineFlag,
                       PhotoStatus = objs.PhotoStatus,
                       PostingStatus = objs.PostingStatus,
                       PricingStatus = objs.PricingStatus,
                       SpecialPrice = objs.SpecialPrice,
                       StatusBucket = objs.StatusBucket,
                       StockNumber = objs.StockNumber,
                       TradePurchase = objs.TradePurchase,
                       Trim = objs.Trim,
                       UnitCost = objs.UnitCost,
                       VehicleLocation = objs.VehicleLocation,
                       Vin = objs.Vin,
                       Year = objs.Year,
                       CarsDotComMakeID=objs.CarsDotComMakeID,
                       CarsDotComModelId=objs.CarsDotComModelId,
                       AutoTraderMake=objs.Make,
                       AutoTraderModel=objs.AutoTraderModel
                   };
         

            return new Response.InventoryInfo() { Inventory = invdet };

        }
    }
}















