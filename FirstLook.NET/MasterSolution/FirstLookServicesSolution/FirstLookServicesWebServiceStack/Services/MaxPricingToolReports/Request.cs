﻿using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.MaxPricingToolReports
{
    [Route("/MaxPricingToolReports/carfax/dealers/{dealer}/vins/{vin}/user/{username}")]
    [Route("/{ver}/MaxPricingToolReports/carfax/dealers/{dealer}/vins/{vin}/user/{username}")]
    public class Carfax
    {
        public int Dealer { get; set; }
        public string Ver { get; set; }
        public string Vin { get; set; }
        public string UserName { get; set; }
    }

    [Route("/MaxPricingToolReports/autocheck/dealers/{dealer}/vins/{vin}/user/{username}")]
    [Route("/{ver}/MaxPricingToolReports/autocheck/dealers/{dealer}/vins/{vin}/user/{username}")]
    public class AutoCheck
    {
        public int Dealer { get; set; }
        public string Ver { get; set; }   
        public string Vin { get; set; }
        public string UserName { get; set; }
    }

}