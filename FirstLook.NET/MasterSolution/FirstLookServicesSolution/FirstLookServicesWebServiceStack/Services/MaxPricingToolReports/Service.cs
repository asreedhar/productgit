﻿using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Threading;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.FirstLookServices.DomainModel.MaxPricingToolReports.Commands;
using FirstLook.FirstLookServices.DomainModel.MaxPricingToolReports.Commands.TransferObjects.Envelopes.AutoCheck;
using FirstLook.FirstLookServices.DomainModel.MaxPricingToolReports.Commands.TransferObjects.Envelopes.Carfax;
using FirstLook.FirstLookServices.DomainModel.Utility;
using FirstLook.FirstLookServices.DomainModel.Validation;
using FirstLook.FirstLookServices.WebServiceStack.Utility;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;
using System;
using FirstLook.VehicleHistoryReport.DomainModel.Carfax;
using Helper = FirstLook.FirstLookServices.WebServiceStack.Utility.Helper;


namespace FirstLook.FirstLookServices.WebServiceStack.Services.MaxPricingToolReports
{

    [Authenticate]
    public class Service : ServiceStack.ServiceInterface.Service
    {
        private const string AutoCheckReportUrlBase = "AutoCheckReportUrlBase";

        VersionMapper<Carfax> carfaxMapper = new VersionMapper<Carfax>("v1");
        VersionMapper<AutoCheck> autoCheckMapper = new VersionMapper<AutoCheck>("v1");

        public Service()
        {
            autoCheckMapper.Add( "v1", "get", Get_v1);
            autoCheckMapper.Add("v1", "post", Post_v1);
            carfaxMapper.Add("v1", "get", Get_v1);
            carfaxMapper.Add("v1", "post", Post_v1);
        }

        #region autocheck

        public object Get(AutoCheck request)
        {
            try
            {
                return autoCheckMapper.GetServiceFunction(request.Ver, "get")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public object Post(AutoCheck request)
        {
            try
            {
                return autoCheckMapper.GetServiceFunction(request.Ver, "post")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public object Get_v1(AutoCheck request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            string oldETag = this.RequestContext.GetHeader("If-None-Match");
            var fetchAutoCheckReportCommand = factory.CreateFetchAutoCheckReportCommand();

            //reach into autocheck reportquery command (client / vin)
            var argumentsDto = new FetchAutoCheckReportArgumentsDto
            {
                DealerId = request.Dealer,
                Vin = request.Vin,
                Username = request.UserName
            };
            FetchAutoCheckReportResultsDto resultsDto = fetchAutoCheckReportCommand.Execute(Helper.WrapInContext(argumentsDto, request.UserName));
            IHttpResult result = null;
            if (resultsDto.Report == null)
            {
                string msg = this.GetString(StringKeys.UserMayOrderReport);
                var ex = new NoDataFoundException(msg);
                throw ex;
            }

            AutoCheckResponse response = BuildAutoCheckReport(resultsDto.Report, request.Dealer);
            result = handle304(oldETag, response, true);
            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                result = Helper.SetStaticHeaders(result, new string[] { "cacheControlHour" });
            }

            return result;
        }

        public object Post_v1(AutoCheck request)
        {
            var vhrfactory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            var purchaseAutoCheckReportCommand = vhrfactory.CreatePurchaseAutoCheckReportCommand();

            var argumentsDto = new PurchaseAutoCheckReportArgumentsDto
            {
                DealerId = request.Dealer,
                Vin = request.Vin,
                Username = request.UserName
            };

            PurchaseAutoCheckReportResultsDto resultsDto = null;

            resultsDto =
                purchaseAutoCheckReportCommand.Execute(Helper.WrapInContext(argumentsDto, request.UserName));

            IHttpResult result = null;

            /*if (resultsDto.Report == null)
            {
                //the report failed to be purchasd for some reason, for now assume Forbidden
                return (new HttpResult(new Dictionary<string, string>(), HttpStatusCode.Forbidden));
            }*/

            AutoCheckResponse response = BuildAutoCheckReport(resultsDto.Report, request.Dealer);
            result = Helper.Compress(this, response);
            result.StatusCode = HttpStatusCode.Created;
            result = Helper.SetStaticHeaders(result, new string[] { "cacheControlHour" });

            return result;
        }

        #endregion

        #region carfax

        public object Get(Carfax request)
        {
            try
            {
                return carfaxMapper.GetServiceFunction(request.Ver, "get")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public object Post(Carfax request)
        {
            try
            {
                return carfaxMapper.GetServiceFunction(request.Ver, "post")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public object Get_v1(Carfax request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            string oldETag = this.RequestContext.GetHeader("If-None-Match");
            var fetchCarfaxReportCommand = factory.CreateFetchCarfaxReportCommand();


            if (!this.ValidVin(request.Vin))
                throw new InvalidInputException("Vin", request.Vin, "Bad Vin: " + request.Vin);

            var argumentsDto = new FetchCarfaxReportArgumentsDto
            {
                DealerId = request.Dealer,
                Vin = request.Vin,
                Username = request.UserName
            };

            FetchCarfaxReportResultsDto resultsDto = fetchCarfaxReportCommand.Execute(Helper.WrapInContext(argumentsDto, request.UserName));

            IHttpResult result = null;
            if (resultsDto.Report == null && resultsDto.ReportAvailable == true)
            {
                string msg = this.GetString(StringKeys.UserMayOrderReport);
                var ex = new NoDataFoundException (msg);
                //ex.Data["code"] = (int)ErrorCodeEnumeration.NoDataFound;
                throw ex; // throw new CarfaxException(-1, CarfaxResponseCode.Failure_AccountStatus);
            }
            if (resultsDto.Report == null && resultsDto.ReportAvailable == false)
            {
                string msg = this.GetString(StringKeys.CarfaxReportNotAvailable);
                var ex = new NoDataFoundException(msg);
                throw ex; 
            }

            CarfaxResponse response = BuildCarFaxReport(resultsDto.Report);
            result = handle304(oldETag, response, true);
            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                result = Helper.SetStaticHeaders(result, new string[] { "cacheControlHour" });
            }

            return result;
        }

        public object Post_v1(Carfax request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            var purchaseCarfaxReportCommand = factory.CreatePurchaseCarfaxReportCommand();

            if (!this.ValidVin(request.Vin))
                throw new InvalidInputException("Vin", request.Vin, "Bad Vin: " + request.Vin);

            var argumentsDto = new PurchaseCarfaxReportArgumentsDto
                                   {
                                       DealerId = request.Dealer,
                                       Vin = request.Vin,
                                       Username = request.UserName
                                   };

            PurchaseCarfaxReportResultsDto purchaseCarfaxReportResultsDto = null;

            purchaseCarfaxReportResultsDto =
                purchaseCarfaxReportCommand.Execute(Helper.WrapInContext(argumentsDto, request.UserName));

            IHttpResult result = null;
            /*if (purchaseCarfaxReportResultsDto.Report == null)
            {
                //the report was not purchased for some reason, for now assume Forbidden
                return (new HttpResult(new Dictionary<string, string>(), HttpStatusCode.Forbidden));
            }*/

            CarfaxResponse response = BuildCarFaxReport(purchaseCarfaxReportResultsDto.Report);
            result = Helper.Compress(this, response);
            result = Helper.SetStaticHeaders(result, new string[] { "cacheControlHour" });
            result.StatusCode = HttpStatusCode.Created;

            return result;
        }


        #endregion

        #region helper

        private static AutoCheckResponse BuildAutoCheckReport(DomainModel.MaxPricingToolReports.Commands.TransferObjects.AutoCheck.ReportDto autoCheckReport, int dealer)
        {
            if (autoCheckReport == null)
            {
                return null;
            }

            string autoCheckUrlBase = ConfigurationManager.AppSettings[AutoCheckReportUrlBase];

            IDictionary<string, bool> inspections = new Dictionary<string, bool>();

            foreach (DomainModel.MaxPricingToolReports.Commands.TransferObjects.AutoCheck.InspectionDto inspection in autoCheckReport.Inspections)
            {
                inspections.Add(new KeyValuePair<string, bool>(inspection.Name.ToLower(), inspection.Selected));
            }

            var report = new AutoCheckResponse
            {
                CompareScoreRangeHigh = autoCheckReport.CompareScoreRangeHigh,
                CompareScoreRangeLow = autoCheckReport.CompareScoreRangeLow,
                ExpirationDate = autoCheckReport.ExpirationDate.ToUniversalTime().ToString("yyyyMMdd'T'HHmmss'Z'"),
                OwnerCount = autoCheckReport.OwnerCount,
                Score = autoCheckReport.Score,
                UserName = autoCheckReport.UserName,
                Href = autoCheckUrlBase + "/support/Reports/AutoCheckReport.aspx?DealerId=" + dealer + "&Vin=" + autoCheckReport.Vin,
                Inspections = inspections
            };

            return report;
        }

        private static CarfaxResponse BuildCarFaxReport(DomainModel.MaxPricingToolReports.Commands.TransferObjects.Carfax.ReportDto carfaxReport)
        {
            if (carfaxReport == null)
            {
                return null;
            }

            IDictionary<string, bool> inspections = new Dictionary<string, bool>();

            foreach (DomainModel.MaxPricingToolReports.Commands.TransferObjects.Carfax.InspectionDto inspection in carfaxReport.Inspections)
            {
                inspections.Add(new KeyValuePair<string, bool>(inspection.Name.ToLower(), inspection.Selected));
            }

            var report = new CarfaxResponse
            {
                ExpirationDate = carfaxReport.ExpirationDate.ToUniversalTime().ToString("yyyyMMdd'T'000000'Z'"),
                OwnerCount = carfaxReport.OwnerCount,
                HasProblems = carfaxReport.HasProblems,
                UserName = carfaxReport.UserName,
                Href = "http://www.carfaxonline.com/cfm/Display_Dealer_Report.cfm?partner=FLK_0&UID=" + carfaxReport.UserName + "&vin=" + carfaxReport.Vin,
                ReportType = carfaxReport.ReportType,
                Inspections = inspections
            };

            return report;
        }

        IHttpResult handle304(string oldETag, object response, bool compress)
        {
            string newETag = ETagHelper.CreateETagHash(response);
            IHttpResult result = new HttpResult();
            if (oldETag != null && oldETag.Equals(newETag))
            {
                //  ETags match so no need to zip and send the whole response - just headers
                result.StatusCode = HttpStatusCode.NotModified;
            }
            else
            {
                // ETag does not match so we need to send to updated resource
                if (compress)
                    result = Helper.Compress(this, response);
                else
                    result = new HttpResult
                    {
                        Response = response
                    };

                ETagHelper.AddETagHeader(result, response);
                result.StatusCode = HttpStatusCode.OK;
            }
            return result;
        }

        #endregion
    }
}