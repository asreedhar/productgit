﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.MaxPricingToolReports
{
    [DataContract]
    public class CarfaxResponse : FirstLook.FirstLookServices.WebServiceStack.Services.Reports.Models.Carfax.Report
    {
    }

    [DataContract]
    public class AutoCheckResponse : FirstLook.FirstLookServices.WebServiceStack.Services.Reports.Models.AutoCheck.Report 
    {
    }
}