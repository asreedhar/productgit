﻿
namespace FirstLook.FirstLookServices.WebServiceStack.Services.AmazonSnsSubscriber
{
    public enum SubscriptionMessageTypes
    {
        Undefined=0,
        SubscriptionConfirmation=1,
        Notification=2
    }
}