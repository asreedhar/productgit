﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.AmazonSnsSubscriber
{
    [DataContract]
    public class InventoryBookoutResponse
    {
        [DataMember(Name = "bookoutRequired")]
        public int BookoutRequired { get; set; }
    }

}