﻿using System.IO;
using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.AmazonSnsSubscriber
{
    [Route("/{ver}/AmazonSNSSubscription")]
    [Route("/AmazonSNSSubscription")]

    public class AmazonSnsSubscription : IRequiresRequestStream
    {   
        public string Ver { get; set; }

        #region IRequiresRequestStream Members

        public Stream RequestStream { get; set; }

        #endregion
    }

    
}
