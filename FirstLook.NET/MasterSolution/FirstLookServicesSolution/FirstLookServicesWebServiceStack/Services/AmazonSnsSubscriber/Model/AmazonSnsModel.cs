﻿using System;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.AmazonSnsSubscriber.Model
{
    public class AmazonSnsModel
    {
        public string Type { get; set; }

        public string MessageId { get; set; }

        public string TopicArn { get; set; }

        public string Subject { get; set; }

        public string Message { get; set; }

        public DateTime Timestamp { get; set; }

        public string SignatureVersion { get; set; }

        public string Signature { get; set; }

        public string SigningCertUrl { get; set; }

        public string UnsubscribeUrl { get; set; }

        public string SubscribeUrl { get; set; }

    }
}
