﻿
using System;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.AmazonSnsSubscriber.Model
{
    public class Inventory
    {
        public int InventoryId { get; set; }
        public int BusinessUnitId { get; set; }
        public string User { get; set; }
        public DateTime Sent { get; set; }
        public string SentFrom { get; set; }
        public string Delay { get; set; }
    }
}