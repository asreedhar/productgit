﻿using System;
using System.IO;
using System.Net;
using FirstLook.Fault.DomainModel.Utilities;
using FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Commands;
using FirstLook.FirstLookServices.DomainModel.AmazonSnsSubscriber.Commands.TransferObjects;
using FirstLook.FirstLookServices.WebServiceStack.Services.AmazonSnsSubscriber.Model;
using FirstLook.FirstLookServices.WebServiceStack.Utility;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;
using ServiceStack.Text;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.AmazonSnsSubscriber
{
    [Authenticate]
    public class Service : ServiceStack.ServiceInterface.Service
    {
        private static readonly log4net.ILog Log4net = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        readonly VersionMapper<AmazonSnsSubscription> _amazonSnsSubscriptionMapper = new VersionMapper<AmazonSnsSubscription>("v1");

        public Service()
        {
            _amazonSnsSubscriptionMapper.Add("v1", "post", PostV1);
        }


        public object Post(AmazonSnsSubscription request)
        {
            try
            {
                return _amazonSnsSubscriptionMapper.GetServiceFunction(request.Ver, "post")(request);
            }
            catch (Exception ex)
            {
                FaultWebExceptionObserver.SaveException(ex);

                var err = this.ExceptionToError(ex, null);

                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public IHttpResult PostV1(AmazonSnsSubscription request)
        {
            string messageType = this.RequestContext.GetHeader("x-amz-sns-message-type");
            string oldETag = this.RequestContext.GetHeader("If-None-Match");

            StreamReader sr = new StreamReader(request.RequestStream);
            string data = sr.ReadToEnd();
            if (!data.Trim().StartsWith("{"))
                data = "{" + data;

            if (!data.Trim().EndsWith("}"))
                data = data + "}";
            AmazonSnsModel amazonSnsModel = JsonSerializer.DeserializeFromString<AmazonSnsModel>(data);

            IHttpResult result = null;
          
            //Log4net.Info("Test SNS inventory :" + data);
            

            if (!string.IsNullOrEmpty(messageType) &&
                messageType == SubscriptionMessageTypes.SubscriptionConfirmation.ToString())
            {
                ICommandFactory factory = new CommandFactory();
                var command = factory.CreateSubscribeCommand();

                var arguments = new SubscriberCommandArgumentDto()
                {
                    Url = amazonSnsModel.SubscribeUrl
                };

                SubscriberCommandResultDto subscriberCommandResult = Helper.ExecuteCommand(command, Helper.WrapInContext(arguments, this.GetSession().UserAuthName));


                result = Handle304(oldETag, subscriberCommandResult, true);
            }
            else
            {
                Model.Inventory inventory = JsonSerializer.DeserializeFromString<Model.Inventory>(amazonSnsModel.Message.Replace(@"\", ""));
                //Log4net.Info("InventoryId :" + inventory.InventoryId);
                var arguments = new InventoryBookoutCommandArgumentDto()
                {
                    InventoryId = inventory.InventoryId
                };

                ICommandFactory factory = new CommandFactory();
                var command = factory.CreateInventoryBookoutCommand();
                InventoryBookoutCommandResultDto inventoryBookoutCommandResult = Helper.ExecuteCommand(command, Helper.WrapInContext(arguments, this.GetSession().UserAuthName));

                result = Handle304(oldETag, Map(inventoryBookoutCommandResult), true);
            }

            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                Helper.SetStaticHeaders(result, new string[] { "referenceDataTrue", "cacheControlNoCache" });
            }
            return result;
        }

        IHttpResult Handle304(string oldETag, object response, bool compress)
        {
            string newETag = ETagHelper.CreateETagHash(response);
            IHttpResult result = new HttpResult();
            if (oldETag != null && oldETag.Equals(newETag))
            {
                //  ETags match so no need to zip and send the whole response - just headers
                result.StatusCode = HttpStatusCode.NotModified;
            }
            else
            {
                // ETag does not match so we need to send to updated resource
                if (compress)
                    result = Helper.Compress(this, response);
                else
                    result = new HttpResult
                    {
                        Response = response
                    };

                ETagHelper.AddETagHeader(result, response);
                result.StatusCode = HttpStatusCode.OK;
            }
            return result;
        }

        private InventoryBookoutResponse Map(InventoryBookoutCommandResultDto inventoryBookoutCommandResult)
        {
            return new InventoryBookoutResponse()
                   {
                       BookoutRequired = inventoryBookoutCommandResult.BookoutRequired
                   };

        }
    }
}