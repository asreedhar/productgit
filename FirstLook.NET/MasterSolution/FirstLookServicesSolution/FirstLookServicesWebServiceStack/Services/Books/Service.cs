﻿using System.Linq;
using System.Net;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Books.Commands;
using FirstLook.FirstLookServices.DomainModel.Books.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.WebServiceStack.Services.Books.Models;
using FirstLook.FirstLookServices.WebServiceStack.Utility;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;
using ServiceStack.Common.Web;
using System;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Books
{

   [Authenticate]
    public class Service : ServiceStack.ServiceInterface.Service
    {
        readonly VersionMapper<DealerBooks> _dealerBooksMapper = new VersionMapper<DealerBooks>("v1");
        readonly VersionMapper<Books> _booksMapper = new VersionMapper<Books>("v1");

        public Service()
        {
            _dealerBooksMapper.Add("v1", "get", Get_v1);
            _booksMapper.Add("v1", "get", Get_v1);

        }

        public object Get(DealerBooks request)
        {

            try
            {
                return _dealerBooksMapper.GetServiceFunction(request.Ver, "get")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }

        }

        public object Get_v1(DealerBooks request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            string oldETag = RequestContext.GetHeader("If-None-Match");

            var command = factory.CreateFetchDealerBooksCommand();

            var arguments = new FetchDealerBooksArgumentsDto
                                   {
                                       DealerId = request.Dealer
                                   };

            FetchDealerBooksResultsDto results = Helper.ExecuteCommand(command, Helper.WrapInContext(arguments, this.GetSession().UserAuthName));

            var response = new DealerBooksResponse
                               {
                                   Books = results.Books.Select(book => book.Description.ToLower()).ToList()
                               };

            IHttpResult result = Handle304(oldETag, response, true);

            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                result = Helper.SetStaticHeaders(result, new[] { "cacheControlZero" });
            }

            return result;
        }

        public object Get(Books request)
        {
                
            try
            {
                return _booksMapper.GetServiceFunction(request.Ver, "get")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public object Get_v1(Books request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            string oldETag = RequestContext.GetHeader("If-None-Match");

            var command = factory.CreateFetchBooksCommand();

            var arguments = new FetchBooksArgumentsDto();

            FetchBooksResultsDto results = Helper.ExecuteCommand(command, arguments);

            //builds books response list, while filtering out edmunds from the list.
            var response = new BooksResponse
                               {
                                   Books = results.Books.Select(book => new Book
                                                                            {
                                                                                Description = book.Name,
                                                                                Name = book.Description.ToLower()
                                                                            }).ToList().FindAll(book => book.Name.ToLower().CompareTo("edmunds") != 0)
                               };

            IHttpResult result = Handle304(oldETag, response, true);

            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                result = Helper.SetStaticHeaders(result, new[] { "referenceDataTrue", "cacheControlDay" });
            }

            return result;
        }


        IHttpResult Handle304(string oldETag, object response, bool compress)
        {
            string newETag = ETagHelper.CreateETagHash(response);

            IHttpResult result = new HttpResult();

            if (oldETag != null && oldETag.Equals(newETag))
            {
                //  ETags match so no need to zip and send the whole response - just headers
                result.StatusCode = HttpStatusCode.NotModified;
            }
            else
            {
                // ETag does not match so we need to send to updated resource
                // 
                if (compress)
                    result = Helper.Compress(this, response);
                else
                    result = new HttpResult
                    {
                        Response = response
                    };

                ETagHelper.AddETagHeader(result, response);

                result.StatusCode = HttpStatusCode.OK;
            }
            return result;
        }
    }
}