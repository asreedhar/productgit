﻿using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Books
{
    [Route("/{ver}/books/dealers/{dealer}")]
    [Route("/books/dealers/{dealer}")]
    public class DealerBooks
    {
        public int Dealer { get; set; }
        public string Ver { get; set; }
    }

    [Route("/{ver}/books")]
    [Route("/books")]
    public class Books
    {
        public string Ver { get; set; }
    }

}