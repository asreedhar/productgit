﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Books.Models
{
    [DataContract]
    public class Book
    {
        [DataMember(Name = "book")]
        public string Name { get; internal set; }

        [DataMember(Name = "desc")]
        public string Description { get; internal set; }
    }
}