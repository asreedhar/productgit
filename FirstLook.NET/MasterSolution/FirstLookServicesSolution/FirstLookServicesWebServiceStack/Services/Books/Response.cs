﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using FirstLook.FirstLookServices.WebServiceStack.Services.Books.Models;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Books
{
    [DataContract]
    public class DealerBooksResponse
    {
        [DataMember(Name = "books")]
        public List<string> Books { get; set; }
    }

    [DataContract]
    public class BooksResponse
    {
        [DataMember(Name = "books")]
        public List<Book> Books { get; set; }
    }

}