﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace FirstLook.FirstLookServices.WebServiceStack.Services.MarketListings.Models
{
    [DataContract]
    public class MarketListingsResponse
    {
        [DataMember(Name = "marketListings")]
        public List<SearchMarketListingsResponse> MarketListings { get; set; }
    }
    
}