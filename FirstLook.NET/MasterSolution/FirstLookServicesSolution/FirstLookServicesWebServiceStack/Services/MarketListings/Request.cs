﻿using System.Collections.Generic;
using ServiceStack.ServiceHost;
using FirstLook.FirstLookServices.WebServiceStack.Services.MarketListings.Models;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.MarketListings
{
    [Route("/marketListing/search")]
    [Route("/{ver}/marketListing/search")]
    public class MarketListingsRequest
    {
        public string Ver { get; set; }
        public List<SearchMarketListings> SearchMarketListings { get; set; }
    }
}