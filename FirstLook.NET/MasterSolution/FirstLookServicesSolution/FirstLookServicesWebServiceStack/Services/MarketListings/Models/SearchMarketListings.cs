﻿using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Model;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.MarketListings.Models
{
    public class SearchMarketListings
    {
        public int BusinessUnitId { get; set; }

        public int InventoryId { get; set; }

        public int? Year { get; set; }

        public string Make { get; set; }

        public string Model { get; set; }

        public List<string> Trims { get; set; }

        public int? MinMileage { get; set; }

        public int? MaxMileage { get; set; }

        public int? MinPrice { get; set; }

        public int? MaxPrice { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public int? Distance { get; set; }

        public int? Size { get; set; }

        public int? From { get; set; }

        public bool? OemCertified { get; set; }

        public bool? RecentActive { get; set; }

        public bool? ActiveOnly { get; set; }

        public bool? AggregateListings { get; set; }

        public List<string> Transmissions { get; set; }

        public List<string> DriveTrains { get; set; }

        public List<string> Engines { get; set; }

        public List<string> FuelTypes { get; set; }

        public List<string> Aggregations { get; set; }

        public List<Sorts> Sorts { get; set; }

        public bool? IncludeZeroPrice { get; set; }

        public List<string> Equipment { get; set; }

        public bool? GetRanks { get; set; }

        public string Comment { get; set; }
    }
}