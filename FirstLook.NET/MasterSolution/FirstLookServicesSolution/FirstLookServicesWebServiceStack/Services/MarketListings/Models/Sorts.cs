﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.MarketListings.Models
{
    public class Sorts
    {
        public string Field { get; set; }
     
        public string Order { get; set; }   
    }
}