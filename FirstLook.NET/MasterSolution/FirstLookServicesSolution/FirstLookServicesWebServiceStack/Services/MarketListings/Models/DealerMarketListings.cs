﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.MarketListings.Models
{
    [DataContract]
    public class DealerMarketListings
    {
        [DataMember(Name = "age")]
        public int? Age { get; set; }

        [DataMember(Name = "seller")]
        public string Seller { get; set; }

        [DataMember(Name = "certified")]
        public bool Certified { get; set; }

        [DataMember(Name = "color")]
        public string Color { get; set; }

        [DataMember(Name = "mileage")]
        public int? Mileage { get; set; }

        [DataMember(Name = "internetPrice")]
        public decimal? InternetPrice { get; set; }

        [DataMember(Name = "Location")]
        public string Location { get; set; }

        [DataMember(Name = "vin")]
        public string Vin { get; set; }

        [DataMember(Name = "year")]
        public int? Year { get; set; }

        [DataMember(Name = "make")]
        public string Make { get; set; }

        [DataMember(Name = "model")]
        public string Model { get; set; }

        [DataMember(Name = "trim")]
        public string Trim { get; set; }

        [DataMember(Name = "adText")]
        public string AdText { get; set; }

        [DataMember(Name = "stockNumber")]
        public string StockNumber { get; set; }

    }
}