﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Model;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.MarketListings.Models
{

    [DataContract]
    public class SearchMarketListingsResponse
    {
        [DataMember(Name = "searchMarketListings")]
        public List<DealerMarketListings> DealerMarketListings { get; set; }

        [DataMember(Name = "facetCounts")]
        public List<FacetEquipments> TierOneEquipments { get; set; }

        [DataMember(Name = "priceStats")]
        public PriceStat PriceStats { get; set; }

        [DataMember(Name = "oemCertifiedTerms")]
        public List<EquipmentCount> CertifiedTerms { get; set; }

        [DataMember(Name = "driveTrainTerms")]
        public List<EquipmentCount> DriveTrainTerms { get; set; }

        [DataMember(Name = "transmissionTerms")]
        public List<EquipmentCount> TransmissionTerms { get; set; }

        [DataMember(Name = "trimTerms")]
        public List<EquipmentCount> TrimTerms { get; set; }

        [DataMember(Name = "engineTerms")]
        public List<EquipmentCount> EngineTerms { get; set; }

        [DataMember(Name = "fuelTypeTerms")]
        public List<EquipmentCount> FuelTypeTerms { get; set; }

        [DataMember(Name = "price_histogram")]
        public List<HistogramCount> PriceHistogram { get; set; }

        [DataMember(Name = "odometerStats")]
        public OdometerStat OdometerStats { get; set; }

        [DataMember(Name = "modelTerms")]
        public List<EquipmentCount> ModelTerms { get; set; }

        [DataMember(Name = "marketDaysSupply")]
        public List<MarketDaysSupply> MarketDaysSupplyInfo { get; set; }

        [DataMember(Name = "inventoryId")]
        public int? InventoryId { get; set; }

        [DataMember(Name = "marketPrices")]
        public IList<int> MarketPrices { get; set; }

        [DataMember(Name = "recordCount")]
        public int RecordCount { get; set; }

        [DataMember(Name = "recentActiveCount")]
        public int RecentActiveCount { get; set; }

        [DataMember(Name = "activeCount")]
        public int ActiveCount { get; set; }

        [DataMember(Name = "isIncludesCurrentVehicle")]
        public bool IsIncludesCurrentVehicle { get; set; }
    }
}