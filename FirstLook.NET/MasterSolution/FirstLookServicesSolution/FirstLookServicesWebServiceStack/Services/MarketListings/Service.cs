﻿using System;
using System.Linq;
using System.Net;
using System.Web.Routing;
using FirstLook.FirstLookServices.WebServiceStack.Services.MarketListings.Models;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;
using FirstLook.FirstLookServices.WebServiceStack.Utility;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Commands;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Commands.TransferObjects.Envelopes;
using System.Collections.Generic;
using FirstLook.FirstLookServices.DomainModel.MarketListings.Model;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.MarketListings
{
    [Authenticate]
    public class Service : ServiceStack.ServiceInterface.Service
    {
        private readonly VersionMapper<MarketListingsRequest> _marketListingsMapper = new VersionMapper<MarketListingsRequest>("v1");

        public Service()
        {
            _marketListingsMapper.Add("v1", "post", Post_v1);

        }
        public object Post(MarketListingsRequest request)
        {
            try
            {

                return _marketListingsMapper.GetServiceFunction(request.Ver, "post")(request);

            }
            catch (Exception ex)
            {
                {
                    var err = this.ExceptionToError(ex, null);
                    return (err != null) ? this.HandleError(err) : null;
                }
            }

        }

        public object Post_v1(MarketListingsRequest req)
        {
            var resList = new MarketListingsResponse();
            
            var argsList = new List<FetchMarketListingsArgumentsDto>();
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            string oldETag = RequestContext.GetHeader("If-None-Match");
            var command = factory.CreateFetchMarketListingsCommand();
         
            resList.MarketListings = new List<SearchMarketListingsResponse>();

            foreach (var request in req.SearchMarketListings)
            {

                var arguments = new FetchMarketListingsArgumentsDto()
                {
                    Year = request.Year,
                    Make = request.Make,
                    Model = request.Model,
                    Trims = request.Trims,
                    MinMileage = request.MinMileage,
                    MaxMileage = request.MaxMileage,
                    MinPrice = request.MinPrice,
                    MaxPrice = request.MaxPrice,
                    Latitude = request.Latitude,
                    Longitude = request.Longitude,
                    Distance = request.Distance,
                    Size = request.Size,
                    From = request.From,
                    OemCertified = request.OemCertified,
                    ActiveOnly=request.ActiveOnly,
                    RecentActive=request.RecentActive,
                    AggregateListings=request.AggregateListings,
                    Transmissions = request.Transmissions,
                    DriveTrains = request.DriveTrains,
                    Engines = request.Engines,
                    FuelTypes = request.FuelTypes,
                    Aggregations = request.Aggregations,
                    IncludeZeroPrice = request.IncludeZeroPrice,
                    Equipment = request.Equipment,
                    InventoryId = request.InventoryId,
                    BusinessUnitId= request.BusinessUnitId,
                    GetRanks=request.GetRanks,                    
                    IsRequiredAggregations=request.Aggregations.Contains("equipment_terms"),
                    Comment=request.Comment
                };

                if (request.Sorts != null)
                {
                    arguments.Sorts = new List<DomainModel.MarketListings.Model.Sorts>();
                    request.Sorts.ForEach(st => arguments.Sorts.Add(new DomainModel.MarketListings.Model.Sorts() { Field = st.Field, Order = st.Order }));
                    
                }

                argsList.Add(arguments);
               
            }
            List<ListingsAndCounts> finalResults = Helper.ExecuteCommand(command, Helper.WrapInContext(argsList, this.GetSession().UserAuthName));
            int iCtr = 0;
            foreach (var results in finalResults)
            {
                var response = new SearchMarketListingsResponse();
                response.DealerMarketListings = new List<DealerMarketListings>();
                response.TierOneEquipments = new List<FacetEquipments>();
                response.CertifiedTerms = new List<EquipmentCount>();
                response.DriveTrainTerms = new List<EquipmentCount>();
                response.EngineTerms = new List<EquipmentCount>();
                response.FuelTypeTerms = new List<EquipmentCount>();
                response.TransmissionTerms = new List<EquipmentCount>();
                response.TrimTerms = new List<EquipmentCount>();
                response.ModelTerms = new List<EquipmentCount>();
                response.PriceHistogram = new List<HistogramCount>();
                response.MarketDaysSupplyInfo = new List<MarketDaysSupply>();
                response.MarketPrices = results.MarketPrices;
                response.RecordCount = results.RecordCount;
                response.TierOneEquipments = results.TierOneEquipments;
                response.ActiveCount = results.ListingTerms.ActiveCounts;
                response.RecentActiveCount = results.ListingTerms.RecentActiveCounts;

                if (results != null && results.Listings.FirstOrDefault() != null)
                {
                    results.Listings.ToList().ForEach(marketListing => response.DealerMarketListings.Add(new DealerMarketListings()
                    {
                        Age = marketListing.Age,
                        Certified = marketListing.Certified,
                        Color = marketListing.Color,
                        Location = marketListing.Location,
                        InternetPrice = marketListing.InternetPrice,
                        Mileage = marketListing.Mileage,
                        Seller = marketListing.Seller,
                        Vin = marketListing.Vin,
                        Year = marketListing.Year,
                        Make = marketListing.Make,
                        Model = marketListing.Model,
                        Trim = marketListing.Trim,
                        AdText=marketListing.AdText,
                        StockNumber=marketListing.StockNumber
                    }));
                }

                if (results.CertifiedTerms != null)
                {
                    foreach (var certified in results.CertifiedTerms.Buckets)
                    {
                        response.CertifiedTerms.Add(new EquipmentCount()
                        {
                            Key = certified.Key,
                            DocCount = certified.DocCount
                        });
                    }
                }

                if (results.DriveTrainTerms != null)
                {
                    foreach (var driveTrain in results.DriveTrainTerms.Buckets)
                    {
                        response.DriveTrainTerms.Add(new EquipmentCount()
                        {
                            Key = driveTrain.Key,
                            DocCount = driveTrain.DocCount
                        });
                    }
                }
                if (results.EngineTerms != null)
                {
                    foreach (var engine in results.EngineTerms.Buckets)
                    {
                        response.EngineTerms.Add(new EquipmentCount()
                        {
                            Key = engine.Key,
                            DocCount = engine.DocCount
                        });
                    }
                }
                if (results.FuelTypeTerms != null)
                {
                    foreach (var fuelType in results.FuelTypeTerms.Buckets)
                    {
                        response.FuelTypeTerms.Add(new EquipmentCount()
                        {
                            Key = fuelType.Key,
                            DocCount = fuelType.DocCount
                        });
                    }
                }

                if (results.TransmissionTerms != null)
                {
                    foreach (var transmission in results.TransmissionTerms.Buckets)
                    {
                        response.TransmissionTerms.Add(new EquipmentCount()
                        {
                            Key = transmission.Key,
                            DocCount = transmission.DocCount
                        });
                    }
                }

                if (results.TrimTerms != null)
                {
                    foreach (var trim in results.TrimTerms.Buckets)
                    {
                        response.TrimTerms.Add(new EquipmentCount()
                        {
                            Key = trim.Key,
                            DocCount = trim.DocCount
                        });
                    }
                }

                if (results.ModelTerms != null)
                {
                    foreach (var model in results.ModelTerms.Buckets)
                    {
                        response.ModelTerms.Add(new EquipmentCount()
                        {
                            Key = model.Key,
                            DocCount = model.DocCount
                        });
                    }
                }

                if (results.PriceHistogramTerm != null)
                {
                    foreach (var histogram in results.PriceHistogramTerm.Buckets)
                    {
                        response.PriceHistogram.Add(new HistogramCount()
                        {
                            Key = histogram.Key,
                            DocCount = histogram.DocCount
                        });
                    }
                }

                foreach (var mktDays in results.MarketDaysSupplyInfo)
                {
                    response.MarketDaysSupplyInfo.Add(new MarketDaysSupply()
                    {
                        SearchTypeId = mktDays.SearchTypeId,
                        MarketDaysSupplyCount = mktDays.MarketDaysSupplyCount
                    });
                }

                response.PriceStats = new PriceStat();
                response.OdometerStats = new OdometerStat();
                response.PriceStats = results.PriceStats;
                response.OdometerStats = results.OdometerStats;
                response.InventoryId = results.InventoryId;
                resList.MarketListings.Add(response);
                iCtr++;
            }

            IHttpResult result = Handle304(oldETag, resList, true);

            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                result = Helper.SetStaticHeaders(result, new[] { "cacheControlHour" });
            }


            return result;

        }
        IHttpResult Handle304(string oldETag, object response, bool compress)
        {
            string newETag = ETagHelper.CreateETagHash(response);

            IHttpResult result = new HttpResult();

            if (oldETag != null && oldETag.Equals(newETag))
            {
                //  ETags match so no need to zip and send the whole response - just headers
                result.StatusCode = HttpStatusCode.NotModified;
            }
            else
            {
                // ETag does not match so we need to send to updated resource
                // 
                if (compress)
                    result = Helper.Compress(this, response);
                else
                    result = new HttpResult
                    {
                        Response = response
                    };

                ETagHelper.AddETagHeader(result, response);

                result.StatusCode = HttpStatusCode.OK;
            }
            return result;
        }
    }
}