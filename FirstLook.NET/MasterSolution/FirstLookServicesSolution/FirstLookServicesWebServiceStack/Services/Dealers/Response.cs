﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Dealers
{
    [DataContract]
    public class DealersResponse
    {
        [DataMember(Name = "dealers")]
        public List<Models.Dealer> Dealers { get; set; }
    }
}