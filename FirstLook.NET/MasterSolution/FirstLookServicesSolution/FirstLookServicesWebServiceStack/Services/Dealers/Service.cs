﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Dealers.Commands;
using FirstLook.FirstLookServices.DomainModel.Dealers.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.WebServiceStack.Services.Dealers.Models;
using FirstLook.FirstLookServices.WebServiceStack.Utility;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Dealers
{

    [Authenticate]
    public class Service : ServiceStack.ServiceInterface.Service
    {
        readonly VersionMapper<Dealers> _dealersMapper = new VersionMapper<Dealers>("v1");

        public Service()
        {
            _dealersMapper.Add("v1", "get", Get_v1);
        }

        public object Get(Dealers request)
        {
            try
            {
                return _dealersMapper.GetServiceFunction(request.Ver, "get")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);

                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public object Get_v1(Dealers request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            string oldETag = RequestContext.GetHeader("If-None-Match");

            var command = factory.CreateFetchDealersCommand();

            var arguments = new FetchDealersArgumentsDto();

            FetchDealersResultsDto results = Helper.ExecuteCommand(command, Helper.WrapInContext(arguments, this.GetSession().UserAuthName));

            //Map to dealer
            IList<Dealer> dealers = results.Dealers.Select(dealer => new Dealer
            {
                Id = dealer.Id,
                Description = dealer.Description
            }).ToList();

            var response = new DealersResponse
            {
                Dealers = (List<Dealer>)dealers
            };

            IHttpResult result = Handle304(oldETag, response, true);
            //throw new Exception();
            return result;
        }

        IHttpResult Handle304(string oldETag, object response, bool compress)
        {
            string newETag = ETagHelper.CreateETagHash(response);

            IHttpResult result = new HttpResult();

            if (oldETag != null && oldETag.Equals(newETag))
            {
                //  ETags match so no need to zip and send the whole response - just headers
                result.StatusCode = HttpStatusCode.NotModified;
            }
            else
            {
                // ETag does not match so we need to send to updated resource
                // 
                if (compress)
                    result = Helper.Compress(this, response);
                else
                    result = new HttpResult
                    {
                        Response = response
                    };

                Helper.SetStaticHeaders(result, new[] { "cacheControlHour" });

                ETagHelper.AddETagHeader(result, response);

                result.StatusCode = HttpStatusCode.OK;
            }

            return result;
        }
    }
}