﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Dealers.Models
{
    [DataContract]
    public class Dealer
    {
        [DataMember(Name = "dealer")]
        public int Id { get; internal set; }

        [DataMember(Name = "desc")]
        public string Description { get; internal set; }
    }
}