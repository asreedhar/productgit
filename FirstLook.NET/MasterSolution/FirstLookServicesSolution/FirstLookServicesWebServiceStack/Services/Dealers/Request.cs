﻿using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Dealers
{
    [Route("/{ver}/dealers")]
    [Route("/dealers")]
    public class Dealers
    {
        public string Ver { get; set; }
    }
}