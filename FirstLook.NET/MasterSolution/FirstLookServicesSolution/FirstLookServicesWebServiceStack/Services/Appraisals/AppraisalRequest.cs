﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using FirstLook.FirstLookServices.WebServiceStack.Services.Appraisals.Models;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Appraisals
{
    [DataContract]
    public class AppraisalRequest
    {
        [DataMember(Name = "appraisalAmount")]
        public int AppraisalAmount { get; set; }

        [DataMember(Name = "appraisalType")]
        public string AppraisalType { get; set; }

        [DataMember(Name = "color")]
        public string Color { get; set; }

        [DataMember(Name = "targetGrossProfit")]
        public int TargetGrossProfit { get; set; }

        [DataMember(Name = "estimatedRecon")]
        public int EstimatedRecon { get; set; }

        [DataMember(Name = "targetSellingPrice")]
        public int TargetSellingPrice { get; set; }

        [DataMember(Name = "bookValuationRequests")]
        public IList<BookEvaluationRequest> BookValuationRequests { get; set; }

        [DataMember(Name = "mmrVehicle", IsRequired=false, EmitDefaultValue=false)]
        public MMRVehicle MmrVehicle { get; set; }

        [DataMember(Name = "salespersonId")]
        public int SalespersonId { get; set; }

        [DataMember(Name = "uniqueID")]
        public string UniqueID { get; set; }

        [DataMember(Name = "reconNotes")]
        public string ReconNotes { get; set; }

    }
}