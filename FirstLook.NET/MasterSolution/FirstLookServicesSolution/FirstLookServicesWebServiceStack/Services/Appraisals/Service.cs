﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Commands;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Appraisals.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Valuations.Commands.TransferObjects;
using FirstLook.FirstLookServices.WebServiceStack.Services.Appraisals.Models;
using FirstLook.FirstLookServices.WebServiceStack.Services.Valuations;
using FirstLook.FirstLookServices.WebServiceStack.Services.Valuations.Models;
using FirstLook.FirstLookServices.WebServiceStack.Utility;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;
using System;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Appraisals
{
    [Authenticate]
    public class Service : ServiceStack.ServiceInterface.Service
    {
        readonly VersionMapper<Appraisal> _appraisalsMapper = new VersionMapper<Appraisal>("v1");

        readonly VersionMapper<AppraisalsFetch> _appraisalsFetchMapper = new VersionMapper<AppraisalsFetch>("v1");

        readonly VersionMapper<AppraisalTypesFetch> _appraisalTypesFetchMapper = new VersionMapper<AppraisalTypesFetch>("v1");

        public Service()
        {
            _appraisalsMapper.Add("v1", "post", Post_v1);

            _appraisalsFetchMapper.Add("v1", "get", Get_v1);

            _appraisalTypesFetchMapper.Add("v1", "get", Get_v1);
        }

        public object Post(Appraisal request)
        {
           try
            {
                return _appraisalsMapper.GetServiceFunction(request.Ver, "post")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public object Get(AppraisalsFetch request)
        {
            try
            {
                return _appraisalsFetchMapper.GetServiceFunction(request.Ver, "get")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public object Get(AppraisalTypesFetch request)
        {
            try
            {
                return _appraisalTypesFetchMapper.GetServiceFunction(request.Ver, "get")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public IHttpResult Post_v1(Appraisal request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            var command = factory.CreateSaveAndPostAppraisalCommand();

            int? mileage = null;

            if (request.BookValuationRequests.Count > 0)
            {
                mileage = request.BookValuationRequests[0].BookValuationRequest.Mileage;
            }

            var arguments = new SaveAppraisalArgumentsDto
            {
                Appraisal = new AppraisalDto
                                {
                                    AppraisalType = request.AppraisalType == "T" ? AppraisalTypeDto.Trade : AppraisalTypeDto.Purchase,
                                    User = this.GetSession().UserAuthName,
                                    DealerId = request.Dealer,
                                    UniqueId=request.UniqueID,
                                    Vehicle = new VehicleDto
                                                  {
                                                      Color = request.Color,
                                                      //Below change will convert the lower case to upper case
                                                      //to avoid issues in DB,S3 Doc and SQS queue
                                                      //Modified on 01/09/2014
                                                      Vin = request.Vin.ToUpper(),
                                                      Mileage = mileage
                                                  },
                                    AppraisalAmount = request.AppraisalAmount,
                                    EstimatedRecon = request.EstimatedRecon,
                                    TargetGrossProfit = request.TargetGrossProfit,
                                    TargetSellingPrice = request.TargetSellingPrice,
                                    MmrVehicle = (request.MmrVehicle != null && ( request.MmrVehicle.Mid != null && request.MmrVehicle.RegionCode !=null)
                                        ? new MMRVehicleDto{
                                            trimId = request.MmrVehicle.Mid,
                                            area = request.MmrVehicle.RegionCode
                                            }
                                        : null),
                                   SalespersonId=request.SalespersonId,
                                   ReconNotes=request.ReconNotes
                                },
                 BookEvaluationRequests = Map(request)
            };

            SaveAppraisalResultsDto results = Helper.ExecuteCommand(command, Helper.WrapInContext(arguments, this.GetSession().UserAuthName));

            var response = Map(results.AppraisalSummary);

            IHttpResult result = Helper.Compress(this, response);

            result.StatusCode = HttpStatusCode.Created;

            result = Helper.SetStaticHeaders(result, new[]{ "cacheControlNoCache" });

            return result;
        }

        public IHttpResult Get_v1(AppraisalsFetch request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            string oldETag = this.RequestContext.GetHeader("If-None-Match");

            var command = factory.CreateFetchAppraisalsCommand();

            int? days = (request.Days == 0 || request.Days == null) ? null : request.Days;

            var arguments = new FetchAppraisalsArgumentsDto
                                {
                Days = days,
                DealerId = request.Dealer
            };

            var results = Helper.ExecuteCommand(command, Helper.WrapInContext(arguments, this.GetSession().UserAuthName));

            var response = new AppraisalsFetchResponse
                               {
                                   AppraisalSummaries = Map(results.AppraisalSummaries)
                               };

            IHttpResult result = Handle304(oldETag, response, true);

            // ETag does not match so we need to send to updated resource
            Helper.SetStaticHeaders(result, new string[] { "cacheControlZero" });

            return result;
        }

        public IHttpResult Get_v1(AppraisalTypesFetch request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            string oldETag = RequestContext.GetHeader("If-None-Match");

            var command = factory.CreateFetchAppraisalTypesCommand();

            var arguments = new FetchAppraisalTypesArgumentsDto();

            var results = Helper.ExecuteCommand(command, arguments);

            IList<AppraisalType> types = results.AppraisalTypes.Select(x => new AppraisalType
                                                                 {
                                                                     Description = x,
                                                                     Type = x.Substring(0, 1)
                                                                 }).ToList();

            var response = new AppraisalTypesFetchResponse
                               {
                                   AppraisalTypes = (List<AppraisalType>) types
                               };

            IHttpResult result = Handle304(oldETag, response, true);

            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                result = Helper.SetStaticHeaders(result, new[] { "referenceDataTrue", "cacheControlDay" });
            }

            return result;
        }

        #region mapper methods

        private static IList<BookEvaluationRequestDto> Map(Appraisal request)
        {
            if (request.BookValuationRequests == null || request.BookValuationRequests.Count == 0)
            {
                return null;
            }

            return request.BookValuationRequests.Select(bookEvaluationRequest =>
                                    {
                                        BookValuationRequest bookValuationRequest = bookEvaluationRequest.BookValuationRequest;

                                        return new BookEvaluationRequestDto
                                                                        {
                                                                            CheckSum = bookEvaluationRequest.CheckSum,
                                                                            BookValuationRequest =
                                                                                new BookValuationRequestDto
                                                                                    {
                                                                                        Book = bookValuationRequest.Book,
                                                                                        EquipmentUserActions = Map(bookValuationRequest.EquipmentExplicitActions),
                                                                                        EquipmentSelected = bookValuationRequest
                                                                                            .EquipmentSelected
                                                                                            .Select(option => option.ToString())
                                                                                            .ToList(),
                                                                                        Mileage = bookValuationRequest.Mileage,
                                                                                        ReturnId = bookValuationRequest.ReturnId,
                                                                                        TrimId = bookValuationRequest.TrimId
                                                                                    }
                                                                        };
                                    }).ToList();
        }

        private static IList<EquipmentUserActionDto> Map(IEnumerable<EquipmentUserAction> equipmentUserActions)
        {
            return equipmentUserActions.Select(equipmentUserAction => new EquipmentUserActionDto
                                                                          {
                                                                              Action = equipmentUserAction.Action == "A" 
                                                                              ? EquipmentActionTypeDto.Add 
                                                                              : EquipmentActionTypeDto.Remove, 
                                                                              Equipment = equipmentUserAction.Equipment
                                                                          }).ToList();
        }

        public static AppraisalSummary Map(AppraisalSummaryDto dto)
        {
            if (dto == null)
            {
                return null;
            }

            return new AppraisalSummary
            {
                AppraisalAmount = dto.AppraisalAmount,
                Color = dto.Vehicle.Color,
                DateCreated = dto.DateCreated.ToUniversalTime().ToString("yyyyMMdd'T'HHmmss'Z'"),
                Description = dto.Vehicle.Description,
                Vin = dto.Vehicle.Vin,
                AppraisalType =
                    dto.AppraisalType == AppraisalTypeDto.Trade
                        ? "T"
                        : "P",
                Href = dto.Href,
                SalespersonId=dto.SalespersonId,
            };
        }

        public List<AppraisalSummary> Map(IList<AppraisalSummaryDto> dtos)
        {
            return dtos.Select(Map).ToList();
        }

        #endregion

        IHttpResult Handle304(string oldETag, object response, bool compress)
        {
            string newETag = ETagHelper.CreateETagHash(response);

            IHttpResult result = new HttpResult();

            if (oldETag != null && oldETag.Equals(newETag))
            {
                //  ETags match so no need to zip and send the whole response - just headers
                result.StatusCode = HttpStatusCode.NotModified;
            }
            else
            {
                // ETag does not match so we need to send to updated resource
                // 
                if (compress)
                    result = Helper.Compress(this, response);
                else
                    result = new HttpResult
                    {
                        Response = response
                    };

                ETagHelper.AddETagHeader(result, response);

                result.StatusCode = HttpStatusCode.OK;
            }
            return result;
        }
    }
}