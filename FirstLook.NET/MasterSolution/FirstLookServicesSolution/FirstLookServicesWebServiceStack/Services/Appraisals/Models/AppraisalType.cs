﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Appraisals.Models
{
    [DataContract]
    public class AppraisalType
    {
        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "desc")]
        public string Description { get; set; }
    }
}