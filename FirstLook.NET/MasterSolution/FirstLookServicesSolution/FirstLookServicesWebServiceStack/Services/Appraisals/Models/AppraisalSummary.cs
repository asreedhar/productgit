﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Appraisals.Models
{
    [DataContract]
    public class AppraisalSummary
    {
        [DataMember(Name = "href")]
        public string Href { get; set; }

        [DataMember(Name = "dateCreated")]
        public string DateCreated { get; set; }

        [DataMember(Name = "vin")]
        public string Vin { get; set; }

        [DataMember(Name = "desc")]
        public string Description { get; set; }

        [DataMember(Name = "color")]
        public string Color { get; set; }

        [DataMember(Name = "appraisalType")]
        public string AppraisalType { get; set; }

        [DataMember(Name = "appraisalAmount")]
        public int? AppraisalAmount { get; set; }

        [DataMember(Name = "salespersonId")]
        public int SalespersonId { get; set; }
    }
}