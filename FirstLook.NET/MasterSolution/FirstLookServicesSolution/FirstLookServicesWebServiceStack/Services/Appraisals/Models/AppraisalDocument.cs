﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using FirstLook.FirstLookServices.WebServiceStack.Services.Valuations.Models;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Appraisals.Models
{
    [DataContract]
    public class AppraisalDocument
    {
        [DataMember (Name = "appraisalId")]
        public int AppraisalId { get; set; }

        [DataMember(Name = "publicationIds")]
        public IList<int> PublicationIds { get; set; }

        [DataMember(Name = "href")]
        public string Href { get; set; }

        [DataMember(Name = "dateCreated")]
        public string DateCreated { get; set; }

        [DataMember(Name = "user")]
        public string User { get; set; }

        [DataMember(Name = "appraisalRequest")]
        public AppraisalRequest AppraisalRequest { get; set; }

        [DataMember(Name = "valuations")]
        public IList<BookValuation> BookValuations { get; set; }

        [DataMember(Name = "mmrVehicle")]
        public MMRVehicle MmrVehicle { get; set; }
    }
}