﻿using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Appraisals
{
    [Route("/{ver}/appraisals/dealers/{dealer}/vins/{vin}")]
    public class Appraisal : AppraisalRequest
    {
        public int Dealer { get; set; }
        public string Vin { get; set; }
        public string Ver { get; set; }
    }

    [Route("/{ver}/appraisals/dealers/{dealer}")]
    public class AppraisalsFetch {

        public int Dealer { get; set; }
        public int? Days { get; set; }
        public string Ver { get; set; }    
    }

    [Route("/{ver}/appraisals/types")]
    public class AppraisalTypesFetch
    {
        public string Ver { get; set; }    
    }

}