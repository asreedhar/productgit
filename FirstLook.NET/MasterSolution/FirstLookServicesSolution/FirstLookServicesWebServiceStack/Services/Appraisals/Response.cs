﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using FirstLook.FirstLookServices.WebServiceStack.Services.Appraisals.Models;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Appraisals
{
    [DataContract]
    public class AppraisalsFetchResponse
    {
        [DataMember(Name = "appraisalSummaries")]
        public List<AppraisalSummary> AppraisalSummaries { get; set; }
    }

    [DataContract]
    public class AppraisalTypesFetchResponse
    {
        [DataMember(Name = "types")]
        public List<AppraisalType> AppraisalTypes { get; set; }
    }

}