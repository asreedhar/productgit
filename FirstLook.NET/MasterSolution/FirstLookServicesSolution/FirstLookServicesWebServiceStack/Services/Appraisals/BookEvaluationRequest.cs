﻿using System.Runtime.Serialization;
using FirstLook.FirstLookServices.WebServiceStack.Services.Valuations;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Appraisals
{
    [DataContract]
    public class BookEvaluationRequest
    {
        [DataMember(Name = "checkSum")]
        public string CheckSum { get; set; }

        [DataMember(Name = "bookValuationRequest")]
        public BookValuationRequest BookValuationRequest { get; set; }

    }
}