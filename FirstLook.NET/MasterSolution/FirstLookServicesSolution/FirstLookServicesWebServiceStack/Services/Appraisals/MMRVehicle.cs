using System.Collections.Generic;
using System.Runtime.Serialization;
using FirstLook.FirstLookServices.WebServiceStack.Services.Appraisals.Models;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Appraisals
{
    [DataContract]
    public class MMRVehicle
    {
        [DataMember(Name = "area")]
        public string RegionCode { get; set; }

        [DataMember(Name = "trimId")]
        public string Mid { get; set; }

    }
}