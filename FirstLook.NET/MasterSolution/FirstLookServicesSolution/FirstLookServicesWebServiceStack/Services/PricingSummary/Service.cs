﻿using System;
using System.Linq;
using System.Net;
using System.Collections.Generic;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands;
using FirstLook.FirstLookServices.DomainModel.PricingSummary.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.WebServiceStack.Services.PricingSummary.Models;
using FirstLook.FirstLookServices.WebServiceStack.Utility;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.PricingSummary
{
    [Authenticate]
    public class Service : ServiceStack.ServiceInterface.Service
    {
        private readonly VersionMapper<PricingSumary> _pricingSummaryMapper = new VersionMapper<PricingSumary>("v1");

        public Service()
        {
            _pricingSummaryMapper.Add("v1", "post", Post_v1);

        }
        public object Post(PricingSumary request)
        {
            try
            {
                return _pricingSummaryMapper.GetServiceFunction(request.Ver, "post")(request);
            }
            catch (Exception ex)
            {
                {
                    var err = this.ExceptionToError(ex, null);
                    return (err != null) ? this.HandleError(err) : null;
                }
            }

        }

        public object Post_v1(PricingSumary request)
        {

            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            string oldETag = RequestContext.GetHeader("If-None-Match");
            var command = factory.CreateFetchPricingSummaryCommand();
            var arguments = new FetchPricingSummaryArgumentsDto()
            {
                DealerId = request.DealerId,
                Mode = request.Mode,
                SalesStrategy = request.SalesStrategy,
                ColumnIndex = request.ColumnIndex,
                SortColumns = request.SortColumns,
                MaximumRows = request.MaximumRows,
                StartPageIndex = request.StartPageIndex,
                InventoryFilter = request.InventoryFilter
            };

            FetchPricingSummaryResultsDto results = Helper.ExecuteCommand(command, Helper.WrapInContext(arguments, this.GetSession().UserAuthName));
            var response = new PricingSummary();
            response.OwnerName = results.OwnerName;
            response.BucketName = results.BucketName;
            var dealerBuckets = results.PricingGraph.Buckets.Select(b => new BucketDto()
            {
                BucketId = b.BucketId,
                BucketName = b.BucketName,
                BucketPercentage = b.BucketPercentage,
                Bucketunits = b.Bucketunits
            }).ToList();
            response.PricingGraph = new Graph()
            {
                Buckets = dealerBuckets
            };

            response.PringGraphSummary = new GraphSummary()
            {
                UnitsInStock = results.PringGraphSummary.UnitsInStock,
                AvgInternetPrice = results.PringGraphSummary.AvgInternetPrice,
                PctMarketAverage = results.PringGraphSummary.PctMarketAverage,
                MarketDaysSupply = results.PringGraphSummary.MarketDaysSupply,
                DealerFavourableThreshold = results.PringGraphSummary.DealerFavourableThreshold
            };


            var inventories = results.PricingInventoryTable.Inventory.Select(i => new InventoryDto()
            {
                Age = i.Age,
                VehicleDescription = i.VehicleDescription,
                Make = i.Make,
                VehicleColor = i.VehicleColor,
                VehicleMileage = i.VehicleMileage,
                UnitCost = i.UnitCost,
                InternetPrice = i.InternetPrice,
                PctMarketAverage = i.PctMarketAverage,
                MarketDaysSupply = i.MarketDaysSupply,
                StockNumber = i.StockNumber,
                InventoryId = i.InventoryId,
                TrimList = i.TrimList,
                PriceComparisons = i.PriceComparisons.Select(il => new BookDto { BookName = il.BookName, BookValue = il.BookValue }).ToList(),
                ComparisonColor=i.ComparisonColor,
                AgeBucket = i.AgeBucket
            }).ToList();

            response.PricingInventoryTable = new InventoryTable()
            {
                Inventory = inventories
            };

            IHttpResult result = Handle304(oldETag, response, true);

            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                result = Helper.SetStaticHeaders(result, new[] { "cacheControlHour" });
            }

            return result;

        }
        IHttpResult Handle304(string oldETag, object response, bool compress)
        {
            string newETag = ETagHelper.CreateETagHash(response);

            IHttpResult result = new HttpResult();

            if (oldETag != null && oldETag.Equals(newETag))
            {
                //  ETags match so no need to zip and send the whole response - just headers
                result.StatusCode = HttpStatusCode.NotModified;
            }
            else
            {
                // ETag does not match so we need to send to updated resource
                // 
                if (compress)
                    result = Helper.Compress(this, response);
                else
                    result = new HttpResult
                    {
                        Response = response
                    };

                ETagHelper.AddETagHeader(result, response);

                result.StatusCode = HttpStatusCode.OK;
            }
            return result;
        }
    }
}