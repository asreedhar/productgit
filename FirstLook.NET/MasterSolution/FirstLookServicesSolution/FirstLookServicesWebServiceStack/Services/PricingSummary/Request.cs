﻿using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.PricingSummary
{
    [Route("/{ver}/pricingSummary/DealerId/{DealerId}")]
        [Route("/pricingSummary/DealerId/{DealerId}")]
        public class PricingSumary
        {
            public int DealerId { get; set; }

            public string Mode { get; set; }

            public string SalesStrategy { get; set; }

            public int ColumnIndex { get; set; }            

            public string SortColumns { get; set; }

            public int MaximumRows { get; set; }

            public int StartPageIndex { get; set; }

            public string InventoryFilter { get; set; }

            public bool IsAggregateAndDetail { get; set; }

            public string Ver { get; set; }       
    }
}