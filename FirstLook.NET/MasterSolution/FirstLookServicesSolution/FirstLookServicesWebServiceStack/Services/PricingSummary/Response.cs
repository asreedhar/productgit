﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using FirstLook.FirstLookServices.WebServiceStack.Services.PricingSummary.Models;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.PricingSummary
{   
        [DataContract]
        public class PricingSummary
        {
            [DataMember(Name = "ownerName")]
            public string OwnerName { get; set; }

            [DataMember(Name = "pricingGraph")]
            public Graph PricingGraph { get; set; }

            [DataMember(Name = "pringGraphSummary")]
            public GraphSummary PringGraphSummary { get; set; }

            [DataMember(Name = "pricingInventoryTable")]
            public InventoryTable PricingInventoryTable { get; set; }

            [DataMember(Name = "bucketName")]
            public string BucketName { get; set; }
        }
    
}