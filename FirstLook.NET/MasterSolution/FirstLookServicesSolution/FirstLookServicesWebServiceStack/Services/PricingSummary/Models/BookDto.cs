﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.PricingSummary.Models
{
    [DataContract]
    public class BookDto
    {
        [DataMember(Name = "bookName")]
        public string BookName { get; set; }

        [DataMember(Name = "bookValue")]
        public int BookValue { get; set; }
    }
}