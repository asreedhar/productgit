﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.PricingSummary.Models
{
    [DataContract]
    public class BucketDto
    {
        [DataMember(Name = "bucketId")]
        public int BucketId { get; set; }

        [DataMember(Name = "bucketName")]
        public string BucketName { get; set; }

        [DataMember(Name = "bucketunits")]
        public int Bucketunits { get; set; }

        [DataMember(Name = "bucketPercentage")]
        public decimal BucketPercentage { get; set; }
    }
}