﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.PricingSummary.Models
{
    [DataContract]
    public class InventoryTable
    {
        [DataMember(Name = "inventory")]
        public List<InventoryDto> Inventory { get; set; } 
    }
}