﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.PricingSummary.Models
{
    [DataContract]
    public class Graph
    {
       [DataMember(Name = "buckets")]
       public List<BucketDto> Buckets { get; set; }
    }
}