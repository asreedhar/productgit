﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.PricingSummary.Models
{
    [DataContract]
    public class GraphSummary
    {
        [DataMember(Name = "unitsInStock")]
        public int UnitsInStock { get; set; }

        [DataMember(Name = "avgInternetPrice")]
        public int AvgInternetPrice { get; set; }

        [DataMember(Name = "pctMarketAverage")]
        public int PctMarketAverage { get; set; }

        [DataMember(Name = "marketDaysSupply")]
        public int MarketDaysSupply { get; set; }

        [DataMember(Name = "dealerFavourableThreshold")]
        public int DealerFavourableThreshold { get; set; }
    }
}