﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using FirstLook.Common.Core.Registry;
using FirstLook.Fault.DomainModel.Utilities;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Mmr;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Sales;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Sales.Envelopes;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model.Sale;
using FirstLook.FirstLookServices.DomainModel.Exceptions;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands;
using FirstLook.FirstLookServices.DomainModel.Auctions.Commands.TransferObjects.Naaa.Envelopes;
using FirstLook.FirstLookServices.WebServiceStack.Services.Auctions.Models;
using FirstLook.FirstLookServices.WebServiceStack.Services.Auctions.Models.Mmr;
using FirstLook.FirstLookServices.WebServiceStack.Services.Auctions.Models.Naaa;

using FirstLook.FirstLookServices.WebServiceStack.Utility;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;
using FirstLook.FirstLookServices.DomainModel.BookMapping;

using System;
using Mileages = FirstLook.FirstLookServices.WebServiceStack.Services.Auctions.Models.Mmr.Mileages;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Auctions
{
    using FirstLook.FirstLookServices.DomainModel.Validation;
    [Authenticate]
    public class Service : ServiceStack.ServiceInterface.Service
    {
        readonly VersionMapper<Auctions> auctionsMapper = new VersionMapper<Auctions>("v1");
        readonly VersionMapper<Reference> referenceMapper = new VersionMapper<Reference>("v1");
        readonly VersionMapper<MMRReference> MMRReferenceMapper = new VersionMapper<MMRReference>("v1");
        readonly VersionMapper<GetMMRAuctionSummary> getMMRAuctionSummaryMapper = new VersionMapper<GetMMRAuctionSummary>("v1");
        readonly VersionMapper<GetMmrAuctionTransactions> getMMRAuctionTransactionsMapper = new VersionMapper<GetMmrAuctionTransactions>("v1");
        readonly VersionMapper<GetAuctionsDetails>  getAuctionsDetails = new VersionMapper<GetAuctionsDetails>("v1");
        readonly VersionMapper<GetAuctionVehicles> getAuctionVehicles = new VersionMapper<GetAuctionVehicles>("v1");
        readonly VersionMapper<GetAuctionLanes> getAuctionLanes = new VersionMapper<GetAuctionLanes>("v1");
        public Service()
        {
            auctionsMapper.Add("v1", "get", Get_v1);
            referenceMapper.Add("v1", "get", Get_v1);
            MMRReferenceMapper.Add("v1", "get", Get_v1);
            getMMRAuctionSummaryMapper.Add("v1", "get", Get_v1);
            getMMRAuctionTransactionsMapper.Add("v1", "get", Get_v1);
			getAuctionsDetails.Add("v1", "get", Get_v1);
            getAuctionVehicles.Add("v1","get",Get_v1);
            getAuctionLanes.Add("v1", "get", Get_v1);
        }

        public object Get(Auctions request)
        {
            try
            {
                return auctionsMapper.GetServiceFunction(request.Ver, "get")(request);
            }
            catch (Exception ex)
            {
                FaultWebExceptionObserver.SaveException(ex);

                var err = this.ExceptionToError(ex, null);

                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public IHttpResult Get_v1(Auctions request)
        {
            switch (request.Id)
            {
                case "naaa":
                {

                    if (!string.IsNullOrEmpty(request.Vin))
                    {
                        if (!this.ValidVin(request.Vin))
                            throw new InvalidInputException("Vin", request.Vin, "Bad Vin: " + request.Vin);

                        return AuctionResults(LoadUids(request.Vin), request.Area, request.Period, request.Mileage ?? 0);
                    }
                    if( request.Style.HasValue )
                        return AuctionResults( LoadUids(request.Style.Value), request.Area, request.Period, request.Mileage );

                    return new HttpResult();
                }
                default:
                {
                    return new HttpResult();
                }
            }
        }

        public object Get(Reference request)
        {

            try
            {
                return referenceMapper.GetServiceFunction(request.Ver, "get")(request);
            }
            catch (Exception ex)
            {
                FaultWebExceptionObserver.SaveException(ex);

                var err = this.ExceptionToError(ex, null);

                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public IHttpResult Get_v1(Reference request)
        {
            switch (request.Id)
            {
                case "naaa":
                    {
                        return NaaaReference();
                    }
                default:
                    {
                        return null;
                    }
            }
        }

        public object Get(MMRReference request)
        {
            try
            {
                return MMRReferenceMapper.GetServiceFunction(request.Ver, "get")(request);
            }
            catch (Exception ex)
            {
                FaultWebExceptionObserver.SaveException(ex);

                var err = this.ExceptionToError(ex, null);

                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public IHttpResult Get_v1(MMRReference request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            string oldETag = this.RequestContext.GetHeader("If-None-Match");
            var command = factory.CreateFetchMmrReferenceCommand();

            MmrReferenceResultsDto results = command.Execute(null);

            MMRReferenceResponse response = new MMRReferenceResponse();
            List<MMRArea> areas = new List<MMRArea>();

            foreach (MmrAreaDto area in results.Areas)
            {
                areas.Add(new MMRArea
                {
                    Id = area.Id,
                    Name = area.Name,
                });
            }

            response.Areas = areas;

            IHttpResult result = handle304(oldETag, response, true);
            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                Helper.SetStaticHeaders(result, new string[] { "referenceDataTrue", "cacheControlDay" });
            }

            return result;
        }

        public object Get(GetMMRAuctionSummary request)
        {
            try
            {
                return getMMRAuctionSummaryMapper.GetServiceFunction(request.Ver, "get")(request);
            }
            catch (Exception ex)
            {
                FaultWebExceptionObserver.SaveException(ex);

                var err = this.ExceptionToError(ex, null);

                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public IHttpResult Get_v1(GetMMRAuctionSummary request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            string oldETag = this.RequestContext.GetHeader("If-None-Match");
            var command = factory.CreateFetchMmrAuctionSummaryCommand();

            var area = "NA";

            if (String.IsNullOrEmpty(request.Area) == false)
            {
                area = request.Area;
            }

            var argumentsDto = new FetchMMRAuctionSummaryArgumentsDto {Vin = request.Vin, Area = area};
            FetchMMRAuctionSummaryResultsDto results = command.Execute(argumentsDto);

            GetMMRVehiclesResponse response = new GetMMRVehiclesResponse();
            List<MMRVehicle> vehicles = new List<MMRVehicle>();

            foreach (MMRVehicleDto mmrveh in results.Vehicles)
            {
                string lateDate = "";

                if (mmrveh.LatestSaleDate != null) lateDate = mmrveh.LatestSaleDate.Value.ToString("yyyyMMddTHHmmssZ");

                string earlyDate = "";

                if (mmrveh.EarliestSaleDate != null) earlyDate = mmrveh.EarliestSaleDate.Value.ToString("yyyyMMddTHHmmssZ");

                vehicles.Add(new MMRVehicle
                {
                    TrimId = mmrveh.Mid,
                    Area = mmrveh.Region,
                    Make = mmrveh.Make,
                    Mileages = new Mileages {Average = mmrveh.Mileages.Average, Max = mmrveh.Mileages.Max, Min = mmrveh.Mileages.Min, SampleSize = mmrveh.Mileages.SampleSize},
                    Model = mmrveh.Model,
                    SalesPrices = new Prices {Average = mmrveh.Prices.Average, Max = mmrveh.Prices.Max, Min = mmrveh.Prices.Min, SampleSize = mmrveh.Prices.SampleSize},
                    Trim = mmrveh.Trim,
                    Year = mmrveh.Year,
                    LatestSaleDate = lateDate,
                    EarliestSaleDate = earlyDate
                });
            }

            response.Vehicles = vehicles;

            IHttpResult result = handle304(oldETag, response, true);
            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                Helper.SetStaticHeaders(result, new string[] { "cacheControlDay" });
            }

            return result;
        }

        public object Get(GetMmrAuctionTransactions request)
        {
            try
            {
                return getMMRAuctionTransactionsMapper.GetServiceFunction(request.Ver, "get")(request);
            }
            catch (Exception ex)
            {
                FaultWebExceptionObserver.SaveException(ex);

                var err = this.ExceptionToError(ex, null);

                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public IHttpResult Get_v1(GetMmrAuctionTransactions request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            string oldETag = this.RequestContext.GetHeader("If-None-Match");
            var command = factory.CreateFetchMmrAuctionTransactionsCommand();

            var area = "NA";

            if (String.IsNullOrEmpty(request.Area) == false)
            {
                area = request.Area;
            }

            var argumentsDto = new FetchMMRAuctionTransactionsArgumentsDto { Mid = request.TrimId, Area = area, Year = request.Year};
            FetchMMRAuctionTransactionsResultsDto results = command.Execute(argumentsDto);

            List<MmrTransaction> transactions = new List<MmrTransaction>();

            foreach (MmrTransactionDto t in results.Transactions)
            {
                transactions.Add(new MmrTransaction
                {
                    Date = t.Date,
                    Auction = t.Auction,
                    SalesType = t.SalesType,
                    Price = t.Price,
                    Mileage = t.Mileage,
                    Condition = t.Condition,
                    Color = t.Color,
                    Engine = t.Engine,
                    Transmission = t.Transmission,
                    InSample = t.InSample
                });
            }

            GetMmrAuctionTransactionResponse response = new GetMmrAuctionTransactionResponse
            {
                TrimId = results.Mid,
                Area = results.Area,
                Year = results.Year,
                Transactions = transactions
            };

            IHttpResult result = handle304(oldETag, response, true);
            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                Helper.SetStaticHeaders(result, new string[] { "cacheControlDay" });
            }

            return result;
        }

		public object Get(GetAuctionsDetails request)
        {
            try
            {
                return getAuctionsDetails.GetServiceFunction(request.Ver, "get")(request);
            }
            catch (Exception ex)
            {
                FaultWebExceptionObserver.SaveException(ex);

                var err = this.ExceptionToError(ex, null);

                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public IHttpResult Get_v1(GetAuctionsDetails request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            string oldETag = this.RequestContext.GetHeader("If-None-Match");
            var command = factory.CreateFetchSalesDetailsCommand();

            var argumentsDto = new FetchSalesArgumentsDto() {Lat = request.Lat , Long = request.Long};
            FetchSalesResultsDto results = command.Execute(argumentsDto);

            List<Sales> nearSales = results.NearSales.Select(
                    d =>
                        new Sales
                        {
                            State = d.State,
                            Location =
                                d.Location.Select(
                                    x =>
                                new Location()
                                {
                                    LocationId = x.LocationId,
                                    LocationDescription = x.LocationDescription,
                                    SalesDate = x.SalesDate
                                }).ToList()
                        }).ToList();

            List<Sales> allSales = results.AllSales.Select(
                               d =>
                                   new Sales
                                   {
                                       State = d.State,
                                       Location =
                                           d.Location.Select(
                                               x =>
                                           new Location()
                                           {
                                               LocationId = x.LocationId,
                                               LocationDescription = x.LocationDescription,
                                               SalesDate = x.SalesDate
                                           }).ToList()
                                   }).ToList();
            //foreach (SalesDto t in results.NearSales)
            //{
            //    nearSales.Add(new Sales()
            //    {
            //       State = t.State,
            //       Location = new Location() {LocationId = t.Location.LocationId , LocationDescription = t.Location.LocationDescription},
            //       SalesDate = t.SalesDate
            //    });
            //}
            //foreach (SalesDto t in results.AllSales)
            //{
            //    allSales.Add(new Sales()
            //    {
            //        State = t.State,
            //        Location = new Location() { LocationId = t.Location.LocationId, LocationDescription = t.Location.LocationDescription },
            //        SalesDate = t.SalesDate
            //    });
            //}
            GetAuctionDataResponse response = new GetAuctionDataResponse
            {
                NearSales = nearSales,
                AllSales = allSales
            };

            IHttpResult result = handle304(oldETag, response, true);
            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                Helper.SetStaticHeaders(result, new string[] { "cacheControlMin" });
            }

            return result;
          
        }

        public object Get(GetAuctionVehicles request)
        {
            try
            {
                return getAuctionVehicles.GetServiceFunction(request.Ver, "get")(request);
            }
            catch (Exception ex)
            {
                FaultWebExceptionObserver.SaveException(ex);

                var err = this.ExceptionToError(ex, null);

                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public IHttpResult Get_v1(GetAuctionVehicles request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            string oldETag = this.RequestContext.GetHeader("If-None-Match");
            var command = factory.CreateFetchAuctionVehiclesCommand();

            var argumentsDto = new FetchVehiclesArgumentsDto(){ Date = request.Date ,DealerId = request.DealerId, LocationId = request.LocationId , Lane = request.Lane};
            FetchVehiclesResultsDto results = command.Execute(argumentsDto);

            
            GetAuctionVehicleResponse response = new GetAuctionVehicleResponse
            {
                AuctionVehicles = results.Vehicles.Select(obj => new Vehicle(){ Color = obj.Color,Engine = obj.Engine,MakeModelTrimStyle = obj.MakeModelTrimStyle,Mileage = obj.Mileage,RunNumber = obj.RunNumber,Vin = obj.Vin,Year = obj.Year}).ToList()
            };

            IHttpResult result = handle304(oldETag, response, true);
            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                Helper.SetStaticHeaders(result, new string[] { "cacheControlMin" });
            }

            return result;

        }
        public object Get(GetAuctionLanes request)
        {
            try
            {
                return getAuctionLanes.GetServiceFunction(request.Ver, "get")(request);
            }
            catch (Exception ex)
            {
                FaultWebExceptionObserver.SaveException(ex);

                var err = this.ExceptionToError(ex, null);

                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public IHttpResult Get_v1(GetAuctionLanes request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            string oldETag = this.RequestContext.GetHeader("If-None-Match");
            var command = factory.CreateFetchAuctionLanesCommand();

            var argumentsDto = new FetchLanesArgumentsDto() { Date = request.Date,  LocationId = request.Location };
            FetchLanesResultsDto results = command.Execute(argumentsDto);


            GetAuctionLanesResponse response = new GetAuctionLanesResponse
            {
                LaneDetails = new LaneDetails()
                              {
                                  LocationId = results.Lanes.Select(x => x.LocationId).FirstOrDefault(), SaleDate = results.Lanes.Select(x => x.SaleDate).FirstOrDefault() ,
                                  Lane = results.Lanes.Select(x => x.Lane).ToList()
                              }
            };

            IHttpResult result = handle304(oldETag, response, true);
            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                Helper.SetStaticHeaders(result, new string[] { "cacheControlMin" });
            }

            return result;

        }
        private IHttpResult NaaaReference()
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            string oldETag = this.RequestContext.GetHeader("If-None-Match");

            var command = factory.CreateFetchNaaaReferenceCommand();

            var arguments = new FetchNaaaReferenceArgumentsDto();

            FetchNaaaReferenceResultsDto results = Helper.ExecuteCommand(command, arguments);

            IList<Area> areas = results.Areas.Select(area => new Area { Id = area.Id, Name = area.Name }).ToList();

            IList<Period> periods = results.Periods.Select(period => new Period { Id = period.Id, Name = period.Name }).ToList();

            var response = new ReferenceResponse
                               {
                                   Areas = (List<Area>) areas,
                                   Periods = (List<Period>) periods
                               };

            IHttpResult result = handle304(oldETag, response, true);
            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                result = Helper.SetStaticHeaders(result, new string[] { "referenceDataTrue", "cacheControlDay" } );
            }            
            
            return result;
        }

        private const int DEFAULT_AREA = 1; // National
        private const int DEFAULT_PERIOD = 11; // Two weeks

        private List<Tuple<int, bool>> LoadUids(string vin)
        {
            IBookMapper bookMapper = RegistryFactory.GetResolver().Resolve<IBookMapper>();

            var uids = bookMapper.NadaTrimIds(vin).ToList();

            //var uids = new List<Tuple<int,bool>>();
            if(uids.Count == 0){ throw new NoDataFoundException("UID"); }
            return uids;
        }

        private List<Tuple<int, bool>> LoadUids(int style)
        {
            IBookMapper bookMapper = RegistryFactory.GetResolver().Resolve<IBookMapper>();
            var uids = bookMapper.NadaTrimIds(style).ToList();
            if(uids.Count == 0){ throw new NoDataFoundException("UID"); }
            return uids;
        }

        private IHttpResult AuctionResults(List<Tuple<int, bool>> uids, int? area, int? period, int? mileage)
        {
            string oldETag = this.RequestContext.GetHeader("If-None-Match");

            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            var command = factory.CreateFetchNaaaTransactionSummaryCommand();
            int uid = uids.First().Item1;

            var arguments = new FetchNaaaTransactionSummaryArgumentsDto()
            {
                Uid = uid,
                Area = (area.HasValue ? area.Value : DEFAULT_AREA),
                Period = (period.HasValue ? period.Value : DEFAULT_PERIOD),
                Mileage = (mileage.HasValue ? mileage.Value : 0)
            };

            FetchNaaaTransactionSummaryResultsDto results = command.Execute(Helper.WrapInContext(arguments, HttpContext.Current.User.Identity.Name));

            var response = new AuctionsResponse
            {
                Area = arguments.Area,
                Period = arguments.Period,
                LowMileage = results.NaaaTransactionSummary.LowMileage,
                HighMileage = results.NaaaTransactionSummary.HighMileage,
                SalePrices = new SalePriceAggregation
                {
                    Max = results.NaaaTransactionSummary.MaxSalePrice,
                    Average = results.NaaaTransactionSummary.AvgSalePrice,
                    Min = results.NaaaTransactionSummary.MinSalePrice,
                    SampleSize = results.NaaaTransactionSummary.SalePriceSampleSize
                },
                SalePricesSimilarMileage = new SalePriceAggregation
                {
                    Max = results.NaaaTransactionSummary.MaxSalePriceSimilarMileage,
                    Average = results.NaaaTransactionSummary.AvgSalePriceSimilarMileage,
                    Min = results.NaaaTransactionSummary.MinSalePriceSimilarMileage,
                    SampleSize = results.NaaaTransactionSummary.SalePriceSimilarMileageSampleSize
                },
                Mileages = new MileagesAggregation
                {
                    Max = results.NaaaTransactionSummary.MaxMileage,
                    Average = results.NaaaTransactionSummary.AvgMileage,
                    Min = results.NaaaTransactionSummary.MinMileage,
                    SampleSize = results.NaaaTransactionSummary.MileageSampleSize
                }
            };

            IHttpResult result = handle304(oldETag, response, true);
            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                result = Helper.SetStaticHeaders(result, new string[] { "cacheControlDay" });
            }

            return result;

        }

        IHttpResult handle304(string oldETag, object response, bool compress)
        {
            string newETag = ETagHelper.CreateETagHash(response);
            IHttpResult result = new HttpResult();
            if (oldETag != null && oldETag.Equals(newETag))
            {
                //  ETags match so no need to zip and send the whole response - just headers
                result.StatusCode = HttpStatusCode.NotModified;
            }
            else
            {
                // ETag does not match so we need to send to updated resource
                if (compress)
                    result = Helper.Compress(this, response);
                else
                    result = new HttpResult
                    {
                        Response = response
                    };
   
                ETagHelper.AddETagHeader(result, response);
                result.StatusCode = HttpStatusCode.OK;
            }
            return result;
        }
    }
}