﻿using System;
using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Auctions
{
    [Route("/{ver}/auctions/{id}/vins/{vin}")]
    [Route("/auctions/{id}/vins/{vin}")]
    [Route("/{ver}/auctions/{id}/styles/{style}")]
    [Route("/auctions/{id}/styles/{style}")]
    public class Auctions
    {
        public string Ver { get; set; }
        public string Id { get; set; }
        public string Vin { get; set; }
        public int? Style { get; set; }
        public int? Area { get; set; }
        public int? Period { get; set; }
        public int? Mileage { get; set; }
    }

    [Route("/{ver}/auctions/{id}/reference")]
    [Route("/auctions/{id}/reference")]
    public class Reference
    {
        public string Ver { get; set; }
        public string Id { get; set; }
    }

    [Route("/{ver}/auctions/mmr/reference")]
    [Route("/auctions/mmr/reference")]
    public class MMRReference
    {
        public string Ver { get; set; }
    }

    [Route("/{ver}/auctions/mmr/vins/{vin}")]
    [Route("/auctions/mmr/vins/{vin}")]
    public class GetMMRAuctionSummary
    {
        public string Ver { get; set; }
        public string Vin { get; set; }
        public string Area { get; set; }
    }

    [Route("/{ver}/auctions/mmr/transactions/trims/{trimid}/years/{year}")]
    [Route("/auctions/mmr/transactions/trims/{trimId}/years/{year}")]
    public class GetMmrAuctionTransactions
    {
        public string Ver { get; set; }
        public string TrimId { get; set; }
        public int Year { get; set; }
        public string Area { get; set; }
    }
	
	[Route("/{ver}/auctions/sales")]
    //[Route("/{ver}/auctions/sales/lat/{lat}/long/{long}")]
    [Route("/auctions/sales")]
    //[Route("/auctions/sales/lat/{lat}/long/{long}")]
    public class GetAuctionsDetails
    {
        public string Ver { get; set; }
        public double Lat { get; set; }
        public double Long { get; set; }
    }

    [Route("/{ver}/auctions/vehicles/dealers/{dealerID}/locations/{locationID}/date/{date}/lane/{lane}")]
    [Route("/auctions/vehicles/dealers/{dealerID}/locations/{locationID}/date/{date}/lane/{lane}")]
    [Route("/{ver}/auctions/vehicles/dealers/{dealerID}/locations/{locationID}/lane/{lane}")]
    [Route("/auctions/vehicles/dealers/{dealerID}/locations/{locationID}/lane/{lane}")]
    public class GetAuctionVehicles
    {
        public string Ver { get; set; }
        public int DealerId { get; set; }
        public int LocationId { get; set; }
        public string Date { get; set; }
        public string Lane { get; set; }
    }

    [Route("/{ver}/auctions/lanes/location/{location}/date/{date}")]
    [Route("/auctions/lanes/location/{location}/date/{date}")]
    public class GetAuctionLanes
    {
        public string Ver { get; set; }
        public int Location { get; set; }
        public string Date { get; set; }
    }
}
