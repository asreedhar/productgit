﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Auctions.Models
{
    [DataContract]
    public class MileagesAggregation
    {
        [DataMember(Name = "min")]
        public int Min { get; internal set; }

        [DataMember(Name = "average")]
        public int Average { get; internal set; }

        [DataMember(Name = "max")]
        public int Max { get; internal set; }

        [DataMember(Name = "sampleSize")]
        public int SampleSize { get; internal set; }
    }
}