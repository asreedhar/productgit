﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Auctions.Models
{
    [DataContract]
    public class SalePricesSimilarMileageAggregation
    {
        [DataMember(Name = "min")]
        public decimal Min { get; internal set; }

        [DataMember(Name = "average")]
        public decimal Average { get; internal set; }

        [DataMember(Name = "max")]
        public decimal Max { get; internal set; }

        [DataMember(Name = "sampleSize")]
        public int SampleSize { get; internal set; }
    }
}