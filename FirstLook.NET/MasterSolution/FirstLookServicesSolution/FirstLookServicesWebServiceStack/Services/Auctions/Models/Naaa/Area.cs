﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Auctions.Models.Naaa
{
    [DataContract]
    public class Area
    {
        [DataMember(Name = "name")]
        public string Name { get; internal set; }

        [DataMember(Name = "id")]
        public int Id { get; internal set; }
    }
}