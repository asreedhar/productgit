﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Auctions.Models.Mmr
{
    [DataContract]
    public class MmrTransaction
    {
        [DataMember(Name = "date")]
        public string Date { get; set; }

        [DataMember(Name = "auction")]
        public string Auction { get; set; }

        [DataMember(Name = "salesType")]
        public string SalesType { get; set; }

        [DataMember(Name = "price")]
        public int Price { get; set; }

        [DataMember(Name = "mileage")]
        public int Mileage { get; set; }

        [DataMember(Name = "condition")]
        public string Condition { get; set; }

        [DataMember(Name = "color")]
        public string Color { get; set; }

        [DataMember(Name = "engine")]
        public string Engine { get; set; }

        [DataMember(Name = "transmission")]
        public string Transmission { get; set; }

        [DataMember(Name = "inSample")]
        public string InSample { get; set; }
    }
}
