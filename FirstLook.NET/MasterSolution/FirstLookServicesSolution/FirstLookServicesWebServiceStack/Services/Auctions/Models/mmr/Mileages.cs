﻿
using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Auctions.Models.Mmr
{
    [DataContract]
    public class Mileages
    {
        [DataMember(Name = "average")]
        public int Average { get; set; }

        [DataMember(Name = "min")]
        public int Min { get; set; }

        [DataMember(Name = "max")]
        public int Max { get; set; }

        [DataMember(Name = "sampleSize")]
        public int SampleSize { get; set; }
    }
}
