﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Auctions.Models.Mmr
{
    [DataContract]
    public class MMRVehicle
    {
        [DataMember(Name = "trimId")]
        public string TrimId { get; set; }

        [DataMember(Name = "area")]
        public string Area { get; set; }

        [DataMember(Name = "year")]
        public int Year { get; set; }

        [DataMember(Name = "make")]
        public string Make { get; set; }

        [DataMember(Name = "model")]
        public string Model { get; set; }

        [DataMember(Name = "trim")]
        public string Trim { get; set; }

        [DataMember(Name = "mileages")]
        public Mileages Mileages { get; set; }

        [DataMember(Name = "salesPrices")]
        public Prices SalesPrices { get; set; }

        [DataMember(Name = "earliestSaleDate")]
        public string EarliestSaleDate { get; set; }

        [DataMember(Name = "latestSaleDate")]
        public string LatestSaleDate { get; set; }

    }
}
