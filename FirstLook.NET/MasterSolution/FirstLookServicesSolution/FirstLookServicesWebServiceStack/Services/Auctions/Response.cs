﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using FirstLook.FirstLookServices.DomainModel.Auctions.Model.Sale;
using FirstLook.FirstLookServices.WebServiceStack.Services.Auctions.Models;
using FirstLook.FirstLookServices.WebServiceStack.Services.Auctions.Models.Naaa;
using FirstLook.FirstLookServices.WebServiceStack.Services.Auctions.Models.Mmr;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Auctions
{
    [DataContract]
    public class AuctionsResponse
    {
        [DataMember(Name = "period")]
        public int Period { get; set; }

        [DataMember(Name = "area")]
        public int Area { get; set; }

        [DataMember(Name = "mileage")]
        public int Mileage { get; set; }

        [DataMember(Name = "lowMileage")]
        public int LowMileage { get; set; }

        [DataMember(Name = "highMileage")]
        public int HighMileage { get; set; }

        [DataMember(Name = "salePrices")]
        public SalePriceAggregation SalePrices { get; set; }

        [DataMember(Name = "salePricesSimilarMileage")]
        public SalePriceAggregation SalePricesSimilarMileage { get; set; }

        [DataMember(Name = "mileages")]
        public MileagesAggregation Mileages { get; set; }
    }

    [DataContract]
    public class ReferenceResponse
    {
        [DataMember(Name = "periods")]
        public List<Period> Periods { get; set; }

        [DataMember(Name = "areas")]
        public List<Area> Areas { get; set; }
    }

    [DataContract]
    public class MMRReferenceResponse
    {
        [DataMember(Name = "areas")]
        public List<MMRArea> Areas { get; set; }
    }

    [DataContract]
    public class GetMMRVehiclesResponse
    {
        [DataMember(Name = "auctionsSummary")]
        public List<MMRVehicle> Vehicles { get; set; }
    }

    [DataContract]
    public class GetMmrAuctionTransactionResponse
    {
        [DataMember(Name = "trimId")]
        public string TrimId { get; set; }

        [DataMember(Name = "area")]
        public string Area { get; set; }

        [DataMember(Name = "year")]
        public int Year { get; set; }

        [DataMember(Name = "transactions")]
        public List<MmrTransaction> Transactions { get; set; }
    }

	[DataContract]
    public class GetAuctionDataResponse
    {
        [DataMember(Name = "nearbySales")]
        public List<Sales> NearSales { get; set; }

        [DataMember(Name = "allSales")]
        public List<Sales> AllSales { get; set; }
    }

    [DataContract]
    public class GetAuctionVehicleResponse
    {
        [DataMember(Name = "auctionVehicles")]
        public List<Vehicle> AuctionVehicles { get; set; }
    }

    [DataContract]
    public class GetAuctionLanesResponse
    {
        [DataMember(Name = "laneDetails")]
        public LaneDetails LaneDetails { get; set; }
    }
    //feels lame. Still not sure how to just chuck a string into the response like '{}' to represent an empty set for legit queries that find nothing
    [DataContract]
    public class EmptyResponse
    {
    }

}