﻿using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.StorePerformance
{
    public class Request
    {

        [Route("/{ver}/storePerformance/dealers/{dealer}/vins/{vin}")]
        [Route("/{ver}/storePerformance/dealers/{dealer}/vins/{vin}/mileage/{mileage}")]
        [Route("/storePerformance/dealers/{dealer}/vins/{vin}")]
        [Route("/storePerformance/dealers/{dealer}/vins/{vin}/mileage/{mileage}")]
        public class StorePerformance
        {
            public int Dealer { get; set; }
            public string Ver { get; set; }
            public string Vin { get; set; }
            public int Mileage { get; set; }
        }
        
    }
}