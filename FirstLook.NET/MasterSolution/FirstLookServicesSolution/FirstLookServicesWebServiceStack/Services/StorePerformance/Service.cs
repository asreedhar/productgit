﻿using System;
using System.Linq;
using System.Net;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.StorePerformance.Commands;
using FirstLook.FirstLookServices.DomainModel.StorePerformance.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.WebServiceStack.Utility;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.StorePerformance
{
    [Authenticate]
    public class Service : ServiceStack.ServiceInterface.Service
    {
        readonly VersionMapper<Request.StorePerformance> _storePerformanceMapper = new VersionMapper<Request.StorePerformance>("v1");

        public Service()
        {
            _storePerformanceMapper.Add("v1", "get", Get_v1);
        }

        public object Get(Request.StorePerformance request)
        {

            try
            {
                return _storePerformanceMapper.GetServiceFunction(request.Ver, "get")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }

        }

        public object Get_v1(Request.StorePerformance request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            string oldETag = RequestContext.GetHeader("If-None-Match");

            var command = factory.CreateFetchStorePerformanceCommand();

            var arguments = new FetchStorePerformanceArgumentsDto()
            {
                BusinessUnitID = request.Dealer,
                VIN =request.Vin,
                Mileage=request.Mileage
            };

            FetchStorePerformanceResultsDto results = Helper.ExecuteCommand(command, Helper.WrapInContext(arguments, this.GetSession().UserAuthName));
            
            Response.StorePerformanceResponse response = null;
            if (results.StorePerformance != null && results.StorePerformance.FirstOrDefault()!=null)
            {
                response = new Response.StorePerformanceResponse()
                               {
                                   RiskLight = results.StorePerformance.FirstOrDefault().RiskLight
                               };
            }

            IHttpResult result = Handle304(oldETag, response, true);

            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                result = Helper.SetStaticHeaders(result, new[] { "cacheControlHour" });
            }

            return result;
        }
        
        IHttpResult Handle304(string oldETag, object response, bool compress)
        {
            string newETag = ETagHelper.CreateETagHash(response);

            IHttpResult result = new HttpResult();

            if (oldETag != null && oldETag.Equals(newETag))
            {
                //  ETags match so no need to zip and send the whole response - just headers
                result.StatusCode = HttpStatusCode.NotModified;
            }
            else
            {
                // ETag does not match so we need to send to updated resource
                // 
                if (compress)
                    result = Helper.Compress(this, response);
                else
                    result = new HttpResult
                    {
                        Response = response
                    };

                ETagHelper.AddETagHeader(result, response);

                result.StatusCode = HttpStatusCode.OK;
            }
            return result;
        }
    }
}