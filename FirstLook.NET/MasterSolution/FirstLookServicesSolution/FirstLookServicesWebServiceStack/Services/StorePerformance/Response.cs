﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.StorePerformance
{
    public class Response
    {
        [DataContract]
        public class StorePerformanceResponse
        {
            [DataMember(Name = "riskLight")]
            public string RiskLight { get; set; }
        }
        
    }
}