﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.StorePerformance.Models
{
    public class StorePerformance
    {
        [DataMember(Name = "riskLight")]
        public string RiskLight { get; set; }
    }
}