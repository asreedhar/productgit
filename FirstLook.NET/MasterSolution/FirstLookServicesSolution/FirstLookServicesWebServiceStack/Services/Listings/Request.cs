﻿using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Listings
{
    [Route("/{ver}/listings/searchTypes")]
    [Route("/listings/searchTypes")]
    public class SearchTypes
    {
        public string Ver { get; set; }
    }

    [Route("/{ver}/listings/distances")]
    [Route("/listings/distances")]
    public class Distances
    {
        public string Ver { get; set; }
    }

    [Route("/{ver}/listings/dealers/{dealer}/vins/{vin}")]
    [Route("/listings/dealers/{dealer}/vins/{vin}")]
    public class Listings
    {
        public string Ver { get; set; }
        public int Dealer { get; set; }
        public string Vin { get; set; }
        public string SearchType { get; set; }
        public int Distance { get; set; }
        public int? MileageMin { get; set; }
        public string MileageMax { get; set; }
    }

    [Route("/{ver}/listings/dealers/{dealer}/inventoryId/{inventoryId}")]
    [Route("/listings/dealers/{dealer}/inventoryId/{inventoryId}")]
    public class InventoryListings
    {
        public int InventoryId { get; set; }
        public string Ver { get; set; }
        public int Dealer { get; set; }
        //public string Vin { get; set; }
        public string SearchType { get; set; }
        public int Distance { get; set; }
        public int? MileageMin { get; set; }
        public string MileageMax { get; set; } 
        public bool? NextYear { get; set; }
        public bool? PreviousYear { get; set; }

    }

	
    [Route("/{ver}/listings/mileages")]
    [Route("/listings/mileages")]
    public class Mileages
    {
        public string Ver { get; set; }
    }
}