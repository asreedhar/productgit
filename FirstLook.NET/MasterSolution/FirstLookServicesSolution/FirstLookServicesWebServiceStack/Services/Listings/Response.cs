﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using FirstLook.FirstLookServices.WebServiceStack.Services.Listings.Models;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Listings
{
    [DataContract]
    public class SearchTypesResponse
    {
        [DataMember(Name = "listings")]
        public List<string> SearchTypes { get; set; }
    }

    [DataContract]
    public class DistancesResponse
    {
        [DataMember(Name = "distances")]
        public List<int> Distances { get; set; }
    }

    [DataContract]
    public class ListingResponse
    {
        public SearchResults SearchResults { get; set; }
    }

    [DataContract]
    public class InventoryListingResponse
    {
        public SearchResults SearchResults { get; set; }
    }

    [DataContract]
    public class MileagesResponse
    {
        [DataMember(Name = "mileages")]
        public List<string>Mileages { get; set; }
    }
}