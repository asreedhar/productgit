﻿using System.Collections.Generic;
using System;
using System.Linq;
using System.Net;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Listings.Commands;
using FirstLook.FirstLookServices.DomainModel.Listings.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.WebServiceStack.Services.Listings.Models;
using FirstLook.FirstLookServices.WebServiceStack.Utility;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;
using ServiceStack.Common.Web;
using MarketListingDto = FirstLook.FirstLookServices.DomainModel.Listings.Commands.TransferObjects.MarketListingDto;
using MarketPricingDto = FirstLook.FirstLookServices.DomainModel.Listings.Commands.TransferObjects.MarketPricingDto;
using SearchSummaryInfoDto = FirstLook.FirstLookServices.DomainModel.Listings.Commands.TransferObjects.SearchSummaryInfoDto;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Listings
{ [Authenticate]
    public class Service : ServiceStack.ServiceInterface.Service
    {
        VersionMapper<SearchTypes> searchTypesMapper = new VersionMapper<SearchTypes>("v1");
        VersionMapper<Distances> distancesMapper = new VersionMapper<Distances>("v1");
        VersionMapper<Listings> listingsMapper = new VersionMapper<Listings>("v1");
        VersionMapper<Mileages> mileagesMapper = new VersionMapper<Mileages>("v1");
        VersionMapper<InventoryListings> inventoryListingsMapper = new VersionMapper<InventoryListings>("v1");

        //private const string UnlimitedMileage = "unlimited";

        public Service()
        {
            searchTypesMapper.Add("v1", "get", Get_v1);
            distancesMapper.Add("v1", "get", Get_v1);
            listingsMapper.Add("v1", "get", Get_v1);
            mileagesMapper.Add("v1", "get", Get_v1);
            inventoryListingsMapper.Add("v1", "get", Get_v1);
        }

        public object Get(SearchTypes request)
        {
            try
            {
                return searchTypesMapper.GetServiceFunction(request.Ver, "get")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }
        public object Get(Distances request)
        {
            try
            {
                return distancesMapper.GetServiceFunction(request.Ver, "get")(request);

            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }
        public object Get(Listings request)
        {
            try
            {
                return listingsMapper.GetServiceFunction(request.Ver, "get")(request);

            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }
        public object Get(InventoryListings request)
        {
            try
            {
                return inventoryListingsMapper.GetServiceFunction(request.Ver, "get")(request);

            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }
        public object Get(Mileages request)
        {
            try
            {
                return mileagesMapper.GetServiceFunction(request.Ver, "get")(request);

            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public IHttpResult Get_v1(SearchTypes request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            string oldETag = this.RequestContext.GetHeader("If-None-Match");
            var command = factory.CreateFetchMarketListingsReferenceCommand();

            var argumentsDto = new FetchMarketListingsReferenceArgumentsDto();
            FetchMarketListingsReferenceResultsDto results = command.Execute(argumentsDto);

            var response = new SearchTypesResponse
            {
                SearchTypes = results.SearchTypes.Select(searchType => searchType.ToLower()).ToList()
            };
            IHttpResult result = handle304(oldETag, response, true);
            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                Helper.SetStaticHeaders(result, new string[] { "referenceDataTrue", "cacheControlDay" });
            }
            return result;
        }

        public IHttpResult Get_v1(Distances request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            string oldETag = this.RequestContext.GetHeader("If-None-Match");
            var command = factory.CreateFetchMarketListingsReferenceCommand();

            var argumentsDto = new FetchMarketListingsReferenceArgumentsDto();
            FetchMarketListingsReferenceResultsDto results = null;

            results = command.Execute(argumentsDto);

            var response = new DistancesResponse
            {
                Distances = results.Distances.Select(distance => distance.Value).ToList()
            };

            IHttpResult result = handle304(oldETag, response, true);
            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                Helper.SetStaticHeaders(result, new string[] { "referenceDataTrue", "cacheControlDay" });
            }
            return result;
        }

        public object Get_v1(Listings request)
        {
            List<IDictionary<string, object>> errors = new List<IDictionary<string, object>>();
            if (request.Vin == null)
            {
                IDictionary<string, object> err = this.MissingField("vin", null);
                if (err != null)
                    errors.Add(err);
            }
            if (request.Dealer == 0)
            {
                IDictionary<string, object> err = this.MissingField("dealer", null);
                if (err != null)
                    errors.Add(err);
            }


            if (errors.Count() > 0)
            {
                return this.HandleError(errors);
            }

            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            string oldETag = this.RequestContext.GetHeader("If-None-Match");
            var command = factory.CreateFetchMarketListingsCommand();

            var argumentsDto = new FetchMarketListingsArgumentsDto
            {
                DealerId = request.Dealer,
                Vin = request.Vin,
                Distance = request.Distance,
                MileageMax = request.MileageMax,
                MileageMin = request.MileageMin,
                SearchType = request.SearchType
            };
            FetchMarketListingsResultsDto fetchMarketListingsResultsDto = null;
            IHttpResult result = null;

            fetchMarketListingsResultsDto = command.Execute(Helper.WrapInContext(argumentsDto, this.GetSession().UserAuthName));

            List<MarketListingDto> mListingDtoList = fetchMarketListingsResultsDto.MarketListings;

            List<MarketListing> marketListings = mListingDtoList.Select(marketListingDto => new MarketListing()
            {
                
                Age = marketListingDto.Age,
                Desc = marketListingDto.Desc,
                Color = marketListingDto.Color,
                Mileage = marketListingDto.VehicleMileage,
                Price = marketListingDto.ListPrice,
                PercentOfMarketAverage = marketListingDto.PercentMarketAverage,
                Distance = marketListingDto.Distance,
                Certified = marketListingDto.IsCertified,
                Seller = marketListingDto.Seller
            }).ToList();

            SearchSummaryInfoDto searchSummaryInfoDto = fetchMarketListingsResultsDto.SearchResult.SearchSummaryInfo;
            var respSearchSumDesc = "";
            if (fetchMarketListingsResultsDto.SearchResult.SearchDetail.SearchType == "Overall")
            {
                if (searchSummaryInfoDto.YearMakeModel != null)
                {
                    respSearchSumDesc += searchSummaryInfoDto.YearMakeModel.Trim();
                }
            }
            else
            {
                if (searchSummaryInfoDto.Segment != null)
                {
                    respSearchSumDesc += searchSummaryInfoDto.Segment.Trim() + " | ";
                }

                if (searchSummaryInfoDto.Trim != null)
                {
                    respSearchSumDesc += searchSummaryInfoDto.Trim.Trim() + " | ";
                }

                if (searchSummaryInfoDto.Doors != null)
                {
                    respSearchSumDesc += searchSummaryInfoDto.Doors.Trim() + " | ";
                }

                if (searchSummaryInfoDto.Engine != null)
                {
                    respSearchSumDesc += searchSummaryInfoDto.Engine.Trim() + " | ";
                }

                if (searchSummaryInfoDto.DriveTrain != null)
                {
                    respSearchSumDesc += searchSummaryInfoDto.DriveTrain.Trim() + " | ";
                }

                if (searchSummaryInfoDto.FuelType != null)
                {
                    respSearchSumDesc += searchSummaryInfoDto.FuelType.Trim() + " | ";
                }

                if (searchSummaryInfoDto.Transmission != null)
                {
                    respSearchSumDesc += searchSummaryInfoDto.Transmission.Trim() + " | ";
                }

                // Trim the last " |" off the end.
                if (respSearchSumDesc.Length > 0)
                {
                    respSearchSumDesc = respSearchSumDesc.TrimEnd(new char[] { ' ', '|' });
                }
            }

            MarketPricingDto marketPricingDto = fetchMarketListingsResultsDto.SearchResult.SearchDetail.MarketPricing;

            var response = new SearchResults
            {   
               
                MarketAverage = marketPricingDto.MarketPrice.Average,
                MarketMin = marketPricingDto.MarketPrice.Minimum,
                MarketMax = marketPricingDto.MarketPrice.Maximum,
                DaysSupply = marketPricingDto.DaySupply,
                SearchType = fetchMarketListingsResultsDto.SearchResult.SearchDetail.SearchType.ToLower(),
                Distance = marketPricingDto.Distance,
                MileageMin = marketPricingDto.Mileage.Minimum,
                MileageMax = marketPricingDto.Mileage.Maximum,
                MileageAvg = marketPricingDto.Mileage.Average,
                MinMileageSearched = marketPricingDto.MinMileageSearched,
                MaxMileageSearched = marketPricingDto.MaxMileageSearched,
                SearchSummary = respSearchSumDesc,
                Listings = marketListings
            };

            result = handle304(oldETag, response, true);
            result = Helper.SetStaticHeaders(result, new string[] { "cacheControlHour" });
            return result;
        }

        public IHttpResult Get_v1(InventoryListings request)
        {
            List<IDictionary<string, object>> errors = new List<IDictionary<string, object>>();
            if (request.InventoryId == 0)
            {
                IDictionary<string, object> err = this.MissingField("inventoryId", null);
                if (err != null)
                    errors.Add(err);
            }
            if (request.Dealer == 0)
            {
                IDictionary<string, object> err = this.MissingField("dealer", null);
                if (err != null)
                    errors.Add(err);
            }


            if (errors.Count() > 0)
            {
                return this.HandleError(errors);
            }

            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            string oldETag = this.RequestContext.GetHeader("If-None-Match");
            var command = factory.CreateFetchInventoryMarketListingsCommand();

            var argumentsDto = new FetchInventoryMarketListingsArgumentsDto
            {
                DealerId = request.Dealer,
                InventoryId = request.InventoryId,
                Distance = request.Distance,
                MileageMax = request.MileageMax,
                MileageMin = request.MileageMin,
                SearchType = request.SearchType,
                NextYear = request.NextYear,
                PreviousYear = request.PreviousYear
            };

            FetchInventoryMarketListingsResultsDto fetchInventoryMarketListingsResultsDto = null;
            IHttpResult result = null;

            fetchInventoryMarketListingsResultsDto =
                command.Execute(Helper.WrapInContext(argumentsDto, this.GetSession().UserAuthName));

            List<MarketListingDto> mListingDtoList = fetchInventoryMarketListingsResultsDto.MarketListings;

            List<MarketListing> marketListings = mListingDtoList.Select(marketListingDto => new MarketListing()
            {
                Age = marketListingDto.Age,
                Desc = marketListingDto.Desc,
                Color = marketListingDto.Color,
                Mileage = marketListingDto.VehicleMileage,
                Price = marketListingDto.ListPrice,
                PercentOfMarketAverage = marketListingDto.PercentMarketAverage,
                Distance = marketListingDto.Distance,
                Certified = marketListingDto.IsCertified,
                Seller = marketListingDto.Seller
            }).ToList();

            SearchSummaryInfoDto searchSummaryInfoDto = fetchInventoryMarketListingsResultsDto.SearchResult.SearchSummaryInfo;
            var respSearchSumDesc = "";
            if (fetchInventoryMarketListingsResultsDto.SearchResult.SearchDetail.SearchType == "Overall")
            {
                if (searchSummaryInfoDto.YearMakeModel != null)
                {
                    respSearchSumDesc += searchSummaryInfoDto.YearMakeModel.Trim();
                }
            }
            else
            {
                if (searchSummaryInfoDto.Segment != null)
                {
                    respSearchSumDesc += searchSummaryInfoDto.Segment.Trim() + " | ";
                }

                if (searchSummaryInfoDto.Trim != null)
                {
                    respSearchSumDesc += searchSummaryInfoDto.Trim.Trim() + " | ";
                }

                if (searchSummaryInfoDto.Doors != null)
                {
                    respSearchSumDesc += searchSummaryInfoDto.Doors.Trim() + " | ";
                }

                if (searchSummaryInfoDto.Engine != null)
                {
                    respSearchSumDesc += searchSummaryInfoDto.Engine.Trim() + " | ";
                }

                if (searchSummaryInfoDto.DriveTrain != null)
                {
                    respSearchSumDesc += searchSummaryInfoDto.DriveTrain.Trim() + " | ";
                }

                if (searchSummaryInfoDto.FuelType != null)
                {
                    respSearchSumDesc += searchSummaryInfoDto.FuelType.Trim() + " | ";
                }

                if (searchSummaryInfoDto.Transmission != null)
                {
                    respSearchSumDesc += searchSummaryInfoDto.Transmission.Trim() + " | ";
                }

                if (respSearchSumDesc.Length > 0)
                {
                    respSearchSumDesc = respSearchSumDesc.TrimEnd(new char[] { ' ', '|' });
                }
            }

            MarketPricingDto marketPricingDto = fetchInventoryMarketListingsResultsDto.SearchResult.SearchDetail.MarketPricing;

            var response = new SearchResults
            {
                MarketAverage = marketPricingDto.MarketPrice.Average,
                MarketMin = marketPricingDto.MarketPrice.Minimum,
                MarketMax = marketPricingDto.MarketPrice.Maximum,
                DaysSupply = marketPricingDto.DaySupply,
                SearchType = fetchInventoryMarketListingsResultsDto.SearchResult.SearchDetail.SearchType.ToLower(),
                Distance = marketPricingDto.Distance,
                MileageMin = marketPricingDto.Mileage.Minimum,
                MileageMax = marketPricingDto.Mileage.Maximum,
                MileageAvg = marketPricingDto.Mileage.Average,
                MinMileageSearched = marketPricingDto.MinMileageSearched,
                MaxMileageSearched = marketPricingDto.MaxMileageSearched,
                SearchSummary = respSearchSumDesc,
                Listings = marketListings
            };

            result = handle304(oldETag, response, true);
            result = Helper.SetStaticHeaders(result, new string[] { "cacheControlHour" });
            return result;
        }
        public IHttpResult Get_v1(Mileages request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            string oldETag = this.RequestContext.GetHeader("If-None-Match");
            var command = factory.CreateFetchMarketListingsReferenceCommand();

            var argumentsDto = new FetchMarketListingsReferenceArgumentsDto();

            FetchMarketListingsReferenceResultsDto results = null;
            try
            {
                results = command.Execute(argumentsDto);
            }
            catch (Exception ex)
            {
                var errMsg = this.ExceptionToError(ex, null);
                var list = new List<IDictionary<string, object>>();
                list.Add(errMsg);
                return this.HandleError(list);
            }

            List<string> mileages =  new List<string>();
            results.Mileages.ForEach(ml=>mileages.Add(ml.ToString()));
            //mileages.Add(UnlimitedMileage);

            var response = new MileagesResponse
            {
                Mileages = mileages
            };

            IHttpResult result = handle304(oldETag, response, true);
            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                Helper.SetStaticHeaders(result, new string[] { "referenceDataTrue", "cacheControlDay" });
            }

            return result;
        }

        IHttpResult handle304(string oldETag, object response, bool compress)
        {
            string newETag = ETagHelper.CreateETagHash(response);
            IHttpResult result = new HttpResult();
            if (oldETag != null && oldETag.Equals(newETag))
            {
                //  ETags match so no need to zip and send the whole response - just headers
                result.StatusCode = HttpStatusCode.NotModified;
            }
            else
            {
                // ETag does not match so we need to send to updated resource
                if (compress)
                    result = Helper.Compress(this, response);
                else
                    result = new HttpResult
                    {
                        Response = response
                    };

                ETagHelper.AddETagHeader(result, response);
                result.StatusCode = HttpStatusCode.OK;
            }
            return result;
        }

    }
}