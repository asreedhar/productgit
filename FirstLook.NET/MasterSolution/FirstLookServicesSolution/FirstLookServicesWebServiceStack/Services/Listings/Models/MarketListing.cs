using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Listings.Models
{
    [DataContract]
    public class MarketListing
    {
        [DataMember(Name = "age")]
        public int Age { get; internal set; }

        [DataMember(Name = "desc")]
        public string Desc { get; internal set; }

        [DataMember(Name = "color")]
        public string Color { get; internal set; }

        [DataMember(Name = "mileage")]
        public int Mileage { get; internal set; }

        [DataMember(Name = "price")]
        public int Price { get; internal set; }

        [DataMember(Name = "percentOfMarketAverage")]
        public int PercentOfMarketAverage { get; internal set; }

        [DataMember(Name = "distance")]
        public int Distance { get; internal set; }

        [DataMember(Name = "certified")]
        public bool Certified { get; internal set; }

        [DataMember(Name = "seller")]
        public string Seller { get; internal set; }
    }
}