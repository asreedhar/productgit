using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Listings.Models
{
    [DataContract]
    public class SearchResults
    {
        [DataMember(Name = "marketAverage")]
        public int MarketAverage { get; internal set; }

        [DataMember(Name = "marketMin")]
        public int MarketMin  { get; internal set; }

        [DataMember(Name = "marketMax")]
        public int MarketMax  { get; internal set; }

        [DataMember(Name = "daysSupply")]
        public int DaysSupply  { get; internal set; }

        [DataMember(Name = "searchType")]
        public string SearchType  { get; internal set; }

        [DataMember(Name = "distance")]
        public int Distance { get; internal set; }

        [DataMember(Name = "mileageMin")]
        public int MileageMin { get; internal set; }

        [DataMember(Name = "mileageMax")]
        public int MileageMax { get; internal set; }

        [DataMember(Name = "mileageAvg")]
        public int MileageAvg { get; internal set; }

        [DataMember(Name = "searchSummary")]
        public string SearchSummary { get; internal set; }

        [DataMember(Name = "minMileageSearched")]
        public int MinMileageSearched { get; internal set; }

        [DataMember(Name = "maxMileageSearched")]
        public int? MaxMileageSearched { get; internal set; }

        [DataMember(Name = "listings")]
        public List<MarketListing> Listings { get; internal set; }
    }
}