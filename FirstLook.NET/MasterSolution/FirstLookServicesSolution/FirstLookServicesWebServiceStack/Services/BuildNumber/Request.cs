﻿using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.BuildNumber
{
    [Route("/{ver}/buildnumber")]
    [Route("/buildnumber")]
    public class BuildNumberRequest
    {
        public string Ver { get; set; }
    }
}