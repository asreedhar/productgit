﻿using System;
using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.BuildNumber
{
    [DataContract]
    public class BuildNumberResponse
    {
        [DataMember(Name = "buildNumber")]
        public String BuildNumber { get; set; }
    }

}