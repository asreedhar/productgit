﻿using System;
using ServiceStack.ServiceInterface;
using System.Reflection;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.BuildNumber
{

    [Authenticate]
    public class Service : ServiceStack.ServiceInterface.Service
    {
        public object Get(BuildNumberRequest request)
        {
            return new {
                buildNumber = Assembly.GetExecutingAssembly().GetName().Version.ToString()
            };
        }
    }
}