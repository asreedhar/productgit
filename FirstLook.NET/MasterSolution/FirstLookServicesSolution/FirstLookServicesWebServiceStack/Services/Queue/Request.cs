﻿using FirstLook.Common.Core.Utilities;
using FirstLook.QueueProcessor.DomainModel.BookQueue.Model;
using FirstLook.QueueProcessor.DomainModel.ManheimQueue.Model;
using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Queue
{
    public class Request
    {
        [Route("/{ver}/queue/recordcount/{recordcount}")]
        [Route("/queue/recordcount/{recordcount}")]
        public class MMRQueueProcessor
        {
            public int RecordCount { get; set; }
        }

        [Route("/{ver}/queue/days/{days}")]
        [Route("/queue/days/{days}")]
        public class MMRPushDataInQueue
        {
            public int Days { get; set; }
        }

        [Route("/{ver}/queue/days/{days}")]
        [Route("/queue/days/{days}")]
        public class MMRPurgeSuccessData
        {
            public int Days { get; set; }
            public int Status { get; set; }
        }

        [Route("/{ver}/MMRAveragePrice")]
        [Route("/MMRAveragePrice")]
        public class MMRAveragePrice:MMRInventoryQueue
        {
            
        }
    }
}