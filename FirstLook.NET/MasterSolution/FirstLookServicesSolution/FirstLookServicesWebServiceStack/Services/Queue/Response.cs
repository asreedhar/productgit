﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Queue
{
    public class Response
    {

        [DataContract]
        public class MMRAveragePriceResponse
        {
            [DataMember(Name = "price")]
            public List<decimal> price { get; set; }
        }
    }
}