﻿using System.Runtime.Serialization;
using FirstLook.FirstLookServices.WebServiceStack.Services.ShowPricingHistory.Models;
using System.Collections.Generic;


namespace FirstLook.FirstLookServices.WebServiceStack.Services.ShowPricingHistory
{
    public class Response
    {
        [DataContract]
        public class PricingHistoryInfo
        {
            [DataMember(Name = "pricinghistory")]
            public IList<PricingHistoryDetails> PricingHistory { get; set; }
        }
    }
}