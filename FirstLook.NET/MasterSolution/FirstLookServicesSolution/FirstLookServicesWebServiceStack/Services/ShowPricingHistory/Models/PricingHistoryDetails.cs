﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.ShowPricingHistory.Models
{
    [DataContract]
    public class PricingHistoryDetails
    {
        [DataMember(Name = "inventoryId")]
        public int InventoryId { get; set; }
        [DataMember(Name = "login")]
        public string Login { get; set; }
        [DataMember(Name = "listPrice")]
        public int ListPrice { get; set; }
        [DataMember(Name = "updatedDate")]
        public string UpdatedDate { get; set; }
    }
}