﻿using System;
using System.Net;
using System.Linq;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.PricingHistory.Commands;
using FirstLook.FirstLookServices.DomainModel.PricingHistory.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.PricingHistory.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.WebServiceStack.Services.ShowPricingHistory.Models;
using FirstLook.FirstLookServices.WebServiceStack.Utility;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;
using System.Collections.Generic;


namespace FirstLook.FirstLookServices.WebServiceStack.Services.ShowPricingHistory
{
    [Authenticate]
    public class Service : ServiceStack.ServiceInterface.Service
    {
        private readonly VersionMapper<Request.PricingHistory> _pricingHistoryMapper = new VersionMapper<Request.PricingHistory>("v1");

        public Service()
        {
            _pricingHistoryMapper.Add("v1", "get", Get_v1);

        }

        public object Get(Request.PricingHistory request)
        {
            try
            {
                return _pricingHistoryMapper.GetServiceFunction(request.Ver, "get")(request);
            }

            catch (Exception ex)
            {
                {
                    var err = this.ExceptionToError(ex, null);
                    return (err != null) ? this.HandleError(err) : null;
                }
            }
        }

        public object Get_v1(Request.PricingHistory request)
        {

            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            string oldETag = RequestContext.GetHeader("If-None-Match");
            var command = factory.CreateFetchPricingHistoryCommand();
            var arguments = new FetchPricingHistoryArgumentsDto
            {
                InventoryId = request.InventoryId,
                DealerId = request.DealerId

            };

            FetchPricingHistoryDetailsResultsDto results = Helper.ExecuteCommand(command, Helper.WrapInContext(arguments, this.GetSession().UserAuthName));

            Response.PricingHistoryInfo response = null;

            if (results.PricingHistoryData != null)
            {
                response = GetPricingHistoryInfo(results.PricingHistoryData);
            }

            IHttpResult result = Handle304(oldETag, response, true);

            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                result = Helper.SetStaticHeaders(result, new[] { "referenceDataTrue", "cacheControlDay" });
            }

            return result;

        }

        IHttpResult Handle304(string oldETag, object response, bool compress)
        {
            string newETag = ETagHelper.CreateETagHash(response);

            IHttpResult result = new HttpResult();

            if (oldETag != null && oldETag.Equals(newETag))
            {
                //  ETags match so no need to zip and send the whole response - just headers
                result.StatusCode = HttpStatusCode.NotModified;
            }
            else
            {
                // ETag does not match so we need to send to updated resource
                // 
                if (compress)
                    result = Helper.Compress(this, response);
                else
                    result = new HttpResult
                    {
                        Response = response
                    };

                ETagHelper.AddETagHeader(result, response);

                result.StatusCode = HttpStatusCode.OK;
            }
            return result;
        }

        private Response.PricingHistoryInfo GetPricingHistoryInfo(IList<PricingHistoryDataDto> objs)
        {
            var pricinghistorydetails = new List<PricingHistoryDetails>();
            foreach (var obj in objs)
            {
                PricingHistoryDetails temp = new PricingHistoryDetails();
                temp.InventoryId = obj.InventoryId;
                temp.Login = obj.Login;
                temp.UpdatedDate = obj.UpdatedDate;
                temp.ListPrice = obj.ListPrice;
                pricinghistorydetails.Add(temp);
            }
            return new Response.PricingHistoryInfo() { PricingHistory = pricinghistorydetails };
        }
    }
}





