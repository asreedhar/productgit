﻿using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.ShowPricingHistory
{
    public class Request
    {
        [Route("/{ver}/showPricingHistory/{dealerid}/{inventoryid}")]
        [Route("/showPricingHistory/{dealerid}/{inventoryid}")]


        public class PricingHistory
        {
            public int InventoryId { get; set; }
            public int DealerId { get; set; }
            public string Ver { get; set; }
        }
    }
  
}