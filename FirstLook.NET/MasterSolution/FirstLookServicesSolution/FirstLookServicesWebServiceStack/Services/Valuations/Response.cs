﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using FirstLook.FirstLookServices.WebServiceStack.Services.Valuations.Models;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Valuations
{    
    [DataContract]
    public class EquipmentActionsResponse
    {
        [DataMember (Name = "actions")]
        public List<EquipmentAction> EquipmentActions { get; set; }
    }

    [DataContract]
    public class ValuationsResponse
    {
        [DataMember(Name = "bookValuations")]
        public List<BookValuation> BookValuations { get; set; }
    }
}