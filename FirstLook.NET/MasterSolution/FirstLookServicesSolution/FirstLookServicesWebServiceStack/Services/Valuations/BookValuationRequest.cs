﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using FirstLook.FirstLookServices.WebServiceStack.Services.Valuations.Models;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Valuations
{
    [DataContract]
    public class BookValuationRequest
    {
        [DataMember(Name = "book")]
        public string Book { get; set; }

        [DataMember(Name = "mileage")]
        public int? Mileage { get; set; }

        [DataMember(Name = "trimId")]
        public string TrimId { get; set; }

        [DataMember(Name = "returnId")]
        public string ReturnId { get; set; }

        [DataMember(Name = "equipmentSelected")]
        public IList<string> EquipmentSelected { get; set; }

        [DataMember(Name = "equipmentExplicitActions")]
        public IList<EquipmentUserAction> EquipmentExplicitActions { get; set; }
    }
}