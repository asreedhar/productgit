﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Valuations.Models
{
    [DataContract]
    public class BookValuation
    {
        [DataMember (Name = "returnId")]
        public string ReturnId { get; set; }

        [DataMember(Name = "checkSum")]
        public string CheckSum { get; set; }

        [DataMember(Name = "valuationData")]
        public IList<ValuationData> ValuationData { get; set; }
    }
}