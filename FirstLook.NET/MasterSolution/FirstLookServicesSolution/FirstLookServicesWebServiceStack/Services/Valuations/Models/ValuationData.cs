﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Valuations.Models
{
    [DataContract]
    public class ValuationData
    {
        [DataMember(Name = "market")]
        public string Market { get; set; }

        [DataMember(Name = "condition")]
        public string Condition { get; set; }

        [DataMember(Name = "valuations")]
        public List<Valuation> Valuations { get; set; }

    }
}