﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Valuations.Models
{
    [DataContract]
    public class Valuation
    {
        [DataMember (Name = "desc")]
        public string Description { get; set; }

        [DataMember(Name = "value")]
        public int Value { get; set; }
    }
}