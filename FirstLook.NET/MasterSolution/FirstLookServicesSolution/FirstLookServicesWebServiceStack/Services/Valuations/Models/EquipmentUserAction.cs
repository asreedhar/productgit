﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Valuations.Models
{
    [DataContract]
    public class EquipmentUserAction
    {
        [DataMember(Name = "action")]
        public string Action { get; set; }

        [DataMember(Name = "equipment")]
        public string Equipment { get; set; }
    }
}