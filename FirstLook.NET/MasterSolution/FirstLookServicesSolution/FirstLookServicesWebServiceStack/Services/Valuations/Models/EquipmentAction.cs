﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Valuations.Models
{
    [DataContract]
    public class EquipmentAction
    {
        [DataMember(Name = "action")]
        public string Action { get; internal set; }

        [DataMember(Name = "desc")]
        public string Description { get; internal set; }
    }
}