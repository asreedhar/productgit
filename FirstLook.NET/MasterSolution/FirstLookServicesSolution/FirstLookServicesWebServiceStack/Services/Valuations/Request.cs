﻿using System.Collections.Generic;
using ServiceStack.ServiceHost;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Valuations
{
    [Route("/{ver}/valuations/equipmentActions")]
    [Route("/valuations/equipmentActions")]
    public class EquipmentActions
    {
        public string Ver { get; set; }
    }

    [Route("/{ver}/valuations/dealers/{dealer}")]
    [Route("/valuations/dealers/{dealer}")]
    public class Valuations
    {
        public IList<BookValuationRequest> BookValuationRequests { get; set; }
        public string Ver { get; set; }
        public int Dealer { get; set; }
    }
}