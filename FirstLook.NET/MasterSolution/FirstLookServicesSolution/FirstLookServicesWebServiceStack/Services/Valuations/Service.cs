﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.Valuations.Commands;
using FirstLook.FirstLookServices.DomainModel.Valuations.Commands.TransferObjects;
using FirstLook.FirstLookServices.DomainModel.Valuations.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.WebServiceStack.Services.Valuations.Models;
using FirstLook.FirstLookServices.WebServiceStack.Utility;
using ServiceStack.ServiceHost;
using ServiceStack.Common.Web;
using ServiceStack.ServiceInterface;
using System;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.Valuations
{

    [Authenticate]
    public class Service : ServiceStack.ServiceInterface.Service
    {
        readonly VersionMapper<EquipmentActions> _equipActionsMapper = new VersionMapper<EquipmentActions>("v1");

        readonly VersionMapper<Valuations> _valuationsMapper = new VersionMapper<Valuations>("v1");

        public Service()
        {
            _equipActionsMapper.Add("v1", "get", Get_v1);
            _valuationsMapper.Add("v1", "post", Post_v1);
        }

        public object Get(EquipmentActions request)
        {
            try
            {
                return _equipActionsMapper.GetServiceFunction(request.Ver, "get")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public IHttpResult Get_v1(EquipmentActions request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            string oldETag = RequestContext.GetHeader("If-None-Match");

            var command = factory.CreateFetchEquipmentActionsCommand();

            var arguments = new FetchEquipmentActionsArgumentsDto();

            FetchEquipmentActionsResultsDto results = Helper.ExecuteCommand(command, arguments);

            IList<EquipmentAction> equipmentActions = results.EquipmentActions.Select(action => new EquipmentAction
                                                                                                    {
                                                                                                        Action = action.Action,
                                                                                                        Description = action.Description
                                                                                                    }).ToList();

            var response = new EquipmentActionsResponse
                               {
                                   EquipmentActions = (List<EquipmentAction>)equipmentActions
                               };

            IHttpResult result = Handle304(oldETag, response, true);

            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                result = Helper.SetStaticHeaders(result, new[] { "referenceDataTrue", "cacheControlDay" });
            }

            return result;
        }

        IHttpResult Handle304(string oldETag, object response, bool compress)
        {
            string newETag = ETagHelper.CreateETagHash(response);

            IHttpResult result = new HttpResult();

            if (oldETag != null && oldETag.Equals(newETag))
            {
                //  ETags match so no need to zip and send the whole response - just headers
                result.StatusCode = HttpStatusCode.NotModified;
            }
            else
            {
                // ETag does not match so we need to send to updated resource
                if (compress)
                    result = Helper.Compress(this, response);
                else
                    result = new HttpResult
                    {
                        Response = response
                    };

                ETagHelper.AddETagHeader(result, response);

                result.StatusCode = HttpStatusCode.OK;
            }
            return result;
        }

        public object Post(Valuations request)
        {
            try
            {
                return _valuationsMapper.GetServiceFunction(request.Ver, "post")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }

        public object Post_v1(Valuations request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            var command = factory.CreateFetchBookValuationsCommand();

            var arguments = new FetchBookValuationsArgumentsDto
                                   {
                                       DealerId = request.Dealer,
                                       BookValuationRequests = (List<BookValuationRequestDto>)Map(request.BookValuationRequests)
                                   };

            FetchBookValuationsResultsDto results = Helper.ExecuteCommand(command, Helper.WrapInContext(arguments, this.GetSession().UserAuthName));

            var response = new ValuationsResponse
                               {
                                   BookValuations = (List<BookValuation>)Map(results.BookValuations)
                               };

            IHttpResult result = Helper.Compress(this, response);

            result.StatusCode = HttpStatusCode.Created;

            result = Helper.SetStaticHeaders(result, new[] { "cacheControlHour" });

            return result;
        }

        private static IList<BookValuationRequestDto> Map(IEnumerable<BookValuationRequest> bookValuationRequests)
        {
            return bookValuationRequests.Select(bookValuationRequest => new BookValuationRequestDto
            {
                Book = bookValuationRequest.Book,
                EquipmentSelected = bookValuationRequest.EquipmentSelected,
                EquipmentUserActions = Map(bookValuationRequest.EquipmentExplicitActions),
                Mileage = bookValuationRequest.Mileage,
                ReturnId = bookValuationRequest.ReturnId,
                TrimId = bookValuationRequest.TrimId
            }).ToList();
        }

        private static IList<EquipmentUserActionDto> Map(IEnumerable<EquipmentUserAction> equipmentUserActions)
        {
            return equipmentUserActions.Select(equipmentUserAction => new EquipmentUserActionDto
            {
                Action = equipmentUserAction.Action == "A" ? EquipmentActionTypeDto.Add : EquipmentActionTypeDto.Remove,
                Equipment = equipmentUserAction.Equipment
            }).ToList();
        }

        private static IList<BookValuation> Map(IEnumerable<BookValuationDto> bookValuationDtos)
        {
            return bookValuationDtos.Select(bookValuationDto => new BookValuation
                                                                    {
                                                                        ReturnId = bookValuationDto.ReturnId,
                                                                        CheckSum = bookValuationDto.CheckSum,
                                                                        ValuationData = Map(bookValuationDto.ValuationData)
                                                                    }).ToList();
        }

        private static IList<ValuationData> Map(IEnumerable<ValuationDataDto> valuationDataDtos)
        {
            return valuationDataDtos.Select(valuationDataDto => new ValuationData
                                                                    {
                                                                        Condition = valuationDataDto.Condition,
                                                                        Market = valuationDataDto.Market,
                                                                        Valuations = (List<Valuation>)Map(valuationDataDto.Valuations)
                                                                    }).ToList();
        }

        private static IList<Valuation> Map(IEnumerable<ValuationDto> valuationDtos)
        {
            return valuationDtos.Select(valuationDto => new Valuation
                                                            {
                                                                Description = valuationDto.Description,
                                                                Value = valuationDto.Value
                                                            }).ToList();
        }
    }
}