﻿using System;
using System.Net;
using FirstLook.Common.Core.Registry;
using FirstLook.FirstLookServices.DomainModel.PriceComparisons.Commands;
using FirstLook.FirstLookServices.DomainModel.PriceComparisons.Commands.TransferObjects.Envelopes;
using FirstLook.FirstLookServices.WebServiceStack.Utility;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;
using ServiceStack.Common.Web;
using System.Linq;
using FirstLook.FirstLookServices.WebServiceStack.Services.PriceComparisons.Models;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.PriceComparisons
{
    [Authenticate]
    public class Service : ServiceStack.ServiceInterface.Service
    {

        private readonly VersionMapper<Request.PriceComaprison> _PricingComparisons = new VersionMapper<Request.PriceComaprison>("v1");


        public object Post(Request.PriceComaprison request)
        {
            try
            {
                return _PricingComparisons.GetServiceFunction(request.Ver, "post")(request);
            }
            catch (Exception ex)
            {
                var err = this.ExceptionToError(ex, null);
                return (err != null) ? this.HandleError(err) : null;
            }
        }



        public Service()
        {
            _PricingComparisons.Add("v1", "post", post_v1);

        }

        public IHttpResult post_v1(Request.PriceComaprison request)
        {
            var factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();
            string oldETag = RequestContext.GetHeader("If-None-Match");
            var command = factory.CreateFetchPriceComparisonsCommand();


            //var arguments = new PriceComparisonsArgumentsDto ()
            //{
                
            //};

            var arguments = new PriceComparisonsArgumentsDto()
            {
                DealerId=request.DealerId,
                InventoryIds=request.InventoryIds
                
                //PriceComparisonsInputArgumentDto = request.PriceComparisonsInput.Select(obj => new PriceComparisonsInputArgumentDto {
                //DealerId=obj.DealerId,
                //InventoryId=obj.InventoryId
                
                //}).ToList()
 };

            PriceComparisonsResultsDto results = Helper.ExecuteCommand(command, Helper.WrapInContext(arguments, this.GetSession().UserAuthName));



            Response.PriceComparisonsResponse response = new Response.PriceComparisonsResponse() ;
            response.PriceComparisons = new List<PriceComparison>();
            

            if (results.PriceComparisons != null && results.PriceComparisons.FirstOrDefault() != null)
            {
                foreach (var v in results.PriceComparisons)
                {
                    PriceComparison pc = new PriceComparison();
                    pc.InventoryId = v.InventoryId;
                    if (v.PriceComparisonsDetails != null)
                    {
                        pc.PriceComparisonDetail = v.PriceComparisonsDetails.Select(obj => new PriceComparisonDetail()
                        {

                            BookId = obj.BookId,
                            BookName = obj.BookName,
                            BookValue = obj.BookValue,
                            IsAccurate = obj.IsAccurate,
                            IsValid = obj.IsValid,
                            ComparisonColor = obj.ComparisonColor
                        }).ToList();
                    }
                    else
                    {
                        pc.PriceComparisonDetail = null;
                    }
                    response.PriceComparisons.Add(pc);
                }
               

            }

            IHttpResult result = Handle304(oldETag, response, true);
            
            if (result.StatusCode != HttpStatusCode.NotModified)
            {
                // ETag does not match so we need to send to updated resource
                result = Helper.SetStaticHeaders(result, new[] { "cacheControlHour" });
            }

       

            return result;
                    

        }

        IHttpResult Handle304(string oldETag, object response, bool compress)
        {
            string newETag = ETagHelper.CreateETagHash(response);

            IHttpResult result = new HttpResult();

            if (oldETag != null && oldETag.Equals(newETag))
            {
                //  ETags match so no need to zip and send the whole response - just headers
                result.StatusCode = HttpStatusCode.NotModified;
            }
            else
            {
                // ETag does not match so we need to send to updated resource
                // 
                if (compress)
                    result = Helper.Compress(this, response);
                else
                    result = new HttpResult
                    {
                        Response = response
                    };

                ETagHelper.AddETagHeader(result, response);

                result.StatusCode = HttpStatusCode.OK;
            }
            return result;
        }

    }
}