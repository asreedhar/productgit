﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using FirstLook.FirstLookServices.WebServiceStack.Services.PriceComparisons.Models;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.PriceComparisons
{
    public class Response
    {
        [DataContract]
        public class PriceComparisonsResponse
        {
            [DataMember(Name = "PriceComaprisons")]
            public List<PriceComparison> PriceComparisons { get; set; }
        }
    }
}