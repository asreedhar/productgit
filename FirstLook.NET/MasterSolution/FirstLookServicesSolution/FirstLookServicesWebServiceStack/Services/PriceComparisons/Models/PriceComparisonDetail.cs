﻿using System.Runtime.Serialization;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.PriceComparisons.Models
{
    [DataContract]
    public class PriceComparisonDetail
    {
     
        [DataMember(Name="BookId")]
        public int BookId { get; set; }
        [DataMember(Name = "BookName")]
        public string BookName { get; set; }
        [DataMember(Name = "BookValue")]
        public int BookValue { get; set; }
        [DataMember(Name = "IsValid")]
        public bool IsValid { get; set; }
        [DataMember(Name = "IsAccurate")]
        public bool IsAccurate { get; set; }
        [DataMember(Name = "ComparisonColor")]
        public int ComparisonColor { get; set; }
    }
}