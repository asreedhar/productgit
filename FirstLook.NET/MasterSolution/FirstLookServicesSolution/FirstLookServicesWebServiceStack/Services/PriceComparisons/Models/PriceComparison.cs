﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.PriceComparisons.Models
{
    public class PriceComparison
    {
        public int InventoryId { get; set; }
        public List<PriceComparisonDetail> PriceComparisonDetail { get; set; }
    }
}