﻿using ServiceStack.ServiceHost;
using System.Collections.Generic;

namespace FirstLook.FirstLookServices.WebServiceStack.Services.PriceComparisons
{
    
    public class Request
    {
        [Route("/{Ver}/PriceComparisons/dealer/{DealerId}")]
        [Route("/PriceComparisons/dealer/{DealerId}")]
        public class PriceComaprison
        {
            public string Ver { get; set; }
            public int DealerId { get; set; }
            public List<int> InventoryIds { get; set; }
            
        }
    }
}