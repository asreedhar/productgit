﻿using System;
using System.Web;
using System.Web.Caching;
using FirstLook.Client.DomainModel;
using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;
using FirstLook.Fault.DomainModel.Utilities;

namespace FirstLook.Client.WebApplication
{
    public class Global : HttpApplication
    {
        /// <summary>
        /// Register the required modules on application start.
        /// </summary>        
        protected void Application_Start(object sender, EventArgs e)
        {
            IRegistry registry = RegistryFactory.GetRegistry();

            registry.Register<Module>();

            registry.Register<IDataSessionManager, DataSessionManager>(ImplementationScope.Shared);

            registry.Register<ICache, WebCache>(ImplementationScope.Shared);

            registry.Register<Fault.DomainModel.Module>();

            registry.Register<ICommandExecutor, ListeningCommandExecutor>(ImplementationScope.Isolated);
        }

        /// <summary>
        /// Empty the registry on application end.
        /// </summary>        
        protected void Application_End(object sender, EventArgs e)
        {
            RegistryFactory.GetRegistry().Dispose();
        }

        private class WebCache : ICache
        {
            private readonly Cache _cache;

            public WebCache()
            {
                _cache = HttpContext.Current.Cache;
            }

            public object Add(string key, object value, DateTime absoluteExpiration, TimeSpan slidingExpiration)
            {
                return _cache.Add(
                    key,
                    value,
                    null,
                    absoluteExpiration,
                    slidingExpiration,
                    CacheItemPriority.Normal,
                    null);
            }

            public object Get(string key)
            {
                return _cache.Get(key);
            }

            public object Remove(string key)
            {
                return _cache.Remove(key);
            }
        }

        private class ListeningCommandExecutor : SimpleCommandExecutor
        {

            public ListeningCommandExecutor()
            {
                Add(new FaultWebExceptionObserver());
            }

        }
    }
}