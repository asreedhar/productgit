﻿if (typeof (Licenses) !== "function") {
    var Licenses = function () { };
}

(function () {
    this.raises = this.raises || [];
    // Subjects.
    this.raises.push("Licenses.BrokerChange");
    this.raises.push("Licenses.UserNameChange");
    this.raises.push("Licenses.DeviceTypeChange");
    this.raises.push("Licenses.DeviceChange");
    this.raises.push("Licenses.LoadBroker");
    // Licenses.
    this.raises.push("Licenses.LoadLicenses");
    this.raises.push("Licenses.LoadLicense");
    this.raises.push("Licenses.ChangeLicense");
    this.raises.push("Licenses.SaveLicense");
    this.raises.push("Licenses.RevokeLicense");
    // Leases.
    this.raises.push("Licenses.LoadLeases");
    this.raises.push("Licenses.LoadLease");
    this.raises.push("Licenses.RevokeLease");
    this.raises.push("Licenses.CreateLease");
    // Tokens.
    this.raises.push("Licenses.LoadTokens");    

    this.listensTo = this.listensTo || [];
    // Subjects.
    this.listensTo.push("Licenses.Main");
    this.listensTo.push("Licenses.BrokerLoaded");
    this.listensTo.push("Licenses.UsersLoaded");
    // Licenses.
    this.listensTo.push("Licenses.LicensesLoaded");
    this.listensTo.push("Licenses.LicenseLoaded");
    this.listensTo.push("Licenses.LicenseSaved");
    this.listensTo.push("Licenses.LicenseChanged");
    this.listensTo.push("Licenses.LicenseRevoked");
    // Leases.
    this.listensTo.push("Licenses.LeasesLoaded");
    this.listensTo.push("Licenses.LeaseLoaded");
    this.listensTo.push("Licenses.LeaseRevoked");
    this.listensTo.push("Licenses.LeaseCreated");
    // Tokens.
    this.listensTo.push("Licenses.TokensLoaded");    

    var PublicEvents = {
        "Licenses.Main": function(evt) {                   
            var layout = new LayoutTemplateBuilder();
            layout.clean();
            layout.init();                     
        },
        "Licenses.UsersLoaded": function(evt, data) {            
            var layout = new UsersTemplateBuilder(data.Users);
            layout.clean();
            layout.init();
        },
        "Licenses.BrokerLoaded": function(evt, data) {            
            var layout = new BrokerTemplateBuilder(data.Broker);
            layout.clean();
            layout.init();            
        },
        "Licenses.LicensesLoaded": function(evt, data) {
            var template = new LicensesTemplateBuilder(data.Licenses);
            template.clean();
            template.init();
        },
        "Licenses.LicenseLoaded": function(evt, data) {
            var template = new LicenseTemplateBuilder(data.License);
            template.clean();
            template.init();
        },
        "Licenses.LicenseSaved": function(evt, data) {
            var template = new LicensesTemplateBuilder(data.Licenses);
            template.clean();
            template.init();
        },
        "Licenses.LicenseChanged": function(evt, data) {            
            var template = new LicenseTemplateBuilder(data.License);
            template.clean();
            template.init();
        },
        "Licenses.LicenseRevoked": function(evt, data) {
            var template = new LicensesTemplateBuilder(data.Licenses);
            template.clean();
            template.init();
        },
        "Licenses.LeasesLoaded": function(evt, data, users) {            
            var template = new LeasesTemplateBuilder(data.License.LicenseModel, data.Leases, users);
            template.clean();
            template.init();
        },
        "Licenses.LeaseLoaded": function(evt, data) {
            var template = new LeaseTemplateBuilder(data.Lease);
            template.clean();
            template.init();
        },
        "Licenses.LeaseRevoked": function(evt, data, users) {            
            var template = new LeasesTemplateBuilder(data.License.LicenseModel, data.Leases, users);
            template.clean();
            template.init();
        },
        "Licenses.LeaseCreated": function(evt, data, users) {            
            var template = new LeasesTemplateBuilder(data.License.LicenseModel, data.Leases, users);
            template.clean();
            template.init();
        },
        "Licenses.TokensLoaded": function(evt, data) {
            var template = new TokensTemplateBuilder(data.Tokens);
            template.clean();
            template.init();
        }
    };
    $(Licenses).bind(PublicEvents);

    /* ====================== */
    /* == Layout Template   = */
    /* ====================== */

    var LayoutTemplateBuilder = function(data) {
        this.data = data;
        this.$dom = {
            Form: $('#subject'),
            Broker : $('#broker')
        };
    };

    LayoutTemplateBuilder.prototype = {
        init: function() {                            
            this.build();                    
        },
        clean: function() {            
        },
        build: function() {                        
            this.$dom.Form.bind('click', $.proxy(this.stateChange, this));            
        },
        stateChange: function(evt) {            
            // raise event
            var tgt = evt.target;
            if (tgt.nodeName === "A") 
            {
                var i = tgt.href.lastIndexOf('#'),
                n = tgt.href.substring(i),
                e = document.getElementById(n.substring(1)),
                v = (e == null ? null : e.value);

                // Broker change.
                if (n == "#broker") 
                {
                    $(Licenses).trigger("Licenses.BrokerChange", v);                    
                    $(Licenses).trigger("Licenses.LoadLicenses");
                }
                // Username change.
                else if (n == "#userName") 
                {                    
                    var u = document.getElementById('users');
                    var uval = (u == null ? null : u.value);
                    $(Licenses).trigger("Licenses.UserNameChange", uval);
                    $(Licenses).trigger("Licenses.LoadTokens");
                }
                // Device type.
                else if (n == "#deviceType") 
                {
                    $(Licenses).trigger("Licenses.DeviceTypeChange", v);
                    $(Licenses).trigger("Licenses.LoadTokens");
                }
                // Device handle.
                else if (n == "#device") 
                {
                    $(Licenses).trigger("Licenses.DeviceChange", v);
                    $(Licenses).trigger("Licenses.LoadTokens");
                }
            }
            // kill event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };

    /* ======================== */
    /* == Licenses Template   = */
    /* ======================== */

    var LicensesTemplateBuilder = function(data) {
        this.data = data;
        this.$dom = {
            Form: $('#licenses'),
            Body: $('#licenses_body')            
        };

        // Row with empty inputs and a create link.
        this.createRow = '<tr><td><select name="lproduct" id="lproduct"><option value="0"></option><option value="BlackBook">BlackBook</option><option value="Edmunds">Edmunds</option><option value="Galves">Galves</option><option value="Kelley Blue Book">Kelley Blue Book</option><option value="Manheim">Manheim</option><option value="NAAA">NAAA</option><option value="NADA">NADA</option></select></td>' +
                         '<td><select name="lmodel" id="lmodel"><option value="0"></option><option value="1">Site</option><option value="2">Floating</option><option value="3">Named</option></select></td>' + 
                         '<td><select id="ldeviceType" name="ldeviceType"><option value="0"></option><option value="1">Desktop</option><option value="2">Mobile</option></select></td>' +
                         '<td></td><td colspan="3"><center><a href="#createLicense">Create</a></center></td></tr>';
    };

    LicensesTemplateBuilder.prototype = {        
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {            
            this._table();            
        },
        clean: function() {
            this.$dom.Body.empty();      
            this.$dom.Body.html(this.createRow);
            this.$dom.Form.unbind('click');            
        },
        _table: function() {
            var list = this.data;
            var code = '';
            for (var i = 0; i < list.length; i++) {
                var item = list[i];
                var tr = '<tr>'
                    + '<td>' + item.Product + '</td>' 
                    + '<td><select name="lmodel_' + item.License + '" id="lmodel_' + item.License + '"><option value="1" ' + (item.LicenseModel === 1 ? "selected" : "") + '>Site</option><option value="2" ' + (item.LicenseModel == 2 ? "selected" : "") + '>Floating</option><option value="3" ' + (item.LicenseModel == 3 ? "selected" : "") + '>Named</option>'
                    + '<td>' + Licenses.DeviceType(item.DeviceType) + '</td>'
                    + '<td>' + item.Uses + '</td>'
                    + '<td><a href="#selectLicense_' + item.License + '">Select</a></td>'
                    + '<td><a href="#saveLicense_' + item.License + '">Save</a></td>'
                    + '<td><a href="#revokeLicense_' + item.License + '">Revoke</a></td>'
                    + '<input type="hidden" id="lproduct_' + item.License + '" name="lproduct_' + item.License + '" value="' + item.Product + '" />'
                    + '<input type="hidden" id="ldeviceType_' + item.License + '" name="ldeviceType_' + item.License + '" value="' + item.DeviceType + '" />'
                    + '<input type="hidden" id="lnumuses_' + item.License + '" name="lnumuses_' + item.License + '" value="' + item.Uses + '" />'
                    + '</tr>';                
                code += tr;
            }
            code += this.createRow;

            this.$dom.Body.html(code);
            this.$dom.Form.bind('click', $.proxy(this.licenseAction, this));
        },
        licenseAction: function(evt) {
            // raise event
            var tgt = evt.target;
            if (tgt.nodeName === "A") {

                var cmdStartIndex = tgt.href.lastIndexOf('#');
                var cmdEndIndex   = tgt.href.lastIndexOf('_');
                if (cmdEndIndex == -1) {
                    cmdEndIndex = tgt.href.length;
                }
                var cmd     = tgt.href.substring(cmdStartIndex + 1, cmdEndIndex);
                var license = tgt.href.substring(cmdEndIndex + 1);

                // Create.
                if (cmd === "createLicense") 
                {
                    var product    = document.getElementById('lproduct');                    
                    var model      = document.getElementById('lmodel');
                    var deviceType = document.getElementById('ldeviceType');

                    var productVal    = (product    == null ? null : product.value);
                    var modelVal      = (model      == null ? null : model.value);
                    var deviceTypeVal = (deviceType == null ? null : deviceType.value);

                    $(Licenses).trigger("Licenses.SaveLicense", [null, productVal, modelVal, deviceTypeVal, null, null, null, null]);
                    $(Licenses).trigger("Licenses.LoadLicenses");
                }
                // Select.
                else if (cmd === "selectLicense") 
                {
                    $(Licenses).trigger("Licenses.LoadLicense", license);
                }
                // Save.
                else if (cmd === "saveLicense") 
                {
                    // Add 1 to selected index since 'undefined' is left off this dropdown.                    
                    var model2 = document.getElementById('lmodel_' + license);
                    var model2Val = (model2 == null ? null : model2.selectedIndex + 1);                    
                    $(Licenses).trigger("Licenses.ChangeLicense", [license, model2Val]);
                }
                // Revoke.
                else if (cmd === "revokeLicense") 
                {
                    $(Licenses).trigger("Licenses.RevokeLicense", license);
                }
            }
            // kill event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };

    /* ======================= */
    /* == License Template   = */
    /* ======================= */

    var LicenseTemplateBuilder = function(data) {
        this.data = data;
        this.$dom = {
            Form: $('#licenseDetails'),
            Body: $('#licenseDetails_body'),
            Leases: $('#leases_body'),
            Lease: $('#leaseDetails_body')
        };
    };

    LicenseTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            this._table();            
        },
        clean: function() {
            this.$dom.Body.empty();            
            this.$dom.Leases.empty();
            this.$dom.Leases.html('<tr><td></td><td colspan="2">&nbsp;</td></tr>');
            this.$dom.Lease.empty();
            this.$dom.Lease.html('<td>&nbsp;</td><td></td><td></td><td></td><td></td>');
            this.$dom.Form.unbind('click');
        },
        _table: function() {
            var item = this.data;
            var code = '';                                      

            var dateStr;
            if (item.Expires)
            {
                var dt = new Date(parseInt(item.ExpiryDate.substr(6)));
                dateStr = dt.getFullYear() + '-' + (dt.getMonth() + 1) + '-' + dt.getDate();
            }

            var tr = '<tr>'
                + '<td>' + item.Product + '</td>'
                + '<td>' + Licenses.LicenseModel(item.LicenseModel) + '</td>'
                + '<td>' + Licenses.DeviceType(item.DeviceType) + '</td>'
                + '<td><input type="text" id="lexpiration_' + item.License + '" name="lexpiration_' + item.License + '" size="10" maxlength="10" value="' + (item.Expires ? dateStr : '') + '"/></td>' 
                + '<td><input type="text" id="lseats_' + item.License + '" name="lseats_' + item.License + '" size="10" maxlength="10" value="' + (item.Seats == null ? '' : item.Seats) + '"/></td>'
                + '<td><input type="text" id="luses_' + item.License + '" name="luses_' + item.License + '" size="10" maxlength="10" value="' + (item.UseLimit ? item.AllowedUses : '') + '"/></td>'
                + '<td><a href="#selectLeases">Select</a></td>'
                + '<td><a href="#saveLicense_' + item.License + '">Save</a></td>'
                + '</tr>';            
            code += tr;
            
            this.$dom.Body.html(code);
            this.$dom.Form.bind('click', $.proxy(this.handleAction, this));
        },
        handleAction: function(evt) {
            // raise event
            var tgt = evt.target;
            if (tgt.nodeName === "A") 
            {
                var cmdStartIndex = tgt.href.lastIndexOf('#');
                var cmdEndIndex   = tgt.href.lastIndexOf('_');
                if (cmdEndIndex == -1) {
                    cmdEndIndex = tgt.href.length;
                }
                var cmd     = tgt.href.substring(cmdStartIndex + 1, cmdEndIndex);
                var license = tgt.href.substring(cmdEndIndex + 1);
                
                // Select a license's leases.
                if (cmd === "selectLeases") 
                {
                    $(Licenses).trigger("Licenses.LoadLeases");
                }
                // Save a license.
                else if (cmd === "saveLicense") 
                {                    
                    var product3    = document.getElementById('lproduct_' + license);                    
                    var model3      = document.getElementById('lmodel_' + license);
                    var deviceType3 = document.getElementById('ldeviceType_' + license);
                    var numUses     = document.getElementById('lnumuses_' + license);
                    var seats       = document.getElementById('lseats_' + license);
                    var uses        = document.getElementById('luses_' + license);
                    var expiration  = document.getElementById('lexpiration_' + license);

                    var product3Val    = (product3    == null ? null : product3.value);
                    var model3Val      = (model3      == null ? null : model3.selectedIndex + 1);
                    var deviceType3Val = (deviceType3 == null ? null : deviceType3.value);
                    var numUsesVal     = (numUses     == null ? null : numUses.value);
                    var seatsVal       = (seats       == null ? null : seats.value);
                    var usesVal        = (uses        == null ? null : uses.value);
                    var expirationVal  = (expiration  == null ? null : expiration.value);

                    $(Licenses).trigger("Licenses.SaveLicense", [license, product3Val, model3Val, deviceType3Val, numUsesVal, expirationVal, seatsVal, usesVal]);
                    $(Licenses).trigger("Licenses.LoadLicenses");
                }
            }
            // kill event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };
    
    /* ====================== */
    /* == Leases Template   = */
    /* ====================== */

    var LeasesTemplateBuilder = function(model, data, users) {
        this.data = data;
        this.users = users;
        this.model = model;
        this.$dom = {
            Form: $('#leases'),
            Body: $('#leases_body'),
            Lease: $('#leaseDetails_body')
        };
    };

    LeasesTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            this._table();            
        },
        clean: function() {
            this.$dom.Body.empty();
            this.$dom.Lease.empty();
            this.$dom.Lease.html('<td>&nbsp;</td><td></td><td></td><td></td>');
            this.$dom.Form.unbind('click');
        },
        _table: function() {
            var list = this.data;
            var code = '';
            for (var i = 0; i < list.length; i++) {
                var item = list[i];
                var tr = '<tr>'
                    + '<td>' + item.User.FirstName + ' ' + item.User.LastName + '</td>'                                  
                    + '<td><a href="#selectLease_' + item.User.UserName + '">Select</a></td>'
                    + '<td><a href="#revokeLease_' + item.User.UserName + '">Revoke</a></td>'
                    + '</tr>';                    
                    code += tr;
            }
            // Only named licenses can explicilty create leases.
            if (this.model === 3)
            {                
                code += '<tr><td><select id="createLease" name="createLease"><option value="">&nbsp;</option>';                
                var userList = this.users;
                for (var j = 0; j < userList.length; j++) {
                    var userItem = userList[j];
                    code += '<option value="' + userItem.UserName + '">' + userItem.FirstName + ' ' + userItem.LastName + '</option>';
                }
                code += '</select></td><td colspan="2"><center><a href="#createLease">Create</a></center></td></tr>';
            }
            else if (list.length == 0)
            {
                code += '<tr><td></td><td></td><td colspan="2">&nbsp;</td></tr>';
            }
            
            this.$dom.Body.html(code);            
            this.$dom.Form.bind('click', $.proxy(this.leaseAction, this));
        },
        leaseAction: function(evt) {
            // raise event
            var tgt = evt.target;
            if (tgt.nodeName === "A") 
            {
                var cmdStartIndex = tgt.href.lastIndexOf('#');
                var cmdEndIndex   = tgt.href.lastIndexOf('_');
                if (cmdEndIndex == -1) {
                    cmdEndIndex = tgt.href.length;
                }
                var cmd   = tgt.href.substring(cmdStartIndex + 1, cmdEndIndex);
                var user  = tgt.href.substring(cmdEndIndex + 1);                
                
                // Create a lease.
                if (cmd == "createLease") 
                {                                        
                    var newUser    = document.getElementById('createLease');
                    var newUserVal = (newUser == null ? null : newUser.value);

                    if (newUserVal !== '') {
                        $(Licenses).trigger("Licenses.CreateLease", newUserVal);
                        $(Licenses).trigger("Licenses.LoadLeases");
                    }
                }
                // Revoke a lease.
                else if (cmd == "revokeLease") 
                {
                    $(Licenses).trigger("Licenses.RevokeLease", user);
                    $(Licenses).trigger("Licenses.LoadLeases");
                }
                // Select a lease.
                else if (cmd == "selectLease") 
                {           
                    $(Licenses).trigger("Licenses.LoadLease", user);
                }
            }
            // kill event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };

    /* ======================= */
    /* == Lease Template   = */
    /* ======================= */

    var LeaseTemplateBuilder = function(data) {
        this.data = data;
        this.$dom = {
            Form: $('#leaseDetails'),
            Body: $('#leaseDetails_body')            
        };
    };

    LeaseTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            this._table();            
        },
        clean: function() {
            this.$dom.Body.empty();                        
        },
        _table: function() {
            var item = this.data;            

            var code = '<tr>'
                + '<td>' + item.User.FirstName + ' ' + item.User.LastName + '</td>'
                + '<td>' + new Date(parseInt(item.AssignedOn.substr(6))) + '</td>' 
                + '<td>' + item.AssignedBy.UserName + '</td>'
                + '<td>' + (item.LastUsed == null ? '' : new Date(parseInt(item.LastUsed.substr(6)))) + '</td>'
                + '<td>' + item.Uses + '</td>'                
                + '</tr>';            
            
            this.$dom.Body.html(code);            
        }
    };

    /* ====================== */
    /* == Tokens Template   = */
    /* ====================== */

    var TokensTemplateBuilder = function(data) {
        this.data = data;
        this.$dom = {
            Form: $('#tokens'),
            Body: $('#tokens_body')            
        };
    };

    TokensTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            this._table();            
        },
        clean: function() {
            this.$dom.Body.empty();                        
        },
        _table: function() {
            var list = this.data;
            var code = '';
            for (var i = 0; i < list.length; i++) 
            {
                var item = list[i];

                var statusName = Licenses.TokenStatus(item.Status);
                var style = 'style="background-color: ' + (statusName === 'Valid' ? 'lime' : 'red') + ';color:white;"';
                
                var tr = '<tr>'
                    + '<td>' + item.Product + '</td>'
                    + '<td ' + style +'><b><center>' + statusName + '</center></b></td>'
                    + '<td><center>' + (item.Token == null ? '' : item.Token)  + '</center></td>'
                    + '</tr>';
                    code += tr;
            }
            this.$dom.Body.html(code);            
        }
    };

    /* ====================== */
    /* == Broker Template   = */
    /* ====================== */

    var BrokerTemplateBuilder = function(data) {
        this.data = data;
        this.$dom = {            
            Body: $('#brokerName')            
        };
    };

    BrokerTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            this._table();            
        },
        clean: function() {
            this.$dom.Body.empty();                        
        },
        _table: function() {            
            this.$dom.Body.html(this.data.Client.Name);            
        }
    };

    /* ===================== */
    /* == Users Template   = */
    /* ===================== */

    var UsersTemplateBuilder = function(data) {
        this.data = data;
        this.$dom = {            
            Body: $('#users')            
        };
    };

    UsersTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            this._table();            
        },
        clean: function() {
            this.$dom.Body.empty();                        
        },
        _table: function() {                     
            var list = this.data;
            var code = '<option value="">&nbsp;</option>';
            for (var i = 0; i < list.length; i++) 
            {
                var item = list[i];                                
                var tr = '<option value="' + item.UserName + '">' + item.FirstName + ' ' + item.LastName + '</option>';                    
                code += tr;
            }
            this.$dom.Body.html(code);           
        }
    };

    this.LayoutTemplateBuilder   = LayoutTemplateBuilder;    
    this.LicensesTemplateBuilder = LicensesTemplateBuilder;
    this.LicenseTemplateBuilder  = LicenseTemplateBuilder;
    this.LeasesTemplateBuilder   = LeasesTemplateBuilder;
    this.LeaseTemplateBuilder    = LeaseTemplateBuilder;
    this.TokensTemplateBuilder   = TokensTemplateBuilder;    
    this.BrokerTemplateBuilder   = BrokerTemplateBuilder;
    this.UsersTemplateBuilder    = UsersTemplateBuilder;
    
}).apply(Licenses);