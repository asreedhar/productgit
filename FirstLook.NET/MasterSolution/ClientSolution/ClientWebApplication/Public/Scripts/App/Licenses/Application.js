﻿if (typeof (Licenses) !== "function") {
    var Licenses = function () { };
}

(function () {

    var ns = this;

    var State = {
        Broker: '',
        License: '',
        UserName: '',
        DeviceType: '',
        Device: '',
        Products: [],
        Users: []
    };

    /* ========== */
    /* == Enums = */
    /* ========== */

    var DeviceTypeDto = {
        "Undefined": 0,
        "Desktop": 1,
        "Mobile": 2,
        "toString": function(i) {
            var names = ["Undefined", "Desktop", "Mobile"];
            if (i >= 0 && i < names.length) {
                return names[i];
            }
            return i;
        }
    };

    var LicenseModelTypeDto = {
        "Undefined": 0,
        "Site": 1,
        "Floating": 2,
        "Named": 3,
        "toString": function(i) {
            var names = ["Undefined", "Site", "Floating", "Named"];
            if (i >= 0 && i < names.length) {
                return names[i];
            }
            return i;
        }
    };

    var ProductTypeDto = {
        "Undefined": 0,
        "BlackBook": 1,
        "Edmunds": 2,
        "Galves": 3,
        "Kelley Blue Book": 4,
        "Manheim": 5,
        "NAAA": 6,
        "NADA": 7,
        "toString": function(i) {
            var names = ["Undefined", "BlackBook", "Edmunds", "Galves", "Kelley Blue Book", "Manheim", "NAAA", "NADA"];
            if (i >= 0 && i < names.length) {
                return names[i];
            }
            return i;
        }
    };

    var TokenStatusDto = {
        "Undefined": 0,
        "Valid": 1,        
        "InvalidToken": 2,
        "ExpiredToken": 3,
        "NoLease": 4,
        "NoLicense": 5,
        "ExpiredLicense": 6,
        "SeatLimitExceed": 7,
        "UsageLimitExceeded": 8,
        "toString": function(i) {
            var names = ["Undefined", "Valid", "Invalid Token", "Expired Token", "No Lease", "No License", "Expired License", "No Seats", "License Usage Exceeded"];
            if (i >= 0 && i < names.length) {
                return names[i];
            }
            return i;
        }
    };

    /* ===================== */
    /* == Classes - Common = */
    /* ===================== */

    function UserDto() {
        this.FirstName = null;
        this.LastName = null;
        this.UserName = null;
    }

    function AddressDto() {
        this.Line1 = '';
        this.Line2 = '';
        this.City = '';
        this.State = '';
        this.ZipCode = '';
    }

    function IdentityDto() {
        this.AuthorityName = '';
        this.Name = '';
    }

    function BrokerDto() {
        this.AuthorityName = '';
        this.Client = null;
        this.Handle = '';
    }

    function PrincipalDto() {
        this.AuthorityName = '';
        this.User = null;
        this.Handle = '';
    }

    function ClientDto() {
        this.Id = 0;
        this.Name = '';
        this.OrganizationName = '';
        this.Address = null;
    }

    /* ======================== */
    /* == Classes - Licensing = */
    /* ======================== */

    function LeaseDto() {
        // parent
        this.User = null;
        // self
        this.AssignedBy = null;
        this.AssignedOn = null;
        this.Uses = 0;
        this.LastUsed = null;        
    }

    function LeaseInfoDto() {
        this.User = null;
    }

    function LicenseDto() {
        // parent
        this.License = null;
        this.Product = '';
        this.LicenseModel = 0;
        this.DeviceType = 0;
        this.Uses = 0;
        // self
        this.Seats = null;
        this.UseLimit = false;
        this.AllowedUses = null;
        this.Expires = false;
        this.ExpiryDate = null;        
    }

    function LicenseInfoDto() {        
        this.License = null;
        this.Product = '';
        this.LicenseModel = 0;
        this.DeviceType = 0;
        this.Uses = 0;
    }

    function TokenDto() {
        this.Product = '';
        this.Status = null;
        this.Token = null;
    }        
    
    /* ========================== */
    /* == Envelopes - Licensing = */
    /* ========================== */

    function ChangeLicenseModelArgumentsDto() {
        this.Broker = '';
        this.License = '';
        this.Model = '';
        EventBinder(this);
    }

    function ChangeLicenseModelResultsDto(){
        this.Arguments = null;        
        this.Broker = null;
        this.License = null;
    }

    function CreateLeaseArgumentsDto() {
        this.Broker = '';
        this.License = '';
        this.UserName= '';
        EventBinder(this);
    }

    function CreateLeaseResultsDto() {    
        this.Arguments = null;
        this.Broker = null;
        this.License = null;
        this.Lease = null;
        this.Leases = [];
    }

    function LeaseArgumentsDto() {           
        this.Broker = '';
        this.License = '';
        this.UserName = '';
        EventBinder(this);
    }

    function LeaseResultsDto() {
        this.Arguments = null;
        this.Broker = null;
        this.License = null;
        this.Lease = null;
    }

    function LeasesArgumentsDto() {
        this.Broker = '';
        this.License = '';
        EventBinder(this);
    }

    function LeasesResultsDto() {    
        this.Arguments = null;
        this.Broker = null;
        this.License = null;
        this.Leases = [];
    }

    function LicenseArgumentsDto() {    
        this.Broker = '';
        this.License = '';
        EventBinder(this);
    }

    function LicenseResultsDto() {
        this.Arguments = null;
        this.Broker = null;
        this.License = null;        
    }

    function LicensesArgumentsDto() {
        this.Broker = '';
        EventBinder(this);
    }

    function LicensesResultsDto() {
        this.Arguments = null;
        this.Broker = null;
        this.Licenses = [];
    }

    function RevokeLeaseArgumentsDto() {
        this.Broker = '';
        this.License = '';
        this.UserName = '';
        EventBinder(this);
    }

    function RevokeLeaseResultsDto() {
        this.Arguments = null;
        this.Broker = null;
        this.License = null;
        this.Leases = [];
    }

    function RevokeLicenseArgumentsDto() {
        this.Broker = '';
        this.License = '';
        EventBinder(this);
    }

    function RevokeLicenseResultsDto() {
        this.Arguments = null;
        this.Broker = null;
        this.Licenses = [];    
    }

    function SaveLicenseArgumentsDto() {
        this.Broker = '';
        this.License = null;
        EventBinder(this);
    }

    function SaveLicenseResultsDto() {
        this.Arguments = null;
        this.Broker = null;
        this.License = null;
        this.Licenses = [];
    }

    function TokensArgumentsDto() {
        this.Broker = '';
        this.UserName = '';
        this.Device = null;
        this.DeviceType = null;
        EventBinder(this);
    }

    function TokensResultsDto() {
        this.Arguments = null;
        this.Device = '';
        this.Tokens = [];
    }    

    /* ======================== */
    /* == Envelopes - Clients = */
    /* ======================== */

    function BrokerForClientArgumentsDto() {
        this.Identity = null;
        this.Client = null;
        this.AuthorityName = null;
        EventBinder(this);
    }

    function BrokerForClientResultsDto() {
        this.Arguments = null;
        this.Broker = null;
    }

    function UsersForBrokerArgumentsDto() {
        this.Broker = null;
        EventBinder(this);
    }

    function UsersForBrokerResultsDto() {
        this.Arguments = null;
        this.Users = [];
    }

    /* ================= */
    /* == Data Mapping = */
    /* ================= */

    function genericMapper(type, config) {
        if (typeof config != "object") {
            config = {};
        }
        return function(json) {
            var result = new type(),
            prop,
            subJson,
            idx;
            for (prop in json) {
                if (!json.hasOwnProperty(prop)) continue; /// Ensure actual property
                subJson = json[prop];
                if (config.hasOwnProperty(prop)) {
                    if (subJson !== null && typeof subJson.length == "number" && typeof result[prop].push == "function") {
                        for (idx in subJson) {
                            if (!subJson.hasOwnProperty(idx)) continue;
                            result[prop].push(DataMapper[config[prop]](subJson[idx]));
                        }
                    } else {
                        result[prop] = DataMapper[config[prop]](subJson);
                    }
                } else if (typeof result[prop] !== "undefined") { // don't overwrite properties
                    result[prop] = subJson;
                }
            }
            return result;
        };
    }

    var DataMapper = {
        "BrokerDto": genericMapper(BrokerDto),
        "ClientDto": genericMapper(ClientDto),
        "AddressDto": genericMapper(AddressDto),
        "IdentityDto": genericMapper(IdentityDto),
        "LeaseDto": genericMapper(LeaseDto, {
            "User": "UserDto"
        }),
        "LeaseInfoDto": genericMapper(LeaseInfoDto, {
            "User": "UserDto"
        }),
        "LicenseDto": genericMapper(LicenseDto),
        "LicenseInfoDto": genericMapper(LicenseInfoDto),
        "TokenDto": genericMapper(TokenDto),
        "UserDto": genericMapper(UserDto),
        "ChangeLicenseModelArgumentsDto": genericMapper(ChangeLicenseModelArgumentsDto),
        "ChangeLicenseModelResultsDto": genericMapper(ChangeLicenseModelResultsDto, {
            "Arguments": "ChangeLicenseModelArgumentsDto",
            "Broker": "BrokerDto",
            "License": "LicenseDto"
        }),
        "CreateLeaseArgumentsDto": genericMapper(CreateLeaseArgumentsDto),
        "CreateLeaseResultsDto": genericMapper(CreateLeaseResultsDto, {
            "Arguments": "CreateLeaseArgumentsDto",
            "Broker": "BrokerDto",
            "License": "LicenseInfoDto",
            "Lease": "LeaseDto",
            "Leases": "LeaseInfoDto"
        }),
        "LeaseArgumentsDto": genericMapper(LeaseArgumentsDto),
        "LeaseResultsDto": genericMapper(LeaseResultsDto, {
            "Arguments": "LeaseArgumentsDto", 
            "Broker": "BrokerDto",
            "License": "LicenseInfoDto",
            "Lease": "LeaseDto"
        }),
        "LeasesArgumentsDto": genericMapper(LeasesArgumentsDto),
        "LeasesResultsDto": genericMapper(LeasesResultsDto, {
            "Arguments": "LeasesArgumentsDto",
            "Broker": "BrokerDto",
            "License": "LicenseDto",
            "Leases": "LeaseInfoDto"
        }),
        "LicenseArgumentsDto": genericMapper(LicenseArgumentsDto),
        "LicenseResultsDto": genericMapper(LicenseResultsDto, {
            "Arguments": "LicenseArgumentsDto",
            "Broker": "BrokerDto",
            "License": "LicenseDto"
        }),
        "LicensesArgumentsDto": genericMapper(LicensesArgumentsDto),
        "LicensesResultsDto": genericMapper(LicensesResultsDto, {
            "Arguments": "LicensesArgumentsDto",
            "Broker": "BrokerDto",
            "Licenses": "LicenseInfoDto"
        }),
        "RevokeLeaseArgumentsDto": genericMapper(RevokeLeaseArgumentsDto),
        "RevokeLeaseResultsDto": genericMapper(RevokeLeaseResultsDto, {
            "Arguments": "RevokeLeaseArgumentsDto",
            "Broker": "BrokerDto",
            "License": "LicenseDto",
            "Leases": "LeaseInfoDto"
        }),
        "RevokeLicenseArgumentsDto": genericMapper(RevokeLicenseArgumentsDto),
        "RevokeLicenseResultsDto": genericMapper(RevokeLicenseResultsDto, {
            "Arguments": "RevokeLicenseArgumentsDto",
            "Broker": "BrokerDto",
            "Licenses": "LicenseInfoDto"
        }),
        "SaveLicenseArgumentsDto": genericMapper(SaveLicenseArgumentsDto, {
            "License": "LicenseDto"
        }),
        "SaveLicenseResultsDto": genericMapper(SaveLicenseResultsDto, {
            "Arguments": "SaveLicenseArgumentsDto",
            "Broker": "BrokerDto",
            "License": "LicenseDto",
            "Licenses": "LicenseInfoDto"
        }),
        "TokensArgumentsDto": genericMapper(TokensArgumentsDto),
        "TokensResultsDto": genericMapper(TokensResultsDto, {
            "Arguments": "TokensArgumentsDto",
            "Tokens": "TokenDto"
        }),
        "BrokerForClientArgumentsDto": genericMapper(BrokerForClientArgumentsDto, {
            "Identity": "IdentityDto",
            "Client": "ClientDto"
        }),
        "BrokerForClientResultsDto": genericMapper(BrokerForClientResultsDto, {
            "Arguments": "BrokerForClientArgumentsDto",
            "Broker": "BrokerDto"
        }),
        "UsersForBrokerArgumentsDto": genericMapper(UsersForBrokerArgumentsDto),
        "UsersForBrokerResultsDto": genericMapper(UsersForBrokerResultsDto, {
            "Arguments": "UsersForBrokerArgumentsDto",
            "Users": "UserDto"
        })
    };

    /* ============= */
    /* == Services = */
    /* ============= */

    function genericService2(serviceUrl, resultType, getDataFunc, callSetup) {
        return {
            AjaxSetup: {
                "url": serviceUrl
            },
            fetch: function() {
                var me = this;
                var ajaxSetup = $.extend(Services.Default.AjaxSetup, this.AjaxSetup, callSetup || {});
                ajaxSetup.data = this.getData();
                function onFetchSuccess(json) {
                    var results = resultType.fromJSON(json.d);
                    $(me).trigger("fetchComplete", [results]);
                }
                ajaxSetup.success = onFetchSuccess;
                $.ajax(ajaxSetup);
            },
            getData: getDataFunc
        };
    }

    function genericService(serviceUrl, resultType, getDataFunc) {
        return {
            AjaxSetup: {
                "url": serviceUrl
            },
            fetch: function() {
                var me = this;
                var ajaxSetup = $.extend(Services.Default.AjaxSetup, this.AjaxSetup);
                ajaxSetup.data = this.getData();
                function onFetchSuccess(json) {
                    var results = resultType.fromJSON(json.d);
                    $(me).trigger("fetchComplete", [results]);
                }
                ajaxSetup.success = onFetchSuccess;
                $.ajax(ajaxSetup);
            },
            getData: getDataFunc
        };
    }

    var Services = {
        "Default": {
            AjaxSetup: {
                type: "POST",
                dataType: "json",
                processData: false,
                contentType: "application/json; charset=utf-8",
                error: function(xhr, status, errorThrown) {
                    if (xhr.status === 500) {                        
                        $(Licenses).trigger("Licenses.ErrorState", {errorType: errorThrown, errorResponse: xhr});
                    } else if (xhr.status === 401) {
                        var response = JSON.parse(xhr.responseText);
                        if (!!response['auth-login']) {
                            window.location.assign(response['auth-login']);
                        }
                    }
                }
            }
        },
        "GetLicenses": genericService2("/Client/Services/License.asmx/GetLicenses", LicensesResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes.LicensesArgumentsDto",
                    "Broker": this.Broker
                } 
            });
        }, {
            type: "POST",
            cache: false,
            processData: false
        }),
        "GetLicense": genericService("/Client/Services/License.asmx/GetLicense", LicenseResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes.LicenseArgumentsDto",
                    "Broker": this.Broker,
                    "License": this.License
                } 
            });
        }, {
            type: "POST",
            cache: false,
            processData: false
        }),
        "SaveLicense": genericService("/Client/Services/License.asmx/SaveLicense", SaveLicenseResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes.SaveLicenseArgumentsDto",
                    "Broker": this.Broker,
                    "License": this.License
                } 
            });
        }, {
            type: "POST",
            cache: false,
            processData: false
        }),
        "ChangeLicenseModel": genericService("/Client/Services/License.asmx/ChangeLicenseModel", ChangeLicenseModelResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes.ChangeLicenseModelArgumentsDto",
                    "Broker": this.Broker,
                    "License": this.License,
                    "Model": this.Model
                } 
            });
        }, {
            type: "POST",
            cache: false,
            processData: false
        }),
        "RevokeLicense": genericService("/Client/Services/License.asmx/RevokeLicense", RevokeLicenseResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes.RevokeLicenseArgumentsDto",
                    "Broker": this.Broker,
                    "License": this.License
                } 
            });
        }, {
            type: "POST",
            cache: false,
            processData: false
        }),
        "GetLeases": genericService("/Client/Services/License.asmx/GetLeases", LeasesResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes.LeasesArgumentsDto",
                    "Broker": this.Broker,
                    "License": this.License
                } 
            });
        }, {
            type: "POST",
            cache: false,
            processData: false
        }),
        "GetLease": genericService("/Client/Services/License.asmx/GetLease", LeaseResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes.LeaseArgumentsDto",
                    "Broker": this.Broker,
                    "License": this.License,
                    "UserName": this.UserName
                } 
            });
        }, {
            type: "POST",
            cache: false,
            processData: false
        }),
        "CreateLease": genericService("/Client/Services/License.asmx/CreateLease", CreateLeaseResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes.CreateLeaseArgumentsDto",
                    "Broker": this.Broker,
                    "License": this.License,
                    "UserName": this.UserName
                } 
            });
        }, {
            type: "POST",
            cache: false,
            processData: false
        }),
        "RevokeLease": genericService("/Client/Services/License.asmx/RevokeLease", RevokeLeaseResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes.RevokeLeaseArgumentsDto",
                    "Broker": this.Broker,
                    "License": this.License,
                    "UserName": this.UserName
                } 
            });
        }, {
            type: "POST",
            cache: false,
            processData: false
        }),
        "GetTokens": genericService("/Client/Services/License.asmx/GetTokens", TokensResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes.TokensArgumentsDto",
                    "Broker": this.Broker,                    
                    "UserName": this.UserName,
                    "Device": this.Device,
                    "DeviceType": this.DeviceType
                } 
            });
        }, {
            type: "POST",
            cache: false,
            processData: false
        }),
        "BrokerForClient": genericService2("/Client/Services/Client.asmx/GetBrokerForClient", BrokerForClientResultsDto, function() {
            return {
                "arguments": JSON.stringify({
                    "__type": "FirstLook.Client.DomainModel.Clients.Commands.TransferObjects.Envelopes.BrokerForClientArgumentsDto",
                    "AuthorityName": this.AuthorityName,
                    "Client": this.Client
                })
            };
        }, {
            type: "GET",
            cache: true,
            processData: true
        }),
        "UsersForBroker": genericService2("/Client/Services/Client.asmx/UsersForBroker", UsersForBrokerResultsDto, function() {
            return {
                "arguments": JSON.stringify({
                    "__type": "FirstLook.Client.DomainModel.Clients.Commands.TransferObjects.Envelopes.UsersForBrokerArgumentsDto",                    
                    "Broker": this.Broker
                })
            };
        }, {
            type: "GET",
            cache: true,
            processData: true
        })
    };

    /* =========== */
    /* == Events = */
    /* =========== */

    var Events = {        
        "LicensesArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(Licenses).trigger("Licenses.LicensesLoaded", data);
            }
        },
        "LicenseArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(Licenses).trigger("Licenses.LicenseLoaded", data);
            }
        },
        "SaveLicenseArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(Licenses).trigger("Licenses.LicenseSaved", data);
            }
        },
        "ChangeLicenseModelArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(Licenses).trigger("Licenses.LicenseChanged", data);
            }
        },
        "RevokeLicenseArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(Licenses).trigger("Licenses.LicenseRevoked", data);
            }
        },
        "LeasesArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(Licenses).trigger("Licenses.LeasesLoaded", [data, State.Users]);
            }
        },
        "LeaseArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(Licenses).trigger("Licenses.LeaseLoaded", data);
            }
        },
        "CreateLeaseArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(Licenses).trigger("Licenses.LeaseCreated", [data, State.Users]);
            }
        },
        "RevokeLeaseArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(Licenses).trigger("Licenses.LeaseRevoked", [data, State.Users]);
            }
        },
        "TokensArgumentsDto": {
            fetchComplete: function(event, data) {
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(Licenses).trigger("Licenses.TokensLoaded", data);
            }
        },        
        "BrokerForClientArgumentsDto": {
            fetchComplete: function(event, data) {                
                if (!!!data) {
                    throw new Error("Missing Data");
                }
                $(Licenses).trigger("Licenses.BrokerLoaded", data);
                $(Licenses).trigger("Licenses.BrokerChange", data.Broker.Handle);
                $(Licenses).trigger("Licenses.LoadLicenses");
            }
        },        
        "UsersForBrokerArgumentsDto": {
            fetchComplete: function(event, data) {                
                if (!!!data) {
                    throw new Error("Missing Data");
                }                
                $(Licenses).trigger("Licenses.UsersLoaded", data);
                State.Users = data.Users;
            }
        }
    };

    var EventBinder = function(obj) {
        var type = EventBinder.getType(obj);
        if (!!Events[type]) {
            var events = Events[type];
            for (var e in events) {
                if (!events.hasOwnProperty(e)) continue;
                $(obj).bind(e, events[e]); // TODO: events need to be framework independent
            }
        }
    };

    EventBinder.map = {        
        'LicensesArgumentsDto':           LicensesArgumentsDto,
        'LicenseArgumentsDto':            LicenseArgumentsDto,
        'SaveLicenseArgumentsDto':        SaveLicenseArgumentsDto,
        'ChangeLicenseModelArgumentsDto': ChangeLicenseModelArgumentsDto,
        'RevokeLicenseArgumentsDto':      RevokeLicenseArgumentsDto,
        'LeasesArgumentsDto':             LeasesArgumentsDto,
        'LeaseArgumentsDto':              LeaseArgumentsDto,
        'CreateLeaseArgumentsDto':        CreateLeaseArgumentsDto,
        'RevokeLeaseArgumentsDto':        RevokeLeaseArgumentsDto,
        'TokensArgumentsDto':             TokensArgumentsDto,
        'BrokerForClientArgumentsDto':    BrokerForClientArgumentsDto,
        'UsersForBrokerArgumentsDto':     UsersForBrokerArgumentsDto
    };

    EventBinder.getType = function(obj) {
        for (var type in EventBinder.map) {
            if (!EventBinder.map.hasOwnProperty(type)) continue;
            if (obj instanceof EventBinder.map[type]) {
                return type;
            }
        }
        return undefined;
    };

    // ====================
    // = Bind DataMappers =
    // ====================

    (function() {
        function BindDataMapper(target, source) {
            target.fromJSON = source;
        }        

        BindDataMapper(LeaseDto,                       DataMapper.LeaseDto);
        BindDataMapper(LeaseInfoDto,                   DataMapper.LeaseInfoDto);
        BindDataMapper(LicenseDto,                     DataMapper.LicenseDto);
        BindDataMapper(LicenseInfoDto,                 DataMapper.LicenseInfoDto);
        BindDataMapper(TokenDto,                       DataMapper.TokenDto);
        BindDataMapper(UserDto,                        DataMapper.UserDto);
        BindDataMapper(ChangeLicenseModelArgumentsDto, DataMapper.ChangeLicenseModelArgumentsDto);
        BindDataMapper(ChangeLicenseModelResultsDto,   DataMapper.ChangeLicenseModelResultsDto);
        BindDataMapper(CreateLeaseArgumentsDto,        DataMapper.CreateLeaseArgumentsDto);
        BindDataMapper(CreateLeaseResultsDto,          DataMapper.CreateLeaseResultsDto);
        BindDataMapper(LeaseArgumentsDto,              DataMapper.LeaseArgumentsDto);
        BindDataMapper(LeaseResultsDto,                DataMapper.LeaseResultsDto);
        BindDataMapper(LeasesArgumentsDto,             DataMapper.LeasesArgumentsDto);
        BindDataMapper(LeasesResultsDto,               DataMapper.LeasesResultsDto);
        BindDataMapper(LicenseArgumentsDto,            DataMapper.LicenseArgumentsDto);
        BindDataMapper(LicenseResultsDto,              DataMapper.LicenseResultsDto);        
        BindDataMapper(LicensesArgumentsDto,           DataMapper.LicensesArgumentsDto);
        BindDataMapper(LicensesResultsDto,             DataMapper.LicensesResultsDto);
        BindDataMapper(RevokeLeaseArgumentsDto,        DataMapper.RevokeLeaseArgumentsDto);
        BindDataMapper(RevokeLeaseResultsDto,          DataMapper.RevokeLeaseResultsDto);     
        BindDataMapper(RevokeLicenseArgumentsDto,      DataMapper.RevokeLicenseArgumentsDto);
        BindDataMapper(RevokeLicenseResultsDto,        DataMapper.RevokeLicenseResultsDto);
        BindDataMapper(SaveLicenseArgumentsDto,        DataMapper.SaveLicenseArgumentsDto);
        BindDataMapper(SaveLicenseResultsDto,          DataMapper.SaveLicenseResultsDto);
        BindDataMapper(TokensArgumentsDto,             DataMapper.TokensArgumentsDto);
        BindDataMapper(TokensResultsDto,               DataMapper.TokensResultsDto);
        BindDataMapper(BrokerForClientArgumentsDto,    DataMapper.BrokerForClientArgumentsDto);
        BindDataMapper(BrokerForClientResultsDto,      DataMapper.BrokerForClientResultsDto);
        BindDataMapper(UsersForBrokerArgumentsDto,     DataMapper.UsersForBrokerArgumentsDto);
        BindDataMapper(UsersForBrokerResultsDto,       DataMapper.UsersForBrokerResultsDto);
    })();

    // =======================
    // = Bind Service Fetchs =
    // =======================
    (function() {
        function BindFetch(target, source) {
            for (var m in source) {
                target.prototype[m] = source[m];
            }
            $(target).bind("fetch", target.prototype.fetch); // TODO: events need to be framework independent
        }
        
        BindFetch(LicensesArgumentsDto,           Services.GetLicenses);
        BindFetch(LicenseArgumentsDto,            Services.GetLicense);                       
        BindFetch(SaveLicenseArgumentsDto,        Services.SaveLicense);
        BindFetch(ChangeLicenseModelArgumentsDto, Services.ChangeLicenseModel);
        BindFetch(RevokeLicenseArgumentsDto,      Services.RevokeLicense);
        BindFetch(LeasesArgumentsDto,             Services.GetLeases);
        BindFetch(LeaseArgumentsDto,              Services.GetLease);
        BindFetch(CreateLeaseArgumentsDto,        Services.CreateLease);
        BindFetch(RevokeLeaseArgumentsDto,        Services.RevokeLease);               
        BindFetch(TokensArgumentsDto,             Services.GetTokens);
        BindFetch(BrokerForClientArgumentsDto,    Services.BrokerForClient);
        BindFetch(UsersForBrokerArgumentsDto,     Services.UsersForBroker);
    })();

    /* ================== */
    /* = PUBLIC METHODS = */
    /* ================== */
    if (typeof this.prototype === "undefined") {
        this.prototype = {};
    }
    this.prototype.main = function(selected) {
        this.State = State; /// TODO: refactor so this can is not a single global, PM
        if (typeof selected === "undefined") {
            selected = {};
        }        
        
        // Business unit id.
        if (selected.buid) {
            $(Licenses).trigger("Licenses.LoadBroker", selected.buid);
        }               
        else {
            $(Licenses).trigger("Licenses.Main");
        }                
    };

    this.raises = this.raises || [];
    this.raises.push("Licenses.Main");
    this.raises.push("Licenses.LicensesLoaded");
    this.raises.push("Licenses.LicenseLoaded");
    this.raises.push("Licenses.LicenseSaved");
    this.raises.push("Licenses.LicenseChanged");
    this.raises.push("Licenses.LicenseRevoked");
    this.raises.push("Licenses.LeasesLoaded");
    this.raises.push("Licenses.LeaseLoaded");
    this.raises.push("Licenses.LeaseRevoked");
    this.raises.push("Licenses.LeaseCreated");
    this.raises.push("Licenses.TokensLoaded");    
    this.raises.push("Licenses.BrokerLoaded");
    this.raises.push("Licenses.UsersLoaded");

    this.listensTo = this.listensTo || [];
    this.listensTo.push("Licenses.BrokerChange");
    this.listensTo.push("Licenses.UserNameChange");
    this.listensTo.push("Licenses.DeviceTypeChange");
    this.listensTo.push("Licenses.DeviceChange");
    this.listensTo.push("Licenses.LoadLicenses");
    this.listensTo.push("Licenses.LoadLicense");
    this.listensTo.push("Licenses.SaveLicense");
    this.listensTo.push("Licenses.ChangeLicense");
    this.listensTo.push("Licenses.RevokeLicense");
    this.listensTo.push("Licenses.LoadLeases");
    this.listensTo.push("Licenses.LoadLease");
    this.listensTo.push("Licenses.CreateLease");
    this.listensTo.push("Licenses.RevokeLease");
    this.listensTo.push("Licenses.LoadTokens");
    this.listensTo.push("Licenses.LoadBroker");
    this.listensTo.push("Licenses.LoadUsers");
    this.listensTo.push("Licenses.ErrorState");


    var PublicEvents = {  
        
        // Subjects (i.e. dealer / user / device /etc).

        "Licenses.BrokerChange": function(evt, newBroker) {                
            State.Broker = newBroker;            
            $(Licenses).trigger("Licenses.LoadUsers");
        },
        "Licenses.UserNameChange": function(evt, newUserName) {            
            State.UserName = newUserName;            
        },
        "Licenses.DeviceTypeChange": function(evt, newDeviceType) {             
            State.DeviceType = newDeviceType;
        },
        "Licenses.DeviceChange": function(evt, newDevice) {            
            State.Device = newDevice;
        },
        "Licenses.LoadBroker": function(evt, clientId) {                        
            var brokerForClientArgs = new BrokerForClientArgumentsDto();
            brokerForClientArgs.Client = new ClientDto();
            brokerForClientArgs.Client.Id = clientId;
            brokerForClientArgs.AuthorityName = "CAS";
            
            $(brokerForClientArgs).trigger("fetch");
        },
        "Licenses.LoadUsers": function(evt) {            
            var usersForBrokerArgs = new UsersForBrokerArgumentsDto();
            usersForBrokerArgs.Broker = State.Broker;
            $(usersForBrokerArgs).trigger("fetch");
        },

        // Licenses.

        "Licenses.LoadLicenses": function(evt) {            
            if (State.Broker !== '') {
                var licensesArgs = new LicensesArgumentsDto();

                licensesArgs.Broker = State.Broker;                

                $(licensesArgs).trigger("fetch");
            }
        },
        "Licenses.LoadLicense": function(evt, lid) {                    
            if (State.Broker !== '') {                
                State.License = lid;

                var licenseArgs = new LicenseArgumentsDto();

                licenseArgs.Broker = State.Broker;
                licenseArgs.License = State.License;                                            
                
                $(licenseArgs).trigger("fetch");
            }            
        },
        "Licenses.SaveLicense": function(evt, license, product, model, deviceType, numUses, expiration, seats, usage) {         
            if (State.Broker !== '') {
                var saveLicenseArgs = new SaveLicenseArgumentsDto();
                                                
                saveLicenseArgs.Broker               = State.Broker;
                saveLicenseArgs.License              = new LicenseDto();
                saveLicenseArgs.License.License      = license;
                saveLicenseArgs.License.Product      = product;
                saveLicenseArgs.License.LicenseModel = model;
                saveLicenseArgs.License.DeviceType   = deviceType;

                if (numUses !== null) {
                    saveLicenseArgs.License.Uses = numUses;
                }

                if (expiration !== null && expiration !== '') {
                    saveLicenseArgs.License.Expires = true;
                    saveLicenseArgs.License.ExpiryDate = expiration;
                }
                else {
                    saveLicenseArgs.License.Expires = false;
                    saveLicenseArgs.License.ExpiryDate = null;
                }

                if (seats === '') {
                    seats = null;
                }
                saveLicenseArgs.License.Seats = seats;

                if (usage !== null && usage !== '') {
                    saveLicenseArgs.License.UseLimit = true;
                    saveLicenseArgs.License.AllowedUses = usage;
                }
                else {
                    saveLicenseArgs.License.UseLimit = false;
                    saveLicenseArgs.License.AllowedUses = null;
                }
                
                $(saveLicenseArgs).trigger("fetch");
            }
        },
        "Licenses.ChangeLicense": function(evt, license, model)  {
            if (State.Broker !== '') {
                var changeLicenseArgs = new ChangeLicenseModelArgumentsDto();                
                                
                changeLicenseArgs.Broker  = State.Broker;
                changeLicenseArgs.License = license;
                changeLicenseArgs.Model   = model;                  
                
                $(changeLicenseArgs).trigger("fetch");
            }
        },
        "Licenses.RevokeLicense": function(evt, license)  {            
            if (State.Broker !== '') {
                State.License = license;

                var revokeLicenseArgs = new RevokeLicenseArgumentsDto();

                revokeLicenseArgs.Broker = State.Broker;
                revokeLicenseArgs.License = State.License;                
                
                $(revokeLicenseArgs).trigger("fetch");
            }
        },

        // Leases.

        "Licenses.LoadLeases": function(evt)  {
            if (State.Broker !== '' && State.License !== '') {                
                var leasesArgs = new LeasesArgumentsDto();

                leasesArgs.Broker  = State.Broker;
                leasesArgs.License = State.License;
                
                $(leasesArgs).trigger("fetch");
            }
        },
        "Licenses.LoadLease": function(evt, userName) {
            if (State.Broker !== '' && State.License !== '') {                
                State.UserName = userName;

                var leaseArgs = new LeaseArgumentsDto();

                leaseArgs.Broker   = State.Broker;
                leaseArgs.License  = State.License;
                leaseArgs.UserName = State.UserName;

                $(leaseArgs).trigger("fetch");
            }
        },
        "Licenses.CreateLease": function(evt, userName) {
            if (State.Broker !== '' && State.License !== '') {                
                State.UserName = userName;

                var createLeaseArgs = new CreateLeaseArgumentsDto();

                createLeaseArgs.Broker   = State.Broker;
                createLeaseArgs.License  = State.License;
                createLeaseArgs.UserName = State.UserName;

                $(createLeaseArgs).trigger("fetch");
            }
        },
        "Licenses.RevokeLease": function(evt, userName) {            
            if (State.Broker !== '' && State.License !== '' && userName !== '') {
                var revokeLeaseArgs = new RevokeLeaseArgumentsDto();

                revokeLeaseArgs.Broker   = State.Broker;
                revokeLeaseArgs.License  = State.License;
                revokeLeaseArgs.UserName = userName;

                $(revokeLeaseArgs).trigger("fetch");
            }
        },

        // Tokens.

        "Licenses.LoadTokens": function(evt) {
            if (State.Broker !== '' && State.UserName !== '' && State.DeviceType !== '' && State.DeviceType !== '0') {
                var tokensArgs = new TokensArgumentsDto();

                tokensArgs.Broker   = State.Broker;                
                tokensArgs.UserName = State.UserName;
                tokensArgs.DeviceType = State.DeviceType;

                if (State.Device !== '') {
                    tokensArgs.Device = State.Device;
                }

                $(tokensArgs).trigger("fetch");
            }
        },

        // Errors.

        "Licenses.ErrorState": function(evt, error) {            
            alert(error.errorResponse.responseText);
        }
    };
    $(Licenses).bind(PublicEvents);
    
    /* ========================== */
    /* = PUBLIC STATIC METHODS  = */
    /* ========================== */
    this.ProductType  = ProductTypeDto.toString;
    this.LicenseModel = LicenseModelTypeDto.toString;
    this.DeviceType   = DeviceTypeDto.toString;
    this.TokenStatus  = TokenStatusDto.toString;
    
}).apply(Licenses);