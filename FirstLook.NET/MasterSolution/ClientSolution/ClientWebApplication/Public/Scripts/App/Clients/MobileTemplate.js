if (typeof(Clients) !== "function") {
    var Clients = function() {};
}

(function() {
    this.raises = this.raises || [];
    this.raises.push("Clients.QueryChange");
    this.raises.push("Clients.StartIndexChange");
    this.raises.push("Clients.ClientChange");

    this.listensTo = this.listensTo || [];
    this.listensTo.push("Clients.StateLoaded");
    this.listensTo.push("Clients.ClientsLoaded");
    this.listensTo.push("Clients.BrokerLoaded");
    this.listensTo.push("Clients.ShowQuery");

    var State = {
        ShouldShowSearch: false
    };

    var PublicEvents = {
        "Clients.StateLoaded": function(evt, state) {
            _.extend(State, state);
        },
        "Clients.ShowQuery": function(evt) {
            var card = FirstLook.cards.get("Clients");
            State.ShouldShowSearch = true;

            State.Clients = []; // Reset Client list, PM

            card.build(State);
            FirstLook.templates.showToolbarBackButton();
            FirstLook.templates.hideToolbarActionButton();
        },
        "Clients.HideQuery": function(evt) {
            var card = FirstLook.cards.get("Clients");
            State.ShouldShowSearch = false;

            card.build(State);
            FirstLook.templates.hideToolbarBackButton();
            FirstLook.templates.showToolbarActionButton();
        },
        "Clients.ClientsLoaded": function(evt, clients, authority) {
            var card = FirstLook.cards.get("Clients");

            if (clients.Clients.length === 1) {
                $(Clients).trigger("Clients.ClientChange", clients.Clients[0].Id);
            } else {
                card.build(clients);
                FirstLook.templates.hideToolbarBackButton();
                FirstLook.templates.showToolbarActionButton();
            }
        },
        "Clients.BrokerLoaded": function(evt, broker) {
            State = {ShouldShowSearch:true};
            $(Clients).trigger("Clients.StartIndexChange", [0]);
        }
    };

    $(Clients).bind(PublicEvents);

    function isNullOrString(val) {
        return _.isNull(val) && _.isString(val);
    }
    function isNullOrNumber(val) {
        return _.isNull(val) && _.isNumber(val);
    }

    var cards = {
        title: "Dealerships",
        pattern: {
            broker_id: _.isEmpty
        },
        view: {
            select: {
                order: 2,
                pattern: {
                    Clients: function(c) {
                        return _.isArray(c) && c.length > 0;
                    },
                    Arguments: {
                        StartRowIndex: _.isNumber,
                        MaximumRows: _.isNumber
                    },
                    TotalRowCount: _.isNumber
                },
                ClientList: {
                    type: "GroupedList",
                    behaviors: {
                        selectable: {
                            pattern: {
                                Clients: [{
                                    Id: _.isNumber
                                }]
                            },
                            data_accessor: "Id",
                            binds: {
                                "item_selected": {
                                    type: "Clients.ClientChange",
                                    scope: Clients
                                }
                            }
                        },
                        pageable: {
                            pattern: {
                                Arguments: {
                                    StartRowIndex: _.isNumber,
                                    MaximumRows: _.isNumber
                                },
                                TotalRowCount: _.isNumber
                            },
                            has_next_accessor: _,
                            page_size_accessor: "Arguments.MaximumRows",
                            total_items_accessor: "TotalRowCount",
                            start_index_accessor: "Arguments.StartRowIndex",
                            binds: {
                                "start_index_change": {
                                    type: "Clients.StartIndexChange",
                                    scope: Clients
                                }
                            }
                        },
                        infinate_scrollable: _,
                        groupable: {
                            group_accessor: "Clients",
                            group_by_accessor: "OrganizationName",
                            pattern: {
                                Clients: [{
                                    OrganizationName: _.isString
                                }]
                            }
                        }
                    },
                    class_name: "select_list",
                    item_accessor: "Clients",
                    text_accessor: "Name",
                    pattern: {
                        Clients: [{
                            Name: _.isString
                        }]
                    },
                    raises: ["item_selected", "next"]
                }
            },
            empty: {
                order: 1,
                pattern: {
                    Clients: function(c) {
                        return _.isArray(c) && c.length === 0;
                    }
                },
                EmptyComponent: {
                    type: "Static",
                    pattern: _,
                    template: function() {
                        return "<p class='message'>No Clients to Display</p>";
                    }
                }
            },
            search: {
                order: 0,
                pattern: {
                    ShouldShowSearch: function(v) {
                        return v === true;
                    }
                },
                ClientSearchForm: {
                    type: "Fieldset",
                    behaviors: {
                        data_mergable: {
                            data_destination_accessor: "inputs"
                        },
                        submitable: {
                            event: "Clients.QueryChange",
                            scope: Clients
                        }
                    },
                    legend_accessor: "legend",
                    input_accessor: "inputs",
                    name_accessor: "name",
                    button_accessor: "buttons",
                    label_accessor: "label",
                    value_accessor: "data",
                    type_accessor: "type",
                    static_data: {
                        legend: "Search",
                        inputs: [{
                            label: "Type",
                            name: "AuthorityName",
                            data: "AuthorityName",
                            options: [{
                                name: "Dealer",
                                value: "CAS"
                            },
                            {
                                name: "Seller",
                                value: "SELLER"
                            }]
                        },
                        {
                            label: "Name",
                            name: "Client$Name",
                            data: "Client$Name"
                        },
                        {
                            label: "Group",
                            name: "Client$OrganizationName",
                            data: "Client$OrganizationName"
                        },
                        {
                            label: "State",
                            name: "Address$State",
                            data: "Address$State"
                        },
                        {
                            label: "City",
                            name: "Address$City",
                            data: "Address$City"
                        },
                        {
                            label: "Zip",
                            name: "Address$Zip",
                            data: "Address$Zip",
                            type: "tel"
                        }],
                        buttons: ["Search"]
                    },
                    pattern: {
                        "AuthorityName": _.isString,
                        "Client$Name": _.isNullOrString,
                        "Client$OrganizationName": _.isNullOrString,
                        "Address$State": _.isNullOrString,
                        "Address$City": _.isNullOrString,
                        "Address$Zip": _.isNullOrNumber
                    },
                    raises: ["before_submit", "submitted", "client_search"]
                }
            },
            error: {
                order: 10,
                pattern: _,
                EmptyComponent: {
                    type: "Static",
                    pattern: _,
                    template: function(d) {
                        console.log(["Client Called With bad Data:", data]);
                        return "<p class='error'>An Error Has Occured</p>";
                    }
                }
            }
        }
    };

    FirstLook.cards.add(cards, "Clients"); // Cards is a misnomer here
}).apply(Clients);

