if (typeof(Clients) !== "function") {
	var Clients = function() {};
}

(function() {
    this.raises = this.raises || [];
    this.raises.push("Clients.QueryChange");
    this.raises.push("Clients.StartIndexChange");
    this.raises.push("Clients.AuthorityName");
    this.raises.push("Clients.OrganizationNameChange");
    this.raises.push("Clients.AddressLine1Change");
    this.raises.push("Clients.AddressLine2Change");
    this.raises.push("Clients.CityChange");
    this.raises.push("Clients.StateChange");
    this.raises.push("Clients.ZipCodeChange");
    this.raises.push("Clients.ClientChange");

    this.listensTo = this.listensTo || [];
    this.listensTo.push("Clients.StateLoaded"); 
    this.listensTo.push("Clients.ClientsLoaded"); 
    this.listensTo.push("Clients.BrokerLoaded");

    var Events = {
        "Clients.StateLoaded": function(evt, current_state) {
            var template = new ExampleTemplateBuilder(current_state);
            template.clean();
            template.init();
        },
        "Clients.ClientsLoaded": function(evt, clients, authority) {
            var template = new ClientTemplateBuilder(clients, authority);
            template.clean();
            template.init();
        },
        "Clients.BrokerLoaded": function(evt, broker) {
            var brokerTemplate = new BrokerTemplateBuilder(broker);
            brokerTemplate.clean();
            brokerTemplate.init();
        }
    };
    //$(Clients).bind(Events);

	var ns = this;
	var ns_string = "Clients.";

	Ext.regModel(ns_string + "Client", {
		fields: ["Name", "OrganizationName", "Id"]
	});

	Ext.regModel(ns_string + "Query", {
		fields: ["AuthorityName", "Client$Name", "Client$OrganizationName", "Address$Line1", "Address$AddressLine2", "Address$City", "Address$State", "Address$ZipCode", "SortColumns"]
	})

	var queryStore = {};
	var queryView = {};
	function ExampleTemplateBuilder(data) {
		this.data = data;
	}
	ExampleTemplateBuilder.prototype = {
		init: function() {
			if (this.data) {
				if (queryStore.data) {} else {
					queryStore = new Ext.data.JsonStore({
						model: ns_string + "Query",
						data: [this.data]
					});
				}
				this.build();
			}
		},
		build: function() {
			if (queryView.rendered) {
				ns.render(queryView);
			} else {
				queryView = new Ext.form.FormPanel({
					listeners: {
						beforesubmit: function() {
							return false;
						}
					},
					items: [{
						xtype: "fieldset",
						defaults: {
							xtype: "textfield",
							labelWidth: "35%"
						},
						items: [{
							xtype: "selectfield",
							label: "Type",
							name: "AuthorityName",
							options: [{
								text: "Dealer",
								value: "CAS"
							},
							{
								text: "Seller",
								value: "DEMO"
							}]
						},
						{
							label: "Name",
							name: "Client$Name"
						},
						{
							label: "Group",
							name: "Client$OrganizationName"
						},
						{
							label: "City",
							name: "Address$City"
						},
						{
							label: "State",
							name: "Address$State",
							maxLength: 2
						},
						{
							label: "ZIP",
							name: "Address$ZipCode",
							inputType: Ext.is.Phone ? "tel": "number"
						}]
					},
					{
						xtype: "button",
						text: "Search",
						listeners: {
							scope: this,
							tap: this._onTap
						}
					}]
				});
				queryView.load(queryStore.first());
				ns.render(queryView);
			}
		},
		clean: function() {},
		_onTap: function() {
			var first = queryStore.first();
			queryView.updateRecord(first);
			$(new ns.QueryChange(first.data)).trigger("set");
			queryView.hide();
			clientStore.clearData();
			if (clientList) {
				clientList = {};
			};
		}
	};

	var clientStore = {};
	var clientList = {};
	function ClientTemplateBuilder(data) {
		this.data = data;
	}
	ClientTemplateBuilder.prototype = {
		init: function() {
			if (this.data) {
				if (clientStore.data) {
					var client;
					for (var i = 1, l = this.data.Clients.length; i <= l; i++) {
						clientStore.add(this.data.Clients[i - 1]);
					}
				} else {
					clientStore = new Ext.data.JsonStore({
						model: ns_string + "Client",
						groupField: "OrganizationName",
						data: this.data.Clients
					});
				}
				this.build();
			}
		},
		build: function() {
			if (clientList.rendered) {
				clientList.refresh();
				clientList.setLoading(false);
			} else {
				clientList = new Ext.List({
					itemTpl: "<span class='{Class}'>{Name}</span>",
					grouped: this.showGrouped(),
					store: clientStore,
					emptyText: "No Clients Match Search",
					listeners: {
						scope: this,
						afterrender: this._afterrender,
						itemswipe: this._itemswipe,
						itemtap: this._itemtap
					}
				});
				ns.render(clientList);
			}
		},
		showGrouped: function() {
			if (queryStore && queryStore.first) {
				var first = queryStore.first();
				return first ? true: first.get("SortColumns") !== "Name"; // Name has no useful groupings, PM
			}

			return true;
		},
		_afterrender: function(list) {
			list.scroller.addListener("scrollend", this._scrollend, this);
			list.scroller.addListener("scrollstart", this._scrollstart, this);
		},
		_loadMore: function() {
			var indexChange = new ns.StartIndexChange(clientStore.getCount());
			$(indexChange).trigger("set");
			setTimeout(function() {
				clientList.setLoading(true);
			},
			0);
		},
		_itemswipe: function(list, idx, el, evt) {},
		_itemtap: function(list, idx, el, evt) {
			var dealer = list.store.getAt(idx);
			$(new ns.ClientChange(dealer.get("Id"))).trigger("set");
		},
		_scrollstart: function(scroller) {
			this.startingOffset = scroller.getOffset().y;
		},
		_scrollend: function(scroller, offsets) {
			if (this.startingOffset < offsets.y) {
				if (scroller.offsetBoundary.top + offsets.y === 0) {
					this._loadMore();
				}
			}
			this.startingOffset = offsets.y;
		},
		clean: function() {}
	};

	function BrokerTemplateBuilder(data) {
		this.data = data;
	}
	BrokerTemplateBuilder.prototype = {
		init: function() {
			if (this.data) this.build();
		},
		build: function() {
			var formated_broker = Ext.util.Format.guid_for_url(this.data.Broker.Handle);
			if (clientStore) {
				clientStore.clearData();
			};
			if (clientList) {
				clientList.destroy();
				clientList = {};
			}
			$(new ns.StartIndexChange(0)).trigger("set"); // Reset the index for second pass, PM
			Ext.dispatch({
				controller: "selectVehicle",
				action: "select",
				broker: formated_broker,
				historyUrl: "selectVehicle/select/" + formated_broker
			});
		},
		clean: function() {}
	};

	this.ExampleTemplateBuilder = ExampleTemplateBuilder;
	this.ClientTemplateBuilder = ClientTemplateBuilder;
	this.BrokerTemplateBuilder = BrokerTemplateBuilder;
}).apply(Clients);

