(function() {
    this.raises = this.raises || [];
    this.raises.push("Clients.QueryChange");
    this.raises.push("Clients.StartIndexChange");
    this.raises.push("Clients.AuthorityName");
    this.raises.push("Clients.OrganizationNameChange");
    this.raises.push("Clients.AddressLine1Change");
    this.raises.push("Clients.AddressLine2Change");
    this.raises.push("Clients.CityChange");
    this.raises.push("Clients.StateChange");
    this.raises.push("Clients.ZipCodeChange");
    this.raises.push("Clients.ClientChange");

    this.listensTo = this.listensTo || [];
    this.listensTo.push("Clients.StateLoaded"); 
    this.listensTo.push("Clients.ClientsLoaded"); 
    this.listensTo.push("Clients.BrokerLoaded");

    var Events = {
        "Clients.StateLoaded": function(evt, current_state) {
            var template = new ExampleTemplateBuilder(current_state);
            template.clean();
            template.init();
        },
        "Clients.ClientsLoaded": function(evt, clients, authority) {
            var template = new ClientTemplateBuilder(clients, authority);
            template.clean();
            template.init();
        },
        "Clients.BrokerLoaded": function(evt, broker) {
            var brokerTemplate = new BrokerTemplateBuilder(broker);
            brokerTemplate.clean();
            brokerTemplate.init();
        }
    };
    $(Clients).bind(Events);
    

    var ExampleTemplateBuilder = function() {
        this.$dom = {
            AuthorityName: $('#authority_name'),
            ClientName: $('#client_name'),
            ClientOrganizationName: $('#client_organization_name'),
            AddressLine1: $('#address_line_1'),
            AddressLine2: $('#address_line_2'),
            AddressCity: $('#address_city'),
            AddressState: $('#address_state'),
            AddressZipCode: $('#address_zip_code')
        };
    };

    ExampleTemplateBuilder.prototype = {
        init: function() {
            this.build();
        },
        build: function() {
            this.$dom.AuthorityName.bind('change', this.changeAuthorityName);
            this.$dom.ClientName.bind('change', this.changeClientName);
            this.$dom.ClientOrganizationName.bind('change', this.changeClientOrganizationName);
            this.$dom.AddressLine1.bind('change', this.changeAddressLine1);
            this.$dom.AddressLine2.bind('change', this.changeAddressLine2);
            this.$dom.AddressCity.bind('change', this.changeAddressCity);
            this.$dom.AddressState.bind('change', this.changeAddressState);
            this.$dom.AddressZipCode.bind('change', this.changeAddressZipCode);
        },
        clean: function() {
            this.$dom.AuthorityName.unbind('change');
            this.$dom.ClientName.html('').unbind('change');
            this.$dom.ClientOrganizationName.html('').unbind('change');
            this.$dom.AddressLine1.html('').unbind('change');
            this.$dom.AddressLine2.html('').unbind('change');
            this.$dom.AddressCity.html('').unbind('change');
            this.$dom.AddressState.html('').unbind('change');
            this.$dom.AddressZipCode.html('').unbind('change');
        },
        changeAuthorityName: function(evt) {
            $(Clients).trigger("Clients.AuthorityNameChange", [evt.target.value]);
        },
        changeClientName: function(evt) {
            $(Clients).trigger("Clients.NameChange", [evt.target.value]);
        },
        changeClientOrganizationName: function(evt) {
            $(Clients).trigger("Clients.OrganizationNameChange", [evt.target.value]);
        },
        changeAddressLine1: function(evt) {
            $(Clients).trigger("Clients.AddressLine1Change", [evt.target.value]);
        },
        changeAddressLine2: function(evt) {
            $(Clients).trigger("Clients.AddressLine2Change", [evt.target.value]);
        },
        changeAddressCity: function(evt) {
            $(Clients).trigger("Clients.CityChange", [evt.target.value]);
        },
        changeAddressState: function(evt) {
            $(Clients).trigger("Clients.StateChange", [evt.target.value]);
        },
        changeAddressZipCode: function(evt) {
            $(Clients).trigger("Clients.ZipCodeChange", [evt.target.value]);
        }
    };

    var ClientTemplateBuilder = function(data, authority_name) {
        this.authority_name = authority_name;
        this.data = data;
        this.$dom = {
            'authorities': $('#authority_name'),
            'clients': $('#results_body')
        };
    };

    ClientTemplateBuilder.prototype = {
        init: function() {
            if (!!this.data) {
                this.build();
            }
        },
        build: function() {
            if (this.data.AccountManagers) {
                var mgrs = this.data.AccountManagers;
                var sel = this.$dom.authorities;
                for (var m = 0; m < mgrs.length; m++) {
                    var mgr = mgrs[m];
                    var opt = $('<option></option>').val(mgr.AuthorityName).html(mgr.Name);
                    if (mgr.AuthorityName == this.authority_name) {
                        opt.attr('selected', 'selected');
                    }
                    sel.append(opt);
                }
            }
            var list = this.data.Clients;
            var tb = '';
            for (var i = 0; i < list.length; i++) {
                var item = list[i];
                var tr = '<tr>'
                 + '<td>' + item.Name + '</td>'
                 + '<td>' + item.OrganizationName + '</td>'
                 + '<td>' + item.Address.Line1 + '</td>'
                 + '<td>' + item.Address.Line2 + '</td>'
                 + '<td>' + item.Address.City + '</td>'
                 + '<td>' + item.Address.State + '</td>'
                 + '<td>' + item.Address.ZipCode + '</td>'
                 + '<td><a href="#' + item.Id + '">Enter</a></td>'
                 + '</tr>';
                tb += tr;
            }
            this.$dom.clients.html(tb);
            this.$dom.clients.bind('click', this._click);
        },
        clean: function() {
            this.$dom.clients.unbind('change').html('');
        },
        _click: function(evt) {
            var href = evt.target.href;
            var id = href.substring(href.lastIndexOf('#') + 1);
            $(Clients).trigger("Clients.ClientChange", [id]);
            evt.preventDefault();
            return false;
        }
    };

    function BrokerTemplateBuilder(data) {
        this.data = data;
    }
    BrokerTemplateBuilder.prototype = {
        init: function() {
            if (this.data) { 
                this.build();
            };
        },
        build: function() {
           window.location.assign("../Vehicles/Demonstration.aspx?b=" + this.data.Broker.Handle);
        },
        clean: function() {
        }
    }

}).apply(Clients);
