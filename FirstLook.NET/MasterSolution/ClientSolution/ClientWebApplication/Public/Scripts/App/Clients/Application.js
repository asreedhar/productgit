if (typeof(Clients) !== "function") {
    var Clients = function() {};
}

(function() {
    var State = {
        AuthorityName: "CAS",
        // example
        Client$Id: 0,
        Client$Name: null,
        Client$OrganizationName: null,
        Address$Line1: null,
        Address$AddressLine2: null,
        Address$City: null,
        Address$State: null,
        Address$ZipCode: null,
        // pagination
        SortColumns: [{
            'ColumnName': 'OrganizationName',
            'Ascending': true
        },
        {
            'ColumnName': 'Name',
            'Ascending': true
        }],
        MaximumRows: 25,
        StartRowIndex: 0,
        // results
        Clients: []
    };

    /* dto */

    function UserDto() {
        this.FirstName = '';
        this.LastName = '';
        this.UserName = '';
    }

    function ClientDto() {
        this.Id = 0;
        this.Name = '';
        this.OrganizationName = '';
        this.Address = null;
    }
    ClientDto.prototype = {
        fromState: function() {
            var address = new AddressDto();
            address.fromState();

            this.Id = State.Client$Id || 0;
            this.Name = State.Client$Name;
            this.OrganizationName = State.Client$OrganizationName;
            this.Address = address;
        }
    };

    function AddressDto() {
        this.Line1 = '';
        this.Line2 = '';
        this.City = '';
        this.State = '';
        this.ZipCode = '';
    }
    AddressDto.prototype = {
        fromState: function() {
            this.Line1 = State.Address$Line1;
            this.Line2 = State.Address$Line2;
            this.City = State.Address$City;
            this.State = State.Address$State;
            this.ZipCode = State.Address$ZipCode;
        }
    };

    function AccountManagerDto() {
        this.AuthorityName = '';
        this.Name = '';
    }

    function IdentityDto() {
        this.AuthorityName = '';
        this.Name = '';
    }

    function BrokerDto() {
        this.AuthorityName = '';
        this.Client = null;
        this.Handle = '';
    }

    function PrincipalDto() {
        this.AuthorityName = '';
        this.User = null;
        this.Handle = '';
    }

    /* envelopes dto */

    function QueryClientArgumentsDto() {
        this.Identity = null;
        // pagination
        this.SortColumns = '';
        this.MaximumRows = 1;
        this.StartRowIndex = 0;
        // event trigger
        EventBinder(this);
    }

    QueryClientArgumentsDto.prototype = {
        fromState: function() {
            this.SortColumns = State.SortColumns;
            this.MaximumRows = State.MaximumRows;
            this.StartRowIndex = State.StartRowIndex;
        }
    };

    function QueryClientResultsDto() {
        this.Arguments = null;
        this.AccountManagers = [];
        this.Clients = [];
        this.Principal = null;
        this.TotalRowCount = 0;
    }

    function QueryClientByExampleArgumentsDto() {
        this.Identity = null;
        this.AuthorityName = null;
        this.Example = null;
        // pagination
        this.SortColumns = '';
        this.MaximumRows = 1;
        this.StartRowIndex = 0;
        // event trigger
        EventBinder(this);
    }

    QueryClientByExampleArgumentsDto.prototype = {
        fromState: function() {
            var example = new ClientDto();
            example.fromState();

            this.AuthorityName = State.AuthorityName;
            this.Example = example;
            this.SortColumns = State.SortColumns;
            this.MaximumRows = State.MaximumRows;
            this.StartRowIndex = State.StartRowIndex;
        }
    };

    function QueryClientByExampleResultsDto() {
        this.Arguments = null;
        this.Clients = [];
        this.TotalRowCount = 0;
    }

    function BrokerForIdArgumentsDto() {
        this.Identity = null;
        this.Id = 0;
        EventBinder(this);
    }

    function BrokerForIdResultsDto() {
        this.Arguments = null;
        this.Broker = null;
    }

    function BrokerForClientArgumentsDto(client, authorityName) {
        this.Identity = null;
        this.Client = client || null;
        this.AuthorityName = authorityName || null;
        EventBinder(this);
    }

    function BrokerForClientResultsDto() {
        this.Arguments = null;
        this.Broker = null;
    }

    function genericMapper(type, map) {
        if (typeof map != "object") {
            map = {};
        }
        return function(json) {
            var result = new type(),
            prop,
            subJson,
            idx;
            for (prop in json) {
                if (!json.hasOwnProperty(prop)) continue; /// Ensure actual property
                subJson = json[prop];
                if (map.hasOwnProperty(prop)) {
                    if (subJson !== null && typeof subJson.length == "number" && typeof result[prop].push == "function") {
                        for (idx in subJson) {
                            if (!subJson.hasOwnProperty(idx)) continue;
                            result[prop].push(DataMapper[map[prop]](subJson[idx])); // Sub DataMapper call for Typed Lists, PM
                        }
                    } else {
                        result[prop] = DataMapper[map[prop]](subJson); /// Sub DataMapper call for Nested Object Graphs, PM
                    }
                } else if (typeof result[prop] !== "undefined") { // don't overwrite properties
                    result[prop] = subJson;
                }
            }
            return result;
        };
    }
    var DataMapper = {
        "QueryClientArgumentsDto": genericMapper(QueryClientArgumentsDto, {
            "Identity": "IdentityDto"
        }),
        "QueryClientResultsDto": genericMapper(QueryClientResultsDto, {
            "Arguments": "QueryClientArgumentsDto",
            "AccountManagers": "AccountManagerDto",
            "Principal": "PrincipalDto",
            "Clients": "ClientDto"
        }),
        "QueryClientByExampleArgumentsDto": genericMapper(QueryClientByExampleArgumentsDto, {
            "Identity": "IdentityDto",
            "Example": "ClientDto"
        }),
        "QueryClientByExampleResultsDto": genericMapper(QueryClientByExampleResultsDto, {
            "Arguments": "QueryClientByExampleArgumentsDto",
            "Clients": "ClientDto"
        }),
        "BrokerForIdArgumentsDto": genericMapper(BrokerForIdArgumentsDto, {
            "Identity": "IdentityDto"
        }),
        "BrokerForIdResultsDto": genericMapper(BrokerForIdResultsDto, {
            "Arguments": "BrokerByForArgumentsDto",
            "Broker": "BrokerDto"
        }),
        "BrokerForClientArgumentsDto": genericMapper(BrokerForClientArgumentsDto, {
            "Identity": "IdentityDto",
            "Client": "ClientDto"
        }),
        "BrokerForClientResultsDto": genericMapper(BrokerForClientResultsDto, {
            "Arguments": "BrokerForClientArgumentsDto",
            "Broker": "BrokerDto"
        }),
        "ClientDto": genericMapper(ClientDto, {
            "Address": "AddressDto"
        }),
        "AddressDto": genericMapper(AddressDto),
        "IdentityDto": genericMapper(IdentityDto),
        "PrincipalDto": genericMapper(PrincipalDto, {
            "User": "UserDto"
        }),
        "UserDto": genericMapper(UserDto),
        "BrokerDto": genericMapper(BrokerDto, {
            "Client": "ClientDto"
        }),
        "AccountManagerDto": genericMapper(AccountManagerDto)
    };

    function genericService(serviceUrl, resultType, getDataFunc, callSetup) {
        return {
            AjaxSetup: {
                "url": serviceUrl
            },
            fetch: function() {
                var me = this;
                var ajaxSetup = $.extend({}, Services.Default.AjaxSetup, this.AjaxSetup, callSetup || {});
                ajaxSetup.data = this.getData();
                function onFetchSuccess(json) {
                    var results = resultType.fromJSON(json.d);
                    $(me).trigger("fetchComplete", [results]);
                }
                ajaxSetup.success = onFetchSuccess;
                $.ajax(ajaxSetup);
            },
            getData: getDataFunc
        };
    }
    var Services = {
        "Default": {
            AjaxSetup: {
                type: "POST",
                dataType: "json",
                processData: false,
                contentType: "application/json; charset=utf-8",
                error: function(xhr, status, errorThrown) {
                    if (console) console.log(xhr, status, errorThrown);
                    if (xhr.status === 401) {
                        var response = JSON.parse(xhr.responseText);
                        if ( !! response['auth-login']) {
                            window.location.assign(response['auth-login']);
                        }
                    }
                }
            }
        },
        "Query": genericService("/Client/Services/Client.asmx/Query", QueryClientResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Client.DomainModel.Clients.Commands.TransferObjects.Envelopes.QueryClientArgumentsDto",
                    "SortColumns": this.SortColumns,
                    "MaximumRows": this.MaximumRows,
                    "StartRowIndex": this.StartRowIndex,
                    "Identity": this.Identity
                }
            });
        }),
        "QueryByExample": genericService("/Client/Services/Client.asmx/QueryByExample", QueryClientByExampleResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Client.DomainModel.Clients.Commands.TransferObjects.Envelopes.QueryClientByExampleArgumentsDto",
                    "SortColumns": this.SortColumns,
                    "MaximumRows": this.MaximumRows,
                    "StartRowIndex": this.StartRowIndex,
                    "AuthorityName": this.AuthorityName,
                    "Example": this.Example
                }
            });
        }),
        "BrokerForId": genericService("/Client/Services/Client.asmx/GetBrokerForId", BrokerForIdResultsDto, function() {
            return {
                "arguments": JSON.stringify({
                    "__type": "FirstLook.Client.DomainModel.Clients.Commands.TransferObjects.Envelopes.BrokerForIdArgumentsDto",
                    "Id": this.Id
                })
            };
        }, {
            type: "GET",
            cache: true,
            processData: true
        }),
        "BrokerForClient": genericService("/Client/Services/Client.asmx/GetBrokerForClient", BrokerForClientResultsDto, function() {
            return {
                "arguments": JSON.stringify({
                    "__type": "FirstLook.Client.DomainModel.Clients.Commands.TransferObjects.Envelopes.BrokerForClientArgumentsDto",
                    "AuthorityName": this.AuthorityName,
                    "Client": this.Client
                })
            };
        }, {
            type: "GET",
            cache: true,
            processData: true
        })
    };

    var Events = {
        "QueryClientArgumentsDto": {
            fetchComplete: function(event, data) {
                if ( !! ! data) {
                    throw new Error("Missing Data");
                }
                // save client list and authority name
                State.AuthorityName = data.Principal.AuthorityName;
                State.Clients = data.Clients;
                // update template
                $(Clients).trigger("Clients.ClientsLoaded", [data, State.AuthorityName]);
            },
            stateChange: function() {
                var query = new QueryClientArgumentsDto();
                query.fromState();
                $(query).trigger("fetch"); // TODO: events need to be framework independent
            }
        },
        "QueryClientByExampleArgumentsDto": {
            fetchComplete: function(event, data) {
                if ( !! ! data) {
                    throw new Error("Missing Data");
                }
                // save client list
                if (typeof State.Clients === "undefined") {
                    State.Clients = [];
                }
                for (var client in data.Clients) {
                    if (!data.Clients.hasOwnProperty(client)) continue;
                    State.Clients.push(data.Clients[client]);
                }
                // update template
                $(Clients).trigger("Clients.ClientsLoaded", [data, State.AuthorityName]);
            },
            stateChange: function() {
                var example = new QueryClientByExampleArgumentsDto();
                example.fromState();
                $(example).trigger("fetch"); // TODO: events need to be framework independent
            }
        },
        "BrokerForIdArgumentsDto": {
            fetchComplete: function(event, data) {
                if ( !! ! data) {
                    throw new Error("Missing Data");
                }
                alert(JSON.stringify(data));
            },
            stateChange: function() {
                $(this).trigger("fetch"); // TODO: events need to be framework independent
            }
        },
        "BrokerForClientArgumentsDto": {
            fetchComplete: function(event, data) {
                if ( !! ! data) {
                    throw new Error("Missing Data");
                }
                $(Clients).trigger("Clients.BrokerLoaded", [data]);
            },
            stateChange: function() {
                $(this).trigger("fetch"); // TODO: events need to be framework independent
            }
        }
    };

    var EventBinder = function(obj) {
        var type = EventBinder.getType(obj);
        if ( !! Events[type]) {
            var events = Events[type];
            for (var e in events) {
                $(obj).bind(e, events[e]); // TODO: events need to be framework independent
            }
        }
    };
    EventBinder.map = {
        'QueryClientArgumentsDto': QueryClientArgumentsDto,
        'QueryClientByExampleArgumentsDto': QueryClientByExampleArgumentsDto,
        'BrokerForIdArgumentsDto': BrokerForIdArgumentsDto,
        'BrokerForClientArgumentsDto': BrokerForClientArgumentsDto
    };
    EventBinder.getType = function(obj) {
        for (var type in EventBinder.map) {
            if (obj instanceof EventBinder.map[type]) {
                return type;
            }
        }
        return undefined;
    };

    // ====================
    // = Bind DataMappers =
    // ====================
    (function() {
        function BindDataMapper(target, source) {
            target.fromJSON = source;
        }

        BindDataMapper(QueryClientByExampleArgumentsDto, DataMapper.QueryClientByExampleArgumentsDto);
        BindDataMapper(QueryClientByExampleResultsDto, DataMapper.QueryClientByExampleResultsDto);

        BindDataMapper(QueryClientArgumentsDto, DataMapper.QueryClientArgumentsDto);
        BindDataMapper(QueryClientResultsDto, DataMapper.QueryClientResultsDto);

        BindDataMapper(BrokerForIdArgumentsDto, DataMapper.BrokerForIdArgumentsDto);
        BindDataMapper(BrokerForIdResultsDto, DataMapper.BrokerForIdResultsDto);

        BindDataMapper(BrokerForClientArgumentsDto, DataMapper.BrokerForClientArgumentsDto);
        BindDataMapper(BrokerForClientResultsDto, DataMapper.BrokerForClientResultsDto);

        BindDataMapper(ClientDto, DataMapper.ClientDto);
        BindDataMapper(AddressDto, DataMapper.AddressDto);
        BindDataMapper(IdentityDto, DataMapper.IdentityDto);
        BindDataMapper(UserDto, DataMapper.UserDto);
        BindDataMapper(PrincipalDto, DataMapper.PrincipalDto);
        BindDataMapper(BrokerDto, DataMapper.BrokerDto);

    })();

    // =======================
    // = Bind Service Fetchs =
    // =======================
    (function() {
        function BindFetch(target, source) {
            for (var m in source) {
                target.prototype[m] = source[m];
            }
            $(target).bind("fetch", target.prototype.fetch); // TODO: events need to be framework independent
        }

        BindFetch(QueryClientArgumentsDto, Services.Query);
        BindFetch(QueryClientByExampleArgumentsDto, Services.QueryByExample);

        BindFetch(BrokerForIdArgumentsDto, Services.BrokerForId);
        BindFetch(BrokerForClientArgumentsDto, Services.BrokerForClient);

    })();

    /* ================== */
    /* = PUBLIC METHODS = */
    /* ================== */
    if (typeof this.prototype === "undefined") {
        this.prototype = {};
    }
    this.prototype.main = function(config) {
        var opts = {
            showQuery: true
        };
        if (typeof config !== "undefined") {
            opts.showQuery = !! config.showQuery;
        }
        this.State = State;
        if (opts.showQuery) this.showQueryView();

        var query = new QueryClientArgumentsDto();
        query.fromState();
        $(query).trigger("fetch"); // TODO: events need to be framework independent
    };
    this.prototype.showQueryView = function() {
        $(Clients).trigger("Clients.StateLoaded", [State]);
    };

    this.raises = this.raises || [];
    this.raises.push("Clients.StateLoaded");
    this.raises.push("Clients.ClientsLoaded");
    this.raises.push("Clients.BrokerLoaded");

    this.listensTo = this.listensTo || [];
    this.listensTo.push("Clients.QueryChange");
    this.listensTo.push("Clients.StartIndexChange");
    this.listensTo.push("Clients.NameChange");
    this.listensTo.push("Clients.AuthorityNameChange");
    this.listensTo.push("Clients.OrganizationNameChange");
    this.listensTo.push("Clients.AddressLine1Change");
    this.listensTo.push("Clients.AddressLine2Change");
    this.listensTo.push("Clients.CityChange");
    this.listensTo.push("Clients.StateChange");
    this.listensTo.push("Clients.ZipCodeChange");
    this.listensTo.push("Clients.ClientChange");

    /* ================== */
    /* = PUBLIC CLASSES = */
    /* ================== */
    var PublicEvents = {
        "Clients.QueryChange": function(evt, newState) {
            State.AuthorityName = newState.AuthorityName || State.AuthorityName;
            State.Client$Id = newState.Client$Id || State.Client$Id;
            State.Client$Name = newState.Client$Name || State.Client$Name;
            State.Client$OrganizationName = newState.Client$OrganizationName || State.Client$OrganizationName;
            State.Address$Line1 = newState.Address$Line1 || State.Address$Line1;
            State.Address$AddressLine2 = newState.Address$AddressLine2 || State.Address$AddressLine2;
            State.Address$City = newState.Address$City || State.Address$City;
            State.Address$State = newState.Address$State || State.Address$State;
            State.Address$ZipCode = newState.Address$ZipCode || State.Address$ZipCode;
            State.Clients = [];
            $(new QueryClientByExampleArgumentsDto()).trigger("stateChange"); // TODO: events need to be framework independent
        },
        "Clients.StartIndexChange": function(evt, newStartIndex) {
            State.StartRowIndex = newStartIndex;
            if (State.StartRowIndex !== 0) {
                $(new QueryClientByExampleArgumentsDto()).trigger("stateChange"); // TODO: events need to be framework independent
            }
        },
        "Clients.NameChange": function(evt, newName) {
            State.Name = newName;
            $(new QueryClientByExampleArgumentsDto()).trigger("stateChange"); // TODO: events need to be framework independent
        },
        "Clients.AuthorityNameChange": function(evt, newAuthorityName) {
            State.AuthorityName = newAuthorityName;
            $(new QueryClientByExampleArgumentsDto()).trigger("stateChange"); // TODO: events need to be framework independent
        },
        "Clients.OrganizationNameChange": function(evt, newName) {
            State.Client$Name = newName;
            $(new QueryClientByExampleArgumentsDto()).trigger("stateChange"); // TODO: events need to be framework independent
        },
        "Clients.AddressLine1Change": function(evt, newAddressLine1) {
            State.Address$Line1 = this.newAddressLine1;
            $(new QueryClientByExampleArgumentsDto()).trigger("stateChange"); // TODO: events need to be framework independent
        },
        "Clients.AddressLine2Change": function(evt, newAddressLine2) {
            State.Address$Line2 = this.newAddressLine2;
            $(new QueryClientByExampleArgumentsDto()).trigger("stateChange"); // TODO: events need to be framework independent
        },
        "Clients.CityChange": function(evt, newCity) {
            State.Address$City = newCity;
            $(new QueryClientByExampleArgumentsDto()).trigger("stateChange"); // TODO: events need to be framework independent
        },
        "Clients.StateChange": function(evt, newState) {
            State.Address$State = this.newState;
            $(new QueryClientByExampleArgumentsDto()).trigger("stateChange"); // TODO: events need to be framework independent
        },
        "Clients.ZipCodeChange": function(evt, newZipCode) {
            State.Address$ZipCode = newZipCode;
            $(new QueryClientByExampleArgumentsDto()).trigger("stateChange"); // TODO: events need to be framework independent
        },
        "Clients.ClientChange": function(evt, newClientId) {
            var authorityName = State.AuthorityName;
            var items = State.Clients;
            for (var i = 0, l = items.length; i < l; i++) {
                var item = items[i];
                if (item.Id == newClientId) {
                    var args = new BrokerForClientArgumentsDto(item, authorityName);
                    $(args).trigger("stateChange"); // TODO: events need to be framework independent
                    break;
                }
            }
        }
    };
    $(Clients).bind(PublicEvents);

}).apply(Clients);

