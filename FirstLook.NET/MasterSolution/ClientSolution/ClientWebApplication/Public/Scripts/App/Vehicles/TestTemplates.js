﻿if (typeof(Vehicles) !== "function") {
    var Vehicles = function() {};
} 

(function() {
    this.raises = this.raises || [];
    this.raises.push("Vehicles.BrokerChange");
    this.raises.push("Vehicles.VinChange");
    this.raises.push("Vehicles.VinChange");
    this.raises.push("Vehicles.MakeNameChange");
    this.raises.push("Vehicles.ModelFamilyNameChange");
    this.raises.push("Vehicles.SegmentNameChange");
    this.raises.push("Vehicles.ModelYearChange");
    this.raises.push("Vehicles.EngineNameChange");
    this.raises.push("Vehicles.DriveTrainNameChange");
    this.raises.push("Vehicles.TransmissionNameChange");
    this.raises.push("Vehicles.FuelTypeNameChange");
    this.raises.push("Vehicles.PassengerDoorNameChange");
    this.raises.push("Vehicles.CreateTag");
    this.raises.push("Vehicles.UpdateTag");
    this.raises.push("Vehicles.DeleteTag");
    this.raises.push("Vehicles.TaggedVehicles");
    this.raises.push("Vehicles.ApplyTag");
    this.raises.push("Vehicles.RemoveTag");
    this.raises.push("Vehicles.SummaryRefine");
    this.raises.push("Vehicles.SummaryDetails");

    this.listensTo = this.listensTo || [];
    this.listensTo.push("Vehicles.Main");
    this.listensTo.push("Vehicles.BrokerTagsLoaded");
    this.listensTo.push("Vehicles.StateLoaded");
    this.listensTo.push("Vehicles.SummaryLoaded");
    this.listensTo.push("Vehicles.QueryLoaded");

    var PublicEvents = {
        "Vehicles.Main": function(evt) {
            var layout = new LayoutTemplateBuilder();
            layout.clean();
            layout.init();

            var tags = new TagsTemplateBuilder();
            tags.clean();
            tags.init();
        },
        "Vehicles.BrokerChange": function(evt, newBroker) {
            $(Vehicles).trigger("Vehicles.QueryChange", {});
            $(Vehicles).trigger("Vehicles.SummaryReset");
        },
        "Vehicles.BrokerTagsLoaded": function(evt, tags) {
            var template = new TagsTemplateBuilder(tags);
            template.clean();
            template.init();
        },
        "Vehicles.StateLoaded": function(evt, state) {
            var template = new ExampleTemplateBuilder(state);
            template.clean();
            template.init();
        },
        "Vehicles.SummaryLoaded": function(evt, summary) {
            var template = new SummaryTemplateBuilder(summary);
            template.clean();
            template.init();
        },
        "Vehicles.QueryLoaded": function(evt, query) {
            var template = new VehicleTableTemplateBuilder(query);
            template.clean();
            template.init();
        },
        "Vehicles.VehicleLoaded": function(evt, vehicle) {
            $(Vehicles).trigger("Vehicles.VehicleTags", [vehicle]);
        }
    };
    $(Vehicles).bind(PublicEvents);

    var ns = this;

    var LayoutTemplateBuilder = function() {
        this.$dom = {
            Form: $('#subject'),
            Broker: $('#broker'),
            Vin: $('#vin'),
            Menu: $('#menu'),
            Menu$Item: $('#menu a')
        };
    };

    LayoutTemplateBuilder.prototype = {
        init: function() {
            this.build();
        },
        build: function() {
            this.$dom.Form.bind('click', $.proxy(this.changeBrokerOrVin, this));
            this.$dom.Menu.bind('click', $.proxy(this.changeMenuItem, this));
        },
        clean: function() {
            this.$dom.Menu.unbind('change');
        },
        changeMenuItem: function(evt) {
            var tgt = evt.target;
            if (tgt.nodeName == "LI") {
                tgt = tgt.getElementsByTagName("A")[0];
            }
            this.$dom.Menu$Item.each(function(index) {
                var parent = $(this).parent();
                var i = this.href.lastIndexOf('#'),
                e = this.href.substring(i);
                if (this.href == tgt.href) {
                    parent.addClass('selected');
                    $(e).removeClass('hidden');
                }
                else {
                    parent.removeClass('selected');
                    $(e).addClass('hidden');
                }
            });
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        },
        changeBrokerOrVin: function(evt) {
            // raise event
            var tgt = evt.target;
            if (tgt.nodeName == "A") {
                var i = tgt.href.lastIndexOf('#'),
                e = tgt.href.substring(i),
                v = document.getElementById(e.substring(1)).value,
                c = null;
                if (e == '#broker') {
                    $(Vehicles).trigger("Vehicles.BrokerChange", v);
                }
                else {
                    $(Vehicles).trigger("Vehicles.VinChange", v);
                }
            }
            // kill event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };

    var ExampleTemplateBuilder = function() {
        this.$dom = {
            Query$VehicleActuality1: $('#vehicle_actuality_1'),
            Query$VehicleActuality2: $('#vehicle_actuality_2'),
            Model$MakeName: $('#make_name'),
            Model$ModelFamilyName: $('#model_family_name'),
            Model$SeriesName: $('#series_name'),
            Model$SegmentName: $('#segment_name'),
            Configuration$ModelYear: $('#model_year'),
            Configuration$EngineName: $('#engine_name'),
            Configuration$DriveTrainName: $('#drive_train_name'),
            Configuration$TransmissionName: $('#transmission_name'),
            Configuration$FuelTypeName: $('#fuel_type_name'),
            Configuration$PassengerDoorName: $('#passenger_door_name')
        };
    };

    ExampleTemplateBuilder.prototype = {
        init: function() {
            this.build();
        },
        build: function() {
            this.$dom.Query$VehicleActuality1.bind('change', this.changeAcuality);
            this.$dom.Query$VehicleActuality2.bind('change', this.changeAcuality);
            this.$dom.Model$MakeName.bind('change', this.changeMakeName);
            this.$dom.Model$ModelFamilyName.bind('change', this.changeModelFamilyName);
            this.$dom.Model$SeriesName.bind('change', this.changeSeriesName);
            this.$dom.Model$SegmentName.bind('change', this.changeSegmentName);
            this.$dom.Configuration$ModelYear.bind('change', this.changeModelYear);
            this.$dom.Configuration$EngineName.bind('change', this.changeEngineName);
            this.$dom.Configuration$DriveTrainName.bind('change', this.changeDriveTrainName);
            this.$dom.Configuration$TransmissionName.bind('change', this.changeTransmissionName);
            this.$dom.Configuration$FuelTypeName.bind('change', this.changeFuelTypeName);
            this.$dom.Configuration$PassengerDoorName.bind('change', this.changePassengerDoorName);
        },
        clean: function() {
            this.$dom.Query$VehicleActuality1.unbind('change').html('');
            this.$dom.Query$VehicleActuality2.unbind('change').html('');
            this.$dom.Model$MakeName.unbind('change').html('');
            this.$dom.Model$ModelFamilyName.unbind('change').html('');
            this.$dom.Model$SeriesName.unbind('change').html('');
            this.$dom.Model$SegmentName.unbind('change').html('');
            this.$dom.Configuration$ModelYear.unbind('change').html('');
            this.$dom.Configuration$EngineName.unbind('change').html('');
            this.$dom.Configuration$DriveTrainName.unbind('change').html('');
            this.$dom.Configuration$TransmissionName.unbind('change').html('');
            this.$dom.Configuration$FuelTypeName.unbind('change').html('');
            this.$dom.Configuration$PassengerDoorName.unbind('change').html('');
        },
        changeAcuality: function(evt) {
            $(Vehicles).trigger("Vehicles.ActualityChange", evt.target.value);
        },
        changeMakeName: function(evt) {
            $(Vehicles).trigger("Vehicles.MakeNameChange", evt.target.value);
        },
        changeModelFamilyName: function(evt) {
            $(Vehicles).trigger("Vehicles.ModelFamilyNameChange", evt.target.value);
        },
        changeSeriesName: function(evt) {
            $(Vehicles).trigger("Vehicles.SeriesNameChange", evt.target.value);
        },
        changeSegmentName: function(evt) {
            $(Vehicles).trigger("Vehicles.SegmentNameChange", evt.target.value);
        },
        changeModelYear: function(evt) {
            $(Vehicles).trigger("Vehicles.ModelYearChange", evt.target.value);
        },
        changeEngineName: function(evt) {
            $(Vehicles).trigger("Vehicles.EngineNameChange", evt.target.value);
        },
        changeDriveTrainName: function(evt) {
            $(Vehicles).trigger("Vehicles.DriveTrainNameChange", evt.target.value);
        },
        changeTransmissionName: function(evt) {
            $(Vehicles).trigger("Vehicles.TransmissionNameChange", evt.target.value);
        },
        changeFuelTypeName: function(evt) {
            $(Vehicles).trigger("Vehicles.FuelTypeName", evt.target.value);
        },
        changePassengerDoorName: function(evt) {
            $(Vehicles).trigger("Vehicles.PassengerDoorNameChange", evt.target.value);
        }
    };

    var VehicleTableTemplateBuilder = function(data) {
        this.data = data;
        this.$dom = {
            Body: $('#results_body')
        };
    };

    VehicleTableTemplateBuilder.prototype = {
        init: function() {
            if ( !! this.data) {
                this.build();
            }
        },
        build: function() {
            var list = this.data.Vehicles;
            var tb = '';
            for (var i = 0; i < list.length; i++) {
                var item = list[i];
                var tr = '<tr>' //
                + '<td>' + item.Identification.Vin + '</td>' //
                + '<td>' + item.Mileage + '</td>' //
                + '<td>' + item.Description.MakeName + '</td>' //
                + '<td>' + item.Description.ModelFamilyName + '</td>' //
                + '<td>' + item.Description.SeriesName + '</td>' //
                + '<td>' + item.Description.SegmentName + '</td>' //
                + '<td>' + item.Description.ModelYear + '</td>' //
                + '<td>' + item.Description.EngineName + '</td>' //
                + '<td>' + item.Description.DriveTrainName + '</td>' //
                + '<td>' + item.Description.TransmissionName + '</td>' //
                + '<td>' + item.Description.FuelTypeName + '</td>' //
                + '<td>' + item.Description.PassengerDoorName + '</td>' //
                + '<td><a href="#' + item.Identification.Vin + '">Enter</a></td>' //
                + '</tr>';
                tb += tr;
            }
            this.$dom.Body.html(tb);
            this.$dom.Body.bind('click', this.selectVehicle);
        },
        clean: function() {
            this.$dom.Body.html('');
        },
        selectVehicle: function(evt) {
            // get vin
            var href = evt.target.href;
            var vin = href.substring(href.lastIndexOf('#') + 1);
            alert(vin);
            // kill event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };

    var TagsTemplateBuilder = function(data) {
        this.data = data;
        this.$dom = {
            Form: $('#tags_form'),
            Name: $('#tag_name'),
            Body: $('#tags_results')
        };
    };

    TagsTemplateBuilder.prototype = {
        init: function() {
            if ( !! this.data) {
                this.build();
            }
            this.listen();
        },
        build: function() {
            var list = this.data.Tags;
            var tb = '';
            for (var i = 0; i < list.length; i++) {
                var item = list[i];
                var id = item.Handle,
                name = item.Name;
                var tr = '<tr>' //
                + '<td><input type="text" id="' + id + '" name="' + id + '" value="' + name + '" maxlength="128" /></td>' //
                + '<td></td>' + '<td><a class="update" href="#' + id + '">Update</a></td>' //
                + '<td><a class="delete" href="#' + id + '">Delete</a></td>' //
                + '<td><a class="fetch" href="#' + id + '">Fetch</a></td>' + '<td><a class="apply" href="#' //
                + id + '">+Tag</a></td>' //
                + '<td><a class="remove" href="#' + id + '">-Tag</a></td>' //
                + '</tr>';
                tb += tr;
            }
            this.$dom.Body.html(tb);
            this.$dom.Body.bind('click', $.proxy(this.changeTag, this));
        },
        listen: function() {
            this.$dom.Form.bind('click', $.proxy(this.createTag, this));
        },
        clean: function() {
            this.$dom.Form.unbind('click');
            this.$dom.Body.unbind('click');
            this.$dom.Body.html('');
            this.$dom.Name.html('');
        },
        createTag: function(evt) {
            if (evt.target.nodeName == "A") {
                $(Vehicles).trigger("Vehicles.CreateTag", this.$dom.Name.val());
            }
            // kill event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        },
        changeTag: function(evt) {
            if (evt.target.nodeName == "A") {
                var t = evt.target,
                h = t.href,
                i = h.lastIndexOf('#'),
                e = h.substring(i),
                v = $(e).val(),
                g = e.substring(1);
                var cmd = null;
                switch (t.className) {
                case 'update':
                    $(Vehicles).trigger("Vehicles.UpdateTag", [k, v]);
                    break;
                case 'delete':
                    $(Vehicles).trigger("Vehicles.DeleteTag", [g]);
                    break;
                case 'fetch':
                    $(Vehicles).trigger("Vehicles.TaggedVehicles", [g]);
                    break;
                case 'apply':
                    $(Vehicles).trigger("Vehicles.ApplyTag", [g]);
                    break;
                case 'remove':
                    $(Vehicles).trigger("Vehicles.RemoveTag", [g]);
                    break;
                }
            }
            // kill event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };

    var SummaryTemplateBuilder = function(data) {
        this.data = data;
        this.$dom = {
            Form: $('#summary')
        };
    };

    SummaryTemplateBuilder.prototype = {
        init: function() {
            if ( !! this.data) {
                this.build();
            }
        },
        build: function() {
            var d = this.data,
            a = d.Arguments,
            r = d.Results;
            var e = this.$dom.Form.find('ul[col="' + a.GroupBy + '"]');
            for (var i = 0, l = r.length; i < l; i++) {
                var j = r[i],
                t = $('<span></span>').html(j.Value),
                c = $('<span></span>').html('(' + j.Count + ')');
                $(e).append($('<li></li>').append(t).append(c));
            }
            this.$dom.Form.find('li').bind('click', $.proxy(this.refine, this));
        },
        clean: function() {
            this.$dom.Form.find('li').unbind('click');
            this.$dom.Form.find('ul[clr="true"]').html('');
        },
        refine: function(evt) {
            if (evt.target.nodeName == "LI") {
                var t = evt.target,
                p = $(t).parent('ul'),
                c = $(p).attr('col'),
                v = $(t).find('span:eq(0)').html();
                $(p).find('li').removeClass('selected');
                $(p).attr('clr', 'false');
                $(t).addClass('selected');
                var cmd = null;
                switch (c) {
                case 'ModelYear':
                    $(Vehicles).trigger("Vehicles.SummaryRefine", ["MakeName", c, v]);
                    break;
                case 'MakeName':
                    $(Vehicles).trigger("Vehicles.SummaryRefine", ["ModelFamilyName", c, v]);
                    break;
                case 'ModelFamilyName':
                    $(Vehicles).trigger("Vehicles.SummaryRefine", ["SeriesName", c, v]);
                    break;
                case 'SeriesName':
                    $(Vehicles).trigger("Vehicles.SummaryDetails");
                    break;
                }
            }
            // kill event
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        }
    };
}).apply(Vehicles);

