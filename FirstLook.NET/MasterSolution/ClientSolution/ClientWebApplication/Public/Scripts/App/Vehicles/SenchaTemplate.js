if (typeof(Vehicles) !== "function") {
	var Vehicles = function() {};
}

(function() {
	var ns = this;
	var ns_string = "Vehicles.";

	Ext.regModel(ns_string + "Vehicles", {
		fields: ["Description", "Id", "Identification", "Mileage"]
	});

	Ext.regModel(ns_string + "Query", {
		fields: ["Query$VehicleActuality", "Example$Model$MakeName", "Example$Model$ModelFamilyName", "Example$Model$SeriesName", "Example$Model$SegmentName", "Example$Model$BodyTypeName", "Example$Configuration$ModelYear", "Example$Configuration$EngineName", "Example$Configuration$DriveTrainName", "Example$Configuration$TransmissionName", "Example$Configuration$FuelTypeName", "Example$Configuration$PassengerDoorName", "Example$SortColumns"]
	});

	function LayoutTemplateBuilder(data) {}
	LayoutTemplateBuilder.prototype = {
		init: function() {},
		clean: function() {}
	};

	function TagsTemplateBuilder(data) {}
	TagsTemplateBuilder.prototype = {
		init: function() {},
		clean: function() {}
	};

	var queryStore = {};
	var queryView = {};
	function ExampleTemplateBuilder(data) {
		this.data = data;
	}
	ExampleTemplateBuilder.prototype = {
		init: function() {
			if (this.data) {
				if (queryStore.data) {} else {
					queryStore = new Ext.data.JsonStore({
						model: ns_string + "Query",
						data: [this.data]
					});
				}
				this.build();
			}
		},
		build: function() {
			if (queryView.rendered) {
				ns.render(queryView);
			} else {
				queryView = new Ext.form.FormPanel({
					listeners: {
						beforesubmit: function() {
							return false;
						}
					},
					items: [{
						xtype: "fieldset",
						defaults: {
							xtype: "textfield",
							labelWidth: "35%"
						},
						items: [{
							name: "Example$Model$MakeName",
							label: "Make"
						},
						{
							name: "Example$Model$ModelFamilyName",
							label: "Model"
						},
						{
							name: "Example$Model$SeriesName",
							label: "Series"
						},
						{
							name: "Example$Configuration$ModelYear",
							label: "Year",
							inputType: Ext.is.Phone ? "tel": "number"
						}
						/*,
						{
                            xtype: "selectfield",
							name: "Example$SortColumns",
							label: "Sort",
                            options: [{
                                text: 'Year', value: 'ModelYear, MakeName, ModelFamilyName, SeriesName'
                            }, {
                                text: 'Make', value: 'MakeName, ModelYear, ModelFamilyName, SeriesName'
                            }, {
                                text: 'Model', value: 'ModelFamilyName, MakeName, ModelYear, SeriesName'
                            }, {
                                text: 'Series', value: 'SeriesName, MakeName, ModelYear, ModelFamilyName'
                            }]
						}*/
						]
					},
					{
						xtype: "button",
						text: "Search",
						listeners: {
							scope: this,
							tap: this._onTap
						}
					}]
				});
				queryView.load(queryStore.first());
				ns.render(queryView);
			}
		},
		clean: function() {},
		_onTap: function() {
			var first = queryStore.first();
			queryView.updateRecord(first);
			$(new ns.QueryChange(first.data)).trigger("set");
			queryView.hide();
			vehicleStore.clearData();
			if (vehicleList) {
				vehicleList = {};
			};
		}
	};

	var vehicleStore = {};
	var vehicleList = {};
	function VehicleTableTemplateBuilder(data) {
		this.data = data;
	}
	VehicleTableTemplateBuilder.prototype = {
		init: function() {
			if (this.data) {
				if (vehicleStore.data) {
					for (var i = 1, l = this.data.Vehicles.length; i <= l; i++) {
						vehicleStore.add(this.data.Vehicles[i - 1]);
					}
				} else {
					vehicleStore = new Ext.data.JsonStore({
						model: ns_string + "Vehicles",
						getGroupString: function(record) {
							return record.get("Description").ModelYear.toString();
						},
						data: this.data.Vehicles
					});
				}
				this.build();
			}
		},
		build: function() {
			if ( !! ! vehicleList.store) {
				vehicleList = {};
			};
			if (vehicleList.rendered) {
				vehicleList.refresh();
				vehicleList.setLoading(false);
			} else {
				var template = new Ext.XTemplate('<sapn class="make">{Description.MakeName}</span> ', // ModelFamily
				'<sapn class="model_family">{Description.ModelFamilyName}</span> ', // ModelFamily
				'<sapn class="series">{Description.SeriesName}</span> ', // Series
				'<span class="vin">{Identification.Vin:short_vin}</span> ');
				vehicleList = new Ext.List({
					id: "fl-vehicle-list",
					itemTpl: template,
					grouped: this.showGrouped(),
					store: vehicleStore,
					emptyText: "No Vehicles Match Search",
					listeners: {
						scope: this,
						afterrender: this._afterrender,
						itemswipe: this._itemswipe,
						itemtap: this._itemtap
					}
				});
			}
			ns.render(vehicleList);
		},
		showGrouped: function() {
			if (queryStore && queryStore.first) {
				var first = queryStore.first();
				return first ? true: first.get("SortColumns") !== "Name"; // Name has no useful groupings, PM
			}

			return true;
		},
		_afterrender: function(list) {
			list.scroller.addListener("scrollend", this._scrollend, this);
			list.scroller.addListener("scrollstart", this._scrollstart, this);
		},
		_loadMore: function() {
			var indexChange = new ns.StartIndexChange(vehicleStore.getCount());
			$(indexChange).trigger("set");
			setTimeout(function() {
				vehicleList.setLoading(true);
			},
			0);
		},
		_itemswipe: function(list, idx, el, evt) {},
		_itemtap: function(list, idx, el, evt) {
			var dealer = list.store.getAt(idx);
			var vin = dealer.get("Identification").Vin;
			$(new ns.VinChange(vin)).trigger("set");
			Ext.dispatch({
				controller: "selectVehicle",
				action: "vin",
				vin: vin,
				historyUrl: "VIN/" + vin
			});
		},
		_scrollstart: function(scroller) {
			this.startingOffset = scroller.getOffset().y;
		},
		_scrollend: function(scroller, offsets) {
			if (this.startingOffset < offsets.y) {
				if (scroller.offsetBoundary.top + offsets.y === 0) {
					this._loadMore();
				}
			}
			this.startingOffset = offsets.y;
		},
		clean: function() {}
	};

	function SummaryTemplateBuilder() {}
	SummaryTemplateBuilder.prototype = {
		init: function() {},
		clean: function() {}
	}

	this.LayoutTemplateBuilder = LayoutTemplateBuilder;
	this.TagsTemplateBuilder = TagsTemplateBuilder;
	this.ExampleTemplateBuilder = ExampleTemplateBuilder;
	this.SummaryTemplateBuilder = SummaryTemplateBuilder;
	this.VehicleTableTemplateBuilder = VehicleTableTemplateBuilder;
}).apply(Vehicles);

