﻿if (typeof(Vehicles) !== "function") {
    var Vehicles = function() {};
}

(function() {

    var ns = this;

    var State = {
        Broker: "",
        Vin: "",
        Vehicle: "",
        // query
        Query$VehicleActuality: 1,
        // query example
        Example$Model$MakeName: null,
        Example$Model$ModelFamilyName: null,
        Example$Model$SeriesName: null,
        Example$Model$SegmentName: null,
        Example$Model$BodyTypeName: null,
        Example$Configuration$ModelYear: 0,
        Example$Configuration$EngineName: null,
        Example$Configuration$DriveTrainName: null,
        Example$Configuration$TransmissionName: null,
        Example$Configuration$FuelTypeName: null,
        Example$Configuration$PassengerDoorName: null,
        Example$Configuration$ExcludeMobile: false,
        // pagination
        Example$SortColumns: [{
            'ColumnName': 'Id',
            'Ascending': false
        },        
        {
            'ColumnName': 'ModelYear',
            'Ascending': false
        },
        {
            'ColumnName': 'MakeName',
            'Ascending': true
        },
        {
            'ColumnName': 'ModelFamilyName',
            'Ascending': false
        }],
        Example$MaximumRows: 25,
        Example$StartRowIndex: 0,
        // summary
        Summary$GroupBy: "",
        Summary$Where: [],
        // results
        Vehicles: [],
        Tags: []
    };

    /* ==================== */
    /* = TRANSFER OBJECTS = */
    /* ==================== */

    function VehicleDescriptionDto() {
        // model
        this.MakeName = '';
        this.ModelFamilyName = '';
        this.SeriesName = '';
        this.SegmentName = '';
        this.BodyTypeName = '';
        // configuration
        this.ModelYear = 0;
        this.PassengerDoorName = '';
        this.FuelTypeName = '';
        this.EngineName = '';
        this.DriveTrainName = '';
        this.TransmissionName = '';
        this.ExcludeMobile = false;
    }
    VehicleDescriptionDto.prototype = {
        fromState: function() {
            // model
            this.MakeName = State.Example$Model$MakeName;
            this.ModelFamilyName = State.Example$Model$ModelFamilyName;
            this.SeriesName = State.Example$Model$SeriesName;
            this.SegmentName = State.Example$Model$SegmentName;
            this.BodyTypeName = State.Example$Model$BodyTypeName;
            // configuration
            this.ModelYear = State.Example$Configuration$ModelYear || 0;
            this.PassengerDoorName = State.Example$Configuration$PassengerDoorName;
            this.FuelTypeName = State.Example$Configuration$FuelTypeName;
            this.EngineName = State.Example$Configuration$EngineName;
            this.DriveTrainName = State.Example$Configuration$DriveTrainName;
            this.TransmissionName = State.Example$Configuration$TransmissionName;
            this.ExcludeMobile = State.Example$Configuration$ExcludeMobile;
        }
    };

    function VehicleIdentificationDto() {
        this.Vin = '';
        this.Id = '';
    }

    function VehicleDto() {
        this.Description = null;
        this.Id = 0;
        this.Identification = null;
        this.Mileage = 0;
    }

    function TagDto() {
        this.Name = "";
        this.Handle = "";
        this.TagType = 0;
    }

    var TagTypeDto = {
        "Undefined": 0,
        "Dynamic": 1,
        "Static": 2
    };

    /* ==================== */
    /* =     ENVELOPES    = */
    /* ==================== */

    function QueryArgumentsDto() {
        this.Actuality = null;
        this.Broker = null;
        this.Example = null;
        // pagination
        this.SortColumns = '';
        this.MaximumRows = 1;
        this.StartRowIndex = 0;
        // event trigger
        EventBinder(this);
    }
    QueryArgumentsDto.prototype = {
        fromState: function() {
            var example = new VehicleDescriptionDto();
            example.fromState();

            this.Example = example;
            this.Broker = State.Broker;
            this.Actuality = State.Query$VehicleActuality;
            this.SortColumns = State.Example$SortColumns;
            this.MaximumRows = State.Example$MaximumRows;
            this.StartRowIndex = State.Example$StartRowIndex;
        }
    };

    function QueryResultsDto() {
        this.Arguments = null;
        this.Vehicles = [];
        this.TotalRowCount = 0;
    }

    function IdentifyVehicleArgumentsDto() {
        this.Broker = "";
        this.Vin = "";
        EventBinder(this);
    }
    IdentifyVehicleArgumentsDto.prototype = {
        fromState: function() {
            this.Vin = State.Vin;
            this.Broker = State.Broker;
        }
    };

    function IdentifyVehicleResultsDto() {
        this.Arguments = null;
        this.Vehicle = "";
        this.Description = null;
    }

    function CreateTagArgumentsDto(tagName) {
        this.Broker = "";
        this.TagName = tagName || "";
        EventBinder(this);
    }

    function CreateTagResultsDto() {
        this.Arguments = null;
        this.Tag = null;
    }

    function DeleteTagArgumentsDto(tag) {
        this.Broker = "";
        this.Tag = tag || "";
        EventBinder(this);
    }

    function DeleteTagResultsDto() {
        this.Arguments = null;
        this.VehiclesAffected = 0;
    }

    function UpdateTagArgumentsDto(tag, name) {
        this.Broker = "";
        this.Tag = tag || "";
        this.Name = name || "";
        EventBinder(this);
    }

    function UpdateTagResultsDto() {
        this.Arguments = null;
    }

    function BrokerTagsArgumentsDto() {
        this.Broker = "";
        EventBinder(this);
    }

    function BrokerTagsResultsDto() {
        this.Arguments = null;
        this.Tags = [];
    }

    function ApplyTagArgumentsDto(tag) {
        this.Broker = "";
        this.Tag = tag || "";
        this.Vehicle = "";
        EventBinder(this);
    }

    function ApplyTagResultsDto() {
        this.Arguments = null;
    }

    function RemoveTagArgumentsDto(tag) {
        this.Broker = "";
        this.Tag = tag || "";
        this.Vehicle = "";
        EventBinder(this);
    }

    function RemoveTagResultsDto() {
        this.Arguments = null;
    }

    function SummaryArgumentsDto() {
        this.Broker = "";
        this.Where = [];
        this.GroupBy = "";
        this.Actuality = 1;
        EventBinder(this);
    }

    function SummaryResultsDto() {
        this.Arguments = null;
        this.Results = [];
    }

    function TaggedVehiclesArgumentsDto(tag) {
        this.Broker = "";
        this.Tag = tag || "";
        EventBinder(this);
    }

    function TaggedVehiclesResultsDto() {
        this.Arguments = null;
        this.Vehicles = [];
        this.Result = null;
    }

    function VehicleTagsArgumentsDto(vehicle) {
        this.Broker = "";
        this.Vehicle = vehicle || "";
        EventBinder(this);
    }

    function VehicleTagsResultsDto() {
        this.Arguments = null;
        this.Tags = [];
        this.Result = null;
    }

    /* ==================== */
    /* =      MAPPERS     = */
    /* ==================== */
    function genericMapper(type, config) {
        if (typeof config != "object") {
            config = {};
        }
        return function(json) {
            var result = new type(),
            prop,
            subJson,
            idx;
            for (prop in json) {
                if (!json.hasOwnProperty(prop)) continue; /// Ensure actual property
                subJson = json[prop];
                if (config.hasOwnProperty(prop)) {
                    if (subJson !== null && typeof subJson.length == "number" && typeof result[prop].push == "function") {
                        for (idx in subJson) {
                            if (!subJson.hasOwnProperty(idx)) continue;
                            result[prop].push(DataMapper[config[prop]](subJson[idx]));
                        }
                    } else {
                        result[prop] = DataMapper[config[prop]](subJson);
                    }
                } else if (typeof result[prop] !== "undefined") { // don't overwrite properties
                    result[prop] = subJson;
                }
            }
            return result;
        };
    }

    var DataMapper = {
        "QueryArgumentsDto": genericMapper(QueryArgumentsDto, {
            "Example": "VehicleDescriptionDto"
        }),
        "QueryResultsDto": genericMapper(QueryResultsDto, {
            "Arguments": "QueryArgumentsDto",
            "Vehicles": "VehicleDto"
        }),
        "IdentifyVehicleArgumentsDto": genericMapper(IdentifyVehicleArgumentsDto),
        "IdentifyVehicleResultsDto": genericMapper(IdentifyVehicleResultsDto, {
            "Arguments": "IdentifyVehicleArgumentsDto",
            "Description": "VehicleDescriptionDto"
        }),
        "CreateTagArgumentsDto": genericMapper(CreateTagArgumentsDto),
        "CreateTagResultsDto": genericMapper(CreateTagResultsDto, {
            "Arguments": "CreateTagArgumentsDto",
            "Tag": "TagDto"
        }),
        "UpdateTagArgumentsDto": genericMapper(UpdateTagArgumentsDto),
        "UpdateTagResultsDto": genericMapper(UpdateTagResultsDto, {
            "Arguments": "UpdateTagArgumentsDto"
        }),
        "DeleteTagArgumentsDto": genericMapper(DeleteTagArgumentsDto),
        "DeleteTagResultsDto": genericMapper(DeleteTagResultsDto, {
            "Arguments": "DeleteTagArgumentsDto"
        }),
        "BrokerTagsArgumentsDto": genericMapper(BrokerTagsArgumentsDto),
        "BrokerTagsResultsDto": genericMapper(BrokerTagsResultsDto, {
            "Arguments": "BrokerTagsArgumentsDto",
            "Tags": "TagDto"
        }),
        "ApplyTagArgumentsDto": genericMapper(ApplyTagArgumentsDto),
        "ApplyTagResultsDto": genericMapper(ApplyTagResultsDto, {
            "Arguments": "ApplyTagArgumentsDto"
        }),
        "RemoveTagArgumentsDto": genericMapper(RemoveTagArgumentsDto),
        "RemoveTagResultsDto": genericMapper(RemoveTagResultsDto, {
            "Arguments": "RemoveTagArgumentsDto"
        }),
        "SummaryArgumentsDto": genericMapper(SummaryArgumentsDto),
        "SummaryResultsDto": genericMapper(SummaryResultsDto, {
            "Arguments": "SummaryArgumentsDto"
        }),
        "TaggedVehiclesArgumentsDto": genericMapper(TaggedVehiclesArgumentsDto),
        "TaggedVehiclesResultsDto": genericMapper(TaggedVehiclesResultsDto, {
            "Arguments": "TaggedVehiclesArgumentsDto"
        }),
        "VehicleTagsArgumentsDto": genericMapper(VehicleTagsArgumentsDto),
        "VehicleTagsResultsDto": genericMapper(VehicleTagsResultsDto, {
            "Arguments": "VehicleTagsArgumentsDto"
        }),
        "VehicleDto": genericMapper(VehicleDto, {
            "Description": "VehicleDescriptionDto",
            "Identification": "VehicleIdentificationDto"
        }),
        "VehicleDescriptionDto": genericMapper(VehicleDescriptionDto),
        "VehicleIdentificationDto": genericMapper(VehicleIdentificationDto),
        "TagDto": genericMapper(TagDto)
    };

    /* ==================== */
    /* =     SERVICES     = */
    /* ==================== */
    function genericService(serviceUrl, resultType, getDataFunc, callSetup) {
        return {
            AjaxSetup: {
                "url": serviceUrl
            },
            fetch: function() {
                var me = this;
                var ajaxSetup = $.extend({}, Services.Default.AjaxSetup, this.AjaxSetup, callSetup || {});
                ajaxSetup.data = this.getData();
                function onFetchSuccess(json) {
                    var results = resultType.fromJSON(json.d);
                    $(me).trigger("fetchComplete", [results]);
                }
                ajaxSetup.success = onFetchSuccess;
                $.ajax(ajaxSetup);
            },
            getData: getDataFunc
        };
    }

    var Services = {
        "Default": {
            AjaxSetup: {
                type: "POST",
                dataType: "json",
                processData: false,
                contentType: "application/json; charset=utf-8",
                error: function(xhr, status, errorThrown) {
                    if (console) console.log(xhr, status, errorThrown);
                    if (xhr.status === 401) {
                        var response = JSON.parse(xhr.responseText);
                        if ( !! response['auth-login']) {
                            window.location.assign(response['auth-login']);
                        }
                    }
                }
            }
        },
        "Query": genericService("/Client/Services/Vehicle.asmx/Query", QueryResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes.QueryArgumentsDto",
                    "SortColumns": this.SortColumns,
                    "MaximumRows": this.MaximumRows,
                    "StartRowIndex": this.StartRowIndex,
                    "Broker": this.Broker,
                    "Example": this.Example,
                    "Actuality": this.Actuality
                }
            });
        }),
        "Identify": genericService("/Client/Services/Vehicle.asmx/Identify", IdentifyVehicleResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes.IdentifyVehicleArgumentsDto",
                    "Broker": this.Broker,
                    "Vin": this.Vin
                }
            });
        }),
        "CreateTag": genericService("/Client/Services/Vehicle.asmx/CreateTag", CreateTagResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes.CreateTagArgumentsDto",
                    "Broker": this.Broker,
                    "TagName": this.TagName
                }
            });
        }),
        "UpdateTag": genericService("/Client/Services/Vehicle.asmx/UpdateTag", UpdateTagResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes.UpdateTagArgumentsDto",
                    "Broker": this.Broker,
                    "Tag": this.Tag,
                    "Name": this.Name
                }
            });
        }),
        "DeleteTag": genericService("/Client/Services/Vehicle.asmx/DeleteTag", DeleteTagResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes.DeleteTagArgumentsDto",
                    "Broker": this.Broker,
                    "Tag": this.Tag
                }
            });
        }),
        "Tags": genericService("/Client/Services/Vehicle.asmx/Tags", BrokerTagsResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes.BrokerTagsArgumentsDto",
                    "Broker": this.Broker
                }
            });
        }),
        "ApplyTag": genericService("/Client/Services/Vehicle.asmx/ApplyTag", ApplyTagResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes.ApplyTagArgumentsDto",
                    "Broker": this.Broker,
                    "Tag": this.Tag,
                    "Vehicle": this.Vehicle
                }
            });
        }),
        "RemoveTag": genericService("/Client/Services/Vehicle.asmx/RemoveTag", RemoveTagResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes.RemoveTagArgumentsDto",
                    "Broker": this.Broker,
                    "Tag": this.Tag,
                    "Vehicle": this.Vehicle
                }
            });
        }),
        "Summarize": genericService("/Client/Services/Vehicle.asmx/Summarize", SummaryResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes.SummaryArgumentsDto",
                    "Broker": this.Broker,
                    "Where": this.Where,
                    "GroupBy": this.GroupBy,
                    "Actuality": this.Actuality
                }
            });
        }),
        "TaggedVehicles": genericService("/Client/Services/Vehicle.asmx/TaggedVehicles", SummaryResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes.TaggedVehiclesArgumentsDto",
                    "Broker": this.Broker,
                    "Tag": this.Tag
                }
            });
        }),
        "VehicleTags": genericService("/Client/Services/Vehicle.asmx/VehicleTags", VehicleTagsResultsDto, function() {
            return JSON.stringify({
                "arguments": {
                    "__type": "FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes.VehicleTagsArgumentsDto",
                    "Broker": this.Broker,
                    "Vehicle": this.Vehicle
                }
            });
        })
    };

    /* ==================== */
    /* =      EVENTS      = */
    /* ==================== */
    var Events = {
        "QueryArgumentsDto": {
            fetchComplete: function(event, data) {
                if ( !! ! data) {
                    throw new Error("Missing Data");
                }
                // save client list
                State.Vehicles = data.Vehicles;
                // update template
                $(Vehicles).trigger("Vehicles.QueryLoaded", [data]);
            },
            stateChange: function() {
                var query = new QueryArgumentsDto();
                query.fromState();
                $(query).trigger("fetch");
            }
        },
        "IdentifyVehicleArgumentsDto": {
            fetchComplete: function(event, data) {
                if ( !! ! data) {
                    throw new Error("Missing Data");
                }
                State.Vehicle = data.Vehicle;
                $(Vehicles).trigger("Vehicles.VehicleLoaded", [data]);
            },
            stateChange: function() {
                var identity = new IdentifyVehicleArgumentsDto();
                identity.fromState();
                $(identity).trigger("fetch");
            }
        },
        "CreateTagArgumentsDto": {
            fetchComplete: function(event, data) {
                if ( !! ! data) {
                    throw new Error("Missing Data");
                }
                if (State.Vehicle) { 
                    $(Vehicles).trigger("Vehicles.ApplyTag", data.Tag.Handle);
                }
            },
            stateChange: function() {
                this.Broker = State.Broker;
                $(this).trigger("fetch");
            }
        },
        "UpdateTagArgumentsDto": {
            fetchComplete: function(event, data) {
                if ( !! ! data) {
                    throw new Error("Missing Data");
                }

            },
            stateChange: function() {
                this.Broker = State.Broker;
                $(this).trigger("fetch");
            }
        },
        "DeleteTagArgumentsDto": {
            fetchComplete: function(event, data) {
                if ( !! ! data) {
                    throw new Error("Missing Data");
                }

            },
            stateChange: function() {
                this.Broker = State.Broker;
                $(this).trigger("fetch");
            }
        },
        "BrokerTagsArgumentsDto": {
            fetchComplete: function(event, data) {
                if ( !! ! data) {
                    throw new Error("Missing Data");
                }
                State.Tags = data.Tags;
                $(Vehicles).trigger("Vehicles.BrokerTagsLoaded", [data]);
            },
            stateChange: function() {
                this.Broker = State.Broker;
                $(this).trigger("fetch");
            }
        },
        "ApplyTagArgumentsDto": {
            fetchComplete: function(event, data) {
                if ( !! ! data) {
                    throw new Error("Missing Data");
                }
                // tagged? now what?
            },
            stateChange: function() {
                this.Broker = State.Broker;
                this.Vehicle = State.Vehicle;
                $(this).trigger("fetch");
            }
        },
        "RemoveTagArgumentsDto": {
            fetchComplete: function(event, data) {
                if ( !! ! data) {
                    throw new Error("Missing Data");
                }
                // de-tagged? now what?
            },
            stateChange: function() {
                this.Broker = State.Broker;
                this.Vehicle = State.Vehicle;
                $(this).trigger("fetch");
            }
        },
        "SummaryArgumentsDto": {
            fetchComplete: function(event, data) {
                if ( !! ! data) {
                    throw new Error("Missing Data");
                }
                $(Vehicles).trigger("Vehicles.SummaryLoaded", [data]);
            },
            stateChange: function() {
                if (State.Summary$GroupBy === "") {
                    State.Summary$GroupBy = "ModelYear";
                }
                this.Broker = State.Broker;
                this.Where = State.Summary$Where;
                this.GroupBy = State.Summary$GroupBy;
                $(this).trigger("fetch");
            }
        },
        "TaggedVehiclesArgumentsDto": {
            fetchComplete: function(event, data) {
                if ( !! ! data) {
                    throw new Error("Missing Data");
                }
                // modal tag template ? seperate template?
                //                var template = new ns.SummaryTemplateBuilder(data);
                //                template.clean();
                //                template.init();
            },
            stateChange: function() {
                this.Broker = State.Broker;
                $(this).trigger("fetch");
            }
        },
        "VehicleTagsArgumentsDto": {
            fetchComplete: function(event, data) {
                if ( !! ! data) {
                    throw new Error("Missing Data");
                }
                $(Vehicles).trigger("Vehicles.VehicleTagsLoaded", [data]);
                // modal tag template ? seperate template?
                //                var template = new ns.SummaryTemplateBuilder(data);
                //                template.clean();
                //                template.init();
            },
            stateChange: function() {
                this.Broker = State.Broker;
                $(this).trigger("fetch");
            }
        }
    };

    /* ==================== */
    /* = Bind Events      = */
    /* ==================== */
    var EventBinder = function(obj) {
        var type = EventBinder.getType(obj);
        if ( !! Events[type]) {
            var events = Events[type];
            for (var e in events) {
                $(obj).bind(e, events[e]);
            }
        }
    };
    EventBinder.map = {
        'QueryArgumentsDto': QueryArgumentsDto,
        'IdentifyVehicleArgumentsDto': IdentifyVehicleArgumentsDto,
        'CreateTagArgumentsDto': CreateTagArgumentsDto,
        'UpdateTagArgumentsDto': UpdateTagArgumentsDto,
        'DeleteTagArgumentsDto': DeleteTagArgumentsDto,
        'BrokerTagsArgumentsDto': BrokerTagsArgumentsDto,
        'ApplyTagArgumentsDto': ApplyTagArgumentsDto,
        'RemoveTagArgumentsDto': RemoveTagArgumentsDto,
        'TaggedVehiclesArgumentsDto': TaggedVehiclesArgumentsDto,
        'VehicleTagsArgumentsDto': VehicleTagsArgumentsDto,
        'SummaryArgumentsDto': SummaryArgumentsDto
    };
    EventBinder.getType = function(obj) {
        for (var type in EventBinder.map) {
            if (obj instanceof EventBinder.map[type]) {
                return type;
            }
        }
        return undefined;
    };

    /* ==================== */
    /* = Bind DataMappers = */
    /* ==================== */
    (function() {
        function BindDataMapper(target, source) {
            target.fromJSON = source;
        }

        BindDataMapper(QueryArgumentsDto, DataMapper.QueryArgumentsDto);
        BindDataMapper(QueryResultsDto, DataMapper.QueryResultsDto);

        BindDataMapper(IdentifyVehicleArgumentsDto, DataMapper.IdentifyVehicleArgumentsDto);
        BindDataMapper(IdentifyVehicleResultsDto, DataMapper.IdentifyVehicleResultsDto);

        BindDataMapper(CreateTagArgumentsDto, DataMapper.CreateTagArgumentsDto);
        BindDataMapper(CreateTagResultsDto, DataMapper.CreateTagResultsDto);

        BindDataMapper(UpdateTagArgumentsDto, DataMapper.UpdateTagArgumentsDto);
        BindDataMapper(UpdateTagResultsDto, DataMapper.UpdateTagResultsDto);

        BindDataMapper(DeleteTagArgumentsDto, DataMapper.DeleteTagArgumentsDto);
        BindDataMapper(DeleteTagResultsDto, DataMapper.DeleteTagResultsDto);

        BindDataMapper(ApplyTagArgumentsDto, DataMapper.ApplyTagArgumentsDto);
        BindDataMapper(ApplyTagResultsDto, DataMapper.ApplyTagResultsDto);

        BindDataMapper(BrokerTagsArgumentsDto, DataMapper.BrokerTagsArgumentsDto);
        BindDataMapper(BrokerTagsResultsDto, DataMapper.BrokerTagsResultsDto);

        BindDataMapper(SummaryArgumentsDto, DataMapper.SummaryArgumentsDto);
        BindDataMapper(SummaryResultsDto, DataMapper.SummaryResultsDto);

        BindDataMapper(TaggedVehiclesArgumentsDto, DataMapper.TaggedVehiclesArgumentsDto);
        BindDataMapper(TaggedVehiclesResultsDto, DataMapper.TaggedVehiclesResultsDto);

        BindDataMapper(VehicleTagsArgumentsDto, DataMapper.VehicleTagsArgumentsDto);
        BindDataMapper(VehicleTagsResultsDto, DataMapper.VehicleTagsResultsDto);

        BindDataMapper(VehicleDto, DataMapper.VehicleDto);
        BindDataMapper(VehicleDescriptionDto, DataMapper.VehicleDescriptionDto);
        BindDataMapper(VehicleIdentificationDto, DataMapper.VehicleIdentificationDto);

        BindDataMapper(TagDto, DataMapper.TagDto);
    })();

    /* ==================== */
    /* = Bind Services    = */
    /* ==================== */
    (function() {
        function BindFetch(target, source) {
            for (var m in source) {
                target.prototype[m] = source[m];
            }
            $(target).bind("fetch", target.prototype.fetch);
        }

        BindFetch(QueryArgumentsDto, Services.Query);
        BindFetch(IdentifyVehicleArgumentsDto, Services.Identify);
        BindFetch(CreateTagArgumentsDto, Services.CreateTag);
        BindFetch(UpdateTagArgumentsDto, Services.UpdateTag);
        BindFetch(DeleteTagArgumentsDto, Services.DeleteTag);
        BindFetch(BrokerTagsArgumentsDto, Services.Tags);
        BindFetch(ApplyTagArgumentsDto, Services.ApplyTag);
        BindFetch(RemoveTagArgumentsDto, Services.RemoveTag);
        BindFetch(SummaryArgumentsDto, Services.Summarize);
        BindFetch(TaggedVehiclesArgumentsDto, Services.TaggedVehicles);
        BindFetch(VehicleTagsArgumentsDto, Services.VehicleTags);

    })();

    /* ================== */
    /* = PUBLIC METHODS = */
    /* ================== */
    if (typeof this.prototype === "undefined") {
        this.prototype = {};
    }

    this.prototype.main = function(config) {
        var opts = {
            showQuery: true
        };
        if (typeof config !== "undefined") {
            opts.showQuery = !! config.showQuery;
            opts.broker = config.broker;
            opts.actuality = config.actuality;
        }

        if (typeof opts.actuality === "string") {
            State.Query$VehicleActuality = parseInt(opts.actuality, 10) || State.Query$VehicleActuality;
        }

        if (typeof opts.broker === "string") {
            State.Broker = config.broker;
        }

        $(Vehicles).trigger("Vehicles.Main");

        if (opts.showQuery) this.showQueryView();
        $(Vehicles).trigger("Vehicles.StateLoaded", State);
    };

    this.prototype.showQueryView = function() {
        $(Vehicles).trigger("Vehicles.StateLoaded", State);
    };

    this.raises = this.raises || [];
    this.raises.push("Vehicles.Main");
    this.raises.push("Vehicles.StateLoaded");
    this.raises.push("Vehicles.BrokerChange");
    this.raises.push("Vehicles.ExcludeMobileChange");
    this.raises.push("Vehicles.SummaryLoaded");
    this.raises.push("Vehicles.QueryLoaded");
    this.raises.push("Vehicles.VehicleTagLoaded");

    this.listensTo = this.listensTo || [];
    var PublicEvents = {
        "Vehicles.QueryChange": function(evt, newState) {
            State.Example$StartRowIndex = 0;

            for (var prop in newState) {
                if (prop === "Vehicles") continue;
                if (newState.hasOwnProperty(prop) && State.hasOwnProperty(prop)) {
                    State[prop] = newState[prop];
                }
            }

            $(new QueryArgumentsDto()).trigger("stateChange");
        },
        "Vehicles.StartIndexChange": function(evt, newIndex) {
            State.Example$StartRowIndex = newIndex;

            $(new QueryArgumentsDto()).trigger("stateChange");
        },
        "Vehicles.VinChange": function(evt, newVin) {
            State.Vin = newVin;
            $(new IdentifyVehicleArgumentsDto()).trigger("stateChange");
        },
        "Vehicles.BrokerChange": function(evt, newBroker) {
            State.Broker = newBroker;
            // tags
        },
        "Vehicles.ExcludeMobileChange": function(evt, excludeMobile) {
            State.Example$Configuration$ExcludeMobile = excludeMobile;
        },
        "Vehicles.ActualityChange": function(evt, newActuality) {
            State.Query$VehicleActuality = newActuality;
            $(new QueryArgumentsDto()).trigger("stateChange");
        },
        "Vehicles.MakeNameChange": function(evt, newMakeName) {
            State.Example$Model$MakeName = newMakeName;
            $(new QueryArgumentsDto()).trigger("stateChange");
        },
        "Vehicles.SeriesNameChange": function(evt, newSeriesName) {
            State.Example$Model$SeriesName = newSeriesName;
            $(new QueryArgumentsDto()).trigger("stateChange");
        },
        "Vehicles.SegmentNameChange": function(evt, newSegmentName) {
            State.Example$Model$SegmentName = newSegmentName;
            $(new QueryArgumentsDto()).trigger("stateChange");
        },
        "Vehicles.ModelYearChange": function(evt, newModelYear) {
            State.Example$Configuration$ModelYear = newModelYear;
            $(new QueryArgumentsDto()).trigger("stateChange");
        },
        "Vehicles.ModelFamilyNameChange": function(evt, newModelFamily) {
            State.Example$Model$ModelFamilyName = newModelFamily;
            $(new QueryArgumentsDto()).trigger("stateChange");
        },
        "Vehicles.EngineNameChange": function(evt, newEngineName) {
            State.Example$Configuration$EngineName = newEngineName;
            $(new QueryArgumentsDto()).trigger("stateChange");
        },
        "Vehicles.TransmissionNameChange": function(evt, newTranmissionName) {
            State.Example$Configuration$TransmissionName = newTransmissionName;
            $(new QueryArgumentsDto()).trigger("stateChange");
        },
        "Vehicles.DriveTrainNameChange": function(evt, newDriveTrain) {
            State.Example$Configuration$DriveTrainName = newDriveTrainName;
            $(new QueryArgumentsDto()).trigger("stateChange");
        },
        "Vehicles.FuelTypeNameChange": function(evt, newFuelType) {
            State.Example$Configuration$FuelTypeName = newFuelTypeName;
            $(new QueryArgumentsDto()).trigger("stateChange");
        },
        "Vehicles.PassengerDoorNameChange": function(evt, newPassengerDoor) {
            State.Example$Configuration$PassengerDoorName = newPassengerDoorName;
            $(new QueryArgumentsDto()).trigger("stateChange");
        },
        "Vehicles.CreateTag": function(evt, newTag) {
            $(new CreateTagArgumentsDto(newTag)).trigger("stateChange");
        },
        "Vehicles.UpdateTag": function(evt, theTag, newTag) {
            $(new UpdateTagArgumentsDto(theTag, newName)).trigger("stateChange");
        },
        "Vehicles.ApplyTag": function(evt, theTag) {
            $(new ApplyTagArgumentsDto(theTag)).trigger("stateChange");
        },
        "Vehicles.RemoveTag": function(evt, theTag) {
            $(new RemoveTagArgumentsDto(theTag)).trigger("stateChange");
        },
        "Vehicles.SummaryReset": function(evt) {
            State.Summary$Where = [];
            State.Summary$GroupBy = "";
            $(new SummaryArgumentsDto()).trigger("stateChange");
        },
        "Vehicles.SummaryRefine": function(evt, newGroupBy, columnName, columnValue) {
            State.Summary$Where.push({
                'ColumnName': columnName,
                'Value': columnValue
            });
            State.Summary$GroupBy = newGroupBy;
            $(new SummaryArgumentsDto()).trigger("stateChange");
        },
        "Vehicles.SummaryDetails": function(evt) {
            // helper function
            var value = function(items, columnName, defaultValue) {
                for (var i = 0, l = items.length; i < l; i++) {
                    var item = items[i];
                    if (item.ColumnName == columnName) {
                        return item.Value;
                    }
                }
                return defaultValue;
            };
            // reset example
            State.Example$Model$MakeName = value(State.Summary$Where, "MakeName", null);
            State.Example$Model$ModelFamilyName = value(State.Summary$Where, "ModelFamilyName", null);
            State.Example$Model$SeriesName = value(State.Summary$Where, "SeriesName", null);
            State.Example$Model$SegmentName = value(State.Summary$Where, "SegmentName", null);
            State.Example$Model$BodyTypeName = value(State.Summary$Where, "BodyTypeName", null);
            State.Example$Configuration$ModelYear = value(State.Summary$Where, "ModelYear", 0);
            State.Example$Configuration$EngineName = value(State.Summary$Where, "EngineName", null);
            State.Example$Configuration$DriveTrainName = value(State.Summary$Where, "DriveTrainName", null);
            State.Example$Configuration$TransmissionName = value(State.Summary$Where, "TransmissionName", null);
            State.Example$Configuration$FuelTypeName = value(State.Summary$Where, "FuelTypeName", null);
            State.Example$Configuration$PassengerDoorName = value(State.Summary$Where, "PassengerDoorName", null);
            // fetch vehicles
            $(new QueryArgumentsDto()).trigger("stateChange");
        },
        "Vehicles.VehicleTags": function(evt, theVehicle) {
            $(new VehicleTagsArgumentsDto(theVehicle)).trigger("stateChange");
        },
        "Vehicles.DeleteTag": function(evt, theTag) {
            $(new DeleteTagArgumentsDto(theTag)).trigger("stateChange");
        },
        "Vehicles.BrokerTags": function(evt) {
            $(new BrokerTagsArgumentsDto()).trigger("stateChange");
        },
        "Vehicles.TaggedVehicles": function(evt, theTag) {
            $(new TaggedVehiclesArgumentsDto(theTag)).trigger("stateChange");
        },
        "Vehicles.ApplyAndCreate": function(evt, theTag) {
            var has_tag = false;
            for (var i=0, l = State.Tags.length; i < l; i++) {
                if (theTag == State.Tags[i].Name) {
                    has_tag = true;
                    $(Vehicles).trigger("Vehicles.ApplyTag", State.Tags[i].Handle);
                }
            }
            if (!has_tag) { 
                $(Vehicles).trigger("Vehicles.CreateTag", theTag);
            }
        }
    };

    for (var e in PublicEvents) {
        this.listensTo.push(e);
    }

    $(Vehicles).bind(PublicEvents);

}).apply(Vehicles);

