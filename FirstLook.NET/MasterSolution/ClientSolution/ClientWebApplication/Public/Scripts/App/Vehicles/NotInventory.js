﻿if (typeof(Vehicles) !== "function") {
    var Vehicles = function() {};
}

(function() {
    var CONST = {
        vin_url: "/IMT/SearchStockNumberOrVinAction.go",
        vin_param: "stockNumberOrVin"
    };
    var State = {
        RenderedPagger: false
    };
    var PublicEvents = {
        "Vehicles.BrokerChange": function(evt, handle) {
            $(Vehicles).trigger("Vehicles.ActualityChange", 2); // Non-Inventory
        },
        "Vehicles.QueryLoaded": function(evt, data) {
            clean_table();
            build_table(data.Vehicles);
            if (!State.RenderedPagger && data.TotalRowCount > (data.Vehicles.length + data.Arguments.StartRowIndex)) {  
                State.RenderedPagger = true;
                build_pagger(data.TotalRowCount, data.Vehicles.length, data.Arguments.StartRowIndex);
            }
        },
        "Vehicles.BrokerTagsLoaded": function(evt, data) {
            $(data.Tags).each(function() {
                $(Vehicles).trigger("Vehicles.TaggedVehicles", this.Handle);
            });
        },
        "Vehicles.TaggedVehicles": function(evt, data) {}
    };
    $(this).bind(PublicEvents);

    function clean_table() {
        $("#Vehicles tbody tr").not(".vehicle_template").remove();
    }

    function build_table(vehicles) {
        var table = $("#Vehicles"),
        body = table.children("tbody"),
        template_row = body.children(".vehicle_template"),
        columns = template_row.children();

        function get_access(obj, str) {
            if (str.indexOf(".") === - 1) {
                return obj[str];
            }

            var strs = str.split(".");
            return get_access(obj[strs[0]], strs.slice(1).join("."));
        }

        function build_row(idx, vehicle) {
            var row = $("<tr />").addClass(idx%2===0?"even":"odd");
            columns.each(function(idx) {
                var col = $("<td />").addClass(this.className),
                text = get_access(vehicle, this.className.replace("-", "."));
                if (this.className === "Identification-Vin") {
                    col.append($("<a/>").attr({
                        href: CONST.vin_url + "?" + CONST.vin_param+"="+text
                    }).text(text));
                } else col.text(text);
                row.append(col);
            });

            table.append(row);
        }

        $(vehicles).each(build_row);
    }

    function build_pagger(total, perPage, index) {
        var list = $("#VehiclesPaging"),
        numOfPages = Math.ceil(total/perPage),
        pageToBuild = 0,
        item;
        do  {
            item = $("<li/>").text(pageToBuild);
            pageToBuild++;
            item.bind("click", ((pageToBuild - 1)* perPage), function(evt) {
                $(Vehicles).trigger("Vehicles.StartIndexChange", [evt.data]);
            });
            list.append(item);
        } while (numOfPages > pageToBuild)
    }
}).apply(Vehicles);

if (typeof(Clients) !== "function") {
    var Clients = function() {};
}

(function() {
    var PublicEvents = {
        "Clients.ClientsLoaded": function(evt, data) {
            $(Clients).trigger("Clients.ClientChange", data.Clients[0].Id);
        },
        "Clients.BrokerLoaded": function(evt, data) {
            $(Vehicles).trigger("Vehicles.BrokerChange", data.Broker.Handle);
        }
    };
    $(this).bind(PublicEvents);
}).apply(Clients);

if (typeof(Application) !== "function") {
    var Application = function() {};
}

(function() {
    this.prototype.main = function(obj) {
        var dealerId, buid;

        if (window.location.search.indexOf("buid") !== - 1) {
            buid = window.location.search.match(/buid=([^&]*)/)[1];
            $(Clients).trigger("Vehicles.BrokerChange", buid);
        } else if (window.location.search.indexOf("dealerId") !== - 1) {
            dealerId = window.location.search.match(/dealerId=(\d*(?:[^&]))/)[1];
            $(Clients).trigger("Clients.QueryChange", {
                "Client$Id": dealerId
            });
            $(Clients).trigger("Vehicles.ExcludeMobileChange", true);
        }
    };
}).apply(Application);

$().ready(function() {
    var app = new Application();
    var clients = new Clients();
    var vehicles = new Vehicles();

    vehicles.main();

    app.main();
});

