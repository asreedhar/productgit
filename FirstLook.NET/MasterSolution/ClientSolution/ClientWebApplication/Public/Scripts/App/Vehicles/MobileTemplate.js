if (typeof(Vehicles) !== "function") {
	var Vehicles = function() {};
}

(function() {
	this.raises = this.raises || [];
	this.raises.push("Vehicles.BrokerChange");
	this.raises.push("Vehicles.VinChange");
	this.raises.push("Vehicles.MakeNameChange");
	this.raises.push("Vehicles.ModelFamilyNameChange");
	this.raises.push("Vehicles.SegmentNameChange");
	this.raises.push("Vehicles.ModelYearChange");
	this.raises.push("Vehicles.EngineNameChange");
	this.raises.push("Vehicles.DriveTrainNameChange");
	this.raises.push("Vehicles.TransmissionNameChange");
	this.raises.push("Vehicles.FuelTypeNameChange");
	this.raises.push("Vehicles.PassengerDoorNameChange");
	this.raises.push("Vehicles.CreateTag");
	this.raises.push("Vehicles.UpdateTag");
	this.raises.push("Vehicles.DeleteTag");
	this.raises.push("Vehicles.TaggedVehicles");
	this.raises.push("Vehicles.ApplyTag");
	this.raises.push("Vehicles.RemoveTag");
	this.raises.push("Vehicles.SummaryRefine");
	this.raises.push("Vehicles.SummaryDetails");

	this.listensTo = this.listensTo || [];
	this.listensTo.push("Vehicles.BrokerTagsLoaded");
	this.listensTo.push("Vehicles.StateLoaded");
	this.listensTo.push("Vehicles.SummaryLoaded");
	this.listensTo.push("Vehicles.QueryLoaded");

	var State = {
		DealerTags: [],
		VinTags: []
	};

	var PublicEvents = {
		"Vehicles.Main": function(evt) {
			$(Vehicles).trigger("Vehicles.QueryChange", {});
		},
		"Vehicles.BrokerTagsLoaded": function(evt, tags) {
			State.DealerTags = tags;
			$(Vehicles).trigger("Vehicles._TagsLoaded");
		},
		"Vehicles.VehicleTagsLoaded": function(evt, tags) {
			State.VinTags = tags;
			$(Vehicles).trigger("Vehicles._TagsLoaded");
		},
		"Vehicles.SummaryLoaded": function(evt, summary) {},
		"Vehicles.QueryLoaded": function(evt, vehicles) {
			var card = FirstLook.cards.get("Vehicles");
			card.build(vehicles);
		},
		"Vehicles.TagToCreate": function(evt, inputs) {
			State.VinTags.Tags.push({
				Handle: null,
				Name: inputs.TagName
			});

			var card = FirstLook.cards.get("Tags"),
			data = {
				DealerTags: State.DealerTags,
				VinTags: State.VinTags
			};
			$(Vehicles).trigger("Vehicles.CreateTag", inputs.TagName);
			$(card.views.vin_tags.list).trigger("rebuild", [data]);
			$(card.views.vin_tags.enter).trigger("hide");
			$(card.views.vin_tags.select).trigger("show");
		},
		"Vehicles._TagsLoaded": function(evt) {
			if (!_.isUndefined(State.DealerTags.Tags) && ! _.isUndefined(State.VinTags.Tags)) {
				var data = {
					DealerTags: State.DealerTags,
					VinTags: State.VinTags
				};
				$(Vehicles).trigger("Vehicles.TagsLoaded", [data]);
			}
		},
		"Vehicles.TagToApply": function(evt, inputs) {
			var value = inputs.TagId,
			card = FirstLook.cards.get("Tags");
			if (value === "CreateNew") {
				$(card.views.vin_tags.enter).trigger("show");
				$(card.views.vin_tags.select).trigger("hide");
			} else {
				$(Vehicles).trigger("Vehicles.ApplyTag", inputs.TagId);

				State.VinTags.Tags.push(_.detect(State.DealerTags.Tags, function(v) {
					return v.Handle === value;
				}));

				var data = {
					DealerTags: State.DealerTags,
					VinTags: State.VinTags
				};

				$(card.views.vin_tags.list).trigger("rebuild", [data]);
			}
		}
	};
	$(Vehicles).bind(PublicEvents);

	FirstLook.components.add({
		type: "List",
		pattern: {
			VinTags: {
				Tags: [{
					Handle: _.isString,
					Name: _.isString
				}]
			}
		},
		behaviors: {
			rebuildable: {}
		},
		item_accessor: "VinTags.Tags",
		text_accessor: "Name",
		data_accessor: "Handle",
		class_name: "tags"
	},
	"Vehicles.VinTags");

	FirstLook.components.add({
		type: "Fieldset",
		pattern: _,
		behaviors: {
			submitable: {
				event: "Vehicles.TagToCreate",
				scope: Vehicles
			},
			hideable: {
				visible_on_render: false,
				animate: false
			},
			focusable: {},
			rebuildable: {}
		},
		class_name: "new_tag",
		legend_accessor: "legend",
		input_accessor: "inputs",
		name_accessor: "name",
		button_accessor: "buttons",
		label_accessor: "label",
		value_accessor: "value",
		type_accessor: "type",
		static_data: {
			legend: "Tags",
			inputs: [{
				name: "TagName",
				label: "Tag Name",
				value: ""
			}],
			buttons: ["Add"]
		}
	},
	"Vehicles.NewTag");

	FirstLook.components.add({
		type: "Fieldset",
		pattern: {
			DealerTags: {
				Tags: [{
					Name: _.isString,
					Handle: _.isString
				}]
			}
		},
		behaviors: {
			submitable: {
				event: "Vehicles.TagToApply",
				scope: Vehicles,
				submit_on_change: true
			},
			hideable: {
				visible_on_render: true,
				animate: false
			},
			rebuildable: {}
		},
		class_name: "existing_tags",
		legend_accessor: "legend",
		input_accessor: "inputs",
		name_accessor: "name",
		button_accessor: "buttons",
		label_accessor: "label",
		value_accessor: "data",
		type_accessor: "type",
		static_data: {
			legend: "Tags",
			inputs: [{
				name: "TagId",
				label: "Tag Name",
			}],
			buttons: ["Tag"]
		},
		filter: function(data) {
			data.inputs[0].options = _.reduce(data.DealerTags.Tags, function(m, v, k) {
				m.push({
					name: v.Name,
					value: v.Handle
				});
				return m;
			},
			[{
				name: "",
				value: ""
			},
			{
				name: "Create New",
				value: "CreateNew"
			}]);
			return data;
		}
	},
	"Vehicles.DealerTags");

	var tag_card = {
		title: "Tags",
		pattern: _,
		view: {
			vin_tags: {
				order: 0,
				pattern: {
					DealerTags: {
						Tags: _.isArray
					}
				},
				select: FirstLook.components.get("Vehicles.DealerTags"),
				enter: FirstLook.components.get("Vehicles.NewTag"),
				list: FirstLook.components.get("Vehicles.VinTags")
			},
			error: {
				pattern: {
					errorType: _,
					errorResponse: _
				},
				ErrorMessage: FirstLook.components.get("Application.ServerError")
			}
		}
	};
	FirstLook.cards.add(tag_card, "Tags");

	var vehicles_card = {
		title: "Vehicles",
		pattern: {
			broker_id: _.isString,
			vin: _.isEmpty,
			vid: _.isEmpty
		},
		view: {
			select: {
				order: 0,
				pattern: {
					Vehicles: function(v) {
						return _.isArray(v) && v.length > 0;
					}
				},
				VehicleList: {
					type: "GroupedList",
					pattern: {
						Vehicles: [{
							Identification: {
								Vin: _.isString
							},
							Mileage: _.isNumber,
							Description: {
								ModelYear: _.isNumber,
								ModelFamilyName: _.isString
							}
						}]
					},
					behaviors: {
						selectable: {
							pattern: {
								Vehicles: [{
									Id: _.isNumber,
									Identification: {
										Vin: _.isVin
									}
								}]
							},
							binds: {
								"item_selected": {
									type: "Vehicles.VehicleSelected",
									scope: Vehicles
								}
							},
							data_accessor: ["Id", "Identification.Vin"]
						},
						pageable: {
							has_next_accessor: _,
							page_size_accessor: "Arguments.MaximumRows",
							total_items_accessor: "TotalRowCount",
							start_index_accessor: "Arguments.StartRowIndex",
							pattern: {
								Arguments: {
									StartRowIndex: _.isNumber,
									MaximumRows: _.isNumber
								},
								TotalRowCount: _.isNumber
							},
							binds: {
								"start_index_change": {
									type: "Vehicles.StartIndexChange",
									scope: Vehicles
								}
							}
						},
						groupable: {
							group_accessor: "Vehicles",
							group_by_accessor: "Description.ModelYear",
							pattern: {
								Vehicles: [{
									Description: {
										ModelYear: _.isNumber
									}
								}]
							}
						},
						infinate_scrollable: _
					},
					class_name: "select_list",
					item_accessor: "Vehicles",
					text_accessor: ["Description.MakeName", "Description.ModelFamilyName", "Description.SeriesName", "Mileage_F", "Identification.Vin_F"],
					data_accessor: ["Id", "Identification.Vin"],
					raises: ["item_selected", "next"],
					filter: function(data) {
						_.each(data.Vehicles, function(group) {
							_.each(group, function(veh) {
								veh.Mileage_F = veh.Mileage === 0 ? "": " w/ " + _.roundToK(veh.Mileage);
								veh.Identification.Vin_F = _.shortVin(veh.Identification.Vin);
							});
						});
						return data;
					}
				}
			},
			empty: {
				order: 1,
				pattern: {
					Vehicles: function(v) {
						return _.isArray(v) && v.length === 0;
					}
				},
				EmptyView: {
					type: "Static",
					pattern: _,
					template: function() {
						return "<p class='list_message'>Emtpy List</p>";
					}
				}
			},
			error: {
				pattern: _,
				ErrorView: {
					type: "Static",
					pattern: _,
					template: function() {
						return "<p>Error Loading Vehicles</p>";
					}
				}
			}
		}
	};

	FirstLook.cards.add(vehicles_card, "Vehicles");
}).apply(Vehicles);

