﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using ICommandFactory=FirstLook.Client.DomainModel.Vehicles.Commands.ICommandFactory;

namespace FirstLook.Client.WebApplication.Services
{
    /// <summary>
    /// Web service for vehicle-related commands.
    /// </summary>
    [WebService(Namespace = "http://api.firstlook.biz/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    [ScriptService]
    public class Vehicle : WebService
    {
        #region Vehicle Methods

        /// <summary>
        /// Query for vehicles that match the given example.
        /// </summary>
        /// <param name="arguments">Broker information, example vehicle and pagination arguments.</param>
        /// <returns>Paged list of vehicles.</returns>
        [WebMethod]
        public QueryResultsDto Query(QueryArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<QueryResultsDto, IdentityContextDto<QueryArgumentsDto>> command = factory.CreateQueryCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }

        /// <summary>
        /// Summarize a group of vehicles.
        /// </summary>
        /// <param name="arguments">Broker and summary criteria.</param>
        /// <returns>Summary results.</returns>
        [WebMethod]
        public SummaryResultsDto Summarize(SummaryArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<SummaryResultsDto, IdentityContextDto<SummaryArgumentsDto>> command = factory.CreateSummarizeCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }        

        [WebMethod]
        public IdentifyVehicleResultsDto Identify(IdentifyVehicleArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<IdentifyVehicleResultsDto, IdentityContextDto<IdentifyVehicleArgumentsDto>> command = factory.CreateIdentifyVehicleCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }

        #endregion

        #region Tag Methods

        /// <summary>
        /// Get the broker's tags.
        /// </summary>
        /// <param name="arguments">Broker handle.</param>
        /// <returns>Broker's tags.</returns>
        [WebMethod]
        public BrokerTagsResultsDto Tags(BrokerTagsArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<BrokerTagsResultsDto, IdentityContextDto<BrokerTagsArgumentsDto>> command = factory.CreateBrokerTagsCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }

        /// <summary>
        /// Get the broker's vehicles with a specific tag.
        /// </summary>
        /// <param name="arguments">Broker and tag details.</param>
        /// <returns>Tagged vehicles.</returns>
        [WebMethod]
        public TaggedVehiclesResultsDto TaggedVehicles(TaggedVehiclesArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<TaggedVehiclesResultsDto, IdentityContextDto<TaggedVehiclesArgumentsDto>> command = factory.CreateTaggedVehiclesCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }

        [WebMethod]
        public VehicleTagsResultsDto VehicleTags(VehicleTagsArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<VehicleTagsResultsDto, IdentityContextDto<VehicleTagsArgumentsDto>> command = factory.CreateVehicleTagsCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }

        /// <summary>
        /// Create a tag for a client.
        /// </summary>
        /// <param name="arguments">Tag information and a broker identifier.</param>
        /// <returns>Identification details of the new tag.</returns>
        [WebMethod]
        public CreateTagResultsDto CreateTag(CreateTagArgumentsDto arguments)
        {            
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<CreateTagResultsDto, IdentityContextDto<CreateTagArgumentsDto>> command = factory.CreateCreateTagCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }

        /// <summary>
        /// Update a tag for a client.
        /// </summary>
        /// <param name="arguments">Tag information and a broker identifier.</param>
        /// <returns>Identification details of the new tag.</returns>
        [WebMethod]
        public UpdateTagResultsDto UpdateTag(UpdateTagArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<UpdateTagResultsDto, IdentityContextDto<UpdateTagArgumentsDto>> command = factory.CreateUpdateTagCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }
        
        /// <summary>
        /// Delete a client's tag. Will remove the tag from all vehicles tagged with it.
        /// </summary>
        /// <param name="arguments">Client and tag details.</param>
        /// <returns>Arguments used.</returns>
        [WebMethod]
        public DeleteTagResultsDto DeleteTag(DeleteTagArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<DeleteTagResultsDto, IdentityContextDto<DeleteTagArgumentsDto>> command = factory.CreateDeleteTagCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }

        /// <summary>
        /// Apply a client's tag to a vehicle.
        /// </summary>
        /// <param name="arguments">Client, vehicle and tag details.</param>
        /// <returns>Arguments used.</returns>
        [WebMethod]
        public ApplyTagResultsDto ApplyTag(ApplyTagArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<ApplyTagResultsDto, IdentityContextDto<ApplyTagArgumentsDto>> command = factory.CreateApplyTagCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }        
        
        /// <summary>
        /// Remove a client's tag from a vehicle.
        /// </summary>
        /// <param name="arguments">Client, vehicle and tag details.</param>
        /// <returns>Arguments used.</returns>
        [WebMethod]
        public RemoveTagResultsDto RemoveTag(RemoveTagArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<RemoveTagResultsDto, IdentityContextDto<RemoveTagArgumentsDto>> command = factory.CreateRemoveTagCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }        

        #endregion

        #region Utility

        /// <summary>
        /// Helper to add identity information to a command's arguments.
        /// </summary>
        /// <typeparam name="T">Type of the command arguments.</typeparam>
        /// <param name="arguments">Command arguments to be wrapped into a identity context object.</param>
        /// <returns>Command arguments wrapped inside an identity context object.</returns>
        private static IdentityContextDto<T> Parameterize<T>(T arguments)
        {
            return new IdentityContextDto<T>
            {
                Identity = new IdentityDto
                {
                    Name = HttpContext.Current.User.Identity.Name,
                    AuthorityName = HttpContext.Current.User.Identity.AuthenticationType
                },
                Arguments = arguments
            };
        }

        #endregion

        /// <summary>
        /// Retrieve the registered CommandExecutor as defined in Global.asax.cs.  This command executor will wrap the command in a 
        /// try catch block.  If an exception is thrown the listener(s) associated with the executor is invoked.  In this instance
        /// the listener is FaultWebExceptionListener, it will save the exception information in the fault database.
        /// </summary>
        /// <typeparam name="TResult">The type associated with the result of executing the passed in command.</typeparam>
        /// <typeparam name="TParameter">The type associated with the parameters to pass in on the command.</typeparam>
        /// <param name="command">The command to execute.</param>
        /// <param name="args">The parameters to pass in on the command.</param>
        /// <returns>The respective results dto depending on the command invoked.</returns>
        private static TResult ExecuteCommand<TResult, TParameter>(ICommand<TResult, TParameter> command, TParameter args)
        {
            ICommandExecutor executor = RegistryFactory.GetResolver().Resolve<ICommandExecutor>();
            return executor.Execute(command, args).Result;
    }
}
}
