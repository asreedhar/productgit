﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using ICommandFactory = FirstLook.Client.DomainModel.Clients.Commands.ICommandFactory;

namespace FirstLook.Client.WebApplication.Services
{
    /// <summary>
    /// Web service for client-related commands.
    /// </summary>
    [WebService(Namespace = "http://api.firstlook.biz/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    [ScriptService]
    public class Client : WebService
    {
        /// <summary>
        /// Query for clients accessible to the user that match the user-provided example.
        /// </summary>
        /// <param name="arguments">Username and example client.</param>
        /// <returns>List of accessible clients that match the example.</returns>
        [WebMethod]
        public QueryClientByExampleResultsDto QueryByExample(QueryClientByExampleArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<QueryClientByExampleResultsDto, IdentityContextDto<QueryClientByExampleArgumentsDto>> command = factory.CreateQueryClientByExampleCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="arguments"></param>
        /// <returns></returns>
        [WebMethod]
        public QueryClientResultsDto Query(QueryClientArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<QueryClientResultsDto, IdentityContextDto<QueryClientArgumentsDto>> command = factory.CreateQueryClientCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }

        /// <summary>
        /// Fetch the broker with the given handle.
        /// </summary>
        /// <param name="arguments">Broker handle.</param>
        /// <returns>Broker.</returns>
        [WebMethod]
        public BrokerForIdResultsDto BrokerForHandle(BrokerForIdArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<BrokerForIdResultsDto, IdentityContextDto<BrokerForIdArgumentsDto>> command = factory.CreateBrokerForHandleCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }

        /// <summary>
        /// Fetch the broker tied to the given client. Create the broker if it does not already exist.
        /// </summary>
        /// <param name="arguments">Client.</param>
        /// <returns>Broker.</returns>
        [WebMethod]
        public BrokerForClientResultsDto BrokerForClient(BrokerForClientArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<BrokerForClientResultsDto, IdentityContextDto<BrokerForClientArgumentsDto>> command = factory.CreateBrokerForClientCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }

        /// <summary>
        /// Fetch a broker's users.
        /// </summary>
        /// <param name="arguments">Broker.</param>
        /// <returns>Users.</returns>
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public UsersForBrokerResultsDto UsersForBroker(UsersForBrokerArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<UsersForBrokerResultsDto, IdentityContextDto<UsersForBrokerArgumentsDto>> command = factory.CreateUsersForBrokerCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }

        #region GET Methods

        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public QueryClientByExampleResultsDto GetQueryByExample(QueryClientByExampleArgumentsDto arguments)
        {
            AppendCachingHeadersResponse();
            return QueryByExample(arguments);
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public QueryClientResultsDto GetQuery(QueryClientArgumentsDto arguments)
        {
            AppendCachingHeadersResponse();
            return Query(arguments);
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public BrokerForIdResultsDto GetBrokerForHandle(BrokerForIdArgumentsDto arguments)
        {
            AppendCachingHeadersResponse();
            return BrokerForHandle(arguments);
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public BrokerForClientResultsDto GetBrokerForClient(BrokerForClientArgumentsDto arguments)
        {
            AppendCachingHeadersResponse();          
            return BrokerForClient(arguments);            
        }

        #endregion

        private static IdentityContextDto<T> Parameterize<T>(T arguments)
        {
            return new IdentityContextDto<T>
            {
                Identity = new IdentityDto
                {
                    Name = HttpContext.Current.User.Identity.Name,
                    AuthorityName = HttpContext.Current.User.Identity.AuthenticationType
                },
                Arguments = arguments
            };
        }

        private const double DaysToCache = 7;
        private void AppendCachingHeadersResponse()
        {
            DateTime dateTimeToExpire = DateTime.Now.AddDays(DaysToCache);
            TimeSpan deltaDateTimeToExpire = TimeSpan.FromDays(DaysToCache);

            Context.Response.Cache.SetCacheability(HttpCacheability.Public);
            Context.Response.Cache.SetExpires(dateTimeToExpire);
            Context.Response.Cache.SetSlidingExpiration(true);           
            Context.Response.Cache.SetMaxAge(deltaDateTimeToExpire);
            Context.Response.Cache.SetProxyMaxAge(deltaDateTimeToExpire);
            Context.Response.Cache.SetAllowResponseInBrowserHistory(true);
        }


        /// <summary>
        /// Retrieve the registered CommandExecutor as defined in Global.asax.cs.  This command executor will wrap the command in a 
        /// try catch block.  If an exception is thrown the listener(s) associated with the executor is invoked.  In this instance
        /// the listener is FaultWebExceptionListener, it will save the exception information in the fault database.
        /// </summary>
        /// <typeparam name="TResult">The type associated with the result of executing the passed in command.</typeparam>
        /// <typeparam name="TParameter">The type associated with the parameters to pass in on the command.</typeparam>
        /// <param name="command">The command to execute.</param>
        /// <param name="args">The parameters to pass in on the command.</param>
        /// <returns>The respective results dto depending on the command invoked.</returns>
        private static TResult ExecuteCommand<TResult, TParameter>(ICommand<TResult, TParameter> command, TParameter args)
        {
            ICommandExecutor executor = RegistryFactory.GetResolver().Resolve<ICommandExecutor>();
            return executor.Execute(command, args).Result;
    }
}
}
