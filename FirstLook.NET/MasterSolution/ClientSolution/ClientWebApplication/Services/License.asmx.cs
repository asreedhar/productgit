﻿using System.ComponentModel;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using ICommandFactory = FirstLook.Client.DomainModel.Licenses.Commands.ICommandFactory;

namespace FirstLook.Client.WebApplication.Services
{
    /// <summary>
    /// Web service for license-related commands.
    /// </summary>
    [WebService(Namespace = "http://api.firstlook.biz/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    [ScriptService]
    public class License : WebService
    {
        #region Administration

        /// <summary>
        /// Get all of a client's licenses.
        /// </summary>
        /// <param name="arguments">Client to retrieve licenses for.</param>
        /// <returns>The client's licenses.</returns>
        [WebMethod]
        public LicensesResultsDto GetLicenses(LicensesArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<LicensesResultsDto, IdentityContextDto<LicensesArgumentsDto>> command = factory.CreateLicensesCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }

        /// <summary>
        /// Get the details of a specific license.
        /// </summary>
        /// <param name="arguments">License to retrieve.</param>
        /// <returns>License.</returns>
        [WebMethod]
        public LicenseResultsDto GetLicense(LicenseArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<LicenseResultsDto, IdentityContextDto<LicenseArgumentsDto>> command = factory.CreateLicenseCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }

        /// <summary>
        /// Save a license.
        /// </summary>
        /// <param name="arguments">License to save.</param>
        /// <returns>Results of saving the license.</returns>
        [WebMethod]
        public SaveLicenseResultsDto SaveLicense(SaveLicenseArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<SaveLicenseResultsDto, IdentityContextDto<SaveLicenseArgumentsDto>> command = factory.CreateSaveLicenseCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }

        /// <summary>
        /// Change the license model of a license.
        /// </summary>
        /// <param name="arguments">License to change.</param>
        /// <returns>Results of changing the license model of a license.</returns>
        [WebMethod]
        public ChangeLicenseModelResultsDto ChangeLicenseModel(ChangeLicenseModelArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<ChangeLicenseModelResultsDto, IdentityContextDto<ChangeLicenseModelArgumentsDto>> command = factory.CreateChangeLicenseModelCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }

        /// <summary>
        /// Revoke a license.
        /// </summary>
        /// <param name="arguments">License to revoke.</param>
        /// <returns>Results of revoking the license.</returns>
        [WebMethod]
        public RevokeLicenseResultsDto RevokeLicense(RevokeLicenseArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<RevokeLicenseResultsDto, IdentityContextDto<RevokeLicenseArgumentsDto>> command = factory.CreateRevokeLicenseCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }

        /// <summary>
        /// Get all of a license's leases.
        /// </summary>
        /// <param name="arguments">License and broker to get leases for.</param>
        /// <returns>Leases tied to the license.</returns>
        [WebMethod]
        public LeasesResultsDto GetLeases(LeasesArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<LeasesResultsDto, IdentityContextDto<LeasesArgumentsDto>> command = factory.CreateLeasesCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }

        /// <summary>
        /// Get the details of a specific lease.
        /// </summary>
        /// <param name="arguments">Lease to retrieve.</param>
        /// <returns>Lease.</returns>
        [WebMethod]
        public LeaseResultsDto GetLease(LeaseArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<LeaseResultsDto, IdentityContextDto<LeaseArgumentsDto>> command = factory.CreateLeaseCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }

        /// <summary>
        /// Create a lease.
        /// </summary>
        /// <param name="arguments">Lease to create.</param>
        /// <returns>Results of creating the lease.</returns>
        [WebMethod]
        public CreateLeaseResultsDto CreateLease(CreateLeaseArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<CreateLeaseResultsDto, IdentityContextDto<CreateLeaseArgumentsDto>> command = factory.CreateCreateLeaseCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }

        /// <summary>
        /// Revoke a lease.
        /// </summary>
        /// <param name="arguments">Lease to revoke.</param>
        /// <returns>Results of revoking the lease.</returns>
        [WebMethod]
        public RevokeLeaseResultsDto RevokeLease(RevokeLeaseArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<RevokeLeaseResultsDto, IdentityContextDto<RevokeLeaseArgumentsDto>> command = factory.CreateRevokeLeaseCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }

        #endregion

        #region Authorization

        /// <summary>
        /// Get authorization tokens available to a user.
        /// </summary>
        /// <param name="arguments">User to retrieve tokens for.</param>
        /// <returns>Tokens available to the user.</returns>
        [WebMethod]
        public TokensResultsDto GetTokens(TokensArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<TokensResultsDto, IdentityContextDto<TokensArgumentsDto>> command = factory.CreateTokensCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }

        /// <summary>
        /// Revoke all of a user's authorization tokens.
        /// </summary>
        /// <param name="arguments">User to revoke tokens of.</param>
        /// <returns>Not much of anything.</returns>
        [WebMethod]
        public RevokeTokensResultsDto RevokeTokens(RevokeTokensArgumentsDto arguments)
        {
            ICommandFactory factory = RegistryFactory.GetResolver().Resolve<ICommandFactory>();

            ICommand<RevokeTokensResultsDto, IdentityContextDto<RevokeTokensArgumentsDto>> command = factory.CreateRevokeTokensCommand();

            return ExecuteCommand(command, Parameterize(arguments));
        }

        #endregion                   

        #region Helpers

        /// <summary>
        /// Add user identity information to the given command arguments.
        /// </summary>
        /// <typeparam name="T">Type of command arguments.</typeparam>
        /// <param name="arguments">Command arguments.</param>
        /// <returns>Identity information-wrapped command arguments.</returns>
        private static IdentityContextDto<T> Parameterize<T>(T arguments)
        {
            return new IdentityContextDto<T>
            {
                Identity = new IdentityDto
                {
                    Name = HttpContext.Current.User.Identity.Name,
                    AuthorityName = HttpContext.Current.User.Identity.AuthenticationType
                },
                Arguments = arguments
            };
        }        

        /// <summary>
        /// Retrieve the registered CommandExecutor as defined in Global.asax.cs.  This command executor will wrap the command in a 
        /// try catch block.  If an exception is thrown the listener(s) associated with the executor is invoked.  In this instance
        /// the listener is FaultWebExceptionListener, it will save the exception information in the fault database.
        /// </summary>
        /// <typeparam name="TResult">The type associated with the result of executing the passed in command.</typeparam>
        /// <typeparam name="TParameter">The type associated with the parameters to pass in on the command.</typeparam>
        /// <param name="command">The command to execute.</param>
        /// <param name="args">The parameters to pass in on the command.</param>
        /// <returns>The respective results dto depending on the command invoked.</returns>
        private static TResult ExecuteCommand<TResult, TParameter>(ICommand<TResult, TParameter> command, TParameter args)
        {
            ICommandExecutor executor = RegistryFactory.GetResolver().Resolve<ICommandExecutor>();
            return executor.Execute(command, args).Result;
        }

        #endregion
    }
}
