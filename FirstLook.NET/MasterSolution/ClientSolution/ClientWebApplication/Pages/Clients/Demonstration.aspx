﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Demonstration.aspx.cs" Inherits="FirstLook.Client.WebApplication.Pages.Clients.Demonstration" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Client - Demonstration</title>
    <script src="../../Public/Scripts/Lib/LABjs-1.0.2rc1/LAB.js" type="text/javascript" charset="utf-8"></script>
</head>
<body>
    <form id="ClientForm" runat="server">
    
    <div id="nav_header">
    </div>
    <div id="main_content">
        <div id="header">
            <h1>Client Demonstration</h1>
        </div>
        <div id="body">
            <fieldset id="example">
                <legend>Example</legend>
                <table border="1">
                    <tbody>
                        <tr>
                            <th rowspan="3">Client</th>
                            <th>Type</th>
                            <td>
                                <select id="authority_name" name="authority_name">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="client_name">Name</label></th>
                            <td><input type="text" id="client_name" name="client_name" maxlength="100" /></td>
                        </tr>
                        <tr>
                            <th><label for="client_organization_name">Organization</label></th>
                            <td><input type="text" id="client_organization_name" name="client_organization_name" /></td>
                        </tr>
                        <tr>
                            <th rowspan="5">Address</th>
                            <th><label for="address_line_1">Line 1</label></th>
                            <td>
                                <input type="text" id="address_line_1" name="address_line_1"/>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="address_line_2">Line 2</label></th>
                            <td>
                                <input type="text" id="address_line_2" name="address_line_2"/>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="address_city">City</label></th>
                            <td>
                                <input type="text" id="address_city" name="address_city"/>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="address_state">State</label></th>
                            <td>
                                <input type="text" id="address_state" name="address_state"/>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="address_zip">ZIP</label></th>
                            <td>
                                <input type="text" id="address_zip_code" name="address_zip_code"/>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>
            <fieldset id="results">
                <legend>Results</legend>
                <table border="1">
                    <thead>
                        <tr><th colspan="2">Client</th><th colspan="6">Address</th></tr>
                        <tr><th>Name</th><th>Organization</th><th>Line 1</th><th>Line 2</th><th>City</th><th>State</th><th>ZIP</th><th></th></tr>
                    </thead>
                    <tbody id="results_body">
                        
                    </tbody>
                </table>
            </fieldset>
        </div>
    </div>

    <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/Lib/jquery/jquery-ui-1.8.2.custom/js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/App/Clients/Application.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/App/Clients/TestTemplates.js"></script>

    <script type="text/javascript" charset="utf-8">
        var _Clients = new Clients();
        _Clients.main();

        if (typeof JSON === "undefined") {
            $LAB.script("../../Public/Scripts/Lib/json/json2.min.js");
        }
    </script>
    
    </form>
</body>
</html>
