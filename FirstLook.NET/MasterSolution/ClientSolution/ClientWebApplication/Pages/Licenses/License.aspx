﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="License.aspx.cs" Inherits="FirstLook.Client.WebApplication.Pages.Licenses.License" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Licenses</title>

    <script src="../../Public/Scripts/Lib/LABjs-1.0.2rc1/LAB.js" type="text/javascript" charset="utf-8"></script>    
</head>
<body>
    <form id="LicenseForm" runat="server">
    <div id="nav_header">
    </div>
    <div id="main_content">
        <div id="header">
            <h1>License Administration</h1>            
        </div>                        

        <div id="body-admin" style="width:60%;margin:0 auto;">

            <fieldset id="administration">
                <legend id="brokerName" style="width:150px;text-align:center;font-weight:bolder;">No dealer selected</legend>

                <fieldset id="licenses" style="background-color:#aab;">
                    <legend style="background-color:White;border:1px solid black;">Licenses</legend>
                    <table border="1" width="100%" style="background-color:#fff;">
                        <thead style="background-color:#eee;color:#555;">
                            <tr>
                                <th>Product</th>
                                <th>License Model</th>
                                <th>Device Type</th>
                                <th>Uses</th>
                                <th colspan="3">Actions</th>                            
                            </tr>
                        </thead>
                        <tbody id="licenses_body">                        
                            <tr>                                
                                <td><select name="lproduct" id="lproduct">
                                    <option value="0"></option>
                                    <option value="BlackBook">BlackBook</option>
                                    <option value="Edmunds">Edmunds</option>
                                    <option value="Galves">Galves</option>
                                    <option value="Kelley Blue Book">Kelley Blue Book</option>
                                    <option value="Manheim">Manheim</option>
                                    <option value="NAAA">NAAA</option>
                                    <option value="NADA">NADA</option>
                                </select></td>
                                <td><select id="lmodel" name="lmodel">
                                    <option value="0"></option>
                                    <option value="1">Site</option>
                                    <option value="2">Floating</option>
                                    <option value="3">Named</option>
                                </select></td>
                                <td><select id="ldeviceType" name="ldeviceType">
                                    <option value="0"></option>
                                    <option value="1">Desktop</option>
                                    <option value="2">Mobile</option>                                    
                                </select></td>
                                
                                <td></td>                            
                                <td colspan="3"><center><a href="#createLicense">Create</a></center></td>
                            </tr>
                        </tbody>
                    </table>

                    <div style="display:none;color:Gray; width:100%; text-align:left;">
                        <ul>
                            <li><b>CREATE</b>: Product, License Model and Device Type are all required fields.</li>
                            <li><b>SELECT</b>: View this license's parameters in the table below.</li>                     
                            <li><b>SAVE</b>: Change a license's License Model. Product and Device Type cannot change.</li>
                            <li><b>REVOKE</b>: Delete this license and all its leases.</li>                            
                            <li><i>Note: If changing a license to a Named license model, ensure that there are only as many currently leased users as seats
                            specified in the optional Seat parameter. If not, revoke leases as necessary.</i></li>
                        </ul>
                    </div>
                </fieldset>

                <br />

                <fieldset id="licenseDetails" style="background-color:#aab;">
                    <legend style="background-color:White;border:1px solid black;">License Details</legend>
                    <table border="1" width="100%" style="background-color:#fff;">
                        <thead style="background-color:#eee;color:#555;">
                            <tr>
                                <th>Product</th>
                                <th>License Model</th>
                                <th>Device Type</th>
                                <th>Expiration Date<br />yyyy-mm-dd</th>
                                <th>Seat Limit</th>
                                <th>Usage Limit</th>                            
                                <th colspan="2">Actions</th>                                
                            </tr>
                        </thead>
                        <tbody id="licenseDetails_body">
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>                                
                                <td colspan="2">&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>

                    <div style="display:none;color:Gray; width:100%; text-align:left;">
                        <ul>
                            
                            <li><b>SELECT</b>: View all of this license's leases in the table below.</li>
                            <li><b>SAVE</b>: Set this license's optional parameters.</li>
                            <li><i>Note: Expiration Date, Seat Limit and Usage Limit are all optional parameters.</i></li>
                        </ul>
                    </div>
                </fieldset>

                <br />

                <fieldset id="leases" style="background-color:#aab;">
                    <legend style="background-color:White;border:1px solid black;">Leases</legend>
                    <table border="1" width="100%" style="background-color:#fff;">
                        <thead style="background-color:#eee;color:#555;">
                            <tr>
                                <th>Name</th>                                
                                <th colspan="2">Actions</th>
                            </tr>
                        </thead>
                        <tbody id="leases_body">
                            <tr>
                                <td></td>                                                                                 
                                <td colspan="2">&nbsp;</td>                                
                            </tr>
                        </tbody>                                                                                    
                    </table>
                    <div style="display:none;color:Gray; width:100%; text-align:left;">
                        <ul>
                            <li><b>CREATE</b>: Grant a user a lease. ONLY FOR NAMED LICENSES. Leases for Floating and Site licenses are assigned automatically as accessed.</li>
                            <li><b>SELECT</b>: View details of this lease below.</li>
                            <li><b>REVOKE</b>: Delete this lease.</li>
                        </ul>
                    </div>
                </fieldset>

                <br />

                <fieldset id="leaseDetails" style="background-color:#aab;">
                    <legend style="background-color:White;border:1px solid black;">Lease Details</legend>
                    <table border="1" width="100%" style="background-color:#fff;">
                        <thead style="background-color:#eee;color:#555;">
                            <tr>
                                <th>Name</th>
                                <th>Assigned On</th>
                                <th>Assigned By</th>
                                <th>Last Accessed</th>
                                <th>Used</th>                                
                            </tr>
                        </thead>
                        <tbody id="leaseDetails_body">
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>                            
                                <td></td>
                                <td>&nbsp;</td>                                
                            </tr>
                        </tbody>
                    </table>
                </fieldset>
            
            </fieldset>

        </div>            
    </div>

    <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/Lib/jquery/jquery-ui-1.8.2.custom/js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/App/Licenses/Application.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/App/Licenses/TestTemplate.js"></script>

    <script type="text/javascript" charset="utf-8">
        var _Licenses = new Licenses();        
        var m = window.location.search.match(/buid=(.*)&?/);
        _Licenses.main({ buid: m ? m[1] : undefined });        

        if (typeof JSON === "undefined") {
            $LAB.script("../../Public/Scripts/Lib/json/json2.min.js");
        }
    </script>

    </form>
</body>
</html>
