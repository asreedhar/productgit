﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Vehicles/Report/Report.Master" AutoEventWireup="true" CodeBehind="NotInventory.aspx.cs" Inherits="FirstLook.Client.WebApplication.Pages.Vehicles.Report.NotInventory" Theme="Basic" %>
<%@ OutputCache Duration="604800" VaryByParam="*" %>

<asp:Content ID="Header_Content" ContentPlaceHolderID="Head_ContentPlaceHolder" runat="server">
    <h1><asp:Literal ID="Title_Literal" runat="server"></asp:Literal></h1>
</asp:Content>
<asp:Content ID="Main_Content" ContentPlaceHolderID="Main_ContentPlaceHolder" runat="server">
    <table id="Vehicles">
        <thead>
            <tr>
                <th class="Description-ModelYear">Year</th>
                <th class="Description-MakeName">Make</th>
                <th class="Description-ModelFamilyName">Model</th>
                <th class="Description-SeriesName">Series</th>
                <th class="Identification-Vin">VIN</th>
            </tr>
        </thead>
        <tbody class="grouped_by_Year">
            <tr class="vehicle_template">
                <td class="Description-ModelYear"></td>
                <td class="Description-MakeName"></td>
                <td class="Description-ModelFamilyName"></td>
                <td class="Description-SeriesName"></td>
                <td class="Identification-Vin"></td>
            </tr>
        </tbody>
    </table>
    <ul id="VehiclesPaging"></ul>
</asp:Content>
<asp:Content ID="Footer_Content" ContentPlaceHolderID="Footer_ContentPlaceHolder" runat="server">
    <script src="../../../Public/Scripts/App/Vehicles/NotInventory.js" type="text/javascript"></script>
</asp:Content>
