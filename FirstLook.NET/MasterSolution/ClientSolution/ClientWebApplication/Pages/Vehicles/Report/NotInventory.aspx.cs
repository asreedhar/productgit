﻿using System;

namespace FirstLook.Client.WebApplication.Pages.Vehicles.Report
{
    public partial class NotInventory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Title = Title_Literal.Text = "Scanned Vins";            
        }
    }
}
