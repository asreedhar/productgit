﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Demonstration.aspx.cs" Inherits="FirstLook.Client.WebApplication.Pages.Vehicles.Demonstration" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Vehicles - Demonstration</title>
    <script src="../../Public/Scripts/Lib/LABjs-1.0.2rc1/LAB.js" type="text/javascript" charset="utf-8"></script>
    <style media="screen" type="text/css">
        #body { width: 950px; }
        #menu ul { padding-left: 1ex; list-style-type: none; list-style-position: inside; }
        #menu li {  float: left; width: 20ex; padding: 1ex; border: solid 1px grey; margin-left: 2px; text-align: center; }
        fieldset, table { clear: both; }
        #tags input { width: 20ex; }
        #tags table { width: 100% }
        #tags table input { width: 97%; margin-left: 1%; }
        #tags table td { text-align: center; }
        #summary div { float: left; }
        #summary h3 { padding-left: 1ex; }
        #summary ul { padding-left: 1ex; list-style-type: none; list-style-position: inside; }
        #summary li { width: 25ex; padding: 1ex; border: solid 1px grey; margin-left: 2px; text-align: left; }
        .selected { background-color: Lime; }
        #summary .selected { background-color: RoyalBlue; }
        .hidden { display: none; }
        #example table { width: 100%; }
        #example input { width: 95%; margin-left: 2%; }
    </style>
</head>
<body>
    <form id="VehiclesForm" runat="server">
    
    <div id="nav_header">
        
    </div>
    
    <div id="main_content">
        <div id="header">
            <h1>Vehicle - Development</h1>
        </div>
        <div id="body">
        
            <fieldset id="subject">
                <legend>Broker / Vehicle</legend>
                <table border="1">
                    <tbody>
                        <tr>
                            <th><label for="broker">Broker</label></th>
                            <td><input type="text" id="broker" name="broker" maxlength="36" /></td>
                            <td><a href="#broker">Select</a></td>
                        </tr>
                        <tr>
                            <th><label for="vin">VIN</label></th>
                            <td><input type="text" id="vin" name="vin" maxlength="17" /></td>
                            <td><a href="#vin">Select</a></td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>
            
            <div id="menu">
                <ul>
                    <li id="menu_item_tags" class="selected"><a href="#tags">Tags</a></li>
                    <li id="menu_item_summary"><a href="#summary">Summary</a></li>
                    <li id="menu_item_example"><a href="#example">Example</a></li>
                </ul>
            </div>
            
            <fieldset id="tags">
                <legend>Tags</legend>
                <table border="1">
                    <caption>Tags</caption>
                    <thead>
                        <tr>
                            <th><label for="tag_name">Name</label></th>
                            <th>Insert</th>
                            <th>Update</th>
                            <th>Delete</th>
                            <th>Fetch</th>
                            <th>+ Tag</th>
                            <th>- Tag</th>
                        </tr>
                    </thead>
                    <tbody id="tags_form">
                        <tr>
                            <td><input type="text" id="tag_name" name="tag_name" maxlength="64" /></td>
                            <td><a href="#insert">Insert</a></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                    <tbody id="tags_results">
                        <tr>
                            <td><input type="text" id="" name="" maxlength="64" /></td>
                            <td></td>
                            <td><a class="update" href="#123">Update</a></td>
                            <td><a class="delete" href="#123">Delete</a></td>
                            <td><a class="fetch" href="#123">Fetch</a></td>
                            <td><a class="apply" href="#123">+Tag</a></td>
                            <td><a class="remove" href="#123">-Tag</a></td>
                        </tr>
                    </tbody>
                </table>
                <table border>
                    <caption>Vehicles</caption>
                    <thead>
                        <tr><th colspan="2">Vehicle</th><th colspan="4">Model</th><th colspan="6">Configuration</th><th></th></tr>
                        <tr><th>VIN</th><th>Mileage</th>
                            <th>Make</th><th>Model Family</th><th>Series</th><th>Segment</th>
                            <th>Model Year</th><th>Engine</th><th>Drive Train</th><th>Transmission</th><th>Fuel Type</th><th>Passenger Doors</th>
                            <th></th></tr>
                    </thead>
                    <tbody id="tag_vehicles">
                        
                    </tbody>
                </table>
            </fieldset>
            
            <fieldset id="summary" class="hidden">
                <legend>Summary</legend>
                <div>
                    <h3>Model Year</h3>
                    <ul col="ModelYear" clr="true">
                        <li>...</li>
                    </ul>
                </div>
                <div>
                    <h3>Make</h3>
                    <ul col="MakeName" clr="true">
                        <li>...</li>
                    </ul>
                </div>
                <div>
                    <h3>Model Family</h3>
                    <ul col="ModelFamilyName" clr="true">
                        <li>...</li>
                    </ul>
                </div>
                <div>
                    <h3>Series</h3>
                    <ul col="SeriesName" clr="true">
                        <li>...</li>
                    </ul>
                </div>
            </fieldset>
            
            <fieldset id="example" class="hidden">
                <legend>Example</legend>
                <div>
                    <table border="1">
                        <caption>Vehicle Actuality</caption>
                        <thead>
                            <tr>
                                <th style="width: 50%"><label for="vehicle_actuality_1">Inventory</label></th>
                                <th style="width: 50%"><label for="vehicle_actuality_2">Not Inventory</label></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><input type="radio" id="vehicle_actuality_1" name="vehicle_actuality" value="1" checked="checked" /></td>
                                <td><input type="radio" id="vehicle_actuality_2" name="vehicle_actuality" value="2" /></td>
                            </tr>
                        </tbody>
                    </table>
                    <table border="1">
                        <caption>Model</caption>
                        <thead>
                            <tr>
                                <th><label for="make_name">Make</label></th>
                                <th><label for="model_family_name">Model Family</label></th>
                                <th><label for="series_name">Series</label></th>
                                <th><label for="segment_name">Segment</label></th>
                                <th><label for="body_type_name">Body Type</label></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><input type="text" id="make_name" name="make_name"/></td>
                                <td><input type="text" id="model_family_name" name="model_family_name"/></td>
                                <td><input type="text" id="series_name" name="series_name"/></td>
                                <td><input type="text" id="segment_name" name="segment_name"/></td>
                                <td><input type="text" id="body_type_name" name="body_type_name"/></td>
                            </tr>
                        </tbody>
                    </table>
                    <table border="1">
                        <caption>Configuration</caption>
                        <thead>
                            <tr>
                                <th><label for="model_year">Model Year</label></th>
                                <th><label for="engine_name">Engine</label></th>
                                <th><label for="drive_train">Drive Train</label></th>
                                <th><label for="transmission_name">Transmission</label></th>
                                <th><label for="fuel_type_name">Fuel Type</label></th>
                                <th><label for="passenger_doors_name">Passenger Doors</label></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><input type="text" id="model_year" name="model_year"/></td>
                                <td><input type="text" id="engine_name" name="engine_name"/></td>
                                <td><input type="text" id="drive_train" name="drive_train"/></td>
                                <td><input type="text" id="transmission_name" name="transmission_name"/></td>
                                <td><input type="text" id="fuel_type_name" name="fuel_type_name"/></td>
                                <td><input type="text" id="passenger_doors_name" name="passenger_doors_name"/></td>
                            </tr>
                        </tbody>
                    </table>
                    <table border="1">
                        <caption>Results</caption>
                        <thead>
                            <tr><th colspan="2">Vehicle</th><th colspan="4">Model</th><th colspan="6">Configuration</th><th></th></tr>
                            <tr><th>VIN</th><th>Mileage</th>
                                <th>Make</th><th>Model Family</th><th>Series</th><th>Segment</th>
                                <th>Model Year</th><th>Engine</th><th>Drive Train</th><th>Transmission</th><th>Fuel Type</th><th>Passenger Doors</th>
                                <th></th></tr>
                        </thead>
                        <tbody id="results_body">
                            
                        </tbody>
                    </table>
                </div>
            </fieldset>
            
        </div>
    </div>
    
    <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/Lib/jquery/jquery-ui-1.8.2.custom/js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/App/Vehicles/Application.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../Public/Scripts/App/Vehicles/TestTemplates.js"></script>

    <script type="text/javascript" charset="utf-8">
        var _Vehicles = new Vehicles();
        var m = window.location.search.match(/b=(.*)&?/);
        _Vehicles.main({ broker: m ? m[1] : undefined});

        if (typeof JSON === "undefined") {
            $LAB.script("../../Public/Scripts/Lib/json/json2.min.js");
        }
    </script>

    </form>
</body>
</html>
