﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MobileDemonstration.aspx.cs" Inherits="FirstLook.Client.WebApplication.Pages.Vehicles.MobileDemonstration" %>
<!doctype html>

<html lang="en">
<head id="Head1" runat="server">
    <title></title>
    <meta charset="utf-8"/>

    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="HandheldFriendly" content="yes" />
    <link rel="stylesheet" href="/resources/Scripts/FirstLook.Mobile/css/style.css"/>
  <script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-22250230-1']);
    _gaq.push(['_trackPageview']);

    (function() {
         var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
         ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
         var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
     })();
  </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="container">
        <header id="header" class="toolbar clearfix">
            <h1 id="#page_title">Vehicles</h1>
        </header>
        <div id="main" class="clearfix">
            <div id="scrollable" class="handle">
            </div>
        </div>
        <footer id="footer" class="clearfix">
        </footer>     
    </div>
    </form>
    <!-- Grab Google CDN's jQuery. fall back to local if necessary -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.js"></script>
  <script>!window.jQuery && document.write(unescape('%3Cscript src="/resources/Scripts/jQuery/1.5/jquery.js"%3E%3C/script%3E'))</script>

  <script src="/resources/Scripts/underscore/1.1.4/underscore.js"></script>
  <script src="/resources/Scripts/DragDealer/0.9.5/dragdealer.js"></script>
  <script src="/resources/Scripts/FirstLook.Mobile/lib.js"></script>

  <script src="../../Public/Scripts/App/Vehicles/Application.js"></script>
  <script src="../../Public/Scripts/App/Vehicles/MobileTemplate.js"></script>

  <script type="text/javascript" charset="utf-8">
       $(Vehicles).bind(Vehicles.raises.join(" "), function(evt) {
           var vals = _(arguments).chain().filter(function(v) { return _.isString(v) || _.isNumber(v); }).toArray().value().join(";");
           console.log("_trackEvent", evt.type, evt.namespace, vals);
           _gaq.push(["_trackEvent", evt.type, evt.namespace, vals])
       });

       var wires = {
           broker_id: "broker/:broker_id",
           actuality: "act/:actuality"
       }
       FirstLook.Session.wire(wires);
       
       var options = {
           broker: FirstLook.Session.get_state("broker_id"),
           actuality: FirstLook.Session.get_state("actuality")
       }

       var _Vehicles = new Vehicles();
       _Vehicles.main(options);

       if (typeof JSON === "undefined") {
           $LAB.script("../../Public/Scripts/Lib/json/json2.min.js");
       }
   </script>
</body>
</html>
