﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="FirstLook.Client.WebApplication.Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Default</title>
</head>
<body>
    <form id="DefaultForm" runat="server">
    <div>
        <ul>
            <li>Pages
                <ul>
                    <li>
                        <asp:HyperLink runat="server" ID="ClientPage" NavigateUrl="~/Pages/Clients/Demonstration.aspx" Text="Clients" />
                    </li>
                    <li>
                        <asp:HyperLink runat="server" ID="ClientMobile" NavigateUrl="~/Pages/Clients/MobileDemonstration.aspx" Text="Mobile Clients" />
                    </li>
                    <li>
                        <asp:HyperLink runat="server" ID="VehiclePage" NavigateUrl="~/Pages/Vehicles/Demonstration.aspx" Text="Vehicles" />
                    </li>
                    <li>
                        <asp:HyperLink runat="server" ID="VehicleMobile" NavigateUrl="~/Pages/Vehicles/MobileDemonstration.aspx" Text="Mobile Vehicles" />
                    </li>
                </ul>
            </li>
            <li>Services
                <ul>
                    <li>
                        <asp:HyperLink runat="server" ID="ClientService" NavigateUrl="~/Services/Client.asmx" Text="Clients" />
                    </li>
                    <li>
                        <asp:HyperLink runat="server" ID="LicenseService" NavigateUrl="~/Services/License.asmx" Text="Licenses" />
                    </li>
                    <li>
                        <asp:HyperLink runat="server" ID="VehicleService" NavigateUrl="~/Services/Vehicle.asmx" Text="Vehicle" />
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    </form>
</body>
</html>
