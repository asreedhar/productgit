﻿using System;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;
using NUnit.Framework;

namespace FirstLook.Client.DomainModel.Test.Integration.Licenses
{
    /// <summary>
    /// Licenseing integration tests.
    /// </summary>
    [TestFixture]
    public class LicenseTests
    {
        #region Setup

        /// <summary>
        /// Register all components.
        /// </summary>
        [TestFixtureSetUp]
        public void Setup()
        {
            Common.Utility.RegisterEverything();
        }        

        #endregion        

        #region License Tests

        /// <summary>
        /// Test that saving a license works correctly.
        /// </summary>
        [Test]
        public void SaveLicenseTest()
        {
            Guid        broker   = Common.Clients.Utility.BrokerHandle();
            IdentityDto identity = Common.Clients.Utility.Identity();
            ClearOutLicenses(broker, identity);

            LicenseDto license = new LicenseDto
            {
                LicenseModel = LicenseModelTypeDto.Site,
                DeviceType   = DeviceTypeDto.Mobile,
                Product      = "BlackBook",
                UseLimit     = true,
                AllowedUses  = 100,
                Expires      = true,
                ExpiryDate   = DateTime.Now.AddDays(30),
                Seats        = 3
            };

            // Save the license.
            SaveLicenseResultsDto results = Common.Licenses.Utility.SaveLicense(broker, identity, license);
                                                                                
            LicenseDto result = results.License;

            // Compare!
            Assert.NotNull(result);
            Assert.AreEqual(license.LicenseModel, result.LicenseModel);
            Assert.AreEqual(license.Product,      result.Product);
            Assert.AreEqual(license.DeviceType,   result.DeviceType);            
            Assert.AreEqual(license.Expires,      result.Expires);
            Assert.AreEqual(license.ExpiryDate,   result.ExpiryDate);
            Assert.AreEqual(license.UseLimit,     result.UseLimit);
            Assert.AreEqual(license.AllowedUses,  result.AllowedUses);
            Assert.AreEqual(license.Seats,        result.Seats);
        }

        /// <summary>
        /// Save a license and then fetch it and ensure they are the same.
        /// </summary>
        [Test]
        public void FetchLicenseTest()
        {
            Guid        broker   = Common.Clients.Utility.BrokerHandle();
            IdentityDto identity = Common.Clients.Utility.Identity();
            ClearOutLicenses(broker, identity);

            LicenseDto license = new LicenseDto
            {
                LicenseModel = LicenseModelTypeDto.Site,
                DeviceType   = DeviceTypeDto.Mobile,
                Product      = "BlackBook",
                UseLimit     = true,
                AllowedUses  = 100,
                Expires      = true,
                ExpiryDate   = DateTime.Now.AddDays(30),
                Seats        = 3
            };

            // Save the license.
            SaveLicenseResultsDto saveResults = Common.Licenses.Utility.SaveLicense(broker, identity, license);
            LicenseDto savedLicense = saveResults.License;

            if (!savedLicense.License.HasValue)
            {
                Assert.Fail("License not saved correctly!");                
            }

            // Fetch the license.
            LicenseResultsDto fetchResults = Common.Licenses.Utility.FetchLicense(broker, identity, savedLicense.License.Value);            

            // Compare!
            Assert.NotNull(saveResults.License.License);
            Assert.NotNull(fetchResults.License.License);
            Common.Licenses.Utility.Compare(saveResults.License, fetchResults.License);            
        }

        /// <summary>
        /// Test that fetching all of a dealer's licenses works. Will delete all existing licenses for the dealer, save 
        /// a bunch of new ones, and then try to fetch them and verify the list is correct.
        /// </summary>
        [Test]
        public void FetchLicensesTest()
        {
            Guid        broker   = Common.Clients.Utility.BrokerHandle();
            IdentityDto identity = Common.Clients.Utility.Identity();
            ClearOutLicenses(broker, identity);

            // First, get all the licenses.
            LicensesResultsDto licensesResults = Common.Licenses.Utility.FetchLicenses(broker, identity);

            // Delete them all, so we're starting from scratch.
            foreach (LicenseInfoDto dto in licensesResults.Licenses)
            {
                if (!dto.License.HasValue)
                {
                    throw new ApplicationException("License has no guid.");
                }
                Common.Licenses.Utility.DeleteLicense(broker, identity, dto.License.Value);
            }

            // Make sure there aren't any more licenses.
            licensesResults = Common.Licenses.Utility.FetchLicenses(broker, identity);
            Assert.AreEqual(0, licensesResults.Licenses.Count);

            LicenseDto license1 = new LicenseDto
            {
                LicenseModel = LicenseModelTypeDto.Floating,
                DeviceType   = DeviceTypeDto.Desktop,
                Product      = "Kelley Blue Book",
                UseLimit     = false,
                Expires      = false,
            };

            // Save the first.
            LicenseDto savedLicense1 = Common.Licenses.Utility.SaveLicense(broker, identity, license1).License;

            LicenseDto license2 = new LicenseDto
            {
                LicenseModel = LicenseModelTypeDto.Named,
                DeviceType   = DeviceTypeDto.Mobile,
                Product      = "Galves",
                Expires      = true,
                ExpiryDate   = DateTime.Now.AddDays(30),
                Seats        = 3
            };

            // Save the second.
            LicenseDto savedLicense2 = Common.Licenses.Utility.SaveLicense(broker, identity, license2).License;

            LicenseDto license3 = new LicenseDto
            {
                LicenseModel = LicenseModelTypeDto.Site,
                DeviceType   = DeviceTypeDto.Desktop,
                Product      = "NAAA",                
                UseLimit     = true,
                AllowedUses  = 100,
                Expires      = true,
                ExpiryDate   = DateTime.Now.AddDays(30),
                Seats        = 3
            };

            // Save the third.
            LicenseDto savedLicense3 = Common.Licenses.Utility.SaveLicense(broker, identity, license3).License;

            // Fetch them all.
            licensesResults = Common.Licenses.Utility.FetchLicenses(broker, identity);
            Assert.AreEqual(3, licensesResults.Licenses.Count);

            foreach (LicenseInfoDto dto in licensesResults.Licenses)
            {
                if (dto.License == savedLicense1.License)
                {
                    Common.Licenses.Utility.Compare(dto, savedLicense1);
                }
                else if (dto.License == savedLicense2.License)
                {
                    Common.Licenses.Utility.Compare(dto, savedLicense2);
                }
                else if (dto.License == savedLicense3.License)
                {
                    Common.Licenses.Utility.Compare(dto, savedLicense3);
                }
                else
                {
                    Assert.Fail("Couldn't match license.");
                }
            }
        }

        /// <summary>
        /// Test that revoke a license works correctly.
        /// </summary>
        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void DeleteLicenseTest()
        {
            Guid        broker   = Common.Clients.Utility.BrokerHandle();
            IdentityDto identity = Common.Clients.Utility.Identity();
            ClearOutLicenses(broker, identity);

            LicenseDto license = new LicenseDto
            {
                LicenseModel = LicenseModelTypeDto.Site,
                DeviceType   = DeviceTypeDto.Mobile,
                Product      = "BlackBook",
                UseLimit     = true,
                AllowedUses  = 100,
                Expires      = true,
                ExpiryDate   = DateTime.Now.AddDays(30),
                Seats        = 3
            };

            // Save the license.
            SaveLicenseResultsDto saveResults = Common.Licenses.Utility.SaveLicense(broker, identity, license);
            LicenseDto savedLicense = saveResults.License;

            if (!savedLicense.License.HasValue)
            {
                Assert.Fail("License not saved correctly!");                
            }

            // Delete the license.
            Common.Licenses.Utility.DeleteLicense(broker, identity, savedLicense.License.Value);
            
            // Try to fetch the license. Should throw exception.
            Common.Licenses.Utility.FetchLicense(broker, identity, savedLicense.License.Value);                        
        }

        #endregion

        #region Lease Tests

        /// <summary>
        /// Verify that creating a lease works as expected.
        /// </summary>
        [Test]
        public void SaveLeaseTest()
        {
            Guid        broker   = Common.Clients.Utility.BrokerHandle();
            IdentityDto identity = Common.Clients.Utility.Identity();
            ClearOutLicenses(broker, identity);

            LicenseDto license = new LicenseDto
            {
                LicenseModel = LicenseModelTypeDto.Named,
                DeviceType   = DeviceTypeDto.Mobile,
                Product      = "BlackBook",
                UseLimit     = true,
                AllowedUses  = 100,
                Expires      = true,
                ExpiryDate   = DateTime.Now.AddDays(30),
                Seats        = 3
            };

            // Save the license.
            SaveLicenseResultsDto saveResults = Common.Licenses.Utility.SaveLicense(broker, identity, license);
            LicenseDto savedLicense = saveResults.License;

            if (!savedLicense.License.HasValue)
            {
                Assert.Fail("License wasn't saved.");
            }

            // Create the lease.
            CreateLeaseResultsDto createResults = Common.Licenses.Utility.CreateLease(broker, identity, savedLicense.License.Value, "cclouston");
            LeaseDto lease = createResults.Lease;

            // Compare!
            Assert.IsNotNull(lease);
            Assert.IsNull(lease.LastUsed);
            Assert.AreEqual(savedLicense.License, createResults.License.License);
            Assert.AreEqual("cclouston",          lease.User.UserName);
            Assert.AreEqual("bfung",              lease.AssignedBy.UserName);            
            Assert.AreEqual(0,                    lease.Uses);            
        }

        /// <summary>
        /// Test that fetching a lease works properly.
        /// </summary>
        [Test]
        public void FetchLeaseTest()
        {
            Guid        broker   = Common.Clients.Utility.BrokerHandle();
            IdentityDto identity = Common.Clients.Utility.Identity();
            ClearOutLicenses(broker, identity);

            LicenseDto license = new LicenseDto
            {
                LicenseModel = LicenseModelTypeDto.Named,
                DeviceType   = DeviceTypeDto.Mobile,
                Product      = "BlackBook",
                UseLimit     = true,
                AllowedUses  = 100,
                Expires      = true,
                ExpiryDate   = DateTime.Now.AddDays(30),
                Seats        = 3
            };

            // Save the license.
            SaveLicenseResultsDto saveResults = Common.Licenses.Utility.SaveLicense(broker, identity, license);
            LicenseDto savedLicense = saveResults.License;

            if (!savedLicense.License.HasValue)
            {
                Assert.Fail("License wasn't saved.");
            }

            // Create the lease.
            CreateLeaseResultsDto createResults = Common.Licenses.Utility.CreateLease(broker, identity, savedLicense.License.Value, "cclouston");
            LeaseDto savedLease = createResults.Lease;

            // Fetch the lease.
            LeaseResultsDto fetchResults = Common.Licenses.Utility.FetchLease(broker, identity, savedLicense.License.Value, "cclouston");
            LeaseDto fetchedLease = fetchResults.Lease;

            // Compare!
            Assert.IsNotNull(fetchedLease);
            Assert.IsNull(fetchedLease.LastUsed);
            Assert.AreEqual(savedLicense.License,  fetchResults.License.License);
            Assert.AreEqual(savedLease.AssignedOn, fetchedLease.AssignedOn);
            Assert.AreEqual("cclouston",           fetchedLease.User.UserName);
            Assert.AreEqual("bfung",               fetchedLease.AssignedBy.UserName);            
            Assert.AreEqual(0,                     fetchedLease.Uses);            
        }

        /// <summary>
        /// Test that fetching leases for a license works.
        /// </summary>
        [Test]
        public void FetchLeasesTest()
        {
            Guid        broker   = Common.Clients.Utility.BrokerHandle();
            IdentityDto identity = Common.Clients.Utility.Identity();
            ClearOutLicenses(broker, identity);

            LicenseDto license = new LicenseDto
            {
                LicenseModel = LicenseModelTypeDto.Named,
                DeviceType   = DeviceTypeDto.Mobile,
                Product      = "BlackBook",
                UseLimit     = true,
                AllowedUses  = 100,
                Expires      = true,
                ExpiryDate   = DateTime.Now.AddDays(30),
                Seats        = 3
            };

            // Save the license.
            SaveLicenseResultsDto saveResults = Common.Licenses.Utility.SaveLicense(broker, identity, license);
            LicenseDto savedLicense = saveResults.License;

            if (!savedLicense.License.HasValue)
            {
                Assert.Fail("License wasn't saved.");
            }

            // Create the leases.
            Common.Licenses.Utility.CreateLease(broker, identity, savedLicense.License.Value, "cclouston");
            Common.Licenses.Utility.CreateLease(broker, identity, savedLicense.License.Value, "bfung");
            Common.Licenses.Utility.CreateLease(broker, identity, savedLicense.License.Value, "swenmouth");            

            // Fetch the leases.
            LeasesResultsDto fetchedLeases = Common.Licenses.Utility.FetchLeases(broker, identity, savedLicense.License.Value);

            Assert.AreEqual(fetchedLeases.License.License, savedLicense.License);
            Assert.AreEqual(3, fetchedLeases.Leases.Count);

            bool cclouston = false;
            bool bfung = false;
            bool swenmouth = false;

            foreach (LeaseInfoDto lease in fetchedLeases.Leases)
            {
                switch (lease.User.UserName)
                {
                    case "cclouston":
                        cclouston = true;
                        break;

                    case "bfung":
                        bfung = true;
                        break;

                    case "swenmouth":
                        swenmouth = true;
                        break;                        
                }                   
            }

            if (!cclouston || !bfung || !swenmouth)
            {
                Assert.Fail("Did not retrieve all leases.");
            }
        }

        /// <summary>
        /// Test that deleting a lease works correctly.
        /// </summary>
        [Test]
        public void DeleteLeaseTest()
        {
            Guid        broker   = Common.Clients.Utility.BrokerHandle();
            IdentityDto identity = Common.Clients.Utility.Identity();
            ClearOutLicenses(broker, identity);

            LicenseDto license = new LicenseDto
            {
                LicenseModel = LicenseModelTypeDto.Named,
                DeviceType   = DeviceTypeDto.Mobile,
                Product      = "BlackBook",
                UseLimit     = true,
                AllowedUses  = 100,
                Expires      = true,
                ExpiryDate   = DateTime.Now.AddDays(30),
                Seats        = 3
            };

            // Save the license.
            SaveLicenseResultsDto saveResults = Common.Licenses.Utility.SaveLicense(broker, identity, license);
            LicenseDto savedLicense = saveResults.License;

            if (!savedLicense.License.HasValue)
            {
                Assert.Fail("License wasn't saved.");
            }

            // Create the lease.
            Common.Licenses.Utility.CreateLease(broker, identity, savedLicense.License.Value, "cclouston");

            // Delete the lease.
            RevokeLeaseResultsDto deleteResults = Common.Licenses.Utility.DeleteLease(broker, identity, savedLicense.License.Value, "cclouston");
            Assert.AreEqual(savedLicense.License, deleteResults.License.License);
            Assert.AreEqual(0, deleteResults.Leases.Count);

            // Try to fetch the lease.
            LeaseResultsDto fetchResults = Common.Licenses.Utility.FetchLease(broker, identity, savedLicense.License.Value, "cclouston");
            Assert.IsNull(fetchResults.Lease);

            // Create more leases.
            Common.Licenses.Utility.CreateLease(broker, identity, savedLicense.License.Value, "bfung");
            Common.Licenses.Utility.CreateLease(broker, identity, savedLicense.License.Value, "cclouston");
            Common.Licenses.Utility.CreateLease(broker, identity, savedLicense.License.Value, "swenmouth");

            // Delete one of the leases.
            deleteResults = Common.Licenses.Utility.DeleteLease(broker, identity, savedLicense.License.Value, "cclouston");
            Assert.AreEqual(savedLicense.License, deleteResults.License.License);
            Assert.AreEqual(2, deleteResults.Leases.Count);

            // Make sure the right leases are left.
            bool bfung = false;
            bool swenmouth = false;
            foreach (LeaseInfoDto lease in deleteResults.Leases)
            {
                switch(lease.User.UserName)
                {
                    case "bfung":
                        bfung = true;
                        break;

                    case "swenmouth":
                        swenmouth = true;
                        break;
                }
            }

            if (!bfung || !swenmouth)
            {
                Assert.Fail("Leases not properly retrieved.");
            }

            // Try to fetch the deleted lease again.
            fetchResults = Common.Licenses.Utility.FetchLease(broker, identity, savedLicense.License.Value, "cclouston");
            Assert.IsNull(fetchResults.Lease);
        }

        #endregion

        #region Token Tests

        /// <summary>
        /// Test that getting a bag of tokens works as expected.
        /// </summary>
        [Test]
        public void FetchTokens()
        {
            Guid        broker   = Common.Clients.Utility.BrokerHandle();
            IdentityDto identity = Common.Clients.Utility.Identity();
            ClearOutLicenses(broker, identity);

            // First, fetch all existing licenses and delete them all.
            LicensesResultsDto fetchResults = Common.Licenses.Utility.FetchLicenses(broker, identity);
            foreach (LicenseInfoDto info in fetchResults.Licenses)
            {
                if (!info.License.HasValue)
                {
                    throw new ApplicationException("Whaa?");
                }
                Common.Licenses.Utility.DeleteLicense(broker, identity, info.License.Value);
            }

            LicenseDto license1 = new LicenseDto
            {
                LicenseModel = LicenseModelTypeDto.Site,
                DeviceType   = DeviceTypeDto.Mobile,
                Product      = "BlackBook",
                UseLimit     = true,
                AllowedUses  = 100,
                Expires      = true,
                ExpiryDate   = DateTime.Now.AddDays(30),
                Seats        = 3
            };

            LicenseDto license2 = new LicenseDto
            {
                LicenseModel = LicenseModelTypeDto.Site,
                DeviceType   = DeviceTypeDto.Mobile,
                Product      = "Galves",
                UseLimit     = true,
                AllowedUses  = 100,
                Expires      = true,
                ExpiryDate   = DateTime.Now.AddDays(30),
                Seats        = 1
            };

            LicenseDto license3 = new LicenseDto
            {
                LicenseModel = LicenseModelTypeDto.Named,
                DeviceType   = DeviceTypeDto.Mobile,
                Product      = "Edmunds",
                UseLimit     = false,                
                Expires      = false,                
                Seats        = 5
            };

            // Save the licenses.
            Common.Licenses.Utility.SaveLicense(broker, identity, license1);
            Common.Licenses.Utility.SaveLicense(broker, identity, license2);
            Common.Licenses.Utility.SaveLicense(broker, identity, license3);

            // Get the tokens!
            TokensResultsDto results1 = Common.Licenses.Utility.Tokens(broker, identity, null, DeviceTypeDto.Mobile, identity.Name);

            Assert.IsNotNull(results1.Device);

            foreach (TokenDto token in results1.Tokens)
            {
                switch (token.Product)
                {
                    case "BlackBook":
                    case "Galves":
                        Assert.AreEqual(TokenStatusDto.Valid, token.Status);
                        Assert.IsNotNull(token.Token);
                        break;

                    case "Edmunds":
                        Assert.AreEqual(TokenStatusDto.NoLease, token.Status);
                        Assert.IsNull(token.Token);
                        break;

                    case "Kelley Blue Book":
                    case "Manheim":
                    case "NAAA":
                    case "NADA":
                        Assert.AreEqual(TokenStatusDto.NoLicense, token.Status);
                        Assert.IsNull(token.Token);
                        break;

                    default:
                        throw new ApplicationException("Unexpected product.");                        
                }                
            }

            // Get the tokens again, this time with the device.
            TokensResultsDto results2 = Common.Licenses.Utility.Tokens(broker, identity, results1.Device, DeviceTypeDto.Mobile, identity.Name);

            Assert.IsNotNull(results2.Device);
            Assert.AreEqual(results1.Device, results2.Device);

            foreach (TokenDto token in results2.Tokens)
            {
                switch (token.Product)
                {
                    case "BlackBook":
                    case "Galves":
                        Assert.AreEqual(TokenStatusDto.Valid, token.Status);
                        Assert.IsNotNull(token.Token);
                        break;

                    case "Edmunds":
                        Assert.AreEqual(TokenStatusDto.NoLease, token.Status);
                        Assert.IsNull(token.Token);
                        break;
                    
                    case "Kelley Blue Book":
                    case "Manheim":
                    case "NAAA":
                    case "NADA":
                        Assert.AreEqual(TokenStatusDto.NoLicense, token.Status);
                        Assert.IsNull(token.Token);
                        break;

                    default:
                        throw new ApplicationException("Unexpected product.");
                }
            }

            // Get tokens for another user for the same broker.
            TokensResultsDto results3 = Common.Licenses.Utility.Tokens(broker, identity, null, DeviceTypeDto.Mobile, "cclouston");

            Assert.IsNotNull(results3.Device);
            Assert.AreNotEqual(results1.Device, results3.Device);

            foreach (TokenDto token in results3.Tokens)
            {
                switch (token.Product)
                {
                    case "BlackBook":
                        Assert.AreEqual(TokenStatusDto.Valid, token.Status);
                        Assert.IsNotNull(token.Token);
                        break;

                    case "Galves":
                        Assert.AreEqual(TokenStatusDto.SeatLimitExceed, token.Status);
                        Assert.IsNull(token.Token);
                        break;

                    case "Edmunds":
                        Assert.AreEqual(TokenStatusDto.NoLease, token.Status);
                        Assert.IsNull(token.Token);
                        break;
                    
                    case "Kelley Blue Book":
                    case "Manheim":
                    case "NAAA":
                    case "NADA":
                        Assert.AreEqual(TokenStatusDto.NoLicense, token.Status);
                        Assert.IsNull(token.Token);
                        break;

                    default:
                        throw new ApplicationException("Unexpected product.");
                }
            }
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Delete all of a client's licenses.
        /// </summary>
        /// <param name="broker">Client to delete licenses for.</param>
        /// <param name="identity">Identity information.</param>
        public static void ClearOutLicenses(Guid broker, IdentityDto identity)
        {
            LicensesResultsDto result = Common.Licenses.Utility.FetchLicenses(broker, identity);

            foreach (LicenseInfoDto license in result.Licenses)
            {
                if (license.License.HasValue)
                {
                    Common.Licenses.Utility.DeleteLicense(broker, identity, license.License.Value);
                }
            }
        }

        #endregion
    }
}
