﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Data;
using NUnit.Framework;

namespace FirstLook.Client.DomainModel.Test.Integration.Vehicles
{
    /// <summary>
    /// Tag integration tests.
    /// </summary>
    [TestFixture]
    public class TagTest
    {
        #region Setup

        /// <summary>
        /// Register everything we'll need in the registry. Also, grab a broker handle from the database.
        /// </summary>
        [TestFixtureSetUp]
        public void FixtureSetup()
        {            
            Common.Utility.RegisterEverything();                   
        }

        /// <summary>
        /// Clear out the registry.
        /// </summary>
        [TestFixtureTearDown]
        public void FixtureTeardown()
        {
            Common.Utility.ClearRegistry();
        }

        #endregion

        /// <summary>
        /// Test that creating a tag works.
        /// </summary>
        [Test]
        public void CreateTagTest()
        {
            // Set the arguments.
            Guid        broker   = Common.Clients.Utility.BrokerHandle();
            IdentityDto identity = Common.Clients.Utility.Identity();
            string      tag      = Common.Vehicles.Utility.TagName();

            // Create a tag.
            CreateTagResultsDto results = Common.Vehicles.Utility.CreateTag(identity, broker, tag);

            // Verify the results.
            Assert.AreEqual(TagResultDto.Success, results.Result);
            
            using (IDbConnection connection = Database.Connection("Vehicle"))
            {                                
                using (IDbCommand command = connection.CreateCommand())
                {
                    const string commandText =
                        "SELECT T.Handle, " +
                        "       TH.Name, " +
                        "       C.Handle AS ClientHandle " +
                        "FROM   Client.Tag T " +
                        "JOIN   Client.Tag_History TH ON TH.TagID = T.TagID " +
                        "JOIN   Audit.AuditRow R ON R.AuditRowID = TH.AuditRowID " +
                        "JOIN   Client.Client C ON C.ClientID = T.ClientID " +
                        "WHERE  TH.Name = @Tag " +
                        "AND    GETDATE() BETWEEN R.ValidFrom AND R.ValidUpTo " +
                        "AND    R.WasInsert = 1 ";

                    command.CommandType = CommandType.Text;
                    command.CommandText = commandText;
                    Database.AddWithValue(command, "Tag", tag, DbType.String);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {                            
                            Assert.AreEqual(broker,             reader.GetGuid(reader.GetOrdinal("ClientHandle")));
                            Assert.AreEqual(results.Tag.Handle, reader.GetGuid(reader.GetOrdinal("Handle")));
                            Assert.AreEqual(results.Tag.Name,   reader.GetString(reader.GetOrdinal("Name")));
                            Assert.AreEqual(results.Tag.Name,   tag);
                            Assert.AreEqual(results.Tag.Type,   TagTypeDto.Static);                                                                                    
                        }
                        else
                        {
                            Assert.Fail("Couldn't verify tag was inserted.");
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Test that creating a tag for a bogus broker doesn't work.
        /// </summary>
        [Test]
        [ExpectedException(typeof(ApplicationException))]
        public void CreateTagBogusBrokerTest()
        {
            // Set the arguments.
            Guid        broker   = Guid.Empty;
            IdentityDto identity = Common.Clients.Utility.Identity();
            string      tag      = Common.Vehicles.Utility.TagName();

            // Explodo?
            Common.Vehicles.Utility.CreateTag(identity, broker, tag); 
        }

        /// <summary>
        /// Test that creating a tag with a duplicate value is rejected.
        /// </summary>
        [Test]
        public void CreateDuplicateTagTest()
        {
            // Set the arguments.
            Guid        broker   = Common.Clients.Utility.BrokerHandle();
            IdentityDto identity = Common.Clients.Utility.Identity();
            string      tag      = Common.Vehicles.Utility.TagName();

            // Create the tag.
            CreateTagResultsDto results = Common.Vehicles.Utility.CreateTag(identity, broker, tag);

            Assert.AreEqual(TagResultDto.Success, results.Result);            

            // Try to create it again..
            results = Common.Vehicles.Utility.CreateTag(identity, broker, tag);

            Assert.AreEqual(TagResultDto.TagAlreadyExists, results.Result);            
        }

        /// <summary>
        /// Test that updating a tag works.
        /// </summary>
        [Test]
        public void UpdateTagTest()
        {
            // Set the arguments.
            Guid        broker   = Common.Clients.Utility.BrokerHandle();
            IdentityDto identity = Common.Clients.Utility.Identity();
            string      oldTag   = Common.Vehicles.Utility.TagName();

            // Create a tag.
            CreateTagResultsDto createResults = Common.Vehicles.Utility.CreateTag(identity, broker, oldTag);
            Assert.AreEqual(TagResultDto.Success, createResults.Result);

            // Change the tag value.
            Guid   tagHandle = createResults.Tag.Handle;
            string newTag    = Common.Vehicles.Utility.TagName();

            // Update it.
            UpdateTagResultsDto updateResults = Common.Vehicles.Utility.UpdateTag(identity, broker, tagHandle, newTag);
            Assert.AreEqual(TagResultDto.Success, updateResults.Result);

            // Verify the results.
            using (IDbConnection connection = Database.Connection("Vehicle"))
            {
                // Make sure old value was closed off.
                using (IDbCommand command = connection.CreateCommand())
                {
                    const string commandText =
                        "SELECT 1 " +
                        "FROM   Client.Tag T " +
                        "JOIN   Client.Tag_History TH ON TH.TagID = T.TagID " +
                        "JOIN   Audit.AuditRow R ON R.AuditRowID = TH.AuditRowID " +
                        "WHERE  T.Handle = @Handle " +
                        "AND    TH.Name = @Tag " +
                        "AND    GETDATE() > R.ValidUpTo " +
                        "AND    R.WasInsert = 1";

                    command.CommandType = CommandType.Text;
                    command.CommandText = commandText;
                    Database.AddWithValue(command, "Handle", tagHandle, DbType.Guid);
                    Database.AddWithValue(command, "Tag", oldTag, DbType.String);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        Assert.IsTrue(reader.Read());
                    }
                }

                // Make sure new value was put in.
                using (IDbCommand command = connection.CreateCommand())
                {
                    const string commandText =
                        "SELECT 1 " +
                        "FROM   Client.Tag T " +
                        "JOIN   Client.Tag_History TH ON TH.TagID = T.TagID " +
                        "JOIN   Audit.AuditRow R ON R.AuditRowID = TH.AuditRowID " +
                        "WHERE  T.Handle = @Handle " +
                        "AND    TH.Name = @Tag " +
                        "AND    GETDATE() BETWEEN R.ValidFrom AND R.ValidUpTo " +
                        "AND    R.WasUpdate = 1 ";

                    command.CommandType = CommandType.Text;
                    command.CommandText = commandText;
                    Database.AddWithValue(command, "Handle", tagHandle, DbType.Guid);
                    Database.AddWithValue(command, "Tag", newTag, DbType.String);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        Assert.IsTrue(reader.Read());
                    }
                }
            }
        }

        /// <summary>
        /// Tets that deleting a tag works.
        /// </summary>
        [Test]
        public void DeleteTagTest()
        {
            // Set the arguments.
            Guid        broker   = Common.Clients.Utility.BrokerHandle();
            Guid        vehicle  = Common.Vehicles.Utility.VehicleHandle();
            IdentityDto identity = Common.Clients.Utility.Identity();

            // Create a random tag value.
            string tagName = Common.Vehicles.Utility.TagName();

            // Create the tag to ensure at least one is in the database.
            CreateTagResultsDto createResults = Common.Vehicles.Utility.CreateTag(identity, broker, tagName);
            Assert.AreEqual(TagResultDto.Success, createResults.Result);

            Guid tag = createResults.Tag.Handle;

            // Apply the tag to at least one vehicle.
            ApplyTagResultsDto applyResults = Common.Vehicles.Utility.ApplyTag(identity, broker, vehicle, tag);
            Assert.AreEqual(TagResultDto.Success, applyResults.Result);

            // Get the number of tagged vehicles prior to deletion.
            int taggedVehiclesBefore = Common.Vehicles.Utility.VehiclesTagged(createResults.Tag.Handle);

            // Execute!
            DeleteTagResultsDto results = Common.Vehicles.Utility.DeleteTag(identity, broker, tag);
            Assert.AreEqual(TagResultDto.Success, results.Result);
            
            using (IDbConnection connection = Database.Connection("Vehicle"))
            {                                
                using (IDbCommand command = connection.CreateCommand())
                {
                    const string commandText =
                        "SELECT 1 " +
                        "FROM   Client.Tag T " +
                        "JOIN   Client.Tag_History TH ON TH.TagID = T.TagID " +
                        "JOIN   Audit.AuditRow R ON R.AuditRowID = TH.AuditRowID " +                        
                        "WHERE  T.Handle = @Tag " +
                        "AND    GETDATE() BETWEEN R.ValidFrom AND R.ValidUpTo " +                        
                        "AND    R.WasDelete = 1";

                    command.CommandType = CommandType.Text;
                    command.CommandText = commandText;
                    Database.AddWithValue(command, "Tag", tag, DbType.Guid);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        Assert.IsTrue(reader.Read());                        
                    }
                }
            }

            // Get the number of tagged vehicles post deletion.
            int taggedVehiclesAfter = Common.Vehicles.Utility.VehiclesTagged(tag);

            // Make sure the counts line up.
            Assert.AreEqual(results.VehiclesAffected, taggedVehiclesBefore);
            Assert.AreEqual(0, taggedVehiclesAfter);
        }        

        /// <summary>
        /// Test the retrieval of all of a client's tags.
        /// </summary>
        [Test]
        public void FetchTagsTest()
        {
            Guid broker = Common.Clients.Utility.BrokerHandle();
            IdentityDto identity = Common.Clients.Utility.Identity();

            // Create a random tag value.
            string tag = Common.Vehicles.Utility.TagName();

            // Create the tag to ensure at least one is in the database.
            CreateTagResultsDto createResults = Common.Vehicles.Utility.CreateTag(identity, broker, tag);
            Assert.AreEqual(TagResultDto.Success, createResults.Result);
           
            // Get a brokers tags.
            BrokerTagsResultsDto brokerResults = Common.Vehicles.Utility.BrokerTags(identity, broker);
            Assert.AreEqual(TagResultDto.Success, brokerResults.Result);
                        
            IList<string> tagNames = new List<string>();

            // Get the tags directly from the database.
            using (IDbConnection connection = Database.Connection("Vehicle"))
            {                
                using (IDbCommand command = connection.CreateCommand())
                {
                    const string commandText = 
                        "SELECT Name " +
                        "FROM   Client.Tag T " +
                        "JOIN   Client.Tag_History TH ON TH.TagID = T.TagID " +                        
                        "JOIN   Audit.AuditRow R ON R.AuditRowID = TH.AuditRowID " +
                        "JOIN   Client.Client C ON C.ClientID = T.ClientID " +
                        "WHERE  C.Handle = @Handle " +
                        "AND    GETDATE() BETWEEN R.ValidFrom AND R.ValidUpTo " +
                        "AND    R.WasDelete = 0";

                    command.CommandType = CommandType.Text;
                    command.CommandText = commandText;
                    Database.AddWithValue(command, "Handle", broker, DbType.Guid);                        

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        while(reader.Read())
                        {
                            tagNames.Add(reader.GetString(reader.GetOrdinal("Name")));
                        }                        
                    }
                }
            }

            // Verify the results.
            Assert.AreEqual(brokerResults.Tags.Count, tagNames.Count);

            // Jesus, this is ugly. I'm so sorry. It's just trying to match the two collections of tag names against 
            // each other.
            if (tagNames.Where(name => brokerResults.Tags.Where(x => x.Name.Equals(name)).FirstOrDefault() == null).Count() > 0)
            {
                Assert.Fail("Not all tags returned.");
            }
        }

        /// <summary>
        /// Test the association of a tag with a vehicle.
        /// </summary>
        [Test]
        public void ApplyTagTest()
        {
            // Set the arguments.
            Guid        broker   = Common.Clients.Utility.BrokerHandle();
            Guid        vehicle  = Common.Vehicles.Utility.VehicleHandle();            
            IdentityDto identity = Common.Clients.Utility.Identity();

            // Create a random tag value.
            string tagName = Common.Vehicles.Utility.TagName();

            // Create a tag.
            CreateTagResultsDto createResults = Common.Vehicles.Utility.CreateTag(identity, broker, tagName);
            Assert.AreEqual(TagResultDto.Success, createResults.Result);

            // Apply it.
            ApplyTagResultsDto applyResults = Common.Vehicles.Utility.ApplyTag(identity, broker, vehicle, createResults.Tag.Handle);
            Assert.AreEqual(TagResultDto.Success, applyResults.Result);
        }

        /// <summary>
        /// Test the association of a tag with a vehicle.
        /// </summary>
        [Test]
        public void RemoveTagTest()
        {
            // Set the arguments.
            Guid        broker   = Common.Clients.Utility.BrokerHandle();
            Guid        vehicle  = Common.Vehicles.Utility.VehicleHandle();            
            IdentityDto identity = Common.Clients.Utility.Identity();

            // Create a random tag value.
            string tagName = Common.Vehicles.Utility.TagName();

            // Create a tag.
            CreateTagResultsDto createResults = Common.Vehicles.Utility.CreateTag(identity, broker, tagName);
            Assert.AreEqual(TagResultDto.Success, createResults.Result);

            // Apply it.
            ApplyTagResultsDto applyResults = Common.Vehicles.Utility.ApplyTag(identity, broker, vehicle, createResults.Tag.Handle);
            Assert.AreEqual(TagResultDto.Success, applyResults.Result);

            // Remove it.
            RemoveTagResultsDto removeResults = Common.Vehicles.Utility.RemoveTag(identity, broker, vehicle, createResults.Tag.Handle);
            Assert.AreEqual(TagResultDto.Success, removeResults.Result);
        }

        /// <summary>
        /// Test that a vehicles tags come back correctly.
        /// </summary>
        [Test]
        public void VehicleTagsTest()
        {
            // Set the arguments.
            Guid        broker   = Common.Clients.Utility.BrokerHandle();
            Guid        vehicle  = Common.Vehicles.Utility.VehicleHandle();            
            IdentityDto identity = Common.Clients.Utility.Identity();

            // Create a random tag value.
            string tagName = Common.Vehicles.Utility.TagName();

            // Create a tag.
            CreateTagResultsDto createResults = Common.Vehicles.Utility.CreateTag(identity, broker, tagName);
            Assert.AreEqual(TagResultDto.Success, createResults.Result);

            // Apply it.
            ApplyTagResultsDto applyResults = Common.Vehicles.Utility.ApplyTag(identity, broker, vehicle, createResults.Tag.Handle);
            Assert.AreEqual(TagResultDto.Success, applyResults.Result);

            // Create a different tag value.
            tagName = Common.Vehicles.Utility.TagName();

            // Create another.
            createResults = Common.Vehicles.Utility.CreateTag(identity, broker, tagName);
            Assert.AreEqual(TagResultDto.Success, createResults.Result);

            // Apply that one too.
            applyResults = Common.Vehicles.Utility.ApplyTag(identity, broker, vehicle, createResults.Tag.Handle);
            Assert.AreEqual(TagResultDto.Success, applyResults.Result);

            // Get the number of tags for the vehicle.
            VehicleTagsResultsDto vehicleResults = Common.Vehicles.Utility.VehicleTags(identity, broker, vehicle);
            Assert.AreEqual(TagResultDto.Success, vehicleResults.Result);
            Assert.GreaterOrEqual(vehicleResults.Tags.Count, 2);
        }

        /// <summary>
        /// Test that retrieving vehicles tagged with a given tag works.
        /// </summary>
        [Test]
        public void TaggedVehiclesTest()
        {
            // Set the arguments.
            Guid        broker   = Common.Clients.Utility.BrokerHandle();
            Guid        vehicle  = Common.Vehicles.Utility.VehicleHandle();            
            IdentityDto identity = Common.Clients.Utility.Identity();

            // Create a random tag value.
            string tagName = Common.Vehicles.Utility.TagName();

            // Create a tag.
            CreateTagResultsDto createResults = Common.Vehicles.Utility.CreateTag(identity, broker, tagName);
            Assert.AreEqual(TagResultDto.Success, createResults.Result);

            // Apply it.
            ApplyTagResultsDto applyResults = Common.Vehicles.Utility.ApplyTag(identity, broker, vehicle, createResults.Tag.Handle);
            Assert.AreEqual(TagResultDto.Success, applyResults.Result);

            // Get the number of tagged vehicles.
            TaggedVehiclesResultsDto taggedResults = Common.Vehicles.Utility.TaggedVehicles(identity, broker, createResults.Tag.Handle);
            Assert.GreaterOrEqual(taggedResults.Vehicles.Count, 1);
        }
    }
}
