﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Commands.Impl;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Data;

namespace FirstLook.Client.DomainModel.Test.Common.Vehicles
{
    /// <summary>
    /// Helper utilities for testing the vehicle domain model.
    /// </summary>
    internal static class Utility
    {
        #region Database

        /// <summary>
        /// Get a vehicle handle, or insert one if one doesn't exist.
        /// </summary>
        /// <returns></returns>
        internal static Guid VehicleHandle()
        {
            using (IDbConnection connection = Database.Connection("Vehicle"))
            {
                // Try to grab a vehicle handle.
                using (IDbCommand command = connection.CreateCommand())
                {
                    const string commandText =
                        "SELECT Top 1 Handle " +
                        "FROM   Client.Vehicle";

                    command.CommandType = CommandType.Text;
                    command.CommandText = commandText;

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return reader.GetGuid(reader.GetOrdinal("Handle"));
                        }
                    }
                }
                throw new DataException("No vehicle handle exists in the database.");
            }
        }

        /// <summary>
        /// Get the number of vehicles tagged with a given tag. Assumes the tag exists.
        /// </summary>
        /// <param name="tagHandle">Tag handle.</param>
        /// <returns>Number of vehicles with the given tag.</returns>
        internal static int VehiclesTagged(Guid tagHandle)
        {
            using (IDbConnection connection = Database.Connection("Vehicle"))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    const string commandText =
                        "SELECT COUNT(*) AS TaggedCount " +
                        "FROM   Client.Tag T " +
                        "JOIN   Client.Vehicle_Tag VT ON VT.TagID = T.TagID " +
                        "JOIN   Audit.Association A1 ON A1.AssociationID = VT.AssociationID " +
                        "JOIN   Audit.Association_History A2 ON A2.AssociationID = A1.AssociationID " +
                        "JOIN   Audit.AuditRow R ON R.AuditRowID = A2.AuditRowID " +
                        "JOIN   Client.Client C ON C.ClientID = T.ClientID " +
                        "WHERE  T.Handle = @Handle " +
                        "AND    GETDATE() BETWEEN R.ValidFrom AND R.ValidUpTo " +
                        "AND    R.WasDelete = 0";

                    command.CommandType = CommandType.Text;
                    command.CommandText = commandText;
                    Database.AddWithValue(command, "Handle", tagHandle, DbType.Guid);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return reader.GetInt32(reader.GetOrdinal("TaggedCount"));
                        }
                    }
                    throw new DataException("Couldn't verify tag was deleted.");
                }
            }
        }

        #endregion

        #region Commands

        /// <summary>
        /// Helper for executing a broker tags command.
        /// </summary>
        /// <param name="identity">User identity.</param>
        /// <param name="broker">Broker handle.</param>
        /// <returns>Broker's tags.</returns>
        internal static BrokerTagsResultsDto BrokerTags(IdentityDto identity, Guid broker)
        {
            BrokerTagsArgumentsDto args = new BrokerTagsArgumentsDto
            {
                Broker = broker
            };

            IdentityContextDto<BrokerTagsArgumentsDto> cmdArgs = new IdentityContextDto<BrokerTagsArgumentsDto>
            {
                Arguments = args,
                Identity = identity
            };

            return new BrokerTagsCommand().Execute(cmdArgs);
        }

        /// <summary>
        /// Helper for executing a create tag command.
        /// </summary>
        /// <param name="identity">User identity.</param>
        /// <param name="broker">Broker handle.</param>
        /// <param name="tag">Tag.</param>
        /// <returns>Results of tag creation.</returns>
        internal static CreateTagResultsDto CreateTag(IdentityDto identity, Guid broker, string tag)
        {
            CreateTagArgumentsDto args = new CreateTagArgumentsDto
            {
                Broker = broker,
                TagName = tag
            };

            IdentityContextDto<CreateTagArgumentsDto> cmdArgs = new IdentityContextDto<CreateTagArgumentsDto>
            {
                Identity = identity,
                Arguments = args
            };

            return new CreateTagCommand().Execute(cmdArgs);
        }

        /// <summary>
        /// Helper for executing an update tag command.
        /// </summary>
        /// <param name="identity">User identity.</param>
        /// <param name="broker">Broker handle.</param>
        /// <param name="tag">Tag handle.</param>
        /// <param name="name">Tag name.</param>
        /// <returns>Results of tag updating.</returns>
        internal static UpdateTagResultsDto UpdateTag(IdentityDto identity, Guid broker, Guid tag, string name)
        {
            UpdateTagArgumentsDto args = new UpdateTagArgumentsDto
            {
                Broker = broker,
                Name = name,
                Tag = tag
            };

            IdentityContextDto<UpdateTagArgumentsDto> cmdArgs = new IdentityContextDto<UpdateTagArgumentsDto>
            {
                Arguments = args,
                Identity = identity
            };

            return new UpdateTagCommand().Execute(cmdArgs);
        }

        /// <summary>
        /// Helper for executing a delete tag command.
        /// </summary>
        /// <param name="identity">User identity.</param>
        /// <param name="broker">Broker handle.</param>
        /// <param name="tag">Tag handle.</param>
        /// <returns>Results of tag deletion.</returns>
        internal static DeleteTagResultsDto DeleteTag(IdentityDto identity, Guid broker, Guid tag)
        {
            DeleteTagArgumentsDto args = new DeleteTagArgumentsDto
            {
                Broker = broker,
                Tag = tag
            };

            IdentityContextDto<DeleteTagArgumentsDto> cmdArgs = new IdentityContextDto<DeleteTagArgumentsDto>
            {
                Identity = identity,
                Arguments = args
            };

            return new DeleteTagCommand().Execute(cmdArgs);
        }

        /// <summary>
        /// Helper for executing an apply tag command.
        /// </summary>
        /// <param name="identity">User identity.</param>
        /// <param name="broker">Broker handle.</param>
        /// <param name="vehicle">Vehicle handle.</param>
        /// <param name="tag">Tag handle.</param>        
        /// <returns>Results of tag deletion.</returns>
        internal static ApplyTagResultsDto ApplyTag(IdentityDto identity, Guid broker, Guid vehicle, Guid tag)
        {
            ApplyTagArgumentsDto args = new ApplyTagArgumentsDto
            {
                Broker = broker,
                Tag = tag,
                Vehicle = vehicle
            };

            IdentityContextDto<ApplyTagArgumentsDto> cmdArgs = new IdentityContextDto<ApplyTagArgumentsDto>
            {
                Identity = identity,
                Arguments = args
            };

            return new ApplyTagCommand().Execute(cmdArgs);
        }

        /// <summary>
        /// Helper for executing a remove tag command.
        /// </summary>
        /// <param name="identity">User identity.</param>
        /// <param name="broker">Broker handle.</param>
        /// <param name="vehicle">Vehicle handle.</param>
        /// <param name="tag">Tag handle.</param>        
        /// <returns>Results of tag deletion.</returns>
        internal static RemoveTagResultsDto RemoveTag(IdentityDto identity, Guid broker, Guid vehicle, Guid tag)
        {
            RemoveTagArgumentsDto args = new RemoveTagArgumentsDto
            {
                Broker = broker,
                Tag = tag,
                Vehicle = vehicle
            };

            IdentityContextDto<RemoveTagArgumentsDto> cmdArgs = new IdentityContextDto<RemoveTagArgumentsDto>
            {
                Identity = identity,
                Arguments = args
            };

            return new RemoveTagCommand().Execute(cmdArgs);
        }

        /// <summary>
        /// Helper for executing a vehicle tags command.
        /// </summary>
        /// <param name="identity">User identity.</param>
        /// <param name="broker">Broker handle.</param>
        /// <param name="vehicle">Vehicle handle.</param>
        /// <returns>Results of finding a vehicle's tags.</returns>
        internal static VehicleTagsResultsDto VehicleTags(IdentityDto identity, Guid broker, Guid vehicle)
        {
            VehicleTagsArgumentsDto args = new VehicleTagsArgumentsDto
            {
                Broker = broker,
                Vehicle = vehicle
            };

            IdentityContextDto<VehicleTagsArgumentsDto> cmdArgs = new IdentityContextDto<VehicleTagsArgumentsDto>
            {
                Arguments = args,
                Identity = identity
            };

            return new VehicleTagsCommand().Execute(cmdArgs);
        }

        /// <summary>
        /// Helper for executing a vehicle tags command.
        /// </summary>
        /// <param name="identity">User identity.</param>
        /// <param name="broker">Broker handle.</param>
        /// <param name="tag">Tag handle.</param>
        /// <returns>Results of finding tagged vehicles.</returns>
        internal static TaggedVehiclesResultsDto TaggedVehicles(IdentityDto identity, Guid broker, Guid tag)
        {
            TaggedVehiclesArgumentsDto args = new TaggedVehiclesArgumentsDto
            {
                Broker = broker,
                Tag = tag
            };

            IdentityContextDto<TaggedVehiclesArgumentsDto> cmdArgs = new IdentityContextDto<TaggedVehiclesArgumentsDto>
            {
                Arguments = args,
                Identity = identity
            };

            return new TaggedVehiclesCommand().Execute(cmdArgs);
        }

        #endregion

        #region Tags

        private static readonly Random _random = new Random(DateTime.Now.Millisecond);

        /// <summary>
        /// Get a random tag name.
        /// </summary>
        /// <returns>Random tag name.</returns>
        internal static string TagName()
        {
            return "Tag - " + _random.Next();
        }

        #endregion
    }
}    