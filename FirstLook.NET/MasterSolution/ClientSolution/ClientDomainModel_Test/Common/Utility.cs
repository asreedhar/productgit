﻿using FirstLook.Common.Core.Cache;
using FirstLook.Common.Core.Data;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Test.Common
{
    /// <summary>
    /// Helper utility for client domain model testing.
    /// </summary>
    internal static class Utility
    {      
        /// <summary>
        /// Register all components for the client domain model.
        /// </summary>
        internal static void RegisterEverything()
        {
            IRegistry registry = RegistryFactory.GetRegistry();

            registry.Register<Module>();            

            registry.Register<IDataSessionManager, DataSessionManager>(ImplementationScope.Shared);
            registry.Register<ICache,              MemoryCache>       (ImplementationScope.Shared);
        }

        /// <summary>
        /// Clear out the registry.
        /// </summary>
        internal static void ClearRegistry()
        {            
            RegistryFactory.GetRegistry().Dispose();
        }        
    }
}
