﻿using System;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Licenses.Commands.Impl;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;
using NUnit.Framework;

namespace FirstLook.Client.DomainModel.Test.Common.Licenses
{
    /// <summary>
    /// Helper utilities for testing the licensing domain model.
    /// </summary>
    internal static class Utility
    {
        #region Commands

        /// <summary>
        /// Helper for fetching a license.
        /// </summary>
        /// <param name="brokerHandle">Broker handle.</param>
        /// <param name="identity">User identity.</param>
        /// <param name="licenseHandle">License handle.</param>
        /// <returns>Result transfer object.</returns>
        internal static LicenseResultsDto FetchLicense(Guid brokerHandle, IdentityDto identity, Guid licenseHandle)
        {
            LicenseArgumentsDto args = new LicenseArgumentsDto
            {
                Broker = brokerHandle,
                License = licenseHandle
            };

            IdentityContextDto<LicenseArgumentsDto> cmdArgs = new IdentityContextDto<LicenseArgumentsDto>
            {
                Arguments = args,
                Identity = identity
            };

            return new LicenseCommand().Execute(cmdArgs);
        }

        /// <summary>
        /// Helper for fetch all licenses for a dealer.
        /// </summary>
        /// <param name="brokerHandle">Broker handle.</param>
        /// <param name="identity">User identity.</param>
        /// <returns>Result transfer object.</returns>
        internal static LicensesResultsDto FetchLicenses(Guid brokerHandle, IdentityDto identity)
        {
            LicensesArgumentsDto args = new LicensesArgumentsDto
            {
                Broker = brokerHandle
            };

            IdentityContextDto<LicensesArgumentsDto> cmdArgs = new IdentityContextDto<LicensesArgumentsDto>
            {
                Arguments = args,
                Identity = identity
            };

            return new LicensesCommand().Execute(cmdArgs);
        }

        /// <summary>
        /// Helper for saving a license.
        /// </summary>
        /// <param name="brokerHandle">Broker handle.</param>
        /// <param name="identity">User identity.</param>
        /// <param name="license">License to save.</param>
        /// <returns>Result transfer object.</returns>
        internal static SaveLicenseResultsDto SaveLicense(Guid brokerHandle, IdentityDto identity, LicenseDto license)
        {
            SaveLicenseArgumentsDto args = new SaveLicenseArgumentsDto
            {
                Broker = brokerHandle,
                License = license
            };

            IdentityContextDto<SaveLicenseArgumentsDto> cmdArgs = new IdentityContextDto<SaveLicenseArgumentsDto>
            {
                Arguments = args,
                Identity = identity
            };

            return new SaveLicenseCommand().Execute(cmdArgs);
        }

        /// <summary>
        /// Helper for fetching a license.
        /// </summary>
        /// <param name="brokerHandle">Broker handle.</param>
        /// <param name="identity">User identity.</param>
        /// <param name="licenseHandle">License handle.</param>        
        internal static void DeleteLicense(Guid brokerHandle, IdentityDto identity, Guid licenseHandle)
        {
            RevokeLicenseArgumentsDto args = new RevokeLicenseArgumentsDto
            {
                Broker = brokerHandle,
                License = licenseHandle
            };

            IdentityContextDto<RevokeLicenseArgumentsDto> cmdArgs = new IdentityContextDto<RevokeLicenseArgumentsDto>
            {
                Arguments = args,
                Identity = identity
            };

            new RevokeLicenseCommand().Execute(cmdArgs);
        }

        /// <summary>
        /// Helper for creating a license.
        /// </summary>
        /// <param name="brokerHandle">Broker handle.</param>
        /// <param name="identity">User identity.</param>
        /// <param name="licenseHandle">License handle.</param>
        /// <param name="user">User to create lease for.</param>
        /// <returns>Result transfer object.</returns>
        internal static CreateLeaseResultsDto CreateLease(Guid brokerHandle, IdentityDto identity, Guid licenseHandle, string user)
        {
            CreateLeaseArgumentsDto args = new CreateLeaseArgumentsDto
            {
                Broker = brokerHandle,
                License = licenseHandle,
                UserName = user
            };

            IdentityContextDto<CreateLeaseArgumentsDto> cmdArgs = new IdentityContextDto<CreateLeaseArgumentsDto>
            {
                Arguments = args,
                Identity = identity
            };

            return new CreateLeaseCommand().Execute(cmdArgs);
        }

        /// <summary>
        /// Helper for fetching a lease.
        /// </summary>
        /// <param name="brokerHandle">Broker handle.</param>
        /// <param name="identity">User identity.</param>
        /// <param name="licenseHandle">License handle.</param>
        /// <param name="user">User whose lease we're retrieving.</param>
        /// <returns>Result transfer object.</returns>
        internal static LeaseResultsDto FetchLease(Guid brokerHandle, IdentityDto identity, Guid licenseHandle, string user)
        {
            LeaseArgumentsDto args = new LeaseArgumentsDto
            {
                Broker = brokerHandle,
                License = licenseHandle,
                UserName = user
            };

            IdentityContextDto<LeaseArgumentsDto> cmdArgs = new IdentityContextDto<LeaseArgumentsDto>
            {
                Arguments = args,
                Identity = identity
            };

            return new LeaseCommand().Execute(cmdArgs);
        }

        /// <summary>
        /// Helper for fetching a license's leases.
        /// </summary>
        /// <param name="brokerHandle">Broker handle.</param>
        /// <param name="identity">User identity.</param>
        /// <param name="licenseHandle">License handle.</param>
        /// <returns>Result transfer object.</returns>
        internal static LeasesResultsDto FetchLeases(Guid brokerHandle, IdentityDto identity, Guid licenseHandle)
        {
            LeasesArgumentsDto args = new LeasesArgumentsDto
            {
                Broker = brokerHandle,
                License = licenseHandle
            };

            IdentityContextDto<LeasesArgumentsDto> cmdArgs = new IdentityContextDto<LeasesArgumentsDto>
            {
                Arguments = args,
                Identity = identity
            };

            return new LeasesCommand().Execute(cmdArgs);
        }

        /// <summary>
        /// Helper for revoking a lease.
        /// </summary>
        /// <param name="brokerHandle">Broker handle.</param>
        /// <param name="identity">User identity.</param>
        /// <param name="licenseHandle">License handle.</param>
        /// <param name="user">Leased user.</param>
        /// <returns>Result transfer object.</returns>
        internal static RevokeLeaseResultsDto DeleteLease(Guid brokerHandle, IdentityDto identity, Guid licenseHandle, string user)
        {
            RevokeLeaseArgumentsDto args = new RevokeLeaseArgumentsDto
            {
                Broker = brokerHandle,
                License = licenseHandle,
                UserName = user
            };

            IdentityContextDto<RevokeLeaseArgumentsDto> cmdArgs = new IdentityContextDto<RevokeLeaseArgumentsDto>
            {
                Arguments = args,
                Identity = identity
            };

            return new RevokeLeaseCommand().Execute(cmdArgs);
        }

        /// <summary>
        /// Helper for fetching a user's bag of tokens.
        /// </summary>
        /// <param name="brokerHandle">Broker handle.</param>
        /// <param name="identity">User identity.</param>
        /// <param name="device">Optional device guid.</param>
        /// <param name="deviceType">Device type.</param>
        /// <param name="user">User.</param>
        /// <returns>Result transfer object.</returns>
        internal static TokensResultsDto Tokens(Guid brokerHandle, IdentityDto identity, Guid? device, DeviceTypeDto deviceType, string user)
        {
            TokensArgumentsDto args = new TokensArgumentsDto
            {
                Broker = brokerHandle,
                Device = device,
                DeviceType = deviceType,
                UserName = user
            };

            IdentityContextDto<TokensArgumentsDto> cmdArgs = new IdentityContextDto<TokensArgumentsDto>
            {
                Arguments = args,
                Identity = identity
            };

            return new TokensCommand().Execute(cmdArgs);
        }

        #endregion

        #region Comparitors

        /// <summary>
        /// Compare two licenses.
        /// </summary>
        /// <param name="lhs">License.</param>
        /// <param name="rhs">License.</param>
        internal static void Compare(LicenseDto lhs, LicenseDto rhs)
        {
            Assert.AreEqual(lhs.License, rhs.License);
            Assert.AreEqual(lhs.LicenseModel, rhs.LicenseModel);
            Assert.AreEqual(lhs.Product, rhs.Product);
            Assert.AreEqual(lhs.DeviceType, rhs.DeviceType);

            Assert.AreEqual(lhs.Expires, rhs.Expires);
            if (!lhs.ExpiryDate.HasValue || !rhs.ExpiryDate.HasValue)
            {
                Assert.Fail("Expiration dates are missing!");
            }
            Assert.AreEqual(lhs.ExpiryDate.Value.Date, rhs.ExpiryDate.Value.Date);
            Assert.AreEqual(lhs.ExpiryDate.Value.Hour, rhs.ExpiryDate.Value.Hour);
            Assert.AreEqual(lhs.ExpiryDate.Value.Minute, rhs.ExpiryDate.Value.Minute);
            Assert.AreEqual(lhs.ExpiryDate.Value.Second, rhs.ExpiryDate.Value.Second);

            Assert.AreEqual(lhs.UseLimit, rhs.UseLimit);
            Assert.AreEqual(lhs.AllowedUses, rhs.AllowedUses);
            Assert.AreEqual(lhs.Seats, rhs.Seats);
        }

        /// <summary>
        /// Compare general details of two licenses.
        /// </summary>
        /// <param name="lhs">License info.</param>
        /// <param name="rhs">License info.</param>
        internal static void Compare(LicenseInfoDto lhs, LicenseInfoDto rhs)
        {
            Assert.AreEqual(lhs.License, rhs.License);
            Assert.AreEqual(lhs.LicenseModel, rhs.LicenseModel);
            Assert.AreEqual(lhs.Product, rhs.Product);
            Assert.AreEqual(lhs.DeviceType, rhs.DeviceType);
        }

        #endregion
    }
}
