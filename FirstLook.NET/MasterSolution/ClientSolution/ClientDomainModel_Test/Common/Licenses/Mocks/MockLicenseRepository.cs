﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Licenses.Model;

namespace FirstLook.Client.DomainModel.Test.Common.Licenses.Mocks
{
    /// <summary>
    /// Mock of a license repository.
    /// </summary>
    public class MockLicenseRepository : ILicenseRepository
    {
        #region Test Values

        public static IList<LicenseInfo> Licenses = new List<LicenseInfo>();
        public static License License;
        public static IList<LeaseInfo> Leases = new List<LeaseInfo>();
        public static Lease Lease;
        public static Device Device;
        public static IList<Token> Tokens = new List<Token>();
        public static Token Token;
        public static IList<Product> Products = new List<Product>();
        public static Product Product;

        /// <summary>
        /// Reset all test values.
        /// </summary>
        public static void Reset()
        {
            Licenses.Clear();
            Leases.Clear();
            Tokens.Clear();
            Products.Clear();

            Licenses = null;
            Lease = null;
            Device = null;
            Token = null;
            Product = null;
        }

        #endregion

        /// <summary>
        /// Fetch a client's licenses.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <returns>List of licenses.</returns>
        public IList<LicenseInfo> Fetch_Licenses(IBroker broker)
        {
            return Licenses;
        }

        /// <summary>
        /// Fetch a particular license.
        /// </summary>        
        /// <param name="license">License handle.</param>
        /// <returns>License.</returns>
        public License Fetch_License(Guid license)
        {
            return License;
        }

        /// <summary>
        /// Save a new or updated license.
        /// </summary>        
        /// <param name="principal">Principal.</param>
        /// <param name="broker">Broker.</param>
        /// <param name="license">License to save.</param>
        /// <returns>License that was saved.</returns>
        public License Save_License(IPrincipal principal, IBroker broker, License license)
        {
            return License;
        }

        /// <summary>
        /// Delete a license and all of its associated leases.
        /// </summary>        
        /// <param name="principal">Principal.</param>
        /// <param name="license">License to delete.</param>
        /// <returns>License that was deleted.</returns>
        public License Delete_License(IPrincipal principal, Guid license)
        {
            return License;
        }

        /// <summary>
        /// Fetch all of a license's leases.
        /// </summary>        
        /// <param name="license">License handle.</param>
        /// <returns>List of leases.</returns>
        public IList<LeaseInfo> Fetch_Leases(Guid license)
        {
            return Leases;
        }

        /// <summary>
        /// Fetch a particular lease.
        /// </summary>
        /// <param name="license">License handle.</param>
        /// <param name="userId">User identifier.</param>
        /// <returns>Lease.</returns>
        public Lease Fetch_Lease(Guid license, int userId)
        {
            return Lease;
        }

        /// <summary>
        /// Create a lease for the given user on the given license.
        /// </summary>
        /// <param name="principal">Principal.</param>
        /// <param name="license">License handle.</param>
        /// <param name="userId">User identifier.</param>
        /// <returns>New lease.</returns>
        public Lease Create_Lease(IPrincipal principal, Guid license, int userId)
        {
            return Lease;
        }

        /// <summary>
        /// Delete the lease for the given user on the given license.
        /// </summary>
        /// <param name="principal">Principal.</param>
        /// <param name="license">License handle.</param>        
        /// <param name="userId">User identifier.</param>
        /// <returns>Lease that was deleted.</returns>
        public LeaseInfo Delete_Lease(IPrincipal principal, Guid license, int userId)
        {
            return Lease;
        }

        /// <summary>
        /// Mark a lease as having been used.
        /// </summary>
        /// <param name="principal">Principal.</param>
        /// <param name="license">License handle.</param>
        /// <param name="userId">User identifier.</param>
        /// <returns>Lease that was used.</returns>
        public Lease Use_Lease(IPrincipal principal, Guid license, int userId)
        {
            return Lease;
        }

        /// <summary>
        /// Fetch the device with the given handle.
        /// </summary>
        /// <param name="handle">Device handle.</param>
        /// <returns>Device identifiers.</returns>
        public Device Fetch_Device(Guid handle)
        {
            return Device;
        }

        /// <summary>
        /// Register a device of the given type.
        /// </summary>
        /// <param name="principal">Principal.</param>        
        /// <param name="type">Device type.</param>
        /// <returns>Device identifiers.</returns>
        public Device Create_Device(IPrincipal principal, DeviceType type)
        {
            return Device;
        }

        /// <summary>
        /// Fetch all of a license's tokens.
        /// </summary>        
        /// <param name="license">License.</param>
        /// <returns>List of tokens.</returns>
        public IList<Token> Fetch_Tokens(Guid license)
        {
            return Tokens;
        }

        /// <summary>
        /// Fetch a specific token.
        /// </summary>        
        /// <param name="token">Token handle.</param>
        /// <returns>Token.</returns>
        public Token Fetch_Token(Guid token)
        {
            return Token;
        }

        /// <summary>
        /// Fetch a token.
        /// </summary>        
        /// <param name="license">License handle.</param>
        /// <param name="userId">User identifier.</param>
        /// <param name="device">Device handle.</param>
        /// <returns>Token.</returns>
        public Token Fetch_Token(Guid license, int userId, Guid device)
        {
            return Token;
        }

        /// <summary>
        /// Save a token.
        /// </summary>
        /// <param name="principal">Principal.</param>
        /// <param name="token">Token to save.</param>
        /// <returns>Saved token.</returns>
        public Token Save_Token(IPrincipal principal, Token token)
        {
            return Token;
        }

        /// <summary>
        /// Delete tokens.
        /// </summary>
        /// <param name="principal">Principal.</param>
        /// <param name="userId">User identifier.</param>
        /// <param name="deviceType">Device type.</param>
        /// <param name="device">Device handle.</param>
        /// <returns>Number of deleted tokens.</returns>
        public int Delete_Tokens(IPrincipal principal, int userId, DeviceType? deviceType, Guid? device)
        {
            return 0;
        }

        /// <summary>
        /// Get all license-able products.
        /// </summary>
        /// <returns>List of products.</returns>
        public IList<Product> Fetch_Products()
        {
            return Products;
        }

        /// <summary>
        /// Fetch the product with the given name.
        /// </summary>
        /// <param name="name">Product name.</param>
        /// <returns>Product.</returns>
        public Product Fetch_Product(string name)
        {
            return Product;
        }
    }
}
