﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Common.Core.Data;

namespace FirstLook.Client.DomainModel.Test.Common.Clients
{
    /// <summary>
    /// Helper utilities for testing the client domain model.
    /// </summary>
    internal static class Utility
    {
        /// <summary>
        /// Get an identity transfer object for use in commands.
        /// </summary>
        /// <returns>Identity transfer object.</returns>
        internal static IdentityDto Identity()
        {
            return new IdentityDto
            {
                AuthorityName = "CAS",
                Name = "bfung"
            };
        }

        /// <summary>
        /// Grab the handle of a broker for a dealer from the database. Assumes at least one broker exists.
        /// </summary>
        /// <returns>Broker handle.</returns>
        internal static Guid BrokerHandle()
        {
            using (IDbConnection connection = Database.Connection("Vehicle"))
            {                
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        "SELECT Top 1 C.Handle " +
                        "FROM Client.Client C " +
                        "JOIN Client.Client_Dealer D ON D.ClientID = C.ClientID";

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return reader.GetGuid(reader.GetOrdinal("Handle"));
                        }
                    }
                }
                throw new DataException("No clients exist in the database.");
            }
        }
    }
}
