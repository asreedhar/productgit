﻿namespace FirstLook.Client.DomainModel.Vehicles.Model
{
    /// <summary>
    /// Additional relevant information that pertains to a given summarization.
    /// </summary>
    public class SummaryDatapoint
    {
        /// <summary>
        /// Name of the information relevant to a summarization.
        /// </summary>
        public string Datapoint { get; set; }

        /// <summary>
        /// Value of the datapoint.
        /// </summary>
        public string Value { get; set; }
    }
}
