﻿using System.Collections.Generic;

namespace FirstLook.Client.DomainModel.Vehicles.Model
{
    /// <summary>
    /// One result in a summarization.
    /// </summary>
    public class SummaryResult
    {
        /// <summary>
        /// Value produced by summarizing a group of vehicles.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// The number of vehicles in the group with this value.
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Additional datapoints related to the summarization.
        /// </summary>
        public List<SummaryDatapoint> Datapoints { get; set; }
    }
}
