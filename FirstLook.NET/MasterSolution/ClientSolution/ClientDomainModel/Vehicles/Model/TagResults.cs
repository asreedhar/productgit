﻿using System.Collections.Generic;
using FirstLook.Client.DomainModel.Vehicles.Repositories.Entities;

namespace FirstLook.Client.DomainModel.Vehicles.Model
{
    /// <summary>
    /// Result of fetching tags.
    /// </summary>
    public class TagResults
    {
        /// <summary>
        /// Resulting tags.
        /// </summary>
        public IList<TagEntity> Tags { get; set; }

        /// <summary>
        /// The result of the operation - e.g. sucecss, failure, etc.
        /// </summary>
        public TagOperationState Result { get; set; }

        /// <summary>
        /// Initialize the resulting tags with an empty list.
        /// </summary>
        public TagResults()
        {
            Tags = new List<TagEntity>();
        }
    }
}
