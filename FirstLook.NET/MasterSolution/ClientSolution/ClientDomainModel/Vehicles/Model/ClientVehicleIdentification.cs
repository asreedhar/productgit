﻿using System;

namespace FirstLook.Client.DomainModel.Vehicles.Model
{
    /// <summary>
    /// Details of a client-vehicle association.
    /// </summary>
    [Serializable]
    public class ClientVehicleIdentification
    {
        /// <summary>
        /// Identifier of the client-vehicle association.
        /// </summary>
        public int Id { get; internal set; }

        /// <summary>
        /// Handle of the client-vehicle association.
        /// </summary>
        public Guid Handle { get; internal set; }

        /// <summary>
        /// Identifying details of the vehicle.
        /// </summary>
        public VehicleIdentification Vehicle { get; internal set; }
    }
}