﻿using System;

namespace FirstLook.Client.DomainModel.Vehicles.Model
{
    [Serializable]
    public class VehicleIdentification : IEquatable<VehicleIdentification>
    {
        public string Vin { get; internal set; }

        public int InventoryId { get; internal set; }

        public int Id { get; internal set; }

        #region IEquatable

        public bool Equals(VehicleIdentification other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.Vin, Vin);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(VehicleIdentification)) return false;
            return Equals((VehicleIdentification)obj);
        }

        public override int GetHashCode()
        {
            return Vin.GetHashCode();
        }

        public static bool operator ==(VehicleIdentification left, VehicleIdentification right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(VehicleIdentification left, VehicleIdentification right)
        {
            return !Equals(left, right);
        }

        #endregion
    }
}
