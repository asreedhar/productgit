﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Client.DomainModel.Vehicles.Repositories.Entities;

namespace FirstLook.Client.DomainModel.Vehicles.Model
{
    /// <summary>
    /// Interface for a vehicle repository.
    /// </summary>
    public interface IVehicleRepository
    {
        #region Vehicle Operations

        VehicleDescription Describe(string vin);




        // VehicleDescription Describe(int dealerId,int inventoryId);

        /// <summary>
        /// Fetch a broker's vehicles that match the given example. Actuality parameter changes the group of vehicles
        /// over which we are searching. Results will be paginated.
        /// </summary>
        /// <param name="broker">Broker.</param>                
        /// <param name="actuality">Actuality -- i.e. inventory or not inventory?</param>
        /// <param name="example">Example vehicle.</param>
        /// <param name="pagination">Paging arguments.</param>
        /// <returns>Page of matching vehicles.</returns>
        /// 
        Page<Vehicle> Fetch(IBroker broker, VehicleActuality actuality, VehicleDescription example, PageArguments pagination);

        /// <summary>
        /// Summarize a group of vehicles using the given criteria.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="columnName">Vehicle dimension we want to summarize.</param>
        /// <param name="filters">Filters to apply.</param>
        /// <param name="actuality">Actuality -- i.e. inventory or not inventory?</param>
        /// <returns>List of summary results.</returns>
        IList<SummaryResult> Summarize(IBroker broker, string columnName, IList<RowFilter> filters, VehicleActuality actuality);

        /// <summary>
        /// Get the details of the vehicle with the given vin. If the vin has not been seen before, it will be 
        /// remembered (i.e. inserted into Vehicle.Reference).
        /// </summary>
        /// <param name="vin">Vin.</param>
        /// <returns>Vehicle identification details.</returns>
        VehicleIdentification Identification(string vin);

        /// <summary>
        /// Get the details of the vehicle with the given inventoryId. If the inventoryId has not been seen before, it will be 
        /// remembered (i.e. inserted into Vehicle.Reference).
        /// </summary>
        /// <param name="inventoryId">inventoryId.</param>
        /// <returns>Vehicle identification details.</returns>
        VehicleIdentification Identification(int dealerId, int inventoryId);//inventoryId

        /// <summary>
        /// Get the identification details of an association between a broker and vehicle. If this association doesn't
        /// exist in the database, it will be created.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Vehicle details.</param>
        /// <returns>Details of the association.</returns>
        ClientVehicleIdentification Identification(IBroker broker, VehicleIdentification vehicle);

        /// <summary>
        /// Get the identification details of a client and vehicle association. It will not create the association if 
        /// it does not already exist because that we're passing down a vehicle handle necessarily means it already has
        /// been created.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Vehicle handle.</param>
        /// <returns>Details of the association.</returns>
        ClientVehicleIdentification Identification(IBroker broker, Guid vehicle);

        #endregion

        #region Tag Operations

        /// <summary>
        /// Get all of a broker's tags.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <returns>Result state and a collection of tags.</returns>
        TagResults FetchTags(IBroker broker);

        /// <summary>
        /// Get all tags applied to a broker's vehicle.
        /// </summary>
        /// <param name="vehicle">Broker's vehicle.</param>
        /// <returns>Result state and a collection of tags.</returns>
        TagResults FetchTags(ClientVehicleIdentification vehicle);

        /// <summary>
        /// Get the broker's tag with the given handle.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="tag">Tag handle.</param>
        /// <returns>Result state and a tag.</returns>
        TagResult FetchTag(IBroker broker, Guid tag);

        /// <summary>
        /// Get the broker's vehicles tagged with the given tag.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="tag">Tag.</param>
        /// <returns>Vehicles tagged with this tag.</returns>
        IList<Vehicle> FetchTaggedVehicles(IBroker broker, TagEntity tag);

        /// <summary>
        /// Get the vins of the vehicles tagged with a given tag.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="tag">Tag.</param>
        /// <returns>Vins of vehicles tagged with this tag.</returns>
        IList<string> FetchTaggedVins(IBroker broker, TagEntity tag);

        /// <summary>
        /// Save the tag for the given broker.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="principal">User performing this operation.</param>
        /// <param name="tag">Tag to save.</param>
        /// <returns>Result state and the newly created tag.</returns>
        TagResult SaveTag(IBroker broker, IPrincipal principal, Tag tag);

        /// <summary>
        /// Delete a broker's tag.
        /// </summary>
        /// <param name="broker">Broker.</param>        
        /// <param name="principal">User performing this operation.</param>
        /// <param name="tag">Tag to delete.</param>
        /// <returns>Result state.</returns>        
        TagOperationState DeleteTag(IBroker broker, IPrincipal principal, TagEntity tag);

        /// <summary>
        /// Apply a broker's tag to a vehicle.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="principal">User performing this operation.</param>
        /// <param name="vehicle">Broker's vehicle.</param>
        /// <param name="tag">Tag.</param>
        /// <returns>Result state.</returns>
        TagOperationState ApplyTag(IBroker broker, IPrincipal principal, ClientVehicleIdentification vehicle, TagEntity tag);

        /// <summary>
        /// Remove a broker's tag from a vehicle.
        /// </summary>        
        /// <param name="principal">User performing this operation.</param>
        /// <param name="vehicle">Broker's vehicle.</param>
        /// <param name="tag">Tag.</param>
        /// <returns>Result state.</returns>
        TagOperationState RemoveTag(IPrincipal principal, ClientVehicleIdentification vehicle, TagEntity tag);

        #endregion
    }
}
