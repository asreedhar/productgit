﻿using System;

namespace FirstLook.Client.DomainModel.Vehicles.Model
{
    [Serializable]
    public class Vehicle
    {
        public int Id { get; internal set; }

        public VehicleIdentification Identification { get; internal set; }

        public VehicleDescription Description { get; internal set; }

        public int Mileage { get; internal set; }
    }
}
