﻿namespace FirstLook.Client.DomainModel.Vehicles.Model
{
    /// <summary>
    /// Types of existence for a vehicle.
    /// </summary>
    public enum VehicleActuality
    {
        /// <summary>
        /// Undefined. The universe 'splodes.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// Inventory.
        /// </summary>
        Inventory = 1,

        /// <summary>
        /// Not inventory.
        /// </summary>
        NotInventory = 2
    }
}
