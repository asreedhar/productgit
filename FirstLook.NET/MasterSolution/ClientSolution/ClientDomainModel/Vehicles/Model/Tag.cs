﻿using System;
using FirstLook.Common.Core;

namespace FirstLook.Client.DomainModel.Vehicles.Model
{
    /// <summary>
    /// Tag. Essentially a means of grouping vehicles together arbitrarily.
    /// </summary>
    public class Tag : PropertyChangeSupport
    {
        private Guid _handle;
        private string _name;
        private TagType _tagType;

        /// <summary>
        /// Tag identifier.
        /// </summary>
        public Guid Handle
        {
            get { return _handle; }
            set { Assignment(()=> _handle = value, "Handle"); }
        }

        /// <summary>
        /// Name of the tag.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { Assignment(() => _name = value, "Name"); }
        }

        /// <summary>
        /// Type of tag - static or dynamic.
        /// </summary>
        public TagType TagType
        {
            get { return _tagType; }
            set { Assignment(() => _tagType = value, "TagType"); }
        }
    }
}
