﻿using System;

namespace FirstLook.Client.DomainModel.Vehicles.Model
{
    [Serializable]
    public class VehicleDescription
    {
        public VehicleDescription()
        {
            Vin = string.Empty;
            TransmissionName = string.Empty;
            DriveTrainName = string.Empty;
            EngineName = string.Empty;
            FuelTypeName = string.Empty;
            PassengerDoorName = string.Empty;
            BodyTypeName = string.Empty;
            SegmentName = string.Empty;
            SeriesName = string.Empty;
            ModelFamilyName = string.Empty;
            MakeName = string.Empty;
            ExcludeMobile = false;
        }

        public string Vin { get; set; }

        #region Model

        public string MakeName { get; set; }

        public string ModelFamilyName { get; set; }

        public string SeriesName { get; set; }

        public string SegmentName { get; set; }

        public string BodyTypeName { get; set; }

        #endregion

        #region Configuration

        public int ModelYear { get; set; }

        public string PassengerDoorName { get; set; }

        public string FuelTypeName { get; set; }

        public string EngineName { get; set; }

        public string DriveTrainName { get; set; }

        public string TransmissionName { get; set; }

        #endregion

        #region Other

        public bool ExcludeMobile { get; set; }

        #endregion
    }
}
