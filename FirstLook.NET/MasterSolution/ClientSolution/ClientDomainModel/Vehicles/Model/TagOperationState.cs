﻿namespace FirstLook.Client.DomainModel.Vehicles.Model
{
    /// <summary>
    /// Possible results of a tag operation.
    /// </summary>
    public enum TagOperationState
    {
        /// <summary>
        /// Undefined.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// The tag operation was successful.
        /// </summary>
        Success = 1,

        /// <summary>
        /// The tag operation produced no change.
        /// </summary>
        NoChange = 2,

        /// <summary>
        /// Tag in question does not exist.
        /// </summary>
        TagDoesntExist = 3,

        /// <summary>
        /// Tag in question already exists.
        /// </summary>
        TagAlreadyExists = 4,

        /// <summary>
        /// Vehicle to perform tag operation on doesn't exist.
        /// </summary>
        VehicleDoesntExist = 5,

        /// <summary>
        /// Broker to perform tag operation for doesn't exist.
        /// </summary>
        BrokerDoesntExist = 6,

        /// <summary>
        /// Vehicle is already tagged with that tag.
        /// </summary>
        VehicleAlreadyTagged = 7,

        /// <summary>
        /// Vehicle is not tagged with this tag.
        /// </summary>
        VehicleNotTagged = 8
    }
}
