﻿using FirstLook.Client.DomainModel.Vehicles.Repositories.Entities;

namespace FirstLook.Client.DomainModel.Vehicles.Model
{
    /// <summary>
    /// Result of a tag operation.
    /// </summary>
    public class TagResult
    {
        /// <summary>
        /// Tag produced by the operation.
        /// </summary>
        public TagEntity Tag { get; set; }

        /// <summary>
        /// Result of the operation.
        /// </summary>
        public TagOperationState Result { get; set; }
    }
}
