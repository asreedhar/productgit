﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Vehicles
{
    /// <summary>
    /// Module for registering vehicle components.
    /// </summary>
    public class Module : IModule
    {
        /// <summary>
        /// Register vehicle components.
        /// </summary>
        /// <param name="registry">Registry.</param>
        public void Configure(IRegistry registry)
        {
            registry.Register<Commands.Module>();

            registry.Register<Repositories.Module>();
        }
    }
}
