﻿
SELECT  T.TagID ,
        T.ClientID ,
        T.Handle
FROM    Client.Tag T
WHERE   TagID = @@IDENTITY
