﻿SELECT DISTINCT 
		Id                      = -1,
		Mileage                 = -1,
		VIN                     = RV.Vin,
        ModelYear               = CAST(CO.ModelYear AS INT),
        MakeName                = MA.Name,
        LineName                = LI.Name,
        ModelFamilyName         = MF.Name,
        SeriesName              = SR.Name,
        BodyTypeName            = BT.Name,
        SegmentName             = SG.Name,
        DriveTrainName          = DT.Name,
        EngineName              = EN.Name,
        FuelTypeName            = FT.Name,
        TransmissionName        = TR.Name,
        PassengerDoorName       = PD.Name
-- tag		
FROM    Client.Vehicle CV
JOIN    Reference.Vehicle RV ON RV.VehicleID = CV.ReferenceID
JOIN	Client.Vehicle_Tag VT ON VT.VehicleID = CV.VehicleID
JOIN	Audit.Association_History AH ON AH.AssociationID = VT.AssociationID
JOIN	Audit.AuditRow R1 ON R1.AuditRowID = AH.AuditRowID
JOIN	Client.Tag_History TH ON TH.TagID = VT.TagID
JOIN	Audit.AuditRow R2 ON R2.AuditRowID = TH.AuditRowID	
-- vin pattern		
JOIN	VehicleCatalog.Categorization.VinPattern VP ON RV.Vin LIKE VP.VinPattern
JOIN	VehicleCatalog.Categorization.VinPattern_ModelConfiguration_Lookup VPMC ON VPMC.VinPatternID = VP.VinPatternID
JOIN    VehicleCatalog.Categorization.ModelConfiguration_VehicleCatalog J ON J.ModelConfigurationID = VPMC.ModelConfigurationID
JOIN    VehicleCatalog.Categorization.ModelConfiguration MC ON J.ModelConfigurationID = MC.ModelConfigurationID
JOIN    VehicleCatalog.Categorization.Model MO ON MC.ModelID = MO.ModelID
JOIN    VehicleCatalog.Categorization.Configuration CO ON MC.ConfigurationID = CO.ConfigurationID
-- model
JOIN    VehicleCatalog.Categorization.Make MA ON MA.MakeID = MO.MakeID
JOIN    VehicleCatalog.Categorization.Line LI ON LI.LineID = MO.LineID
JOIN	VehicleCatalog.Categorization.ModelFamily MF ON MO.ModelFamilyID =MF.ModelFamilyID 
LEFT
JOIN    VehicleCatalog.Categorization.Series SR ON SR.SeriesID = MO.SeriesID
LEFT
JOIN    VehicleCatalog.Categorization.Segment SG ON SG.SegmentID = MO.SegmentID
LEFT
JOIN    VehicleCatalog.Categorization.BodyType BT ON BT.BodyTypeID = MO.BodyTypeID
-- configuration
LEFT
JOIN    VehicleCatalog.Categorization.DriveTrain DT ON DT.DriveTrainID = CO.DriveTrainID
LEFT
JOIN    VehicleCatalog.Categorization.Engine EN ON EN.EngineID = Co.EngineID
LEFT
JOIN    VehicleCatalog.Categorization.FuelType FT ON FT.FuelTypeID = Co.FuelTypeID
LEFT
JOIN    VehicleCatalog.Categorization.Transmission TR ON TR.TransmissionID = CO.TransmissionID
LEFT
JOIN    VehicleCatalog.Categorization.PassengerDoors PD ON PD.PassengerDoors = CO.PassengerDoors
WHERE   CV.ClientID = @ClientID
AND		VT.TagID = @TagID
AND		GETDATE() BETWEEN R1.ValidFrom AND R1.ValidUpTo
AND		R1.WasDelete = 0
AND		GETDATE() BETWEEN R2.ValidFrom AND R2.ValidUpTo
AND		R2.WasDelete = 0
AND		VPMC.CountryID = 1