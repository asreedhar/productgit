﻿
SELECT  V.VehicleID ,
        V.Handle ,
        V.ClientID ,
        V.ReferenceID
FROM    Client.Vehicle V
WHERE   V.VehicleID = @@IDENTITY
