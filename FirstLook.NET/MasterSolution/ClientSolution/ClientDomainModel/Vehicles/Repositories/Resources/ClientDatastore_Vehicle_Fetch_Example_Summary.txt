﻿
--DECLARE @ClientID INT
--
--SET @ClientID = 38
--
--DECLARE @ColumnName VARCHAR(128)
--
--SELECT  @ColumnName = 'ModelYear'
--
--DECLARE @VIN                    CHAR(17),
--        @ModelYear              INT,
--        @MakeName               VARCHAR(255),
--        @ModelFamilyName        VARCHAR(255),
--        @SeriesName             VARCHAR(255),
--        @SegmentName            VARCHAR(255),
--        @BodyTypeName           VARCHAR(255),
--        @EngineName             VARCHAR(255),
--        @DriveTrainName         VARCHAR(255),
--        @TransmissionName       VARCHAR(255),
--        @FuelTypeName           VARCHAR(255)

CREATE TABLE #Results (
        Id                      INT NOT NULL,
        Mileage                 INT NULL,
        VIN                     CHAR(17) NOT NULL,
        ModelYear               INT NOT NULL,
        MakeName                VARCHAR(255) NOT NULL,
        LineName                VARCHAR(255) NOT NULL,
        ModelFamilyName         VARCHAR(255) NOT NULL,
        SeriesName              VARCHAR(255) NULL,
        BodyTypeName            VARCHAR(255) NULL,
        SegmentName             VARCHAR(255) NULL,
        DriveTrainName          VARCHAR(255) NULL,
        EngineName              VARCHAR(255) NULL,
        FuelTypeName            VARCHAR(255) NULL,
        TransmissionName        VARCHAR(255) NULL,
        PassengerDoorName       VARCHAR(255) NULL
)

INSERT INTO #Results

SELECT  Id                      = V.VehicleID,
        Mileage                 = NULL,
        VIN                     = R.VIN,
        ModelYear               = CO.ModelYear,
        MakeName                = MA.Name,
        LineName                = LI.Name,
        ModelFamilyName         = MF.Name,
        SeriesName              = SR.Name,
        BodyTypeName            = BT.Name,
        SegmentName             = SG.Name,
        DriveTrainName          = DT.Name,
        EngineName              = EN.Name,
        FuelTypeName            = FT.Name,
        TransmissionName        = TR.Name,
        PassengerDoorName       = PD.Name
FROM    Reference.Vehicle R
JOIN    Client.Vehicle V ON R.VehicleID = V.ReferenceID
-- vin pattern mapping
JOIN    [VehicleCatalog].Categorization.VinPattern VP
        ON     SUBSTRING(R.VIN,1,8) + SUBSTRING(R.VIN,10,1)  = VP.SquishVin
        AND    R.VIN LIKE VP.VinPattern
JOIN    [VehicleCatalog].Categorization.VinPattern_ModelConfiguration_Lookup VPMC
        ON      VPMC.VinPatternID = VP.VinPatternID
        AND     VPMC.CountryID = 1
LEFT
JOIN    [VehicleCatalog].Firstlook.VINPatternPriority VPP
            ON      VP.VINPattern = VPP.VINPattern
            AND     VPP.CountryCode = 1
            AND     R.VIN LIKE VPP.PriorityVINPattern 
-- model configuration
JOIN    [VehicleCatalog].Categorization.ModelConfiguration MC ON VPMC.ModelConfigurationID = MC.ModelConfigurationID
JOIN    [VehicleCatalog].Categorization.Model MO ON MC.ModelID = MO.ModelID
JOIN    [VehicleCatalog].Categorization.Configuration CO ON MC.ConfigurationID = CO.ConfigurationID
-- model
JOIN    [VehicleCatalog].Categorization.Make MA ON MA.MakeID = MO.MakeID
JOIN    [VehicleCatalog].Categorization.Line LI ON LI.LineID = MO.LineID
JOIN	[VehicleCatalog].Categorization.ModelFamily MF ON MO.ModelFamilyID =MF.ModelFamilyID 
LEFT
JOIN    [VehicleCatalog].Categorization.Series SR ON SR.SeriesID = MO.SeriesID
LEFT
JOIN    [VehicleCatalog].Categorization.Segment SG ON SG.SegmentID = MO.SegmentID
LEFT
JOIN    [VehicleCatalog].Categorization.BodyType BT ON BT.BodyTypeID = MO.BodyTypeID
-- configuration
LEFT
JOIN    [VehicleCatalog].Categorization.DriveTrain DT ON DT.DriveTrainID = CO.DriveTrainID
LEFT
JOIN    [VehicleCatalog].Categorization.Engine EN ON EN.EngineID = Co.EngineID
LEFT
JOIN    [VehicleCatalog].Categorization.FuelType FT ON FT.FuelTypeID = Co.FuelTypeID
LEFT
JOIN    [VehicleCatalog].Categorization.Transmission TR ON TR.TransmissionID = CO.TransmissionID
LEFT
JOIN    [VehicleCatalog].Categorization.PassengerDoors PD ON PD.PassengerDoors = CO.PassengerDoors
WHERE   V.ClientID = @ClientID
AND     VPP.PriorityVINPattern IS NULL
-- example
AND     (@VIN IS NULL OR (R.VIN LIKE '%' + @VIN + '%'))
AND     (@ModelYear IS NULL OR (CO.ModelYear = @ModelYear))
AND     (@MakeName IS NULL OR (MA.Name LIKE '%' + @MakeName + '%'))
AND     (@ModelFamilyName IS NULL OR (MF.Name LIKE '%' + @ModelFamilyName + '%'))
AND     (@SeriesName IS NULL OR (SR.Name LIKE '%' + @SeriesName + '%'))
AND     (@SegmentName IS NULL OR (SG.Name LIKE '%' + @SegmentName + '%'))
AND     (@BodyTypeName IS NULL OR (BT.Name LIKE '%' + @BodyTypeName + '%'))
AND     (@EngineName IS NULL OR (EN.Name LIKE '%' + @EngineName + '%'))
AND     (@DriveTrainName IS NULL OR (DT.Name LIKE '%' + @DriveTrainName + '%'))
AND     (@TransmissionName IS NULL OR (TR.Name LIKE '%' + @TransmissionName + '%'))
AND     (@FuelTypeName IS NULL OR (FT.Name LIKE '%' + @FuelTypeName + '%'))


DECLARE @err    INT,
	@sql    NVARCHAR(4000)

SET @sql = '
SELECT	ColumnValue=CONVERT(VARCHAR,' + @ColumnName + '), ColumnCount=COUNT(*)
FROM	#Results
GROUP
BY	' + @ColumnName + '
ORDER
BY      ' + @ColumnName

EXEC @err = sp_executesql @sql

DROP TABLE #Results
