﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Client.DomainModel.Vehicles.Repositories.Datastores;
using FirstLook.Common.Core;

namespace FirstLook.Client.DomainModel.Vehicles.Repositories.Gateways
{
    /// <summary>
    /// Data gateway for inventory.
    /// </summary>
    public class InventoryGateway : GatewayBase
    {
        /// <summary>
        /// Fetch inventory that matches the user-provided example. Results will be paginated.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>          
        /// <param name="example">Example vehicle to match by.</param>
        /// <param name="pagination">Pagination arguments.</param>
        /// <returns>Page of vehicle results.</returns>
        public Page<Vehicle> Fetch(int dealerId, VehicleDescription example, PageArguments pagination)
        {
            IDealerDatastore datastore = Resolve<IDealerDatastore>();

            ISerializer<Vehicle> serializer = Resolve<ISerializer<Vehicle>>();

            using (IDataReader reader = datastore.Inventory(dealerId, example, pagination))
            {
                IList<Vehicle> items = serializer.Deserialize(reader);

                int totalRowCount = 0;
                if (reader.NextResult())
                {
                    if (reader.Read())
                    {
                        totalRowCount = reader.GetInt32(reader.GetOrdinal("TotalRowCount")); 
                    }
                }

                return new Page<Vehicle>
                {
                    Arguments = pagination,
                    Items = items,
                    TotalRowCount = totalRowCount
                };
            }
        }

        /// <summary>
        /// Fetch the summary results for the column name that match the given example.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>
        /// <param name="example">Example vehicle to match by.</param>
        /// <param name="columnName">Column name to focus summary on.</param>
        /// <returns>List of summary results.</returns>
        public IList<SummaryResult> Fetch(int dealerId, VehicleDescription example, string columnName)
        {
            IDealerDatastore datastore = Resolve<IDealerDatastore>();

            ISerializer<SummaryResult> serializer = Resolve<ISerializer<SummaryResult>>();

            using (IDataReader reader = datastore.Inventory(dealerId, example, columnName))
            {
                return serializer.Deserialize(reader);
            }
        }
    }
}
