﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Client.DomainModel.Vehicles.Repositories.Datastores;
using FirstLook.Common.Core;

namespace FirstLook.Client.DomainModel.Vehicles.Repositories.Gateways
{
    /// <summary>
    /// Data gateway for market listings.
    /// </summary>
    public class ListingGateway : GatewayBase
    {
        /// <summary>
        /// Fetch the market listings that match the user-provided example. Results will be paged.
        /// </summary>
        /// <param name="sellerId">Seller identifier.</param>                
        /// <param name="example">Example vehicle to match by.</param>
        /// <param name="pagination">Pagination arguments.</param>
        /// <returns>Page of vehicles.</returns>
        public Page<Vehicle> Fetch(int sellerId, VehicleDescription example, PageArguments pagination)
        {
            ISellerDatastore datastore = Resolve<ISellerDatastore>();

            ISerializer<Vehicle> serializer = Resolve<ISerializer<Vehicle>>();

            using (IDataReader reader = datastore.Listings(sellerId, example, pagination))
            {
                IList<Vehicle> items = serializer.Deserialize(reader);

                int totalRowCount = 0;

                if (reader.NextResult())
                {
                    totalRowCount = reader.GetInt32(reader.GetOrdinal("TotalRowCount"));
                }

                return new Page<Vehicle>
                {
                    Arguments = pagination,
                    Items = items,
                    TotalRowCount = totalRowCount
                };
            }
        }

        /// <summary>
        /// Fetch the summary results for the column name that match the given example.
        /// </summary>
        /// <param name="sellerId">Seller identifier.</param>
        /// <param name="example">Example vehicle to match by.</param>
        /// <param name="columnName">Column name to focus summary on.</param>
        /// <returns>List of summary results.</returns>
        public IList<SummaryResult> Fetch(int sellerId, VehicleDescription example, string columnName)
        {
            ISellerDatastore datastore = Resolve<ISellerDatastore>();

            ISerializer<SummaryResult> serializer = Resolve<ISerializer<SummaryResult>>();

            using (IDataReader reader = datastore.Listings(sellerId, example, columnName))
            {
                return serializer.Deserialize(reader);
            }
        }        
    }
}
