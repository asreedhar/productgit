﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Client.DomainModel.Vehicles.Repositories.Datastores;
using FirstLook.Common.Core;

namespace FirstLook.Client.DomainModel.Vehicles.Repositories.Gateways.Reference
{
    /// <summary>
    /// Data gateway for vehicles.
    /// </summary>
    public class VehicleGateway : GatewayBase
    {
        public VehicleDescription Describe(string vin)
        {
            IReferenceDatastore store = Resolve<IReferenceDatastore>();

            ISerializer<VehicleDescription> serializer = Resolve<ISerializer<VehicleDescription>>();

            using (IDataReader reader = store.VehicleDescription_Fetch(vin))
            {
                if (reader.Read())
                {
                    return serializer.Deserialize((IDataRecord)reader);
                }
            }

            return null;
        }

        public VehicleDescription Describe(int dealerId, int inventoryId)
        {
            IReferenceDatastore store = Resolve<IReferenceDatastore>();

            ISerializer<VehicleDescription> serializer = Resolve<ISerializer<VehicleDescription>>();

            using (IDataReader reader = store.InventoryDescription_Fetch(dealerId, inventoryId))
            {
                if (reader.Read())
                {
                    return serializer.Deserialize((IDataRecord)reader);
                }
            }

            return null;
        }

        /// <summary>
        /// Fetch the identifying information of the vehicle with the given vin.
        /// </summary>
        /// <param name="vin">Vin.</param>
        /// <returns>Identifying information of the vehicle.</returns>
        public VehicleIdentification Fetch(string vin)
        {
            string key = CreateCacheKey(vin);

            // If the vehicle has been cached, return that.
            VehicleIdentification value = Cache.Get(key) as VehicleIdentification;

            if (value != null)
            {
                return value;
            }

            IReferenceDatastore store = Resolve<IReferenceDatastore>();

            ISerializer<VehicleIdentification> serializer = Resolve<ISerializer<VehicleIdentification>>();

            // Read the vehicle from the database.
            using (IDataReader reader = store.Vehicle_Fetch(vin))
            {
                if (reader.Read())
                {
                    value = serializer.Deserialize((IDataRecord)reader);
                }
            }

            // If the vehicle is not already in the database, insert it.
            if (value == null)
            {
                using (IDataReader reader = store.Vehicle_Insert(vin))
                {
                    if (reader.Read())
                    {
                        value = serializer.Deserialize((IDataRecord)reader);
                    }
                }
            }

            // Cache the vehicle and return.
            if (value != null)
            {
                Remember(key, value);
                return value;
            }

            throw new ApplicationException("Could not fetch the vehicle.");
        }


        public VehicleIdentification Fetch(int dealerId, int inventoryId)
        {
            string key = CreateCacheKey(inventoryId);

            // If the vehicle has been cached, return that.
            VehicleIdentification value = Cache.Get(key) as VehicleIdentification;

            if (value != null)
            {
                return value;
            }

            IReferenceDatastore store = Resolve<IReferenceDatastore>();

            ISerializer<VehicleIdentification> serializer = Resolve<ISerializer<VehicleIdentification>>();

            // Read the vehicle from the database.
            using (IDataReader reader = store.InventoryDescription_Fetch(dealerId, inventoryId))
            {
                if (reader.Read())
                {
                    value = serializer.Deserialize((IDataRecord)reader);
                }
            }

            // If the vehicle is not already in the database, insert it.
            //if (value == null)
            //{
            //    using (IDataReader reader = store.Vehicle_Insert(inventoryId))
            //    {
            //        if (reader.Read())
            //        {
            //            value = serializer.Deserialize((IDataRecord) reader);
            //        }
            //    }
            //}

            // Cache the vehicle and return.
            if (value != null)
            {
                Remember(key, value);
                return value;
            }

            throw new ApplicationException("Could not fetch the vehicle.");
        }
    }

}