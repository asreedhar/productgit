﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Gateways;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Common.Repositories.Entities;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Client.DomainModel.Vehicles.Repositories.Datastores;
using FirstLook.Client.DomainModel.Vehicles.Repositories.Entities;

namespace FirstLook.Client.DomainModel.Vehicles.Repositories.Gateways.Client
{
    /// <summary>
    /// Tag data gateway.
    /// </summary>
    public class TagGateway : AssociationGateway
    {
        /// <summary>
        /// Save a tag. If the tag is not dirty or marked for deletion, return no change. If we're trying to insert or
        /// update the tag, check that a tag for that name doesn't already exist.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="tag">Tag to operate on.</param>
        /// <returns>Result of the operation i.e. tag and the result state.</returns>
        public TagResult Save(IBroker broker, Tag tag)
        {
            TagEntity entity = tag as TagEntity;

            // Make sure the tag is new or has new values.
            if (entity != null && !entity.IsDirty && !entity.IsDeleted)
            {
                return new TagResult
                {
                    Tag = entity,
                    Result = TagOperationState.NoChange
                };                
            }
            
            // Make sure the tag doesn't already exist if we're updating or inserting.
            if (entity == null || (entity.IsDirty && !entity.IsDeleted))
            {
                TagResult matchingTag = Fetch(broker, tag.Name);
                if (matchingTag.Result == TagOperationState.Success)
                {
                    return new TagResult
                    {
                        Tag = matchingTag.Tag,
                        Result = TagOperationState.TagAlreadyExists
                    };                    
                }                
            }
            
            TagEntity.Identity identity;
            AuditRow row;

            // Save the auditing values of the tag.
            Save(broker, entity, out identity, out row);

            // Save the values of the tag.
            Resolve<IClientDatastore>().Tag_History_Insert(row.Id, identity.Id, tag.Name);

            // Update the audit row of an existing tag.
            if (entity != null)
            {
                entity.OnSaved(this, new SavedEventArgs(row));
            }
            // Set the entity info of a new tag.
            else
            {
                entity = new TagEntity(
                    delegate(TagEntity self)
                        {
                            self.AuditRow = row;
                            self.Id = identity;
                            self.Name = tag.Name;
                            self.TagType = TagType.Static;
                            self.Handle = identity.Handle;                            
                        });
            }

            return new TagResult
            {
                Tag = entity,
                Result = TagOperationState.Success
            };            
        }

        /// <summary>
        /// Save a tag and return its entity and auditing info.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="entity">Tag entity to save.</param>
        /// <param name="identity">Output. Identity info of the tag.</param>
        /// <param name="row">Output. Auditing info of the tag.</param>
        private void Save(IBroker broker, TagEntity entity, out TagEntity.Identity identity, out AuditRow row)
        {
            if (entity == null)
            {
                ISerializer<TagEntity.Identity> serializer = Resolve<ISerializer<TagEntity.Identity>>();

                using (IDataReader reader = Resolve<IClientDatastore>().Tag_Insert(broker.Id))
                {
                    if (reader.Read())
                    {
                        identity = serializer.Deserialize((IDataRecord) reader);
                    }
                    else
                    {
                        throw new NotSupportedException();
                    }
                }

                row = Insert();
            }
            else
            {
                identity = entity.Id;

                if (entity.IsDeleted)
                {
                    row = Delete(entity.AuditRow);
                }
                else
                {
                    row = Update(entity.AuditRow);
                }
            }
        }

        /// <summary>
        /// Get all of a broker's tags.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <returns>Result of the operation, and a collection of tags.</returns>
        public TagResults Fetch(IBroker broker)
        {
            ISerializer<TagEntity> serializer = Resolve<ISerializer<TagEntity>>();

            TagResults result = new TagResults();

            using (IDataReader reader = Resolve<IClientDatastore>().Tags_Fetch(broker.Id))
            {
                while(reader.Read())
                {
                    result.Tags.Add(serializer.Deserialize((IDataRecord)reader));
                }
            }

            result.Result = TagOperationState.Success;
            return result;
        }

        /// <summary>
        /// Get the broker's tag with the given handle.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="tag">Tag handle.</param>
        /// <returns>Result of the operation i.e. tag, if found, and the result state.</returns>
        public TagResult Fetch(IBroker broker, Guid tag)
        {
            ISerializer<TagEntity> serializer = Resolve<ISerializer<TagEntity>>();

            using (IDataReader reader = Resolve<IClientDatastore>().Tag_Fetch(broker.Id, tag))
            {
                if (!reader.Read())
                {
                    return new TagResult
                    {
                        Tag = null,
                        Result = TagOperationState.TagDoesntExist
                    };
                }

                return new TagResult
                {
                    Tag = serializer.Deserialize((IDataRecord)reader),
                    Result = TagOperationState.Success
                };
            }
        }

        /// <summary>
        /// Get the broker's tag with the given name.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="tagName">Tag name.</param>
        /// <returns>Result of the operation i.e. tag, if found, and the result state.</returns>
        public TagResult Fetch(IBroker broker, string tagName)
        {
            ISerializer<TagEntity> serializer = Resolve<ISerializer<TagEntity>>();

            using (IDataReader reader = Resolve<IClientDatastore>().Tag_Fetch(broker.Id, tagName))
            {
                if (!reader.Read())
                {
                    return new TagResult
                    {
                        Tag = null,
                        Result = TagOperationState.TagDoesntExist
                    };
                }
                
                return new TagResult
                {
                    Tag = serializer.Deserialize((IDataRecord)reader),
                    Result = TagOperationState.Success
                };
            }
        }                

        /// <summary>
        /// Associate a vehicle with a tag. Assume tag and vehicle are not already associated.
        /// </summary>        
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Details of a client's vehicle.</param>
        /// <param name="tag">Tag to apply.</param>        
        /// <returns>Result of the operation.</returns>
        public TagOperationState InsertAssociation(IBroker broker, ClientVehicleIdentification vehicle, TagEntity tag)
        {
            Association association = InsertAssociation();            
            if (association == null)
            {
                throw new ApplicationException("Could not make association.");
            }
            
            Resolve<IClientDatastore>().Vehicle_Tag_Insert(vehicle.Id, tag.Id.Id, association.Id);            
            return TagOperationState.Success;
        }

        /// <summary>
        /// Update the association between a vehicle and tag.
        /// </summary>
        /// <param name="association">Association to update.</param>
        /// <returns>Result of the operation.</returns>
        public TagOperationState UpdateAssociation(Association association)
        {
            if (association == null)
            {
                throw new ArgumentException("Tried to update a null association.");
            }

            AuditRow row = Update(association.AuditRow);
            InsertAssociation(association.Id, row.Id);

            return TagOperationState.Success;
        }

        /// <summary>
        /// Remove the given tag from a broker's vehicle.
        /// </summary>
        /// <param name="association">Association to delete.</param>
        /// <returns>Result of the operation.</returns>
        public TagOperationState DeleteAssociation(Association association)
        {                                    
            if (association == null)
            {
                throw new ArgumentException("Tried to delete a null association.");
            }

            AuditRow row = Delete(association.AuditRow);            
            InsertAssociation(association.Id, row.Id);

            return TagOperationState.Success;
        }

        /// <summary>
        /// Check if a client's vehicle is already tagged with a given tag.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Vehicle.</param>
        /// <param name="tag">Tag.</param>
        /// <returns>True if they're associated, false otherwise.</returns>
        public bool IsAssociated(IBroker broker, ClientVehicleIdentification vehicle, TagEntity tag)
        {
            using (IDataReader reader = Resolve<IClientDatastore>().Is_Vehicle_Tagged(broker.Id, vehicle.Id, tag.Id.Id))
            {
                if (reader.Read())
                {
                    return reader.GetInt32(reader.GetOrdinal("AssociationExists")) == 1;
                }                
            }
            return false;
        }

        /// <summary>
        /// Fetch the association between a vehicle and a tag, if it exists.
        /// </summary>        
        /// <param name="vehicle">Broker's vehicle.</param>
        /// <param name="tag">Tag.</param>
        /// <returns></returns>
        public Association FetchAssociation(ClientVehicleIdentification vehicle, TagEntity tag)
        {
            ISerializer<Association> serializer = Resolve<ISerializer<Association>>();

            using (IDataReader reader = Resolve<IClientDatastore>().Vehicle_Tag_Fetch(vehicle.Id, tag.Id.Id))
            {
                if (reader.Read())
                {
                    return serializer.Deserialize((IDataRecord)reader);
                }
            }
            return null;
        }

        /// <summary>
        /// Get the tags tied to a broker's vehicle.      
        /// </summary>        
        /// <param name="vehicle">Vehicle.</param>
        /// <returns>Tags and the result of the operation.</returns>
        public TagResults VehicleTags(ClientVehicleIdentification vehicle)
        {
            IClientDatastore clientDatastore = Resolve<IClientDatastore>();
            ISerializer<TagEntity> serializer = Resolve<ISerializer<TagEntity>>();

            TagResults results = new TagResults();

            using (IDataReader reader = clientDatastore.Vehicle_Tags_Fetch(vehicle.Id))
            {
                while (reader.Read())
                {
                    results.Tags.Add(serializer.Deserialize((IDataRecord)reader));
                }
            }
            results.Result = TagOperationState.Success;
            return results;
        }        

        /// <summary>
        /// Fetch the vins of the vehicles tied to the given broker that are tagged with the given tag.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="tag">Tag.</param>
        /// <returns>List of vin strings.</returns>
        public IList<string> TaggedVins(IBroker broker, TagEntity tag)
        {
            IClientDatastore clientDatastore = Resolve<IClientDatastore>();

            IList<string> vins = new List<string>();

            using (IDataReader reader = clientDatastore.Tagged_Vins_Fetch(broker.Id, tag.Id.Id))
            {
                while (reader.Read())
                {
                    vins.Add(reader.GetString(reader.GetOrdinal("VIN")));
                }
            }
            return vins;
        }

        /// <summary>
        /// Fetch the vehicles tied to the given broker that are tagged with the given tag.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="tag">Tag.</param>
        /// <returns>List of vehicles.</returns>
        public IList<Vehicle> TaggedVehicles(IBroker broker, TagEntity tag)
        {
            ISerializer<Vehicle> serializer = Resolve<ISerializer<Vehicle>>();

            IClientDatastore clientDatastore = Resolve<IClientDatastore>();

            IList<Vehicle> vehicles = new List<Vehicle>();

            using (IDataReader reader = clientDatastore.Tagged_Vehicles_Fetch(broker.Id, tag.Id.Id))
            {
                while (reader.Read())
                {
                    vehicles.Add(serializer.Deserialize((IDataRecord)reader));
                }
            }
            return vehicles;
        }
    }
}