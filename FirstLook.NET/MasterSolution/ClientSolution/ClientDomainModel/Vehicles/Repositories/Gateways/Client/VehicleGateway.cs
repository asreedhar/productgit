﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Client.DomainModel.Vehicles.Repositories.Datastores;
using FirstLook.Common.Core;

namespace FirstLook.Client.DomainModel.Vehicles.Repositories.Gateways.Client
{
    /// <summary>
    /// Vehicle data gateway.
    /// </summary>
    public class VehicleGateway : GatewayBase
    {
        /// <summary>
        /// Get the details of the association between a broker and a vehicle. If the association does not exist, it 
        /// will be created in the database.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Vehicle.</param>
        /// <returns>Details of the association.</returns>
        public ClientVehicleIdentification Fetch(IBroker broker, VehicleIdentification vehicle)
        {
            string key = CreateCacheKey(broker.Id, vehicle.Vin);

            // If the association has been cached, return that.
            ClientVehicleIdentification value = Cache.Get(key) as ClientVehicleIdentification;

            if (value != null)
            {
                return value;
            }

            IClientDatastore store = Resolve<IClientDatastore>();

            ISerializer<ClientVehicleIdentification> serializer = Resolve<ISerializer<ClientVehicleIdentification>>();

            // Read the association from the database.
            using (IDataReader reader = store.Vehicle_Fetch(broker.Id, vehicle.Vin))
            {
                if (reader.Read())
                {
                    value = serializer.Deserialize((IDataRecord)reader);
                }
            }

            // If the association wasn't in the database, create it.
            if (value == null)
            {
                using (IDataReader reader = store.Vehicle_Insert(broker.Id, vehicle.Id))
                {
                    if (reader.Read())
                    {
                        value = serializer.Deserialize((IDataRecord)reader);
                    }
                }
            }

            // Cache the association and return.
            if (value != null)
            {
                value.Vehicle.Vin = vehicle.Vin;

                Remember(key, value);
                return value;
            }

            throw new ApplicationException("Could not fetch client vehicle association.");
        }

        /// <summary>
        /// Get the details of the association between a broker and a vehicle. Unlike the other variant of this method,
        /// we will not be inserting anything if the association does not already exist. This is because we are passing
        /// in a vehicle handle which necessarily means the association has already been made.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Vehicle handle.</param>
        /// <returns>Details of the association.</returns>
        public ClientVehicleIdentification Fetch(IBroker broker, Guid vehicle)
        {
            IClientDatastore store = Resolve<IClientDatastore>();

            ISerializer<ClientVehicleIdentification> serializer = Resolve<ISerializer<ClientVehicleIdentification>>();

            using (IDataReader reader = store.Vehicle_Fetch(broker.Id, vehicle))
            {
                if (reader.Read())
                {
                    return serializer.Deserialize((IDataRecord)reader);
                }
            }
            return null;
        }

        public Page<Vehicle> Fetch(IBroker broker, VehicleDescription example, PageArguments pagination)
        {
            IClientDatastore datastore = Resolve<IClientDatastore>();

            ISerializer<Vehicle> serializer = Resolve<ISerializer<Vehicle>>();

            using (IDataReader reader = datastore.Vehicle_Fetch(broker.Id, example, pagination))
            {
                IList<Vehicle> items = serializer.Deserialize(reader);

                int totalRowCount = 0;

                if (reader.NextResult())
                {
                    if (reader.Read())
                    {
                        totalRowCount = reader.GetInt32(reader.GetOrdinal("TotalRowCount"));
                    }
                }

                return new Page<Vehicle>
                {
                    Arguments = pagination,
                    Items = items,
                    TotalRowCount = totalRowCount
                };
            }
        }

        public IList<SummaryResult> Fetch(IBroker broker, VehicleDescription example, string columnName)
        {
            IClientDatastore datastore = Resolve<IClientDatastore>();

            ISerializer<SummaryResult> serializer = Resolve<ISerializer<SummaryResult>>();

            using (IDataReader reader = datastore.Vehicle_Summary_Fetch(broker.Id, example, columnName))
            {
                return serializer.Deserialize(reader);
            }
        }
    }
}