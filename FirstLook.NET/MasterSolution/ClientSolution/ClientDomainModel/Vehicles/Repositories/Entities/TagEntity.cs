﻿using System;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Common.Repositories.Entities;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Component;

namespace FirstLook.Client.DomainModel.Vehicles.Repositories.Entities
{
    public class TagEntity : Tag, ISavable
    {
        public class Identity
        {
            public int Id { get; internal set; }

            public int ClientId { get; internal set; }

            public Guid Handle { get; set; }
        }

        private readonly ReadOnlyListener _readOnly = new ReadOnlyListener("Handle", "TagType");

        private readonly SavableState _savable = new SavableState();

        /// <summary>
        /// Serialization requires an empty constructor.
        /// </summary>
        public TagEntity()
        {            
        }

        public TagEntity(Initializer<TagEntity> initialize)
        {
            initialize(this);

            base.PropertyChanging += _readOnly.OnPropertyChanging;

            base.PropertyChanged += _savable.OnPropertyChanged;
        }

        public AuditRow AuditRow { get; internal set; }

        public Identity Id { get; internal set; }        

        #region ISavable Members

        public bool IsNew
        {
            get { return _savable.IsNew; }
        }

        public bool IsDeleted
        {
            get { return _savable.IsDeleted; }
        }

        public bool IsDirty
        {
            get { return _savable.IsDirty; }
        }

        public void MarkDeleted()
        {
            _savable.MarkDeleted();
        }

        void IPersisted.MarkNew()
        {
            _savable.MarkNew();
        }

        void IPersisted.MarkOld()
        {
            _savable.MarkOld();
        }

        public void OnSaved(object sender, SavedEventArgs e)
        {
            AuditRow = e.AuditRow;
        }

        #endregion
    }
}