﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Client.DomainModel.Vehicles.Repositories.Entities;
using FirstLook.Client.DomainModel.Vehicles.Repositories.Gateways;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;

namespace FirstLook.Client.DomainModel.Vehicles.Repositories
{
    /// <summary>
    /// Vehicle repository.
    /// </summary>
    public class VehicleRepository : RepositoryBase, IVehicleRepository
    {
        #region Vehicle Operations

        public VehicleDescription Describe(string vin)
        {
            return DoInSession(() => new Gateways.Reference.VehicleGateway().Describe(vin));
        }

        /// <summary>
        /// Fetch a broker's vehicles that match the given example. Actuality parameter changes the group of vehicles
        /// over which we are searching. Results will be paginated.
        /// </summary>
        /// <param name="broker">Broker.</param>                
        /// <param name="actuality">Actuality -- i.e. inventory or not inventory?</param>
        /// <param name="example">Example vehicle.</param>
        /// <param name="pagination">Paging arguments.</param>
        /// <returns>Page of matching vehicles.</returns>
        public Page<Vehicle> Fetch(IBroker broker, VehicleActuality actuality, VehicleDescription example, PageArguments pagination)
        {
            return DoInSession(() =>
            {
                if (actuality == VehicleActuality.Inventory)
                {
                    if (broker.Integration.IsDealer)
                    {
                        return new InventoryGateway().Fetch(broker.Client.Id, example, pagination);
                    }

                    if (broker.Integration.IsSeller)
                    {
                        return new ListingGateway().Fetch(broker.Client.Id, example, pagination);
                    }
                }
                else if (actuality == VehicleActuality.NotInventory)
                {
                    return new Gateways.Client.VehicleGateway().Fetch(broker, example, pagination);
                }

                throw new NotSupportedException();
            });
        }

        /// <summary>
        /// Summarize a group of vehicles using the given criteria.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="columnName">Vehicle dimension we want to summarize.</param>
        /// <param name="filters">Filters to apply.</param>
        /// <param name="actuality">Actuality -- i.e. inventory or not inventory?</param>
        /// <returns>List of summary results.</returns>
        public IList<SummaryResult> Summarize(IBroker broker, string columnName, IList<RowFilter> filters, VehicleActuality actuality)
        {
            VehicleDescription example = new VehicleDescription();

            foreach (RowFilter filter in filters)
            {
                string value = filter.Value;

                switch (filter.ColumnName)
                {
                    case "Vin":
                        example.Vin = value;
                        break;
                    case "ModelYear":
                        int i; int.TryParse(value, out i);
                        example.ModelYear = i;
                        break;
                    case "TransmissionName":
                        example.TransmissionName = value;
                        break;
                    case "DriveTrainName":
                        example.DriveTrainName = value;
                        break;
                    case "EngineName":
                        example.EngineName = value;
                        break;
                    case "FuelTypeName":
                        example.FuelTypeName = value;
                        break;
                    case "PassengerDoorName":
                        example.PassengerDoorName = value;
                        break;
                    case "BodyTypeName":
                        example.BodyTypeName = value;
                        break;
                    case "SegmentName":
                        example.SegmentName = value;
                        break;
                    case "SeriesName":
                        example.SeriesName = value;
                        break;
                    case "ModelFamilyName":
                        example.ModelFamilyName = value;
                        break;
                    case "MakeName":
                        example.MakeName = value;
                        break;
                }
            }

            if (actuality == VehicleActuality.Inventory)
            {
                if (broker.Integration.IsDealer)
                {
                    return new InventoryGateway().Fetch(broker.Client.Id, example, columnName);
                }

                if (broker.Integration.IsSeller)
                {
                    return new ListingGateway().Fetch(broker.Client.Id, example, columnName);
                }
            }
            else if (actuality == VehicleActuality.NotInventory)
            {
                return new Gateways.Client.VehicleGateway().Fetch(broker, example, columnName);
            }

            throw new NotSupportedException();
        }

        /// <summary>
        /// Get the details of the vehicle with the given vin. If the vin has not been seen before, it will be 
        /// remembered (i.e. inserted into Vehicle.Reference).
        /// </summary>
        /// <param name="vin">Vin.</param>
        /// <returns>Vehicle identification details.</returns>
        public VehicleIdentification Identification(string vin)
        {
            return DoInTransaction(() => new Gateways.Reference.VehicleGateway().Fetch(vin));
        }

        /// <summary>
        /// Get the identification details of an association between a broker and vehicle. If this association doesn't
        /// exist in the database, it will be created.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Vehicle details.</param>
        /// <returns>Details of the association.</returns>
        public ClientVehicleIdentification Identification(IBroker broker, VehicleIdentification vehicle)
        {
            return DoInTransaction(() => new Gateways.Client.VehicleGateway().Fetch(broker, vehicle));
        }

        /// <summary>
        /// Get the identification details of a client and vehicle association. It will not create the association if 
        /// it does not already exist because that we're passing down a vehicle handle necessarily means it already has
        /// been created.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="vehicle">Vehicle handle.</param>
        /// <returns>Details of the association.</returns>
        public ClientVehicleIdentification Identification(IBroker broker, Guid vehicle)
        {
            return DoInSession(() => new Gateways.Client.VehicleGateway().Fetch(broker, vehicle));
        }

        #endregion

        #region Tag Operations

        /// <summary>
        /// Get all of a broker's tags.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <returns>Result state and a collection of tags.</returns>
        public TagResults FetchTags(IBroker broker)
        {
            return DoInSession(() => new Gateways.Client.TagGateway().Fetch(broker));
        }

        /// <summary>
        /// Get all tags applied to a broker's vehicle.
        /// </summary>
        /// <param name="vehicle">Broker's vehicle.</param>
        /// <returns>Result state and a collection of tags.</returns>
        public TagResults FetchTags(ClientVehicleIdentification vehicle)
        {
            return DoInSession((() => new Gateways.Client.TagGateway().VehicleTags(vehicle)));
        }

        /// <summary>
        /// Get the broker's tag with the given handle.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="tag">Tag handle.</param>
        /// <returns>Result state and a tag.</returns>
        public TagResult FetchTag(IBroker broker, Guid tag)
        {
            return DoInSession(() => new Gateways.Client.TagGateway().Fetch(broker, tag));
        }

        /// <summary>
        /// Get the broker's vehicles tagged with the given tag.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="tag">Tag.</param>
        /// <returns>Vehicles tagged with this tag.</returns>
        public IList<Vehicle> FetchTaggedVehicles(IBroker broker, TagEntity tag)
        {
            return DoInSession(() => new Gateways.Client.TagGateway().TaggedVehicles(broker, tag));
        }

        /// <summary>
        /// Get the vins of the vehicles tagged with a given tag.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="tag">Tag.</param>
        /// <returns>Vins of vehicles tagged with this tag.</returns>
        public IList<string> FetchTaggedVins(IBroker broker, TagEntity tag)
        {
            return DoInSession(() => new Gateways.Client.TagGateway().TaggedVins(broker, tag));
        }

        /// <summary>
        /// Save the tag for the given broker.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="principal">User performing this operation.</param>
        /// <param name="tag">Tag to save.</param>
        /// <returns>Result state and the newly created tag.</returns>
        public TagResult SaveTag(IBroker broker, IPrincipal principal, Tag tag)
        {
            return DoInTransaction
            (
                delegate(IDataSession session)
                {
                    if (session.Items["Audit"] as Audit == null)
                    {
                        session.Items["Audit"] = Resolve<IRepository>().Create(principal);
                    }
                },
                () => new Gateways.Client.TagGateway().Save(broker, tag)
            );
        }

        /// <summary>
        /// Delete a broker's tag and all the vehicles associations tied to it.
        /// </summary>
        /// <param name="broker">Broker.</param>        
        /// <param name="principal">User performing this operation.</param>
        /// <param name="tag">Tag to delete.</param>
        /// <returns>Result state.</returns>
        public TagOperationState DeleteTag(IBroker broker, IPrincipal principal, TagEntity tag)
        {
            return DoInTransaction
            (
                delegate(IDataSession session)
                {
                    if (session.Items["Audit"] as Audit == null)
                    {
                        session.Items["Audit"] = Resolve<IRepository>().Create(principal);
                    }
                },
                () =>
                {
                    Gateways.Client.TagGateway tagGateway = new Gateways.Client.TagGateway();

                    // Get the associated vehicles.
                    IList<string> vins = FetchTaggedVins(broker, tag);

                    // Remove all the associations.
                    foreach (string vin in vins)
                    {
                        VehicleIdentification vehicle = new Gateways.Reference.VehicleGateway().Fetch(vin);
                        ClientVehicleIdentification clientVehicle = new Gateways.Client.VehicleGateway().Fetch(broker, vehicle);
                        Association association = FetchVehicleTagAssociation(clientVehicle, tag);

                        TagOperationState removeResult = tagGateway.DeleteAssociation(association);
                        if (removeResult != TagOperationState.Success)
                        {
                            throw new ApplicationException("Couldn't remove association.");
                        }
                    }

                    // Delete the tag.
                    tag.MarkDeleted();
                    return tagGateway.Save(broker, tag).Result;
                }
            );
        }

        /// <summary>
        /// Fetch the association of a vehicle and a tag.
        /// </summary>
        /// <param name="vehicle">Broker's vehicle.</param>
        /// <param name="tag">Tag.</param>
        /// <returns>Association.</returns>
        private Association FetchVehicleTagAssociation(ClientVehicleIdentification vehicle, TagEntity tag)
        {
            return DoInSession(() => new Gateways.Client.TagGateway().FetchAssociation(vehicle, tag));
        }

        /// <summary>
        /// Apply a broker's tag to a vehicle.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <param name="principal">User performing this operation.</param>
        /// <param name="vehicle">Broker's vehicle.</param>
        /// <param name="tag">Tag.</param>
        /// <returns>Result state.</returns>
        public TagOperationState ApplyTag(IBroker broker, IPrincipal principal, ClientVehicleIdentification vehicle, TagEntity tag)
        {
            Association association = FetchVehicleTagAssociation(vehicle, tag);

            // There already is an active association. Do nothing.
            if (association != null && !association.AuditRow.WasDelete)
            {
                return TagOperationState.VehicleAlreadyTagged;
            }

            // There was an association, but it was deleted. Update it.
            if (association != null && association.AuditRow.WasDelete)
            {
                return DoInTransaction
                (
                    delegate(IDataSession session)
                    {
                        if (session.Items["Audit"] as Audit == null)
                        {
                            session.Items["Audit"] = Resolve<IRepository>().Create(principal);
                        }
                    },
                    () => new Gateways.Client.TagGateway().UpdateAssociation(association)
                );
            }

            // Otherwise no association exists - create it.
            return DoInTransaction
            (
                delegate(IDataSession session)
                {
                    if (session.Items["Audit"] as Audit == null)
                    {
                        session.Items["Audit"] = Resolve<IRepository>().Create(principal);
                    }
                },
                () => new Gateways.Client.TagGateway().InsertAssociation(broker, vehicle, tag)
            );
        }

        /// <summary>
        /// Remove a broker's tag from a vehicle.
        /// </summary>        
        /// <param name="principal">User performing this operation.</param>
        /// <param name="vehicle">Broker's vehicle.</param>
        /// <param name="tag">Tag.</param>
        /// <returns>Result state.</returns>
        public TagOperationState RemoveTag(IPrincipal principal, ClientVehicleIdentification vehicle, TagEntity tag)
        {
            Association association = FetchVehicleTagAssociation(vehicle, tag);

            // There is no association or it's already been deleted. Do nothing.
            if (association == null || association.AuditRow.WasDelete)
            {
                return TagOperationState.VehicleNotTagged;
            }

            // Otherwise there is an active association. Delete it.
            return DoInTransaction
            (
                delegate(IDataSession session)
                {
                    if (session.Items["Audit"] as Audit == null)
                    {
                        session.Items["Audit"] = Resolve<IRepository>().Create(principal);
                    }
                },
                () => new Gateways.Client.TagGateway().DeleteAssociation(association)
            );
        }

        #endregion

        #region Miscellaneous

        /// <summary>
        /// The vehicle database is "Vehicle".
        /// </summary>
        protected override string DatabaseName
        {
            get { return "Vehicle"; }
        }

        /// <summary>
        /// Do no auditing at the start of a transaction.
        /// </summary>
        /// <param name="session">Session.</param>
        protected override void OnBeginTransaction(IDataSession session)
        {
            // Do nothing.
        }

        #endregion

        #region IVehicleRepository Members


        public VehicleIdentification Identification(int dealerId, int inventoryId)
        {
            return DoInTransaction(() => new Gateways.Reference.VehicleGateway().Fetch(dealerId, inventoryId));
        }

        #endregion
    }
}
