﻿using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Vehicles.Repositories
{
    /// <summary>
    /// Module for registering repository components.
    /// </summary>
    public class Module : IModule
    {
        /// <summary>
        /// Register the vehicle repository components.
        /// </summary>
        /// <param name="registry">Registry.</param>
        public void Configure(IRegistry registry)
        {
            registry.Register<Datastores.Module>();

            registry.Register<Serializers.Module>();

            registry.Register<IVehicleRepository, VehicleRepository>(ImplementationScope.Shared);
        }
    }
}
