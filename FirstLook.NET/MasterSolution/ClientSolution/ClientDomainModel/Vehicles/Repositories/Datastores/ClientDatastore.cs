﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Data;

namespace FirstLook.Client.DomainModel.Vehicles.Repositories.Datastores
{
    /// <summary>
    /// Client datastore.
    /// </summary>
    public class ClientDatastore : SessionDataStore, IClientDatastore
    {
        #region Properties

        /// <summary>
        /// Get the current assembly.
        /// </summary>
        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        /// <summary>
        /// Assembly prefix for resources.
        /// </summary>
        const string Prefix = "FirstLook.Client.DomainModel.Vehicles.Repositories.Resources";

        #endregion

        #region Vehicle Methods

        /// <summary>
        /// Get a client's vehicle that has the given vin.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="vin">Vin.</param>
        /// <returns>Data reader.</returns>
        public IDataReader Vehicle_Fetch(int clientId, string vin)
        {
            const string queryName = Prefix + ".ClientDatastore_Vehicle_Fetch_Vin.txt";

            return Query(
                new[] { "Vehicle" },
                queryName,
                new Parameter("ClientID", clientId, DbType.Int32),
                new Parameter("VIN", vin, DbType.String));
        }

        /// <summary>
        /// Get a client's vehicle that has the given handle.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="handle">Vehicle handle.</param>
        /// <returns>Data reader.</returns>
        public IDataReader Vehicle_Fetch(int clientId, Guid handle)
        {
            const string queryName = Prefix + ".ClientDatastore_Vehicle_Fetch_Handle.txt";

            return Query(
                new[] { "Vehicle" },
                queryName,
                new Parameter("ClientID", clientId, DbType.Int32),
                new Parameter("Handle", handle, DbType.Guid));
        }

        /// <summary>
        /// Associate a vehicle with a client.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="referenceId">Reference identifier of the vehicle.</param>
        /// <returns>Data reader.</returns>
        public IDataReader Vehicle_Insert(int clientId, int referenceId)
        {
            const string insertQuery = Prefix + ".ClientDatastore_Vehicle_Insert.txt";

            NonQuery(insertQuery,
                     new Parameter("ClientID", clientId, DbType.Int32),
                     new Parameter("ReferenceID", referenceId, DbType.Int32));

            const string queryName = Prefix + ".ClientDatastore_Vehicle_Fetch_Identity.txt";

            return Query(new[] { "Vehicle" }, queryName);
        }

        private static List<Parameter> Map(VehicleDescription example)
        {
            object year = (example.ModelYear == 0) ? DBNull.Value : (object)example.ModelYear;

            return new List<Parameter>
                       {
                           new Parameter("VIN",              example.Vin,               DbType.String),
                           new Parameter("ModelYear",        year,                      DbType.Int32),
                           new Parameter("MakeName",         example.MakeName,          DbType.String),
                           new Parameter("ModelFamilyName",  example.ModelFamilyName,   DbType.String),
                           new Parameter("SeriesName",       example.SeriesName,        DbType.String),
                           new Parameter("BodyTypeName",     example.BodyTypeName,      DbType.String),
                           new Parameter("SegmentName",      example.SegmentName,       DbType.String),
                           new Parameter("EngineName",       example.EngineName,        DbType.String),
                           new Parameter("DriveTrainName",   example.DriveTrainName,    DbType.String),
                           new Parameter("TransmissionName", example.TransmissionName,  DbType.String),
                           new Parameter("FuelTypeName",     example.FuelTypeName,      DbType.String)
                       };
        }

        public IDataReader Vehicle_Fetch(int clientId, VehicleDescription example, PageArguments pagination)
        {
            const string queryName = Prefix + ".ClientDatastore_Vehicle_Fetch_Example.txt";

            List<Parameter> parameters = Map(example);

            parameters.AddRange(new[]
                       {
                           new Parameter("ClientID",         clientId,                  DbType.Int32),
                           new Parameter("SortColumns",      pagination.SortExpression, DbType.String),
                           new Parameter("MaximumRows",      pagination.MaximumRows,    DbType.Int32),
                           new Parameter("StartRowIndex",    pagination.StartRowIndex,  DbType.Int32),
                           new Parameter("ExcludeMobile",    example.ExcludeMobile,     DbType.Boolean), 
                       });

            return Query(new[] { "Vehicle", "TotalRowCount" }, queryName, parameters.ToArray());
        }

        public IDataReader Vehicle_Summary_Fetch(int clientId, VehicleDescription example, string columnName)
        {
            const string queryName = Prefix + ".ClientDatastore_Vehicle_Fetch_Example_Summary.txt";

            List<Parameter> parameters = Map(example);

            parameters.AddRange(new[]
                                    {
                                        new Parameter("ClientID", clientId, DbType.Int32),
                                        new Parameter("ColumnName", columnName, DbType.String)
                                    });

            return Query(new[] { "Summary" }, queryName, parameters.ToArray());
        }

        #endregion

        #region Tag Methods

        /// <summary>
        /// Fetch a client's tags.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <returns>Data reader.</returns>
        public IDataReader Tags_Fetch(int clientId)
        {
            const string queryName = Prefix + ".ClientDatastore_Tag_Fetch_Client.txt";

            return Query(
                new[] { "Tag" },
                queryName,
                new Parameter("ClientID", clientId, DbType.Int32));
        }

        /// <summary>
        /// Fetch the client's tag with the given handle.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="tagHandle">Tag handle.</param>
        /// <returns>Data reader.</returns>
        public IDataReader Tag_Fetch(int clientId, Guid tagHandle)
        {
            const string queryName = Prefix + ".ClientDatastore_Tag_Fetch_Handle.txt";

            return Query(
                new[] { "Tag" },
                queryName,
                new Parameter("ClientID", clientId, DbType.Int32),
                new Parameter("Handle", tagHandle, DbType.Guid));
        }

        /// <summary>
        /// Fetch the client's tag with the given name.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="tagName">Tag name.</param>
        /// <returns>Data reader.</returns>
        public IDataReader Tag_Fetch(int clientId, string tagName)
        {
            const string queryName = Prefix + ".ClientDatastore_Tag_Fetch_Name.txt";

            return Query(
                new[] { "Tag" },
                queryName,
                new Parameter("ClientID", clientId, DbType.Int32),
                new Parameter("Name", tagName, DbType.String));
        }

        /// <summary>
        /// Create a tag for a client.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <returns>Data reader.</returns>
        public IDataReader Tag_Insert(int clientId)
        {
            const string insertQuery = Prefix + ".ClientDatastore_Tag_Insert.txt";

            NonQuery(insertQuery,
                     new Parameter("ClientID", clientId, DbType.Int32));

            const string queryName = Prefix + ".ClientDatastore_Tag_Fetch_Identity.txt";

            return Query(new[] { "Tag" }, queryName);
        }

        /// <summary>
        /// Insert the value of a tag into the database.
        /// </summary>
        /// <param name="auditRowId">Audit row identifier.</param>
        /// <param name="id">Tag identifier.</param>
        /// <param name="name">Tag name.</param>
        public void Tag_History_Insert(int auditRowId, int id, string name)
        {
            const string insertQuery = Prefix + ".ClientDatastore_Tag_History_Insert.txt";

            NonQuery(insertQuery,
                     new Parameter("AuditRowID", auditRowId, DbType.Int32),
                     new Parameter("TagID", id, DbType.Int32),
                     new Parameter("Name", name, DbType.String));
        }

        /// <summary>
        /// Associate a tag with a vehicle.
        /// </summary>
        /// <param name="vehicleId">Vehicle identifier.</param>
        /// <param name="tagId">Tag identifier.</param>
        /// <param name="associationId">Identifier of the association between the two.</param>
        public void Vehicle_Tag_Insert(int vehicleId, int tagId, int associationId)
        {
            const string insertQuery = Prefix + ".ClientDatastore_Vehicle_Tag_Insert.txt";

            NonQuery(insertQuery,
                     new Parameter("VehicleID", vehicleId, DbType.Int32),
                     new Parameter("TagID", tagId, DbType.Int32),
                     new Parameter("AssociationID", associationId, DbType.Int32));
        }

        /// <summary>
        /// Fetch tags for a vehicle.
        /// </summary>
        /// <param name="vehicleId">Vehicle identifier.</param>
        /// <returns>Data reader.</returns>
        public IDataReader Vehicle_Tags_Fetch(int vehicleId)
        {
            const string queryName = Prefix + ".ClientDatastore_Vehicle_Tag_Fetch_Vehicle.txt";

            return Query(
                new[] { "Vehicle_Tag" },
                queryName,
                new Parameter("VehicleID", vehicleId, DbType.Int32));
        }

        /// <summary>
        /// Fetch the details of an association between a tag and a vehicle.
        /// </summary>
        /// <param name="vehicleId">Vehicle identifier.</param>
        /// <param name="tagId">Tag identifier.</param>
        /// <returns>Data reader.</returns>
        public IDataReader Vehicle_Tag_Fetch(int vehicleId, int tagId)
        {
            const string queryName = Prefix + ".ClientDatastore_Vehicle_Tag_Fetch_Id.txt";

            return Query(
                new[] { "Vehicle_Tag" },
                queryName,
                new Parameter("VehicleID", vehicleId, DbType.Int32),
                new Parameter("TagID", tagId, DbType.Int32));
        }

        /// <summary>
        /// Is a client's vehicle tagged with a given tag?
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="vehicleId">Vehicle identifier.</param>
        /// <param name="tagId">Tag identifier.</param>
        /// <returns>Data reader.</returns>
        public IDataReader Is_Vehicle_Tagged(int clientId, int vehicleId, int tagId)
        {
            const string queryName = Prefix + ".ClientDatastore_Vehicle_Tag_Exists.txt";

            return Query(
                new[] { "Vehicle_Tag" },
                queryName,
                new Parameter("ClientID", clientId, DbType.Int32),
                new Parameter("VehicleID", vehicleId, DbType.Int32),
                new Parameter("TagID", tagId, DbType.Int32)
            );
        }

        /// <summary>
        /// Get the vins of the broker's vehicles tagged with the given tag.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="tagId">Tag identifier.</param>
        /// <returns>Data reader.</returns>
        public IDataReader Tagged_Vins_Fetch(int clientId, int tagId)
        {
            const string queryName = Prefix + ".ClientDatastore_Vehicle_Fetch_Tagged_Vin.txt";

            return Query(
                new[] { "Vehicle" },
                queryName,
                new Parameter("ClientID", clientId, DbType.Int32),                
                new Parameter("TagID", tagId, DbType.Int32)
            );
        }

        /// <summary>
        /// Get the broker's vehicles tagged with the given tag.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="tagId">Tag identifier.</param>
        /// <returns>Data reader.</returns>
        public IDataReader Tagged_Vehicles_Fetch(int clientId, int tagId)
        {
            const string queryName = Prefix + ".ClientDatastore_Vehicle_Fetch_Tagged_Vehicle.txt";

            return Query(
                new[] { "Vehicle" },
                queryName,
                new Parameter("ClientID", clientId, DbType.Int32),
                new Parameter("TagID", tagId, DbType.Int32)
            );
        }

        #endregion
    }
}
