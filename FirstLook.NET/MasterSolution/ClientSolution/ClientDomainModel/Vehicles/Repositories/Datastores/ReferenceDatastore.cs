﻿using System.Data;
using System.Reflection;
using FirstLook.Common.Core.Data;
using System.Web;

namespace FirstLook.Client.DomainModel.Vehicles.Repositories.Datastores
{
    /// <summary>
    /// Interface for a datastore for reference data.
    /// </summary>
    public class ReferenceDatastore : SessionDataStore, IReferenceDatastore
    {
        #region Properties

        /// <summary>
        /// The current assembly.
        /// </summary>
        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        /// <summary>
        /// Assembly prefix for resources.
        /// </summary>
        private const string Prefix = "FirstLook.Client.DomainModel.Vehicles.Repositories.Resources";

        #endregion

        public IDataReader InventoryDescription_Fetch(int dealerId, int inventoryId)
        {

            using (IDbConnection connection = Database.Connection("IMT"))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandText = "dbo.GetInventoryForMobile";

                    Database.AddWithValue(command, "BusinessUnitId", dealerId, DbType.Int32);
                    Database.AddWithValue(command, "InventoryId", inventoryId, DbType.Int32);


                    var set = new DataSet();

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        var table = new DataTable("Inventories");

                        table.Load(reader);

                        set.Tables.Add(table);
                    }

                    return set.CreateDataReader();
                }
            }

        }
        /// <summary>
        /// Fetch the vehicle with the given vin.
        /// </summary>
        /// <param name="vin">Vin.</param>
        /// <returns>Data reader.</returns>

        public IDataReader VehicleDescription_Fetch(string vin)
        {
            const string fetch = Prefix + ".ReferenceDatastore_Vehicle_Fetch_Vin_Pattern.txt";

            Parameter parameter = new Parameter("Vin", vin, DbType.String);

            return Query(new[] { "Vehicle" }, fetch, parameter);
        }
        /// <summary>
        /// Fetch the vehicle with the given vin.
        /// </summary>
        /// <param name="vin">Vin.</param>
        /// <returns>Data reader.</returns>
        /// 
        public IDataReader Vehicle_Fetch(string vin)
        {
            const string fetch = Prefix + ".ReferenceDatastore_Vehicle_Fetch_Vin.txt";

            Parameter parameter = new Parameter("VIN", vin, DbType.String);

            return Query(new[] { "Vehicle" }, fetch, parameter);
        }

        /// <summary>
        /// Insert the vehicle with the given vin.
        /// </summary>
        /// <param name="vin">Vin.</param>
        /// <returns>Data reader.</returns>
        public IDataReader Vehicle_Insert(string vin)
        {
            const string insert = Prefix + ".ReferenceDatastore_Vehicle_Insert.txt";

            Parameter parameter = new Parameter("VIN", vin, DbType.String);

            NonQuery(insert, parameter);

            const string fetch = Prefix + ".ReferenceDatastore_Vehicle_Fetch_Identity.txt";

            return Query(new[] { "Vehicle" }, fetch);
        }


        /// <summary>
        /// Insert the vehicle with the given vin.
        /// </summary>
        /// <param name="vin">Vin.</param>
        /// <returns>Data reader.</returns>
        public IDataReader Inventory_Insert(int inventoryId)
        {
            const string insert = Prefix + ".ReferenceDatastore_Vehicle_Insert.txt";

            Parameter parameter = new Parameter("InventoryId", inventoryId, DbType.String);

            NonQuery(insert, parameter);

            const string fetch = Prefix + ".ReferenceDatastore_Vehicle_Fetch_Identity.txt";

            return Query(new[] { "Vehicle" }, fetch);
        }


    }
}
