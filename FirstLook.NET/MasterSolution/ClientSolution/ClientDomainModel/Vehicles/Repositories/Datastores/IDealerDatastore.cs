using System.Data;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Client.DomainModel.Vehicles.Model;

namespace FirstLook.Client.DomainModel.Vehicles.Repositories.Datastores
{
    /// <summary>
    /// Interface for a datastore for dealer-related vehicles.
    /// </summary>
    public interface IDealerDatastore
    {
        /// <summary>
        /// Get dealer inventory vehicles that match the given example. Results will be paginated.
        /// </summary>
        /// <param name="dealerId">Dealer identifier.</param>                
        /// <param name="example">Example vehicle.</param>
        /// <param name="pagination">Pagination arguments.</param>
        /// <returns>Data reader.</returns>
        IDataReader Inventory(int dealerId, VehicleDescription example, PageArguments pagination);

        IDataReader Inventory(int dealerId, VehicleDescription example, string columnName);
    }
}