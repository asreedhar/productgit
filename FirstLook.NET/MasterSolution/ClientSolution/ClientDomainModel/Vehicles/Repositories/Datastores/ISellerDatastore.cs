using System.Data;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Client.DomainModel.Vehicles.Model;

namespace FirstLook.Client.DomainModel.Vehicles.Repositories.Datastores
{
    /// <summary>
    /// Interface for a datastore for seller-related vehicles.
    /// </summary>
    public interface ISellerDatastore
    {
        /// <summary>
        /// Get market listings for the given dealer that matches the provided example. Results will be paginated.
        /// </summary>
        /// <param name="sellerId">Seller identifier.</param>        
        /// <param name="example">Example vehicle.</param>
        /// <param name="pagination">Pagination arguments.</param>
        /// <returns>Data reader.</returns>
        IDataReader Listings(int sellerId, VehicleDescription example, PageArguments pagination);

        IDataReader Listings(int sellerId, VehicleDescription example, string columnName);        
    }
}