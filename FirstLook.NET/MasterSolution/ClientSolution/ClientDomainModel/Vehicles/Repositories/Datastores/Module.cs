﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Vehicles.Repositories.Datastores
{
    /// <summary>
    /// Module for registering datastore components.
    /// </summary>
    public class Module : IModule
    {
        /// <summary>
        /// Register the vehicle datastore components.
        /// </summary>
        /// <param name="registry">Registry.</param>
        public void Configure(IRegistry registry)
        {
            registry.Register<IClientDatastore, ClientDatastore>(ImplementationScope.Shared);

            registry.Register<IDealerDatastore, DealerDatastore>(ImplementationScope.Shared);

            registry.Register<ISellerDatastore, SellerDatastore>(ImplementationScope.Shared);

            registry.Register<IReferenceDatastore, ReferenceDatastore>(ImplementationScope.Shared);            
        }
    }
}
