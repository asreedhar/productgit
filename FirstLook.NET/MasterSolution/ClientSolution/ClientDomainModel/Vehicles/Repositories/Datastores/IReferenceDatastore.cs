﻿using System.Data;

namespace FirstLook.Client.DomainModel.Vehicles.Repositories.Datastores
{
    /// <summary>
    /// Datastore for reference data.
    /// </summary>
    public interface IReferenceDatastore
    {
        IDataReader VehicleDescription_Fetch(string vin);

        /// <summary>
        /// Fetch the vehicle with the given vin.
        /// </summary>
        /// <param name="vin">Vin.</param>
        /// <returns>Data reader.</returns>6
        IDataReader Vehicle_Fetch(string vin);

        /// <summary>
        /// Insert the vehicle with the given vin.
        /// </summary>
        /// <param name="vin">Vin.</param>
        /// <returns>Data reader.</returns>
        IDataReader Vehicle_Insert(string vin);

        IDataReader InventoryDescription_Fetch(int dealerId, int inventoryId);

    }
}
