﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Data;

namespace FirstLook.Client.DomainModel.Vehicles.Repositories.Datastores
{
    /// <summary>
    /// Datastore for seller-related vehicles.
    /// </summary>
    public class SellerDatastore : SimpleDataStore, ISellerDatastore
    {
        #region Properties

        /// <summary>
        /// The current assembly.
        /// </summary>
        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        /// <summary>
        /// Market listings live in the "Market" database.
        /// </summary>
        protected override string DatabaseName
        {
            get { return "Market"; }
        }

        /// <summary>
        /// Assembly prefix for resources.
        /// </summary>
        internal const string Prefix = "FirstLook.Client.DomainModel.Vehicles.Repositories.Resources";

        #endregion

        private static List<Parameter> Map(VehicleDescription example)
        {
            object year = (example.ModelYear == 0) ? DBNull.Value : (object)example.ModelYear;

            return new List<Parameter>
                       {
                           new Parameter("VIN", example.Vin, DbType.String),
                           new Parameter("ModelYear", year, DbType.Int32),
                           new Parameter("MakeName", example.MakeName, DbType.String),
                           new Parameter("ModelFamilyName", example.ModelFamilyName, DbType.String),
                           new Parameter("SeriesName", example.SeriesName, DbType.String),
                           new Parameter("BodyTypeName", example.BodyTypeName, DbType.String),
                           new Parameter("SegmentName", example.SegmentName, DbType.String),
                           new Parameter("EngineName", example.EngineName, DbType.String),
                           new Parameter("DriveTrainName", example.DriveTrainName, DbType.String),
                           new Parameter("TransmissionName", example.TransmissionName, DbType.String),
                           new Parameter("FuelTypeName", example.FuelTypeName, DbType.String)
                       };
        }

        /// <summary>
        /// Get market listings for the given seller that match the provided example. Results will be paginated.
        /// </summary>
        /// <param name="sellerId">Seller identifier.</param>        
        /// <param name="example">Example vehicle.</param>
        /// <param name="pagination">Pagination arguments.</param>
        /// <returns>Data reader.</returns>
        public IDataReader Listings(int sellerId, VehicleDescription example, PageArguments pagination)
        {
            const string queryName = Prefix + ".SellerDatastore_Inventory_Fetch_Example.txt";

            List<Parameter> parameters =  Map(example);
            
            parameters.AddRange(
                new[]
                    {
                        new Parameter("SellerID", sellerId, DbType.Int32),
                        new Parameter("SortColumns", pagination.SortExpression, DbType.String),
                        new Parameter("MaximumRows", pagination.MaximumRows, DbType.Int32),
                        new Parameter("StartRowIndex", pagination.StartRowIndex, DbType.Int32)
                    });

            return Query(new[] { "Listings" }, queryName, parameters.ToArray());
        }

        public IDataReader Listings(int sellerId, VehicleDescription example, string columnName)
        {
            const string queryName = Prefix + ".SellerDatastore_Inventory_Fetch_Example_Summary.txt";

            List<Parameter> parameters = Map(example);

            parameters.AddRange(
                new[]
                    {
                        new Parameter("SellerID", sellerId, DbType.Int32),
                        new Parameter("ColumnName", columnName, DbType.String)
                    });

            return Query(new[] { "Summary" }, queryName, parameters.ToArray());
        }
    }
}
