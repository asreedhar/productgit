﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Client.DomainModel.Vehicles.Model;

namespace FirstLook.Client.DomainModel.Vehicles.Repositories.Datastores
{
    /// <summary>
    /// Interface for a client datastore.
    /// </summary>
    public interface IClientDatastore
    {
        #region Vehicle Methods

        /// <remarks>
        /// VehicleID, Handle, ClientID, ReferenceID
        /// </remarks>
        IDataReader Vehicle_Fetch(int clientId, string vin);

        /// <remarks>
        /// VehicleID, Handle, ClientID, ReferenceID
        /// </remarks>
        IDataReader Vehicle_Fetch(int clientId, Guid handle);

        /// <remarks>
        /// VehicleID, Handle, ClientID, ReferenceID
        /// </remarks>
        IDataReader Vehicle_Insert(int clientId, int referenceId);

        IDataReader Vehicle_Fetch(int clientId, VehicleDescription example, PageArguments pagination);

        IDataReader Vehicle_Summary_Fetch(int clientId, VehicleDescription example, string columnName);

        #endregion

        #region Tag Methods

        /// <summary>
        /// Fetch a client's tags.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <returns>Data reader.</returns>
        IDataReader Tags_Fetch(int clientId);

        /// <summary>
        /// Fetch the client's tag with the given handle.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="tagHandle">Tag handle.</param>
        /// <returns>Data reader.</returns>
        IDataReader Tag_Fetch(int clientId, Guid tagHandle);

        /// <summary>
        /// Fetch the client's tag with the given name.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="tagName">Tag name.</param>
        /// <returns>Data reader.</returns>
        IDataReader Tag_Fetch(int clientId, string tagName);

        /// <summary>
        /// Create a tag for a client.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <returns>Data reader.</returns>
        IDataReader Tag_Insert(int clientId);

        /// <summary>
        /// Insert the value of a tag into the database.
        /// </summary>
        /// <param name="auditRowId">Audit row identifier.</param>
        /// <param name="id">Tag identifier.</param>
        /// <param name="name">Tag name.</param>
        void Tag_History_Insert(int auditRowId, int id, string name);

        /// <summary>
        /// Associate a tag with a vehicle.
        /// </summary>
        /// <param name="vehicleId">Vehicle identifier.</param>
        /// <param name="tagId">Tag identifier.</param>
        /// <param name="associationId">Identifier of the association between the two.</param>
        void Vehicle_Tag_Insert(int vehicleId, int tagId, int associationId);

        /// <summary>
        /// Fetch tags for a vehicle.
        /// </summary>
        /// <param name="vehicleId">Vehicle identifier.</param>
        /// <returns>Data reader.</returns>
        IDataReader Vehicle_Tags_Fetch(int vehicleId);

        /// <summary>
        /// Fetch the details of an association between a tag and a vehicle.
        /// </summary>
        /// <param name="vehicleId">Vehicle identifier.</param>
        /// <param name="tagId">Tag identifier.</param>
        /// <returns>Data reader.</returns>
        IDataReader Vehicle_Tag_Fetch(int vehicleId, int tagId);

        /// <summary>
        /// Is a client's vehicle tagged with a given tag?
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="vehicleId">Vehicle identifier.</param>
        /// <param name="tagId">Tag identifier.</param>
        /// <returns>Data reader.</returns>
        IDataReader Is_Vehicle_Tagged(int clientId, int vehicleId, int tagId);

        /// <summary>
        /// Get the vins of the broker's vehicles tagged with the given tag.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="tagId">Tag identifier.</param>
        /// <returns>Data reader.</returns>
        IDataReader Tagged_Vins_Fetch(int clientId, int tagId);

        /// <summary>
        /// Get the broker's vehicles tagged with the given tag.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="tagId">Tag identifier.</param>
        /// <returns>Data reader.</returns>
        IDataReader Tagged_Vehicles_Fetch(int clientId, int tagId);

        #endregion
    }
}
