﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Vehicles.Model;

namespace FirstLook.Client.DomainModel.Vehicles.Repositories.Serializers
{
    public class ClientVehicleIdentificationSerializer : Serializer<ClientVehicleIdentification>
    {
        public override ClientVehicleIdentification Deserialize(IDataRecord record)
        {
            Guid handle = record.GetGuid(record.GetOrdinal("Handle"));

            int id = record.GetInt32(record.GetOrdinal("VehicleID"));

            int vehicleId = record.GetInt32(record.GetOrdinal("ReferenceID"));

            return new ClientVehicleIdentification
                       {
                           Handle = handle,
                           Id = id,
                           Vehicle = new VehicleIdentification
                                         {
                                             Id = vehicleId
                                         }
                       };
        }
    }
}