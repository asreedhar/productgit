﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Data;

namespace FirstLook.Client.DomainModel.Vehicles.Repositories.Serializers
{
    public class VehicleDescriptionSerializer : Serializer<VehicleDescription>
    {
        public override VehicleDescription Deserialize(IDataRecord record)
        {
            return new VehicleDescription
            {
                BodyTypeName = DataRecord.GetString(record, "BodyTypeName"),
                DriveTrainName = DataRecord.GetString(record, "DriveTrainName"),
                EngineName = DataRecord.GetString(record, "EngineName"),
                FuelTypeName = DataRecord.GetString(record, "FuelTypeName"),
                MakeName = DataRecord.GetString(record, "MakeName"),
                ModelFamilyName = DataRecord.GetString(record, "ModelFamilyName"),
                ModelYear = record.GetInt32(record.GetOrdinal("ModelYear")),
                PassengerDoorName = DataRecord.GetString(record, "PassengerDoorName"),
                SegmentName = DataRecord.GetString(record, "SegmentName"),
                SeriesName = DataRecord.GetString(record, "SeriesName")
            };
        }
    }
}
