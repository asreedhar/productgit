﻿using System.Data;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Client.DomainModel.Vehicles.Repositories.Entities;

namespace FirstLook.Client.DomainModel.Vehicles.Repositories.Serializers
{
    public class TagEntitySerializer : Serializer<TagEntity>
    {
        public override TagEntity Deserialize(IDataRecord record)
        {
            return new TagEntity
            {
                AuditRow = new AuditRow
                {
                    Id = record.GetInt32(record.GetOrdinal("AuditRowID")),
                    ValidFrom = record.GetDateTime(record.GetOrdinal("ValidFrom")),
                    ValidUpTo = record.GetDateTime(record.GetOrdinal("ValidUpTo")),
                    WasInsert = record.GetBoolean(record.GetOrdinal("WasInsert")),
                    WasUpdate = record.GetBoolean(record.GetOrdinal("WasUpdate")),
                    WasDelete = record.GetBoolean(record.GetOrdinal("WasDelete"))                    
                },
                Handle = record.GetGuid(record.GetOrdinal("Handle")),
                Id = new TagEntity.Identity
                {
                    ClientId = record.GetInt32(record.GetOrdinal("ClientID")),
                    Handle = record.GetGuid(record.GetOrdinal("Handle")),
                    Id = record.GetInt32(record.GetOrdinal("TagID"))
                },
                Name = record.GetString(record.GetOrdinal("Name")),
                TagType = TagType.Static               
            };            
        }
    }
}
