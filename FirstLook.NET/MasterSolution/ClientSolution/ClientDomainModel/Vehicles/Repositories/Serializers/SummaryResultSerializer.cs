﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Vehicles.Model;

namespace FirstLook.Client.DomainModel.Vehicles.Repositories.Serializers
{
    public class SummaryResultSerializer : Serializer<SummaryResult>
    {
        public override SummaryResult Deserialize(IDataRecord record)
        {
            int recordCount = record.GetInt32(record.GetOrdinal("ColumnCount"));

            string value = record.GetString(record.GetOrdinal("ColumnValue"));

            return new SummaryResult
            {
                Count = recordCount,
                Value = value
            };
        }
    }
}
