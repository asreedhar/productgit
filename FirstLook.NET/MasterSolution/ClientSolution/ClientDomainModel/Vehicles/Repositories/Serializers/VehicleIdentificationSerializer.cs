﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Vehicles.Model;

namespace FirstLook.Client.DomainModel.Vehicles.Repositories.Serializers
{
    /// <summary>
    /// Serializer for vehicle identification objects.
    /// </summary>
    public class VehicleIdentificationSerializer : Serializer<VehicleIdentification>
    {
        /// <summary>
        /// Deserialize the data record into a vehicle identification object.
        /// </summary>
        /// <param name="record">Data record.</param>
        /// <returns>Vehicle identification object.</returns>
        public override VehicleIdentification Deserialize(IDataRecord record)
        {
            return new VehicleIdentification
            {
                Vin = record.GetString(record.GetOrdinal("VIN")),
                Id = record.GetInt32(record.GetOrdinal("VehicleID"))
            };
        }
    }
}
