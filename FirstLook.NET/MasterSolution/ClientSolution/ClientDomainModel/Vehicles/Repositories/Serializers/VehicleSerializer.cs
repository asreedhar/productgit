﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Data;

namespace FirstLook.Client.DomainModel.Vehicles.Repositories.Serializers
{
    /// <summary>
    /// Serializer for vehicle objects.
    /// </summary>
    public class VehicleSerializer : Serializer<Vehicle>
    {
        /// <summary>
        /// Deserialize the data record into a vehicle object.
        /// </summary>
        /// <param name="record">Data record.</param>
        /// <returns>Vehicle object.</returns>
        public override Vehicle Deserialize(IDataRecord record)
        {
            VehicleDescription description = new VehicleDescriptionSerializer().Deserialize(record);

            int mileage = 0;

            if (!record.IsDBNull(record.GetOrdinal("Mileage")))
            {
                mileage = record.GetInt32(record.GetOrdinal("Mileage"));
            }

            return new Vehicle
            {
                Id = record.GetInt32(record.GetOrdinal("Id")),
                Identification = new VehicleIdentification
                {
                    Vin = DataRecord.GetString(record, "VIN"),
                },
                Mileage = mileage,
                Description = description
            };
        }
    }
}
