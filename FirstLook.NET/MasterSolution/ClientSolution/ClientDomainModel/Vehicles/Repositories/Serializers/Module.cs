﻿using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Client.DomainModel.Vehicles.Repositories.Entities;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Vehicles.Repositories.Serializers
{
    /// <summary>
    /// Module for registering serialization components.
    /// </summary>
    public class Module : IModule
    {
        /// <summary>
        /// Register the vehicle serializers.
        /// </summary>
        /// <param name="registry">Registry.</param>
        public void Configure(IRegistry registry)
        {
            registry.Register<ISerializer<Vehicle>, VehicleSerializer>(ImplementationScope.Shared);

            registry.Register<ISerializer<VehicleDescription>, VehicleDescriptionSerializer>(ImplementationScope.Shared);

            registry.Register<ISerializer<VehicleIdentification>, VehicleIdentificationSerializer>(ImplementationScope.Shared);

            registry.Register<ISerializer<ClientVehicleIdentification>, ClientVehicleIdentificationSerializer>(ImplementationScope.Shared);

            registry.Register<ISerializer<TagEntity.Identity>, TagEntityIdentitySerializer>(ImplementationScope.Shared);
            
            registry.Register<ISerializer<TagEntity>, TagEntitySerializer>(ImplementationScope.Shared);

            registry.Register<ISerializer<SummaryResult>, SummaryResultSerializer>(ImplementationScope.Shared);
        }
    }
}
