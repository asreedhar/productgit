﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Vehicles.Repositories.Entities;

namespace FirstLook.Client.DomainModel.Vehicles.Repositories.Serializers
{
    public class TagEntityIdentitySerializer : Serializer<TagEntity.Identity>
    {
        public override TagEntity.Identity Deserialize(IDataRecord record)
        {
            return new TagEntity.Identity
            {
                Id = record.GetInt32(record.GetOrdinal("TagID")),
                ClientId = record.GetInt32(record.GetOrdinal("ClientID")),
                Handle = record.GetGuid(record.GetOrdinal("Handle"))
            };
        }
    }
}
