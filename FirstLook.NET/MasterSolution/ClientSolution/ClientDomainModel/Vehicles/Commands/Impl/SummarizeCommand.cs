﻿using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.Impl
{
    /// <summary>
    /// Command to filter a group of vehicles.
    /// </summary>
    public class SummarizeCommand : ICommand<SummaryResultsDto, IdentityContextDto<SummaryArgumentsDto>>
    {
        /// <summary>
        /// Summarize a group of vehicles based on the given criteria.
        /// </summary>
        /// <param name="parameters">Broker and summary criteria.</param>
        /// <returns>Summary results.</returns>
        public SummaryResultsDto Execute(IdentityContextDto<SummaryArgumentsDto> parameters)
        {
            SummaryArgumentsDto arguments  = parameters.Arguments;
            IdentityDto         identity   = parameters.Identity;
            IBroker             broker     = Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);
            IVehicleRepository  repository = RegistryFactory.GetResolver().Resolve<IVehicleRepository>();

            // Summarize the vehicles.
            IList<SummaryResult> results = repository.Summarize(
                broker,
                arguments.GroupBy,
                Mapper.Map(arguments.Where),
                (VehicleActuality) arguments.Actuality);

            return new SummaryResultsDto
            {
                Arguments = arguments,
                Results = Mapper.Map(results)
            };
        }
    }
}
