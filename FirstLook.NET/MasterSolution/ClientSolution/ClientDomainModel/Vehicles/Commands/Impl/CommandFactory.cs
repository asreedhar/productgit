﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.Impl
{
    /// <summary>
    /// Vehicle command factory.
    /// </summary>
    public class CommandFactory : ICommandFactory
    {
        #region Vehicle Commands

        /// <summary>
        /// Create a command to query for a vehicle by example.
        /// </summary>
        /// <returns>Command to query for vehicles.</returns>
        public ICommand<QueryResultsDto, IdentityContextDto<QueryArgumentsDto>> CreateQueryCommand()
        {
            return new QueryCommand();
        }

        /// <summary>
        /// Create a command to summarize a group of vehicles.
        /// </summary>
        /// <returns>Command to summarize vehicles.</returns>
        public ICommand<SummaryResultsDto, IdentityContextDto<SummaryArgumentsDto>> CreateSummarizeCommand()
        {
            return new SummarizeCommand();
        }

        /// <summary>
        /// Create a command to identify a vehicle by its vin.
        /// </summary>
        /// <returns>Vehicle handle.</returns>
        public ICommand<IdentifyVehicleResultsDto, IdentityContextDto<IdentifyVehicleArgumentsDto>> CreateIdentifyVehicleCommand()
        {
            return new IdentifyVehicleCommand();
        }

        public ICommand<IdentifyInventoryResultsDto, IdentityContextDto<IdentifyInventoryArgumentsDto>> CreateIdentifyInventoryCommand()
        {
            return new IdentifyInventoryCommand();
        }

        #endregion

        #region Tag Commands

        /// <summary>
        /// Create a command to get a client's tags.
        /// </summary>
        /// <returns>Command to get a client's tags.</returns>
        public ICommand<BrokerTagsResultsDto, IdentityContextDto<BrokerTagsArgumentsDto>> CreateBrokerTagsCommand()
        {
            return new BrokerTagsCommand();
        }

        /// <summary>
        /// Create a command to get vehicles tagged with a given tag.
        /// </summary>
        /// <returns>Command to get tag vehicles.</returns>
        public ICommand<TaggedVehiclesResultsDto, IdentityContextDto<TaggedVehiclesArgumentsDto>> CreateTaggedVehiclesCommand()
        {
            return new TaggedVehiclesCommand();
        }

        /// <summary>
        /// Create a command to get the tags for a given vehicle.
        /// </summary>
        /// <returns>Command to get tags for a vehicle.</returns>
        public ICommand<VehicleTagsResultsDto, IdentityContextDto<VehicleTagsArgumentsDto>> CreateVehicleTagsCommand()
        {
            return new VehicleTagsCommand();
        }

        /// <summary>
        /// Create a command to create a tag.
        /// </summary>
        /// <returns>Command to create a tag.</returns>
        public ICommand<CreateTagResultsDto, IdentityContextDto<CreateTagArgumentsDto>> CreateCreateTagCommand()
        {
            return new CreateTagCommand();
        }

        /// <summary>
        /// Create a command to update a tag.
        /// </summary>
        /// <returns>Command to update a tag.</returns>
        public ICommand<UpdateTagResultsDto, IdentityContextDto<UpdateTagArgumentsDto>> CreateUpdateTagCommand()
        {
            return new UpdateTagCommand();
        }

        /// <summary>
        /// Create a command to delete a tag.
        /// </summary>
        /// <returns>Command to delete a tag.</returns>
        public ICommand<DeleteTagResultsDto, IdentityContextDto<DeleteTagArgumentsDto>> CreateDeleteTagCommand()
        {
            return new DeleteTagCommand();
        }

        /// <summary>
        /// Create a command to apply a tag to a vehicle.
        /// </summary>
        /// <returns>Command to apply a tag.</returns>
        public ICommand<ApplyTagResultsDto, IdentityContextDto<ApplyTagArgumentsDto>> CreateApplyTagCommand()
        {
            return new ApplyTagCommand();
        }

        /// <summary>
        /// Create a command to remove a tag from a vehicle.
        /// </summary>
        /// <returns>Command to remove a tag.</returns>
        public ICommand<RemoveTagResultsDto, IdentityContextDto<RemoveTagArgumentsDto>> CreateRemoveTagCommand()
        {
            return new RemoveTagCommand();
        }

        #endregion
    }
}
