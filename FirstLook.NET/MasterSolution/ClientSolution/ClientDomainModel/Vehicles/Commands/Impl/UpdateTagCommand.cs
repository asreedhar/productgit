﻿using System;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.Impl
{
    /// <summary>
    /// Command to update a broker's tag.
    /// </summary>
    public class UpdateTagCommand : ICommand<UpdateTagResultsDto, IdentityContextDto<UpdateTagArgumentsDto>>
    {
        /// <summary>
        /// Update a broker's tag, if it exists.
        /// </summary>
        /// <param name="parameters">Broker and tag identifiers, and the new name.</param>
        /// <returns>Result of the operation.</returns>
        public UpdateTagResultsDto Execute(IdentityContextDto<UpdateTagArgumentsDto> parameters)
        {
            UpdateTagArgumentsDto arguments  = parameters.Arguments;
            IdentityDto           identity   = parameters.Identity;
            IBroker               broker     = Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);
            IPrincipal            principal  = Principal.Get(identity.Name, identity.AuthorityName);
            IVehicleRepository    repository = RegistryFactory.GetResolver().Resolve<IVehicleRepository>();

            // Validate new tag name.
            if (string.IsNullOrEmpty(arguments.Name))
            {
                throw new ArgumentException("Tag name cannot be null or empty.");
            }

            // Validate the tag.
            TagResult tagResult = repository.FetchTag(broker, arguments.Tag);            
            if (tagResult.Result != TagOperationState.Success)
            {
                return new UpdateTagResultsDto
                {
                    Arguments = arguments,
                    Result = TagResultDto.TagDoesntExist
                };
            }
            
            // Update the tag name and save.
            tagResult.Tag.Name = arguments.Name;
            repository.SaveTag(broker, principal, tagResult.Tag);

            return new UpdateTagResultsDto
            {
                Arguments = arguments,
                Result = TagResultDto.Success
            };
        }
    }
}