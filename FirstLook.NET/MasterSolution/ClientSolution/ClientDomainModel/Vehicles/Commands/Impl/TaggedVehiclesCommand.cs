﻿using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.Impl
{
    /// <summary>
    /// Command to get a broker's vehicles that are tagged with a given tag.
    /// </summary>
    public class TaggedVehiclesCommand : ICommand<TaggedVehiclesResultsDto, IdentityContextDto<TaggedVehiclesArgumentsDto>>
    {
        /// <summary>
        /// Get a broker's vehicles that are tagged with a given tag.
        /// </summary>
        /// <param name="parameters">Broker and tag information.</param>
        /// <returns>List of vehicles.</returns>
        public TaggedVehiclesResultsDto Execute(IdentityContextDto<TaggedVehiclesArgumentsDto> parameters)
        {
            TaggedVehiclesArgumentsDto arguments  = parameters.Arguments;
            IdentityDto                identity   = parameters.Identity;
            IBroker                    broker     = Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);           
            IVehicleRepository         repository = RegistryFactory.GetResolver().Resolve<IVehicleRepository>();

            // Validate the tag.
            TagResult tag = repository.FetchTag(broker, arguments.Tag);
            if (tag.Result != TagOperationState.Success)
            {
                return new TaggedVehiclesResultsDto
                {
                    Arguments = arguments,
                    Result = TagResultDto.TagDoesntExist
                };
            }

            // Get the vehicles.
            IList<Vehicle> vehicles = repository.FetchTaggedVehicles(broker, tag.Tag);

            return new TaggedVehiclesResultsDto
            {
                Arguments = arguments,
                Vehicles = Mapper.Map(vehicles),
                Result = TagResultDto.Success
            };
        }
    }
}
