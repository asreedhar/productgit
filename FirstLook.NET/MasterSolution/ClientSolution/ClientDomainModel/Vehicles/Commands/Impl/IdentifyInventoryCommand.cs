﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.Impl
{

    public class IdentifyInventoryCommand : ICommand<IdentifyInventoryResultsDto, IdentityContextDto<IdentifyInventoryArgumentsDto>>
    {
        /// <summary>
        /// Get the handle of the vehicle with the given vin.
        /// </summary>
        /// <param name="parameters">Broker and vin.</param>
        /// <returns>Vehicle handle.</returns>
        public IdentifyInventoryResultsDto Execute(IdentityContextDto<IdentifyInventoryArgumentsDto> parameters)
        {
            IdentifyInventoryArgumentsDto arguments = parameters.Arguments;
            IdentityDto identity = parameters.Identity;
            IBroker broker = Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);
            IVehicleRepository repository = RegistryFactory.GetResolver().Resolve<IVehicleRepository>();

            VehicleIdentification vehicle = repository.Identification(arguments.DealerId, arguments.InventoryId);

            if (vehicle == null)
            {
                throw new ArgumentException("Could not get the vehicle.");
            }

            //ClientVehicleIdentification identification = repository.Identification(broker, vehicle);

            //if (identification == null)
            //{
            //    throw new ArgumentException("Could not find client vehicle association.");
            //}

            VehicleDescription description = repository.Describe(vehicle.Vin);

            return new IdentifyInventoryResultsDto
            {
                Arguments = arguments,
                VIN = vehicle.Vin,
                Description = Mapper.Map(description)
            };
        }
    }
}

