﻿using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.Impl
{
    /// <summary>
    /// Command to get the tags associated with a given vehicle.
    /// </summary>
    public class VehicleTagsCommand : ICommand<VehicleTagsResultsDto, IdentityContextDto<VehicleTagsArgumentsDto>>
    {
        /// <summary>
        /// Get a vehicle's tags.
        /// </summary>
        /// <param name="parameters">Broker and vehicle details.</param>
        /// <returns>List of tags, and the result of the operation.</returns>
        public VehicleTagsResultsDto Execute(IdentityContextDto<VehicleTagsArgumentsDto> parameters)
        {
            VehicleTagsArgumentsDto arguments  = parameters.Arguments;
            IdentityDto             identity   = parameters.Identity;
            IBroker                 broker     = Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);            
            IVehicleRepository      repository = RegistryFactory.GetResolver().Resolve<IVehicleRepository>();                        

            // Validate the association.
            ClientVehicleIdentification vehicle = repository.Identification(broker, arguments.Vehicle);
            if (vehicle == null)
            {
                return new VehicleTagsResultsDto
                {
                    Arguments = arguments,
                    Tags = new List<TagDto>(),
                    Result = TagResultDto.VehicleDoesntExist
                };
            }

            // Get the vehicle's tags.
            TagResults results = repository.FetchTags(vehicle);

            return new VehicleTagsResultsDto
            {
                Arguments = arguments,                
                Tags = Mapper.Map(results.Tags),
                Result = (TagResultDto)results.Result
            };
        }
    }
}
