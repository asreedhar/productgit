﻿using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.Impl
{
    /// <summary>
    /// Command to get a client's tags.
    /// </summary>
    public class BrokerTagsCommand : ICommand<BrokerTagsResultsDto, IdentityContextDto<BrokerTagsArgumentsDto>>
    {
        /// <summary>
        /// Get a broker's tags.
        /// </summary>
        /// <param name="parameters">Broker identifier.</param>
        /// <returns>Tags available to a broker.</returns>
        public BrokerTagsResultsDto Execute(IdentityContextDto<BrokerTagsArgumentsDto> parameters)
        {
            BrokerTagsArgumentsDto arguments = parameters.Arguments;
            IdentityDto            identity   = parameters.Identity;
            IBroker                broker     = Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);            
            IVehicleRepository     repository = RegistryFactory.GetResolver().Resolve<IVehicleRepository>();

            // Get the broker's tags.
            TagResults results = repository.FetchTags(broker);            

            return new BrokerTagsResultsDto
            {
               Arguments = parameters.Arguments,
               Tags = Mapper.Map(results.Tags),
               Result = (TagResultDto)results.Result
            };
        }
    }
}
