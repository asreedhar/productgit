﻿using System;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.Impl
{
    /// <summary>
    /// Command to get identify a vehicle by its vin.
    /// </summary>
    public class IdentifyVehicleCommand : ICommand<IdentifyVehicleResultsDto, IdentityContextDto<IdentifyVehicleArgumentsDto>>
    {
        /// <summary>
        /// Get the handle of the vehicle with the given vin.
        /// </summary>
        /// <param name="parameters">Broker and vin.</param>
        /// <returns>Vehicle handle.</returns>
        public IdentifyVehicleResultsDto Execute(IdentityContextDto<IdentifyVehicleArgumentsDto> parameters)
        {
            IdentifyVehicleArgumentsDto arguments = parameters.Arguments;
            IdentityDto                 identity   = parameters.Identity;
            IBroker                     broker     = Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);            
            IVehicleRepository          repository = RegistryFactory.GetResolver().Resolve<IVehicleRepository>();

            VehicleIdentification vehicle = repository.Identification(arguments.Vin);

            if (vehicle == null)
            {
                throw new ArgumentException("Could not get the vehicle.");
            }

            ClientVehicleIdentification identification = repository.Identification(broker, vehicle);

            if (identification == null)
            {
                throw new ArgumentException("Could not find client vehicle association.");
            }

            VehicleDescription description = repository.Describe(arguments.Vin);

            return new IdentifyVehicleResultsDto
            {
                Arguments = arguments,
                Vehicle = identification.Handle,
                Description = Mapper.Map(description)
            };
        }
    }
}