﻿using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.Impl
{
    /// <summary>
    /// Command to query for vehicles that match the given example.
    /// </summary>
    public class QueryCommand : ICommand<QueryResultsDto, IdentityContextDto<QueryArgumentsDto>>
    {
        internal static readonly IList<string> ColumnNames;

        static QueryCommand()
        {
            ColumnNames = new List<string>
                          {
                              "Id", "VIN", "ModelYear", "MakeName", "LineName",
                              "ModelFamilyName", "SeriesName", "BodyTypeName",
                              "SegmentName", "DriveTrainName", "EngineName",
                              "FuelTypeName", "TransmissionName", "PassengerDoorName"
                          }.AsReadOnly();
        }

        /// <summary>
        /// Query for vehicles that match the given example.
        /// </summary>
        public QueryResultsDto Execute(IdentityContextDto<QueryArgumentsDto> parameters)
        {
            QueryArgumentsDto  arguments  = parameters.Arguments;
            IdentityDto        identity   = parameters.Identity;
            IBroker            broker     = Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);                        
            IVehicleRepository repository = RegistryFactory.GetResolver().Resolve<IVehicleRepository>();

            // Fetch a page of vehicles.
            Page<Vehicle> page = repository.Fetch(
                broker,
                (VehicleActuality) arguments.Actuality,
                Mapper.Map(arguments.Example),
                Common.Commands.Mapper.Map(ColumnNames, arguments));

            return new QueryResultsDto
            {
                Arguments = arguments,
                Vehicles = Mapper.Map(page.Items),
                TotalRowCount = page.TotalRowCount
            };
        }
    }
}
