﻿using System.Collections.Generic;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Model;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Client.DomainModel.Vehicles.Repositories.Entities;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.Impl
{
    /// <summary>
    /// Helper for mapping vehicle command arguments.
    /// </summary>
    public static class Mapper
    {
        #region Vehicles

        /// <summary>
        /// Map a list of vehicles into a list of transfer objects.
        /// </summary>
        /// <param name="vehicles">List of vehicles.</param>
        /// <returns>List of transfer objects.</returns>
        public static List<VehicleDto> Map(IList<Vehicle> vehicles)
        {
            List<VehicleDto> values = new List<VehicleDto>();

            foreach (Vehicle vehicle in vehicles)
            {
                values.Add(
                    new VehicleDto
                    {
                        Description = Map(vehicle.Description),
                        Id = vehicle.Id,
                        Mileage = vehicle.Mileage,
                        Identification = new VehicleIdentificationDto
                        {
                            Vin = vehicle.Identification.Vin
                        }
                    });
            }

            return values;
        }

        /// <summary>
        /// Map vehicle description info into a transfer object.
        /// </summary>
        /// <param name="value">Vehicle description.</param>
        /// <returns>Transfer object.</returns>
        public static VehicleDescriptionDto Map(VehicleDescription value)
        {
            return new VehicleDescriptionDto
            {
                BodyTypeName = value.BodyTypeName,
                DriveTrainName = value.DriveTrainName,
                EngineName = value.EngineName,
                FuelTypeName = value.FuelTypeName,
                MakeName = value.MakeName,
                ModelFamilyName = value.ModelFamilyName,
                ModelYear = value.ModelYear,
                PassengerDoorName = value.PassengerDoorName,
                SegmentName = value.SegmentName,
                SeriesName = value.SeriesName,
                TransmissionName = value.TransmissionName
            };
        }

        /// <summary>
        /// Map a transfer object into a vehicle description object.
        /// </summary>
        /// <param name="value">Transfer object.</param>
        /// <returns>Vehicle description object.</returns>
        public static VehicleDescription Map(VehicleDescriptionDto value)
        {
            return new VehicleDescription
            {
                BodyTypeName = value.BodyTypeName,
                DriveTrainName = value.DriveTrainName,
                EngineName = value.EngineName,
                FuelTypeName = value.FuelTypeName,
                MakeName = value.MakeName,
                ModelFamilyName = value.ModelFamilyName,
                ModelYear = value.ModelYear,
                PassengerDoorName = value.PassengerDoorName,
                SegmentName = value.SegmentName,
                SeriesName = value.SeriesName,
                TransmissionName = value.TransmissionName,
                Vin = value.Vin,
                ExcludeMobile = value.ExcludeMobile
            };
        }

        #endregion

        #region Tags

        /// <summary>
        /// Map a tag object into a transfer object.
        /// </summary>
        /// <param name="value">Tag object.</param>
        /// <returns>Transfer object.</returns>
        public static TagDto Map(Tag value)
        {
            return new TagDto
            {
                Handle = value.Handle,
                Name = value.Name,
                Type = (TagTypeDto)value.TagType
            };
        }

        /// <summary>
        /// Map a transfer object into a tag object.
        /// </summary>
        /// <param name="value">Transfer object.</param>
        /// <returns>Tag object.</returns>
        public static Tag Map(TagDto value)
        {
            return new Tag
            {
                Handle = value.Handle,
                Name = value.Name,
                TagType = (TagType)value.Type
            };
        }

        /// <summary>
        /// Map a collection of tag entities into a collection of transfer objects.
        /// </summary>
        /// <param name="tags">Collection of tags.</param>
        /// <returns>Collection of tag transfer objects.</returns>
        public static List<TagDto> Map(IList<TagEntity> tags)
        {
            List<TagDto> values = new List<TagDto>();

            foreach (TagEntity tag in tags)
            {
                values.Add(Map(tag));
            }
            return values;
        }

        /// <summary>
        /// Map a collection of transfer objects into a collection of tags.
        /// </summary>
        /// <param name="tags">Collection of tag transfer objects.</param>
        /// <returns>Collection of tag objects.</returns>
        public static List<Tag> Map(IList<TagDto> tags)
        {
            List<Tag> values = new List<Tag>();

            foreach (TagDto tag in tags)
            {
                values.Add(Map(tag));
            }
            return values;
        }      

        #endregion

        #region Summarizing

        /// <summary>
        /// Map a collection of filters into a collection of transfer objects.
        /// </summary>
        /// <param name="filters">Collection of filters.</param>
        /// <returns>Collection of transfer objects.</returns>
        public static List<RowFilterDto> Map(IList<RowFilter> filters)
        {
            List<RowFilterDto> values = new List<RowFilterDto>();

            foreach (RowFilter filter in filters)
            {
                string columnName = filter.ColumnName;

                if (QueryCommand.ColumnNames.Contains(columnName))
                {
                    values.Add(
                        new RowFilterDto
                            {
                                ColumnName = columnName,
                                Value = filter.Value
                            });
                }
            }

            return values;
        }

        /// <summary>
        /// Map a collection of transfer objects into a collection of filters.
        /// </summary>
        /// <param name="filters">Collection of transfer objects.</param>
        /// <returns>Collection of filters.</returns>
        public static List<RowFilter> Map(IList<RowFilterDto> filters)
        {
            List<RowFilter> values = new List<RowFilter>();

            if (filters != null)
            {
                foreach (RowFilterDto filter in filters)
                {
                    values.Add(
                        new RowFilter
                        {
                            ColumnName = filter.ColumnName,
                            Value = filter.Value
                        });
                }
            }

            return values;
        }

        /// <summary>
        /// Map a collection of filter results into a collection of transfer objects.
        /// </summary>
        /// <param name="results">Collection of filter results.</param>
        /// <returns>Collection of transfer objects.</returns>
        public static List<SummaryResultDto> Map(IList<SummaryResult> results)
        {
            List<SummaryResultDto> values = new List<SummaryResultDto>();

            if (results != null)
            {
                foreach (SummaryResult result in results)
                {
                    values.Add(
                        new SummaryResultDto
                            {
                                Value = result.Value,
                                Count = result.Count,
                                Datapoints = Map(result.Datapoints)
                            }
                        );
                }
            }

            return values;
        }

        /// <summary>
        /// Map a collection of transfer objects into a collection of filter results.
        /// </summary>
        /// <param name="results">Collection of transfer objects.</param>
        /// <returns>Collection of filter results.</returns>
        public static List<SummaryResult> Map(IList<SummaryResultDto> results)
        {
            List<SummaryResult> values = new List<SummaryResult>();

            foreach (SummaryResultDto result in results)
            {
                values.Add(
                    new SummaryResult
                    {
                        Value = result.Value,
                        Count = result.Count,
                        Datapoints = Map(result.Datapoints)
                    }
                );
            }
            return values;
        }

        /// <summary>
        /// Map a collection of transfer objects into a collection of filter contexts.
        /// </summary>
        /// <param name="contexts">Collection of filter contexts.</param>
        /// <returns>Collection of transfer objects.</returns>
        public static List<SummaryDatapointDto> Map(IList<SummaryDatapoint> contexts)
        {
            List<SummaryDatapointDto> values = new List<SummaryDatapointDto>();

            if (contexts != null)
            {
                foreach (SummaryDatapoint context in contexts)
                {
                    values.Add(
                        new SummaryDatapointDto
                            {
                                Datapoint = context.Datapoint,
                                Value = context.Value
                            }
                        );
                }
            }

            return values;
        }

        /// <summary>
        /// Map a collection of filter contexts into a collection of transfer objects.
        /// </summary>
        /// <param name="contexts">Collection of transfer objects.</param>
        /// <returns>Collection of filter contexts.</returns>
        public static List<SummaryDatapoint> Map(IList<SummaryDatapointDto> contexts)
        {
            List<SummaryDatapoint> values = new List<SummaryDatapoint>();

            foreach (SummaryDatapointDto context in contexts)
            {
                values.Add(
                    new SummaryDatapoint
                    {
                        Datapoint = context.Datapoint,
                        Value = context.Value
                    }
                );
            }
            return values;
        }

        #endregion
    }
}

