﻿using System;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.Impl
{
    /// <summary>
    /// Command to delete a broker's tag.
    /// </summary>
    public class DeleteTagCommand : ICommand<DeleteTagResultsDto, IdentityContextDto<DeleteTagArgumentsDto>>
    {
        /// <summary>
        /// Delete a broker's tag. Will remove the tag from all associated vehicles.
        /// </summary>
        /// <param name="parameters">Broker and tag identifiers.</param>
        /// <returns>Number of vehicles affected, and the result of the operation.</returns>
        public DeleteTagResultsDto Execute(IdentityContextDto<DeleteTagArgumentsDto> parameters)
        {
            DeleteTagArgumentsDto arguments  = parameters.Arguments;
            IdentityDto           identity   = parameters.Identity;
            IBroker               broker     = Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);
            IPrincipal            principal  = Principal.Get(identity.Name, identity.AuthorityName);
            IVehicleRepository    repository = RegistryFactory.GetResolver().Resolve<IVehicleRepository>();

            // Validate the tag handle.
            if (arguments.Tag == Guid.Empty)
            {
                throw new ArgumentException("Tag handle must be specified.");
            }

            // Validate the tag.
            TagResult tag = repository.FetchTag(broker, arguments.Tag);            
            if (tag.Result != TagOperationState.Success || tag.Tag == null)
            {
                return new DeleteTagResultsDto
                {
                    Arguments = arguments,
                    VehiclesAffected = 0,
                    Result = (TagResultDto)tag.Result
                };
            }

            // Get the number of tagged vehicles.
            int taggedVehicles = repository.FetchTaggedVins(broker, tag.Tag).Count;

            // Delete.
            TagOperationState result = repository.DeleteTag(broker, principal, tag.Tag);

            // Make sure there are no more tagged vehicles.
            if (repository.FetchTaggedVins(broker, tag.Tag).Count != 0)
            {
                throw new ApplicationException("Deleting tag didn't untag vehicles.");
            }

            return new DeleteTagResultsDto
            {
                Arguments = arguments,
                VehiclesAffected = taggedVehicles,
                Result = (TagResultDto)result
            };
        }
    }
}