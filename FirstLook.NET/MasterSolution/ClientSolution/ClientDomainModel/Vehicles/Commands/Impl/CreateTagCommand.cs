﻿using System;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.Impl
{
    /// <summary>
    /// Command to create a tag for a broker.
    /// </summary>
    public class CreateTagCommand : ICommand<CreateTagResultsDto, IdentityContextDto<CreateTagArgumentsDto>>
    {
        /// <summary>
        /// Create a tag.
        /// </summary>
        /// <param name="parameters">Broker identifier and tag details.</param>
        /// <returns>Tag that we've tried to create, and the result of the operation.</returns>
        public CreateTagResultsDto Execute(IdentityContextDto<CreateTagArgumentsDto> parameters)
        {
            CreateTagArgumentsDto arguments  = parameters.Arguments;
            IdentityDto           identity   = parameters.Identity;
            IBroker               broker     = Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);
            IPrincipal            principal  = Principal.Get(identity.Name, identity.AuthorityName);
            IVehicleRepository    repository = RegistryFactory.GetResolver().Resolve<IVehicleRepository>();

            // Validate the tag name.
            if (string.IsNullOrEmpty(arguments.TagName))
            {
                throw new ArgumentException("Tag name cannot be null or empty.");
            }

            // Create the tag.
            TagResult tag = repository.SaveTag(broker, principal, new Tag {Name = arguments.TagName});

            return new CreateTagResultsDto
            {
               Arguments = arguments,
               Tag = Mapper.Map(tag.Tag),
               Result = (TagResultDto)tag.Result
            };
        }
    }
}
