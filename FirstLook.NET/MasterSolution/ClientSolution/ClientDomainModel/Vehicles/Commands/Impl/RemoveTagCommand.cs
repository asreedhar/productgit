﻿using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.Impl
{
    /// <summary>
    /// Command to remove a tag from a vehicle.
    /// </summary>
    public class RemoveTagCommand : ICommand<RemoveTagResultsDto, IdentityContextDto<RemoveTagArgumentsDto>>
    {
        /// <summary>
        /// Remove a tag from a vehicle.
        /// </summary>
        /// <param name="parameters">Broker, vehicle and tag identifiers.</param>
        /// <returns>Result of the operation, and the arguments used.</returns>
        public RemoveTagResultsDto Execute(IdentityContextDto<RemoveTagArgumentsDto> parameters)
        {
            RemoveTagArgumentsDto arguments  = parameters.Arguments;
            IdentityDto           identity   = parameters.Identity;
            IBroker               broker     = Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);
            IPrincipal            principal  = Principal.Get(identity.Name, identity.AuthorityName);
            IVehicleRepository    repository = RegistryFactory.GetResolver().Resolve<IVehicleRepository>();

            // Validate the vehicle.
            ClientVehicleIdentification vehicle = repository.Identification(broker, arguments.Vehicle);
            if (vehicle == null)
            {
                return new RemoveTagResultsDto
                {
                    Arguments = arguments,
                    Result = TagResultDto.VehicleDoesntExist
                };
            }

            // Validate the tag.
            TagResult tag = repository.FetchTag(broker, arguments.Tag);
            if (tag.Result != TagOperationState.Success)
            {
                return new RemoveTagResultsDto
                {
                    Arguments = arguments,
                    Result = TagResultDto.TagDoesntExist
                };
            }

            // Remove the tag.
            TagOperationState result = repository.RemoveTag(principal, vehicle, tag.Tag);

            return new RemoveTagResultsDto
            {
                Arguments = parameters.Arguments,
                Result = (TagResultDto)result
            };
        }
    }
}
