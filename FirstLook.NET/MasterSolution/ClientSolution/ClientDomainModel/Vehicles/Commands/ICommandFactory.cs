﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;

namespace FirstLook.Client.DomainModel.Vehicles.Commands
{
    /// <summary>
    /// Interface for a vehicle command factory.
    /// </summary>
    public interface ICommandFactory
    {
        #region Vehicle Commands

        /// <summary>
        /// Create a command to query for a vehicle by example.
        /// </summary>
        /// <returns>Command to query for vehicles.</returns>
        ICommand<QueryResultsDto, IdentityContextDto<QueryArgumentsDto>> CreateQueryCommand();

        /// <summary>
        /// Create a command to summarize the state of a group of vehicles.
        /// </summary>
        /// <returns>Command to summarize vehicles.</returns>
        ICommand<SummaryResultsDto, IdentityContextDto<SummaryArgumentsDto>> CreateSummarizeCommand();

        /// <summary>
        /// Create a command to identify a vehicle by its vin.
        /// </summary>
        /// <returns>Vehicle handle.</returns>
        ICommand<IdentifyVehicleResultsDto, IdentityContextDto<IdentifyVehicleArgumentsDto>> CreateIdentifyVehicleCommand();


        ICommand<IdentifyInventoryResultsDto, IdentityContextDto<IdentifyInventoryArgumentsDto>> CreateIdentifyInventoryCommand();

        #endregion

        #region Tag Commands

        /// <summary>
        /// Create a command to get a client's tags.
        /// </summary>
        /// <returns>Command to get a client's tags.</returns>
        ICommand<BrokerTagsResultsDto, IdentityContextDto<BrokerTagsArgumentsDto>> CreateBrokerTagsCommand();

        /// <summary>
        /// Create a command to get vehicles tagged with a given tag.
        /// </summary>
        /// <returns>Command to get tag vehicles.</returns>
        ICommand<TaggedVehiclesResultsDto, IdentityContextDto<TaggedVehiclesArgumentsDto>> CreateTaggedVehiclesCommand();

        /// <summary>
        /// Create a command to get the tags for a given vehicle.
        /// </summary>
        /// <returns>Command to get tags for a vehicle.</returns>
        ICommand<VehicleTagsResultsDto, IdentityContextDto<VehicleTagsArgumentsDto>> CreateVehicleTagsCommand();

        /// <summary>
        /// Create a command to create a tag.
        /// </summary>
        /// <returns>Command to create a tag.</returns>
        ICommand<CreateTagResultsDto, IdentityContextDto<CreateTagArgumentsDto>> CreateCreateTagCommand();

        /// <summary>
        /// Create a command to delete a tag.
        /// </summary>
        /// <returns>Command to delete a tag.</returns>
        ICommand<DeleteTagResultsDto, IdentityContextDto<DeleteTagArgumentsDto>> CreateDeleteTagCommand();

        /// <summary>
        /// Create a command to update a tag.
        /// </summary>
        /// <returns>Command to update a tag.</returns>
        ICommand<UpdateTagResultsDto, IdentityContextDto<UpdateTagArgumentsDto>> CreateUpdateTagCommand();

        /// <summary>
        /// Create a command to apply a tag to a vehicle.
        /// </summary>
        /// <returns>Command to apply a tag.</returns>
        ICommand<ApplyTagResultsDto, IdentityContextDto<ApplyTagArgumentsDto>> CreateApplyTagCommand();

        /// <summary>
        /// Create a command to remove a tag from a vehicle.
        /// </summary>
        /// <returns>Command to remove a tag.</returns>
        ICommand<RemoveTagResultsDto, IdentityContextDto<RemoveTagArgumentsDto>> CreateRemoveTagCommand();


        #endregion
    }
}
