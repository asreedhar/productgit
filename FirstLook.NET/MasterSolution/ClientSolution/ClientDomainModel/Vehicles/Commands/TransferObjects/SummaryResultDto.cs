﻿using System;
using System.Collections.Generic;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects
{
    /// <summary>
    /// One result in a summarization.
    /// </summary>
    [Serializable]
    public class SummaryResultDto
    {
        /// <summary>
        /// Value produced by summarizing a group of vehicles.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// The number of vehicles in the group with this value.
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Additional datapoints related to the summarization.
        /// </summary>
        public List<SummaryDatapointDto> Datapoints { get; set; }
    }
}
