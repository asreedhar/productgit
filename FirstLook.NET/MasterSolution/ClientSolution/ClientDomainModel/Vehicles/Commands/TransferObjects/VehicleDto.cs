﻿using System;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects
{
    /// <summary>
    /// A vehicle.
    /// </summary>
    [Serializable]
    public class VehicleDto
    {
        /// <summary>
        /// Vehicle identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Description details of the vehicle.
        /// </summary>
        public VehicleDescriptionDto Description { get; set; }

        /// <summary>
        /// Vehicle mileage.
        /// </summary>
        public int Mileage { get; set; }

        /// <summary>
        /// Identifying details of the vehicle.
        /// </summary>
        public VehicleIdentificationDto Identification { get; set; }
    }
}
