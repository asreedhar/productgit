﻿using System;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects
{
    /// <summary>
    /// Description of a vehicle.
    /// </summary>
    [Serializable]
    public class VehicleDescriptionDto
    {
        /// <summary>
        /// Initialize description with empty valies.
        /// </summary>

        public VehicleDescriptionDto()
        {
            InventoryId = 0;
            BodyTypeName = string.Empty;
            DriveTrainName = string.Empty;
            EngineName = string.Empty;
            FuelTypeName = string.Empty;
            MakeName = string.Empty;
            ModelFamilyName = string.Empty;
            PassengerDoorName = string.Empty;
            SegmentName = string.Empty;
            SeriesName = string.Empty;
            TransmissionName = string.Empty;
            Vin = string.Empty;
            ExcludeMobile = false;
            Color = string.Empty;
        }
        /// <summary>
        /// Vin.
        /// </summary>
        public string Vin { get; set; }
        /// <summary>
        /// InventoryId.
        /// </summary>
        public int InventoryId { get; set; }

        #region Model

        /// <summary>
        /// Make.
        /// </summary>
        public string MakeName { get; set; }

        /// <summary>
        /// Model family.
        /// </summary>
        public string ModelFamilyName { get; set; }

        /// <summary>
        /// Series.
        /// </summary>
        public string SeriesName { get; set; }

        /// <summary>
        /// Segment.
        /// </summary>
        public string SegmentName { get; set; }

        /// <summary>
        /// Body type.
        /// </summary>
        public string BodyTypeName { get; set; }

        #endregion

        #region Configuration

        /// <summary>
        /// Model year.
        /// </summary>
        public int ModelYear { get; set; }

        /// <summary>
        /// Passenger door.
        /// </summary>
        public string PassengerDoorName { get; set; }

        /// <summary>
        /// Fuel type.
        /// </summary>
        public string FuelTypeName { get; set; }

        /// <summary>
        /// Engine.
        /// </summary>
        public string EngineName { get; set; }

        /// <summary>
        /// Drive train.
        /// </summary>
        public string DriveTrainName { get; set; }

        /// <summary>
        /// Transmission.
        /// </summary>
        public string TransmissionName { get; set; }

        /// <summary>
        /// Color
        /// </summary>
        public string Color { get; set; }

        #endregion

        #region Other

        public bool ExcludeMobile { get; set; }

        #endregion
    }
}
