﻿using System;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects
{
    /// <summary>
    /// Types of existence for a vehicle.
    /// </summary>
    [Serializable]
    public enum VehicleActualityDto
    {
        /// <summary>
        /// Undefined. The universe 'splodes.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// Inventory.
        /// </summary>
        Inventory = 1,

        /// <summary>
        /// Not inventory.
        /// </summary>
        NotInventory = 2
    }
}
