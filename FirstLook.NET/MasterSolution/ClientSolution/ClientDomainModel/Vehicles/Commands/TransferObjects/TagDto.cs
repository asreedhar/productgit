﻿
using System;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects
{
    /// <summary>
    /// Tag. Essentially a means of grouping vehicles together arbitrarily.
    /// </summary>
    [Serializable]
    public class TagDto
    {
        /// <summary>
        /// Tag identifier.
        /// </summary>
        public Guid Handle { get; set; }

        /// <summary>
        /// Name of the tag.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Type of tag - static or dynamic.
        /// </summary>
        public TagTypeDto Type { get; set; }
    }
}
