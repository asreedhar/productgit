﻿using System;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments for getting the tags tied to a vehicle.
    /// </summary>
    [Serializable]
    public class VehicleTagsArgumentsDto
    {
        /// <summary>
        /// Broker whose vehicle this is.
        /// </summary>
        public Guid Broker { get; set; }

        /// <summary>
        /// Vehicle to find tags of.
        /// </summary>
        public Guid Vehicle { get; set; }
    }
}
