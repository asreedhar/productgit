﻿using System;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class UpdateTagArgumentsDto
    {
        /// <summary>
        /// Broker to create a tag for.
        /// </summary>
        public Guid Broker;

        /// <summary>
        /// Tag to delete.
        /// </summary>
        public Guid Tag { get; set; }

        /// <summary>
        /// New name of the tag.
        /// </summary>        
        public string Name;
    }
}