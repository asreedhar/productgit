﻿using System;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Results of deleting a broker's tag.
    /// </summary>
    [Serializable]
    public class DeleteTagResultsDto
    {
        /// <summary>
        /// Arguments used to delete a broker's tag.
        /// </summary>
        public DeleteTagArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Number of vehicles the tag was removed from.
        /// </summary>
        public int VehiclesAffected { get; set; }

        /// <summary>
        /// Result of the delete operation.
        /// </summary>
        public TagResultDto Result { get; set; }
    }
}
