﻿using System;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Results of a removing a tag from a vehicle.
    /// </summary>
    [Serializable]
    public class RemoveTagResultsDto
    {
        /// <summary>
        /// Arguments used to produce these results.
        /// </summary>
        public RemoveTagArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Result of the remove operation.
        /// </summary>
        public TagResultDto Result { get; set; }
    }
}
