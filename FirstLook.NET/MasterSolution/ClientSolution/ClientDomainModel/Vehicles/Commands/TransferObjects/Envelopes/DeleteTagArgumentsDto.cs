﻿using System;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments used to delete a broker's tag.
    /// </summary>
    [Serializable]
    public class DeleteTagArgumentsDto
    {
        /// <summary>
        /// Broker whose tag we're deleting.
        /// </summary>
        public Guid Broker { get; set; }

        /// <summary>
        /// Tag to delete.
        /// </summary>
        public Guid Tag { get; set; }
    }
}
