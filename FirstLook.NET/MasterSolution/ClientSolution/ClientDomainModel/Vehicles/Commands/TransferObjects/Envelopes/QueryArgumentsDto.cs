﻿using System;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments for querying for matching vehicles.
    /// </summary>
    [Serializable]
    public class QueryArgumentsDto : PaginatedArgumentsDto
    {
        /// <summary>
        /// Broker handle.
        /// </summary>
        public Guid Broker { get; set; }        

        /// <summary>
        /// Example vehicle to match results against.
        /// </summary>
        public VehicleDescriptionDto Example { get; set; }

        /// <summary>
        /// What should we query against? Inventory? Not inventory?
        /// </summary>
        public VehicleActualityDto Actuality { get; set; }
    }
}
