﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Results of querying for vehicles.
    /// </summary>
    [Serializable]
    public class QueryResultsDto : PaginatedResultsDto
    {
        /// <summary>
        /// Arguments used to produce the results.
        /// </summary>
        public QueryArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Matching vehicles.
        /// </summary>
        public List<VehicleDto> Vehicles { get; set; }
    }
}
