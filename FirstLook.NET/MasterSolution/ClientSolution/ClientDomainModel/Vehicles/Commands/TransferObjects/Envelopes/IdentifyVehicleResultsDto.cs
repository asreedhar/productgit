﻿using System;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Results of identifying a vehicle by its vin.
    /// </summary>
    [Serializable]
    public class IdentifyVehicleResultsDto
    {
        /// <summary>
        /// Arguments used to produce these results.
        /// </summary>
        public IdentifyVehicleArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Vehicle handle.
        /// </summary>
        public Guid Vehicle { get; set; }

        /// <summary>
        /// A description of the vehicle.
        /// </summary>
        public VehicleDescriptionDto Description { get; set; }
    }
}
