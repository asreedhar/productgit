﻿using System;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments used to create a tag.
    /// </summary>
    [Serializable]
    public class CreateTagArgumentsDto
    {
        /// <summary>
        /// Broker to create a tag for.
        /// </summary>
        public Guid Broker { get; set; }

        /// <summary>
        /// Name of the tag to create.
        /// </summary>        
        public string TagName { get; set; } 
    }
}
