﻿using System;
using System.Collections.Generic;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Results of getting a broker's tags.
    /// </summary>
    [Serializable]
    public class BrokerTagsResultsDto
    {
        /// <summary>
        /// Arguments used to produce the results.
        /// </summary>
        public BrokerTagsArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Broker's tags.
        /// </summary>
        public List<TagDto> Tags;

        /// <summary>
        /// Result of the operation.
        /// </summary>
        public TagResultDto Result { get; set; }
    }
}
