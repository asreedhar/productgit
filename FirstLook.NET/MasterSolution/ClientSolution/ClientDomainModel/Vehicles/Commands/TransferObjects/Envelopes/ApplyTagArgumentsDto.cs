﻿using System;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments for tagging a broker's vehicle.
    /// </summary>
    [Serializable]
    public class ApplyTagArgumentsDto
    {
        /// <summary>
        /// Broker whose vehicle will be tagged.
        /// </summary>
        public Guid Broker;

        /// <summary>
        /// Vehicle to be tagged.
        /// </summary>
        public Guid Vehicle;

        /// <summary>
        /// Tag to apply.
        /// </summary>
        public Guid Tag;
    }
}
