﻿using System;
using System.Collections.Generic;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Results of getting a broker's vehicles tagged with a given tag.
    /// </summary>
    [Serializable]
    public class TaggedVehiclesResultsDto
    {
        /// <summary>
        /// Arguments used to produce these results.
        /// </summary>
        public TaggedVehiclesArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Vehicles that match the given tag for a broker.
        /// </summary>
        public List<VehicleDto> Vehicles { get; set; }

        /// <summary>
        /// Result of the operation.
        /// </summary>
        public TagResultDto Result { get; set; }
    }
}
