﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments used to summarize a group of vehicles.
    /// </summary>
    [Serializable]
    public class SummaryArgumentsDto
    {
        /// <summary>
        /// Whose vehicles we should be summarizing.
        /// </summary>
        public Guid Broker { get; set; }

        /// <summary>
        /// Filters to apply to the list of vehicles.
        /// </summary>
        public List<RowFilterDto> Where { get; set; }

        /// <summary>
        /// The name of the column by which we want the result-set aggregated.
        /// </summary>
        public string GroupBy { get; set; }

        /// <summary>
        /// Type of vehicle to summarize -- i.e. inventory or not inventory.
        /// </summary>
        public VehicleActualityDto Actuality { get; set; }

        /// <summary>
        /// Prevent null reference explosions on the filter list.
        /// </summary>
        public SummaryArgumentsDto()
        {
            Where = new List<RowFilterDto>();
        }
    }
}
