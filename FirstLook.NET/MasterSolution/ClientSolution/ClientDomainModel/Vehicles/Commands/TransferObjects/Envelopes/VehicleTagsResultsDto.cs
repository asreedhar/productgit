﻿using System;
using System.Collections.Generic;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Results of getting a vehicle's tags.
    /// </summary>
    [Serializable]
    public class VehicleTagsResultsDto
    {
        /// <summary>
        /// Arguments used to produce these results.
        /// </summary>
        public VehicleTagsArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Tags that apply to a vehicle.
        /// </summary>
        public List<TagDto> Tags;

        /// <summary>
        /// Result of the operation.
        /// </summary>
        public TagResultDto Result { get; set; }
    }
}
