﻿using System;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Result of creating a tag.
    /// </summary>
    [Serializable]
    public class CreateTagResultsDto
    {
        /// <summary>
        /// Arguments used to produce these results.
        /// </summary>
        public CreateTagArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Newly created tag.
        /// </summary>
        public TagDto Tag { get; set; }

        /// <summary>
        /// Result of the create operation.
        /// </summary>
        public TagResultDto Result { get; set; }
    }
}
