﻿using System;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Result of updating a tag.
    /// </summary>
    [Serializable]
    public class UpdateTagResultsDto
    {
        /// <summary>
        /// Arguments used to produce these results.
        /// </summary>
        public UpdateTagArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Result of the update operation.
        /// </summary>
        public TagResultDto Result { get; set; }
    }
}