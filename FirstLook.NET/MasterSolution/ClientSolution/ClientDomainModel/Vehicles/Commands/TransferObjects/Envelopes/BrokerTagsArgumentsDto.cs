﻿using System;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments for getting the list of broker's tags.
    /// </summary>
    [Serializable]
    public class BrokerTagsArgumentsDto
    {
        /// <summary>
        /// Broker to get tags for.
        /// </summary>
        public Guid Broker { get; set; }
    }
}
