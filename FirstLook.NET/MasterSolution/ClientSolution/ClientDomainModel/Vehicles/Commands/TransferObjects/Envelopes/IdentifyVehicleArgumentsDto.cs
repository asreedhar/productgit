﻿using System;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments to identify a vehicle by its vin.
    /// </summary>
    [Serializable]
    public class IdentifyVehicleArgumentsDto
    {
        /// <summary>
        /// Broker whose vehicle we are identifying.
        /// </summary>
        public Guid Broker { get; set; }

        /// <summary>
        /// Vin of the vehicle to identify.
        /// </summary>
        public string Vin { get; set; }
    }
}
