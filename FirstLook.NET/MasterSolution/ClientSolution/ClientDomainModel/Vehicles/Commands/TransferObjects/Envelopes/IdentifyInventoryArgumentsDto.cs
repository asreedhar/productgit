﻿using System;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    public class IdentifyInventoryArgumentsDto
    {
        /// <summary>
        /// Broker whose vehicle we are identifying.
        /// </summary>
        public Guid Broker { get; set; }

        /// <summary>
        /// Vin of the vehicle to identify.
        /// </summary>
        public int InventoryId { get; set; }
        /// <summary>
        /// DealerId
        /// </summary>
        public int DealerId { get; set; }

    }
}
