﻿using System;
using System.Collections.Generic;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Results of summarizing vehicles.
    /// </summary>
    [Serializable]
    public class SummaryResultsDto
    {
        /// <summary>
        /// Arguments used to produce the results.
        /// </summary>
        public SummaryArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Results of summarizing a group of vehicles.
        /// </summary>
        public List<SummaryResultDto> Results { get; set; }

        /// <summary>
        /// Prevent null reference explosions on the result set.
        /// </summary>
        public SummaryResultsDto()
        {
            Results = new List<SummaryResultDto>();
        }
    }
}
