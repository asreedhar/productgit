﻿using System;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Results of tagging a broker's vehicle.
    /// </summary>
    [Serializable]
    public class ApplyTagResultsDto
    {
        /// <summary>
        /// Arguments used to produce the results.
        /// </summary>
        public ApplyTagArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Result of the apply operation.
        /// </summary>
        public TagResultDto Result { get; set; }
    }
}
