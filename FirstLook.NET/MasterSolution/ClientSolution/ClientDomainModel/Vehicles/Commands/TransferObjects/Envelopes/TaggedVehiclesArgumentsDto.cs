﻿using System;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments for retrieving vehicles a broker has tagged with a specific tag.
    /// </summary>
    [Serializable]
    public class TaggedVehiclesArgumentsDto
    {
        /// <summary>
        /// Broker whose tagged vehicles we want to retrieve.
        /// </summary>
        public Guid Broker { get; set; }

        /// <summary>
        /// Tag to retrieve vehicles for.
        /// </summary>
        public Guid Tag { get; set; }
    }
}
