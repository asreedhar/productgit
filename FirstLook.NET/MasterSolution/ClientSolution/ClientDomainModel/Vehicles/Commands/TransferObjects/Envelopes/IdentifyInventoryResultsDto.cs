﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Results of identifying a vehicle by its inventoryId.
    /// </summary>
    [Serializable]
    public class IdentifyInventoryResultsDto
    {
        /// <summary>
        /// Arguments used to produce these results.
        /// </summary>
        public IdentifyInventoryArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Vehicle vin.
        /// </summary>
        public string VIN { get; set; }

        /// <summary>
        /// A description of the vehicle.
        /// </summary>
        public VehicleDescriptionDto Description { get; set; }
    }

}
