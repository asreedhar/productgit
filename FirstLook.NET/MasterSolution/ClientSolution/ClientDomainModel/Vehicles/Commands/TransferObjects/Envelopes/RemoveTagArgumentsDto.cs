﻿using System;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments used to remove a tag from a broker's vehicle.
    /// </summary>
    [Serializable]
    public class RemoveTagArgumentsDto
    {
        /// <summary>
        /// Broker whose vehicle is being untagged.
        /// </summary>
        public Guid Broker;

        /// <summary>
        /// Vehicle that is having tag removed.
        /// </summary>
        public Guid Vehicle;

        /// <summary>
        /// Tag to remove from vehicle.
        /// </summary>
        public Guid Tag;
    }
}
