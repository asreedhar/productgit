﻿using System;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects
{
    /// <summary>
    /// Identifying details of a vehicle.
    /// </summary>
    [Serializable]
    public class VehicleIdentificationDto
    {
        /// <summary>
        /// Vin.
        /// </summary>
        public string Vin { get; set; }

        /// <summary>
        /// Vehicle identifier.
        /// </summary>
        public int Id { get; set; }
    }
}
