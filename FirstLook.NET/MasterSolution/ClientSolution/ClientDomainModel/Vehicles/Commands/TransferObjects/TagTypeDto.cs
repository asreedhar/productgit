﻿using System;

namespace FirstLook.Client.DomainModel.Vehicles.Commands.TransferObjects
{
    /// <summary>
    /// Types of tags.
    /// </summary>
    [Serializable]
    public enum TagTypeDto
    {
        /// <summary>
        /// Undefined.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// Dynamic tags i.e. tags that are not client-defined, and are essentially queries.
        /// </summary>
        Dynamic = 1,

        /// <summary>
        /// Static tags i.e. client-defined tags that apply to a static list of vehicles.
        /// </summary>
        Static = 2
    }
}
