﻿using FirstLook.Client.DomainModel.Vehicles.Commands.Impl;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Vehicles.Commands
{
    /// <summary>
    /// Module for configuring vehicle command components.
    /// </summary>
    public class Module : IModule
    {
        /// <summary>
        /// Register components necessary for vehicle commands.
        /// </summary>
        /// <param name="registry"></param>
        public void Configure(IRegistry registry)
        {
            registry.Register<ICommandFactory, CommandFactory>(ImplementationScope.Shared);
        }
    }
}
