﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Licenses.Model;
using FirstLook.Client.DomainModel.Licenses.Repositories.Entities;
using FirstLook.Client.DomainModel.Licenses.Repositories.Gateways;
using FirstLook.Common.Core;
using FirstLook.Common.Core.Data;

namespace FirstLook.Client.DomainModel.Licenses.Repositories
{
    /// <summary>
    /// Licensing data repository.
    /// </summary>
    internal class LicenseRepository : RepositoryBase, ILicenseRepository
    {
        #region Licenses

        /// <summary>
        /// Fetch a client's licenses.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <returns>List of licenses.</returns>
        public IList<LicenseInfo> Fetch_Licenses(IBroker broker)
        {
            return DoInSession(() => 
            {
                IList<LicenseInfo> licenses = new List<LicenseInfo>();
                foreach(LicenseEntity entity in new LicenseGateway().Fetch_Licenses(broker))
                {
                    licenses.Add(entity);
                }
                return licenses;
            });
        }

        /// <summary>
        /// Fetch a particular license.
        /// </summary>        
        /// <param name="license">License handle.</param>
        /// <returns>License.</returns>
        public License Fetch_License(Guid license)
        {
            return DoInSession(() => new LicenseGateway().Fetch_License(license));
        }

        /// <summary>
        /// Save a new or updated license.
        /// </summary>        
        /// <param name="principal">Principal.</param>
        /// <param name="broker">Broker.</param>
        /// <param name="license">License to save.</param>
        /// <returns>License that was saved.</returns>
        public License Save_License(IPrincipal principal, IBroker broker, License license)
        {
            return DoInTransaction
            (
                delegate(IDataSession session)
                {
                    if (session.Items["Audit"] as Audit == null)
                    {
                        session.Items["Audit"] = Resolve<IRepository>().Create(principal);
                    }
                },
                () => new LicenseGateway().Save_License(broker, license)
            );
        }

        /// <summary>
        /// Delete a license and all of its associated leases.
        /// </summary>        
        /// <param name="principal">Principal.</param>
        /// <param name="license">License to delete.</param>
        /// <returns>License that was deleted.</returns>
        public License Delete_License(IPrincipal principal, Guid license)
        {
            return DoInTransaction
            (
                delegate(IDataSession session)
                {
                    if (session.Items["Audit"] as Audit == null)
                    {
                        session.Items["Audit"] = Resolve<IRepository>().Create(principal);
                    }
                },
                () => new LicenseGateway().Delete_License(license)
            );    
        }

        #endregion

        #region Leases

        /// <summary>
        /// Fetch all of a license's leases.
        /// </summary>        
        /// <param name="license">License handle.</param>
        /// <returns>List of leases.</returns>
        public IList<LeaseInfo> Fetch_Leases(Guid license)
        {
            return DoInSession(() => 
            {
                IList<LeaseInfo> leases = new List<LeaseInfo>();
                foreach(LeaseEntity entity in new LeaseGateway().Fetch_Leases(license))
                {
                    leases.Add(entity);
                }
                return leases;
            });
        }

        /// <summary>
        /// Fetch a particular lease.
        /// </summary>
        /// <param name="license">License handle.</param>
        /// <param name="userId">User identifier.</param>
        /// <returns>Lease.</returns>
        public Lease Fetch_Lease(Guid license, int userId)
        {
            return DoInSession(() => new LeaseGateway().Fetch_Lease(license, userId));
        }

        /// <summary>
        /// Create a lease for the given user on the given license.
        /// </summary>
        /// <param name="principal">Principal.</param>
        /// <param name="license">License handle.</param>
        /// <param name="userId">User identifier.</param>
        /// <returns>New lease.</returns>
        public Lease Create_Lease(IPrincipal principal, Guid license, int userId)
        {
            return DoInTransaction
            (
                delegate(IDataSession session)
                {
                    if (session.Items["Audit"] as Audit == null)
                    {
                        session.Items["Audit"] = Resolve<IRepository>().Create(principal);
                    }
                },
                () => new LeaseGateway().Insert_Lease(license, userId)
            ); 
        }

        /// <summary>
        /// Delete the lease for the given user on the given license.
        /// </summary>
        /// <param name="principal">Principal.</param>
        /// <param name="license">License handle.</param>        
        /// <param name="userId">User identifier.</param>
        /// <returns>Lease that was deleted.</returns>
        public LeaseInfo Delete_Lease(IPrincipal principal, Guid license, int userId)
        {
            return DoInTransaction
            (
                delegate(IDataSession session)
                {
                    if (session.Items["Audit"] as Audit == null)
                    {
                        session.Items["Audit"] = Resolve<IRepository>().Create(principal);
                    }
                },
                () => new LeaseGateway().Delete_Lease(license, userId)
            ); 
        }

        /// <summary>
        /// Mark a lease as having been used.
        /// </summary>
        /// <param name="principal">Principal.</param>
        /// <param name="license">License handle.</param>
        /// <param name="userId">User identifier.</param>
        /// <returns>Lease that was used.</returns>
        public Lease Use_Lease(IPrincipal principal, Guid license, int userId)
        {
            return DoInTransaction
            (
                delegate(IDataSession session)
                {
                    if (session.Items["Audit"] as Audit == null)
                    {
                        session.Items["Audit"] = Resolve<IRepository>().Create(principal);
                    }
                },
                () => new LeaseGateway().Use_Lease(license, userId)
            ); 
        }

        #endregion

        #region Devices

        /// <summary>
        /// Fetch the device with the given handle.
        /// </summary>
        /// <param name="handle">Device handle.</param>
        /// <returns>Device identifiers.</returns>
        public Device Fetch_Device(Guid handle)
        {
            return DoInSession(() => new DeviceGateway().Fetch_Device(handle));
        }

        /// <summary>
        /// Create a device of the given type.
        /// </summary>
        /// <param name="principal">Principal.</param>        
        /// <param name="type">Device type.</param>
        /// <returns>Device identifiers.</returns>
        public Device Create_Device(IPrincipal principal, DeviceType type)
        {
            return DoInTransaction
            (
                delegate(IDataSession session)
                {
                    if (session.Items["Audit"] as Audit == null)
                    {
                        session.Items["Audit"] = Resolve<IRepository>().Create(principal);
                    }
                },
                () => new DeviceGateway().Insert_Device(type)); 
        }

        #endregion

        #region Tokens

        /// <summary>
        /// Fetch all of a license's tokens.
        /// </summary>        
        /// <param name="license">License.</param>
        /// <returns>List of tokens.</returns>
        public IList<Token> Fetch_Tokens(Guid license)
        {
            return DoInSession(() => 
            {
                IList<Token> tokens = new List<Token>();
                foreach (TokenEntity token in new TokenGateway().Fetch_Tokens(license))
                {
                    tokens.Add(token);
                }
                return tokens;
            });
        }

        /// <summary>
        /// Fetch the token for the licensed user on a device.
        /// </summary>        
        /// <param name="license">License handle.</param>
        /// <param name="userId">User identifier.</param>      
        /// <param name="device">Device identifier.</param>  
        /// <returns>Token, or null if one does not exists.</returns>
        public Token Fetch_Token(Guid license, int userId, Guid device)
        {
            return DoInSession(() => new TokenGateway().Fetch_Token(license, userId, device));
        }

        /// <summary>
        /// Fetch the token with the given handle.
        /// </summary>        
        /// <param name="token">Token handle.</param>
        /// <returns>Token, or null if one does not exist.</returns>
        public Token Fetch_Token(Guid token)
        {
            return DoInSession(() => new TokenGateway().Fetch_Token(token));
        }                

        /// <summary>
        /// Save a token.
        /// </summary>
        /// <param name="principal">Principal.</param>
        /// <param name="token">Token to save.</param>
        /// <returns>Saved token.</returns>
        public Token Save_Token(IPrincipal principal, Token token)
        {
            return DoInTransaction
            (
                delegate(IDataSession session)
                {
                    if (session.Items["Audit"] as Audit == null)
                    {
                        session.Items["Audit"] = Resolve<IRepository>().Create(principal);
                    }
                },
                () => new TokenGateway().Save_Token(token)
            ); 
        }

        /// <summary>
        /// Delete tokens for the user for the type of device. If a device identifier is provided, do not delete any
        /// of its tokens.
        /// </summary>
        /// <param name="principal">Principal.</param>
        /// <param name="userId">User identifier.</param>
        /// <param name="deviceType">Device type.</param>
        /// <param name="safeDevice">Device to spare, if applicable.</param>
        /// <returns>Number of tokens invalidated.</returns>
        public int Delete_Tokens(IPrincipal principal, int userId, DeviceType? deviceType, Guid? safeDevice)
        {
            return DoInTransaction
            (
                delegate(IDataSession session)
                {
                    if (session.Items["Audit"] as Audit == null)
                    {
                        session.Items["Audit"] = Resolve<IRepository>().Create(principal);
                    }
                },
                () => new TokenGateway().Delete_Tokens(userId, deviceType, safeDevice)
            ); 
        }

        #endregion

        #region Products

        /// <summary>
        /// Get all license-able products.
        /// </summary>
        /// <returns>List of products.</returns>
        public IList<Product> Fetch_Products()
        {
            return DoInSession(() => new ProductGateway().Fetch_Products());
        }

        /// <summary>
        /// Fetch the product with the given name.
        /// </summary>
        /// <param name="name">Product name.</param>
        /// <returns>Product.</returns>
        public Product Fetch_Product(string name)
        {
            return DoInSession(() => new ProductGateway().Fetch_Product(name));
        }

        #endregion

        #region Miscellaneous

        /// <summary>
        /// Vehicle is the licensing database.
        /// </summary>
        protected override string DatabaseName
        {
            get { return "Vehicle"; }
        }

        /// <summary>
        /// Don't do anything on transaction begin.
        /// </summary>
        /// <param name="session"></param>
        protected override void OnBeginTransaction(IDataSession session)
        {
            // Do nothing.
        }

        #endregion
    }
}
