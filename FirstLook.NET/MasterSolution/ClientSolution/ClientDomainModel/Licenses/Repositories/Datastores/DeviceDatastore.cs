﻿using System;
using System.Data;
using System.Reflection;
using FirstLook.Client.DomainModel.Licenses.Model;
using FirstLook.Common.Core.Data;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Datastores
{
    /// <summary>
    /// Device datastore.
    /// </summary>
    internal class DeviceDatastore : SessionDataStore, IDeviceDatastore
    {
        #region Miscellaneous

        /// <summary>
        /// Prefix for resources.
        /// </summary>
        internal const string Prefix = "FirstLook.Client.DomainModel.Licenses.Repositories.Resources";

        /// <summary>
        /// Get the current assembly.
        /// </summary>
        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        #endregion   

        /// <summary>
        /// Fetch the device with the given handle.
        /// </summary>
        /// <param name="handle">Device handle.</param>
        /// <returns>Data reader with the device.</returns>
        public IDataReader Device_Fetch(Guid handle)
        {
            const string queryName = Prefix + ".DeviceDatastore_Device_Fetch_Handle.txt";

            return Query(new[] { "Device" },
                         queryName,
                         new Parameter("Handle", handle, DbType.Guid));
        }

        /// <summary>
        /// Insert a device of the given type.
        /// </summary>
        /// <param name="deviceType">Device type.</param>
        /// <returns>Data reader with the device that was saved.</returns>
        public IDataReader Device_Insert(DeviceType deviceType)
        {
            const string queryNameI = Prefix + ".DeviceDatastore_Device_Insert.txt";

            const string queryNameF = Prefix + ".DeviceDatastore_Device_Fetch_Identity.txt";

            NonQuery(queryNameI,
                     new Parameter("DeviceTypeID", deviceType, DbType.Byte));

            return Query(new[] { "Device" }, queryNameF);
        }
    }
}
