﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Licenses.Model;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Datastores
{
    /// <summary>
    /// Interface for a device datastore.
    /// </summary>
    internal interface IDeviceDatastore
    {
        /// <summary>
        /// Fetch the device with the given handle.
        /// </summary>
        /// <param name="handle">Device handle.</param>
        /// <returns>Data reader with the device.</returns>
        IDataReader Device_Fetch(Guid handle);

        /// <summary>
        /// Insert a device of the given type.
        /// </summary>
        /// <param name="deviceType">Device type.</param>
        /// <returns>Data reader with the device handle.</returns>
        IDataReader Device_Insert(DeviceType deviceType);
    }
}
