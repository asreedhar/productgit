﻿using System.Data;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Datastores
{
    /// <summary>
    /// Interface for a lease datastore. Offers data operations on leases, license-to-lease mappings, lease-to-user 
    /// mappings, and lease-to-device mappings.
    /// </summary>
    internal interface ILeaseDatastore
    {
        #region Lease

        /// <summary>
        /// Fetch the identifying details of the current leases tied to the given license.
        /// </summary>        
        /// <param name="licenseId">License identifier.</param>
        /// <returns>Data reader with identifying details of leases.</returns>
        IDataReader Leases_Fetch(int licenseId);

        /// <summary>
        /// Fetch the lease with the given identifiers.
        /// </summary>        
        /// <param name="licenseId">License identifier.</param>
        /// <param name="userId">User identifier.</param>
        /// <returns>Data reader with the lease.</returns>
        IDataReader Lease_Fetch(int licenseId, int userId);
        
        /// <summary>
        /// Insert a new lease.
        /// </summary>
        /// <returns>Data reader with the lease identifier.</returns>
        IDataReader Lease_Insert();

        #endregion

        #region License_Lease

        /// <summary>
        /// Fetch the association between a license and a lease.
        /// </summary>
        /// <param name="licenseId">License identifier.</param>
        /// <param name="leaseId">Lease identifier.</param>
        /// <returns>Data reader with the association.</returns>
        IDataReader License_Lease_Fetch(int licenseId, int leaseId);

        /// <summary>
        /// Insert the association of a license with a lease.
        /// </summary>
        /// <param name="licenseId">License identifier.</param>
        /// <param name="leaseId">Lease identifier.</param>
        /// <param name="associationId">Association identifier.</param>
        void License_Lease_Insert(int licenseId, int leaseId, int associationId);

        #endregion

        #region Lease_User

        /// <summary>
        /// Fetch the association between a user and a lease.
        /// </summary>        
        /// <param name="leaseId">Lease identifier.</param>
        /// <param name="userId">User identifier.</param>
        /// <returns>Data reader with the details of the association.</returns>
        IDataReader Lease_User_Fetch(int leaseId, int userId);

        /// <summary>
        /// Insert the association of a lease with a user.
        /// </summary>        
        /// <param name="leaseId">Lease identifier.</param>
        /// <param name="userId">User identifier.</param>
        /// <param name="associationId">Association identifier.</param>
        void Lease_User_Insert(int leaseId, int userId, int associationId);

        #endregion

        #region Lease_User_History

        /// <summary>
        /// Fetch the history of the association betweena user and a lease.
        /// </summary>
        /// <param name="leaseId">Lease identifier.</param>
        /// <param name="userId">User identifier.</param>
        /// <returns>Data reader with the details of the history of the association.</returns>
        IDataReader Lease_User_History_Fetch(int leaseId, int userId);

        /// <summary>
        /// Insert an historical record for the association of a lease with a user.
        /// </summary>
        /// <param name="leaseId">Lease identifier.</param>
        /// <param name="userId">User identifier.</param>
        void Lease_User_History_Insert(int leaseId, int userId);

        #endregion        
    }
}
