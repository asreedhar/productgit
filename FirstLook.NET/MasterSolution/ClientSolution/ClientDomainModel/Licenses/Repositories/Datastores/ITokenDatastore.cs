﻿using System;
using System.Data;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Datastores
{
    /// <summary>
    /// Interface for a token datastore.
    /// </summary>
    internal interface ITokenDatastore
    {
        /// <summary>
        /// Fetch all tokens tied to the given license.
        /// </summary>
        /// <param name="license">License handle.</param>
        /// <returns>Data reader with a list of tokens.</returns>
        IDataReader Lease_Device_Fetch_By_License(Guid license);

        /// <summary>
        /// Fetch all tokens tied to the given user.
        /// </summary>
        /// <param name="userId">User identifier.</param>
        /// <returns>Data reader with a list of tokens.</returns>
        IDataReader Lease_Device_Fetch(int userId);

        /// <summary>
        /// Fetch the tokens for the given user and lease.
        /// </summary>
        /// <param name="userId">User identifier.</param>
        /// <param name="leaseId">Lease identifier.</param>        
        /// <returns>Data reader with the tokens.</returns>
        IDataReader Lease_Device_Fetch(int userId, int leaseId);

        /// <summary>
        /// Fetch the token for the leased user on the device.
        /// </summary>
        /// <param name="userId">User identifier.</param>
        /// <param name="leaseId">Lease identifier.</param>        
        /// <param name="device">Device handle.</param>
        /// <returns>Data reader with the token.</returns>
        IDataReader Lease_Device_Fetch(int userId, int leaseId, Guid device);

        /// <summary>
        /// Fetch a token.
        /// </summary>                
        /// <param name="token">Token handle.</param>
        /// <returns>Data reader with the token.</returns>
        IDataReader Lease_Device_Fetch(Guid token);

        /// <summary>
        /// Insert a new token.
        /// </summary>
        /// <param name="userId">User identifier.</param>
        /// <param name="leaseId">Lease identifier.</param>
        /// <param name="deviceId">Device identifier.</param>
        /// <param name="expiresOn">Date of expiration.</param>
        /// <param name="associationId">Association identifier.</param>
        /// <returns>Data reader with the new token.</returns>
        IDataReader Lease_Device_Insert(int userId, int leaseId, int deviceId, DateTime expiresOn, int associationId);

        /// <summary>
        /// Update the expiration date of the token with the given handle.
        /// </summary>
        /// <param name="handle">Token handle.</param>
        /// <param name="expiresOn">Date of expiration.</param>
        /// <returns>Data reader with the token.</returns>
        IDataReader Lease_Device_Update(Guid handle, DateTime expiresOn);
    }
}
