﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Datastores
{
    /// <summary>
    /// Module for registering repository datastores.
    /// </summary>
    internal class Module : IModule
    {
        /// <summary>
        /// Register repository datastores.
        /// </summary>        
        public void Configure(IRegistry registry)
        {
            registry.Register<IDeviceDatastore,  DeviceDatastore>(ImplementationScope.Shared);
            registry.Register<ILeaseDatastore,   LeaseDatastore>(ImplementationScope.Shared);
            registry.Register<ILicenseDatastore, LicenseDatastore>(ImplementationScope.Shared);
            registry.Register<IProductDatastore, ProductDatastore>(ImplementationScope.Shared);
            registry.Register<ITokenDatastore,   TokenDatastore>(ImplementationScope.Shared);
        }
    }
}
