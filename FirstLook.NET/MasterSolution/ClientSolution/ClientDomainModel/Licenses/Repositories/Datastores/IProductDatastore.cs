﻿using System.Data;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Datastores
{
    /// <summary>
    /// Interface for a product datastore.
    /// </summary>
    internal interface IProductDatastore
    {
        /// <summary>
        /// Fetch all products.
        /// </summary>
        /// <returns>Data reader with name and identifiers of all products.</returns>
        IDataReader Products_Fetch();

        /// <summary>
        /// Fetch the product with the given name.
        /// </summary>
        /// <param name="name">Product name.</param>
        /// <returns>Data reader with name and identifier of the product.</returns>
        IDataReader Product_Fetch(string name);
    }
}
