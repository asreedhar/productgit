﻿using System;
using System.Data;
using System.Reflection;
using FirstLook.Common.Core.Data;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Datastores
{
    /// <summary>
    /// Token datastore.
    /// </summary>
    internal class TokenDatastore : SessionDataStore, ITokenDatastore
    {
        #region Miscellaneous

        /// <summary>
        /// Prefix for resources.
        /// </summary>
        internal const string Prefix = "FirstLook.Client.DomainModel.Licenses.Repositories.Resources";

        /// <summary>
        /// Get the current assembly.
        /// </summary>
        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        #endregion

        /// <summary>
        /// Fetch all tokens tied to the given license.
        /// </summary>
        /// <param name="license">License handle.</param>
        /// <returns>Data reader with a list of tokens.</returns>
        public IDataReader Lease_Device_Fetch_By_License(Guid license)
        {
            const string queryName = Prefix + ".TokenDatastore_Lease_Device_Fetch_License.txt";

            return Query(new[] { "Lease_Device" },
                         queryName,
                         new Parameter("License", license, DbType.Guid));
        }

        /// <summary>
        /// Fetch all tokens tied to the given user.
        /// </summary>
        /// <param name="userId">User identifier.</param>
        /// <returns>Data reader with a list of tokens.</returns>
        public IDataReader Lease_Device_Fetch(int userId)
        {
            const string queryName = Prefix + ".TokenDatastore_Lease_Device_Fetch_User.txt";

            return Query(new[] { "Lease_Device" },
                         queryName,
                         new Parameter("UserID", userId, DbType.Int32));
        }

        /// <summary>
        /// Fetch the tokens for the given user and lease.
        /// </summary>
        /// <param name="userId">User identifier.</param>
        /// <param name="leaseId">Lease identifier.</param>        
        /// <returns>Data reader with the tokens.</returns>
        public IDataReader Lease_Device_Fetch(int userId, int leaseId)
        {
            const string queryName = Prefix + ".TokenDatastore_Lease_Device_Fetch_Lease.txt";

            return Query(new[] { "Lease_Device" },
                         queryName,                         
                         new Parameter("UserID",  userId,  DbType.Int32),
                         new Parameter("LeaseID", leaseId, DbType.Int32));
        }

        /// <summary>
        /// Fetch the token for the leased user on the device.
        /// </summary>
        /// <param name="userId">User identifier.</param>
        /// <param name="leaseId">Lease identifier.</param>        
        /// <param name="device">Device handle.</param>
        /// <returns>Data reader with the token.</returns>
        public IDataReader Lease_Device_Fetch(int userId, int leaseId, Guid device)
        {
            const string queryName = Prefix + ".TokenDatastore_Lease_Device_Fetch_Device.txt";

            return Query(new[] { "Lease_Device" },
                         queryName,
                         new Parameter("UserID",  userId,  DbType.Int32),
                         new Parameter("LeaseID", leaseId, DbType.Int32),
                         new Parameter("Device",  device,  DbType.Guid));
        }

        /// <summary>
        /// Fetch a token.
        /// </summary>                
        /// <param name="token">Token handle.</param>
        /// <returns>Data reader with the token.</returns>
        public IDataReader Lease_Device_Fetch(Guid token)
        {
            const string queryName = Prefix + ".TokenDatastore_Lease_Device_Fetch_Token.txt";

            return Query(new[] { "Lease_Device" },
                         queryName,                                                  
                         new Parameter("Token", token, DbType.Guid));
        }

        /// <summary>
        /// Insert the association of a lease, a user and a device.
        /// </summary>
        /// <param name="userId">User identifier.</param>
        /// <param name="leaseId">Lease identifier.</param>
        /// <param name="deviceId">Device identifier.</param>
        /// <param name="expiresOn">Date of expiration.</param>
        /// <param name="associationId">Association identifier.</param>
        /// <returns>Data reader with the new token.</returns>
        public IDataReader Lease_Device_Insert(int userId, int leaseId, int deviceId, DateTime expiresOn, int associationId)
        {
            const string queryNameI = Prefix + ".TokenDatastore_Lease_Device_Insert.txt";

            const string queryNameF = Prefix + ".TokenDatastore_Lease_Device_Fetch_Lease.txt";

            NonQuery(queryNameI,
                     new Parameter("UserID",        userId,        DbType.Int32),
                     new Parameter("LeaseID",       leaseId,       DbType.Int32),
                     new Parameter("DeviceID",      deviceId,      DbType.Int32),
                     new Parameter("ExpiresOn",     expiresOn,     DbType.DateTime),
                     new Parameter("AssociationID", associationId, DbType.Int32));

            return Query(new[] { "Lease_Device" }, 
                         queryNameF,
                         new Parameter("UserID",  userId,  DbType.Int32),
                         new Parameter("LeaseID", leaseId, DbType.Int32));
        }

        /// <summary>
        /// Update the expiration date of the token with the given handle.
        /// </summary>
        /// <param name="handle">Token handle.</param>
        /// <param name="expiresOn">Date of expiration.</param>
        /// <returns>Data reader with the token.</returns>
        public IDataReader Lease_Device_Update(Guid handle, DateTime expiresOn)
        {
            const string queryNameU = Prefix + ".TokenDatastore_Lease_Device_Update.txt";

            const string queryNameF = Prefix + ".TokenDatastore_Lease_Device_Fetch_Token.txt";

            NonQuery(queryNameU,
                     new Parameter("Token",     handle,    DbType.Guid),                     
                     new Parameter("ExpiresOn", expiresOn, DbType.DateTime));            

            return Query(new[] { "Lease_Device" },
                         queryNameF,
                         new Parameter("Token", handle, DbType.Guid));            
        }
    }
}
