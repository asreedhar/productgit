﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Licenses.Model;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Datastores
{
    /// <summary>
    /// Interface for a license datastore.
    /// </summary>
    internal interface ILicenseDatastore
    {
        #region License

        /// <summary>
        /// Fetch the details of all current licenses tied to the client with the given identifier.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <returns>Data reader with a list of the identifying details of the client's licenses.</returns>
        IDataReader Licenses_Fetch(int clientId);

        /// <summary>
        /// Fetch the details of a particular license.
        /// </summary>        
        /// <param name="license">License handle.</param>
        /// <returns>Data reader with license details.</returns>
        IDataReader License_Fetch(Guid license);

        /// <summary>
        /// Insert a license row.
        /// </summary>
        /// <param name="licenseModel">Type of license model.</param>
        /// <param name="productId">Licensed product identifier.</param>
        /// <param name="deviceType">Type of device.</param>
        /// <param name="uses">Number of uses.</param>
        /// <param name="auditRowId">Audit row identifier.</param>
        /// <returns>Data reader with license identifier.</returns>
        IDataReader License_Insert(LicenseModelType licenseModel, int productId, DeviceType deviceType, int uses, int auditRowId);

        /// <summary>
        /// Update the number of uses of a license.
        /// </summary>
        /// <param name="licenseId">License identifier.</param>
        /// <param name="licenseModel">License model.</param>
        /// <param name="uses">Number of times the license has been used.</param>
        void License_Update(int licenseId, LicenseModelType licenseModel, int uses);
       
        #endregion

        #region Client_License

        /// <summary>
        /// Fetch the association between a client and a license.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="licenseId">License identifier.</param>
        /// <returns>Data reader with the association details.</returns>
        IDataReader Client_License_Fetch(int clientId, int licenseId);

        /// <summary>
        /// Insert the association of a client with a license.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="licenseId">License identifier.</param>
        /// <param name="associationId">Association identifier.</param>
        void Client_License_Insert(int clientId, int licenseId, int associationId);

        #endregion

        #region License Parameters

        /// <summary>
        /// Fetch the identifying details of all parameters tied to a license.
        /// </summary>
        /// <param name="licenseId">License identifier.</param>
        /// <returns>Data reader with list of parameter details.</returns>
        IDataReader Parameters_Fetch(int licenseId);

        /// <summary>
        /// Insert a base record of a license parameter.
        /// </summary>
        /// <param name="licenseId">License identifier.</param>
        /// <param name="parameterType">Type of the license parameter.</param>
        /// <returns>Data reader with the license parameter identifier.</returns>
        IDataReader Parameter_Insert(int licenseId, LicenseParameterType parameterType);

        /// <summary>
        /// Fetch the number of seats allowed for a license.
        /// </summary>        
        /// <param name="licenseParameterId">License parameter identifier.</param>
        /// <returns>Data reader with the number of allowed seats.</returns>
        IDataReader Seats_Fetch(int licenseParameterId);

        /// <summary>
        /// Insert the number of seats allowed for a license.
        /// </summary>
        /// <param name="licenseParameterId">License parameter identifier.</param>
        /// <param name="quantity">Number of allowed seats.</param>
        /// <param name="auditRowId">Audit row identifier.</param>
        void Seats_Insert(int licenseParameterId, int quantity, int auditRowId);

        /// <summary>
        /// Fetch the usage limit for a license.
        /// </summary>        
        /// <param name="licenseParameterId">License parameter identifier.</param>
        /// <returns>Data reader with the usage limit.</returns>
        IDataReader UsageLimit_Fetch(int licenseParameterId);

        /// <summary>
        /// Insert the number of allowed uses for a license.
        /// </summary>
        /// <param name="licenseParameterId">License parameter identifier.</param>
        /// <param name="uses">Number of allowed uses.</param>
        /// <param name="auditRowId">Audit row identifier.</param>
        void UsageLimit_Insert(int licenseParameterId, int uses, int auditRowId);

        /// <summary>
        /// Fetch the expiration date of a license.
        /// </summary>        
        /// <param name="licenseParameterId">License parameter identifier.</param>
        /// <returns>Data reader with the expiration date.</returns>
        IDataReader Expiration_Fetch(int licenseParameterId);

        /// <summary>
        /// Insert the expiration date of a license.
        /// </summary>
        /// <param name="licenseParameterId">License parameter identifier.</param>
        /// <param name="expirationDate">Expiration date.</param>
        /// <param name="auditRowId">Audit row identifier.</param>
        void Expiration_Insert(int licenseParameterId, DateTime expirationDate, int auditRowId);

        #endregion        
    }
}
