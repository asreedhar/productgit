﻿using System.Data;
using System.Reflection;
using FirstLook.Common.Core.Data;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Datastores
{
    /// <summary>
    /// Lease datastore.
    /// </summary>
    internal class LeaseDatastore : SessionDataStore, ILeaseDatastore
    {
        #region Miscellaneous

        /// <summary>
        /// Prefix for resources.
        /// </summary>
        internal const string Prefix = "FirstLook.Client.DomainModel.Licenses.Repositories.Resources";

        /// <summary>
        /// Get the current assembly.
        /// </summary>
        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        #endregion        

        #region Lease

        /// <summary>
        /// Fetch the identifying details of the current leases tied to the given license.
        /// </summary>        
        /// <param name="licenseId">License identifier.</param>
        /// <returns>Data reader with identifying details of leases.</returns>
        public IDataReader Leases_Fetch(int licenseId)
        {
            const string queryName = Prefix + ".LeaseDatastore_Leases_Fetch.txt";

            return Query(new[] { "Leases" },
                         queryName,                         
                         new Parameter("LicenseID", licenseId, DbType.Int32));
        }

        /// <summary>
        /// Fetch the lease with the given identifiers.
        /// </summary>        
        /// <param name="licenseId">License identifier.</param>
        /// <param name="userId">User identifier.</param>
        /// <returns>Data reader with the lease.</returns>
        public IDataReader Lease_Fetch(int licenseId, int userId)
        {
            const string queryName = Prefix + ".LeaseDatastore_Lease_Fetch.txt";

            return Query(new[] { "Lease" },
                         queryName,
                         new Parameter("LicenseID", licenseId, DbType.Int32),
                         new Parameter("UserID",    userId,    DbType.Int32));
        }

        /// <summary>
        /// Insert a new lease.
        /// </summary>
        /// <returns>Data reader with the lease identifier.</returns>
        public IDataReader Lease_Insert()
        {
            const string queryNameI = Prefix + ".LeaseDatastore_Lease_Insert.txt";

            const string queryNameF = Prefix + ".LeaseDatastore_Lease_Fetch_Identity.txt";

            NonQuery(queryNameI);

            return Query(new[] { "Lease" }, queryNameF);
        }

        #endregion

        #region License_Lease

        /// <summary>
        /// Fetch the association between a license and a lease.
        /// </summary>
        /// <param name="licenseId">License identifier.</param>
        /// <param name="leaseId">Lease identifier.</param>
        /// <returns>Data reader with the association.</returns>
        public IDataReader License_Lease_Fetch(int licenseId, int leaseId)
        {
            const string queryName = Prefix + ".LeaseDatastore_License_Lease_Fetch.txt";

            return Query(new[] { "License_Lease" },
                         queryName,
                         new Parameter("LicenseID", licenseId, DbType.Int32),
                         new Parameter("LeaseID",   leaseId,   DbType.Int32));
        }

        /// <summary>
        /// Insert the association of a license with a lease.
        /// </summary>
        /// <param name="licenseId">License identifier.</param>
        /// <param name="leaseId">Lease identifier.</param>
        /// <param name="associationId">Association identifier.</param>
        public void License_Lease_Insert(int licenseId, int leaseId, int associationId)
        {
            const string queryName = Prefix + ".LeaseDatastore_License_Lease_Insert.txt";

            NonQuery(queryName,
                     new Parameter("LicenseID",     licenseId,     DbType.Int32),
                     new Parameter("LeaseID",       leaseId,       DbType.Int32),
                     new Parameter("AssociationID", associationId, DbType.Int32));
        }

        #endregion

        #region Lease_User

        /// <summary>
        /// Fetch the association between a user and a lease.
        /// </summary>        
        /// <param name="leaseId">Lease identifier.</param>
        /// <param name="userId">User identifier.</param>
        /// <returns>Data reader with the details of the association.</returns>
        public IDataReader Lease_User_Fetch(int leaseId, int userId)
        {
            const string queryName = Prefix + ".LeaseDatastore_Lease_User_Fetch.txt";

            return Query(new[] { "Lease_User" },
                         queryName,                         
                         new Parameter("LeaseID", leaseId, DbType.Int32),
                         new Parameter("UserID",  userId,  DbType.Int32));
        }

        /// <summary>
        /// Insert the association of a lease with a user.
        /// </summary>        
        /// <param name="leaseId">Lease identifier.</param>
        /// <param name="userId">User identifier.</param>
        /// <param name="associationId">Association identifier.</param>
        public void Lease_User_Insert(int leaseId, int userId, int associationId)
        {
            const string queryName = Prefix + ".LeaseDatastore_Lease_User_Insert.txt";

            NonQuery(queryName,                
                     new Parameter("LeaseID",       leaseId,       DbType.Int32),
                     new Parameter("UserID",        userId,        DbType.Int32),
                     new Parameter("AssociationID", associationId, DbType.Int32));
        }

        #endregion

        #region Lease_User_History

        /// <summary>
        /// Fetch the history of the association betweena user and a lease.
        /// </summary>
        /// <param name="leaseId">Lease identifier.</param>
        /// <param name="userId">User identifier.</param>
        /// <returns>Data reader with the details of the history of the association.</returns>
        public IDataReader Lease_User_History_Fetch(int leaseId, int userId)
        {
            const string queryName = Prefix + ".LeaseDatastore_Lease_User_History_Fetch.txt";

            return Query(new[] { "Lease_User_History" },
                         queryName,                         
                         new Parameter("LeaseID", leaseId, DbType.Int32),
                         new Parameter("UserID",  userId,  DbType.Int32));
        }

        /// <summary>
        /// Insert an historical record for the association of a lease with a user.
        /// </summary>
        /// <param name="leaseId">Lease identifier.</param>
        /// <param name="userId">User identifier.</param>
        public void Lease_User_History_Insert(int leaseId, int userId)
        {
            const string queryName = Prefix + ".LeaseDatastore_Lease_User_History_Insert.txt";

            NonQuery(queryName,
                     new Parameter("LeaseID", leaseId, DbType.Int32),
                     new Parameter("UserID", userId, DbType.Int32));            
        }

        #endregion
    }
}
