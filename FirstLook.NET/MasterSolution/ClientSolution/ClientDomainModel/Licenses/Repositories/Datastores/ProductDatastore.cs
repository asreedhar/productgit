﻿using System;
using System.Data;
using System.Reflection;
using FirstLook.Common.Core.Data;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Datastores
{
    /// <summary>
    /// Product datastore.
    /// </summary>
    internal class ProductDatastore : SessionDataStore, IProductDatastore
    {
        #region Miscellaneous

        /// <summary>
        /// Prefix for resources.
        /// </summary>
        internal const string Prefix = "FirstLook.Client.DomainModel.Licenses.Repositories.Resources";

        /// <summary>
        /// Get the current assembly.
        /// </summary>
        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        #endregion 

        /// <summary>
        /// Fetch all products.
        /// </summary>
        /// <returns>Data reader with name and identifiers of all products.</returns>
        public IDataReader Products_Fetch()
        {
            const string queryName = Prefix + ".ProductDatastore_Products_Fetch.txt";

            return Query(new[] { "Products" },
                         queryName);
        }

        /// <summary>
        /// Fetch the product with the given name.
        /// </summary>
        /// <param name="name">Product name.</param>
        /// <returns>Data reader with name and identifier of the product.</returns>
        public IDataReader Product_Fetch(string name)
        {
            const string queryName = Prefix + ".ProductDatastore_Product_Fetch.txt";

            return Query(new[] {"Product"},
                         queryName,
                         new Parameter("Name", name, DbType.String));
        }
    }
}
