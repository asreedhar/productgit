﻿using System;
using System.Data;
using System.Reflection;
using FirstLook.Client.DomainModel.Licenses.Model;
using FirstLook.Common.Core.Data;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Datastores
{
    /// <summary>
    /// License datastore.
    /// </summary>
    internal class LicenseDatastore : SessionDataStore, ILicenseDatastore
    {
        #region Miscellaneous

        /// <summary>
        /// Prefix for resources.
        /// </summary>
        internal const string Prefix = "FirstLook.Client.DomainModel.Licenses.Repositories.Resources";

        /// <summary>
        /// Get the current assembly.
        /// </summary>
        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        #endregion        

        #region License

        /// <summary>
        /// Fetch the details of all current and valid licenses tied to the client with the given identifier.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <returns>Data reader with a list of the identifying details of the client's valid licenses.</returns>
        public IDataReader Licenses_Fetch(int clientId)
        {
            const string queryName = Prefix + ".LicenseDatastore_Licenses_Fetch.txt";

            return Query(new[] { "Licenses" },
                         queryName,
                         new Parameter("ClientID", clientId, DbType.Int32));
        }

        /// <summary>
        /// Fetch the details of a particular license.
        /// </summary>        
        /// <param name="license">License handle.</param>
        /// <returns>Data reader with license details.</returns>
        public IDataReader License_Fetch(Guid license)
        {
            const string queryName = Prefix + ".LicenseDatastore_License_Fetch_Handle.txt";

            return Query(new[] { "License" },
                         queryName,                         
                         new Parameter("Handle",   license,  DbType.Guid));
        }

        /// <summary>
        /// Insert a license row.
        /// </summary>
        /// <param name="licenseModel">Type of license model.</param>
        /// <param name="productId">Licensed product identifier.</param>
        /// <param name="deviceType">Type of device.</param>
        /// <param name="uses">Number of uses.</param>
        /// <param name="auditRowId">Audit row identifier.</param>
        /// <returns>Data reader with license identifier.</returns>
        public IDataReader License_Insert(LicenseModelType licenseModel, int productId, DeviceType deviceType, int uses, int auditRowId)
        {
            const string queryNameI = Prefix + ".LicenseDatastore_License_Insert.txt";

            const string queryNameF = Prefix + ".LicenseDatastore_License_Fetch_Identity.txt";

            NonQuery(queryNameI, 
                     new Parameter("LicenseModelID", (byte)licenseModel, DbType.Byte),
                     new Parameter("ProductID",      productId,          DbType.Int16),
                     new Parameter("DeviceTypeID",   (byte)deviceType,   DbType.Byte),
                     new Parameter("Uses",           uses,               DbType.Int32),
                     new Parameter("AuditRowID",     auditRowId,         DbType.Int32));

            return Query(new[] { "License" }, queryNameF);
        }

        /// <summary>
        /// Update the number of uses of a license.
        /// </summary>
        /// <param name="licenseId">License identifier.</param>
        /// <param name="licenseModel">License model.</param>
        /// <param name="uses">Number of times the license has been used.</param>
        public void License_Update(int licenseId, LicenseModelType licenseModel, int uses)
        {
            const string queryName = Prefix + ".LicenseDatastore_License_Update.txt";

            NonQuery(queryName,
                     new Parameter("LicenseID",      licenseId,          DbType.Int32),
                     new Parameter("LicenseModelID", (byte)licenseModel, DbType.Byte),
                     new Parameter("Uses",           uses,               DbType.Int32));
        }        

        #endregion

        #region Client_License

        /// <summary>
        /// Fetch the association between a client and a license.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="licenseId">License identifier.</param>
        /// <returns>Data reader with the association details.</returns>
        public IDataReader Client_License_Fetch(int clientId, int licenseId)
        {
            const string queryName = Prefix + ".LicenseDatastore_Client_License_Fetch.txt";

            return Query(new[] { "Client_License" },
                         queryName,
                         new Parameter("ClientID",  clientId, DbType.Int32),
                         new Parameter("LicenseID", licenseId, DbType.Int32));
        }

        /// <summary>
        /// Insert the association of a client with a license.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="licenseId">License identifier.</param>
        /// <param name="associationId">Association identifier.</param>
        public void Client_License_Insert(int clientId, int licenseId, int associationId)
        {
            const string queryName = Prefix + ".LicenseDatastore_Client_License_Insert.txt";

            NonQuery(queryName,
                     new Parameter("ClientID",      clientId, DbType.Int32),
                     new Parameter("LicenseID",     licenseId, DbType.Int32),
                     new Parameter("AssociationID", associationId, DbType.Int32));
        }

        #endregion

        #region License Parameters

        /// <summary>
        /// Fetch the identifying details of all parameters tied to a license.
        /// </summary>
        /// <param name="licenseId">License identifier.</param>
        /// <returns>Data reader with list of parameter details.</returns>
        public IDataReader Parameters_Fetch(int licenseId)
        {
            const string queryName = Prefix + ".LicenseDatastore_Parameters_Fetch.txt";

            return Query(new[] { "Parameters" },
                         queryName,
                         new Parameter("LicenseID", licenseId, DbType.Int32));
        }

        /// <summary>
        /// Insert a base record of a license parameter.
        /// </summary>
        /// <param name="licenseId">License identifier.</param>
        /// <param name="parameterType">Type of the license parameter.</param>
        /// <returns>Data reader with the license parameter identifier.</returns>
        public IDataReader Parameter_Insert(int licenseId, LicenseParameterType parameterType)
        {
            const string queryNameI = Prefix + ".LicenseDatastore_Parameter_Insert.txt";

            const string queryNameF = Prefix + ".LicenseDatastore_Parameter_Fetch_Identity.txt";

            NonQuery(queryNameI,
                     new Parameter("LicenseID",              licenseId,           DbType.Int32),
                     new Parameter("LicenseParameterTypeID", (byte)parameterType, DbType.Byte));

            return Query(new[] { "Parameter" }, queryNameF);   
        }

        /// <summary>
        /// Fetch the number of seats allowed for a license.
        /// </summary>        
        /// <param name="licenseParameterId">License parameter identifier.</param>
        /// <returns>Data reader with the number of allowed seats.</returns>
        public IDataReader Seats_Fetch(int licenseParameterId)
        {
            const string queryName = Prefix + ".LicenseDatastore_Seats_Fetch.txt";

            return Query(new[] { "Seats" },
                         queryName,                         
                         new Parameter("LicenseParameterID", licenseParameterId, DbType.Int32));   
        }

        /// <summary>
        /// Insert the number of seats allowed for a license.
        /// </summary>
        /// <param name="licenseParameterId">License parameter identifier.</param>
        /// <param name="quantity">Number of allowed seats.</param>
        /// <param name="auditRowId">Audit row identifier.</param>
        public void Seats_Insert(int licenseParameterId, int quantity, int auditRowId)
        {
            const string queryName = Prefix + ".LicenseDatastore_Seats_Insert.txt";

            NonQuery(queryName,
                     new Parameter("LicenseParameterID", licenseParameterId, DbType.Int32),
                     new Parameter("Quantity",           quantity,           DbType.Int32),
                     new Parameter("AuditRowID",         auditRowId,         DbType.Int32));
        }

        /// <summary>
        /// Fetch the usage limit for a license.
        /// </summary>        
        /// <param name="licenseParameterId">License parameter identifier.</param>
        /// <returns>Data reader with the usage limit.</returns>
        public IDataReader UsageLimit_Fetch(int licenseParameterId)
        {
            const string queryName = Prefix + ".LicenseDatastore_UsageLimit_Fetch.txt";

            return Query(new[] { "UsageLimit"},
                         queryName,                         
                         new Parameter("LicenseParameterID", licenseParameterId, DbType.Int32));
        }

        /// <summary>
        /// Insert the number of allowed uses for a license.
        /// </summary>
        /// <param name="licenseParameterId">License parameter identifier.</param>
        /// <param name="uses">Number of allowed uses.</param>
        /// <param name="auditRowId">Audit row identifier.</param>
        public void UsageLimit_Insert(int licenseParameterId, int uses, int auditRowId)
        {
            const string queryName = Prefix + ".LicenseDatastore_UsageLimit_Insert.txt";

            NonQuery(queryName,
                     new Parameter("LicenseParameterID", licenseParameterId, DbType.Int32),
                     new Parameter("Uses",               uses,               DbType.Int32),
                     new Parameter("AuditRowID",         auditRowId,         DbType.Int32));   
        }

        /// <summary>
        /// Fetch the expiration date of a license.
        /// </summary>        
        /// <param name="licenseParameterId">License parameter identifier.</param>
        /// <returns>Data reader with the expiration date.</returns>
        public IDataReader Expiration_Fetch(int licenseParameterId)
        {
            const string queryName = Prefix + ".LicenseDatastore_Expiration_Fetch.txt";

            return Query(new[] { "Expiration" },
                         queryName,                         
                         new Parameter("LicenseParameterID", licenseParameterId, DbType.Int32));
        }

        /// <summary>
        /// Insert the expiration date of a license.
        /// </summary>
        /// <param name="licenseParameterId">License parameter identifier.</param>
        /// <param name="expirationDate">Expiration date.</param>
        /// <param name="auditRowId">Audit row identifier.</param>
        public void Expiration_Insert(int licenseParameterId, DateTime expirationDate, int auditRowId)
        {
            const string queryName = Prefix + ".LicenseDatastore_Expiration_Insert.txt";

            NonQuery(queryName,
                     new Parameter("LicenseParameterID", licenseParameterId, DbType.Int32),
                     new Parameter("ExpirationDate",     expirationDate,     DbType.DateTime),
                     new Parameter("AuditRowID",         auditRowId,         DbType.Int32));
        }

        #endregion        
    }
}
