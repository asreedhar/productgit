﻿using FirstLook.Client.DomainModel.Licenses.Model;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Licenses.Repositories
{
    /// <summary>
    /// Module for registering repository components.
    /// </summary>
    internal class Module : IModule
    {
        /// <summary>
        /// Register repository components.
        /// </summary>        
        public void Configure(IRegistry registry)
        {
            registry.Register<Datastores.Module>();
            registry.Register<Serializers.Module>();

            registry.Register<ILicenseRepository, LicenseRepository>(ImplementationScope.Shared);
        }
    }
}
