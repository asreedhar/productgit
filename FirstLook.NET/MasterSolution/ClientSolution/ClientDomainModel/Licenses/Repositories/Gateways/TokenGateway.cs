﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Gateways;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Licenses.Model;
using FirstLook.Client.DomainModel.Licenses.Repositories.Datastores;
using FirstLook.Client.DomainModel.Licenses.Repositories.Entities;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Gateways
{
    /// <summary>
    /// Token data gateway.
    /// </summary>
    internal class TokenGateway : AssociationGateway
    {
        /// <summary>
        /// Fetch all of a user's tokens.
        /// </summary>
        /// <param name="userId">User identifier.</param>
        /// <returns>List of active tokens.</returns>
        internal IList<TokenEntity> Fetch_Tokens(int userId)
        {
            ITokenDatastore datastore = Resolve<ITokenDatastore>();
            ISerializer<TokenEntity> serializer = Resolve<ISerializer<TokenEntity>>();            

            using (IDataReader reader = datastore.Lease_Device_Fetch(userId))
            {
                return serializer.Deserialize(reader);
            }            
        }        

        /// <summary>
        /// Fetch all of a license's tokens.
        /// </summary>        
        /// <param name="license">License.</param>
        /// <returns>List of tokens.</returns>
        internal IList<TokenEntity> Fetch_Tokens(Guid license)
        {
            ITokenDatastore datastore = Resolve<ITokenDatastore>();
            ISerializer<TokenEntity> serializer = Resolve<ISerializer<TokenEntity>>();

            using (IDataReader reader = datastore.Lease_Device_Fetch_By_License(license))
            {
                return serializer.Deserialize(reader);
            }
        }

        /// <summary>
        /// Fetch the token for the licensed user on a device.
        /// </summary>        
        /// <param name="license">License handle.</param>
        /// <param name="userId">User identifier.</param>        
        /// <param name="device">Device handle.</param>
        /// <returns>Token, or null if one does not exists.</returns>
        internal TokenEntity Fetch_Token(Guid license, int userId, Guid device)
        {                        
            // The user might just not have a lease - don't throw an exception.
            LeaseEntity leaseEntity = new LeaseGateway().Fetch_Lease(license, userId);
            if (leaseEntity == null)
            {
                return null;
            }

            ITokenDatastore datastore = Resolve<ITokenDatastore>();
            ISerializer<TokenEntity> serializer = Resolve<ISerializer<TokenEntity>>();

            using (IDataReader reader = datastore.Lease_Device_Fetch(userId, leaseEntity.Id, device))
            {
                return reader.Read() ? serializer.Deserialize((IDataRecord)reader) : null;
            }            
        }

        /// <summary>
        /// Fetch the token with the given handle.
        /// </summary>        
        /// <param name="token">Token handle.</param>
        /// <returns>Token, or null if one does not exist.</returns>
        internal TokenEntity Fetch_Token(Guid token)
        {
            ITokenDatastore datastore = Resolve<ITokenDatastore>();
            ISerializer<TokenEntity> serializer = Resolve<ISerializer<TokenEntity>>();

            using (IDataReader reader = datastore.Lease_Device_Fetch(token))
            {
                if (!reader.Read())
                {
                    throw new ArgumentException("Invalid token handle.", "token");
                }

                return serializer.Deserialize((IDataRecord) reader);
            }                       
        }        
        
        /// <summary>
        /// Save a token.
        /// </summary>
        /// <param name="token">Token to save.</param>
        /// <returns>Saved token.</returns>
        internal TokenEntity Save_Token(Token token)
        {
            return token.Handle.HasValue ? Update_Token(token) : Insert_Token(token);
        }        

        /// <summary>
        /// Insert a new token. Token must have valid license and device handles.
        /// </summary>
        /// <param name="token">Token to save.</param>
        /// <returns>Inserted token.</returns>
        protected TokenEntity Insert_Token(Token token)
        {
            // Validate the license.
            LicenseEntity license = new LicenseGateway().Fetch_License(token.License);
            if (license == null)
            {
                throw new ArgumentException("Invalid license handle.");
            }

            // Validate the lease.
            LeaseEntity lease = new LeaseGateway().Fetch_Lease(token.License, token.UserId);
            if (lease == null)
            {
                throw new ArgumentException("Invalid lease identifiers.");
            }

            // Validate the device.
            DeviceEntity device = new DeviceGateway().Fetch_Device(token.Device);
            if (device == null)
            {
                throw new ArgumentException("Invalid device handle.");
            }            

            // Initialize the token.
            TokenEntity entity = new TokenEntity(token)
            {
                AssociationId = InsertAssociation().Id,
                DeviceId      = device.Id,
                LeaseId       = lease.Id,
                LicenseId     = license.Id
            };

            ITokenDatastore datastore = Resolve<ITokenDatastore>();            
            ISerializer<TokenEntity> serializer = Resolve<ISerializer<TokenEntity>>();

            // Insert the token.
            using (IDataReader reader = datastore.Lease_Device_Insert(entity.UserId, 
                                                                      entity.LeaseId, 
                                                                      entity.DeviceId, 
                                                                      entity.ExpiresOn, 
                                                                      entity.AssociationId))
            {
                if (!reader.Read())
                {
                    throw new DataException("Could not insert token.");
                }
                return serializer.Deserialize((IDataRecord)reader);
            } 
        }

        /// <summary>
        /// Update the expiration date of a token.
        /// </summary>
        /// <param name="token">Token to update.</param>
        /// <returns>Updated token.</returns>
        protected TokenEntity Update_Token(Token token)
        {
            if (!token.Handle.HasValue)
            {
                throw new ArgumentException("Invalid handle.", "token");
            }

            ITokenDatastore datastore = Resolve<ITokenDatastore>();
            ISerializer<TokenEntity> serializer = Resolve<ISerializer<TokenEntity>>();            

            using (IDataReader reader = datastore.Lease_Device_Update(token.Handle.Value, token.ExpiresOn))
            {
                if (!reader.Read())
                {
                    throw new DataException("Could not update token.");
                }
                return serializer.Deserialize((IDataRecord)reader);
            }
        }

        /// <summary>
        /// Mark all tokens for devices OTHER THAN the provided device as deleted.
        /// </summary>
        /// <param name="userId">User identifier.</param>
        /// <param name="deviceType">Type of device to delete tokens for.</param>
        /// <param name="device">Device to leave alone.</param>
        /// <returns>Number of tokens invalidated.</returns>
        internal int Delete_Tokens(int userId, DeviceType? deviceType, Guid? device)
        {           
            IEnumerable<TokenEntity> tokens;

            // If we have a device identifier, get the tokens just for other devices.
            if (device.HasValue)
            {
                DeviceEntity deviceEntity = new DeviceGateway().Fetch_Device(device.Value);
                if (deviceEntity == null)
                {
                    throw new ArgumentException("Invalid device identifier", "device");
                }

                tokens = Fetch_Tokens(userId).Where(x => x.DeviceId != deviceEntity.Id);
            }
            // No device? Get all tokens.
            else
            {
                tokens = Fetch_Tokens(userId);
            }

            IRepository auditRepository = Resolve<IRepository>();
            Association association;
            AuditRow auditRow;
            
            // Mark each token as deleted if it's the specified type.
            foreach (TokenEntity token in tokens)
            {
                if (deviceType.HasValue)
                {
                    DeviceEntity matchingDevice = new DeviceGateway().Fetch_Device(token.Device);
                    if (matchingDevice.Type != deviceType.Value)
                    {
                        continue;
                    }
                }

                association = FetchAssociation(token.AssociationId);
                if (association == null)
                {
                    throw new DataException("Could not retrieve association.");
                }

                auditRow = auditRepository.Update(association.AuditRow, false, false, true);
                InsertAssociation(token.AssociationId, auditRow.Id);                
            }
            
            return tokens.Count();
        }
    }
}
