﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Gateways;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Licenses.Model;
using FirstLook.Client.DomainModel.Licenses.Repositories.Datastores;
using FirstLook.Client.DomainModel.Licenses.Repositories.Entities;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Gateways
{
    /// <summary>
    /// License data gateway.
    /// </summary>
    internal class LicenseGateway : AssociationGateway
    {
        #region Licenses

        /// <summary>
        /// Fetch a client's licenses.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <returns>List of license details.</returns>
        internal IList<LicenseEntity> Fetch_Licenses(IBroker broker)
        {
            ILicenseDatastore datastore = Resolve<ILicenseDatastore>();

            using (IDataReader reader = datastore.Licenses_Fetch(broker.Id))
            {
                ISerializer<LicenseEntity> serializer = Resolve<ISerializer<LicenseEntity>>();
                return serializer.Deserialize(reader);
            }
        }

        /// <summary>
        /// Fetch a specific license.
        /// </summary>        
        /// <param name="license">License handle.</param>
        /// <returns>License details.</returns>
        internal LicenseEntity Fetch_License(Guid license)
        {
            ILicenseDatastore datastore = Resolve<ILicenseDatastore>();
            LicenseEntity entity;

            // Get details of the license.
            using (IDataReader reader = datastore.License_Fetch(license))
            {
                if (!reader.Read())
                {
                    throw new ArgumentException("Invalid license handle.", "license");
                }

                ISerializer<LicenseEntity> serializer = Resolve<ISerializer<LicenseEntity>>();
                entity = serializer.Deserialize((IDataRecord)reader);
            }            

            // Get license parameters.            
            foreach (ParameterEntity parameter in Fetch_Parameters(entity.Id))
            {
                switch (parameter.Type)
                {
                    case LicenseParameterType.Expiration:
                        entity.Expiration = Fetch_Expiration(parameter.Id);                        
                        break;

                    case LicenseParameterType.SeatLimit:
                        entity.SeatLimit = Fetch_Seats(parameter.Id);
                        break;                    

                    case LicenseParameterType.UsageLimit:
                        entity.UsageLimit = Fetch_UsageLimit(parameter.Id);                        
                        break;

                    default:
                        throw new DataException("Unrecognized parameter type.");
                }
            }

            return entity;
        }            
    
        /// <summary>
        /// Save a license. Will either insert a new license or udpate an existing one.
        /// </summary>        
        /// <param name="broker">Broker.</param>
        /// <param name="license">License to save.</param>
        /// <returns>License that was saved.</returns>
        internal LicenseEntity Save_License(IBroker broker, License license)
        {   
            return license.Handle == null ? Insert_License(broker, license) : Update_License(license);
        }               

        /// <summary>
        /// Insert a new license into the database.
        /// </summary>        
        /// <param name="broker">Broker.</param>
        /// <param name="license">License to save.</param>
        /// <returns>License that was saved.</returns>
        protected LicenseEntity Insert_License(IBroker broker, License license)
        {
            // Ensure we are not trying to insert a license for a product and device type  twice.
            foreach (LicenseEntity existingLicense in Fetch_Licenses(broker))
            {
                if (existingLicense.Product.Name.Equals(license.Product.Name) &&
                    existingLicense.DeviceType == license.DeviceType)
                {
                    throw new ArgumentException("Cannot save multiple licenses for a product and device type.");
                }
            }

            ILicenseDatastore datastore = Resolve<ILicenseDatastore>();
            IRepository auditRepository = Resolve<IRepository>();

            LicenseEntity entity = new LicenseEntity(license)
            {
                AuditRow = auditRepository.Insert(true, false, false),
                ClientId = broker.Id
            };

            // Insert the license details.
            using (IDataReader reader = datastore.License_Insert(entity.LicenseModel, entity.Product.Id, entity.DeviceType, 
                                                                 entity.Uses, entity.AuditRow.Id))
            {
                if (!reader.Read())
                {
                    throw new DataException("Could not save license.");
                }
                
                entity.Handle = reader.GetGuid(reader.GetOrdinal("Handle"));
                entity.Id     = reader.GetInt32(reader.GetOrdinal("LicenseID"));
            }

            // Map the license to the client.            
            Association association = InsertAssociation();
            if (association == null)
            {                
                throw new ApplicationException("Could not create association.");
            }
            datastore.Client_License_Insert(entity.ClientId, entity.Id, association.Id);

            // Insert the expiration date, if one exists.
            if (license.Expiration != null)
            {
                ExpirationEntity expiration = new ExpirationEntity
                {
                    AuditRow = auditRepository.Insert(true, false, false),
                    Id       = Insert_Parameter(entity.Id, LicenseParameterType.Expiration),
                    Date     = license.Expiration.Date
                };                                

                datastore.Expiration_Insert(expiration.Id, expiration.Date, expiration.AuditRow.Id);
                entity.Expiration = expiration;
            }

            // Insert the seats parameter, if it exists.
            if (license.SeatLimit != null)
            {
                if (license.SeatLimit.Seats < 0)
                {
                    throw new ArgumentException("Cannot set negative seat limit.");
                }

                SeatLimitEntity seats = new SeatLimitEntity
                {                                        
                    AuditRow = auditRepository.Insert(true, false, false),
                    Id       = Insert_Parameter(entity.Id, LicenseParameterType.SeatLimit),
                    Seats    = license.SeatLimit.Seats,
                };
                
                datastore.Seats_Insert(seats.Id, seats.Seats, seats.AuditRow.Id);
                entity.SeatLimit = seats;
            }            

            // Insert the usage limit parameter, if one exists.
            if (license.UsageLimit != null)
            {
                if (license.UsageLimit.Uses < 0)
                {
                    throw new ArgumentException("Cannot set negative use limit.");
                }

                UsageLimitEntity uses = new UsageLimitEntity
                {
                    AuditRow = auditRepository.Insert(true, false, false),
                    Id       = Insert_Parameter(entity.Id, LicenseParameterType.UsageLimit),
                    Uses     = license.UsageLimit.Uses
                };                

                datastore.UsageLimit_Insert(uses.Id, uses.Uses, uses.AuditRow.Id);
                entity.UsageLimit = uses;
            }            

            return entity;
        }

        /// <summary>
        /// Update an existing license in the database.
        /// </summary>
        /// <remarks>
        /// A license's product, device type and license model are immutable. If any of these values from the passed-
        /// in license are different from the existing license record, an exception will be thrown.
        /// </remarks>                
        /// <param name="license">License to update.</param>
        /// <returns>License that was updated.</returns>
        protected LicenseEntity Update_License(License license)
        {
            if (!license.Handle.HasValue)
            {
                throw new ArgumentException("Invalid handle.", "license");
            }
            
            LicenseEntity previous = Fetch_License(license.Handle.Value);
            if (previous == null)
            {
                throw new ArgumentException("Invalid handle.", "license");
            }

            LicenseEntity entity = new LicenseEntity(license)
            {
                AuditRow = previous.AuditRow,
                ClientId = previous.ClientId,
                Id       = previous.Id,
                Uses     = license.Uses
            };

            // Device type is immutable.
            if (license.DeviceType != previous.DeviceType)
            {
                throw new ArgumentException("Device type is immutable.", "license");
            }

            // Product is immutable.
            if (!license.Product.Name.Equals(previous.Product.Name))
            {
                throw new ArgumentException("Product is immutable.", "license");
            }    
     
            // Validate any license model transitions.
            if (license.LicenseModel != previous.LicenseModel)
            {
                entity.LicenseModel = license.LicenseModel;

                TokenGateway tokenGateway = new TokenGateway();
                IList<TokenEntity> tokens = tokenGateway.Fetch_Tokens(license.Handle.Value);

                // Transitioning to a floating license? Bring token expirations forward.
                if (license.LicenseModel == LicenseModelType.Floating)
                {                                                            
                    foreach (TokenEntity token in tokens)
                    {
                        token.ExpiresOn = DateTime.Now.AddMinutes(15);
                        tokenGateway.Save_Token(token);
                    }
                }
                // Transitioning away from a floating license? Push token expirations out.
                else if (previous.LicenseModel == LicenseModelType.Floating)
                {                    
                    foreach (TokenEntity token in tokens)
                    {
                        token.ExpiresOn = DateTime.Now.AddYears(100);
                        tokenGateway.Save_Token(token);
                    }
                }
                // Make sure there aren't too many leases for a named license.
                if (license.LicenseModel == LicenseModelType.Named)
                {
                    int count = tokens.Where(x => x.ExpiresOn > DateTime.Now).Count();
                    if (license.SeatLimit != null && count > license.SeatLimit.Seats)
                    {
                        throw new ArgumentException("More leases exist than desired number of named seats.");
                    }
                }
            }

            ILicenseDatastore datastore = Resolve<ILicenseDatastore>();
            IRepository auditRepository = Resolve<IRepository>();

            // Update the license.            
            datastore.License_Update(entity.Id, entity.LicenseModel, entity.Uses);
            
            // Upsert the expiration parameter.
            if (license.Expiration != null)
            {
                ExpirationEntity expiration;
                                
                if (previous.Expiration != null && license.Expiration.Date != previous.Expiration.Date)
                {
                    ExpirationEntity previousExpiration = (ExpirationEntity)previous.Expiration;
                    expiration = new ExpirationEntity
                    {
                        AuditRow = auditRepository.Update(previousExpiration.AuditRow, false, true, false),                        
                        Id       = previousExpiration.Id,                     
                        Date     = license.Expiration.Date
                    };

                    datastore.Expiration_Insert(expiration.Id, expiration.Date, expiration.AuditRow.Id);
                    entity.Expiration = expiration;
                }
                else
                {
                    expiration = new ExpirationEntity
                    {
                        AuditRow = auditRepository.Insert(true, false, false),
                        Id       = Insert_Parameter(entity.Id, LicenseParameterType.Expiration),
                        Date     = license.Expiration.Date
                    };

                    datastore.Expiration_Insert(expiration.Id, expiration.Date, expiration.AuditRow.Id);
                    entity.Expiration = expiration;
                }                
            }
            // No expiration exists, but one used to? Mark it as deleted.
            else if (previous.Expiration != null)
            {
                ExpirationEntity previousExpiration = (ExpirationEntity)previous.Expiration;
                auditRepository.Update(previousExpiration.AuditRow, false, false, true);
            }

            // Upsert the seat limit parameter.
            if (license.SeatLimit != null)
            {
                if (license.SeatLimit.Seats < 0)
                {
                    throw new ArgumentException("Cannot set negative seat limit.");
                }

                SeatLimitEntity seatLimit;
                                
                if (previous.SeatLimit != null && license.SeatLimit.Seats != previous.SeatLimit.Seats)
                {
                    SeatLimitEntity previousSeatLimit = (SeatLimitEntity)previous.SeatLimit;
                    seatLimit = new SeatLimitEntity
                    {
                        AuditRow = auditRepository.Update(previousSeatLimit.AuditRow, false, true, false),                        
                        Id       = previousSeatLimit.Id,                     
                        Seats    = license.SeatLimit.Seats
                    };

                    datastore.Seats_Insert(seatLimit.Id, seatLimit.Seats, seatLimit.AuditRow.Id);
                    entity.SeatLimit = seatLimit;
                }
                else
                {
                    seatLimit = new SeatLimitEntity
                    {
                        AuditRow = auditRepository.Insert(true, false, false),
                        Id       = Insert_Parameter(entity.Id, LicenseParameterType.SeatLimit),
                        Seats    = license.SeatLimit.Seats
                    };

                    datastore.Seats_Insert(seatLimit.Id, seatLimit.Seats, seatLimit.AuditRow.Id);
                    entity.SeatLimit = seatLimit;
                }                
            }
            // No seat limit exists, but one used to? Mark it as deleted.
            else if (previous.SeatLimit != null)
            {
                SeatLimitEntity previousSeatLimit = (SeatLimitEntity)previous.SeatLimit;
                auditRepository.Update(previousSeatLimit.AuditRow, false, false, true);
            }

            // Upsert the usage limit parameter.
            if (license.UsageLimit != null)
            {
                if (license.UsageLimit.Uses < 0)
                {
                    throw new ArgumentException("Cannot set negative usage limit.");
                }

                UsageLimitEntity usageLimit;
                                
                if (previous.UsageLimit != null && license.UsageLimit.Uses != previous.UsageLimit.Uses)
                {
                    UsageLimitEntity previousUsageLimit = (UsageLimitEntity)previous.UsageLimit;
                    usageLimit = new UsageLimitEntity
                    {
                        AuditRow = auditRepository.Update(previousUsageLimit.AuditRow, false, true, false),                        
                        Id       = previousUsageLimit.Id,                     
                        Uses     = license.UsageLimit.Uses
                    };

                    datastore.UsageLimit_Insert(usageLimit.Id, usageLimit.Uses, usageLimit.AuditRow.Id);
                    entity.UsageLimit = usageLimit;
                }
                else
                {
                    usageLimit = new UsageLimitEntity
                    {
                        AuditRow = auditRepository.Insert(true, false, false),
                        Id       = Insert_Parameter(entity.Id, LicenseParameterType.UsageLimit),
                        Uses     = license.UsageLimit.Uses
                    };

                    datastore.UsageLimit_Insert(usageLimit.Id, usageLimit.Uses, usageLimit.AuditRow.Id);
                    entity.UsageLimit = usageLimit;
                }                
            }
            // No usage limit exists, but one used to? Mark it as deleted.
            else if (previous.UsageLimit != null)
            {
                UsageLimitEntity previousUsageLimit = (UsageLimitEntity)previous.UsageLimit;
                auditRepository.Update(previousUsageLimit.AuditRow, false, false, true);
            }

            return entity;
        }

        /// <summary>
        /// Delete a license.
        /// </summary>        
        /// <param name="license">License to delete.</param>
        /// <returns>License that was deleted.</returns>
        internal LicenseEntity Delete_License(Guid license)
        {
            LicenseEntity entity = Fetch_License(license);
            if (entity == null)
            {
                throw new ArgumentException("Invalid license handle.", "license");
            }

            ILicenseDatastore datastore = Resolve<ILicenseDatastore>();
            IRepository auditRepository = Resolve<IRepository>();            
            LeaseGateway leaseGateway = new LeaseGateway();

            // Delete all attached leases.
            leaseGateway.Delete_Leases(license);            

            // Mark the license record as deleted.
            auditRepository.Update(entity.AuditRow, false, false, true);

            // Mark the expiration date parameter as deleted.
            ExpirationEntity expiration = entity.Expiration as ExpirationEntity;
            if (expiration != null)
            {
                auditRepository.Update(expiration.AuditRow, false, false, true);
            }

            // Mark the seats parameter as deleted.
            SeatLimitEntity seatLimit = entity.SeatLimit as SeatLimitEntity;
            if (seatLimit != null)
            {
                auditRepository.Update(seatLimit.AuditRow, false, false, true);
            }

            // Mark the allowed uses parameter as deleted.
            UsageLimitEntity usageLimit = entity.UsageLimit as UsageLimitEntity;
            if (usageLimit != null)
            {
                auditRepository.Update(usageLimit.AuditRow, false, false, true);
            }            

            // Mark the association between the client and license as deleted.
            using (IDataReader reader = datastore.Client_License_Fetch(entity.ClientId, entity.Id))
            {
                if (!reader.Read())
                {
                    throw new ArgumentException("Trying to delete association that does not exist.");
                }

                int associationId = reader.GetInt32(reader.GetOrdinal("AssociationID"));

                Association association = FetchAssociation(associationId);
                if (association == null)
                {
                    throw new DataException("Could not retrieve association.");
                }

                AuditRow auditRow = auditRepository.Update(association.AuditRow, false, false, true);
                InsertAssociation(associationId, auditRow.Id);
            }

            return entity;
        }

        #endregion

        #region License Parameters

        /// <summary>
        /// Fetch the identifiers of the parameters tied to the license with the given identifier.
        /// </summary>
        /// <param name="licenseId">License identifier.</param>
        /// <returns>List of parameter details.</returns>
        protected IList<ParameterEntity> Fetch_Parameters(int licenseId)
        {
            ILicenseDatastore datastore = Resolve<ILicenseDatastore>();

            using (IDataReader reader = datastore.Parameters_Fetch(licenseId))
            {
                ISerializer<ParameterEntity> serializer = Resolve<ISerializer<ParameterEntity>>();
                return serializer.Deserialize(reader);
            }
        }

        /// <summary>
        /// Insert the base record of a license parameter.
        /// </summary>
        /// <param name="licenseId">License identifier.</param>
        /// <param name="parameterType">Type of the license parameter.</param>
        /// <returns>License parameter identifier.</returns>
        protected int Insert_Parameter(int licenseId, LicenseParameterType parameterType)
        {
            ILicenseDatastore datastore = Resolve<ILicenseDatastore>();
            
            using (IDataReader reader = datastore.Parameter_Insert(licenseId, parameterType))
            {
                if (!reader.Read())
                {
                    throw new ApplicationException("Problem saving license parameter.");
                }
                return reader.GetInt32(reader.GetOrdinal("LicenseParameterID"));
            }
        }        

        /// <summary>
        /// Fetch the number of seats available to a license.
        /// </summary>        
        /// <param name="licenseParameterId">License parameter identifier.</param>
        /// <returns>Number of allowed seats, or null if there's no limit.</returns>
        protected SeatLimitEntity Fetch_Seats(int licenseParameterId)
        {
            ILicenseDatastore datastore = Resolve<ILicenseDatastore>();

            using (IDataReader reader = datastore.Seats_Fetch(licenseParameterId))
            {
                if (reader.Read())
                {
                    ISerializer<SeatLimitEntity> serializer = Resolve<ISerializer<SeatLimitEntity>>();
                    return serializer.Deserialize((IDataRecord) reader);
                }
            }
            return null;
        }        

        /// <summary>
        /// Fetch a license's usage limit.
        /// </summary>        
        /// <param name="licenseParameterId">License parameter identifier.</param>
        /// <returns>Usage limit, or null if one doesn't exist.</returns>
        protected UsageLimitEntity Fetch_UsageLimit(int licenseParameterId)
        {
            ILicenseDatastore datastore = Resolve<ILicenseDatastore>();

            using (IDataReader reader = datastore.UsageLimit_Fetch(licenseParameterId))
            {
                if (reader.Read())
                {
                    ISerializer<UsageLimitEntity> serializer = Resolve<ISerializer<UsageLimitEntity>>();
                    return serializer.Deserialize((IDataRecord)reader);
                }
            }
            return null;
        }

        /// <summary>
        /// Fetch a license's exiration date.
        /// </summary>        
        /// <param name="licenseParameterId">License parameter identifier.</param>
        /// <returns>Expiration date, or null if one doesn't exist.</returns>
        protected ExpirationEntity Fetch_Expiration(int licenseParameterId)
        {
            ILicenseDatastore datastore = Resolve<ILicenseDatastore>();

            using (IDataReader reader = datastore.Expiration_Fetch(licenseParameterId))
            {
                if (reader.Read())
                {
                    ISerializer<ExpirationEntity> serializer = Resolve<ISerializer<ExpirationEntity>>();
                    return serializer.Deserialize((IDataRecord)reader);
                }
            }
            return null;
        }

        #endregion        
    }
}
