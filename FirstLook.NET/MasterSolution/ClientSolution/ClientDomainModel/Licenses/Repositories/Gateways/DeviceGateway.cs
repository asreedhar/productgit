﻿using System;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Gateways;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Licenses.Model;
using FirstLook.Client.DomainModel.Licenses.Repositories.Datastores;
using FirstLook.Client.DomainModel.Licenses.Repositories.Entities;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Gateways
{
    /// <summary>
    /// Device data gateway.
    /// </summary>
    internal class DeviceGateway : AssociationGateway
    {
        /// <summary>
        /// Fetch the device with the given handle.
        /// </summary>
        /// <param name="device">Device handle.</param>
        /// <returns>Device entity.</returns>
        internal DeviceEntity Fetch_Device(Guid device)
        {
            IDeviceDatastore datastore = Resolve<IDeviceDatastore>();
            ISerializer<DeviceEntity> serializer = Resolve<ISerializer<DeviceEntity>>();

            using (IDataReader reader = datastore.Device_Fetch(device))
            {
                if (!reader.Read())
                {
                    throw new ArgumentException("Invalid device handle.", "device");
                }

                return serializer.Deserialize((IDataRecord)reader);
            }            
        }

        /// <summary>
        /// Insert a device of the given type.
        /// </summary>
        /// <param name="deviceType">Type of device.</param>
        /// <returns>Device that was saved.</returns>
        internal DeviceEntity Insert_Device(DeviceType deviceType)
        {
            IDeviceDatastore datastore = Resolve<IDeviceDatastore>();
            ISerializer<DeviceEntity> serializer = Resolve<ISerializer<DeviceEntity>>();

            using (IDataReader reader = datastore.Device_Insert(deviceType))
            {
                if (!reader.Read())
                {
                    throw new DataException("Could not register device.");           
                }
                return serializer.Deserialize((IDataRecord) reader);
            }            
        }    
               
        /// <summary>
        /// Register a new device for a user. Will invalidate all of the user's tokens.
        /// </summary>
        /// <param name="userId">User identifier.</param>
        /// <param name="deviceType">Type of device to register.</param>
        /// <returns>Newly registered device.</returns>
        internal DeviceEntity Register_Device(int userId, DeviceType deviceType)
        {
            IRepository auditRepository = Resolve<IRepository>();
            TokenGateway tokenGateway = new TokenGateway();

            Association association;
            AuditRow auditRow;

            // Mark all existing tokens as invalidated.
            foreach (TokenEntity token in tokenGateway.Fetch_Tokens(userId))
            {
                association = FetchAssociation(token.AssociationId);
                if (association == null)
                {
                    throw new DataException("Could not retrieve association.");
                }

                auditRow = auditRepository.Update(association.AuditRow, false, false, true);
                InsertAssociation(token.AssociationId, auditRow.Id);
            }

            // Insert a new device.
            return Insert_Device(deviceType);
        }
    }
}
