﻿using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Licenses.Model;
using FirstLook.Client.DomainModel.Licenses.Repositories.Datastores;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Gateways
{
    /// <summary>
    /// Product data gateway.
    /// </summary>
    internal class ProductGateway
    {
        /// <summary>
        /// Fetch all products from the database.
        /// </summary>
        /// <returns>List of products.</returns>
        internal IList<Product> Fetch_Products()
        {
            IResolver resolver = RegistryFactory.GetResolver();

            IProductDatastore datastore = resolver.Resolve<IProductDatastore>();
            ISerializer<Product> serializer = resolver.Resolve<ISerializer<Product>>();

            using (IDataReader reader = datastore.Products_Fetch())
            {
                return serializer.Deserialize(reader);
            }
        }

        /// <summary>
        /// Fetch the identifier of the product with the given name.
        /// </summary>
        /// <param name="name">Product name.</param>
        /// <returns>Product.</returns>
        internal Product Fetch_Product(string name)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            IProductDatastore datastore = resolver.Resolve<IProductDatastore>();
            ISerializer<Product> serializer = resolver.Resolve<ISerializer<Product>>();

            using (IDataReader reader = datastore.Product_Fetch(name))
            {
                return reader.Read() ? serializer.Deserialize((IDataRecord) reader) : null;
            }
        }
    }
}
