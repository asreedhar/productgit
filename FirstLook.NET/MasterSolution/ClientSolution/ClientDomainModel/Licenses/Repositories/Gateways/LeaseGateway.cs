﻿using System;
using System.Collections.Generic;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Entities;
using FirstLook.Client.DomainModel.Clients.Repositories.Gateways;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Licenses.Repositories.Datastores;
using FirstLook.Client.DomainModel.Licenses.Repositories.Entities;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Gateways
{
    /// <summary>
    /// Lease data gateway.
    /// </summary>
    internal class LeaseGateway : AssociationGateway
    {
        /// <summary>
        /// Fetch all of a license's leases.
        /// </summary>        
        /// <param name="license">License handle.</param>
        /// <returns>List of leases.</returns>
        internal IList<LeaseEntity> Fetch_Leases(Guid license)
        {            
            // Fetch the license.
            LicenseEntity licenseEntity = new LicenseGateway().Fetch_License(license);
            if (licenseEntity == null)
            {
                throw new ArgumentException("Invalid license handle.", "license");
            }

            ILeaseDatastore datastore = Resolve<ILeaseDatastore>();
            ISerializer<LeaseEntity> serializer = Resolve<ISerializer<LeaseEntity>>();            

            // Fetch the leases.
            using (IDataReader reader = datastore.Leases_Fetch(licenseEntity.Id))
            {
                return serializer.Deserialize(reader);
            }            
        }

        /// <summary>
        /// Fetch a specific lease.
        /// </summary>
        /// <param name="license">License handle.</param>
        /// <param name="userId">User identifier.</param>       
        /// <returns>Lease.</returns>
        internal LeaseEntity Fetch_Lease(Guid license, int userId)
        {
            // Fetch the license.
            LicenseEntity licenseEntity = new LicenseGateway().Fetch_License(license);
            if (licenseEntity == null)
            {
                throw new ArgumentException("Invalid license handle.", "license");
            }

            ILeaseDatastore datastore = Resolve<ILeaseDatastore>();
            UserGateway userGateway = new UserGateway();
            MemberGateway memberGateway = new MemberGateway();

            LeaseEntity lease = null;

            // Fetch the lease.
            using (IDataReader reader = datastore.Lease_Fetch(licenseEntity.Id, userId))
            {
                if (reader.Read())
                {
                    lease = new LeaseEntity
                    {
                        // Lease info.
                        Id         = reader.GetInt32(reader.GetOrdinal("LeaseID")),                        
                        UserEntity = new User
                        {
                            Handle   = reader.GetGuid(reader.GetOrdinal("UserHandle")),
                            Id       = reader.GetInt32(reader.GetOrdinal("UserID")),
                            UserType = (UserType)reader.GetByte(reader.GetOrdinal("UserTypeID"))
                        },                        

                        // Lease.
                        AssignedByEntity = new User
                        {
                            Handle   = reader.GetGuid(reader.GetOrdinal("AssignerHandle")),
                            Id       = reader.GetInt32(reader.GetOrdinal("AssignerID")),
                            UserType = (UserType)reader.GetByte(reader.GetOrdinal("AssignerTypeID"))
                        },
                        AssignedOn = reader.GetDateTime(reader.GetOrdinal("AssignedDate"))                        
                    };

                    // Get the user details.
                    switch (lease.UserEntity.UserType)
                    {
                        case UserType.Member:
                            int memberId = userGateway.Fetch_Member(lease.UserEntity.Id);
                            lease.User = memberGateway.Fetch(memberId);
                            break;

                        default:
                            throw new DataException("Unexpected user type.");
                    }

                    // Get the assigning user details.
                    switch (lease.AssignedByEntity.UserType)
                    {
                        case UserType.Member:
                            int memberId = userGateway.Fetch_Member(lease.AssignedByEntity.Id);
                            lease.AssignedBy = memberGateway.Fetch(memberId);
                            break;

                        default:
                            throw new DataException("Unexpected user type.");
                    }                    
                }
            }
            // Get usage details.
            if (lease != null)
            {
                using (IDataReader reader = datastore.Lease_User_History_Fetch(lease.Id, userId))
                {
                    if (!reader.Read())
                    {
                        throw new DataException("Could not retrieve lease usage details.");
                    }

                    lease.Uses = reader.GetInt32(reader.GetOrdinal("Count"));

                    int accessedIndex = reader.GetOrdinal("Accessed");
                    if (!reader.IsDBNull(accessedIndex))
                    {
                        lease.LastUsed = reader.GetDateTime(accessedIndex);                        
                    }                    
                }
            }

            return lease;
        }

        /// <summary>
        /// Create a lease for the given user on the given license.
        /// </summary>
        /// <param name="license">License handle.</param>
        /// <param name="userId">User identifier.</param>
        /// <returns>New lease.</returns>
        internal LeaseEntity Insert_Lease(Guid license, int userId)
        {
            // Fetch the license.
            LicenseEntity licenseEntity = new LicenseGateway().Fetch_License(license);
            if (licenseEntity == null)
            {
                throw new ArgumentException("Invalid license handle.", "license");
            }

            ILeaseDatastore datastore = Resolve<ILeaseDatastore>();            

            int leaseId;
           
            // Insert a new lease.
            using (IDataReader reader = datastore.Lease_Insert())
            {
                if (!reader.Read())
                {
                    throw new DataException("Could not create new lease.");
                }
                leaseId = reader.GetInt32(reader.GetOrdinal("LeaseID"));
            }

            // Associate the lease to the license.            
            Association association = InsertAssociation();
            if (association == null)
            {                
                throw new ApplicationException("Could not create association.");
            }
            datastore.License_Lease_Insert(licenseEntity.Id, leaseId, association.Id);

            // Associate the lease to the user.            
            association = InsertAssociation();
            if (association == null)
            {                
                throw new ApplicationException("Could not create association.");
            }
            datastore.Lease_User_Insert(leaseId, userId, association.Id);

            // Return details of the lease.
            return Fetch_Lease(license, userId);
        }

        /// <summary>
        /// Delete all of a license's leases.
        /// </summary>
        /// <param name="license">License handle.</param>
        internal void Delete_Leases(Guid license)
        {            
            foreach (LeaseEntity lease in Fetch_Leases(license))
            {
                Delete_Lease(license, lease.UserEntity.Id);
            }
        }

        /// <summary>
        /// Delete the lease for the given user on the given license.
        /// </summary>
        /// <param name="license">License handle.</param>        
        /// <param name="userId">User identifier.</param>
        /// <returns>Lease that was deleted.</returns>
        internal LeaseEntity Delete_Lease(Guid license, int userId)
        {
            // Fetch the license.
            LicenseEntity licenseEntity = new LicenseGateway().Fetch_License(license);
            if (licenseEntity == null)
            {
                throw new ArgumentException("Invalid license handle.", "license");
            }

            // Fetch the lease to delete.
            LeaseEntity lease = Fetch_Lease(license, userId);
            if (lease == null)
            {
                throw new ArgumentException("Cannot delete nonexistent lease.");
            }

            ILeaseDatastore datastore = Resolve<ILeaseDatastore>();
            ITokenDatastore tokenDatastore = Resolve<ITokenDatastore>();            
            IRepository auditRepository = Resolve<IRepository>();

            Association association;
            AuditRow auditRow;        

            // Mark the association between the license and lease as deleted.
            using (IDataReader reader = datastore.License_Lease_Fetch(licenseEntity.Id, lease.Id))
            {
                if (!reader.Read())
                {
                    throw new ArgumentException("Trying to delete association that does not exist.");
                }

                int associationId = reader.GetInt32(reader.GetOrdinal("AssociationID"));

                association = FetchAssociation(associationId);
                if (association == null)
                {
                    throw new DataException("Could not retrieve association.");
                }

                auditRow = auditRepository.Update(association.AuditRow, false, false, true);
                InsertAssociation(associationId, auditRow.Id);
            }

            // Mark the association between the lease and the user as deleted.
            using (IDataReader reader = datastore.Lease_User_Fetch(lease.Id, userId))
            {
                if (!reader.Read())
                {
                    throw new ArgumentException("Trying to delete association that does not exist.");
                }

                int associationId = reader.GetInt32(reader.GetOrdinal("AssociationID"));                
                association = FetchAssociation(associationId);

                if (association == null)
                {
                    throw new DataException("Could not retrieve association.");
                }

                auditRow = auditRepository.Update(association.AuditRow, false, false, true);
                InsertAssociation(associationId, auditRow.Id);
            }

            // Mark any tokens for the user on the lease as deleted. It's possible there are no such associations.            
            using (IDataReader reader = tokenDatastore.Lease_Device_Fetch(userId, lease.Id))
            {
                while (reader.Read())
                {
                    int associationId = reader.GetInt32(reader.GetOrdinal("AssociationID"));

                    association = FetchAssociation(associationId);
                    if (association == null)
                    {
                        throw new DataException("Could not retrieve association.");
                    }

                    auditRow = auditRepository.Update(association.AuditRow, false, false, true);
                    InsertAssociation(associationId, auditRow.Id);
                }
            }
                                  
            return lease;
        }

        /// <summary>
        /// Mark a lease as having been used.
        /// </summary>        
        /// <param name="license">License.</param>
        /// <param name="userId">User identifier.</param>
        internal LeaseEntity Use_Lease(Guid license, int userId)
        {
            LeaseEntity lease = Fetch_Lease(license, userId);
            if (lease != null)
            {
                ILeaseDatastore datastore = Resolve<ILeaseDatastore>();
                datastore.Lease_User_History_Insert(lease.Id, userId);
            }
            return lease;
        }
    }
}
