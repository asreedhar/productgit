﻿using System.Data;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Licenses.Model;
using FirstLook.Client.DomainModel.Licenses.Repositories.Entities;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Serializers
{
    /// <summary>
    /// License info serializer.
    /// </summary>
    internal class LicenseSerializer : Serializer<LicenseEntity>
    {
        /// <summary>
        /// Deserialize a license entity record.
        /// </summary>
        /// <param name="record">Data record.</param>
        /// <returns>License entity.</returns>
        public override LicenseEntity Deserialize(IDataRecord record)
        {
            IResolver resolver = RegistryFactory.GetResolver();

            ISerializer<AuditRow> auditRowSerializer = resolver.Resolve<ISerializer<AuditRow>>();
            ISerializer<Product>  productSerializer  = resolver.Resolve<ISerializer<Product>>();

            return new LicenseEntity
            {
                // Entities.LicenseEntity.
                AuditRow     = auditRowSerializer.Deserialize(record),
                ClientId     = record.GetInt32(record.GetOrdinal("ClientID")),
                Id           = record.GetInt32(record.GetOrdinal("LicenseID")),
                // Model.LicenseInfo.                
                DeviceType   = (DeviceType) record.GetByte(record.GetOrdinal("DeviceTypeID")),                           
                Handle       = record.GetGuid(record.GetOrdinal("Handle")),
                LicenseModel = (LicenseModelType) record.GetByte(record.GetOrdinal("LicenseModelID")),
                Product      = productSerializer.Deserialize(record),
                Uses         = record.GetInt32(record.GetOrdinal("Uses"))
            };
        }
    }
}
