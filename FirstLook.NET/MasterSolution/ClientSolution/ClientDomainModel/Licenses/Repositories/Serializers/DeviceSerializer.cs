﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Licenses.Model;
using FirstLook.Client.DomainModel.Licenses.Repositories.Entities;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Serializers
{
    /// <summary>
    /// Device serializer.
    /// </summary>
    internal class DeviceSerializer : Serializer<DeviceEntity>
    {
        /// <summary>
        /// Deserialize a device entity record.
        /// </summary>
        /// <param name="record">Data record.</param>
        /// <returns>Device entity.</returns>
        public override DeviceEntity Deserialize(IDataRecord record)
        {
            return new DeviceEntity
            {                      
                Handle = record.GetGuid(record.GetOrdinal("Handle")),
                Id     = record.GetInt32(record.GetOrdinal("DeviceID")),
                Type   = (DeviceType)record.GetByte(record.GetOrdinal("DeviceTypeID"))
            };
        }
    }
}
