﻿using System.Data;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Licenses.Repositories.Entities;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Serializers
{
    /// <summary>
    /// Seat license parameter serializer.
    /// </summary>
    internal class SeatLimitSerializer : Serializer<SeatLimitEntity>
    {
        /// <summary>
        /// Deserialize a seat limit parameter entity record.
        /// </summary>
        /// <param name="record">Data record.</param>
        /// <returns>Seat limit parameter entity.</returns>
        public override SeatLimitEntity Deserialize(IDataRecord record)
        {
            ISerializer<AuditRow> auditRowSerializer = RegistryFactory.GetResolver().Resolve<ISerializer<AuditRow>>();

            return new SeatLimitEntity
            {
                AuditRow = auditRowSerializer.Deserialize(record),
                Id       = record.GetInt32(record.GetOrdinal("LicenseParameterID")),                
                Seats    = record.GetInt16(record.GetOrdinal("Quantity"))
            };
        }
    }
}
