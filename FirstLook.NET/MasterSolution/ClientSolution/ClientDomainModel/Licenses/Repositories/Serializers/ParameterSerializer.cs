﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Licenses.Model;
using FirstLook.Client.DomainModel.Licenses.Repositories.Entities;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Serializers
{
    /// <summary>
    /// License parameter serializer.
    /// </summary>
    internal class ParameterSerializer : Serializer<ParameterEntity>
    {
        /// <summary>
        /// Deserialize a license parameter entity record.
        /// </summary>
        /// <param name="record">Data record with license parameter details.</param>
        /// <returns>License parameter details.</returns>
        public override ParameterEntity Deserialize(IDataRecord record)
        {
            return new ParameterEntity
            {                           
                Id   = record.GetInt32(record.GetOrdinal("LicenseParameterID")),
                Type = (LicenseParameterType) record.GetByte(record.GetOrdinal("LicenseParameterTypeID"))
            };
        }
    }
}
