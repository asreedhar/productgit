﻿using System.Data;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Licenses.Repositories.Entities;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Serializers
{
    /// <summary>
    /// Usage limit license parameter serializer.
    /// </summary>
    internal class UsageLimitSerializer : Serializer<UsageLimitEntity>
    {
        /// <summary>
        /// Deserialize a usage limit parameter entity record.
        /// </summary>
        /// <param name="record">Data record.</param>
        /// <returns>Usage limit parameter entity.</returns>
        public override UsageLimitEntity Deserialize(IDataRecord record)
        {
            ISerializer<AuditRow> auditRowSerializer = RegistryFactory.GetResolver().Resolve<ISerializer<AuditRow>>();

            return new UsageLimitEntity
            {
                AuditRow = auditRowSerializer.Deserialize(record),
                Id       = record.GetInt32(record.GetOrdinal("LicenseParameterID")),                
                Uses     = record.GetInt32(record.GetOrdinal("Uses"))
            };
        }
    }
}
