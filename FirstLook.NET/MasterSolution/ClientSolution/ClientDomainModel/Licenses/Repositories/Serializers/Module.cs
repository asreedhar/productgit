﻿using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Licenses.Model;
using FirstLook.Client.DomainModel.Licenses.Repositories.Entities;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Serializers
{
    /// <summary>
    /// Module for registering serializers.
    /// </summary>
    internal class Module : IModule
    {        
        /// <summary>
        /// Register licenseing serializers.
        /// </summary>
        /// <param name="registry"></param>
        public void Configure(IRegistry registry)
        {
            registry.Register<ISerializer<DeviceEntity>,      DeviceSerializer>     (ImplementationScope.Shared);
            registry.Register<ISerializer<ExpirationEntity>,  ExpirationSerializer> (ImplementationScope.Shared);
            registry.Register<ISerializer<LeaseEntity>,       LeaseSerializer>      (ImplementationScope.Shared);
            registry.Register<ISerializer<LicenseEntity>,     LicenseSerializer>    (ImplementationScope.Shared);
            registry.Register<ISerializer<ParameterEntity>,   ParameterSerializer>  (ImplementationScope.Shared);
            registry.Register<ISerializer<Product>,           ProductSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<SeatLimitEntity>,   SeatLimitSerializer>  (ImplementationScope.Shared);
            registry.Register<ISerializer<TokenEntity>,       TokenSerializer>      (ImplementationScope.Shared);
            registry.Register<ISerializer<UsageLimitEntity>,  UsageLimitSerializer> (ImplementationScope.Shared);
        }
    }
}
