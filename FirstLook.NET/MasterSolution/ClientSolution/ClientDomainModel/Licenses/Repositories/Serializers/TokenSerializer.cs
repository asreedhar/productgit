﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Licenses.Repositories.Entities;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Serializers
{
    /// <summary>
    /// Token serializer.
    /// </summary>
    internal class TokenSerializer : Serializer<TokenEntity>
    {
        /// <summary>
        /// Deserialize a token entity record.
        /// </summary>
        /// <param name="record">Data record.</param>
        /// <returns>Token entity.</returns>
        public override TokenEntity Deserialize(IDataRecord record)
        {
            return new TokenEntity
            {
                License       = record.GetGuid(record.GetOrdinal("Handle")),
                LicenseId     = record.GetInt32(record.GetOrdinal("LicenseID")),
                UserId        = record.GetInt32(record.GetOrdinal("UserId")),
                LeaseId       = record.GetInt32(record.GetOrdinal("LeaseID")),
                DeviceId      = record.GetInt32(record.GetOrdinal("DeviceID")),
                Device        = record.GetGuid(record.GetOrdinal("Device")),
                Handle        = record.GetGuid(record.GetOrdinal("Token")),
                ExpiresOn     = record.GetDateTime(record.GetOrdinal("ExpiresOn")),                
                AssociationId = record.GetInt32(record.GetOrdinal("AssociationID"))
            };
        }
    }
}
