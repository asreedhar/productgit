﻿using System.Data;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Licenses.Repositories.Entities;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Serializers
{
    /// <summary>
    /// Expiration date license parameter serializer.
    /// </summary>
    internal class ExpirationSerializer : Serializer<ExpirationEntity>
    {
        /// <summary>
        /// Deserialize an expiration parameter entity record.
        /// </summary>
        /// <param name="record">Data record.</param>
        /// <returns>Expiration parameter entity.</returns>
        public override ExpirationEntity Deserialize(IDataRecord record)
        {
            ISerializer<AuditRow> auditRowSerializer = RegistryFactory.GetResolver().Resolve<ISerializer<AuditRow>>();

            return new ExpirationEntity
            {
                AuditRow = auditRowSerializer.Deserialize(record),                
                Date     = record.GetDateTime(record.GetOrdinal("ExpirationDate")),
                Id       = record.GetInt32(record.GetOrdinal("LicenseParameterID")),
            };
        }
    }
}
