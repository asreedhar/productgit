﻿using System.Data;
using FirstLook.Client.DomainModel.Clients.Repositories.Entities;
using FirstLook.Client.DomainModel.Clients.Repositories.Gateways;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Licenses.Repositories.Entities;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Serializers
{
    /// <summary>
    /// Lease info serializer.
    /// </summary>
    internal class LeaseSerializer : Serializer<LeaseEntity>
    {
        /// <summary>
        /// Deserialize a lease entity record.
        /// </summary>
        /// <param name="record">Data record.</param>
        /// <returns>Lease entity.</returns>
        public override LeaseEntity Deserialize(IDataRecord record)
        {
            ISerializer<User> serializer    = RegistryFactory.GetResolver().Resolve<ISerializer<User>>();
            UserGateway       userGateway   = new UserGateway();
            MemberGateway     memberGateway = new MemberGateway();

            LeaseEntity lease = new LeaseEntity
            {
                Id         = record.GetInt32(record.GetOrdinal("LeaseID")),
                UserEntity = serializer.Deserialize(record)
            };

            // Get the user details.
            switch (lease.UserEntity.UserType)
            {
                case UserType.Member:
                    int memberId = userGateway.Fetch_Member(lease.UserEntity.Id);
                    lease.User = memberGateway.Fetch(memberId);
                    break;

                default:
                    throw new DataException("Unexpected user type.");
            }

            return lease;
        }
    }
}
