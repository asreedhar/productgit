﻿using System.Data;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Client.DomainModel.Licenses.Model;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Serializers
{
    /// <summary>
    /// Product serializer.
    /// </summary>
    internal class ProductSerializer : Serializer<Product>
    {        
        /// <summary>
        /// Deserialize a product record.
        /// </summary>
        /// <param name="record">Data record with a product.</param>
        /// <returns>Product.</returns>
        public override Product Deserialize(IDataRecord record)
        {
            return new Product
            {
                Id   = record.GetInt16(record.GetOrdinal("ProductID")),
                Name = record.GetString(record.GetOrdinal("ProductName"))
            };
        }        
    }
}
