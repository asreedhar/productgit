﻿using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Licenses.Model;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Entities
{
    /// <summary>
    /// License seat limit repository entity.
    /// </summary>
    internal class SeatLimitEntity : LicenseSeatLimit
    {
        /// <summary>
        /// License parameter identifier.
        /// </summary>
        internal int Id { get; set; }

        /// <summary>
        /// Audit row.
        /// </summary>
        internal AuditRow AuditRow { get; set; }
    }
}
