﻿using FirstLook.Client.DomainModel.Licenses.Model;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Entities
{
    /// <summary>
    /// License parameter respository entity.
    /// </summary>
    internal class ParameterEntity : LicenseParameter
    {
        /// <summary>
        /// License parameter identifier.
        /// </summary>
        internal int Id { get; set; }        
    }
}
