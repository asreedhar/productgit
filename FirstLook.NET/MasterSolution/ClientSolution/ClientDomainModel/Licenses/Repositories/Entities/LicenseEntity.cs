﻿using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Licenses.Model;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Entities
{
    /// <summary>
    /// License repository entity.
    /// </summary>
    internal class LicenseEntity : License
    {               
        /// <summary>
        /// Default constructor.
        /// </summary>
        internal LicenseEntity()
        {            
        }

        /// <summary>
        /// Initialize this entity with values from the given license.
        /// </summary>
        /// <param name="license">License.</param>
        internal LicenseEntity(LicenseInfo license)
        {
            DeviceType   = license.DeviceType;
            Handle       = license.Handle;
            LicenseModel = license.LicenseModel;
            Product      = new Product {Id = license.Product.Id, Name = license.Product.Name};
            Uses         = license.Uses;            
        }

        /// <summary>
        /// License identifier.
        /// </summary>
        internal int Id { get; set; }

        /// <summary>
        /// Client identifier.
        /// </summary>
        internal int ClientId { get; set; }

        /// <summary>
        /// Audit row.
        /// </summary>
        internal AuditRow AuditRow { get; set; }
    }
}
