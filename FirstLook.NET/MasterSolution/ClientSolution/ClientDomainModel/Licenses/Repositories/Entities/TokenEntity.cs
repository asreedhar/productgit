﻿using FirstLook.Client.DomainModel.Licenses.Model;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Entities
{
    /// <summary>
    /// The association between a user, lease and device.
    /// </summary>
    internal class TokenEntity : Token
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        internal TokenEntity()
        {            
        }

        /// <summary>
        /// Initialize this entity with values from a token.
        /// </summary>
        /// <param name="token">Token.</param>
        internal TokenEntity(Token token)
        {
            Device    = token.Device;
            ExpiresOn = token.ExpiresOn;
            Handle    = token.Handle;
            License   = token.License;
            UserId    = token.UserId;
        }

        /// <summary>
        /// Lease identifier.
        /// </summary>
        internal int LeaseId { get; set; }

        /// <summary>
        /// License identifier.
        /// </summary>
        internal int LicenseId { get; set; }        

        /// <summary>
        /// Device identifier.
        /// </summary>
        internal int DeviceId { get; set; }        
       
        /// <summary>
        /// Association identifier.
        /// </summary>
        internal int AssociationId { get; set; }
    }
}
