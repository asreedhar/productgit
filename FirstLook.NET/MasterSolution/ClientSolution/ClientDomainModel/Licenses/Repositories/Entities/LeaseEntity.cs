﻿using FirstLook.Client.DomainModel.Clients.Repositories.Entities;
using FirstLook.Client.DomainModel.Licenses.Model;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Entities
{
    /// <summary>
    /// Lease respository entity.
    /// </summary>
    internal class LeaseEntity : Lease
    {
        /// <summary>
        /// Lease identifier.
        /// </summary>
        internal int Id { get; set; }

        /// <summary>
        /// User entity.
        /// </summary>
        internal User UserEntity { get; set; }
       
        /// <summary>
        /// Entity of the assigning user.
        /// </summary>
        internal User AssignedByEntity { get; set; }
    }
}
