﻿using FirstLook.Client.DomainModel.Licenses.Model;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Entities
{
    /// <summary>
    /// Device repository entity.
    /// </summary>
    internal class DeviceEntity : Device
    {
        /// <summary>
        /// Device identifier.
        /// </summary>
        internal int Id { get; set; }
    }
}
