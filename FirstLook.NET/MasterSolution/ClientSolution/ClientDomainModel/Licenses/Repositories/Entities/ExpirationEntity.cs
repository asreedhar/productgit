﻿using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Licenses.Model;

namespace FirstLook.Client.DomainModel.Licenses.Repositories.Entities
{
    /// <summary>
    /// License expiration repository entity.
    /// </summary>
    internal class ExpirationEntity : LicenseExpiration
    {
        /// <summary>
        /// License parameter identifier.
        /// </summary>
        internal int Id { get; set; }

        /// <summary>
        /// Audit row.
        /// </summary>
        internal AuditRow AuditRow { get; set; }
    }
}
