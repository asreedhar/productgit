﻿
SELECT	
	LicenseID,
	Handle,
	LicenseModelID,
	ProductID,
	DeviceTypeID,
	Uses
FROM	
	License.License
WHERE
	LicenseID = @@IDENTITY