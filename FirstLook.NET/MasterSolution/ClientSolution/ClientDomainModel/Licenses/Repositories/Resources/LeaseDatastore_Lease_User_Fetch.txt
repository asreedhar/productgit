﻿
SELECT	
	LU.LeaseID, 
	LU.UserID, 
	LU.AssociationID 
FROM
	License.Lease_User LU
JOIN
	Audit.Association_History AH ON AH.AssociationID = LU.AssociationID
JOIN
	Audit.AuditRow AR ON AR.AuditRowID = AH.AuditRowID
WHERE 
	AR.WasDelete = 0
AND
	GETDATE() BETWEEN AR.ValidFrom AND AR.ValidUpTo
AND
	LU.LeaseID = @LeaseID
AND
	LU.UserID = @UserID