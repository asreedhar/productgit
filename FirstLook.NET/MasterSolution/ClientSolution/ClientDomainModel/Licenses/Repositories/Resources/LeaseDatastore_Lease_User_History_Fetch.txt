﻿
SELECT	
	COUNT(Accessed) AS Count,
	MAX(Accessed) AS Accessed
FROM
	License.Lease_User_History
WHERE
	LeaseID = @LeaseID
AND
	UserID = @UserID