﻿namespace FirstLook.Client.DomainModel.Licenses.Model
{
    /// <summary>
    /// Base class of a license parameter.
    /// </summary>
    public abstract class LicenseParameter
    {
        /// <summary>
        /// Type of the parameter.
        /// </summary>
        public LicenseParameterType Type { get; internal set; }
    }
}
