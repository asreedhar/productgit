﻿using System;

namespace FirstLook.Client.DomainModel.Licenses.Model
{
    /// <summary>
    /// General details of a license for a client to a product for a type of device.
    /// </summary>
    public class LicenseInfo
    {
        /// <summary>
        /// Initialize the product.
        /// </summary>
        public LicenseInfo()
        {
            Product = new Product();            
        }

        /// <summary>
        /// License handle. Null if this is a new license.
        /// </summary>
        public Guid? Handle { get; internal set; }

        /// <summary>
        /// License model.
        /// </summary>
        public LicenseModelType LicenseModel { get; internal set; }

        /// <summary>
        /// Device type.
        /// </summary>
        public DeviceType DeviceType { get; internal set; }

        /// <summary>
        /// Product.
        /// </summary>
        public Product Product { get; internal set; }

        /// <summary>
        /// How many times has this license been used?
        /// </summary>
        public int Uses { get; internal set; }        

               
    }
}
