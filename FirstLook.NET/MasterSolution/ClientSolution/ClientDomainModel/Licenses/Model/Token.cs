﻿using System;

namespace FirstLook.Client.DomainModel.Licenses.Model
{
    /// <summary>
    /// Token for a leased user on a device.
    /// </summary>
    public class Token
    {
        /// <summary>
        /// Lease token.
        /// </summary>
        public Guid? Handle { get; internal set; }

        /// <summary>
        /// License handle.
        /// </summary>
        public Guid License { get; internal set; }

        /// <summary>
        /// User identifier.
        /// </summary>
        public int UserId { get; internal set; }

        /// <summary>
        /// Device handle.
        /// </summary>
        public Guid Device { get; internal set; }

        /// <summary>
        /// Date on which the token expires.
        /// </summary>
        public DateTime ExpiresOn { get; internal set; }
    }
}
