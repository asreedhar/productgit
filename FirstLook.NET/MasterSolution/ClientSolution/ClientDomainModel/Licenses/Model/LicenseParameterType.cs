﻿namespace FirstLook.Client.DomainModel.Licenses.Model
{
    /// <summary>
    /// Types of license parameters.
    /// </summary>
    public enum LicenseParameterType : byte
    {
        /// <summary>
        /// Not defined.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// Number of allowed seats.
        /// </summary>
        SeatLimit = 1,

        /// <summary>
        /// Usage limit.
        /// </summary>
        UsageLimit = 2,

        /// <summary>
        /// Expiration date.
        /// </summary>
        Expiration = 3
    }
}
