﻿namespace FirstLook.Client.DomainModel.Licenses.Model
{
    /// <summary>
    /// Types of devices used to access the application.
    /// </summary>
    public enum DeviceType : byte
    {
        /// <summary>
        /// Undefined.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// Desktop.
        /// </summary>
        Desktop = 1,

        /// <summary>
        /// Mobile.
        /// </summary>
        Mobile = 2
    }
}
