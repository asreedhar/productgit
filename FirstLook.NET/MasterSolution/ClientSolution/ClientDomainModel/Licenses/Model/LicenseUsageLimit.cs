﻿namespace FirstLook.Client.DomainModel.Licenses.Model
{
    /// <summary>
    /// License usage limit parameter.
    /// </summary>
    public class LicenseUsageLimit : LicenseParameter
    {        
        /// <summary>
        /// License parameter type.
        /// </summary>
        public new LicenseParameterType Type
        {
            get { return LicenseParameterType.UsageLimit; }
        }

        /// <summary>
        /// Number of uses a license is restricted to.
        /// </summary>
        public int Uses { get; internal set; }
    }
}
