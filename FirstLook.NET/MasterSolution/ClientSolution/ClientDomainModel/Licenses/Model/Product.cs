﻿using System;

namespace FirstLook.Client.DomainModel.Licenses.Model
{
    /// <summary>
    /// Product that can be licensed.
    /// </summary>
    public class Product : IComparable
    {
        /// <summary>
        /// Product identifier.
        /// </summary>
        public int Id { get; internal set; }

        /// <summary>
        /// Product name.
        /// </summary>
        public string Name { get; internal set; }        

        /// <summary>
        /// Compares the current instance with another object of the same type and returns an integer that indicates 
        /// whether the current instance precedes, follows, or occurs in the same position in the sort order as the 
        /// other object.
        /// </summary>
        /// <returns>
        /// A value that indicates the relative order of the objects being compared. The return value has these 
        /// meanings: Value Meaning Less than zero This instance is less than <paramref name="obj"/>. Zero This 
        /// instance is equal to <paramref name="obj"/>. Greater than zero This instance is greater 
        /// than <paramref name="obj"/>. 
        /// </returns>
        /// <param name="obj">An object to compare with this instance. </param>
        /// <exception cref="T:System.ArgumentException"><paramref name="obj"/> is not the same type as this 
        /// instance. </exception>
        /// <filterpriority>2</filterpriority>
        public int CompareTo(object obj)
        {
            Product prod = obj as Product;
            if (prod == null)
            {
                throw new ArgumentException("Can only compare products to products.");
            }
            return Name.CompareTo(prod.Name);
        }        
    }
}
