﻿using System;

namespace FirstLook.Client.DomainModel.Licenses.Model
{
    /// <summary>
    /// A user's device.
    /// </summary>
    public class Device
    {        
        /// <summary>
        /// Device handle.
        /// </summary>
        public Guid Handle { get; internal set; }

        /// <summary>
        /// Type of device.
        /// </summary>
        public DeviceType Type { get; internal set; }
    }
}
