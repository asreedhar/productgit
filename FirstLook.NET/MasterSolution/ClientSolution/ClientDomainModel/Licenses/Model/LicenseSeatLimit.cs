﻿namespace FirstLook.Client.DomainModel.Licenses.Model
{
    /// <summary>
    /// License seat limit parameter.
    /// </summary>
    public class LicenseSeatLimit : LicenseParameter
    {
        /// <summary>
        /// License parameter type.
        /// </summary>
        public new LicenseParameterType Type
        {
            get { return LicenseParameterType.SeatLimit; }
        }

        /// <summary>
        /// Number of seats available to a license.
        /// </summary>
        public int Seats { get; internal set; }
    }
}
