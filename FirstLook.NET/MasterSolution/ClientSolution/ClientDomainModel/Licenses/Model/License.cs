﻿namespace FirstLook.Client.DomainModel.Licenses.Model
{
    /// <summary>
    /// A license for a client to a product for a type of device.
    /// </summary>
    public class License : LicenseInfo
    {
        /// <summary>
        /// Date of the expiration of this license, if it expires.
        /// </summary>
        public LicenseExpiration Expiration { get; internal set; }

        /// <summary>
        /// Number of seats available to the license, if there's a limit.
        /// </summary>
        public LicenseSeatLimit SeatLimit { get; internal set; }

        /// <summary>
        /// Number of uses the license is restricted to, if there is a restriction.
        /// </summary>
        public LicenseUsageLimit UsageLimit { get; internal set; }
    }
}
