﻿using System;

namespace FirstLook.Client.DomainModel.Licenses.Model
{
    /// <summary>
    /// License expiration parameter.
    /// </summary>
    public class LicenseExpiration : LicenseParameter
    {        
        /// <summary>
        /// License parameter type.
        /// </summary>
        public new LicenseParameterType Type
        {
            get { return LicenseParameterType.Expiration; }
        }

        /// <summary>
        /// Date on which a license expires, if at all.
        /// </summary>
        public DateTime Date { get; internal set; }
    }
}
