﻿using FirstLook.Client.DomainModel.Clients.Model;

namespace FirstLook.Client.DomainModel.Licenses.Model
{
    /// <summary>
    /// General details of a lease for a user to a license.
    /// </summary>
    public class LeaseInfo
    {
        /// <summary>
        /// Name of the user granted the lease.
        /// </summary>
        public IUser User { get; internal set; }
    }
}
