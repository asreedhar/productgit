﻿using System;
using FirstLook.Client.DomainModel.Clients.Model;

namespace FirstLook.Client.DomainModel.Licenses.Model
{
    /// <summary>
    /// Lease for a user to a license.
    /// </summary>
    public class Lease : LeaseInfo
    {
        /// <summary>
        /// User that assigned this lease.
        /// </summary>
        public IUser AssignedBy { get; internal set; }

        /// <summary>
        /// Date on which the lease was assigned.
        /// </summary>
        public DateTime AssignedOn { get; internal set; }

        /// <summary>
        /// Number of uses of this lease.
        /// </summary>
        public int Uses { get; internal set; }

        /// <summary>
        /// Date of the last access using this lease.
        /// </summary>
        public DateTime? LastUsed { get; internal set; }
    }
}
