﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Model;

namespace FirstLook.Client.DomainModel.Licenses.Model
{
    /// <summary>
    /// Interface for a licensing data repository.
    /// </summary>
    public interface ILicenseRepository
    {
        #region Licenses

        /// <summary>
        /// Fetch a client's licenses.
        /// </summary>
        /// <param name="broker">Broker.</param>
        /// <returns>List of licenses.</returns>
        IList<LicenseInfo> Fetch_Licenses(IBroker broker);

        /// <summary>
        /// Fetch a particular license.
        /// </summary>        
        /// <param name="license">License handle.</param>
        /// <returns>License.</returns>
        License Fetch_License(Guid license);

        /// <summary>
        /// Save a new or updated license.
        /// </summary>        
        /// <param name="principal">Principal.</param>
        /// <param name="broker">Broker.</param>
        /// <param name="license">License to save.</param>
        /// <returns>License that was saved.</returns>
        License Save_License(IPrincipal principal, IBroker broker, License license);

        /// <summary>
        /// Delete a license and all of its associated leases.
        /// </summary>        
        /// <param name="principal">Principal.</param>
        /// <param name="license">License to delete.</param>
        /// <returns>License that was deleted.</returns>
        License Delete_License(IPrincipal principal, Guid license);

        #endregion

        #region Leases

        /// <summary>
        /// Fetch all of a license's leases.
        /// </summary>        
        /// <param name="license">License handle.</param>
        /// <returns>List of leases.</returns>
        IList<LeaseInfo> Fetch_Leases(Guid license);

        /// <summary>
        /// Fetch a particular lease.
        /// </summary>
        /// <param name="license">License handle.</param>
        /// <param name="userId">User identifier.</param>
        /// <returns>Lease.</returns>
        Lease Fetch_Lease(Guid license, int userId);

        /// <summary>
        /// Create a lease for the given user on the given license.
        /// </summary>
        /// <param name="principal">Principal.</param>
        /// <param name="license">License handle.</param>
        /// <param name="userId">User identifier.</param>
        /// <returns>New lease.</returns>
        Lease Create_Lease(IPrincipal principal, Guid license, int userId);

        /// <summary>
        /// Delete the lease for the given user on the given license.
        /// </summary>
        /// <param name="principal">Principal.</param>
        /// <param name="license">License handle.</param>        
        /// <param name="userId">User identifier.</param>
        /// <returns>Lease that was deleted.</returns>
        LeaseInfo Delete_Lease(IPrincipal principal, Guid license, int userId);

        /// <summary>
        /// Mark a lease as having been used.
        /// </summary>
        /// <param name="principal">Principal.</param>
        /// <param name="license">License handle.</param>
        /// <param name="userId">User identifier.</param>
        /// <returns>Lease that was used.</returns>
        Lease Use_Lease(IPrincipal principal, Guid license, int userId);

        #endregion

        #region Devices

        /// <summary>
        /// Fetch the device with the given handle.
        /// </summary>
        /// <param name="handle">Device handle.</param>
        /// <returns>Device identifiers.</returns>
        Device Fetch_Device(Guid handle);

        /// <summary>
        /// Create a device of the given type.
        /// </summary>
        /// <param name="principal">Principal.</param>        
        /// <param name="type">Device type.</param>
        /// <returns>Device identifiers.</returns>
        Device Create_Device(IPrincipal principal, DeviceType type);

        #endregion

        #region Tokens

        /// <summary>
        /// Fetch all of a license's tokens.
        /// </summary>        
        /// <param name="license">License.</param>
        /// <returns>List of tokens.</returns>
        IList<Token> Fetch_Tokens(Guid license);

        /// <summary>
        /// Fetch the token for the licensed user on a device.
        /// </summary>        
        /// <param name="license">License handle.</param>
        /// <param name="userId">User identifier.</param>      
        /// <param name="device">Device identifier.</param>  
        /// <returns>Token, or null if one does not exists.</returns>
        Token Fetch_Token(Guid license, int userId, Guid device);

        /// <summary>
        /// Fetch the token with the given handle.
        /// </summary>        
        /// <param name="token">Token handle.</param>
        /// <returns>Token, or null if one does not exist.</returns>
        Token Fetch_Token(Guid token);        

        /// <summary>
        /// Save a token.
        /// </summary>
        /// <param name="principal">Principal.</param>
        /// <param name="token">Token to save.</param>
        /// <returns>Saved token.</returns>
        Token Save_Token(IPrincipal principal, Token token);

        /// <summary>
        /// Delete tokens for the user for the type of device. If a device identifier is provided, do not delete any
        /// of its tokens.
        /// </summary>
        /// <param name="principal">Principal.</param>
        /// <param name="userId">User identifier.</param>
        /// <param name="deviceType">Device type, if applicable.</param>
        /// <param name="safeDevice">Device to spare, if applicable.</param>
        /// <returns>Number of tokens invalidated.</returns>
        int Delete_Tokens(IPrincipal principal, int userId, DeviceType? deviceType, Guid? safeDevice);

        #endregion

        #region Products

        /// <summary>
        /// Get all license-able products.
        /// </summary>
        /// <returns>List of products.</returns>
        IList<Product> Fetch_Products();

        /// <summary>
        /// Fetch the product with the given name.
        /// </summary>
        /// <param name="name">Product name.</param>
        /// <returns>Product.</returns>
        Product Fetch_Product(string name);

        #endregion
    }
}
