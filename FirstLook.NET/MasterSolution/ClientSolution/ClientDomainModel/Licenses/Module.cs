﻿using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Licenses
{
    /// <summary>
    /// Module for registering licensing components.
    /// </summary>
    internal class Module : IModule
    {
        /// <summary>
        /// Register licensing components.
        /// </summary>        
        public void Configure(IRegistry registry)
        {
            registry.Register<Commands.Module>();            

            registry.Register<Repositories.Module>();
        }
    }
}
