﻿using System;

namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects
{
    /// <summary>
    /// Status of an authorization token.
    /// </summary>
    [Serializable]
    public enum TokenStatusDto : byte
    {
        /// <summary>
        /// Undefined.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// Token for a user, product and device is valid.
        /// </summary>
        Valid = 1,        

        /// <summary>
        /// Token is invalid.
        /// </summary>
        InvalidToken = 2,

        /// <summary>
        /// Token has expired.
        /// </summary>
        ExpiredToken = 3,

        /// <summary>
        /// User is not leased. Applicable only for named licenses.
        /// </summary>
        NoLease = 4,

        /// <summary>
        /// No licenses exists.
        /// </summary>
        NoLicense = 5,

        /// <summary>
        /// License has expired.
        /// </summary>
        ExpiredLicense = 6,

        /// <summary>
        /// No seats are open for this token.
        /// </summary>
        SeatLimitExceed = 7,

        /// <summary>
        /// License reached usage limit.
        /// </summary>
        UsageLimitExceeded = 8
    }
}
