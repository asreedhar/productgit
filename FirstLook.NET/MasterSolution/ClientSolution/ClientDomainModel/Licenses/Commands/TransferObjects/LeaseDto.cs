﻿using System;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;

namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects
{
    /// <summary>
    /// Details of a lease.
    /// </summary>
    [Serializable]
    public class LeaseDto : LeaseInfoDto
    {
        /// <summary>
        /// User that assigned this lease.
        /// </summary>
        public UserDto AssignedBy { get; set; }

        /// <summary>
        /// Date on which the lease was assigned.
        /// </summary>
        public DateTime AssignedOn { get; set; }

        /// <summary>
        /// Number of uses of this lease.
        /// </summary>
        public int Uses { get; set; }      

        /// <summary>
        /// Date of the last access using this lease.
        /// </summary>
        public DateTime? LastUsed { get; set; }
    }
}
