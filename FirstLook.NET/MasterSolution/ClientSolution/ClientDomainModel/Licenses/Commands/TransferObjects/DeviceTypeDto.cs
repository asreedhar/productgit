﻿using System;

namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects
{
    /// <summary>
    /// Types of devices used to access the application.
    /// </summary>
    [Serializable]
    public enum DeviceTypeDto : byte
    {
        /// <summary>
        /// Undefined.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// Desktop.
        /// </summary>
        Desktop = 1,

        /// <summary>
        /// Mobile.
        /// </summary>
        Mobile = 2
    }
}
