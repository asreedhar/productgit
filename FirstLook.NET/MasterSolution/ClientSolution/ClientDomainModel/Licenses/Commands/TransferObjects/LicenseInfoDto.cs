﻿using System;

namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects
{
    /// <summary>
    /// Identifying details of a license.
    /// </summary>
    [Serializable]
    public class LicenseInfoDto
    {
        /// <summary>
        /// License identifier.
        /// </summary>
        public Guid? License { get; set; }

        /// <summary>
        /// Licensed product.
        /// </summary>
        public string Product { get; set; }

        /// <summary>
        /// Type of license model (e.g. site, floating, etc).
        /// </summary>
        public LicenseModelTypeDto LicenseModel { get; set; }

        /// <summary>
        /// Type of device (e.g. desktop, mobile, etc).
        /// </summary>
        public DeviceTypeDto DeviceType { get; set; }

        /// <summary>
        /// How many times has this license been used?
        /// </summary>
        public int Uses { get; set; }
    }
}
