﻿using System;

namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects
{
    /// <summary>
    /// Types of license models.
    /// </summary>
    [Serializable]
    public enum LicenseModelTypeDto : byte
    {
        /// <summary>
        /// Undefined.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// Site license: allows for unlimited users.
        /// </summary>
        Site = 1,

        /// <summary>
        /// Floating license: allows a designated number of concurrent users.
        /// </summary>
        Floating = 2,

        /// <summary>
        /// Named license: allows a fixed number of manually designated users.
        /// </summary>
        Named = 3
    }
}
