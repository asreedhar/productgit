﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;

namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Results of querying for all of a broker's licenses.
    /// </summary>
    [Serializable]
    public class LicensesResultsDto
    {        
        /// <summary>
        /// Arguments used to produce these results.
        /// </summary>
        public LicensesArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Details of the broker.
        /// </summary>
        public BrokerDto Broker { get; set; }

        /// <summary>
        /// General details of the broker's licenses.
        /// </summary>
        public List<LicenseInfoDto> Licenses { get; set; }                       
    }
}