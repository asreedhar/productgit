﻿using System;

namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments for creating a lease.
    /// </summary>
    [Serializable]
    public class CreateLeaseArgumentsDto
    {
        /// <summary>
        /// Broker identifier.
        /// </summary>
        public Guid Broker { get; set; }

        /// <summary>
        /// License identifier.
        /// </summary>
        public Guid License { get; set; }

        /// <summary>
        /// Username.
        /// </summary>
        public string UserName { get; set; }               
    }
}
