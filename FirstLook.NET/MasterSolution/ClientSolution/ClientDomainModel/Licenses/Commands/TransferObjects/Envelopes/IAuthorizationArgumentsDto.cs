﻿namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Interface for authorization arguments.
    /// </summary>
    public interface IAuthorizationArgumentsDto
    {
        /// <summary>
        /// Authorization arguments.
        /// </summary>
        AuthorizationDto Authorization { get; set; }
    }
}
