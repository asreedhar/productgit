﻿using System;

namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments for changing a license's model type.
    /// </summary>
    [Serializable]
    public class ChangeLicenseModelArgumentsDto
    {
        /// <summary>
        /// Broker identifier.
        /// </summary>
        public Guid Broker { get; set; }

        /// <summary>
        /// License identifier.
        /// </summary>
        public Guid License { get; set; }

        /// <summary>
        /// New license model.
        /// </summary>
        public LicenseModelTypeDto Model { get; set; }
    }
}
