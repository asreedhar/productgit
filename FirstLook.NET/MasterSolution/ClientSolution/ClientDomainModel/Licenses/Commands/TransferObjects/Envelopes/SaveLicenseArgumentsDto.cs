﻿using System;

namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arugments for saving a license for a broker.
    /// </summary>
    [Serializable]
    public class SaveLicenseArgumentsDto
    {
        /// <summary>
        /// Broker whose license we are saving.
        /// </summary>
        public Guid Broker { get; set; }

        /// <summary>
        /// License to save.
        /// </summary>
        public LicenseDto License { get; set; }    
    }
}
