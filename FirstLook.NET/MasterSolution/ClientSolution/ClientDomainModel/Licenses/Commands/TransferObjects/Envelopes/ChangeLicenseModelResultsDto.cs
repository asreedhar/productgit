﻿using System;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;

namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Results of changing the license model of a license.
    /// </summary>
    [Serializable]
    public class ChangeLicenseModelResultsDto
    {
        /// <summary>
        /// Arguments used to produce these results.
        /// </summary>
        public ChangeLicenseModelArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Details of the broker.
        /// </summary>
        public BrokerDto Broker { get; set; }

        /// <summary>
        /// License that was changed.
        /// </summary>
        public LicenseDto License { get; set; }
    }
}
