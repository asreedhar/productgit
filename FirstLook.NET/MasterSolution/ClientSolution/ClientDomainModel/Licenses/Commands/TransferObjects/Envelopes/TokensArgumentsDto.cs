﻿using System;

namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments for retrieving tokens available to the user.
    /// </summary>
    [Serializable]
    public class TokensArgumentsDto
    {
        /// <summary>
        /// Broker.
        /// </summary>
        public Guid Broker { get; set; }

        /// <summary>
        /// Username.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Device identifier. Possibly not defined if the device has not been used before.
        /// </summary>
        public Guid? Device { get; set; }

        /// <summary>
        /// Device type. Used if a device guid has not yet been assigned.
        /// </summary>
        public DeviceTypeDto DeviceType { get; set; }
    }
}
