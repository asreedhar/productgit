﻿using System;

namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments for revoking a license.
    /// </summary>
    [Serializable]
    public class RevokeLicenseArgumentsDto
    {
        /// <summary>
        /// Broker identifier.
        /// </summary>
        public Guid Broker { get; set; }

        /// <summary>
        /// License identifier.
        /// </summary>
        public Guid License { get; set; }
    }
}
