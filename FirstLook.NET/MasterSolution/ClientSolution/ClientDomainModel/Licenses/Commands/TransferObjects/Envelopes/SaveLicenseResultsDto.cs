﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;

namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Results of saving a license.
    /// </summary>
    [Serializable]
    public class SaveLicenseResultsDto
    {
        /// <summary>
        /// Arguments used to save the license.
        /// </summary>
        public SaveLicenseArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Details of the broker.
        /// </summary>
        public BrokerDto Broker { get; set; }

        /// <summary>
        /// Client's licenses.
        /// </summary>
        public List<LicenseInfoDto> Licenses { get; set; }

        /// <summary>
        /// Saved license.
        /// </summary>
        public LicenseDto License { get; set; }
    }
}
