﻿using System;

namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class RevokeTokensArgumentsDto
    {
        /// <summary>
        /// Broker.
        /// </summary>
        public Guid Broker { get; set; }

        /// <summary>
        /// Username.
        /// </summary>
        public string UserName { get; set; }
    }
}
