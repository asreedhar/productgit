﻿using System;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;

namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Results of retrieving a particular lease.
    /// </summary>
    [Serializable]
    public class LeaseResultsDto
    {
        /// <summary>
        /// Arguments used to produce these results.
        /// </summary>
        public LeaseArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Broker details.
        /// </summary>
        public BrokerDto Broker { get; set; }

        /// <summary>
        /// License details.
        /// </summary>
        public LicenseInfoDto License { get; set; }        

        /// <summary>
        /// Lease details.
        /// </summary>
        public LeaseDto Lease { get; set; }        
    }
}
