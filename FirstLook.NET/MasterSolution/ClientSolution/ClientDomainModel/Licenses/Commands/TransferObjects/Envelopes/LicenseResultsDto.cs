﻿using System;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;

namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Results of retrieving a particular license.
    /// </summary>
    [Serializable]
    public class LicenseResultsDto
    {
        /// <summary>
        /// Arguments used to produce these results.
        /// </summary>
        public LicenseArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Details of the broker.
        /// </summary>
        public BrokerDto Broker { get; set; }

        /// <summary>
        /// Details of the license.
        /// </summary>
        public LicenseDto License { get; set; }
    }
}