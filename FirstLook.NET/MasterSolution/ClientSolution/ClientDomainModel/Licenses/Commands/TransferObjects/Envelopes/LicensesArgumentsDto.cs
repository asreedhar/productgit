﻿using System;

namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments used to retrieve all of a broker's licenses.
    /// </summary>
    [Serializable]
    public class LicensesArgumentsDto
    {        
        /// <summary>
        /// Broker identifier.
        /// </summary>
        public Guid Broker { get; set; }                       
    }
}