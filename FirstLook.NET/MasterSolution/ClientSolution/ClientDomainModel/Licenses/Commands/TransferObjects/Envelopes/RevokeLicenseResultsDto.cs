﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;

namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Results of revoking a license.
    /// </summary>
    [Serializable]
    public class RevokeLicenseResultsDto
    {
        /// <summary>
        /// Arguments used to produce these results.
        /// </summary>
        public RevokeLicenseArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Details of the broker.
        /// </summary>
        public BrokerDto Broker { get; set; }

        /// <summary>
        /// List of identifying details on the rest of the broker's licenses.
        /// </summary>
        public List<LicenseInfoDto> Licenses;
    }
}
