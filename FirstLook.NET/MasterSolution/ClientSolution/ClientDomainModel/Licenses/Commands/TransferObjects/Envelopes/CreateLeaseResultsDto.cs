﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;

namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Results of creating a lease.
    /// </summary>
    [Serializable]
    public class CreateLeaseResultsDto
    {
        /// <summary>
        /// Arguments used to produce these results.
        /// </summary>
        public CreateLeaseArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Broker details.
        /// </summary>
        public BrokerDto Broker { get; set; }

        /// <summary>
        /// License details.
        /// </summary>
        public LicenseInfoDto License { get; set; }

        /// <summary>
        /// Lease details.
        /// </summary>
        public LeaseDto Lease { get; set; }

        /// <summary>
        /// Identifying details of the license's leases.
        /// </summary>
        public List<LeaseInfoDto> Leases { get; set; }
    }
}
