﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;

namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Results of revoking a lease.
    /// </summary>
    [Serializable]
    public class RevokeLeaseResultsDto
    {
        /// <summary>
        /// Arguments used to produce these resutls.
        /// </summary>
        public RevokeLeaseArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Details of the broker.
        /// </summary>
        public BrokerDto Broker { get; set; }

        /// <summary>
        /// Details of the license.
        /// </summary>
        public LicenseDto License { get; set; }

        /// <summary>
        /// Identifying details of the license's leases.
        /// </summary>
        public List<LeaseInfoDto> Leases { get; set; }
    }
}
