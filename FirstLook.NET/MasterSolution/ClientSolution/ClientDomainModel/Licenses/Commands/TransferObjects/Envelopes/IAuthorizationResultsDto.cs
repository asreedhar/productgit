﻿namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Interface for authorization results.
    /// </summary>
    public interface IAuthorizationResultsDto
    {
        /// <summary>
        /// Result of authorizatino.
        /// </summary>
        TokenStatusDto AuthorizationStatus { get; set; }
    }
}
