﻿using System;

namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments to retrieve a particular license.
    /// </summary>
    [Serializable]
    public class LicenseArgumentsDto
    {
        /// <summary>
        /// Identifier of the broker whose license it is.
        /// </summary>
        public Guid Broker { get; set; }

        /// <summary>
        /// License identifier.
        /// </summary>
        public Guid License { get; set; }
    }
}