﻿using System;
using System.Collections.Generic;

namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Results of retrieving tokens available to a user.
    /// </summary>
    [Serializable]
    public class TokensResultsDto
    {
        /// <summary>
        /// Arguments used to produce these results.
        /// </summary>
        public TokensArgumentsDto Arguments { get; set; }

        /// <summary>
        /// Device identifier. This will be a new guid if no device guid was passed in with the arguments.
        /// </summary>
        public Guid Device { get; set; }

        /// <summary>
        /// List of each product and the token for it, if one exists.
        /// </summary>
        public List<TokenDto> Tokens { get; set; }
    }
}
