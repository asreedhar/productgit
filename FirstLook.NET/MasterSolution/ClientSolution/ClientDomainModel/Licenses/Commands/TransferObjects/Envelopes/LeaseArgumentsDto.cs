﻿using System;

namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments used to get details of a particular lease.
    /// </summary>
    [Serializable]
    public class LeaseArgumentsDto
    {
        /// <summary>
        /// Broker identifier.
        /// </summary>
        public Guid Broker { get; set; }

        /// <summary>
        /// License identifier.
        /// </summary>
        public Guid License { get; set; }

        /// <summary>
        /// Username.
        /// </summary>
        public string UserName { get; set; }
    }
}
