﻿using System;

namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes
{
    [Serializable]
    public class RevokeTokensResultsDto
    {
        /// <summary>
        /// Arguments used to produce these results.
        /// </summary>
        public RevokeTokensArgumentsDto Arguments { get; set; }
    }
}
