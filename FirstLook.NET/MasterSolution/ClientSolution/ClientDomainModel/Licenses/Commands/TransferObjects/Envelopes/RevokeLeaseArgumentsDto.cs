﻿using System;

namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes
{
    /// <summary>
    /// Arguments for revoking the lease of a license to a user and device.
    /// </summary>
    [Serializable]
    public class RevokeLeaseArgumentsDto
    {
        /// <summary>
        /// Broker identifier.
        /// </summary>
        public Guid Broker { get; set; }

        /// <summary>
        /// License identifier.
        /// </summary>
        public Guid License { get; set; }

        /// <summary>
        /// Username.
        /// </summary>
        public string UserName { get; set; }
    }
}
