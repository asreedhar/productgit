﻿using System;

namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects
{
    /// <summary>
    /// Authorization arguments.
    /// </summary>
    [Serializable]
    public class AuthorizationDto
    {
        /// <summary>
        /// Lease token.
        /// </summary>
        public Guid Token { get; set; }
    }
}