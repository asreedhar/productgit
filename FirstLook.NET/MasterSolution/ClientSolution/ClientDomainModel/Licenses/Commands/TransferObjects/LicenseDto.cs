﻿using System;

namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects
{
    /// <summary>
    /// Details of a broker's license.
    /// </summary>
    [Serializable]
    public class LicenseDto : LicenseInfoDto
    {        
        /// <summary>
        /// Number of seats available, if the license restricts seats.
        /// </summary>
        public int? Seats { get; set; }

        /// <summary>
        /// Is there a usage limit?
        /// </summary>
        public bool UseLimit { get; set; }

        /// <summary>
        /// Number of uses if there is a usage limit.
        /// </summary>
        public int? AllowedUses { get; set; }

        /// <summary>
        /// Does this license expire?
        /// </summary>
        public bool Expires { get; set; }

        /// <summary>
        /// Date of the expiration of this license, if it expires.
        /// </summary>
        public DateTime? ExpiryDate { get; set; }
    }
}
