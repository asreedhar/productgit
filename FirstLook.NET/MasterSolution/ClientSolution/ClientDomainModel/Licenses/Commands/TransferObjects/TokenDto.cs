﻿using System;

namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects
{
    /// <summary>
    /// Token for access to a product by a user.
    /// </summary>
    [Serializable]
    public class TokenDto
    {
        /// <summary>
        /// Product the token is (or would be) for.
        /// </summary>
        public string Product { get; set; }

        /// <summary>
        /// Status of the token (e.g. is valid, doesn't exist, has expired, etc).
        /// </summary>
        public TokenStatusDto Status { get; set; }

        /// <summary>
        /// Token. Null if no valid token exists.
        /// </summary>
        public Guid? Token { get; set; }               
    }
}
