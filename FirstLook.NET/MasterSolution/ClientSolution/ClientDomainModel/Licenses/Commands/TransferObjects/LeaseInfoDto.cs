﻿using System;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;

namespace FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects
{
    /// <summary>
    /// Identifying details of a lease.
    /// </summary>
    [Serializable]
    public class LeaseInfoDto
    {        
        /// <summary>
        /// Leased user.
        /// </summary>
        public UserDto User { get; set; }         
    }
}
