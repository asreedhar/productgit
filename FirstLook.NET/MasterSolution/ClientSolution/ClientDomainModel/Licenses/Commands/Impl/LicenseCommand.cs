﻿using System;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Licenses.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Licenses.Commands.Impl
{
    /// <summary>
    /// Command to get the details of a license. For license administration.
    /// </summary>
    [Serializable]
    public class LicenseCommand : ICommand<LicenseResultsDto, IdentityContextDto<LicenseArgumentsDto>>
    {
        /// <summary>
        /// Get the details of a license.
        /// </summary>
        /// <param name="parameters">Broker and license identifiers.</param>
        /// <returns>Details of the license.</returns>
        public LicenseResultsDto Execute(IdentityContextDto<LicenseArgumentsDto> parameters)
        {
            ILicenseRepository  repository = RegistryFactory.GetResolver().Resolve<ILicenseRepository>();
            IdentityDto         identity   = parameters.Identity;
            LicenseArgumentsDto arguments  = parameters.Arguments;

            // Fetch the broker.
            IBroker broker = Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);

            // Fetch the license.
            License license = repository.Fetch_License(arguments.License);

            return new LicenseResultsDto
            {
                Arguments = arguments,
                Broker    = new BrokerDto
                {
                    AuthorityName = broker.AuthorityName,
                    Client        = Clients.Commands.Impl.Mapper.Map(broker.Client),
                    Handle        = broker.Handle
                },
                License = Mapper.Map(license)
            };
        }
    }
}
