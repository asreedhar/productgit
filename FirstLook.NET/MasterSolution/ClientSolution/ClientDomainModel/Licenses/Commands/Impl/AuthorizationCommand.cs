﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;

namespace FirstLook.Client.DomainModel.Licenses.Commands.Impl
{
    /// <summary>
    /// Command authorizer. Parameters to the authorized command do not need to be wrapped in identity context 
    /// information, as these are passed in explicitly.
    /// </summary>
    /// <typeparam name="TResults">Command results.</typeparam>
    /// <typeparam name="TArguments">Command arguments.</typeparam>
    public class AuthorizationCommandA<TResults, TArguments> : ICommand<TResults, TArguments>
        where TResults : IAuthorizationResultsDto, new()
        where TArguments : IAuthorizationArgumentsDto
    {
        /// <summary>
        /// Command to ensure the user is authorized to run.
        /// </summary>
        private readonly ICommand<TResults, TArguments> _command;

        /// <summary>
        /// User identity information.
        /// </summary>
        private readonly IIdentityContextDto _context;

        /// <summary>
        /// Initialize this authorization command.
        /// </summary>
        /// <param name="command">Command to authorize.</param>
        /// <param name="context">User identity.</param>
        public AuthorizationCommandA(ICommand<TResults, TArguments> command, IIdentityContextDto context)
        {
            _command = command;
            _context = context;
        }

        /// <summary>
        /// Ensure a user is authorized to execute a command.
        /// </summary>
        /// <param name="parameters">Command parameters.</param>
        /// <returns>Command results.</returns>
        public TResults Execute(TArguments parameters)
        {
            TokenStatusDto status = Assertion.Check(_context.Identity, parameters.Authorization);

            TResults results = status == TokenStatusDto.Valid
                                   ? _command.Execute(parameters)
                                   : new TResults();

            results.AuthorizationStatus = status;

            return results;
        }
    }

    /// <summary>
    /// Command authorizer. Parameters to the authorized command need to be wrapped in identity context information.
    /// </summary>
    /// <typeparam name="TResults">Command results.</typeparam>
    /// <typeparam name="TArguments">Identity context wrapped arguments.</typeparam>
    /// <typeparam name="TWrapped">Type of arguments that are wrapped.</typeparam>
    public class AuthorizationCommandB<TResults, TArguments, TWrapped> : ICommand<TResults, TArguments>
        where TResults : IAuthorizationResultsDto, new()
        where TArguments : IIdentityContextDto<TWrapped>
        where TWrapped : IAuthorizationArgumentsDto
    {
        /// <summary>
        /// Command to ensure the user is authorized to run.
        /// </summary>
        private readonly ICommand<TResults, TArguments> _command;        

        /// <summary>
        /// Initialize this authorization command.
        /// </summary>
        /// <param name="command">Command to authorize.</param>
        public AuthorizationCommandB(ICommand<TResults, TArguments> command)
        {
            _command = command;
        }
        
        /// <summary>
        /// Ensure a user is authorized to execute a command.
        /// </summary>
        /// <param name="parameters">Command parameters.</param>
        /// <returns>Command results.</returns>
        public TResults Execute(TArguments parameters)
        {
            TokenStatusDto status = Assertion.Check(parameters.Identity, parameters.Arguments.Authorization);

            TResults results = status == TokenStatusDto.Valid
                                   ? _command.Execute(parameters)
                                   : new TResults();

            results.AuthorizationStatus = status;

            return results;
        }
    }
}