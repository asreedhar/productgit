﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Repositories.Entities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Licenses.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Licenses.Commands.Impl
{
    /// <summary>
    /// Command to retrieve authorization tokens available to a user.
    /// </summary>
    [Serializable]
    public class TokensCommand : ICommand<TokensResultsDto, IdentityContextDto<TokensArgumentsDto>>
    {
        /// <summary>
        /// Get the authorization status of each product for a user on a device. If the device has not been used before
        /// it will be registered and all active tokens on other devices for that user will be invalidated.
        /// </summary>
        /// <param name="parameters">User and device identifiers.</param>
        /// <returns>List of each product and the authorization status of each.</returns>
        public TokensResultsDto Execute(IdentityContextDto<TokensArgumentsDto> parameters)
        {            
            ILicenseRepository repository = RegistryFactory.GetResolver().Resolve<ILicenseRepository>();
            IdentityDto        identity   = parameters.Identity;
            TokensArgumentsDto arguments  = parameters.Arguments;

            // Fetch the broker and principal.
            IBroker    broker    = Clients.Utilities.Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);
            IPrincipal principal = Clients.Utilities.Principal.Get(identity.Name, identity.AuthorityName);
            
            IAuthority authority = AuthorityFactory.GetAuthority(identity.AuthorityName);
            IAccountManager acctManager = authority.AccountManager;

            // Fetch the leased user.
            IUser     leasedUser      = acctManager.Users(broker.Client).Where(x => x.UserName.Equals(arguments.UserName)).FirstOrDefault();
            Principal leasedPrincipal = (Principal)acctManager.Principal(leasedUser);
            int       userId          = leasedPrincipal.Entity.Id;

            // If a device identifier is present, fetch that device. Otherwise create a new device.
            Device device = arguments.Device.HasValue ? repository.Fetch_Device(arguments.Device.Value) 
                                                      : repository.Create_Device(principal, (DeviceType)arguments.DeviceType);               
            if (device == null)
            {
                throw new ArgumentException("Cannot retrieve or create the device.");
            }

            // Invalidate tokens for devices of the same type OTHER THAN this one.
            repository.Delete_Tokens(principal, userId, device.Type, device.Handle);

            // Get all of the client's licenses for this device type.
            Dictionary<string, LicenseInfo> licenses = new Dictionary<string, LicenseInfo>();            
            foreach (LicenseInfo info in repository.Fetch_Licenses(broker).Where(x => x.DeviceType == device.Type))
            {                
                licenses[info.Product.Name] = info;                
            }

            List<TokenDto> tokens = new List<TokenDto>();

            // Prepare a token status for each product.
            foreach (Product product in repository.Fetch_Products())
            {                
                TokenDto tokenDto = new TokenDto { Product = product.Name };

                // If this product is unlicensed, move along.
                if (!licenses.ContainsKey(product.Name))
                {
                    tokenDto.Status = TokenStatusDto.NoLicense;
                    tokens.Add(tokenDto);
                    continue;
                }

                // Get the license details.
                LicenseInfo license = licenses[product.Name];
                if (license == null || !license.Handle.HasValue)
                {
                    throw new ApplicationException("Invalid license.");
                }
                License licenseDetails = repository.Fetch_License(license.Handle.Value);
                
                // Validate license usage.
                if (licenseDetails.UsageLimit != null && licenseDetails.Uses >= licenseDetails.UsageLimit.Uses)
                {
                    tokenDto.Status = TokenStatusDto.UsageLimitExceeded;
                    tokens.Add(tokenDto);
                    continue;
                }

                // Validate license expiration.
                if (licenseDetails.Expiration != null && licenseDetails.Expiration.Date < DateTime.Now)
                {
                    tokenDto.Status = TokenStatusDto.ExpiredLicense;
                    tokens.Add(tokenDto);
                    continue;
                }                

                // Get the lease. If it doesn't exist and the license is not Named, create it.
                Lease lease = repository.Fetch_Lease(license.Handle.Value, userId);
                if (lease == null)
                {                                        
                    if (license.LicenseModel == LicenseModelType.Named)
                    {
                        tokenDto.Status = TokenStatusDto.NoLease;
                        tokens.Add(tokenDto);
                        continue;
                    }

                    lease = repository.Create_Lease(principal, license.Handle.Value, userId);
                    if (lease == null)
                    {
                        throw new ApplicationException("Could not create lease.");
                    }
                }                

                // Get the token for the lease on a device. If it doesn't exist, try to create it.
                Token token = repository.Fetch_Token(license.Handle.Value, userId, device.Handle);                
                if (token == null)
                {                    
                    // Validate license seat limit.
                    if (licenseDetails.SeatLimit != null && licenseDetails.SeatLimit.Seats > 0)
                    {
                        int activeTokens =
                            repository.Fetch_Tokens(license.Handle.Value).Where(x => x.ExpiresOn > DateTime.Now).Count();

                        if (activeTokens >= licenseDetails.SeatLimit.Seats)
                        {
                            tokenDto.Status = TokenStatusDto.SeatLimitExceed;
                            tokens.Add(tokenDto);
                            continue;
                        }
                    }

                    // Floating licenses expires every 15 minutes. Others don't practically expire.
                    DateTime expiration = license.LicenseModel == LicenseModelType.Floating ? 
                        DateTime.Now.AddMinutes(15) : DateTime.Now.AddYears(100);

                    token = new Token
                    {
                        Device    = device.Handle,
                        License   = license.Handle.Value,
                        UserId    = userId,
                        ExpiresOn = expiration
                    };

                    // Save the token.
                    token = repository.Save_Token(principal, token);
                    if (token == null)
                    {
                        throw new DataException("Could not create token.");
                    }
                }

                // If the token exists but has expired, see if it can be extended. Only possible for floating licenses.
                else if (token.ExpiresOn < DateTime.Now)
                {                    
                    if (license.LicenseModel != LicenseModelType.Floating)
                    {
                        tokenDto.Status = TokenStatusDto.ExpiredToken;
                        tokens.Add(tokenDto);
                        continue;
                    }                    

                    // Validate license seat limit.
                    if (licenseDetails.SeatLimit != null && licenseDetails.SeatLimit.Seats > 0)
                    {
                        int activeTokens =
                            repository.Fetch_Tokens(token.License).Where(x => x.ExpiresOn > DateTime.Now).Count();

                        if (activeTokens >= licenseDetails.SeatLimit.Seats)
                        {
                            tokenDto.Status = TokenStatusDto.SeatLimitExceed;
                            tokens.Add(tokenDto);
                            continue;
                        }
                    }

                    // Extend the expiration of the token.
                    token.ExpiresOn = DateTime.Now.AddMinutes(15);

                    token = repository.Save_Token(principal, token);
                    if (token == null)
                    {
                        throw new DataException("Could not update token.");
                    }
                }

                // License is valid, lease is valid, token is valid: we're valid.
                tokenDto.Status = TokenStatusDto.Valid;
                tokenDto.Token = token.Handle;
                tokens.Add(tokenDto);

                // Update the usage of the license.
                licenseDetails.Uses++;
                repository.Save_License(principal, broker, licenseDetails);

                // Mark the lease as having been used.
                repository.Use_Lease(principal, license.Handle.Value, userId);
            }

            return new TokensResultsDto
            {
                Arguments = arguments,
                Device    = device.Handle,
                Tokens    = tokens
            };
        }
    }
}
