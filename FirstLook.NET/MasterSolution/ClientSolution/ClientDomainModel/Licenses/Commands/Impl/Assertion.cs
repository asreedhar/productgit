﻿using System;
using System.Data;
using System.Linq;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Licenses.Model;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Licenses.Commands.Impl
{
    /// <summary>
    /// Helper for asserting authorization.
    /// </summary>
    internal static class Assertion
    {
        /// <summary>
        /// Check the authorization status of a user accessing a licensed product.
        /// </summary>
        /// <param name="identity">User identity.</param>
        /// <param name="arguments">Authorization arguments.</param>
        /// <returns>Authorization status.</returns>
        public static TokenStatusDto Check(IdentityDto identity, AuthorizationDto arguments)
        {
            ILicenseRepository repository = RegistryFactory.GetResolver().Resolve<ILicenseRepository>();            

            IPrincipal principal = Clients.Utilities.Principal.Get(identity.Name, identity.AuthorityName);
           
            // Fetch and validate the token.
            Token token = repository.Fetch_Token(arguments.Token);
            if (token == null)
            {
                return TokenStatusDto.InvalidToken;
            }

            // Fetch and validate the license.
            License license = repository.Fetch_License(token.License);
            if (license == null)
            {
                return TokenStatusDto.NoLicense;
            }
            if (license.Expiration != null && license.Expiration.Date < DateTime.Now)
            {
                return TokenStatusDto.ExpiredLicense;
            }

            // If the token is past its expiration date, see if it can be extended.
            if (token.ExpiresOn < DateTime.Now)
            {
                // Can only extend tokens on floating licenses.
                if (license.LicenseModel != LicenseModelType.Floating)
                {
                    return TokenStatusDto.ExpiredToken;
                }

                // Make sure there's availability for the token.
                if (license.SeatLimit != null && license.SeatLimit.Seats > 0)
                {
                    int activeTokens =
                        repository.Fetch_Tokens(token.License).Where(x => x.ExpiresOn < DateTime.Now).Count();

                    if (activeTokens >= license.SeatLimit.Seats)
                    {
                        return TokenStatusDto.SeatLimitExceed;
                    }
                }

                // Extend the expiration of the token.
                token.ExpiresOn = DateTime.Now.AddMinutes(15);

                token = repository.Save_Token(principal, token);

                if (token == null)
                {
                    throw new DataException("Could not update token.");
                }
            }

            // Token is valid.
            return TokenStatusDto.Valid;
        }
    }
}