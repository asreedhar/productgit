﻿using System;
using System.Linq;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Licenses.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Licenses.Commands.Impl
{
    /// <summary>
    /// Command to get a broker's licenses. For license administration.
    /// </summary>
    [Serializable]
    public class LicensesCommand : ICommand<LicensesResultsDto, IdentityContextDto<LicensesArgumentsDto>>
    {
        /// <summary>
        /// Get a broker's licenses.
        /// </summary>
        /// <param name="parameters">Broker identifier.</param>
        /// <returns>Broker's licenses.</returns>
        public LicensesResultsDto Execute(IdentityContextDto<LicensesArgumentsDto> parameters)
        {            
            ILicenseRepository   repository = RegistryFactory.GetResolver().Resolve<ILicenseRepository>();
            IdentityDto          identity   = parameters.Identity;
            LicensesArgumentsDto arguments  = parameters.Arguments;

            // Fetch the broker.
            IBroker broker = Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);

            // Fetch all of the client's licenses, sorted by product, device.            
            var licenses = from l 
                           in repository.Fetch_Licenses(broker)
                           orderby l.Product, l.DeviceType
                           select l;

            return new LicensesResultsDto
            {
                Arguments = arguments,
                Broker    = new BrokerDto
                {
                    AuthorityName = broker.AuthorityName,
                    Client        = Clients.Commands.Impl.Mapper.Map(broker.Client),
                    Handle        = broker.Handle
                },
                Licenses = Mapper.Map(licenses)
            };
        }
    }
}
