﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Repositories.Entities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Licenses.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using Broker = FirstLook.Client.DomainModel.Clients.Utilities.Broker;

namespace FirstLook.Client.DomainModel.Licenses.Commands.Impl
{
    /// <summary>
    /// Command to revoke a lease. For license administration.
    /// </summary>
    [Serializable]
    public class RevokeLeaseCommand : ICommand<RevokeLeaseResultsDto, IdentityContextDto<RevokeLeaseArgumentsDto>>
    {
        /// <summary>
        /// Revoke a lease.
        /// </summary>
        /// <param name="parameters">Broker, license and lease identifiers.</param>
        /// <returns>List of remaining leases for the license.</returns>
        public RevokeLeaseResultsDto Execute(IdentityContextDto<RevokeLeaseArgumentsDto> parameters)
        {
            ILicenseRepository      repository = RegistryFactory.GetResolver().Resolve<ILicenseRepository>();
            IdentityDto             identity   = parameters.Identity;
            RevokeLeaseArgumentsDto arguments  = parameters.Arguments;

            // Fetch the broker and principal user.
            IBroker    broker    = Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);
            IPrincipal principal = Clients.Utilities.Principal.Get(identity.Name, identity.AuthorityName);
            
            // Fetch the license and the leased user.
            License license = repository.Fetch_License(arguments.License);
            Principal user = Clients.Utilities.Principal.Get(arguments.UserName, identity.AuthorityName) as Principal;
            if (user == null)
            {
                throw new ApplicationException("Could not get user.");
            }
            User entity = user.Entity;

            // Delete the lease.
            repository.Delete_Lease(principal, arguments.License, entity.Id);

            // Fetch the remaining leases.
            IList<LeaseInfo> leases = repository.Fetch_Leases(arguments.License);

            return new RevokeLeaseResultsDto
            {
                Arguments = arguments,
                Broker    = new BrokerDto
                {
                    AuthorityName = broker.AuthorityName,
                    Client        = Clients.Commands.Impl.Mapper.Map(broker.Client),
                    Handle        = broker.Handle
                },                
                License = Mapper.Map(license),
                Leases  = Mapper.Map(leases)
            };
        }
    }
}
