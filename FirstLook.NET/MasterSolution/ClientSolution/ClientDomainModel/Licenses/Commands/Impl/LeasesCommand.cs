﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Licenses.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Licenses.Commands.Impl
{
    /// <summary>
    /// Command to get all leases tied to a license. For license administration.
    /// </summary>
    [Serializable]
    public class LeasesCommand : ICommand<LeasesResultsDto, IdentityContextDto<LeasesArgumentsDto>>
    {
        /// <summary>
        /// Get all leases tied to a license.
        /// </summary>
        /// <param name="parameters">Broker and license identifiers.</param>
        /// <returns>Broker and license detais, and a list of leases.</returns>
        public LeasesResultsDto Execute(IdentityContextDto<LeasesArgumentsDto> parameters)
        {            
            ILicenseRepository repository = RegistryFactory.GetResolver().Resolve<ILicenseRepository>();
            IdentityDto        identity   = parameters.Identity;
            LeasesArgumentsDto arguments  = parameters.Arguments;
            
            // Fetch the broker.
            IBroker broker = Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);            

            // Fetch the license.
            License license = repository.Fetch_License(arguments.License);

            // Fetch all of the license's leases.
            IList<LeaseInfo> leases = repository.Fetch_Leases(arguments.License);

            return new LeasesResultsDto
            {
                Arguments = arguments,
                Broker    = new BrokerDto
                {
                    AuthorityName = broker.AuthorityName,
                    Client        = Clients.Commands.Impl.Mapper.Map(broker.Client),
                    Handle        = broker.Handle
                },
                License = Mapper.Map(license),
                Leases  = Mapper.Map(leases)
            };
        }
    }
}
