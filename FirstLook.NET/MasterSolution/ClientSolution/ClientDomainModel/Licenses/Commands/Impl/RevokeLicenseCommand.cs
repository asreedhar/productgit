﻿using System;
using System.Linq;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Licenses.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Licenses.Commands.Impl
{
    /// <summary>
    /// Command to revoke a license. For license administration.
    /// </summary>
    [Serializable]
    public class RevokeLicenseCommand : ICommand<RevokeLicenseResultsDto, IdentityContextDto<RevokeLicenseArgumentsDto>>
    {
        /// <summary>
        /// Revoke a license.
        /// </summary>
        /// <param name="parameters">Broker and license identifiers.</param>
        /// <returns>List of the broker's licenses.</returns>
        public RevokeLicenseResultsDto Execute(IdentityContextDto<RevokeLicenseArgumentsDto> parameters)
        {
            ILicenseRepository        repository = RegistryFactory.GetResolver().Resolve<ILicenseRepository>();
            IdentityDto               identity   = parameters.Identity;
            RevokeLicenseArgumentsDto arguments  = parameters.Arguments;

            // Fetch the broker and principal user.
            IBroker    broker    = Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);
            IPrincipal principal = Principal.Get(identity.Name, identity.AuthorityName);            

            // Delete the license.
            repository.Delete_License(principal, arguments.License);

            // Fetch the remaining licenses.
            var licenses = from l
                           in repository.Fetch_Licenses(broker)
                           orderby l.Product, l.DeviceType
                           select l;

            return new RevokeLicenseResultsDto
            {
                Arguments = arguments,
                Broker    = new BrokerDto
                {
                    AuthorityName = broker.AuthorityName,
                    Client        = Clients.Commands.Impl.Mapper.Map(broker.Client),
                    Handle        = broker.Handle
                },
                Licenses = Mapper.Map(licenses)
            };
        }
    }
}
