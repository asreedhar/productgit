﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Licenses.Model;

namespace FirstLook.Client.DomainModel.Licenses.Commands.Impl
{
    /// <summary>
    /// Utility for mapping between license entities and transfer objects.
    /// </summary>
    public static class Mapper
    {
        #region Licenses

        /// <summary>
        /// Map a collection of license details to a collection of transfer objects.
        /// </summary>
        /// <param name="licenses">Collection of details on licenses.</param>
        /// <returns>Collection of transfer objects.</returns>
        public static List<LicenseInfoDto> Map(IEnumerable<LicenseInfo> licenses)
        {
            List<LicenseInfoDto> dtos = new List<LicenseInfoDto>();

            foreach (LicenseInfo license in licenses)
            {
                dtos.Add(new LicenseInfoDto
                {
                    DeviceType   = (DeviceTypeDto)license.DeviceType,
                    License      = license.Handle,
                    LicenseModel = (LicenseModelTypeDto)license.LicenseModel,
                    Product      = license.Product.Name,
                    Uses         = license.Uses                               
                });
            }

            return dtos;
        }

        /// <summary>
        /// Map a license to a transfer object.
        /// </summary>
        /// <param name="license">License.</param>
        /// <returns>License transfer object, or null.</returns>
        public static LicenseDto Map(License license)
        {
            if (license == null)
            {
                return null;
            }

            DateTime? expiration = license.Expiration == null ? (DateTime?)null : license.Expiration.Date;
            int?      seats      = license.SeatLimit  == null ? (int?)null      : license.SeatLimit.Seats;
            int?      usage      = license.UsageLimit == null ? (int?)null      : license.UsageLimit.Uses;

            return new LicenseDto
            {
                // License info.
                License      = license.Handle,
                DeviceType   = (DeviceTypeDto)license.DeviceType,                
                LicenseModel = (LicenseModelTypeDto)license.LicenseModel,
                Product      = license.Product.Name,
                Uses         = license.Uses,

                // License details.
                Expires     = expiration.HasValue,
                ExpiryDate  = expiration,
                Seats       = seats,
                UseLimit    = usage.HasValue,
                AllowedUses = usage
            };
        }      

        /// <summary>
        /// Map a transfer object into a license.
        /// </summary>
        /// <param name="dto">Transfer object.</param>
        /// <param name="product">Product information.</param>
        /// <returns>License.</returns>
        public static License Map(LicenseDto dto, Product product)
        {
            LicenseExpiration expiry = null;
            if (dto.Expires && dto.ExpiryDate.HasValue)
            {
                expiry = new LicenseExpiration { Date = dto.ExpiryDate.Value };                
            }

            LicenseSeatLimit seats = null;
            if (dto.Seats.HasValue)
            {
                seats = new LicenseSeatLimit { Seats = dto.Seats.Value };
            }

            LicenseUsageLimit usage = null;
            if (dto.UseLimit && dto.AllowedUses.HasValue)
            {
                usage = new LicenseUsageLimit { Uses = dto.AllowedUses.Value };
            }

            return new License
            {                
                // License details.
                Handle       = dto.License,
                DeviceType   = (DeviceType)dto.DeviceType,
                LicenseModel = (LicenseModelType)dto.LicenseModel,
                Product      = product,
                Uses         = dto.Uses,

                // Parameters.
                Expiration = expiry,
                SeatLimit  = seats,
                UsageLimit = usage
            };
        }

        #endregion

        #region Leases

        /// <summary>
        /// Map a list of identifying details of a lease into a list of transfer objects.
        /// </summary>
        /// <param name="leases">List of leases.</param>
        /// <returns>List of transfer objects.</returns>
        public static List<LeaseInfoDto> Map(IList<LeaseInfo> leases)
        {
            List<LeaseInfoDto> dtos = new List<LeaseInfoDto>();

            foreach (LeaseInfo lease in leases)
            {
                dtos.Add(new LeaseInfoDto { User = Clients.Commands.Impl.Mapper.Map(lease.User) });
            }

            return dtos;
        }

        /// <summary>
        /// Map a lease into a transfer object.
        /// </summary>
        /// <param name="lease">Lease.</param>
        /// <returns>Lease transfer object, or null.</returns>
        public static LeaseDto Map(Lease lease)
        {
            if (lease == null)
            {
                return null;
            }

            return new LeaseDto
            {                
                AssignedBy = Clients.Commands.Impl.Mapper.Map(lease.AssignedBy),
                AssignedOn = lease.AssignedOn,                
                LastUsed   = lease.LastUsed,
                Uses       = lease.Uses,
                User       = Clients.Commands.Impl.Mapper.Map(lease.User)
            };
        }

        #endregion
    }
}
