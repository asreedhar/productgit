﻿using System;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Licenses.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Licenses.Commands.Impl
{
    /// <summary>
    /// Command to change the license model type of a license. For license administration.
    /// </summary>
    [Serializable]
    public class ChangeLicenseModelCommand : ICommand<ChangeLicenseModelResultsDto, IdentityContextDto<ChangeLicenseModelArgumentsDto>>
    {
        /// <summary>
        /// Change the license model type of a license.
        /// </summary>
        /// <param name="parameters">Broker and license identifiers, and the new model type to change to.</param>
        /// <returns>List of the broker's licenses.</returns>
        public ChangeLicenseModelResultsDto Execute(IdentityContextDto<ChangeLicenseModelArgumentsDto> parameters)
        {
            ILicenseRepository repository = RegistryFactory.GetResolver().Resolve<ILicenseRepository>();
            IdentityDto identity = parameters.Identity;
            ChangeLicenseModelArgumentsDto arguments = parameters.Arguments;

            // Fetch the broker and principal user.
            IBroker broker = Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);
            IPrincipal principal = Principal.Get(identity.Name, identity.AuthorityName);

            License license = repository.Fetch_License(arguments.License);

            // Save if something has changed.
            if (license.LicenseModel != (LicenseModelType)arguments.Model)
            {
                license.LicenseModel = (LicenseModelType)arguments.Model;                
                license = repository.Save_License(principal, broker, license);   
            }            

            return new ChangeLicenseModelResultsDto
            {
                Arguments = arguments,
                Broker = new BrokerDto
                {
                    AuthorityName = broker.AuthorityName,
                    Client = Clients.Commands.Impl.Mapper.Map(broker.Client),
                    Handle = broker.Handle
                },
                License = Mapper.Map(license)
            };
        }
    }
}
