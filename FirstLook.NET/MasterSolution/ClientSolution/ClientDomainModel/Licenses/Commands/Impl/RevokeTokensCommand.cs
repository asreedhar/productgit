﻿using System.Linq;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Repositories.Entities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Licenses.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Licenses.Commands.Impl
{
    /// <summary>
    /// Command to revoke all of a user's tokens.
    /// </summary>
    public class RevokeTokensCommand :  ICommand<RevokeTokensResultsDto, IdentityContextDto<RevokeTokensArgumentsDto>>
    {
        /// <summary>
        /// Revoke all of a user's tokens.
        /// </summary>
        /// <param name="parameters">Broker and user identifiers.</param>
        /// <returns>Nothing, really.</returns>
        public RevokeTokensResultsDto Execute(IdentityContextDto<RevokeTokensArgumentsDto> parameters)
        {
            ILicenseRepository repository = RegistryFactory.GetResolver().Resolve<ILicenseRepository>();
            IdentityDto identity = parameters.Identity;
            RevokeTokensArgumentsDto arguments = parameters.Arguments;

            // Fetch the broker and principal.
            IBroker broker = Clients.Utilities.Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);
            IPrincipal principal = Clients.Utilities.Principal.Get(identity.Name, identity.AuthorityName);

            // Fetch the leased user.
            IAuthority authority = AuthorityFactory.GetAuthority(identity.AuthorityName);
            IAccountManager acctManager = authority.AccountManager;
            IUser leasedUser = acctManager.Users(broker.Client).Where(x => x.UserName.Equals(arguments.UserName)).FirstOrDefault();
            Principal prin = (Principal)acctManager.Principal(leasedUser);
            int userId = prin.Entity.Id;

            // Delete the tokens!
            repository.Delete_Tokens(principal, userId, null, null);

            return new RevokeTokensResultsDto
            {
                Arguments = arguments
            };
        }
    }
}
