﻿using System;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Repositories.Entities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Licenses.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using Broker = FirstLook.Client.DomainModel.Clients.Utilities.Broker;

namespace FirstLook.Client.DomainModel.Licenses.Commands.Impl
{
    /// <summary>
    /// Command to get a particular lease tied to a license. For license administration.
    /// </summary>
    [Serializable]
    public class LeaseCommand : ICommand<LeaseResultsDto, IdentityContextDto<LeaseArgumentsDto>>
    {
        /// <summary>
        /// Get a specific lease.
        /// </summary>
        /// <param name="parameters">Broker, license and lease identifiers.</param>
        /// <returns>Lease details.</returns>
        public LeaseResultsDto Execute(IdentityContextDto<LeaseArgumentsDto> parameters)
        {
            ILicenseRepository repository = RegistryFactory.GetResolver().Resolve<ILicenseRepository>();
            IdentityDto        identity   = parameters.Identity;
            LeaseArgumentsDto  arguments  = parameters.Arguments;            

            // Fetch the broker and user.
            IBroker broker = Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);
            Principal user = Clients.Utilities.Principal.Get(arguments.UserName, identity.AuthorityName) as Principal;
            if (user == null)
            {
                throw new ApplicationException("Could not get user.");
            }
            User entity = user.Entity;

            // Fetch the license and lease.
            License license = repository.Fetch_License(arguments.License);
            Lease   lease   = repository.Fetch_Lease(arguments.License, entity.Id);

            return new LeaseResultsDto
            {
                Arguments = arguments,
                Broker    = new BrokerDto
                {
                    AuthorityName = broker.AuthorityName,
                    Client        = Clients.Commands.Impl.Mapper.Map(broker.Client),
                    Handle        = broker.Handle
                },                
                License = Mapper.Map(license),
                Lease   = Mapper.Map(lease)
            };
        }
    }
}
