﻿using System;
using System.Collections.Generic;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Repositories.Entities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Licenses.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;
using Broker = FirstLook.Client.DomainModel.Clients.Utilities.Broker;

namespace FirstLook.Client.DomainModel.Licenses.Commands.Impl
{
    /// <summary>
    /// Command to create a lease. For license administration.
    /// </summary>
    [Serializable]
    public class CreateLeaseCommand : ICommand<CreateLeaseResultsDto, IdentityContextDto<CreateLeaseArgumentsDto>>
    {
        /// <summary>
        /// Create a lease by tying a user to a license.
        /// </summary>
        /// <param name="parameters">Broker, license and user identifiers.</param>
        /// <returns>Details of the new lease.</returns>
        public CreateLeaseResultsDto Execute(IdentityContextDto<CreateLeaseArgumentsDto> parameters)
        {
            ILicenseRepository repository = RegistryFactory.GetResolver().Resolve<ILicenseRepository>();
            IdentityDto identity = parameters.Identity;
            CreateLeaseArgumentsDto arguments = parameters.Arguments;

            // Fetch the broker and principal user.
            IBroker broker = Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);
            IPrincipal principal = Clients.Utilities.Principal.Get(identity.Name, identity.AuthorityName);

            // Fetch the license and leased user.
            License license = repository.Fetch_License(arguments.License);

            Principal user = Clients.Utilities.Principal.Get(arguments.UserName, identity.AuthorityName) as Principal;
            if (user == null)
            {
                throw new ApplicationException("Could not get user.");
            }
            User entity = user.Entity;

            // Verify the license model.
            if (license.LicenseModel != LicenseModelType.Named)
            {
                throw new ArgumentException("Can only explicitly create leases for named licenses.");
            }

            // Check that a lease doesn't already exist.
            Lease lease = repository.Fetch_Lease(arguments.License, entity.Id);
            if (lease != null)
            {
                throw new ApplicationException("Lease for user on license already exists.");
            }

            // Verify the availability of a seat.
            if (license.SeatLimit != null && license.SeatLimit.Seats > 0)
            {                
                if (repository.Fetch_Leases(arguments.License).Count >= license.SeatLimit.Seats)
                {
                    throw new ArgumentException("Cannot add lease because there are no available seats.");
                }
            }

            // Create the lease.
            lease = repository.Create_Lease(principal, arguments.License, entity.Id);

            // Get the license's other leases.
            IList<LeaseInfo> leases = repository.Fetch_Leases(arguments.License);

            return new CreateLeaseResultsDto
            {
                Arguments = arguments,
                Broker    = new BrokerDto
                {
                    AuthorityName = broker.AuthorityName,
                    Client        = Clients.Commands.Impl.Mapper.Map(broker.Client),
                    Handle        = broker.Handle
                },
                License = Mapper.Map(license),
                Lease   = Mapper.Map(lease),
                Leases  = Mapper.Map(leases)
            };
        }
    }
}
