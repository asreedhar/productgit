﻿using System;
using System.Linq;
using FirstLook.Client.DomainModel.Clients.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Utilities;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Licenses.Model;
using FirstLook.Common.Core.Command;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Licenses.Commands.Impl
{
    /// <summary>
    /// Command to save a license. For license administration.
    /// </summary>
    [Serializable]
    public class SaveLicenseCommand : ICommand<SaveLicenseResultsDto, IdentityContextDto<SaveLicenseArgumentsDto>>
    {
        /// <summary>
        /// Save a license.
        /// </summary>
        /// <param name="parameters">Broker identifier and the license to save.</param>
        /// <returns>License that was saved.</returns>
        public SaveLicenseResultsDto Execute(IdentityContextDto<SaveLicenseArgumentsDto> parameters)
        {
            ILicenseRepository      repository = RegistryFactory.GetResolver().Resolve<ILicenseRepository>();
            IdentityDto             identity   = parameters.Identity;
            SaveLicenseArgumentsDto arguments  = parameters.Arguments;

            // Fetch the broker and principal user.
            IBroker    broker    = Broker.Get(identity.Name, identity.AuthorityName, arguments.Broker);
            IPrincipal principal = Principal.Get(identity.Name, identity.AuthorityName);

            // Fetch the product.
            Product product = repository.Fetch_Product(arguments.License.Product);
            if (product == null)
            {
                throw new ArgumentException("Invalid product.");
            }

            // Save the license.            
            License license = repository.Save_License(principal, broker, Mapper.Map(arguments.License, product));
            
            // Fetch all of the client's licenses, sorted by product, device.            
            var licenses = from l
                           in repository.Fetch_Licenses(broker)
                           orderby l.Product, l.DeviceType
                           select l;

            return new SaveLicenseResultsDto
            {
                Arguments = arguments,
                Broker    = new BrokerDto
                {
                    AuthorityName = broker.AuthorityName,
                    Client        = Clients.Commands.Impl.Mapper.Map(broker.Client),
                    Handle        = broker.Handle
                },
                License = Mapper.Map(license),
                Licenses = Mapper.Map(licenses)
            };
        }
    }
}
