﻿using System;
using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;

namespace FirstLook.Client.DomainModel.Licenses.Commands.Impl
{
    /// <summary>
    /// Factory for licensing commands.
    /// </summary>
    public class CommandFactory : ICommandFactory
    {
        #region Administration: Licenses

        /// <summary>
        /// Create a command to retrieve all the licenses available to a broker.
        /// </summary>
        /// <returns>Command to retrieve a broker's licenses.</returns>
        public ICommand<LicensesResultsDto, IdentityContextDto<LicensesArgumentsDto>> CreateLicensesCommand()
        {
            return new LicensesCommand();
        }

        /// <summary>
        /// Create a command to retrieve a particular license.
        /// </summary>
        /// <returns>Command to retrieve a broker's license.</returns>
        public ICommand<LicenseResultsDto, IdentityContextDto<LicenseArgumentsDto>> CreateLicenseCommand()
        {
            return new LicenseCommand();
        }

        /// <summary>
        /// Create a command to save a license.
        /// </summary>
        /// <returns>Command to save a license.</returns>
        public ICommand<SaveLicenseResultsDto, IdentityContextDto<SaveLicenseArgumentsDto>> CreateSaveLicenseCommand()
        {
            return new SaveLicenseCommand();
        }

        /// <summary>
        /// Create a command to revoke a license.
        /// </summary>
        /// <returns></returns>
        public ICommand<RevokeLicenseResultsDto, IdentityContextDto<RevokeLicenseArgumentsDto>> CreateRevokeLicenseCommand()
        {
            return new RevokeLicenseCommand();
        }

        /// <summary>
        /// Create a command to change the license model of a license.
        /// </summary>
        /// <returns>Command to change a license's model.</returns>
        public ICommand<ChangeLicenseModelResultsDto, IdentityContextDto<ChangeLicenseModelArgumentsDto>> CreateChangeLicenseModelCommand()
        {
            return new ChangeLicenseModelCommand();
        }

        #endregion

        #region Administration: Leases

        /// <summary>
        /// Create a command to get all leases tied a license.
        /// </summary>
        /// <returns>Command to get a license's leases.</returns>
        public ICommand<LeasesResultsDto, IdentityContextDto<LeasesArgumentsDto>> CreateLeasesCommand()
        {
            return new LeasesCommand();
        }

        /// <summary>
        /// Create a command to get a particular lease.
        /// </summary>
        /// <returns>Command to retrieve a lease.</returns>
        public ICommand<LeaseResultsDto, IdentityContextDto<LeaseArgumentsDto>> CreateLeaseCommand()
        {
            return new LeaseCommand();
        }

        /// <summary>
        /// Create a command to create a lease.
        /// </summary>
        /// <returns>Command to create a lease.</returns>
        public ICommand<CreateLeaseResultsDto, IdentityContextDto<CreateLeaseArgumentsDto>> CreateCreateLeaseCommand()
        {
            return new CreateLeaseCommand();
        }

        /// <summary>
        /// Create a command to revoke a lease.
        /// </summary>
        /// <returns>Command to revoke a lease.</returns>
        public ICommand<RevokeLeaseResultsDto, IdentityContextDto<RevokeLeaseArgumentsDto>> CreateRevokeLeaseCommand()
        {
            return new RevokeLeaseCommand();
        }        

        #endregion

        #region Enforcement

        /// <summary>
        /// Create a command to retrieve all tokens available to a user on a device.
        /// </summary>
        /// <returns>Command to retrieve tokens available to a user on a device.</returns>
        public ICommand<TokensResultsDto, IdentityContextDto<TokensArgumentsDto>> CreateTokensCommand()
        {
            return new TokensCommand();
        }

        /// <summary>
        /// Create a command to revoke all of a users tokens.
        /// </summary>
        /// <returns>Command to revoke tokens.</returns>
        public ICommand<RevokeTokensResultsDto, IdentityContextDto<RevokeTokensArgumentsDto>> CreateRevokeTokensCommand()
        {
            return new RevokeTokensCommand();
        }

        #endregion

        public ICommand<TResults, TArguments> DecorateCommand<TResults, TArguments>(ICommand<TResults, TArguments> command, IIdentityContextDto context)
            where TResults : IAuthorizationResultsDto, new()
            where TArguments : IAuthorizationArgumentsDto
        {
            return new AuthorizationCommandA<TResults, TArguments>(command, context);
        }

        public ICommand<TResults, TArguments> DecorateCommand<TResults, TArguments, TWrapped>(ICommand<TResults, TArguments> command)
            where TResults : IAuthorizationResultsDto, new()
            where TArguments : IIdentityContextDto<TWrapped>
            where TWrapped : IAuthorizationArgumentsDto
        {
            return new AuthorizationCommandB<TResults, TArguments, TWrapped>(command);
        }
    }
}
