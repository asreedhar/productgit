﻿using FirstLook.Client.DomainModel.Common.Commands.TransferObjects.Envelopes;
using FirstLook.Client.DomainModel.Licenses.Commands.TransferObjects.Envelopes;
using FirstLook.Common.Core.Command;

namespace FirstLook.Client.DomainModel.Licenses.Commands
{
    /// <summary>
    /// Interface for a licensing command factory.
    /// </summary>
    public interface ICommandFactory
    {
        #region Administration: Licenses

        /// <summary>
        /// Create a command to retrieve all the licenses available to a broker.
        /// </summary>
        /// <returns>Command to retrieve a broker's licenses.</returns>
        ICommand<LicensesResultsDto, IdentityContextDto<LicensesArgumentsDto>> CreateLicensesCommand();

        /// <summary>
        /// Create a command to retrieve a particular license.
        /// </summary>
        /// <returns>Command to retrieve a broker's license.</returns>
        ICommand<LicenseResultsDto, IdentityContextDto<LicenseArgumentsDto>> CreateLicenseCommand();

        /// <summary>
        /// Create a command to save a license.
        /// </summary>
        /// <returns>Command to save a license.</returns>
        ICommand<SaveLicenseResultsDto, IdentityContextDto<SaveLicenseArgumentsDto>> CreateSaveLicenseCommand();

        /// <summary>
        /// Create a command to revoke a license.
        /// </summary>
        /// <returns></returns>
        ICommand<RevokeLicenseResultsDto, IdentityContextDto<RevokeLicenseArgumentsDto>> CreateRevokeLicenseCommand();

        /// <summary>
        /// Create a command to change the license model of a license.
        /// </summary>
        /// <returns>Command to change a license's model.</returns>
        ICommand<ChangeLicenseModelResultsDto, IdentityContextDto<ChangeLicenseModelArgumentsDto>> CreateChangeLicenseModelCommand();

        #endregion

        #region Administration: Leases

        /// <summary>
        /// Create a command to get all leases tied a license.
        /// </summary>
        /// <returns>Command to get a license's leases.</returns>
        ICommand<LeasesResultsDto, IdentityContextDto<LeasesArgumentsDto>> CreateLeasesCommand();

        /// <summary>
        /// Create a command to get a particular lease.
        /// </summary>
        /// <returns>Command to retrieve a lease.</returns>
        ICommand<LeaseResultsDto, IdentityContextDto<LeaseArgumentsDto>> CreateLeaseCommand();

        /// <summary>
        /// Create a command to create a lease.
        /// </summary>
        /// <returns>Command to create a lease.</returns>
        ICommand<CreateLeaseResultsDto, IdentityContextDto<CreateLeaseArgumentsDto>> CreateCreateLeaseCommand();

        /// <summary>
        /// Create a command to revoke a lease.
        /// </summary>
        /// <returns>Command to revoke a lease.</returns>
        ICommand<RevokeLeaseResultsDto, IdentityContextDto<RevokeLeaseArgumentsDto>> CreateRevokeLeaseCommand();        

        #endregion

        #region Enforcement

        /// <summary>
        /// Create a command to retrieve all tokens available to a user on a device.
        /// </summary>
        /// <returns>Command to retrieve tokens available to a user on a device.</returns>
        ICommand<TokensResultsDto, IdentityContextDto<TokensArgumentsDto>> CreateTokensCommand();

        /// <summary>
        /// Create a command to revoke all of a users tokens.
        /// </summary>
        /// <returns>Command to revoke tokens.</returns>
        ICommand<RevokeTokensResultsDto, IdentityContextDto<RevokeTokensArgumentsDto>> CreateRevokeTokensCommand();

        #endregion

        ICommand<TResults, TArguments> DecorateCommand<TResults, TArguments>(ICommand<TResults, TArguments> command, IIdentityContextDto context)
            where TResults : IAuthorizationResultsDto, new()
            where TArguments : IAuthorizationArgumentsDto;

        ICommand<TResults, TArguments> DecorateCommand<TResults, TArguments, TWrapped>(ICommand<TResults, TArguments> command)
            where TArguments : IIdentityContextDto<TWrapped>
            where TResults : IAuthorizationResultsDto, new()
            where TWrapped : IAuthorizationArgumentsDto;
    }
}
