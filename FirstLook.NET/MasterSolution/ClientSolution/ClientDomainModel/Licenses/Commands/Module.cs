﻿using FirstLook.Client.DomainModel.Licenses.Commands.Impl;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Licenses.Commands
{
    /// <summary>
    /// Module for registering licensing command components.
    /// </summary>
    public class Module : IModule
    {        
        /// <summary>
        /// Register the command factory.
        /// </summary>        
        public void Configure(IRegistry registry)
        {
            registry.Register<ICommandFactory, CommandFactory>(ImplementationScope.Shared);
        }        
    }
}
