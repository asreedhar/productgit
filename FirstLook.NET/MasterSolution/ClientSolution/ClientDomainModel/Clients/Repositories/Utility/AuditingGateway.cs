using System;
using System.Data;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Repositories.Gateways;
using FirstLook.Common.Core;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Utility
{
    public abstract class AuditingGateway : GatewayBase
    {
        private readonly AuditGateway _auditGateway = new AuditGateway();

        private readonly AuditRowGateway _auditRowGateway = new AuditRowGateway();

        protected AuditGateway AuditGateway
        {
            get { return _auditGateway; }
        }

        protected AuditRowGateway AuditRowGateway
        {
            get { return _auditRowGateway; }
        }

        protected AuditRow Insert()
        {
            return AuditRowGateway.Insert(true, false, false);
        }

        protected AuditRow Update(AuditRow row)
        {
            return AuditRowGateway.Insert(row, false, true, false);
        }

        protected AuditRow Delete(AuditRow row)
        {
            return AuditRowGateway.Insert(row, false, false, true);
        }

        protected static AuditRow ReadAuditRow(IDataReader reader, string prefix)
        {
            return new AuditRow
            {
                Id = reader.GetInt32(reader.GetOrdinal(prefix + "AuditRowID")),
                ValidFrom = reader.GetDateTime(reader.GetOrdinal(prefix + "ValidFrom")),
                ValidUpTo = reader.GetDateTime(reader.GetOrdinal(prefix + "ValidUpTo")),
                WasInsert = reader.GetBoolean(reader.GetOrdinal(prefix + "WasInsert")),
                WasUpdate = reader.GetBoolean(reader.GetOrdinal(prefix + "WasUpdate")),
                WasDelete = reader.GetBoolean(reader.GetOrdinal(prefix + "WasDelete"))
            };
        }
    }
}