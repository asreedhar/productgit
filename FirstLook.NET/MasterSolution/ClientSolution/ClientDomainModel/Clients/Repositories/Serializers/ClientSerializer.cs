using System.Data;
using FirstLook.Client.DomainModel.Clients.Repositories.Entities;
using FirstLook.Client.DomainModel.Common.Repositories;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Serializers
{
    public class ClientSerializer : Serializer<Entities.Client>
    {
        public override Entities.Client Deserialize(IDataRecord record)
        {
            return new Entities.Client
            {
                ClientType = (ClientType)record.GetByte(record.GetOrdinal("ClientTypeID")),
                Handle = record.GetGuid(record.GetOrdinal("Handle")),
                Id = record.GetInt32(record.GetOrdinal("ClientId"))
            };
        }
    }
}
