using System.Data;
using FirstLook.Client.DomainModel.Clients.Repositories.Entities;
using FirstLook.Client.DomainModel.Common.Repositories;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Serializers
{
    public class UserSerializer : Serializer<User>
    {
        public override User Deserialize(IDataRecord record)
        {
            return new User
            {
                Handle = record.GetGuid(record.GetOrdinal("Handle")),
                Id = record.GetInt32(record.GetOrdinal("UserId")),
                UserType = (UserType)record.GetByte(record.GetOrdinal("UserTypeId"))
            };
        }
    }
}
