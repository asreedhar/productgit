using System.Data;
using FirstLook.Client.DomainModel.Clients.Model.Authorities.Dealers;
using FirstLook.Client.DomainModel.Common.Repositories;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Serializers
{
    public class MemberSerializer : Serializer<Member>
    {
        public override Member Deserialize(IDataRecord record)
        {
            int memberType = record.GetInt32(record.GetOrdinal("MemberType"));

            int id = record.GetInt32(record.GetOrdinal("Id"));

            string userName = record.GetString(record.GetOrdinal("UserName"));

            string firstName = record.GetString(record.GetOrdinal("FirstName"));

            string lastName = record.GetString(record.GetOrdinal("LastName"));

            return new Member
            {
                Id = id,
                UserName = userName,
                FirstName = firstName,
                LastName = lastName,
                MemberType = (MemberType)memberType
            };
        }
    }
}