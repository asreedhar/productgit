using System.Data;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Common.Repositories;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Serializers
{
    public class AuditSerializer : Serializer<Audit>
    {
        public override Audit Deserialize(IDataRecord record)
        {
            return new Audit
            {
                Date = record.GetDateTime(record.GetOrdinal("AuditDate")),
                Id = record.GetInt32(record.GetOrdinal("AuditId")),
                User = record.GetInt32(record.GetOrdinal("UserId"))
            };
        }
    }
}
