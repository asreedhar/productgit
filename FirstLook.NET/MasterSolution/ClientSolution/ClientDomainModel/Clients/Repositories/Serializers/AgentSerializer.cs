﻿using System.Data;
using FirstLook.Client.DomainModel.Clients.Model.Authorities.Agents;
using FirstLook.Client.DomainModel.Clients.Repositories.Entities;
using FirstLook.Client.DomainModel.Common.Repositories;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Serializers
{
    public class AgentSerializer : Serializer<Agent>
    {
        public override Agent Deserialize(IDataRecord record)
        {
            int id = record.GetInt32(record.GetOrdinal("UserId"));

            string userName = record.GetString(record.GetOrdinal("UserName"));

            string displayName = record.GetString(record.GetOrdinal("DisplayName"));

            return new Agent
            {
                Id = id,
                UserName = userName,
                FirstName = displayName,
                LastName = string.Empty,
                Type = UserType.System
            };
        }
    }
}
