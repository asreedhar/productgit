﻿using System.Data;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Common.Repositories;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Serializers
{
    public class AssociationSerializer : Serializer<Association>
    {
        public override Association Deserialize(IDataRecord record)
        {
            return new Association
            {
                AuditRow = new AuditRow
                {
                    Id        = record.GetInt32(record.GetOrdinal("AuditRowID")),
                    ValidFrom = record.GetDateTime(record.GetOrdinal("ValidFrom")),
                    ValidUpTo = record.GetDateTime(record.GetOrdinal("ValidUpTo")),
                    WasInsert = record.GetBoolean(record.GetOrdinal("WasInsert")),
                    WasUpdate = record.GetBoolean(record.GetOrdinal("WasUpdate")),
                    WasDelete = record.GetBoolean(record.GetOrdinal("WasDelete"))                    
                },
                Id = record.GetInt32(record.GetOrdinal("AssociationID"))
            };
        }
    }
}
