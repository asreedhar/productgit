using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Clients.Model.Authorities.Sellers;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Data;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Serializers
{
    public class SellerSerializer : Serializer<Seller>
    {
        public override Seller Deserialize(IDataRecord record)
        {
            int id = record.GetInt32(record.GetOrdinal("Id"));

            string name = record.GetString(record.GetOrdinal("Name"));

            return new Seller
            {
                Address = new Address
                {
                    Line1 = DataRecord.GetString(record, "AddressLine1"),
                    Line2 = DataRecord.GetString(record, "AddressLine2"),
                    City = DataRecord.GetString(record, "City"),
                    State = DataRecord.GetString(record, "State"),
                    ZipCode = DataRecord.GetString(record, "ZipCode")
                },
                Id = id,
                Name = name,
                OrganizationName = string.Empty
            };
        }
    }
}