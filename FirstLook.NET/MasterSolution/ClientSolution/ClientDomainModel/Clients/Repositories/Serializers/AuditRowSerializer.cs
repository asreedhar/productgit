using System.Data;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Common.Repositories;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Serializers
{
    public class AuditRowSerializer : Serializer<AuditRow>
    {
        private readonly string _prefix;

        public AuditRowSerializer() : this(string.Empty)
        {
        }

        public AuditRowSerializer(string prefix)
        {
            _prefix = prefix;
        }

        public override AuditRow Deserialize(IDataRecord record)
        {
            return new AuditRow
            {
                Id = record.GetInt32(record.GetOrdinal(_prefix + "AuditRowID")),
                ValidFrom = record.GetDateTime(record.GetOrdinal(_prefix + "ValidFrom")),
                ValidUpTo = record.GetDateTime(record.GetOrdinal(_prefix + "ValidUpTo")),
                WasInsert = record.GetBoolean(record.GetOrdinal(_prefix + "WasInsert")),
                WasUpdate = record.GetBoolean(record.GetOrdinal(_prefix + "WasUpdate")),
                WasDelete = record.GetBoolean(record.GetOrdinal(_prefix + "WasDelete"))
            };
        }
    }
}
