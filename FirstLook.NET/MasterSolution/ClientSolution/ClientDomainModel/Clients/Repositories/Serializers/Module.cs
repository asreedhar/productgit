using System;
using FirstLook.Client.DomainModel.Clients.Model.Auditing;
using FirstLook.Client.DomainModel.Clients.Model.Authorities.Agents;
using FirstLook.Client.DomainModel.Clients.Model.Authorities.Dealers;
using FirstLook.Client.DomainModel.Clients.Model.Authorities.Sellers;
using FirstLook.Client.DomainModel.Clients.Repositories.Entities;
using FirstLook.Client.DomainModel.Common.Repositories;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Serializers
{
    [Serializable]
    public class Module : IModule
    {
        public void Configure(IRegistry registry)
        {
            registry.Register<ISerializer<Agent>, AgentSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<Association>, AssociationSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<AuditRow>, AuditRowSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<Audit>, AuditSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<Dealer>, DealerSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<Member>, MemberSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<Seller>, SellerSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<User>, UserSerializer>(ImplementationScope.Shared);
            registry.Register<ISerializer<Entities.Client>, ClientSerializer>(ImplementationScope.Shared);
        }
    }
}
