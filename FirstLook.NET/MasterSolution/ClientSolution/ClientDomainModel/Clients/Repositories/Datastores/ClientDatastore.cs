using System;
using System.Data;
using System.Reflection;
using FirstLook.Client.DomainModel.Clients.Repositories.Entities;
using FirstLook.Common.Core.Data;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Datastores
{
    /// <summary>
    /// A client datastore. Performs tasks on: clients; the mapping of clients to specific client types; users; and 
    /// the mapping of users to specific user types.
    /// </summary>    
    public class ClientDatastore : SessionDataStore, IClientDatastore
    {        
        #region Clients

        /// <summary>
        /// Get the identifying details of the client with the given handle.
        /// </summary>
        /// <param name="handle">Client handle.</param>
        /// <returns>Data reader with client identifiers.</returns>
        public IDataReader Client(Guid handle)
        {
            const string queryName = Datastore.Prefix + ".ClientDatastore_Client_Fetch_Handle.txt";

            return Query(
                new[] { "Client" },
                queryName,
                new Parameter("Handle", handle, DbType.Guid));
        }

        /// <summary>
        /// For the given identifier that is specific to a type of client (e.g dealer identifier for dealers, seller
        /// identifier for sellers, etc), get the client identifying details.        
        /// </summary>        
        /// <param name="type">Client type (e.g. Dealer, Seller).</param>
        /// <param name="id">Client type-specific identifier.</param>
        /// <returns>Data reader with client identifiers.</returns>
        public IDataReader Client(ClientType type, int id)
        {                       
            // Dealers.
            if (type == ClientType.Dealer)
            {
                const string queryNameD = Datastore.Prefix + ".ClientDatastore_Client_Fetch_Dealer.txt";

                return Query(
                    new[] {"Client"},
                    queryNameD,
                    new Parameter("ClientTypeID", (int) type, DbType.Int32),
                    new Parameter("DealerID", id, DbType.Int32));
            }
            // Sellers.
            if (type == ClientType.Dealer)
            {
                const string queryNameS = Datastore.Prefix + ".ClientDatastore_Client_Fetch_Seller.txt";

                return Query(
                    new[] {"Client"},
                    queryNameS,
                    new Parameter("ClientTypeID", (int) type, DbType.Int32),
                    new Parameter("SellerID", (int) type, DbType.Int32));
            }

            throw new ArgumentOutOfRangeException("type", type, "Unsupported ClientType");
        }        

        /// <summary>
        /// Insert a new client of the given type.
        /// </summary>
        /// <param name="type">Client type.</param>
        /// <returns>Data reader with client values.</returns>
        public IDataReader Client_Insert(ClientType type)
        {
            const string queryNameI = Datastore.Prefix + ".ClientDatastore_Client_Insert.txt";

            const string queryNameF = Datastore.Prefix + ".ClientDatastore_Client_Fetch_Identity.txt";

            NonQuery(queryNameI, new Parameter("ClientTypeID", (int)type, DbType.Int32));

            return Query(new[] { "Client" }, queryNameF);
        }

        /// <summary>
        /// Fetch the identifier of the dealer that is mapped to the client with the given identifier.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <returns>Data reader with a dealer identifier.</returns>
        public IDataReader Dealer(int clientId)
        {
            const string queryName = Datastore.Prefix + ".ClientDatastore_Client_Dealer_Fetch_Client.txt";

            return Query(
                new[] { "Client_Dealer" },
                queryName,
                new Parameter("ClientID", clientId, DbType.Int32));
        }

        /// <summary>
        /// Map a client to a dealer via the given identifiers.        
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="dealerId">Dealer identifier.</param>
        public void Dealer_Insert(int clientId, int dealerId)
        {
            const string queryName = Datastore.Prefix + ".ClientDatastore_Client_Dealer_Insert.txt";

            NonQuery(
                queryName,
                new Parameter("ClientID", clientId, DbType.Int32),
                new Parameter("DealerID", dealerId, DbType.Int32));
        }

        /// <summary>
        /// Fetch the identifier of the seller that is mapped to the client witht he given identifier.        
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <returns>Data reader with a seller identifier.</returns>
        public IDataReader Seller(int clientId)
        {
            const string queryName = Datastore.Prefix + ".ClientDatastore_Client_Seller_Fetch_Client.txt";

            return Query(
                new[] { "Client_Seller" },
                queryName,
                new Parameter("ClientID", clientId, DbType.Int32));
        }

        /// <summary>
        /// Map a client to a seller via the given identifiers.        
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="sellerId">Seller identifier.</param>
        public void Seller_Insert(int clientId, int sellerId)
        {
            const string queryName = Datastore.Prefix + ".ClientDatastore_Client_Seller_Insert.txt";

            NonQuery(
                queryName,
                new Parameter("ClientID", clientId, DbType.Int32),
                new Parameter("SellerID", sellerId, DbType.Int32));
        }

        #endregion

        #region Users

        /// <summary>
        /// Get the identifying details of the user with the given handle.
        /// </summary>
        /// <param name="handle">User handle.</param>
        /// <returns>Data reader with user identifiers.</returns>
        public IDataReader User(Guid handle)
        {
            const string queryName = Datastore.Prefix + ".ClientDatastore_User_Fetch_Handle.txt";

            return Query(
                new[] { "Client" },
                queryName,
                new Parameter("Handle", handle, DbType.Guid));
        }

        /// <summary>
        /// For the given identifier that is specific to a type of user (e.g member identifier for dealers, system 
        /// identifier for system accounts, etc), get the user identifying details.   
        /// </summary>        
        /// <param name="userType">User type (e.g. Member, System)</param>
        /// <param name="id">User type-specific identifier.</param>
        /// <returns>Data reader with user values.</returns>
        public IDataReader User(UserType userType, int id)
        {
            // Member.
            if (userType == UserType.Member)
            {
                const string queryNameD = Datastore.Prefix + ".ClientDatastore_User_Fetch_Member.txt";

                return Query(
                    new[] { "User" },
                    queryNameD,
                    new Parameter("UserTypeID", (int)userType, DbType.Int32),
                    new Parameter("MemberID", id, DbType.Int32));
            }
            // System.
            if (userType == UserType.System)
            {
                const string queryNameS = Datastore.Prefix + ".ClientDatastore_User_Fetch_System.txt";

                return Query(
                    new[] { "User" },
                    queryNameS,
                    new Parameter("UserTypeID", (int)userType, DbType.Int32),
                    new Parameter("UserID", id, DbType.Int32));
            }

            throw new ArgumentOutOfRangeException("userType", userType, "Unsupported UserType");
        }

        /// <summary>
        /// Insert a new user of the given type.
        /// </summary>
        /// <param name="type">User type.</param>
        /// <returns>Data reader with user values.</returns>
        public IDataReader User_Insert(UserType type)
        {
            const string queryNameI = Datastore.Prefix + ".ClientDatastore_User_Insert.txt";

            const string queryNameF = Datastore.Prefix + ".ClientDatastore_User_Fetch_Identity.txt";

            NonQuery(queryNameI, new Parameter("UserTypeID", (byte)type, DbType.Byte));

            return Query(new[] { "User" }, queryNameF);
        }
        
        /// <summary>
        /// Fetch the identifier of the dealer member that is mapped to the user with the given identifier.
        /// </summary>
        /// <param name="userId">User identifier.</param>
        /// <returns>Data reader with a dealer member identifier.</returns>
        public IDataReader Member(int userId)
        {
            const string queryName = Datastore.Prefix + ".ClientDatastore_User_Member_Fetch_User.txt";

            return Query(
                new[] { "User_Member" },
                queryName,
                new Parameter("UserID", userId, DbType.Int32));
        }

        /// <summary>
        /// Map a user to a dealer member via the given identifiers.        
        /// </summary>
        /// <param name="userId">User identifier.</param>
        /// <param name="memberId">Dealer member identifier.</param>
        public void Member_Insert(int userId, int memberId)
        {
            const string queryName = Datastore.Prefix + ".ClientDatastore_User_Member_Insert.txt";

            NonQuery(
                queryName,
                new Parameter("UserID", userId, DbType.Int32),
                new Parameter("MemberID", memberId, DbType.Int32));
        }

        /// <summary>
        /// Fetch the identifier of the system account that is mapped to the user with the given identifier.        
        /// </summary>
        /// <param name="userId">System user identifier.</param>
        /// <returns>Data reader with a system user identifier.</returns>
        public IDataReader System(int userId)
        {
            const string queryName = Datastore.Prefix + ".ClientDatastore_User_System_Fetch_User.txt";

            return Query(
                new[] { "User_System" },
                queryName,
                new Parameter("UserID", userId, DbType.Int32));
        }

        /// <summary>
        /// Fetch the identifier of the system account that has the given username.        
        /// </summary>
        /// <param name="userName">System username.</param>
        /// <returns>Data reader with a system user identifier.</returns>
        public IDataReader System(string userName)
        {
            const string queryName = Datastore.Prefix + ".ClientDatastore_User_System_Fetch_UserName.txt";

            return Query(
                new[] { "User_System" },
                queryName,
                new Parameter("UserName", userName, DbType.String));
        }

        #endregion

        #region Miscellaneous

        /// <summary>
        /// Get the type of the current assembly.
        /// </summary>
        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        #endregion
    }
}
