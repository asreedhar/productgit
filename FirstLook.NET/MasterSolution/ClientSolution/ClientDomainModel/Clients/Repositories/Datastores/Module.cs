﻿using System;
using FirstLook.Common.Core.Registry;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Datastores
{
    /// <summary>
    /// Module for registering necessary client repository items.
    /// </summary>
    [Serializable]
    public class Module : IModule
    {
        /// <summary>
        /// Register the client datastores.
        /// </summary>
        /// <param name="registry"></param>
        public void Configure(IRegistry registry)
        {
            registry.Register<IAuditDatastore,     AuditDatastore>(ImplementationScope.Shared);
            registry.Register<IClientDatastore,    ClientDatastore>(ImplementationScope.Shared);
            registry.Register<IDealerDatastore,    DealerDatastore>(ImplementationScope.Shared);
            registry.Register<ISellerDatastore,    SellerDatastore>(ImplementationScope.Shared);
            registry.Register<IReferenceDatastore, ReferenceDatastore>(ImplementationScope.Shared);            
        }
    }
}
