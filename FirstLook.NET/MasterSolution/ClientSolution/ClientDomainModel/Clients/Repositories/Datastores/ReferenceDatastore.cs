﻿using System.Data;
using System.Reflection;
using FirstLook.Common.Core.Data;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Datastores
{
    /// <summary>
    /// A reference data datastore. The reference data maintains identifiers to components that are defined in other 
    /// databases.
    /// </summary>
    public class ReferenceDatastore : SessionDataStore, IReferenceDatastore
    {        
        #region Dealer

        /// <summary>
        /// Fetch the dealer row from the reference data that has the given identifier.
        /// </summary>
        /// <param name="id">Dealer identifier.</param>
        /// <returns>Data reader with the dealer identifier.</returns>
        public IDataReader Dealer_Fetch(int id)
        {
            const string fetch = Datastore.Prefix + ".ReferenceDatastore_Dealer_Fetch_Id.txt";

            Parameter parameter = new Parameter("DealerID", id, DbType.Int32);

            return Query(new[] { "Dealer" }, fetch, parameter);
        }

        /// <summary>
        /// Insert the given dealer identifier into the reference data.
        /// </summary>
        /// <param name="id">Dealer identifier.</param>
        /// <returns>Data reader with the dealer identifier.</returns>
        public IDataReader Dealer_Insert(int id)
        {
            const string insert = Datastore.Prefix + ".ReferenceDatastore_Dealer_Insert.txt";

            Parameter parameter = new Parameter("DealerID", id, DbType.Int32);

            NonQuery(insert, parameter);

            const string fetch = Datastore.Prefix + ".ReferenceDatastore_Dealer_Fetch_Id.txt";

            return Query(new[] { "Dealer" }, fetch, parameter);
        }

        #endregion

        #region Seller

        /// <summary>
        /// Fetch the seller row from the reference data that has the given identifier.
        /// </summary>
        /// <param name="id">Seller identifier.</param>
        /// <returns>Data reader with the seller identifier.</returns>
        public IDataReader Seller_Fetch(int id)
        {
            const string fetch = Datastore.Prefix + ".ReferenceDatastore_Seller_Fetch_Id.txt";

            Parameter parameter = new Parameter("SellerID", id, DbType.Int32);

            return Query(new[] { "Seller" }, fetch, parameter);
        }

        /// <summary>
        /// Insert the given seller identifier into the reference data.
        /// </summary>
        /// <param name="id">Seller identifier.</param>
        /// <returns>Data reader with the seller identifier.</returns>
        public IDataReader Seller_Insert(int id)
        {
            const string insert = Datastore.Prefix + ".ReferenceDatastore_Seller_Insert.txt";

            Parameter parameter = new Parameter("SellerID", id, DbType.Int32);

            NonQuery(insert, parameter);

            const string fetch = Datastore.Prefix + ".ReferenceDatastore_Seller_Fetch_Id.txt";

            return Query(new[] { "Seller" }, fetch, parameter);
        }

        #endregion

        #region Member

        /// <summary>
        /// Fetch the member row from the reference data that has the given identifier.
        /// </summary>
        /// <param name="id">Member identifier.</param>
        /// <returns>Data reader with the member identifier.</returns>
        public IDataReader Member_Fetch(int id)
        {
            const string fetch = Datastore.Prefix + ".ReferenceDatastore_Member_Fetch_Id.txt";

            Parameter parameter = new Parameter("MemberID", id, DbType.Int32);

            return Query(new[] { "Member" }, fetch, parameter);
        }

        /// <summary>
        /// Insert the given member identifier into the reference data.
        /// </summary>
        /// <param name="id">Member identifier.</param>
        /// <returns>Data reader with the member identifier.</returns>
        public IDataReader Member_Insert(int id)
        {
            const string insert = Datastore.Prefix + ".ReferenceDatastore_Member_Insert.txt";

            Parameter parameter = new Parameter("MemberID", id, DbType.Int32);

            NonQuery(insert, parameter);

            const string fetch = Datastore.Prefix + ".ReferenceDatastore_Member_Fetch_Id.txt";

            return Query(new[] { "Member" }, fetch, parameter);
        }

        #endregion
      
        #region Miscellaneous

        /// <summary>
        /// Get the type of the current assembly.
        /// </summary>
        protected override Assembly Assembly
        {
            get { return GetType().Assembly; }
        }

        #endregion
    }
}
