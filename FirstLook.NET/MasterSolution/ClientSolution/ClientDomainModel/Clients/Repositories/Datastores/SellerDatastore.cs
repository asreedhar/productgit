﻿using System.Data;
using FirstLook.Client.DomainModel.Clients.Model;
using FirstLook.Client.DomainModel.Common.Model;

namespace FirstLook.Client.DomainModel.Clients.Repositories.Datastores
{
    /// <summary>
    /// A seller datastore.
    /// </summary>
    public class SellerDatastore : Datastore, ISellerDatastore
    {
        /// <summary>
        /// Fetch the details of the seller with the given seller identifier.
        /// </summary>
        /// <param name="id">Seller identifier.</param>
        /// <returns>Data reader with seller details.</returns>
        public IDataReader Seller(int id)
        {
            const string queryName = Prefix + ".SellerDatastore_Seller_Fetch_Id.txt";

            return Query(new[] { "Seller" }, queryName, new Parameter("Id", id, DbType.Int32));
        }

        /// <summary>
        /// Fetch a paginated list of sellers that match the given example.
        /// </summary>
        /// <param name="userName">Username. Ignored.</param>
        /// <param name="example">Example seller to match by.</param>
        /// <param name="pagination">Pagination arguments.</param>
        /// <returns>Data reader with a paginated list of sellers.</returns>
        public IDataReader Sellers(string userName, IClient example, PageArguments pagination)
        {
            const string queryName = Prefix + ".SellerDatastore_Seller_Fetch_Example.txt";

            return Query(new[] { "Seller", "TotalRowCount" }, queryName, Map(userName, example, pagination));
        }

        #region Miscellaneous

        /// <summary>
        /// The seller database is Market.
        /// </summary>
        protected const string Name = "Market";

        /// <summary>
        /// The seller database is Market.
        /// </summary>
        protected override string DatabaseName
        {
            get { return Name; }
        }

        #endregion
    }
}
